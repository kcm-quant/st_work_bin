function date = daysadd(d1,num,basis)
%DAYSADD Date from a starting date for any day count basis.
%   DAYSADD returns a date, NUM number of days from D1, using the given
%   day count BASIS. Enter dates as serial date numbers or date strings.
%
%   Date = daysadd(D1, NUM)
%   Date = daysadd(D1, NUM, BASIS)
%
%   Optional Inputs: BASIS
%
%   Inputs:
%      D1    - NDATESx1 or 1xNDATES matrix of serial date or date strings
%      NUM   - NNUMx1 or 1xNNUM matrix of integer number of days.
%
%   Optional Inputs:
%      BASIS - NBASISx1 or 1xNBASIS matrix of day-count basis.
%
%      Valid Basis are:
%            0 = actual/actual (default)
%            1 = 30/360 SIA
%            2 = actual/360
%            3 = actual/365
%            4 - 30/360 PSA
%            5 - 30/360 ISDA
%            6 - 30/360 European
%            7 - act/365 Japanese
%            8 - act/act ISMA
%            9 - act/360 ISMA
%           10 - act/365 ISMA
%           11 - 30/360 ISMA
%           12 - act/365 ISDA
%           13 - bus/252
%
%   Output:
%      Date  -  Nx1 matrix of serial dates of new date
%
%   Note: Due to the nature of the 30/360 day-count basis, it may not be
%        possible to find the exact date, NUM number of days away because
%        of a known disconuity in the method of counting days.  A warning
%        will be displayed in that event.
%
%  Example:
%
%      startDt = datenum('5-Feb-2004');
%      num     = 26;
%      basis   = 1;
%
%      newDt   = daysadd(startDt,num,basis)
%
%      newDt   =
%               732007
%
%  See also DAYSDIF

% Copyright 1995-2006 The MathWorks, Inc.
% $Revision: 1$   $Date: 02/17/2012 08:19:09 PM$

% Error check
if nargin < 2
    error('finance:daysadd:invalidNumberOfInputs', ...
        'Please enter D1 and NUM.');
end
if ischar(d1) || iscell(d1)
    d1 = datenum(d1);
end

if any(rem(num,1)>0)
    error('finance:daysadd:invalidNumber', ...
        'NUM must be an integer number of days.');
end


if nargin < 3
    basis = zeros(size(d1));
end

if any(~isvalidbasis(basis))
    error('finance:daysadd:invalidBasis',...
        'Invalid day count basis specified.');
end

% columnize inputs
d1 = d1(:);
num = num(:);
basis = basis(:);

sz = [size(d1); size(num); size(basis)];
if numel(d1) == 1
    d1 = repmat(d1,max(sz(:,1)), max(sz(:,2)));
end
if numel(num) == 1
    num = repmat(num,max(sz(:,1)), max(sz(:,2)));
end
if numel(basis) == 1
    basis = repmat(basis,max(sz(:,1)), max(sz(:,2)));
end

if checksiz([size(d1); size(num); size(basis)], mfilename)
    return
end

% Add the number of days based on basis
date = zeros(size(d1));
% act/act, act/360, act/365, act/act ISMA
i = (basis == 0 | basis == 2 | basis == 3 | basis == 8 | basis == 9 ...
    | basis == 10 | basis == 12);
if any(i)
    date(i) = d1(i) + num(i);
end

% 30/360 SIA, 30/360 PSA, 30/360 ISDA, 30E/360
i = (basis == 1 | basis == 4 | basis == 5 | basis == 6 | basis == 11);
if any(i)
    % calculate years
    years = fix(num(i)./360);
    
    % estimate months
    mNum = rem(num(i),360);
    months = fix(mNum./30);
    
    % estimate date
    days = rem(num(i),30);
    newDt = day(d1(i)) + days;
    
    % calculate new year
    years = year(d1(i)) + years;
    
    % realign year and month if at boundary
    newMnth = month(d1(i))+months;
    if (newMnth>12)
        newMnth = newMnth-12;
        years = years + 1;
    end
    
    % determine if current month is February
    isfeb = newMnth==2;
    
    % perform calculation based on a 30 day month
    % with special case for February
    isFix = ((newDt >= 28 & isfeb) | (newDt > 30));
    
    % realign dates that requires fixing
    if any(isFix)
        newMnth(isFix) = newMnth(isFix) + 1;
        if newMnth(isFix) > 12
            newMnth(isFix) = newMnth(isFix) - 12;
            years(isFix) = years(isFix) + 1;
        end
        % estimate calculations
        if any(isFix)
            newDt(isFix) = newDt(isFix) - day(eomdate(years(isFix),2));
        else
            newDt(isFix) = newDt(isFix) - 30;
        end
    end
    
    % assemble new date
    tempD = datevec(d1(i));
    tempD(:,1) = years;
    tempD(:,2) = newMnth;
    tempD(:,3) = newDt;
    
    % Due to the nature of 30/360 days counts basis, it may not be possible
    % there may be a discontinuity in the counting of days.  This makes it
    % impossible to find the actual required number of days in the past or
    % future.  A check is made here and a warning is display in that case.
    match = false;
    result = datenum(tempD);
    daysDifference = daysdif(d1(i),result,basis(i));
    if daysDifference==num(i)
        match = true;
    else
        misIdx = ~(daysDifference==num(i));
        for idx=-3:3
            estimDt = datenum(tempD(misIdx,:));
            result(misIdx) = estimDt + idx;
            misIdx = ~(daysdif(d1(i),result,basis(i))==num(i));
            if all(~misIdx)
                match = true;
                break;
            elseif daysdif(d1(i),result,basis(i))>=num(i)
                break;
            end
        end
    end
    
    if ~match
        warning('finance:daysadd:noSuchDate',...
            ['No such number of days in the future exist for a 30/360 ',...
            'day-count basis.']);
    end
    
    date(i) = reshape(result,size(d1(i)));
end

% act/365 japanese
i = (basis == 7);
if any(i)
    % Need cumulative sum of days at beginning of each month
    % to convert months into days.
    daytotal = [0;31;59;90;120;151;181;212;243;273;304;334];
    years = fix(num(i)./365);
    daysl = rem(num(i),365);
    
    curMonth = month(d1(i));
    curDate = day(d1(i));
    
    % resize daytotal
    daytotal = repmat(daytotal,size(curMonth'));
    
    % days in current months
    curDayMnth = daytotal(curMonth);
    
    % change starting point for cumulative sum of days
    for didx = 1:length(curMonth)
        daytotal(:,didx) = daytotal(:,didx) - curDayMnth(didx);
    end
    
    % find negative to reset
    idx = find(daytotal < 0);
    
    % reset sum
    daytotal(idx) = daytotal(idx) + 365;
    
    % create month counter
    mCounter = curMonth;
    
    %create new base day of month
    newDt = curDate;
    
    % determine new month
    if mCounter == 12;mCounter=0;end
    while(daysl > daytotal(mCounter+1))
        % increment month
        mCounter=mCounter+1;
        % decrement days left
        daysl = daysl - daytotal(mCounter);
        % re-align cumulative sum of days
        daytotal = daytotal - daytotal(mCounter);
        % find negative to reset
        idx = find(daytotal < 0);
        % reset sum
        daytotal(idx) = daytotal(idx) + 365;
        % check if pass boundary
        if mCounter>12
            mCounter = mCounter-12;
            years = years+1;
        end
        % check if entered into new year
        if (mCounter==1)
            years = years+1;
        end
    end
    
    % count the days
    if (daysl <= daytotal(mCounter+1))
        newDt = curDate + daysl;
        
        if mCounter == 12;mCounter=0;end
        if (newDt > daytotal(mCounter+1))
            newDt = newDt - daytotal(mCounter+1);
            % increment month
            mCounter = mCounter+1;
            % check if pass boundary
            if mCounter>12
                mCounter = mCounter-12;
                years = years+1;
                % check if entered into new year
            elseif (mCounter==1)
                years = years+1;
            end
        end
    end
    if mCounter == 0;mCounter=12;end
    % reassemble date
    tempD = datevec(d1(i));
    tempD(:,1) = year(d1(i)) + years;
    tempD(:,2) = mCounter;
    tempD(:,3) = newDt;
    
    % reshape
    date(i) = reshape(datenum(tempD),size(d1(i)));
end