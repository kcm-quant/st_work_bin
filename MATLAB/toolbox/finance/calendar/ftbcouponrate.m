function ftbcouponrate()
%
%COUPON RATE refers to the annual percentage rate used to determine the
%coupons payable on a bond. Coupon rates should be entered in decimal form.

disp(' ');
disp('Type "help ftbcouponrate" for an explanation of COUPON RATE');
disp(' ');

%Author(s): C. Bassignani, 03-11-98
%   Copyright 1995-2002 The MathWorks, Inc. 
%$Revision: 1$   $Date: 02/17/2012 08:19:09 PM$
