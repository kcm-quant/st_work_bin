function ftbmaturity()
%
%MATURITY refers to the date upon which a bond matures (i.e. the date upon
%which the holder of the bond may redeem it in exchange for face value). 
%This date may or may not be a coupon date.

disp(' ');
disp('Type "help ftbmaturity" for an explanation of MATURITY');
disp(' ');

%Author(s): C. Bassignani, 03-11-98
%   Copyright 1995-2002 The MathWorks, Inc. 
%$Revision: 1$   $Date: 02/17/2012 08:19:09 PM$

