function ftbnumcouponsremaining()
%
%NUMBER COUPOND REMAINING refers the number of coupon periods remaining until
%maturity.

disp(' ');
disp('Type "help ftbnumcouponsremaining" for an explanation of NUMBER COUPONS REMAINING');
disp(' ');

%Author(s): C. Bassignani, 03-11-98
%   Copyright 1995-2002 The MathWorks, Inc. 
%$Revision: 1$   $Date: 02/17/2012 08:19:09 PM$
