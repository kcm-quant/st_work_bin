function t = today() 
%TODAY Current date. 
%   T = TODAY returns the current date.
%
%   See also CLOCK, DATENUM, DATESTR, NOW. 
 
%    Copyright 1995-2006 The MathWorks, Inc.  
%    $Revision: 1$   $Date: 02/17/2012 08:19:11 PM$ 
 
c = clock; 
t = datenum(c(1),c(2),c(3)); 

