function nd = yeardays(y, basis)
%YEARDAYS Number of days in year.
%
%   ND = YEARDAYS(Y)
%   ND = YEARDAYS(Y, Basis)
%
%   Optional Inputs: Basis
%
%   Inputs:
%   Y     - [Scalar or Vector] of year(s).
%           For example:
%              Y = 1999;
%              Y = 1999:2010;
%
%   Optional Inputs:
%   Basis - [Scalar or Vector] of day count convention(s) to be used.
%           Possible values are:
%              0 - actual/actual   (default)
%              1 - 30/360 SIA
%              2 - actual/360
%              3 - actual/365
%              4 - 30/360 PSA
%              5 - 30/360 ISDA
%              6 - 30/360 European
%              7 - actual/365 Japanese
%              8 - actual/actual ISMA
%              9 - actual/360 ISMA
%             10 - actual/365 ISMA
%             11 - 30/360 ISMA
%             12 - actual/365 ISDA
%             13 - bus/252
%
%   Outputs:
%   ND    - [Scalar or Vector] of the number of days in year Y.
%
%   For example:
%      >> nd = yeardays(2000)
%
%      nd =
%
%         366
%
%   See also DAYS360, DAYS365, DAYSACT, YEAR, YEARFRAC.

%   Copyright 1995-2006 The MathWorks, Inc.
%   $Revision: 1$   $Date: 02/17/2012 08:19:12 PM$

if ischar(y)
    error('Finance:yeardays:invalidInput',...
        'Input is string. Use YEAR to extract years from dates if necessary.');
end

if  nargin < 2 || isempty(basis)
    basis = 0;
end

% Check to see that years is a vector
[i, j] = size(y);
if (i ~= 1 && j ~= 1)
    error('Finance:yeardays:invalidYDim', ...
        'Years, ''y'', must be a scalar or vector.')
end

% Check to make sure that basis is the same length as years.
[ib, jb] = size(basis);
if numel(basis) ~= 1
    if (ib ~= 1 && jb ~= 1)
        error('Finance:yeardays:invalidBasisDim', ...
            'BASIS must be a scalar or vector.')
    end
    
    if numel(basis) ~= numel(y)
        error('Finance:yeardays:invalidNumelBasis', ...
            'The number of BASIS and years must match.')
    end
    
    if any(~isvalidbasis(basis))
        error('Finance:yeardays:invalidBasis', ...
            'Invalid day count basis specified.')
    end
    
else
    
    if any(~isvalidbasis(basis))
        error('Finance:yeardays:invalidBasis',...
            'Invalid day count basis specified.');
    end
    
    % Linearly expand the basis
    basis = repmat(basis, i, j);
end

if checktyp('y', y, 'int', mfilename)
    return
end

% Month and day values to pass to datenum function
mts_dys = ones(size(y));

% Next year values
next_y = y+1;

% Start date
first = datenum(y, mts_dys, mts_dys);

% End date
last = datenum(next_y, mts_dys, mts_dys);

% Initialize nd
nd = nan(size(first));

% Generate number of days in year.
bidx = (basis == 0 | basis == 8 | basis == 10 | basis == 12);
nd(bidx) = daysact(first(bidx), last(bidx));

bidx = (basis == 1 | basis == 2 | basis == 4 | basis == 5 | basis == 6 | basis == 9 | basis == 11);
nd(bidx) = days360(first(bidx), last(bidx));

bidx = (basis == 3 | basis == 7);
nd(bidx) = days365(first(bidx), last(bidx));