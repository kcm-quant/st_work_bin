function [status,message,messageid] = mkdir(varargin)
% MKDIR - Overriden Function in order to cope with Unix rights issues
%   This function will set directories rights to 775
%
%
% Examples:
%
% 
% 
%
% See also: dirup
%    
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   date     :  '19/02/2013'
%   
%   last_checkin_info : $Header: mkdir.m: Revision: 7: Author: malas: Date: 04/22/2013 02:33:33 PM$


if ~ispc 
    
    assert(length(varargin)<3, ...
        ['This Overriden function is currently implemented for only two ' ...
        'inputs. Please extend my features']);
    
    % < Managing the two args, just concat to make it only one
    if length(varargin) == 2 
        target_dir = sprintf('%s/%s', varargin{1}, varargin{2});
    else
        target_dir = varargin{1};
    end
    % >
    
    % < Managing relative path, won't have the same meaning 
    % for matlab and the system
    if  target_dir(1) ~= '/' 
        target_dir = sprintf('%s/%s', cd, target_dir);
    end
    % >
    
    % < As matlab will recursively create the directory tree, 
    % we should recursively chnage rights, starting with the highest
    % directory that does not already exist.
    uppest_non_existing_dir = target_dir;
    up_one_dir = dirup(uppest_non_existing_dir);
    while ~exist(up_one_dir,'dir') && length(up_one_dir) > 1 % 7 for does this directory exist?
        uppest_non_existing_dir = up_one_dir;
        up_one_dir = dirup(uppest_non_existing_dir);
        assert( length(up_one_dir) < length(uppest_non_existing_dir), ...
            'No progress made up the tree');
    end
    % >
    
    % < calling the builtin function
    [status,message,messageid] = builtin('mkdir', varargin{:});
    % >
    
    % < Recursive chmod starting to the highest non existing dir
    [status__, result] = system(sprintf('chmod -R 775 "%s"', ...
            uppest_non_existing_dir));
    % >
    
    assert(status__ == 0, 'OS : Unable to chmod : %s', result);
else
    [status,message,messageid] = builtin('mkdir', varargin{:});
end

end