#pragma once
#include "volume_curves.h"
#include <assert.h>
#include <lavc.h>
#include <lavd.h>
#include <gmc.h>
#include <math.h>


namespace VolumeCurves{
	class MathModule
	{
	public:
		MathModule(void){};
		~MathModule(void){};

		//static TNT::Vector<double> linspace(double a, double b, int n);			

		/** 
	  * Solves the equation Ax=b using Gauss-Jordan method.  You pass
	  * in the matrix A and the vector b.  Upon return, A holds the inverse of
	  * the original matrix A and b holds the solution x = A^-1 * b.  In other
	  * words, this method destroys both input A and b.
	  * @param A Input Matrix, set to the inverse of A upon return.
	  * \param b Input vector, set to the solution x upon return.
	  * \return False If matrix A is singular or some other problem arises.
	  */
		//static bool GaussJordanSolve(TNT::Matrix<double> &A, TNT::Vector<double> &b);

		static double Mean(LaVectorDouble v);
		static double sum(LaVectorDouble v, int index1, int index2);
	};
}
