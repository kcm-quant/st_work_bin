#ifndef LA_COMPLEX_SUPPORT
#define LA_COMPLEX_SUPPORT
#endif

#ifndef TARGETCLOSETRCURVES
#define TARGETCLOSETRCURVES

#if defined(VOLUMECURVES_EXPORTS)
#define VOLUMECURVES_API __declspec  ( dllexport ) 
#elif defined(VOLUMECURVES_IMPORTS) 
#define VOLUMECURVES_API __declspec  ( dllimport )
#else
#define VOLUMECURVES_API
#endif

#pragma once
#include "volume_curves.h"
#include <lavc.h>
#include <lavd.h>
#include <gmc.h>

namespace VolumeCurves{	

	class VOLUMECURVES_API TargetCloseTrCurves :
		public VolumeCurveBoundaries
	{
	public:		
		TargetCloseTrCurves(int N, int orderVolume,double* closingAuctionVolume, 
			double arbitrage, double dLambda, double dKappa, double dGamma, 
			double* market_volume, double* volatility_curve, double* volCurve, 
			double auction_participation, double auction_participation_min, double auction_participation_max);

		~TargetCloseTrCurves(void);		

		static const double** computeTradingCurves(int N, int orderVolume, double* closingAuctionVolume, 
			double arbitrage, double dLambda, double dKappa, double dGamma, 
			double *market_volume, double *volatility_curve, double* volCurve,
			double auction_participation, double auction_participation_min, double auction_participation_max);

	protected:			
		virtual LaVectorDouble			init(LaVectorDouble partialSolution, int step, double arbitrage);
		virtual LaGenMatComplex			buildMatrix(LaVectorDouble partialSolution, int iStep);		
		virtual LaVectorComplex			buildConstant(LaVectorDouble partialSolution, int iStep, double arb, double dstartValue, double dEndValue);

		double m_dKappa;
		double m_dGamma;
		double m_dLambda;
		int m_iOrderVolume;
				
		LaVectorDouble		m_dClosingAuctionVolume;
		LaVectorDouble		marketVolume;
		LaVectorDouble		volatilityCurve;
		LaVectorDouble		volumeCurve;

		double m_dAuctionParticipation;
		double m_dAuctionParticipationMin;
		double m_dAuctionParticipationMax;

		double dEpsSolution;
	};

}
#endif
