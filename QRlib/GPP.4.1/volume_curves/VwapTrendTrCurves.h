//#pragma once
#ifndef LA_COMPLEX_SUPPORT
#define LA_COMPLEX_SUPPORT
#endif

#ifndef VWAPTRENDTRCURVES
#define VWAPTRENDTRCURVES

#if defined(VOLUMECURVES_EXPORTS)
#define VOLUMECURVES_API __declspec  ( dllexport ) 
#elif defined(VOLUMECURVES_IMPORTS) 
#define VOLUMECURVES_API __declspec  ( dllimport )
#else
#define VOLUMECURVES_API
#endif

#pragma once
#include "volume_curves.h"
#include <lavc.h>
#include <lavd.h>
#include <gmc.h>

namespace VolumeCurves{	

	class VOLUMECURVES_API VwapTrendTrCurves :
		public VolumeCurveBoundaries
	{
	public:
		VwapTrendTrCurves(int N, int iOrderVolume, double arbitrage, double dLambda, double dKappa, double dGamma, double* volumeCurve, double* volatility, double* volatilityTrend, double* trend, double dailyVolume);
		VwapTrendTrCurves(int N, int iOrderVolume, double arbitrage_up, double arbitrage_down, double dLambda, double dKappa, double dGamma, double* volumeCurve, double* volatility, double dailyVolume);
		~VwapTrendTrCurves(void);

		static const double ** computeTradingCurves(int N, int orderVolume, double arbitrage, double dLambda, double dKappa, double dGamma, 
			double * volumeCurve, double *volatility_curve, double* trend_std, double* trend, double dailyVolume);

		static const double ** computeTradingCurves(int N, int orderVolume, double arbitrage_up, double arbitrage_down, double dLambda, double dKappa, double dGamma, 
			double * volumeCurve, double *volatility_curve, double dailyVolume);
			
	protected:
		virtual LaVectorDouble			init(LaVectorDouble partialSolution, int step, double arbitrage);
		virtual LaGenMatComplex			buildMatrix(LaVectorDouble partialSolution, int iStep);		
		virtual LaVectorComplex			buildConstant(LaVectorDouble partialSolution, int iStep, double arb, double dstartValue, double dEndValue);

		int m_iOrderVolume;
		double m_dLambda;
		double m_dKappa;
		double m_dGamma;

		LaVectorDouble lavdVolumeCurve;
		LaVectorDouble lavdVolatility;
		LaVectorDouble lavdVolatilityTrend;
		LaVectorDouble lavdTrend;
		LaVectorDouble lavdMarketVolume;

	};


}
#endif

