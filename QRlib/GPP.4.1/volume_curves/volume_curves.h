// volume_curves.h
//#pragma once
//#define LA_COMPLEX_SUPPORT
//#define LA_BOUNDS_CHECK

#ifndef VOLUMECURVEBOUNDARIES
#define VOLUMECURVEBOUNDARIES

#ifndef LA_COMPLEX_SUPPORT
#define LA_COMPLEX_SUPPORT
#endif

#if defined(VOLUMECURVES_EXPORTS)
#define VOLUMECURVES_API __declspec  ( dllexport ) 
#elif defined(VOLUMECURVES_IMPORTS) 
#define VOLUMECURVES_API __declspec  ( dllimport )
#else
#define VOLUMECURVES_API
#endif

#include <stdlib.h>
#include "lafnames.h"
#include LA_GEN_MAT_COMPLEX_H
#include LA_EXCEPTION_H
#include "blas3pp.h"
#include <gmc.h>
#include <lavc.h>
#include <lavd.h>

using namespace la;

namespace VolumeCurves {

	class VOLUMECURVES_API VolumeCurveBoundaries
	{
	public:
		virtual const double* tradingCurve()	{return computeTradingCurve(0);}
		virtual const double* wrappingUP()		{return computeTradingCurve(dArbitrage_up);}
		virtual const double* wrappingDOWN()	{return computeTradingCurve(-dArbitrage_down);}		
		
	protected:		
		const double* computeTradingCurve(double arb);
		
		/********************************************************************************
		*						Virtual Methods											*
		*********************************************************************************/
		virtual LaVectorDouble		init(LaVectorDouble partialSolution, int step, double arbitrage) = 0;				
		virtual LaGenMatComplex  	buildMatrix(LaVectorDouble partialSolution, int iStep) = 0;		
		virtual LaVectorComplex		buildConstant(LaVectorDouble partialSolution, int iStep, double arbitrage, double dstartValue, double dEndValue) = 0;
		
		/*******************************************************************************
		*						Private Members										   *
		********************************************************************************/
		LaVectorDouble lavdTradingCurve;

		double	dArbitrage_up;
		double  dArbitrage_down;
		int		iN;	
		double	dEpsilonMax;	
		int		iNoIterations;	
	};
}

#endif

