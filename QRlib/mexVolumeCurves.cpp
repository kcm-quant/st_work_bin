
#include "TargetCloseTrCurves.h"
#include "VwapTrendTrCurves.h"
#include "mex.h"

using namespace VolumeCurves;

void
mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
{
	char mode[256];	 
	mxGetString(prhs[0],mode,(mxGetM(prhs[0]) * mxGetN(prhs[0]) * sizeof(mxChar)) + 1);
	if(strcmp(mode, "TargetClose")==0){

		int N							= (int)mxGetScalar(prhs[1]);
		int orderVolume					= (int)mxGetScalar(prhs[2]);
		double* closingAuctionVolume	= mxGetPr(prhs[3]);

		double arbitrage				= mxGetScalar(prhs[4]);
		double dLambda					= mxGetScalar(prhs[5]);
		double dKappa					= mxGetScalar(prhs[6]);
		double dGamma					= mxGetScalar(prhs[7]);

		double *market_volume			= mxGetPr(prhs[8]);
		double *volatility_curve		= mxGetPr(prhs[9]);
		double* volCurve				= mxGetPr(prhs[10]);
		
		double auction_participation	= mxGetScalar(prhs[11]);
		double auction_participation_min= mxGetScalar(prhs[12]);
		double auction_participation_max= mxGetScalar(prhs[13]);

		const double** curves = TargetCloseTrCurves::computeTradingCurves(N, orderVolume, closingAuctionVolume, 
				arbitrage, dLambda, dKappa, dGamma, 
				market_volume, volatility_curve, volCurve, 
				auction_participation, auction_participation_min, auction_participation_max);
		

		plhs[0] = mxCreateDoubleMatrix(N+1, 3, mxREAL);
		double* outArray = mxGetPr(plhs[0]);
		for(int i = 0; i < 3;i++)
		{
			for(int j = 0; j < N+1; j++)
			{
				outArray[(i*(N+1))+j] = curves[i][j];
			}
		}
	}

	if(strcmp(mode, "VwapTrend")==0){

		int N							= (int)mxGetScalar(prhs[1]);		
		int orderVolume					= (int)mxGetScalar(prhs[2]);
		
		double arbitrage_up				= mxGetScalar(prhs[3]);
		double arbitrage_down			= mxGetScalar(prhs[4]);
		double dLambda					= mxGetScalar(prhs[5]);
		double dKappa					= mxGetScalar(prhs[6]);
		double dGamma					= mxGetScalar(prhs[7]);
		
		double* volCurve				= mxGetPr(prhs[8]);
		double* volatility_curve		= mxGetPr(prhs[9]);		
		double dailyVolume				= mxGetScalar(prhs[10]);

		const double** curves = VolumeCurves::VwapTrendTrCurves::computeTradingCurves(N, orderVolume, arbitrage_up, arbitrage_down, dLambda, dKappa, dGamma, 
			volCurve, volatility_curve, dailyVolume);
		
		plhs[0] = mxCreateDoubleMatrix(N+1, 3, mxREAL);
		double* outArray = mxGetPr(plhs[0]);
		for(int i = 0; i < 3;i++)
		{
			for(int j = 0; j < N+1; j++)
			{
				outArray[(i*(N+1))+j] = curves[i][j];
			}
		}
	}

	


}