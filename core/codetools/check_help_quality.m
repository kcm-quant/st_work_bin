function [s, info] = check_help_quality(filename_)
% CHECK_HELP_QUALITY - checks that the comments for help are well formed
%
% A well formed comment for help has to follow the rules below :
%
% * Having a one liner with the function name in uppercase
% * Having a section for Examples, begining by "Examples:" and ending by
% "See also:" all the examples should work properly when executed
% * Having an "author" flag with a valid mail between single quotes (ending by @cheuvreux.com or @smtpnotes, if not valid, mail will be sent to se_admin)
% * Having an "reviewer" flag between single quotes not necessarily filled (except for bin functions => TODO)
% % Having a version flag which should be a numeric between single quotes (e.g. '1')
% % Having a date flag with date format 'dd/mm/yyyy' between '01/01/2007'
% and '01/01/2100' between single quotes
%
%
% Examples:
% [es, info] = check_help_quality('st_data')
% [es, info] = check_help_quality('check_help_quality')
%
% See also: makedoc
%    
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   version  : '0.51'
%   date     :  '07/03/2011'

% xml  = xmltools('st_work.xml')
% xml  = xml.get_tag('ENVS');
% idx_c2keep = ~cellfun(@isempty, {xml.children.children.attribs}, 'uni', true);
% xml = xml.children.children(idx_c2keep).attribs;

MINIMAL_CHAR_INFO = 5;
MANDATORY_FLAGS = {'author', 'reviewer', 'version', 'date'};
FUNC4TEST = {@test_author, @test_reviewer, @test_version, @test_date};
ERROR_TYPE_ID = num2cell(2:length(MANDATORY_FLAGS)+2);
ERROR_TYPE = MANDATORY_FLAGS;
INFO_FIELD = MANDATORY_FLAGS;

s = struct('filename', filename_, 'error_type_id', [], ...
    'error_type', {}, 'error_message', {});

if strcmp(filename_(end-1:end), '.m')
    filename_(end-1:end) = [];
end

help_comments = help(filename_);
info = struct('author', [], 'reviewer', [], 'version', [], ...
    'date', [], 'cpu_time', NaN);
%< Checking wether there is a well formed help comment
if isempty(help_comments)
    s(end+1) = struct('filename', filename_, 'error_type_id', 0, ...
        'error_type', 'No help Comment', 'error_message', ...
        'Please add some comments just after the function declaration');
    return;
end
%>

help_lines = tokenize(help_comments, sprintf('\n'));
for i = 1 : length(help_lines)
    help_lines{i} = strtrim(help_lines{i}); % do not care about space caracters
end

%< Checking wether there is a well formed oneliner
expected_begining = [upper(filename_) ' - '];
if ~strcmp_begin(expected_begining, help_lines{1})
    s(end+1) = struct('filename', filename_, 'error_type_id', 1, ...
        'error_type', 'One liner', 'error_message', ...
        sprintf('Please add a one liner which has to begin exactly with : <%s>', expected_begining));
elseif length(help_lines{1}(length(expected_begining):end)) < MINIMAL_CHAR_INFO
    s(end+1) = struct('filename', filename_, 'error_type_id', 1, ...
        'error_type', 'One liner', 'error_message', ...
        'Too short one liner to be true');
end
%>

%< Checking Examples section
start_index = find_line_begining_with(help_lines, 'Examples:');
if isempty(start_index)
    s(end+1) = struct('filename', filename_, 'error_type_id', 2, ...
         'error_type', 'Examples', 'error_message', ...
         'No examples section please add one');
elseif length(start_index) >1
    s(end+1) = struct('filename', filename_, 'error_type_id', 2, ...
         'error_type', 'Examples', 'error_message', ...
         'You have too many "Examples:" in help unable to identify examples section.');
end
end_index = find_line_begining_with(help_lines, 'See also');
if isempty(end_index)
    s(end+1) = struct('filename', filename_, 'error_type_id', 3, ...
         'error_type', 'See also', 'error_message', ...
         'No "See also" please add one');
elseif length(end_index) >1
    s(end+1) = struct('filename', filename_, 'error_type_id', 3, ...
         'error_type', 'See also', 'error_message', ...
         'Too many "See also"');
end
if end_index <= start_index
    s(end+1) = struct('filename', filename_, 'error_type_id', 2, ...
         'error_type', 'Examples', 'error_message', ...
         'No examples section please add one');
end

if ~isempty(end_index) && ~isempty(start_index)                     
    code2eval = help_lines(start_index+1 : end_index-1);
    code2eval = strtrim(sprintf('%s\n', code2eval{:}));
    if isempty(code2eval)
        s(end+1) = struct('filename', filename_, 'error_type_id', 2, ...
             'error_type', 'Examples', 'error_message', ...
             'Empty section "Examples" please add some');
    end

    try
        tic;
        evalin('base', code2eval);
        info.cpu_time = toc;
    catch ME
         s(end+1) = struct('filename', filename_, 'error_type_id', 2, ...
             'error_type', 'Examples', 'error_message', ...
             text_to_printable_text(se_stack_error_message(ME)));
         if ~isfield(info, 'cpu_time')
            info.cpu_time = NaN;
         end
    end
end
%>

%< Checking rules about Mandatory flags
for i = 1 : length(MANDATORY_FLAGS)
    [line_number, str_after_bs] = find_line_begining_with(help_lines, MANDATORY_FLAGS{i});
    if isempty(line_number)
        s(end+1) = struct('filename', filename_, 'error_type_id', ERROR_TYPE_ID{i}, ...
             'error_type', ERROR_TYPE{i}, 'error_message', ...
             sprintf('No flag <%s> found', MANDATORY_FLAGS{i}));
    else
        [b, em, info_field] = FUNC4TEST{i}(strtrim(str_between_single_quotes(str_after_bs)));
        info.(INFO_FIELD{i}) = info_field;
        if ~b
            s(end+1) = struct('filename', filename_, 'error_type_id', ERROR_TYPE_ID{i}, ...
             'error_type', ERROR_TYPE{i}, 'error_message', em);
        end
    end
end
%>

end

function [b,em, info] = test_author(str)
b = regexp(str, '(@cheuvreux.com|@smtpnotes)$', 'once');
if ~isempty(b)
    info = str(1:b-1);
else
    info = '';
end
b = ~isempty(b);
if ~b
    em = 'Please add an author with a @cheuvreux.com or @smtpnotes mail';
else
    em = '';
end
end

function [b,em, info] = test_reviewer(str)
% TODO ??
info = '';
b = true;
em = '';
if isempty(str)
    return;
end
b = regexp(str, '(@cheuvreux.com|@smtpnotes)$', 'once');
if ~isempty(b)
    info = str(1:b-1);
else
    info = '';
end
end

function [b,em, info] = test_date(str)
info = NaN;
b = false;
em = '';
if isempty(str)
    em = 'Please add a piece of information';
    return;
end
try
    info = datenum(str, 'dd/mm/yyyy');
catch ME
    em = sprintf('Date is not in the right format, it should be "dd/mm/yyyy" ME stck : <%s>', ...
        se_stack_error_message(ME));
    return;
end
if info >= datenum(2007,1,1,0,0,0) && info <= datenum(2100,1,1,0,0,0)
    b = true;
else
    em = 'Date should be between 01/01/2007 and 01/01/2100';
end
end

function [b,em, info] = test_version(str)
info = NaN;
b = false;
em = '';
if isempty(str)
    em = 'Please add a piece of information';
    return;
end
try
    info = str2double(str);
    b = ~isnan(info);
    if ~b
        em = 'Please add a correct version number';
    end
catch ME
    em = sprintf('Please add a correct version number, error is <%s>', ...
        se_stack_error_message(ME));
end
end

function s = str_between_single_quotes(str)
s = regexp(str, '''(.*)''', 'tokens');
if ~isempty(s)
    s = s{1}{1};
else
    s = '';
end
end

function [b, str_after_bs] = strcmp_begin(beg_str, s)
b = length(s) >= length(beg_str) && strcmpi(beg_str, s(1:length(beg_str)));
if b
    str_after_bs = s(length(beg_str)+1:end);
else
    str_after_bs = '';
end
end

function [indices, str_after_bs] = find_line_begining_with(help_lines, beg_str)
indices = [];
str_after_bs = '';
for i = 1 : length(help_lines)
    [b, s] = strcmp_begin(beg_str, help_lines{i});
    if b
        str_after_bs = s;
        indices(end+1) = i;
    end
end
end