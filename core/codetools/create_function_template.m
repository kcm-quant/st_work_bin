function create_function_template(template_name,login_email,doc2fill)
% CREATE_FUNCTION_TEMPLATE - helps you creating a new function
% adds a function template to the clipboard, open a
% new m-file, paste the contents of template_name and open the save dialog
% box
%
% % press Ctrl+Shift+n % if you have st_work_standard_shortcuts.xml
%
% Examples:
% 
%
% See also: editorservices, EditorMacro
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '08/03/2011'
%   
%   last_checkin_info : $Header: create_function_template.m: Revision: 1: Author: robur: Date: 01/12/2012 02:19:52 PM$


global st_version

if nargin < 1 || isempty(template_name)
    template_name = 'mode_func_template.m';
end
if nargin < 2 || isempty(login_email)
    login_email = st_version.my_env.mail_from;
    if isempty(login_email)
        login_email = getenv('USERNAME');
    end
end
if nargin < 3 || isempty(doc2fill)
    func_name = inputdlg('Choose a name for your function');
    if isempty(func_name)
        return;
    end
    func_name =  func_name{:};
    doc2fill = [];
else
    func_name = doc2fill.Filename;
end

% Read template file
func_template = fileread(template_name);

% Make sure template is traversing the right dimension
func_template = func_template(:)';

% Replace $login$ Token
func_template = strrep(func_template, '$login$', login_email);

% Replace $mode_func_template$ Token
func_template = strrep(func_template, '$mode_func_template$', func_name);

% Replace $MODE_FUNC_TEMPLATE$ Token
func_template = strrep(func_template, '$MODE_FUNC_TEMPLATE$', upper(func_name));

% Replace $date_created$ Token
createdToken = datestr(now, 'dd/mm/yyyy');
func_template = strrep(func_template, '$date_created$', createdToken);

if isempty(doc2fill)
    % Open a new m-file
    edit;
    doc2fill = editorservices.getActive();
    
    % Paste the completed template
    doc2fill.appendText(char(func_template));
    
    % open the save dialog box
    doc2fill.save();
else
    % Paste the completed template
    doc2fill.appendText(char(func_template));
end


