function s = get_checkin_history(fp_filename_)
% GET_CHECKIN_HISTORY - Ask Surround for the list of modifiers in history
%
%
%
% Examples:
%
% get_checkin_history(which('st_data'))
% 
%
% See also: verctrl
%    
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '08/03/2011'

global st_version

repository = strrep(fp_filename_, st_version.my_env.st_work, ...
    fullfile(st_version.my_env.sscm_branch, 'st_work'));

[b, str]=system(sprintf('sscm rh "%s" -y%s -b%s -x -qTable -fTabs', ...
    repository, st_version.my_env.sscm_reader, st_version.my_env.sscm_branch));

if b
    error('get_checkin_history:error_from_surround', str);
end

s = tokenize(str, sprintf('\n'));
s(1) = [];
s = tokenize(s, sprintf('\t'));
for i = 1 : length(s)
    s{i} = s{i}{2};
end

s = struct('user', s); % if someone wants to add some info please help yourself

end