function ls = get_ls_from_surround(repository)
% GET_LS_FROM_SURROUND - retrieve the list of files in a repository
%
%
%
% Examples:
%
% get_ls_from_surround()
% 
%
% See also: get_checkin_history verctrl
%    
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '09/03/2011'

global st_version

if nargin < 1
    repository = fullfile(st_version.my_env.sscm_branch, 'st_work', 'bin');
end

[b, str] = system(sprintf('sscm ls -y%s -p%s -b%s -r', ...
    st_version.my_env.sscm_reader, repository, st_version.my_env.sscm_branch));

if b
    error('get_ls_from_surround:error_from_surround', str);
end

repository = strrep(repository, '\', '/');

rep_len = length(repository);

str = tokenize(str, sprintf('\n'));
str(end) = []; % should be a summary of the number of files
ls = {};
for i = 1 : length(str)
    str{i} = strtrim(str{i});
    if strcmp(str{i}(1:min(rep_len, length(str{i}))), repository)
        curr_rep = str{i};
    elseif strcmp(str{i}(1:min(12, length(str{i}))), '-checked out')% isempty(str{i}) % 
        % nothing to do
    else
        ind_end = strfind(str{i}, '  ');
        str{i} = strtrim(str{i}(1:ind_end(1)-1));
        ls{end+1} = [curr_rep '/' str{i}];
    end
end

end