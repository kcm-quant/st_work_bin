function msc = get_most_surround_commiter(fp_filename_)
% GET_MOST_SURROUND_COMMITER - Find who has made most commit to a file
%
%
%
% Examples:
%
% get_most_surround_commiter(which('st_data'))
%
% See also:
%    
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '09/03/2011'

pot_author = get_checkin_history(fp_filename_);
[u_author, ~, idx] = unique({pot_author.user});
nb_c = accumarray(idx', ones(length(pot_author), 1));
[~, idx] = max(nb_c);
msc = get_dico_user_mail(u_author{idx});

end