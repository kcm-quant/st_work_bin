function lff = list_forbidden_files(lf)
% LIST_FORBIDDEN_FILES - What is the list of "surround forbidden files"
%
% input  : cell array of filenames
% output : cell array of filenames which shouldnt be put in surround
%
% Examples:
%
% lff = list_forbidden_files(get_ls_from_surround())
% lff = list_forbidden_files(get_ls_from_surround('QuantitativeResearch/st_work'))
% 
%
% See also: get_ls_from_surround
%    
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '09/03/2011'

allowed_ext = {'m', 'M', 'mexw32', 'tcl', 'rc', 'bat', 'c', 'h', 'cpp', ...
                    'hpp', 'cs', 'hs', 'dsp', 'dsw', 'ncb', 'opt', 'plg', ...
                    'csproj', 'sln', 'vssscc', 'ini', 'vcproj', 'vspscc', ...
                    'exp', 'sql', 'vdproj', 'mexa64', 'p', 'lib', 'jar', ...
                    'dll', 'xml'};
allowed_ext = sprintf('\\.%s|', allowed_ext{:});
allowed_ext(end) = [];

is_ff = false(length(lf), 1);
for i = 1 : length(lf)
    is_ff(i) = isempty(regexp(lf{i}, ['(' allowed_ext ')$'], 'once'));
end

lff = lf(is_ff);

%<* Exceptions Management
% .mat are allowed for graphs, and it should be in st_work/bin/graph_indicators/projects
is_ff = true(length(lff), 1);
for i = 1 : length(lff)
    if strcmp(lff{i}(end-3:end), '.mat')
        is_ff(i) = isempty(strfind(lff{i}, 'st_work/bin/graph_indicators/projects'));
    end
end
% evrything allowed in st_work\doc
for i = 1 : length(lff)
    is_ff(i) = is_ff(i) && isempty(strfind(lff{i}, 'st_work/doc'));
end
%>*

lff = lff(is_ff);
end