function svn(varargin)
% svn Control TortoiseSVN from within matlab for windows 
% EDIT(robur) : svn for linux is far better, see help below ;-) <a href="matlab:disp('Thank you! Then please correct my bugs and extend functionnalities'),sendmail({'rburgot@keplercheuvreux.com', 'romain.burgot@gmail.com'}, 'I like your svn function as well as your documentation!')">I agree</a>
%
% Wrapper for TortoiseSVN.
% Useage: SVN command dir/file(s)
% Supported commands: about, checkout*, import*, update*, commit*, add,
% revert*, cleanup*, resolve*, repocreate*, switch*, export*, mergeall*,
% settings, remove, rename, diff, conflicteditor, relocate, help,
% repostatus, repobrowser, ignore, blame, createpatch, revisiongraph, lock,
% unlock, properties.
%
% Commands with an * only take one directory as an argument.
% Other commands that take a path can accept more than one argument. If no
% directory is given, the current working directory is used.
%
% This function is not intended to completely replace SVN. It is a quick
% way to do commits, checkouts and adds to a directory while in Matlab.
%
% Examples:
%   svn checkout [Dialog]
%   edit newfile.m [Dialog]
%   svn add newfile.m [Dialog]
%   svn commit [Dialog]
%
% For a full description of what each command does, visit the tortoisesvn
% website: http://tortoisesvn.net/docs/nightly/TortoiseSVN_en/tsvn-automation.html
%
% See also: add, checkout, commit
%
%
% 
% EDIT for Linux : rburgot@keplercheuvreux.com <a href="matlab:disp('Thank you! Then please correct my bugs and extend functionnalities'),sendmail({'rburgot@keplercheuvreux.com', 'romain.burgot@gmail.com'}, 'I like your svn function as well as your documentation!')">Like</a>
%
% svn for linux, works with command lines and will do the job on the
% current file in your matlab editor. For instance just type 'svn lock' 
% in command window, edit your file then type 'svn ci' in command window
%
%   <a href="matlab:svn lock">lock</a>
%   <a href="matlab:svn add">add : add + commit</a>
%   <a href="matlab:svn ci">commit (ci)</a>
%   <a href="matlab:svn up">update (up)</a>
%   <a href="matlab:svn unlock">unlock</a>
%   <a href="matlab:svn help">help</a>
%   <a href="matlab:svn backup">backup : commit with a comment "backup, not functional : do not use" then will relock the file so you can keep on working</a>
%   <a href="matlab:svn refresh">refresh : will do a commit then update on your st_work/bin </a>
%
% If you do not like to type commands, you can either (and please make it working for evryone!) : 
% 
%   <a href="matlab:doc('EditorMacro'),edit('st_work_standard_shortcuts.xml')">-Configure some shortcuts thanks to EditorMacro and st_work_standard_shortcuts.xml</a>
%   <a href="matlab:disp('Run a command like :'),disp('awtinvoke(com.mathworks.mlwidgets.shortcuts.ShortcutUtils,''addShortcutToBottom'', ''lock'', ''svn lock'', [], ''Toolbar Shortcuts'', ''true'')'),msgbox('Have a Look in your command window, you should use a command like : awtinvoke(com.mathworks.mlwidgets.shortcuts.ShortcutUtils,''addShortcutToBottom'', ''lock'', ''svn lock'', [], ''Toolbar Shortcuts'', ''true'' )')">-Add some mouse clickable buttons</a>
%
% For more examples of this kind of stuff : <a href="matlab:edit('source_control_sc.m')">See how we used to do with another source control</a>
%
% <a href="matlab:global st_version,web(fullfile(st_version.my_env.st_work, 'doc', 'mmqr_toolbox', 'demos', 'global', 'how_to_publish.html'), '-helpbrowser')">I am aware that without some good docs, knowledge get lost but I need some help to do that</a>
%
% <a href="matlab:web('! firefox -new-window http://undocumentedmatlab.com/')">I wanna see more of this kind of cool and usefull stuff!</a>
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '20/08/2013'



global st_version

if ispc
    if nargin<1
        error('You must give at least one command\nSee: http://tortoisesvn.net/docs/nightly/TortoiseSVN_en/tsvn-automation.html');
    else
        command=varargin{1};
    end
    if nargin<2
        switch command
        case 'lock'
            varargin{2} = find_active_file();
        case 'commit'
            varargin{2} = dirup(find_active_file());
        otherwise
            varargin{2} = pwd;
        end
    end
    if nargin > 2
        option = varargin{3};
    else
        option = '';
    end
    switch command
        case 'about'
        case 'checkout' % Check out one path at a time
            varargin=singleDirOnly(command,varargin);
        case 'import'% Import one path at a time
            varargin=singleDirOnly(command,varargin);
        case 'update'
            varargin=singleDirOnly(command,varargin);
        case 'commit' % Commit path at a time
            varargin=singleDirOnly(command,varargin);
        case 'add'
        case 'revert'
            varargin=singleDirOnly(command,varargin);
        case 'cleanup'
            varargin=singleDirOnly(command,varargin);
        case 'resolve'
            varargin=singleDirOnly(command,varargin);
        case 'repocreate'
            varargin=singleDirOnly(command,varargin);
        case 'switch'
            varargin=singleDirOnly(command,varargin);
        case 'export'
            varargin=singleDirOnly(command,varargin);
        case 'merge'
            error('Unsupported command.');
        case 'mergeall'
            varargin=singleDirOnly(command,varargin);
        case 'copy'
            error('Unsupported command.');
        case 'settings'
        case 'remove'
        case 'rename'
        case 'diff'
        case 'showcompare'
            error('Unsupported command.');
        case 'conflicteditor'
        case 'relocate'
        case 'help'
        case 'repostatus'
            command = [command,' /',option];
        case 'repobrowser'
        case 'ignore'
        case 'blame'
        case 'cat'
            error('Unsupported command.');
        case 'createpatch'
        case 'revisiongraph'
        case 'lock'
        case 'unlock'
        case 'properties'
        otherwise
            error(sprintf('Unknown TortoiseSVN command: %s\nSee: %s',command,'http://tortoisesvn.net/docs/nightly/TortoiseSVN_en/tsvn-automation.html'));
    end
    % Use double quotes if there is a space in the path.
    % No UNC as this is called using the dos command
    switch 2
        case exist('C:\Program Files\TortoiseSVN\bin\TortoiseProc.exe','file')
            tsvn='"C:\Program Files\TortoiseSVN\bin\TortoiseProc.exe"';
        case exist('C:\Apps\Tortoise\bin\TortoiseProc.exe','file')
            tsvn = 'C:\Apps\Tortoise\bin\TortoiseProc.exe';
        case exist('C:\Apps\TortoiseSVN\bin\TortoiseProc.exe','file')
            tsvn='"C:\Apps\TortoiseSVN\bin\TortoiseProc.exe"';
        otherwise
            % In last resort, checking whether it is on the %PATH%...
            assert(system('where TortoiseProc > nul')==0,'Could not find <TortoiseProc> on this host')
            tsvn = 'TortoiseProc';
    end
    cmd=sprintf('%s /command:%s /closeonend:1 /path:"%s"',tsvn,command,abspath(varargin{2}));
    dos(cmd);
else
    if strcmp('refresh', varargin{1})
        svn('ci', fullfile(st_version.my_env.st_work, 'bin'));
        svn('up', fullfile(st_version.my_env.st_work, 'bin'));
        return
    end
    if nargin == 1
        v = version();
        if strtok(v, '.') > '7' %v(1)
            active_file = matlab.desktop.editor.getActive;
            isdirty = active_file.Modified; % is the file modified without having been saved? => should be used but we'll keep it simple for now
        else
            active_file = editorservices.getActive(); % the object of the editor enabling to edit the current file
            isdirty = active_file.IsDirty(); % is the file modified without having been saved? => should be used but we'll keep it simple for now
        end
        active_filen = active_file.Filename; % should be the full path of the file being edited, including filename and extension
        
        if ~any(strcmp(varargin{1}, {'lock', 'unlock', 'add', 'import', 'backup'} ) ) % il y aurait plus de diff?rents cas ? g?rer, mais pour l'instant je ne compte utiliser que lovk, update et add
            active_filen = dirup(active_filen);
        end
    else
        active_filen = varargin{2};
    end
    
%     if ~isfield(st_version.my_env, 'mail_from') || isempty(st_version.my_env.mail_from)
%         error('svn:check', 'Please set your mail_from variable in st_work.xml for : <%s,%s>', ...
%             st_version.my_env.hostname, username);
%     end
%     userlogin = tokenize(st_version.my_env.mail_from, '@');
%     userlogin = userlogin{1};
    
    switch varargin{1} 
        case 'backup'
            [succes, iseditable] = fileattrib(active_filen); % is the file modified locally? => the property Editable does not seem to be this
            if ~succes
                error('svn:backup', 'Cannot determine if file is writable');
            end
            iseditable = iseditable.UserWrite;
            if ~iseditable
                error('svn:backup', 'If file is not writable why doing a backup?');
            end
            % < Saving the current text of active file in case you have started
            % to make some modifications without previously checking out
            if isdirty
                error('svn:backup', 'Please save your file before a backup?');
            end
            % >
%             result = system_wc(['svn ci --username ' userlogin ' --password ' userlogin ' --message "backup, not functional : do not use" ' active_filen ]);
            result = system_wc(['svn ci --message "backup, not functional : do not use" ' active_filen ]);
            svn lock
        case {'commit', 'ci'}
            str = input('Please give your comment about this commit : \n','s');
%             result = system_wc(['svn ci --username ' userlogin ' --password ' userlogin ' --message "' str '" ' active_filen ]);
            result = system_wc(['svn ci --message "' str '" ' active_filen ]);
        case 'lock'
            [succes, iseditable] = fileattrib(active_filen); % is the file modified locally? => the property Editable does not seem to be this
            if ~succes
                error('source_control_sc:checkout', 'Cannot determine if file is writable');
            end
            iseditable = iseditable.UserWrite;
            % < Saving the current text of active file in case you have started
            % to make some modifications without previously checking out
            if isdirty || iseditable
                t = active_file.Text;
            end
            % >

            svn('up', active_filen); % making an update from svn in any case without asking user : do not do that dirty things!!!

%             result = system_wc(['svn lock --username ' userlogin ' --password ' userlogin ' ' active_filen ]);
            result = system_wc(['svn lock ' active_filen ]);

            active_file.reload(); % reloading the file with the right content (as in surround)

            % < opening another untitled fileeditor to save the dirty work of
            % the user
            if isdirty || iseditable
                edit;
                if strtok(v, '.') > '7' %v(1)
                    matlab.desktop.editor.getActive().appendText([get_warn_text t]);
                    matlab.desktop.editor.getActive().goToLine(1);
                else
                    editorservices.getActive().appendText([get_warn_text t]);
                    editorservices.getActive().goToLine(1);
                end
            end
            % >
        case 'help'
            result = system_wc('svn help'); % So stupid : !svn help would do the job !!!
        case 'add'
            result = system_wc(['svn add ' active_filen ]);
            result = cat(2, result, system_wc(['svn propset svn:needs-lock "on" ' active_filen ]));
            str = input('Please give your comment about this file : \n','s');
%             result = cat(2, result, system_wc(['svn ci --username ' userlogin ' --password ' userlogin ' --message "' str '" ' active_filen ]));
            result = cat(2, result, system_wc(['svn ci --message "' str '" ' active_filen ]));
        case 'import'
            error('svn:check', 'Do it by yourself outside of matlab, there is no point in managing repositories URL in matlab. \n If you already cheked out a repository, then use the add command which will work and do the add + svn:needlock + commit');
        case {'update', 'up', 'unlock'}
%             result = system_wc(['svn ' varargin{1} ' --username ' userlogin ' --password ' userlogin ' ' active_filen ]);
            result = system_wc(['svn ' varargin{1} ' ' active_filen ]);
        otherwise
            error('svn:check', 'Command not implemented : please implement it explicitly (not in the otherwise of the switch)');
    end
    st_log(result);
end
end

% For functions which should only take a single directory
function in=singleDirOnly(cmd,in)
if (~isdir(abspath(in{2})))
    error('You can only commit directories');
end
if length(in)>2
    warning('SVN:DIRLIMIT',['You can only ' cmd ' one directory at a time. Only using first directory']);
    in(3:end)='';
end
end

function [absolutepath]=abspath(partialpath)
% Taken from xlswrite.
% parse partial path into path parts
[pathname filename ext] = fileparts(partialpath);
% no path qualification is present in partial path; assume parent is pwd, except
% when path string starts with '~' or is identical to '~'.
if isempty(pathname) && isempty(strmatch('~',partialpath))
    Directory = pwd;
elseif isempty(regexp(partialpath,'(.:|\\\\)','once')) && ...
        isempty(strmatch('/',partialpath)) && ...
        isempty(strmatch('~',partialpath));
    % path did not start with any of drive name, UNC path or '~'.
    Directory = [pwd,filesep,pathname];
else
    % path content present in partial path; assume relative to current directory,
    % or absolute.
    Directory = pathname;
end
% construct absulute filename
absolutepath = fullfile(Directory,[filename,ext]);
end

function s = get_warn_text()
s = sprintf([...
'\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n' ...
'You tried to make a checkout while your local file was modified (saved or not).\n' ...
'This is not the right way to do this but we retrieved the last file from \n' ...
'SVN as you should have done yourself. You''ll find below your local file. \n' ...
'THINK BEFORE DOING ANYTHING OR JUST CLOSE THIS EDITOR (LOSE YOUR CHANGES)\n' ...
'PLEASE DO NOT COPY PASTE : USE DIFF MERGE TOOLS!!!!!!\n' ... 
'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n\n' ...
]);
end

function active_filen = find_active_file()
v = version();
if strtok(v, '.') > '7' %v(1)
    active_file = matlab.desktop.editor.getActive;
else
    active_file = editorservices.getActive(); % the object of the editor enabling to edit the current file
end
active_filen = active_file.Filename; % should be the full path of the file being edited, including filename and extension
end