function varargout = gr_import_xml( mode, varargin)
% GR_IMPORT_XML - import of XML informations for an st_grapher
%
% [global_params, enrich, nodes] = gr_import_xml( 'fileparts', 'C:\st_work\usr\dev\lehalle\projects\test.xml')
% [node_desc, connections_desc] = gr_import_xml('node', xmltools( nodes.this('children', 1)) )
% info_gp  = gr_import_xml('global', global_params)
% info_enr = gr_import_xml('enrichments', enrich)
% txt = gr_import_xml( 'build-file', 'C:\st_work\usr\dev\lehalle\projects\test.xml', 'fileout', 'C:\st_work\usr\dev\lehalle\projects\test.m')
varargout = {};
switch lower(mode)
    case 'build-file'
        fname = varargin{1};
        opt   = options({'fileout', ''}, varargin(2:end));
        [global_params, enrich, nodes] = gr_import_xml( 'fileparts', fname);
        info_gp  = gr_import_xml('global', global_params);
        info_enr = gr_import_xml('enrichments', enrich);
        
        txt = { 'gr = st_grapher;', 'id = options({});' };
        connections = [];
        for c=1:nodes.nb_children()
            [node_desc, connections_desc] = gr_import_xml('node', xmltools( nodes.this('children', c)) );
            % cr�ation du node
            txt{end+1} = sprintf('%%%% Creation of the node <%s:%d>', ...
                node_desc.title, node_desc.id );
            txt{end+1} = sprintf('num = gr.add_node( ''st_function'', ''%s'', ''title'', ''%s'' );', ...
                node_desc.st_function, node_desc.title);
            txt{end+1} = 'gr.node_set(num,''id'', num);';
            txt{end+1} = sprintf('gr.node_set(num, ''position'', [%d,%d,%d,%d]);', ...
                node_desc.position(1), node_desc.position(2), node_desc.position(3), node_desc.position(4));
            txt{end+1} = sprintf('id.set(''%d'', num);', node_desc.id );
            % init du node
            txt{end+1} = 'gr.init( num);';
            % set de ses params
            if ~isempty( node_desc.params(1).name)
                for p=1:length(node_desc.params)
                    txt{end+1} = sprintf( 'gr.set_param( num, ''%s'', convs(''param'', ''%s'') );', ...
                        node_desc.params(p).name, strrep(node_desc.params(p).value, '''', ''''''));
                end
            else
                txt{end+1} = '% No parameter to write';
            end
            % stockage des connections pour plus tard
            connections= cat(1, connections, connections_desc);
        end
        
        % connections
        txt{end+1} = '%% Connections';
        for r=1:size(connections, 1)
            txt{end+1} = sprintf('gr.connect(id.get(''%d''), %d, id.get(''%d''), %d);', connections(r, 1), connections(r, 2), connections(r, 3), connections(r, 4));
        end
        %gr.connect(id.get('3'),1,id.get('8'),1);

        % Enrichments
        txt{end+1} = '%% Enrichments';
        for e = 1:length(info_enr)
            e_content = info_enr(e).function;
            if e_content(1)=='"'
                e_content = ['''' e_content(2:end) ''''];
            end
            txt{end+1} = sprintf('gr.set_enrichment(''%s'', ''fun'', %s, ''position'', [ %d %d %d %d ], ''display'', %d, ''color'', [%d %d %d], ''txtcolor'', [ %d %d %d ] );', ...
                info_enr(e).name, ...
                e_content, ...
                info_enr(e).position(1), info_enr(e).position(2), info_enr(e).position(3), info_enr(e).position(4), ...
                info_enr(e).display, ...
                info_enr(e).color(1), info_enr(e).color(2), info_enr(e).color(3), ...
                info_enr(e).txtcolor(1), info_enr(e).txtcolor(1), info_enr(e).txtcolor(1) ...
            );
        end
        %gr.set_enrichment( 'Params', 'fun', @(g)test_reader(g), 'position', [71.960606 91.960606 -63.428051 -53.428051], 'display', 1, 'color', [0.670000 0.000000 0.120000], 'txtcolor', [1.000000 1.000000 1.000000]);
        
        % Global Params. TODO: Add plot panel params
        txt{end+1} = '%% Global Params';
        
        txt{end+1} = sprintf('gr.set_global(''nb_nodes'', %d);', info_gp.nb_nodes);
        txt{end+1} = sprintf('gr.set_global(''filename'', ''%s'');', info_gp.filename);
        txt{end+1} = sprintf('gr.set_global(''root_title'', ''%s'');', info_gp.root_title);
        txt{end+1} = sprintf('gr.set_global(''root_id'', %d);', info_gp.root_id);        
        
        fout = opt.get('fileout');
        if ~isempty( fout)
            fod = fopen( fout, 'w');
            fprintf(fod,'%s\n', txt{:});
            fclose(fod);
            varargout = { txt};
        else
            eval(sprintf('%s\n', txt{:}));
            varargout = { gr, txt };          
        end
        
    case 'fileparts'
        xml = xmltools( varargin{1});
        gp  = xml.get_tag('global_parameters');
        nr  = xml.get_tag('enrichments');
        nd  = xml.get_tag('nodes');
        varargout = { ...
            xmltools(gp.children ), ...
            xmltools(nr.children), ...
            xmltools(nd.children) };
    case 'enrichments'
        %<* Enrichments parsing
        this_enr =  varargin{1};
        enrs     = this_enr.this();
        all_enrs = [];
        for i=1:length( enrs.children )
            one_enrichment = xmltools(enrs.children(i));
            one_enr = struct( ...
                'name', one_enrichment.get_my_attrib_value('name'), ...
                'txtcolor', str2num(one_enrichment.get_attrib_value('txtcolor', 'rgb')), ...
                'color', str2num(one_enrichment.get_attrib_value('color', 'rgb')), ...
                'position', [ ...
                str2double(one_enrichment.get_attrib_value('position', 'xmin')), ...
                str2double(one_enrichment.get_attrib_value('position', 'xmax')), ...
                str2double(one_enrichment.get_attrib_value('position', 'ymin')), ...
                str2double(one_enrichment.get_attrib_value('position', 'ymax')) ...
                ] , ...
                'function', one_enrichment.get_tag_value('function'), ...
                'display', str2double(one_enrichment.get_tag_value('display') ));
            if isempty( all_enrs)
                all_enrs = one_enr;
            else
                all_enrs(end+1) = one_enr;
            end
        end
        varargout = { all_enrs};
        %>*
    case 'global'
        %<* Global params parsing
        this_gp = varargin{1};
        info_gp = struct('nb_nodes', str2double( this_gp.get_tag_value('nb_nodes')), ...
            'filename', this_gp.get_tag_value('filename'), ...
            'root_title', this_gp.get_attrib_value('root', 'title'), ...
            'root_id', str2double( this_gp.get_attrib_value('root', 'id')) );
        varargout = { info_gp };
        %>*
    case 'node'
        %<* Node info parsing
        this_node = varargin{1};
        
        % parsing des param�tres
        param_names = this_node.get_attrib_value('param', 'label');
        if ~iscell( param_names)
            param_names = { param_names};
        end
        param_values = cell(1,length(param_names));
        for p=1:length(param_names)
            param_values{p} = this_node.get_tag_value_where_attrib('param', 'label', param_names{p});
        end
        % parsing des connections (mes inputs)
        inputs = this_node.get_tag('input');
        inputs = inputs.children(1);
        connections = repmat(nan,length( inputs.children ), 4);
        for i=1:length( inputs.children )
            one_connection = xmltools(inputs.children(i));
            connections(i,:) = [ ...
                str2double(one_connection.get_attrib_value('source', 'node_id')), ...
                str2double(one_connection.get_attrib_value('source', 'connection_id')), ...
                str2double(one_connection.get_attrib_value('destination', 'node_id')), ...
                str2double(one_connection.get_attrib_value('destination', 'connection_id'))];
        end
        % parsing de la config
        node = struct('title', this_node.get_my_attrib_value('title'), ...
            'id', str2double(this_node.get_my_attrib_value('id')), ...
            'st_function', this_node.get_tag_value('st_function'), ...
            'position', [ ...
            str2double(this_node.get_attrib_value('position', 'xmin')), ...
            str2double(this_node.get_attrib_value('position', 'xmax')), ...
            str2double(this_node.get_attrib_value('position', 'ymin')), ...
            str2double(this_node.get_attrib_value('position', 'ymax')) ...
            ], ...
            'params', struct('name', param_names, 'value', param_values), ...
            'connections', connections);
        
        varargout = { node, connections };
        %>*
    otherwise
        error('gr_import_xml:mode', 'mode <%s> unknown', mode);
end