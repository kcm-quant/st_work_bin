function varargout = node_tools( mode, varargin)
% NODE_TOOLS - a sttic set of functionnal tools for manipulating st_nodes
%
% [n1, n2] = node_tools('connect'  , n1, o1, n2, i2)
% [n1, n2] = node_tools('unconnect', n1, o1, n2, i2)
% les connections sont stock�es ainsi:
%  out -> [ nb_out, target_id, nb_target  ; ...]
%  in  -> [ nb_in , provenance_id, nb_prov; ...]
%
% See also st_node
switch lower(mode)
    case 'connect'
        %<* Connect two nodes
        n1 = varargin{1};
        o1 = varargin{2};
        n2 = varargin{3};
        i2 = varargin{4};
        out_connections1 = n1.get('out_connections');
        n1.set('out_connections', [out_connections1; o1, n2.get('id'), i2]);
        in_connections2 = n2.get('in_connections');
        n2.set('in_connections',  [in_connections2; i2, n1.get('id'), o1]);
        varargout = { n1, n2};
        %>*
    case { 'un-connect', 'unconnect'}
        %<* Unconnect
        n1 = varargin{1};
        o1 = varargin{2};
        n2 = varargin{3};
        i2 = varargin{4};
        % out connections
        out_connections1 = n1.get('out_connections');
        idx = all( bsxfun(@eq, out_connections1, [o1, n2.get('id'), i2]),2);
        if sum(idx)==0
            error('node_tools:unconnect','node <%s> and <%s> does not seem to be connected that way...', n1.get('title'), n2.get('title'));
        end
        out_connections1(idx,:)=[];
        n1.set('out_connections', out_connections1);
        % in connections
        in_connections2 = n2.get('in_connections');
        idx = all( bsxfun(@eq, in_connections2, [i2, n1.get('id'), o1]),2);
        if sum(idx)==0
            error('node_tools:unconnect','node <%s> and <%d> does not seem to be connected that way...', n1.get('title'), n2.get('title'));
        end
        in_connections2(idx,:)=[];
        n2.set('in_connections',  in_connections2);
        varargout = { n1, n2};
        %>*
    otherwise
        error('node_tools:mode', 'mode <%s> unknwon', mode);
end