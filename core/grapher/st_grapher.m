function gr = st_grapher( varargin)
% ST_GRAPHER - gestionnaire d'un assemblage de graphes
%
% gr = st_grapher
% gr = st_grapher('clone', this);
%
% TODO:
% - il semble que lorsqu'on effece un noeud qui �tait 'target', cela ne
%   r�initialise pas la target...
%
% upgrade grapher
%    % this = st_ans.get(); gr = st_grapher('clone', this);
%    % fname = gr.get_global('filename'); save( fname, 'gr')
%    1. click on 'graph' in the win_graph
%    2. execute :
%    > gr = st_ans.upgrade()
%    > win_graph('show', gr)
%    3. puis reloader le graphe
%
% interface publique
% 
%               add_node: @st_grapher/add_node
%                connect: @st_grapher/connect    - (n1,o1,n2,i2)
%            delete_node: @st_grapher/delete_node 
%              unconnect: @st_grapher/unconnect  - (n1,o1,n2,i2)
%            get_outputs: @st_grapher/get_outputs
%     get_target_outputs: @st_grapher/get_target_outputs
%       get_root_outputs: @st_grapher/get_root_outputs
%             get_inputs: @st_grapher/get_inputs
%        get_connections: @st_grapher/get_connections - ( node_name, 'in/out')
%                    get: @st_grapher/get_
%               node_set: @st_grapher/node_set
%              init_node: @st_grapher/init_node
%                   init: @st_grapher/init
%                   next: @st_grapher/next
%                  learn: @st_grapher/learn
%     global_end_of_prod: @st_grapher/global_end_of_prod
%               get_node: @st_grapher/get_node
%                   plot: @st_grapher/plot
%                node_id: @st_grapher/node_id
%                   code: @st_grapher/code
%                compile: @st_grapher/compile
%                 export: @st_grapher/export
%             get_memory: @st_grapher/get_memory
%             set_memory: @st_grapher/set_memory
%              get_param: @st_grapher/get_param
%              set_param: @st_grapher/set_param
%             set_global: @st_grapher/set_global
%             get_global: @st_grapher/get_global
%           set_position: @st_grapher/set_position
%           visual_param: @st_grapher/visual_param
%                 length: @st_grapher/length_
%         set_node_param: @st_grapher/set_node_param
%                 max_id: @st_grapher/max_id
%               children: @st_grapher/children - ( 'node-name', 'in/out')
%                 market: @st_grapher/market
%                   info: @st_grapher/info
%         set_enrichment: @st_grapher/set_enrichment
%         get_enrichment: @st_grapher/get_enrichment
%         del_enrichment: @st_grapher/del_enrichment
%         run_enrichment: @st_grapher/run_enrichment
%                upgrade: @st_grapher/upgrade
%                 to_xml: @st_grapher/to_xml
%
% il y a aussi les enrichments:
%   gr.set_enrichment('next', 'fun', @(g)g.next(), 'display', true, 'color', [.67 0 .12], 'txtcolor', [0,0,0] )
%   gr.get('enrichments', 'names')
% il est alors possible de faire:
%   gr.set_global('commands', false)
%
% le param global: 'panel-correspondances' associe un num�ro de panel P � un
% module de nom MODULE ainsi: 'P1:MODULE1;P2:MODULE2;...' la position des
% planels est d�finie dans 'panel-layout'
%
% pour plot syst�matique, utiliser le param global 'systematic-plot'
%
% ce grapher est aussi capable d'ex�cuter des simulations de trading, pour
% cela les fonctions st_sim_market0, st_sim_trading0, st_sim_volatility et
% st_simple_market contiennent une courte documentation.
%
% See also st_node node_tools 
global st_version 
st_version.st_grapher = '3.1';

%<* Load
if nargin > 0 && strcmp(varargin{1}, 'load')
    gr = load( varargin{2});
    gr = gr.gr;
    return
end
%>*
%<* Clone
if nargin > 0 && strcmp(varargin{1}, 'clone')
    if isfield( varargin{2}, 'this')
        this = varargin{2}.this;
    elseif isfield( varargin{2}, 'graph')
        this = varargin{2};
    elseif isfield( varargin{2}, 'gr')
        gr = varargin{2}.gr;
        return
    else
        gr = varargin{2};
        return
    end
else
    this = struct('graph', {{}}, 'params', options({'title', 'New grapher', ...
        'root', 0, 'target', Inf, 'panel', [], 'trace', [], 'filename', '', ...
        'verbosity', inf, 'commands', true, 'plot-panel', false, ...
        'systematic-plot', '', ...
        'panel-positions', [.22 .6], 'panel-layout', [], 'panels-layout-handles', [], ...
        'panel-correspondances', '' }, varargin), ...
        'enrichments', []);
end
%>*
if ~isfield( this, 'enrichments')
    this.enrichments = [];
end
gr = struct('add_node', @add_node, ...
            'connect' , @connect, ...
            'delete_node', @delete_node, ...
            'unconnect', @unconnect, ...
            'get_outputs', @get_outputs, ...
            'clear_outputs', @clear_outputs, ...
            'get_target_outputs', @get_target_outputs, ...
            'get_root_outputs', @get_root_outputs, ...
            'get_inputs', @get_inputs, ...
            'get_connections', @get_connections, ...
            'get', @get_, ...
            'node_set', @node_set, ...
            'init', @init, ...
            'next', @next, ...
            'learn', @learn, ...
            'global_end_of_prod', @global_end_of_prod, ...
            'get_node', @get_node, ...
            'plot', @plot, ...
            'node_id', @node_id, ...
            'init_node', @init_node, ...
            'code', @code, ...
            'compile', @compile, ...
            'export', @export, ...
            'get_memory', @get_memory, ...
            'set_memory', @set_memory, ...
            'get_param', @get_param, ...
            'set_param', @set_param, ...
            'set_global', @set_global, ...
            'get_global', @get_global, ...
            'set_position', @set_position, ...
            'visual_param', @visual_param, ...
            'length', @length_, ...
            'set_node_param', @set_node_param, ...
            'max_id', @max_id, ...
            'children', @children, ...
            'market', @market, ...
            'info', @info, ...
            'set_enrichment', @set_enrichment, ...
            'get_enrichment', @get_enrichment, ...
            'del_enrichment', @del_enrichment, ...
            'run_enrichment', @run_enrichment, ...
            'upgrade', @upgrade,...
            'to_xml', @to_xml...
            );

% Print in XML file
function to_xml(varargin)
    
if isempty(varargin)
    fname = this.filename;
else 
    fname = varargin{1};
    if(~strcmp(fname(end-3:end), '.xml'))
        fname = [fname '.xml'];
    end
end 
fid = fopen(fname, 'w');
info = gr.info();
fprintf(fid, '%s\n', info{:});
fclose(fid);
end

        
%%** Print info about the grapher
function stru_info = info( stru_info, n, e, pos)
    stru_info = { '<?xml version="1.0" encoding="ISO-8859-1"?>', ...
        ['<graph title="', this.params.get('title') ,'">']};
    % param�tres globaux
    stru_info{end+1} = '   <global_parameters>';
    stru_info{end+1} = sprintf('      <nb_nodes>%d</nb_nodes>', length(this.graph));
    stru_info{end+1} = sprintf('      <panel>%d</panel>', this.params.get('panel'));
    stru_info{end+1} = sprintf('      <filename>%s</filename>', this.params.get('filename'));
    % je ne veux pas s�rialise trace
    %n = this.params.get('root');
    %if n==0
    %    stru_info{end+1} = '      <root/>';
    %else
    %    stru_info{end+1} = sprintf('      <root title="%s" id="%d"/>', ...
    %        this.graph{n}.get('title'), n);
    %end
    n = this.params.get('target');
    if isinf(n)
        stru_info{end+1} = '      <target/>';
    else
        stru_info{end+1} = sprintf('      <target title="%s" id="%d"/>', ...
            this.graph{n}.get('title'), n);
    end
    stru_info{end+1} = '   </global_parameters>';
    % enrichments
    stru_info{end+1} = '   <enrichments>';
    for e=1:length(this.enrichments)
        stru_info{end+1} =sprintf('      <enrichment name="%s">', this.enrichments(e).name);
        e_content = this.enrichments(e).content.get('fun');
        if ischar( e_content)
            e_content = ['"' e_content];
        else
            e_content = func2str(e_content);
        end
        stru_info{end+1} =sprintf('         <function>%s</function>', e_content);
        pos = this.enrichments(e).content.get('position');
        stru_info{end+1} = sprintf('         <position xmin="%d" xmax="%d" ymin="%d" ymax="%d" center="[%d,%d]"/>', ...
            ceil(pos(1)), ceil(pos(2)), ceil(pos(3)), ceil(pos(4)), ...
            ceil((pos(1)+pos(2))/2), ceil((pos(3)+pos(4))/2));
        pos = this.enrichments(e).content.get('txtcolor');
        stru_info{end+1} =sprintf('         <txtcolor rgb="[%d,%d,%d]"/>', pos(1), pos(2), pos(3));
        pos = this.enrichments(e).content.get('color');
        stru_info{end+1} =sprintf('         <color rgb="[%d,%d,%d]"/>', pos(1), pos(2), pos(3));
        stru_info{end+1} =sprintf('         <display>%d</display>', this.enrichments(e).content.get('display'));
        stru_info{end+1} =sprintf('      </enrichment>');
    end
    stru_info{end+1} = '   </enrichments>';
    % nodes
    stru_info{end+1} = '   <nodes>';
    for n=1:length(this.graph)
        if ~isempty( this.graph{n})
            stru_info{end+1} = this.graph{n}.info();
        end
    end
    stru_info{end+1} = '   </nodes>';
    
    stru_info{end+1} = '</graph>';
    stru_info = cellflat( stru_info);
end
%%** Get children of one node
% direction in or out
function c = children( node, direction, n, sc, all_c)
    c   = get_connections( node, direction);
    if ~isempty(c )
        c   = [c(:,2), ones(size(c,1),1)];
        all_c = c;
        for n=1:size(c,1)
            sc = children( c(n,1), direction);
            if ~isempty( sc)
                all_c = [all_c; [sc(:,1), sc(:,2)+1]];
            end
        end
        c = all_c;
    end
end
%%** Get anything internal
function o = get_( varargin)
    if nargin==0
        o = this;
        return
    end
    switch lower(varargin{1})
        case 'this'
            o = this;
        case 'graph'
            o = this.graph;
        case 'params'
            o = this.params;
        case 'enrichments'
            o = {};
            if ~isfield( this, 'enrichments')
                this.enrichments = [];
            else
                o = this.enrichments;
                if ~isempty(o) && nargin>1 && strcmpi(varargin{2}, 'names')
                    o = {o.name};
                end
            end
        case 'node'
            o = get_node( varargin{2:end});
        otherwise
            error('st_grapher:get:object', 'I do not know object <%s>', varargin{1});
    end
end
%%** Add node
function id = add_node( varargin)
    this.graph{end+1} = st_node( 'id', length(this.graph)+1, varargin{:});
    id = this.graph{end}.get('id');
    if id==1
        this.params.set('root', id);
    end
end
%%** Node set
function v = node_set(n, varargin)
    v = this.graph{node_id( n)}.set( varargin{:});
end
%%** Node Id
function idx = node_id( n_name, n)
    if ~ischar(n_name)
        idx = n_name;
    else
        idx = nan;
        for n=1:length(this.graph)
            if ~isempty(this.graph{n}) && strcmpi(this.graph{n}.get('title'),n_name)
                idx = n;
                break
            end
        end
        if isnan(idx)
            error('st_grapher:node_id', 'unable to find exactly one node names <%s>', n_name);
        end
    end
end

%%** Get node
function [n, idx] = get_node( n_name, key)
    idx = node_id( n_name);
    n = this.graph{idx};
    if nargin>1
        n = n.get( key);
    end
end
%%** Connect nodes
function [idx1, idx2] = connect( n1, o1, n2, i2)
    [n1, idx1] = get_node( n1);
    [n2, idx2] = get_node( n2);
    
    [n1, n2] = node_tools( 'connect', n1, o1, n2, i2);
    this.graph{idx1} = n1;
    this.graph{idx2} = n2;
end
%%** Unconnect nodes
function [idx1, idx2] = unconnect( n1, o1, n2, i2)
    [n1, idx1] = get_node( n1);
    [n2, idx2] = get_node( n2);
    
    [n1, n2] = node_tools( 'unconnect', n1, o1, n2, i2);
    this.graph{idx1} = n1;
    this.graph{idx2} = n2;
end
%%** Get/Set global parameters
function v = get_global( varargin)
    v = this.params.get( varargin{:});
end
function v = set_global( name, value)
    v = this.params.set( name, value);
end
%%** Get memory
function o = get_memory( n_name, varargin)
    o = get_node( n_name);
    o = o.get_memory( varargin{:});
end
%%** Set memory
function o = set_memory( n_name, varargin)
    o = get_node( n_name);
    o = o.set_memory( varargin{:});
end
%%** Get param
function o = get_param( n_name, varargin)
    o = get_node( n_name);
    o = o.get_param( varargin{:});
end
%%** Set param
function o = set_param( n_name, varargin)
    o = get_node( n_name);
    o = o.set_param( varargin{:});
end
%%** Get outputs
function o = get_outputs( n_name, varargin)
    o = get_node( n_name);
    o = o.outputs( varargin{:});
end
%%** Clear outputs
function s = clear_outputs( u, n)
    s = 0;
    for u=1:length(this.graph)
        if ~isempty( this.graph{u})
            this.graph{u}.set('outputs', {});
            s=s+1;
        end
    end
end
%%** Get target outputs
function o = get_target_outputs(varargin)
    o = this.graph{this.params.get('target')}.outputs( varargin{:});
end
%%** Get root outputs
function o = get_root_outputs(varargin)
    o = this.graph{this.params.get('root')}.outputs( varargin{:});
end
%%** Get connections
function o = get_connections( n_name, mode, n1)
    n1 = get_node( n_name);
    o  = n1.get([ mode '_connections']);
end
%%** length
function o = length_
    o  = length(this.graph);
end
%<* length
function o = max_id( name, n)
    o = -Inf;
    for n=1:length(this.graph)
        if ~isempty(this.graph{n})
            o = max([o, this.graph{n}.get('id')]);
        end
    end
end
%>*
%%** set position
function o = set_position( n_name, pos, n1, id1)
    [n1, id1] = get_node( n_name);
    n1.set('position', pos);
    this.graph{id1} = n1;
end
%<* delete
function o = delete_node( n_name, id, oc, c, nc)
    id = node_id( n_name);
    % il faut aussi supprimer les connections des noueds voisins
    oc = get_connections(id, 'out');
    if ~isempty( oc)
        for c=1:size(oc,1)
            unconnect(id, oc(c,1), oc(c,2), oc(c,3));
        end
    end
    oc = get_connections(id, 'in');
    if ~isempty( oc)
        for c=1:size(oc,1)
            unconnect(oc(c,2), oc(c,3), id, oc(c,1));
        end
    end
    
    this.graph{id} = [];
end
%>*
%<* Init a node
function init_node( n, varargin)
    if ischar(n)
        n = node_id( n);
    end
    this.graph{n}.state('init', varargin{:});
end
%>*
%<* Init all
function init(n )
    this.params.set('trace', []);
    try
        verbosity = this.params.get_no_err_no_ddot('verbosity');
    catch er
        verbosity = inf;
        st_log('Check that you have the last version of params...\n');
    end
    for n=1:length(this.graph)
        if ~isempty( this.graph{n})
            if verbosity > 0
                st_log('>>> init box <%s:%d> <<<\n', this.graph{n}.get('title'), n);
            end
            params = this.graph{n}.get_param();
            if isempty(params)
                params = {};
            else
                params = params.get();
            end
            this.graph{n}.state('init', params{:});
            this.graph{n}.set('postcondition', false);
            % pour l'instant je ne remet pas les outputs � false
        end
    end
end
%>*
%<* Remise � jour des postconditions
% En fait il faudra le faire � rebour en checkant |end_of_prod| 
function reset_postcond( idx)
    %< R�cup�ration de la trace
    if nargin<1
        idx = this.params.get('trace');
    end
    if isempty(idx)
        % si il n'y en a pas, je fais un truc simpliste
        idx = [];
        for n=1:length(this.graph)
            if ~isempty( this.graph{n})
                idx = [idx, n];
            end
        end
    end
    % je RETOURNE la trace
    idx = flipud(idx(:));
    %>
    for n=1:length(idx)
        if ~isempty(this.graph{idx(n)})
            this.graph{idx(n)}.set('postcondition', false);
            if ~this.graph{idx(n)}.end_of_prod()
                % je m'arr�te d�s que je renconter quelqu'un qui doit encore
                % produire
                return
            end
        end
    end
end
%>*
%<* gloabl end_of_prod
function b = global_end_of_prod
    b = true;
    for n=1:length(this.graph)
       if ~isempty(this.graph{n})
           if ~this.graph{n}.end_of_prod()
               b = false;
               return
           end
       end
   end
end
%>*
%<* Get inputs
function inputs = get_inputs( n_name, ic)
    i  = node_id( n_name);
    ic = get_connections(i, 'in');
    inputs  = repmat({[]}, 1, max(ic(:,1)));
    for i=1:size( ic)
        inputs{ic(i,1)} = this.graph{ic(i,2)}.outputs( ic(i,3));
    end
end
%>*

%<* Apprentissage
function trace = learn( n_name, i, stacked_inputs, conns)
    % TODO: prise en compte des param�tres globaux par les noeuds
    set_global('phase', 'learn');
    i = node_id( n_name);
    %< Stack des inputs
    stacked_inputs = {};
    st_log('>>> learn: step %d <<<\n', length(stacked_inputs)+1);
    trace = next( i);
    stacked_inputs{end+1} = get_inputs( i);
    while ~all(arrayfun(@(n)this.graph{n}.end_of_prod(),trace))
        % ~this.graph{this.params.get('root')}.end_of_prod()
        st_log('>>> learn: step %d <<<\n', length(stacked_inputs)+1);
        trace = next( i);
        stacked_inputs{end+1} = get_inputs( i);
    end
    %>
    %< Feval du mode learn
    st_log('>>> learning node <%d:%s> <<<\n', i, n_name);
    this.graph{i}.state('learn', stacked_inputs);
    this.graph{i}.set('outputs', {});
    st_log('>>> end of learning phase <<<\n');
    set_global('phase', 'exec');
    %>
end
%>*

%<* Sequencement
function trace = next( n_name, idx, ronde, n, trace, ic, i, outputs, inputs, sync, t, verbosity)
    try
        verbosity = this.params.get_no_err_no_ddot('verbosity');
    catch er
        verbosity = inf;
        st_log('Check that you have the last version of params...\n');
    end
    if isempty(verbosity)
        verbosity = inf;
    end
    if verbosity > 0
        st_log('--> Next run for graph <%s>...\n', this.params.get('title'));
    end
    try
        syst_plot = tokenize(this.params.get('systematic-plot'));
    catch er
        syst_plot = {};
    end
    sync = ceil( rand(1)*200);
    if nargin < 2
        idx = this.params.get('trace');
        if isempty( idx) || length(idx)~=length(this.graph)
            idx = 1:length(this.graph);
        end
    end
    ronde = repmat(true, 1, length(this.graph));
    if nargin > 0
        % j'ai une cible
        i = node_id( n_name);
        %%t = find( idx==i);
        t = children( i, 'out');
        if ~isempty( t)
            t = t(:,1);
        end
        %idx([i;t])    = [];
        ronde([t; i]) = false;
        reset_postcond( idx);
    else
        % sinon �a sera pour tout le monde
        reset_postcond;
    end
    trace = [];
    n = 1;
    while any( ronde)
        % pour chaque �l�ment
        if isempty( this.graph{idx(n)}) || ...
                this.graph{idx(n)}.get('postcondition')
            % je ne traite pas les vides
            % ni les termin�s 
            if ~isempty( this.graph{idx(n)})
                if verbosity > 0
                    st_log('>>> Node [%s:%d] closed... <<<\n', this.graph{idx(n)}.get('title'), idx(n));
                end
            end
            ronde(idx(n)) = false; % normalement pas n�cessaire
        else
            % je peux travailler sur lui
            ic = get_connections(idx(n), 'in');
            if isempty(ic) || all( cellfun(@(x)x.get('postcondition'), this.graph(ic(:,2))))
                if verbosity > 0
                    st_log('>>> Node [%s:%d] launched... <<<\n', this.graph{idx(n)}.get('title'), idx(n));
                end
                if ~isempty( ic)
                    inputs  = repmat({[]}, 1, max(ic(:,1)));
                    for i=1:size( ic)
                        inputs{ic(i,1)} = this.graph{ic(i,2)}.outputs( ic(i,3));
                    end
                else
                    inputs = {};
                end
                t = cputime;
                % je regarde si je suis sur une entr�e parall�le
                nb_ins   = cellfun(@length, inputs);
                % gestion du model parallel
                n_state  = this.graph{idx(n)}.get('state');
                if ~isfield( n_state, 'parallel')
                    par_mode = false;
                else
                    par_mode = n_state.parallel;
                    if isempty( par_mode)
                        par_mode = true;
                    elseif ~isreal( par_mode)
                        par_mode = feval( par_mode);
                    end
                end
                % bouclage �ventuel
                if any( nb_ins>1) && ~par_mode
                    if verbosity > 0
                        st_log('..> implicit parallel mode for node <%s>...\n', this.graph{idx(n)}.get('title'));
                    end
                    if any( nb_ins~=nb_ins(1))
                        error('st_grapher:parallel', 'at this stage, st_grapher cannot manager heterogeneous parallel mode...');
                    end
                    % je vais empiler les sorties dans le mauvais sens
                    % (mais c'est le plus naturel)
                    outp = cell(nb_ins(1),1);
                    outm = -Inf;
                    for i=1:nb_ins(1)
                        inp     = cellfun(@(c)c{i}, inputs, 'uni', false);
                        outp{i} =  this.graph{idx(n)}.state('exec', inp);
                        outm = max([outm, length(outp{i})]);
                    end
                    % et maintenant je les d�pile
                    outputs = cell(1, outm);
                    for o=1:outm
                        outputs{o} = cellfun(@(c)c{o}, outp, 'uni', false);
                    end
                    if nb_ins==1
                       % si je ne suis pas en mode parallele, alors je regarde si j'ai un affichage auto ou pas
                       if ismember( this.graph{idx(n)}.get('title'), syst_plot)
                           try
                               plot( n);
                               st_log('    [node:%s] <-- systematic plot\n', this.graph{idx(n)}.get('title'));
                           catch er
                               st_log('    [node:%s] <-- systematic plot FAILED!<<%s>>\n', ...
                                   this.graph{idx(n)}.get('title'), er.message);
                           end
                       end
                    end
                else
                    % ou bien pas de raison d'�tre en parallele ou bien
                    % parallele explicite
                    if verbosity > 0
                        st_log('..> explicit (not parallel) mode for node <%s>...\n', this.graph{idx(n)}.get('title'));
                    end
                    outputs = this.graph{idx(n)}.state('exec', inputs);
                    
                    if ismember( this.graph{idx(n)}.get('title'), syst_plot)
                        try
                            plot( n);
                            st_log('    [node:%s] <-- systematic plot\n', this.graph{idx(n)}.get('title'));
                        catch er
                            st_log('    [node:%s] <-- systematic plot FAILED!<<%s>>\n', ...
                                this.graph{idx(n)}.get('title'), er.message);
                        end
                    end
                end
                this.graph{idx(n)}.set('cpu', cputime -t);
                this.graph{idx(n)}.set('outputs', outputs);
                this.graph{idx(n)}.set('postcondition', true);
                ronde(idx(n)) = false;
                trace = [trace, idx(n)];
                this.graph{idx(n)}.set('synchro', sync);
            else
                if verbosity > 0
                    st_log('>>> Node [%s:%d] is waiting... <<<\n', this.graph{idx(n)}.get('title'), idx(n));
                end
            end
        end
        
        n=n+1;
        % je boucle en fin de ronde
        if n>length(ronde)
            n=1;
        end
    end
    % que faire de trace?
    this.params.set('trace', trace);
    if verbosity > 0
        st_log('<-- End of run for graph <%s>...\n', this.params.get('title'));
    end
end
%>*
%<* Get/set plot
function h = plot( n)
    lst = this.params.get();
    h = this.graph{n}.state('plot', lst{:});
end
%>*
%<* Visual parameters modif
function visual_param( n_id)
    this.graph{n_id}.visual_param();
end
%>*
%<* Compile
% fname est le nom de la fonction, pas du fichier
function gr = compile( fname)
    [p,f,e] = fileparts( fname);
    if isempty(p)
        p = fullfile(getenv('st_work'), 'to_compile', fname);
        if isempty(dir(p))
            mkdir( p);
        end
    end
    fname = fullfile(p, [f '.m']);
    fmat  = fullfile(p, [f '.mat']);
    str = { sprintf('function %s( cmds)', f) };
    str{end+1} = sprintf('%% %s - standalone compilation of grapher <%s>', upper(f), get_global('title'));
    str{end+1} = '%   compile with:';
    %str{end+1} = sprintf('%%     mcc -m %s -a %s', f, fmat);
    str{end+1} = sprintf('%%     st_compile gr2exe %s append %s.mat %s.xml', f, f, f);
    % help
    str{end+1} = '';
    str{end+1} = 'if nargin>0 && strcmpi(cmds, ''help'')';
    str{end+1} = sprintf('   fprintf(''use: %s [no-commands]\\n'');', f);
    str{end+1} = '   return';
    str{end+1} = 'end';
    % loading du startup
    str{end+1} = '';
    str{end+1} = '% Run the startup to load st_version and the drivers';
    str{end+1} = 'if isdeployed';
    str{end+1} = '   mcr_startup';
    str{end+1} = 'end';
    % Chargement du graphe
    str{end+1} = '';
    str{end+1} = '% Load the mat file ';
    save( fmat, 'this');
    str{end+1} = 'd = fileparts(which(mfilename));';
    str{end+1} = sprintf('g = load( fullfile(d, ''%s.mat''));', f);
    str{end+1} = 'gr = st_grapher(''clone'', g);';
    str{end+1} = 'clear g;';
    str{end+1} = '';
    str{end+1} = 'if nargin>0 && strcmpi(cmds, ''no-commands'')';
    str{end+1} = '   gr.set_global(''display-commands'', false);';
    str{end+1} = 'end';
    % correction dynamique du chemin
    str{end+1} = '[tmp, fname, ext] = fileparts(gr.get_global(''filename''));';
    str{end+1} = 'gr.set_global(''filename'', fullfile( d, [fname, ext]));';
    % pragma des noeuds
    str{end+1} = '';
    str{end+1} = '% nodes';
    for n=1:length(this.graph)
        if ~isempty( this.graph{n})
            str{end+1} = sprintf('%%#function %s', this.graph{n}.get('st_function'));
        end
    end
    % pragma des enrichments
    str{end+1} = '';
    str{end+1} = '% enrichments';
    for r=1:length(this.enrichments)
        rfun = this.enrichments(r).content.get('fun');
        if ~ischar(rfun)
            rfun = func2str(rfun);
        end
        if rfun(1)~='@'
            str{end+1} = sprintf('%%#function %s', rfun);
        end
    end
    % pragma des enrichments
    str{end+1} = '';
    str{end+1} = '% pragma std';
    str{end+1} = '%#function st_grapher';
    str{end+1} = '%#function st_node';
    str{end+1} = '%#function st_plot';
    str{end+1} = '%#function st_data';
    % lancement du graphe
    str{end+1} = '';
    str{end+1} = '% Lancement du graphe';
    str{end+1} = 'win_graph(''show'', gr);';
    
    fod = fopen( fname , 'w');
    fprintf(fod, '%s\n', str{:});
    fclose(fod);
end
%>*
%<* convert to matlab code
function str = code( fname, varargin)
    if nargin<2
        cmode = 'm';
    else
        cmode = varargin{1};
    end
    switch lower( cmode)
        case 'm'
            xmode = 0;
        case 'xml'
            xmode = 1;
        otherwise
            error('st_grapher:code', 'output format <%s> unknown', cmode);
    end
    if xmode==0
        str = { sprintf( '%% Building graph <%s>', this.params.get('title'))};
    elseif xmode == 1
        str = { '<?xml version="1.0" encoding="ISO-8859-1"?>', ...
            '<graph>', ...
            sprintf( '<!-- Building graph %s -->', this.params.get('title')) };
    end
    try
        st_v =  st_version.st_grapher;
    catch
        lasterr('');
        st_v = 'unknown';
    end
    if xmode==0
        str{end+1} = sprintf('% Code generated by st_grapher version %s', st_v );
        str{end+1} = '';
        str{end+1} = '%% Creation of the grapher ';
        str{end+1} = 'gr = st_grapher;';
        str{end+1} = 'id = options({});';
    elseif xmode == 1
        str{end+1} = sprintf('<!-- Code generated by st_grapher version %s -->', st_v );
        str{end+1} = '';
        str{end+1} = '<nodes>';
        % pas besoin de grapher
    end
    % nodes
    for n=1:length(this.graph)
        if ~isempty( this.graph{n})
            if xmode==0
                str{end+1} = sprintf('%% Creation of node <%s>', this.graph{n}.get('title'));
                str{end+1} = sprintf('num = gr.add_node( ''st_function'', ''%s'', ''title'', ''%s'' );', ...
                    this.graph{n}.get('st_function'), this.graph{n}.get('title') );
                % NOT CORRECT
                str{end+1} = 'gr.node_set(num,''id'', num);';
                pos = floor(this.graph{n}.get('position'));
                str{end+1} = sprintf('gr.node_set(num, ''position'', [%d %d %d %d]);', pos);
                str{end+1} = sprintf('id.set(''%d'', num);', n);
                str{end+1} = 'gr.init( num);';
            elseif xmode ==1
                str{end+1} = sprintf('   <!-- Creation of node |%s| -->', this.graph{n}.get('title'));
                str{end+1} = sprintf('   <node name="%s" st_function="%s">', ...
                    this.graph{n}.get('st_function'), this.graph{n}.get('title') );
                % NOT CORRECT
                pos = floor(this.graph{n}.get('position'));
                str{end+1} = sprintf('      <position coords="[%d %d %d %d]"/>', pos);
            end
            % params
            params = this.graph{n}.get_param();
            lst    = params.get();
            for p=1:2:length(lst)-1
                v = lst{p+1};
                if isstr( v)
                    v = sprintf('''%s''', v);
                elseif isa(v, 'function_handle')
                    v = func2str( v);
                    if v(1)~='@'
                        v = ['@' v];
                    end
                else
                    v = num2str( v);
                end
                if xmode == 0
                    str{end+1} = sprintf('gr.set_param( num, ''%s'', %s);', lst{p}, v);
                elseif xmode == 1
                    str{end+1} = sprintf('      <param name="%s" value="%s"/>', lst{p}, v);
                end
            end
            %str{end+1} = 'gr.node_set(num,''id'', num);';
            if xmode ==1
                str{end+1} = '   </node>';
            end
        end
    end
    if xmode ==1
         str{end+1} = '</nodes>';
    end
    str{end+1} = '';
    % connections
    if xmode ==0
        str{end+1} = '%% Connections';
    elseif xmode ==1
         str{end+1} = '<!-- Connections -->';
         str{end+1} = '<connections>';
    end
    for n=1:length(this.graph)
        if ~isempty( this.graph{n})
            if xmode ==0
                str{end+1} = sprintf('%% connection of node <%s>', this.graph{n}.get('title'));
            elseif xmode ==1
                str{end+1} = sprintf('<!-- connection of node |%s| -->', this.graph{n}.get('title'));
            end
            out_c = get_connections( n, 'out');
            for c=1:size(out_c,1)
                if xmode ==0
                    str{end+1} = sprintf('gr.connect(id.get(''%d''),%d,id.get(''%d''),%d);', n, out_c(c,:));
                elseif xmode ==1
                    str{end+1} = sprintf('   <connect node="%s" out="%d" to="%s" in="%d"/>', this.graph{n}.get('title'), out_c(c,1), ...
                        this.graph{out_c(c,2)}.get('title'), out_c(c,3) );
                end
            end
        end
    end
    str{end+1} = '';
    if xmode ==1
         str{end+1} = '</connections>';
    end
    % Enrichments
    if xmode ==0
        str{end+1} = '%% Enrichments';
    elseif xmode ==1
        str{end+1} = '<enrichments>';
    end
    for r=1:length(this.enrichments)
        rfun = func2str(this.enrichments(r).content.get('fun'));
        if rfun(1)~='@'
            rfun = ['@' rfun];
        end
        pos = this.enrichments(r).content.get('position');
        col = this.enrichments(r).content.get('color');
        tol = this.enrichments(r).content.get('txtcolor');
        if xmode ==0
            str{end+1} = sprintf( 'gr.set_enrichment( ''%s'', ''fun'', %s, ''position'', [%f %f %f %f], ''display'', %d, ''color'', [%f %f %f], ''txtcolor'', [%f %f %f]);', ...
                this.enrichments(r).name, rfun, pos(1), pos(2), pos(3), pos(4), ...
                this.enrichments(r).content.get('display'), col(1), col(2), col(3), ...
                tol(1), tol(2), tol(3) );
        elseif xmode ==1
            str{end+1} = sprintf( '   <enrichment name="%s" function="%s">', this.enrichments(r).name, rfun);
            str{end+1} = sprintf( '      <display position="[%f %f %f %f]"  visible="%d" color="[%f %f %f]" txtcolor="[%f %f %f]"/>', ...
                pos(1), pos(2), pos(3), pos(4), ...
                this.enrichments(r).content.get('display'), col(1), col(2), col(3), ...
                 tol(1), tol(2), tol(3) );
            str{end+1} = '   </enrichment>';
        end
    end
    if xmode ==1
        str{end+1} = '</enrichments>';
    end
    str{end+1} = '';
    if xmode ==0
        str{end+1} = '%% Show it';
        str{end+1} = 'win_graph(''show'', gr);';
    elseif xmode ==1
         str{end+1} = '<!-- Connections -->';
         str{end+1} = '</graph>';
    end
    % save
    fod = fopen( fname, 'w');
    fprintf(fod, '%s\n', str{:});
    fclose(fod);
    st_log('not fully functionnal yet...\n');
end
%>*
%<* Set node param
function v = set_node_param(n, varargin)
    this.graph{node_id( n)}.set_param( varargin{:});
end
%>*
%<* convert to matlab code
% Varargin vont �tre utilis�s pour customizer les op�rations
function str = export( fname, varargin)
    str = { sprintf( '%% Code to execute graph <%s>', this.params.get('title'))};
    str{end+1} = '';
    str{end+1} = '% Load the mat file ';
    [p,f,e] = fileparts( fname);
    save( fullfile( p, [f '.mat']), 'this');
    str{end+1} = sprintf('g = load( ''%s.mat'');', f);
    str{end+1} = 'gr = st_grapher(''clone'', g.this);';
    str{end+1} = 'clear g;';
    str{end+1} = '';
    str{end+1} = '% Init the graph';
    str{end+1} = 'gr.init();';
    str{end+1} = '';
    str{end+1} = '% Exec the graph';
    str{end+1} = 'while ~gr.global_end_of_prod()';
    str{end+1} = '   gr.next();';
    str{end+1} = 'end';
    
    % save
    fod = fopen( fname, 'w');
    fprintf(fod, '%s\n', str{:});
    fclose(fod);
end
%>*
%<* Export to a market simulation
function str = market( fname, varargin)
    str = { sprintf('%%%% Code to execute market sim <%s>', this.params.get('title'))};
    str{end+1} = '';
    str{end+1} = '%% I need this tool...';
    str{end+1} = 'f_exp=@(x)x{:};';
    % load the mat file (for the node info)
    str{end+1} = '% Load the mat file ';
    [p,f,e] = fileparts( fname);
    save( fullfile( p, [f '.mat']), 'this');
    str{end+1} = sprintf('g = load( ''%s.mat'');', f);
    str{end+1} = 'if isfield(g , ''this'')';
    str{end+1} = '    gr = st_grapher(''clone'', g.this);';
    str{end+1} = 'else';
    str{end+1} = '    gr = g.gr;';
    str{end+1} = 'end';
    str{end+1} = 'graph = gr.get(''graph'');';
    str{end+1} = 'clear g;';
    % load the market source 
    str{end+1} = '% load the market source';
    root_id = get_global('root');
    market_source = this.graph{root_id}.get_param('market-source:char');
    str{end+1} = sprintf('market_source = %s( %s);', market_source, ...
        this.graph{root_id}.get_param('market-source-params:char')); 
    % initialisation des trat�gies
    str{end+1} = '% init strategies';
    sons = children(root_id, 'in');
    str{end+1} = 'strat_builder=@(ob,hist,o,ho)struct(''orderbook'', ob, ''history'', hist, ''orders'', o, ''ob_history'', ho);';
    str{end+1} = 'warning off';
    str{end+1} = 'empty_ob = st_data(''init'', ''title'', ''orderbook'', ''value'', [], ''date'', [], ''colnames'', {''bid'', ''ask'', ''price'', ''volume''});';
    str{end+1} = 'empty_or = st_data(''init'', ''title'', ''orders'', ''value'', [], ''date'', [], ''colnames'', {''price'', ''volume'', ''direction''});';
    str{end+1} = 'strategy = strat_builder({},{},{},{});';
    str{end+1} = 'warning on';
    for s=1:size(sons,1)
        str{end+1} = 'strategy(end+1) = strat_builder(empty_ob, {cell(1, market_source.size())}, empty_or, {cell(1, market_source.size())});';
    end
    % tant qu'il y a des donn�es
    str{end+1} = '%% boucle principale';
    str{end+1} = 'u=1;';
    str{end+1} = 'while ~market_source.end_of_prod()';
    str{end+1} = '    if mod(u,100)==0';
    str{end+1} = '        st_log(''.'');';
    str{end+1} = '        if mod(u,3000)==0';
    str{end+1} = '            st_log(''\n'');';
    str{end+1} = '        end';
    str{end+1} = '    end';
    % ask for the next market data
    str{end+1} = '    % ask for the next market data';
    str{end+1} = '    market = market_source.next();';
    % calcul des indicateurs
    sons = children(root_id, 'out');
    if ~isempty( sons)
        [tmp, idx] = sort( sons(:,2));
        sons = sons(idx,:);
        for s=1:size(sons,1)
            this_id = sons(s,1);
            str{end+1} = sprintf('    %% indicator <%s> (rank %d)', this.graph{this_id}.get('title'), sons(s,2));
            those_inputs  = this.graph{this_id}.get_param('input-names:char');
            those_outputs = this.graph{this_id}.get_param('output-names:char');
            str{end+1} = sprintf('    [%s]=f_exp(graph{%d}.state(''exec'', {%s}));', ...
                strrep(those_outputs,';',','), sons(s,1), strrep(those_inputs,';',',') );
        end
    end
    % calcul des strat�gies
    sons = children(root_id, 'in');
    [tmp, idx] = sort( sons(:,2));
    sons = sons(idx,:);
    u = 1; % u est le num�ro d'ordre de la strat�gie
    for s=1:size(sons,1)
        this_id = sons(s,1);
        % impl�mentation de la strat�gie
        str{end+1} = sprintf('    %% trading strategy <%s> (rank %d)', this.graph{this_id}.get('title'), sons(s,2));
        those_inputs  = this.graph{this_id}.get_param('input-names:char');
        str{end+1} = sprintf('    my_orders=f_exp(graph{%d}.state(''exec'', {st_data(''fast-stack'', strategy(%d).history),strategy(%d).orderbook,%s}));', ...
            sons(s,1), u, u, strrep(those_inputs,';',',') );
        str{end+1} =  '    flatten_ob(''check-orders'', my_orders);';
        % mise � jour du carnet
        str{end+1} = '    % mise � jour du carnet';
        str{end+1} = sprintf('    my_orderbook = flatten_ob( my_orders, strategy(%d).orderbook);', u);
        str{end+1} =  '    flatten_ob(''check-orderbook'', my_orderbook);';
        % passage dans le march�
        str{end+1} = '    % passage dans le march�';
        str{end+1} = sprintf('    [my_orderbook, my_exec]=f_exp(graph{%d}.state(''exec'', { market, my_orderbook}));', root_id);
        str{end+1} =  '    flatten_ob(''check-orderbook'', my_orderbook);';
        str{end+1} =  '    flatten_ob(''check-exec'', my_exec);';
        % mise � jour
        str{end+1} = sprintf('    strategy(%d).history{u} = my_exec;', u);
        str{end+1} = sprintf('    strategy(%d).orders     = my_orders;', u);
        str{end+1} = sprintf('    strategy(%d).ob_history{u} = strategy(%d).orderbook;', u, u);
        str{end+1} = sprintf('    strategy(%d).orderbook  = my_orderbook;', u);
        u = u+1;
    end
    % fin de la boucle
    str{end+1} = '    u=u+1;';
    str{end+1} = 'end';
    str{end+1} = 'st_log(''\nDone.\n'');';
    % il faudra surtout trouver un moyen de r�cup�rer les r�sultats
    % pour plusieurs strat�gies
    str{end+1} = 'execs = st_data(''fast-stack'', strategy.history);';
    str{end+1} = 'market=market_source.all();';
    str{end+1} = sprintf('save(fullfile(getenv(''st_repository''), ''%s_sim.mat''), ''execs'', ''market'');', f);
    % save
    fod = fopen( fname, 'w');
    fprintf(fod, '%s\n', str{:});
    fclose(fod);
end
%>*
%<* Set enrichment
% Un 'enrichment' est un code ex�cutable associ� au graphe qui peut avoir
% une repr�sentation graphique.
% Les propri�t�s d'un enrichment sont:
% - fun
% - position
% - display ture/false*
% - txtcolor
% - color
function h = set_enrichment( rname, varargin)
    try
        del_enrichment( rname);
        st_log('Replacing enrichment <%s> by this new value\n', rname);
    catch
        lasterr('');
    end
    if isempty( this.enrichments)
        this.enrichments = struct('name', rname, ...
            'content', options({'fun', @()disp('Enrichment: nothing to do'), 'position', [], ...
            'color', [.67 0 .12], 'txtcolor', [1 1 1], 'display', false}, varargin));
    else
        this.enrichments(end+1) = struct('name', rname, ...
            'content', options({'fun', @()disp('Enrichment: nothing to do'), ...
            'position', [], 'color', [.67 0 .12], 'txtcolor', [1 1 1], 'display', false}, varargin));
    end
    h = this.enrichments;
end
%>*
%<* Get enrichment
function [h,idx] = get_enrichment( rname, varargin)
    idx = strmatch( lower(rname), lower({ this.enrichments.name}), 'exact');
    if isempty( idx)
        error('st_grapher:get_enrichment', 'No enrichment named <%s> is known', rname);
    elseif length(idx)>1
        st_log('st_grapher:get_enrichment - More that one enrichment named <%s> is known, I will take the first one', rname);
        idx = idx(1);
    end
    h  = this.enrichments(idx).content;
    if nargin>1
        h = h.get( varargin{:});
    end
end
%>*
%<* Del enrichment
function h = del_enrichment( rname)
    [h, idx] = get_enrichment( rname);
    this.enrichments(idx) = [];
end
%>*
%<* Run enrichment
function h = run_enrichment( rname, varargin)
    h = get_enrichment( rname, 'fun');
    st_log('--> Running enrichment <%s>:\n', rname);
    h = feval( h, gr, varargin{:});
    st_log('<-- End of enrichment <%s>\n', rname);
end
%>*
%<* Upgrade a grapher
function h = upgrade( n, this_params, lst)
    for n=1:length(this.graph)
        if ~isempty( this.graph{n})
            st_log('st_grapher: upgrading node <%s>...\n', this.graph{n}.get('title'));
            if isfield(this.graph{n}, 'upgrade')
                this.graph{n} = this.graph{n}.upgrade();
            else
                lst         = this.graph{n}.get();
                this_params = this.graph{n}.get_param();
                this_params = this_params.get();
                this.graph{n} = st_node( lst{:});
                init_node(n, this_params{:} );
            end
        end
    end
    h = st_grapher('clone', this);
end
%>*
end