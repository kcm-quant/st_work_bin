function node = st_node( varargin)
% ST_NODE - cr�ation d'un boite
%   le format est celui de options
% les champs possibles (obligatoires*) sont:
% - title*
% - st_function*
% - out_connections
% - in_connections
% - id*
% - outputs#
% - state#
% - plot_handle#
% - precond#
% - postcond#
%
% C'est ici que sont interfac�s/verrouill�s les modes des noeuds
%
% methodes disponibles:
% - get
% - set
% - eval( 'mode', ...)
% - info
%
% example:
%  n = st_node('st_function', 'st_inout')
%  n.state('plot', data)
%
% See also options node_tools 
if ischar(varargin{1})&&strcmpi(varargin{1}, 'clone')
    lst = varargin{2}.get();
    varargin = lst;
    warning('st_node:clone', 'attention: cela ne semble pas fonctionner correctement...');
end

this.node = options({'title', 'New node', 'st_function', 'st_inout', ...
    'out_connections', [], 'in_connections', [], 'id', -1, ...
    'outputs', {}, 'state', [], 'plot_handle', [], ...
    'precondition', false, 'postcondition', false, ...
    'position', [], 'synchro', [], 'cpu', [] }, varargin);

this.node.set('state', this.node.eval('st_function', varargin{:}));
    
node = struct('get', @get, 'set', @set, ...
              'outputs', @outputs, ...
              'end_of_prod', @eop, ...
              'state', @eval_state, 'info', @info, ...
              'set_param', @set_param, 'get_param', @get_param, ...
              'get_memory', @get_memory, 'set_memory', @set_memory, ...
              'visual_param', @visual_param );
          % TODO: ajout d'un mode 'upgrade'

%%** Embedded
%<* direct get
function [v, idx] = get( varargin)
    [v, idx] = this.node.get( varargin{:});
end
%>*
%<* Direct set
function [v, idx] = set( varargin)
    [v, idx] = this.node.set( varargin{:});
end
%>*
%<* Get memory
function o = get_memory( key, state, mem)
    o = {};
    state = this.node.get('state');
    mem = state.get('memory');
    if nargin==0
        o = mem.get();
    else
        o = mem.get( key);
    end
end
%>*
%<* Set memory
function val = set_memory( key, val, state, mem)
    state = this.node.get('state');
    if ischar(key)
        mem = state.get('memory');
        mem.set( key, val); % non test�
    else
        val = key;
        state.set('memory', val);
    end
end
%>*
%<* Eval
function o = eval_state( mode, varargin)
    o = {};
    state = this.node.get('state');
    switch lower(mode)
        case 'init'
            state.init( varargin{:});
        case 'exec'
            o = state.exec( varargin{:}, 'title', this.node.get('title'), 'id', this.node.get('id'));
        case 'learn'
            if isempty( state.learn)
                warning('st_node:eval_state:mode', 'structured node <%s> has no learning capability', this.get('title'));
            else
                state.learn( varargin{:});
            end
        case 'plot'
            % est-ce que je suis redirig�
            opt = options(varargin);
            p_idx = [];
            try
                p_corresp = opt.get('panel-correspondances');
                if ~isempty( p_corresp)
                    p_idx = regexp([ p_corresp, ';'], ['([0-9]+)(?=:' this.node.get('title') ';)'], 'match');
                    if ~isempty( p_idx)
                        p_idx = str2double( p_idx);
                    else
                        p_idx = [];
                    end
                end
            catch er
                p_corresp = '';
            end
            try
                p_handles = opt.get('panels-layout-handles');
            catch er
                p_handles = [];
            end
            if ~isempty(p_corresp) && isempty(p_handles)
                st_log('st_node:eval_state:plot no panel handle, and some needed...\n');
                p_idx = [];
                p_corresp = '';
            end
            if ~isempty( p_idx)
                this_handle = p_handles(p_idx);
                st_log('st_node:eval_state:plot I will redirect this plot on a win_grapher panel\n');
            else
                this_handle = [];
            end
            % gestion de la figure et du panel
            
            %h  = this.node.get('plot_handle');
            hn = findobj( 'name', this.node.get('title'));
            %if  ~isempty(h) && ishandle(h)
            %    figure(h);
            fset = evalin('base', 'str2func(''set'')');
            if ~isempty(hn)
                figure(hn);
                if ~isempty(p_idx)
                    feval(fset, hn, 'visible', 'off');
                end
            else
                if ~isempty(p_idx)
                    % je vais le rerouter donc je l'affiche d'abord dans un
                    % truc invisible
                    figure('name', this.node.get('title'), 'NumberTitle', 'off', 'visible', 'off');
                else
                    figure('name', this.node.get('title'), 'NumberTitle', 'off', 'visible', 'on');
                end
                hp = uipanel('backgroundcolor', [238 238 208]/256, 'borderwidth', 0);
                this.node.set('plot_handle', hp);
            end
            hp = this.node.get('plot_handle');
            if isempty( state.plot)
                o = st_plot( this.node.get('outputs'), varargin{:}, 'panel', hp);
            elseif ischar( state.plot)
                o = feval( state.plot, varargin{:}, this.node.get('outputs'), 'panel', hp);
            elseif strcmp(class(state.plot), 'function_handle')
                o = state.plot(this.node.get('outputs'), varargin{:}, 'panel', hp);
            else
                error('st_node:plot', 'plot associated with <%s> unknown',  this.node.get('st_function'));
            end
            if ~isempty( p_idx)
                all_child = findobj(this_handle);
                delete(all_child(all_child~=this_handle));
                all_axes = findobj(gcf, 'type', 'axes');
                feval(fset,all_axes, 'parent', this_handle);
            end
        case 'params'
            o = state.params( varargin{:});
        otherwise
            error('st_node:eval_state:mode', 'mode <%s> unknwon', mode);
    end
end
%>*
%<* Get param
function [p,t] = get_param( name, state, prm, u)
    state = this.node.get('state');
    if nargin==0
        p = state.get('params');
        t = {};
        return
    end
    prm   = state.get('params');
    p     = prm.get( name);
    t     = '';
    % � explorer...
    if ischar( p)
        warning('mysterious tokenize warning: please call charles...');
        p = tokenize(p,':');
        if length(p)>1
            t = p{2};
        else
            t = '';
        end
        p     = p{1};
    end
end
%>*
%<* Set param
function state = set_param( varargin) % value, state, prm)
    state = this.node.get('state');
    prm   = state.get('params');
    % check type?!?
    prm.set( varargin{:});
    state.set( 'params', prm);
    this.node.set('state', state);
end
%>*
%<* Visual params
function visual_param
    state = this.node.get('state');
    prm   = state.get('params');
    if isempty( prm)
        st_log('node <%s> has no parameter yet, launch an ''init'' phase and try again...\n',this.node.get('title'))
    else
        prm.visual( this.node.get('title'));
        state.set( 'params', prm);
        this.node.set('state', state);
    end
end
%>*
%<* Info about the node
function ifo = info( state, params, p_info, p, cn, pos, tp)
    % core info
    ifo      = { sprintf('   <node title="%s" id="%d">', this.node.get('title'), this.node.get('id'))};
    state    = this.node.get('state');
    str_plot = state.plot;
    if isempty( str_plot)
        str_plot = 'st_plot';
    elseif strcmp(class(state.plot), 'function_handle')
        str_plot = func2str(state.plot);
    end
    ifo{end+1} = sprintf('      <st_function>%s</st_function>', this.node.get( 'st_function'));
    ifo{end+1} = sprintf('      <plot>%s</plot>', str_plot);
    ifo{end+1} = sprintf('      <visual>');
    pos = this.node.get('position');
    ifo{end+1} = sprintf('         <position xmin="%d" xmax="%d" ymin="%d" ymax="%d" center="[%d,%d]"/>', ...
        ceil(pos(1)), ceil(pos(2)), ceil(pos(3)), ceil(pos(4)), ...
        ceil((pos(1)+pos(2))/2), ceil((pos(3)+pos(4))/2));
    ifo{end+1} = sprintf('      </visual>');
    % connections
    % out
    cn = this.node.get('out_connections');
    ifo{end+1} = '      <output>';
    for p=1:size(cn,1)
        ifo{end+1} = '         <connection>';
        ifo{end+1} = sprintf('            <source      node_id="%d" connection_id="%d"/>', ...
            this.node.get('id'), cn(p,1) );
        ifo{end+1} = sprintf('            <destination node_id="%d" connection_id="%d"/>', ...
            cn(p,3), cn(p,2));
        ifo{end+1} = '         </connection>';
    end
    ifo{end+1} = '      </output>';
    % in
    cn = this.node.get('in_connections');
    ifo{end+1} = '      <input>';
    for p=1:size(cn,1)
        ifo{end+1} = '         <connection>';
        ifo{end+1} = sprintf('            <source      node_id="%d" connection_id="%d"/>', ...
            cn(p,2), cn(p,3));
        ifo{end+1} = sprintf('            <destination node_id="%d" connection_id="%d"/>', ...
            this.node.get('id'), cn(p,1) );
        ifo{end+1} = '         </connection>';
        %ifo{end+1} = sprintf('   input %d connected to output %d of [%d]', ...
        %    cn(p,1), cn(p,3), cn(p,2) );
    end
    ifo{end+1} = '      </input>';
    % end
    % param�tres
    params = get_param;
    ifo{end+1} = '      <parameters>';
    if ~isempty( params)
        params  = params.get();
        p_info = {};
        for p = 1:2:length(params)-1
            cn = params{p};
            tp = tokenize(cn,':');
            if length(tp)==2
                tp = tp{2};
            else
                tp = 'generic';
            end
            p_info{end+1} = sprintf('         <param label="%s" type="%s">%s</param>', ...
                cn, tp, convs('str',params{p+1}));
        end
        ifo{end+1} = p_info;
    end
    ifo{end+1} = '      </parameters>';
    ifo{end+1} = '   </node>';
    ifo = cellflat( ifo);
end
%>*
%<* Get outputs
function o = outputs( i)
    o = this.node.get('outputs');
    if nargin > 0
        if i>length(o)
            error('st_node:outputs', 'node <%s> has not %d outputs', this.node.get('title'), i);
        else
            o = o{i};
        end
    end
end
%>
%<* Get end of prod flag
function b = eop( ns)
    ns = this.node.get('state');
    b = ns.end_of_prod();
end
%>*
end