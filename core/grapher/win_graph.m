function st_ans = win_graph(mode, varargin )
% WIN_GRAPH - affichage d'un graphe
%
% win_graph load
% win_graph load_xml
% win_graph create
% win_graph clear
%
%
% global parameters used by win_graph:
% - display-commands  : true*/false
% - plot-panel: true/false*
% - panel-positions: [commands_width, grapher_width]
% - panel-layout: cf subgrid -> panels-layout-handles
% - toolbar : to have the toolbar available
%
% See also st_grapher st_node subgrid
global st_ans
menu_items = [ 'inspect|plot|params|init|next|' ...
    'add|move|connect|learn|edit|unconnect|delete|root|target|' ...
    'load|save|global params|code|export|graph|upgrade|market|' ...
    '+enrichment|-enrichment|refresh|title|rename|clean_save|to_xml' ];
% lorsqu'on ajoute un �l�ment au menu, il faut aussi ajouter l'action
% correspondante dans la fonction ACTION

% je ne suis PAS DU TOUT CONTENT de cette ligne:
persistent gr action_mode temp_info
% mes action_modes sont:
% - inspect: comportement normal
% - add/move/connect/delete/root/target/plot

switch lower( mode)
    case 'graph'
        st_ans = gr;
    case 'load'
        win_graph('show', st_grapher);
    case 'load_xml'
        if isempty(varargin)
            [filename pathname] = uigetfile('*.xml', 'Select XML file:');
            fname = [pathname filename];
        else
            fname = varargin{1};
        end
        gr = gr_import_xml('build-file', fname);
        win_graph('show', gr);
        st_ans = gr;
    case 'create'
        gr = [];
        win_graph('select-menu', 'add');
    case 'create-gi'
        gi_create();
    case 'clear'
        gr = [];
    case 'get-grapher'
        %<* To have the grapher back
        st_ans = gr;
        %>*
    case { 'show', 'init' }
        %<* Affichage
        %< Couleurs
        g_color = [238 238 208]/256; % couleur de fond
        b_color = [238 208 168]/256; % couleur des boites
        t_color = [238 188 111] /256; % couleur des traits
        f_color = [200 138 18]/256;  % couleur des fleches
        %>
        action_mode = 'inspect';
        %< Cr�ation du panel principal
        if nargin>1
            gr = varargin{1};
            if ischar( gr)
                gr = load( gr);
                gr = gr.gr;
            end
        end
        if isempty( gr.get('graph'))
            win_graph( 'select-menu', 'load');
        end
        hp = findobj('tag', 'grapher');
        % hp = gr.get_global('panel');
        if isempty(hp) 
            % figure
            hf = findobj( 'tag', 'grapher');
            if isempty( hf)
                hf = figure('name', sprintf('Structured graphs: <%s>', gr.get_global('title')), 'NumberTitle', 'off', ...
                    'toolbar', 'none', 'menubar', 'none', 'tag', 'grapher' );
                pos = get(hf, 'position');
                set(hf,'position', [pos(1) 200 600 640]);
            else
                figure( hf);
            end
            %< Plot panel
            try
                panel_pos = gr.get_global('panel-positions');
            catch er
                lasterr('');
                panel_pos = [.22 , .6];
            end
            %>
            %< Menubar
            try
                set(gcf, 'toolbar', gr.get_global('toolbar'));
            catch er
                lasterr('');
            end
            %>
            
            %< Commands panel
            try
                commands = gr.get_global('display-commands');
                if commands
                    commands_rwidth = panel_pos(1);
                else
                    commands_rwidth = 0;
                end                
            catch er
                lasterr('');
                commands = true;
                commands_rwidth = panel_pos(1);
            end
            %>
            %< Plot panel
            try
                plot_panel = gr.get_global('plot-panel');
            catch er
                lasterr('');
                plot_panel = false;
            end
            if ~plot_panel
                panel_pos(2) = 1;
            end
            %>
            %< Panel layout
            try
                panel_layout = gr.get_global('panel-layout');
            catch er
                lasterr('');
                panel_layout = [];
            end
            %>
            
            % grapher panel
            hp = uipanel('backgroundcolor', g_color, 'borderwidth', 0, ...
                'position', [0,0, panel_pos(2), 1]);
            if plot_panel
                hplot = uipanel('backgroundcolor', [238 238 208]/256, 'borderwidth', 1, ...
                    'position', [panel_pos(2), 0, 1-panel_pos(2), 1]);
                if ~isempty( panel_layout)
                    hpy = subgrid( 'panels-layout', panel_layout, 'parent', hplot);
                    gr.set_global('panels-layout-handles', hpy);
                end
            end
            % menu
            if commands
                hlb = uicontrol(hp, 'style', 'listbox', 'string', menu_items, ...
                    'unit', 'normalized', ...
                    'position', [.01 .01 (.01+commands_rwidth) .98], ...
                    'callback', 'win_graph(''select-menu'');', ...
                    'fontsize', 14, 'backgroundcolor', g_color, ...
                    'foregroundcolor', f_color);
            end
            % axes
            hax = axes('parent', hp, 'unit', 'normalized', 'box', 'on', ...
                'position', [(.03+commands_rwidth) .01 (.98-(.03+commands_rwidth)) .98], ...
                'color', g_color, ...
                'xtick', [], 'ytick', [], 'xticklabel', [], 'yticklabel', [], 'tag', 'grapher_panel');
            % stockage
            gr.set_global( 'panel', hp);
        end
        fo = findobj('tag', 'grapher');
        hf = figure( fo(1));
        set( hf, 'name', sprintf('Structured graphs: <%s>', gr.get_global('title')));
        set( findobj(gcf, 'style', 'listbox'), 'value', 1);
        %>
        axes( findobj( hf, 'tag', 'grapher_panel'));
        graph = gr.get('graph');
        lim_p = [Inf, -Inf, Inf, -Inf];
        max_xpos = 30;
        for n=1:length(graph)
            if ~isempty( graph{n})
                position = graph{n}.get('position');
                if isempty( position)
                    % par d�faut, je positionne les boites selon une ligne
                    center   = [max_xpos 0];
                    % j'utilise une RECOPIE de cette ligne en mode de
                    % mouvement de node
                    position = [center(1) - 10, center(1) + 10, center(2)-5, center(2)+5];
                    graph{n}.set('position', position);
                end
                center = [mean(position(1:2)), mean(position(3:4))];
                max_xpos = max([max_xpos, center(1)+30]);
                fill( [position(1), position(2), position(2), position(1)], ...
                    [position(3), position(3), position(4), position(4)], 'r', ...
                    'facecolor', b_color, 'edgecolor', t_color, ...
                    'linewidth', 2);
                hold on
                txt = graph{n}.get('title');
                txt = sprintf('%s', strrep(txt, '__', '\n'));
                ht  = text( center(1), center(2), txt, ...
                    'HorizontalAlignment', 'center', ...
                    'VerticalAlignment', 'middle', 'fontsize', 8, ...
                    'ButtonDownFcn', 'win_graph(''select-box'');', ...
                    'tag', graph{n}.get('title'));
                t = graph{n}.get('cpu');
                if ~isempty( t)
                    t = sprintf(':%5.3f', t);
                end
                text( center(1), position(4)+1, sprintf('[%d%s]', n, t), ...
                    'HorizontalAlignment', 'center', ...
                    'VerticalAlignment','bottom', 'fontsize', 8, 'color', f_color);
                % pour le root
                if gr.get_global('root')==n
                    set(ht, 'FontWeight', 'bold', 'color', [0 128 50]/256);
                end
                % pour le target
                if gr.get_global('target')==n
                    set(ht, 'FontWeight', 'bold', 'color', [128 0 50]/256);
                end
                lim_p = [min(lim_p(1),position(1)), max(lim_p(2),position(2)),...
                    min(lim_p(3),position(3)), max(lim_p(4),position(4))];
            end
        end
        % les fleches
        for n=1:length(graph)
            if ~isempty( graph{n})
                pos1     = graph{n}.get('position');
                connect1 = graph{n}.get('out_connections');
                if ~isempty( connect1)
                    nc1      = size(connect1,1);
                    mc1      = max( connect1(:,1));
                    for p=1:nc1
                        dep      = 1-(connect1(p,1)-.5)/mc1;
                        pos2     = graph{connect1(p,2)}.get('position');
                        connect2 = graph{connect1(p,2)}.get('in_connections');
                        mc2      = max( connect2(:,1));
                        ar       = 1-(connect1(p,3)-.5)/mc2;
                        plot( [pos1(2) pos2(1)], ...
                            [pos1(3)+dep*diff(pos1(3:4)), pos2(3)+ar*diff(pos2(3:4))], ...
                            'linewidth', 2, 'color', f_color, ...
                            'ButtonDownFcn', 'win_graph(''select-arrow'');', ...
                            'tag', sprintf('%s:%d', graph{n}.get('title'), connect1(p,1)));
                        plot( pos2(1) , pos2(3)+ar*diff(pos2(3:4)), 'd', ...
                            'linewidth', 2, 'color', f_color);
                    end
                end
            end
        end

        %< Les enrichments
        rnames = gr.get('enrichments', 'names');
        if ~isempty( rnames)
            for r=1:length(rnames)
                enr  = gr.get_enrichment( rnames{r});
                if enr.get('display')
                    position = enr.get('position');
                    if isempty( position)
                        % par d�faut, je positionne les boites selon une ligne
                        center   = [max_xpos 0];
                        % j'utilise une RECOPIE de cette ligne en mode de
                        % mouvement de node
                        position = [center(1) - 10, center(1) + 10, center(2)-5, center(2)+5];
                        enr.set('position', position);
                    end
                    center = [mean(position(1:2)), mean(position(3:4))];
                    max_xpos = max([max_xpos, center(1)+30]);
                    col = enr.get('color');
                    fill( [position(1), position(2), position(2), position(1)], ...
                        [position(3), position(3), position(4), position(4)], 'r', ...
                        'facecolor', col, 'edgecolor', col, ...
                        'linewidth', 2);
                    hold on
                    txt = rnames{r};
                    txt = sprintf('%s', strrep(txt, '__', '\n'));
                    ht  = text( center(1), center(2), txt, ...
                        'HorizontalAlignment', 'center', ...
                        'VerticalAlignment', 'middle', 'fontsize', 8, ...
                        'ButtonDownFcn', 'win_graph(''select-enrichment'');', ...
                        'tag', rnames{r}, 'color', enr.get('txtcolor') );
                    lim_p = [min(lim_p(1),position(1)), max(lim_p(2),position(2)),...
                        min(lim_p(3),position(3)), max(lim_p(4),position(4))];
                end
            end
        end
        %>
        %< Positionnement des axes
        % positionnement par rapport aux axes
        set(gca, 'unit', 'normalized', 'box', 'on', ...
            'color', g_color, ...
            'xtick', [], 'ytick', [], 'xticklabel', [], 'yticklabel', []);
        lim_p(1) = lim_p(1)-.1*diff(lim_p([1 2]));
        lim_p(2) = lim_p(2)+.1*diff(lim_p([1 2]));
        lim_p(3) = lim_p(3)-.1*diff(lim_p([3 4]));
        lim_p(4) = lim_p(4)+.1*diff(lim_p([3 4]));
        lim_c = [mean(lim_p(1:2)), mean(lim_p(3:4))];
        lim_r = max([diff(lim_p(1:2)), diff(lim_p(3:4))]/2);
        lim_p = [lim_c(1) - lim_r, lim_c(1)+lim_r, ...
            lim_c(2) - lim_r, lim_c(2)+lim_r];
        %lim_p = [min(lim_p([1 3])) max(lim_p([2 4])), min(lim_p([1 3])) max(lim_p([2 4]))];
        axis( lim_p) ;
        hold off
        % tag de l'axe
        set(gca, 'tag', 'grapher_panel');
        
        % je recopie le noeud dans le graphe!

        %>
        %>*
    case 'select-menu'
        %<* Selection on the left menu
        if nargin>1
            st_log('direct call\n');
            mode = varargin{1};
        else
            % mouse selection
            opts = cellstr(get(gco, 'string'));
            v    = get(gco, 'value');
            st_log('[%s:%d] pressed\n', opts{v}, v);
            mode = opts{v};
        end
        switch lower(mode)
            case 'inspect'
                %< Force le retour en mode inspection
                st_log('old mode was <%s>\n', action_mode);
                action_mode = mode;
                %>
            case 'refresh'
                %< Refresh l'IHM
                win_graph('show');
                %>
            case 'clean_save'
                %< Clear memory
                gr = gr.upgrade();
                gr.clear_outputs();
                gr.init();
                win_graph( 'select-menu', 'save');
                %>
            case 'upgrade'
                %< Update graph
                fd = fieldnames( gr);
                gr = gr.upgrade();
                fd = setdiff( fieldnames( gr), fd);
                if isempty( fd)
                    st_log('Graph updated, but no new method available...\n');
                else
                    st_log('Graph updated, new methods :\n');
                    st_log('- %s\n', fd{:});
                end
                win_graph('show');
                %>
            case '-enrichment'
                %< Retirer un enrichment
                names = gr.get('enrichments', 'names');
                [s,v] = listdlg('PromptString','Select an enrichment to delete:',...
                    'SelectionMode','single',...
                    'ListString', names);
                if v==1
                    gr.del_enrichment( names{s});
                end
                win_graph('show');
                %>
            case '+enrichment'
                %< Ajout d'un enrichment
                opt = options({'title', 'Next', 'function', '@(g)g.next()', ...
                    'display', 1, 'color', '[.67 0 .12]', 'txtcolor', '[1 1 1]'});
                h = opt.visual();
                if h
                    gr.set_enrichment( opt.get('title'), 'fun', eval(opt.get('function')), ...
                        'display', opt.get('display'), 'color', eval(opt.get('color')), ...
                        'txtcolor', eval(opt.get('txtcolor')));
                end
                win_graph('show');
                %>
            case 'add'
                %< Ajout d'un node
                if isempty(gr)
                    mid = 1;
                    gr = st_grapher;
                    def_name = sprintf('Node-%d', mid);
                else
                    mid = gr.max_id()+1;
                    def_name = sprintf('Node-%d', mid);
                end
                ans_ = add_box_dlg('ListString', makedoc('find-boxes'), 'node-name', def_name);
                if ~isempty(ans_)
                    id = gr.add_node( 'st_function', ans_{2}, ...
                        'title', ans_{1});
                    gr.init_node( id);
                end
                win_graph('show');
                %>
            case 'move'
                %< Move a node
                action_mode = mode;
                %>
            case 'rename'
                %< Rename a node
                action_mode = mode;
                %>
            case 'title'
                %< Modify title of the graph
                prompt = {'New name for the graph'};
                dlg_title = 'Rename';
                num_lines = 1;
                def = {gr.get_global('title')};
                answer = inputdlg(prompt,dlg_title,num_lines,def);
                if isempty(answer)
                    st_log('nothing done...\n');
                else
                    gr.set_global('title', answer{1});
                    win_graph('show');
                end
                %>
            case 'connect'
                action_mode = mode;
            case 'unconnect'
                action_mode = mode;
            case 'delete'
                action_mode = mode;
                st_log('select node to be deleted...\n');
            case 'root'
                action_mode = mode;
            case 'target'
                action_mode = mode;
            case 'params'
                action_mode = mode;
            case 'init'
                gr.init();
                win_graph('show', gr);
            case 'next'
                gr.next()
                win_graph('show');
            case 'plot'
                action_mode = mode;
            case 'edit'
                action_mode = mode;
            case 'learn'
                action_mode = mode;
            case 'load'
                fname = gr.get_global('filename');
                [file,path] = uigetfile(fname,'Load file name');
                if ~ischar( file)
                    st_log('nothing done\n');
                else
                    fname = fullfile( path, file);
                    g  = load(fname);
                    if isfield(g , 'gr')
                        % sauvegarde de win_graph
                        gr = g.gr;
                    else
                        % sauvegarde de st_grapher
                        gr = st_grapher('clone', g.this);
                    end
                    gr.set_global('filename', fname);
                end
                close(gcf);
                win_graph('show', gr);
            case 'save'
                fname = gr.get_global('filename');
                [file,path] = uiputfile( fname ,'Save file name');
                if ~ischar( file)
                    st_log('nothing done\n');
                else
                    fname = fullfile( path, file);
                    if ~strcmpi(fname(end-3:end), '.mat')
                        fname = [fname '.mat'];
                    end
                    gr.set_global('filename', fname);
                    st_log('Saving MAT file...\n');
                    try
                        save( fname, 'gr');
                    catch er
                        warning('win_graph:save', 'An error occured during the save process.\n %s \n Please call Charles or Silviu if you see this. \n', er);
                    end
                    
                    st_log('Saving XML back-up...\n');
                    fname_xml = [fname(1:end-4) '.xml'];
                    fid = fopen(fname_xml, 'w');
                    info = gr.info();
                    fprintf(fid, '%s\n', info{:});
                    fclose(fid);
                end
                win_graph('show', gr);
            case 'to_xml'
                %< Save in XML format
                fname = gr.get_global('filename');
                fname(end-3:end) = '.xml';
                [filename, pathname] = uiputfile(fname, 'XML file');   
                if ~ischar( filename)
                    st_log('nothing done\n');
                else
                    fname = [pathname filename];
                    gr.to_xml(fname);
                end
                %>
            case 'global params'
                gp = gr.get('params');
                gp.visual();
                action_mode = 'inspect';
                win_graph('show');
            case 'code'
                %<* Generation of a MATLAB code
                % that will generate the graph
                fname = gr.get_global('filename');
                if ~isempty( fname)
                    [p,f,e] = fileparts( fname);
                    fname = fullfile(p,[f '.m']);
                end
                [file,path] = uiputfile( fname ,'Save file name');
                if ~ischar( file)
                    st_log('nothing done\n');
                else
                    fname = fullfile( path, file);
                    [d,f,ext] = fileparts( fname);
                    if ~isempty( ext)
                        ext = ext(2:end);
                    end
                    if ~ismember( ext, { 'm', 'xml'})
                        st_log('win_graph: my point of view is that you want to code an <m> file\n');
                        ext = 'm';
                        fname = [ fname '.m'];
                    end
                    gr.code( fname, ext);
                    edit( fname);
                end
                win_graph('show', gr);
                %>*
            case 'export'
                %<* Export to a MATLAB file
                % that will execute the graph
                fname = gr.get_global('filename');
                if ~isempty( fname)
                    [p,f,e] = fileparts( fname);
                    fname = fullfile(p,[f '.m']);
                end
                [file,path] = uiputfile( fname ,'Save file name');
                if ~ischar( file)
                    st_log('nothing done\n');
                else
                    fname = fullfile( path, file);
                    if ~strcmpi(fname(end-1:end), '.m')
                        fname = [fname '.m'];
                    end
                    gr.export( fname);
                    edit( fname);
                end
                win_graph('show', gr);
                %>*
            case 'market'
                %<* Market simulations
                fname = gr.get_global('filename');
                if ~isempty( fname)
                    [p,f,e] = fileparts( fname);
                    fname = fullfile(p,[f '.m']);
                end
                [file,path] = uiputfile( fname ,'Save file name');
                if ~ischar( file)
                    st_log('nothing done\n');
                else
                    fname = fullfile( path, file);
                    if ~strcmpi(fname(end-1:end), '.m')
                        fname = [fname '.m'];
                    end
                    gr.market( fname);
                    edit( fname);
                end
                win_graph('show', gr);
                %>*
            case 'graph'
                st_ans = gr
                win_graph('show', gr);
            otherwise
                error('win_graph:ACTION', 'action mode <%s> unknown', mode);
        end
        %>*
    case 'select-arrow'
        %<* Selection of an arrow
        a_name = get(gco, 'tag');
        st_log('Arrow[%s] pressed\n', a_name);
        tk = tokenize( a_name, ':');
        n_name = tk{1};
        o_id   = str2double( tk{2});
        st_ans = gr.get_outputs( n_name);
        if ~isempty(st_ans)
            st_ans = st_ans{o_id}
            st_plot( st_ans);
        else
            st_log('No output available...\n');
        end
        %>*
    case 'select-enrichment'
        %<* Action sur les enrichments
        % Pour l'instant je n'en reconnais que trois:
        %   move    : eponym ;
        %   params  : passe dans |st_ans| ;
        %   inspect : run!
        b_name   = get(gco, 'tag');
        st_log('Enrichment[%s] pressed\n', b_name);
        switch lower(action_mode)
            case 'params'
                %< Sortie
                st_ans = gr.get_enrichment( b_name);
                st_ans.visual();
%                 action_mode = 'inspect';
                win_graph('show');
                %>
            case 'inspect'
                %< Run
                fr = gr.run_enrichment( b_name);
                action_mode = 'inspect';
                win_graph('show');
                %>
            case 'move'
                %< Move an enrichment
                st_log('Select the new place for enrichment <%s>\n', b_name);
                [x,y] = ginput(1);
                center = [x,y];
                position = [center(1) - 10, center(1) + 10, center(2)-5, center(2)+5];
                enr = gr.get_enrichment( b_name);
                enr.set( 'position', position);
                action_mode = 'inspect';
                win_graph('show');
                %>
            case 'edit'
                try
                    enr = gr.get_enrichment( b_name);
                    edit(func2str( enr.get('fun')));
                    action_mode = 'inspect';
                    win_graph('show');
                catch er
                    st_log(['It seems complex to find the .m file for this enrichment.\n'...
                        'If you want this feature to work change the function handle of your enrichment to : "@enrichmentfunc"']);
                end
            otherwise
                st_log('This mode is not available for enrichments,\nplease click on a node or on an arrow!\n');

        end
        %>*
    case 'select-box'
        %<* Selection of an arrow
        b_name   = get(gco, 'tag');
        st_log('Box[%s] pressed\n', b_name);
        n_id = gr.node_id( b_name);
        switch lower(action_mode)
            case 'inspect'
                %< inspection: renvoie du node dans st_ans
                st_ans   = gr.get_node( n_id)
                str_info = st_ans.info();
                dname = makedoc('name-userdoc', st_ans.get('st_function'));
                web(['file://' dname]);
                st_log('%s\n', str_info{:});
                %>
            case 'connect'
                %< Connection de deux noeuds
                if isempty( temp_info)
                    % premier noeud
                    oc = gr.get_connections( n_id, 'out');
                    if isempty(oc)
                        oc = 1;
                    else
                        oc = max(oc(:,1))+1;
                    end
                    prompt = {sprintf('Output of node %s to connect', b_name)};
                    dlg_title = 'Connection wizzard';
                    num_lines = 1;
                    def = {num2str( oc)};
                    ans = inputdlg(prompt,dlg_title,num_lines,def);
                    temp_info = [n_id, str2double(ans{1})];
                else
                    % second noeud
                    oc = gr.get_connections( n_id, 'in');
                    if isempty(oc)
                        oc = 1;
                    else
                        oc = max(oc(:,1))+1;
                    end
                    prompt = {sprintf('Inuput of node %s to connect', b_name)};
                    dlg_title = 'Connection wizzard';
                    num_lines = 1;
                    def = {num2str( oc)};
                    ans = inputdlg(prompt,dlg_title,num_lines,def);
                    gr.connect(temp_info(1), temp_info(2), n_id, str2double( ans{1}));
                    % remise � vide
                    temp_info = [];
                    win_graph('show');
                end
                %>
            case 'unconnect'
                %<* Unconnect
                if isempty( temp_info)
                    % premier noeud
                    prompt = {sprintf('Output of node %s to unconnect', b_name)};
                    dlg_title = 'Connection wizzard';
                    num_lines = 1;
                    def = {'1'};
                    ans = inputdlg(prompt,dlg_title,num_lines,def);
                    temp_info = [n_id, str2double(ans{1})];
                else
                    % second noeud
                    prompt = {sprintf('Inuput of node %s to unconnect', b_name)};
                    dlg_title = 'Connection wizzard';
                    num_lines = 1;
                    def = {'1'};
                    ans = inputdlg(prompt,dlg_title,num_lines,def);
                    try
                        gr.unconnect(temp_info(1), temp_info(2), n_id, str2double( ans{1}));
                    catch
                        st_log('nothing to seems to be connected that way...\n');
                        lasterr('');
                    end
                    % remise � vide
                    temp_info = [];
                    win_graph('show');
                end
                %>*
            case 'move'
                %< Move a box
                st_log('Select the new place for box <%s:%d>\n', b_name, n_id);
                [x,y] = ginput(1);
                center = [x,y];
                position = [center(1) - 10, center(1) + 10, center(2)-5, center(2)+5];
                gr.set_position( b_name, position);
                action_mode = 'inspect';
                win_graph('show');
                %>
            case 'rename'
                %<* Rename the node
                n=gr.get_node( n_id);
                prompt = {'Enter New name:'};
                dlg_title = sprintf('Rename <%s>', b_name);
                num_lines = 1;
                def = { b_name };
                answer = inputdlg(prompt,dlg_title,num_lines,def);
                if ~isempty( answer)
                    n.set('title', answer{1});
                end
                action_mode = 'inspect';
                win_graph('show');
                %>*
            case 'delete'
                %< Delete node
                gr.delete_node( n_id);
                action_mode = 'inspect';
                win_graph('show');
                %>
            case 'root'
                %< d�finition de la racine
                gr.set_global('root', n_id);
                action_mode = 'inspect';
                win_graph('show');
                %>
            case 'target'
                %< d�finition de la cible
                gr.set_global('target', n_id);
                action_mode = 'inspect';
                win_graph('show');
                %>
            case 'edit'
                %< edition du fichier
                edit( gr.get_node( n_id, 'st_function'));
                action_mode = 'inspect';
                win_graph('show');
                %>
            case 'learn'
                %< Apprentissage du module
                gr.learn( b_name);
                action_mode = 'inspect';
                win_graph('show');
                %>
            case 'params'
                %< Affichage des param�tres
                gr.visual_param( n_id);
                action_mode = 'inspect';
                win_graph('show');
                %>
            case 'plot'
                %< Affichage
                gr.plot( n_id);
                action_mode = 'inspect';
                win_graph('show');
                %>           
            otherwise
                st_log('You should not touch any node in this mode\n');
        end
        if strcmpi(action_mode, 'inspect')
            h    = findobj( gcf, 'style', 'listbox');
            set(h, 'value', 1);
        end
        %>*
    otherwise
        error('win_graph:mode', 'mode <%s> unknwon', mode);
end