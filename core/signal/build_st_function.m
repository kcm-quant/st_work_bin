function f = build_st_function( mode, varargin)
% BUILD_ST_FUNCTION - g�n�rateur de st_functions
%
% use:
%  f             = build_st_function( 'init', @(v)mean(v), 'date-fun', @(t)t, 'colnames-fun', @(c)c, 'title-fun', @(i)i);
%  str           = build_st_function( 'build-box', @(v)mean(v), 'date-fun', @(t)t, 'colnames-fun', @(c)c, 'title-fun', @(i)i);
%  f_ratio       = build_st_function( '&ratio', @sum, 1/2);
%  f_last_ratio  = build_st_function( '&last-ratio', @mean);
%  f_time_ratio  = build_st_function( '&time-ratio', @mean, 1/5);
%  f_time_ratio2 = build_st_function( '&time-ratio-2', @sum , 1/5, @(x)sum(x)/5);
%  f_proportion  = build_st_function( '&proportion', 1/20);
%  f_point_ratio = build_st_function( '&point-ratio', @sum, @mean, [1 2]);
%  f_vol_gk      = build_st_function( '&vol-gk');
%
% example:
%  f_sigma  = build_st_function( '&volatility');
%
% See also st_function template_file time_ratio point_ratio

mode = lower(mode);

switch mode    
    case 'init'
        opt = options({'date-fun', @(t)t, 'colnames-fun', @(c)c, 'title-fun', @(i)i, 'title', 'unknown'}, varargin(2:end));
        cnames = opt.get('colnames-fun');
        if ischar(cnames)
            cnames = tokenize( cnames, ';');
            opt.set('colnames-fun', @(c) cnames);
        end
        f = st_function( varargin{1}, opt.get('date-fun'), opt.get('colnames-fun'), opt.get('title-fun'), opt.get('title'));
    case { 'bb', 'build-box', 'build-node' }
        %<* Build a node
        opt   = options({'date-fun', @(t)t, 'colnames-fun', @(c)c, 'title-fun', @(i)i, 'fname', '', 'title', ''}, varargin(2:end));
        fdir  = fullfile( getenv('st_work'), 'libs', 'boxes', 'st_functions');
        fname = opt.get('fname');
        if isempty( fname)
            base_n = [ 'st_function_' getenv('computername') '_'];
            all_f  = dir( fullfile( fdir, [base_n '*.m']));
            all_f  = {all_f.name};
            last_n = regexprep( all_f, [base_n '([^\.]+)\.m'], '$1');
            if isempty( last_n)
                fname = sprintf('%s1', base_n);
            else
                last_n = cellfun(@(s)str2num(s), last_n);
                fname = sprintf('%s%d', base_n, max(last_n)+1);
            end
            opt.set('fname', fname);
        end
        
        creation = STRFUN( varargin{1});
        lst = opt.get();
        for l=1:2:length(lst)-1
            v = lst{l+1};
            if ~ischar(v)
                v = STRFUN( v);
            else
                v = [ '''' v ''''];
            end
            creation = sprintf('%s, ''%s'', %s', creation, lst{l}, v);
        end
        local_opts = options({'fname', fname, 'varargin', creation});            
        
        fod = fopen( fullfile( fdir, [fname '.m']), 'w');
        f = template_file( fullfile(getenv('st_work'), 'libs', 'boxes', 'st_functions', 'st_function.proto'), local_opts, fod);
        fclose( fod);
        %>*
        
    case '&volatility'
        f = st_function( @(s)diff(s)./s(2:end), @(t)t(end), @(c){'volatility'}, @(i)i, mode(2:end));
    case '&vol-gk'
        f = st_function( @(s, t) 2500/3 * sqrt(((max(s)-min(s)).^2/2-(2*log(2)-1)*(s(1)-s(end)).^2) / (mean(s).^2 * (t(end) - t(1)))) , @(t)t(end), @(c){'volatility GK'}, @(i)i, mode(2:end)); % 2500/3 car volatilit� 10 minutes exprim�e en points de base
    case '&vol-range'
        f = st_function( @(s, t) 2500/3 * log(max(s)/min(s)) / (t(end) - t(1)) , @(t)t(end), @(c){'volatility Range'}, @(i)i, mode(2:end)); % 2500/3 car volatilit� 10 minutes exprim�e en points de base
    case '&last-ratio'
        name = strcat(mode(2:end), strcatdelim(varargin(:),':'));
        f = st_function(@(s)s(end)/feval(varargin{1},s), @(t)t(end), @(c){'Last ratio'}, @(i)i, name);
    case '&time-ratio'
        name = strcat(mode(2:end), strcatdelim(varargin(:),':'));
        f = st_function(time_ratio(varargin{1}, varargin{2}), @(t)t(end), @(c){'Time ratio'}, @(i)i, name);
    case '&time-ratio-2'
        name = strcat(mode(2:end), strcatdelim(varargin(:),':'));
        f = st_function(time_ratio(varargin{:}), @(t)t(end), @(c){'Time ratio'}, @(i)i, name);        
    case '&point-ratio'
        name = strcat(mode(2:end), strcatdelim(varargin(:),':'));
        f = st_function(point_ratio(varargin{:}), @(t)t(end), @(c)sprintf('Ratio %s/%s', c{1}, c{2}), @(i)i, name);
    case '&ratio'
        fct1 = varargin{1};
        r    = varargin{2};
        if nargin == 3
            fct2 = fct1;
        elseif nargin == 4;
            fct2 = varargin{3};
        end
        name = strcat(mode(2:end), strcatdelim(varargin(:),':'));
        f    = st_function(@(x)feval(fct1,x(1+floor(end*(1-r)):end,:))./feval(fct2, x), ...
            @(t)t(end), ...
            @(c)sprintf('Ratio%s ', sprintf('-%s', c{:})), ...
            @(i)i,...
            name);
    case '&proportion'
        name = strcat(mode(2:end), strcatdelim(varargin(:),':'));
        f = st_function(ind_proportion(varargin{1}), @(t)t(end), @(c){'Proportion '}, @(i)i, name);
        
    case '&liquidity_card'
        f = struct('name', 'liquidity_card', 'apply', @f_liquidity_card);
        
    otherwise
        error('build_st_function:mode', 'unknown mode <%s>', mode);
end


%%** Internals

%<* strfun
function str = STRFUN( f)
str = func2str( f);
if str(1)~='@'
    str = ['@' str];
end