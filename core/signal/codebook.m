function [z, t] = codebook( mode, varargin)
% CODEBOOK - Handling of codebooks
%
% Examples:
% cdk1 = codebook('new',{'rond','carre','triangle'},'geometrie',[1 2 3]);
% vals1 = codebook('extract','ids2names',cdk1,'geometrie',[ 1 2 10 1]);
% vals1b = codebook('extract','names2ids',cdk1,'geometrie',{'rond','carre','rond','a'});
% [b1 cdk1] = codebook('iscodebook',cdk1);
% bequ = codebook('isequal',cdk1,cdk1);
% cdk1 = codebook('merge',cdk1,cdk1);
% cdk1b = codebook('new',{'rond','carre','triangle','trapeze','cylindre'},'geometrie',[1 110 3 4 5]);
% cdk1c = codebook('new',{'trapeze','cylindre'},'geometrie',[11 21]);
% cdk1m1 = codebook('merge',cdk1,cdk1b);
% cdk1m2 = codebook('merge',cdk1,cdk1c);
% cdk2 = codebook('new',{'rond','carre','triangle','paralelogramme'},'geometrie2',[1 2 3 4]);
% cdk3 = codebook('new',{'rond','carre','triangle'},'geometrie',[1 4 3]);
% cdk4 = codebook('new',{'trapeze','cylindre'},'geometrie4');
% cdk5 = codebook('stack',cdk1,cdk2);
% cdk6 = codebook('stack',cdk3,cdk4);
% cdk7 = codebook('stack',cdk1,cdk1);
% [cdk8 cell_merged_colnames]=codebook('stack',cdk5,cdk6);
%
%
% See also:
%
%   author   : 'njoseph@cheuvreux.com'
%   reviewer : ''
%   version  : '1.0'
%   date     :  11/03/2011



t = [];

switch lower(mode)
    case 'build'
        %<* Add a codebook
        vals = cellfun(@strtrim, varargin{1},'uni', false);
        [uvals, idx, jdx] = unique(vals);
        z = jdx;
        t = struct('colname', '', 'colnum', (1:length(uvals))', 'book', {uvals});
        %>*
    case 'get'
        %< Get codebook
        idx = strmatch( lower(varargin{2}), lower({varargin{1}.colname}), 'exact');
        if isempty(idx)
            z = [];
        else
            z = varargin{1}(idx).book;
        end
        t = idx;
        %>
    case 'clean'
        %< Clean a codebook
        % separator = /
        data    = varargin{1};
        colname = varargin{2};
        [vals, col_id] = st_data('col', data, colname);
        vals_day  = floor( vals/100);
        vals_code = mod(vals, 100);
        [cbk, book_id]  = codebook('get', data.codebook, colname);
        last_char = cellfun(@(c)c(end), cbk, 'uni', true);
        cbk(last_char=='/') = cellfun(@(c)[c '_'], cbk(last_char=='/'), 'uni', false);
        all_days = cellfun(@(s)str2num(tokenize(s,'/',1)), cbk, 'uni', true);
        [days, tmp, udays] = unique( all_days);
        keys = cellfun(@(s)tokenize(s,'/',2), cbk, 'uni', false);
        
        str_vals = cell( size(vals));
        for d=1:length(days)
            these_keys = keys(udays==d);
            idx_this_day = vals_day==days(d);
            str_vals(idx_this_day) = these_keys(vals_code(idx_this_day));
        end
        
        [final_vals, cdk] = codebook('build', str_vals);
        cdk.colname = colname;
        data.codebook(book_id) = cdk;
        
        data.value(:,col_id) = final_vals;
        z = data;
        
        %>
        
    case 'replace'
        %< replace codebook
        data = varargin{1};
        colname = varargin{2};
        vals = st_data('col', data, colname);
        book = codebook('get', data.codebook, colname);
        z = book(vals);
        %>
        
        
        
    case 'print'
        %< Print codebook
        t = [];
        cb = varargin{1}.codebook;
        for c=1:length(cb)
            st_log('Column <%s>:\n', cb(c).colname);
            for e=1:length(cb(c).book)
                st_log('%3d->%s\n', e, cb(c).book{e});
            end
        end
        %>
        
        
        
        
        %-------------------------------------------------------------------------------------------------------
        %---------- NEW version
        %-------------------------------------------------------------------------------------------------------
    
    
        
    
    case 'iscodebook'
        
        %< test if the data is a valid codebook
        % is also valid, a struct of codebooks
        cdk = varargin{1};
        z=false;
        t=cdk;
        if isstruct(cdk)
            nb_cdk=length(cdk);
            if nb_cdk>=1
                cdk_colname={};
                for i=1:nb_cdk
                    cdk_tmp=cdk(i);
                    if isfield(cdk_tmp,'colname') && ...
                            isfield(cdk_tmp,'colnum') && ...
                            isfield(cdk_tmp,'book') && ...
                            ~isempty(cdk_tmp.colname) && ...
                            iscellstr(cdk_tmp.book) && ...
                            isnumeric(cdk_tmp.colnum) && ...
                            length(unique(cdk_tmp.colnum))==length(cdk_tmp.colnum) && ...
                            length(unique(cdk_tmp.book))==length(cdk_tmp.book) && ...
                            length(cdk_tmp.colnum)==length(cdk_tmp.book)
                        z=true;
                        cdk_colname=cat(1,cdk_colname,cdk_tmp.colname);
                    else
                        z=false;
                        return
                    end
                end
                
                if z && length(cdk_colname)~=length(unique(cdk_colname))
                    z=false;
                    same colname are not allowed !!
                end
            end
        end
        %>
        
    case 'isequal'
        
        z=true;
        t=[];
        %< test if two "simple" codebooks are equal
        if nargin~=3
            error('codebook:isequal,bad inputs')
        end
        [b1 data1]=codebook('iscodebook',varargin{1});
        [b2 data2]=codebook('iscodebook',varargin{2});
        if length(data1)>1 || length(data2)>1
            error('codebook:isequal ,only compare two "simple" codebooks')
        end
        
        % start test
        if ~(b1 && b2)
            z=false;
        else
            if ~strcmp(data1.colname,data2.colname) || ...
                    length(data1.colnum)~=length(data2.colnum)
                z=false;
            elseif ~isempty(data1.colnum) && ~isempty(data2.colnum) 
                % reshape data1
                [book1,idx1]=sort(data1.book);
                data1.book=book1;
                data1.colnum=data1.colnum(idx1);
                % reshape data2
                [book2,idx2]=sort(data2.book);
                data2.book=book2;
                data2.colnum=data2.colnum(idx2);
                % test
                if ~all(data1.colnum==data2.colnum) || ...
                        ~all(cellfun(@(c1,c2)(strcmp(c1,c2)),data1.book,data2.book))
                    z=false;
                end
            end
        end
        
        if z
            t=data1;
        end
            
 
    case 'new'
        
        z=[];
        t=false;
        
        %<* create a new codebook
        % varargin{1} : cell of strings
        % if varargin{2} : string = colnames
        % if varargin{3} : colnum corresponding to varargin
        if nargin==3
            [~,z]=codebook('build',varargin{1});
            z.colname=varargin{2};
        elseif nargin==4
            z = struct('colname',varargin{2}, 'colnum',varargin{3}, 'book',{varargin{1}});
        else
            error('codebook:new','inputs are not valid')
        end
        
        % size handling
        if size(z.colnum,2)>1
            z.colnum=transpose(z.colnum);
        end
        if size(z.book,1)>1
            z.book=transpose(z.book);
        end
        
        % // buiding test
        [b z]=codebook('iscodebook',z);
        if ~b
            error('codebook:new','inputs are not valid');
        else
            t=true;
        end
        %>*
        
        
    case 'extract'
        
        z=[];
        %<* extract from the codebook
        extract_mode=varargin{1};
        cdk=varargin{2};
        extract_colname=varargin{3};
        if nargin==5
            extract_value=varargin{4};
        else
            extract_value=[];
        end
        
        [n,k]=size(extract_value);
        
        if ~ischar(extract_mode) || ...
                ~ischar(extract_colname) || ...
                (isempty(extract_colname) && length(extract_value)>1) || ...
                (n>1 && k>1)
            error('codebook:extract, bad input')
        end
        
        idx_cdk = strcmp(extract_colname, {cdk.colname});
        
        if ~any(idx_cdk)
            error('codebook:extract, this codebook does not contains the required colname <%s>',extract_colname)
        end
        
        cdk=cdk(idx_cdk);
        
        switch extract_mode
            case 'colnum'
                z=cdk.colnum;
            case 'book'
                z=cdk.book;
            case 'codebook'
                z=cdk;
                
            case 'ids2names'
                
               
                %%% -- new lionel : faster 
                z=repmat({''},n,k);
                id_finite = isfinite(extract_value);
                if any(id_finite)
                    [uni_extract,~,idx_uni_in_data] = unique(extract_value(id_finite));
                    [~,ia,ib] = intersect(cdk.colnum,uni_extract);
                    uni_extract_val = repmat({''},size(uni_extract));
                    uni_extract_val(ib) = cdk.book(ia);
                    z(id_finite) = uni_extract_val(idx_uni_in_data);
                end

                
                %%% -- old with for ...
%                 z=repmat({''},n,k);
%                 
%                 uni_finite_extract=unique(extract_value(isfinite(extract_value)));
%                 
%                 for i=1:length(uni_finite_extract)
%                     idx=find(cdk.colnum==uni_finite_extract(i));
%                     if ~isempty(idx)
%                         z(extract_value==uni_finite_extract(i))=cdk.book(idx);
%                     end
%                 end
                
            case 'names2ids'
                if ~iscellstr(extract_value)
                    error('codebook:extract, In mode names2ids input has to be a cell array')
                end
                
                
                %%% -- new with intersect
                [uni_extract,~,idx_uni_in_data]=unique(extract_value);
                [~,ia,ib] = intersect(cdk.book,uni_extract);
                uni_extract_val = NaN(size(uni_extract));
                uni_extract_val(ib) = cdk.colnum(ia);
                z=uni_extract_val(idx_uni_in_data);
      
                
                %%% -- old with for ...
%                 z=NaN(n,k);
%                 uni_extract=unique(extract_value);
%                 
%                 for i=1:length(uni_extract)
%                     idx=find(strcmp(cdk.book,uni_extract{i}));
%                     if ~isempty(idx)
%                         z(strcmp(extract_value,uni_extract{i}))=cdk.colnum(idx);
%                     end
%                 end
 
            otherwise
                
                error('codebook:extract, this mode is not handle')
        end
        %>*
        
        
        
    case 'stack'
        
        z=[];
        t={};
        
        %< stack the codebook
        % 'stack' possibility
        % - stack 2 valids codebook + or empty codebooks
        % - if one/several elements of the codebooks has the same colname
        %    *if codebooks are "equal" : OK
        %    *if not,  same codebooks are merged + output value "t" correspond to the list of merged codebooks
        %  - This means, for example for a st_data, these merged codebooks has to be handled !!!!
        
        if nargin==2
            [b1 data1]=codebook('iscodebook',varargin{1});
            if b1
                z = data1;
            else
                error('codebook:stack , input is not a valid codebook')
            end
            return
        end
        if nargin > 3
            z = varargin{1};
            for n=2:length(varargin)
                z = codebook('stack',z, varargin{n});
            end
            return;
        end
        [b1 data1]=codebook('iscodebook',varargin{1});
        [b2 data2]=codebook('iscodebook',varargin{2});
        
        if b1 && b2
            
            [col_intersect,idx_sCol1,idx_sCol2]=intersect({data1.colname},{data2.colname});
            if isempty(col_intersect)
                z=data1;
                for i=1:length(data2)
                    z(end+1)=data2(i);
                end
            else
                idx_sCol1_uni=setdiff(1:length({data1.colname}),idx_sCol1);
                idx_sCol2_uni=setdiff(1:length({data2.colname}),idx_sCol2);
                if ~isempty(idx_sCol1_uni)
                    z=data1(idx_sCol1_uni);
                end
                if ~isempty(idx_sCol2_uni)
                    if ~isempty(z)
                        for i=1:length(idx_sCol2_uni)
                            z(end+1)=data2(idx_sCol2_uni(i));
                        end
                    else
                        z=data2(idx_sCol2_uni);
                    end
                end
                if ~isempty(idx_sCol1)
                    for i=1:length(idx_sCol1)
                        if ~codebook('isequal',data1(idx_sCol1(i)),data2(idx_sCol2(i)))
                            t=cat(2,t,{data1(idx_sCol1(i)).colname});
                            [z_tmp tmp_merge]=codebook('merge',data1(idx_sCol1(i)),data2(idx_sCol2(i)));
                            if ~isempty(tmp_merge)
                                st_log(sprintf('codebook:stack, colname %s has been merged \n',data1(idx_sCol1(i)).colname));
                            end
                        else
                            z_tmp=data1(idx_sCol1(i));
                        end
                        
                        if ~isempty(z)
                            z(end+1)=z_tmp;
                        else
                            z=z_tmp;
                        end
                        
                    end
                end
            end
            
            [b z]=codebook('iscodebook',z);
            if ~b
                error('codebook:stack , impossible to stack these codebooks')
            end
        elseif b1 && isempty(data2)
            z=data1;
        elseif b2 && isempty(data1)
            z=data2;
        elseif isempty(data1) && isempty(data2)
            z=[];
        else
            error('codebook:stack inputs are not valids codebooks !!!');
        end
        
        
        
    case 'merge'
        
       
        %< merge of two "simple" codebooks
        if nargin~=3
            error('codebook:merge,bad inputs')
        end
        [b1 data1]=codebook('iscodebook',varargin{1});
        [b2 data2]=codebook('iscodebook',varargin{2});
        if length(data1)>1 || length(data2)>1 || ...
                ~b1 || ~b2 || ...
                ~strcmp(data1.colname,data2.colname)
            error('codebook:merge ,only merge two "simple" codebooks with same colname')
        end
        
        
        out_book=unique(cat(2,data1.book,data2.book));
        out_colnum=NaN(length(out_book),1);
        
        [int_book int_1 int_2]=intersect(data1.book,data2.book);
        if ~isempty(int_book)
            % gives colnum  : not intersect book + not equal
            if length(int_book)~=length(out_book)
                [book_in1_not2 idx_in1_not2]=setdiff(data1.book,data2.book);
                [book_in2_not1 idx_in2_not1]=setdiff(data2.book,data1.book);
                if ~isempty(book_in1_not2)
                    [id_tmp12 idx_in_tmp12]=ismember(out_book,book_in1_not2);
                    colnum_tmp12=data1.colnum(idx_in1_not2(idx_in_tmp12(id_tmp12)));
                    out_colnum(id_tmp12)=colnum_tmp12;
                end
                if ~isempty(book_in2_not1)
                    [id_tmp21 idx_in_tmp21]=ismember(out_book,book_in2_not1);
                    colnum_tmp21=data2.colnum(idx_in2_not1(idx_in_tmp21(id_tmp21)));
                    
                    if ~isempty(book_in1_not2) && ~isempty(intersect(colnum_tmp12,colnum_tmp21))
                        % case where there are similar colnum for different book...
                        start_tmp=max(0,max(cat(1,data1.colnum,data2.colnum)));
                        colnum_val_2change=intersect(colnum_tmp12,colnum_tmp21);
                        colnum_val_change=(start_tmp+1):1:(start_tmp+length(colnum_val_2change));
                        [id_cchange idx_cchange]=ismember(colnum_tmp21,colnum_val_2change);
                        colnum_tmp21(id_cchange)=colnum_val_change(idx_cchange(id_cchange));
                    end
                    out_colnum(id_tmp21)=colnum_tmp21;
                end
            end

            id_int_book_equal=data1.colnum(int_1)==data2.colnum(int_2);
            % gives colnum  : intersect book + equal
            if any(id_int_book_equal)
                idx_int_book_equal=find(id_int_book_equal);
                [id_tmp idx_in_tmp]=ismember(out_book,int_book(idx_int_book_equal));
                out_colnum(id_tmp)=data1.colnum(int_1(idx_int_book_equal(idx_in_tmp(id_tmp))));
            end
            % gives colnum  : intersect book + not equal
            if any(~id_int_book_equal)
                id_tmp=ismember(out_book,int_book(~id_int_book_equal));
                out_colnum(id_tmp)=transpose(max(0,max(out_colnum))+1:1:max(0,max(out_colnum))+sum(id_tmp));
            end
              
        else
           int_num=intersect(data1.colnum,data2.colnum); 
           if isempty(int_num)
               [id_tmp1 idx_in_tmp1]=ismember(out_book,data1.book);
               out_colnum(id_tmp1)=data1.colnum(idx_in_tmp1(id_tmp1));
               [id_tmp2 idx_in_tmp2]=ismember(out_book,data2.book);
               out_colnum(id_tmp2)=data2.colnum(idx_in_tmp2(id_tmp2));               
           else
               out_colnum=transpose(1:length(out_colnum));
           end
        end

        z = codebook('new',out_book,data1.colname,out_colnum);
        
    
    otherwise
        error('codebook:mode', 'mode <%s> unknown', mode);
end

