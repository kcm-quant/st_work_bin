function bool = isempty_st_data(x)
bool = isempty(x)||isempty(x.date);