function [h, v] = plot_focus( title)
% PLOT_FOCUS - get the plot focus on the node
h = findobj( 'name', title);
if isempty(h) || ~ishandle( h)
    h = figure('name', title, 'NumberTitle', 'off');
    v = uipanel('backgroundcolor', [238 238 208]/256, 'borderwidth', 0);
else
    v = findobj(h,'type', 'uipanel');
end
figure(h);
