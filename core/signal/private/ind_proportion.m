function fp = ind_proportion( r)
% IND_PROPORTION - indicateur de proportion
%
% fp = ind_proportion( 1/20)
%
% See also build_st_function

fp = @IND_PROPORTION;

function p = IND_PROPORTION( v, t)
    idx_sup = t>t(1)+(max(t)-min(t))*(1-r);
    nb_all  = length(v);
    p = (sum(idx_sup)/nb_all - r)*sqrt(nb_all)/sqrt(r*(1-r));
end
end