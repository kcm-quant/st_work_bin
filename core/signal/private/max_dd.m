function [mdd idx] = max_dd( data)
% MAX_DD calcule le max-drop-down d'une s�rie temporelle
% 
% On suppose que 'data' contient des colonnnes suivantes:
% - max
% - min
%
% See also build_st_function st_max_min
if isstruct(data)
    max_vals = [st_data('cols', data, 'max'); Inf];
    min_vals = st_data('cols', data, 'min');
else
    max_vals = [data(:,1); Inf];
    min_vals = data(:,2);
end
n = length(min_vals);
next_idx = arrayfun(@(i)(i + find(max_vals(i+1:end) > max_vals(i), 1, 'first')),1:n);
next_idx = min(next_idx, repmat(n,1,n));
% chercher le max dropdown
[mdd idxM] = max(arrayfun(@(i,next_i)max_vals(i) - min(min_vals(i:next_i)), 1:n,next_idx));
[val idxm] = min(min_vals(idxM:next_idx(idxM))); 
idx        = [idxM idxm + idxM - 1];