function f_pr = point_ratio( f0, f1, idx)
% POINT_RATIO - to be used in st_function
%
% f_pr = point_ratio( @mean, r)
% f_pr = point_ratio( @sum, r, @(x)sum(x)/5)
%
% See also st_function build_st_function

if nargin < 2
    f1  = f0;    
    idx = [1,1];
elseif nargin == 2
    idx = [1,1];
end

f_pr = @POINT_RATIO;

%%** Fonction de ratio temps business
function v = POINT_RATIO( s)    
    v = feval(f0, s(:,idx(1)))./feval(f1, s(:,idx(2)));
end

end