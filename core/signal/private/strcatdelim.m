function str = strcatdelim( cell_strs, varargin)
% STRCATDELIM concate an cell array of string in using an other string like
%  delimiter, to obtain an unique string.
%
% See also build_st_function

if (nargin == 0)
    delim = '';
else
    delim = varargin{1};
end

str = '';
for i=1:length(cell_strs)
    try 
        str = strcat(str,  delim, func2str(cell_strs{i}));
    catch
        if isempty(findstr(cell_strs{i}, '@'))
            str = strcat(str,  delim, num2str(cell_strs{i}));        
        end
    end
end

