function f_tr = time_ratio( f0, r, f1, idx)
% TIME_RATIO - to be used in st_functions
%
% f_tr = time_ratio( @mean, r)
% f_tr = time_ratio( @sum, r, @(x)sum(x)/5)
%
% See also st_function build_st_function

if nargin<3
    f1 = f0;
    idx = [1,1];
elseif nargin == 3
    idx = [1,1];
end

f_tr = @TIME_RATIO;

%%** Fonction de ratio temporel
function v = TIME_RATIO( s,t)
    idx_sup = t>t(1)+(max(t)-min(t))*(1-r);
    v = feval(f0, s(idx_sup,idx(1)))./feval(f1, s(:,idx(2)));
end

end