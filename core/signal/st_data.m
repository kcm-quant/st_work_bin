function [data, idx, varargout] = st_data( mode, varargin)
% ST_DATA - working with our structured data
%
% - creation and extension
% data = st_data( 'init', 'title', 'Mes donn?es', 'value', cumsum(randn(100,2)), ...
%                   'date', (1:100)', 'colnames', { 'V-1', 'V-2' })
% data = st_data( 'empty-init');
% data = st_data('sql', 'VEGA', 'miswork', 'TccAlgoId,OrderDate*,SalesTraderName')
% [data, b] = st_data('isempty', data);
% data = st_data('log', data, 'gieo', 'Les donn?es en input contiennent des volumes n?gatifs, je pr?f?re ne pas travailler dans ces conditions')
% st_data('check', data [,raise_error_if_check_fails])
% data = st_data('from-matrix', cumsum(randn(100,2)))
% data = st_data('from-cell', title, colnames, values)
% data = st_data('from-struct', struct with uniform fields, field for date, non colums-field)
% data = st_data('stack', data1, data2)
% data = st_data('add-col', data, value, 'colnames;')
% data = st_data('sort', data, 'colname|.date' [, 'ascend'])
% data = st_data('transpose', data)
% data = st_data('copy_info_field', data, data2.info, {'thefieldIwantToCat'})
%
% - extraction
% data = st_data( 'keep-cols', data, 'V-1;V-2')
% data = st_data( 'remove', data, 'V-2');
% vals = st_data( 'cols', data, 'V-2;.date' [, idx])
% {data} = st_data('demux', data);                  % Return a cellarray of signals
% data = st_data('merge', data1, data2, 'precision', 1/24/60/60/10)
% data = st_data('unio', { data1, data2, ...}) - replace missing values by nans, not tested with rownames
% data = st_data('align', data_ref, data_plus)
% data = st_data('intersect', data1, data2) - ne retient que les dates communes
% [data, idx1, idx2] = st_data('intersect-approx', data1, data2) - ne retient que les dates ayant approximativement moins d'une second d'?cart
% [data, idx1, idx2] = st_data('intersect-approx', data1, data2, datenum(0,0,0,0,1,0)) - ne retient que les dates ayant approximativement moins d'une minute d'?cart
% data = st_data('interp', data1, data2, ..., dataN) - interpolle avec interp1
% data_sub = st_data('subsample', data, datenum(0,0,0, 0,10,0))
% data_all = st_data('last', data1, data2) - interp last data2 at data1 dates
% data_sub = st_data('grid_rm', data, idx) - les donn?es doivent ?tre sur une grille temporelle uniform?ment espac?e, et cela retire par exemple les deux premiers points de chaque jour
% vals = st_data('to-vals', data);
% str = st_data('log2str', data) % - makes a string to describe the logged data processing that the st_data has been undergone
%
%
%
% - Financial Equity specific needs
% data_sub = st_data('filter-td', data, {'MAIN', 'CHIX'}) - filtre les trading_destinations qui nous int?ressent, le march? primaire et CHIX dans cet exemple
% data_corr = st_data('cac', data) - makes corrections for corporate actions on the columns for which we know what to do
%
% - affichage
% h    = st_data('plot', data)
%
% - modifications d'un signal structur?
% data = st_data('formula-indicators', data, {'phase', 'turnover'} [, 'on_the_fly_indicators', ...]) % thx to st_methodology.xml formulas to get turnover from vwap and volume are automatically used
% data_agg = st_data('agg', data, 'colnames', aggcols_wanted, 'grid_options', {'time_type', 'seconds', 'window', 60, 'step', 60});
% data = st_data('apply-formula', data, '{V-1} - {V-2}' [, 'output', 'value|st_data'])
% data = st_data('each-row-formula', data, '{V-1} - {V-2}')
% [data1, ..., dataN] = st_data('get-rows', data) - un signal par rowname
% data = st_data('rows-to-cols', data) - une colonne par rowname
% data = st_data('where', data, 'selection-formula')
% data = st_data('from-idx', data, 10:20)
% data = st_data('extract-day', data, '26/06/2007', 'format', 'dd/mm/yyyy')
% data = st_data('nonan-rows', data);
% data = st_data('iday2array', data)
% data = st_data('iday2array', data, 'volume')
% data = st_data('group-by', perf_data, '{V-1},{V-2}', 'apply-formula', 'nanmean({V-3})');
% data = st_data('accum', data, 'col1;col2;...;colN' [, 'formula', {'{formula}', 'newnames'} ] [, 'f_colnames', @fc])
%    data2 = st_data('accum', data, '.date;td;range', ...
%              'formula', { '[{trading destination},{high_prc}-{low_prc},{average_spread_numer}./{average_spread_denom}]', {'td','range', 'psi'} }, ...
%              'f_colnames', @(c)arrayfun(@(t)get_repository('td_id2td_name',t),c,'uni',false) );
%
% - xlsread
% data = st_data('xls', 'E:\DATA\incoming\Book6.xls', 'Sheet1', 'P2:S3368', 'colnames', 'MaxPercentVolume;WouldLevel;ExecSpread;quantity_to_reach' )
% data = st_data('xls', 'E:\DATA\incoming\Book6.xls', 'Sheet1', 'P2:S3368', 'colnames', ':P1:S1' )
% data = st_data('xls', 'E:\DATA\incoming\Book6.xls', 'Sheet1', 'P2:S3368', 'colnames', ':P1:S1', 'rownames', 'B2:B3368' )
% data = st_data('xls', 'E:\DATA\incoming\Book6.xls', 'Sheet1', 'B2:H3368', 'colnames', ':B1:H1', 'rownames', 'B2:B3368' )
%
% - save/load operations
% {data} = st_data('data2ope', data, 'days')
% st_data( 'ope-save', 'fname', {data})
% {data} = st_data('ope-load', 'fname')
% {data} = st_data('ope-load', 'fname', {'titles' })
% st_data('ope-add', 'fname', {data});
%
% st_data('2csv', data, 'fname.csv')
% options:
% - sep: ';'
% - date-format: 'dd/mm/yy HH:MM:SS.FFF'
% - verbose: 1
%
% Examples:
%%% extended_stack
% cdk_1a=codebook('new',{'a1','a2','a3','a4','a5','a6','a7'},'a',1:7);
% cdk_1b=codebook('new',{'a1','a2','a3','a4','a5','a6','a7','a8'},'a',1:8);
% data1a=st_data('init','title','','date',(700001:700004)','value',[1:4;2:5]','colnames',{'a','b'},'codebook',cdk_1a);
% data1b=st_data('init','title','','date',(700001:700004)','value',[1:4;2:5]','colnames',{'a','b'},'codebook',cdk_1b);
% data2=st_data('init','title','','date',(700000:700006)','value',[1:7;1:7]','colnames',{'a','c'},'codebook',cdk_1a);
% data_out1=st_data('extended_stack',{data1a,data1b},'a');
% data_out2=st_data('extended_stack',{data2,data1b},'a');
%%% extended_union
% data1=st_data('init','title','','date',(700001:700004)','value',[1:4;2:5]','colnames',{'a','b'});
% data2=st_data('init','title','','date',(700000:700006)','value',[1:7;1:7]','colnames',{'c','b'});
% data3=st_data('init','title','','date',(700006:700012)','value',[1:7;8:14]','colnames',{'d','b'});
% data_out1=st_data('extended_union',{data1,data2,data3},'b');
%%% formula-indicators
% assert(4==getfield(st_data('fi', st_data('init', 'value', [8 2], 'colnames', {'turnover', 'volume'}, 'title', 'test', 'date', NaN), 'vwap'), 'value'))
%
% See also st_plot codebook
%
% author : 'clehalle@cheuvreux.com'
% reviewer :
% date : '08/03/2011'
% version : '1.0'
% last_checkin_info : $Header: st_data.m: Revision: 125: Author: namay: Date: 04/04/2013 01:42:17 PM$

idx  = [];
data = [];


switch lower(mode)
    
    case '2struct'
        data_in = varargin{1};
        data = rmfield(data_in, {'value', 'colnames'});
        fns = fieldnames(data);
        for i = 1 : length(data_in.colnames)
            assert(~ismember(data_in.colnames{i}, fns), ...
                'There is already a field in your st_data named as one of the colnumns');
            data.(data_in.colnames{i}) = st_data('col', data_in, data_in.colnames{i});
        end
        
    case 'copy_info_field'
        % this mode takes an st_data, an info field and (optionally) a list of fiednames
        % as input and should return the st_data with a merged info field according to the following rules
        %   - if a fieldname appears only in one info field, it will appear
        %   in the result with the same content.
        %   - if a fieldname appears in both fields but not in the input
        %   list of fieldnames (fieldnames2cat below), then the merged info
        %   field will keep only the content of the info field with that
        %   fieldname from the st_data
        % - if a fieldname appears in both fields and also in the input
        %   list of fieldnames (fieldnames2cat below), then the merged info
        %   field will concatenate the contents of both info fields
        %
        % Rq : do not forget to copy the title if you have no interest in
        % changing it
        data = varargin{1};
        if ~isfield(data, 'info')
            data.info = varargin{2};
            return;
        end
        
        info = varargin{2};
        if length(varargin) > 2
            fieldnames2cat = varargin{3};
        else
            fieldnames2cat = {};
        end
        
        data.info = struct_recursive_merge(data.info, info, fieldnames2cat);
        
    case 'filter-td' % not really a general mode for st_data, but rather a
        % mode for stocks traded on several destinations. This mode assumes
        % - there are some standards info on the stock in the info field.
        %       namely : security_id, in the field td_info : trading_destination_id, place_id
        
        data = varargin{1};
        td = varargin{2};
        clear varargin;
        if isempty( data)
            return
        end
        if isempty(td)
            return;
        end
        % filter on trading destinations
        if ~iscell( data)
            no_cell = true;
            data = { data };
            idx_empty = false;
            first_ne = 1;
        else
            no_cell = false;
            idx_empty = cellfun(@(c)st_data('isempty-no_log', c), data);
            first_ne = find(~idx_empty, 1, 'first');
        end
        if isempty(first_ne)
            return;
        end
        sec_id = data{first_ne}.info.security_id;
        % < This part outside the loop doe not allow for heterogenuous data in sec id
        kodes = get_repository( 'any-to-sec-td', 'security_id', sec_id, ...
            'trading-destinations', td);
        o_tdi = kodes.trading_destination_id;
        % >
        % < TODO wonder if this has any interest
        full_tdi = get_repository( 'trading-destination-info', sec_id);
        % >
        %         warn = false;
        idx = cell(size(data));
        for c=1:length(data)
            if idx_empty(c)
                % nothing to do
            else
                % On note le place_id initial car les donn?es sont
                % timestamp?es en heures locales de cette place
                main_timezone = data{c}.info.td_info(1).timezone;
                % fitrage du .info.td_info
                all_td_info      = [data{c}.info.td_info.trading_destination_id];
                if length( all_td_info) < length( o_tdi)
                    % Is this informative?
                    %                     if ~warn
                    %                         st_log(['st_data:filter-td - your data and the DB..repository' ...
                    %                             'does not agree about available TD for sec id <%d>\n'], sec_id);
                    %                     end
                    %                     warn = true;
                    data{c}.info.td_info = full_tdi;
                end
                try
                    keep_td_info_idx = arrayfun(@(td_)find(td_==all_td_info), o_tdi, 'uni', true);
                    % retrouver les trading destination demand?es dans td_info
                    data{c}.info.td_info = data{c}.info.td_info(keep_td_info_idx);
                catch ME %#ok<NASGU>
                    keep_td_info_idx = arrayfun(@(td_)find(td_==all_td_info), o_tdi, 'uni', false);
                    keep_td_info_idx = [keep_td_info_idx{:}];
                    data{c}.info.td_info = data{c}.info.td_info(keep_td_info_idx);
                end
                % filtrage des donn?es
                td_vals  = st_data('col', data{c}, 'trading_destination_id');
                idx{c} = ismember( td_vals, o_tdi);
                data{c}.value = data{c}.value(idx{c},:);
                data{c}.date  = data{c}.date(idx{c});
                if isfield( data{c}, 'rownames')
                    data{c}.rownames = data{c}.rownames(idx{c});
                end
                % apr?s avoir filtr? les donn?es, si la trading_destination
                % principale n'a pas ?t? conserv?e, on peut avoir besoin de
                % changer l'heure
                % Il faut de plus le faire pour tout indicateur qui
                % repr?sente un horodatage, ici cette liste est cod?e en
                % dur!!!! 'begin_slice', 'end_slice', 'time_close', 'time_open'
                if ~isempty(keep_td_info_idx) && ~any(keep_td_info_idx ==1) ...
                        && ~isempty(data{c}.date)
                    offset_ = timezone('get_offset','initial_timezone', main_timezone, ...
                        'final_timezone', data{c}.info.td_info(1).timezone, ...
                        'dates', data{c}.date);
                    
                    data{c}.date = offset_ + data{c}.date;
                    
                    idx_col2convert = ismember(data{c}.colnames, ...
                        {'begin_slice', 'end_slice', 'time_close', 'time_open'});
                    if any(idx_col2convert)
                        data{c}.value(:, idx_col2convert) = bsxfun(@plus, data{c}.value(:, idx_col2convert),offset_);
                    end
                end
            end
        end
        if no_cell
            data = data{1};
            idx = idx{1};
        end
        
    case 'trading_daily_cac'
        % for 1 stock: equivalent to st_data('cac', data, 'ignore-unknown-indic', 'trading-daily');
        % enable several security_id in underlying daily data
        data = varargin{1};
        if any(strcmpi(data.colnames, 'security_id'))
            usec = unique(st_data('cols', data, 'security_id'));
            fdata = st_data('empty-init');
            for s = 1:length(usec)
                tmp = st_data('where', data, ['{security_id}==' num2str(usec(s))]);
                tmp.info.security_id = usec(s);
                tmp = st_data('cac', tmp, 'ignore-unknown-indic', 'trading-daily');
                fdata = st_data('stack', fdata, tmp);
            end
            data = fdata;
        else
            data = st_data('cac', data, 'ignore-unknown-indic', 'trading-daily');
        end
        
    case 'eps_cac'
        % for 1 stock: equivalent to st_data('cac', data, 'ignore-unknown-indic', 'trading-daily');
        % enable several security_id in underlying daily data
        data = varargin{1};
        if any(strcmpi(data.colnames, 'security_id'))
            usec = unique(st_data('cols', data, 'security_id'));
            fdata = st_data('empty-init');
            for s = 1:length(usec)
                tmp = st_data('where', data, ['{security_id}==' num2str(usec(s))]);
                tmp.info.security_id = usec(s);
                tmp = st_data('cac', tmp, 'ignore-unknown-indic', 'eps');
                fdata = st_data('stack', fdata, tmp);
            end
            data = fdata;
        else
            data = st_data('cac', data, 'ignore-unknown-indic', 'eps');
        end
        
    case {'cac', 'corporate_action_correction'}
        % if you provide a cell-array of st_data then this mode assumes
        % that the cols will be the same in each st_data and taht each cell
        % array contains the data for only one day and one security_id
        %
        % you can provide a third argument 'ignore-unknown-indic' to avoid
        % errors
        data = varargin{1};
        mode_ignore_unkown= false;
        trd = false;
        eq = false;
        if nargin> 2
            if isequal(varargin{2},'ignore-unknown-indic')
                mode_ignore_unkown= true;
            elseif isequal(varargin{2},'trading-daily')
                trd = true;
            elseif isequal(varargin{2},'eps')
                eq = true;
            end
        end
        if nargin> 3
            if isequal(varargin{3},'trading-daily')
                trd = true;
            elseif isequal(varargin{3},'eps')
                eq = true;
            end
        end
        ic = iscell(data);
        
        %< testing for emptyness and getting the security_id from info
        %field
        if ic
            empty_cells = cellfun(@(c)st_data('isempty-nl', c), data);
            first_ne = find(~empty_cells, 1, 'first');
            if isempty(first_ne)
                return;
            end
            sec_id = data{first_ne}.info.security_id;
            cols = data{first_ne}.colnames;
        else
            if st_data('isempty-nl', data)
                return;
            end
            sec_id = data.info.security_id;
            cols = data.colnames;
        end
        %>
        
        %< wondering which cols have to be corrected
        if ~trd & ~eq
            indicators = indicator_factory();
            indicators_s2ca_names = {indicators.name};
        end
        mult4price  = false(size(cols));
        mult4volume = false(size(cols));
        for i = 1 : length(cols)
            if trd
                indv = regexp(cols{i}, 'volume', 'once');
                indp = regexp(cols{i}, 'prc', 'once');
                indt = regexp(cols{i}, 'turnover', 'once');
                if ~isempty(indv)
                    mult4volume(i) = 1;
                    mult4price(i) = 0;
                elseif ~isempty(indp)
                    mult4volume(i) = 0;
                    mult4price(i) = 1;
                elseif ~isempty(indt)
                    mult4volume(i) = 1;
                    mult4price(i) = 1;
                end
            elseif eq
                inde = regexp(cols{i}, 'EPS', 'once');
                if ~isempty(inde)
                    mult4volume(i) = 0;
                    mult4price(i) = 1;
                end
            else
                indc = strcmpi(cols{i}, indicators_s2ca_names);
                if ~any(indc)
                    if mode_ignore_unkown;continue;end
                    error('st_data:check_args', 'Unknown indicator <%s>', cols{i});
                elseif ~indicators(indc).is_s2ca
                    continue;
                end
                mult4price(i)  = indicators(indc).simplified_cac.mult_by_cum_price_ratio;
                mult4volume(i) = indicators(indc).simplified_cac.mult_by_cum_volume_ratio;
            end
        end
        if ~any(mult4price|mult4volume)
            return;
        end
        %>
        
        %< retrieving data about corporate actions
        s = get_repository('cac', sec_id, get_as_of_date('yyyymmdd'));
        if st_data('isempty-nl', s)
            return;
        end
        %>
        
        %< Applying corrections
        price_multipliers  = st_data('cols', s, 'cum_price_ratio');
        volume_multipliers = st_data('cols', s, 'cum_volume_ratio');
        if ic
            
            % TEST BOUCLE SUR CHAQUE CELLULE
            if false
                for j = 1:length(data)
                    idx2corr = data{j}.date < s.date(1);
                    if ~isempty(idx2corr)
                        data{j}.value(idx2corr, mult4price) = data{j}.value(idx2corr, mult4price)*price_multipliers(1);
                        data{j}.value(idx2corr, mult4volume) = data{j}.value(idx2corr, mult4volume)*volume_multipliers(1);
                    end
                    for i = 2 : length(s.date)
                        if ~isempty(idx2corr)
                            idx2corr = data{j}.date >= s.date(i-1) & data{j}.date < s.date(i);
                            data{j}.value(idx2corr, mult4price) = data{j}.value(idx2corr, mult4price)*price_multipliers(i);
                            data{j}.value(idx2corr, mult4volume) = data{j}.value(idx2corr, mult4volume)*volume_multipliers(i);
                        end
                    end
                end
            end
            
            % ANCIENNE METHODE CORRIGEE
            agenda = NaN(size(data));
            for i = 1 : length(data)
                if ~empty_cells(i)
                    agenda(i) = floor(data{i}.date(1));
                end
            end
            idx2corr = find(agenda < s.date(1));
            for j = 1 : length(idx2corr)
                if ~empty_cells(j)
                    data{j}.value(:, mult4price) = data{j}.value(:, mult4price)*price_multipliers(1);
                    data{j}.value(:, mult4volume) = data{j}.value(:, mult4volume)*volume_multipliers(1);
                end
            end
            for i = 2 : length(s.date)
                idx2corr = find(agenda >= s.date(i-1) & agenda < s.date(i));
                for j = 1 : length(idx2corr)
                    if ~empty_cells(j)
                        data{j}.value(:, mult4price) = data{j}.value(:, mult4price)*price_multipliers(i);
                        data{j}.value(:, mult4volume) = data{j}.value(:, mult4volume)*volume_multipliers(i);
                    end
                end
            end
            
        else
            idx2corr = data.date < s.date(1);
            data.value(idx2corr, mult4price) = data.value(idx2corr, mult4price)*price_multipliers(1);
            data.value(idx2corr, mult4volume) = data.value(idx2corr, mult4volume)*volume_multipliers(1);
            for i = 2 : length(s.date)
                idx2corr = data.date >= s.date(i-1) & data.date < s.date(i);
                data.value(idx2corr, mult4price) = data.value(idx2corr, mult4price)*price_multipliers(i);
                data.value(idx2corr, mult4volume) = data.value(idx2corr, mult4volume)*volume_multipliers(i);
            end
        end
        %>
        
    case {'agg', 'fi', 'formula-indicators'}
        % These mode assumes : TODO put it in doc
        % - if you give a cell array of st_data then they should be
        %       homogenuous in terms of wether they are in local time or gmt
        %       time. Furthermore it is mandatory to have the field
        %       locatime in the field info of the st_data
        GMT_PLACE_ID = 0;
        
        if ~iscell(varargin{1})
            data = varargin(1);
        else
            data = varargin{1};
        end
        
        if isempty(data)
            varargout = {[], [], []};
            return;
        end
        
        cell_is_not_empty = true(length(data),1);
        nb_data = length(data);
        
        if ~iscell(varargin{1})
            if isempty(data{1}) || isempty(data{1}.value)
                data=data{1}; % On veut bien sortir le m?me type qu'en entr?e !
                varargout = {[], [], []};
                return; % Attention si l'on change le champs info plus loin
            end
        else
            idx = cell(nb_data,1);
            for c=1:nb_data
                if isempty(data{c}) || isempty(data{c}.value)
                    idx{c} = [];
                    varargout{1}{c} = [];
                    varargout{2}{c} = [];
                    varargout{3} = []; %TODO : {c} ????
                    cell_is_not_empty(c) = false;
                end
            end
            if ~any(cell_is_not_empty)
                return;
            end
        end
        
        %< On repere le premier cell non vide
        ne_1st = find(cell_is_not_empty,1, 'first');
        %>
        
        available_cols = data{ne_1st}.colnames;
        
        switch lower(mode)
            case 'agg'
                opt = options({'colnames', {}, ...
                    'grid_mode', 'time_and_phase', ...
                    'no_empty_bins', false,...
                    'grid_options', {}, ...
                    'is_gmt', ~data{ne_1st}.info.localtime, ... % eponym
                    'on_the_fly_indicators', {}, ...
                    },...
                    varargin(2:end));
                
                is_gmt = opt.get('is_gmt');
                if is_gmt
                    dts4conv = NaN(nb_data, 1);
                    dts4conv(cell_is_not_empty) = cellfun(@(c)floor(c.date(1)), data(cell_is_not_empty));
                    dts4conv(cell_is_not_empty) = timezone('get_offset', 'dates', dts4conv(cell_is_not_empty), ...
                        'init_place_id', GMT_PLACE_ID, ...
                        'final_place_id', data{cell_is_not_empty}.info.place_id);
                    for i = 1 : nb_data
                        if cell_is_not_empty(i)
                            data{i}.date = data{i}.date + dts4conv(i);
                        end
                    end
                end
                
                %The columns that should be present in the output
                wanted_outcols = opt.get('colnames') ;
                rc_hk = hash(convs('safe_str', unique(wanted_outcols)), 'MD5');
                
                %< preparing on the fly indicators, adding them to wanted_outcols
                on_the_fly_indicators = opt.get('on_the_fly_indicators');
                % NOT ADDING THEM AS COMPUTATION INTERMEDIARIES CANNOT BE IN FINAL
                % OUTCOLS
                %         if ~isempty(on_the_fly_indicators)
                %             wanted_outcols = cat(2, wanted_outcols, ...
                %                 on_the_fly_indicators.name);
                %         end
                %>
                
                %< create grid and apply aggregation path
                
                grid_inputs = struct('o_grid', {opt.get('grid_options')},...
                    'grid_mode', {opt.get('grid_mode')},...
                    'no_empty_bins', {opt.get('no_empty_bins')});
                
                mg_cols = make_grid(grid_inputs.grid_mode, 'return_cols',grid_inputs.o_grid{:});
                mg_hk = hash(convs('safe_str', mg_cols), 'MD5');
                
                cols = struct('wanted_outcols', {wanted_outcols},...
                    'available_cols', {available_cols},...
                    'rc_hk', rc_hk,...
                    'mg_hk', mg_hk);
                
                same_colnames = all(cellfun(@(c) isequal(c.colnames,available_cols), data(cell_is_not_empty)));
                
                if ~same_colnames
                    f_cine = find(cell_is_not_empty);
                    data_out = cell(size(data));
                    grid = cell(size(data));
                    idx_dump = cell(size(data));
                    extract_idx = cell(size(data));
                    kingpin_out = cell(size(data));
                    for i = 1:length(f_cine)
                        cols.available_cols = data{f_cine(i)}.colnames;
                        plan_output = ...
                            apply_plan_to_grid(data(f_cine(i)),true,grid_inputs,...
                            cols, on_the_fly_indicators) ;
                        data_out(f_cine(i)) = plan_output.data_out;
                        
                        grid(f_cine(i)) = plan_output.grid;
                        idx_dump(f_cine(i)) = plan_output.idx_dump;
                        extract_idx(f_cine(i)) = plan_output.extract_idx;
                        kingpin_out(f_cine(i)) = plan_output.kingpin_out;
                    end
                else
                    plan_output = apply_plan_to_grid(data,cell_is_not_empty,grid_inputs,...
                        cols, on_the_fly_indicators);
                    data_out = plan_output.data_out;
                    
                    grid = plan_output.grid ;
                    idx_dump = plan_output.idx_dump;
                    extract_idx = plan_output.extract_idx ;
                    kingpin_out = plan_output.kingpin_out;
                end
                
                if is_gmt
                    for i = 1 : nb_data
                        if cell_is_not_empty(i)
                            data_out{i}.date = data_out{i}.date - dts4conv(i);
                        end
                    end
                end
                
                %< Prepare output
                if ~iscell(varargin{1})
                    data = data_out{1};
                    idx = extract_idx{1};
                    varargout{1} = idx_dump{1};
                    varargout{2} = grid{1};
                    varargout{3} = kingpin_out{1};
                else
                    data = data_out;
                    idx = extract_idx;
                    varargout{1} = idx_dump;
                    varargout{2} = grid;
                    varargout{3} = kingpin_out;
                end
                %>
            case {'fi', 'formula-indicators'}
                opt = options_light({...
                    'on_the_fly_indicators', {}, ...
                    },...
                    varargin(3:end));
                if ~iscell(varargin{2})
                    varargin{2} = varargin(2);
                end
                data = apply_formula_indicators(data,cell_is_not_empty,...
                    available_cols,varargin{2},opt.on_the_fly_indicators);
                
                %< Prepare output
                if ~iscell(varargin{1})
                    data = data{1};
                end
                %>
                varargout{1} = data;
            otherwise
                error('st_data:mode', 'mode <%s> unknown', mode);
        end
        
    case 'to-vals'
        data = varargin{1}.value;
        
    case {'empty-init', 'empty'}
        % no input (or optionnaly a data_log message for the reason of an empty initilization), empty st_data as output
        data = st_data('log', struct('title', '', 'value', [], 'date', [], 'colnames', {{}},'info', []), 'eini', varargin{:});
        
    case {'isempty-no_log', 'isempty-nl'}
        % quite the same as 'isempty' but first output is the boolean,
        % which allows to use it this way :
        % if st_data('isempty-nl', data)
        %   ...
        % end
        %         [idx, data] = st_data('isempty', varargin{:});
        data = varargin{1};
        if iscell(data)
            data = cellfun(@(c)(isempty(c) || ~isfield(c, 'value') || isempty(c.value)), data, 'uni', true);
        else
            data = isempty(data) || ~isfield(data, 'value') || isempty(data.value);
        end
        
    case 'isempty'
        % varargin{1} = st_data/cell-array de st_data
        % output : [data, b, idxes_empty] dans data une data_log avec un
        % identifiant
        %       eieo a ?t? ajout? s'il se trouve que les donn?es sont vides
        %       b est un bool?en idiquant si le st_data est vide, ou si'lun
        %       des st_data du cell-array est vide (en fait c'est pas un
        %       booleen dans ce cas, mais l'indice du premier st_data vide)
        %
        % l'utilisation classique (dans un st_node) devrait ?tre la
        % suivante :
        % [b, outputs] = st_data('isempty', inputs)
        % if b
        %   return;
        % end
        data = varargin{1};
        if iscell(data)
            idx = 0;
            for i = 1 : length(data)
                [data{i}, idx_tmp] = st_data('isempty', data{i});
                if idx_tmp
                    if ~idx
                        idx = i;
                        if nargout > 2
                            varargout = {i};
                        end
                    else
                        if nargout > 2
                            varargout{1}(end+1) = i;
                        end
                    end
                end
            end
            return;
        end
        idx = isempty(data) || ~isfield(data, 'value') || isempty(data.value);
        if idx
            data = st_data('log', data, 'eieo');
        end
        
    case 'log'
        % varargin{1} = st_data/[]
        %   si vide, alors le st_data retourn? sera intialis? par st_data('empty-init')
        %
        % varargin{2} = log_id : un identifiant pour le type de message,
        %   c'est une chaine de caract?res parmis les suivantes :
        %
        %   - eini = empty initialization (automatiquement ajout? lorsque vous utilisez st_data('empty-init'))
        %   - dini = data initialization (automatiquement ajout? lorsque vous utilisez st_data('init', ...))
        %   - eieo = empty in => empty out (automatiquement ajout? lorsque vous utilisez st_data('isempty', ...) et que le data est vide)
        %
        %   dans le cas des 3 identifiants ci-dessus varargin{3} est
        %   relativement inutile et peut ?tre homis, mais pas pour ceux qui
        %   figurent ci-dessous
        %
        %   - gini = garbage initialization (vous avez rep?r? des erreurs dans les donn?es mais vous les cr?ez quand m?me, des fois que ce soit quand m?me utilisable)
        %   - gigo = garbage in => garbage out
        %   - gieo = garbage in => empty out
        %   - gido = garbage in -> data out (il y avait des erreurs dans les donn?es mais vous les avez corrig?es : merci beaucoup!!!)
        %   - digo = data in    -> garbage out (par exemple vous navez pas pu trouver dans le r?f?rentiel une info dont vous aviez besoin alors vous avez improvis? une solution un peu bancale)
        %   - dieo = data in    -> empty out (par exemple une sp?cificit? des donn?es en input ne vous plait pas...)
        %   - warn = neither empty in nor garbage in but something special to
        %           warn the user about
        %
        % varargin{3} = message : une chaine de caract?re explicitant les
        %       sp?cifit?s du traitement subit par les donn?es
        data = varargin{1};
        log_id = varargin{2};
        if isempty(strmatch(log_id, {'dini', 'eini', 'eieo', 'gini', 'gigo', 'gieo', 'digo', 'gido', 'dieo', 'warn'}, 'exact'))
            error('st_data:log:check_args', 'Unrecognized identifier for message <%s>', log_id);
        end
        %<* empty init
        if isempty(data)
            data = st_data('empty-init');
            was_empty = true;
        else
            was_empty = false;
        end
        %>*
        % Patch traitement des data_log qui sont des cha?nes de caract?res
        if isfield(data, 'info') && isfield(data.info, 'data_log') && ischar(data.info.data_log)
            data.info = rmfield(data.info,'data_log');
        end
        %<* Looking for next index in data_log
        if isfield(data, 'info') && isfield(data.info, 'data_log')
            idx = length(data.info.data_log) + 1 - was_empty;
        else
            idx = 1;
        end
        %>*
        %<* looking in dbstack to know what to log bout the calling function
        dbs = dbstack;
        relevant_func_found = false;
        for i = 1 : length(dbs)
            if ~strcmp(dbs(i).name, 'st_data')
                relevant_func_found = true;
                break;
            end
        end
        if ~relevant_func_found
            fname = 'Base';
            line = NaN;
        else
            fname = dbs(i).file;
            line = dbs(i).line;
        end
        %>*
        %<* writing in data_log
        if ~isempty(strmatch(log_id, {'eieo', 'dini', 'eini'}, 'exact'))
            if length(varargin) > 2
                data.info.data_log(idx) = struct('timestamp', now, 'fname', fname, 'line', line,...
                    'id', log_id, 'message', varargin{3});
            else
                data.info.data_log(idx) = struct('timestamp', now, 'fname', fname, 'line', line,...
                    'id', log_id, 'message', '');
            end
        else
            data.info.data_log(idx) = struct('timestamp', now, 'fname', fname, 'line', line,...
                'id', log_id, 'message', varargin{3});
        end
        %>*
        %<* If user wants to log 'empty out' then we empties it
        if strcmp(log_id(end-1:end), 'eo')
            if isfield(data, 'value')
                data.value = data.value([],:);
            elseif isfield(data, 'colnames')
                data.value = zeros(0, length(data.colnames));
            else
                data.value = [];
            end
            data.date  = zeros(0, 1);
            if isfield(data, 'rownames')
                data.rownames = [];
            end
        end
        %>*
        
    case 'log2str'
        datain = varargin{1};
        if isstruct(datain) && isfield(datain, 'info') && ...
                isfield(datain.info, 'data_log') && ...
                isstruct(datain.info.data_log) && ...
                ~isempty(datain.info.data_log)
            idx = cell(length(datain.info.data_log), 1);
            for i = 1:length(datain.info.data_log)
                idx{i} = sprintf('%s : id=<%s> message=<%s> in filename=<%s> at line=<%d>', ...
                    datestr_ms(datain.info.data_log(i).timestamp), ...
                    datain.info.data_log(i).id, ...
                    datain.info.data_log(i).message, ...
                    datain.info.data_log(i).fname, ...
                    datain.info.data_log(i).line);
            end
            data = sprintf('%s\n', idx{:});
        else
            data = '';
            idx  = '';
            warning('st_data:log2str', ...
                'Unexpected format for st_data or no logged informations');
        end
        
    case 'last'
        %<* Aligne le deuxi?me signal sur le premier
        data1 = varargin{1};
        data2 = varargin{2};
        opt   = options({'padding-value', NaN}, varargin(3:end));
        c2    = size(data2.value,2);
        c1    = size(data1.value,2);
        data1.value    = cat(2, data1.value, repmat(opt.get('padding-value'), length(data1.date), c2));
        data1.colnames = cat(2, data1.colnames, data2.colnames);
        for n1=1:length(data1.date)
            n2=find(data2.date>data1.date(n1),1);
            if ~isempty(n2) && n2>1
                data1.value(n1,c1+1:end) = data2.value(n2-1,:);
            end
        end
        data = data1;
        %>*
        
    case 'merge'
        %<* Merge deux signaux
        % Le premier est multivari? et pas le second
        data1 = varargin{1};
        data2 = varargin{2};
        opt = options({ 'precision', 1/24/60/60/10}, varargin(3:end));
        
        if size(data2.value,2)~=1
            warning('st_data:merge', 'this should only be used with a vectorial 2nd input');
        end
        
        d1 = data1.date; d2 = data2.date;
        L1 = length(d1); L2 = length(d2);
        v1 = data1.value; v2 = data2.value;
        
        %< Union
        K1=size(v1,2);
        K2=size(v2,2);
        [d12, idx] = sort( [d1; d2]);
        v12 = repmat(nan, length(d12),K1+K2);
        v12(1:L1,1:K1) = v1;
        v12(L1+(1:L2),(K1+1):(K1+K2)) = v2;
        v12 = v12(idx,:);
        %>
        %< padding
        inan = isnan(v12);
        for d=2:L1+L2
            v12(d,inan(d,:)) = v12(d-1,inan(d,:));
        end
        % remove nans
        idx_nan = any(isnan(v12),2);
        if max(find(idx_nan))>sum(idx_nan)
            warning('st_data:merge', 'unexpected holes in non-nan values');
        end
        v12(idx_nan,:) = [];
        d12(idx_nan,:) = [];
        %>
        %< Calcul r?duction des dates
        precision = opt.get('precision');
        [d12_u, idx] = unique( floor(d12/precision)*precision);
        v12_u = v12(idx,:);
        %>
        data = data1;
        data.value = v12_u;
        data.date  = d12_u;
        data.colnames = cat(1, data1.colnames, cellfun(@(c)sprintf('%s - %s', data2.title, c), data2.colnames, 'uni', false));
        if length( unique( data.colnames)) ~= length( data.colnames)
            error('st_data:merge', 'ambiguous colnames');
        end
        %>*
        
    case 'merge_beta'
        %<* Fusion de deux signaux
        % les deux peuvent etre multi-varies
        data1 = varargin{1};
        data2 = varargin{2};
        opt = options({ 'precision', 1/24/60/60/10}, varargin(3:end));
        
        d1 = data1.date; d2 = data2.date;
        L1 = length(d1); L2 = length(d2);
        v1 = data1.value; v2 = data2.value;
        
        %< Union
        K1=size(v1,2);
        K2=size(v2,2);
        [d12, idx] = sort( [d1; d2]);
        v12 = zeros(length(d12),K1+K2);
        v12(1:L1,1:K1) = v1;
        v12(L1+(1:L2),(K1+1):(K1+K2)) = v2;
        v12 = v12(idx,:);
        %>
        %< padding
        idx_padd = [ones(L1,1),zeros(L1,1);zeros(L2,1),ones(L2,1)];
        v12 = [nan(1,size(v12,2));v12];
        idx_padd = cumsum([ones(1,2);idx_padd(idx,:)]);
        
        temp = accumarray(idx_padd(:,1),(1:size(v12,1))',[],@(x){repmat(v12(x(1),1:K1),size(x,1),1)});
        v12(:,1:K1) = vertcat(temp{:});
        temp = accumarray(idx_padd(:,2),(1:size(v12,1))',[],@(x){repmat(v12(x(1),K1+(1:K2)),size(x,1),1)});
        v12(:,K1+(1:K2)) = vertcat(temp{:});
        v12(1,:)     = [];
        idx_padd(1,:) = [];
        idx_rm   = any(idx_padd==1,2);
        
        v12(idx_rm,:)      = [];
        d12(idx_rm,:)      = [];
        %>
        %< Calcul r?duction des dates
        precision = opt.get('precision');
        [d12_u, idx,idx_un] = unique( floor(d12/precision)*precision);
        v12_u = v12(idx,:);
        %>
        data = data1;
        if isfield(data,'title') || isfield(data2,'title')
            data.title = [data1.title ' merged with ' data2.title];
        end
        data.value = v12_u;
        data.date  = d12_u;
        data.colnames = [data1.colnames,data2.colnames];
        if length( unique( data.colnames)) ~= length( data.colnames)
            error('st_data:merge_beta', 'ambiguous colnames');
        end
        %*>
        
    case 'fast-stack'
        %<* empilage rapide
        datas = varargin{1};
        idx_e = cellfun(@(c)st_data('isempty-nl', c), datas);
        datas(idx_e) = [];
        if isempty(datas)
            data = [];
        else
            data  = datas{1};
            data.date  = cellfun(@(d)d.date,datas,'uni',false);
            data.date  = cat(1,data.date{:});
            data.value = cellfun(@(d)d.value,datas,'uni',false);
            data.value = cat(1,data.value{:});
            if isfield(datas{end}, 'info')
                data.info = datas{end}.info;
            end
            if isfield(datas{end}, 'rownames')
                data.rownames = cellfun(@(d)d.rownames,datas,'uni',false);
                data.rownames = cat(1,data.rownames{:});
            end
        end
        %>*
        
    case '_h_fast-stack'
        %<* Uses HWrapper. Requires (:) in cellfuns ! (TODO overwrite iscell/is* ?)
        assert(0,'_h_fast-stack is not yet fixed');
        datas = varargin{1};
        idx_e = cellfun(@(c)st_data('isempty-nl', c), datas(:));
        datas(idx_e) = [];
        if isempty(datas)
            data = [];
        else
            data  = datas{1};
            data= Hwrap(data);
            data.date  = cellfun(@(d)d.date,datas(:),'uni',false);
            data.date  = cat(1,data.date{:});
            data.value = cellfun(@(d)d.value,datas(:),'uni',false);
            data.value = cat(1,data.value{:});
            if isfield(datas{end}, 'info')
                data.info = datas{end}.info;
            end
            if isfield(datas{end}, 'rownames')
                data.rownames = cellfun(@(d)d.rownames,datas,'uni',false);
                data.rownames = cat(1,data.rownames{:});
            end
        end
        
    case 'data2ope'
        %<* Translate data to operation list
        data  = varargin{1};
        tmode = varargin{2};
        switch lower(tmode)
            case { 'day', 'daily', 'days' }
                [days, idx, jdx] = unique(floor(data.date));
                datas = cell(1, length(days));
                for d=1:length(days)
                    cdx = d==jdx;
                    datas{d} = st_data('from-idx', data, cdx);
                    datas{d}.title = [ datas{d}.title ' x ' datestr(days(d), 'dd/mm/yyyy')];
                end
            otherwise
                error('st_data:data2ope:trans_mode', 'translation mode <%s> unknown', tmode);
        end
        data = datas;
        %>*
        
    case { 'to-csv', '2csv' }
        %<* Save to csv
        data  = varargin{1};
        fname = varargin{2};
        opt   = options({'sep', ';', 'date-format', 'dd/mm/yy HH:MM:SS.FFF', 'verbose', 1 }, ...
            varargin(3:end) );
        sep   = opt.get('sep');
        dt_format = opt.get('date-format');
        verb  = opt.get('verbose');
        st_log('Saving data <%s> to file <%s>...\n', ...
            data.title, fname);
        fod   = fopen( fname, 'w');
        % titre
        fprintf(fod, '%s\n', data.title);
        % noms de colonnes
        if isfield( data, 'rownames');
            fprintf(fod, ['rowname' sep]);
            rows = data.rownames;
        else
            rows = {};
        end
        fprintf(fod, ['date' sep]);
        fprintf(fod, [ '%s' sep], data.colnames{:});
        fprintf(fod, '\n');
        for r=1:length(data.date)
            if verb>0 && r>1 && floor(data.date(r))>floor(data.date(r-1))
                st_log('.');
            end
            if ~isempty(rows)
                % rowname
                fprintf( fod, [ rows{r} sep]);
            end
            % date
            fprintf( fod, '%s;', datestr( data.date(r), dt_format));
            % valeurs
            
            for j = 1 :length(data.colnames)
                if isfield(data,'codebook')
                    book =  codebook( 'get', data.codebook, data.colnames(j));
                    if ~isempty(book)
                        fprintf( fod, ['%s' sep], book{data.value(r,j)});
                    else
                        fprintf( fod, ['%f' sep], data.value(r,j));
                    end
                    
                else
                    fprintf( fod, ['%f' sep], data.value(r,j));
                end
            end
            fprintf( fod, '\n');
        end
        fclose( fod);
        st_log('done.\n');
        
    case { '2csv_nijos' }
        %<* Save to csv
        data  = varargin{1};
        fname = varargin{2};
        opt   = options({'sep', ';', 'date-format', 'dd/mm/yy HH:MM:SS.FFF', 'verbose', 1 }, ...
            varargin(3:end) );
        sep   = opt.get('sep');
        dt_format = opt.get('date-format');
        verb  = opt.get('verbose');
        st_log('Saving data <%s> to file <%s>...\n', ...
            data.title, fname);
        fod   = fopen( fname, 'w');
        % titre
        %         fprintf(fod, '%s\n', data.title);
        % noms de colonnes
        
        %         fprintf(fod, ['date' sep]);
        fprintf(fod, [ '%s' sep], data.colnames{:});
        fprintf(fod, '\n');
        for r=1:length(data.date)
            if verb>0 && r>1 && floor(data.date(r))>floor(data.date(r-1))
                st_log('.');
            end
            %             % date
            %             fprintf( fod, '%s;', datestr( data.date(r), dt_format));
            %             % valeurs
            
            for j = 1 :length(data.colnames)
                if isfield(data,'codebook') && any(ismember({data.codebook.colname},data.colnames(j)))
                    fprintf( fod, ['%s' sep], cell2mat(codebook('extract','ids2names',data.codebook,data.colnames{j},data.value(r,j))));
                else
                    fprintf( fod, ['%f' sep], data.value(r,j));
                end
            end
            fprintf( fod, '\n');
        end
        fclose( fod);
        st_log('done.\n');
        
    case { 'to_csv_analyse_perf'}
        %<* Save to csv
        data  = varargin{1};
        fname = varargin{2};
        opt   = options({'sep', ';', 'date-format', 'HH:MM:SS.FFF', 'verbose', 1 }, ...
            varargin(3:end) );
        sep   = opt.get('sep');
        dt_format = opt.get('date-format');
        verb  = opt.get('verbose');
        st_log('Saving data <%s> to file <%s>...\n', ...
            data.title, fname);
        fod   = fopen( fname, 'w');
        % titre
        fprintf(fod, '%s\n', data.title);
        % noms de colonnes
        if isfield( data, 'rownames');
            fprintf(fod, ['rowname' sep]);
            rows = data.rownames;
        else
            rows = {};
        end
        % nom des colnames time_ -> a transformer en datestr
        idx_time_=cellfun(@(c)(strcmp(c(1:min(10,length(c))),'time_diff_')),data.colnames);
        if ~isempty(idx_time_)
            % cr?e data_time_
            data_time_=st_data('empty-init');
            data_time_.date=data.date;
            data_time_.value=st_data('cols',data,find(idx_time_));
            data_time_.colnames=cellfun(@(c)(c(11:end)),data.colnames(idx_time_),'uni',false);
            % on modifie data
            data.value=data.value(:,~idx_time_);
            data.colnames=data.colnames(~idx_time_);
        else
            data_time_=[];
        end
        idx_security_id=cellfun(@(c)(strcmp(c(1:min(11,length(c))),'security_id')),data.colnames);
        if ~isempty(idx_security_id)
            % cr?e data_time_
            data_security_id=st_data('empty-init');
            data_security_id.date=data.date;
            data_security_id.value=st_data('cols',data,find(idx_security_id));
            data_security_id.colnames={'security_name'};
            % on modifie data
            data.value=data.value(:,~idx_security_id);
            data.colnames=data.colnames(~idx_security_id);
        else
            data_security_id=[];
        end
        
        
        if ~isempty(data_security_id)
            fprintf(fod, [ '%s' sep], data_security_id.colnames{:});
            fprintf(fod, [ '%s' sep], 'ric');
        end
        fprintf(fod, ['date' sep]);
        if ~isempty(data_time_)
            fprintf(fod, [ '%s' sep], data_time_.colnames{:});
        end
        fprintf(fod, [ '%s' sep], data.colnames{:});
        fprintf(fod, '\n');
        for r=1:length(data.date)
            if verb>0 && r>1 && floor(data.date(r))>floor(data.date(r-1))
                st_log('.');
            end
            % rowname
            if ~isempty(rows)
                
                fprintf( fod, [ rows{r} sep]);
            end
            % security_id
            if ~isempty(data_security_id)
                tmp_ric=get_ric_from_sec_id(data_security_id.value(r,1));
                fprintf(fod, [ '%s' sep], get_security_name(data_security_id.value(r,1)));
                fprintf(fod, [ '%s' sep], tmp_ric{1});
            end
            % date
            fprintf(fod, '%s;', datestr( data.date(r), dt_format));
            % valeurs sous forme date
            if ~isempty(data_time_)
                for i_time=1:length(data_time_.colnames)
                    fprintf( fod, '%s;', datestr( data_time_.date(r)+data_time_.value(r,i_time), dt_format));
                end
            end
            % valeurs
            fprintf( fod, ['%f' sep], data.value(r,:));
            fprintf( fod, '\n');
        end
        fclose( fod);
        st_log('done.\n');
        %>*
        
    case 'ope-save'
        %<* Save an operation list
        fname = varargin{1};
        [d,f] = fileparts( fname);
        if isempty( d)
            fname  = fullfile( getenv( 'st_repository'), fname);
        end
        datas  = varargin{2};
        z = [];
        z.titles = cellfun(@(c)c.title, datas, 'uni', false);
        if length(z.titles) ~= length(unique(z.titles))
            error('read_dataset:ope_save:title', 'titles must be different');
        end
        z.ope_names = tokenize( sprintf('ope_%d;', 1:length(z.titles)), ';');
        for c=1:length(datas)
            z.(z.ope_names{c}) = datas{c};
        end
        save( fname, '-struct', 'z');
        %>*
        
    case 'ope-add'
        %<* Add a dataset
        fname = varargin{1};
        [d,f] = fileparts( fname);
        if isempty( d)
            fname  = fullfile( getenv( 'st_repository'), fname);
        end
        datas  = varargin{2};
        z = [];
        u = load(fname, 'titles');
        z.titles = cat(2, u.titles, cellfun(@(c)c.title,datas,'uni',false));
        if length(z.titles) ~= length(unique(z.titles))
            error('read_dataset:ope_save:title', 'titles must be different');
        end
        u = load(fname, 'ope_names');
        ope_names = u.ope_names;
        ope_max = max(str2num( strrep( [ope_names{:}], 'ope_', ';')));
        ope_new = tokenize( sprintf('ope_%d;', ope_max+1:length(z.titles)), ';');
        z.ope_names = cat(2, ope_names, ope_new);
        for n=ope_max+1:length(z.titles)
            z.(z.ope_names{n}) = datas{n-ope_max};
        end
        save( fname, '-struct', 'z', '-append');
        %>*
        
    case 'ope-load'
        %<* Load an operation list
        % Or only some operations
        fname = varargin{1};
        [d,f] = fileparts( fname);
        if isempty( d)
            fname  = fullfile( getenv( 'st_repository'), fname);
        end
        if nargin<3
            %< Load all
            u = load(fname, 'ope_names');
            ope_names = u.ope_names;
            %>
        else
            %< load only some of them
            sub_ope = varargin{2};
            u = load(fname, 'titles');
            idx = cellfun(@(c)strmatch(c,u.titles), sub_ope, 'uni', false);
            idx(cellfun(@isempty, idx)) = [];
            idx = [idx{:}];
            if isempty( idx)
                data = {};
                return
            end
            u = load(fname, 'ope_names');
            ope_names = u.ope_names(idx);
            %>
        end
        data = cell(1, length(ope_names));
        for c=1:length(ope_names)
            u = load(fname, ope_names{c});
            data{c} = u.(ope_names{c});
        end
        %>*
        
    case 'from-struct'
        % le mode from cell est il inutile, ie equivalent ? st_data?cell2mat
        data = st_data('empty-init');
        data.title= 'from_struct';
        struct_= varargin{1};
        if length(varargin)> 2
            untouched= varargin{3};
            if ~iscell(untouched);untouched={untouched}; end
            for u= (untouched(:)')
                data.(u{1})= struct_.(u{1});
                struct_= rmfield(struct_,u{1} );
            end
        end
        data.value= cell2mat(struct2cell(struct_)');
        data.colnames= fieldnames(struct_)';
        i= find(strcmp(data.colnames,varargin{2}));
        assert(length(i)==1);
        data.date= data.value(:,i);
        data.value(:,i)=[];
        data.colnames(:,i)=[];
        
    case 'from-cell'
        title_   = varargin{1};
        colnames = varargin{2};
        vals     = varargin{3};
        if size(colnames, 2) ~= size( vals,2)
            error('st_data:fom_cell', 'dimension of colnames and value not compatible (%d=/=%d)', ...
                size(colnames, 2), size( vals,2));
        end
        % remove empty colnames
        idx = find(cellfun(@(c)~ischar(c),colnames));
        colnames(:,idx) = [];
        vals(:,idx) = [];
        % numeric part
        idx_num  = cellfun(@isnumeric, vals(1,:));
        num_data = struct('value', cell2mat(vals(:,idx_num)), 'colnames', { colnames(idx_num)});
        % txt part
        idx_txt  = find(~idx_num);
        txt_vals = [];
        for t=1:length(idx_txt)
            this_colname = colnames{idx_txt(t)};
            [this_txt_num, this_cdk] = codebook('build', vals(:,idx_txt(t)));
            this_cdk.colname = this_colname;
            if t==1
                cdk = this_cdk;
            else
                cdk(end+1) = this_cdk;
            end
            txt_vals = cat(2, txt_vals, this_txt_num);
        end
        % final signal
        len_vals = size(vals,1);
        clear varargin vals
        data = st_data('init', 'title', title_ , ...
            'value', [txt_vals, num_data.value], ...
            'colnames', cat(2, colnames(idx_txt), num_data.colnames) , ...
            'date', (1:len_vals)', ...
            'codebook', cdk);
        
    case 'sql'
        base      = varargin{1};
        table     = varargin{2};
        request   = varargin{3};
        opt       = options({'date-format', 'yyyy-mm-dd HH:MM:SS'}, varargin(4:end));
        reqnostar = strrep( request,'*','');
        vals      = exec_sql(base, sprintf('select %s from %s', reqnostar, table));
        colnames  = tokenize(reqnostar, ',');
        % extract dates
        colnameso = tokenize(request, ',');
        dtcol     = find(cellfun(@(s)s(end)=='*',colnameso));
        if ~isempty(dtcol)
            st_log('st_data:sql extracting dates...\n');
            colnames(dtcol) = [];
            dts = datenum( vals(:,dtcol),opt.get('date-format'));
            vals(:,dtcol) = [];
        else
            dts = (1:size(vals,1))';
        end
        % extract numerics
        nbcols    = cellfun(@isnumeric, vals(1,:));
        colnames_ = colnames(:,nbcols);
        values    = cellfun(@double, vals(:,nbcols));
        % others
        txcols    = find(~nbcols);
        cdkb      = [];
        L         = length(txcols);
        if L>0
            st_log('st_data:sql working on codebooks...\n');
        end
        for t=1:L
            this_col = txcols(t);
            try
                [v_, cdk]   = codebook('build', vals(:,this_col));
                values      = cat(2, values, v_);
                cdk.colname = colnames{this_col};
                if isempty( cdkb)
                    cdbk = cdk;
                else
                    cdbk(end+1) = cdk;
                end
                colnames_ = cat(2, colnames_, colnames{this_col});
            catch er
                st_log('st_data:sql problem converting <%s>:%s\n', colnames{this_col}, er.message);
            end
        end
        data = st_data('init', 'title', [ base ':' table], ...
            'value', values, 'date', dts, 'colnames', colnames_, 'codebook', cdbk);
        
    case {'from-rectangle', 'from-matrix'}
        %<* G?n?re un signal depuis une matrice
        opt  = options({'title', 'New dataset', 'date', '', 'time', '', 'colnames', {}}, varargin(2:end));
        vals = varargin{1};
        clear varargin;
        dt   = opt.get('date');
        tm   = opt.get('time');
        colnames = opt.get('colnames');
        nb_cols = size(vals,2)-(~isempty(dt)+~isempty(tm));
        if length(colnames)~=nb_cols
            if ~isempty(colnames)
                warning('st_data:from_rectangle', 'problem with colnames size, reinit');
            end
            colnames = tokenize( sprintf('V-%d;', 1:nb_cols),';');
        end
        %< Cr?ation de la date
        if ~isempty(dt)
            dt_col = str2num(regexprep(dt, '{([^}]+)}[^}]*', '$1'));
            dt_frm = regexprep(dt, '{[^}]+}([^}]+)', '$1');
            if isempty(dt_frm)
                % ? tester
                dts = vals(:,dt_col);
            else
                dts = datenum( vals(:,dt_col), dt_frm);
            end
        else
            dt_col = [];
            dts    = repmat(0, size(vals,1),1);
        end
        if ~isempty(tm)
            tm_col = str2double(regexprep(tm, '{([^}]+)}[^}]*', '$1'));
            tm_frm = regexprep(tm, '{[^}]+}([^}]+)', '$1');
            if isempty(tm_frm)
                % ? tester
                dts = dts + vals(:,tm_col);
            else
                dts = dts + mod(datenum( vals(:,tm_col), tm_frm),1);
            end
        else
            tm_col = [];
        end
        if all(abs(dts)<eps)
            dts = (1:size(vals,1))';
        end
        %>
        vals(:,[dt_col, tm_col])=[];
        if iscell(vals)
            vals = cellfun(@(x)double(x),vals);
        end
        data = st_data( 'init', 'title', opt.get('title'), 'value', vals, 'date', dts, 'colnames', colnames);
        %>*
        
    case { 'init', 'create', 'make'}
        %<* Fabrication d'une structure de donn?es data
        data = [];
        for i=1:2:length(varargin)-1
            % pour les colnames j'ajoute une accolade s'il n'y en a qu'une
            if strcmp(varargin{i}, 'colnames') && ischar( varargin{i+1})
                varargin{i+1} = { varargin{i+1} };
            end
            data.(varargin{i}) = varargin{i+1};
        end
        if ~isfield(data, 'title')
            data.title = '';
        end
        clear varargin;
        if ~st_data( 'check', data)
            error('st_data:check', 'Check failed at initialization');
        else
            data = st_data('log', data, 'dini');
        end
        %>*
        
    case 'check'
        % Is this really a st_data?
        data_in = varargin{1};
        do_raise= false;
        if length(varargin)>1 && varargin{2}
            data= st_data('check',data_in);
            if ~data; assert(false,'st_data check failed');end
            return
        end
        % 3rd argument <=> raises error if check fails
        
        clear varargin;
        data = false; % for early returns, an early returns in this function is necessarily due to failing the test
        %< Are mandatory fields here?
        ok = all(isfield(data_in, {'title', 'value', 'date', 'colnames'}));
        if ~ok
            return;
        end
        %>
        %< Typage
        ok = ischar(data_in.title);
        ok = ok && (isnumeric(data_in.value) || islogical(data_in.value) );
        ok = ok && isnumeric(data_in.date);
        ok = ok && (isempty(data_in.colnames) || iscellstr(data_in.colnames));
        if ~ok
            return;
        end
        %>
        %< Dim check
        val_sz = size(data_in.value);
        date_sz = size(data_in.date);
        date_nel = numel(data_in.date);
        coln_sz = size(data_in.colnames);
        coln_sz = [coln_sz(2),coln_sz(1),coln_sz(3:end)];
        if coln_sz(end)==1 || coln_sz(end)==0
            coln_sz(end) = [];
        end
        if val_sz(1) ~= date_sz(1)
            ok = false;
            warning('st_data:size', 'nb rows =/= nb dates');
        end
        if date_nel > date_sz(1)
            ok = false;
            warning('st_data:size', 'dates should be a col vector');
        end
        if ~isequal(coln_sz,val_sz(2:end))
            ok = false;
            warning('st_data:size', 'Colnames size does not "match" value size');
        end
        %>
        data = ok;
        
    case 'transpose'
        %<* Transposition
        data = varargin{1};
        if isfield(data, 'info')
            info = data.info;
        else
            info = [];
        end
        if ~isfield( info, 'transposed')
            info.transposed = struct('date', []);
        end
        % valeurs: le plus facile
        data.value = data.value';
        % dates  : ?nervant
        dts        = data.date;
        if ~isempty( info.transposed.date)
            data.date = info.transposed.date;
            rownames = {};
        else
            data.date = (1:size(data.value,1))';
            % rownames
            rownames  = data.colnames;
        end
        if isfield( data, 'rownames')
            % il y a un rownames
            colnames  = data.rownames;
        elseif min(dts)>datenum(1970,04,19)
            % dates
            colnames  = cellstr( datestr( dts, 'yyyy/mm/dd HH:MM:SS.FFF'));
        elseif all(dts==floor(dts))
            % entiers
            colnames  = tokenize( sprintf('%d;', dts), ';');
        else
            % floats
            colnames  = tokenize( sprintf('%f;', dts), ';');
        end
        
        if isempty( info.transposed.date)
            info.transposed.date = dts;
        else
            info = rmfield( info, 'transposed');
        end
        if ~isempty( rownames)
            data.rownames = rownames;
        else
            data = rmfield( data, 'rownames');
        end
        
        data.colnames = colnames;
        if ~isempty( info)
            data.info = info;
        end
        %>*
        
    case { 'keep-cols', 'keep-columns', 'keep'}
        %<* Conserver certaines colonnes
        data     = varargin{1};
        my_names = varargin{2};
        if ~isempty(my_names)
            if isnumeric( my_names) || islogical(my_names)
                data.colnames = data.colnames(my_names);
                data.value    = data.value(:,my_names);
                return
            end
            if ~iscell( my_names)
                my_names = tokenize( my_names, ';');
            end
            my_names = GREP_NAMES( my_names, data.colnames);
            
            idx = nan(length(my_names),1);
            for n=1:length(my_names)
                this_idx = strmatch( lower(my_names{n}), lower(data.colnames), 'exact');
                if isempty(this_idx)
                    error('The column <%s> is not present in yout data', my_names{n});
                elseif length(this_idx) > 1
                    this_idx = this_idx(1);
                    warning(['There are several columns named <%s> in your st_data. ' ...
                        'I''ll take the first one. '], my_names{n});
                end
                idx(n)=this_idx;
            end
            data.colnames = my_names;
            data.value = data.value(:,idx);
        end
        %>*
        
    case { 'remove', 'rm', 'drop'}
        %<* Retirer certaines colonnes
        data     = varargin{1};
        my_names = varargin{2};
        if ~iscell( my_names)
            my_names = tokenize( my_names, ';');
        end
        my_names = GREP_NAMES( my_names, data.colnames);
        
        idx = cellfun(@(c)strmatch( lower(c), lower(data.colnames), 'exact'), my_names, 'uni', false);
        idx = [idx{:}];
        data.value(:, idx) = [];
        data.colnames(:, idx) =[];
        if isfield(data,'codebook') && ~isempty(data.codebook)
            id_in_my_names = cellfun(@(c)ismember( c, my_names), {data.codebook.colname});
            if any(id_in_my_names)
                data.codebook(id_in_my_names)=[];
            end
        end
        %>*
        
    case { 'cols', 'columns', 'col'}
        %<* Retourne le vecteur colonne
        data     = varargin{1};
        if iscell(data) && length(data) == 1 && (isstruct(data{1}) || isempty(data{1}))
            data = data{1};
            if isempty(data)
                data = st_data('empty-init');
            end
        end
        % Commentaire [malas]: la reconfiguration op?r?e ci-dessous n'a pas lieu
        % d'?tre. Je l'imbrique dans un bloc "if ... end" pour ?viter de tout casser
        % si "data.value" est de dimension sup?rieure ou ?gale ? 3.
        if size(data.value,1)*length(data.colnames)==numel(data.value)
            data.value = reshape(data.value,size(data.value,1),length(data.colnames));
        end
        
        my_names = varargin{2};
        idx0 = [];
        if length(varargin)>2
            idx = varargin{3};
        else
            idx = [];
        end
        if isnumeric( my_names)
            data = data.value(:,my_names);
            if ~isempty(idx)
                data = data(idx,:);
            end
            %data.colnames = data.colnames(my_names);
            return
        end
        if ~iscell( my_names)
            my_names = tokenize( my_names, ';');
        end
        my_names = GREP_NAMES( my_names, data.colnames);
        
        vals = nan(size(data.date,1),length(my_names));
        for n=1:length(my_names)
            this_name = my_names{n};
            if this_name(1)=='.'
                str = this_name(2:end);
                flds = tokenize( str, '.');
                vlds = data;
                for f=1:length(flds)
                    vlds = vlds.(flds{f});
                end
                vals(:,n) = vlds;
            else
                this_idx = find(strcmpi( this_name, permute(data.colnames,[2,1,3:ndims(data.colnames)])));
                if isempty(this_idx)
                    error('The column <%s> is absent of your data', this_name);
                elseif length(this_idx) > 1
                    this_idx = this_idx(1);
                    warning(['There are several columns named <%s> in your st_data. ' ...
                        'I''ll take the first one. '], this_name);
                end
                idx0 = [idx0, this_idx];
                vals(:,n)  = data.value(:, this_idx);
            end
        end
        data = vals;
        if ~isempty( idx)
            data = vals(idx,:);
        else
            idx = idx0;
        end
        %>*
        
    case 'col_cdk'
        %<* Retourne le vecteur colonne au format codebook
        
        %------------------------------------------------------------------
        %---- EXTRACT INPUTS
        %------------------------------------------------------------------
        data = varargin{1};
        my_names = varargin{2};
        if length(varargin)>2
            idx = varargin{3};
        else
            idx = [];
        end
        if isnumeric(my_names)
            error('st_data:col_cdk', 'varargin{2} can not be numbers');
        end
        if ~iscell(my_names)
            my_names = tokenize( my_names, ';');
        end
        my_names = GREP_NAMES( my_names, data.colnames);
        
        %------------------------------------------------------------------
        %---- TESTS
        %------------------------------------------------------------------
        if ~isfield(data,'codebook') && ~codebook('iscodebook',data.codebook)
            error('st_data:col_cdk', 'codebook is not valid');
        end
        
        %------------------------------------------------------------------
        %---- CREATE OUTPUTS
        %------------------------------------------------------------------
        if ~isempty( idx)
            data=st_data('from-idx',data,idx);
        end
        vals = cell(size(data.date,1),length(my_names));
        for n=1:length(my_names)
            this_name = my_names{n};
            this_idx = strmatch( lower(this_name), lower(data.colnames), 'exact');
            vals(:,n) =  codebook('extract','ids2names',data.codebook,this_name,data.value(:,this_idx));
        end
        data = vals;
        %>*
        
    case 'from-idx'
        %<* extract idx
        data  = varargin{1};
        idx   = varargin{2};
        clear varargin;
        data.value=data.value(idx,:);
        data.date=data.date(idx);
        if isfield(data, 'rownames')
            data.rownames = data.rownames(idx);
        end
        %>*
        
    case 'extract-day'
        %<* Extract a given day
        opt  = options({'format', 'dd/mm/yyyy'}, varargin(3:end));
        data = varargin{1};
        day  = varargin{2};
        if ischar( day)
            day = datenum( day, opt.get('format'));
        end
        idx = abs(floor(data.date)-day)>eps;
        data.date(idx,:) = [];
        data.value(idx,:) = [];
        %>*
        
    case 'explode'
        %<* split a st_data and different st_data
        original_data  = varargin{1};
        % we will split the st_data by respect to this column
        % we will create a single st_data for each differnt value of this
        % column
        explode_column = varargin{2};
        all_values=  st_data('col',original_data, explode_column);
        all_unique_values = unique(all_values);
        nb_output = length(all_unique_values );
        st_log('Splitting the st_data in %d st_data ...',nb_output);
        data =  cell(nb_output,1);
        for i = 1 : nb_output
            data{i} = st_data('from-idx', original_data,all_values == all_unique_values(i));
        end
        %>*
        
    case 'accum'
        %<* Acumarray for st data
        data = varargin{1};
        colnames = varargin{2};
        opt = options({ 'formula', {}, 'f_colnames', []}, varargin(3:end));
        formula = opt.get('formula');
        if ~isempty( formula)
            data = st_data('apply-formula', data, formula{:});
        end
        vals = st_data('col', data, colnames);
        L = size(vals,2)-1;
        if L>2
            error('st_data:accum', 'I can only work with less than 2 conditionning variables, here you asked for %d', L);
        end
        dico = repmat({[]},1,L);
        cond = repmat(nan,length(data.date),L);
        for c=1:size(vals,2)-1
            [u, tmp, v] = unique(vals(:,c));
            dico{c} = u;
            cond(:,c) = v;
        end
        o_colnames = dico{2}';
        f_colnames = opt.get('f_colnames');
        if ~isempty( f_colnames)
            colnames = feval( f_colnames, o_colnames);
        else
            colnames = o_colnames;
        end
        data = st_data('init', 'title', data.title, ...
            'value', accumarray(cond, vals(:,end)), 'date', dico{1}, ...
            'colnames', colnames, 'info', struct('o_colnames', o_colnames) );
        %>*
        
    case 'iday2array'
        %<* intra day 2 array
        half_sec_dt = 5.78703703703704e-006;
        data = varargin{1};
        if ~isempty(strmatch( 'stop_auction', data.colnames, 'exact'))
            data = st_data('where', data, '~{stop_auction}');
        end
        dat_ = rmfield( data, { 'value', 'date', 'colnames', 'title' });
        [days, tmp, ddx] = unique( floor( data.date));
        [tms , tmp, tdx] = unique( round(mod( data.date, 1) / half_sec_dt) * half_sec_dt);
        if length(tms)*length(days) ~= length(data.date)
            warning('st_data:iday2array:check_args', 'Your data does not have the same number of record in each day');
        end
        dat_.date = tms;
        if isfield( dat_, 'info')
            info = dat_.info;
        else
            info = [];
        end
        info.days = days;
        dat_.info = info;
        dat_.colnames = cellstr(datestr( days, 'dd/mm/yyyy'))';
        rez  = repmat( {dat_}, 1, length(data.colnames));
        for c = 1:length(data.colnames)
            rez{c}.value = accumarray([tdx, ddx], data.value(:,c));
            rez{c}.title = [ data.title ' - ' data.colnames{c}];
        end
        data = rez{1};
        if length(rez)>1
            idx  = rez{2};
        else
            idx = [];
        end
        varargout = rez(3:end);
        if nargin>2
            i = varargin{2};
            if ischar(i)
                cnames = cellfun(@(c)regexprep(c.title, '.*- ([^-])+','$1'),rez,'uni',false);
                i = strmatch(lower(i), lower(cnames), 'exact');
            end
            data = rez{i};
            idx  = [];
        end
        %>*
        
    case 'each-row-formula'
        %<* Appliquer une formule ? chaque rowname unique
        data         = varargin{1};
        my_formula   = varargin{2};
        if nargin>3
            colnames = varargin{3};
            if ischar( colnames)
                colnames = tokenize( colnames, ';');
            end
        else
            colnames = {};
        end
        
        real_formula = regexprep(my_formula, ...
            '{([^}]+)}', 'datar.value(:,strmatch(''$1'', data.colnames, ''exact''))');
        [urows, tmp, udx] = unique( data.rownames);
        vals = [];
        rows = {};
        for u=1:length(urows)
            idx = u==udx;
            datar = st_data('from-idx', data, idx);
            v = [];
            eval(['v = ' real_formula ';']);
            dts = datar.date;
            dts(1:size(dts,1)-size(v,1)) = [];
            vals = [vals; dts, v];
            if isempty(colnames)
                colnames = tokenize(sprintf('formula-%d;', 1:size(v,2)), ';');
            end
            rows = cat(1, rows, repmat(urows(u), size(v,1), 1));
        end
        data.value    = vals(:,2:end);
        data.date     = vals(:,1);
        data.colnames = colnames;
        data.rownames = rows;
        %>*
        
    case 'get-rows'
        %<* Get rownames
        idata = varargin{1};
        [urows, tmp, udx] = unique(idata.rownames);
        odata = repmat({[]}, length(urows), 1);
        for u=1:length(urows)
            idx = (u==udx);
            odata{u} = st_data('from-idx', idata, idx);
            odata{u}.title = sprintf('%s - %s', idata.title, urows{u});
        end
        if length(varargin)>1
            rname = varargin{2};
            if ischar( rname)
                rname = strmatch( rname, urows, 'exact');
            end
            data = odata{rname};
            idx = [];
            varargout = {};
        else
            data = odata{1};
            idx  = odata{2};
            varargout = odata(3:end);
        end
        %>*
        
    case { 'rows-to-cols', 'rows2cols', 'r2c' }
        %<* Get rownames
        data = varargin{1};
        [urows, tmp, udx] = unique(data.rownames);
        [days,  tmp, ddx] = unique(data.date);
        vals = [];
        colnames = {};
        for c=1:length(data.colnames)
            cals = st_data('col', data, c);
            vals = [vals, full(sparse(ddx, udx, cals))];
            colnames = cat(1, colnames, tokenize(sprintf([ '%s - ', data.colnames{c} ';'], urows{:}), ';'));
        end
        data.value = vals;
        data.date  = days;
        data.colnames = colnames;
        data = rmfield( data, 'rownames');
        %>*
        
    case 'apply-formula'
        %<* Appliquer une formule
        [data, b]= st_data('isempty', varargin{1});
        if b
            opt = options({'output', 'st_data'}, varargin(3:end));
            switch lower(opt.get('output'))
                case 'st_data'
                    % rien
                case 'value'
                    data = [];
                otherwise
                    error('st_data:apply_formula', 'unknown output mode <%s>', opt.get('output'));
            end
            return
        end
        data.colnames = lower(data.colnames);
        my_formula = lower(varargin{2});
        col_asked = regexprep(regexp(my_formula,'{([^}]+)}','match'),'({|})','');
        if ~all(ismember(col_asked,data.colnames))
            unknown_col = unique(col_asked(~ismember(col_asked,data.colnames)));
            error('st_data:apply_formula','unknown colname <%s>',regexprep(sprintf('%s;',unknown_col{:}),';$',''));
        end
        opt = options({'output', 'st_data'}, varargin(3:end));
        % {B1} - {B2}
        % data.value(:,1) - data.value(:,2)
        % st_data('cols', data, 'B1') - st_data('cols', data, 'B2')
        real_formula = regexprep(my_formula, ...
            '{([^}]+)}', 'data.value(:,strmatch(''$1'', data.colnames, ''exact''))');
        v = eval(real_formula);
        data.value = v;
        switch lower(opt.get('output'))
            case 'st_data'
                % rien
            case 'value'
                data = data.value;
            otherwise
                error('st_data:apply_formula', 'unknown output mode <%s>', opt.get('output'));
        end
        
        if isfield(data, 'colnames') && ~isempty(data.colnames)
            if nargin>3
                colnames = varargin{3};
                if ischar( colnames)
                    colnames = tokenize( colnames, ';');
                end
            else
                try
                    if my_formula(1) == '[' && my_formula(end) == ']'
                        %                         colnames = cellflat(regexp(my_formula(2:end), '{(\w+)}', 'tokens'));
                        colnames = strtrim(tokenize(regexprep(my_formula(2:end-1), '{|}', ''), ','));
                        %                         colnames = tokenize(strrep(my_formula(2:end)));
                        if length(colnames) ~= size(data.value, 2)
                            error('st_data:apply_formula', 'go to catch 1');
                        end
                    else
                        error('st_data:apply_formula', 'go to catch 2');
                    end
                catch
                    colnames = {my_formula};
                    if length(colnames)~=size(data.value,2)
                        colnames = tokenize( sprintf( [ my_formula '[%d];'], 1:size(data.value,2)), ';');
                    end
                end
            end
            data.colnames = colnames;
        end
        if (strcmpi(opt.get('output'),'st_data'))
            D = size(data.value,1);
            d = size(data.date,1);
            if D ~= d
                if d<D
                    st_log('st_data:apply_formula - number of rows (%d) > number fo date (%d) I will generate an empty st_datas\n', D, d);
                    data.value = repmat(nan,0,length(data.colnames));
                else
                    st_log('st_data:apply_formula - number of rows (%d) =/= number fo date (%d) I will take the last dates\n', D, d);
                    data.date = data.date(end-D+1:end);
                end
            end
            if isfield(varargin{1}, 'info')
                data.info = varargin{1}.info;
            end
        else
            %output value
        end
        %>*
        
    case 'sort'
        %<* Trier
        data = varargin{1};
        colname = varargin{2};
        if nargin<4
            order = 'ascend';
        else
            order = varargin{3};
        end
        if colname(1)=='.'
            vals = data.(colname(2:end));
        else
            vals = st_data('col', data, colname);
        end
        [tmp, idx] = sort( vals, order);
        data.value = data.value(idx,:);
        data.date  = data.date(idx);
        if isfield( data, 'rownames')
            data.rownames = data.rownames(idx);
        end
        if isfield( data, 'labels')
            data.labels = data.labels(idx);
        end
        %>*
        
    case 'plot'
        %<* Affichage d?di?
        data = varargin{1};
        data = st_plot(data );
        %>*
        
    case 'xls'
        %<* Read from xls file
        fname = varargin{1};
        sheet = varargin{2};
        range = varargin{3};
        opt   = options({'colnames', '', 'date', '', 'rownames', '', 'date-format', 'dd/mm/yyyy'}, varargin(4:end));
        
        %< extract
        [vals, txt, raw] = xlsread( fname, sheet, range);
        %>
        
        %< colnames
        colnames = opt.get('colnames');
        if isempty(colnames)
            colnames = tokenize(sprintf('V-%d', 1:size(vals,2)),';');
        elseif colnames(1)==':'
            [v, colnames] = xlsread( fname, sheet, colnames(2:end));
        else
            colnames = tokenize(colnames,';');
        end
        %>
        
        %< construction ?ventuelle d'un codebook
        codebook_=[];
        if ~isempty(txt)
            % il va falloir coder tout cela
            vals_ = NaN(size(raw));
            v = 1;
            for c=1:size(raw,2)
                if all(cellfun(@(c)(ischar(c)),raw(:,c)))
                    % c'est un caract?re
                    cdk_=codebook('new',unique(raw(:,c)),colnames{c}); %previously: txt, now: raw (stpel)
                    codebook_=codebook('stack',codebook_,cdk_);
                    vals_(:,c) = codebook('extract','names2ids',codebook_,colnames{c},raw(:,c)); %previously: txt, now: raw (stpel)
                elseif all(cellfun(@(c)(isnumeric(c)),raw(:,c)))
                    % c'est un num?rique
                    vals_(:,c) = cellfun(@(x)x,raw(:,c));
                else
                    % numerique et char...on met tout en char
                    tmp_vals_=raw(:,c);
                    tmp_vals_(cellfun(@(c)(isnumeric(c)),tmp_vals_))=cellfun(@(c)(num2str(c)),tmp_vals_(cellfun(@(c)(isnumeric(c)),tmp_vals_)),'uni',false);
                    cdk_=codebook('new',unique(tmp_vals_),colnames{c});
                    codebook_=codebook('stack',codebook_,cdk_);
                    vals_(:,c) = codebook('extract','names2ids',codebook_,colnames{c},tmp_vals_);
                end
            end
            vals = vals_;
        end
        %>
        
        rownames = opt.get('rownames');
        if ~isempty( rownames)
            [v, rownames] = xlsread(fname, sheet, rownames);
        end
        dts = opt.get('date');
        if ~isempty( dts)
            [dts, txt] = xlsread(fname, sheet, dts);
            if isempty( dts)
                try
                    dts = datenum(txt, opt.get('date-format'));
                catch
                    lasterr('');
                end
            end
        end
        if isempty( dts)
            dts = (1:size(vals,1))';
        end
        [p, ti] = fileparts( fname);
        data = st_data('init', 'title', ti, 'value', vals, 'colnames', colnames, ...
            'date', dts, 'codebook', codebook_);
        if ~isempty( rownames)
            data.rownames = rownames;
        end
        %>*
        
    case 'add-col'
        %<* Add a column
        data = varargin{1};
        vals = varargin{2};
        coln = varargin{3};
        if ~iscellstr(coln)
            new_colnames = tokenize(coln,';');
        else
            new_colnames=coln;
        end
        %<* Suppot for adding a column of text or numeric based data
        
        if iscell(vals)
            nb_col = size(vals,2);
            idx_txt = false(1,nb_col);
            %<* detect text columns
            for c = 1 : nb_col
                idx_txt(c) = all(cellfun(@isempty, vals(:, c))) || sum(cellfun(@ischar, vals(:,c)))> 0;
                % replace empty values by empty strings
                vals(cellfun(@isempty,vals(:,c)),c) = {''};
                
                if idx_txt(c) && ~iscellstr(vals(:,c))
                    %<* there is some mixed data in a column , raise an error
                    error('st_data:add-col', 'text columns must contain only text (see cellstr)');
                    return;
                end
                
            end
            
            idx_num = ~idx_txt;
            num_data = struct('value', cell2mat(vals(:,idx_num)), 'colnames', { new_colnames(idx_num)});
            
            % txt part
            indices_txt  = find(idx_txt);
            txt_vals = [];
            
            use_codebook = isfield(data, 'codebook');
            if use_codebook
                cdk = data.codebook;
            end
            %<* create codebook
            
            for t=1:length(indices_txt)
                this_cdk=codebook('new',vals(:,indices_txt(t)),new_colnames{indices_txt(t)});
                this_txt_num=codebook('extract','names2ids',this_cdk,new_colnames{indices_txt(t)},cellfun(@strtrim, vals(:,indices_txt(t)),'uni', false));
                if t==1 && ~use_codebook
                    cdk = this_cdk;
                else
                    cdk(end+1) = this_cdk;
                end
                txt_vals = cat(2, txt_vals, this_txt_num);
            end
            infos =data.info;
            has_rownames = isfield(data, 'rownames');
            if has_rownames
                rownames = data.rownames;
            end
            
            data = st_data('init', 'title', data.title , ...
                'value', [data.value,txt_vals, num_data.value], ...
                'colnames', cat(2,data.colnames, new_colnames{idx_txt}, num_data.colnames) , ...
                'date', data.date, ...
                'codebook', cdk);
            
            data.info = infos;
            if has_rownames
                data.rownames = rownames;
            end
            
        else
            data.colnames = cat(2,data.colnames,new_colnames);
            data.value = [data.value, vals];
        end
        %>*
        
    case 'modif-cols'
        %<* Add a column
        data = varargin{1};
        if st_data('isempty-nl',data)
            return
        end
        colnames2change = varargin{2};
        if ischar(colnames2change)
            colnames2change=tokenize(colnames2change,';');
        end
        opt = options_light({'names','','values',[]}, varargin(3:end));
        
        %<- change the names
        if ~isempty(opt.names)
            names=opt.names;
            if ischar(names)
                names=tokenize(names,';');
            end
            [id_s idx_in]=ismember(colnames2change,data.colnames);
            % tests
            if length(names)~=length(colnames2change) || ...
                    ~all(id_s) || ...
                    any(ismember(names,data.colnames))
                error('st_data:modif-cols','bad inputs');
            end
            % change colnames
            data.colnames(idx_in(id_s))=names(id_s);
            % change codebook colnames
            if isfield(data,'codebook') && ~isempty(data.codebook)
                colnames_cdk={data.codebook.colname};
                [id_s idx_in]=ismember(colnames2change,colnames_cdk);
                if any(id_s)
                    idx_s=find(id_s);
                    for i_=1:length(idx_s)
                        data.codebook(idx_in(idx_s(i_))).colname=names{idx_s(i_)};
                    end
                end
            end
            
        end
        
        %<- change the value
        if ~isempty(opt.values)
            error('st_data:modif-cols','STILL TO CODE');
        end
        %>*
        
    case 'nonan-rows'
        %<* remove nan rows
        data = varargin{1};
        idx = any(isnan(data.value),2);
        data.value(idx,:)=[];
        data.date(idx)=[];
        %>*
        
    case 'where'
        %<* select rows where
        data = varargin{1};
        crit = varargin{2};
        clear varargin
        sel  = st_data('apply-formula', data, crit, 'where');
        idx  = logical(sel.value);
        clear sel
        data = st_data('from-idx', data, idx);
        %>*
        
    case 'stack'
        %<* Stack two signals
        sort_date = true;
        idx_sd = cellfun(@(c) ischar(c), varargin);
        if ~isempty(find(idx_sd, 1))
            sort_date = varargin{find(idx_sd)+1};
        end
        idx_sd(find(idx_sd)+1) = 1;
        varargin = varargin(~idx_sd);
        
        if length(varargin)==1
            data = varargin{1};
            return
        end
        if length(varargin) > 2
            data = varargin{1};
            for n=2:length(varargin)
                data = st_data('stack', data, varargin{n}, 'sort_date',sort_date);
            end
            return
        end
        data1 = varargin{1};
        data2 = varargin{2};
        if st_data('isempty-nl', data1)
            data  = data2;
        elseif st_data('isempty-nl', data2)
            data = data1;
        else
            names = sprintf('%s;', data1.colnames{:});
            data  = st_data('keep', data2, names(1:end-1));
            data.value    = [data.value; data1.value];
            data.date     = [data.date ; data1.date ];
            if isfield(data, 'rownames') && isfield(data1, 'rownames')
                data.rownames = cat(1, data.rownames, data1.rownames);
            end
            if sort_date
                data = st_data('sort', data, '.date');
            end
        end
        %>*
        
    case 'extended_stack'
        % !!! BEWARE !!!
        % !!!! Ce code peut changer a tout moment  !!!!
        % !!!! ONLY NIJOS USER CAN USE IT !!!!
        
        %<* Stack cell arrays of st_data
        % 1/ no merge of rows at all !!!
        %    -> after the stack the number of rows is the sum of the number of rows of each st_data
        %    -> output may have similar .date
        % 2/ merge of colnames !!!
        % Same colnames in several st_data -> in the output : stacked in same colnames
        % 3/ take account of codebooks !
        % 4/ take account of .info.localtime and .info.place_timezone_id, in order to correct the .date fields
        % 5/ TO DO : handle the rest of the .info
        
        %------------------------------------------------------------------
        %--- EXAMPLES
        %------------------------------------------------------------------
        % bad examples...
        % dataLSE=read_dataset('tick4simap','security_id',12058,'from','04/05/2011','to','04/05/2011','trading-destinations', {});
        % dataCAC=read_dataset('tick4simap','security_id',110,'from','03/05/2011','to','04/05/2011','trading-destinations', {});
        % test=st_data('extended_stack',{dataLSE,dataCAC});
        
        %------------------------------------------------------------------
        %--- INTRO
        %------------------------------------------------------------------
        
        %------------------------
        %--- Inputs handle
        %------------------------
        datas = varargin{1};
        if ~iscell(datas)
            error('st_data:extended_stack', 'varargin{1} has to be a cell array of st_data');
        end
        idx_e = cellfun(@(c)st_data('isempty-nl', c), datas);
        datas(idx_e) = [];
        
        %------------------------
        %--- handle when cell of st_data >=3
        %------------------------
        if length(datas)<=1
            if isempty(datas)
                data = [];
            else
                data=datas{1};
            end
            return
        elseif length(datas)>=3
            while length(datas)>=3
                data = st_data('extended_stack',datas(1:2));
                datas{1}=data;
                datas(2)=[];
            end
        end
        
        %------------------------------------------------------------------
        %--- COMPUTATION : handle when cell of st_data==2
        %------------------------------------------------------------------
        data1 = datas{1};
        data2 = datas{2};
        
        %------------------------
        % - Handle .info
        %------------------------
        [data_info data1 data2]=STACK_STDATA_INFO(data1,data2);
        
        %------------------------
        % - Handle .codebook
        %------------------------
        [data_codebook data1 data2]=STACK_STDATA_CDK(data1,data2);
        
        %------------------------
        % - Handle .date
        %------------------------
        [data1 data2]=STACK_STDATA_DATE(data1,data2);
        data_date=cat(1,data1.date,data2.date);
        
        %------------------------
        % - Handle .colnames
        %------------------------
        data_colnames=data1.colnames;
        data2_colnames_not_in_1=data2.colnames(~ismember(data2.colnames,data_colnames));
        data_colnames=cat(2,data_colnames,data2_colnames_not_in_1);
        
        %------------------------
        % - Handle .value
        %------------------------
        data1_value=NaN(size(data1.value,1),length(data_colnames));
        [idx1_all, idx1]=ismember(data_colnames,data1.colnames);
        data1_value(:,idx1_all)=data1.value(:,idx1(idx1_all));
        
        data2_value=NaN(size(data2.value,1),length(data_colnames));
        [idx2_all, idx2]=ismember(data_colnames,data2.colnames);
        data2_value(:,idx2_all)=data2.value(:,idx2(idx2_all));
        data_value=cat(1,data1_value,data2_value);
        
        % -- sort
        [data_date,idx_sort]=sort(data_date);
        data_value=data_value(idx_sort,:);
        
        %------------------------
        % - Handle .rownames
        %------------------------
        data_rownames={};
        if isfield(data1, 'rownames') && isfield(data2, 'rownames')
            data_rownames = cat(1, data1.rownames, data2.rownames);
            data_rownames=data_rownames(idx_sort);
        elseif isfield(data1, 'rownames')
            data_rownames = cat(1, data1.rownames, repmat({''},size(data2.value,1),1));
            data_rownames=data_rownames(idx_sort);
        elseif isfield(data2, 'rownames')
            data_rownames = cat(1,repmat({''},size(data1.value,1),1),data2.rownames);
            data_rownames=data_rownames(idx_sort);
        end
        
        %------------------------
        % - Create outputs
        %------------------------
        if ~isempty(data_rownames)
            data=st_data('init',...
                'title','extended_stack',...
                'date',data_date,...
                'value',data_value,...
                'codebook',data_codebook,...
                'colnames',data_colnames,...
                'rownames',data_rownames);
        else
            data=st_data('init',...
                'title','extended_stack',...
                'date',data_date,...
                'value',data_value,...
                'codebook',data_codebook,...
                'colnames',data_colnames);
        end
        
        if ~isempty(data_info)
            data.info=data_info;
        end
        %>*
        
    case 'extended_union'
        % !!! BEWARE !!!
        % !!!! Ce code peut changer a tout moment  !!!!
        % !!!! ONLY NIJOS USER CAN USE IT !!!!
        
        %<* Union/merge cell arrays of st_data
        % 1/ merge of colnames !!!
        % Same colnames in several st_data -> in the output : stacked in same colnames
        % 2/ union of rows !!!
        % Multiple conditions in order to merge two differents rows of the 2 st_data
        %    a/ inputs parameters
        %       * varargin{2} = (default = '') names of the colnames to be used for searching the merge rows
        %       * varargin{3} = (default = true) true/false if the .date has to be considered for merging the rows
        %      -> define needed columns where to find the merge rows (=merge_matrix = X)
        % Which rows can be merged :
        %    * the X(i,:) in data1 and data2 is equal
        %    * the X(i,:) in data1 is unique
        %    * the X(i,:) in data2 is unique
        %    * the other values corresponding to this rows (values from colnames not used for the merge)
        %       are also equal
        % 3/ take account of codebooks !!!
        % 4/ TO DO : take account of .info.localtime and .info.place_timezone_id
        % BEWARE : the sorting of the data can be lost !!!
        
        %------------------------------------------------------------------
        %--- INTRO
        %------------------------------------------------------------------
        
        %------------------------
        %--- Inputs handle
        %------------------------
        datas = varargin{1};
        union_colnames={};
        use_union_date=true;
        if nargin==3
            union_colnames= varargin{2};
        elseif nargin==4
            union_colnames= varargin{2};
            use_union_date= varargin{3};
        end
        if ~iscell(datas)
            error('st_data:extended_union', 'varargin{1} has to be a cell array of st_data');
        end
        if isempty(union_colnames) && ~use_union_date
            error('st_data:extended_union', 'varargin{2} and varargin{3} bad');
        end
        idx_e = cellfun(@(c)st_data('isempty-nl', c), datas);
        datas(idx_e) = [];
        
        %------------------------
        %--- handle when cell of st_data >=3
        %------------------------
        if length(datas)<=1
            if isempty(datas)
                data = [];
            else
                data=datas{1};
            end
            return
        elseif length(datas)>=3
            while length(datas)>=3
                data = st_data('extended_union',datas(1:2),union_colnames,use_union_date);
                datas{1}=data;
                datas(2)=[];
            end
        end
        
        %------------------------------------------------------------------
        %--- COMPUTATION : handle when cell of st_data==2
        %------------------------------------------------------------------
        data1 = datas{1};
        data2 = datas{2};
        
        %------------------------
        % - Handle .info
        %------------------------
        [data_info data1 data2]=STACK_STDATA_INFO(data1,data2);
        
        %------------------------
        % - Handle .codebook
        %------------------------
        [data_codebook data1 data2]=STACK_STDATA_CDK(data1,data2);
        
        %------------------------
        % - Handle .date
        %------------------------
        [data1 data2]=STACK_STDATA_DATE(data1,data2);
        
        %------------------------
        % - Handle colnames
        %------------------------
        if iscell(union_colnames)
            union_colnames_cell=union_colnames;
        else
            union_colnames_cell=tokenize(union_colnames,';');
        end
        
        if ~isempty(union_colnames) && ...
                (~all(ismember(union_colnames_cell,data1.colnames)) || ...
                ~all(ismember(union_colnames_cell,data2.colnames)))
            error('st_data:extended_union', 'union_colnames are not in the inputs data !!!');
        end
        
        data_colnames=cat(2,data1.colnames,setdiff(data2.colnames,data1.colnames));
        [col_intersect,idx_sCol1,idx_sCol2]=intersect(data1.colnames,data2.colnames);
        
        %------------------------
        % - Handle merge_matrix
        %------------------------
        % merge_matrix
        % col 1:end-2 = data intersect values
        % col end-1 =index in data1
        % col end =index in data2
        
        % -- unique merge_data in each st_data
        merge_data1=[];
        merge_data2=[];
        if ~isempty(union_colnames)
            merge_data1=st_data('col',data1,union_colnames);
            merge_data2=st_data('col',data2,union_colnames);
        end
        
        if use_union_date
            % TO DO !!
            % GERER UNE PRECISION + ?crire un jour + intraday en precision souhait?e
            merge_data1=cat(2,data1.date,merge_data1);
            merge_data2=cat(2,data2.date,merge_data2);
        end
        
        [~,ix1,iy1] = unique(merge_data1,'rows');
        idx_uni1 = ix1(accumarray(iy1,1,[],@(x)numel(x)==1));
        [~,ix2,iy2] = unique(merge_data2,'rows');
        idx_uni2 = ix2(accumarray(iy2,1,[],@(x)numel(x)==1));
        
        % -- intersect of merge_data
        [tmp1,tmp2,tmp3]= intersect(merge_data1,merge_data2,'rows');
        merge_matrix=[tmp1,tmp2,tmp3];
        
        % -- index that could be merge
        if ~isempty(merge_matrix)
            [~,im1]=intersect(merge_matrix(:,end-1),idx_uni1);
            [~,im2]=intersect(merge_matrix(:,end),idx_uni2);
            im=intersect(im1,im2);
            if isempty(im)
                merge_matrix=[];
            else
                merge_matrix=merge_matrix(im,:);
                % Data has to be the same on every same colnames between st_data
                % We have to check if the values are the same !!!
                if ~isempty(col_intersect)
                    i_2keep=false(size(merge_matrix,1),1);
                    MAGIC_NAN_TRANSFORM_NUMBER=123456789101112;
                    for i_mer=1:size(merge_matrix,1)
                        tmp_data=cat(1,...
                            data1.value(merge_matrix(i_mer,end-1),idx_sCol1),...
                            data2.value(merge_matrix(i_mer,end),idx_sCol2));
                        if any(isnan(tmp_data(:)))
                            tmp_data(isnan(tmp_data))=MAGIC_NAN_TRANSFORM_NUMBER;
                        end
                        if all(tmp_data(1,:)==tmp_data(2,:))
                            i_2keep(i_mer)=true;
                        end
                    end
                    merge_matrix=merge_matrix(i_2keep,:);
                end
            end
        end
        
        %------------------------
        % - Handle merge_matrix
        %------------------------
        % -- data_value1
        [idx_Col1inAll, idx_Col1]=ismember(data_colnames,data1.colnames);
        idx_Row1in1=(1:size(data1.value,1))';
        if ~isempty(merge_matrix)
            idx_Row1in1=setdiff(idx_Row1in1,merge_matrix(:,end-1));
            st_log(sprintf('st_data:extended_union - %d number of rows that has been merged \n',size(merge_matrix,1)));
        end
        data1_value=NaN(length(idx_Row1in1),length(data_colnames));
        data1_value(:,idx_Col1inAll)=data1.value(idx_Row1in1,idx_Col1(idx_Col1inAll));
        % -- data_value2
        [idx_Col2inAll, idx_Col2]=ismember(data_colnames,data2.colnames);
        idx_Row2in2=(1:size(data2.value,1))';
        if ~isempty(merge_matrix)
            idx_Row2in2=setdiff(idx_Row2in2,merge_matrix(:,end));
        end
        data2_value=NaN(length(idx_Row2in2),length(data_colnames));
        data2_value(:,idx_Col2inAll)=data2.value(idx_Row2in2,idx_Col2(idx_Col2inAll));
        % -- data_value_merge
        data_value_merge=[];
        if ~isempty(merge_matrix)
            data_value_merge=NaN(size(merge_matrix,1),length(data_colnames));
            data_value_merge(:,idx_Col1inAll)=data1.value(merge_matrix(:,end-1),idx_Col1(idx_Col1inAll));
            data_value_merge(:,idx_Col2inAll)=data2.value(merge_matrix(:,end),idx_Col2(idx_Col2inAll));
        end
        % -- data_date_merge
        data_date_merge=[];
        if ~isempty(merge_matrix)
            if use_union_date
                data_date_merge=merge_matrix(:,1);
            else
                data_date_merge=data1.date(merge_matrix(:,end-1));
            end
        end
        
        % -- data : output
        data_value=cat(1,data1_value,data2_value,data_value_merge);
        data_date=cat(1,data1.date(idx_Row1in1),data2.date(idx_Row2in2),data_date_merge);
        if use_union_date
            [data_date,idx_sort]=sort(data_date);
            data_value=data_value(idx_sort,:);
        elseif ~isempty(union_colnames_cell)
            [id_UnInAll, idx_Un]=ismember(union_colnames_cell,data_colnames);
            [~,idx_sort]=sortrows(data_value(:,idx_Un(id_UnInAll)),ones(1,length(union_colnames_cell)));
            data_value=data_value(idx_sort,:);
            data_date=data_date(idx_sort);
        end
        
        %------------------------
        % - Handle title
        %------------------------
        title_out=data1.title;
        
        %------------------------
        % - Handle .rownames
        %------------------------
        data_rownames={};
        if isfield(data1, 'rownames') && isfield(data2, 'rownames')
            if isempty(merge_matrix)
                data_rownames = cat(1, data1.rownames(idx_Row1in1), data2.rownames(idx_Row2in2));
            else
                data_rownames = cat(1, data1.rownames(idx_Row1in1), data2.rownames(idx_Row2in2),...
                    data1.rownames(merge_matrix(:,end-1)));
            end
            data_rownames=data_rownames(idx_sort);
        elseif isfield(data1, 'rownames')
            if isempty(merge_matrix)
                data_rownames = cat(1, data1.rownames(idx_Row1in1), repmat({''},length(idx_Row2in2),1));
            else
                data_rownames = cat(1, data1.rownames(idx_Row1in1), repmat({''},length(idx_Row2in2),1),...
                    data1.rownames(merge_matrix(:,end-1)));
            end
        end
        
        %------------------------
        % - Create outputs
        %------------------------
        
        if ~isempty(data_rownames)
            data=st_data('init',...
                'title',title_out,...
                'date',data_date,...
                'value',data_value,...
                'codebook',data_codebook,...
                'colnames',data_colnames,...
                'rownames',data_rownames);
        else
            data=st_data('init',...
                'title',title_out,...
                'date',data_date,...
                'value',data_value,...
                'codebook',data_codebook,...
                'colnames',data_colnames);
        end
        
        if ~isempty(data_info)
            data.info=data_info;
        end
        %>*
        
    case 'demux'
        %<* Return a cellarray of signals
        dati = varargin{1};
        data = cell(1,length(dati.colnames));
        for d=1:length(dati.colnames)
            data{d} = dati;
            data{d}.colnames = dati.colnames(d);
            data{d}.value    = dati.value(:,d);
        end
        %>*
        
    case 'align'
        %<* Echantillonne une data sur une autre
        % La premi?re est la r?f?rence
        data_ref  = varargin{1};
        data_plus = varargin{2};
        
        idx_c     = arrayfun(@(x)find(abs(x-data_ref.date)<eps), data_plus.date, 'uni', false);
        idx_c     = [idx_c{:}]';
        
        all_vals  = cat(2, data_ref.value, repmat(nan,length(idx_c),size(data_plus.value,2)));
        all_vals(idx_c,(size(data_ref.value,2)+1):end) = data_plus.value(1:length(idx_c),:);
        data      = data_ref;
        data.value    = all_vals;
        data.colnames = [ cellfun(@(n)sprintf(['%s - ' data_ref.title], n), data_ref.colnames, 'uni', false), ...
            cellfun(@(n)sprintf(['%s - ' data_plus.title], n), data_plus.colnames, 'uni', false) ];
        %>*
        
    case 'interp'
        %<* Interp at dates
        datas = varargin;
        data = datas{1};
        [data.date, tmp, idx] = unique(data.date);
        data.value = data.value(idx,:);
        for d=2:length(datas)
            [datas{d}.date, tmp, idx] = unique(datas{d}.date);
            datas{d}.value = interp1( datas{d}.date, datas{d}.value(idx,:), data.date);
            data.value = [data.value, datas{d}.value];
        end
        colnames = cellfun(@(d)cellfun(@(c)sprintf('%s - %s', d.title, c), d.colnames, 'uni', false), datas, 'uni', false);
        data.colnames = cellflat( colnames);
        %>*
        
    case 'union'
        %<* Union of two signals
        opt = options({'change-colnames', false}, varargin(2:end));
        datas = varargin{1};
        data = datas{1};
        for d=2:length(datas)
            [dt1, in1] = setdiff( data.date, datas{d}.date);
            [dt2, in2] = setdiff( datas{d}.date, data.date);
            [dtC, inC1, inC2] = intersect(  data.date, datas{d}.date);
            
            data.date  = [dt1; dtC; dt2];
            data.value = [ ...
                [data.value(in1,:), NaN( length(in1), size(datas{d}.value,2))] ; ...
                [data.value(inC1,:), datas{d}.value(inC2,:)] ; ...
                [NaN( length(in2), size(data.value,2)), datas{d}.value(in2,:) ] ];
            if opt.get('change-colnames')
                data.colnames = cat(2, cellfun(@(c)sprintf('%s - %s', data.title, c), data.colnames, 'uni', false), ...
                    cellfun(@(c)sprintf('%s - %s', datas{d}.title, c), datas{d}.colnames, 'uni', false));
            else
                data.colnames = cat(2, data.colnames, ...
                    datas{d}.colnames);
            end
            if isfield( data, 'rownames') && isfield( datas{d}, 'rownames')
                data.rownames = cat(1, data.rownames(in1), data.rownames(inC1), datas{d}.rownames(in2));
            end
        end
        if opt.get('change-colnames')
            ti_ = datas{1}.title;
            for t=2:length(datas)
                ti_ = sprintf('%s + %s', ti_, datas{t}.title);
            end
            data.title = ti_;
        end
        data = st_data('sort', data, '.date');
        %>*
        
    case 'intersect'
        %<* Intesect dates
        data  = varargin{1};
        datap = varargin{2};
        [dt, idx, pdx] = intersect( data.date, datap.date);
        data.date = dt;
        data.value = [data.value(idx,:), datap.value(pdx,:)];
        data.colnames = cat(2, ...
            cellfun(@(c)sprintf('%s - %s', data.title , c),  data.colnames, 'uni', false), ...
            cellfun(@(c)sprintf('%s - %s', datap.title, c), datap.colnames, 'uni', false));
        data.title = 'Intersected';
        %>*
        
    case 'intersect_v2'
        %<* Intesect dates
        % -- input recup
        data  = varargin{1};
        datap = varargin{2};
        if nargin<4
            colnames_handle=false;
        else
            if ~islogical(varargin{3})
                error('st_data:intersect_v2', 'colnames_handle has to be a logical');
            else
                colnames_handle=varargin{3};
            end
        end
        
        % - same colnames in data is not always handled
        if ~colnames_handle && any(ismember(data.colnames,datap.colnames))
            error('st_data:intersect_v2', 'same colnames in data is not handle');
        end
        
        if ~colnames_handle
            [dt, idx, pdx] = intersect( data.date, datap.date);
            data.date = dt;
            data.value = [data.value(idx,:), datap.value(pdx,:)];
            data.colnames = cat(2,data.colnames,datap.colnames);
            data.title = 'Intersected_v2';
        else
            [dt, idx, pdx] = intersect( data.date, datap.date);
            %- colnames : keep in priority data colnames
            data_colnames=data.colnames;
            datap_colnames_not_in_1=datap.colnames(~ismember(datap.colnames,data_colnames));
            data_all_colnames=cat(2,data_colnames,datap_colnames_not_in_1);
            
            %- value
            data.value = data.value(idx,:);
            data_value=data.value;
            
            datap.value = datap.value(pdx,:);
            [idxp, no_use]=ismember(datap.colnames,datap_colnames_not_in_1);
            datap_value=datap.value(:,idxp);
            
            data.date = dt;
            data.value = cat(2,data_value,datap_value);
            data.colnames = data_all_colnames;
            data.title = 'Intersected_v2';
            
        end
        %>*
        
    case 'union_v2'
        %<* union dates
        data  = varargin{1};
        datap = varargin{2};
        
        % -- test--
        % - same dates in data are not handle
        if length(unique(data.date))<length(data.date) || ...
                length(unique(datap.date))<length(datap.date)
            error('st_data:union', 'same dates in data are not handle');
        end
        % - same colnames in data is not handle
        if any(ismember(data.colnames,datap.colnames))
            error('st_data:union', 'same colnames in data is not handle');
        end
        
        %--- computation
        dt= unique([data.date;datap.date]);
        data_value= extended_time_series(data.value,data.date, dt);
        datap_value= extended_time_series(datap.value,datap.date, dt);
        data.date = dt;
        data.value = cat(2,data_value,datap_value);
        data.colnames = cat(2,data.colnames,datap.colnames);
        data.title = 'Union';
        %>*
        
    case 'intersect-approx'
        half_sec_dt = 5.78703703703704e-006; % une demi seconde en date matlab
        data  = varargin{1};
        datap = varargin{2};
        if length(varargin) > 2
            half_sec_dt = varargin{3};
        end
        %<* intersect dates which have less than a second of difference
        [dt, idx, pdx] = intersect( round(data.date / half_sec_dt) * half_sec_dt, round(datap.date / half_sec_dt) * half_sec_dt);
        data.date = dt;
        data.value = [data.value(idx,:), datap.value(pdx,:)];
        data.colnames = cat(2, ...
            cellfun(@(c)sprintf('%s - %s', data.title , c),  data.colnames, 'uni', false), ...
            cellfun(@(c)sprintf('%s - %s', datap.title, c), datap.colnames, 'uni', false));
        data.title = 'Intersected Approximately';
        varargout = {pdx};
        %>*
        
    case 'subsample'
        %<* Sous ?chantillonne
        data  = varargin{1};
        t_ref = varargin{2};
        if numel(t_ref)==1
            t_ref = (data.date(1):t_ref:data.date(end))';
        end
        
        t_idx = arrayfun(@(i)sum(data.date<=i),t_ref);
        
        data.date  = t_ref;
        data.value = data.value(t_idx,:);
        %>*
        
    case 'grid_rm'
        %> retire des points d'une grille uniform?ment espac?e
        data = varargin{1};
        idx  = varargin{2};
        nb_days = length(unique(floor(data.date)));
        nb_perdays = floor(length(data.date) / nb_days);
        if nb_days * nb_perdays ~= length(data.date)
            error('st_data:exec', 'les donn?es ne sont pas sur une grille temporelle uniform?ment espac?e');
        end
        idx2rm = idx;
        for i = 1 : nb_days - 1
            idx2rm = [idx2rm; nb_perdays * i + idx];
        end
        data.value(idx2rm, :) = [];
        data.date(idx2rm, :) = [];
        %>
        
    case 'accumarray'
        data = st_data('group-by', varargin{1}, sprintf('{%s},{%s}', varargin{2}, varargin{3}), 'apply-formula', varargin{4});
        [vals1, idx, idx1] = unique(st_data('cols', data, varargin{2}));
        [vals2, tmp, idx2] = unique(st_data('cols', data, varargin{3}));
        vals = accumarray([idx1 idx2], data.value(:, 3));
        if isfield(varargin{1}, 'codebook')
            colnames = codebook( 'get', varargin{1}.codebook, varargin{3});
            if isempty(colnames)
                colnames = arrayfun(@num2str, vals2, 'uni', false);
            else
                colnames = colnames(vals2)';
            end
            rownames = codebook( 'get', varargin{1}.codebook, varargin{2});
            if isempty(rownames)
                rownames = arrayfun(@num2str, vals1, 'uni', false);
            else
                rownames = rownames(vals1)';
            end
        else
            colnames = arrayfun(@num2str, vals2, 'uni', false);
            rownames = arrayfun(@num2str, vals1, 'uni', false);
        end
        data.value = vals;
        data.date  = data.date(idx);
        data.colnames = colnames;
        data.rownames = rownames;
        
    case 'group-by'
        selection = tokenize(varargin{2}, ',');
        
        explore_val = cell(size(selection));
        idx_min = repmat(1, size(selection));
        idx_max = repmat(0, size(selection));
        for i=1:length(selection)
            explore_val{i} = st_data('to-vals', st_data('apply-formula', varargin{1}, sprintf('unique(%s)', selection{i})));
            idx_max(i) = length(explore_val{i});
        end
        
        % Exploration de la condition GROUP BY
        val = [];
        dt = [];
        
        idx = idx_min;
        while 1
            
            arg = [];
            for i=1:length(selection)
                if i == 1
                    arg = sprintf('%s==%d', selection{i}, explore_val{i}(idx(i)));
                else
                    arg = [arg, sprintf('&%s==%d', selection{i}, explore_val{i}(idx(i)))]; %#ok<AGROW>
                end
            end
            
            data_tmp = st_data('apply-formula', st_data('where', varargin{1}, arg), varargin{4});
            
            % Enregistrement des r?sultats dans des tableaux
            
            val_tmp = [];
            for i=1:length(selection)
                val_tmp = [val_tmp, explore_val{i}(idx(i))]; %#ok<AGROW>
            end
            if ~st_data('isempty-nl', data_tmp)
                val_tmp = [val_tmp, st_data('to-vals', data_tmp)]; %#ok<AGROW>
                val = [val; val_tmp]; %#ok<AGROW>
                dt = [dt; data_tmp.date]; %#ok<AGROW>
            end
            
            idx_old = idx;
            idx = array_incr(idx, idx_min, idx_max, 1, 1);
            
            if idx == idx_old
                break;
            end
        end
        
        % Conversion des tableaux en st_data
        washed_selection = cellfun(@(x) regexprep(x, '[{}]', ''), selection, 'uni', 0);
        
        colnames = {washed_selection{:}, regexprep(varargin{4}, '[{}]', '')};
        if size(val,2) ~= length(colnames)
            st_log('st_data:group_by - I try to guess the colnames you wanted for this group by\n');
            colnames = cat(2, washed_selection, ...
                tokenize( strrep(strrep( regexprep(varargin{4}, '[{}]', ''), ']', ''), '[', ''), ',') );
        end
        
        data = st_data('init', ...
            'title', sprintf('%s - SELECT %s GROUP BY %s', varargin{1}.title, varargin{4}, varargin{2}), ...
            'date', dt, ...
            'value', val, ...
            'colnames', colnames);
        
    case 'accumarray-nan'
        data = st_data('group-by-nan', varargin{1}, sprintf('{%s},{%s}', varargin{2}, varargin{3}), 'apply-formula', varargin{4});
        
        data_varargin_2=st_data('cols', data, varargin{2});
        data_varargin_3=st_data('cols', data, varargin{3});
        
        if any(isnan(data_varargin_2))
            
            [vals1, idx, idx1] = unique(data_varargin_2);
            idx_nan=find(isnan(vals1));
            
            if ~isempty(idx_nan) && length(idx_nan)>1
                vals1(idx_nan(2:end))=[];
                idx(idx_nan(2:end))=[];
                idx1(ismember(idx1,idx_nan))=length(vals1);
            end
            
        else
            [vals1, idx, idx1] = unique(st_data('cols', data, varargin{2}));
        end
        
        if any(isnan(data_varargin_3))
            
            [vals2, tmp, idx2] = unique(data_varargin_3);
            idx_nan=find(isnan(vals2));
            
            if ~isempty(idx_nan) && length(idx_nan)>1
                vals2(idx_nan(2:end))=[];
                idx2(ismember(idx2,idx_nan))=length(vals2);
            end
        else
            [vals2, tmp, idx2] = unique(st_data('cols', data, varargin{3}));
        end
        
        vals = accumarray([idx1 idx2], data.value(:, 3));
        if isfield(varargin{1}, 'codebook')
            colnames = codebook( 'get', varargin{1}.codebook, varargin{3});
            if isempty(colnames)
                colnames = arrayfun(@num2str, vals2, 'uni', false);
            else
                colnames = colnames(vals2)';
            end
            rownames = codebook( 'get', varargin{1}.codebook, varargin{2});
            if isempty(rownames)
                rownames = arrayfun(@num2str, vals1, 'uni', false);
            else
                rownames = rownames(vals1)';
            end
        else
            colnames = arrayfun(@num2str, vals2, 'uni', false);
            rownames = arrayfun(@num2str, vals1, 'uni', false);
        end
        data.value = vals;
        data.date  = data.date(idx);
        data.colnames = colnames;
        data.rownames = rownames;
        
    case 'group-by-nan'
        selection_cell=tokenize(varargin{2},',');
        selection_cols=regexprep(varargin{2}, '[{} ]', '');
        selection_washed=tokenize(selection_cols,',');
        selection_cols=regexprep(selection_cols, '[,]', ';');
        group_by_data=st_data('col', varargin{1},selection_cols);
        
        % -------------------- ALL possibilities building : group_by_key
        % -- create keys
        [group_by_key,~,~]=unique(group_by_data,'rows');
        % -- handling NaNs
        if any(isnan(group_by_key(:)))
            MAGIC_NAN_TRANSFORM_NUMBER=123456789101112;
            group_by_key(isnan(group_by_key))=MAGIC_NAN_TRANSFORM_NUMBER;
            [group_by_key,~,~]=unique(group_by_key,'rows');
            group_by_key(group_by_key==MAGIC_NAN_TRANSFORM_NUMBER)=NaN;
        end
        
        % ------------------------Exploration de la condition GROUP BY
        val = [];
        dt = [];
        
        for i_pos=1:size(group_by_key,1)
            
            for i=1:length(selection_cell)
                if i == 1
                    if isnan(group_by_key(i_pos,i))
                        arg = sprintf('isnan(%s)', selection_cell{i});
                    elseif mod(group_by_key(i_pos,i),1)==0
                        arg = sprintf('%s==%d', selection_cell{i},group_by_key(i_pos,i));
                    else
                        arg = sprintf('%s==%24.24e', selection_cell{i},group_by_key(i_pos,i));
                    end
                else
                    if isnan(group_by_key(i_pos,i))
                        arg = [arg, sprintf('&isnan(%s)', selection_cell{i})]; %#ok<AGROW>
                    elseif mod(group_by_key(i_pos,i),1)==0
                        arg = [arg, sprintf('&%s==%d', selection_cell{i},group_by_key(i_pos,i))]; %#ok<AGROW>
                    else
                        arg = [arg, sprintf('&%s==%24.24e', selection_cell{i},group_by_key(i_pos,i))]; %#ok<AGROW>
                    end
                end
            end
            
            data_tmp = st_data('apply-formula', st_data('where', varargin{1}, arg), varargin{4});
            
            % Enregistrement des r?sultats dans des tableaux
            
            val_tmp = [];
            for i=1:length(selection_cell)
                val_tmp = [val_tmp, group_by_key(i_pos,i)]; %#ok<AGROW>
            end
            if ~st_data('isempty-nl', data_tmp)
                val_tmp = [val_tmp, st_data('to-vals', data_tmp)]; %#ok<AGROW>
                val = [val; val_tmp]; %#ok<AGROW>
                dt = [dt; data_tmp.date]; %#ok<AGROW>
            end
            
        end
        
        % Conversion des colnames du tableau
        colnames=varargin{4};
        if colnames(1) == '[' && colnames(end) == ']'
            try
                colnames = regexprep(colnames, '[\[{}\]]', '');
                commas = colnames== ',';
                brackets_equilibrium = cumsum((colnames == '(') - (colnames == ')'));
                commas = find(commas & (brackets_equilibrium == 0));
                
                commas = [0, commas, length(colnames)+1];
                tmp_coln = cell(1,length(commas)-1);
                for i = 1 : length(commas)-1
                    tmp_coln{i} = strtrim(colnames(commas(i)+1:commas(i+1)-1));
                end
                
                colnames=cat(2, selection_washed,tmp_coln);
            catch ME
                st_log(se_stack_error_message(ME));
                st_log('st_data:group-by-Nan unable to identify good colnames.');
                colnames = tokenize( sprintf( [ varargin(4) '[%d];'], 1:size(val,2)), ';');
            end
        else
            colnames={selection_washed{:},regexprep(colnames, '[{}]', '')};
        end
        
        data = st_data('init', ...
            'title', sprintf('%s - SELECT %s GROUP BY %s', varargin{1}.title, varargin{4}, varargin{2}), ...
            'date', dt, ...
            'value', val, ...
            'colnames', colnames);
        if isfield(varargin{1},'codebook')
            data.codebook=varargin{1}.codebook;
        end
        
    case 'to_cell'
        data_in = varargin{1};
        if nargin < 3
            colnames = data_in.colnames;
        else
            colnames = varargin{2};
        end
        if ~iscell(colnames)
            colnames = tokenize(colnames,';');
        end
        if isfield(data_in, 'codebook') && codebook('iscodebook', data_in.codebook)
            [cols_cdk, cols_numeric] = intersect_and_diff(data_in.colnames, {data_in.codebook.colname});
            cols_cdk = intersect(cols_cdk,colnames);
            cols_numeric = intersect(cols_numeric,colnames);
            data = cell(length(data_in.date),length(cols_numeric)+length(cols_cdk));
            if ~isempty(cols_cdk)
                data_cdk = st_data('col_cdk',data_in,cols_cdk);
                data(:,1:length(cols_cdk)) = data_cdk;
            end
            if ~isempty(cols_numeric)
                data_numeric = num2cell(st_data('cols',data_in,cols_numeric));
                data(:,length(cols_cdk)+(1:length(cols_numeric))) = data_numeric;
            end
            data_colnames = [cols_cdk,cols_numeric];
            [~,a,b] = intersect(colnames,data_colnames);
            data(:,a) = data(:,b);
        else
            data = num2cell(st_data('cols',data_in,colnames));
        end
    case 'undock_col'
        % Permet d'extraire une colonne d'un st_data. Le st_data avec la
        % colonne en moins peut ?tre r?cup?r? en second argument.
        % [data, idx, varargout] = st_data( mode, varargin)
        idx = varargin{1}; % le nom n'est pas terrible mais il nous est impos?.
        col_name = varargin{2};
        data = idx;
        idx_col = strcmp(data.colnames,col_name);
        assert(any(idx_col),'No such colnames in the data')
        assert(sum(idx_col)<2,'More than two columns with the same name')
        data.value = data.value(:,idx_col);
        data.colnames = data.colnames(idx_col);
        if nargout > 1
            idx.colnames(idx_col) = [];
            idx.value(:,idx_col) = [];
        end
        
    case 'from_date'
        assert(length(varargin)>=2,'Incorrect number of input arguments')
        if length(varargin)==2
            empty_val = nan;
        else
            empty_val = varargin{3};
        end
        data_in = varargin{1};
        date = varargin{2};
        nb_col = length(data_in.colnames);
        [un_date,~,idx_date] = unique(date);
        [un_dt,idx_dt,~] = unique(data_in.date);
        val_out = empty_val*ones(length(un_date),nb_col);
        [~,ix_in,ix_out] = intersect(un_dt,un_date);
        val_out(ix_out,:) = data_in.value(idx_dt(ix_in),:);
        
        % output preparation
        data = st_data('init','value',val_out(idx_date,:),'date',date,'colnames',data_in.colnames);
        
    case 'reindex'
        % out = st_data( 'reindex', data, values, colnames)
        % - values : nouvel index. Dans le m?me ordre que les colonnes de "colnames"
        % - colnames : noms des colonnes s?par?s par des points-virgules
        %
        % Exemple :
        %    st_data('reindex',st_data('from-matrix',cumsum(randn(10,2))),[(1:3)',randn(3,1)],'.date;V-1')
        
        data = varargin{1};
        index = varargin{2};
        colstr = varargin{3};
        
        colnames = tokenize(colstr,';');
        cols = nan(length(data.date),length(colnames));
        for i = 1:length(colnames)
            if strcmp(colnames{i},'.date')
                cols(:,i) = data.date;
            else
                cols(:,i) = st_data('cols',data,colnames{i});
            end
        end
        data.date = joint(cols,data.date,index);
        data.value = joint(cols,data.value,index);
        for i = 1:length(colnames)
            if strcmp(colnames{i},'.date')
                data.date = index(:,i);
            else
                data.value(:,strcmp(data.colnames,colnames{i})) = index(:,i);
            end
        end
        
    otherwise
        error('st_data:mode', 'mode <%s> unknown', mode);
end

%% ** Internals
    function [cdk data1 data2]=STACK_STDATA_CDK(data1,data2)
        % // create stacked codebook
        cdk=[];
        colname_merged=[];
        if isfield(data1,'codebook') || isfield(data2,'codebook')
            cdk1=[];
            cdk2=[];
            if isfield(data1,'codebook')
                cdk1=data1.codebook;
            end
            if isfield(data2,'codebook')
                cdk2=data2.codebook;
            end
            [cdk colname_merged]=codebook('stack',cdk1,cdk2);
        end
        
        % // transform needed outputs data
        if ~isempty(cdk) && ~isempty(colname_merged)
            for i_col=1:length(colname_merged)
                %// data1
                id_col1_tmp=strcmp(data1.colnames,colname_merged{i_col});
                if any(id_col1_tmp)
                    data1.value(:,id_col1_tmp)=...
                        codebook('extract','names2ids',cdk,colname_merged{i_col},...
                        codebook('extract','ids2names',data1.codebook,colname_merged{i_col},...
                        data1.value(:,id_col1_tmp)));
                end
                %// data2
                id_col2_tmp=strcmp(data2.colnames,colname_merged{i_col});
                if any(id_col2_tmp)
                    data2.value(:,id_col2_tmp)=...
                        codebook('extract','names2ids',cdk,colname_merged{i_col},...
                        codebook('extract','ids2names',data2.codebook,colname_merged{i_col},...
                        data2.value(:,id_col2_tmp)));
                end
            end
        end
    end

    function [stack_info data1 data2]=STACK_STDATA_INFO(data1,data2)
        % TO DO : different security, aload, but create a security_id colnames + remove td_info... ?
        % handle .td_info... : several security_id...?? different dates ???
        % handle .security_id / .security_key / .security_type / .security_rate2euro
        % handle td_phase_info...
        
        %//////////////////
        %//////// INTRO
        %//////////////////
        
        stack_info=[];
        data1_info=[];
        data2_info=[];
        
        if isfield(data1,'info')
            data1_info=data1.info;
        end
        if isfield(data2,'info')
            data2_info=data2.info;
        end
        
        if isempty(data1_info) && isempty(data2_info)
            return
        end
        
        %//////////////////
        % /////// STEP 1 : localtime AND place_timezone_id
        %//////////////////
        % IDEA: if place_timezone_id is different we correct the dates
        % (choosing the place_timezone_id of the data1 if no gmt)
        % if place_timezone_id is only in one of the two
        data1_pltid=NaN;
        data2_pltid=NaN;
        if ~isempty(data1_info) && isfield(data1_info,'place_timezone_id')
            data1_pltid=data1_info.place_timezone_id;
        elseif ~isempty(data1_info) && isfield(data1_info,'localtime') && isfield(data1_info,'td_info')
            if data1_info.localtime
                data1_pltid=data1_info.td_info(1).place_id;
            else
                data1_pltid=0;
            end
        end
        
        if ~isempty(data2_info) && isfield(data2_info,'place_timezone_id')
            data2_pltid=data2_info.place_timezone_id;
        elseif ~isempty(data2_info) && isfield(data2_info,'localtime') && isfield(data2_info,'td_info')
            if data2_info.localtime
                data2_pltid=data2_info.td_info(1).place_id;
            else
                data2_pltid=0;
            end
        end
        
        data_pltid=[data1_pltid data2_pltid];
        
        if all(isfinite(data_pltid))
            if data1_pltid==data2_pltid
                place_tmp=data1_pltid;
            else
                place_tmp=0;
            end
            data1=timezone('convert_stdata','final_place_id',place_tmp,'data',data1);
            data2=timezone('convert_stdata','final_place_id',place_tmp,'data',data2);
        elseif any(isfinite(data_pltid))
            data1=timezone('convert_stdata','final_place_id',data_pltid(isfinite(data_pltid)),'data',data1);
            data2=timezone('convert_stdata','final_place_id',data_pltid(isfinite(data_pltid)),'data',data2);
        end
        
        %-- edit data1_info
        data1_info=data1.info;
        data2_info=data2.info;
        
        %//////////////////
        % /////// STEP 2 : .date_datestamp
        %//////////////////
        % dates and time has already been handled before
        % here, if we aggregated several dates, we have to delete this field
        is_data_datestamp1=~isempty(data1_info) && isfield(data1_info,'data_datestamp');
        is_data_datestamp2=~isempty(data2_info) && isfield(data2_info,'data_datestamp');
        if  is_data_datestamp1 && is_data_datestamp2 && ...
                data1_info.data_datestamp~=data2_info.data_datestamp
            data1_info=rmfield(data1_info,'data_datestamp');
            data2_info=rmfield(data2_info,'data_datestamp');
        elseif is_data_datestamp1 && ~is_data_datestamp2
            data1_info=rmfield(data1_info,'data_datestamp');
        elseif is_data_datestamp2 && ~is_data_datestamp1
            data2_info=rmfield(data2_info,'data_datestamp');
        end
        
        %//////////////////
        % /////// STEP 3 : others values
        %//////////////////
        % By default we adds all of the others fieldnames from data1_info or instead data2_info
        if ~isempty(data1_info)
            data1_info_fieldnames=fieldnames(data1_info);
            for i_f=1:length(data1_info_fieldnames)
                stack_info=setfield(stack_info,data1_info_fieldnames{i_f},...
                    getfield(data1_info,data1_info_fieldnames{i_f}));
            end
        elseif ~isempty(data2_info)
            data2_info_fieldnames=fieldnames(data2_info);
            for i_f=1:length(data2_info_fieldnames)
                stack_info=setfield(stack_info,data2_info_fieldnames{i_f},...
                    getfield(data2_info,data2_info_fieldnames{i_f}));
            end
        end
        
    end

    function [data1 data2]=STACK_STDATA_DATE(data1,data2)
        % First check if .date seems a real timeserie
        date_ok1=all(data1.date>=693962) || all(data1.date>=0 & data1.date<=1); %1900-01-01... .
        date_ok2=all(data2.date>=693962) || all(data2.date>=0 & data2.date<=1);
        if ~date_ok1 && ~date_ok2
            return
        elseif (~date_ok1 && date_ok2) || (date_ok1 && ~date_ok2)
            error('st_data:extended_stack', 'STACK_STDATA_DATE : .date are not compatible !! ');
        end
        
        % TO DO : loose precision, has to go for houirs + "day" ??
        % if the 2 types are available : change on dates format date + hours
        
        date_num1=all(data1.date>1);
        date_num2=all(data2.date>1);
        
        if (date_num1 && date_num2) || (~date_num1 && ~date_num2)
            return
        elseif date_num1
            if any(strcmp(data2.colnames,'day'))
                data2.date=data2.date+data2.value(:,strcmp(data2.colnames,'day'));
            elseif ~isempty(data2.info) && isfield(data2.info,'data_datestamp')
                data2.date=data2.date+data2.info.data_datestamp;
            else
                error('st_data:extended_stack', 'STACK_STDATA_DATE : impossible to stack dates !! ');
            end
        elseif date_num2
            if any(strcmp(data1.colnames,'day'))
                data1.date=data1.date+data1.value(:,strcmp(data1.colnames,'day'));
            elseif ~isempty(data1.info) && isfield(data1.info,'data_datestamp')
                data1.date=data1.date+data1.info.data_datestamp;
            else
                error('st_data:extended_stack', 'STACK_STDATA_DATE : impossible to stack dates !! ');
            end
        end
    end

%<* Grep colnames
    function my_names = GREP_NAMES( my_names, colnames)
        grepped_names_idx = cellfun(@(n)n(1)=='&', my_names);
        if any( grepped_names_idx)
            grepped_names = my_names(grepped_names_idx);
            grepped_names_num = find( grepped_names_idx);
            for g=1:length(grepped_names)
                found_idx = find(cellfun(@(x)~isempty(x),regexp( colnames, grepped_names{g}(2:end))));
                if ~isempty( found_idx)
                    my_names{grepped_names_num(g)} = colnames(found_idx);
                end
            end
            my_names = cellflat( my_names);
        end
    end
end
%>*

function test()
% Unitary test to control the presence of regression
test_result = false(2,1);
rand_name_gen = @(N)cellstr(char(randi([48,90],[N,9])))';
data_test_1dim = st_data('init','value',cumsum(randn(100,3)),'colnames',...
    rand_name_gen(3),'date',(1:100)');
data_test_2dim = st_data('init','value',cumsum(randn(100,3,2)),'colnames',...
    reshape(rand_name_gen(6),2,3),'date',(1:100)');
test_result(1) = all(st_data('cols',data_test_1dim,data_test_1dim.colnames{2}) == ...
    data_test_1dim.value(:,2));
idx = false(length(data_test_1dim.date),1);
idx(randsample(data_test_1dim.date,floor(length(data_test_1dim.date)/3))) = true;
test_result(2) = all(st_data('cols',data_test_1dim,data_test_1dim.colnames{2},idx) == ...
    data_test_1dim.value(idx,2));
test_result(3) = all(st_data('cols',data_test_2dim,data_test_2dim.colnames{2,2})==...
    data_test_2dim.value(:,2,2));
test_result(4) = all(st_data('cols',data_test_2dim,data_test_2dim.colnames{2,2},idx)==...
    data_test_2dim.value(idx,2,2));
if all(test_result)
    fprintf('All tests are OK\n');
else
    fprintf('Some tests are NOT OK\n');
end
end
