function py_out = st_data2pydico(mode, st_in, varargin)
% ST_DATA2PYDICO - 
%
%   py_out =  st_data2pydico( 'date'|'colname', st_in, opts)
%
% example:
%   data = st_data('from-matrix', cumsum(randn(10,2)))
%   py_out =  st_data2pydico( 'date', data)
%   py_out =  st_data2pydico( 'date', data, 'filename', 'C:\st_repository\simep_scenarii\results\scheduled_simulations\custom_data.py')
%   py_out =  st_data2pydico( 'date', data, 'filename', fod )

opt = options({'filename', '', 'key-name', ''}, varargin);

%% codebook management

use_cdk = isfield(st_in,'codebook');


if use_cdk
    is_cdk = false(size(st_in.colnames));
    cdk_values = cell(size(st_in.value));
    if use_cdk
        for c = 1 : length(st_in.colnames)
            is_cdk(c) = any(strcmp(st_in.colnames{c},{st_in.codebook.colname}));
            if is_cdk(c)
                cdk_values(:,c) =  codebook('replace',st_in,  st_in.colnames{c});
            end
        end
    end
end
switch lower(mode)
    case 'date'
        py_out = '{';
        for d=1:length(st_in.date)
            py_out = [py_out ' ''' datestr(st_in.date(d), 'yyyymmdd') ''' : {  '];
            for c=1:length(st_in.colnames)
                % codebook ? 
                if use_cdk && is_cdk(c)
                    % col is in hte codebook 
                    py_out = sprintf('%s ''%s'' : ''%s'' ,', py_out ,  st_in.colnames{c}, cdk_values{d,c});    
                else
                    py_out = sprintf('%s ''%s'' : %f ,', py_out ,  st_in.colnames{c}, st_in.value(d,c));
                end
            end
            py_out(end) = '}';
            py_out = [py_out, sprintf(',\n')];
        end
        py_out(end-1) = '}';
        py_out(end) = '';
        fname = opt.get('filename');
        if ~isempty( fname)
            if ischar( fname)
                fod = fopen( fname, 'w');
            else
                fod = fname;
            end
            key_name = opt.get('key-name');
            if ~isempty( key_name)
                key_name = ['["' key_name '"]'];
            end
            fprintf(fod, 'custom_data%s = %s\n', key_name, py_out);
            if ischar( fname)
                fclose(fod);
            end
        end
    case 'colname'
        eror('st_data2pydico:mode', 'mode <colname> not implemented yet');
    otherwise
        error('st_data2pydico:mode', 'mode <%s> unknown', mode);
end