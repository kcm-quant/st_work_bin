function h = st_function( f_value, f_date, f_colnames, f_title, name, f_check, k_func)
% ST_FUNCTION - buider de fonctions structur�es
%
%  Please use build_slicer rather than this function...
%
% methods:
%  - f.apply( data) -> data
%  - f.sliding( data, width, step, 'p') -> data
%  - f.check( data, width, step, horizon, 'p') -> data
%
% See also st_data st_node st_grid build_slicer
if nargin < 5
    name = 'unknown';
end
if ischar(f_colnames)
    f_colnames = tokenize( f_colnames, ';');
end

%this = struct('f_value', f_value, 'f_date', f_date, 'f_colname', f_colname, 'f_title', f_title, 'name', name, 'f_check', f_check);

h = struct('name', name, 'apply', @apply, 'sliding', @sliding, 'check', @check);

    function data = apply( data, theta)
        % function proprement dite
        args = {data.value, data.date};
        data.date  = feval( f_date, args{end:end-nargin(f_date)+1});
        if nargin==1
            % je suis en mode sliding classique
            data.value = feval( f_value, args{1:nargin(f_value)});
            if iscell( f_colnames)
                data.colnames = f_colnames;
            else
                data.colnames = feval(f_colnames, data.colnames);
            end
        else
            % je suis en mode check
            data.value = feval( f_check, theta, args{1:nargin(f_value)});
            if iscell( k_func)
                data.colnames = k_func;
            else
                data.colnames = feval(k_func, data.colnames);
            end     
        end
        if ischar( f_title)
            data.title = f_title;
        else
            data.title = feval(f_title, data.title);
        end
    end

    function [data_sliding, data_check] = check( data, window, step, horizon, type) 
        [data_sliding, data_check] = sliding( data, window, step, type, 'dynamic', 'horizon', horizon);
    end

    function varargout = sliding( data, varargin)
        % compatibilit� ascendante
        if (nargin>1 && ~ischar( varargin{1})) || nargin>6
            if nargin>=4
                switch lower(varargin{3})
                    case { 'p', 'point', 'points'} % must be inline with get_slicer/parse_args
                        sm = 'points';
                    case { 'date', 'd' , 't', 'time'} % must be inline with get_slicer/parse_args
                        sm = 'time';                        
                    otherwise
                        error('st_function:sliding:mode', 'mode <%s> unknown', varargin{3}); 
                end
                if length(varargin)<4
                    gridmode = 'dynamic';
                else
                    gridmode = varargin{4};
                end
            else
                sm = 'points';
            end
            varargin = { 'window:date', varargin{1}, 'step:date', varargin{2}, 'mode:char', sm, 'gridmode:char', gridmode, varargin{5:end}};
        end
        % idem fenetre glissante
        opt = options({'mode:char', 'points', ...
            'gridmode:char', 'dynamic',...
            'window:date',datenum(0,0,0,0,10,0), ...
            'step:date',datenum(0,0,0,0,10,0), ...
            'horizon', 0 ...
            }, varargin);
        
        vals  = [];
        dates = [];
        step    = opt.get('step:date');
        width   = opt.get('window:date');
        horizon = opt.get('horizon');
        vals_h  = [];
        dates_h = [];
        switch lower(opt.get('mode:char'))
            case { 'p', 'point', 'points'}
                for d=1:step:size(data.date,1)-(width+horizon)
                    data_temp = apply( st_data('from-idx', data, d:d+width));
                    vals  = [vals; data_temp.value];
                    dates = [dates; data_temp.date];
                    
                    if horizon>0
                        data_h  = apply( st_data('from-idx', data, d+width+1:d+width+horizon), data_temp.value);
                        vals_h  = [vals_h; data_h.value];
                        dates_h = [dates_h; data_h.date];
                    end
                end
            case { 'date', 'd' , 't', 'time'}
                % ATTENTION: width et step sont en unit� de temps
                modegrid = opt.get('gridmode:char');
                if horizon>0 && ~strcmpi( modegrid, 'dynamic')
                    error('st_function:check', 'only the <dynamic> timegrid mode is available for check mode');
                end
                
                grid = st_grid( modegrid, 'data', data,  varargin{:});                              
                
                if nargin(f_value)==1
                    % pour les fonctions � 1 argument
                    
                    % appel en mode sliding classique
                    vals   = arrayfun(@(inf,sup)feval( f_value, data.value(inf:sup,:)), grid(:,1), grid(:,2), 'uni', false);
                    if horizon>0
                        % appel en mode check
                        vals_h = arrayfun(@(i,sup,hor)feval(f_check, ...
                            vals{i}, data.value(sup+1:hor,:)), (1:size(grid,1))', grid(:,2), grid(:,3), 'uni', false);
                    end
                elseif nargin(f_value)==3 
                    % pour les fonctions qui veulent savoir si le premier point d'un interval appartient au pr�c�dent
                    vals  = arrayfun(@(inf,sup, b)feval( f_value, data.value(inf:sup,:), data.date(inf:sup), b), grid(:,1), grid(:,2), [true; grid(2:end,1) ~= grid(1:end-1,2)], 'uni', false);
                else
                    vals  = arrayfun(@(inf,sup)feval( f_value, data.value(inf:sup,:), data.date(inf:sup)), grid(:,1), grid(:,2), 'uni', false);
                end
                vals   = cat(1, vals{:});
                if horizon>0
                    vals_h = cat(1, vals_h{:});
                end
                if (modegrid(1) == 'd') % dynamic mode
                    dates = arrayfun( @(inf,sup)feval( f_date, data.date(inf:sup,:)), grid(:,1), grid(:,2));
                    [dates, idx] = unique(dates);
                    vals = vals(idx,:);
                    if horizon>0
                        dates_h = arrayfun( @(sup,hor)feval( f_date, data.date(sup+1:hor,:)), grid(:,2), grid(:,3));
                        [dates_h, idx] = unique(dates_h);
                        vals_h = vals_h(idx,:);
                    end
                else
                    dates = grid(:, 3);
                end
                data_temp.title    = feval(f_title, data.title);
                if iscell( f_colnames)
                    data_temp.colnames = f_colnames;
                else
                    data_temp.colnames = feval(f_colnames, data.colnames);
                end
                if horizon>0
                    data_h.title = data_temp.title;
                    if iscell( k_func)
                        data_h.colnames = k_func;
                    else
                        data_h.colnames = feval(k_func, data.colnames);
                    end
                end
            otherwise
                error('st_function:sliding', 'mode <%s> unknown', mode);
        end
        data.value    = vals;
        data.date     = dates;
        data.title    = data_temp.title;
        data.colnames = data_temp.colnames;
        
        if horizon==0
            data_check = [];
        else
            data_check = rmfield( data, 'value');
            data_check.value    = vals_h;
            data_check.date     = dates_h;
            data_check.title    = data_h.title;
            data_check.colnames = data_h.colnames;   
        end
        varargout = { data, data_check };
    end
end