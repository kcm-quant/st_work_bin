function idx = st_grid( mode, varargin)
% ST_GRID cr�e une grille temporelle: tout d'abord on �chantillone la s�rie
% avec le pas = 'step', ensuite on renvoie l'intervalle (sous forme indice)
% de longueur = 'window', � partir des points �chantillon�s.
% Actuelle on fournit deux modes: 'fix' et 'dynamic'. Dans le mode 'fix' on
% commence toujours � 9h et termine � 17h30, tandis que avec le mode
% 'dynamic', on �chantillone suivant la s�rie pass�e 'date'.
%
% See also st_function get_time_grid
opt = options({'data', [],...
    'window:date', 0,...
    'step:date', 0,...
    'horizon', 0, ...
    'fix_mode:char', 'disjoint',...
    }, varargin);
switch lower(mode)
    case {'dynamic', 'd'}
        date    = opt.get('data');
        date    = date.date;
        step    = opt.get('step:date');
        width   = opt.get('window:date');
        horizon = opt.get('horizon');

        tps_first = (date(1):step:date(end)-width)';
        idx_first = arrayfun(@(i)find(date>i,1,'first'), tps_first);

        tps_last = tps_first + width;
        idx_last = arrayfun(@(i)find(date>=i,1,'first'), tps_last);

        if horizon>0
            tps_horizon = tps_last + horizon;
            idx_horizon = arrayfun(@(i)find(date>=i,1,'first'), tps_horizon, 'uni', false);
            idx_horizon = cell2mat( idx_horizon);
            idx_first   = idx_first(1:length(idx_horizon));
            idx_last    = idx_last(1:length(idx_horizon));
        else
            idx_horizon = [];
        end
        
        idx = [idx_first, idx_last, idx_horizon];

    case {'fix', 'f'}
        fix_mode = opt.get('fix_mode:char');
        if ~isempty(strfind(fix_mode, ':'))
            error('You cant currently use such a mode via st_grid, ask Romain to implement it...');
        end
        % , ...
        %    'grid_mode','disjoint'
        idx = get_time_grid(fix_mode, opt.get('data'), 'is_gmt:bool', true, 'back_to_gmt:bool', true, varargin{:});
    otherwise
        error('Error in st_grid, mode <%s> unknown', lower(mode));
end