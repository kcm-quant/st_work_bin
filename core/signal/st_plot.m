function h = st_plot( data, varargin)
% ST_PLOT - plot de don�nes structur�es
% 
% options:
% - panel     : []
% - link_axes : true or false
% - cheuvreux_colors : true or false
%
% data = st_data( 'init', 'title', 'Mes donn�es', 'value', cumsum(randn(100,5)), ...
%                     'date', (1:100)', 'colnames', { 'V-1', 'V-2', 'V-3', 'V-4', 'V-5' })
% st_plot(data)
% st_plot(data, 'cheuvreux_cg', true)
% st_plot(data, 'cheuvreux_cg', true, 'cc_order', 'rgrgm')
%
% champs standardis�s:
% - plot_type   : lines*|stairs|points|discrete
% - subplot_mode: horizontal|vertical|grid*
% - tube : un st_data avec trois colnames : min/centre/max
% - alarm : vecteur colonne de dates
%     data.alarm = [10 100 ;200 220]
% - plot_fun : plot function handle
% Renvoie une structure contenant des pointeurs sur les objets cr��s
%
% Il y a un mode d'utilisation sp�cial qui g�n�re l'uimenu des les plots
% il s'utilise ainsi: st_plot('menu', mode, options):
% - st_plot('menu', 'init', gca) en est la principale utilisation
%
% pour cr�er un panel sympa:
% f = uipanel('backgroundcolor', [238 238 208]/256, 'borderwidth', 0);
%
% % Plot de trois dimension dans une figure � deux dimensions :
% data = st_data('init', 'value', repmat((1 : 10)', 1, 3), 'colnames', {'axe1', 'axe2', 'axe3'}, 'date', (1 : 10)');
% st_plot(data, 'axe1;axe2;axe3');
%
% See also subgrid attach_date plot_focus get_focus axaxis

%%** Special use: uimenu
if isstr( data) && strcmpi(data,'menu')
    mode = varargin{1};
    switch lower(mode)
        case { 'init', 'create' }
            %<* Menu creation
            target_h = varargin{2};
            h = uicontextmenu;
            uimenu(h, 'label', 'X grid', 'callback', 'st_plot(''menu'', ''xgrid'')');
            uimenu(h, 'label', 'Y grid', 'callback', 'st_plot(''menu'', ''ygrid'')');
            uimenu(h, 'label', 'dates' , 'callback', 'st_plot(''menu'', ''date'')');
            uimenu(h, 'label', 'dates (HH:MM)' , 'callback', 'st_plot(''menu'', ''date:HH:MM'')');
            uimenu(h, 'label', 'dates (HH:MM:SS)' , 'callback', 'st_plot(''menu'', ''date:HH:MM:SS'')');
            uimenu(h, 'label', 'dates (mm/yy)' , 'callback', 'st_plot(''menu'', ''date:mm/yy'')');
            uimenu(h, 'label', 'dates (dd/mm/yy)' , 'callback', 'st_plot(''menu'', ''date:dd/mm/yy'')');
            uimenu(h, 'label', 'background (white)' , 'callback', 'st_plot(''menu'', ''white-panel'');');
            uimenu(h, 'label', 'export' , 'callback', 'st_plot(''menu'', ''export'');');
            set(target_h, 'uicontextmenu', h);
            %>*
        case 'xgrid'
            %<* X grid
            xg = get(gca, 'xgrid');
            switch lower(xg)
                case 'on'
                    set(gca, 'xgrid', 'off');
                case 'off'
                    set(gca, 'xgrid', 'on');
            end
            %>*
        case 'ygrid'
            %<* X grid
            xg = get(gca, 'ygrid');
            switch lower(xg)
                case 'on'
                    set(gca, 'ygrid', 'off');
                case 'off'
                    set(gca, 'ygrid', 'on');
            end
            %>*
        case 'date'
            %<* Date (on/off)
            opt = userdata(gca, 'get', 'attached-date');
            ht = opt.get('handle');
            if ~isempty( ht)
                delete(ht);
                opt.set('handle', []);
                userdata(gca, 'set', 'attached-date', opt);
                set(gca,'XTickMode', 'auto');
                xt = get(gca, 'xtick');
                set(gca,'xticklabel', xt);
            else
                attach_date('show');
            end
            %>*
        case 'date:hh:mm'
            %<* Date HH:MM
            attach_date('draw', 'format', 'HH:MM');
            %>*
        case 'date:hh:mm:ss'
            %<* Date HH:MM
            attach_date('draw', 'format', 'HH:MM:SS');
            %>*
        case 'date:mm/yy'
            %<* Date HH:MM
            attach_date('draw', 'format', 'mm/yy');
            %>*
        case 'date:dd/mm/yy'
            %<* Date HH:MM
            attach_date('draw', 'format', 'dd/mm/yy');
            %>*
        case 'white-panel'
            %<* Turn panel colors to white
            os = findobj(gcf,'type', 'uipanel');
            set(os, 'backgroundcolor', [1 1 1]);
            %>*
        case 'export'
            %<* Export to file
            img_dir = getenv('matlab_save_plot');
            if isempty( img_dir)
                img_dir = fullfile(getenv('st_work'), 'doc', 'images');
            end
            [fname, pname] = uiputfile(fullfile( img_dir, ...
                ['plot-' datestr(now, 'dd-mm-yy_HH-MM')]), ...
                'Save as...');
            ffname = fullfile( pname, fname);
            [p,f,ext] = fileparts( fname);
            if isempty( ext)
               % je prend plusieurs formats
               ext = { '.png', '.pdf', '.eps', '.emf' };
               cellfun(@(e) saveas(gcf, [ffname e]), ext);
            else
               saveas(gcf, ffname); 
            end
            %>*
        otherwise
            error('st_plot:uimenu:mode', 'mode <%s> unknown', mode);
    end
    return
elseif all(arrayfun(@(i)st_data('isempty-nl', data(i)),1:length(data)))
    st_log('Nothing to plot as your data is empty\n');
    h = [];
    return;
end

%%** Direct use: plot
opt = options( {'panel', [] , 'linewidth', 2, 'link_axes', false, ...
    'visible', 'on', 'cheuvreux_cg', false, ... % cheuvreux_cg : means cheuvreux charte graphique
    'cc_order', 'rgnm', ... % rgnm : means red/grey/black/brown
    'leg_location', 'NorthEast', ...
    'leg_FontSize', 10, ...
    'leg_box', 'on', ...
    'leg_edge_color', [0 0 0], ...
    'fontname', 'FixedWidth', ...
    'title', true, ...
    'no_legend', false, ...
    'layout', [],...
    'tick_on_dates', false, ... % if false then Xtick are determined by attachdate (uniformly distributed between the minimum and the maximum). If true, then every date in the st_data will be an Xtick
    }, varargin);
f = opt.get('panel');
cc = opt.get('cheuvreux_cg');
if cc
    % 100% : 0-150-97 (pour text et graphiques)
    %
    % Autres nuances de vert pour les graphiques :
    % 80% 51 717 129
    % 60% 102 192 160
    % 40% 153 213 192
    c0 = [0 0 0]; 
    c1 = [170 160 149]/256;
    if iscell(data)
        Mc = max(cellfun(@(d)length(d.colnames),data));
    else
        Mc = length(data.colnames);
    end
    cs = arrayfun(@(a,b)linspace(a,b, max([2,Mc-2]))',c0,c1, 'uni', false); 
    c_init_colors = [[0 150 97] / 256; [153 213 192]/ 256; [cs{:}]];
    cc_init_order  = ['rgnm' char(65:65+Mc-5)];
    cc_asked_order = opt.get('cc_order');
    if isempty( cc_asked_order)
        cc_asked_order = cc_init_order;
    end
    c_colors = NaN(length(cc_asked_order), 3);
    for i = 1 : length(cc_asked_order)
        try
            c_colors(i, :) = c_init_colors(strfind(cc_init_order, cc_asked_order(i)), :);
        catch ME
            if strcmp(ME.message, 'Improper assignment with rectangular empty matrix.')
                error('st_plot:cheuvreux_colors', 'The character : <%s> is not a cheuvreux color use one of these : <%s>', cc_asked_order(i), cc_init_order);
            else
                rethrow(ME);
            end
        end
    end
    opt.set('leg_box', 'off');
    opt.set('fontname', 'Helvetica');
    opt.set('title', false);
    opt.set('leg_edge_color', [1 1 1]);
end
if isempty(f)
    if strcmpi('off', opt.get('visible'))
        figure('visible', 'off');
    else
        figure; 
    end
    if cc
        f = uipanel('backgroundcolor', [1 1 1], 'borderwidth', 0);
    else
        f = uipanel('backgroundcolor', [238 238 208]/256, 'borderwidth', 0);
    end
end
if ~iscell( data)
    data = { data};
end
if cc
    for i = 1 : length(data)
        if isfield(data{i}, 'attach_format')
%             if strcmpi(data{i}.attach_format, 'dd/mm') || strcmpi(data{i}.attach_format, 'dd/mm/yyyy')
%                 data{i}.attach_format = 'mm/dd/yyyy';
%             end
        else
            data{i}.attach_format = 'dd/mm/yyyy';
        end
    end
end
idx_subplot = cellfun(@(d)isfield( d, 'subplot_mode'), data);
if any( idx_subplot)
    if sum( idx_subplot)>1
        st_log('st_plot:submode_plot -> more than on dataset have a subplot field, I will take the first encoutered one\n');
    end
    subplot_mode = data{find(idx_subplot,1)}.subplot_mode;
else
    subplot_mode = 'grid';
end
switch lower( subplot_mode)
    case 'grid'
        [n,p] = subgrid( length(data));
    case { 'h', 'horizontal'}
        n = 1;
        p = length(data);
    case { 'v', 'vertical' }
        n = length(data);
        p = 1;  
    otherwise
        error('st_plot:subplot_mode', 'subplot mode <%s> unkown', subplot_mode);
end
handle_axes = [];
build_axe = @(ha,hp,hl,ht,hm)struct('axe', ha, 'plots', hp, 'legend', hl, 'title', ht, 'menu', hm);
ha = build_axe({},{},{},{},{});
for d = 1:length(data)
    % added by sivla
    if(isempty(opt.get('layout'))) 
        sh = subplot(n,p,d, 'parent', f);
    else
        layout = opt.get('layout')';
        size_layout = size(layout);
        layout = layout(:);
        mask = find(layout == d);
        sh = subplot(size_layout(2), size_layout(1), mask, 'parent', f);
    end
    % ---------------
    cla;
    handle_axes = [handle_axes, sh];
    % set(sh, 'parent', f);
    if ~st_data('isempty-nl', data(d))
        %< Function plot
        if isfield( data{d}, 'plot_fun')
           feval(data{d}.plot_fun, data{d});
           continue;
        end        
        %>        
        %< Tube plot
        if isfield( data{d}, 'tube')
            clair = @(c)1-(1-c)/5;
            dts = data{d}.tube.date;
            fill([dts;flipud(dts)], [data{d}.tube.value(:,1);flipud(data{d}.tube.value(:,3))], 'r', ...
                'edgecolor', [0 .67 .12], 'facecolor', clair([0 .67 .12]));
            hold on
            plot(dts, data{d}.tube.value(:,2), 'linewidth',2, 'color', [0 .67 .12]);
            
        end
        %>
        %< Alarm plot
        if isfield( data{d}, 'alarm')
            mi = min(data{d}.value(:));
            ma = max(data{d}.value(:));
            a  = data{d}.alarm;
            x  = [a, fliplr(a), a(:,1) repmat(nan,size(a,1),1)]';
            y  = repmat([ma ma mi mi ma nan], size(a,1),1)';
            fill(x(:), y(:), 'r', 'facecolor', [255 138 138]/255, 'edgecolor', [255 138 138]/255);
            hold on
        end
        %>
        if ~isfield( data{d}, 'plot_type')
            plot_type = 'lines';
        else
            plot_type = data{d}.plot_type;
        end
        switch lower(plot_type)
            case { 'line', 'lines' }
                h  = plot(data{d}.date, data{d}.value, 'linewidth', opt.get('linewidth'));
                if cc
                    children = get(gca, 'children');
                    for nch = 1 : length(children)
                        set(children(length(children)-nch + 1), 'color', c_colors(1+mod(nch-1, length(c_colors)), :));
                    end
                end
            case { 'point', 'points' }
                h  = plot(data{d}.date, data{d}.value, '.', 'linewidth', opt.get('linewidth'));
            case { 'stairs', 'stair' }
                h  = stairs(data{d}.date, data{d}.value, 'linewidth', opt.get('linewidth'));
            case { 'stem', 'discrete'}
                h  = stem(data{d}.date, data{d}.value, 'linewidth', opt.get('linewidth'));
                % Too dirty to be used this way
                %         case '2d+color'
                %             axes_str = tokenize(varargin{1});
                %             axe1 =st_data('col', data{d}, axes_str{1});
                %             nb_axe1 = length(axe1);
                %             axe2 =st_data('col', data{d}, axes_str{2});
                %             axe3 =st_data('col', data{d}, axes_str{3});
                %
                %             set(gca, 'Xlim', [min(axe1) max(axe1)]);
                %             set(gca, 'Ylim', [min(axe2) max(axe2)]);
                %             tmp = num2str((1 : length(data{d}.date))');
                %             try
                %                 tmp = datestr(data{d}.date, data{d}.attach_format);
                %             catch end;
                %             cm = hot(256);z_max = max(axe3);z_min = min(axe3);
                %             for count = 1 : nb_axe1
                %                 text(axe1(count), axe2(count), tmp(count, :), 'HorizontalAlignment', 'center', 'color', cm(1 + floor(255 *(axe3(count) - z_min )/(z_max - z_min)), :));
                %             end
                %             set(gcf, 'Colormap', cm);
                %             set(gca, 'xticklabel', []);
                %             set(gca, 'xtick', []);
                %             set(gca, 'yticklabel', []);
                %             set(gca, 'ytick', []);
                %             colorbar;
            otherwise
                error('st_plot:plot_type', 'plot_type <%s> unknown', plot_type);
        end
        ax = axis;
        if length(unique(data{d}.date))>1
            axis([min(data{d}.date), max(data{d}.date), ax(3:4)]);
        end
        lgd = data{d}.colnames;
        % datenum(1970,1,1)
        if min(data{d}.date)>719529 || isfield(data{d}, 'attach_format')
            if isfield(data{d}, 'attach_format');
                attach_date('init', 'date', data{d}.date, 'format', data{d}.attach_format, 'tick_on_dates', opt.get('tick_on_dates'));
            else
                attach_date('init', 'date', data{d}.date, 'tick_on_dates', opt.get('tick_on_dates'))
            end
        end
        if (opt.get('link_axes'))
            linkaxes(handle_axes, 'x');
        end
        %< Gestion des threshold
        % pour l'instant, les thresholds sont intemporelles
        if isfield( data{d}, 'threshold')
            hold on
            thres    = data{d}.threshold;
            nb_thres = numel( thres.value);
            cm       = hsv( nb_thres);
            signs    = '.+o*';
            % patch pour le cas o� je n'ai pas renseign� les rownames des
            % thresholds
            for t=1:length(thres.rownames)
                thres_colname  = thres.rownames{t};
                try
                    vals = st_data('col', data{d}, thres_colname);
                    for c=1:length(thres.colnames)
                        %< Si j'ai une colonne
                        % pour cette threshold
                        u = length(thres.colnames)*(t-1)+c;
                        thres_name = thres.colnames{c};
                        if ~isnan( thres.value(t,c))
                            idx_sup    = vals>thres.value(t,c);
                            h = [h; plot([min(data{d}.date) max(data{d}.date)], repmat(thres.value(t,c),2,1), ':', 'color', cm(u,:))];
                            plot( data{d}.date(idx_sup), vals(idx_sup), signs(mod(u-1,length(signs))+1), 'color', cm(u,:));
                            lgd{end+1} = thres_name;
                        end
                        %>
                    end
                catch
                    st_log('st_plot', 'no column found for threshold <%s>\n', thres_colname);
                end
            end
        end
        %>
        %< XLabel by rownames
        if isfield( data{d}, 'rownames')
            set(gca,'xticklabel', []);
            ax = axis;
            text(data{d}.date, repmat(ax(3), length(data{d}.date),1), data{d}.rownames, 'rotation', -90, ...
                'horizontalalignment', 'left', 'fontsize', 8);
        end
        %>
        if ~opt.get('no_legend')
            lh = legend( h, lgd, 'Location', opt.get('leg_location'), 'Fontname', opt.get('fontname'), 'Box', opt.get('leg_box'), 'EdgeColor', opt.get('leg_edge_color'), 'FontSize', opt.get('leg_FontSize'));
        else
            lh = [];
        end
        if strcmp(opt.get('leg_box'), 'off')
            %set(gca, 'Box', 'off');
            legend(gca, 'boxoff');
        end
        hold off
        if opt.get('title')
            th = title(  data{d}.title, 'tag', 'title');
        else
            th = [];
        end
        mh = st_plot('menu', 'init', gca);
        ha(end+1) = build_axe( sh, h, lh, th, mh);
    end
end

% Addition of a customized data cursor
dcm_obj = datacursormode(gcf);
set(dcm_obj,'UpdateFcn',@update_data_cursor)

h = struct('figure', gcf, 'panel', f, 'axes', ha);

function txt = update_data_cursor(~,event_obj)
% Callback for data cursor
pos = get(event_obj,'Position');
txt = {['Date: ',datestr(pos(1),'dd/mm/yyyy')],...
	      ['Y: ',num2str(pos(2))]};