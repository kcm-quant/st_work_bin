function [st_result] = create_test_result_dataset(mode, varargin)
%% CREATE_TEST_RESULT_DATASET aggregate result from a  list of st_data 
%
% example
%    st_result = create_test_result_dataset( 'matfiles', { .... } );
%    st_result = create_test_result_dataset( 'session', 45 [, 'select', '='] );
%    st_result = create_test_result_dataset( 'last-session' );
%
% take  all the mat file  provided as input and
% aggregate them in a single st_data
% see also create_tactic_test run_python_scheduled_test
global st_version

switch lower(mode)
    case 'session-list'
        c = userdatabase('jdbc_sqlite', 'simep_db' , fullfile(st_version.my_env.simep_repository,'simep_scenarii','metascenario_dict'))
        session_list_num = varargin{1};
        session_list_str = ['(' sprintf('%d,',session_list_num)]; 
        session_list_str(end) = ')';
        matfiles_scenario = exec_sql('simep_db', sprintf( 'select full_filename, simple_scenario_id from simple_scenario where metascenario_session_id in %s', ...
            session_list_str ));
        [matfiles, mdx]  = unique(matfiles_scenario(:,1));
        scenario_id = cell2mat( matfiles_scenario(mdx,2));
        h = waitbar(0,'Extracting parameters  ...') ;
        nb_steps = length(matfiles);
        for m=1:nb_steps
            these_params = exec_sql( 'simep_db', sprintf( 'select param_name, param_value_f from scenario_parameter where simple_scenario_id = %d and param_type = "F"', scenario_id(m)));
            these_params = cat(1, {'matfile', matfiles{m}}, these_params);
            waitbar(m/nb_steps,h,sprintf('processing scenario %d/%d',m,nb_steps));
            if m==1
                all_params = these_params;
            else
                [new_names, tmp, udx] = unique(cat(1,all_params(:,1), these_params(:,1)));
                L = size(all_params,1);
                idx_all   = udx(1:L);
                idx_these = udx(L+1:end);
                K = size(all_params,2);
                new_params = cat(2, new_names, cell(size(new_names,1), K ));
                new_params(idx_all,2:K)   = all_params(:,2:end);
                new_params(idx_these,K+1) = these_params(:,2);
                all_params = new_params;
            end
        end
        
        
        delete(h);
        idx_empty = cellfun(@isempty, all_params);
        all_params(idx_empty) = {NaN};
        matfile_idx = strcmp( 'matfile', all_params(:,1));
        st_result = create_test_result_dataset( 'matfiles', all_params(matfile_idx,2:end)', 'params', all_params(~matfile_idx,1:end)', 'session', session_list_str); 
        
    case 'session'
        c = userdatabase('jdbc_sqlite', 'simep_db' , fullfile(st_version.my_env.simep_repository,'simep_scenarii','metascenario_dict'));
        session_id = varargin{1};
        opt = options({ 'select', '='},varargin(2:end));
        matfiles_scenario = exec_sql('simep_db', sprintf( 'select full_filename, simple_scenario_id, security_id, trading_destination_ids , simulated_date from simple_scenario where metascenario_session_id %s %d', ...
            opt.get('select'), session_id ));
        [matfiles, mdx]  = unique(matfiles_scenario(:,1));
        scenario_id = cell2mat( matfiles_scenario(mdx,2));
        security_id = vertcat(unique(vertcat(matfiles_scenario{mdx,3})));
        td_ids =vertcat(matfiles_scenario{mdx,4});
        sim_date = matfiles_scenario{mdx,5};
        h = waitbar(0,'Extracting parameters  ...') ;
        nb_steps = length(matfiles);
        for m=1:nb_steps
            these_params = exec_sql( 'simep_db', sprintf( 'select param_name, param_value_f from scenario_parameter where simple_scenario_id = %d and param_type = "F"', scenario_id(m)));
            these_params = cat(1, {'matfile', matfiles{m}}, these_params);
            waitbar(m/nb_steps,h,sprintf('processing scenario %d/%d',m,nb_steps));
            if m==1
                all_params = these_params;
            else
                [new_names, tmp, udx] = unique(cat(1,all_params(:,1), these_params(:,1)));
                L = size(all_params,1);
                idx_all   = udx(1:L);
                idx_these = udx(L+1:end);
                K = size(all_params,2);
                new_params = cat(2, new_names, cell(size(new_names,1), K ));
                new_params(idx_all,2:K)   = all_params(:,2:end);
                new_params(idx_these,K+1) = these_params(:,2);
                all_params = new_params;
            end
        end
        
        
        delete(h);
        idx_empty = cellfun(@isempty, all_params);
        all_params(idx_empty) = {NaN};
        matfile_idx = strcmp( 'matfile', all_params(:,1));
        session_str = sprintf('%d-', session_id);
        session_str = ['[' session_str(1:end-1) ']'];
        st_result = create_test_result_dataset( 'matfiles', all_params(matfile_idx,2:end)', 'params', all_params(~matfile_idx,1:end)', 'session', session_str, 'sec_id', security_id, 'td_ids', td_ids, 'simulated_date', sim_date );
        
    case 'last-session'
        opt = options({ 'split_by_security_id', true}, varargin);
        c = userdatabase('jdbc_sqlite', 'simep_db' , fullfile(st_version.my_env.simep_repository,'simep_scenarii','metascenario_dict'));
        last_session = exec_sql( 'simep_db', 'select max(metascenario_session_id) from metascenario_session');
        st_result = create_test_result_dataset( 'session', last_session{1});
    
    case 'matfiles'
        mat_files = varargin{1};
        opt = options({ 'params', {}, 'session', '???','sec_id', NaN, 'td_ids',{}, 'simulated_date', '','use milliseconds', false}, varargin(2:end));
        
        params = opt.get('params');
        
        g = load( mat_files{1});
        st_result = g.A000;
        nb_data = length(g.A000.date);
        if isfield(st_result.info.context,'security_id') 
            st_result  =st_data('add-col', st_result, ones(nb_data,1) .* double(g.A000.info.context.security_id), 'sec_id');
        else
            security_id = opt.get('sec_id');
            if security_id ~= NaN
                st_result  =st_data('add-col', st_result, ones(nb_data,1) .* double(security_id), 'sec_id');
            else
                 error('create_test_result_dataset:mode', 'mode <%s> , not able to find the security_id', mode);
            end
        end
        
        if isfield( st_result.info.context,'trading_destination_id')
            st_result = st_data('add-col', st_result, ones(nb_data,1) .* double(g.A000.info.context.trading_destination_id),'trading_destination_id');
        else
            td_ids = opt.get('td_ids');
            td_list = tokenize(td_ids(2 : end -1),',');
            if  length(td_list) == 1 
                st_result = st_data('add-col', st_result, ones(nb_data,1) .* str2num(td_list{1}),'trading_destination_id');
            else
               st_result = st_data('add-col', st_result, cellstr(repmat(td_ids(1,:),nb_data,1)),'trading_destination_id');
            end
        end
        
        if ~isempty( params)
            st_result  = st_data('add-col', st_result, bsxfun(@times, ones(nb_data,1) , cell2mat( params(2,:))) , ...
                sprintf('%s;', params{1,:}) );
        end
        
        if isfield(g.A000.info.context, 'date')
            my_date  = datenum(g.A000.info.context.date, 'yyyymmdd');
            st_result.date = my_date + st_result.date;
        else
            date = opt.get('simulated_date');
            if ~isempty(date)
                st_result.date = my_date + st_result.date;
            else
                error('create_test_result_dataset:mode', 'mode <%s> , not able to find the trading_destination_ids', mode);
            end
            
        end
        
        
        h = waitbar(0,'Aggregating files ...') ;
        
        nb_steps = length(mat_files);
        for i = 2 : nb_steps
            waitbar(i/nb_steps,h,sprintf('processing file %d/%d',i,nb_steps));
            the_file = mat_files{i};
            g = load( the_file);
            nb_data = length(g.A000.date);
            g.A000  = st_data('add-col', g.A000, ones(nb_data,1) .* double( g.A000.info.context.security_id),'sec_id');
            g.A000  = st_data('add-col', g.A000, cellstr(repmat(td_ids(i,:),nb_data,1)),'trading_destination_id');
            %g.A000  = st_data('add-col', g.A000, ones(nb_data,1) .* double( g.A000.info.context.trading_venue_ids),'trading_venue_id');
            if ~isempty( params)
                g.A000  = st_data('add-col',  g.A000, bsxfun(@times, ones(nb_data,1) , cell2mat( params(1+i,:))) , ...
                    sprintf('%s;', params{1,:}) );
            end
            my_date  = datenum(g.A000.info.context.date, 'yyyymmdd');
            use_millisecond  = opt.get('use milliseconds');
            if use_millisecond 
                g.A000.date = my_date + datenum(0,0,0, 0,0,double(g.A000.date)*1e-6);
            else
                g.A000.date = my_date + g.A000.date;
            end
            st_result = st_data('stack', st_result, g.A000);
        end
        
        delete(h);
        st_result.title = sprintf('Simulations for session %s', opt.get('session'));
        
    otherwise
        error('create_test_result_dataset:mode', 'mode <%s> does not exist', mode);
    
end
