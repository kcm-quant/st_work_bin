function BuildSimepWindows( mode )
	switch mode
		case 'debug'
			mex -g -v -output Simep ../MexCalling.c MexCalling_d.lib C:\python\release\libs\python26.lib
		case 'release'
			mex -v -output Simep ../MexCalling.c MexCalling.lib C:\python\release\libs\python26.lib
		otherwise
			disp 'Wrong parameter, either ''debug'' or ''release'''
	end
end
