function [ allResults, data ] = ConvertSimepOutput( o )
	allResults = fieldnames( o );
	data = {};
	nbResults = size( allResults );
	nbResults = nbResults( 1 );
	for i = 1 : nbResults
		d = o.( allResults{ i } );
		f = fieldnames( d );
		n = size( f );
		n = n( 1 );
		for j = 1 : n
			data = horzcat( data, { d.( f{ j } ) } );
		end
	end
end
