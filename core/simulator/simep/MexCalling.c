#include <mex.h>
#include "BeginEnd.h"

void mexFunction( int nlhs, mxArray* plhs[],
				  int nrhs, const mxArray* prhs[] ) 
{
	if ( !CheckParams( nrhs, prhs) )
		return;

	if ( !Prologue() )
		return;

	Program( nlhs, plhs, nrhs, prhs );

	Epilogue();
}
