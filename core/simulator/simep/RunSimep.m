function [result] = RunSimep()
    disp 'Loading the Simep library'
    disp 'into Matlab...'
    
    result = PythonRunner('simep.wimep.wimep', 'main()' );
end    
   
    
function [result] = PythonRunner( module, func )
   result = Simep(module, func) 
   clear Simep;
end

function [ a, b, c, d, e, f ] = RunSimepOld( varargin )
    nbArgs = size( varargin, 2 );
    disp 'Loading the Simep library'
    disp 'into Matlab...'
    switch nbArgs
        case 0
            [ a, b, c, d, e, f ] = Simep( 0 );
        case 1
            [ a, b, c, d, e, f ] = Simep( 0, varargin{ 1 } );
        case 2
            [ a, b, c, d, e, f ] = Simep( 0, varargin{ 1 }, varargin{ 2 } );
        case 3
            [ a, b, c, d, e, f ] = Simep( 0, varargin{ 1 }, varargin{ 2 }, varargin{ 3 } );
        case 4
            [ a, b, c, d, e, f ] = Simep( 0, varargin{ 1 }, varargin{ 2 }, varargin{ 3 }, varargin{ 4 } );
    end
    clear Simep;
end
