function as_of_date = get_as_of_date(format)
% GET_AS_OF_DATE - fonction permettant de r�cup�rer la as_of_date du SE
% c'est � dire now - decay (mal nomm�, devrait �tre delay)
% cette fonction doit �tre utilis� plut�t que la fonction today, car la date
% du jour pour le SE doit �tre la as_of_date
%
% as_of_date = get_as_of_date()

global st_version;

if isfield(st_version, 'my_env') && isfield(st_version.my_env, 'decay') && isfinite(st_version.my_env.decay) % decay le mal nomm�!
    as_of_date = today - st_version.my_env.decay;
else
    error('as_of_date:exec', 'BAD_USE: Either the "decay" tag is missing in your st_work.xml, or your startup has a problem'); % un jour peut-�tre quelqu'un corrigera dans le xml decay en delay et alors cette fonction ne marchera plus
end

if nargin
    as_of_date = datestr(as_of_date, format);
end

end