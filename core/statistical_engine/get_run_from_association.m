function st_run = get_run_from_association(st_job, varargin)
%GET_RUN_FROM_ASSOCIATION - function that associates a run_id (valid or
%not) to a specific association contained in the structure "st_job"
%
%
% use in get_se
%
% author:  'Dana Croiz�'
% version: '1.0'
% date:    '15/09/2010'
% project: 'get_se'

opt = options({'is_valid',1 , ...
    'as_of_date' ,'', ...
    'retrieve_all_context', false}, varargin);

is_valid        = opt.get('is_valid');
as_of_date      = opt.get('as_of_date');

if opt.get('retrieve_all_context')
    context = [];
else
    context = st_data('cols',st_job,'context_id');
end
job_id  =  st_data('cols',st_job,'job_id');

st_run_id = se_get_run_frm_job ('run' , job_id, 'is_valid', is_valid,'context_id', context,'output', 0, 'as_of_date' , as_of_date);

if length(st_data('col' , st_run_id , 'run_id'))>1 && ~isempty(context)
    error('<get_run_from_association>: There are more than one run_id validated for job_id : %d , context_id : %d  ' , job_id , context)
end

if is_valid %Si un run Valide est demand�
    
    % TODO if retrieve_all_context what happens?
    
    if st_data('isempty-nl' , st_run_id ) %si aucun run valide pour ce job, le dernier run non valide est renvoy�
        st_run_id = se_get_run_frm_job ('run' , job_id, 'is_valid', 0,'context_id', context,'output', 0, 'as_of_date' , as_of_date);
        if ~st_data('isempty-nl' , st_run_id )
            run_id = st_data('cols',st_run_id,'run_id');
            st_run = st_data('add-col', st_job,  run_id(end), 'run_id');
            st_run = st_data('add-col', st_run,  0, 'is_valid');
            st_run.date = st_run_id.date(end);
        else %si aucun run n'a tourn� pour ce job
            st_run = st_data('add-col', st_job,  -1, 'run_id');
            st_run = st_data('add-col', st_run,  -1, 'is_valid');
        end
        
    else 
        run_id = st_data('cols',st_run_id,'run_id');  
        job_id_or_run_id = st_data('cols',st_run_id,'job_id');
        idx_job_id = arrayfun(@(j_id)find(j_id == st_data('col', st_job, 'job_id')), job_id_or_run_id);
        
        st_run = st_job;
        st_run.date = st_run_id.date;
        st_run.value = cell2mat(arrayfun(@(idx)st_job.value(idx,:), idx_job_id, 'uni', false));
        
        st_run = st_data('add-col', st_run,  run_id, 'run_id');
        st_run = st_data('add-col', st_run,  repmat(is_valid, size(run_id)), 'is_valid');
        
        st_run = st_data('drop', st_run, 'context_id');
        st_run = st_data('add-col', st_run, st_data('col', st_run_id, 'context_id'), 'context_id');
    end
    
else %si un run Non VALIDE est demand�
    if ~st_data('isempty-nl' , st_run_id )
        run_id = st_data('cols',st_run_id,'run_id');
        st_run = st_data('add-col', st_job,  run_id(end), 'run_id');
        st_run = st_data('add-col', st_run,  is_valid, 'is_valid');
        st_run.date = st_run_id.date(end);
    else %si aucun run n'a tourn� pour ce job
        st_run = st_data('add-col', st_job,  -1, 'run_id');
        st_run = st_data('add-col', st_run,  -1, 'is_valid');
    end
    
end


end


