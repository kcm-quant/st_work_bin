function params = get_se(mode, varargin)
%GET_SE : fonction qui renvoie en output les param�tres ou run_id calcul�s
%par le STATISTICAL ENGINE pour le quadruplet (estimator_id x security_id x trading_destination_id x varargin)
%
%
% 1. Gestion des options et modes
% Liste des options :
% 'output_mode'
% --params: Renvoie tous les param�tres correspondants � la profondeur appropri�e ou
% demand�e
% -- run:   Renvoie le run correspondant � la profondeur appropri�e ou
% demand�e
%
% 'is_valid'
% -- 0 : Renvoie tout indif�remment du is_valid
% -- 1 : Renvoie uniquement si is_valid == 1
%
%
% exemples :
% En entree : Mode security/avec backup.
% En sortie : liste parametres dans un st_data
% get_se('security','estimator', 'volume curve' , 'security_id' , 110,'trading_destination_id',4,'is_valid', 1 , 'backup_process','backup')
%
% En entree :  Mode security/sans Backup
% En sortie : liste parametres dans un st_data
% get_se('security','estimator', 'volume curve' , 'security_id' , 110,'trading_destination_id',4,'is_valid', 1 , 'backup_process','none')
%
% En entree :  Mode security/avec Backup
% En sortie : run_id dans un st_data
% get_se('security','estimator', 'volume curve','trading_destination_id',6,'varargin_','MB1','is_valid', 1 , 'backup_process','backup', 'output_mode', 'run')
%
% En entree :  Mode security/ niveau specifique
% En sortie : liste parametres dans un st_data
% get_se('security','estimator', 'volume curve', 'security_id' , 110 ,'trading_destination_id', 4,'is_valid', 1 , 'specific_level','first')
%
% En entree :  Mode run/ sans backup
% En sortie : liste parametres dans un st_data
% get_se('run','estimator', 'volume curve', 'run_id',1209998,'backup_process','default')
% get_se('run','estimator', 'volume curve', 'run_id',-1,'security_id',110,'backup_process','default')
%
% use in get_se

% author:  'Dana Croiz�'
% version: '1.0'
% date:    '15/09/2010'


global st_version ;

% 3 modes : 'security'/'run_id'/'masterkey'
switch mode
    case 'masterkey'
        % TODO une vrai clef de bufferisation : prkoi 'output_mode', 'run',
        % ... alors que dans le mode 'security' c'est 'params'!!! et plein
        % d'autres trucs de ce type
        
        optx = options({ ...
            'security_id', [], ...
            'trading_destination_id', [], ...
            'run_id' , NaN , ...
            'from', '01/01/2009', ...
            'to', '01/01/2009', ...
            'context', 'Overall' , ...
            'varargin_', '' , ...
            'output_mode', 'run', ...
            }, {varargin{2:end}});
        
        
        estimator = lower(sprintf('%s', strrep(optx.get('estimator') ,' ','_')));
        
        fmode = varargin{1};
        fmode = regexprep( fmode, '[ |*|\\|/|:|?|"|<|>|\|]', '_');
        
        security_id = optx.get('security_id');
        trading_destination_id = optx.get('trading_destination_id');
        if isempty(trading_destination_id)
            trading_destination_id_str = 'all';
        else
            trading_destination_id_str = regexprep(convs('safe_str', trading_destination_id), '["~]', '');
        end
        
        my_env         = st_version.my_env.target_name ;
        output_mode    = optx.get('output_mode');
        is_valid       = optx.get('is_valid');
        backup_process = optx.get('backup_process');
        run_id       = optx.get('run_id');
        
        if is_valid
            is_valid_str = 'valid';
        else
            is_valid_str = 'unvalid';
        end
        
        switch fmode
            case 'security'
                
                if isempty(security_id)
                    if isempty(optx.get('varargin_'))
                        security = sprintf('index_place_%s',trading_destination_id_str);
                    else
                        varagin_str = optx.get('varargin_');
                        security = sprintf('cotation_group_%s_%s',trading_destination_id_str, varagin_str);
                    end
                else
                    security = get_repository('security-key', security_id);
                    security = sprintf('%s_%s', security, trading_destination_id_str);
                end
                
                params = fullfile( ...
                    my_env , ...
                    fmode, ...
                    estimator, ...
                    backup_process , ...
                    optx.get('context'), ...
                    is_valid_str , ...
                    optx.get('output_mode'), ...
                    security);
                
            case 'run'
                run_str = sprintf('%d' , run_id);
                
                if run_id > 0 && ~isnan(run_id)
                    params = fullfile( ...
                        my_env , ...
                        fmode, ...
                        estimator, ...
                        backup_process , ...
                        run_str);
                else
                    switch backup_process
                        case 'none'
                            run_str = 'EMPTY';
                            params = fullfile( ...
                                my_env , ...
                                fmode, ...
                                estimator, ...
                                backup_process , ...
                                run_str );
                        case 'default'
                             run_str = 'NaN';
                            if ~isempty(security_id)
                                security = get_repository('security-key', security_id);
                                params = fullfile( ...
                                    my_env , ...
                                    fmode, ...
                                    estimator, ...
                                    backup_process , ...
                                    run_str , security);
                            else
                                error('<get_se>/masterkey :: please enter security_id and appropriate date for the parametric estimating ');
                            end
                        otherwise
                            error('<get_se>/masterkey :: Input argument "backup_process" = ''%s'' unknown ' , backup_process);
                    end
                end
                
            otherwise
                error('<get_se> :: Input argument ''mode'' unknown ');
                
        end
        
        
    case  'security'
        opt = options({ 'security_id', [], 'trading_destination_id', [], 'varargin_', '',...
            'run_id' , [] , ...
            'as_of_date', datestr(today,'dd/mm/yyyy'),...
            'output_mode','params',...
            'specific_level' , '' , ...
            'backup_process' , 'none' ,...
            'is_valid',1, ...
            'estimator', '', ...
            'retrieve_all_context', false}, varargin);
        
        retrieve_all_context = opt.get('retrieve_all_context');
        sec_id        = opt.get('security_id');
        trading_destination_id            = opt.get('trading_destination_id');
        varargin_association              = opt.get('varargin_');
        as_of_date      = opt.get('as_of_date');
        is_valid        = opt.get('is_valid');
        specific_level     = opt.get('specific_level');
        backup_process  = opt.get('backup_process');     %none/backup/params
        output_mode = opt.get('output_mode');
        run_id = opt.get('run_id');
        
        estimator_struct =  se_db('get_estimator' , 'estimator_name',opt.get('estimator')) ;
        estimator = lower(sprintf('%s', strrep(opt.get('estimator') ,' ','_')));
        
        %R�cup�ration de toutes les associations pour le sec_id demand�
        
        AllAssociations = feval(str2func(sprintf('get_association_%s', estimator)),sec_id, trading_destination_id, varargin_association, 'as_of_date', as_of_date);
        
        idx = cellfun(@(c)~isempty(c),AllAssociations );
        AllAssociations = AllAssociations(idx);
        
        for i =1 : length(AllAssociations)
            AllAssociations{i} = get_run_from_association(AllAssociations{i}, 'is_valid', is_valid , 'as_of_date' ,as_of_date, 'retrieve_all_context', retrieve_all_context );
        end
        
        %Nature du run renvoy�
        % 'first'   = run r�pondant � tous les crit�res demand�s (sec_id x td x varargin x is_valid)
        % 'backup'  = run pour lequel le run de type 'first' n'a pas �t� trouv�
        % 'default' = valeur du calcul param�trique
        
        [vals, cdk] = codebook('build', {'first','backup','default' });
        cdk.colname = 'depth_of_association' ;
        book = cdk.book;
        
        %R�cup�ration d'une valeur sp�cifique(first/backup/default)
        if ~isempty(specific_level)
            params = get_specific_level(specific_level,'AllAssociations', AllAssociations,'is_valid', is_valid ,'output_mode',output_mode, 'security_id', sec_id ,'trading_destination_id', trading_destination_id ,'varargin_',varargin_association,'as_of_date', as_of_date, 'estimator_id', estimator_struct.estimator_id  );
            return;
        end
        
        %Repli si besoin est: backup_process
        %'none'   : aucun repli n'est appliqu� si aucun run ne reponds aux
        %           crit�res (sec_id x td x varargin x is_valid) demand�s
        %'backup' : repli sur un run dit "de repli" (par exemple: indice/groupe de quotation)
        %'default': si aucun des pr�c�dents n'a retourn� de run possibilit� de renvoy� des param�tres par default
        
        switch backup_process
            case 'none'
                params = get_specific_level('first', 'AllAssociations' ,AllAssociations,'is_valid',is_valid, 'output_mode' ,output_mode, 'security_id' , sec_id ,'trading_destination_id', trading_destination_id ,'varargin_',varargin_association, 'estimator_id', estimator_struct.estimator_id , 'retrieve_all_context', retrieve_all_context  );
                
            case 'backup'
                params = get_se('security','estimator', estimator, 'security_id' , sec_id ,'trading_destination_id', trading_destination_id ,'varargin_',varargin_association, 'is_valid', is_valid , 'backup_process' , 'none', 'output_mode', output_mode ,'as_of_date' ,as_of_date, 'retrieve_all_context', retrieve_all_context );
                if st_data('isempty-nl',params)
                    params = get_specific_level('backup', 'AllAssociations',AllAssociations,'is_valid',is_valid, 'output_mode',output_mode, 'security_id' , sec_id ,'trading_destination_id', trading_destination_id ,'varargin_',varargin_association, 'estimator_id', estimator_struct.estimator_id , 'retrieve_all_context', retrieve_all_context  );
                end
                
            case  'default'
                params = get_se('security','estimator', estimator, 'security_id' , sec_id ,'trading_destination_id', trading_destination_id ,'varargin_',varargin_association, 'is_valid', is_valid , 'backup_process' , 'backup','output_mode', output_mode,'as_of_date' ,as_of_date, 'retrieve_all_context', retrieve_all_context );
                
                %calcul des param�tres par default
                if st_data('isempty-nl',params)
                    params = get_specific_level('default','security_id',sec_id,'as_of_date',as_of_date, 'estimator_id', estimator_struct.estimator_id  );
                end
        end
        
        
    case  'run'   %entr�e en argument run_id x date
        opt = options({ 'security_id', [], 'trading_destination_id', [], 'varargin_', '',...
            'run_id' , [] , ...
            'as_of_date', datestr(today,'dd/mm/yyyy'),...
            'output_mode','params',...
            'specific_level' , '' , ...
            'backup_process' , 'none' ,...
            'is_valid',1, ...
            'estimator', ''}, varargin);
        
        estimator       = opt.get('estimator');
        sec_id          = opt.get('security_id');
        trading_destination_id            = opt.get('trading_destination_id');
        varargin_association              = opt.get('varargin_');
        as_of_date      = opt.get('as_of_date');
        is_valid        = opt.get('is_valid');
        backup_process  = opt.get('backup_process');     %none/backup/params
        output_mode = opt.get('output_mode');
        run_id = opt.get('run_id');
        
        estimator_struct =  se_db('get_estimator' , 'estimator_name', estimator) ;
        estimator = lower(sprintf('%s', strrep(opt.get('estimator') ,' ','_')));
        
        %test NaN
        if (isnan(run_id) || run_id<0) && isempty(sec_id)
            error('<get_se>  run_id : %d not found in the SE, please enter security_id and appropriate date for the parametric estimating', run_id)
        end
        
        
        %run_id pr�sent dans les SE
        params = param_se2st_data(run_id , 'estimator_id' , estimator_struct.estimator_id);
        
        if ~st_data('isempty-nl',params)
            domain_class = se_domain_class ('run_id', run_id);
            params.info.result_type = 'first';
            params.info.is_valid = 0;
            params.info.class = domain_class;
        else
            switch backup_process
                case 'none'
                    params = [];
                case  'default'
                    % le run_id n'existe pas dans le SE, on calcule alors une courbe
                    %parametrique
                    if st_data('isempty-nl',params)
                        params = get_specific_level('default','security_id',sec_id,'as_of_date',as_of_date);
                        params.info.result_type = 'default';
                        params.info.is_valid = 0;
                        domain_class = se_domain_class ('run_id', -1);
                        params.info.class = domain_class ;
                        
                        
                    end
            end
        end
end

    function params = get_specific_level( level, varargin)
        opt = options({'AllAssociations', [], ...
            'is_valid', 0 , ...
            'security_id', [], ...
            'trading_destination_id', [], ...
            'varargin_association', '', ...
            'estimator_id' , NaN , ...
            'output_mode', 'run', ...
            'retrieve_all_context', false}, varargin);
        
        sec_id                 = opt.get('security_id');
        trading_destination_id = opt.get('trading_destination_id');
        varargin_association   = opt.get('varargin_association');
        AllAssociations        = opt.get('AllAssociations');
        is_valid               = opt.get('is_valid');
        estimator_id           = opt.get('estimator_id');
        output_mode            = opt.get('output_mode');
        
        
        switch level
            case 'default'
                as_of_date    = opt.get('as_of_date');
                if ~isempty(sec_id)
                    params = feval(str2func(sprintf('se_create_parametric_%s', estimator)),'security_id' , sec_id,'date',as_of_date);
                    
                    if ~isempty(params)
                        params.info.result_type = 'default';
                        params.info.is_valid = 0;
                        params.info.stamp_date = datenum(as_of_date , 'dd/mm/yyyy');
                        params.info.class = se_domain_class ('run_id', -1);
                        
                    else
                        params = [];
                    end
                else
                    error('<get_se>  run_id : %d not found in the SE, please enter sec_id and appropriate date for the parametric estimating', run_id)
                end
                
            otherwise
                if ~isempty(AllAssociations)
                    asso_valid = cellfun(@(c)all(st_data('cols', c,'is_valid')), AllAssociations ); % all because evyr run of an estimation are valid or none
                    asso_rank = cellfun(@(c)max(st_data('cols', c,'rank')), AllAssociations );% max because there is only one assoc for all copntext => one ranking
                    asso_depth_of_association = cellfun(@(c)book(max(st_data('cols', c,'depth_of_association'))), AllAssociations );% idem
                    
                    if is_valid
                        index = find(strcmpi(asso_depth_of_association, level));
                        
                        if ~isempty(index) && asso_valid(index)== 1
                            run_id =  st_data('cols' , AllAssociations{index} , 'run_id');
                            domain_class = se_domain_class ('run_id', run_id);
                            
                            if strcmpi(output_mode, 'params')
                                params = param_se2st_data(st_data('cols',AllAssociations{index},'run_id') ,'estimator_id' , estimator_id);
                                params.info.class = domain_class;
                            else
                                params = st_data( 'init', 'title', 'SE run_id', 'value', [run_id  st_data('cols' ,domain_class,'class')], ...
                                    'date', AllAssociations{index}.date, 'colnames', {'run_id', 'class'});
                                params.codebook = domain_class.codebook ;
                            end
                            params.info.result_type = sprintf('%s',level);
                            params.info.is_valid = is_valid;
                            
                        else
                            st_log('<get_se>: No valid %s association for triple (sec_id: %d td:%d varargin: %s) ',upper(level),sec_id,trading_destination_id,varargin_association)
                            params = [];
                        end
                        
                    else
                        index = find(strcmpi(asso_depth_of_association, level));
                        if asso_valid(index)==0
                            run_id =  st_data('cols' , AllAssociations{index} , 'run_id');
                            domain_class = se_domain_class ('run_id', run_id);
                            
                            if strcmpi(output_mode, 'params')
                                params = param_se2st_data(st_data('cols',AllAssociations{index},'run_id'), 'estimator_id' , estimator_id);
                                params.info.class = domain_class;
                            else
                                params = st_data( 'init', 'title', 'SE run_id', 'value', [run_id  st_data('cols' ,domain_class,'class')], ...
                                    'date', AllAssociations{index}.date, 'colnames', {'run_id', 'class'});
                                params.codebook = domain_class.codebook ;
                                
                            end
                            params.info.result_type = sprintf('%s',level);
                            params.info.is_valid = is_valid;
                        else
                            st_log('<get_se>: No %s association for triple (sec_id: %d td:%d varargin: %s) ',upper(level),sec_id,trading_destination_id,varargin_association)
                            params = [];
                        end
                    end
                else
                    st_log('<get_se>: No %s association for triple (sec_id: %d td:%d varargin: %s) ',upper(level),sec_id,trading_destination_id,varargin_association)
                    params = [];
                    
                end
        end
        
    end

end

