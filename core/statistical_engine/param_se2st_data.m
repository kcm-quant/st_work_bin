function data = param_se2st_data(run_id, varargin )
%PARAM_SE2ST_DATA : funtion that returns run_id's parameters in st_data
%
% example : param_se2st_data(826717)
% use in get_se

% author:  'Dana Croiz�'
% version: '1.0'
% date:    '15/09/2010'


opt = options({'estimator_id' , NaN}, varargin) ;


estimator_run = cell(length(run_id), 1);


if  ~isnan(run_id(1))&& isnumeric(run_id(1)) && run_id(1)>0
    estimator_run{1} = se_db('get_estimator_run', 'run_id', run_id(1));
    job = se_db('get_job', 'job_id', estimator_run{1}.job_id);
    param_desc = se_db('get_param_desc', 'estimator_id', job.estimator_id);
%     [~, idx_s] = sort([param_desc.parameter_id]);
%     param_desc = param_desc(idx_s);
    
    context = cell(length(run_id), 1);
    
    for i = 1 : length(run_id)
        estimator_run{i} = se_db('get_estimator_run', 'run_id', run_id(i));
        
        if isempty(estimator_run{i})
            st_log('<param_se2st_data>  NO such run_id in the SE, in %s environment \n ' , upper(st_version.my_env.target_name));
            data = st_data( 'empty-init');
            return;
        end
        
        if ~isequal(estimator_run{i}.estimator_id, opt.get('estimator_id'))
            error('<param_se2st_data> :: there is no job with run_id =%d associated with estimator_id= %d ' , run_id , opt.get('estimator_id') )
        end
        
        
        
        context{i} = se_db('get_context', 'context_id', estimator_run{i}.context_id);
        param_value = se_db('get_param_value', 'run_id', run_id(i));
        [~, idx_s] = sort([param_value.parameter_id]);
        param_value = param_value(idx_s);
        
        % Create an st_data that contains the list of the parameters returned by se_run
        
        if isempty( param_value)
            error('se_check:local', 'SE_JOB_PARAMS: param_value is empty');
        end
        data_value = [param_value.value]';
        
        data_colnames = context{i}.context_name;
        
        data_rownames = {param_desc(:).parameter_name}';
        
        if i == 1
            a=[param_value.parameter_id];
            b = [param_desc(:).parameter_id];
            [c, ia, ib] = intersect(a, b);
            
            param_desc = param_desc(ib);
        else
            assert(all([param_value.parameter_id] == [param_desc.parameter_id]));
        end
        
        if i == 1
            
            run_stamp_date = datenum(estimator_run{1}.stamp_date);
            
            data = st_data( 'init', 'title', 'SE parameters', 'value', data_value, ...
                'date', (1:size(data_value, 1))' , ...
                'colnames', data_colnames, ...
                'rownames', data_rownames(ib)');
            
            data.info.stamp_date = run_stamp_date ;
            
        else
            data.value = [data.value, data_value];
            data.colnames = cat(2, data.colnames, data_colnames);
        end
    end
else
    
    data = st_data( 'empty-init');
    
end
