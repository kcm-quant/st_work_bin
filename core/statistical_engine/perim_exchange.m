function out = perim_exchange(mode,varargin)
% Exceptionnellement cette fonction de mise a jour du perimetre "exchange"
% ne fait pas appelle a la fonction se_create_domain.
%
% Exemple :
% perim_exchange('update',[])

global st_version
quant = st_version.bases.quant;
mail_to = st_version.my_env.se_admin;

opt = options ({'params',''}, varargin);
try
    % read xml file
    [DOMAIN_TYPE_RANK, DOMAIN_TYPE] = load_xml_params('perim_exchange.xml');
    domain_type_id = cell2mat(exec_sql('QUANT',...
        ['select domain_type_id from ',quant,'..domain_type where domain_type_name = ''',DOMAIN_TYPE,'''']));
    is_ok = 1;
    
    switch mode
        case {'update','virtual'}
            params = eval(opt.get('params'));
            o = options(params);
            lst_ = o.get();
            opt.set(lst_{:});
            exchgid_list = opt.get('exchgid_list');
            
            perimeter_id = cell2mat(exec_sql('QUANT',['select perimeter_id from ',quant,'..perimeter where perimeter_name = ''exchange''']));
            domain_names = [strcat('exchange/',exchgid_list,' primary'),strcat('exchange/',exchgid_list,' consolidated')]';
            domain_type_variable = [strcat(exchgid_list,' primary'),strcat(exchgid_list,' consolidated')]';
            domain_comments = repmat({''},length(domain_type_variable),1);
            switch lower(mode)
                case 'update'
                    
                    dtv_list = sprintf('''%s'',',domain_type_variable{:});
                    dtv_list(end) = [];
                    domain_present = exec_sql('QUANT',['select d.domain_id, d.domain_type_variable, p.is_active',...
                        ' from ',quant,'..domain d,',quant,'..perimeter_domain p',...
                        ' where d.domain_type_variable in (',dtv_list,')',...
                        ' and d.domain_id = p.domain_id']);
                    domain_present = reshape(domain_present,[],3);
                    reactivate = intersect(domain_type_variable,domain_present(cell2mat(domain_present(:,3))==0,2));
                    deactivate = setdiff(domain_present(cell2mat(domain_present(:,3))==1,2),...
                        domain_type_variable);
                    create = setdiff(domain_type_variable,domain_present(:,2));
                    
                    % Domain reactivation
                    if ~isempty(reactivate)
                        list_str = sprintf('''%s'',',reactivate{:});
                        list_str(end) = [];
                        exec_sql('QUANT',['update p set p.is_active = 1',...
                            ' from ',quant,'..perimeter_domain p,',quant,'..domain d',...
                            ' where p.domain_id = d.domain_id',...
                            ' and d.domain_type_variable in (',list_str,')']);
                    end
                    
                    % Domain deactivation
                    if ~isempty(deactivate)
                        list_str = sprintf('''%s'',',deactivate{:});
                        list_str(end) = [];
                        exec_sql('QUANT',['update p set p.is_active = 0',...
                            ' from ',quant,'..perimeter_domain p,',quant,'..domain d',...
                            ' where p.domain_id = d.domain_id',...
                            ' and d.domain_type_variable in (',list_str,')']);
                    end
                    
                    % Domain creation: impacted tables are "domain" and "perimeter_domain"
                    if ~isempty(create)
                        idx = ismember(domain_type_variable,create);
                        temp = [domain_names(idx), domain_comments(idx), domain_type_variable(idx),...
                            repmat(num2cell([domain_type_id,  DOMAIN_TYPE_RANK]),sum(idx),1)]';
                        str = sprintf('(''%s'',''%s'',''%s'',%d,%d),',temp{:});
                        str(end) = [];
                        exec_sql('QUANT',['insert into QUANT..domain',...
                            ' (domain_name, comment, domain_type_variable, domain_type_id, domain_type_rank)',...
                            ' values ',str]);
                        list_str = sprintf('''%s'',',create{:});
                        list_str(end) = [];
                        created_dom_id = cell2mat(exec_sql('QUANT',['select domain_id',...
                            ' from QUANT..domain',...
                            ' where domain_type_variable in (',list_str,')']));
                        temp = [created_dom_id,repmat([perimeter_id,1],length(created_dom_id),1)]';
                        str = sprintf('(%d,%d,%d),',temp);
                        str(end) = [];
                        exec_sql('QUANT',['insert into QUANT..perimeter_domain (domain_id, perimeter_id, is_active)',...
                            ' values ',str]);
                    end
                    
                    r = 0;
                    
                case 'virtual'
                    r = struct('perimeter_id', perimeter_id, 'domain_names', domain_names, 'domain_sec', domain_sec_ids, ...
                        'domain_type_variable', domain_type_variable, 'domain_comments', domain_comments, ...
                        'domain_type', DOMAIN_TYPE, 'domain_type_rank', DOMAIN_TYPE_RANK);
            end
            
            out = struct('perimeter_id', r, 'is_successfully_update', is_ok);
            
        case 'association'
            
            base = varargin{1};
            rank = varargin{2};
            domain_type_variable = varargin{3};
            estimator_id = varargin{4};
            job_id = varargin{5};

            if length(varargin)>=6
                context_num = varargin{6};
            else
                context_num = 1;
            end
            
            domain_type_variable = regexp(domain_type_variable,' ','split');
            td_id = cell2mat(exec_sql('KGR',['select EXCHANGE from KGR..EXCHANGEREFCOMPL',...
                ' where EXCHGID = ''',domain_type_variable{1},'''']));
            conso_type = domain_type_variable{2};
        
            context_id = cell2mat(exec_sql('QUANT', sprintf( ...
                'SELECT context_id FROM %s..context WHERE estimator_id=%d AND context_num=%d',...
                base, estimator_id ,context_num)));
            next_ass_id = cell2mat(exec_sql('QUANT', ...
                sprintf(['SELECT next_association_id',...
                ' FROM %s..next_association' ...
                ' where context_id=%d AND rank=%d',...
                ' and security_id is NULL AND trading_destination_id=%d',...
                ' and estimator_id=%d AND job_id=%d and varargin = ''%s'''], ...
                base, context_id, rank, td_id, estimator_id, job_id, conso_type)));

        if isempty(next_ass_id)
            exec_sql('QUANT', sprintf([ ...
                'INSERT INTO %s..next_association (context_id, rank, security_id,',...
                'trading_destination_id, estimator_id, job_id, varargin, from_meta)',...
                ' VALUES (%d, %d, NULL, %d, %d, %d, ''%s'',1)'], ...
                base, context_id, rank, td_id, estimator_id, job_id, conso_type));
        end
        
    end
catch e
    switch mode
        case 'update'
            subject = 'ERREUR LORS DE LA MISE A JOUR DU PERIMETRE <EXCHANGE>';
            message = sprintf('Identifiant : %s\nMessage : %s',e.identifier,e.message);
            sendmail(mail_to,subject,message)
            
            out = struct('perimeter_id', 0, 'is_successfully_update', 0);
        otherwise 
            rethrow(e)
    end
end


function [domain_type_rank, domain_type] = load_xml_params(filename)
xml = xmlread(filename);
root = get_unique_node(xml,'perimeter');
assert(~isempty(root),'xml root must be <perimeter>')
definition = get_unique_node(root,'definition');
assert(~isempty(definition),'No node <definition>')
params = get_list_node(definition,{'param'});
name = get_attribute_value(params,'name');
x_value = get_attribute_value(params,'x_value');
domain_type_rank = str2double(x_value{strcmp(cellfun(@char,name,'uni',false),'DOMAIN_TYPE_RANK')});
domain_type = char(x_value{strcmp(cellfun(@char,name,'uni',false),'DOMAIN_TYPE')});

function fils = get_unique_node(pere,name)
aine = pere.getFirstChild;
benjamin = pere.getLastChild;
frere = aine;
fils = [];
while 1
    if frere.getNodeType==frere.ELEMENT_NODE && strcmp(frere.getNodeName,name)
        fils = frere;
    end
    if isequal(frere,benjamin)
        break
    end
    frere = frere.getNextSibling;
end

function fils = get_list_node(pere,name)
aine = pere.getFirstChild;
benjamin = pere.getLastChild;
frere = aine;
fils = {};
while 1
    if frere.getNodeType==frere.ELEMENT_NODE && any(strcmp(frere.getNodeName,name))
        fils{end+1} = frere; %#ok<AGROW>
    end
    if isequal(frere,benjamin)
        break
    end
    frere = frere.getNextSibling;
end

function value = get_attribute_value(node,name)
value = cell(length(node),1);
for i = 1:length(node)
    attribs = node{i}.getAttributes;
    attrib = attribs.getNamedItem(name);
    if  ~isempty(attrib)
        value{i} = attrib.getValue;
    end
end