function out = perim_lmi(mode, varargin)
% PERIM_LMI- generation of a perimeter of European Main Index
%
% use:
%  perim_lmi('update')
%  perim_lmi('create')
%  perim_lmi('virtual')
%
%
% perim_lmi('association', 'quant', 10, 'SMI', 2, 666)

global st_version;
quant = st_version.bases.quant;
repository = st_version.bases.repository;
market_data = st_version.bases.market_data;

opt = options ({'params',''}, varargin);

% HARD CODED VARIABLES
DOMAIN_TYPE = 'place index';
DOMAIN_TYPE_RANK = 20;
NB_DEAL_LIMIT_FOR_ASE = 500;
NB_DEAL_LIMIT_FOR_SPI = 1000;
NB_DEAL_LIMIT_FOR_ISEQ = 225;
TD_ID_OF_ZURICH = 18;
TD_ID_OF_DUBLIN = 39;

is_successfully_update =1;  %variable qui permet de remonter s'il y a eu une erreur lors de la mise � jour d'un indice

%R�cup�ration des informations du p�rim�tre � partir de QUANT
out = exec_sql('QUANT',['SELECT perimeter_name , perimeter_id  from ',quant,...
    '..perimeter where request = ''',mfilename,''' ']);
colnames = {'perimeter_name', 'perimeter_id' };
idx_p_name  = strmatch( 'perimeter_name', colnames, 'exact');
idx_p_id  = strmatch( 'perimeter_id', colnames, 'exact');

PERIMETER_NAME = cell2mat(out(:,idx_p_name));
perimeter_id  = cell2mat(out(:,idx_p_id)) ;

%mail
address = exec_sql('QUANT',['SELECT owner from ',quant,'..estimator GROUP BY owner']);

if ispc()
    blank_char = sprintf('\n');
else
    blank_char = sprintf('\\n');
end


switch lower(mode)
    
    case {'update', 'virtual'}
        
        dir_path = fullfile(getenv('st_repository'), 'log', 'perim_update');
        if isempty(dir(dir_path))
            mkdir(dir_path);
        end
        log_path = fullfile(dir_path , sprintf('%s_update.txt', PERIMETER_NAME));
        st_log('$out$', log_path);
        
        %R�cuperation des param�tres du p�rim�tre
        params = eval(opt.get('params'));
        o = options(params);
        parse_params = str2opt(opt2str(o));
        lst_ = parse_params.get();
        opt.set( lst_{:});
        
        INDEX_NAME = opt.get('index');
        if isempty(INDEX_NAME)
            st_log(sprintf('WARNING : index_list (that is needed for the perimeter''s construction) is empty '))
        end
        
        % Contruction des param�tres
        % **************************
        domain_sec_ids = cell(1, length(INDEX_NAME));
        domain_type_variable = cell(1, length(INDEX_NAME));
        domain_comments = cell(1, length(INDEX_NAME));
        
        % domain names
        perim_name = cell2mat(exec_sql('QUANT',...
            ['SELECT perimeter_name ' ...
            'FROM ',quant,'..perimeter ' ...
            'WHERE perimeter_id=',num2str(perimeter_id)]));
        domain_names = cellfun(@(x) [perim_name '/' x], INDEX_NAME, 'UniformOutput', false);
        
        for i=1:length(domain_sec_ids)
            
            % domain_type_variable
            
            %             em_id = get_repository('place', 'index-name', INDEX_NAME{i});
            %             domain_type_variable{i} = [num2str(em_id)];
            domain_type_variable{i} = INDEX_NAME{i};
            
            % security ids
            try
                sec_id = get_repository('index-comp', INDEX_NAME{i},'recent_date_constraint', false);
                sec_id = check_data(sec_id);
                % Rustine pour l'ASE
                if strcmp(INDEX_NAME{i},'ASE')
                    sec_str = sprintf('%d,',sec_id);
                    sec_id = cell2mat(exec_sql('MARKET_DATA',['select td.security_id',...
                        ' from ',market_data,'..trading_daily td,',repository,'..security_market sm',...
                        ' where sm.security_id = td.security_id',...
                        ' and sm.trading_destination_id = td.trading_destination_id',...
                        ' and sm.ranking = 1',...
                        ' and td.date between ''',datestr(today-7-90,'yyyy-mm-dd'),''' and ''',datestr(today-7,'yyyy-mm-dd'),'''',...
                        ' and td.security_id in (',sec_str(1:end-1),')',...
                        ' group by td.security_id',...
                        ' having avg(td.nb_deal) > ',num2str(NB_DEAL_LIMIT_FOR_ASE)]));
                end
                % Derniere ligne de la rustine
                
                % Rustine pour SPI (Indice Suisse). On ne garde que Zurich.
                if strcmp(INDEX_NAME{i},'SPI')
                    sec_str = sprintf('%d,',sec_id);
                    sec_id = cell2mat(exec_sql('MARKET_DATA',['select security_id',...
                        ' from ',market_data,'..trading_daily',...
                        ' where trading_destination_id = ',num2str(TD_ID_OF_ZURICH),...
                        ' and date between ''',datestr(today-7-90,'yyyy-mm-dd'),''' and ''',datestr(today-7,'yyyy-mm-dd'),'''',...
                        ' and security_id in (',sec_str(1:end-1),')',...
                        ' group by security_id',...
                        ' having avg(nb_deal) > ',num2str(NB_DEAL_LIMIT_FOR_SPI)]));
                end
                % derniere ligne de la rustine
                
                % Rustine pour ISEQ (Indice Irlandais). On ne que les valeurs qui ont nombre de trades journalier minimum.
                if strcmp(INDEX_NAME{i},'ISEQ')
                    sec_str = sprintf('%d,',sec_id);
                    sec_id = cell2mat(exec_sql('MARKET_DATA',['select security_id',...
                        ' from ',market_data,'..trading_daily',...
                        ' where trading_destination_id = ',num2str(TD_ID_OF_DUBLIN),...
                        ' and date between ''',datestr(today-7-90,'yyyy-mm-dd'),''' and ''',datestr(today-7,'yyyy-mm-dd'),'''',...
                        ' and security_id in (',sec_str(1:end-1),')',...
                        ' group by security_id',...
                        ' having avg(nb_deal) > ',num2str(NB_DEAL_LIMIT_FOR_ISEQ)]));
                end
                % derniere ligne de la rustine
                
                td = arrayfun(@(x)min(get_repository( 'trading-destination', x)), sec_id);
                domain_sec_ids{i} = struct('security_id', num2cell(sec_id), 'trading_destination_id', num2cell(td));
                
            catch e
                st_log(sprintf('ERROR : %s\n', e.message));
                
                error_message = se_stack_error_message(e);
                if ~ispc()
                    error_message = regexprep(error_message, '\n', '\r');
                end
                %envoie de mail
                identification_err = sprintf('%s%s%s', 'ECHEC DANS LA MISE A JOUR DE L''INDICE:',sprintf('%s%s',upper(INDEX_NAME{i}),blank_char),error_message);
                subject = sprintf('Erreur de mise a jour de l''indice:%s, perimetre:%s %s' ,upper(INDEX_NAME{i}),upper(PERIMETER_NAME), hostname());
                
                for j=1:length(address)
                    se_sendmail( address{j},subject,identification_err)
                end
                
                try  %s'il n'a pas r�ussi � r�cup�rer les sec_id ou trading-destination , one essaie de les r�cuperer dans la base quant/quant_homolo
                    security_td = exec_sql('QUANT',['SELECT security_id , trading_destination_id  ' ...
                        'FROM ',quant,'..domain_security ' ...
                        'WHERE domain_id in '...
                        '(SELECT max(domain_id) FROM ',quant,'..domain ' ...
                        'WHERE domain_name = ''',domain_names{i},''') ']) ;
                    domain_sec_ids{i} = struct('security_id', security_td(:,1),...
                        'trading_destination_id', security_td(:,2));
                catch e
                    st_log(sprintf('ERROR : %s\n', e.message));
                    
                    error_message = se_stack_error_message(e);
                    if ~ispc()
                        error_message = regexprep(error_message, '\n', '\r');
                    end
                    %envoie de mail
                    identification_err = sprintf('%s%s%s','ECHEC DANS LA RECUPERATION DES SECURITY OU TD A PARTIR DE LA BASE QUANT POUR L''INDICE :',sprintf('%s%s',upper(INDEX_NAME{i}),blank_char),error_message);
                    subject = sprintf('Erreur de mise a jour de l''indice:%s, ATTENTION L''INDICE A ETE SUPPRIME DU PERIMETRE:%s %s' ,upper(INDEX_NAME{i}),upper(PERIMETER_NAME), hostname());
                    
                    for j=1:length(address)
                        se_sendmail( address{j},subject,identification_err)
                    end
                    
                end
                
                is_successfully_update = 0;
                
            end
            
            % domain_comments
            domain_comments{i} = '';
            
        end
        
        
        
        % Nettoyage du p�rim�tre
        idx = se_remove_perim(domain_sec_ids);
        domain_sec_ids(idx) = [];
        domain_type_variable(idx) = [];
        domain_comments(idx) = [];
        domain_names(idx) = [];
        
        % Ex�cution de la cr�ation
        % ************************
        
        switch lower(mode)
            case 'update'
                se_create_domain(perimeter_id, domain_names, domain_sec_ids, domain_type_variable, domain_comments, DOMAIN_TYPE, DOMAIN_TYPE_RANK);
                r=0;
                
            case 'virtual'
                r = struct('perimeter_id', perimeter_id, 'domain_names', domain_names, 'domain_sec', domain_sec_ids, ...
                    'domain_type_variable', domain_type_variable, 'domain_comments', domain_comments, ...
                    'domain_type', DOMAIN_TYPE, 'domain_type_rank', DOMAIN_TYPE_RANK);
        end
        
        out = struct('perimeter_id' ,r, 'is_successfully_update',is_successfully_update);
        
        st_log('$close$');
        
        
        
        
        
    case 'association' % varargin = {$b, $r, $v, $e, $j)
        
        % Cr�ation des associations
        % *************************
        
        opt = options( { ...
            'quant_table', 'next_association' , ...
            }, varargin);
        
        table = opt.get('quant_table');
        if ~strcmpi(table,'next_association')
            st_log('WARNING:::::::::::::::::::::::::::<%s> inserst associations in the TABLE : %s ' ,mfilename , table )
        end
        
        base = varargin{1};
        rank = varargin{2};
        index_name = varargin{3};
        estimator_id = varargin{4};
        job_id = varargin{5};
        
        if nargin>6
            context_num = varargin{6};
        else
            context_num = 1;
        end
        
        
        td_id = get_main_tds_for_index(index_name);
        
        % On teste l'existence de l'association avant d'eventuellement
        % l'ins�rer
        context_id = cell2mat(exec_sql('QUANT', sprintf( ...
            'SELECT context_id FROM %s..context WHERE estimator_id=%d AND context_num=%d', base, estimator_id ,context_num)));
        
        association_id = cell2mat(exec_sql('QUANT', ...
            sprintf(['SELECT %s_id FROM %s..%s  WHERE ' ...
            'context_id=%d AND rank=%d AND security_id is NULL AND trading_destination_id=%d AND estimator_id=%d AND job_id=%d'], ...
            table, base, table, context_id, rank, td_id(1, 1), estimator_id, job_id)));
        
        if isempty(association_id)
            exec_sql('QUANT', sprintf([ ...
                'INSERT INTO %s..%s (context_id, rank, security_id, trading_destination_id, estimator_id, job_id) ' ...
                'VALUES (%d, %d, NULL, %d, %d, %d)'], ...
                base, table, context_id, rank, td_id(1, 1),...
                estimator_id, job_id));
        end
        
        
    otherwise
        
        error('perim_lmi:mode', '<%s> unknown', mode);
        
end

function sec_list = check_data(sec_list)
global st_version
market_data = st_version.bases.market_data;
sec_str = join(',',sec_list);
sec_list = cell2mat(exec_sql('MARKET_DATA',['select security_id',...
    ' from ',market_data,'..trading_daily',...
    ' where date between ''',datestr(today-30-90-7,'yyyy-mm-dd'),''' and ''',datestr(today-90-7,'yyyy-mm-dd'),'''',...
    ' and security_id in (',sec_str,')',...
    ' group by security_id']));

function str = join(separator,list)
if isempty(list)
    str = '';
    return
end
if isnumeric(list)
    str = sprintf(['%d',separator],list);
elseif iscellstr(list)
    str = sprintf(['%s',separator],list{:});
else
    error('join:wronginputargs','Second argument should be either numeric or cellstr')
end
str(end-length(sprintf(separator))+1:end) = [];