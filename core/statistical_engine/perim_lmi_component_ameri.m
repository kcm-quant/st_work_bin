function out = perim_lmi_component_ameri(mode, varargin)
% PERIM_LMI_COMPONENT_AMERI - generation of a perimeter of Latin American
% Major Indices
%
% use:
%  perim_lmi_component('update')
%  perim_lmi_component('create')
%  perim_lmi_component('virtual')



global st_version;
sv = st_version.bases.quant;

opt = options ({'params',''}, varargin);

% *****
DOMAIN_TYPE = 'mono instrument';
DOMAIN_TYPE_RANK = 2;

is_successfully_update =1;

%mail
address = exec_sql('QUANT', sprintf('SELECT owner from %s..estimator GROUP BY owner', sv));

%R�cup�ration des informations du p�rim�tre � partir de QUANT
out = (exec_sql('QUANT', sprintf('SELECT perimeter_name , perimeter_id  from %s..perimeter where request = ''%s'' ', sv,mfilename)));
colnames = {'perimeter_name', 'perimeter_id' };
idx_p_name  = strmatch( 'perimeter_name', colnames, 'exact');
idx_p_id  = strmatch( 'perimeter_id', colnames, 'exact');

PERIMETER_NAME = cell2mat(out(:,idx_p_name));
perimeter_id  = cell2mat(out(:,idx_p_id)) ;

if ispc()
    blank_char = sprintf('\n');
else
    blank_char = sprintf('\\n');
end

switch lower(mode)
    
    case {'update', 'virtual'}
        
        dir_path = fullfile(getenv('st_repository'), 'log', 'perim_update');
        if isempty(dir(dir_path))
            mkdir(dir_path);
        end
        
        log_path = fullfile(dir_path , sprintf('%s_update.txt', strrep(PERIMETER_NAME ,' ','_')));
        st_log('$out$', log_path);
        
        %R�cuperation des param�tres du p�rim�tre
        params = eval(opt.get('params'));
        o = options(params);
        parse_params = str2opt(opt2str(o));
        lst_ = parse_params.get();
        opt.set( lst_{:});
           
        INDEX_NAME = opt.get('index');
        if isempty(INDEX_NAME)
            st_log(sprintf('WARNING : index_list (that is needed for the perimeter''s construction) is empty '))
        end
            
        
        % Contruction des param�tres
        % **************************
        domain_sec_ids = {};
        domain_type_variable = {};
        domain_comments = {};
        domain_names = {};
        
        sec_id_present_inside = cell2mat(exec_sql('QUANT',sprintf([...
            'select s.security_id from %s..domain_security s,%s..perimeter_domain p,%s..perimeter pp'...
            ' where p.domain_id=s.domain_id'...
            ' and p.perimeter_id=pp.perimeter_id'...
            ' and p.is_active = 1'...
            ' and pp.perimeter_name=''lmi components ameri'''],sv,sv,sv )));
        
        max_runtime = opt.get('max_runtime');
        beta = opt.get('beta');
        min_nb_of_deals_per_day = opt.get('minimum_nb_of_deals_per_day');
        
        try
            sec_id = get_index_comp(regexprep(sprintf('%s UNION ',INDEX_NAME{:}),' UNION $',''), 'recent_date_constraint', false);
            sec_nb_deal = exec_sql('BSIRIUS',sprintf([...
                'select security_id,avg(nb_deal)'...
                ' from tick_db..trading_daily_ameri where security_id in (%s)'...
                ' and trading_destination_id is null and date between ''%s'' and ''%s'''...
                ' group by security_id having avg(nb_deal) > %d'],...
                regexprep(sprintf('%d,',sec_id),',$',''),datestr(today-45,'yyyymmdd') , datestr(today-15,'yyyymmdd'),min_nb_of_deals_per_day));
            sec_id_present_inside = intersect(cell2mat(sec_nb_deal(:,1)),sec_id_present_inside);
            sec_nb_deal(ismember(cell2mat(sec_nb_deal(:,1)),sec_id_present_inside),:) = [];
            sec_id = cell2mat(sec_nb_deal(cumsum(cell2mat(sec_nb_deal(:,2))*beta(2)+beta(1))<=max_runtime*60,1));
            sec_id = [sec_id_present_inside;sec_id];
            for j=1:length(sec_id)

                td = get_repository( 'any-to-sec-td', 'security_id', sec_id(j));
                domain_sec_ids{end+1} = struct('security_id', num2cell(sec_id(j)), 'trading_destination_id', td.trading_destination_id(1));%#ok
                % domain_type_variable
                domain_type_variable{end+1} = [num2str(sec_id(j)) ',' num2str(td.trading_destination_id(1))];%#ok
                % domain_comments
                domain_comments{end+1} = '';%#ok
                % domain names
                domain_names{end+1} = [PERIMETER_NAME '/' td.security_key];%#ok

            end
                
            
        catch e
            st_log(sprintf('ERROR : %s\n', e.message));
            
            error_message = se_stack_error_message(e);
            if ~ispc()
                error_message = regexprep(error_message, '\n', '\r');
            end
            %envoie de mail
            identification_err = sprintf('%s%s%s', 'ECHEC DANS LA MISE A JOUR DU PERIMETRE:',sprintf('%s%s',upper(PERIMETER_NAME),blank_char),error_message);
            subject = sprintf('Erreur de mise a jour du perimetre:%s %s' ,upper(PERIMETER_NAME), hostname());
            
            for j=1:length(address)
                se_sendmail( address{j},subject,identification_err)
            end
            
            domain_sec_ids = {};
            domain_type_variable = {};
            domain_comments = {};
            domain_names = {};
            
            try %s'il n'a pas r�ussi � r�cup�rer les sec_id ou trading-destination , one essaie de les r�cuperer dans la base quant/quant_homolo
                security_td = (exec_sql('QUANT', sprintf(['SELECT dom_sec.security_id , dom_sec.trading_destination_id , ' ...
                    ' substring(dom.domain_name,charindex(''/'',dom.domain_name)+1,datalength(dom.domain_name)) domain_name_end ' ...
                    ' FROM ' ...
                    '%s..domain dom, ' ...
                    '%s..domain_security dom_sec , ' ...
                    '%s..perimeter_domain pd ' ...
                    'WHERE ' ...
                    'pd.perimeter_id = %d AND ' ...
                    'dom.domain_id = pd.domain_id AND ' ...
                    'dom_sec.domain_id = dom.domain_id  '],sv,sv,sv,perimeter_id )));
                
                sec_id = security_td(:,1);
                td =security_td(:,2) ;
                td_security_key  = security_td(:,3);
                
                for j=1:length(sec_id)
                    
                    domain_sec_ids{end+1} = struct('security_id', sec_id{j}, 'trading_destination_id', td{j});%#ok
                    % domain_type_variable
                    domain_type_variable{end+1} = [num2str(sec_id {j}) ',' num2str(td{j})];%#ok
                    % domain_comments
                    domain_comments{end+1} = '';%#ok
                    % domain names
                    domain_names{end+1} = [PERIMETER_NAME '/' td_security_key{j}];%#ok
                    
                end
                
            catch e
                st_log(sprintf('ERROR : %s\n', e.message));
                
                error_message = se_stack_error_message(e);
                if ~ispc()
                    error_message = regexprep(error_message, '\n', '\r');
                end
                %envoie de mail
                identification_err = sprintf('%s%s%s','ECHEC DANS LA RECUPERATION DES SECURITY OU TD A PARTIR DE LA BASE QUANT, POUR LE PERIMETRE:',sprintf('%s%s',upper(PERIMETER_NAME),blank_char),error_message);
                subject = sprintf('Erreur de mise a jour du perimetre:%s %s' ,upper(PERIMETER_NAME), hostname());
                for j=1:length(address)
                    se_sendmail( address{j},subject,identification_err)
                end
                
            end
            
            is_successfully_update = 0;
            
        end
        
        
        % Nettoyage du p�rim�tre
        idx = se_remove_perim(domain_sec_ids);
        domain_sec_ids(idx) = [];
        domain_type_variable(idx) = [];
        domain_comments(idx) = [];
        domain_names(idx) = [];
        
        
        % Ex�cution de la cr�ation
        % ************************
        
        switch lower(mode)
            case 'update'
                se_create_domain(perimeter_id, domain_names, domain_sec_ids, domain_type_variable, domain_comments, DOMAIN_TYPE, DOMAIN_TYPE_RANK);
                r = 0;
                
            case 'virtual'
                r = struct('perimeter_id', perimeter_id, 'domain_names', domain_names, 'domain_sec', domain_sec_ids, ...
                    'domain_type_variable', domain_type_variable, 'domain_comments', domain_comments, ...
                    'domain_type', DOMAIN_TYPE, 'domain_type_rank', DOMAIN_TYPE_RANK);
        end
        
        out = struct('perimeter_id' ,r, 'is_successfully_update',is_successfully_update);
        st_log('$close$');
        
        
        
    otherwise
        
        error('perim_lmi_component:mode', '<%s> unknown', mode);
        
end


end







