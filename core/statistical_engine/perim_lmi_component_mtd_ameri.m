function out = perim_lmi_component_mtd_ameri(mode, varargin)
% PERIM_LMI_COMPONENT_MTD_AMERI - generation of a perimeter of American Main Index
%                           multi trading_destination version
%
% use:
%  params = se_get_params_perim('perim_lmi_component_mtd_ameri');
%  perim_lmi_component_mtd_ameri('update' ,'params',params{:} );
%
%  perim_lmi_component_mtd_ameri('create')
%  perim_lmi_component_mtd_ameri('virtual')



global st_version;
quant = st_version.bases.quant;

opt = options ({'params',''}, varargin);
% *****
DOMAIN_TYPE = 'mono instrument';
DOMAIN_TYPE_RANK = 2;

is_successfully_update =1;

%mail
address = exec_sql('QUANT',['SELECT owner from ',quant,'..estimator GROUP BY owner']);

%R�cup�ration des informations du p�rim�tre � partir de QUANT
out = exec_sql('QUANT',['SELECT perimeter_name , perimeter_id',...
    ' from ',quant,'..perimeter where request = ''',mfilename,'''']);
colnames = {'perimeter_name', 'perimeter_id' };
idx_p_name  = strmatch( 'perimeter_name', colnames, 'exact');
idx_p_id  = strmatch( 'perimeter_id', colnames, 'exact');

PERIMETER_NAME = cell2mat(out(:,idx_p_name));
perimeter_id  = cell2mat(out(:,idx_p_id)) ;

if ispc()
    blank_char = sprintf('\n');
else
    blank_char = sprintf('\\n');
end

switch lower(mode)
    
    case {'update', 'virtual'}
        
        dir_path = fullfile(getenv('st_repository'), 'log', 'perim_update');
        if isempty(dir(dir_path))
            mkdir(dir_path);
        end
        log_path = fullfile(dir_path , sprintf('%s_update.txt', strrep(PERIMETER_NAME ,' ','_')));
        st_log('$out$', log_path);
        
        %R�cuperation des param�tres du p�rim�tre
        params = eval(opt.get('params'));
        o = options(params);
        parse_params = str2opt(opt2str(o));
        lst_ = parse_params.get();
        opt.set( lst_{:});
        
        INDEX_NAME = opt.get('index');
        if isempty(INDEX_NAME)
            st_log(sprintf('WARNING : index_list (that is needed for the perimeter''s construction) is empty '))
        end
        
        
        % Contruction des param�tres
        % **************************
        domain_sec_ids = {};
        domain_type_variable = {};
        domain_comments = {};
        domain_names = {};
        
        try
            sec_id = [];
            for i=1:length(INDEX_NAME)
                sec_id = [sec_id;get_index_comp(INDEX_NAME{i}, 'recent_date_constraint', false)];
            end
            for j=1:length(sec_id)
                try
                    domain_sec_ids{end+1} = struct('security_id', num2cell(sec_id(j)), 'trading_destination_id', NaN);%#ok
                    % domain_type_variable
                    domain_type_variable{end+1} = [num2str(sec_id(j)) ',null'];%#ok
                    % domain_comments
                    domain_comments{end+1} = '';%#ok
                    % domain names
                    domain_names{end+1} = [PERIMETER_NAME '/' num2str(sec_id(j))];%#ok
                catch e
                    st_log(sprintf('ERROR : %s\n', e.message))
                end
            end
            
            
        catch e
            st_log(sprintf('ERROR : %s\n', e.message));
            
            error_message = se_stack_error_message(e);
            if ~ispc()
                error_message = regexprep(error_message, '\n', '\r');
            end
            %envoie de mail
            identification_err = sprintf('%s%s%s', 'ECHEC DANS LA MISE A JOUR DU PERIMETRE:',sprintf('%s%s',upper(PERIMETER_NAME),blank_char),error_message);
            subject = sprintf('Erreur de mise a jour du perimetre:%s %s' ,upper(PERIMETER_NAME), hostname());
            
            for j=1:length(address)
                se_sendmail( address{j},subject,identification_err)
            end
            
            domain_sec_ids = {};
            domain_type_variable = {};
            domain_comments = {};
            domain_names = {};
            
            try
                security_td = exec_sql('QUANT',['SELECT dom_sec.security_id , dom_sec.trading_destination_id , ' ...
                    'substring(dom.domain_name,charindex(''/'',dom.domain_name)+1,datalength(dom.domain_name)) domain_name_end ' ...
                    ' FROM ',quant,'..domain dom, ',...
                    quant,'..domain_security dom_sec , ',...
                    quant,'..perimeter_domain pd ',...
                    'WHERE pd.perimeter_id = ',num2str(perimeter_id),...
                    ' and dom.domain_id = pd.domain_id',...
                    ' and dom_sec.domain_id = dom.domain_id']);
                sec_id = security_td(:,1);
                for j=1:length(sec_id)
                    domain_sec_ids{end+1} = struct('security_id', sec_id{j}, 'trading_destination_id', NaN);%#ok
                    % domain_type_variable
                    domain_type_variable{end+1} = [num2str(sec_id {j}) ',null' ];%#ok
                    % domain_comments
                    domain_comments{end+1} = '';%#ok
                    % domain names
                    domain_names{end+1} = [PERIMETER_NAME '/' num2str(sec_id{j})];%#ok
                end
                
            catch e
                st_log(sprintf('ERROR : %s\n', e.message));
                
                error_message = se_stack_error_message(e);
                if ~ispc()
                    error_message = regexprep(error_message, '\n', '\r');
                end
                %envoie de mail
                identification_err = sprintf('%s%s%s','ECHEC DANS LA RECUPERATION DES SECURITY OU TD A PARTIR DE LA BASE QUANT, POUR LE PERIMETRE:',sprintf('%s%s',upper(PERIMETER_NAME),blank_char),error_message);
                subject = sprintf('Erreur de mise a jour du perimetre:%s %s' ,upper(PERIMETER_NAME), hostname());
                for j=1:length(address)
                    se_sendmail( address{j},subject,identification_err)
                end
                
            end
            is_successfully_update = 0;
            
        end
        
        
        % Nettoyage du p�rim�tre
        idx = se_remove_perim(domain_sec_ids);
        domain_sec_ids(idx) = [];
        domain_type_variable(idx) = [];
        domain_comments(idx) = [];
        domain_names(idx) = [];
        
        
        
        % Ex�cution de la cr�ation
        % ************************
        
        switch lower(mode)
            case 'update'
                se_create_domain(perimeter_id, domain_names, domain_sec_ids, domain_type_variable, domain_comments, DOMAIN_TYPE, DOMAIN_TYPE_RANK);
                r = 0;
                
            case 'virtual'
                r = struct('perimeter_id', perimeter_id, 'domain_names', domain_names, 'domain_sec', domain_sec_ids, ...
                    'domain_type_variable', domain_type_variable, 'domain_comments', domain_comments, ...
                    'domain_type', DOMAIN_TYPE, 'domain_type_rank', DOMAIN_TYPE_RANK);
        end
        
        out = struct('perimeter_id' ,r, 'is_successfully_update',is_successfully_update);
        st_log('$close$');
        
        
        
        
    otherwise
        
        error('perim_lmi_component_mtd:mode', '<%s> unknown', mode);
        
end


end







