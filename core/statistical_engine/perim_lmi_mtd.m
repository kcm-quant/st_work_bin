function out = perim_lmi_mtd(mode, varargin)
% perim_lmi_mtd- generation of a perimeter of European Main Index
%
% use:
%  perim_lmi_mtd('update')
%  perim_lmi_mtd('create')
%  perim_lmi_mtd('virtual')
%
%
% perim_lmi_mtd('association', 'quant', 10, 'SMI', 2, 666)

global st_version;
quant = st_version.bases.quant;
repository = st_version.bases.repository;
market_data = st_version.bases.market_data;

opt = options ({'params',''}, varargin);

% HARD CODED VARIABLES
DOMAIN_TYPE = 'place index';
DOMAIN_TYPE_RANK = 20;
NB_DEAL_LIMIT_FOR_ASE = 500;
NB_DEAL_LIMIT_FOR_SPI = 800;
NB_DEAL_LIMIT_FOR_ISEQ = 225;
TD_ID_OF_ZURICH = 18;
TD_ID_OF_DUBLIN = 39;

is_successfully_update = 1;

%R�cup�ration des informations du p�rim�tre � partir de QUANT
out = exec_sql('QUANT',['SELECT perimeter_name , perimeter_id',...
    ' from ',quant,'..perimeter where request = ''',mfilename,''' ']);
colnames = {'perimeter_name', 'perimeter_id' };
idx_p_name  = strmatch( 'perimeter_name', colnames, 'exact');
idx_p_id  = strmatch( 'perimeter_id', colnames, 'exact');

PERIMETER_NAME = cell2mat(out(:,idx_p_name));
perimeter_id  = cell2mat(out(:,idx_p_id)) ;

%mail
address = exec_sql('QUANT', sprintf('SELECT owner from %s..estimator GROUP BY owner', quant));

switch lower(mode)
    
    case {'update', 'virtual'}
        
        dir_path = fullfile(getenv('st_repository'), 'log', 'perim_update');
        if isempty(dir(dir_path))
            mkdir(dir_path);
        end
        log_path = fullfile(dir_path , sprintf('%s_update.txt', strrep(PERIMETER_NAME ,' ','_')));
        st_log('$out$', log_path);
        
        %R�cuperation des param�tres du p�rim�tre
        params = eval(opt.get('params'));
        o = options(params);
        parse_params = str2opt(opt2str(o));
        lst_ = parse_params.get();
        opt.set( lst_{:});
        
        INDEX_NAME = opt.get('index');
        if isempty(INDEX_NAME)
            st_log(sprintf('WARNING : index_list (that is needed for the perimeter''s construction) is empty '))
        end
        
        % Contruction des param�tres
        % **************************
        domain_sec_ids = cell(1, length(INDEX_NAME));
        domain_type_variable = cell(1, length(INDEX_NAME));
        domain_comments = cell(1, length(INDEX_NAME));
        
        % domain names
        perim_name = exec_sql('QUANT' , sprintf([ ...
            'SELECT perimeter_name ' ...
            'FROM ',quant,'..perimeter ' ...
            'WHERE perimeter_id=',num2str(perimeter_id)]));
        domain_names = strcat(perim_name,'/',INDEX_NAME);
        
        for i=1:length(domain_sec_ids)
            
            % domain_type_variable
            
            %             em_id = get_repository('place', 'index-name', INDEX_NAME{i});
            %             domain_type_variable{i} = [num2str(em_id)];
            domain_type_variable{i} = INDEX_NAME{i};
            
            % security ids
            try
                sec_id = get_repository('index-comp', INDEX_NAME{i}, 'recent_date_constraint', false);
                sec_id = check_data(sec_id);
                % Rustine pour l'ASE
                if strcmp(INDEX_NAME{i},'ASE')
                    if ~isempty(sec_id)
                        sec_str = sprintf('%d,',sec_id);
                        sec_id = cell2mat(exec_sql('MARKET_DATA',['select td.security_id',...
                            ' from ',market_data,'..trading_daily td,',repository,'..security_market sm',...
                            ' where sm.security_id = td.security_id',...
                            ' and sm.trading_destination_id = td.trading_destination_id',...
                            ' and sm.ranking = 1',...
                            ' and td.date between ''',datestr(today-7-90,'yyyy-mm-dd'),''' and ''',datestr(today-7,'yyyy-mm-dd'),'''',...
                            ' and td.security_id in (',sec_str(1:end-1),')',...
                            ' group by td.security_id',...
                            ' having avg(td.nb_deal) > ',num2str(NB_DEAL_LIMIT_FOR_ASE)]));
                    end
                end
                % Derniere ligne de la rustine
                
                % Rustine pour SPI (Indice Suisse). On ne garde que Zurich.
                if strcmp(INDEX_NAME{i},'SPI')
                    if ~isempty(sec_id)
                        sec_str = sprintf('%d,',sec_id);
                        sec_id = cell2mat(exec_sql('MARKET_DATA',['select security_id',...
                            ' from ',market_data,'..trading_daily',...
                            ' where trading_destination_id = ',num2str(TD_ID_OF_ZURICH),...
                            ' and date between ''',datestr(today-7-90,'yyyy-mm-dd'),''' and ''',datestr(today-7,'yyyy-mm-dd'),'''',...
                            ' and security_id in (',sec_str(1:end-1),')',...
                            ' group by security_id',...
                            ' having avg(nb_deal) > ',num2str(NB_DEAL_LIMIT_FOR_SPI)]));
                    end
                end
                % derniere ligne de la rustine
                
                % Rustine pour ISEQ (Indice Irlandais). On ne que les valeurs qui ont nombre de trades journalier minimum.
                if strcmp(INDEX_NAME{i},'ISEQ')
                    if isempty(sec_id)
                        sec_str = sprintf('%d,',sec_id);
                        sec_id = cell2mat(exec_sql('MARKET_DATA',['select security_id',...
                            ' from ',market_data,'..trading_daily',...
                            ' where trading_destination_id = ',num2str(TD_ID_OF_DUBLIN),...
                            ' and date between ''',datestr(today-7-90,'yyyy-mm-dd'),''' and ''',datestr(today-7,'yyyy-mm-dd'),'''',...
                            ' and security_id in (',sec_str(1:end-1),')',...
                            ' group by security_id',...
                            ' having avg(nb_deal) > ',num2str(NB_DEAL_LIMIT_FOR_ISEQ)]));
                    end
                end
                % derniere ligne de la rustine
                
                domain_sec_ids{i} = struct('security_id', num2cell(sec_id), 'trading_destination_id', NaN);
            catch e
                st_log(sprintf('ERROR : %s\n', e.message));
                error_message = se_stack_error_message(e);
                
                %envoie de mail
                identification_err = sprintf('%s%s', 'ECHEC DANS LA MISE A JOUR DE L''INDICE:',sprintf('%s\n',upper(INDEX_NAME{i})),error_message);
                subject = sprintf('Erreur de mise a jour de l''indice:%s, perimetre:%s %s' ,upper(INDEX_NAME{i}),upper(PERIMETER_NAME), hostname());
                sendmail(address,subject,identification_err)
                
                try  % On r�cup�re les valeurs pr�sentes dans domain_security du domain actif.
                    sec_id = cell2mat(exec_sql('QUANT',['SELECT ds.security_id' ...
                        ' FROM ',quant,'..domain_security ds,',quant,'..perimeter_domain pd,',quant,'..domain d',...
                        ' WHERE d.domain_name = ''',domain_names{i},''''...
                        ' and d.domain_id = pd.domain_id',...
                        ' and ds.domain_id = d.domain_id',...
                        ' and pd.is_active = 1']));
                    domain_sec_ids{i} = struct('security_id', sec_id, 'trading_destination_id', NaN(length(sec_id),1));
                catch e % Nouvelle erreur, nouvau mail.
                    st_log(sprintf('ERROR : %s\n', e.message));
                    
                    error_message = se_stack_error_message(e);
                    
                    %envoie de mail
                    identification_err = sprintf(['ECHEC DANS LA RECUPERATION DES SECURITY OU TD',...
                        ' � PARTIR DE LA BASE QUANT,  POUR L''INDICE : %s\n%s'],upper(INDEX_NAME{i}),error_message);
                    subject = sprintf(['Erreur de mise a jour de l''indice:%s, ',...
                        'ATTENTION L''INDICE A ETE SUPPRIME DU PERIMETRE:%s %s'],upper(INDEX_NAME{i}),upper(PERIMETER_NAME), hostname());
                    sendmail(address,subject,identification_err)
                end
                is_successfully_update = 0;
                
            end
            
            % domain_comments
            domain_comments{i} = '';
        end
        
        % Nettoyage du p�rim�tre
        idx = se_remove_perim(domain_sec_ids);
        domain_sec_ids(idx) = [];
        domain_type_variable(idx) = [];
        domain_comments(idx) = [];
        domain_names(idx) = [];
        
        % Ex�cution de la cr�ation
        % ************************
        
        switch lower(mode)
            case 'update'
                is_successfully_update = se_create_domain(perimeter_id,...
                    domain_names, domain_sec_ids, domain_type_variable, domain_comments, DOMAIN_TYPE, DOMAIN_TYPE_RANK);
                r = perimeter_id;
                
            case 'virtual'
                r = struct('perimeter_id',perimeter_id, 'domain_names', domain_names, 'domain_sec', domain_sec_ids, ...
                    'domain_type_variable', domain_type_variable, 'domain_comments', domain_comments, ...
                    'domain_type', DOMAIN_TYPE, 'domain_type_rank', DOMAIN_TYPE_RANK);
        end
        
        out = struct('perimeter_id',r,'is_successfully_update',is_successfully_update);
        st_log('$close$');
        
    case 'association' % varargin = {$b, $r, $v, $e, $j)
        
        % Cr�ation des associations
        % *************************
        
        opt = options( { ...
            'quant_table', 'next_association' , ...
            }, varargin);
        
        table = opt.get('quant_table');
        if ~strcmpi(table,'next_association')
            st_log('WARNING:::::::::::::::::::::::::::<%s> inserst associations in the TABLE : %s ' ,mfilename , table )
        end
        
        base = varargin{1};
        rank = varargin{2};
        index_name = varargin{3};
        estimator_id = varargin{4};
        job_id = varargin{5};
        
        if nargin>6
            context_num = varargin{6};
        else
            context_num = 1;
        end
        
        
        td_id = get_main_tds_for_index(index_name);
        
        % On teste l'existence de l'association avant d'eventuellement
        % l'ins�rer
        context_id = cell2mat(exec_sql('QUANT',['SELECT context_id',...
            ' FROM ',base,'..context',...
            ' WHERE estimator_id=',num2str(estimator_id),...
            ' AND context_num=',num2str(context_num)]));
        
        association_id = cell2mat(exec_sql('QUANT', ...
            sprintf(['SELECT %s_id FROM ',base,'..',table,'  WHERE ' ...
            'context_id=%d AND rank=%d AND security_id is NULL ',...
            ' AND trading_destination_id=%d AND estimator_id=%d AND job_id=%d'], ...
            table,context_id, rank, td_id(1, 1), estimator_id, job_id)));
        
        if isempty(association_id)
            exec_sql('QUANT', sprintf([...
                'INSERT INTO ',base,'..',table,' (context_id, rank, security_id, trading_destination_id, estimator_id, job_id, from_meta) ' ...
                'VALUES (%d, %d, NULL, %d, %d, %d, 1)'], ...
                context_id, rank, td_id(1, 1),...
                estimator_id, job_id));
        end
        
    otherwise
        error('perim_lmi_mtd:mode', '<%s> unknown', mode);
end

function sec_list = check_data(sec_list)
global st_version
market_data = st_version.bases.market_data;
sec_str = join(',',sec_list);
sec_list = cell2mat(exec_sql('MARKET_DATA',['select security_id',...
    ' from ',market_data,'..trading_daily',...
    ' where date between ''',datestr(today-30-90-7,'yyyy-mm-dd'),''' and ''',datestr(today-90-7,'yyyy-mm-dd'),'''',...
    ' and security_id in (',sec_str,')',...
    ' group by security_id']));

function str = join(separator,list)
if isempty(list)
    str = '';
    return
end
if isnumeric(list)
    str = sprintf(['%d',separator],list);
elseif iscellstr(list)
    str = sprintf(['%s',separator],list{:});
else
    error('join:wronginputargs','Second argument should be either numeric or cellstr')
end
str(end-length(sprintf(separator))+1:end) = [];