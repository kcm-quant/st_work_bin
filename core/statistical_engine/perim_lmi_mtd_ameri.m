function out = perim_lmi_mtd_ameri(mode, varargin)
% perim_lmi_mtd- generation of a perimeter of European Main Index
%
% use:
%  perim_lmi_mtd('update')
%  perim_lmi_mtd('create')
%  perim_lmi_mtd('virtual')
%  params = se_get_params_perim('perim_lmi_mtd_ameri');
%  perim_lmi_mtd_ameri('update','params',params{1})
% perim_lmi_mtd('association', 'quant', 10, 'SMI', 2, 666)

global st_version;
quant = st_version.bases.quant;
repository = st_version.bases.repository;

% *****
DOMAIN_TYPE = 'place index';
DOMAIN_TYPE_RANK = 20;


%R�cup�ration des informations du p�rim�tre � partir de QUANT
out = exec_sql('QUANT',['SELECT perimeter_name , perimeter_id ',...
    ' from ',quant,'..perimeter where request = ''',mfilename,''' ']);
PERIMETER_NAME = out{1};
perimeter_id  = out{2};


switch lower(mode)
    
    case {'update', 'virtual'}
        
        dir_path = fullfile(getenv('st_repository'), 'log', 'perim_update');
        if isempty(dir(dir_path))
            mkdir(dir_path);
        end
        log_path = fullfile(dir_path , sprintf('%s_update.txt', strrep(PERIMETER_NAME ,' ','_')));
        st_log('$out$', log_path);
      
        
        %%% Creation of Amrican Quotation groups:
        [index_domains_ameri is_successfully_update] = get_index_domains_ameri;
        
        INDEX_NAME = index_domains_ameri(:,1);
        if isempty(INDEX_NAME)
            st_log(sprintf('WARNING : empty index_list (needed for the perimeter''s construction)'))
        end
        
        % Contruction des param�tres
        % **************************
        domain_sec_ids = cell(1, length(INDEX_NAME));
        domain_type_variable = cell(1, length(INDEX_NAME));
        domain_comments = cell(1, length(INDEX_NAME));
        
        % domain names
        perim_name = cell2mat(exec_sql('QUANT',[ ...
            'SELECT perimeter_name ' ...
            'FROM ',quant,'..perimeter ' ...
            'WHERE perimeter_id=',num2str(perimeter_id)]));
        domain_names = cellfun(@(x) [perim_name '/' x], INDEX_NAME, 'UniformOutput', false);
        
        for i=1:length(domain_sec_ids)

            domain_type_variable{i} = INDEX_NAME{i};    
            domain_sec_ids{i} = struct('security_id', num2cell(index_domains_ameri{i,3}), 'trading_destination_id', NaN);
            domain_comments{i} = '';
        end
        
        
        % Nettoyage du p�rim�tre
        idx = se_remove_perim(domain_sec_ids);
        domain_sec_ids(idx) = [];
        domain_type_variable(idx) = [];
        domain_comments(idx) = [];
        domain_names(idx) = [];
        
        % Ex�cution de la cr�ation
        % ************************
        
        switch lower(mode)
            case 'update'
                se_create_domain(perimeter_id, domain_names, domain_sec_ids, domain_type_variable, domain_comments, DOMAIN_TYPE, DOMAIN_TYPE_RANK);
                r = 0 ;
                
            case 'virtual'
                r = struct('perimeter_id', perimeter_id, 'domain_names', domain_names, 'domain_sec', domain_sec_ids, ...
                    'domain_type_variable', domain_type_variable, 'domain_comments', domain_comments, ...
                    'domain_type', DOMAIN_TYPE, 'domain_type_rank', DOMAIN_TYPE_RANK);
        end
        
        out = struct('perimeter_id' ,r, 'is_successfully_update',is_successfully_update);
        
        st_log('$close$');
        
        
    case 'association' % varargin = {$b, $r, $v, $e, $j)
        
        % Cr�ation des associations
        % *************************
        
        opt = options( { ...
            'quant_table', 'next_association' , ...
            }, varargin);
        
        table = opt.get('quant_table');
        if ~strcmpi(table,'next_association')
            st_log('WARNING:::::::::::::::::::::::::::<%s> inserst associations in the TABLE : %s ' ,mfilename , table )
        end
        
        base = varargin{1};
        rank = varargin{2};
        index_name = varargin{3};
        estimator_id = varargin{4};
        varargin_ = index_name;
        job_id = varargin{5};
        
        if nargin>6
            context_num = varargin{6};
        else
            context_num = 1;
        end
        
        td_id = cell2mat(exec_sql('KGR',['select EXCHANGE',...
            ' from ',repository,'..QUOTATIONGROUP',...
            ' where MKTGROUP = ''',index_name,'''']));
        
        % On teste l'existence de l'association avant d'eventuellement
        % l'ins�rer
        context_id = cell2mat(exec_sql('QUANT', sprintf( ...
            ['SELECT context_id FROM ',base,'..context',...
            ' WHERE estimator_id=%d AND context_num=%d'],...
            estimator_id , context_num)));
        
        association_id = cell2mat(exec_sql('QUANT', ...
            ['SELECT ',table,'_id',...
            ' FROM ',base,'..',table,'  WHERE context_id = ',num2str(context_id),...
            ' AND rank = ',num2str(rank),...
            ' AND security_id is NULL',...
            ' AND trading_destination_id = ',num2str(td_id(1)),...
            ' and varargin = ''',varargin_,'''',...
            ' AND estimator_id = ',num2str(estimator_id),...
            ' AND job_id = ',num2str(job_id)]));
        
        if isempty(association_id)
            exec_sql('QUANT', sprintf(['INSERT INTO ',base,'..',table,...
                ' (context_id, rank, security_id, trading_destination_id, varargin, estimator_id, job_id, from_meta) ',...
                'VALUES (%d, %d, NULL, %d, ''%s'', %d, %d, 1)'], ...
                context_id, rank, td_id(1),varargin_,estimator_id, job_id));
        end
        
        
    otherwise
        
        error('perim_lmi_mtd:mode', '<%s> unknown', mode);
        
end


end