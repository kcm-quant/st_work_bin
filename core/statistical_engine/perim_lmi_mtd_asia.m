function out = perim_lmi_mtd_asia(mode, varargin)
% perim_lmi_mtd_asia- generation of a perimeter of European Main Index
%
% use:
%  perim_lmi_mtd_asia('update')
%  perim_lmi_mtd_asia('create')
%  perim_lmi_mtd_asia('virtual')
%
%
% perim_lmi_mtd('association', 'quant', 10, 'SMI', 2, 666)

global st_version;
sv = st_version.bases.quant;


% *****
DOMAIN_TYPE = 'place index asia';
DOMAIN_TYPE_RANK = 20;

is_successfully_update =1;  %variable qui permet de remonter s'il y a eu une erreur lors de la mise � jour d'un indice

%R�cup�ration des informations du p�rim�tre � partir de QUANT
out = (exec_sql('QUANT', sprintf('SELECT perimeter_name , perimeter_id  from %s..perimeter where request = ''%s'' ', sv,mfilename)));
colnames = {'perimeter_name', 'perimeter_id' };
idx_p_name  = strmatch( 'perimeter_name', colnames, 'exact');
idx_p_id  = strmatch( 'perimeter_id', colnames, 'exact');

PERIMETER_NAME = cell2mat(out(:,idx_p_name));
perimeter_id  = cell2mat(out(:,idx_p_id)) ;


switch lower(mode)
         
    case {'update', 'virtual'}
        
        %cr�ation du fichier de log
        dir_path = fullfile(getenv('st_repository'), 'log', 'perim_update');
        if isempty(dir(dir_path))
            mkdir(dir_path);
        end
        log_path = fullfile(dir_path , sprintf('%s_update.txt', strrep(PERIMETER_NAME ,' ','_')));
        st_log('$out$', log_path);
           
        % r�cup�ration des domaines
        [index_domains_asia is_successfully_update]= get_index_domains_asia();
        
        if ~isempty(index_domains_asia)
            INDEX_NAME=transpose(cellfun(@(c)(sprintf('%d',c)),index_domains_asia(:,1),'uni',false));
            
            % Contruction des param�tres
            % **************************
            domain_sec_ids = cell(1, length(index_domains_asia));
            domain_type_variable = cell(1, length(index_domains_asia));
            domain_comments = cell(1, length(index_domains_asia));
            for i=1:length(domain_sec_ids)
                % security ids
                domain_sec_ids{i} = struct('security_id', num2cell(index_domains_asia{i,3}(:,1)), ...
                    'trading_destination_id', NaN);
                %%% bien mettre le naN parce que c'est un indice ??
                domain_type_variable{i} = INDEX_NAME{i};
                % domain_comments
                domain_comments{i} = '';
            end
            % domain names
            perim_name = cell2mat(exec_sql('QUANT' , sprintf([ ...
                'SELECT perimeter_name ' ...
                'FROM %s..perimeter ' ...
                'WHERE perimeter_id=%d'], sv, perimeter_id)));
            %%% mettre un autre perim_id ???
            domain_names = cellfun(@(x) [perim_name '/' x], INDEX_NAME, 'UniformOutput', false);
            
            % Nettoyage du p�rim�tre
            idx = se_remove_perim(domain_sec_ids);
            domain_sec_ids(idx) = [];
            domain_type_variable(idx) = [];
            domain_comments(idx) = [];
            domain_names(idx) = [];
            
            % Ex�cution de la cr�ation
            % ************************
            
            switch lower(mode)
                case 'update'
                    se_create_domain(perimeter_id, domain_names, domain_sec_ids, domain_type_variable, domain_comments, DOMAIN_TYPE, DOMAIN_TYPE_RANK);
                    r =0;
                    
                case 'virtual'
                    r = struct('perimeter_id', perimeter_id, 'domain_names', domain_names, 'domain_sec', domain_sec_ids, ...
                        'domain_type_variable', domain_type_variable, 'domain_comments', domain_comments, ...
                        'domain_type', DOMAIN_TYPE, 'domain_type_rank', DOMAIN_TYPE_RANK);
            end
        else
            r=0;
        end
        
        
        out = struct('perimeter_id' ,r, 'is_successfully_update',is_successfully_update);
        
        st_log('$close$');
        
        
    case 'association' % varargin = {$b, $r, $v, $e, $j)
        
        % Cr�ation des associations
        % *************************
        
        opt = options( { ...
            'quant_table', 'next_association' , ...
            }, varargin);
        
        table = opt.get('quant_table');
        if ~strcmpi(table,'next_association')
            st_log('WARNING:::::::::::::::::::::::::::<%s> inserst associations in the TABLE : %s ' ,mfilename , table )
        end
         
        base = varargin{1};
        rank = varargin{2};
        index_name = varargin{3};
        estimator_id = varargin{4};
        job_id = varargin{5};
        
        
        td_id = str2num(index_name);
        
        % On teste l'existence de l'association avant d'eventuellement
        % l'ins�rer
        context_id = cell2mat(exec_sql('QUANT', sprintf( ...
            'SELECT context_id FROM %s..context WHERE estimator_id=%d AND context_num=1', base, estimator_id)));
        
        association_id = cell2mat(exec_sql('QUANT', ...
            sprintf(['SELECT %s_id FROM %s..%s  WHERE ' ...
            'context_id=%d AND rank=%d AND security_id is NULL AND trading_destination_id=%d AND estimator_id=%d AND job_id=%d'], ...
            table, base, table, context_id, rank, td_id(1, 1), estimator_id, job_id)));
        
        if isempty(association_id)
            exec_sql('QUANT', sprintf([ ...
                'INSERT INTO %s..%s (context_id, rank, security_id, trading_destination_id, estimator_id, job_id, from_meta) ' ...
                'VALUES (%d, %d, NULL, %d, %d, %d, 1)'], ...
                base, table, context_id, rank, td_id(1, 1),...
                estimator_id, job_id));
        end
        
        
    otherwise
        
        error('perim_lmi_mtd_asia:mode', '<%s> unknown', mode);
        
end


end







