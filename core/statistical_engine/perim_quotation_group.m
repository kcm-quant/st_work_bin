function out = perim_quotation_group(mode,varargin)
% perim_quotation_group - generation of a perimeter of quotation groups
%
% use:
%  perim_quotation_group('update')
%  perim_quotation_group('create')
%  perim_quotation_group('virtual')




global st_version;
quant = st_version.bases.quant;

% *****
DOMAIN_TYPE = 'representative index';
DOMAIN_TYPE_RANK = 10;

is_successfully_update = 1;  %variable qui permet de remonter s'il y a eu une erreur lors de la mise � jour d'un indice

opt = options ({'params',''}, varargin);

%R�cup�ration des informations du p�rim�tre � partir de QUANT
out = exec_sql('QUANT',['SELECT perimeter_name , perimeter_id',...
    ' from ',quant,'..perimeter where request = ''',mfilename,'''']);
colnames = {'perimeter_name', 'perimeter_id' };
idx_p_name  = strmatch( 'perimeter_name', colnames, 'exact');
idx_p_id  = strmatch( 'perimeter_id', colnames, 'exact');

PERIMETER_NAME = cell2mat(out(:,idx_p_name));
perimeter_id  = cell2mat(out(:,idx_p_id)) ;

if ispc()
    blank_char = sprintf('\n');
else
    blank_char = sprintf('\\n');
end

switch lower(mode)
                
    case {'update', 'virtual'}
        
        dir_path = fullfile(getenv('st_repository'), 'log', 'perim_update');
        if isempty(dir(dir_path))
            mkdir(dir_path);
        end
        log_path = fullfile(dir_path , sprintf('%s_update.txt', strrep(PERIMETER_NAME ,' ','_')));
        st_log('$out$', log_path);
        
        %mail
        address = exec_sql('QUANT',['SELECT owner from ',quant,'..estimator GROUP BY owner']);
        
        temp = eval(opt.get('params'));
        o = options(temp);
        exchgid = o.get('EXCHGID');
        
        % r�cup�ration des domaines
        [quotation_group_domains is_successfully_update]= get_quotation_group_domains(exchgid);
        
        % Contruction des param�tres
        % **************************
        domain_names   = cell(1, length(quotation_group_domains));
        domain_sec_ids = cell(1, length(quotation_group_domains));
        domain_type_variable = cell(1, length(quotation_group_domains));
        domain_comments = cell(1, length(quotation_group_domains));
        for i=1:length(quotation_group_domains)
            
            try
                % domain names
                qg_name = quotation_group_domains{i,2};
                if isempty( qg_name)
                    qg_name = 'EMPTY';
                end
                domain_names{i} = sprintf('%s[%s]%s', qg_name, ...
                    quotation_group_domains{i,1}, get_td_name(quotation_group_domains{i,1}));
                
                % security ids
                domain_sec_ids{i} = struct('security_id', num2cell(quotation_group_domains{i,3}), ...
                    'trading_destination_id', get_td_id_from_exchgid(quotation_group_domains(i,1)));
                
                % domain_type_variable
                qg_name = quotation_group_domains{i,2};
                if isempty( qg_name)
                    qg_name = 'NULL';
                else
                    qg_name = sprintf('''%s''', qg_name);
                end
                domain_type_variable{i} = sprintf('%s,%s', quotation_group_domains{i,1}, qg_name);
                % domain_comments
                domain_comments{i} = '';
                
            catch e
                domain_names{i} = [];
                domain_sec_ids{i} = [];
                domain_type_variable{i} = [];
                domain_comments{i} = [];
                
                st_log(sprintf('%s\n', e.message));
                
                error_message = se_stack_error_message(e);

                %envoie de mail
                identification_err = sprintf('%s%s%s', 'ECHEC DANS LA MISE A JOUR D''UN OU PLUSIEURS GROUPES DE QUOTATION',blank_char, error_message);
                subject = sprintf('Erreur de mise a jour du perimetre:QUOTATION_GROUP %s', hostname());
                
                sendmail( address,subject,identification_err)
                
                is_successfully_update = 0;
            end
        end
        
        % Nettoyage du p�rim�tre
        idx = se_remove_perim(domain_sec_ids);
        domain_sec_ids(idx) = [];
        domain_type_variable(idx) = [];
        domain_comments(idx) = [];
        domain_names(idx) = [];
        
        % Ex�cution de la cr�ation
        % ************************
        
        switch lower(mode)
            case 'update'
                se_create_domain( ...
                    perimeter_id, ... int
                    domain_names, ... cell
                    domain_sec_ids, ... cell de structs security_id/trading_destination_id
                    domain_type_variable, ... cell
                    domain_comments, ... cell
                    DOMAIN_TYPE, DOMAIN_TYPE_RANK);
                r=0;
                
            case 'virtual'
                r = struct('perimeter_id', perimeter_id, 'domain_names', domain_names, 'domain_sec', domain_sec_ids, ...
                    'domain_type_variable', domain_type_variable, 'domain_comments', domain_comments, ...
                    'domain_type', DOMAIN_TYPE, 'domain_type_rank', DOMAIN_TYPE_RANK);
        end
        
        out = struct('perimeter_id' ,r, 'is_successfully_update',is_successfully_update);

        st_log('$close$');

    otherwise
        error('perim_quotation_group:mode', '<%s> unknown', mode);     
end


end







