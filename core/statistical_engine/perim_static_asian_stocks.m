function out = perim_static_asian_stocks(mode, varargin)
% PERIM_STATIC_ASIAN_STOCKS - Short_one_line_description
%
%
%
% Examples:
%  perim_static_asian_stocks('update')
%  perim_static_asian_stocks('create')
%  perim_static_asian_stocks('virtual')
%
%
% See also:
%
%
%   author   : 'dcroize@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '06/06/2011'


global st_version;
sv = st_version.bases.quant;

opt = options ({'params',''}, varargin);

% *****
DOMAIN_TYPE = 'mono instrument';
DOMAIN_TYPE_RANK = 1;

is_successfully_update =1;


%mail
address = exec_sql('QUANT', sprintf('SELECT owner from %s..estimator GROUP BY owner', sv));

%R�cup�ration des informations du p�rim�tre � partir de QUANT
out = (exec_sql('QUANT', sprintf('SELECT perimeter_name , perimeter_id  from %s..perimeter where request = ''%s'' ', sv,mfilename)));
colnames = {'perimeter_name', 'perimeter_id' };
idx_p_name  = strmatch( 'perimeter_name', colnames, 'exact');
idx_p_id  = strmatch( 'perimeter_id', colnames, 'exact');

PERIMETER_NAME = cell2mat(out(:,idx_p_name));
perimeter_id  = cell2mat(out(:,idx_p_id)) ;

if ispc()
    blank_char = sprintf('\n');
else
    blank_char = sprintf('\\n');
end


switch lower(mode)
    case {'update', 'virtual'}
        
        dir_path = fullfile(getenv('st_repository'), 'log', 'perim_update');
        if isempty(dir(dir_path))
            mkdir(dir_path);
        end
        
        log_path = fullfile(dir_path , sprintf('%s_update.txt', strrep(PERIMETER_NAME ,' ','_')));
        st_log('$out$', log_path);
        
        
        % Contruction des param�tres
        % **************************
        domain_sec_ids = {};
        domain_type_variable = {};
        domain_comments = {};
        domain_names = {};
        
        
        
        try
            %TEMPORAIRE TEMPORAIRE  TEMPORAIRE  TEMPORAIRE  TEMPORAIRE  TEMPORAIRE 
            %r�cup�ration "one shot", dans un premier temps il s'agit p�rim�tre STATIC...
            
            %data_xls = st_data('xls', 'C:\st_work\usr\dev\croize\funcs\funcs4SE\data\backtest_perimeter_asia.xls', 'backtest_perimeter_asia','A3:C655','colnames', 'sec_id;td;nb_deal5mins' );
            %sec_id = st_data('cols' , data_xls , 'sec_id');
            
            sec_id = [];
              
            for j=1:length(sec_id)
                
                td = get_repository( 'any-to-sec-td', 'security_id', sec_id(j));
                domain_sec_ids{end+1} = struct('security_id', num2cell(sec_id(j)), 'trading_destination_id', td.trading_destination_id(1));%#ok
                % domain_type_variable
                domain_type_variable{end+1} = [num2str(sec_id(j)) ',' num2str(td.trading_destination_id(1))];%#ok
                % domain_comments
                domain_comments{end+1} = '';%#ok
                % domain names
                domain_names{end+1} = [PERIMETER_NAME '/' td.security_key];%#ok
                
            end
            
            
            
        catch e
            st_log(sprintf('ERROR : %s\n', e.message));
            
            error_message = se_stack_error_message(e);
            if ~ispc()
                error_message = regexprep(error_message, '\n', '\r');
            end
            %envoie de mail
            identification_err = sprintf('%s%s%s', 'ECHEC DANS LA MISE A JOUR DU PERIMETRE:',sprintf('%s%s',upper(PERIMETER_NAME),blank_char),error_message);
            subject = sprintf('Erreur de mise a jour du perimetre:%s %s' ,upper(PERIMETER_NAME), hostname());
            
            for j=1:length(address)
                se_sendmail( address{j},subject,identification_err)
            end
            
            domain_sec_ids = {};
            domain_type_variable = {};
            domain_comments = {};
            domain_names = {};
            
            try %s'il n'a pas r�ussi � r�cup�rer les sec_id ou trading-destination , one essaie de les r�cuperer dans la base quant/quant_homolo
                security_td = (exec_sql('QUANT', sprintf(['SELECT dom_sec.security_id , dom_sec.trading_destination_id , ' ...
                    ' substring(dom.domain_name,charindex(''/'',dom.domain_name)+1,datalength(dom.domain_name)) domain_name_end ' ...
                    ' FROM ' ...
                    '%s..domain dom, ' ...
                    '%s..domain_security dom_sec , ' ...
                    '%s..perimeter_domain pd ' ...
                    'WHERE ' ...
                    'pd.perimeter_id = %d AND ' ...
                    'dom.domain_id = pd.domain_id AND ' ...
                    'dom_sec.domain_id = dom.domain_id  '],sv,sv,sv,perimeter_id )));
                
                sec_id = security_td(:,1);
                td =security_td(:,2) ;
                td_security_key  = security_td(:,3);
                
                for j=1:length(sec_id)
                    
                    domain_sec_ids{end+1} = struct('security_id', sec_id{j}, 'trading_destination_id', td{j});%#ok
                    % domain_type_variable
                    domain_type_variable{end+1} = [num2str(sec_id {j}) ',' num2str(td{j})];%#ok
                    % domain_comments
                    domain_comments{end+1} = '';%#ok
                    % domain names
                    domain_names{end+1} = [PERIMETER_NAME '/' td_security_key{j}];%#ok
                    
                end
                
            catch e
                st_log(sprintf('ERROR : %s\n', e.message));
                
                error_message = se_stack_error_message(e);
                if ~ispc()
                    error_message = regexprep(error_message, '\n', '\r');
                end
                %envoie de mail
                identification_err = sprintf('%s%s%s','ECHEC DANS LA RECUPERATION DES SECURITY OU TD A PARTIR DE LA BASE QUANT, POUR LE PERIMETRE:',sprintf('%s%s',upper(PERIMETER_NAME),blank_char),error_message);
                subject = sprintf('Erreur de mise a jour du perimetre:%s %s' ,upper(PERIMETER_NAME), hostname());
                for j=1:length(address)
                    se_sendmail( address{j},subject,identification_err)
                end
                
            end
            
            is_successfully_update = 0;
            
        end
        
        
        % Nettoyage du p�rim�tre
        idx = se_remove_perim(domain_sec_ids);
        domain_sec_ids(idx) = [];
        domain_type_variable(idx) = [];
        domain_comments(idx) = [];
        domain_names(idx) = [];
        
        
        % Ex�cution de la cr�ation
        % ************************
        
        switch lower(mode)
            case 'update'
                se_create_domain(perimeter_id, domain_names, domain_sec_ids, domain_type_variable, domain_comments, DOMAIN_TYPE, DOMAIN_TYPE_RANK);
                r = 0;
                
            case 'virtual'
                r = struct('perimeter_id', perimeter_id, 'domain_names', domain_names, 'domain_sec', domain_sec_ids, ...
                    'domain_type_variable', domain_type_variable, 'domain_comments', domain_comments, ...
                    'domain_type', DOMAIN_TYPE, 'domain_type_rank', DOMAIN_TYPE_RANK);
        end
        
        out = struct('perimeter_id' ,r, 'is_successfully_update',is_successfully_update);
        st_log('$close$');
        
        
    otherwise
        error('perim_asia_test:mode', 'MODE: <%s> unknown', mode);
        
end

end