function msg = get_msg_from_summary_struct(summary_struct)
% GET_MSG_FROM_SUMMARY_STRUCT - takes a summary structure as input and
% makes a message from it
%
% ex :
% sec_info = get_security_informations(362)
% summary = get_severe_info(sec_info.summary, 4, 1)
% get_msg_from_summary_struct(summary)
    
    msg = '';
    if isempty(summary_struct)
        return;
    end
    summary_fields = fieldnames(summary_struct);
    for i = 1 : length(summary_fields)
        msg = [msg sprintf('%s: \n', summary_fields{i})];
        for j = 1 : length(summary_struct.(summary_fields{i}).message)
            msg = [msg  sprintf('\t%s\n', summary_struct.(summary_fields{i}).message{j})];
        end
    end
end