function out = get_security_informations(security_id, as_of_date)
% GET_SECURITY_INFORMATIONS - gathering information about the referential 
% and the recording of trades in tick_db
%
% sec_info = get_security_informations(110)
%
% test on a non-existing security_id :
% sec_info = get_security_informations(0)
% 
% test on a dead stock :
% sec_info = get_security_informations(362)

% < MAGIC NUMBERS
% 
% >

% get_repository modes
repository_modes = {'security_market', [], 'tdinfo', [], ...
    'security_historic', [], 'security_source', [], 'security', []};

% as_of_dates matlab and string
if nargin == 1 || isempty(as_of_date)
    as_of_date = get_as_of_date();
end
as_of_date_str = datestr(as_of_date, 'yyyymmdd');

% the final structure which is the output
out = struct(...
    'summary', struct(repository_modes{:}, 'tick_db', [], ...
        'td_trading_hours', []), ...
    'full_info', struct(repository_modes{:}, 'tick_db', [], ...
        'td_trading_hours', []), ...
    'as_of_date', as_of_date);

% initialisation of summary structures
% severity ranges from 0 to 4,
% 0 is not really a problem (an information or a warning) 4 means that this
% security canot be used by the statistical engine
% already_understandable : tells if you can print the message as it is or
% if it may ba hard to understand outside of the team (for example matlab
% error (boolean)

empty_summary_struct = struct('message', {{}}, 'severity', [], ...
    'understandable', []);

%<* get full ingo and summary initilisation
%<  get_repository modes
for i = 1 : 2 : length(repository_modes)
    try
        out.summary.(repository_modes{i}) = empty_summary_struct;
        out.full_info.(repository_modes{i}) = get_repository(repository_modes{i}, security_id);
        if isempty(out.full_info.(repository_modes{i}))
            out.summary.(repository_modes{i}) = add_summary(out.summary.(repository_modes{i}), ...
                'No security_id <%d> in %s', 4, true, security_id, repository_modes{i});
        end
    catch ME
        out.summary.(repository_modes{i}) = add_summary(out.summary.(repository_modes{i}), se_stack_error_message(ME), 4, false);
    end
end
%>

%< td_trading_hours
try
    out.summary.td_trading_hours = empty_summary_struct;
    out.full_info.td_trading_hours = trading_time(out.full_info.tdinfo, as_of_date);
catch ME
    out.summary.td_trading_hours = add_summary(out.summary.td_trading_hours, se_stack_error_message(ME), 4, false);
end
%>

%< tick_db
try
    out.summary.tick_db = empty_summary_struct;
    req_mean_nb_str = ['select avg(nb_deal) mean_nb_deals, count(1) as nb_days, trading_destination_id ' ...
        ' from tick_db..trading_daily' out.full_info.tdinfo(1).global_zone_suffix ...
        ' where security_id = %d  ' ...
        ' and date > ''%s'' and date <= ''%s'' group by trading_destination_id'];
    
    out.full_info.tick_db = struct( ...
        'dates_by_td', generic_structure_request('BSIRIUS', ...
        sprintf(['select min(date) min_date, max(date) max_date, trading_destination_id ' ...
        ' from tick_db..trading_daily' out.full_info.tdinfo(1).global_zone_suffix ...
        ' where security_id = %d group by trading_destination_id'], security_id)), ...
        'mean_nb_trades_last_quarter', generic_structure_request('BSIRIUS', ...
        sprintf(req_mean_nb_str, security_id, datestr(as_of_date-91, 'yyyymmdd'), as_of_date_str)), ...
        'mean_nb_trades_last_month', generic_structure_request('BSIRIUS', ...
        sprintf(req_mean_nb_str, security_id, datestr(as_of_date-30, 'yyyymmdd'), as_of_date_str)), ...
        'mean_nb_trades_last_week', generic_structure_request('BSIRIUS', ...
        sprintf(req_mean_nb_str, security_id, datestr(as_of_date-7, 'yyyymmdd'), as_of_date_str)));
catch ME
    out.summary.tick_db = add_summary(out.summary.tick_db, se_stack_error_message(ME), 4, false);
end
%>
%>*

% %< If any severe problem so far, then give up, well that may be faster but
% % the aim here is to give comprehensive information
% tmp = get_severe_info(out.summary, 4);
% if ~isempty(tmp)
%     return;
% end
% %>

%<* reading the information and wrting a summary

%< security_market TODO more?
if ~isempty(out.full_info.security_market)
    if strcmp(out.full_info.security_market.quotation_group(1), 'null')
        out.summary.security_market = add_summary(out.summary.security_market, ...
            ['null quotation group should only be allowed for markets ' ...
            'which really does not have any quotation group. Is this ' ...
            'reality case for trading_destination_id : <%d>'], 1, true, ...
            out.full_info.security_market.trading_destination_id);
    end
end
%>

%< security_historic TODO more?
if ~isempty(out.full_info.security_historic)
    current_histo_idx = logical(out.full_info.security_historic.current_histo);
    if ~any(current_histo_idx)
        current_histo_idx(end) = true;
    end
    if ~strcmp(out.full_info.security_historic.end_date{current_histo_idx}, 'null')
        out.summary.security_historic = add_summary(out.summary.security_historic, ...
            'This stock is no longer traded (since <%s>)', 4, true, ...
            out.full_info.security_historic.end_date{current_histo_idx});
    end
% % Ne pas raconter n'importe quoi juste � cause de la as_of date
%     begin_dates = datenum(out.full_info.security_historic.begin_date, 'yyyy-mm-dd');
%     end_dates   = datenum(out.full_info.security_historic.end_date, 'yyyy-mm-dd');
%     as_of_date_histo_idx = begin_dates >= as_of_date & as_of_date <= end_dates;
%     if ~any(as_of_date_histo_idx)
%         out.summary.security_historic = add_summary(out.summary.security_historic, ...
%             'This stock is no longer traded (since <%s> considering as_of_date : <%s>)', 4, true, ...
%             out.full_info.security_historic.end_date{end}, datestr(as_of_date, 'mm/dd/yyyy'));
%     end
end
% %>

%< security_source TODO?

%>

%< security TODO?

%>

%< tick_db TODO more?
if ~isempty(out.full_info.tick_db)
    if isempty(out.full_info.tick_db.mean_nb_trades_last_week)
        if isempty(out.full_info.tick_db.dates_by_td)
            out.summary.tick_db = add_summary(out.summary.tick_db, ...
                'No trades have ever been recorded in tick_db for security_id <%d>', 4, true, security_id);
        else
            tmp_dates = sort(out.full_info.tick_db.dates_by_td.max_date);
            out.summary.tick_db = add_summary(out.summary.tick_db, ...
                'No trades have been recorded for security_id <%d> since <%s>', 4, true, security_id, tmp_dates{end});
        end
    end
else
    out.summary.tick_db = add_summary(out.summary.tick_db, ...
                'No security_id <%d> in %s', 4, true, security_id, 'tick_db..trading_daily');
end
%>

%< td_trading_hours TODO more?
if ~isempty(out.full_info.td_trading_hours)
    if ~isfinite(out.full_info.td_trading_hours(1).opening) 
        % then this stock does not have any continuous trading phase
        out.summary.security_historic = add_summary(out.summary.security_historic, ...
            'This stock has no continuous trading phase', 3, true);
    end
end
%>

%< TODO use td_info and verify consistency with other fields

%>

%>*
end


function summary_struct = add_summary(summary_struct, message, severity, understandable, varargin)
if nargin > 4
    message = sprintf(message, varargin{:});
end
if nargin <= 3
    understandable = true;
end
summary_struct.message{end+1} = message;
summary_struct.severity(end+1) = severity;
summary_struct.understandable(end+1) = understandable;
end