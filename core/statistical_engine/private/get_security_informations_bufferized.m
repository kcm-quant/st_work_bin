function out = get_security_informations_bufferized(security_id, as_of_date)
persistent memory

len_memory = length(memory);

if len_memory == 0
    memory = cell(1000, 1);
    len_memory = 1000;
end

if len_memory >= security_id
    out = memory{security_id};
else
    out = [];
end

if ~isempty(out)
    if out.as_of_date ~= as_of_date
        clear memory;
        out = get_security_informations_bufferized(security_id, as_of_date);
    end
    return;
else
    out = get_security_informations(security_id, as_of_date);
end

if security_id > len_memory
    memory(end+1:security_id) = cell(security_id-len_memory, 1);
end

memory{security_id} = out;

end