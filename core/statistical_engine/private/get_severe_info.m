function summary_struct_filtered = get_severe_info(summary_struct, required_severity, should_be_understandable)
% GET_SEVERE_INFO - ne conserve dans chacun des champs que les informations 
% ayant une s�v�rit� suffisante. S'il n'y en a pas le champs est retir�
% et s'il n'y a plus de champs � la fin, on renvoie juste du vide
% 
% sec_info = get_security_informations(362)
% summary = get_severe_info(sec_info.summary, 4, 1)

if nargin < 3
    should_be_understandable = false;
end
summary_fields = fieldnames(summary_struct);
summary_struct_filtered = summary_struct;
for i = 1 : length(summary_fields)
    idx_severe = summary_struct.(summary_fields{i}).severity >= required_severity;
    if should_be_understandable
        idx_severe = idx_severe & summary_struct.(summary_fields{i}).understandable;
    end
    if any(idx_severe)
        summary_struct_filtered.(summary_fields{i}).message(~idx_severe) = [];
        summary_struct_filtered.(summary_fields{i}).severity(~idx_severe) = [];
        summary_struct_filtered.(summary_fields{i}).understandable(~idx_severe) = [];
    else
        summary_struct_filtered = rmfield(summary_struct_filtered, summary_fields{i}); % S'il n'y en a pas le champs est retir�
    end
end
summary_fields = fieldnames(summary_struct_filtered);
if isempty(summary_fields)
    summary_struct_filtered = []; %s'il n'y a plus de champs � la fin, on renvoie juste du vide
end
end