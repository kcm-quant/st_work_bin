use quant
go




/*******************************CONTRAINTES*********************************/

--Supprime les contraintes
if exists (select 1 from sysobjects where name = 'cont_param_est' and type ='RI')
begin
	print "drop constraint cont_param_est"
	alter table quant..param_desc drop constraint cont_param_est
end
go

if exists (select 1 from sysobjects where name = 'cont_ds_dom' and type ='RI')
begin
	print "drop constraint cont_ds_dom"
	alter table quant..domain_security drop constraint cont_ds_dom
end
go

if exists (select 1 from sysobjects where name = 'cont_dr_ds' and type ='RI')
begin
	print "drop constraint cont_dr_ds"
	alter table quant..date_remove drop constraint cont_dr_ds
end
go

if exists (select 1 from sysobjects where name = 'cont_dr_e' and type ='RI')
begin
	print "drop constraint cont_dr_e"
	alter table quant..date_remove drop constraint cont_dr_e
end
go

if exists (select 1 from sysobjects where name = 'cont_cont_est' and type ='RI')
begin
	print "drop constraint cont_cont_est"
	alter table quant..context drop constraint cont_cont_est
end
go

if exists (select 1 from sysobjects where name = 'cont_job_est' and type ='RI')
begin
	print "drop constraint cont_job_est"
	alter table quant..job drop constraint cont_job_est
end
go

if exists (select 1 from sysobjects where name = 'cont_job_dom' and type ='RI')
begin
	print "drop constraint cont_job_dom"
	alter table quant..job drop constraint cont_job_dom
end
go

if exists (select 1 from sysobjects where name = 'cont_job_freq1' and type ='RI')
begin
	print "drop constraint cont_job_freq1"
	alter table quant..job drop constraint cont_job_freq1
end
go

if exists (select 1 from sysobjects where name = 'cont_job_wt1' and type ='RI')
begin
	print "drop constraint cont_job_wt1"
	alter table quant..job drop constraint cont_job_wt1
end
go

if exists (select 1 from sysobjects where name = 'cont_job_freq2' and type ='RI')
begin
	print "drop constraint cont_job_freq2"
	alter table quant..job drop constraint cont_job_freq2
end
go

if exists (select 1 from sysobjects where name = 'cont_job_wt2' and type ='RI')
begin
	print "drop constraint cont_job_wt2"
	alter table quant..job drop constraint cont_job_wt2
end
go

if exists (select 1 from sysobjects where name = 'cont_run_job' and type ='RI')
begin
	print "drop constraint cont_run_job"
	alter table quant..estimator_runs drop constraint cont_run_job
end
go

if exists (select 1 from sysobjects where name = 'cont_run_est' and type ='RI')
begin
	print "drop constraint cont_run_est"
	alter table quant..estimator_runs drop constraint cont_run_est
end
go

if exists (select 1 from sysobjects where name = 'cont_run_domaine' and type ='RI')
begin
	print "drop constraint cont_run_domaine"
	alter table quant..estimator_runs drop constraint cont_run_domaine
end
go

if exists (select 1 from sysobjects where name = 'cont_value_run' and type ='RI')
begin
	print "drop constraint cont_value_run"
	alter table quant..param_value drop constraint cont_value_run
end
go

if exists (select 1 from sysobjects where name = 'cont_param_run' and type ='RI')
begin
	print "drop constraint cont_param_run"
	alter table quant..param_value drop constraint cont_param_run
end
go

if exists (select 1 from sysobjects where name = 'cont_epq_run' and type ='RI')
begin
	print "drop constraint cont_epq_run"
	alter table quant..check_quality drop constraint cont_epq_run
end
go

if exists (select 1 from sysobjects where name = 'cont_lr_job' and type ='RI')
begin
	print "drop constraint cont_lr_job"
	alter table quant..last_run drop constraint cont_lr_job
end
go

if exists (select 1 from sysobjects where name = 'cont_lr_runs' and type ='RI')
begin
	print "drop constraint cont_lr_runs"
	alter table quant..last_run drop constraint cont_lr_runs
end
go

if exists (select 1 from sysobjects where name = 'cont_lr_vruns' and type ='RI')
begin
	print "drop constraint cont_lr_vruns"
	alter table quant..last_run drop constraint cont_lr_vruns
end
go

if exists (select 1 from sysobjects where name = 'cont_error' and type ='RI')
begin
	print "drop constraint cont_error"
	alter table quant..error drop constraint cont_error
end
go

if exists (select 1 from sysobjects where name = 'cont_association_estimator' and type ='RI')
begin
	print "drop constraint cont_association_estimator"
	alter table quant..association drop constraint cont_association_estimator
end
go

if exists (select 1 from sysobjects where name = 'cont_association_job' and type ='RI')
begin
	print "drop constraint cont_association_job"
	alter table quant..association drop constraint cont_association_job
end
go






/******************************TABLES********************************/

-- Table frequency
print "Supprime la table frequency"
go

if exists (select 1 from sysobjects where name = 'frequency' and type = 'U')
begin
    print "drop table frequency"
    drop table frequency
end
go

print "Creation de la table frequency"
go

create table frequency
(
  frequency_id   int     identity     not null,
  frequency_name varchar(32)          not null,
  comment        varchar(255)         not null  
)
lock DataRows
go

create unique nonclustered index frequency_pk on frequency(frequency_id)
go

grant all on frequency to public
go

print "Insere les valeurs par defaut"
go

insert into frequency (frequency_name, comment)
values ('daily', '')
go

insert into frequency (frequency_name, comment)
values ('weekly', '')
go

insert into frequency (frequency_name, comment)
values ('monthly', '')
go

insert into frequency (frequency_name, comment)
values ('asOfDate', '')
go




-- Table domain
if exists (select 1 from sysobjects where name = 'domain' and type = 'U')
begin
    print "drop table domain"
    drop table domain
end
go

print "Creation de la table domain"
go

create table domain
(
  domain_id       int     identity     not null,
  domain_name     varchar(32)          not null,
  comment         varchar(255)         not null,
  begin_date      date                 not null,
  end_date        date                 null
)
lock DataRows
go

create unique nonclustered index domain_pk on domain(domain_id)
go

grant all on domain to public
go




-- Table output_type
if exists (select 1 from sysobjects where name = 'output_type' and type = 'U')
begin
    print "drop table output_type"
    drop table output_type
end
go

print "Creation de la table output_type"
go

create table output_type
(
  output_type_id   int    identity   not null,
  output_type_name varchar(32)       not null,
  comment          varchar(255)      null
)
lock DataRows
go

create unique nonclustered index output_type_pk on output_type(output_type_id)
go

grant all on output_type to public
go




-- Table estimator
if exists (select 1 from sysobjects where name = 'estimator' and type = 'U')
begin
    print "drop table estimator"
    drop table estimator
end
go

print "Creation de la table estimator"
go

create table estimator
(
  estimator_id    int     identity     not null,
  estimator_name  varchar(32)  not null,
  comment         varchar(255) not null,
  matlab_run      varchar(255) null,
  matlab_check    varchar(255) null,
  output_type_id  int     null
)
lock DataRows
go

create unique nonclustered index estimator_pk on estimator(estimator_id)
go

grant all on estimator to public
go



-- Table param_desc
if exists (select 1 from sysobjects where name = 'param_desc' and type = 'U')
begin
    print "drop table param_desc"
    drop table param_desc
end
go

print "Creation de la table param_desc"
go

create table param_desc
(
  parameter_id   int     identity     not null,
  -- notion d'abcisse associee au parametre. Si non null indique que la famille concerne une courbe
  x_value        float        null,    
  parameter_name varchar(32)  not null,
  estimator_id   int          not null,-- constraint cont_param_est references estimator(estimator_id), -- famille de parametres (parameters_family) a laquelle appartient le parametre
  comment        varchar(255) not null
)
lock DataRows
go

create unique nonclustered index parameter_pk on param_desc(parameter_id)
go
create        nonclustered index estimator_sk    on param_desc(estimator_id)
go

grant all on param_desc to public
go



-- Table domain_security
if exists (select 1 from sysobjects where name = 'domain_security' and type = 'U')
begin
    print "drop table domain_security"
    drop table domain_security
end
go

print "Creation de la table domain_security"
go

create table domain_security
(
  domain_sec_id          int  identity not null,
  domain_id              int  not null,-- constraint cont_ds_dom references domain(domain_id), -- object_id a utiliser, reference la table object_component
  security_id            int  not null, -- reference un security de market..security
  -- marche concerne, permet de differencier par exemple jeu de parametres sur une valeur quand elle est traitee sur Euronext ou sur Chi-X
  trading_destination_id int  not null    
)
lock DataRows
go

create unique  nonclustered index domain_security_pk on domain_security(domain_sec_id)
go

grant all on domain_security to public
go



-- Table date_remove
if exists (select 1 from sysobjects where name = 'date_remove' and type = 'U')
begin
    print "drop table date_remove"
    drop table date_remove
end
go

print "Creation de la table date_remove"
go

create table date_remove
(
  date_remove_id	int identity not null,
  domain_id     int  not null,-- constraint cont_dr_ds references domain(domain_id),
  excluded_date     date not null, -- date a exclure
  run_id            int  null,
  estimator_id      int  not null,-- constraint cont_dr_e references estimator(estimator_id),
  stamp_date        date not null,
  login             varchar(32) null
)
lock DataRows
go

create unique nonclustered index date_remove_k on date_remove(date_remove_id)
go

grant all on date_remove to public
go



-- Table context
if exists (select 1 from sysobjects where name = 'context' and type = 'U')
begin
    print "drop table context"
    drop table context
end
go

print "Creation de la table context"
go

create table context
(
  context_id      int     identity    not null,
  context_name    varchar(36) not null,
  estimator_id    int         not null,-- constraint cont_cont_est references estimator(estimator_id),
  context_num     int         null,
  comment         varchar(255)   null
)
lock DataRows
go

create unique nonclustered index context_pk on context(context_id)
go

grant all on context to public
go



-- Table window_type
if exists (select 1 from sysobjects where name = 'window_type' and type = 'U')
begin
    print "drop table window_type"
    drop table window_type
end
go

print "Creation de la table window_type"
go
create table window_type
(
  window_type_id      int     identity    not null,
  window_type_name    varchar(32) not null,
  comment             varchar(255)   null
)
lock DataRows
go

create unique nonclustered index window_type_pk on window_type(window_type_id)
go

grant all on window_type to public
go


-- Table job
if exists (select 1 from sysobjects where name = 'job' and type = 'U')
begin
   print "drop table job"
   drop table job
end
go

print "Creation de la table job"
go

create table job
(
  job_id  		int     identity      not null,
  estimator_id         		int           not null,-- constraint cont_job_est references estimator(estimator_id),
  domain_id            		int           not null,-- constraint cont_job_dom references domain(domain_id),
  comment              		varchar(255)  null,
  run_frequency_id     		int           not null,-- constraint cont_job_freq1 references frequency(frequency_id),
  run_window_type_id      	int           null,-- constraint cont_job_wt1 references window_type(window_type_id),
  run_window_width     		int           null,
  run_frequency_date   		date          null,
  check_frequency_id     	int           not null,-- constraint cont_job_freq2 references frequency(frequency_id),
  check_window_type_id      int           null,-- constraint cont_job_wt2 references window_type(window_type_id),
  check_window_width     	int           null,
  check_frequency_date   	date          null,
  last_run                  date          null,
  next_run                  date          null,
  varargin        varchar(1025)        null
)
lock DataRows
go

create unique nonclustered index job_pk on job(job_id)
go

grant all on job to public
go


-- Table family_runs
if exists (select 1 from sysobjects where name = 'estimator_runs' and type = 'U')
begin
    print "drop table estimator_runs"
    drop table estimator_runs
end
go

print "Creation de la table estimator_runs"
go

create table estimator_runs
(
  run_id            int    identity   not null, -- numero d execution de calcul unique
  job_id            int       not null,-- constraint cont_run_job references job(job_id),
  estimator_id      int       not null,-- constraint cont_run_est references estimator(estimator_id), -- famille concernee par le calcul
  creation_date     date      not null,
  creation_time     time      not null,
  domain_id         int       not null,-- constraint cont_run_domaine references domain(domain_id), -- identifiant de l'objet sur leauel porte le calcul (action, panier)
  run_quality       float     not null, -- indicateur de qualite du run calcule lors de la creation du run
  context_num       int       null    ,  -- 
  window_end_date   date      not null,
  is_valid          bit       not null, -- flag indiquant si le jeu de parametres a ete valide. smallint pour utilisation dans l'index
  comment           varchar(255) null
)
lock DataRows
go

-- index pour replication
create unique nonclustered index run_pk     on estimator_runs(run_id)
go
-- pertinence des indexes a reevaluer a l'usage
create        nonclustered index domain_sk1 on estimator_runs(domain_id, estimator_id, creation_date)
go
create        nonclustered index estimator_sk1 on estimator_runs(estimator_id, creation_date)
go

grant all on estimator_runs to public
go



-- Table histo_family_runs
if exists (select 1 from sysobjects where name = 'histo_estimator_runs' and type = 'U')
begin
    print "drop table histo_estimator_runs"
    drop table histo_estimator_runs
end
go

print "Creation de la table histo_estimator_runs"
go

create table histo_estimator_runs
(
  run_id            int       not null, -- numero d execution de calcul unique
  job_id            int       not null,
  estimator_id      int       not null, -- famille concernee par le calcul
  creation_date     date      not null,
  creation_time     time      not null,
  domain_id         int       not null, -- identifiant de l'objet sur leauel porte le calcul (action, panier)
  is_valid          bit       not null, -- flag indiquant si le jeu de parametres a ete valide. smallint pour utilisation dans l'index
  run_quality       float     not null, -- indicateur de qualite du run calcule lors de la creation du run
  context_num       int       null    ,  -- 
  window_end_date   date      not null
)
lock DataRows
go

-- index pour replication
create unique nonclustered index run_pk     on histo_estimator_runs(run_id)
go
-- pertinence des indexes a reevaluer a l'usage
create        nonclustered index domain_sk1 on histo_estimator_runs(domain_id, estimator_id, creation_date)
go

grant all on histo_estimator_runs to public
go




-- Table param_value
if exists (select 1 from sysobjects where name = 'param_value' and type = 'U')
begin
    print "drop table param_value"
    drop table param_value
end
go

print "Creation de la table param_value"
go

create table param_value
(
  param_value_id	int    identity	not null,
  run_id       int   not null,-- constraint cont_value_run references estimator_runs(run_id), -- reference une execution de family_runs
  parameter_id int   not null,-- constraint cont_param_run references param_desc(parameter_id), -- reference un parametre de parameter_desc
  value        float null     --null values are used as NaN values in Matlab
)
lock DataRows
go

create unique  nonclustered index parameter_value_pk on param_value(run_id, parameter_id)
go

grant all on param_value to public
go




-- Table histo_param_value
if exists (select 1 from sysobjects where name = 'histo_param_value' and type = 'U')
begin
    print "drop table histo_param_value"
    drop table histo_param_value
end
go

print "Creation de la table histo_param_value"
go

create table histo_param_value
(
  run_id       int   not null, -- reference une execution de family_runs
  parameter_id int   not null, -- reference un parametre de parameter_desc
  value        float not null
)
lock DataRows
go

create unique  nonclustered index parameter_value_pk on histo_param_value(run_id, parameter_id)
go

grant all on histo_param_value to public
go




-- Table estimator_post_quality
if exists (select 1 from sysobjects where name = 'check_quality' and type = 'U')
begin
    print "drop table check_quality"
    drop table check_quality
end
go

print "Creation de la table est_post_quality"
go

create table check_quality
(
  check_quality_id     int     identity     not null,
  run_id                  int   not null,-- constraint cont_epq_run references estimator_runs(run_id),
  value                   float not null,
  stamp_date              date  not null,
  comment                 varchar(255)  null
)
lock DataRows
go

create unique  nonclustered index check_quality_pk on check_quality(check_quality_id,run_id,stamp_date)
go

grant all on check_quality to public
go




-- Table last_run
if exists (select 1 from sysobjects where name = 'last_run' and type = 'U')
begin
    print "drop table last_run"
    drop table last_run
end
go

print "Creation de la table last_run"
go

create table last_run
(
  job_id             int   not null,-- constraint cont_lr_job references job(job_id),
  context_num        int   not null,
  last_run_date      date  null,
  last_run_id        int   null,-- constraint cont_lr_runs references estimator_runs(run_id),
  last_valid_run_id  int   null,-- constraint cont_lr_vruns references estimator_runs(run_id),
  last_check_date    date  null,
  next_check_date    date  null
)
lock DataRows
go

create unique  nonclustered index last_run_pk on last_run(job_id, last_run_id, last_valid_run_id)
go

grant all on last_run to public
go




-- Table error
if exists (select 1 from sysobjects where name = 'error' and type = 'U')
begin
    print "drop table error"
    drop table error
end
go

print "Creation de la table error"
go

create table error
(
  error_id  int  identity             not null,
  job_or_run_id         int                  not null,-- constraint cont_error references job(job_id),   
  gravity_level  int                  not null,
  is_run         bit               	  not null,
  stamp_date     date              	  not null,
  stamp_time     time              	  not null,
  message        text         		  not null   
)
lock DataRows
go

create unique  nonclustered index error_pk on error(error_id, job_or_run_id)
go

grant all on error to public
go




-- Table association
if exists (select 1 from sysobjects where name = 'association' and type = 'U')
begin
    print "drop table association"
    drop table association
end
go

print "Creation de la table assoc"
go

create table association
(
  association_id  int  identity             not null,
  def_context_id      int                  null,
  rank            int                  null, 
  sec_id          int                  null,
  td_id           int                  null, 
  estimator_id    int                  null, 
  job_id    int                        not null,-- constraint cont_association_job references job(job_id),
  varargin  varchar(16)          null
)
lock DataRows
go

create unique  nonclustered index association_pk on association(association_id, def_context_id, sec_id, td_id, estimator_id, job_id)
go

grant all on association to public
go



-- Table job_status

if exists (select 1 from sysobjects where name = 'job_status' and type = 'U')
begin
    print "drop table job_status"
    drop table job_status
end
go

print "Creation de la table job_status"
go

create table job_status
(
  job_status_id     int     identity 	not null,
  job_or_run_id     int    				not null,
  status         	varchar(255)       	not null,
  is_run			bit       			not null,
  beg_date        	date                null,
  beg_time        	time                null,
  end_date        	date                null,
  end_time        	time                null,
  job_name			VARCHAR(32)			NULL
)
lock DataRows
go

create unique nonclustered index job_status_pk on job_status(job_status_id)
go

grant all on job_status to public
go






/********************************CONTRAINTES************************************/

--Ajoute les contraintes
alter table param_desc add constraint cont_param_est foreign key (estimator_id) references estimator(estimator_id)
alter table domain_security add constraint cont_ds_dom foreign key (domain_id) references domain(domain_id)
alter table date_remove add constraint cont_dr_ds foreign key (domain_id) references domain(domain_id)
alter table date_remove add constraint cont_dr_e foreign key (estimator_id) references estimator(estimator_id)
alter table context add constraint cont_cont_est foreign key (estimator_id) references estimator(estimator_id)
alter table job add constraint cont_job_est foreign key (estimator_id) references estimator(estimator_id)
alter table job add constraint cont_job_dom foreign key (domain_id) references domain(domain_id)
alter table job add constraint cont_job_freq1 foreign key (run_frequency_id) references frequency(frequency_id)
alter table job add constraint cont_job_wt1 foreign key (run_window_type_id) references window_type(window_type_id)
alter table job add constraint cont_job_freq2 foreign key (check_frequency_id) references frequency(frequency_id)
alter table job add constraint cont_job_wt2 foreign key (check_window_type_id) references window_type(window_type_id)
alter table estimator_runs add constraint cont_run_job foreign key (job_id) references job(job_id)
alter table estimator_runs add constraint cont_run_est foreign key (estimator_id) references estimator(estimator_id)
alter table estimator_runs add constraint cont_run_domaine foreign key (domain_id) references domain(domain_id)
alter table param_value add constraint cont_value_run foreign key (run_id) references estimator_runs(run_id)
alter table param_value add constraint cont_param_run foreign key (parameter_id) references param_desc(parameter_id)
alter table check_quality add constraint cont_epq_run foreign key (run_id) references estimator_runs(run_id)
alter table last_run add constraint cont_lr_job foreign key (job_id) references job(job_id)
alter table last_run add constraint cont_lr_runs foreign key (last_run_id) references estimator_runs(run_id)
alter table last_run add constraint cont_lr_vruns foreign key (last_valid_run_id) references estimator_runs(run_id)
--alter table error add constraint cont_error foreign key (job_id) references job(job_id)
alter table association add constraint cont_association_estimator foreign key (estimator_id) references estimator(estimator_id)
alter table association add constraint cont_association_job foreign key (job_id) references job(job_id)
go





/******************************PROCEDURES STOCKEES**********************************/



-- ins_estimator
if exists (select 1 from sysobjects where name = 'ins_estimator' and type = 'P')
begin
   print "drop procedure ins_estimator"
   drop procedure ins_estimator
end
go

print "create procedure ins_estimator"
go

create procedure ins_estimator
(
  @estimator_name   varchar(32)  ,
  @comment			varchar(255) ,
  @matlab_run		varchar(255) = null ,
  @matlab_check 	varchar(255) = null ,
  @output_type_id  int = 1
)
as
begin
  declare @myrowcount      int    
  declare @donotmanagetran int    
  declare @myerror         int    
  declare @estimator_id    int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into estimator (
     estimator_name,
     comment       ,
     matlab_run    ,
     matlab_check  ,  
     output_type_id
  ) values (
     @estimator_name,
     @comment		,
     @matlab_run	,
     @matlab_check  ,
     @output_type_id)
    
 select @estimator_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans estimator [ins_estimator] a echouee!"    
    return 100001    
  end 
  
  select @estimator_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_estimator to public
go

exec sp_procxmode "ins_estimator",Unchained
go



-- ins_param_desc
if exists (select 1 from sysobjects where name = 'ins_param_desc' and type = 'P')
begin
   print "drop procedure ins_param_desc"
   drop procedure ins_param_desc
end
go

print "create procedure ins_param_desc"
go

create procedure ins_param_desc
(
  @parameter_name   varchar(32)  ,
  @estimator_id		int ,
  @comment		    varchar(255) = "" ,
  @x_value 	        float = null
)
as
begin
  declare @myrowcount      int    
  declare @donotmanagetran int    
  declare @myerror         int    
  declare @parameter_id    int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into param_desc (
     parameter_name,
     estimator_id  ,
     comment       ,
     x_value    
  ) values (
     @parameter_name,
     @estimator_id	,
     @comment   	,
     @x_value       )
    
 select @parameter_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans param_desc [ins_param_desc] a echouee!"    
    return 100001    
  end 
  
  select @parameter_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_param_desc to public
go

exec sp_procxmode "ins_param_desc",Unchained
go



-- ins_domain
if exists (select 1 from sysobjects where name = 'ins_domain' and type = 'P')
begin
   print "drop procedure ins_domain"
   drop procedure ins_domain
end
go

print "create procedure ins_domain"
go

create procedure ins_domain
(
  @domain_name     varchar(32)   , 
  @comment         varchar(255)  , 
  @begin_date      date          , 
  @end_date        date           
)
as
begin
  declare @myrowcount      int    
  declare @donotmanagetran int    
  declare @myerror         int    
  declare @domain_id       int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into domain (
     domain_name   ,
     comment       ,
     begin_date    ,
     end_date      
  ) values (
     @domain_name   ,
     @comment    	,
     isnull(@begin_date,getdate()) 	,
     @end_date )
    
 select @domain_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans domain [ins_domain] a echouee!"    
    return 100001    
  end 
  
  select @domain_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_domain to public
go

exec sp_procxmode "ins_domain",Unchained
go



-- ins_domain_sec
if exists (select 1 from sysobjects where name = 'ins_domain_sec' and type = 'P')
begin
   print "drop procedure ins_domain_sec"
   drop procedure ins_domain_sec
end
go

print "create procedure ins_domain_sec"
go

create procedure ins_domain_sec
(
  @domain_id                   int   , 
  @security_id                 int  , 
  @trading_destination_id      int      
)
as
begin
  declare @myrowcount      int    
  declare @donotmanagetran int    
  declare @myerror         int    
  declare @domain_sec_id   int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into domain_security (
     domain_id              ,
     security_id            ,
     trading_destination_id  
  ) values (
     @domain_id              ,
     @security_id    	     ,
     @trading_destination_id  )
    
 select @domain_sec_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans domain_security [ins_domain_sec] a echouee!"    
    return 100001    
  end 
  
  select @domain_sec_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_domain_sec to public
go

exec sp_procxmode "ins_domain_sec",Unchained
go



-- ins_association
if exists (select 1 from sysobjects where name = 'ins_association' and type = 'P')
begin
   print "drop procedure ins_association"
   drop procedure ins_association
end
go

print "create procedure ins_association"
go
create procedure ins_association
(
  @def_context_id      int  ,
  @rank			    int = null,
  @sec_id		    int = null,
  @td_id 	        int = null,
  @estimator_id     int ,
  @job_id        int,
  @varargin   varchar(16) = null
)
as
begin
  declare @myrowcount      int    
  declare @donotmanagetran int    
  declare @myerror         int
  declare @association_id   int     
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into association (
     def_context_id   ,
     rank          ,
     sec_id        ,
     td_id         ,  
     estimator_id  ,
     job_id     ,
     varargin
  ) values (
     @def_context_id   ,
     @rank          ,
     @sec_id        ,
     @td_id         ,  
     @estimator_id  ,
     @job_id     ,
     @varargin)
    
 select @association_id=@@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans association [ins_association] a echouee!"    
    return 100001    
  end 
  
  select @association_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_association to public
go

exec sp_procxmode "ins_association",Unchained
go



-- ins_job
if exists (select 1 from sysobjects where name = 'ins_job' and type = 'P')
begin
   print "drop procedure ins_job"
   drop procedure ins_job
end
go

print "create procedure ins_job"
go

create procedure ins_job
(
  @estimator_id              int         ,
  @domain_id            	 int         ,
  @comment              	 varchar(255),
  @run_frequency_id     	 int         ,
  @run_window_type_id      	 int         ,
  @run_window_width     	 int         ,
  @run_frequency_date   	 date        ,
  @check_frequency_id     	 int         ,
  @check_window_type_id      int         ,
  @check_window_width     	 int         ,
  @check_frequency_date   	 date        ,
  @varargin                  varchar(1025) 
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  declare @job_id   int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into job (
    estimator_id         ,
    domain_id            ,
    comment              ,
    run_frequency_id     ,
    run_window_type_id   ,
    run_window_width     ,
    run_frequency_date   ,
    check_frequency_id   ,
    check_window_type_id ,
    check_window_width   ,
    check_frequency_date,
    varargin 
  ) values (
    @estimator_id         ,
    @domain_id            ,
    @comment              ,
    @run_frequency_id     ,
    @run_window_type_id   ,
    @run_window_width     ,
    @run_frequency_date   ,
    @check_frequency_id   ,
    @check_window_type_id ,
    @check_window_width   ,
    @check_frequency_date ,
    @varargin
    )
    
 select @job_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans job [ins_job] a echouee!"    
    return 100001    
  end 
  
  -- verification
  if ((@run_frequency_id = 3 or @run_frequency_id = 4) and @run_frequency_date = null)
  begin
	if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100002 "l'insertion dans job [ins_job] a echouee! : run_frequency_date obligatoire"    
    return 100002 
  end
  
  if ((@check_frequency_id = 3 or @check_frequency_id = 4) and @check_frequency_date = null)
  begin
	if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100003 "l'insertion dans job [ins_job] a echouee! : check_frequency_date obligatoire"    
    return 100003 
  end  
  
  select @job_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_job to public
go

exec sp_procxmode "ins_job",Unchained
go



-- ins_frequency
if exists (select 1 from sysobjects where name = 'ins_frequency' and type = 'P')
begin
   print "drop procedure ins_frequency"
   drop procedure ins_frequency
end
go

print "create procedure ins_frequency"
go

create procedure ins_frequency
(
  @frequency_name            varchar(32) ,
  @comment              	 varchar(255)   
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  declare @frequency_id          int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into frequency (
    frequency_name       ,
    comment              
  ) values (
    @frequency_name       ,
    @comment              )
    
 select @frequency_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans frequency [ins_frequency] a echouee!"    
    return 100001    
  end 
  
  select @frequency_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_frequency to public
go

exec sp_procxmode "ins_frequency",Unchained
go



-- ins_context
if exists (select 1 from sysobjects where name = 'ins_context' and type = 'P')
begin
   print "drop procedure ins_context"
   drop procedure ins_context
end
go

print "create procedure ins_context"
go

create procedure ins_context
(
  @context_name            varchar(32) ,
  @estimator_id            int , 
  @context_num             int ,
  @comment                 varchar(255)   
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  declare @context_id          int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into context (
    context_name       ,
    estimator_id ,
    context_num , 
    comment              
  ) values (
    @context_name       ,
    @estimator_id   ,
    @context_num   , 
    @comment              )
    
 select @context_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans context [ins_context] a echouee!"    
    return 100001    
  end 
  
  select @context_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_context to public
go

exec sp_procxmode "ins_context",Unchained
go



-- ins_output_type
if exists (select 1 from sysobjects where name = 'ins_output_type' and type = 'P')
begin
   print "drop procedure ins_output_type"
   drop procedure ins_output_type
end
go

print "create procedure ins_output_type"
go

create procedure ins_output_type
(
  @output_type_name            varchar(32) ,
  @comment                 varchar(255)   
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  declare @output_type_id          int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into output_type (
    output_type_name       ,
    comment              
  ) values (
    @output_type_name       , 
    @comment              )
    
 select @output_type_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans output_type [ins_output_type] a echouee!"    
    return 100001    
  end 
  
  select @output_type_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_output_type to public
go

exec sp_procxmode "ins_output_type",Unchained
go



-- ins_error
if exists (select 1 from sysobjects where name = 'ins_error' and type = 'P')
begin
   print "drop procedure ins_error"
   drop procedure ins_error
end
go

print "create procedure ins_error"
go

create procedure ins_error
(
  @job_or_run_id            int,
  @gravity_level            int,
  @is_run            int,
  @stamp_date            date,
  @stamp_time            TIME,
  @message                 VARCHAR(4096)
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  declare @error_id          int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into error (
    job_or_run_id,
    gravity_level,
    is_run,
    stamp_date,
    stamp_time,
    message              
  ) values (
    @job_or_run_id       , 
    @gravity_level       , 
    @is_run       , 
    @stamp_date       , 
    @stamp_time       , 
    @message              )
    
 select @error_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans error [ins_error] a echouee!"    
    return 100001    
  end 
  
  select @error_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_error to public
go

exec sp_procxmode "ins_error",Unchained
go




-- ins_window_type
if exists (select 1 from sysobjects where name = 'ins_window_type' and type = 'P')
begin
   print "drop procedure ins_window_type"
   drop procedure ins_window_type
end
go

print "create procedure ins_window_type"
go

create procedure ins_window_type
(
  @window_type_name            varchar(32) ,
  @comment                 varchar(255)   
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  declare @window_type_id          int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into window_type (
    window_type_name       ,
    comment              
  ) values (
    @window_type_name       , 
    @comment              )
    
 select @window_type_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans window_type [ins_window_type] a echouee!"    
    return 100001    
  end 
  
  select @window_type_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_window_type to public
go

exec sp_procxmode "ins_window_type",Unchained
go



-- ins_estimator_runs
if exists (select 1 from sysobjects where name = 'ins_estimator_runs' and type = 'P')
begin
   print "drop procedure ins_estimator_runs"
   drop procedure ins_estimator_runs
end
go

print "create procedure ins_estimator_runs"
go

create procedure ins_estimator_runs
(
  @job_id                int   ,
  @estimator_id          int   ,
  @domain_id             int   ,  
  @run_quality           float ,
  @context_num           int   =  null,  
  @window_end_date       date  =  null,
  @last_run_date         date  =  null,
  @creation_date         date  = null,
  @creation_time         time  = null,
  @comment               varchar = null,
  @is_valid              bit   =  0
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  declare @run_id                int 
  declare @count                 int   
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into estimator_runs (
    job_id          ,
    estimator_id    ,
    creation_date   ,
    creation_time   ,
    domain_id       ,
    run_quality     ,
    context_num     ,
    window_end_date,
    comment,
    is_valid
  ) values (
    @job_id         ,
    @estimator_id   ,
    isnull(@creation_date  ,current_date()),
    isnull(@creation_time  ,current_time()),
    @domain_id      ,
    @run_quality    ,
    @context_num    ,
    isnull(@window_end_date,getdate()), 
    @comment,
    @is_valid
)
  
    
 select @run_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans estimator_runs [ins_estimator_runs] a echouee!"    
    return 100001    
  end 
  
  update job set last_run = isnull(@last_run_date,getdate()) where job_id = @job_id
  
  select @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100002 "l'insertion dans estimator_runs [ins_estimator_runs] a echouee!"    
    return 100002    
  end 
  
  
  select @count = count(1) from last_run where job_id=@job_id and context_num=@context_num
  if(@count = 0)
  begin
		insert into last_run 
		(
		   job_id,
		   context_num,
		   last_run_id
		) values (
		   @job_id,
		   @context_num,
		   @run_id
		)
		select @myrowcount=@@rowcount, @myerror=@@error    
   
	  if @myerror != 0 or @myrowcount != 1    
	  begin    
		if (@donotmanagetran = 0)    
			rollback tran    
		raiserror 100003 "l'insertion dans last_run [ins_estimator_runs] a echouee!"    
		return 100003    
	  end 
  end 
  else if(@count = 1)
  begin
		update last_run set last_run_id=@run_id,last_check_date=null, 
		last_run_date=isnull(@last_run_date,getdate()) where job_id=@job_id 
		if @myerror != 0 or @myrowcount != 1    
		  begin    
			if (@donotmanagetran = 0)    
				rollback tran    
			raiserror 100004 "l'update de last_run [ins_estimator_runs] a echouee!"    
			return 100004    
		  end 
  end 
  else
  begin
	if (@donotmanagetran = 0)    
		rollback tran    
	raiserror 100005 "l'update de last_run [ins_estimator_runs] a echouee!"    
	return 100005    
  end 

    
  select @run_id
  
  if (@donotmanagetran = 0)
     commit tran

  return 0
end
go

grant execute on ins_estimator_runs to public
go

exec sp_procxmode "ins_estimator_runs",Unchained
go




-- ins_check_quality
if exists (select 1 from sysobjects where name = 'ins_check_quality' and type = 'P')
begin
   print "drop procedure ins_check_quality"
   drop procedure ins_check_quality
end
go

print "create procedure ins_check_quality"
go
create procedure ins_check_quality
(
  @job_id     int,
  @run_id     int,
  @value      float, 
  @stamp_date  date,
  @last_check_date         date  =  null
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int   
  declare @check_quality_id      int  
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran   
  
  insert into check_quality (
     run_id     ,
     value      ,
     stamp_date
     ) values (
     @run_id   ,
     @value    ,
     @stamp_date)
     
   select @check_quality_id = @@identity , @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans check_quality [ins_check_quality] a echouee!"    
    return 100001    
  end 
  
  update last_run set r.last_check_date = isnull(@last_check_date,getdate()) from last_run r, estimator_runs er
  where r.job_id = @job_id and r.context_num = er.context_num and r.job_id = er.job_id
  
  select @myrowcount=@@rowcount, @myerror=@@error 
  
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100002 "l'insertion dans check_quality [ins_check_quality] a echouee : !"    
    return 100002    
  end 
  
  select @check_quality_id
  
  if (@donotmanagetran = 0)
     commit tran
     
 return 0
end

go

grant execute on ins_check_quality to public
go

exec sp_procxmode "ins_check_quality",Unchained
go



-- ins_job_status
if exists (select 1 from sysobjects where name = 'ins_job_status' and type = 'P')
begin
   print "drop procedure ins_job_status"
   drop procedure ins_job_status
end
go

print "create procedure ins_job_status"
go

create procedure ins_job_status
(
  @job_or_run_id            	int,
  @status               varchar(255),     
  @is_run               bit,   
  @beg_date             date,   
  @beg_time             time,   
  @end_date             date,   
  @end_time             time,
  @job_name				VARCHAR(32)
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  declare @job_status_id          int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into job_status (
    job_or_run_id,
	status,     
	is_run,   
	beg_date,   
	beg_time,   
	end_date,   
	end_time,
	job_name
  ) values (
    @job_or_run_id,
	@status,   
	@is_run,   
	@beg_date,   
	@beg_time,   
	@end_date,   
	@end_time,
	@job_name)
    
 select @job_status_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans job_status [ins_job_status] a echouee!"    
    return 100001    
  end 
  
  select @job_status_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_job_status to public
go

exec sp_procxmode "ins_job_status",Unchained
go


