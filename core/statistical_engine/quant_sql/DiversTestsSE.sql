

--Pas de run non check� sans erreur

select * from job_status where is_run=0 and job_or_run_id in (
	select last_run_id from last_run
	having next_check_date=min(next_check_date)
)

select * from job where job_id in (
	select job_id from last_run
	having next_check_date=min(next_check_date)
)


--Pas de job non calcul� sans erreur

select * from job
having next_run=min(next_run)
and job_id not in (
	select job_or_run_id from error where is_run=1
)

select * from job having next_run=min(next_run)


--Bon remplissage de la table error

select * from job_status j where (j.status!='killed' and j.status!='ended') and j.job_or_run_id not in (
	select job_or_run_id from error e where e.is_run=j.is_run and j.end_date=e.stamp_date
)


-- Tous les domains utilis�s dans les jobs sont-ils les plus r�cents? Visiblement pas...

select * from perimeter_job where job_id in (
	select job_id from job where domain_id=1646
)
select * from perimeter_job where job_id in (
	select job_id from job where domain_id=1750
)
select * from domain where domain_name in (
	select domain_name from domain where domain_id=1646
)

select * from job j where j.domain_id not in (
	select max(domain_id) from domain group by domain_name, domain_type_variable, domain_type_id
)
and j.job_id in (select job_id from perimeter_job p where p.is_active=1)



-- Autant d'associations que de jobs actifs

select count(1) from job j where j.job_id in (select job_id from perimeter_job p where p.is_active=1)
select count(1) from association

