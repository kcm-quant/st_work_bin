/**********************
*******STRUCTURE*******
***********************/

--estimator description
SELECT
	e.estimator_name,
	e.comment,
	o.output_type_name,
	o.comment
FROM
	quant..estimator e,
	quant..output_type o
WHERE
	e.output_type_id=o.output_type_id
ORDER BY
	e.estimator_name,
	o.output_type_name
	
--context description
SELECT
	e.estimator_name,
	c.context_name,
	c.comment
FROM
	quant..context c,
	quant..estimator e
WHERE
	c.estimator_id=e.estimator_id
ORDER BY
	e.estimator_name,
	c.context_name


--parameters description
SELECT
	e.estimator_name,
	p.parameter_name,
	p.comment
FROM
	quant..param_desc p,
	quant..estimator e
WHERE
	p.estimator_id=e.estimator_id
ORDER BY
	e.estimator_name,
	p.parameter_name


--domain description
SELECT
	d.domain_name,
	d.begin_date,
	d.end_date,
	ds.security_id,
	ds.trading_destination_id,
	d.comment
FROM
	quant..domain d,
	quant..domain_security ds
WHERE
	d.domain_id=ds.domain_id
ORDER BY
	d.domain_name


--job description
SELECT
	e.estimator_name,
	d.domain_name,
	rf.frequency_name,
	rw.window_type_name,
	j.run_window_width,
	j.run_frequency_date,
	cf.frequency_name,
	cw.window_type_name,
	j.check_window_width,
	j.check_frequency_date,
	j.last_run,
	j.next_run,
	j.comment
FROM
	quant..estimator e,
	quant..domain d,
	quant..frequency rf,
	quant..frequency cf,
	quant..window_type rw,	
	quant..window_type cw,
	quant..job j
WHERE
	j.estimator_id=e.estimator_id
	AND j.domain_id=d.domain_id
	AND j.run_frequency_id=rf.frequency_id
	AND j.run_window_type_id=rw.window_type_id
	AND j.check_frequency_id=cf.frequency_id
	AND j.check_window_type_id=cw.window_type_id
ORDER BY
	e.estimator_name,
	d.domain_name
	


/**********************
*******RESULTS*******
***********************/



--error value: RUN
SELECT
	es.estimator_name,
	ds.security_id,
	ds.trading_destination_id,
	e.job_or_run_id,
	e.is_run,
	e.message
FROM
	error e,
	job j,
	domain_security ds,
	estimator es
WHERE
	e.is_run=1
	AND e.job_or_run_id=j.job_id
	AND j.domain_id=ds.domain_id
	AND es.estimator_id=j.estimator_id
ORDER BY
	es.estimator_name,
	ds.trading_destination_id,
	ds.security_id
GO


--error value: CHECK
SELECT
	es.estimator_name,
	ds.security_id,
	ds.trading_destination_id,
	e.job_or_run_id,
	e.is_run,
	e.stamp_date,
	e.stamp_time,
	e.message
FROM
	error e,
	domain_security ds,
	estimator es,
	estimator_runs er
WHERE
	e.is_run=0
	AND e.job_or_run_id=er.run_id
	AND er.domain_id=ds.domain_id
	AND es.estimator_id=er.estimator_id
ORDER BY
	es.estimator_name,
	ds.trading_destination_id,
	ds.security_id,
	e.stamp_date,
	e.stamp_time
GO


--param value
SELECT
	e.estimator_name,
	ds.security_id,
	ds.trading_destination_id,
	er.stamp_date,
	c.context_name,
	p.parameter_name,
	pv.value,
	er.run_quality
FROM
	param_value pv,
	param_desc p,
	estimator_runs er,
	estimator e,
	job j,
	domain d,
	context c,
	domain_security ds
WHERE
	pv.parameter_id=p.parameter_id
	AND pv.run_id=er.run_id
	AND e.estimator_id=er.estimator_id
	AND j.job_id=er.job_id
	AND j.domain_id=d.domain_id
	AND er.context_id=c.context_id
	AND ds.domain_id=d.domain_id
ORDER BY
	e.estimator_name,
	ds.trading_destination_id,
	ds.security_id,
	p.parameter_name
GO	
	

--check value
SELECT
	e.estimator_name,
	ds.security_id,
	ds.trading_destination_id,
	cq.stamp_date,
	c.context_name,
	cq.value,
	er.run_quality
FROM
	check_quality cq,
	estimator_runs er,
	estimator e,
	job j,
	domain d,
	context c,
	domain_security ds	
WHERE
	er.run_id=cq.run_id
	AND e.estimator_id=er.estimator_id
	AND j.job_id=er.job_id
	AND j.domain_id=d.domain_id
	AND er.context_id=c.context_id
	AND ds.domain_id=d.domain_id
ORDER BY
	e.estimator_name,
	ds.trading_destination_id,
	ds.security_id
GO


