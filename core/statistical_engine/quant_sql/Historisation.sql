


BEGIN TRAN
DECLARE @myerror INT
INSERT INTO histo_check_quality SELECT * FROM check_quality
DELETE FROM check_quality
IF @myerror != 0
  COMMIT TRAN
ELSE
  ROLLBACK TRAN
GO


BEGIN TRAN
DECLARE @myerror INT
INSERT INTO histo_error SELECT * FROM error
DELETE FROM error
IF @myerror != 0
  COMMIT TRAN
ELSE
  ROLLBACK TRAN
GO


BEGIN TRAN
DECLARE @myerror INT
INSERT INTO histo_estimator_runs SELECT * FROM estimator_runs
DELETE FROM estimator_runs
IF @myerror != 0
  COMMIT TRAN
ELSE
  ROLLBACK TRAN
GO


BEGIN TRAN
DECLARE @myerror INT
INSERT INTO histo_job_status SELECT * FROM job_status
DELETE FROM job_status
IF @myerror != 0
  COMMIT TRAN
ELSE
  ROLLBACK TRAN
GO


BEGIN TRAN
DECLARE @myerror INT
INSERT INTO histo_param_value SELECT * FROM param_value
DELETE FROM param_value
IF @myerror != 0
  COMMIT TRAN
ELSE
  ROLLBACK TRAN
GO

