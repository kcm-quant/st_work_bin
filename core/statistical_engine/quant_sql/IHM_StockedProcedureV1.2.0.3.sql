
use quant
go

/* DELETE */
if exists (select 1 from sysobjects where type='P' and name='del_quant_removed_date')
begin
 print "Suppression del_quant_removed_date"
 drop procedure del_quant_removed_date
end
go

create procedure del_quant_removed_date (
	@id	int
)
as
begin
	delete
		from	date_remove
		where	date_remove_id = @id
end
go

grant execute on del_quant_removed_date to cdv_users
go

grant execute on del_quant_removed_date to sel_users
go

exec sp_procxmode "del_quant_removed_date",Unchained
go


/* INSERT */
if exists (select 1 from sysobjects where type='P' and name='insert_quant_removed_dates')
begin
 print "Suppression insert_quant_removed_dates"
 drop procedure insert_quant_removed_dates
end
go
create procedure insert_quant_removed_dates (
	@excluded_date	date,
	@run_id			int,
	@login			varchar(32)
)
as
	declare @domain_id		int
	declare @estimator_id	int
begin
	select	@domain_id = domain_id,
			@estimator_id = estimator_id
		from		estimator_runs
		where		run_id = @run_id
	
	insert
		into date_remove (
			excluded_date,
			login,
			domain_id,
			estimator_id,
			stamp_date
		)
		values (
			@excluded_date,
			@login,
			@domain_id,
			@estimator_id,
			current_date()
		)
end
go

grant execute on insert_quant_removed_dates to cdv_users
go

grant execute on insert_quant_removed_dates to sel_users
go

exec sp_procxmode "insert_quant_removed_dates",Unchained
go


/* SELECT */
if exists (select 1 from sysobjects where type='P' and name='sel_quant_check_quality')
begin
 print "Suppression sel_quant_check_quality"
 drop procedure sel_quant_check_quality
end
go

create procedure sel_quant_check_quality
(
	@run_id		int
)
as
begin
	/* Adaptive Server has expanded all '*' elements in the following statement */ select	check_quality.check_quality_id, check_quality.run_id, check_quality.value, check_quality.stamp_date, check_quality.comment
	
	from	check_quality
	
	where	run_id = @run_id
	
	order by stamp_date
	
end
go

grant execute on sel_quant_check_quality to cdv_users
go

grant execute on sel_quant_check_quality to sel_users
go

exec sp_procxmode "sel_quant_check_quality",Unchained
go


if exists (select 1 from sysobjects where type='P' and name='sel_quant_domains')
begin
 print "Suppression sel_quant_domains"
 drop procedure sel_quant_domains
end
go

create procedure sel_quant_domains 
as
begin
	/* Adaptive Server has expanded all '*' elements in the following statement */ select domain.domain_id, domain.domain_name, domain.comment, domain.domain_type_id, domain.domain_type_variable, domain.domain_type_rank
		from		domain
		order by	domain_name
end
go

grant execute on sel_quant_domains to cdv_users
go

grant execute on sel_quant_domains to sel_users
go

exec sp_procxmode "sel_quant_domains",Unchained
go


if exists (select 1 from sysobjects where type='P' and name='sel_quant_estimator')
begin
 print "Suppression sel_quant_estimator"
 drop procedure sel_quant_estimator
end
go 

create procedure sel_quant_estimator
(
	@estimator_id	int
)
as 
begin
	/* possibilitÚ d'appeler la procÚdure sel_quant_estimaotrs ? */
	select	estimator_id,
			estimator_name,
			estimator.comment,
			matlab_run,
			matlab_check,
			output_type.output_type_name
			
	from	estimator,
			output_type
			
	where	estimator.output_type_id = output_type.output_type_id
	and		estimator_id = @estimator_id
	
end
go

grant execute on sel_quant_estimator to cdv_users
go

grant execute on sel_quant_estimator to sel_users
go

exec sp_procxmode "sel_quant_estimator",Unchained
go


if exists (select 1 from sysobjects where type='P' and name='sel_quant_estimatordesc')
begin
 print "Suppression sel_quant_estimatordesc"
 drop procedure sel_quant_estimatordesc
end
go

create procedure sel_quant_estimatordesc
(
	@estimator_id	int
)
as 
begin
	select
		est.estimator_name, 
		est.estimator_id , 
		ctxt.context_name
	from
		estimator est ,
		context ctxt
	where est.estimator_id = @estimator_id
		and ctxt.estimator_id = est.estimator_id
	
end
go

grant execute on sel_quant_estimatordesc to cdv_users
go

grant execute on sel_quant_estimatordesc to sel_users
go

exec sp_procxmode "sel_quant_estimatordesc",Unchained
go


if exists (select 1 from sysobjects where type='P' and name='sel_quant_job')
begin
 print "Suppression sel_quant_job"
 drop procedure sel_quant_job
end
go

create procedure sel_quant_job
(
	@estimator_id	int
)
as 
begin
	create table #result (
		job_id	int,
		next_run date null,
		domain_name varchar(32) null,
		domain_type varchar(255) null,
		validation_date date null,
		is_active bit,
		status varchar(255) null,
		matlab_job varchar(32) null,
		error text null,
		is_associated bit,
		association_id int null
	)
	
	insert into #result
	select 
		a.job_id,
		a.next_run, 
		b.domain_name, 
		c.domain_type_name,
		null,
		d.is_active,
		null, /*status*/
		null, /*matlab_job*/
		f.message,
		1,
		h.association_id
	from
		job a,
		domain b,
		domain_type c,
		perimeter_job d,
		perimeter_domain e,
		error f, 
		last_run g,
		association h
	where 
		a.estimator_id = @estimator_id
		and a.domain_id = b.domain_id 
		and b.domain_type_id = c.domain_type_id
		and e.domain_id = b.domain_id
		and d.job_id = a.job_id
		and d.perimeter_id = e.perimeter_id
		and g.job_id = a.job_id 
		and f.job_or_run_id =* g.last_run_id
		and h.job_id = a.job_id
		

/* validation_date */
update #result 
set validation_date = (select distinct a.stamp_date from estimator_runs a, last_run b 
						where a.run_id = b.last_valid_run_id 
							and #result.job_id = b.job_id)


create table #result2 (
		job_id	int,
		max_job_status int,
		status varchar(255) null,
		job_name varchar(32) null
)

insert into #result2 
select distinct a.job_or_run_id, max(job_status_id), "", ""
	from job_status a, #result b
	where 
	a.job_or_run_id = b.job_id 
	and a.is_run = 1 
group by job_or_run_id

update #result2 
set status = a.status, job_name = a.job_name
from job_status a
where #result2.job_id = a.job_or_run_id
and #result2.max_job_status = a.job_status_id
		  
/* status */
update #result
set status = b.status, matlab_job = b.job_name
from #result2 b
where #result.job_id = b.job_id

/*is_associated , association_id
update #result 
set association_id = (select association_id from association where job_id = #result.job_id)
*/

/* Adaptive Server has expanded all '*' elements in the following statement */ 
select #result.job_id, #result.next_run, #result.domain_name, #result.domain_type, #result.validation_date, #result.is_active, #result.status, #result.matlab_job, #result.error, #result.is_associated, #result.association_id 
from #result order by #result.job_id

truncate table #result
drop table #result

truncate table #result2
drop table #result2
end
go

grant execute on sel_quant_job to cdv_users
go

grant execute on sel_quant_job to sel_users
go

exec sp_procxmode "sel_quant_job",Unchained
go


if exists (select 1 from sysobjects where type='P' and name='sel_quant_asso')
begin
 print "Suppression sel_quant_asso"
 drop procedure sel_quant_asso
end
go

create procedure sel_quant_asso
(
	@estimator_id	int
)
as 
begin
create table #resultasso (
		security_id				int null,
		trading_destination_id	int null,
		varargin				varchar(16) null,
		context_id				int null,
		context_name			varchar(36) null,
		rank					int null,
		run_id					int null,
		
		domain_name				varchar(32) null,
		is_valid				bit,
		job_id					int null,
		estimator_id			int null,
		domain_id				int null,
		run_quality				float null,
		run_date				date null,
		id_valid				bit not null,
		comment					varchar(255)
	)

insert into #resultasso
	select	a.security_id, 
			a.trading_destination_id, 
			a.varargin, 
			a.context_id, 
			b.context_name, 
			a.rank, 
			c.last_run_id, 
			e.domain_name, 
			(case when c.last_valid_run_id = null then 0 else 1 end),
			a.job_id, 
			
			a.estimator_id, 
			e.domain_id, 
			f.run_quality,							
			f.stamp_date as run_date,
			f.is_valid,
			f.comment
	from	association a, context b, last_run c, job d, domain e, estimator_runs f
	where	a.estimator_id = @estimator_id
		and a.context_id = b.context_id
		and a.context_id = c.context_id
		and a.job_id = c.job_id
		and a.job_id = d.job_id
		and d.domain_id = e.domain_id
		and f.run_id = c.last_run_id
		and f.context_id = a.context_id
		
	/* Adaptive Server has expanded all '*' elements in the following statement */
	 select #resultasso.security_id, 
	 #resultasso.trading_destination_id, #resultasso.varargin, #resultasso.context_id as context_num, #resultasso.context_name as context, #resultasso.rank, 
	 #resultasso.run_id, #resultasso.domain_name as domain, #resultasso.is_valid, #resultasso.job_id, #resultasso.estimator_id, #resultasso.domain_id, 
	 #resultasso.run_quality, #resultasso.run_date, #resultasso.is_valid, #resultasso.comment
	 
	 from #resultasso order by job_id		

	truncate table #resultasso
	drop table #resultasso
	
end
go

grant execute on sel_quant_asso to cdv_users
go

grant execute on sel_quant_asso to sel_users
go

exec sp_procxmode "sel_quant_asso",Unchained
go


if exists (select 1 from sysobjects where type='P' and name='sel_quant_desc_recap')
begin
 print "Suppression sel_quant_desc_recap"
 drop procedure sel_quant_desc_recap
end
go
create procedure sel_quant_desc_recap
(
	@estimator_id	int
)
as 
begin
create table #resultdesc (
		target					varchar(32) null,
		total					int null,
		active					int null,
		inactive				int null,
		validated				int null,
		error					int null
	)

insert into #resultdesc values("Job", null, null,null,null,null)

update #resultdesc set total = (select count(distinct j.job_id) from job j, perimeter_job  pj
							where j.estimator_id = @estimator_id AND pj.job_id =j.job_id)


update #resultdesc set active = (select count(distinct j.job_id) from job j, perimeter_job  pj
							where j.estimator_id = @estimator_id and pj.is_active = 1 and pj.job_id =j.job_id)

update #resultdesc set inactive = (select count(distinct j.job_id) from job j, perimeter_job  pj 
								where j.estimator_id = @estimator_id AND pj.is_active = 0 AND pj.job_id =j.job_id)


update #resultdesc set validated = (select count(distinct(job_id)) from last_run where job_id in (select distinct j.job_id from job j, perimeter_job  pj
																							  where j.estimator_id = @estimator_id AND pj.is_active = 1 AND pj.job_id =j.job_id)
																							and last_valid_run_id is not null)


update #resultdesc set error = (select count(1) from  job j, perimeter_job  pj, job_status js, 
			(select js.job_or_run_id job_id, max(js.session_num) max_session  from job_status js 
			  where  convert(int,js.is_run) = 1 and  job_or_run_id in (select job_id from perimeter_job where is_active=1)
		group by js.job_or_run_id) max_s
		where j.estimator_id = @estimator_id AND
			pj.is_active = 1 AND
			pj.job_id =j.job_id AND
			j.job_id = max_s.job_id AND
			js.job_or_run_id = j.job_id and
			js.session_num =max_s.max_session and
			js.status ='cancelled'
		)


insert into #resultdesc values("Association", null, null,null,null,null)
update #resultdesc set total = (select count(job_id) from association where estimator_id = @estimator_id) where target = "Association"

update #resultdesc set validated = (select count(distinct(job_id)) from last_run where job_id in (select job_id from association where estimator_id = @estimator_id)
								and last_valid_run_id is not null)	where target = "Association"

update #resultdesc set error = (select count(1) from association ass, job_status js,  (select js.job_or_run_id job_id, max(js.session_num) max_session 
							from job_status js 
							where convert(int,js.is_run) = 1 
							and job_or_run_id in (select job_id from perimeter_job where is_active=1)
							group by js.job_or_run_id) max_s
							where ass.estimator_id = @estimator_id AND ass.job_id = max_s.job_id AND convert(int,js.is_run) = 1 AND js.job_or_run_id = ass.job_id 
							and js.session_num =max_s.max_session and js.status ='cancelled') 
							where target = "Association"

	select target, total, validated, error, active, inactive from #resultdesc
	
	truncate table #resultdesc
	drop table #resultdesc
end
go

grant execute on sel_quant_desc_recap to cdv_users
go

grant execute on sel_quant_desc_recap to sel_users
go

exec sp_procxmode "sel_quant_desc_recap",Unchained
go

if exists (select 1 from sysobjects where type='P' and name='sel_quant_desc_domain')
begin
 print "Suppression sel_quant_desc_domain"
 drop procedure sel_quant_desc_domain
end
go

create procedure sel_quant_desc_domain
(
	@estimator_id	int
)
as 
begin

create table #resultdomain (
		domain_type				varchar(255) null,
		total					int null,
		validated				int null,
		error					int null
	)
create table #firstResultdomain (
		domain_type				varchar(255) null,
		validated				int null
)

create table #secondResultdomain (
		domain_type				varchar(255) null,
		error				int null
)

insert into #resultdomain
select domain_type_name , count(1), null, null 
from (select
distinct j.job_id job_id, j.domain_id domain_id, dm.domain_type_id domain_type_id, dt.domain_type_name domain_type_name
from 
job j ,
perimeter_job pj, 
domain dm , 
domain_type dt
where
j.estimator_id= @estimator_id AND 
pj.is_active =1  AND
pj.job_id = j.job_id AND
dm.domain_id = j.domain_id AND
dt.domain_type_id = dm.domain_type_id)  domain_tab
group by 
domain_type_name


insert into #firstResultdomain
select domain_type_name , count(1) 
from  
(select
distinct j.job_id job_id, j.domain_id domain_id, dm.domain_type_id domain_type_id, dt.domain_type_name domain_type_name 
from 
job j ,
perimeter_job pj, 
domain dm , 
domain_type dt, 
last_run lr 
where
j.estimator_id= @estimator_id AND 
pj.is_active =1  AND
pj.job_id = j.job_id AND
dm.domain_id = j.domain_id AND
dt.domain_type_id = dm.domain_type_id and
lr.job_id = j.job_id and 
lr.last_valid_run_id <> null) domain_tab_valid
group by 
domain_type_name


/* Adaptive Server has expanded all '*' elements in the following statement */ insert into #secondResultdomain
select domain_type_name , count(1) from
(select 
domain_tab.job_id, domain_tab.domain_id, domain_tab.domain_type_id, domain_tab.domain_type_name        , max_s.max_session , js.status
from 
job_status js,
(select
    js.job_or_run_id job_id,
    max(js.session_num) max_session 
    from 
    job_status js 
    where 
    convert(int, js.is_run) = 1 and 
    job_or_run_id in (select job_id from perimeter_job where is_active=1)
    group by 
    js.job_or_run_id) max_s,
    
(select
  distinct j.job_id job_id, j.domain_id domain_id, dm.domain_type_id domain_type_id, dt.domain_type_name domain_type_name 
from 
job j ,
perimeter_job pj, 
domain dm , 
domain_type dt
where
j.estimator_id= @estimator_id AND 
pj.is_active =1  AND
pj.job_id = j.job_id AND
dm.domain_id = j.domain_id AND
dt.domain_type_id = dm.domain_type_id) domain_tab
where 
domain_tab.job_id = max_s.job_id and 
convert(int, js.is_run)=1 and 
js.job_or_run_id =max_s.job_id and 
js.session_num = max_s.max_session ) domain_max_session
where status = 'cancelled'
group by 
domain_type_name

update #resultdomain
set validated = (select validated from #firstResultdomain where domain_type = #resultdomain.domain_type)

update #resultdomain
set error = (select error from #secondResultdomain where domain_type = #resultdomain.domain_type)

/* Adaptive Server has expanded all '*' elements in the following statement */ select #resultdomain.domain_type, #resultdomain.total, #resultdomain.validated, #resultdomain.error from #resultdomain

	truncate table #resultdomain
	drop table #resultdomain
	
	truncate table #firstResultdomain
	drop table #firstResultdomain

	truncate table #secondResultdomain
	drop table #secondResultdomain
	
end
go

grant execute on sel_quant_desc_domain to cdv_users
go

grant execute on sel_quant_desc_domain to sel_users
go

exec sp_procxmode "sel_quant_desc_domain",Unchained
go


if exists (select 1 from sysobjects where type='P' and name='sel_quant_estimators')
begin
 print "Suppression sel_quant_estimators"
 drop procedure sel_quant_estimators
end
go
create procedure sel_quant_estimators
as
begin
	select	estimator_id,
			estimator_name,
			estimator.comment,
			matlab_run,
			matlab_check,
			output_type.output_type_name
			
	from	estimator,
			output_type
			
	where	estimator.output_type_id = output_type.output_type_id

	order by	estimator.estimator_name
end
go

grant execute on sel_quant_estimators to cdv_users
go

grant execute on sel_quant_estimators to sel_users
go

exec sp_procxmode "sel_quant_estimators",Unchained
go


if exists (select 1 from sysobjects where type='P' and name='sel_quant_parameters')
begin
 print "Suppression sel_quant_parameters"
 drop procedure sel_quant_parameters
end
go

create procedure sel_quant_parameters
(
	@run_id		int
)
as
begin
	select	param_desc.parameter_id,
			param_desc.x_value,
			param_value.value,
			param_desc.parameter_name,
			param_value.run_id,
			param_desc.estimator_id,
			param_desc.comment
			
	from	param_desc,
			param_value
			
	where	param_desc.parameter_id = param_value.parameter_id
	and		param_value.run_id = @run_id
	and 	param_desc.x_value >= 0
	
	order by	param_desc.x_value ASC
end
go

grant execute on sel_quant_parameters to cdv_users
go

grant execute on sel_quant_parameters to sel_users
go

exec sp_procxmode "sel_quant_parameters",Unchained
go



if exists (select 1 from sysobjects where type='P' and name='sel_quant_removed_dates')
begin
 print "Suppression sel_quant_removed_dates"
 drop procedure sel_quant_removed_dates
end
go

create procedure sel_quant_removed_dates (
	@run_id		int
)
as
	declare @domain_id		int
	declare @estimator_id	int
begin
	select	@domain_id =	domain_id,
			@estimator_id =	estimator_id
	from	estimator_runs
	where	run_id = @run_id

	/* Adaptive Server has expanded all '*' elements in the following statement */ select	date_remove.date_remove_id, date_remove.domain_id, date_remove.excluded_date, date_remove.run_id, date_remove.estimator_id, date_remove.stamp_date, date_remove.login
	from	date_remove
	where	@domain_id =	domain_id
	and		@estimator_id =	estimator_id
	order by	excluded_date
end
go

grant execute on sel_quant_removed_dates to cdv_users
go

grant execute on sel_quant_removed_dates to sel_users
go

exec sp_procxmode "sel_quant_removed_dates",Unchained
go

if exists (select 1 from sysobjects where type='P' and name='sel_quant_run_is_valid')
begin
 print "Suppression sel_quant_run_is_valid"
 drop procedure sel_quant_run_is_valid
end
go

create procedure sel_quant_run_is_valid
(
	@run_id	int
)
as 
begin
	select	is_valid
	from	estimator_runs
	where	run_id = @run_id
end
go

grant execute on sel_quant_run_is_valid to cdv_users
go

grant execute on sel_quant_run_is_valid to sel_users
go

exec sp_procxmode "sel_quant_run_is_valid",Unchained
go



if exists (select 1 from sysobjects where type='P' and name='sel_quant_runs')
begin
 print "Suppression sel_quant_runs"
 drop procedure sel_quant_runs
end
go

create procedure sel_quant_runs
(
	@estimator_id	int,
	@domain_id		int,
	@valid			int/*,
	@begin_date		varchar(8), YYYYMMDD*/
	/*@end_date		varchar(8) YYYYMMDD*/
)
as
declare @request	varchar(2048)

/*select	@begin_date =	isnull(@begin_date, "10000101"),
		@end_date =		isnull(@end_date, "99991231")*/
begin
	set @request = '
	select
		run.run_id,
		run.estimator_id,
		context.context_num			as context_num,
		context.context_name		as context,			
		job.domain_id,
		domain.domain_name			as domain,			
		run.run_quality,							
		run.stamp_date			as run_date,
		run.is_valid,
		run.comment,
		(case when a.last_run_id = run.run_id then 1 else 0 end) as is_last_run
		
	from
		estimator_runs run,
		context context, 
		job job,
		domain domain,
		last_run a
		 
	where
	
		 job.estimator_id = isnull(@estimator_id,	job.estimator_id) and 
		 job.domain_id = isnull(@domain_id,		job.domain_id) and 
		 domain.domain_id = job.domain_id and
		 run.job_id =  job.job_id and  
		 context.estimator_id = job.estimator_id and 
		 run.context_id =context.context_id and 
		 a.job_id = job.job_id and 
		 a.context_id = run.context_id 
	
		'
	if (@valid != null)
		set @request = @request || 'and run.is_valid = ' || convert( varchar(1), @valid )
	/*set @request = @request || '
		and @begin_date	<=		isnull(domain.begin_date, @begin_date)
		and isnull(domain.end_date, @end_date)		<=	@end_date
		'*/
	
	execute(@request)
end
go

grant execute on sel_quant_runs to cdv_users
go

grant execute on sel_quant_runs to sel_users
go

exec sp_procxmode "sel_quant_runs",Unchained
go


if exists (select 1 from sysobjects where type='P' and name='update_quant_run_comment')
begin
 print "Suppression update_quant_run_comment"
 drop procedure update_quant_run_comment
end
go

create procedure update_quant_run_comment (
	@run_id			int,
	@comment	varchar(255)
)
as

begin
	update
		estimator_runs
	set
		comment = @comment
	where
		run_id = @run_id
		
end
go

grant execute on update_quant_run_comment to cdv_users
go

grant execute on update_quant_run_comment to sel_users
go

exec sp_procxmode "update_quant_run_comment",Unchained
go

if exists (select 1 from sysobjects where type='P' and name='update_quant_run_is_validated')
begin
 print "Suppression update_quant_run_is_validated"
 drop procedure update_quant_run_is_validated
end
go

create procedure update_quant_run_is_validated (
	@run_id			int,
	@is_validated	bit
)
as
declare @estimator_id	int
declare @domain_id		int
declare @context_num	int
declare @job_id			int
declare @stamp_date		date

begin

	create table #run (
			run_id	int,
			context_id int
		)

	select
			@estimator_id	= estimator_id,
			@domain_id		= domain_id,
			@context_num	= context_id,
			@job_id			= job_id,
			@stamp_date		= stamp_date
		from
			estimator_runs
		where
			run_id = @run_id


	insert into #run
		select run_id, context_id from estimator_runs 
		where job_id = @job_id 
		and run_id >  @run_id - 20
		and run_id <  @run_id + 20

	
	if(@is_validated = 1)
	begin
		
		update
			estimator_runs
		set
			is_valid =	0
		where
				estimator_id	= @estimator_id
		and		domain_id		= @domain_id
		
		
		update
			estimator_runs
		set
			is_valid =	1
		where
				estimator_id	= @estimator_id
		and		domain_id		= @domain_id
		and		context_id		= @context_num
		and run_id in (select run_id from #run)
		
		insert into valid_runs 
			select run_id, @job_id, @estimator_id, @stamp_date, context_id, getdate(), 2, ""
		from #run
		
		update last_run set last_valid_run_id = a.run_id from #run a where last_run.job_id = @job_id and last_run.context_id = a.context_id
		
	end
	
	if(@is_validated <> 1)
		begin
			update last_run set last_valid_run_id = null from #run a where last_run.job_id = @job_id and last_run.context_id = a.context_id
		end

	update
		estimator_runs
	set
		is_valid =	@is_validated
	where
		run_id in (select run_id from #run)
end
go

grant execute on update_quant_run_is_validated to cdv_users
go

grant execute on update_quant_run_is_validated to sel_users
go

exec sp_procxmode "update_quant_run_is_validated",Unchained
go



