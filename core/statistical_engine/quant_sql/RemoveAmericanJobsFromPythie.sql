
select * from perimeter_job

update perimeter_job set is_active=0 where job_id in (
	select job_id from quant_homolo..job where domain_id in (
		select domain_id from quant_homolo..domain_security where security_id in (
			select distinct security_id
			from repository..security_market
			where trading_destination_id in (20, 25, 95, 106)
			and security_id in (
				select security_id from quant_homolo..domain_security
			)
		)
	)
)	
