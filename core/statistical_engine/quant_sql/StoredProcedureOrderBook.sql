use temp_works
go

if exists (select 1 from sysobjects where name = 'get_orderbook_depth' and type = 'P')
begin
   print "drop procedure get_orderbook_depth"
   drop procedure get_orderbook_depth
end
go

create procedure get_orderbook_depth
(
  @security_id            int,
  @trading_destination_id int,
  @date                   date,
  @time                   time
)
as
  declare @source_table  varchar(64)
begin
  select @source_table = 'tick_db_' + substring( convert( char(3), datepart(month,@date) + 100 ), 2, 2 ) + "o_" + convert(varchar(8),datepart(year,@date))

  exec(
  "select max(depth)
  from " + @source_table + "..orderbook_update
  where security_id            = @security_id
     and trading_destination_id = @trading_destination_id
     and date                   = @date
     and image                  = 1
     and time                   < @time")
end
go

grant execute on get_orderbook_depth to public
go

exec sp_procxmode "get_orderbook_depth",Unchained
go

if exists (select 1 from sysobjects where name = 'get_orderbook_evt' and type = 'P')
begin
   print "drop procedure get_orderbook_evt"
   drop procedure get_orderbook_evt
end
go


create procedure get_orderbook_evt
(
  @security_id            int,
  @trading_destination_id int,
  @date                   date,
  @time                   time,
  @endtime                time
)
as
  declare @keyframe_time time
  declare @update_time   time
  declare @microseconds  int
  declare @side          bit
  declare @depth         tinyint
  declare @price         float
  declare @size          int
  declare @source_table  varchar(64)
  declare @source_table_d varchar(64)
begin

  select @source_table = 'tick_db_' + substring( convert( char(3), datepart(month,@date) + 100 ), 2, 2 ) + "o_" + convert(varchar(8),datepart(year,@date))
  select @source_table_d = 'tick_db_' + substring( convert( char(3),
 datepart(month,@date) + 100 ), 2, 2 ) + "d_" + convert(varchar(8),datepart(year,@date))

  exec(
   "select @keyframe_time = max(time)
    from " + @source_table + "..orderbook_update
   where security_id            = @security_id
     and trading_destination_id = @trading_destination_id
     and date                   = @date
     and time                   < @time
     and image                  = 1")

 exec(
  "select time,convert(varchar,time,108),microseconds,
                side,
depth,price,null size,null trade_price,null trade_size,0 over_ask,0 cross, 0 auction
  from " + @source_table + "..orderbook_update
   where security_id            = @security_id
     and trading_destination_id = @trading_destination_id
     and date                   = @date
     and time                   = @keyframe_time
     and image                  = 1
  union
  select time,convert(varchar,time,108),microseconds,
         side,depth,price,size,null trade_price,null trade_size,0 over_ask,0 cross, 0 auction
  from " + @source_table + "..orderbook_update
   where security_id            = @security_id
     and trading_destination_id = @trading_destination_id
     and date                   = @date
     and time                   >= @keyframe_time
     and time                   <= @endtime
     and image                  = 0
     and (price != NULL or size != NULL)
  union
  select time,convert(varchar,time,108),microseconds,
  null, null, null, null,price,size,overask,cross,auction
  from " + @source_table_d + "..deal_archive
    where security_id            = @security_id
     and trading_destination_id = @trading_destination_id
     and date                   = @date
     and time                   >= @time
     and time                   <= @endtime
     and cross                  = 0
     and auction                = 0
     and (overbid = 1 or overask = 1)
  order by time,microseconds")

end
go

grant execute on get_orderbook_evt to public
go

exec sp_procxmode "get_orderbook_evt",Unchained
go
