use quant
go


-- ins_int_alarm
if exists (select 1 from sysobjects where name = 'ins_int_alarm' and type = 'P')
begin
	print "drop procedure ins_int_alarm"
	drop procedure ins_int_alarm
end
go

print "create procedure ins_int_alarm"
go

create procedure ins_int_alarm
(
	@int_alarm_def_id           int,
	@int_check_id	            int,
	@name	                    varchar(255),
	@severity	            decimal(5,4)
)
as
begin
	declare @myrowcount			int
	declare @donotmanagetran    int
	declare @myerror            int
	declare @alarm_id			int
  
	select @donotmanagetran = @@trancount
	if (@donotmanagetran = 0)
		begin tran

	insert into int_alarm (
		int_alarm_def_id,
		int_check_id,
		name,
		severity_dc
	) values (
		@int_alarm_def_id,
		@int_check_id,
		@name,
		@severity
	)

	select @alarm_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error
   
	if @myerror != 0 or @myrowcount != 1
	begin
		if (@donotmanagetran = 0)
			rollback tran
		raiserror 100001 'insert into int_alarm [ins_int_alarm] has failed !'
		return 100001
	end

	select @alarm_id 
    
	if (@donotmanagetran = 0)
		commit tran
	return 0
end
go

grant execute					on ins_int_alarm to sel_users
go

grant execute					on ins_int_alarm to cdv_users
go

exec sp_procxmode "ins_int_alarm",Unchained
go


-- ins_int_alarm_def
if exists (select 1 from sysobjects where name = 'ins_int_alarm_def' and type = 'P')
begin
	print "drop procedure ins_int_alarm_def"
	drop procedure ins_int_alarm_def
end
go

print "create procedure ins_int_alarm_def"
go

create procedure ins_int_alarm_def
(
	@check_id                       int,
	@type                           varchar(32),
	@scrutation                     varchar(32),
	@is_active                      int
)
as
begin
	declare @myrowcount             int
	declare @donotmanagetran    	int
	declare @myerror            	int
	declare @alarm_id       		int
	declare @alarm_type_id          int
	declare @alarm_scrutation_id    int
  
	select @donotmanagetran = @@trancount
	if (@donotmanagetran = 0)
		begin tran

    select @alarm_type_id=int_alarm_type_id from ref_int_alarm_type where lower(name)=lower(@type)
    if @alarm_type_id=NULL
    begin
		rollback tran
		raiserror 100001 'insert into int_alarm_def [ins_int_alarm_def] has failed (alarm uses an unknown type) !'
		return 100001
    end

    select @alarm_scrutation_id=int_alarm_scrutation_id from ref_int_alarm_scrutation where lower(name)=lower(@scrutation)
    if @alarm_scrutation_id=NULL
    begin
		rollback tran
		raiserror 100001 'insert into int_alarm_def [ins_int_alarm_def] has failed (alarm uses an unknown scrutation) !'
		return 100001
    end

	insert into int_alarm_def (
		int_check_def_id,
		int_alarm_type_id,
		int_alarm_scrutation_id,
		is_active
	) values (
		@check_id,
		@alarm_type_id,
		@alarm_scrutation_id,
		@is_active
	)

	select @alarm_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error
   
	if @myerror != 0 or @myrowcount != 1
	begin
		if (@donotmanagetran = 0)
			rollback tran
		raiserror 100001 'insert into int_alarm_def [ins_int_alarm_def] has failed !'
		return 100001
	end

	select @alarm_id
    
	if (@donotmanagetran = 0)
		commit tran
	return 0
end
go

grant execute					on ins_int_alarm_def to sel_users
go

grant execute					on ins_int_alarm_def to cdv_users
go

exec sp_procxmode "ins_int_alarm_def",Unchained
go



-- ins_int_check
if exists (select 1 from sysobjects where name = 'ins_int_check' and type = 'P')
begin
	print "drop procedure ins_int_check"
	drop procedure ins_int_check
end
go

print "create procedure ins_int_check"
go

create procedure ins_int_check
(
	@int_check_def_id           int,
	@begin_dt                   varchar(32),
	@end_dt		                varchar(32),
	@severity_dc				decimal(5,4)
)
as
begin
	declare @myrowcount			int
	declare @donotmanagetran    int
	declare @myerror            int
	declare @check_id			int
  
	select @donotmanagetran = @@trancount
	if (@donotmanagetran = 0)
		begin tran

	insert into int_check (
		int_check_def_id,
		begin_dt,
		end_dt,
		severity_dc
	) values (
		@int_check_def_id,
		convert(datetime,@begin_dt),
		convert(datetime,@end_dt),
		@severity_dc
	)

	select @check_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error
   
	if @myerror != 0 or @myrowcount != 1
	begin
		if (@donotmanagetran = 0)
			rollback tran
		raiserror 100001 'insert into int_check [ins_int_check] has failed !'
		return 100001
	end

	select @check_id
    
	if (@donotmanagetran = 0)
		commit tran
	return 0
end
go

grant execute					on ins_int_check to sel_users
go

grant execute					on ins_int_check to cdv_users
go

exec sp_procxmode "ins_int_check",Unchained
go



-- ins_int_check_def
if exists (select 1 from sysobjects where name = 'ins_int_check_def' and type = 'P')
begin
	print "drop procedure ins_int_check_def"
	drop procedure ins_int_check_def
end
go

print "create procedure ins_int_check_def"
go

create procedure ins_int_check_def
(
	@loaded_file_id             int,
	@name                       varchar(32),
	@cron_regexp                varchar(255),
	@comment                    varchar(255),
	@active_from_dt             varchar(32),
	@active_to_dt               varchar(32)
)
as
begin
	declare @myrowcount		int
	declare @donotmanagetran    	int
	declare @myerror            	int
	declare @check_id		int
	declare @mnemonic		varchar(32)
  
	select @donotmanagetran = @@trancount
	if (@donotmanagetran = 0)
		begin tran

    select @mnemonic=mnemonic from int_check_def where name=@name and active_from_dt in (select max (active_from_dt) from int_check_def where name=@name group by name)

    if @mnemonic=NULL
    begin
        select @mnemonic=''
    end

    if @active_to_dt='NULL'
    begin
        select @active_to_dt=NULL
    end

	insert into int_check_def (
		loaded_file_id,
		mnemonic,
		name,
		cron_regexp,
		comment,
        active_from_dt,
        active_to_dt
	) values (
		@loaded_file_id,
		@mnemonic,
		@name,
		@cron_regexp,
		@comment,
        convert(datetime,@active_from_dt),
        convert(datetime,@active_to_dt)
	)

	select @check_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error
   
	if @myerror != 0 or @myrowcount != 1
	begin
		if (@donotmanagetran = 0)
			rollback tran
		raiserror 100001 'insert into int_check_def [ins_int_check_def] has failed !'
		return 100001
	end

	if ( @myerror = 0 or @myrowcount = 1 ) and @mnemonic = ''
    begin
        select @name=name from int_check_def where int_check_def_id=@check_id
        select @mnemonic=max(mnemonic) from int_check_def where mnemonic=@name
        if @mnemonic=NULL
            select @mnemonic=@name
        else
            select @mnemonic=left(@name,20) + convert(varchar,@check_id)
        update int_check_def set mnemonic = @mnemonic where int_check_def_id=@check_id
    end

	select @check_id
    
	if (@donotmanagetran = 0)
		commit tran
	return 0
end
go

grant execute					on ins_int_check_def to sel_users
go

grant execute					on ins_int_check_def to cdv_users
go

exec sp_procxmode "ins_int_check_def",Unchained
go



-- ins_loaded_file
if exists (select 1 from sysobjects where name = 'ins_loaded_file' and type = 'P')
begin
	print "drop procedure ins_loaded_file"
	drop procedure ins_loaded_file
end
go

print "create procedure ins_loaded_file"
go

create procedure ins_loaded_file
(
	@load_dt                    varchar(32),
	@is_load_ok	                int,
	@file_name                  varchar(255),
	@file_modified_dt           varchar(32),
	@file_md5	                varchar(32)
)
as
begin
	declare @myrowcount				int
	declare @donotmanagetran    	int
	declare @myerror            	int
	declare @loaded_file_id			int
  
	select @donotmanagetran = @@trancount
	if (@donotmanagetran = 0)
		begin tran

	insert into loaded_file (
		load_dt,
		is_load_ok,
		file_name,
		file_modified_dt,
        file_md5
	) values (
		convert(datetime,@load_dt),
		@is_load_ok,
		@file_name,
        convert(datetime,@file_modified_dt),
        @file_md5
	)

	select @loaded_file_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error
   
	if @myerror != 0 or @myrowcount != 1
	begin
		if (@donotmanagetran = 0)
			rollback tran
		raiserror 100001 'insert into loaded_file [ins_loaded_file] has failed !'
		return 100001
	end

	select @loaded_file_id
    
	if (@donotmanagetran = 0)
		commit tran
	return 0
end
go

grant execute					on ins_loaded_file to sel_users
go

grant execute					on ins_loaded_file to cdv_users
go

exec sp_procxmode "ins_loaded_file",Unchained
go



-- insorupd_trigger_buffer
if exists (select 1 from sysobjects where name = 'insorupd_trigger_buffer' and type = 'P')
begin
	print "drop procedure insorupd_trigger_buffer"
	drop procedure insorupd_trigger_buffer
end
go

print "create procedure insorupd_trigger_buffer"
go

create procedure insorupd_trigger_buffer
(
	@table_name	            varchar(32),
	@id_column_name	            varchar(32),
	@id_to_add			int
)
as
begin
	declare @myrowcount			int
	declare @donotmanagetran    		int
	declare @myerror            		int
	declare @trigger_buffer_id		int
	declare @trigger_buffer_id_list_id	int
  
	select @donotmanagetran = @@trancount
	if (@donotmanagetran = 0)
		begin tran

	select @trigger_buffer_id = trigger_buffer_id from trigger_buffer where table_name=@table_name

	if @trigger_buffer_id = NULL
	begin
		insert into trigger_buffer (
			table_name,
			id_column_name
		) values (
			@table_name,
			@id_column_name
		)

		select @trigger_buffer_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error

		if @myerror = 0 and @myrowcount = 1
		begin
			insert into trigger_buffer_id_list (datetime, trigger_buffer_id, id_column_id) values (getdate(), @trigger_buffer_id, @id_to_add)
			select @trigger_buffer_id_list_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error
		end

		if @myerror != 0 or @myrowcount != 1
		begin
			if (@donotmanagetran = 0)
				rollback tran
			raiserror 100001 'insert into trigger_buffer [insorupd_trigger_buffer] has failed !'
			return 100001
		end

		if (@donotmanagetran = 0)
			commit tran
		return 0
		end
	else
	begin
		insert into trigger_buffer_id_list (datetime, trigger_buffer_id, id_column_id) values (getdate(), @trigger_buffer_id, @id_to_add)
		select @trigger_buffer_id_list_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error

		if @myerror != 0 or @myrowcount != 1
		begin
			if (@donotmanagetran = 0)
				rollback tran
			raiserror 100001 'update trigger_buffer [insorupd_trigger_buffer] has failed !'
			return 100001
		end

		if (@donotmanagetran = 0)
			commit tran
		return 0
	end

end
go

grant execute					on insorupd_trigger_buffer to sel_users
go

grant execute					on insorupd_trigger_buffer to cdv_users
go

exec sp_procxmode "insorupd_trigger_buffer",Unchained
go



-- int_rollback
if exists (select 1 from sysobjects where name = 'int_rollback' and type = 'P')
begin
	print "drop procedure int_rollback"
	drop procedure int_rollback
end
go

print "create procedure int_rollback"
go

create procedure int_rollback
(
	@loaded_file_id					int
)
as
begin
	declare @myrowcount			int
	declare @donotmanagetran    		int
	declare @myerror            		int
	declare @active_to_min            		datetime
	declare @active_to_max            		datetime
  
	select @donotmanagetran = @@trancount
	if (@donotmanagetran = 0)
		begin tran

-- get min and max dates at which previous checks were deactivated

	select @active_to_min=min(active_from_dt) from int_check_def where loaded_file_id=@loaded_file_id
	select @myrowcount=@@rowcount, @myerror=@@error
	if @myerror != 0 or @myrowcount != 1
	begin
		if (@donotmanagetran = 0)
			rollback tran
		raiserror 100001 'rollback of xml load for integrity checks [int_rollback] has failed ! the provided loaded_file_id yields no results.'
		return 100001
	end
	select @active_to_max=max(active_from_dt) from int_check_def where loaded_file_id=@loaded_file_id

-- before deleting, make sure only one loaded_file is concerned with the range of dates provided

	select loaded_file_id from int_check_def where active_to_dt>=@active_to_min and active_to_dt<=@active_to_max group by loaded_file_id
	select @myrowcount=@@rowcount, @myerror=@@error
	if @myerror != 0 or @myrowcount > 1
	begin
		if (@donotmanagetran = 0)
			rollback tran
		raiserror 100001 '****************************\n SERIOUS ERROR !!\nrollback of xml load for integrity checks [int_rollback] has failed !\nThe checks to be reactivated correspond to more than one loaded_file_id => please rollback manually.\n****************************'
		return 100001
	end

-- delete all checks and related objects (alarms and properties) corresponding to the loaded_file_id
-- if all is successful, update previous checks in order to reactivate them

	delete from obj_property where obj_name='int_alarm_def' and obj_id in (select int_alarm_def_id from int_alarm_def where int_check_def_id in (select int_check_def_id from int_check_def where loaded_file_id=@loaded_file_id))
	select @myerror=@@error
	if @myerror != 0
	begin
		if (@donotmanagetran = 0)
			rollback tran
		raiserror 100001 'rollback of xml load for integrity checks [int_rollback] has failed (on the 1st (deletion) query)!\n'
		return 100001
	end
	else
	begin
		delete from int_alarm_def where int_check_def_id in (select int_check_def_id from int_check_def where loaded_file_id=@loaded_file_id)
		select @myerror=@@error
		if @myerror != 0
		begin
			if (@donotmanagetran = 0)
				rollback tran
			raiserror 100001 'rollback of xml load for integrity checks [int_rollback] has failed (on the 2nd (deletion) query)!\n'
			return 100001
		end
		else
		begin
			delete from obj_property where obj_name='int_check_def' and obj_id in (select int_check_def_id from int_check_def where loaded_file_id=@loaded_file_id)
			select @myerror=@@error
			if @myerror != 0
			begin
				if (@donotmanagetran = 0)
					rollback tran
				raiserror 100001 'rollback of xml load for integrity checks [int_rollback] has failed (on the 3rd (deletion) query)!\n'
				return 100001
			end
			else
			begin
				delete from int_check_def where loaded_file_id=@loaded_file_id
				select @myerror=@@error
				if @myerror != 0
				begin
					if (@donotmanagetran = 0)
						rollback tran
					raiserror 100001 'rollback of xml load for integrity checks [int_rollback] has failed (on the 4th (deletion) query)!\n'
					return 100001
				end
				else
				begin
					delete from obj_property where obj_name='loaded_file' and obj_id=@loaded_file_id
					select @myerror=@@error
					if @myerror != 0
					begin
						if (@donotmanagetran = 0)
							rollback tran
						raiserror 100001 'rollback of xml load for integrity checks [int_rollback] has failed (on the 5th (deletion) query)!\n'
						return 100001
					end
					else
					begin
						update loaded_file set is_load_ok=-2 where loaded_file_id=@loaded_file_id
						select @myerror=@@error
						if @myerror != 0
						begin
							if (@donotmanagetran = 0)
								rollback tran
							raiserror 100001 'rollback of xml load for integrity checks [int_rollback] has failed (on the 6th (update) query)!\n'
							return 100001
						end
						else
						begin
							update int_check_def set active_to_dt=null where active_to_dt>=@active_to_min and active_to_dt<=@active_to_max
							select @myerror=@@error
							if @myerror != 0
							begin
								if (@donotmanagetran = 0)
									rollback tran
								raiserror 100001 'rollback of xml load for integrity checks [int_rollback] has failed (on the 7th (update) query)!\n'
								return 100001
							end
						end
					end
				end
			end
		end
	end

	if (@donotmanagetran = 0)
		commit tran
	return 0

end
go

grant execute					on int_rollback to sel_users
go

grant execute					on int_rollback to cdv_users
go

exec sp_procxmode "int_rollback",Unchained
go
