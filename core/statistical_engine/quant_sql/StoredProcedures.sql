
use quant
go





/******************************PROCEDURES STOCKEES**********************************/



-- ins_estimator
if exists (select 1 from sysobjects where name = 'ins_estimator' and type = 'P')
begin
   print "drop procedure ins_estimator"
   drop procedure ins_estimator
end
go

print "create procedure ins_estimator"
go

create procedure ins_estimator
(
  @estimator_name   varchar(32)  ,
  @comment			varchar(255) ,
  @matlab_run		varchar(255) = null ,
  @matlab_check 	varchar(255) = null ,
  @output_type_id  int = 1
)
as
begin
  declare @myrowcount      int    
  declare @donotmanagetran int    
  declare @myerror         int    
  declare @estimator_id    int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into estimator (
     estimator_name,
     comment       ,
     matlab_run    ,
     matlab_check  ,  
     output_type_id
  ) values (
     @estimator_name,
     @comment		,
     @matlab_run	,
     @matlab_check  ,
     @output_type_id)
    
 select @estimator_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans estimator [ins_estimator] a echouee!"    
    return 100001    
  end 
  
  select @estimator_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_estimator to sel_users
go

grant execute on ins_estimator to cdv_users
go

exec sp_procxmode "ins_estimator",Unchained
go



-- ins_param_desc
if exists (select 1 from sysobjects where name = 'ins_param_desc' and type = 'P')
begin
   print "drop procedure ins_param_desc"
   drop procedure ins_param_desc
end
go

print "create procedure ins_param_desc"
go

create procedure ins_param_desc
(
  @parameter_name   varchar(32)  ,
  @estimator_id		int ,
  @comment		    varchar(255) = "" ,
  @x_value 	        float = null
)
as
begin
  declare @myrowcount      int    
  declare @donotmanagetran int    
  declare @myerror         int    
  declare @parameter_id    int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into param_desc (
     parameter_name,
     estimator_id  ,
     comment       ,
     x_value    
  ) values (
     @parameter_name,
     @estimator_id	,
     @comment   	,
     @x_value       )
    
 select @parameter_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans param_desc [ins_param_desc] a echouee!"    
    return 100001    
  end 
  
  select @parameter_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_param_desc to sel_users
go

grant execute on ins_param_desc to cdv_users
go

exec sp_procxmode "ins_param_desc",Unchained
go





-- ins_domain_sec
if exists (select 1 from sysobjects where name = 'ins_domain_sec' and type = 'P')
begin
   print "drop procedure ins_domain_sec"
   drop procedure ins_domain_sec
end
go

print "create procedure ins_domain_sec"
go

create procedure ins_domain_sec
(
  @domain_id                   int   , 
  @security_id                 int  , 
  @trading_destination_id      int      
)
as
begin
  declare @myrowcount      int    
  declare @donotmanagetran int    
  declare @myerror         int    
  declare @domain_sec_id   int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into domain_security (
     domain_id              ,
     security_id            ,
     trading_destination_id  
  ) values (
     @domain_id              ,
     @security_id    	     ,
     @trading_destination_id  )
    
 select @domain_sec_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans domain_security [ins_domain_sec] a echouee!"    
    return 100001    
  end 
  
  select @domain_sec_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_domain_sec to sel_users
go

grant execute on ins_domain_sec to cdv_users
go

exec sp_procxmode "ins_domain_sec",Unchained
go



-- ins_association
if exists (select 1 from sysobjects where name = 'ins_association' and type = 'P')
begin
   print "drop procedure ins_association"
   drop procedure ins_association
end
go

print "create procedure ins_association"
go
create procedure ins_association
(
  @context_id      int  ,
  @rank			    int = null,
  @security_id		    int = null,
  @trading_destination_id 	        int = null,
  @estimator_id     int ,
  @job_id        int,
  @varargin   varchar(16) = null
)
as
begin
  declare @myrowcount      int    
  declare @donotmanagetran int    
  declare @myerror         int
  declare @association_id   int     
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into association (
     context_id   ,
     rank          ,
     security_id        ,
     trading_destination_id         ,  
     estimator_id  ,
     job_id     ,
     varargin
  ) values (
     @context_id   ,
     @rank          ,
     @security_id        ,
     @trading_destination_id         ,  
     @estimator_id  ,
     @job_id     ,
     @varargin)
    
 select @association_id=@@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans association [ins_association] a echouee!"    
    return 100001    
  end 
  
  select @association_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_association to sel_users
go

grant execute on ins_association to cdv_users
go

exec sp_procxmode "ins_association",Unchained
go





-- ins_job
if exists (select 1 from sysobjects where name = 'ins_job' and type = 'P')
begin
   print "drop procedure ins_job"
   drop procedure ins_job
end
go

print "create procedure ins_job"
go

create procedure ins_job
(
  @estimator_id              int         ,
  @domain_id            	 int         ,
  @comment              	 varchar(255),
  @run_frequency_id     	 int         ,
  @run_window_type_id      	 int         ,
  @run_window_width     	 int         ,
  @run_frequency_date   	 date        ,
  @check_frequency_id     	 int         ,
  @check_window_type_id      int         ,
  @check_window_width     	 int         ,
  @check_frequency_date   	 date        ,
  @varargin                  varchar(1025) 
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  declare @job_id   int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into job (
    estimator_id         ,
    domain_id            ,
    comment              ,
    run_frequency_id     ,
    run_window_type_id   ,
    run_window_width     ,
    run_frequency_date   ,
    check_frequency_id   ,
    check_window_type_id ,
    check_window_width   ,
    check_frequency_date,
    varargin 
  ) values (
    @estimator_id         ,
    @domain_id            ,
    @comment              ,
    @run_frequency_id     ,
    @run_window_type_id   ,
    @run_window_width     ,
    @run_frequency_date   ,
    @check_frequency_id   ,
    @check_window_type_id ,
    @check_window_width   ,
    @check_frequency_date ,
    @varargin
    )
    
 select @job_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans job [ins_job] a echouee!"    
    return 100001    
  end 
  
  -- verification
  if ((@run_frequency_id = 3 or @run_frequency_id = 4) and @run_frequency_date = null)
  begin
	if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100002 "l'insertion dans job [ins_job] a echouee! : run_frequency_date obligatoire"    
    return 100002 
  end
  
  if ((@check_frequency_id = 3 or @check_frequency_id = 4) and @check_frequency_date = null)
  begin
	if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100003 "l'insertion dans job [ins_job] a echouee! : check_frequency_date obligatoire"    
    return 100003 
  end  
  
  select @job_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_job to sel_users
go

grant execute on ins_job to cdv_users
go

exec sp_procxmode "ins_job",Unchained
go





-- ins_frequency
if exists (select 1 from sysobjects where name = 'ins_frequency' and type = 'P')
begin
   print "drop procedure ins_frequency"
   drop procedure ins_frequency
end
go

print "create procedure ins_frequency"
go

create procedure ins_frequency
(
  @frequency_name            varchar(32) ,
  @comment              	 varchar(255)   
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  declare @frequency_id          int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into frequency (
    frequency_name       ,
    comment              
  ) values (
    @frequency_name       ,
    @comment              )
    
 select @frequency_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans frequency [ins_frequency] a echouee!"    
    return 100001    
  end 
  
  select @frequency_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_frequency to sel_users
go

grant execute on ins_frequency to cdv_users
go

exec sp_procxmode "ins_frequency",Unchained
go





-- ins_context
if exists (select 1 from sysobjects where name = 'ins_context' and type = 'P')
begin
   print "drop procedure ins_context"
   drop procedure ins_context
end
go

print "create procedure ins_context"
go

create procedure ins_context
(
  @context_name            varchar(32) ,
  @estimator_id            int , 
  @context_num             int ,
  @comment                 varchar(255)   
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  declare @context_id          int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into context (
    context_name       ,
    estimator_id ,
    context_num , 
    comment              
  ) values (
    @context_name       ,
    @estimator_id   ,
    @context_num   , 
    @comment              )
    
 select @context_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans context [ins_context] a echouee!"    
    return 100001    
  end 
  
  select @context_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_context to sel_users
go

grant execute on ins_context to cdv_users
go

exec sp_procxmode "ins_context",Unchained
go



-- ins_output_type
if exists (select 1 from sysobjects where name = 'ins_output_type' and type = 'P')
begin
   print "drop procedure ins_output_type"
   drop procedure ins_output_type
end
go

print "create procedure ins_output_type"
go

create procedure ins_output_type
(
  @output_type_name            varchar(32) ,
  @comment                 varchar(255)   
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  declare @output_type_id          int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into output_type (
    output_type_name       ,
    comment              
  ) values (
    @output_type_name       , 
    @comment              )
    
 select @output_type_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans output_type [ins_output_type] a echouee!"    
    return 100001    
  end 
  
  select @output_type_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_output_type to sel_users
go

grant execute on ins_output_type to cdv_users
go

exec sp_procxmode "ins_output_type",Unchained
go



-- ins_error
if exists (select 1 from sysobjects where name = 'ins_error' and type = 'P')
begin
   print "drop procedure ins_error"
   drop procedure ins_error
end
go

print "create procedure ins_error"
go

create procedure ins_error
(
  @job_or_run_id            int,
  @gravity_level            int,
  @is_run            int,
  @stamp_date            date,
  @stamp_time            TIME,
  @message                 VARCHAR(4096)
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  declare @error_id          int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into error (
    job_or_run_id,
    gravity_level,
    is_run,
    stamp_date,
    stamp_time,
    message              
  ) values (
    @job_or_run_id       , 
    @gravity_level       , 
    @is_run       , 
    @stamp_date       , 
    @stamp_time       , 
    @message              )
    
 select @error_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans error [ins_error] a echouee!"    
    return 100001    
  end 
  
  select @error_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_error to sel_users
go

grant execute on ins_error to cdv_users
go

exec sp_procxmode "ins_error",Unchained
go




-- ins_window_type
if exists (select 1 from sysobjects where name = 'ins_window_type' and type = 'P')
begin
   print "drop procedure ins_window_type"
   drop procedure ins_window_type
end
go

print "create procedure ins_window_type"
go

create procedure ins_window_type
(
  @window_type_name            varchar(32) ,
  @comment                 varchar(255)   
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  declare @window_type_id          int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into window_type (
    window_type_name       ,
    comment              
  ) values (
    @window_type_name       , 
    @comment              )
    
 select @window_type_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans window_type [ins_window_type] a echouee!"    
    return 100001    
  end 
  
  select @window_type_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_window_type to sel_users
go

grant execute on ins_window_type to cdv_users
go

exec sp_procxmode "ins_window_type",Unchained
go





-- ins_estimator_runs
if exists (select 1 from sysobjects where name = 'ins_estimator_runs' and type = 'P')
begin
   print "drop procedure ins_estimator_runs"
   drop procedure ins_estimator_runs
end
go

print "create procedure ins_estimator_runs"
go

create procedure ins_estimator_runs
(
  @job_id                int   ,
  @estimator_id          int   ,
  @domain_id             int   ,  
  @run_quality           float ,
  @context_id            int   =  null,  
  @window_end_date       date  =  null,
  @last_run_date         date  =  null,
  @stamp_date         date  = null,
  @comment               varchar = null,
  @validation_type       int ,
  @devalidation_type     int ,
  @session_num           int ,
  @is_valid              bit   =  0
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  declare @run_id                int 
  declare @count                 int   
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into estimator_runs (
    job_id          ,
    estimator_id    ,
    stamp_date   ,
    domain_id       ,
    run_quality     ,
    context_id      ,
    window_end_date,
    comment,
    is_valid,
    validation_type  ,
    devalidation_type  ,
    session_num
  ) values (
    @job_id         ,
    @estimator_id   ,
    @stamp_date     ,
    @domain_id      ,
    @run_quality    ,
    @context_id     ,
    isnull(@window_end_date,getdate()), 
    @comment,
    @is_valid,
    @validation_type,
    @devalidation_type,
    @session_num
)
  
    
 select @run_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans estimator_runs [ins_estimator_runs] a echouee!"    
    return 100001    
  end 
  
  update job set last_run = @last_run_date where job_id = @job_id
  
  select @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100002 "l'insertion dans estimator_runs [ins_estimator_runs] a echouee!"    
    return 100002    
  end 
  
  
  select @count = count(1) from last_run where job_id=@job_id and context_id=@context_id
  if(@count = 0)
  begin
		insert into last_run 
		(
		   job_id,
		   context_id,
		   last_run_date,
		   last_run_id
		) values (
		   @job_id,
		   @context_id,
		   @last_run_date,
		   @run_id
		)
		select @myrowcount=@@rowcount, @myerror=@@error    
   
	  if @myerror != 0 or @myrowcount != 1    
	  begin    
		if (@donotmanagetran = 0)    
			rollback tran    
		raiserror 100003 "l'insertion dans last_run [ins_estimator_runs] a echouee!"    
		return 100003    
	  end 
  end 
  else if(@count = 1)
  begin
		update last_run set last_run_id=@run_id,last_check_date=null, 
		last_run_date=isnull(@last_run_date,getdate()) where job_id=@job_id and context_id=@context_id
		if @myerror != 0 or @myrowcount != 1    
		  begin    
			if (@donotmanagetran = 0)    
				rollback tran    
			raiserror 100004 "l'update de last_run [ins_estimator_runs] a echouee!"    
			return 100004    
		  end 
  end 
  else
  begin
	if (@donotmanagetran = 0)    
		rollback tran    
	raiserror 100005 "l'update de last_run [ins_estimator_runs] a echouee!"    
	return 100005    
  end 

    
  select @run_id
  
  if (@donotmanagetran = 0)
     commit tran

  return 0
end
go

grant execute on ins_estimator_runs to sel_users
go

grant execute on ins_estimator_runs to cdv_users
go

exec sp_procxmode "ins_estimator_runs",Unchained
go


-- ins_check_quality
if exists (select 1 from sysobjects where name = 'ins_check_quality' and type = 'P')
begin
   print "drop procedure ins_check_quality"
   drop procedure ins_check_quality
end
go

print "create procedure ins_check_quality"
go
create procedure ins_check_quality
(
  @job_id     int,
  @run_id     int,
  @value      float, 
  @stamp_date  date,
  @last_check_date         date  =  null,
  @session_num           int = null , 
  @comment  varchar = null

)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int   
  declare @check_quality_id      int  
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran   
  
  insert into check_quality (
     run_id     ,
     value      ,
     stamp_date ,
     comment ,
     session_num
     ) values (
     @run_id   ,
     @value    ,
     @stamp_date ,
     @comment ,
     @session_num )
     
   select @check_quality_id = @@identity , @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans check_quality [ins_check_quality] a echoue!"    
    return 100001    
  end 
  
  update last_run set r.last_check_date = isnull(@last_check_date,getdate()) from last_run r, estimator_runs er
  where r.job_id = @job_id and r.context_id = er.context_id and r.job_id = er.job_id and er.run_id = @run_id
  
  select @myrowcount=@@rowcount, @myerror=@@error 
  
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100002 "l'insertion dans check_quality [ins_check_quality] a echouee : !"    
    return 100002    
  end 
  
  select @check_quality_id
  
  if (@donotmanagetran = 0)
     commit tran
     
 return 0
end
go

grant execute on ins_check_quality to sel_users
go

grant execute on ins_check_quality to cdv_users
go

exec sp_procxmode "ins_check_quality",Unchained
go





-- ins_job_status
if exists (select 1 from sysobjects where name = 'ins_job_status' and type = 'P')
begin
   print "drop procedure ins_job_status"
   drop procedure ins_job_status
end
go

print "create procedure ins_job_status"
go

create procedure ins_job_status
(
  @job_or_run_id            	int,
  @status               varchar(255),     
  @is_run               bit,   
  @beg_date             date,   
  @beg_time             time,   
  @end_date             date,   
  @end_time             time,
  @job_name				VARCHAR(32),
  @session_num				int
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  declare @job_status_id          int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into job_status (
    job_or_run_id,
	status,     
	is_run,   
	beg_date,   
	beg_time,   
	end_date,   
	end_time,
	job_name,
	session_num
  ) values (
    @job_or_run_id,
	@status,   
	@is_run,   
	@beg_date,   
	@beg_time,   
	@end_date,   
	@end_time,
	@job_name,
	@session_num)
    
 select @job_status_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans job_status [ins_job_status] a echouee!"    
    return 100001    
  end 
  
  select @job_status_id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_job_status to sel_users
go

grant execute on ins_job_status to cdv_users
go

exec sp_procxmode "ins_job_status",Unchained
go



--ins_session

if exists (select 1 from sysobjects where name = 'ins_session' and type = 'P')
begin
   print "drop procedure ins_session"
   drop procedure ins_session
end
go

print "create procedure ins_session"
go

create procedure ins_session
(
  @session_num            	int, 
  @beg_date             date,   
  @beg_time             time,   
  @end_date             date,   
  @end_time             time
)
as
begin
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  declare @id          int    
  
  select @donotmanagetran = @@trancount    
  if (@donotmanagetran = 0)    
      begin tran    
      
  insert into session (
    session_num,
    beg_date,
    beg_time,
    end_date,
    end_time
  ) values (
  @session_num,
  @beg_date,         
  @beg_time,               
  @end_date,             
  @end_time)
    
 select @id = @@identity, @myrowcount=@@rowcount, @myerror=@@error    
   
  if @myerror != 0 or @myrowcount != 1    
  begin    
    if (@donotmanagetran = 0)    
        rollback tran    
    raiserror 100001 "l'insertion dans session [ins_session] a echouee!"    
    return 100001    
  end 
  
  select @id
  
  if (@donotmanagetran = 0)
     commit tran
  return 0
end
go

grant execute on ins_session to sel_users
go

grant execute on ins_session to cdv_users
go

exec sp_procxmode "ins_session",Unchained
go



-- get_estimator_list
IF EXISTS (SELECT 1 FROM sysobjects WHERE name = 'get_estimator_list' AND type = 'P')
BEGIN
   PRINT "drop procedure get_estimator_list"
   DROP PROCEDURE get_estimator_list
END
GO
PRINT "create procedure get_estimator_list"
GO
CREATE PROCEDURE get_estimator_list
AS
BEGIN
	SELECT
		e.estimator_name
	FROM
		estimator e
	ORDER BY
		e.estimator_name  
END
GO

GRANT EXECUTE ON get_estimator_list TO sel_users
GO

GRANT EXECUTE ON get_estimator_list TO cdv_users
GO

EXEC sp_procxmode "get_estimator_list",Unchained
GO



-- get_context_list
IF EXISTS (SELECT 1 FROM sysobjects WHERE name = 'get_context_list' AND type = 'P')
BEGIN
   PRINT "drop procedure get_context_list"
   DROP PROCEDURE get_context_list
END
GO
PRINT "create procedure get_context_list"
GO
CREATE PROCEDURE get_context_list
AS
BEGIN
	SELECT
		c.context_name,
		e.estimator_name
	FROM
		context c,
		estimator e
	WHERE
		c.estimator_id=e.estimator_id
	ORDER BY
		e.estimator_name,
		c.context_name
END
GO

GRANT EXECUTE ON get_context_list TO sel_users
GO

GRANT EXECUTE ON get_context_list TO cdv_users
GO

EXEC sp_procxmode "get_context_list",Unchained
GO


-- get_domain_list
IF EXISTS (SELECT 1 FROM sysobjects WHERE name = 'get_domain_list' AND type = 'P')
BEGIN
   PRINT "drop procedure get_domain_list"
   DROP PROCEDURE get_domain_list
END
GO
PRINT "create procedure get_domain_list"
GO
CREATE PROCEDURE get_domain_list
(
@estimator_name varchar(32)
)
AS
BEGIN
	SELECT
		ds.security_id,
		ds.trading_destination_id
	FROM
		job j,
		domain_security ds,
		estimator e
	WHERE
		ds.domain_id=j.domain_id
		AND j.estimator_id=e.estimator_id
		AND e.estimator_name LIKE @estimator_name
	ORDER BY
		ds.security_id
END
GO

GRANT EXECUTE ON get_domain_list TO sel_users
GO

GRANT EXECUTE ON get_domain_list TO cdv_users
GO

EXEC sp_procxmode "get_domain_list",Unchained
GO



-- get_param
IF EXISTS (SELECT 1 FROM sysobjects WHERE name = 'get_param' AND type = 'P')
BEGIN
   PRINT "drop procedure get_param"
   DROP PROCEDURE get_param
END
GO
PRINT "create procedure get_param"
GO
CREATE PROCEDURE get_param
(
@estimator_name varchar(32),
@context_name varchar(32),
@security_id INT,
@trading_destination_id INT,
@is_valid INT, 
@varargin VARCHAR(16)=NULL
)
AS
BEGIN
	SELECT
	    er.is_valid, 
	    er.stamp_date,
	    a.rank, 
		p.x_value,
		p.parameter_name,
		pv.value
	FROM
		param_value pv,
		param_desc p,
		estimator_runs er,
		association a,
		estimator e,
		context c
		--last_run lr
	WHERE
		-- job id dans la table des associations
	    a.security_id=@security_id
		AND (a.trading_destination_id=@trading_destination_id OR @trading_destination_id=NULL)
		AND (a.varargin=@varargin OR @varargin=NULL )
		AND upper(e.estimator_name) LIKE upper(@estimator_name)
		-- 
		AND er.context_id=c.context_id
		AND upper(c.context_name) LIKE upper(@context_name)
		AND e.estimator_id=c.estimator_id
		AND er.job_id=a.job_id
		-- AND lr.context_id=@context_id
		-- AND er.run_id=lr.last_run_id
		-- AND a.job_id=lr.job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		
	ORDER BY
		a.rank, er.run_id, p.x_value
END
GO

GRANT EXECUTE ON get_param TO sel_users
GO

GRANT EXECUTE ON get_param TO cdv_users
GO

EXEC sp_procxmode "get_param",Unchained
GO

--get_associated_job
IF EXISTS (SELECT 1 FROM sysobjects WHERE name = 'get_associated_job' AND type = 'P')
BEGIN
   PRINT "drop procedure get_associated_job"
   DROP PROCEDURE get_associated_job
END
GO
PRINT "create procedure get_associated_job"
GO
CREATE PROCEDURE get_associated_job
(
@estimator_name varchar(32),
@security_id INT,
@trading_destination_id INT,
@is_valid INT, 
@varargin VARCHAR(16)=NULL
)
AS
BEGIN
	SELECT
		a.rank,
	    a.job_id,
	    er.run_id,
	    a.security_id,
	    a.trading_destination_id,
	    a.varargin,
	    c.context_num,
	    c.context_name,
	    lr.last_run_id,
	    lr.last_valid_run_id,
	    convert(varchar(12),lr.last_run_date,111) "date:yyyy/mm/dd"
	FROM
		association a,
		estimator e,
		estimator_runs er,
		context c,
		last_run lr
	WHERE
		-- SECURITY, TD, VARARGIN, IS_VALID
	    a.security_id=@security_id
		AND (a.trading_destination_id=@trading_destination_id OR @trading_destination_id=NULL)
		AND (a.varargin=@varargin OR @varargin=NULL )
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		-- ESTIMATOR
		AND upper(e.estimator_name) LIKE upper(@estimator_name)
		AND e.estimator_id = a.estimator_id
		AND e.estimator_id = er.estimator_id
		-- JOB
		AND er.job_id = a.job_id
		AND lr.job_id = a.job_id
		-- RUNS
		AND lr.last_run_id = er.run_id
		AND lr.context_id = c.context_id
		-- CONTEXT
		AND e.estimator_id=c.estimator_id
		AND a.context_id =c.context_id
		
	ORDER BY
		a.rank, er.run_id
END
GO

GRANT EXECUTE ON get_associated_job TO sel_users
GO

GRANT EXECUTE ON get_associated_job TO cdv_users
GO

EXEC sp_procxmode "get_associated_job",Unchained
GO


--upd_validation

IF EXISTS (SELECT 1 FROM sysobjects WHERE name = 'upd_validation' AND type = 'P')
BEGIN
   PRINT "drop procedure upd_validation"
   DROP PROCEDURE upd_validation
END
GO

PRINT "create procedure upd_validation"
GO

CREATE PROCEDURE upd_validation (
	@job_id			int  ,
	@to_validated	bit  , 
	@session        int = null , 
	@valid_devalid_type int = 1
	
)
AS
BEGIN
	
  declare @myrowcount            int    
  declare @donotmanagetran       int    
  declare @myerror               int    
  
  select @donotmanagetran = @@trancount    
  
	--récuperation des run déjà valides
	create table #run (
	job_id	int,
	run_id int
			)
		
	insert into #run
	select job_id ,run_id 
	from estimator_runs 
	where job_id = @job_id	
	and is_valid = 1
	
	create table #last_run (
	job_id	int,
	last_run_id int , 
	context_id int
	)
	
	if(@to_validated = 1)
	begin
		 if (@donotmanagetran = 0)    
		  begin tran 
			--Dévalidation nécessaire pour éviter de doublons de validation
			UPDATE estimator_runs 
			SET is_valid = 0, devalidation_type = @valid_devalid_type , validation_type =null 
			WHERE run_id in (select run_id from #run) 
			
			 select @myerror=@@error  
			 if @myerror != 0
			 begin    
				if (@donotmanagetran = 0)    
				rollback tran    
				raiserror 100001 "l'update dans estimator_runs [upd_validation] a echouee!"    
				return 100001    
			  end 
			
			--Recuperation des run à valider pour le job demandé
			--si la session n'est pas spécifiée on valide les last_run de la derniere session 
			if isnull(@session , -1) < 0
			begin
				insert into #last_run
				SELECT  job_id, last_run_id , context_id
				FROM last_run 
				WHERE job_id in ( @job_id ) AND 
				last_run_date in (select max(last_run_date) from last_run where job_id = @job_id)
			end
			else
			begin
				insert into #last_run
				SELECT  job_id, run_id , context_id
				FROM estimator_runs 
				WHERE 
				job_id in (@job_id) AND
				session_num = @session
			end
	
			--VALIDATION
			UPDATE estimator_runs 
			SET is_valid = 1,devalidation_type = null , validation_type =@valid_devalid_type
			WHERE run_id in (select last_run_id from #last_run)
			
			select @myerror=@@error  
			 if @myerror != 0
			 begin    
				if (@donotmanagetran = 0)    
				rollback tran    
				raiserror 100002 "l'update dans estimator_runs [upd_validation] a echouee!"    
				return 100002    
			  end 
			  
			if (@donotmanagetran = 0)
			 commit tran
    end
    
    if(@to_validated = 0)
	begin
		
		UPDATE estimator_runs 
        SET is_valid = 0, devalidation_type = @valid_devalid_type , validation_type =null 
        WHERE run_id in (select run_id from #run) 
        
        
        select @myerror=@@error  
		if @myerror != 0
			begin    
				if (@donotmanagetran = 0)    
				rollback tran    
				raiserror 100003 "l'update dans estimator_runs [upd_validation] a echouee!"    
				return 100003    
			 end 
		
    end
    
  
end

GRANT EXECUTE ON get_context_list TO sel_users
GO

GRANT EXECUTE ON get_context_list TO cdv_users
GO

EXEC sp_procxmode "upd_validation",Unchained
GO