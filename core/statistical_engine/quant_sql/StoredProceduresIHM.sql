

use quant
go



/* DELETE */
if exists (select 1 from sysobjects where type='P' and name='del_quant_removed_date')
begin
 print "Suppression del_quant_removed_date"
 drop procedure del_quant_removed_date
end
go
create procedure del_quant_removed_date (
	@id	int
)
as
begin
	delete
		from	date_remove
		where	date_remove_id = @id
end
go

grant execute on del_quant_removed_date to cdv_users
go

grant execute on del_quant_removed_date to sel_users
go

exec sp_procxmode "del_quant_removed_date",Unchained
go



/* INSERT */
if exists (select 1 from sysobjects where type='P' and name='insert_quant_removed_dates')
begin
 print "Suppression insert_quant_removed_dates"
 drop procedure insert_quant_removed_dates
end
go
create procedure insert_quant_removed_dates (
	@excluded_date	date,
	@run_id			int,
	@login			varchar(32)
)
as
	declare @domain_id		int
	declare @estimator_id	int
begin
	select	@domain_id = domain_id,
			@estimator_id = estimator_id
		from		estimator_runs
		where		run_id = @run_id
	
	insert
		into date_remove (
			excluded_date,
			login,
			domain_id,
			estimator_id,
			stamp_date
		)
		values (
			@excluded_date,
			@login,
			@domain_id,
			@estimator_id,
			current_date()
		)
end
go

grant execute on insert_quant_removed_dates to cdv_users
go

grant execute on insert_quant_removed_dates to sel_users
go

exec sp_procxmode "insert_quant_removed_dates",Unchained
go



/* SELECT */
if exists (select 1 from sysobjects where type='P' and name='sel_quant_check_quality')
begin
 print "Suppression sel_quant_check_quality"
 drop procedure sel_quant_check_quality
end
go

create procedure sel_quant_check_quality
(
	@run_id		int
)
as
begin
	select	*
	
	from	check_quality
	
	where	run_id = @run_id
	
	order by stamp_date
	
end
go

grant execute on sel_quant_check_quality to cdv_users
go

grant execute on sel_quant_check_quality to sel_users
go

exec sp_procxmode "sel_quant_check_quality",Unchained
go


if exists (select 1 from sysobjects where type='P' and name='sel_quant_domains')
begin
 print "Suppression sel_quant_domains"
 drop procedure sel_quant_domains
end
go

create procedure sel_quant_domains 
as
begin
	select *
		from		domain
		order by	domain_name
end
go

grant execute on sel_quant_domains to cdv_users
go

grant execute on sel_quant_domains to sel_users
go

exec sp_procxmode "sel_quant_domains",Unchained
go


if exists (select 1 from sysobjects where type='P' and name='sel_quant_estimator')
begin
 print "Suppression sel_quant_estimator"
 drop procedure sel_quant_estimator
end
go

create procedure sel_quant_estimator
(
	@estimator_id	int
)
as 
begin
	/* possibilitÚ d'appeler la procÚdure sel_quant_estimaotrs ? */
	select	estimator_id,
			estimator_name,
			estimator.comment,
			matlab_run,
			matlab_check,
			output_type.output_type_name
			
	from	estimator,
			output_type
			
	where	estimator.output_type_id = output_type.output_type_id
	and		estimator_id = @estimator_id
	
end
go

grant execute on sel_quant_estimator to cdv_users
go

grant execute on sel_quant_estimator to sel_users
go

exec sp_procxmode "sel_quant_estimator",Unchained
go


if exists (select 1 from sysobjects where type='P' and name='sel_quant_estimators')
begin
 print "Suppression sel_quant_estimators"
 drop procedure sel_quant_estimators
end
go

create procedure sel_quant_estimators
as
begin
	select	estimator_id,
			estimator_name,
			estimator.comment,
			matlab_run,
			matlab_check,
			output_type.output_type_name
			
	from	estimator,
			output_type
			
	where	estimator.output_type_id = output_type.output_type_id

	order by	estimator.estimator_name
end
go

grant execute on sel_quant_estimators to cdv_users
go

grant execute on sel_quant_estimators to sel_users
go

exec sp_procxmode "sel_quant_estimators",Unchained
go

if exists (select 1 from sysobjects where type='P' and name='sel_quant_parameters')
begin
 print "Suppression sel_quant_parameters"
 drop procedure sel_quant_parameters
end
go

create procedure sel_quant_parameters
(
	@run_id		int
)
as
begin
	select	param_desc.parameter_id,
			param_desc.x_value,
			param_value.value,
			param_desc.parameter_name,
			param_value.run_id,
			param_desc.estimator_id,
			param_desc.comment
			
	from	param_desc,
			param_value
			
	where	param_desc.parameter_id = param_value.parameter_id
	and		param_value.run_id = @run_id
	and 	param_desc.x_value >= 0
	
	order by	param_desc.x_value ASC
end
go

grant execute on sel_quant_parameters to cdv_users
go

grant execute on sel_quant_parameters to sel_users
go

exec sp_procxmode "sel_quant_parameters",Unchained
go

if exists (select 1 from sysobjects where type='P' and name='sel_quant_removed_dates')
begin
 print "Suppression sel_quant_removed_dates"
 drop procedure sel_quant_removed_dates
end
go

create procedure sel_quant_removed_dates (
	@run_id		int
)
as
	declare @domain_id		int
	declare @estimator_id	int
begin
	select	@domain_id =	domain_id,
			@estimator_id =	estimator_id
	from	estimator_runs
	where	run_id = @run_id

	select	*
	from	date_remove
	where	@domain_id =	domain_id
	and		@estimator_id =	estimator_id
	order by	excluded_date
end
go

grant execute on sel_quant_removed_dates to cdv_users
go

grant execute on sel_quant_removed_dates to sel_users
go

exec sp_procxmode "sel_quant_removed_dates",Unchained
go


if exists (select 1 from sysobjects where type='P' and name='sel_quant_run_is_valid')
begin
 print "Suppression sel_quant_run_is_valid"
 drop procedure sel_quant_run_is_valid
end
go

create procedure sel_quant_run_is_valid
(
	@run_id	int
)
as 
begin
	select	is_valid
	from	estimator_runs
	where	run_id = @run_id
end
go

grant execute on sel_quant_run_is_valid to cdv_users
go

grant execute on sel_quant_run_is_valid to sel_users
go

exec sp_procxmode "sel_quant_run_is_valid",Unchained
go


if exists (select 1 from sysobjects where type='P' and name='sel_quant_runs')
begin
 print "Suppression sel_quant_runs"
 drop procedure sel_quant_runs
end
go

create procedure sel_quant_runs
(
	@estimator_id	int,
	@domain_id		int,
	@valid			int/*,
	@begin_date		varchar(8), YYYYMMDD*/
	/*@end_date		varchar(8) YYYYMMDD*/
)
as
declare @request	varchar(2048)

/*select	@begin_date =	isnull(@begin_date, "10000101"),
		@end_date =		isnull(@end_date, "99991231")*/
begin
	set @request = '
	select
		run.run_id,
		run.estimator_id,
		context.context_num			as context_num,
		context.context_name		as context,			
		job.domain_id,
		domain.domain_name			as domain,			
		run.run_quality,							
		run.stamp_date			as run_date,
		run.is_valid,
		run.comment,
		(case when a.last_run_id = run.run_id then 1 else 0 end) as is_last_run
		
	from
		estimator_runs run,
		context context, 
		job job,
		domain domain,
		last_run a
		 
	where	run.estimator_id =		context.estimator_id
		and run.job_id =			job.job_id 
		and context.context_id =	run.context_id
		and domain.domain_id =		job.domain_id
		
		and a.job_id = job.job_id
                and a.context_id = run.context_id
		
		/* Gestion des parametres */
		and job.estimator_id =	isnull(@estimator_id,	job.estimator_id)
		and job.domain_id =		isnull(@domain_id,		job.domain_id)
		'
	if (@valid != null)
		set @request = @request || 'and run.is_valid = ' || convert( varchar(1), @valid )
	/*set @request = @request || '
		and @begin_date	<=		isnull(domain.begin_date, @begin_date)
		and isnull(domain.end_date, @end_date)		<=	@end_date
		'*/
	
	execute(@request)
end
go

grant execute on sel_quant_runs to cdv_users
go

grant execute on sel_quant_runs to sel_users
go

exec sp_procxmode "sel_quant_runs",Unchained
go


/* UPDATE */
if exists (select 1 from sysobjects where type='P' and name='update_quant_run_comment')
begin
 print "Suppression update_quant_run_comment"
 drop procedure update_quant_run_comment
end
go

create procedure update_quant_run_comment (
	@run_id			int,
	@comment	varchar(255)
)
as

begin
	update
		estimator_runs
	set
		comment = @comment
	where
		run_id = @run_id
		
end
go

grant execute on update_quant_run_comment to cdv_users
go

grant execute on update_quant_run_comment to sel_users
go

exec sp_procxmode "update_quant_run_comment",Unchained
go


if exists (select 1 from sysobjects where type='P' and name='update_quant_run_is_validated')
begin
 print "Suppression update_quant_run_is_validated"
 drop procedure update_quant_run_is_validated
end
go

create procedure update_quant_run_is_validated (
	@run_id			int,
	@is_validated	bit
)
as
declare @estimator_id	int
declare @domain_id		int
declare @context_id	int

begin
	if(@is_validated = 1)
	begin
		select
			@estimator_id	= estimator_id,
			@domain_id		= domain_id,
			@context_id= context_id
		from
			estimator_runs
		where
			run_id = @run_id
		
		update
			estimator_runs
		set
			is_valid =	0
		where 
					estimator_id	= @estimator_id
			and		domain_id		= @domain_id
			and		context_id		= @context_id
	end
		
	update
		estimator_runs
	set
		is_valid =	@is_validated
	where
		run_id = @run_id
end
go

grant execute on update_quant_run_is_validated to cdv_users
go

grant execute on update_quant_run_is_validated to sel_users
go

exec sp_procxmode "update_quant_run_is_validated",Unchained
go