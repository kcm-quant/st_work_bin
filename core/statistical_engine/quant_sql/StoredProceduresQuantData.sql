use quant_data
go


/******************************PROCEDURES STOCKEES**********************************/



-- qd_get_param
IF EXISTS (SELECT 1 FROM sysobjects WHERE name = 'qd_get_param' AND type = 'P')
BEGIN
   PRINT "drop procedure qd_get_param"
   DROP PROCEDURE qd_get_param
END
GO
PRINT "create procedure qd_get_param"
GO

CREATE PROCEDURE qd_get_param
(
@estimator_name varchar(32),
@context_name varchar(32),
@security_id INT,
@trading_destination_id INT,
@varargin VARCHAR(16)=NULL
)
AS 
BEGIN
	SELECT
		qp.estimator_id,
		qp.context_id,
		qp.domain_id,
		qp.x_	
		value,
		qp.parameter_id,
		qp.parameter_name,
		qp.value               
	FROM
		quant_data..quant_reference qr,
		quant_data..quant_param qp,
		quant..estimator e,  
		quant..context c
	WHERE 
		upper(e.estimator_name) LIKE upper(@estimator_name)AND
		qr.security_id =@security_id AND
		(qr.trading_destination_id=isnull(@trading_destination_id,trading_destination_id)) AND
		qr.estimator_id = e.estimator_id  AND
		qp.domain_id = qr.domain_id AND	 
		qp.estimator_id =qr.estimator_id AND 
		upper(c.context_name) LIKE upper(@context_name)AND
		qp.context_id = c.context_id AND 
		qp.context_id =qr.context_id AND
		(qr.varargin=@varargin OR @varargin is NULL )
		
	ORDER BY
		qr.run_id ,qp.x_value
END
GO

GRANT EXECUTE ON qd_get_param TO sel_users
GO

GRANT EXECUTE ON qd_get_param TO cdv_users
GO

EXEC sp_procxmode "qd_get_param",Unchained
GO




-- qd_get_run_id
IF EXISTS (SELECT 1 FROM sysobjects WHERE name = 'qd_get_run_id' AND type = 'P')
BEGIN
   PRINT "drop procedure qd_get_run_id"
   DROP PROCEDURE qd_get_run_id
END
GO
PRINT "create procedure qd_get_run_id"
GO

CREATE PROCEDURE qd_get_run_id
(
@estimator_name varchar(32),
@security_id INT,
@trading_destination_id INT,
@varargin VARCHAR(16)=NULL
)
AS 
BEGIN
		qr.*
	FROM
		quant_data_homolo..quant_reference qr,
		quant_homolo..estimator e
	WHERE 
		upper(e.estimator_name) LIKE upper(@estimator_name)AND
		qr.security_id =@security_id AND
		(qr.trading_destination_id=isnull(@trading_destination_id,trading_destination_id)) AND
		qr.estimator_id = e.estimator_id  AND
        qr.default_context =1 AND
		(qr.varargin=@varargin OR @varargin is NULL )
END
GO

GRANT EXECUTE ON qd_get_run_id TO sel_users
GO

GRANT EXECUTE ON qd_get_run_id TO cdv_users
GO

EXEC sp_procxmode "qd_get_run_id",Unchained
GO