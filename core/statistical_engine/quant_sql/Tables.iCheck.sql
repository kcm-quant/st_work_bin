-- Table int_check_def
if exists (select 1 from sysobjects where name = 'int_check_def' and type = 'U')
begin
	print 'drop table int_check_def'
	drop table int_check_def
end
go

print 'create table int_check_def'
go

create table int_check_def
(
	int_check_def_id					int			identity			not null,
	loaded_file_id						int								not null,
	mnemonic                            varchar(32)         			not null,
	name            					varchar(32)         			not null,
	cron_regexp							varchar(255)					not null,
	comment								varchar(255)					not null,
	active_from_dt						datetime						not null,
	active_to_dt						datetime						null,
	last_verification_dt				datetime						null,
	last_instanciation_dt				datetime						null,
	last_output_dt						datetime						null
)
lock DataRows
go

create unique nonclustered index int_check_def_pk on int_check_def(int_check_def_id)
go

-- %TODO ? add an index on mnemonic

grant select,insert,update,delete on int_check_def to cdv_users
go

grant select on int_check_def to sel_users
go



-- Table int_alarm_def
if exists (select 1 from sysobjects where name = 'int_alarm_def' and type = 'U')
begin
	print 'drop table int_alarm_def'
	drop table int_alarm_def
end
go

print 'create table int_alarm_def'
go

create table int_alarm_def
(
	int_alarm_def_id                	int			identity			not null,
	int_check_def_id					int                         	not null,
	int_alarm_type_id					int                         	not null,
	int_alarm_scrutation_id				int								not null,
    is_active                           int                             not null
)
lock DataRows
go

create unique nonclustered index int_alarm_def_pk on int_alarm_def(int_alarm_def_id)
go

grant select,insert,update,delete on int_alarm_def to cdv_users
go

grant select on int_alarm_def to sel_users
go



-- Table ref_int_alarm_type
if exists (select 1 from sysobjects where name = 'ref_int_alarm_type' and type = 'U')
begin
	print 'drop table ref_int_alarm_type'
	drop table ref_int_alarm_type
end
go

print 'create table ref_int_alarm_type'
go

create table ref_int_alarm_type
(
	int_alarm_type_id               	int			identity			not null,
	name								varchar(32)                     not null,
	norder								int								not null,
	is_active							int								not null
)
lock DataRows
go

create unique nonclustered index int_alarm_type_pk on ref_int_alarm_type(int_alarm_type_id)
go

grant select,insert,update,delete on ref_int_alarm_type to cdv_users
go

grant select on ref_int_alarm_type to sel_users
go

insert into ref_int_alarm_type ( name, norder, is_active ) values ( 'database', 1, 1 )
go

insert into ref_int_alarm_type ( name, norder, is_active ) values ( 'mail', 2, 1 )
go

insert into ref_int_alarm_type ( name, norder, is_active ) values ( 'sms', 3, 1 )
go

insert into ref_int_alarm_type ( name, norder, is_active ) values ( 'log', 4, 1 )
go

insert into ref_int_alarm_type ( name, norder, is_active ) values ( 'report', 5, 1 )
go

insert into ref_int_alarm_type ( name, norder, is_active ) values ( 'itrs', 6, 1 )
go



-- Table ref_int_alarm_scrutation
if exists (select 1 from sysobjects where name = 'ref_int_alarm_scrutation' and type = 'U')
begin
	print 'drop table ref_int_alarm_scrutation'
	drop table ref_int_alarm_scrutation
end
go

print 'create table ref_int_alarm_scrutation'
go

create table ref_int_alarm_scrutation
(
	int_alarm_scrutation_id             int			identity			not null,
	name								varchar(32)             		not null,
	norder								int								not null,
	is_active							int								not null
)
lock DataRows
go

create unique nonclustered index int_alarm_scrutation_pk on ref_int_alarm_scrutation(int_alarm_scrutation_id)
go

grant select,insert,update,delete on ref_int_alarm_scrutation to cdv_users
go

grant select on ref_int_alarm_scrutation to sel_users
go

insert into ref_int_alarm_scrutation ( name, norder, is_active ) values ( 'iterative', 1, 1 )
go

insert into ref_int_alarm_scrutation ( name, norder, is_active ) values ( 'bundle', 2, 1 )
go



-- Table int_check
if exists (select 1 from sysobjects where name = 'int_check' and type = 'U')
begin
	print 'drop table int_check'
	drop table int_check
end
go

print 'create table int_check'
go

create table int_check
(
	int_check_id						int			identity			not null,
	int_check_def_id					int								not null,
	begin_dt							datetime						not null,
	end_dt								datetime						not null,
	request								text							null,
	severity_dc							decimal(5,4)					not null
)
lock DataRows
go

create unique nonclustered index int_check_pk on int_check(int_check_id)
go

grant select,insert,update,delete on int_check to cdv_users
go

grant select on int_check to sel_users
go



-- Table int_alarm
if exists (select 1 from sysobjects where name = 'int_alarm' and type = 'U')
begin
	print 'drop table int_alarm'
	drop table int_alarm
end
go

print 'create table int_alarm'
go

create table int_alarm
(
	int_alarm_id						int			identity			not null,
	int_alarm_def_id					int								not null,
	int_check_id						int								not null,
	name								varchar(255)					not null,
	destination							text							null,
	message								text							null,
	severity_dc							decimal(5,4)					not null
)
lock DataRows
go

create unique nonclustered index int_alarm_pk on int_alarm(int_alarm_id)
go

grant select,insert,update,delete on int_alarm to cdv_users
go

grant select on int_alarm to sel_users
go


-- Table obj_property
if exists (select 1 from sysobjects where name = 'obj_property' and type = 'U')
begin
	print 'drop table obj_property'
	drop table obj_property
end
go

print 'create table obj_property'
go

create table obj_property
(
	obj_property_id						int			identity			not null,
	obj_name							varchar(32)						not null,
	obj_id								int                 			not null,
	xml_element_name    				varchar(32)						not null,
	name								varchar(32)						not null,
	type								varchar(32)						not null,
	value								text							null
)
lock DataRows
go

create unique nonclustered index obj_property_pk on obj_property(obj_property_id)
go

grant select,insert,update,delete on obj_property to cdv_users
go

grant select on obj_property to sel_users
go



-- Table loaded_file
if exists (select 1 from sysobjects where name = 'loaded_file' and type = 'U')
begin
	print 'drop table loaded_file'
	drop table loaded_file
end
go

print 'create table loaded_file'
go

create table loaded_file
(
	loaded_file_id						int			identity			not null,
	load_dt								datetime						not null,
	is_load_ok							int								not null,
	file_name							varchar(255)					not null,
	file_modified_dt					datetime						not null,
	file_md5							varchar(32)						not null
)
lock DataRows
go

create unique nonclustered index loaded_file_pk on loaded_file(loaded_file_id)
go

grant select,insert,update,delete on loaded_file to cdv_users
go

grant select on loaded_file to sel_users
go



-- Table trigger_buffer
if exists (select 1 from sysobjects where name = 'trigger_buffer' and type = 'U')
begin
	print 'drop table trigger_buffer'
	drop table trigger_buffer
end
go

print 'create table trigger_buffer'
go

create table trigger_buffer
(
	trigger_buffer_id					int			identity			not null,
	table_name							varchar(32)						not null,
	id_column_name						varchar(32)						not null)
lock DataRows
go

create unique nonclustered index trigger_buffer_pk on trigger_buffer(trigger_buffer_id)
go

grant select,insert,update,delete on trigger_buffer to cdv_users
go

grant select on trigger_buffer to sel_users
go




-- Table trigger_buffer_id_list
if exists (select 1 from sysobjects where name = 'trigger_buffer_id_list' and type = 'U')
begin
	print 'drop table trigger_buffer_id_list'
	drop table trigger_buffer_id_list
end
go

print 'create table trigger_buffer_id_list'
go

create table trigger_buffer_id_list
(
	datetime						datetime					not null,
	trigger_buffer_id					int						not null,
	id_column_id						int						not null)
lock DataRows
go

-- create index datetime_pk on trigger_buffer_id_list(datetime)
-- go

grant select,insert,update,delete on trigger_buffer_id_list to cdv_users
go

grant select on trigger_buffer_id_list to sel_users
go
