/*******************************************************************************************************/
/********************************************Version 1.0************************************************/
/*******************************************************************************************************/

use quant
go

/******************************TABLES********************************/

-- Table frequency
print "Supprime la table frequency"
go

if exists (select 1 from sysobjects where name = 'frequency' and type = 'U')
begin
    print "drop table frequency"
    drop table frequency
end
go

print "Creation de la table frequency"
go

create table frequency
(
  frequency_id   int     identity     not null,
  frequency_name varchar(32)          not null,
  comment        varchar(255)         not null  
)
lock DataRows
go

create unique nonclustered index frequency_pk on frequency(frequency_id)
go

grant select,insert,update,delete  on frequency  to cdv_users
go
grant select   on frequency  to sel_users
go

print "Insere les valeurs par defaut"
go

insert into frequency (frequency_name, comment)
values ('daily', '')
go

insert into frequency (frequency_name, comment)
values ('weekly', '')
go

insert into frequency (frequency_name, comment)
values ('monthly', '')
go

insert into frequency (frequency_name, comment)
values ('asOfDate', '')
go






-- Table output_type
if exists (select 1 from sysobjects where name = 'output_type' and type = 'U')
begin
    print "drop table output_type"
    drop table output_type
end
go

print "Creation de la table output_type"
go

create table output_type
(
  output_type_id   int    identity   not null,
  output_type_name varchar(32)       not null,
  comment          varchar(255)      null
)
lock DataRows
go

create unique nonclustered index output_type_pk on output_type(output_type_id)
go

grant select,insert,update,delete  on output_type  to cdv_users
go
grant select   on output_type  to sel_users
go


INSERT INTO output_type (output_type_name, comment) VALUES ('table', 'Affiche dans un tableau')
GO
INSERT INTO output_type (output_type_name, comment) VALUES ('plot', 'Affiche une courbe')
GO




-- Table domain
if exists (select 1 from sysobjects where name = 'domain' and type = 'U')
begin
    print "drop table domain"
    drop table domain
end
go

print "Creation de la table domain"
go

create table domain(
	domain_id   			int  								identity,
	domain_name 		  	varchar(32)  						not null,
	comment   				varchar(255)  						not null,
	domain_type_id			int									not null,
	domain_type_variable	varchar(255)						not null,
	domain_type_rank		int									not null
)
lock datarows
go

create unique nonclustered index pk on domain ( domain_id ASC)

grant select,insert,update,delete  on domain  to cdv_users
go
grant select   on domain  to sel_users
go




-- Table estimator
if exists (select 1 from sysobjects where name = 'estimator' and type = 'U')
begin
    print "drop table estimator"
    drop table estimator
end
go

print "Creation de la table estimator"
go

create table estimator
(
  estimator_id    int     identity     not null,
  estimator_name  varchar(32)  not null,
  comment         varchar(255) not null,
  matlab_run      varchar(255) null,
  matlab_check    varchar(255) null,
  output_type_id  int     null,
  owner  		  varchar(255)    null,
  exec_order    int                     not null,
  validation_context_num    int     not null

)
lock DataRows
go

ALTER table estimator add is_check_validation int default 0
go

create unique nonclustered index estimator_pk on estimator(estimator_id)
go

create unique nonclustered index estimator_name_pk on estimator(estimator_name)
go

grant select,insert,update,delete  on estimator  to cdv_users
go
grant select   on estimator  to sel_users
go




-- Table param_desc
if exists (select 1 from sysobjects where name = 'param_desc' and type = 'U')
begin
    print "drop table param_desc"
    drop table param_desc
end
go

print "Creation de la table param_desc"
go

create table param_desc
(
  parameter_id   int     identity     not null,
  -- notion d'abcisse associee au parametre. Si non null indique que la famille concerne une courbe
  x_value        float        null,    
  parameter_name varchar(32)  not null,
  estimator_id   int          not null,-- constraint cont_param_est references estimator(estimator_id), -- famille de parametres (parameters_family) a laquelle appartient le parametre
  comment        varchar(255) not null
)
lock DataRows
go

create unique nonclustered index parameter_pk on param_desc(parameter_id)
go
create        nonclustered index estimator_sk    on param_desc(estimator_id)
go
create unique nonclustered index parameter_pk2 on param_desc (parameter_name ASC,estimator_id ASC)
go

grant select,insert,update,delete  on param_desc  to cdv_users
go
grant select   on param_desc  to sel_users
go




-- Table domain_security
if exists (select 1 from sysobjects where name = 'domain_security' and type = 'U')
begin
    print "drop table domain_security"
    drop table domain_security
end
go

print "Creation de la table domain_security"
go

create table domain_security
(
  domain_sec_id          int  identity not null,
  domain_id              int  not null,-- constraint cont_ds_dom references domain(domain_id), -- object_id a utiliser, reference la table object_component
  security_id            int  not null, -- reference un security de market..security
  -- marche concerne, permet de differencier par exemple jeu de parametres sur une valeur quand elle est traitee sur Euronext ou sur Chi-X
  trading_destination_id int  null    
)
lock DataRows
go

create unique  nonclustered index domain_security_pk on domain_security(domain_sec_id)
go

grant select,insert,update,delete  on domain_security  to cdv_users
go
grant select   on domain_security  to sel_users
go




-- Table date_remove
if exists (select 1 from sysobjects where name = 'date_remove' and type = 'U')
begin
    print "drop table date_remove"
    drop table date_remove
end
go

print "Creation de la table date_remove"
go

create table date_remove
(
  date_remove_id	int identity not null,
  domain_id     int  not null,-- constraint cont_dr_ds references domain(domain_id),
  excluded_date     date not null, -- date a exclure
  run_id            int  null,
  estimator_id      int  not null,-- constraint cont_dr_e references estimator(estimator_id),
  stamp_date        date not null,
  login             varchar(32) null
)
lock DataRows
go

create unique nonclustered index date_remove_k on date_remove(date_remove_id)
go

grant select,insert,update,delete  on date_remove  to cdv_users
go
grant select   on date_remove  to sel_users
go




-- Table context
if exists (select 1 from sysobjects where name = 'context' and type = 'U')
begin
    print "drop table context"
    drop table context
end
go

print "Creation de la table context"
go

create table context
(
  context_id      int     identity    not null,
  context_name    varchar(36) not null,
  estimator_id    int         not null,-- constraint cont_cont_est references estimator(estimator_id),
  context_num     int         null,
  comment         varchar(255)   null
)
lock DataRows
go

create unique nonclustered index context_pk on context(context_id)
go

create unique nonclustered index context_name_pk on context(context_name, estimator_id)
go

grant select,insert,update,delete  on context  to cdv_users
go
grant select   on context  to sel_users
go
-- 08/12/2011 : pour developpement interface de selection de courbes dans TCC
alter table context
  add label varchar(128) default '',
      to_export bit default 1
go


-- Table window_type
if exists (select 1 from sysobjects where name = 'window_type' and type = 'U')
begin
    print "drop table window_type"
    drop table window_type
end
go

print "Creation de la table window_type"
go
create table window_type
(
  window_type_id      int     identity    not null,
  window_type_name    varchar(32) not null,
  comment             varchar(255)   null
)
lock DataRows
go

create unique nonclustered index window_type_pk on window_type(window_type_id)
go

grant select,insert,update,delete  on window_type  to cdv_users
go
grant select   on window_type  to sel_users
go

INSERT INTO window_type (window_type_name) VALUES ('day')
GO




-- Table family_runs
if exists (select 1 from sysobjects where name = 'estimator_runs' and type = 'U')
begin
    print "drop table estimator_runs"
    drop table estimator_runs
end
go

print "Creation de la table estimator_runs"
go

create table estimator_runs
(
  run_id            int    identity   not null, -- numero d execution de calcul unique
  job_id            int       not null,-- constraint cont_run_job references job(job_id),
  estimator_id      int       not null,-- constraint cont_run_est references estimator(estimator_id), -- famille concernee par le calcul
  stamp_date        date      not null,
  domain_id         int       not null,-- constraint cont_run_domaine references domain(domain_id), -- identifiant de l'objet sur leauel porte le calcul (action, panier)
  run_quality       float     not null, -- indicateur de qualite du run calcule lors de la creation du run
  context_id        int       not null,  -- 
  window_end_date   date      not null,
  is_valid          bit       not null, -- flag indiquant si le jeu de parametres a ete valide. smallint pour utilisation dans l'index
  validation_type   int          null,
  devalidation_type int          null,
  session_num       int          null,
  comment           varchar(255) null
)
lock DataRows
go

-- index pour replication
create unique nonclustered index run_pk     on estimator_runs(run_id)
go
-- pertinence des indexes a reevaluer a l'usage
create        nonclustered index domain_sk1 on estimator_runs(domain_id, estimator_id, stamp_date)
go
create        nonclustered index estimator_sk1 on estimator_runs(estimator_id, stamp_date)
go

grant select,insert,update,delete  on estimator_runs  to cdv_users
go
grant select   on estimator_runs  to sel_users
go

-- ALTER ESTIMATOR_RUNS
/*ALTER table estimator_runs add validation_type int null
go
ALTER table estimator_runs add devalidation_type int null
go
ALTER table estimator_runs add session_num int null
go*/



-- Table param_value
if exists (select 1 from sysobjects where name = 'param_value' and type = 'U')
begin
    print "drop table param_value"
    drop table param_value
end
go

print "Creation de la table param_value"
go

create table param_value
(
  param_value_id	int    identity	not null,
  run_id       int   not null,-- constraint cont_value_run references estimator_runs(run_id), -- reference une execution de family_runs
  parameter_id int   not null,-- constraint cont_param_run references param_desc(parameter_id), -- reference un parametre de parameter_desc
  value        float null     --null values are used as NaN values in Matlab
)
lock DataRows
go

create unique  nonclustered index parameter_value_pk on param_value(run_id, parameter_id)
go

grant select,insert,update,delete  on param_value  to cdv_users
go
grant select   on param_value  to sel_users
go





-- Table estimator_post_quality
if exists (select 1 from sysobjects where name = 'check_quality' and type = 'U')
begin
    print "drop table check_quality"
    drop table check_quality
end
go

print "Creation de la table est_post_quality"
go

create table check_quality
(
  check_quality_id     int     identity     not null,
  run_id                  int   not null,-- constraint cont_epq_run references estimator_runs(run_id),
  value                   float not null,
  stamp_date              date  not null,
  comment                 varchar(255)  null
)
lock DataRows
go

create unique  nonclustered index check_quality_pk on check_quality(check_quality_id,run_id,stamp_date)
go

grant select,insert,update,delete  on check_quality  to cdv_users
go
grant select   on check_quality  to sel_users
go

--ALTER TABLE ajout de la colonne session_num
ALTER table check_quality add session_num int null
go

-- Table last_run
if exists (select 1 from sysobjects where name = 'last_run' and type = 'U')
begin
    print "drop table last_run"
    drop table last_run
end
go

print "Creation de la table last_run"
go

create table last_run
(
  job_id             int   not null,-- constraint cont_lr_job references job(job_id),
  context_id          int   not null,
  last_run_date      date  null,
  last_run_id        int   null,-- constraint cont_lr_runs references estimator_runs(run_id),
  last_valid_run_id  int   null,-- constraint cont_lr_vruns references estimator_runs(run_id),
  last_check_date    date  null,
  next_check_date    date  null
)
lock DataRows
go

create unique  nonclustered index last_run_pk on last_run(job_id, last_run_id, last_valid_run_id)
go

grant select,insert,update,delete  on last_run  to cdv_users
go
grant select   on last_run  to sel_users
go





-- Table error
if exists (select 1 from sysobjects where name = 'error' and type = 'U')
begin
    print "drop table error"
    drop table error
end
go

print "Creation de la table error"
go

create table error
(
  error_id  int  identity             not null,
  job_or_run_id         int                  not null,-- constraint cont_error references job(job_id),   
  gravity_level  int                  not null,
  is_run         bit               	  not null,
  stamp_date     date              	  not null,
  stamp_time     time              	  not null,
  message        text         		  not null   
)
lock DataRows
go

create unique  nonclustered index error_pk on error(error_id, job_or_run_id)
go
create index opt on error(job_or_run_id )
go
grant select,insert,update,delete  on error  to cdv_users
go
grant select   on error  to sel_users
go



-- Table job_status

if exists (select 1 from sysobjects where name = 'job_status' and type = 'U')
begin
    print "drop table job_status"
    drop table job_status
end
go

print "Creation de la table job_status"
go

create table job_status
(
  job_status_id     int     identity 	not null,
  job_or_run_id     int    				not null,
  status         	varchar(255)       	not null,
  is_run			bit       			not null,
  beg_date        	date                null,
  beg_time        	time                null,
  end_date        	date                null,
  end_time        	time                null,
  job_name			VARCHAR(32)			NULL,
  session_num		int					not null
)
lock DataRows
go

create unique nonclustered index job_status_pk on job_status(job_status_id)
go
create nonclustered index opt_job_status on job_status ( job_or_run_id ASC,job_status_id)
go 

grant select,insert,update,delete  on job_status  to cdv_users
go
grant select   on job_status  to sel_users
go


-- Table association

if exists (select 1 from sysobjects where name = 'association' and type = 'U')
begin
      print "drop table association"
    drop table association
end
go

print "Creation de la table association"
go

create table association
(
	association_id  			int  identity       not null,
	context_id      			int                 	null,
	rank            			int                 	null, 
	security_id          		int                 	null,
	trading_destination_id      int                 	null, 
	estimator_id    			int                 	null, 
	job_id    					int                 not null,
	varargin  					varchar(16)         	null,
	from_meta                               int null
)
lock DataRows
go

create unique  nonclustered index association_pk on association(context_id, rank, security_id, trading_destination_id, estimator_id, varargin)
go

 grant select,insert,update,delete  on association  to cdv_users
go
grant select   on association  to sel_users
go

/**

-- ALTER ASSOCIATION
ALTER table association add from_meta int default 1
**/


--Table next_association

if exists (select 1 from sysobjects where name = 'next_association' and type = 'U')
begin
      print "drop table next_association"
    drop table next_association
end
go

print "Creation de la table next_association"
go

create table next_association
(
	next_association_id  			int  identity       not null,
	context_id      			int                 	null,
	rank            			int                 	null, 
	security_id          		int                 	null,
	trading_destination_id      int                 	null, 
	estimator_id    			int                 	null, 
	job_id    					int                 not null,
	varargin  					varchar(16)         	null,
	from_meta                               int null
)
lock DataRows
go

create unique  nonclustered index next_association_pk on next_association(context_id, rank, security_id, trading_destination_id, estimator_id, varargin)
go

 grant select,insert,update,delete  on next_association  to cdv_users
go
grant select   on next_association  to sel_users
go



-- Table last_association

if exists (select 1 from sysobjects where name = 'last_association' and type = 'U')
begin
      print "drop table last_association"
    drop table last_association
end
go



-- Table last_association
if exists (select 1 from sysobjects where name = 'last_association' and type = 'U')
begin
      print "drop table last_association"
    drop table last_association
end
go

print "Creation de la table last_association"
go

create table last_association
(
	last_association_id  			int  identity       not null,
	association_id  			int         not null,
	context_id      			int    	null,
	rank            			int                 	null, 
	security_id          		int                 	null,
	trading_destination_id      int                 	null, 
	estimator_id    			int                 	null, 
	job_id    					int                 not null,
	varargin  					varchar(16)         	null,
	from_meta                               int null
)
lock DataRows
go

create unique  nonclustered index last_association_pk on last_association(context_id, rank, security_id, trading_destination_id, estimator_id, varargin)
go

 grant select,insert,update,delete  on last_association  to cdv_users
go
grant select   on last_association  to sel_users
go


-- Table job
if exists (select 1 from sysobjects where name = 'job' and type = 'U')
begin
   print "drop table job"
   drop table job
end
go

print "Creation de la table job"
go

create table job
(
	job_id  					int     identity		not null,
	estimator_id         		int						not null,-- constraint cont_job_est references estimator(estimator_id),
	domain_id            		int           			not null,-- constraint cont_job_dom references domain(domain_id),
	comment              		varchar(255)  				null,
	run_frequency_id     		int           			not null,-- constraint cont_job_freq1 references frequency(frequency_id),
	run_window_type_id      	int           				null,-- constraint cont_job_wt1 references window_type(window_type_id),
	run_window_width     		int           				null,
	run_frequency_date   		date          				null,
	check_frequency_id     		int           			not null,-- constraint cont_job_freq2 references frequency(frequency_id),
	check_window_type_id      	int           				null,-- constraint cont_job_wt2 references window_type(window_type_id),
	check_window_width     		int           				null,
	check_frequency_date   		date          				null,
	last_run                  	date          				null,
	next_run                  	date          				null,
	varargin        			varchar(1025)        		null
)
lock DataRows
go

grant select,insert,update,delete  on job  to cdv_users
go
grant select   on job  to sel_users
go



/*******************************************************************************************************/
/********************************************Version 2.0************************************************/
/*******************************************************************************************************/



/* update_type
 */

print "Creation de la table update_type"

if exists (select 1 from sysobjects where name = 'update_type' and type = 'U')
begin
    drop table update_type
end
go

create table update_type (
	update_type_id		int				identity     not null,
	update_type_name	varchar(32)		not null
)
lock DataRows
go

grant select,insert,update,delete on update_type to cdv_users
go
grant select on update_type to sel_users
go


insert into update_type (update_type_name) values ('union')
go
insert into update_type (update_type_name) values ('replace')
go



/* request_type
 */

print "Creation de la table request_type"

if exists (select 1 from sysobjects where name = 'request_type' and type = 'U')
begin
    drop table request_type
end
go

create table request_type (
	request_type_id		int				identity     not null,
	request_type_name	varchar(32)		not null
)
lock DataRows
go

grant select,insert,update,delete on request_type to cdv_users
go
grant select on request_type to sel_users
go


insert into request_type (request_type_name) values ('matlab')
go
insert into request_type (request_type_name) values ('sql')
go
--p�rim�tre statique
insert into request_type (request_type_name) values ('none')
go



/*
 * perimeter
 */

print "Creation de la table perimeter"

if exists (select 1 from sysobjects where name = 'perimeter' and type = 'U')
begin
    drop table perimeter
end
go

create table perimeter (
	perimeter_id		int				identity     not null,
	perimeter_name		varchar(32)		not null,
	comment				varchar(255)	not null,
	run_frequency_id	int				not null,
	run_frequency_date	date			null,
	next_run			date			null,
	update_type_id		int				not null,
	request				varchar(255)	not null,
	request_type		int	not null , 
	parameters                      varchar(255)	 null,

)
lock DataRows
go

grant select,insert,update,delete on perimeter to cdv_users
go
grant select on perimeter to sel_users
go

-- P�rim�tre par d�faut
insert into perimeter (
	perimeter_name,
	comment, 
	run_frequency_id,
	run_frequency_date,
	update_type_id,
	request,
	request_type
)
values (
	'static',
	'',
	3,
	'1899-01-01',
	1,
	'',
	3
)
go





/*
 * domain_type
 */


print "Creation de la table domain_type"

if exists (select 1 from sysobjects where name = 'domain_type' and type = 'U')
begin
    drop table domain_type
end
go

create table domain_type (
	domain_type_id		int				identity    not null,
	domain_type_name	varchar(255)				not null,
	comment				text						not null,
	domain_type			char						not null
)
lock DataRows go

grant select,insert,update,delete on domain_type to cdv_users go
grant select on domain_type to sel_users go

insert into domain_type (domain_type_name,comment,domain_type) values ('mono instrument', 'value','V') go
insert into domain_type (domain_type_name,comment,domain_type) values ('place index', 'index','I') go
--p�rim�tre statique
insert into domain_type (domain_type_name,comment,domain_type) values ('representative index', 'quotation group','G') go
--p�rim�tre asiatique
insert into domain_type (domain_type_name,comment,domain_type) values ('place index asia', 'quotation group','G') go





/*
 * perimeter_domain
*/
print "Creation de la table perimeter_domain"
go

if exists (select 1 from sysobjects where name = 'perimeter_domain' and type = 'U')
begin
    drop table perimeter_domain
end
go

create table perimeter_domain
(
	perimeter_domain_id       		int     identity    not null,
	domain_id     					int          		not null,
	perimeter_id         			int         		not null ,
	is_active                               int      not null
)
lock DataRows
go

grant select,insert,update,delete  on perimeter_domain  to cdv_users
go
grant select   on perimeter_domain  to sel_users
go

/*ALTER TABLE
alter table perimeter_domain add is_active int default 1 */





/*
 * perimeter_job
*/
print "Creation de la table perimeter_job"
go

if exists (select 1 from sysobjects where name = 'perimeter_job' and type = 'U')
begin
    drop table perimeter_job
end
go

create table perimeter_job
(
	perimeter_job_id       		int     identity    not null,
	job_id     					int          		not null,
	meta_job_id        			int         		not null,
	perimeter_id       			int         		not null,
	is_active      				bit    				not null
)
lock DataRows
go

grant select,insert,update,delete  on perimeter_job  to cdv_users
go
grant select   on perimeter_job  to sel_users
go


/*
 * meta_job
 */

print "Creation de la table meta_job"
go

if exists (select 1 from sysobjects where name = 'meta_job' and type = 'U')
begin
   drop table meta_job
end
go

create table meta_job
(
  meta_job_id  				int     identity	not null,
  estimator_id         		int  		        not null,
  perimeter_id         		int  		        not null,
  comment              		varchar(255) 			null,
  run_frequency_id     		int        		   	not null,
  run_window_type_id      	int         			null,
  run_window_width     		int         		  	null,
  run_frequency_date   		date         		 	null,
  check_frequency_id     	int      		    not null,
  check_window_type_id      int         			null,
  check_window_width     	int						null,
  check_frequency_date   	date         		 	null,
  varargin        			varchar(1025)			null
)
lock DataRows
go

grant select,insert,update,delete  on meta_job  to cdv_users
go
grant select   on meta_job  to sel_users
go


/* 
 * Table meta association
 */

print "Creation de la table meta_association"
go

if exists (select 1 from sysobjects where name = 'meta_association' and type = 'U')
begin
    drop table meta_association
end
go

create table meta_association
(
  meta_association_id		int		identity	not null,
  estimator_id			int                 		null,
  domain_type_id		int                 		null,
  request_type_id              	int                 	        null, 
  request                      	text		        	null,
  perimeter_id              	int 		                not null
)
lock DataRows
go

grant select,insert,update,delete  on meta_association  to cdv_users
go
grant select   on meta_association  to sel_users
go

-- TABLE META_ASSOCIATION
/*ALTER table meta_association add perimeter_id int default 0*/




/*
 * remove_job
*/
print "Creation de la table remove_job"
go

if exists (select 1 from sysobjects where name = 'remove_job' and type = 'U')
begin
    drop table remove_job
end
go

create table remove_job
(
	remove_job_id       		int     identity    not null,
	estimator_id				int          		not null,
	security_id        			int         			null,
	trading_destination_id 		int         			null,
	domain_type_id 				int         		not null
)
lock DataRows
go

grant select,insert,update,delete  on remove_job  to cdv_users
go
grant select   on remove_job  to sel_users
go


 
/*

--Patch filling domain_type_id
update domain
set domain_type_id=2 where domain_id in (
	select domain_id from domain_security group by domain_id having count(1) > 1
)


--Patch filling domain_type_variable
use temp_works
go

create table temp_domains ( di int, dv varchar(255))

insert into temp_domains 
select domain_id di, convert(varchar(255), security_id) + ',null' dv from quant_homolo..domain_security group by domain_id having count(1) = 1

use quant_homolo
go

update domain set domain_type_variable=dv
from temp_works..temp_domains
where domain_type_id=1 and domain_id=di
*/

/* 
 * Table validation_type
 */
if exists (select 1 from sysobjects where name = 'validation_type' and type = 'U')
begin
    print "drop table validation_type"
    drop table validation_type
end
go

print "Creation de la table validation_type"
go

create table validation_type
(
  validation_type_id   int    identity   not null,
  validation_type_name varchar(32)       not null,
  comment          varchar(255)      null
)
lock DataRows
go

create unique nonclustered index validation_type_pk on validation_type(validation_type_id)
go
Set identity_insert validation_type on
INSERT INTO validation_type (validation_type_id, validation_type_name, comment) VALUES (1, 'AUTOMATIC_VALIDATION' ,'')
go
INSERT INTO validation_type (validation_type_id, validation_type_name,  comment) VALUES (2, 'MANUAL_VALIDATION','')
go
insert into validation_type(validation_type_id, validation_type_name , comment) values (3, 'PRE_VALIDATION', '')
go
insert into validation_type(validation_type_id,validation_type_name , comment) values (4,'POST_VALIDATION', '')
go
insert into validation_type(validation_type_id, validation_type_name , comment) values (5 ,'STAND_BY', '')
go
Set identity_insert validation_type off

grant select,insert,update,delete  on validation_type  to cdv_users
go
grant select   on validation_type  to sel_users
go





/* 
 * Table valid_runs
 */
if exists (select 1 from sysobjects where name = 'valid_runs' and type = 'U')
begin
    print "drop table valid_runs"
    drop table valid_runs
end
go

print "Creation de la table valid_runs"
go

create table valid_runs
(
  run_id            int       not null,
  job_id            int       not null,-- constraint cont_run_job references job(job_id),
  estimator_id      int       not null,-- constraint cont_run_est references estimator(estimator_id), -- famille concernee par le calcul
  stamp_date        date      not null,
  context_id        int       not null,  -- 
  validation_date   date       null,
  validation_type   int        null,  -- 
  devalidation_date   date       null,
  devalidation_type   int        null,  -- 
  comment           varchar(255) null
)
lock DataRows
go

--indexes pour replication 
create nonclustered index run_pk     on valid_runs(run_id)
go
 
create        nonclustered index estimator_sk1 on valid_runs(estimator_id, stamp_date)
go

grant select,insert,update,delete  on valid_runs  to cdv_users
go
grant select   on valid_runs  to sel_users
go

--ALTER VALID_RUNS
/*ALTER table valid_runs add devalidation_type int null
ALTER table valid_runs add devalidation_date date null*/

/*ALTER table valid_runs MODIFY validation_type int null
ALTER table valid_runs MODIFY validation_date date null*/



/* 
 * Table pre_post_validation_book
 */

if exists (select 1 from sysobjects where name = 'pre_post_validation_book' and type = 'U')
begin
    print "drop table pre_post_validation_book"
    drop table pre_post_validation_book
end
go

print "Creation de la table pre_post_validation_book"
go

create table pre_post_validation_book
(
  book_id            	int    identity   not null, 
  matlab_func       	varchar(255)       not null,
  estimator_id      	int   not null,-- constraint cont_run_est references estimator(estimator_id), -- famille concernee par le calcul
  validation_type_id    int   not null,
  comment           varchar(255) null
)
lock DataRows
go

-- index pour replication
create unique nonclustered index book_pk     on pre_post_validation_book(book_id)
go

grant select,insert,update,delete  on pre_post_validation_book  to cdv_users
go
grant select   on pre_post_validation_book  to sel_users
go





/*
 * process_type
*/
print "Creation de la table process_type"

if exists (select 1 from sysobjects where name = 'process_type' and type = 'U')
begin
    drop table process_type
end
go

create table process_type (
	process_type_id		int				identity    not null,
	process_type_name	varchar(255)				not null,
	comment				text				not null
)
lock DataRows
go

grant select,insert,update,delete on process_type to cdv_users
go
grant select on process_type to sel_users
go


insert into process_type (process_type_name, comment) values ('META', 'Meta_job et  meta_association')
go

insert into process_type (process_type_name, comment) values ('RUN', '')
go

insert into process_type (process_type_name, comment) values ('CHECK', '')
go

insert into process_type (process_type_name, comment) values ('VALIDATION', '')
go

insert into process_type (process_type_name, comment) values ('PRE_VALIDATION', '')
go

insert into process_type (process_type_name, comment) values ('POST_VALIDATION', '')
go

/*
 * estimator_process
*/
print "Creation de la table estimator_process"
go

if exists (select 1 from sysobjects where name = 'estimator_process' and type = 'U')
begin
    drop table estimator_process
end
go

create table estimator_process
(
	process_id			int 		identity	not null,
	estimator_id       		int     		not null,
	process_type_id     		int                 not null,
	is_active      			bit    		    not null
)
lock DataRows
go

grant select,insert,update,delete  on estimator_process to cdv_users
go
grant select   on estimator_process to sel_users
go

/********************
* histo_association
*/

if exists (select 1 from sysobjects where name = 'histo_association' and type = 'U')
begin
      print "drop table histo_association"
    drop table histo_association
end
go

print "Creation de la table histo_association"
go

create table histo_association
(
	histo_association_id  			int  identity       not null,
	context_id      			int                 	null,
	rank            			int                 	null, 
	security_id          		int                 	null,
	trading_destination_id      int                 	null, 
	estimator_id    			int                 	null, 
	job_id    					int                 not null,
	varargin  					varchar(16)         	null,
	from_meta                               int null , 
	beg_date        	date                null,
	end_date        	date                null
)
lock DataRows
go

create nonclustered index histo_association_id
on histo_association (context_id ASC,rank ASC,security_id ASC,trading_destination_id ASC,estimator_id ASC,varargin ASC,beg_date ASC)
go

 grant select,insert,update,delete  on histo_association  to cdv_users
go
grant select   on histo_association  to sel_users
go


/****************************
* Session
*/

if exists (select 1 from sysobjects where name = 'session' and type = 'U')
begin
    print "drop table session"
    drop table session
end
go

print "Creation de la table session"
go

create table session
(
  id                    int     identity 	                not null,
  session_num		int					not null,
  beg_date        	date                null,
  beg_time        	time                null,
  end_date        	date                null,
  end_time        	time                null,
  
)
lock DataRows
go

create unique nonclustered index session_pk on session(id)
go

grant select,insert,update,delete  on session to cdv_users
go
grant select   on session to sel_users
go






/****************************
* Estimator ranking
*/

-- Table functional_need_to_estimator
print "Dropping table functional_need_to_estimator if it already exists"
go

if exists (select 1 from sysobjects where name = 'functional_need_to_estimator' and type = 'U')
begin
    print "drop functional_need_to_estimator"
    drop table functional_need_to_estimator
end
go

print "Creating the table functional_need_to_estimator"
go

create table functional_need_to_estimator
(
  functional_need   varchar(64)    not null,
  estimator_name    varchar(32)    not null,
  ranking           int            not null  
)
lock DataRows
go

create nonclustered index fneed_pk on functional_need_to_estimator(functional_need)
go

create unique nonclustered index fneed_unique_pk on functional_need_to_estimator(functional_need, ranking)
go

grant select,insert,update,delete  on functional_need_to_estimator  to cdv_users
go
grant select   on functional_need_to_estimator  to sel_users
go


/*
-- Should be done thanks to the xml : .xml ???
print "Inserting some values in functional_need_to_estimator"
go


insert into functional_need_to_estimator (functional_need, estimator_name, ranking)
select 'Forecast intraday volume proportions', 'cvdyn',                       1  union
select 'Forecast intraday volume proportions', 'Volume curve',                2  union
select 'Forecast intraday volume proportions', 'Parametric cv',               64 union
select 'Forecast intraday volume proportions', 'Context volume',              65 union
select 'Forecast Market Impact',               'Market Impact',               2  union
select 'Forecast Market Impact',               'Parametric Market Impact',    64 union
select 'Forecast intraday volatility',         'Parametric volatility curve', 64 union
select 'Forecast intraday volatility',         'Volatility curve',            65 union
select 'Tracking tactic',                      'Placement en cycle bu',       1  union
select 'Tracking tactic',                      'Parametric tracking tactic',  64
go
*/


/****************************
* Action to take for an event
*/

-- Table event_action
print "Dropping table event_action if it already exists"
go

if exists (select 1 from sysobjects where name = 'event_action' and type = 'U')
begin
    print "drop event_action"
    drop table event_action
end
go

print "Creating the table event_action"
go

create table event_action
(
  event_type        int            null,
  context_id        int            null,
  estimator_id      int            not null,
  default_ranking   int            null,
  action            varchar(1)     not null
)
lock DataRows
go

create nonclustered index eaction_pk1 on event_action(estimator_id)
go
create nonclustered index eaction_pk2 on event_action(event_type)
go
create nonclustered index eaction_pk3 on event_action(action)
go
create nonclustered index eaction_pk123 on event_action(estimator_id, event_type, action)
go
create unique nonclustered index eaction_unique_pk on event_action(estimator_id, event_type, action, default_ranking)
go


grant select,insert,update,delete  on event_action  to cdv_users
go
grant select   on event_action  to sel_users
go


/************************************************
* context_ranking
*/

if exists (select 1 from sysobjects where name = 'context_ranking' and type = 'U')
begin
    print "drop context_ranking"
    drop table context_ranking
end
go

print "Creation de la table context_ranking"
go
create table context_ranking
(
  estimator_id            int not null,
  security_id             int null,
  trading_destination_id  int null,
  varargin                varchar(16) null,
  context_id              int null,  
  event_type              int null,  
  ranking                 int not null  
)
lock DataRows
go
create unique nonclustered index uniq_index_event on context_ranking ( security_id,trading_destination_id ,varargin ,estimator_id ,event_type )
go
create unique nonclustered index uniq_index_thisisaranking on context_ranking ( security_id ,trading_destination_id ,varargin ,estimator_id ,ranking )
go

grant select,insert,update,delete  on context_ranking  to cdv_users
go
grant select   on context_ranking  to sel_users
go


/************************************************
* learningmodel
*/

if exists (select 1 from sysobjects where name = 'learningmodel' and type = 'U')
begin
    print "drop learningmodel"
    drop table learningmodel
end
go


print "Creation de la table learningmodel"
go
create table learningmodel
(
  security_id             int null,
  trading_destination_id  int null,
  varargin                varchar(16) null,
  model                   varchar(255),  
  estimator_id            int not null,
  sampling_frequency      int not null
)
lock DataRows
go
create unique nonclustered index assoc_id on learningmodel(estimator_id,security_id,trading_destination_id, varargin)
go
grant select,insert,update,delete  on learningmodel  to cdv_users
go
grant select   on learningmodel  to sel_users
go
