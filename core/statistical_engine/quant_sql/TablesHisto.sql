use quant_homolo
go




/******************************TABLES HISTO********************************/


-- Table family_runs
if exists (select 1 from sysobjects where name = 'histo_estimator_runs' and type = 'U')
begin
    print "drop table histo_estimator_runs"
    drop table histo_estimator_runs
end
go

print "Creation de la table histo_estimator_runs"
go

create table histo_estimator_runs
(
  run_id            int       not null, -- numero d execution de calcul unique
  job_id            int       not null,-- constraint cont_run_job references job(job_id),
  estimator_id      int       not null,-- constraint cont_run_est references estimator(estimator_id), -- famille concernee par le calcul
  stamp_date        date      not null,
  domain_id         int       not null,-- constraint cont_run_domaine references domain(domain_id), -- identifiant de l'objet sur leauel porte le calcul (action, panier)
  run_quality       float     not null, -- indicateur de qualite du run calcule lors de la creation du run
  context_id        int       not null,  -- 
  window_end_date   date      not null,
  is_valid          bit       not null, -- flag indiquant si le jeu de parametres a ete valide. smallint pour utilisation dans l'index
  comment           varchar(255) null
)
lock DataRows
go

grant all on histo_estimator_runs to public
go





-- Table param_value
if exists (select 1 from sysobjects where name = 'histo_param_value' and type = 'U')
begin
    print "drop table histo_param_value"
    drop table histo_param_value
end
go

print "Creation de la table histo_param_value"
go

create table histo_param_value
(
  param_value_id	int    not null,
  run_id       int   not null,-- constraint cont_value_run references estimator_runs(run_id), -- reference une execution de family_runs
  parameter_id int   not null,-- constraint cont_param_run references param_desc(parameter_id), -- reference un parametre de parameter_desc
  value        float null     --null values are used as NaN values in Matlab
)
lock DataRows
go

grant all on histo_param_value to public
go




-- Table estimator_post_quality
if exists (select 1 from sysobjects where name = 'histo_check_quality' and type = 'U')
begin
    print "drop table histo_check_quality"
    drop table histo_check_quality
end
go

print "Creation de la table histo_check_quality"
go

create table histo_check_quality
(
  check_quality_id     int      not null,
  run_id                  int   not null,-- constraint cont_epq_run references estimator_runs(run_id),
  value                   float not null,
  stamp_date              date  not null,
  comment                 varchar(255)  null
)
lock DataRows
go

grant all on histo_check_quality to public
go





-- Table error
if exists (select 1 from sysobjects where name = 'histo_error' and type = 'U')
begin
    print "drop table histo_error"
    drop table histo_error
end
go

print "Creation de la table histo_error"
go

create table histo_error
(
  error_id  int           not null,
  job_or_run_id         int                  not null,-- constraint cont_error references job(job_id),   
  gravity_level  int                  not null,
  is_run         bit               	  not null,
  stamp_date     date              	  not null,
  stamp_time     time              	  not null,
  message        text         		  not null   
)
lock DataRows
go

grant all on histo_error to public
go




-- Table histo_job_status

if exists (select 1 from sysobjects where name = 'histo_job_status' and type = 'U')
begin
    print "drop table histo_job_status"
    drop table histo_job_status
end
go

print "Creation de la table histo_job_status"
go

create table histo_job_status
(
  job_status_id     int              	not null,
  job_or_run_id     int    				not null,
  status         	varchar(255)       	not null,
  is_run			bit       			not null,
  beg_date        	date                null,
  beg_time        	time                null,
  end_date        	date                null,
  end_time        	time                null,
  job_name			VARCHAR(32)			NULL
)
lock DataRows
go

grant all on histo_job_status to public
go



