
--Pour �viter la validation de plusieurs run pour un m�me job


use quant
go


create trigger ins_valid_run
on estimator_runs
for insert, update
as
declare
@is_valid int,
@exist_is_valid int
select @is_valid = inserted.is_valid from inserted
if @is_valid=1
begin
	set @exist_is_valid=0
	select @exist_is_valid =count(*) from estimator_runs, inserted 
					where estimator_runs.estimator_id=inserted.estimator_id
                      and estimator_runs.domain_id=inserted.domain_id
                      and estimator_runs.context_id=inserted.context_id
                      and estimator_runs.is_valid=1
    if @exist_is_valid = 2
       begin
		   rollback transaction 
		   raiserror 20000 "Impossible d'avoir deux job valide"
       end
end



