
/*
TRIGGER estimator_runs
*/

	create trigger trg_estimator_runs_UPDATE on estimator_runs for update
	as 
	begin 
		declare @myrowcount            int    
		declare @donotmanagetran       int    
		declare @myerror               int 
		
		select @donotmanagetran = @@trancount    
		if (@donotmanagetran = 0)    
		  begin tran    
		  
		
	if update(is_valid)
		begin
			--mise � jour de la table last_run
			update last_run 
			set last_valid_run_id = null 
			from last_run, inserted
			where inserted.job_id = last_run.job_id and
			inserted.context_id = last_run.context_id and 
			inserted.is_valid = 0
			
			select @myrowcount=@@rowcount, @myerror=@@error   
			if @myerror != 0  
			begin    
			if (@donotmanagetran = 0)    
				rollback tran    
				raiserror 100001 "la mise � jour de la table 1 last_run [trg_estimator_runs_UPDATE] a echouee!"    
				
			end 
				
			update last_run 
			set last_valid_run_id = run_id
			from last_run, inserted
			where inserted.job_id = last_run.job_id and
			inserted.context_id = last_run.context_id and
			inserted.is_valid = 1	
			
			select @myrowcount=@@rowcount, @myerror=@@error  
			if @myerror != 0 
			begin    
				if (@donotmanagetran = 0)    
					rollback tran    
				raiserror 100002 "la mise � jour de la table  2 last_run [trg_estimator_runs_UPDATE] a echouee!"    
			end 
			
			--INSERTION dans la table valid_runs	
			insert into valid_runs (
			run_id, job_id, estimator_id, stamp_date, context_id, validation_date, validation_type, devalidation_type, devalidation_date, comment)
			select
			inserted.run_id,
			inserted.job_id,
			inserted.estimator_id,
			inserted.stamp_date,
			inserted.context_id,
			getdate(),
			inserted.validation_type,
			null,
			null,
			inserted.comment
			from inserted 
			where 
			inserted.is_valid = 1
			
			select @myrowcount=@@rowcount, @myerror=@@error
			  
			if @myerror != 0 
			begin    
				if (@donotmanagetran = 0)    
				rollback tran    
				raiserror 100003 "l insertion dans la table 3 valid_runs [trg_estimator_runs_UPDATE] a echouee!"    
				
			end 
			
			--UPADTE dans la table valid_runs	
			update valid_runs 
			set vr.devalidation_type = inserted.devalidation_type , vr.devalidation_date =getdate()
			from valid_runs vr, inserted 
		    where
		    inserted.run_id = vr.run_id and
		    inserted.job_id = vr.job_id and
			inserted.context_id= vr.context_id and 
			inserted.is_valid =0
			
			select @myrowcount=@@rowcount, @myerror=@@error
			  
			if @myerror != 0  
			begin    
				if (@donotmanagetran = 0)    
				rollback tran    
				raiserror 100004 "la mise � jour de la table 4 valid_runs [trg_estimator_runs_UPDATE] a echouee!"    
				
			end 
		end		
	end

-- trg_int_perimeter
if exists (select 1 from sysobjects where name = 'trg_int_perimeter' and type = 'TR')
begin
	print "drop trigger trg_int_perimeter"
	drop trigger trg_int_perimeter
end
go

print "create trigger trg_int_perimeter"
go

create trigger trg_int_perimeter on perimeter for insert, update
as
begin
	declare @table_name			varchar(32)
	declare @id_column_name			varchar(32)
	declare @myrowcount			int
	declare @donotmanagetran    		int
	declare @myerror            		int
	declare @trigger_buffer_id		int

---------------------------------------------------------------------------------------------------------
----                                                                                                 ----
	select @table_name='perimeter', @id_column_name='perimeter_id'
----                                                                                                 ----
---------------------------------------------------------------------------------------------------------

	select @donotmanagetran = @@trancount
	if (@donotmanagetran = 0)
		begin tran

	select @trigger_buffer_id = trigger_buffer_id from trigger_buffer where table_name=@table_name

	if @trigger_buffer_id = NULL
	begin
		insert into trigger_buffer (
			table_name,
			id_column_name
		) values (
			@table_name,
			@id_column_name
		)

		select @trigger_buffer_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error

		if @myerror = 0 and @myrowcount = 1
		begin
			insert trigger_buffer_id_list (datetime, trigger_buffer_id, id_column_id) select getdate(), @trigger_buffer_id, perimeter_id from inserted
			select @myrowcount=@@rowcount, @myerror=@@error
		end

		if @myerror != 0 or @myrowcount = 0
		begin
			if (@donotmanagetran = 0)
				rollback tran
			raiserror 100001 'insert into trigger_buffer for table perimeter has failed !'
		end

		if (@donotmanagetran = 0)
			commit tran
		end
	else
	begin
		insert trigger_buffer_id_list (datetime, trigger_buffer_id, id_column_id) select getdate(), @trigger_buffer_id, perimeter_id from inserted
		select @myrowcount=@@rowcount, @myerror=@@error

		if @myerror != 0 or @myrowcount =0
		begin
			if (@donotmanagetran = 0)
				rollback tran
			raiserror 100001 'update trigger_buffer for table perimeter has failed !'
		end

		if (@donotmanagetran = 0)
			commit tran
	end
end
go


-- trg_int_estimator
if exists (select 1 from sysobjects where name = 'trg_int_estimator' and type = 'TR')
begin
	print "drop trigger trg_int_estimator"
	drop trigger trg_int_estimator
end
go

print "create trigger trg_int_estimator"
go

create trigger trg_int_estimator on estimator for insert, update
as
begin
	declare @table_name			varchar(32)
	declare @id_column_name			varchar(32)
	declare @myrowcount			int
	declare @donotmanagetran    		int
	declare @myerror            		int
	declare @trigger_buffer_id		int

---------------------------------------------------------------------------------------------------------
----                                                                                                 ----
	select @table_name='estimator', @id_column_name='estimator_id'
----                                                                                                 ----
---------------------------------------------------------------------------------------------------------

	select @donotmanagetran = @@trancount
	if (@donotmanagetran = 0)
		begin tran

	select @trigger_buffer_id = trigger_buffer_id from trigger_buffer where table_name=@table_name

	if @trigger_buffer_id = NULL
	begin
		insert into trigger_buffer (
			table_name,
			id_column_name
		) values (
			@table_name,
			@id_column_name
		)

		select @trigger_buffer_id = @@identity, @myrowcount=@@rowcount, @myerror=@@error

		if @myerror = 0 and @myrowcount = 1
		begin
			insert trigger_buffer_id_list (datetime, trigger_buffer_id, id_column_id) select getdate(), @trigger_buffer_id, estimator_id from inserted
			select @myrowcount=@@rowcount, @myerror=@@error
		end

		if @myerror != 0 or @myrowcount = 0
		begin
			if (@donotmanagetran = 0)
				rollback tran
			raiserror 100001 'insert into trigger_buffer for table estimator has failed !'
		end

		if (@donotmanagetran = 0)
			commit tran
		end
	else
	begin
		insert trigger_buffer_id_list (datetime, trigger_buffer_id, id_column_id) select getdate(), @trigger_buffer_id, estimator_id from inserted
		select @myrowcount=@@rowcount, @myerror=@@error

		if @myerror != 0 or @myrowcount =0
		begin
			if (@donotmanagetran = 0)
				rollback tran
			raiserror 100001 'update trigger_buffer for table estimator has failed !'
		end

		if (@donotmanagetran = 0)
			commit tran
	end
end
go