

/*******************************************************************************************************/
/********************************************Version 2.0************************************************/
/*******************************************************************************************************/



/* update_type
 */

print "Creation de la table update_type"

if exists (select 1 from sysobjects where name = 'update_type' and type = 'U')
begin
    drop table update_type
end
go

create table update_type (
	update_type_id		int				identity     not null,
	update_type_name	varchar(32)		not null
)
lock DataRows
go

grant select,insert,update,delete on update_type to cdv_users
go
grant select on update_type to sel_users
go


insert into update_type (update_type_name) values ('union')
go
insert into update_type (update_type_name) values ('replace')
go



/* request_type
 */

print "Creation de la table request_type"

if exists (select 1 from sysobjects where name = 'request_type' and type = 'U')
begin
    drop table request_type
end
go

create table request_type (
	request_type_id		int				identity     not null,
	request_type_name	varchar(32)		not null
)
lock DataRows
go

grant select,insert,update,delete on request_type to cdv_users
go
grant select on request_type to sel_users
go


insert into request_type (request_type_name) values ('matlab')
go
insert into request_type (request_type_name) values ('sql')
go
--p�rim�tre statique
insert into request_type (request_type_name) values ('none')
go



/*
 * perimeter
 */

print "Creation de la table perimeter"

if exists (select 1 from sysobjects where name = 'perimeter' and type = 'U')
begin
    drop table perimeter
end
go

create table perimeter (
	perimeter_id		int				identity     not null,
	perimeter_name		varchar(32)		not null,
	comment				varchar(255)	not null,
	run_frequency_id	int				not null,
	run_frequency_date	date			null,
	next_run			date			null,
	update_type_id		int				not null,
	request				varchar(255)	not null,
	request_type		int	not null
)
lock DataRows
go

grant select,insert,update,delete on perimeter to cdv_users
go
grant select on perimeter to sel_users
go

-- P�rim�tre par d�faut
insert into perimeter (
	perimeter_name,
	comment, 
	run_frequency_id,
	run_frequency_date,
	update_type_id,
	request,
	request_type
)
values (
	'static',
	'',
	3,
	'1899-01-01',
	1,
	'',
	3
)
go





/*
 * domain_type
 */


print "Creation de la table domain_type"

if exists (select 1 from sysobjects where name = 'domain_type' and type = 'U')
begin
    drop table domain_type
end
go

create table domain_type (
	domain_type_id		int				identity    not null,
	domain_type_name	varchar(255)				not null,
	comment				text						not null
)
lock DataRows
go

grant select,insert,update,delete on domain_type to cdv_users
go
grant select on domain_type to sel_users
go


insert into domain_type (domain_type_name, comment) values ('mono instrument', 'only releated to one instrument')
go
insert into domain_type (domain_type_name, comment) values ('place index', 'describes a place index')
go
--p�rim�tre statique
insert into domain_type (domain_type_name, comment) values ('representative index', 'represents instruments without any domain (orphan instrument)')
go





/*
 * perimeter_domain
*/
print "Creation de la table perimeter_domain"
go

if exists (select 1 from sysobjects where name = 'perimeter_domain' and type = 'U')
begin
    drop table perimeter_domain
end
go

create table perimeter_domain
(
	perimeter_domain_id       		int     identity    not null,
	domain_id     					int          		not null,
	perimeter_id         			int         		not null
)
lock DataRows
go

grant select,insert,update,delete  on perimeter_domain  to cdv_users
go
grant select   on perimeter_domain  to sel_users
go




/*
 * perimeter_job
*/
print "Creation de la table perimeter_job"
go

if exists (select 1 from sysobjects where name = 'perimeter_job' and type = 'U')
begin
    drop table perimeter_job
end
go

create table perimeter_job
(
	perimeter_job_id       		int     identity    not null,
	job_id     					int          		not null,
	meta_job_id        			int         		not null,
	perimeter_id       			int         		not null,
	is_active      				bit    				not null
)
lock DataRows
go

grant select,insert,update,delete  on perimeter_job  to cdv_users
go
grant select   on perimeter_job  to sel_users
go




/*
 * meta_job
 */

print "Creation de la table meta_job"
go

if exists (select 1 from sysobjects where name = 'meta_job' and type = 'U')
begin
   drop table meta_job
end
go

create table meta_job
(
  meta_job_id  				int     identity	not null,
  estimator_id         		int  		        not null,
  perimeter_id         		int  		        not null,
  comment              		varchar(255) 			null,
  run_frequency_id     		int        		   	not null,
  run_window_type_id      	int         			null,
  run_window_width     		int         		  	null,
  run_frequency_date   		date         		 	null,
  check_frequency_id     	int      		    not null,
  check_window_type_id      int         			null,
  check_window_width     	int						null,
  check_frequency_date   	date         		 	null,
  varargin        			varchar(1025)			null
)
lock DataRows
go

grant select,insert,update,delete  on meta_job  to cdv_users
go
grant select   on meta_job  to sel_users
go


/* 
 * Table meta association
 */

print "Creation de la table meta_association"
go

if exists (select 1 from sysobjects where name = 'meta_association' and type = 'U')
begin
    drop table meta_association
end
go

create table meta_association
(
  meta_association_id		int		identity	not null,
  estimator_id				int                 	null,
  domain_type_id			int                 	null,
  request_type_id           int                 	null, 
  request			        text		        	null
)
lock DataRows
go

grant select,insert,update,delete  on meta_association  to cdv_users
go
grant select   on meta_association  to sel_users
go





/*
 * remove_job
*/
print "Creation de la table remove_job"
go

if exists (select 1 from sysobjects where name = 'remove_job' and type = 'U')
begin
    drop table remove_job
end
go

create table remove_job
(
	remove_job_id       		int     identity    not null,
	estimator_id				int          		not null,
	security_id        			int         			null,
	trading_destination_id 		int         			null,
	domain_type_id 				int         		not null
)
lock DataRows
go

grant select,insert,update,delete  on remove_job  to cdv_users
go
grant select   on remove_job  to sel_users
go







/* 
 * Modifications
 */

-- Table association

/*print "Update de la table association"
go

alter table association add is_active					bit		default 1			not null
go*/



-- Cr�ation propre de la table domain
print "Update de la table domain"
go


alter table domain add domain_type_id		int				default 1			not null
go
alter table domain add domain_type_variable	varchar(255)	default ''			not null
go
alter table domain add domain_type_rank		int				default 1			not null
go
alter table domain drop begin_date
go
alter table domain drop end_date
go



--Ajout des adresses mails
alter table estimator  add  owner  	  varchar(255)   default '' null
go



--Patch filling domain_type_id
update domain
set domain_type_id=2 where domain_id in (
	select domain_id from domain_security group by domain_id having count(1) > 1
)


--Cr�ation d'un index sur les noms
--estimator
create unique nonclustered index estimator_name_pk on estimator(estimator_name)
go

--contexts
create unique nonclustered index context_name_pk on context(context_name, estimator_id)
go


--Patch filling domain_type_variable
use temp_works
go


if exists (select 1 from sysobjects where name = 'temp_domains' and type = 'U')
begin
    drop table temp_domains
end
go
create table temp_domains ( di int, dv varchar(255))
go

insert into temp_domains 
select domain_id di, convert(varchar(255), security_id) + ',null' dv from quant_homolo..domain_security group by domain_id having count(1) = 1


-- update job_status
alter table job_status add
session_num		int	default 0 not null
go



/*****TODO*****/
use quant
go

update domain set domain_type_variable=dv
from temp_works..temp_domains
where domain_type_id=1 and domain_id=di





