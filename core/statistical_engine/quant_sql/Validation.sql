--UPDATE de estimator_runs � la main
UPDATE estimator_runs SET is_valid=1 WHERE run_id=27 OR run_id=626 OR run_id=1359 OR run_id=1633 OR run_id=2630 OR run_id=2631 
OR run_id=2632 OR run_id=2633 OR run_id=2634 OR run_id=2635 OR run_id=2636 OR run_id=2637 OR run_id=2638 OR run_id=2639 OR run_id=5177 



--Synchronisation de last_run par rapport � estimator_runs
UPDATE
	last_run
SET
	last_run.last_valid_run_id= (
	SELECT
		e.run_id
	FROM
		estimator_runs e
	WHERE
		e.is_valid=1
		AND e.job_id=last_run.job_id
		AND e.context_id=last_run.context_id
	)
WHERE EXISTS (
	SELECT
		e.run_id
	FROM
		estimator_runs e
	WHERE
		e.is_valid=1
		AND e.job_id=last_run.job_id
		AND e.context_id=last_run.context_id
	)

