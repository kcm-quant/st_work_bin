
use quant
go


if exists (select 1 from sysobjects where name = 'param' and type = 'V')
begin
    print "drop view param"
    drop view param
end
go

print "Creation de la view param"
go


create view param
as
select r.estimator_id, r.context_id, a.security_id, a.trading_destination_id, d.x_value, d.parameter_name, v.value, a.rank
  from association a, param_desc d, param_value v, estimator_runs r
 where 
       a.job_id = r.job_id
   and v.run_id = r.run_id
   and d.parameter_id = v.parameter_id
   and r.is_valid = 1 -- to be uncommented for prod to hide unvalidated parameters
go
