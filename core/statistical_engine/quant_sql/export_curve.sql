/*
exec get_curve_by_sec 3, 110, null go

exec get_curve_by_job_generic 2, 304, 1000000 go

select * from temp_works..volume_curve order by curve_id, end_time

select
	i.place_id,
	i.indice_id,
	d.domain_name,
	j.job_id,
	hpt.gmt_offset_hours
from 
	market..indice i,
	quant..job j,
	quant..perimeter p,
	quant..domain d,
	quant..perimeter_domain pd,
	repository..historic_place_timezone hpt
where 
	p.perimeter_name = 'lmi mtd'
	and p.perimeter_id = pd.perimeter_id
	and pd.domain_id = j.domain_id
	and d.domain_id = j.domain_id
	and d.domain_name = 'lmi mtd/' + i.name
	and hpt.place_id = i.place_id
	and gmt_end_date = null
*/

use temp_works
go

drop table volume_curve
go

create table volume_curve(
	curve_id   numeric(10,0)  not null,
	phase   char(2)  not null,
	begin_time   datetime  not null,
	end_time   datetime  not null,
	volume   int  not null,
	adjusted_volume   int  not null
)
go

use quant
go

if exists (select 1 from sysobjects where name = 'get_curve_by_job_generic' and type = 'P')
begin
	print "drop procedure get_curve_by_job_generic"
	drop procedure get_curve_by_job_generic
end
go

CREATE PROCEDURE get_curve_by_job_generic
(
@curve_id INT,
@def_job_id INT,
@udv float
)
AS
BEGIN	
declare @estimator_name varchar(32),
		@context_name varchar(32),
		@is_valid INT,
		@oa_time time,   -- opening auction time
		@eofs_time time, -- end of first slice time
		@ia_time time,   -- intraday auction time
		@ca_time time,   -- closing auction time
		@context_id int
		
select 	@estimator_name = "Volume curve"
select  @context_name   = "March special ny"
select  @is_valid        = 1
-- pour test

	-- calcul de context_id
select @context_id = (
	SELECT
	    c.context_id
	FROM
		quant..context c
	WHERE
		c.context_name LIKE @context_name
		)
		
	-- gestion de l'heure de fixing d'ouverture
select @oa_time = dateadd (minute, round((
	SELECT
	    pv.value
	FROM
		quant..param_value pv,
		quant..param_desc p,
		quant..estimator_runs er
	WHERE
		er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		AND p.parameter_name = 'auction_open_closing'
	)*60*24-5, 1), '00:00' )
	
		-- gestion de l'heure de fin du premier intervalle du continu
select @eofs_time = dateadd (minute, round((
	SELECT
	    pv.value
	FROM
		quant..param_value pv,
		quant..param_desc p,
		quant..estimator_runs er
	WHERE
		er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		AND p.parameter_name = 'end_of_first_slice'
	)*60*24, 1), '00:00' )
	
		-- gestion de l'heure de fixing intraday
select @ia_time = dateadd (minute, round((
	SELECT
	    pv.value
	FROM
		quant..param_value pv,
		quant..param_desc p,
		quant..estimator_runs er
	WHERE
		er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		AND p.parameter_name = 'auction_mid_closing'
	)*60*24, 1), '00:00' )
	
	-- select heure du fixing de fermeture
select @ca_time = dateadd (minute, round((
	SELECT
	    pv.value
	FROM
		quant..param_value pv,
		quant..param_desc p,
		quant..estimator_runs er
	WHERE
		p.parameter_name = 'auction_close_closing'
		-- job id dans la table des associations
	    AND er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
	)*60*24+5, 1), '00:00' )

	

	/* select principal */
	INSERT INTO temp_works..volume_curve (curve_id, end_time, begin_time, phase, volume, adjusted_volume)
	(
	SELECT -- select closing auction
		@curve_id curve_id,
	    @ca_time end_time,
	    @ca_time begin_time, -- special: c'est le m�me
	    'CA' phase,
	    convert( INT, pv.value * @udv) USV,
	    convert( INT, pv.value * @udv) adj_USV
	FROM
	    param_value pv,
		param_desc p,
		estimator_runs er
	WHERE
		p.x_value = 105.5
		AND er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		
	UNION 
	/*
	SELECT -- select intra day auction
	    @curve_id curve_id,
	    @ia_time end_time,
	    'IA' phase,
	    convert( INT, pv.value * @udv) USV,
	    convert( INT, pv.value * @udv) adj_USV
	FROM
	    param_value pv,
		param_desc p,
		estimator_runs er
	WHERE
		p.x_value = 52.5
		AND er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		
	UNION 
	*/
	SELECT -- select opening auction
	    @curve_id curve_id,
	    @oa_time end_time,
	    @oa_time begin_time,    
	    'OA' phase,
	    convert( INT, pv.value * @udv) USV,
	    convert( INT, pv.value * @udv) adj_USV
	FROM
	    param_value pv,
		param_desc p,
		estimator_runs er
	WHERE
		p.x_value =0 
		AND er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		
	UNION 
	
	SELECT -- select continuous auction
	    @curve_id curve_id,
	    dateadd(minute, 5*(p.x_value-1), @eofs_time) end_time,
	    dateadd(minute, 5*(p.x_value-2), @eofs_time) begin_time,
	    'C ' phase,
	    convert( INT, pv.value * @udv) USV,
	    convert( INT, pv.value * @udv) adj_USV
	FROM
	    param_value pv,
		param_desc p,
		estimator_runs er
	WHERE
		p.x_value > 0 AND
		convert( FLOAT, convert(INT, p.x_value)) = p.x_value 
		AND er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		
	)
	SELECT * FROM temp_works..volume_curve ORDER BY curve_id, end_time
END
GO

grant execute on get_curve_by_job_generic to public
go


/*
GET CURVE BY SEC
*/
/* 
tests:
exec get_curve_by_job 304 go
exec get_curve_by_job 324 go
exec get_curve_by_job 302 go
exec get_curve_by_job 305 go

exec get_curve_by_sec 110, 4 go
exec get_curve_by_sec 6069, 8 go
exec get_curve_by_sec 387, 6 go
*/

if exists (select 1 from sysobjects where name = 'get_curve_by_sec' and type = 'P')
begin
	print "drop procedure get_curve_by_sec"
	drop procedure get_curve_by_sec
end
go

CREATE PROCEDURE get_curve_by_sec
(
@curve_id INT,
@security_id INT,
@trading_destination_id INT
)
AS
BEGIN	
declare @estimator_name varchar(32),
		@context_name varchar(32),
		@is_valid INT,
		@udv float,
		@oa_time time,   -- opening auction time
		@eofs_time time, -- end of first slice time
		@ia_time time,   -- intraday auction time
		@ca_time time,   -- closing auction time
		@def_job_id int,
		@context_id int
 /*,
		-- pour test
		@security_id INT,
		@trading_destination_id INT */
		
select 	@estimator_name = "Volume curve"
select  @context_name   = "March special ny"
select  @is_valid        = 1
/* -- pour test
select  @security_id    = 110
select  @trading_destination_id = 4 */

	-- calcul du job id
select @def_job_id = (
	SELECT
	    a.job_id
	FROM
		quant..association a,
		quant..estimator e
	WHERE
		a.security_id=@security_id
		AND a.trading_destination_id=@trading_destination_id
		AND e.estimator_name LIKE @estimator_name
		AND a.estimator_id=e.estimator_id
	)

	-- calcul de l'UDV
select @udv = (	
	SELECT
	   exp( avg( log( sum(td.volume))) )
	FROM
	   tick_db..trading_daily td
	WHERE
		td.security_id = @security_id AND
		(td.trading_destination_id = @trading_destination_id OR @trading_destination_id = null)
	GROUP BY date
)

exec get_curve_by_job_generic @curve_id, @def_job_id, @udv
		
		
END
GO

grant execute on get_curve_by_sec to public
go


