/* 
tests:
exec get_curve_by_job 304 go
exec get_curve_by_job 324 go
exec get_curve_by_job 302 go
exec get_curve_by_job 305 go

exec get_curve_by_sec 110, 4 go
exec get_curve_by_sec 6069, 8 go
exec get_curve_by_sec 387, 6 go
*/

if exists (select 1 from sysobjects where name = 'get_curve_by_sec' and type = 'P')
begin
	print "drop procedure get_curve_by_sec"
	drop procedure get_curve_by_sec
end
go

CREATE PROCEDURE get_curve_by_sec
(
@security_id INT,
@trading_destination_id INT
)
AS
BEGIN	
declare @estimator_name varchar(32),
		@context_name varchar(32),
		@is_valid INT,
		@udv float,
		@oa_time time,   -- opening auction time
		@eofs_time time, -- end of first slice time
		@ia_time time,   -- intraday auction time
		@ca_time time,   -- closing auction time
		@def_job_id int,
		@context_id int
 /*,
		-- pour test
		@security_id INT,
		@trading_destination_id INT */
		
select 	@estimator_name = "Volume curve"
select  @context_name   = "Usual day"
select  @is_valid        = 1
/* -- pour test
select  @security_id    = 110
select  @trading_destination_id = 4 */

	-- calcul du job id
select @def_job_id = (
	SELECT
	    a.job_id
	FROM
		quant_homolo..association a,
		quant_homolo..estimator e
	WHERE
		a.security_id=@security_id
		AND (a.trading_destination_id=@trading_destination_id OR @trading_destination_id=NULL)
		AND e.estimator_name LIKE @estimator_name
		AND a.estimator_id=e.estimator_id
	)
	-- calcul de context_id
select @context_id = (
	SELECT
	    c.context_id
	FROM
		quant_homolo..context c
	WHERE
		c.context_name LIKE @context_name
		)
		
	-- calcul de l'UDV
select @udv = (	
	SELECT
	   exp( avg( log( td.volume)) )
	FROM
	   tick_db..trading_daily td
	WHERE
		td.security_id = @security_id AND
		td.trading_destination_id = @trading_destination_id
)

	-- gestion de l'heure de fixing d'ouverture
select @oa_time = dateadd (minute, round((
	SELECT
	    pv.value
	FROM
		quant_homolo..param_value pv,
		quant_homolo..param_desc p,
		quant_homolo..estimator_runs er
	WHERE
		er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		AND p.parameter_name = 'auction_open_closing'
	)*60*24, 1), '00:00' )
	
		-- gestion de l'heure de fin du premier intervalle du continu
select @eofs_time = dateadd (minute, round((
	SELECT
	    pv.value
	FROM
		quant_homolo..param_value pv,
		quant_homolo..param_desc p,
		quant_homolo..estimator_runs er
	WHERE
		er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		AND p.parameter_name = 'end_of_first_slice'
	)*60*24, 1), '00:00' )
	
		-- gestion de l'heure de fixing intraday
select @ia_time = dateadd (minute, round((
	SELECT
	    pv.value
	FROM
		quant_homolo..param_value pv,
		quant_homolo..param_desc p,
		quant_homolo..estimator_runs er
	WHERE
		er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		AND p.parameter_name = 'auction_mid_closing'
	)*60*24, 1), '00:00' )
	
	-- select heure du fixing de fermeture
select @ca_time = dateadd (minute, round((
	SELECT
	    pv.value
	FROM
		quant_homolo..param_value pv,
		quant_homolo..param_desc p,
		quant_homolo..estimator_runs er
	WHERE
		p.parameter_name = 'auction_close_closing'
		-- job id dans la table des associations
	    AND er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
	)*60*24, 1), '00:00' )

	-- select habituelle
	SELECT
	    @ca_time end_time,
	    'CA' phase,
	    convert( INT, pv.value * @udv) USV
	FROM
	    param_value pv,
		param_desc p,
		estimator_runs er
	WHERE
		p.x_value = 105.5
		AND er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		
	UNION 
	
	SELECT
	    @ia_time end_time,
	    'IA' phase,
	    convert( INT, pv.value * @udv) USV
	FROM
	    param_value pv,
		param_desc p,
		estimator_runs er
	WHERE
		p.x_value = 52.5
		AND er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		
	UNION 
	
	SELECT
	    @oa_time end_time,
	    'OA' phase,
	    convert( INT, pv.value * @udv) USV
	FROM
	    param_value pv,
		param_desc p,
		estimator_runs er
	WHERE
		p.x_value =0 
		AND er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		
	UNION 
	
	SELECT
	    dateadd(minute, 5*(p.x_value-1), @eofs_time) end_time,
	    'C ' phase,
	    convert( INT, pv.value * @udv) USV
	FROM
	    param_value pv,
		param_desc p,
		estimator_runs er
	WHERE
		p.x_value > 0 AND
		convert( FLOAT, convert(INT, p.x_value)) = p.x_value 
		AND er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		
	ORDER BY end_time
		
		
END
GO

grant execute on get_curve_by_sec to public
go

-- GET JOB CURVE

if exists (select 1 from sysobjects where name = 'get_curve_by_job' and type = 'P')
begin
	print "drop procedure get_curve_by_job"
	drop procedure get_curve_by_job
end
go

CREATE PROCEDURE get_curve_by_job
(
@def_job_id INT
)
AS
BEGIN	
declare @estimator_name varchar(32),
		@context_name varchar(32),
		@is_valid INT,
		@udv float,
		@oa_time time,   -- opening auction time
		@eofs_time time, -- end of first slice time
		@ia_time time,   -- intraday auction time
		@ca_time time,   -- closing auction time
		@context_id int
		
select 	@estimator_name = "Volume curve"
select  @context_name   = "Usual day"
select  @is_valid        = 1
-- pour test

	-- calcul de context_id
select @context_id = (
	SELECT
	    c.context_id
	FROM
		quant_homolo..context c
	WHERE
		c.context_name LIKE @context_name
		)
		
	-- calcul de l'UDV
select @udv = 100000000
	
	-- gestion de l'heure de fixing d'ouverture
select @oa_time = dateadd (minute, round((
	SELECT
	    pv.value
	FROM
		quant_homolo..param_value pv,
		quant_homolo..param_desc p,
		quant_homolo..estimator_runs er
	WHERE
		er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		AND p.parameter_name = 'auction_open_closing'
	)*60*24, 1), '00:00' )
	
		-- gestion de l'heure de fin du premier intervalle du continu
select @eofs_time = dateadd (minute, round((
	SELECT
	    pv.value
	FROM
		quant_homolo..param_value pv,
		quant_homolo..param_desc p,
		quant_homolo..estimator_runs er
	WHERE
		er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		AND p.parameter_name = 'end_of_first_slice'
	)*60*24, 1), '00:00' )
	
		-- gestion de l'heure de fixing intraday
select @ia_time = dateadd (minute, round((
	SELECT
	    pv.value
	FROM
		quant_homolo..param_value pv,
		quant_homolo..param_desc p,
		quant_homolo..estimator_runs er
	WHERE
		er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		AND p.parameter_name = 'auction_mid_closing'
	)*60*24, 1), '00:00' )
	
	-- select heure du fixing de fermeture
select @ca_time = dateadd (minute, round((
	SELECT
	    pv.value
	FROM
		quant_homolo..param_value pv,
		quant_homolo..param_desc p,
		quant_homolo..estimator_runs er
	WHERE
		p.parameter_name = 'auction_close_closing'
		-- job id dans la table des associations
	    AND er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
	)*60*24, 1), '00:00' )

	-- select habituelle
	SELECT
	    @ca_time end_time,
	    'CA' phase,
	    convert( INT, pv.value * @udv) USV
	FROM
	    param_value pv,
		param_desc p,
		estimator_runs er
	WHERE
		p.x_value = 105.5
		AND er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		
	UNION 
	
	SELECT
	    @ia_time end_time,
	    'IA' phase,
	    convert( INT, pv.value * @udv) USV
	FROM
	    param_value pv,
		param_desc p,
		estimator_runs er
	WHERE
		p.x_value = 52.5
		AND er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		
	UNION 
	
	SELECT
	    @oa_time end_time,
	    'OA' phase,
	    convert( INT, pv.value * @udv) USV
	FROM
	    param_value pv,
		param_desc p,
		estimator_runs er
	WHERE
		p.x_value =0 
		AND er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		
	UNION 
	
	SELECT
	    dateadd(minute, 5*(p.x_value-1), @eofs_time) end_time,
	    'C ' phase,
	    convert( INT, pv.value * @udv) USV
	FROM
	    param_value pv,
		param_desc p,
		estimator_runs er
	WHERE
		p.x_value > 0 AND
		convert( FLOAT, convert(INT, p.x_value)) = p.x_value 
		AND er.context_id=@context_id
		AND er.job_id=@def_job_id
		AND (er.is_valid=@is_valid OR @is_valid=NULL)
		AND pv.parameter_id=p.parameter_id
		AND pv.run_id=er.run_id
		
	ORDER BY end_time
		
END
GO

grant execute on get_curve_by_job to public
go
