
select o.name
from syscolumns col, sysobjects o
where col.name = 'estimator_id'
and o.id = col.id

set identity_update estimator off

update quant..param_desc set estimator_id = 28 where estimator_id = 27
update quant..date_remove set estimator_id = 28 where estimator_id = 27
update quant..estimator set estimator_id = 28 where estimator_id = 27
update quant..context set estimator_id = 28 where estimator_id = 27
update quant..estimator_runs set estimator_id = 28 where estimator_id = 27
update quant..valid_runs_corrected set estimator_id = 28 where estimator_id = 27
update quant..valid_runs set estimator_id = 28 where estimator_id = 27
update quant..association set estimator_id = 28 where estimator_id = 27
update quant..histo_association set estimator_id = 28 where estimator_id = 27
update quant..job set estimator_id = 28 where estimator_id = 27
update quant..histo_estimator_runs set estimator_id = 28 where estimator_id = 27
update quant..context_ranking set estimator_id = 28 where estimator_id = 27
update quant..event_action set estimator_id = 28 where estimator_id = 27
update quant..meta_job set estimator_id = 28 where estimator_id = 27
update quant..meta_association set estimator_id = 28 where estimator_id = 27
update quant..remove_job set estimator_id = 28 where estimator_id = 27
update quant..pd set estimator_id = 28 where estimator_id = 27
update quant..histo_context_ranking set estimator_id = 28 where estimator_id = 27
update quant..histo_event_action set estimator_id = 28 where estimator_id = 27
update quant..learningmodel set estimator_id = 28 where estimator_id = 27
update quant..pre_post_validation_book set estimator_id = 28 where estimator_id = 27
update quant..last_association set estimator_id = 28 where estimator_id = 27
update quant..next_association set estimator_id = 28 where estimator_id = 27
update quant..estimator_process set estimator_id = 28 where estimator_id = 27