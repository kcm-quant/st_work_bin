function se_build_domain(varargin)
% SE_BUILD_DOMAIN - function that create an XML domain list from CHEUVREUX
% PERIMETER
% 
% se_build_domain('sec_id', [2, 8, 110], 'path', 'c:\st_work\bin\core\statistical_engine\domain.xml')
%
% author   : 'edarchimbaud@cheuvreux.com'
% reviewer : ''
% version  : '1'
% date     : '07/11/2008'
%
% See also se_load, se_xml

opt=options({'sec_id', [], 'path', 'c:\domain.xml', 'name', '', 'group', 'A'}, varargin);

% p = get_repository('perch', 'treshold_value', opt.get('thres'));
p = opt.get('sec_id');

txt={};
txt{end+1}=sprintf('<domains>\n');
for i=1:length(p)
    s = get_repository( 'any-to-sec-td', 'security_id', p(i), 'trading-destinations', { 'MAIN' });
    for j=1:length(s.trading_destination_id)
        txt{end+1} = sprintf(['\t<domain name="%s/%s" group="A">\n' ...
            '\t\t<txt></txt>\n' ...
            '\t\t<security id="%d" td="%d" />\n' ...
            '\t</domain>\n'], opt.get('name'), s.security_key, s.security_id, s.trading_destination_id(j));
    end
end
txt{end+1}='</domains>';
txt = [txt{:}];


fid=fopen(opt.get('path'), 'w');
fwrite(fid, txt);
fclose(fid);

end