function varargout = se_check(mode, varargin)
% SE_CHECK - do the evaluation of the run
%
% se_check('distrib_local', 'func_name', 'se_check_volume_threshold', 'run_id', 1)
% se_check('local', 'run_id', {2}, 'job_name', 'test', 'check-date', ...
% datestr(today-14, 'dd/mm/yyyy'), 'debug', 0)
%
% See also se_run
%
% Author   'Edouard d'Archimbaud'
% Version  '1.0'
% Reviewer ''
% Date     '22/10/2008'

opt = options({ ...
    'func_name', '', ...
    'run_id', 0, ...
    'job_manager', '', ...
    'check-date', '', ...
    'job_name', 'default', ...
    'today', today, ...
    'session-num', 0, ...
    'debug', false}, ...
    varargin);

varargout{1} = nan(length(opt.get('run_id')),1);

global st_version;


switch(mode)
    
    case 'local'
        % Fonction de base calculant l'?valuation d'un check
        %D?but de la gestion des jobs multiples
        if length(opt.get('run_id')) > 1
            run_id = opt.get('run_id');
            for i=1:length(run_id)
                temp = se_check('local', 'run_id', run_id(i), 'job_name', opt.get('job_name'), 'check-date', opt.get('check-date'), ...
                    'session-num', opt.get('session-num'),'debug',opt.get('debug'));
                if nargout ~= 0
                    varargout{1}(i) = temp;
                end
            end
            return;
        end
        
        run_id = opt.get('run_id');
        run_id = cell2mat(run_id);
        
        session_num = opt.get('session-num');
        
        try
            job_status_id = zeros(length(run_id), 1);
            if ~opt.get('debug')
                
                for i=1:length(run_id)
                    job_status_id(i) = se_db('add_job_status', run_id(i), 'running', 0, ...
                        datestr(floor(now)), datestr(now, 'HH:MM:SS'), opt.get('job_name'), opt.get('session-num'));
                    dir_path = fullfile(getenv('st_repository'), 'log', 'check', sprintf('run_id_%d', run_id(i)));
                end
                
                if isempty(dir(dir_path))
                    mkdir(dir_path);
                end
                log_path = fullfile(dir_path, sprintf('%s.txt', opt.get('job_name')));
                st_log('$out$', log_path);
            end
            
            estimator_run = cell(length(run_id), 1);
            for i=1:length(run_id)
                estimator_run{i} = se_db('get_estimator_run', 'run_id', run_id(i));
            end
            % On peut se contenter d'utiliser le premier run_id pour
            % extraire les  donn?es
            job = se_db('get_job', 'job_id', estimator_run{1}.job_id);
            estimator = se_db('get_estimator', 'estimator_id', job.estimator_id);
            domain_security = se_db('get_domain_security', 'domain_id', job.domain_id);
            window_type = se_db('get_window_type', 'window_type_id', job.check_window_type_id);
            param_desc = se_db('get_param_desc', 'estimator_id', job.estimator_id);
            context = cell(length(run_id), 1);
            param_value = cell(length(run_id), 1);
            for i=1:length(run_id)
                context{i} = se_db('get_context', 'context_id', estimator_run{i}.context_id);
                param_value{i} = se_db('get_param_value', 'run_id', run_id(i));
            end
            
            if job.check_frequency_id == 4 % asOfDate
                check_date = job.check_frequency_date;
            else
                % ce dervait ?tre le dernier jour ouvr?
                w = weekday(datenum(opt.get('check-date'), 'dd/mm/yyyy'));
                check_date = datestr(datenum(opt.get('check-date'), 'dd/mm/yyyy')-(w==1)*2-(w==7), 'dd/mm/yyyy');
            end
            % Create an st_data that contains the list of the parameters returned by se_run
            data_value = [];
            for i=1:length(run_id)
                if isempty( param_value{i})
                    error('se_check:local', 'SE_JOB_PARAMS: param_value is empty');
                end
                data_value = [data_value [param_value{i}.value]']; %#ok<AGROW>
            end
            data_colnames = {};
            for i=1:length(run_id)
                data_colnames{end+1} = context{i}.context_name; %#ok<AGROW>
            end
            data_rownames = {param_desc(:).parameter_name}';  
            a=[param_value{1}.parameter_id];
            b = [param_desc(:).parameter_id];
            [c, ia, ib] = intersect(a, b);
            data = st_data( 'init', 'title', '', 'value', data_value(ia,:), ...
                'date', (1:size(data_value, 1))', 'colnames', data_colnames, ...
                'rownames', data_rownames(ib)');
            % Coeur du calcul: ?valuation de la fonction
            check_quality = feval(estimator.matlab_check, [domain_security.security_id], [domain_security.trading_destination_id], ...
                window_type.window_type_name, job.check_window_width, check_date, data);
            
            if nargout ~= 0
                varargout{1} = check_quality;
            end
            
            check_quality(isnan(check_quality)) = 0;
            
            if ~opt.get('debug')
                for i=1:length(run_id)
                    se_db('add_check_quality', job.job_id, run_id(i), check_quality(i), ...
                        datestr(datenum(check_date, 'dd/mm/yyyy'), 'yyyymmdd'), ...
                        datestr(datenum(check_date, 'dd/mm/yyyy'), 'yyyymmdd') ,session_num ,'');
                    se_db('update_job_status', 'ended', datestr(floor(now)), datestr(now, 'HH:MM:SS'), job_status_id(i));
                    se_db('update_check_date', job.job_id, job.check_frequency_date, job.check_frequency_id, check_date);
                end
                st_log('$close$');
                if ~st_version.se_spec.keep_all_log
                    delete(log_path);
                    if length(dir(dir_path)) == 2
                        rmdir(dir_path);
                    end
                end
            end
            
        catch e
            error_message = {};
            error_message{end+1} = ['MESSAGE: ' e.message];
            for i=1:length(e.stack)
                error_message{end+1} = [' [STACK' sprintf('%d', i) ': ' e.stack(i).name ':' sprintf('%d', e.stack(i).line) ']' ];
            end
            if ~opt.get('debug')
                for i=1:length(run_id)
                    se_db('add_error', run_id(i), 1, 0, datestr(floor(now), 'yyyymmdd'), datestr(now, 'HH:MM:SS'), [error_message{:}]);
                    se_db('update_job_status', 'cancelled', datestr(floor(now)), datestr(now, 'HH:MM:SS'), job_status_id(i));
                end
            end
            st_log('ERROR:%s\n', e.message);
            if ~opt.get('debug')
                st_log('$close$'); % fermeture du log en cas d'erreur
            end
            if opt.get('debug')
                rethrow(e); % pour d?truire le job qui a plant?
            end
        end

    
    case 'distrib'
        % Fonction lan?ant un run distribu? en local
        if ~isempty(opt.get('job_manager'))
            % On cr?e un nom pour le job
            job_name = hash(now,'md5');
            jm = opt.get('job_manager');
            job = createJob(jm, 'name', job_name);
            
            if ~strcmpi(jm.Type, 'local')
                set(job, 'RestartWorker', true);
                set(job, 'Timeout', 60 * 60 * 24); % Time out ? 24h pour supprimer les jobs trop longs
            end
            
            task_n = createTask(job, str2func('se_check'), 0, {'local', 'run_id', ...
                opt.get('run_id'), 'job_name', job_name, 'check-date', opt.get('check-date'), ...
                'session-num', opt.get('session-num')});
            
            if ~strcmpi(jm.Type, 'local')
                set(task_n,'MaximumNumberOfRetries',0)
            end
                 
            submit(job);
            
            % pour se_last_job
            varargout = {job};
        else
                error('Error in distribution: no job manager');
        end        
            
        
    otherwise
        error(sprintf('se_run: mode <%s> unknown', mode));
end        

