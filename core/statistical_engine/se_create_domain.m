function is_succes = se_create_domain(perimeter_id, domain_names, domain_sec_ids, domain_type_variable, comments, domain_type, domain_type_rank)
% SE_CREATE_DOMAIN - fill the domain and domain_security tables. Called by perim_...
% functions
%
% Author: mlasnier@keplercheuvreux.com
% Date: 12/03/2014

global st_version;
quant = st_version.bases.quant;

domain_type_id = cell2mat(exec_sql('QUANT' ,[ ...
    'SELECT domain_type_id ' ...
    ' FROM ',quant,'..domain_type ' ...
    ' WHERE lower(domain_type_name)=''',lower(domain_type),'''']));

perim_update_type = cell2mat(exec_sql('QUANT' ,[ ...
    'SELECT u.update_type_name ' ...
    ' FROM ',quant,'..perimeter p , ',quant,'..update_type u ' ...
    ' WHERE p.perimeter_id=',num2str(perimeter_id),...
    ' AND u.update_type_id=p.update_type_id']));
% 
% % D�sactivation de tous les domaines des p�rim�tres perim_update_type = 'replace'
% if ~isempty(domain_sec_ids) && strcmpi(perim_update_type, 'replace')
%     exec_sql('QUANT',['UPDATE ',quant,'..perimeter_domain set is_active = 0 WHERE perimeter_id=',num2str(perimeter_id)]);
% end

% Cr�ation de la table temporaire contenant les param�tres de cr�ation des domaines
exec_sql('QUANT',['IF EXISTS (SELECT * FROM sysobjects',...
    ' WHERE id = object_id(N''',quant,'.[dbo].[domain_to_be_created]'')',...
    ' AND OBJECTPROPERTY(id, N''IsUserTable'') = 1)',...
    ' drop TABLE ',quant,'.[dbo].[domain_to_be_created]',...
    ' select 1']);
exec_sql('QUANT',['CREATE TABLE ',quant,'.[dbo].[domain_to_be_created] (',...
    'id int not null,',...
    'domain_name varchar(32) not null,',...
    'domain_type_variable varchar(255) not null,',...
    'domain_type_id int not null,',...
    'domain_type_rank int not null,',...
    'security_id int not null,',...
    'trading_destination_id int null,',...
    'comment varchar(255) null)']);

% Insertion des lignes dans la table [domain_to_be_created]
for s = 1:length(domain_sec_ids)
    query = sprintf(['insert into ',quant,'.[dbo].[domain_to_be_created]',...
        ' ([id],[domain_name],[domain_type_variable],[domain_type_id],[domain_type_rank],',...
        '[security_id],[trading_destination_id],[comment])',...
        ' VALUES (%d,''%s'',''%s'',%d,%d,%%d,%%d,''%s'')'],...
        s,domain_names{s},strrep(domain_type_variable{s},'''',''''''),domain_type_id,domain_type_rank,comments{s});
    query = sprintf([query,'\n'],[domain_sec_ids{s}.security_id;domain_sec_ids{s}.trading_destination_id]);
    % NaN -> NULL
    query = strrep(query,',NaN,',',NULL,');
    exec_sql('QUANT',query);
end

status = cell2mat(exec_sql('QUANT',['DECLARE @RC int ',...
    'EXECUTE @RC = ',quant,'.[dbo].[create_domain_procedure] ',num2str(perimeter_id),',',...
    '''',perim_update_type,''',''',quant,'.[dbo].domain_to_be_created''',...
    ' select @RC']));
is_succes = status == 0;

% % cr�ation des domaines
% for s=1:length(domain_sec_ids)
%     add_domain(domain_names{s}, comments{s}, perimeter_id, domain_type_id, domain_type_variable{s}, domain_type_rank, domain_sec_ids{s}, perim_update_type);
% end




    function add_domain(domain_name, comment, perimeter_id, domain_type_id, domain_type_variable, domain_type_rank, domain_sec, perim_update_type)
        domain_name = domain_name(1:min([32,length(domain_name)]));
        
        domain_id = exist_domain(domain_type_id, domain_type_variable, domain_type_rank, domain_sec);
        
        if domain_id
            pdom_exist = ~isempty(exec_sql('QUANT',['SELECT domain_id',...
                ' from ',quant,'..perimeter_domain WHERE domain_id = ',num2str(domain_id),...
                ' and perimeter_id = ',num2str(perimeter_id)]));
            if  pdom_exist
                if strcmpi(perim_update_type, 'replace')
                    exec_sql('QUANT',['UPDATE ',quant,'..perimeter_domain set is_active =1 WHERE perimeter_id=',num2str(perimeter_id),...
                        ' AND domain_id =',num2str(domain_id)]);
                else
                    st_log('domain %d already exists\n', domain_id);
                end
            else
                st_log('domain %d already exists , updating the "perimeter_domain" table.. \n', domain_id);
                exec_sql('QUANT',['INSERT INTO ',quant,'..perimeter_domain (perimeter_id, domain_id, is_active)',...
                    ' VALUES (',num2str(perimeter_id),',',num2str(domain_id),', 1)']);
            end
            
        else
            exec_sql('QUANT', sprintf( [ ...
                'INSERT INTO ',quant,'..domain ' ...
                '    (domain_name, comment, domain_type_id, domain_type_variable, domain_type_rank) ' ...
                'VALUES (''%s'', ''%s'', %d, ''%s'', %d)'], ...
                domain_name, comment, domain_type_id, strrep(domain_type_variable, '''', ''''''), domain_type_rank));
            % R�cup�ration du domain_id
            domain_id = cell2mat(exec_sql('QUANT', sprintf( [ ...
                'SELECT do.domain_id FROM ',quant,'..domain do ' ...
                'WHERE do.domain_name=''%s'' AND do.comment=''%s'' AND do.domain_type_id=%d AND ' ...
                '    do.domain_type_variable=''%s'' AND do.domain_type_rank=%d ' ...
                '    AND 0 = (SELECT COUNT(1) FROM ',quant,'..domain_security ds WHERE do.domain_id = ds.domain_id)'], ...
                domain_name, comment, domain_type_id, strrep(domain_type_variable, '''', ''''''), domain_type_rank)));
            if numel(domain_id)~=1
                dom_str = sprintf('%d,', domain_id);
                error('se_create_domain:multiple_domains', 'multiple or partial domains: %s', dom_str(1:end-1));
                %                 delete from domain where domain_id in (949,950,951)
                %                 delete from domain_security where domain_id in (949,950,951)
            end
            for i=1:length(domain_sec)
                td_str = strrep(num2str(domain_sec(i).trading_destination_id),'NaN','null');
                exec_sql('QUANT', sprintf([ ...
                    'INSERT INTO ',quant,'..domain_security (domain_id, security_id, trading_destination_id) ' ...
                    'VALUES (%d, %d, %s)'],domain_id, domain_sec(i).security_id, td_str));
            end
            exec_sql('QUANT', sprintf(['INSERT INTO ',quant,'..perimeter_domain (perimeter_id, domain_id, is_active) VALUES (%d, %d , 1)'], perimeter_id, domain_id));
        end
        
    end


    function out = exist_domain(domain_type_id, domain_type_variable, domain_type_rank, domain_sec)
        
        % G�n�ration des clef d'unicit� hexad�cimal :
        
        
        condition1 = '';
        for i=1:length(domain_sec)
            condition1 = [condition1 ...
                sprintf('(security_id %s AND trading_destination_id %s) OR ', ...
                sql_wash_nan(domain_sec(i).security_id), sql_wash_nan(domain_sec(i).trading_destination_id))]; %#ok<AGROW>
        end
        condition1(end-3:end)='';
        % J'utilise 0=1 car je ne sais pas exprimer la condition false
        rslt = exec_sql('QUANT', sprintf([ ...
            'SELECT DISTINCT d.domain_id ' ...
            'FROM ',quant,'..domain d, ',quant,'..domain_security ds ' ...
            'WHERE (' condition1 ') AND ' ...
            '    d.domain_id=ds.domain_id AND ' ...
            '    d.domain_type_id=%d AND ' ...
            '    d.domain_type_variable=''%s'' AND ' ...
            '    d.domain_type_rank=%d ' ...
            ' GROUP BY d.domain_id,ds.domain_id',...
            ' HAVING COUNT(*)=%d AND COUNT(*)=' ...
            '(SELECT COUNT(*) FROM ',quant,'..domain_security dss WHERE d.domain_id=ds.domain_id AND dss.domain_id=ds.domain_id)'], ...
            domain_type_id, strrep(domain_type_variable, '''', ''''''), domain_type_rank, ...
            length(domain_sec)));
        switch length(rslt)
            case 0
                out = 0;
            case 1
                out = cell2mat(rslt);
            otherwise
                error('Domain is present more than one time');
        end
        
    end


    function s_nb=sql_wash_nan(nb)
        if isnan(nb)
            s_nb = ' is null';
        elseif ischar(nb)
            if strcmp(nb,'null')
                s_nb = ' is null';
            else
                s_nb = ['=',nb];
            end
        else
            s_nb = ['=',num2str(nb)];
        end
    end

end

