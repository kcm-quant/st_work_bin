function r = se_create_perimeter(perimeter_name, perimeter_comment, perimeter_frequency, perimeter_frequency_date, update_type, request , varargin)
% SE_CREATE_PERIMETER - fill the perimeter table. Called by perim_...
% functions
%
% Author: edarchimbaud@cheuvreux.com
% Date: 30/01/2008


global st_version;

sv = st_version.bases.quant;

opt = options( { ...
    'parameters', '' }, varargin);

parameters = opt.get('parameters');

perimeter_id = cell2mat(exec_sql('QUANT' , sprintf([ ...
    'SELECT perimeter_id ' ...
    'FROM %s..perimeter ' ...
    'WHERE perimeter_name=''%s'''], sv, perimeter_name)));


if isempty(perimeter_id)
    
    perimeter_frequency_id = cell2mat(exec_sql('QUANT', ...
        sprintf('SELECT frequency_id FROM %s..frequency WHERE frequency_name=''%s''', sv, perimeter_frequency)));
    
    update_type_id = cell2mat(exec_sql('QUANT', ...
        sprintf('SELECT update_type_id FROM %s..update_type WHERE update_type_name=''%s''', sv, update_type)));
    
    request_type_id = cell2mat(exec_sql('QUANT', ...
        sprintf('SELECT request_type_id FROM %s..request_type WHERE request_type_name=''matlab''', sv)));
    
    parameters  = strrep(strrep(parameters, char(10), ' '), '''', '''''');
    exec_sql('QUANT', sprintf(['INSERT INTO %s..perimeter (perimeter_name, comment, run_frequency_id, run_frequency_date, ' ...
        'update_type_id, request, request_type , parameters) ' ...
        'VALUES (''%s'', ''%s'', %d, ''%s'', %d, ''%s'', %d ,''%s'')'], ...
        sv, perimeter_name, perimeter_comment, perimeter_frequency_id, ...
        perimeter_frequency_date, update_type_id, request, request_type_id, parameters));
    
    perimeter_id = cell2mat(exec_sql('QUANT' , sprintf([ ...
        'SELECT perimeter_id ' ...
        'FROM %s..perimeter ' ...
        'WHERE perimeter_name=''%s'''], sv, perimeter_name)));
    
    
else
    st_log('perimeter %s does already exist!\n', perimeter_name);
    
    %R�cup�ration des indices qui composent le perim�tre � partir de la base
    %QUANT
     db_parameters = cell2mat(exec_sql('QUANT' , sprintf([ 'SELECT parameters FROM %s..perimeter WHERE perimeter_name=''%s'''], sv, perimeter_name)));
     %Mise � jour de la colonne parameters si la composition du p�rim�tre a �t� modifi�e dans le se_install.xml  
     if ~strcmp( strtrim(db_parameters),strtrim(parameters)) && ~isempty(parameters)
         st_log('updating perimeter''s parameters!\n');
         parameters  = strrep(strrep(parameters, char(10), ' '), '''', '''''');
         exec_sql('QUANT', ...
             sprintf('UPDATE %s..perimeter set parameters = ''%s'' WHERE perimeter_id = %d',sv,parameters,perimeter_id));
         
     end
    
end

r = perimeter_id;

end