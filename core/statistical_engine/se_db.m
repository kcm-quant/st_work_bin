function [out, out_plus] = se_db(mode, varargin)
% SE_DB - Fonction de bas niveau pour les interactions avec la base du SE
%
% Ajouter une entr?e dans la base. Return an ID value
%     se_db('add_estimator', estimator_name, comment, matlab_run, matlab_check, output_type_id)
%     se_db('add_output_type', output_type_name, output_type_comment)
%     se_db('add_param', parameter_name, x_value, estimator_id, comment)
%     se_db('add_context', context_name, context_num, estimator_id, comment)
%     se_db('add_domain', domain_name, comment, begin_date, end_date, security)
%     se_db('add_frequency', frequency_name, comment)
%     se_db('add_window_type', window_type_name, comment)
%     se_db('add_job', estimator_id, domain_id, comment, run_frequency_id, run_window_type, ...
%         run_window_width, run_frequency_date, check_frequency_id, check_window_type, ...
%         check_window_width, check_frequency_date, varargin)
%     se_db('add_association', context_id, rank, sec_id, td_id, estimator_id, job_id, varargin)
%
% Tester l'existence d'une entr?e dans la base. Return 1 or 0
%     se_db('exist_estimator', estimator_name, matlab_run, matlab_check, output_type_id)
%     se_db('exist_output_type', output_type_name)
%     se_db('exist_param', parameter_name)
%     se_db('exist_context', context_name)
%     se_db('exist_domain', security)
%     se_db('exist_frequency', frequency_name)
%     se_db('exist_window_type', window_type_name)
%     se_db('exist_job', estimator_id, domain_id, comment, run_frequency, run_window_type, run_window_width, ...
%         run_date, check_frequency, check_window_type, check_window_width, check_date)
%     se_db('exist_association', def_context_id, rank, sec_id, td_id, estimator_id, job_id)
%     se_db('exist_pre_validation_function', validation_type ,estimator_id)
%
% Lister differentes tables
%     se_db('list_context')
%     [a,b]=se_db('list_estimator', 'Volume threshold', 'output-type',
%     'struct')
%     jobs=se_db('list_job', 1, 'input-field', 'estimator_id')
%
% See also se_load se_xml
%
% Author   'Edouard d'Archimbaud'
% Version  '1.0'
% Reviewer ''
% Date     '21/10/2008'

global st_version

quant = st_version.bases.quant;


out_plus = [];

switch mode
    
    case 'add_estimator'
        % Ajout dans la table estimator de QUANT
        % (estimator_name, comment, matlab_run, matlab_check, output_type_id)
        query = sprintf('%s..ins_estimator ''%s'',''%s'',''%s'',''%s'', %d ', ...
            quant, varargin{1}, varargin{2}, varargin{3}, varargin{4}, varargin{5});
        out = cell2mat(exec_sql('QUANT', query));
        
        
    case 'add_output_type'
        % Ajout dans la table estimator de QUANT
        % (output_type_name, output_type_comment)
        query = sprintf('%s..ins_output_type ''%s'',''%s'' ', ...
            quant, varargin{1}, varargin{2});
        out = cell2mat(exec_sql('QUANT', query));
        
        
    case 'add_param'
        % Ajout dans param_desc
        % (parameter_name, estimator_id, comment, x_value)
        query = sprintf('%s..ins_param_desc ''%s'',%d,''%s'',%d ',...
            quant, varargin{1}, varargin{2}, varargin{3}, varargin{4});
        out = cell2mat(exec_sql('QUANT', query));
        
        
    case 'add_context'
        % Ajout dans context
        % (context_name, context_num, estimator_id, comment)
        query = sprintf('%s..ins_context ''%s'',%d,%d,''%s''', ...
            quant, varargin{1}, varargin{2}, varargin{3}, varargin{4});
        out = cell2mat(exec_sql('QUANT', query));
        
        
    case 'add_domain'
        % Insertion in domain
        % (domain_name, comment, begin_date, end_date, security)
        if isempty(varargin{4})
            query = sprintf('%s..ins_domain ''%s'',''%s'', ''%s'', null', ...
                quant, wash_sql(varargin{1}), wash_sql(varargin{2}), varargin{3});
        else
            query = sprintf('%s..ins_domain ''%s'',''%s'', ''%s'', ''%s''', ...
                quant, wash_sql(varargin{1}), wash_sql(varargin{2}), varargin{3}, varargin{4});
        end
        domain_id = cell2mat(exec_sql('QUANT', query));
        % Insertion in domain_security
        for i=1:length(varargin{5})
            % (domain_id, security_id, trading_destination_id)
            if ~isnan(varargin{5}{i}.id)
                id = num2str(varargin{5}{i}.id);
            else
                id = 'null';
            end
            if ~isnan(varargin{5}{i}.td)
                td = num2str(varargin{5}{i}.td);
            else
                td = 'null';
            end
            query = sprintf('%s..ins_domain_sec %d, %s, %s', ...
                quant, domain_id, id, td);
            exec_sql('QUANT', query);
        end
        out = domain_id;
        
        
    case 'add_frequency'
        % Add frequency
        % Insertion in frequency (frequency_name, comment)
        query = sprintf('%s..ins_frequency ''%s'',''%s'' ',quant, varargin{1}, varargin{2});
        out = cell2mat(exec_sql('QUANT', query));
        
        
    case 'add_window_type'
        % Add window_type
        % Insertion in frequency (window_type_name, comment)
        query = sprintf('%s..ins_window_type ''%s'',''%s'' ',quant, varargin{1}, varargin{2});
        out = cell2mat(exec_sql('QUANT', query));
        
        
    case 'add_job'
        % Add a job
        % Insertion in job (estimator_id, domain_id, comment, run_frequency_id, run_window_type,
        % run_window_width, run_frequency_date, check_frequency_id, check_window_type, check_window_width,
        % check_frequency_date, varargin)
        query = sprintf('%s..ins_job %d,%d,''%s'',%d,%d,%d,''%s'',%d,%d,%d,''%s'',''%s''', ...
            quant, varargin{1}, varargin{2}, varargin{3}, varargin{4}, varargin{5}, varargin{6}, varargin{7}, ...
            varargin{8}, varargin{9}, varargin{10}, varargin{11}, varargin{12});
        out = cell2mat(exec_sql('QUANT', query));
        
        
    case 'add_association'
        % Add an association to the associations table.
        % (context_id, rank, sec_id, td_id, estimator_id, job_id, varargin)
        query = sprintf('%s..ins_association %d, %d, %d, %d, %d, %d, ''%s''',...
            quant, varargin{1}, varargin{2}, varargin{3}, varargin{4}, varargin{5}, varargin{6}, varargin{7});
        out = cell2mat(exec_sql('QUANT', query));
        
        
    case 'add_check_quality'
        % Add an association to the associations table.
        % (job_id, run_id, value, stamp_date, last_check_date)
        query = sprintf('%s..ins_check_quality %d, %d, %d, ''%s'', ''%s'' , %d , ''%s'' ' ,...
            quant, varargin{1}, varargin{2}, varargin{3}, varargin{4}, varargin{5}, varargin{6} , varargin{7});
        out = cell2mat(exec_sql('QUANT', query));
        
        
    case 'add_error'
        % Add an association to the associations table.
        % (job_id, gravity_level, is_run, stamp_date, message)
        query = sprintf('%s..ins_error %d, %d, %d, ''%s'', ''%s'', ''%s''',...
            quant, varargin{1}, varargin{2}, varargin{3}, varargin{4}, varargin{5}, regexprep(varargin{6}, '''', '"'));
        out = cell2mat(exec_sql('QUANT', query));
        
        
    case 'add_job_status'
        % Add an association to the associations table.
        % (job_id, status, is_run, beg_date, beg_time)
        query = sprintf('%s..ins_job_status %d, ''%s'', %d, ''%s'', ''%s'', null, null, ''%s'', %d',...
            quant, varargin{1}, varargin{2}, varargin{3}, varargin{4}, varargin{5}, varargin{6}, varargin{7});
        out = cell2mat(exec_sql('QUANT', query));
        
    case 'add_session'
        query = sprintf('%s..ins_session %d, ''%s'', ''%s'', null, null ',...
            quant, varargin{1}, varargin{2}, varargin{3});
        out = cell2mat(exec_sql('QUANT', query));
        
    case 'exist_estimator'
        % Check if estimator exists in quant..estimator
        query = sprintf('SELECT estimator_id FROM %s..estimator WHERE estimator_name=''%s'' AND matlab_run=''%s'' AND matlab_check=''%s'' AND output_type_id=%d', ...
            quant, varargin{1}, varargin{2}, varargin{3}, varargin{4});
        rslt = exec_sql('QUANT', query);
        switch length(rslt)
            case 0
                out = 0;
            case 1
                % Estimator does exist
                st_log('This estimator already exists\n');
                out = rslt{1};
            otherwise
                % Message d'erreur si doublon
                out = 0;
                error(['More than one such estimator in estimator: ' ...
                    sprintf('estimator_name = ''%s'' \n', varargin{1})]);
        end
        
        
    case 'exist_output_type'
        % Check if estimator exists in quant..estimator
        query = sprintf('SELECT output_type_id FROM %s..output_type WHERE output_type_name = ''%s''',quant, varargin{1});
        rslt = exec_sql('QUANT', query);
        switch length(rslt)
            case 0
                out = 0;
            case 1
                % Estimator does exist
                st_log('This estimator output type already exists\n');
                out = rslt{1};
            otherwise
                % Message d'erreur si doublon
                out = 0;
                error(['More than one such estimator in estimator: ' ...
                    sprintf('estimator_name = ''%s'' \n', varargin{1})]);
        end
        
        
    case 'exist_param'
        % Check if estimator parameter exists in quant..estimator
        % (parameter_name, estimator_id, comment, x_value)
        query = sprintf('SELECT parameter_id FROM %s..param_desc WHERE parameter_name=''%s'' AND estimator_id=%d', ...
            quant, varargin{1}, varargin{2});
        rslt = exec_sql('QUANT', query);
        switch length(rslt)
            case 0
                out = 0;
            case 1
                % Parameter does exist
                st_log('This parameter already exists\n');
                out = rslt{1};
            otherwise
                % Message d'erreur si doublon
                out = 0;
                error(['More than one such parameter in param_desc: ' ...
                    sprintf('parameter_name=''%s''', varargin{1})]);
        end
        
        
    case 'exist_context'
        % Check if estimator context exists in quant..estimator
        % (context_name, estimator_id, context_num, comment)
        query = sprintf('SELECT context_id FROM %s..context WHERE context_name=''%s'' AND estimator_id=%d', ...
            quant, varargin{1}, varargin{2});
        rslt = exec_sql('QUANT', query);
        switch length(rslt)
            case 0
                out = 0;
            case 1
                % Estimator does exist
                st_log('This context already exists\n');
                out = rslt{1};
            otherwise
                % Message d'erreur si doublon
                out = 0;
                error(['More than one such parameter in context: ' ...
                    sprintf('context_name=''%s''', varargin{1})]);
        end
        
        
    case 'exist_domain'
        % Check if domain exists in quant..domain
        % Loop over the (sec_id, sec_td) couples to build the condition1
        condition1 = cell(1, length(varargin{1}));
        
        for i=1:length(varargin{1})
            if ~isnan(varargin{1}{i}.id)
                id = num2str(varargin{1}{i}.id);
            else
                id = 'null';
            end
            if ~isnan(varargin{1}{i}.td)
                td = num2str(varargin{1}{i}.td);
            else
                td = 'null';
            end
            condition1{i} = sprintf('(security_id=%s AND trading_destination_id=%s) OR ', ...
                id, td);
        end
        condition1 = [condition1{:}];
        condition2 = sprintf('COUNT(*)=%d', length(varargin{1}));
        % On utilise 0=1 car la condition FALSE n'existe pas
        query = sprintf(['SELECT domain_id FROM %s..domain_security a WHERE' ...
            condition1 '0=1 GROUP BY domain_id HAVING ' condition2 ' AND COUNT(*)=' ...
            '(SELECT COUNT(*) FROM %s..domain_security WHERE domain_id=a.domain_id)'], quant, quant);
        rslt = exec_sql('QUANT', query);
        switch length(rslt)
            case 0
                out = 0;
            case 1
                % Domain does exist
                st_log('This domain already exists\n');
                out = rslt{1};
            otherwise
                % Message si doublon
                %out =  max([rslt{:}]);
                out =  [rslt{:}];
                st_log(['More than one such domain in domain_security: ' condition1]);
        end
        
        
    case 'exist_frequency'
        % Check if domain exists in quant..frequency
        query = sprintf('SELECT frequency_id FROM %s..frequency WHERE frequency_name=''%s''', ...
            quant, varargin{1});
        rslt = exec_sql('QUANT', query);
        % If there exists one freq, we return the associated frequency_id. Else we
        % return 0 with an error message if there exists more than one freq
        switch length(rslt)
            case 0
                out = 0;
            case 1
                st_log('This frequency already exists\n');
                out = rslt{1};
            otherwise
                % Message d'erreur si doublon
                out = 0;
                error(['More than one such freq in frequency: ' ...
                    sprintf('frequency_name=''%s''', varargin{1})]);
        end
        
        
    case 'exist_window_type'
        % Check if domain exists in quant..frequency
        query = sprintf('SELECT window_type_id FROM %s..window_type WHERE window_type_name=''%s''', ...
            quant, varargin{1});
        rslt = exec_sql('QUANT', query);
        % If there exists one freq, we return the associated frequency_id. Else we
        % return 0 with an error message if there exists more than one freq
        switch length(rslt)
            case 0
                out = 0;
            case 1
                st_log('This window_type already exists\n');
                out = rslt{1};
            otherwise
                % Message d'erreur si doublon
                out = 0;
                error(['More than one such window_type in window_type: ' ...
                    sprintf('window_type_name=''%s''', varargin{1})]);
        end
        
        
    case 'exist_job'
        % Check if job exists in quant..job
        % (estimator_id, domain_id, comment, run_frequency, run_window_type, run_window_width,
        % run_date, check_frequency, check_window_type, check_window_width, check_date)
        freq_run_id = se_db('exist_frequency', varargin{4});
        freq_check_id = se_db('exist_frequency', varargin{8});
        window_run_id = se_db('exist_window_type', varargin{5});
        window_check_id = se_db('exist_window_type', varargin{9});
        
        if (logical(freq_run_id) && logical(freq_check_id) && ...
                logical(window_run_id) && logical(window_check_id))
            query = sprintf([...
                'SELECT job_id FROM %s..job WHERE estimator_id = %d AND domain_id=%d AND comment=''%s'' AND '...
                'run_frequency_id = %d AND run_window_type_id = %d AND run_window_width=%d AND run_frequency_date=''%s''AND '...
                'check_frequency_id=%d AND check_window_type_id=%d AND check_window_width=%d AND check_frequency_date=''%s'''], ...
                quant, varargin{1}, varargin{2}, varargin{3}, freq_run_id, window_run_id, varargin{6}, varargin{7}, freq_check_id, ...
                window_check_id, varargin{10}, varargin{11});
            rslt = exec_sql('QUANT', query);
            switch length(rslt)
                case 0
                    out = 0;
                case 1
                    st_log('This job already exists\n');
                    out = rslt{1};
                otherwise
                    % Message d'erreur si doublon
                    out = 0;
                    error(['More than one such job in job: ' ...
                        sprintf(['estimator_id = %d AND domain_id=%d AND comment=''%s'' AND '...
                        'run_frequency_id = %d AND run_window_type_id = %d AND run_window_width=%d AND run_frequency_date=''%s''AND '...
                        'check_frequency_id=%d AND check_window_type_id=%d AND check_window_width=%d AND check_frequency_date=''%s'''], ...
                        varargin{1}, varargin{2}, varargin{3}, freq_run_id, window_run_id, varargin{6}, varargin{7}, freq_check_id, ...
                        window_check_id, varargin{10}, varargin{11})]);
            end
        else
            % Cas o? les freq n'existent pas. Si les freq n'existent pas, le job ne
            % peut pas exister.
            out = 0;
        end
        
        
    case 'exist_association'
        % Check if association exists in quant..frequency
        % (context_id, rank, sec_id, td_id, estimator_id, job_id)
        query = sprintf(['SELECT association_id FROM %s..association WHERE def_context_id=%d AND '...
            'rank=%d AND sec_id=%d AND td_id=%d AND estimator_id = %d AND job_id = %d'], ...
            quant, varargin{1}, varargin{2}, varargin{3}, varargin{4}, varargin{5}, varargin{6});
        rslt = exec_sql('QUANT', query);
        switch length(rslt)
            case 0
                out = 0;
            case 1
                st_log('This association already exists\n');
                out = rslt{1};
            otherwise
                % Message d'erreur si doublon
                out = 0;
                error(['More than one such freq in association: ' ...
                    sprintf('def_context_id=%d AND rank=%d AND sec_id=%d AND td_id=%d AND estimator_id = %d AND job_id = %d', ...
                    varargin{1}, varargin{2}, varargin{3}, varargin{4}, varargin{5}, varargin{6})]);
        end
        
    case 'exist_pre_validation_function'
        % Check if pre_validation_function exists in
        % quant..pre_post_validation_book
        %('PRE_VALIDATION',estimator_id/estimator_name )
        
        if ~isinteger(varargin{2})
            estimator_id = cell2mat( exec_sql('QUANT', sprintf('SELECT estimator_id from %s..estimator where upper(estimator_name) = ''%s''', quant,upper(varargin{2}))))
        else
            estimator_id = varargin{2};
        end
        query = sprintf([ ...
            'SELECT val_b.matlab_func '...
            'FROM '...
            '%s.. validation_type vt, '...
            '%s..pre_post_validation_book val_b '...
            'WHERE ' ...
            'upper(vt.validation_type_name) = ''%s'' AND '...
            'val_b.estimator_id= %d AND '...
            'val_b.validation_type_id = vt.validation_type_id '],quant, quant, upper(varargin{1}),estimator_id);
        
        rslt = exec_sql('QUANT', query);
        switch length(rslt)
            case 0
                out = 0;
            case 1
                st_log('Function of type "pre_validation" already exists\n');
                out = rslt{1};
            otherwise
                % Message d'erreur si doublon
                out = 0;
                error(['No such function in validation process']);
        end
        
        
        
        
    case 'get_context'
        % Get quant..context
        opt = options({ ...
            'context_id', NaN, ...
            'context_name', NaN, ...
            'estimator_id', NaN, ...
            'context_num', NaN, ...
            'comment', NaN}, ...
            varargin);
        % We use 0=0 to implement TRUE
        query = {sprintf('SELECT context_id, context_name, estimator_id, context_num, comment FROM %s..context WHERE 0=0', ...
            quant)};
        if ~isnan(opt.get('context_id'))
            query{end+1} = sprintf(' AND context_id=%d', opt.get('context_id'));
        end
        if ~isnan(opt.get('context_name'))
            query{end+1} = sprintf(' AND context_name=''%s''', opt.get('context_name'));
        end
        if ~isnan(opt.get('estimator_id'))
            query{end+1} = sprintf(' AND estimator_id=%d', opt.get('estimator_id'));
        end
        if ~isnan(opt.get('context_num'))
            query{end+1} = sprintf(' AND context_num=%d', opt.get('context_num'));
        end
        if ~isnan(opt.get('comment'))
            query{end+1} = sprintf(' AND comment=''%s''', opt.get('comment'));
        end
        rslt = exec_sql('QUANT', [query{:}]);
        if isempty(rslt)
            out = {};
        else
            for i=1:size(rslt, 1)
                out(i) = struct( ...
                    'context_id', rslt(i, 1), ...
                    'context_name', rslt(i, 2), ...
                    'estimator_id', rslt(i, 3), ...
                    'context_num', rslt(i, 4), ...
                    'comment', rslt(i, 5));
            end
        end
        
        
    case 'get_estimator'
        % Get quant..estimator
        opt = options({ ...
            'estimator_id', NaN, ...
            'estimator_name', NaN, ...
            'comment', NaN, ...
            'matlab_run', NaN, ...
            'matlab_check', NaN, ...
            'output_type_id', NaN}, ...
            varargin);
        % We use 0=0 to implement TRUE
        query = {sprintf('SELECT estimator_id, estimator_name, comment, matlab_run, matlab_check, output_type_id FROM %s..estimator WHERE 0=0', ...
            quant)};
        if ~isnan(opt.get('estimator_id'))
            query{end+1} = sprintf(' AND estimator_id=%d', opt.get('estimator_id'));
        end
        
        if ~isnan(opt.get('estimator_name'))
            query{end+1} = sprintf(' AND upper(estimator_name)=''%s''', upper(sprintf('%s', strrep(opt.get('estimator_name') ,'_',' '))));
        end
        if ~isnan(opt.get('comment'))
            query{end+1} = sprintf(' AND comment=''%s''', opt.get('comment'));
        end
        if ~isnan(opt.get('matlab_run'))
            query{end+1} = sprintf(' AND matlab_run=''%s''', opt.get('matlab_run'));
        end
        if ~isnan(opt.get('matlab_check'))
            query{end+1} = sprintf(' AND matlab_check=''%s''', opt.get('matlab_check'));
        end
        if ~isnan(opt.get('output_type_id'))
            query{end+1} = sprintf(' AND output_type_id=%d', opt.get('output_type_id'));
        end
        rslt = exec_sql('QUANT', [query{:}]);
        if isempty(rslt)
            out = {};
        else
            for i=1:size(rslt, 1)
                out(i) = struct( ...
                    'estimator_id', rslt(i, 1), ...
                    'estimator_name', rslt(i, 2), ...
                    'comment', rslt(i, 3), ...
                    'matlab_run', rslt(i, 4), ...
                    'matlab_check', rslt(i, 5), ...
                    'output_type_id', rslt(i, 6));
            end
        end
        
        
    case 'get_job_status'
        opt = options({ ...
            'job_or_run_id', NaN, ...
            'status', NaN, ...
            'is_run', NaN, ...
            'beg_date', NaN, ...
            'beg_time', NaN, ...
            'end_date', NaN, ...
            'end_time', NaN, ...
            'job_name', NaN, ...
            'session_num', NaN , ...
            'as_of_date' , NaN }, ...
            varargin);
        
        
        query = {'SELECT job_or_run_id, status, is_run, beg_date, beg_time, end_date,'};
        query{end+1} = sprintf(' end_time, job_name, session_num FROM %s..job_status WHERE 0=0', ...
            quant);
        if ~isnan(opt.get('job_or_run_id'))
            query{end+1} = sprintf(' AND job_or_run_id=%d', opt.get('job_or_run_id'));
        end
        if ~isnan(opt.get('job_name'))
            query{end+1} = sprintf(' AND job_name=''%s''', opt.get('job_name'));
        end
        if ~isnan(opt.get('session_num'))
            query{end+1} = sprintf(' AND session_num=%d', opt.get('session_num'));
        end
        if ~isnan(opt.get('is_run'))
            query{end+1} = sprintf(' AND is_run=%s', opt.get('is_run'));
        end
        if ~isnan(opt.get('as_of_date'))
            as_of_date = datestr(datenum(opt.get('as_of_date'), 'dd/mm/yyyy') , 'yyyymmdd');
            query{end+1} = sprintf(' AND beg_date <= ''%s''', as_of_date );
        end
        
        rslt = exec_sql('QUANT', [query{:}]);
        
        if isempty(rslt)
            out = {};
        else
            if isempty(opt.get('as_of_date'))
                for i=1:size(rslt, 1)
                    out(i) = struct( ...
                        'job_or_run_id', rslt(i, 1), ...
                        'status', rslt(i, 2), ...
                        'is_run', rslt(i, 3), ...
                        'beg_date', rslt(i, 4), ...
                        'beg_time', rslt(i, 5), ...
                        'end_date', rslt(i, 6), ...
                        'end_time', rslt(i, 7), ...
                        'job_name', rslt(i, 8), ...
                        'session_num', rslt(i, 9));
                end
            else
                out(1) = struct( ...
                    'job_or_run_id', rslt(end, 1), ...
                    'status', rslt(end, 2), ...
                    'is_run', rslt(end, 3), ...
                    'beg_date', rslt(end, 4), ...
                    'beg_time', rslt(end, 5), ...
                    'end_date', rslt(end, 6), ...
                    'end_time', rslt(end, 7), ...
                    'job_name', rslt(end, 8), ...
                    'session_num', rslt(end, 9));
            end
        end
        
    case 'get_error'
        opt = options({ ...
            'error_id', NaN, ...
            'job_or_run_id', NaN, ...
            'gravity_level', NaN, ...
            'is_run', NaN, ...
            'stamp_date', NaN, ...
            'stamp_time', NaN, ...
            'message', NaN, ...
            'as_of_date' , NaN}, ...
            varargin);
        
        
        % We use 0=0 to implement TRUE
        if ~isnan(opt.get('as_of_date'))
            top = 'TOP 1' ;
        else
            top = '';
        end
        
        query = {sprintf('SELECT %s  error_id, job_or_run_id, gravity_level, is_run,' , top)};
        query{end+1} = sprintf('  convert(date,stamp_date),convert(time,stamp_time), message FROM %s..error WHERE 0=0', ...
            quant);
        if ~isnan(opt.get('error_id'))
            query{end+1} = sprintf(' AND error_id=%d', opt.get('error_id'));
        end
        if ~isnan(opt.get('job_or_run_id'))
            query{end+1} = sprintf(' AND job_or_run_id=%d', opt.get('job_or_run_id'));
        end
        if ~isnan(opt.get('gravity_level'))
            query{end+1} = sprintf(' AND gravity_level=%d', opt.get('gravity_level'));
        end
        if ~isnan(opt.get('is_run'))
            query{end+1} = sprintf(' AND is_run= %s ', opt.get('is_run'));
        end
        if ~isnan(opt.get('stamp_date'))
            query{end+1} = sprintf(' AND stamp_date=''%s''', opt.get('stamp_date'));
        end
        if ~isnan(opt.get('message'))
            query{end+1} = sprintf(' AND message=''%s''', opt.get('message'));
        end
        
        if ~isnan(opt.get('as_of_date'))
            as_of_date = datestr(datenum(opt.get('as_of_date'), 'dd/mm/yyyy') , 'yyyymmdd');
            query{end+1} = sprintf(' AND stamp_date <= ''%s'' ORDER BY error_id desc  ', as_of_date );
        end
        
        rslt = exec_sql('QUANT', [query{:}]);
        if isempty(rslt)
            out = {};
        else
            for i=1:size(rslt, 1)
                out(i) = struct( ...
                    'error_id', rslt(i, 1), ...
                    'job_or_run_id', rslt(i, 2), ...
                    'gravity_level', rslt(i, 3), ...
                    'is_run', rslt(i, 4), ...
                    'stamp_date', rslt(i, 5), ...
                    'stamp_time', rslt(i, 6), ...
                    'message', rslt(i, 7));
            end
        end
        
        
        
    case 'get_estimator_run'
        % Get quant..param_desc
        opt = options({ ...
            'run_id', NaN, ...
            'job_id', NaN, ...
            'estimator_id', NaN, ...
            'stamp_date', NaN, ...
            'domain_id', NaN, ...
            'run_quality', NaN, ...
            'context_id', NaN, ...
            'window_end_date', NaN, ...
            'is_valid', NaN, ...
            'comment', NaN, ...
            'session_num', NaN}, ...
            varargin);
        % We use 0=0 to implement TRUE
        query = {'SELECT run_id, job_id, estimator_id, stamp_date, domain_id, run_quality,'};
        query{end+1} = sprintf(' context_id, window_end_date, is_valid, comment , session_num FROM %s..estimator_runs WHERE 0=0', ...
            quant);
        if ~isnan(opt.get('run_id'))
            query{end+1} = sprintf(' AND run_id=%d', opt.get('run_id'));
        end
        if ~isnan(opt.get('job_id'))
            query{end+1} = sprintf(' AND job_id=%d', opt.get('job_id'));
        end
        if ~isnan(opt.get('estimator_id'))
            query{end+1} = sprintf(' AND estimator_id=''%s''', opt.get('estimator_id'));
        end
        if ~isnan(opt.get('stamp_date'))
            query{end+1} = sprintf(' AND stamp_date=''%s''', opt.get('stamp_date'));
        end
        if ~isnan(opt.get('domain_id'))
            query{end+1} = sprintf(' AND domain_id=%d', opt.get('domain_id'));
        end
        if ~isnan(opt.get('run_quality'))
            query{end+1} = sprintf(' AND run_quality=%d', opt.get('run_quality'));
        end
        if ~isnan(opt.get('context_id'))
            query{end+1} = sprintf(' AND context_id=%d', opt.get('context_id'));
        end
        if ~isnan(opt.get('window_end_date'))
            query{end+1} = sprintf(' AND window_end_date=''%s''', opt.get('window_end_date'));
        end
        if ~isnan(opt.get('is_valid'))
            query{end+1} = sprintf(' AND is_valid=%d', opt.get('is_valid'));
        end
        if ~isnan(opt.get('comment'))
            query{end+1} = sprintf(' AND comment=''%s''', opt.get('comment'));
        end
         if ~isnan(opt.get('session_num'))
            query{end+1} = sprintf(' AND session_num= %d', opt.get('session_num'));
        end
        rslt = exec_sql('QUANT', [query{:}]);
        if isempty(rslt)
            out = {};
        else
            for i=1:size(rslt, 1)
                out(i) = struct( ...
                    'run_id', rslt(i, 1), ...
                    'job_id', rslt(i, 2), ...
                    'estimator_id', rslt(i, 3), ...
                    'stamp_date', rslt(i, 4), ...
                    'domain_id', rslt(i, 5), ...
                    'run_quality', rslt(i, 6), ...
                    'context_id', rslt(i, 7), ...
                    'window_end_date', rslt(i, 8), ...
                    'is_valid', rslt(i, 9), ...
                    'comment', rslt(i, 10), ...
                    'session_num', rslt(i, 11));
            end
        end
        
        
    case 'get_domain_security'
        % Get QUANT..domain_security
        opt = options({ ...
            'domain_sec_id', NaN, ...
            'domain_id', NaN, ...
            'security_id', NaN, ...
            'trading_destination_id', NaN}, ...
            varargin);
        
        query = {['SELECT domain_sec_id, domain_id, security_id, trading_destination_id',...
            ' FROM ',quant,'..domain_security']};
        
        % String building functions: for single *and* multiple values.
        query_single_elt_fun = @(fielname,value){[' ',fielname,' = ',num2str(value)]};
        query_multip_elt_fun = @(fielname,value){[' ',fielname,' in (',sprintf([sprintf('%d,',value(1:end-1)),'%d'],value(end)),')']};
        
        if ~isnan(opt.get('domain_sec_id'))
            domain_sec_id = opt.get('domain_sec_id');
            if length(domain_sec_id) > 1
                query(end+1) = query_multip_elt_fun('domain_sec_id',domain_sec_id);
            else
                query(end+1) = query_single_elt_fun('domain_sec_id',domain_sec_id);
            end
        end
        if ~isnan(opt.get('domain_id'))
            domain_id = opt.get('domain_id');
            if length(domain_id) > 1
                query(end+1) = query_multip_elt_fun('domain_id',domain_id);
            else
                query(end+1) = query_single_elt_fun('domain_id',domain_id);
            end
        end
        if ~isnan(opt.get('security_id'))
            security_id = opt.get('security_id');
            if length(security_id) > 1
                query(end+1) = query_multip_elt_fun('security_id',security_id);
            else
                query(end+1) = query_single_elt_fun('security_id',security_id);
            end
            
        end
        if ~isnan(opt.get('trading_destination_id'))
            trading_destination_id = opt.get('trading_destination_id');
            if length(trading_destination_id) > 1
                query(end+1) = query_multip_elt_fun('trading_destination_id',trading_destination_id);
            else
                query(end+1) = query_single_elt_fun('trading_destination_id',trading_destination_id);
            end
        end
        out = {};
        if length(query) > 1
            query(2) = strcat(' where',query(2));
            query(3:end) = strcat(' and',query(3:end));
            rslt = (exec_sql('QUANT', [query{:}]));
            if ~isempty(rslt)
                out = struct( ...
                    'domain_sec_id', rslt(:, 1), ...
                    'domain_id', rslt(:, 2), ...
                    'security_id', rslt(:, 3), ...
                    'trading_destination_id', rslt(:, 4));
            end
        end
        
        idx_nan = isnan([out.trading_destination_id]);
        [out(idx_nan).trading_destination_id] = deal([]);
        
    case 'get_job'
        % Get quant..param_desc
        opt = options({ ...
            'job_id', NaN, ...
            'estimator_id', NaN, ...
            'domain_id', NaN, ...
            'comment', NaN, ...
            'run_frequency_id', NaN, ...
            'run_window_type_id', NaN, ...
            'run_window_width', NaN, ...
            'run_frequency_date', NaN, ...
            'check_frequency_id', NaN, ...
            'check_window_type_id', NaN, ...
            'check_window_width', NaN, ...
            'check_frequency_date', NaN, ...
            'last_run', NaN, ...
            'varargin', NaN}, ...
            varargin);
        % We use 0=0 to implement TRUE
        query = {'SELECT job_id, estimator_id, domain_id, comment, run_frequency_id, run_window_type_id, run_window_width, '};
        query{end+1} = 'CONVERT(CHAR(30), run_frequency_date, 103), check_frequency_id, check_window_type_id, check_window_width, ';
        query{end+1} = sprintf('CONVERT(CHAR(30), check_frequency_date, 103), last_run, varargin FROM %s..job WHERE 0=0', ...
            quant);
        if ~isnan(opt.get('job_id'))
            query{end+1} = sprintf(' AND job_id=%d', opt.get('job_id'));
        end
        if ~isnan(opt.get('estimator_id'))
            query{end+1} = sprintf(' AND estimator_id=%d', opt.get('estimator_id'));
        end
        if ~isnan(opt.get('domain_id'))
            query{end+1} = sprintf(' AND domain_id=%d', opt.get('domain_id'));
        end
        if ~isnan(opt.get('comment'))
            query{end+1} = sprintf(' AND comment=''%s''', opt.get('comment'));
        end
        if ~isnan(opt.get('run_frequency_id'))
            query{end+1} = sprintf(' AND run_frequency_id=%d', opt.get('run_frequency_id'));
        end
        if ~isnan(opt.get('run_window_type_id'))
            query{end+1} = sprintf(' AND run_window_type_id=%d', opt.get('run_window_type_id'));
        end
        if ~isnan(opt.get('run_window_width'))
            query{end+1} = sprintf(' AND run_window_width=%d', opt.get('run_window_width'));
        end
        if ~isnan(opt.get('run_frequency_date'))
            query{end+1} = sprintf(' AND run_frequency_date=''%s''', opt.get('run_frequency_date'));
        end
        if ~isnan(opt.get('check_frequency_id'))
            query{end+1} = sprintf(' AND check_frequency_id=%d', opt.get('check_frequency_id'));
        end
        if ~isnan(opt.get('check_window_type_id'))
            query{end+1} = sprintf(' AND check_window_type_id=%d', opt.get('check_window_type_id'));
        end
        if ~isnan(opt.get('check_window_width'))
            query{end+1} = sprintf(' AND check_window_width=%d', opt.get('check_window_width'));
        end
        if ~isnan(opt.get('check_frequency_date'))
            query{end+1} = sprintf(' AND check_frequency_date=''%s''', opt.get('check_frequency_date'));
        end
        if ~isnan(opt.get('last_run'))
            query{end+1} = sprintf(' AND last_run=%d', opt.get('last_run'));
        end
        if ~isnan(opt.get('varargin'))
            query{end+1} = sprintf(' AND varargin=''%s''', opt.get('varargin'));
        end
        rslt = exec_sql('QUANT', [query{:}]);
        if isempty(rslt)
            out = {};
        else
            for i=1:size(rslt, 1)
                out(i) = struct( ...
                    'job_id', rslt(i, 1), ...
                    'estimator_id', rslt(i, 2), ...
                    'domain_id', rslt(i, 3), ...
                    'comment', rslt(i, 4), ...
                    'run_frequency_id', rslt(i, 5), ...
                    'run_window_type_id', rslt(i, 6), ...
                    'run_window_width', rslt(i, 7), ...
                    'run_frequency_date', rslt(i, 8), ...
                    'check_frequency_id', rslt(i, 9), ...
                    'check_window_type_id', rslt(i, 10), ...
                    'check_window_width', rslt(i, 11), ...
                    'check_frequency_date', rslt(i, 12), ...
                    'last_run', rslt(i, 13), ...
                    'varargin', rslt(i, 14));
            end
        end
        
        
    case 'get_param_desc'
        % Get quant..param_desc
        opt = options({ ...
            'parameter_id', NaN, ...
            'x_value', NaN, ...
            'parameter_name', NaN, ...
            'estimator_id', NaN, ...
            'comment', NaN}, ...
            varargin);
        % We use 0=0 to implement TRUE
        query = {sprintf('SELECT parameter_id, x_value, parameter_name, estimator_id, comment FROM %s..param_desc WHERE 0=0', ...
            quant)};
        if ~isnan(opt.get('parameter_id'))
            query{end+1} = sprintf(' AND parameter_id=%d', opt.get('parameter_id'));
        end
        if ~isnan(opt.get('x_value'))
            query{end+1} = sprintf(' AND x_value=%d', opt.get('x_value'));
        end
        if ~isnan(opt.get('parameter_name'))
            query{end+1} = sprintf(' AND parameter_name=''%s''', opt.get('parameter_name'));
        end
        if ~isnan(opt.get('estimator_id'))
            query{end+1} = sprintf(' AND estimator_id=%d', opt.get('estimator_id'));
        end
        if ~isnan(opt.get('comment'))
            query{end+1} = sprintf(' AND comment=''%s''', opt.get('comment'));
        end
        rslt = exec_sql('QUANT', [query{:}]);
        if isempty(rslt)
            out = {};
        else
            for i=1:size(rslt, 1)
                out(i) = struct( ...
                    'parameter_id', rslt(i, 1), ...
                    'x_value', rslt(i, 2), ...
                    'parameter_name', rslt(i, 3), ...
                    'estimator_id', rslt(i, 4), ...
                    'comment', rslt(i, 5));
            end
        end
        
        
    case 'get_param_value'
        % Get quant..param_desc
        opt = options({ ...
            'run_id', NaN, ...
            'parameter_id', NaN, ...
            'value', NaN}, ...
            varargin);
        % We use 0=0 to implement TRUE
        query = {sprintf('SELECT run_id, parameter_id, value FROM %s..param_value WHERE 0=0', quant)};
        if ~isnan(opt.get('run_id'))
            query{end+1} = sprintf(' AND run_id=%d', opt.get('run_id'));
        end
        if ~isnan(opt.get('parameter_id'))
            query{end+1} = sprintf(' AND parameter_id=%d', opt.get('parameter_id'));
        end
        if ~isnan(opt.get('value'))
            query{end+1} = sprintf(' AND value=%d', opt.get('value'));
        end
        rslt = exec_sql('QUANT', [query{:}]);
        if isempty(rslt)
            out = {};
        else
            for i=1:size(rslt, 1)
                out(i) = struct( ...
                    'run_id', rslt(i, 1), ...
                    'parameter_id', rslt(i, 2), ...
                    'value', rslt(i, 3));
            end
        end
        
        
    case 'get_window_type'
        % Get quant..param_desc
        opt = options({ ...
            'window_type_id', NaN, ...
            'window_type_name', NaN, ...
            'comment', NaN}, ...
            varargin);
        % We use 0=0 to implement TRUE
        query = {sprintf('SELECT window_type_id, window_type_name, comment FROM %s..window_type WHERE 0=0', quant)};
        if ~isnan(opt.get('window_type_id'))
            query{end+1} = sprintf(' AND window_type_id=%d', opt.get('window_type_id'));
        end
        if ~isnan(opt.get('window_type_name'))
            query{end+1} = sprintf(' AND window_type_name=''%s''', opt.get('window_type_name'));
        end
        if ~isnan(opt.get('comment'))
            query{end+1} = sprintf(' AND comment=''%s''', opt.get('comment'));
        end
        rslt = exec_sql('QUANT', [query{:}]);
        if isempty(rslt)
            out = {};
        else
            for i=1:size(rslt, 1)
                out(i) = struct( ...
                    'window_type_id', rslt(i, 1), ...
                    'window_type_name', rslt(i, 2), ...
                    'comment', rslt(i, 3));
            end
        end
        
    case 'get_run_job'
        % Si l'?cart entre last_run et next_run devient trop grand, c'est qu'il y aura eu un pb
%         out = exec_sql('QUANT', sprintf(['SELECT j.job_id, e.matlab_run, e.estimator_id ' ...
%             'FROM %s..job j, %s..estimator e ' ...
%             'WHERE (j.next_run is null OR j.next_run <= ''%s'') AND j.estimator_id=e.estimator_id AND ' ...
%             'j.job_id IN (SELECT DISTINCT job_id FROM %s..perimeter_job WHERE is_active=1)'], ...
%             sv, sv, varargin{1}, sv));
        out = exec_sql('QUANT',sprintf(['SELECT j.job_id, e.matlab_run, e.estimator_id',...
            ' FROM ',quant,'..job j, ',...
            '    ',quant,'..estimator e,',...
            '   (SELECT job_id FROM ',quant,'..perimeter_job WHERE is_active=1 group by job_id ) pj',...
            ' WHERE isnull( j.next_run, ''1900-01-01'' ) <= ''%s''',...
            ' AND j.estimator_id=e.estimator_id',...
            ' AND j.job_id = pj.job_id',...
            ' and run_frequency_id <> 5'],varargin{1}));
        label = {'job_id', 'matlab_run', 'estimator_id'};
        if ~isempty(out)
            out = cell_to_struct(label, out);
        else
            out = [];
        end
        
        
    case 'get_active_estimators'
        %varargin(1)peut �tre par exemple meta/run/check/validation
        opt = options({ ...
            'process', NaN, ...
            'exec_phase', NaN}, ...
            varargin);
        process_type = opt.get('process');
        query =  {sprintf(['SELECT ep.estimator_id FROM %s.. estimator_process ep , %s..process_type pt , %s.. estimator es '...
            'WHERE upper(pt.process_type_name) =''%s'' AND '...
            'pt.process_type_id = ep.process_type_id  AND ' ...
            'ep.is_active = 1 AND ' ...
            'ep.estimator_id =es.estimator_id '],quant,quant,quant, upper(process_type) )};
        
        if ~isnan(opt.get('exec_phase'))
            query{end+1} = sprintf(' AND es.exec_order= %d ', opt.get('exec_phase'));
        end
        out =cell2mat( exec_sql('QUANT', [query{:}]));
        
        
    case 'get_perimeter'
        % Si l'?cart entre last_run et next_run devient trop grand, c'est qu'il y aura eu un pb
        % On ecarte les run_frequency_id = 5 ('one-off')
        out = cell2mat(exec_sql('QUANT', sprintf(['SELECT perimeter_id FROM %s..perimeter ' ...
            'WHERE (next_run is null OR next_run <= ''%s'') AND perimeter_name!=''static''',...
            ' and run_frequency_id <> 5'], ...
            quant, varargin{1})));
        
        
    case 'update_perimeter'
        run_date = datenum(varargin{4}, 'dd/mm/yyyy');
        run_freq_date = varargin{2};
        if strcmpi(run_freq_date, 'null')
            run_freq_date = run_date;
        else
            run_freq_date = datenum(run_freq_date, 'yyyy-mm-dd');
        end
        switch(varargin{3})
            case 1
                % Daily
                next_run_date = datestr(run_date+1, 'yyyymmdd');
            case 2
                % Weekly
                % next_run_date = datestr(run_date + 7 + mod(weekday(datenum(varargin{2}, 'dd/mm/yyyy')) - weekday(date), 7), 'yyyymmdd');
                next_run_date = datestr((floor((run_date-run_freq_date)/7) + 1) * 7 + run_freq_date, 'yyyymmdd');
            case 3
                % Monthly
                next_run_date = datestr(datenum(year(run_date), month(run_date)+1, day(run_freq_date)), 'yyyymmdd');
            case 4
                % As of date
                next_run_date = datestr(run_date, 'yyyymmdd');
            case 5
                % one-off
                next_run_date = run_freq_date;
        end
        query_update = sprintf('UPDATE %s..perimeter SET next_run=''%s'' WHERE perimeter_id=%d', quant, next_run_date, varargin{1});
        exec_sql('QUANT', query_update);
        
        
    case 'update_next_run'
        run_date = datenum(varargin{4}, 'dd/mm/yyyy');
        run_freq_date = datenum(varargin{2}, 'dd/mm/yyyy');
        switch(varargin{3})
            case 1
                % Daily
                next_run_date = datestr(run_date+1, 'yyyymmdd');
            case 2
                % Weekly
                % next_run_date = datestr(run_date + 7 + mod(weekday(datenum(varargin{2}, 'dd/mm/yyyy')) - weekday(date), 7), 'yyyymmdd');
                next_run_date = datestr((floor((run_date-run_freq_date)/7) + 1) * 7 + run_freq_date, 'yyyymmdd');
            case 3
                % Monthly
                next_run_date = datestr(datenum(year(run_date), month(run_date)+1, day(run_freq_date)), 'yyyymmdd');
            case 4
                % As of date
                next_run_date = datestr(run_date, 'yyyymmdd');
            case 5
                % one-off
                next_run_date = run_freq_date;
        end
        query_update = sprintf('UPDATE %s..job SET next_run=''%s'' WHERE job_id=%d', quant, next_run_date, varargin{1});
        exec_sql('QUANT', query_update);
        
        
    case 'get_check_job'
        % Si l'ecart entre last_run et next_run devient trop grand, c'est qu'il y aura eu un pb
        %         out = exec_sql('QUANT', sprintf(['SELECT lr.last_valid_run_id, e.matlab_check, e.estimator_id, lr.job_id, lr.context_id ' ...
        %             'FROM %s..last_run lr, %s..job j, %s..estimator e ' ...
        %             'WHERE j.job_id=lr.job_id AND (lr.next_check_date is null OR lr.next_check_date<=''%s'') AND ' ...
        %             'j.estimator_id=e.estimator_id AND j.job_id IN (SELECT DISTINCT job_id FROM %s..perimeter_job WHERE is_active=1) ' ...
        %             'AND lr.last_valid_run_id is not null ' ...
        %             'ORDER BY lr.job_id, lr.context_id'], ...
        %             sv, sv, sv, varargin{1}, sv));
        
        
        out = exec_sql('QUANT', sprintf(['SELECT lr.last_valid_run_id, e.matlab_check, e.estimator_id, lr.job_id, lr.context_id , 0 ' ...
            'FROM %s..last_run lr, %s..job j, %s..estimator e '...
            'WHERE j.job_id=lr.job_id AND (lr.next_check_date is null OR lr.next_check_date<=''%s'') AND '...
            'j.estimator_id=e.estimator_id AND '...
            'j.job_id IN (SELECT DISTINCT job_id FROM %s..perimeter_job WHERE is_active=1) AND '...
            'lr.last_valid_run_id is not null '...
            ' and j.check_frequency_id <> 5 '...
            'UNION '...
            'SELECT lr.last_run_id, e.matlab_check, e.estimator_id, lr.job_id, lr.context_id ,1  '...
            'FROM %s..last_run lr, %s..job j, %s..estimator e  '...
            'WHERE j.job_id=lr.job_id AND (lr.next_check_date is null OR lr.next_check_date<=''%s'') AND '...
            'j.estimator_id=e.estimator_id AND '...
            'j.job_id IN (SELECT DISTINCT job_id FROM %s..perimeter_job WHERE is_active=1)'...
            ' and (lr.last_run_id <> lr.last_valid_run_id or lr.last_valid_run_id is null)'...
            ' and lr.last_run_id is not null '...
            ' and j.check_frequency_id <> 5'], ...
            quant, quant, quant, varargin{1}, quant, quant, quant, quant, varargin{1}, quant));
        
        label = {'run_id', 'matlab_check', 'estimator_id', 'job_id', 'context_id','is_last_run'};
        if ~isempty(out)
            out = cell_to_struct(label, out);
        else
            out = [];
        end
        
    case 'update_check_date'
        check_date = datenum(varargin{4}, 'dd/mm/yyyy');
        check_freq_date = datenum(varargin{2}, 'dd/mm/yyyy');
        switch(varargin{3})
            case 1
                % Daily
                next_check_date = datestr(check_date+1, 'yyyymmdd');
            case 2
                % Weekly
                next_check_date = datestr(check_date + 7 + mod(weekday(datenum(varargin{2}, 'dd/mm/yyyy')) - weekday(date), 7), 'yyyymmdd');
            case 3
                % Monthly
                % next_check_date = datestr(datenum(year(check_date), month(check_date)+1, day(datenum(varargin{2}, 'dd/mm/yyyy'))), 'yyyymmdd');
                next_check_date = datestr((floor((check_date-check_freq_date)/7) + 1) * 7 + check_freq_date, 'yyyymmdd');
            case 4
                % As of date
                next_check_date = datestr(check_date, 'yyyymmdd');
            case 5
                % one-off
                next_check_date = check_freq_date;
        end
        query_update = sprintf('UPDATE %s..last_run SET last_check_date=''%s'' WHERE job_id=%d', ...
            quant, datestr(check_date, 'yyyymmdd'), varargin{1});
        exec_sql('QUANT', query_update);
        query_update = sprintf('UPDATE %s..last_run SET next_check_date=''%s'' WHERE job_id=%d', ...
            quant, next_check_date, varargin{1});
        exec_sql('QUANT', query_update);
        
        
        
    case 'update_job_status'
        % Add an association to the associations table.
        % (job_id, status, matlab_job_name, is_run, beg_date, beg_time)
        query = sprintf('UPDATE %s..job_status SET status=''%s'', end_date=''%s'', end_time=''%s'' WHERE job_status_id=%d',...
            quant, varargin{1}, varargin{2}, varargin{3}, varargin{4});
        out = cell2mat(exec_sql('QUANT', query));
        
    case 'update_session'
        query = sprintf('UPDATE %s..session SET end_date=''%s'', end_time=''%s'' WHERE id=%d',...
            quant, varargin{1}, varargin{2}, varargin{3});
        out = cell2mat(exec_sql('QUANT', query));
        
    case 'list_domain_security'
        %<* retrieve domains
        opt = options({ 'request-fields', 'domain_sec_id , domain_id, security_id, trading_destination_id', ...
            'request-table', sprintf('%s..domain_security', quant), ...
            'input-field', 'domain_sec_id', 'output-type', 'struct'}, varargin(2:end));
        [out, out_plus] = LIST_EE( opt, varargin(1:end));
        % >*
        
    case 'list_domain'
        %<* retrieve domains
        opt = options({ 'request-fields', 'domain_id , domain_name, comment, begin_date, end_date', ...
            'request-table', sprintf('%s..domain',quant), ...
            'input-field', 'domain_name', 'output-type', 'struct'}, varargin(2:end));
        [out, out_plus] = LIST_EE( opt, varargin(1:end));
        % >*
    case 'list_window_type'
        %<* retrieve window_type
        opt = options({ 'request-fields', 'window_type_id , window_type_name, comment', ...
            'request-table', sprintf('%s..window_type',quant), ...
            'input-field', 'window_type_id', 'output-type', 'struct'}, varargin(2:end));
        [out, out_plus] = LIST_EE( opt, varargin(1:end));
        % >*
    case 'list_job'
        %<* retreive list_estimator
        opt = options({ 'request-fields', [ 'job_id , estimator_id , domain_id ,  last_run , comment, run_frequency_id, run_window_type_id , run_window_width,  run_frequency_date ,' ...
            'check_frequency_id, check_window_type_id , check_window_width,  check_frequency_date '], ...
            'request-table', sprintf('%s..job',quant), ...
            'input-field', 'job_id', 'output-type', 'struct'}, varargin(2:end));
        [out, out_plus] = LIST_EE( opt, varargin(1:end));
        % >*
    case 'list_estimator'
        %<* retreive list_estimator
        opt = options({ 'request-fields', 'estimator_id, estimator_name, comment, matlab_run, matlab_check, output_type_id', ...
            'request-table', sprintf('%s..estimator',quant), ...
            'input-field', 'estimator_name', 'output-type', 'struct'}, varargin(2:end));
        [out, out_plus] = LIST_EE( opt, varargin(1:end));
        % >*
    case 'list_output_type'
        %<* Retrieve output types
        opt = options({ 'request-fields', 'output_type_id, output_type_name, comment', ...
            'request-table', sprintf('%s..output_type',quant), ...
            'input-field', 'output_type_name', 'output-type', 'struct'}, varargin(2:end));
        [out, out_plus] = LIST_EE( opt, varargin(1:end));
        %>*
    case 'list_context'
        %<* Retrieve output types
        opt = options({ 'request-fields', 'context_id, context_name, estimator_id, context_num, comment', ...
            'request-table', sprintf('%s..context',quant), ...
            'input-field', 'context_name', 'output-type', 'struct'}, varargin(2:end));
        [out, out_plus] = LIST_EE( opt, varargin(1:end));
        %>*
    case 'list_frequency'
        %<* Retrieve frequence
        opt = options({ 'request-fields', 'frequency_id, frequency_name, comment', ...
            'request-table', sprintf('%s..frequency',quant), ...
            'input-field', 'frequency_name', 'output-type', 'struct'}, varargin(2:end));
        [out, out_plus] = LIST_EE( opt, varargin(1:end));
        %>*
    case 'list_run'
        %<* Retrieve output types
        opt = options({ 'request-fields', 'run_id, job_id, estimator_id, stamp_date, domain_id, run_quality, context_num, window_end_date, is_valid, comment', ...
            'request-table', sprintf('%s..estimator_runs',quant), ...
            'input-field', 'job_id', 'output-type', 'struct'}, varargin(2:end));
        [out, out_plus] = LIST_EE( opt, varargin(1:end));
        %>*
    case 'list_check'
        %<* Retrieve output types
        opt = options({ 'request-fields', 'check_quality_id, run_id, value, stamp_date, comment', ...
            'request-table', sprintf('%s..check_quality',quant), ...
            'input-field', 'run_id', 'output-type', 'struct'}, varargin(2:end));
        [out, out_plus] = LIST_EE( opt, varargin(1:end));
        %>*
    case 'list_param_desc'
        %<* Retrieve jobs
        opt = options({ 'request-fields', 'parameter_id, x_value, parameter_name, estimator_id, comment', ...
            'request-table', sprintf('%s..param_desc',quant), ...
            'input-field', 'parameter_name', 'output-type', 'struct'}, varargin(2:end));
        [out, out_plus] = LIST_EE( opt, varargin(1:end));
        %>*
    case 'list_*'
        %<* Retrieve parameters description
        opt = options({ 'request-fields', 'parameter_id, x_value, parameter_name, estimator_id, comment', ...
            'input-field', 'parameter_name', ...
            'output-type', 'struct', ...
            'request-table', sprintf('%s..param_desc',quant), ...
            'request-opt', '' }, varargin(2:end));
        request_fields = opt.get('request-fields');
        if iscell( request_fields)
            request_fields = sprintf('%s,', request_fields{:});
            request_fields(end)='';
        end
        request_fields_4_sql = regexprep( request_fields, '([,| ]*)([^(,| )]*date[^(,| )]*)([,| ]*)', '$1convert(char(10),$2,103)$3');
        if isempty(varargin) || isempty(varargin{1})
            rslt = exec_sql('QUANT', sprintf('select %s from %s %s', request_fields_4_sql, opt.get('request-table'), opt.get('request-opt')));
        else
            request = varargin{1};
            if ~ischar( request)
                request = num2str( request);
            else
                request = sprintf('''%s''', request);
            end
            rslt= exec_sql('QUANT', sprintf('select %s from %s where %s=%s %s',  request_fields_4_sql, opt.get('request-table'), opt.get('input-field'), request, opt.get('request-opt') ));
        end
        switch lower(opt.get('output-type'))
            case 'struct'
                request_fields = lower(strtrim(tokenize( request_fields, ',')));
                request_fields{1} = strtrim(strrep( request_fields{1}, 'distinct ', ''));
                tmp = arrayfun(@(i){request_fields{i},rslt(:,i)},1:length(request_fields), 'uni', false);
                tmp = [tmp{:}];
                out = struct(tmp{:});
            case { 'list', 'cell', 'cellarray'}
                % nothing to do
                out = rslt;
                out_plus = strtrim(tokenize( request_fields, ','));
            otherwise
                error('se_db_estimator:list_output_type', 'output mode <%s> unknown', opt.get('output-type'));
        end
        %>*
    
    case 'get_effective_domain_type'
        % La notation de domain_type pour un perimetre n'existe pas
        % vraiment. Mais de fait il n'y a qu'un domain_type pour tous les
        % domaines d'un perimetre.
        perim_id = varargin{1};
        q = ['select distinct t.domain_type_name',...
            ' from ',quant,'..domain_type t,',quant,'..perimeter_domain p,',...
            quant,'..domain d',...
            ' where p.perimeter_id = ',num2str(perim_id),...
            ' and p.domain_id = d.domain_id',...
            ' and t.domain_type_id = d.domain_type_id'];
        out = char(exec_sql('QUANT',q));
        
    otherwise
        error('se_db:exec', 'mode <%s> unknown', mode);
end


% %%** Embedded functions

%<* shortcut to list_**
    function [out, out_plus] = LIST_EE( opt, u)
        lst = opt.get();
        if ~isempty(u)
            u = u{1};
        end
        [out, out_plus] = se_db( 'list_*', u, lst{:});
    end
%>*
end



