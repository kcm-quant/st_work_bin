function domain_class  = se_domain_class(varargin)
% SE_DOMAIN_CLASS - fonction qui renvoie le type du domain dans le SE (valeur/indice/groupe de cotation)
%
% INPUT  : peut �tre au choix domain_id/job_id/run_id/
% OUTPUT :
%
% exemple:
% se_domain_class ('domain_id', 19)
% se_domain_class ('job_id' , 16985)
% se_domain_class ('run_id', 1343792)
%
% use in get_se

% author:  'Dana Croiz�'
% version: '1.0'
% date:    '15/09/2010'



global st_version
q_base = st_version.bases.quant;

opt = options({ 'job_id' , [] ,...
    'run_id' , [] , ...
    'domain_id',[]}, varargin);

job_id = opt.get('job_id');
run_id = opt.get('run_id');
domain_id = opt.get('domain_id');



if isempty(job_id) && isempty(run_id) && isempty(domain_id)
    error ('you have to enter one of those : domain_id /job_id / run_id  ')
end

sum_arg =sum( [~isempty(job_id) ~isempty(run_id) ~isempty(domain_id)]);

if sum_arg >1
    error ('you have to choose ONLY one of those : domain_id /job_id / run_id  ')
end


if ~isempty(job_id) || ~isempty(run_id)
    if ~isempty (job_id)
        cond_subset = sprintf('est_r.job_id = %d AND ' , job_id ) ;
        col_subset = 'est_r.job_id ';
        colnames = {'job_id','domain_type_name'};
        
    elseif  ~isempty(run_id)
        cond_subset = sprintf('est_r.run_id = %d AND ' , run_id ) ;
        col_subset = 'est_r.run_id ';
        colnames = {'run_id','domain_type_name'};
        
    end
    
    res  = exec_sql('QUANT', sprintf( [ ...
        'SELECT DISTINCT %s, dt.domain_type_name ' ...
        'FROM ' ...
        '%s..estimator_runs  est_r , ' ...
        '%s..domain dom , ' ...
        '%s..domain_type dt ' ...
        'WHERE ' ...
        '%s  ' ...
        'est_r.domain_id= dom.domain_id AND ' ...
        'dom.domain_type_id = dt.domain_type_id ' ], col_subset , q_base, q_base, q_base , cond_subset));
   
    
elseif ~isempty(domain_id)
    res  = exec_sql('QUANT', sprintf( [ ...
        'SELECT DISTINCT dom.domain_id ,dt.domain_type_name  ' ...
        'FROM ' ...
        '%s.. domain dom , ' ...
        '%s..domain_type dt ' ...
        'WHERE ' ...
        'dom.domain_id = %d  AND ' ...
        'dom.domain_type_id = dt.domain_type_id' ],q_base,q_base, domain_id));
    colnames = {'domain_id','domain_type_name'};
    
end

if ~isempty(res)
    
    %%%%ATTENTION EN DUR A METTRE A JOUR REGULIEREMENT.....
    
    domain_class = st_data('from-cell', 'domain_class', colnames, res);
    book = codebook( 'get', domain_class.codebook, 'domain_type_name');
    
    %cr�ation d'un codebook :stock/index/cotation group/default
    [vals, cdk] = codebook('build', {'stock','index','cotation_group' , 'default'});
    cdk.colname = 'class';
    domain_class.codebook(end+1) = cdk;    
    
    tmp = book(st_data('cols', domain_class,'domain_type_name'));
    switch tmp{1}
        case 'mono instrument'          %stock
            val = 4;
            
        case 'representative index'     % cotation group
            val = 1;
            
        otherwise
            val = 3;      %index
    end  
    domain_class = st_data('add-col', domain_class, val, 'class');
    
    if ~isempty(run_id)
        stamp_date = datenum(getfield(se_db('get_estimator_run' , 'run_id' , st_data('cols',domain_class, 'run_id')),'stamp_date'));
        domain_class.date = stamp_date;
    end
    
else
    domain_class = st_data( 'init', 'title', 'domain_class', 'value',  [NaN NaN 2], ...
                   'date', 1, 'colnames', { 'domain_type_name', 'run_id' , 'class' }) ; 
    
    %cr�ation d'un codebook :stock/index/cotation group/default
    [vals, cdk] = codebook('build', {'stock','index','cotation_group' , 'default'});
    cdk.colname = 'class';
    domain_class.codebook = cdk;

end
end
