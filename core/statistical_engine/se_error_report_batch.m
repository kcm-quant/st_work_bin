
CONNEXION = {'production', 'homolo'};
BASE = {'quant', 'quant_homolo'};



for i=1:2
   
    change_connections(CONNEXION{i});
    
    st_version.bases.quant = BASE{i};
    sv = st_version.bases.quant;
    
    estimator_id = cell2mat(exec_sql('QUANT', sprintf('SELECT estimator_id FROM %s..estimator', sv)));
    
    for j=1:length(estimator_id)
        
        se_error_report('last', 'yes', 'is-run', 1, 'estimator-id', estimator_id(j));
        se_error_report('last', 'yes', 'is-run', 0, 'estimator-id', estimator_id(j));

    end

end

exit;

