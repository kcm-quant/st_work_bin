function res = se_get_params_perim(perimeter_file)

global st_version;
sv = st_version.bases.quant;
          
out = exec_sql('QUANT', sprintf('SELECT perimeter_name , perimeter_id  from %s..perimeter where request = ''%s'' ', sv,perimeter_file));
colnames = {'perimeter_name', 'perimeter_id' };

idx_p_name  = strmatch( 'perimeter_name', colnames, 'exact');
PERIMETER_NAME = cell2mat(out(:,idx_p_name));
      
res = exec_sql('QUANT' , sprintf('SELECT parameters FROM %s..perimeter WHERE perimeter_name=''%s''', sv, PERIMETER_NAME));

end