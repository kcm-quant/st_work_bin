
%%%%%%%%%%%%%%%
%MAIN FUNCTION%
%%%%%%%%%%%%%%%

function se_gui

% Figure
figure('units','pixels',...
    'position',[250 250 800 500],...
    'color',[0.925 0.913 0.687],...
    'numbertitle','off',...
    'name','Statistical Engine GUI',...
    'menubar','none',...
    'tag','interface');

% Button View
uipanel('units', 'pixels',...
    'position', [20 453 510 40], ...
    'BackgroundColor',[0.925 0.913 0.687]);

uicontrol('style','popupmenu',...
    'string',{'estimator', 'context', 'parameter','domain','job','result', 'check'},...
    'tag', 'view_opt', ...
    'value',1, ...
    'position',[30 465 100 20]);

uicontrol('style','text',...
    'position',[140 463 80 20],...
    'string','that contains', ...
    'BackgroundColor',[0.925 0.913 0.687]);

uicontrol('style','edit',...
    'position',[240 465 200 20],...
    'string','',...
    'HorizontalAlignment', 'left', ...
    'tag','search');

uicontrol('style','pushbutton',...
    'position',[460 465 60 20],...
    'string','View',...    
    'callback',@view);

% Button Delete
uipanel('units', 'pixels',...
    'position', [550 453 230 40], ...
    'BackgroundColor',[0.925 0.913 0.687]);

uicontrol('style','popupmenu',...
    'string',{'finished jobs', 'cancelled jobs', 'failed jobs','queued jobs','pending jobs','running jobs', 'all jobs'},...
    'tag', 'delete_opt', ...
    'value',1, ...
    'position',[560 465 130 20]);
 
uicontrol('style','pushbutton',...
    'position',[710 465 60 20],...
    'string','Delete',...    
    'callback',@delete);

uicontrol('style','text',...
    'position',[550 420 230 20],...
    'string','', ...
    'tag','delete_label', ...
    'HorizontalAlignment', 'center', ...
    'BackgroundColor',[0.925 0.913 0.687]);

% Result
uicontrol('style','edit',...
    'position',[20 420 510 20],...
    'string','', ...
    'tag','result_label', ...
    'HorizontalAlignment', 'center', ...
    'BackgroundColor',[0.925 0.913 0.687]);

uicontrol('style','edit',...
    'position',[20 20 760 400],...
    'string','',...
    'HorizontalAlignment', 'left', ...
    'tag','result', ...
    'min', 0, 'max', 1000);




%%%%%%%%%%%%%%%
%VIEW FUNCTION%
%%%%%%%%%%%%%%%

function view(obj,event)

% Get option value
h=findobj('tag', 'view_opt');
val = get(h,'value');
opt = get(h,'string');

% Get filter value
h=findobj('tag', 'search');
filter = get(h,'string');

% Do query
f = xmltools('se_gui_sql.xml');
f = xmltools(f.get_tag(opt{val}));
sql_rslt = exec_sql('QUANT', f.get_tag_value('query'));
label = f.get_tag_value('label');

%Print result label
h=findobj('tag', 'result_label');
set(h,'string', regexprep(label, ';', '|'));

% Print result
rslt = '';
for r=1:size(sql_rslt, 1)
    ln = '';
    for c=1:size(sql_rslt, 2)
        if ~ischar(sql_rslt{r, c})
            ln = sprintf('%s%d | ', ln, sql_rslt{r, c});
        else
            ln = sprintf('%s%s | ', ln, sql_rslt{r, c});
        end
    end
    if isempty(filter) || ~isempty(regexpi(ln, filter))
        rslt = sprintf('%s%s\n', rslt, ln);
    end
end
h=findobj('tag','result');
set(h,'string', rslt);



%%%%%%%%%%%%%%%%%
%DELETE FUNCTION%
%%%%%%%%%%%%%%%%%

function delete(obj,event)

h=findobj('tag', 'delete_opt');
val = get(h,'value');
opt = get(h,'string');

switch(opt{val})
    case 'finished jobs'
        to_destroy = {'finished'};
        
    case 'cancelled jobs'
        to_destroy = {'cancelled'};
        
    case 'failed jobs'
        to_destroy = {'destroy'};
        
    case 'queued jobs'
        to_destroy = {'queued'};
        
    case 'pending jobs'
        to_destroy = {'pending'};
        
    case 'running jobs'
        to_destroy = {'running'};
        
    case 'all jobs'
        to_destroy = {'finished', 'cancelled', 'failed', 'queued', 'pending', 'running'};
end

% Get option value
h=findobj('tag','delete_label');
jm = findResource('scheduler','type','local');
job = findJob(jm);
all_state = {'finished', 'cancelled', 'failed', 'queued', 'pending', 'running'};
for i=1:length(all_state)
    if ismember(all_state{i}, to_destroy)
        for j=1:length(job)
            if strcmp(get(job(j), 'state'), 'finished')
                set(h,'string', sprintf('Destroying job %s...', get(job(j), 'name')));
                destroy(job(j));
            end
        end
    end
end
set(h,'string', 'Done');
