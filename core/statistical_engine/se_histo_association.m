function se_histo_association(varargin)
%SE_HISTO_ASSOCIATION : fonction qui g�re l'historisation de la table d'association  
%
% author:  'Dana Croiz�'
% version: '1.0'
% date:    '15/09/2010'

global st_version;
sv = st_version.bases.quant ;

%% DELETE
%R�cup�ration des associations supprim�es et mise � jour 
%de la table histo_association 

try
    exec_sql('QUANT' , sprintf( [...
        'SELECT context_id, rank, security_id, trading_destination_id, estimator_id, varargin, from_meta ' ...
        'INTO #removed  ' ...
        'FROM %s..last_association ' ],sv));
catch er
    if isempty(strfind( er.message, 'JZ0R2'))
        rethrow( er);
    end
end

try
    exec_sql('QUANT' , sprintf( [...
        'DELETE #removed  ' ...
        'FROM #removed r , %s..association ass ' ...
        'WHERE ' ...
        'ass.context_id = r.context_id AND ' ...
        'ass.rank = r.rank AND ' ...
        'isnull(ass.security_id, -1) = isnull(r.security_id, -1) AND  ' ...
        'isnull(ass.trading_destination_id, -1) = isnull(r.trading_destination_id, -1) AND ' ...
        'isnull(ass.varargin, '''') = isnull(r.varargin, '''')  AND ' ...
        'ass.from_meta = r.from_meta AND  ' ...
        'ass.estimator_id= r.estimator_id '], sv))
catch er
    if isempty(strfind( er.message, 'JZ0R2'))
        rethrow( er);
    end 
end

%Liste des associations supprim�es
deleted_job = exec_sql('QUANT' , sprintf('SELECT * FROM #removed r'));

try
    exec_sql('QUANT' , sprintf('DROP TABLE #removed '))
catch er
    if isempty(strfind( er.message, 'JZ0R2'))
        rethrow( er);
    end    
end

%Mise � jour de la table histo_association 
for i = 1 : size(deleted_job ,1)
    deleted_job_list = deleted_job(i,:);
    context_id  =  deleted_job_list{1};
    rank  =  deleted_job_list{2};
    security_id  =  sql_wash_nan(deleted_job_list{3});
    trading_destination_id  =  sql_wash_nan(deleted_job_list{4});
    estimator_id  =  deleted_job_list{5};
    varargin_  =  deleted_job_list{6};
    from_meta  =  deleted_job_list{7};
    
    if strcmpi(varargin_,'null')
        subrequest = sprintf(' varargin is null AND ');
    else
        subrequest = sprintf('varargin = ''%s''  AND ', varargin_);       
    end
    
    histo_association_id  = exec_sql('QUANT' , sprintf( [...
        'SELECT histo_association_id ' ...
        'FROM %s..histo_association ' ....
        'WHERE ' ...
        'context_id = %d AND ' ...
        'rank = %d AND ' ...
        'security_id = %s and ' ...
        'trading_destination_id = %s  AND ' ...
        'estimator_id = %d AND ' ...
        '%s '...
        'from_meta = %d AND ',...
        'end_date is null'], sv, context_id,rank,security_id,trading_destination_id,estimator_id,subrequest,from_meta ));
    
    %update du champs end_date
    if ~isempty(histo_association_id)
        exec_sql('QUANT' , sprintf( [...
            'UPDATE %s..histo_association ' ...
            'SET end_date = ''%s'' ' ...
            'WHERE ' ...
            'histo_association_id = %d '] ,sv , datestr(today,'yyyymmdd'),histo_association_id{1}))
    else
        st_log('WARNING: <se_histo_association> association(%s,%s,%s,%d) is unknown',security_id, trading_destination_id, varargin_, estimator_id)
    end
    
end

%% INSERT 
%R�cup�ration des nouvelles associations et insertion de ces 
%associations dans la table histo_association 

try 
exec_sql('QUANT' , sprintf( [...
    'SELECT context_id, rank, security_id, trading_destination_id, estimator_id, varargin, from_meta ' ...
    'INTO #inserted  ' ...
    'FROM %s..association  ' ],sv));
catch er
    if isempty(strfind( er.message, 'JZ0R2'))
        rethrow( er);
    end
end

try
exec_sql('QUANT' , sprintf( [...
    'DELETE #inserted   ' ...
    'FROM #inserted ins , %s..last_association l_ass  ' ...
    'WHERE  ' ...
    'l_ass.context_id = ins.context_id AND  ' ...
    'l_ass.rank = ins.rank AND  ' ...
    'isnull(l_ass.security_id, -1) = isnull(ins.security_id, -1) AND   ' ...
    'isnull(l_ass.trading_destination_id, -1) = isnull(ins.trading_destination_id, -1) AND  ' ...
    'isnull(l_ass.varargin, '''') = isnull(ins.varargin, '''')  AND  ' ...
    'l_ass.from_meta = ins.from_meta AND   ' ...
    'l_ass.estimator_id= ins.estimator_id ' ], sv));
catch er
    if isempty(strfind( er.message, 'JZ0R2'))
        rethrow( er);
    end
end

%Liste des nouvelles associations 
inserted_job = exec_sql('QUANT' , sprintf('SELECT * FROM #inserted '));

try
    exec_sql('QUANT' , sprintf('DROP TABLE #inserted '))
catch er
    if isempty(strfind( er.message, 'JZ0R2'))
        rethrow( er);
    end    
end

%Mise � jour de la table histo_association
for i = 1 : size(inserted_job   ,1)
    inserted_job_list = inserted_job(i,:);
    context_id  =  inserted_job_list{1};
    rank  =  inserted_job_list{2};
    security_id  =  sql_wash_nan(inserted_job_list{3});
    trading_destination_id  =  sql_wash_nan(inserted_job_list{4});
    estimator_id  =  inserted_job_list{5};
    varargin_  =  inserted_job_list{6};
    from_meta  =  inserted_job_list{7};
    
    if strcmpi(varargin_,'null')
        subrequest = sprintf(' varargin is null AND ');
    else
        subrequest = sprintf('varargin = ''%s''  AND ', varargin_);
    end
    
    job_id = cell2mat(exec_sql('QUANT' , sprintf( [...
        'SELECT job_id FROM %s..association '...
        'WHERE ' ...
        'context_id = %d AND ' ...
        'rank = %d AND ' ...
        'security_id = %s and ' ...
        'trading_destination_id = %s  AND ' ...
        'estimator_id = %d AND ' ...
        '%s '...
        'from_meta = %d ' ],sv,context_id,rank,security_id,trading_destination_id, estimator_id,subrequest,from_meta)));
    
    histo_association_id  = exec_sql('QUANT' , sprintf( [...
        'SELECT histo_association_id FROM %s..histo_association ' ....
        'WHERE ' ...
        'context_id = %d AND ' ...
        'rank = %d AND ' ...
        'security_id = %s and ' ...
        'trading_destination_id = %s  AND ' ...
        'estimator_id = %d AND ' ...
        'job_id = %d AND ' ...
        '%s '...
        'from_meta = %d AND ' ...
        'end_date is null '], sv, context_id,rank,security_id,trading_destination_id,estimator_id,job_id,subrequest,from_meta ));
    
    %INSERT dans la table histo_assocation des nouvelles associations
    if ~isempty(histo_association_id)
        st_log('WARNING: <se_histo_association> INSERT:: job_id <%d> ALREADY EXIST IN HISTO_ASSOCIATION \n',job_id);
    else
        if strcmpi('null' ,varargin_)
            exec_sql('QUANT' , sprintf( [...
            'INSERT INTO %s..histo_association ' ...
            '(context_id, rank, security_id, trading_destination_id, estimator_id, job_id, from_meta, beg_date, end_date ) ' ...
            'VALUES (%d,%d,%s,%s,%d,%d,%d,''%s'',null)'] ,sv ,context_id,rank,security_id,trading_destination_id,estimator_id,job_id,from_meta, datestr(today,'yyyymmdd')));
        
        else
            exec_sql('QUANT' , sprintf( [...
            'INSERT INTO %s..histo_association ' ...
            '(context_id, rank, security_id, trading_destination_id, estimator_id, job_id,varargin, from_meta, beg_date, end_date ) ' ...
            'VALUES (%d,%d,%s,%s,%d,%d,''%s'',%d,''%s'',null)'] ,sv ,context_id,rank,security_id,trading_destination_id,estimator_id,job_id,varargin_,from_meta, datestr(today,'yyyymmdd')));
        end
    end    
end

%% UPDATE : 
%R�cup�ration des associations updat�es.
% Puis dans la table histo_association : 
% 1/ update de la colonne "end_date" pour le job updat�
% 2/ insertion de la nouvelle association

updated_job = exec_sql('QUANT' , sprintf( [...
    'SELECT ass.context_id, ass.rank, ass.security_id, ass.trading_destination_id, ass.estimator_id, ass.job_id, ass.varargin, ass.from_meta , l_ass.job_id ' ...
    'FROM  %s..association ass, %s..last_association l_ass ' ...
    'WHERE  ' ...
    'ass.context_id = l_ass.context_id AND '...
    'ass.rank = l_ass.rank AND '...
    'isnull(ass.security_id, -1) = isnull(l_ass.security_id, -1) AND  ' ...
    'isnull(ass.trading_destination_id, -1) = isnull(l_ass.trading_destination_id, -1) AND  ' ...
    'isnull(ass.varargin, ''none'') = isnull(l_ass.varargin, ''none'') AND  ' ...
    'ass.from_meta = l_ass.from_meta AND  ' ...
    'ass.estimator_id= l_ass.estimator_id AND  ' ...
    'ass.job_id <>l_ass.job_id  ' ],sv ,sv ));

for i = 1 : size(updated_job   ,1)
    updated_job_list = updated_job(i,:);
    context_id  =  updated_job_list{1};
    rank  =  updated_job_list{2};
    security_id  =  sql_wash_nan(updated_job_list{3});
    trading_destination_id  =  sql_wash_nan(updated_job_list{4});
    estimator_id  =  updated_job_list{5};
    new_job_id  =  updated_job_list{6};
    varargin_  =  updated_job_list{7};
    from_meta  =  updated_job_list{8};
    old_job_id  =  updated_job_list{9};
    
    if strcmpi(varargin_,'null')
        subrequest = sprintf(' varargin is null AND ');
    else
        subrequest = sprintf('varargin = ''%s''  AND ', varargin_);
    end
    
    %mise � jour du champs "end_date"  dans la table histo_association
    %pour l'association qui a �t� modifi�e
    exec_sql('QUANT' , sprintf( [...
        'UPDATE %s..histo_association SET end_date = ''%s'' ' ...
        'WHERE histo_association_id in(  ' ...
        'SELECT histo_association_id ' ...
        'FROM %s.. histo_association ' ...
        'WHERE ' ...
        'context_id = %d AND ' ...
        'rank = %d AND ' ...
        'security_id = %s and ' ...
        'trading_destination_id = %s  AND ' ...
        'estimator_id = %d AND ' ...
        'job_id = %d AND ' ...
        '%s  '...
        'from_meta = %d AND ' ...
        'end_date is null )' ], sv, datestr(today , 'yyyymmdd') ,sv , context_id, rank, security_id, trading_destination_id, estimator_id,old_job_id, subrequest, from_meta));
    
    %v�rification si la nouvelle association existe d�j� dans la table
    %histo_assiciation
    job_exist = exec_sql('QUANT' , sprintf( [...
        'SELECT  histo_association_id ' ...
        'FROM %s.. histo_association ' ...
        'WHERE ' ...
        'context_id = %d AND ' ...
        'rank = %d AND ' ...
        'security_id = %s and ' ...
        'trading_destination_id = %s  AND ' ...
        'estimator_id = %d AND ' ...
        'job_id = %d AND ' ...
        '%s '...
        'from_meta = %d AND ' ...
        'end_date is null '], sv, context_id, rank, security_id, trading_destination_id, estimator_id, new_job_id ,subrequest,from_meta));
    
    
    if isempty(job_exist)
        %INSERT de la nouvelle association dans la table histo_association
         if strcmpi('null' ,varargin_)
            exec_sql('QUANT' , sprintf( [...
            'INSERT INTO %s..histo_association ' ...
            '(context_id, rank, security_id, trading_destination_id, estimator_id, job_id, from_meta, beg_date, end_date ) ' ...
            'VALUES (%d,%d,%s,%s,%d,%d,%d,''%s'',null)'] ,sv ,context_id,rank,security_id,trading_destination_id,estimator_id,new_job_id,from_meta, datestr(today,'yyyymmdd')));
        
        else
            exec_sql('QUANT' , sprintf( [...
            'INSERT INTO %s..histo_association ' ...
            '(context_id, rank, security_id, trading_destination_id, estimator_id, job_id,varargin, from_meta, beg_date, end_date ) ' ...
            'VALUES (%d,%d,%s,%s,%d,%d,''%s'',%d,''%s'',null)'] ,sv ,context_id,rank,security_id,trading_destination_id,estimator_id,new_job_id,varargin_,from_meta, datestr(today,'yyyymmdd')));
         end
    else
        st_log('WARNING!!! <se_histo_association> UPDATE::job_id <%d> ALREADY EXIST IN HISTO_ASSOCIATION \n', new_job_id);
    end
    
    
end
end

function s_nb=sql_wash_nan(nb)
if isnan(nb)
    s_nb = 'null';
else
    s_nb = num2str(nb);
end
end