function se_insert_association(mode, varargin)
% SE_INSERT_ASSOCIATION - Called from se_deploy XML files and from
% se_meta_update()
%
% se_insert_association( mode, $b, $r, $v, $e, $j [, context_num] )
%   where:
%    - $b: base
%    - $r: rank
%    - $v: domain_variable
%    - $e: estimator_id
%    - $j: job_id
%    - context_num: le context_num par d�faut
%
% and the 'mode' is into:
% - 'mono instrument' : context_num=1 for everyone
% - 'quotation_group'
% - 'mono instrument primary market'

global st_version
repository = st_version.bases.repository;

opt = options( { ...
    'quant_table', 'next_association' , ...
      },varargin(end-1:end));

   
table = opt.get('quant_table'); 
if ~strcmpi(table,'next_association')
    st_log('WARNING:::::::::::::::::::::::::::<%s> inserst associations in the TABLE : %s ' ,mfilename , table )
end

base  = varargin{1};
rank  = varargin{2};
domain_variable = varargin{3};
estimator_id = varargin{4};
job_id = varargin{5};

if nargin>6
    context_num = varargin{6};
else
    context_num = 1;
end

switch lower(mode)
    
    case 'mono instrument primary market'
        sendmail(st_version.my_env.se_admin,'Attention mode utilise dans se_insert_association',...
            'le mode <mono instrument primary market> de se_insert_association est utilis�');
%         % je duplique le job pour toutes les destinations possibles du
%         % security, and un context_num qui va bien...
%         tmp = tokenize(domain_variable, ',');
%         security_id = str2double(tmp{2});
%         % normalement la trading destination est vide
%         
%        context_via_td = cell2mat(exec_sql('BSIRIUS', sprintf(['SELECT distinct (trading_destination_id*10+1) ' ...
%             'FROM repository..security_source ' ...
%             'WHERE security_id = %d and trading_destination_id is not null '] , security_id )));
%         
%        context_via_td_str = sprintf('%d' , context_via_td(1));
%        for i = 2: length(context_via_td)
%            context_via_td_str = sprintf('%s ,%d' , context_via_td_str ,  context_via_td(i));
%        end
%         
%        context_td_id  = cell2mat(exec_sql('QUANT', sprintf(['SELECT context_id ,(context_num-1) /10  ' ...
%             'FROM %s..context ' ...
%             'WHERE context_num in (%s) and ' ...
%             'estimator_id = %d'],base, context_via_td_str ,estimator_id )));
%                      
%           
%         % pour chaque association
%         for a=1:size(context_td_id,1)
%             % association � ins�rer
%             context_id = context_td_id(a,1);
%             trading_destination_id = context_td_id(a,2);
%             % v�rification qu'elle n'xiste pas
%             association_id = cell2mat(exec_sql('QUANT', sprintf([ ...
%                 'SELECT %s_id FROM %s..%s WHERE ' ...
%                 'context_id=%d AND rank=%d AND security_id=%d AND trading_destination_id=%d AND estimator_id=%d AND job_id=%d'], ...
%                 table, base, table, context_id, rank, security_id, trading_destination_id, estimator_id, job_id)));
%             
%             if isempty(association_id)
%                 % si non, je la cr��e
%                 exec_sql('QUANT', sprintf([ ...
%                     'INSERT INTO %s..%s (context_id, rank, security_id, trading_destination_id, estimator_id, job_id, from_meta) ' ...
%                     'VALUES (%d, %d, %d, %d, %d, %d, 1)'], ...
%                     base, table, context_id, rank, security_id, trading_destination_id, estimator_id, job_id));
%             end
%         end
        
    case 'mono instrument'
        
        tmp = tokenize(domain_variable, ',');
        security_id = str2double(tmp{1});
        trading_destination_id = sql_wash_nan(tmp{2});
        
        context_id = cell2mat(exec_sql('QUANT',...
            ['SELECT context_id',...
            ' FROM ',base,'..context',...
            ' WHERE estimator_id=',num2str(estimator_id),...
            ' and context_num=',num2str(context_num)]));
        
        % Insertion de l'association
        association_id = cell2mat(exec_sql('QUANT',['SELECT ',table,'_id',...
            ' FROM ',base,'..',table,' WHERE context_id=',num2str(context_id),...
            ' AND rank=',num2str(rank),...
            ' AND security_id=',num2str(security_id),...
            ' AND trading_destination_id ',trading_destination_id,...
            ' AND estimator_id=',num2str(estimator_id),...
            ' AND job_id=',num2str(job_id)]));
        
        if isempty(association_id)
            exec_sql('QUANT',sprintf(['INSERT INTO ',base,'..',table,...
                ' (context_id, rank, security_id, trading_destination_id, estimator_id, job_id, from_meta) ' ...
                'VALUES (%d, %d, %d, %s, %d, %d, 1)'], ...
                context_id, rank, security_id, regexprep(trading_destination_id,'=|( is )',''), estimator_id, job_id));
        end
        
        
        
        
    case 'quotation group'
        tmp = tokenize(domain_variable, ',');
        exchgid = tmp{1};
        varargin_ = tmp{2};
        trading_destination_id = cell2mat(exec_sql('KGR',['select distinct EXCHANGE',...
            ' from ',repository,'..[EXCHANGEREFCOMPL]',...
            ' where EXCHGID = ''',exchgid,'''']));
        context_id = cell2mat(exec_sql('QUANT',['SELECT context_id',...
            ' FROM ',base,'..context',...
            ' WHERE estimator_id=',num2str(estimator_id),...
            ' and context_num=',num2str(context_num)]));
        
        % Insertion de l'association
        association_id = cell2mat(exec_sql('QUANT',sprintf([ ...
            'SELECT ',table,'_id FROM ',base,'..',table,' WHERE ' ...
            'context_id=%d AND rank=%d AND security_id is NULL AND trading_destination_id=%d AND estimator_id=%d AND job_id=%d ' ...
            'AND varargin=%s'], ...
            context_id, rank, trading_destination_id, estimator_id, job_id, varargin_)));
        
        if isempty(association_id)
            exec_sql('QUANT', sprintf([ ...
                'INSERT INTO ',base,'..',table,...
                ' (context_id, rank, security_id, trading_destination_id, estimator_id, job_id, varargin, from_meta) ' ...
                'VALUES (%d, %d, NULL, %d, %d, %d, %s, 1)'], ...
                context_id, rank, trading_destination_id, estimator_id, job_id, varargin_));
        end
        
        
        
    otherwise
        
        error('se_insert_association:mode', '<%s> unknown', lower(mode));
        
        
        
end

end
function s_nb=sql_wash_nan(nb)
if isnan(nb)
    s_nb = ' is null';
elseif ischar(nb)
    if strcmp(nb,'null')
        s_nb = ' is null';
    else
        s_nb = ['=',nb];
    end
else
    s_nb = ['=',num2str(nb)];
end
end