% SE_INSTALL - install the SE
%
% ./matlab -nodisplay -c license.dat -r se_start_mdce
%
% See also se_main, se_stop
%
% Author   'Edouard d'Archimbaud'
% Version  '1.0'
% Reviewer ''
% Date     '30/01/2008'



xmlfile  = xmltools('se_install.xml');


% Perimeters
% **********
% perimeters = xmltools(xmlfile.get_tag('perimeters'));
% perimeter = xmltools(perimeters.get_tag('perimeter'));
% for i=1:perimeter.nb_children()
%     p = xmltools(perimeter.keep_children(i));
%     se_load_perimeter_xml(p.get_attrib_value('perimeter','file'));
%     params = se_get_params_perim(p.get_attrib_value('perimeter','file'));
%     feval(str2func(p.get_attrib_value('perimeter','file')), 'update' ,'params',params{:} );
% end
% 
% % Estimators
% % **********
% estimators = xmltools(xmlfile.get_tag('estimators'));
% estimator = xmltools(estimators.get_tag('estimator'));
% for i=1:estimator.nb_children()
%     e = xmltools(estimator.keep_children(i));
%     se_load_xml(e.get_attrib_value('estimator','file'));
% end
% 
% % iChecks
% % **********
% % iChecks = xmltools ( xmlfile.get_tag ( 'iChecks' ) );
% % iCheck = xmltools ( iChecks.get_tag ( 'iCheck' ) );
% % for i = 1 : iCheck.nb_children ()
% %     e = xmltools(iCheck.keep_children(i));
% %     se_integrity_load_xml ( fullfile ( getenv ( 'st_work' ), 'bin','iCheck', e.get_attrib_value ( 'iCheck', 'file' ) ) )
% % end
% 
% % functional needs
% ****************
functional_needs = xmltools ( xmlfile.get_tag ( 'functional_needs' ) );
functional_need = xmltools ( functional_needs.get_tag ( 'functional_need' ) );
for i = 1 : functional_need.nb_children ()
    e = xmltools(functional_need.keep_children(i));
    se_load_functional_need('functional_need',e.get_attrib_value('functional_need','name'),...
        'estimator_name',e.get_attrib_value('functional_need','estimator_name'),...
        'ranking',e.get_attrib_value('functional_need','ranking'))
end


se_meta_update();

exit;

