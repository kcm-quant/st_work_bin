function se_kill_daemon()
% SE_KILL_DAEMON - d�truit les processus du MDCE qui tournent depuis trop
% longtemps


global st_version


sv = st_version.bases.quant;

% G�n�ration de la date du jour
threshold = st_version.my_env.killer_thld;
jobmanager = st_version.my_env.jobmanager;


js = exec_sql('QUANT', sprintf( ...
    'SELECT job_status_id, job_name, beg_date, beg_time FROM %s..job_status where status=''running''', ...
    sv));

for i=1:size(js, 1)
    if now - (datenum(js{i, 3}, 'yyyy-mm-dd') + mod(datenum(js{i, 4}, 'HH:MM:SS'), 1)) > threshold
        se_monitor('destroy', 'name', js{i, 2}, ...
            'jm', findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL',jobmanager));
        se_db('update_job_status', 'killed', datestr(floor(now)), datestr(now, 'HH:MM:SS'), js{i,1});
               
    end
end


% TODO : explicitly route to ITRS log!
%LOG ITRS
if size(js, 1)>0
    st_log(sprintf('%s-MECANIC-KILLED_JOBS: WARNING : %d jobs matlab have been killed ' , datestr(now , 'yyyymmdd'), size(js, 1)))
end

end
