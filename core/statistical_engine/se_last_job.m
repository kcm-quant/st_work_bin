function se_last_job(job_list)
% SE_LAST_JOB - fonction qui se lance lorsque tous les autres job du SE
% sont termin?s

global st_version
sv = st_version.bases.quant;

st_log('$out$', fullfile(getenv('st_repository'), 'log', sprintf('se_last_job_%s.txt' , datestr(now,'ddmmyyyy_HHMM'))));

try
    
    for i=1:length(job_list)
        st_log('Job %d finished at %s\n', i, datestr(now));
        wait(job_list{i}, 'finished');
    end
    
    % DEBUT AJOUT DE CODE
    % %%%%%%%%%%%%%%%%%%%
    %Proc?dure de validation pour les differents estimateurs et generation des
    %rapport d'erreurs
    
    mail = exec_sql('QUANT', sprintf('SELECT estimator_id, owner, estimator_name , is_check_validation FROM %s..estimator', sv));
    
    if ispc()
        blank_char = sprintf('\n');
    else
        blank_char = sprintf('\\n');
    end
    
    %Validation/ %G?n?ration des rapports d'erreurs
    for k=1:size(mail, 1)
        try
            
            switch mail{k, 4}
                case 0
                    if time_to_validate('run_validation', mail{k, 3})
                        se_proc_validation_run('estimators', mail{k, 3});
                        se_meta_update('meta-association', 'estimator' , mail{k, 3});
                        se_devalidation_orphans_inactif_jobs('estimator' , mail{k, 3});
                        
                    end
                case 1
                    if time_to_validate('check_validation' , mail{k, 3})
                        se_proc_validation_run('estimators', mail{k, 3});
                        se_meta_update('meta-association', 'estimator' , mail{k, 3});
                        se_devalidation_orphans_inactif_jobs('estimator' , mail{k, 3});
                    end
                    
            end
            
            if (time_to_report_error( mail{k, 3}, 'is-run' , 1 ))
                se_error_report('last', 'yes', 'mail', mail{k, 2}, 'is-run', 1, 'estimator-id', mail{k, 1});
            end
            
            if (time_to_report_error( mail{k, 3}, 'is-run' , 0 ))
                se_error_report('last', 'yes', 'mail', mail{k, 2}, 'is-run', 0, 'estimator-id', mail{k, 1});
            end
        catch e
            st_log(sprintf('ERROR : %s\n', e.message));
            
            %ITRS
            st_log(sprintf('%s-VALIDATION-VALIDATION_UPDATE: CRITICAL : Failed during the validation process -estimator_id<%d>-' , datestr(now , 'yyyymmdd') ,mail{k, 1}));
            
            error_message = se_stack_error_message(e);
            if ~ispc()
                error_message = regexprep(error_message, '\n', '\r');
            end
            %envoie de mail
            identification_err = sprintf('%s%s%s', 'ECHEC DANS LA VALIDATION/GENERATION DES RAPPORTS D ERREUR POUR L''ESTIMATEUR SUIVANT:',sprintf('%s%s',upper(mail{k, 3}),blank_char),error_message);
            subject = sprintf('Echec dans la validation ou generation des rapports d erreurs pour l estimateur: %s %s' ,upper(mail{k, 3}),hostname());
            
            sendmail(mail{k, 2},subject,identification_err)
            
        end
        
        
    end
    
    
    
    
    % FIN AJOUT DE CODE
    % %%%%%%%%%%%%%%%%%
    
catch e
    st_log('%s\n', e.message);
end

st_log('SE_LAST_JOB ended at %s\n', datestr(now));
st_log('$close$');

end

