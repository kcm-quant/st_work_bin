function se_load_functional_need(varargin)
% SE_LOAD_FUNCTIONAL_NEED - Short_one_line_description
%
%
%
% See also: se_load_xml
%    
%
%   author   : 'mlasnier-ext@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  08/11/2011
global st_version
quant = st_version.bases.quant;

opt = options([{'functional_need','','estimator_name','','ranking',64},varargin]);
if strcmp(opt.get('functional_need'),'')
    error('se_load_functional_need:incorrect_function_call','Incorrect call to function SE_LOAD_FUNCTIONAL_NEED');
end
nb = cell2mat(exec_sql('QUANT',sprintf(['select count(*) from ',quant,'..functional_need_to_estimator',...
    ' where functional_need = ''%s'' and estimator_name = ''%s'''],...
    opt.get('functional_need'),opt.get('estimator_name'))));
if nb > 0
    query = sprintf(['update ',quant,'..functional_need_to_estimator',...
        ' set ranking = %s',...
        ' where functional_need = ''%s'' and estimator_name = ''%s'''],...
    opt.get('ranking'),opt.get('functional_need'),opt.get('estimator_name'));
else
    query = sprintf(['insert into ',quant,'..functional_need_to_estimator',...
        ' (functional_need,estimator_name,ranking)',...
        ' values (''%s'',''%s'',%s)'],opt.get('functional_need'),opt.get('estimator_name'),opt.get('ranking'));
end
exec_sql('QUANT',query);
