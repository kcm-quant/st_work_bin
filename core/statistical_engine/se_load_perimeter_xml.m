function r = se_load_perimeter_xml(filename)
% SE_LOAD_PERIMETER_XML - fill the perimeter table. Called by se_install...
% functions
%
%example:
% se_load_perimeter_xml('perim_lmi_mtd')
%
%
% see also se_install()
%
% Author: dcroize@cheuvreux.com
% Date: 02/02/2008
%



global st_version;

quant = st_version.bases.quant;


xmlfile  = xmltools(strcat(filename ,'.xml'));

perimeter = xmltools(xmlfile.get_tag('perimeter'));

% PERIMETER definition
% ********************

definition = xmltools(perimeter.get_tag('definition'));
perimeter_name = definition.get_attrib_value('param', 'PERIMETER_NAME');

perimeter_id = cell2mat(exec_sql('QUANT',['SELECT perimeter_id FROM ',quant,'..perimeter',...
    ' WHERE LOWER(perimeter_name)=''',lower(perimeter_name),'''']));

if isempty(perimeter_id)
    perimeter_comment = definition.get_attrib_value('param', 'PERIMETER_COMMENT');
    perimeter_frequency = definition.get_attrib_value('param', 'PERIMETER_FREQUENCY');
    perimeter_frequency_id = cell2mat(exec_sql('QUANT',['SELECT frequency_id',...
        ' FROM ',quant,'..frequency WHERE LOWER(frequency_name)=''',lower(perimeter_frequency),'''']));
    
    perimeter_frequency_date  = definition.get_attrib_value('param', 'PERIMETER_FREQUENCY_DATE');
    update_type  = definition.get_attrib_value('param', 'UPDATE_TYPE');
    update_type_id = cell2mat(exec_sql('QUANT',['SELECT update_type_id',...
        ' FROM ',quant,'..update_type WHERE LOWER(update_type_name)=''',lower(update_type),'''']));
    
    request  = definition.get_attrib_value('def', 'file');
    request_type_name = definition.get_attrib_value('param', 'REQUEST_TYPE_NAME');
    request_type_id = cell2mat(exec_sql('QUANT',[ ...
        'SELECT request_type_id FROM ',quant,'..request_type',...
        ' WHERE request_type_name=''',request_type_name,'''']));
    parameters = definition.get_attrib_value('def', 'parameters');
    parameters =  strrep(strrep(parameters, char(10), ' '), '''', '''''');
    
    exec_sql('QUANT',['INSERT INTO ',quant,'..perimeter (perimeter_name, comment, run_frequency_id, run_frequency_date, ' ...
        'update_type_id, request, request_type , parameters) ' ...
        'VALUES (''',perimeter_name,''', ''',perimeter_comment,''',',...
        num2str(perimeter_frequency_id),', ''',perimeter_frequency_date,''',',...
        num2str(update_type_id),', ''',request,''',',num2str(request_type_id),',''',parameters,''')'])
    
    
else
    st_log('perimeter %s does already exist!\n', perimeter_name);
    
    db_parameters = exec_sql('QUANT',[...
        'SELECT p.parameters, p.comment, f.frequency_name ' ...
        'FROM ',quant,'..perimeter p , ',quant,'.. frequency f ' ...
        'WHERE p.perimeter_name=''',lower(perimeter_name),''' AND ' ...
        'p.run_frequency_id = f.frequency_id ']);
    
    db_parameters_data = st_data('from-cell' , 'perimeter_params' , {'parameters' ,'comment' ,'frequency' },db_parameters);
    
    %Mise � jour des champs de la table perimeter si besoins est:
    for i = 1 : length(db_parameters_data.colnames)
        field = db_parameters_data.colnames{i};
        book = codebook( 'get', db_parameters_data.codebook, field );
        quant_params = book(st_data('cols',db_parameters_data, field  ));
        
        if strcmpi(field , 'parameters')
            xml_params = definition.get_attrib_value('def', 'parameters');
        else
            xml_params = definition.get_attrib_value('param', upper(sprintf('PERIMETER_%s',field)));
        end
        
        %Mise � jour des colonnes parameters/comment/frequency si le xml a
        %�t� modifi�
        if ~strcmp( strtrim(quant_params),strtrim(xml_params)) && ~isempty(xml_params)
            st_log(sprintf('updating %s ''s parameters ...field : %s \n', perimeter_name, field));
            parameters  = strrep(strrep(xml_params, char(10), ' '), '''', '''''');
            
            if strcmpi(field,'frequency')
                field = 'run_frequency_id';
                frequency_id = se_db('exist_frequency',parameters);
                
                exec_sql('QUANT',...
                    ['UPDATE ',quant,'..perimeter set ',field,' = ',num2str(frequency_id),...
                    ' WHERE perimeter_id = ',num2str(perimeter_id)]);
                
            else                
                exec_sql('QUANT', ...
                    ['UPDATE ',quant,'..perimeter set ',field,' = ''',parameters,'''',...
                    ' WHERE perimeter_id = ',num2str(perimeter_id)]);
            end
            
        end
    end
    
end


end