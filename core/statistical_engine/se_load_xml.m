function r = se_load_xml(filename)
% SE_LOAD_XML - creates estimators
%
% se_load_xml('se_deploy_example.xml')
%
% Author: mlasnier@keplercheuvreux.com
% Date: 17/07/2013
%
% See also se_load_xml_perim



global st_version;
quant = st_version.bases.quant;
repository = st_version.bases.repository;

xmlfile  = xmltools(filename);
estimator = xmltools(xmlfile.get_tag('estimator'));

% Estimator definition
% ********************
estimator_name = estimator.get_attrib_value('definition','name');
definition = xmltools(estimator.get_tag('definition'));

estimator_id = cell2mat(exec_sql('QUANT',['SELECT estimator_id',...
    ' FROM ',quant,'..estimator',...
    ' WHERE LOWER(estimator_name)=''',lower(estimator_name),'''']));

if isempty(estimator_id)
    comment = wash_sql(definition.get_tag_value('description'));
    email = definition.get_tag_value('email');
    matlab_run = definition.get_attrib_value('matlab', 'run');
    matlab_check = definition.get_attrib_value('matlab', 'check');
    output_type_name = definition.get_attrib_value('output', 'type');
    output_type_id = cell2mat(exec_sql('QUANT',['SELECT output_type_id',...
        ' FROM ',quant,'..output_type',...
        ' WHERE LOWER(output_type_name)=''',lower(output_type_name),'''']));
    exec_order = str2double(definition.get_attrib_value('phase', 'exec_order'));
    validation_context_num = str2double(definition.get_attrib_value('validation', 'context_num'));
    is_check_validation = str2double(definition.get_attrib_value('validation', 'is_check_validation'));
    if isnan(is_check_validation)
        is_check_validation = 0;
    end
    exec_sql('QUANT', sprintf(['INSERT INTO ',quant,'..estimator',...
        ' (estimator_name, comment, matlab_run, matlab_check, output_type_id, owner, exec_order, validation_context_num , is_check_validation)' ...
        ' VALUES (''%s'', ''%s'', ''%s'', ''%s'', %d, ''%s'', %d, %d ,%d)'],...
        estimator_name, ...
        comment, ...
        matlab_run, ...
        matlab_check, ...
        output_type_id, ...
        email,...
        exec_order, ...
        validation_context_num , ...
        is_check_validation));
    estimator_id = cell2mat(exec_sql('QUANT',['SELECT estimator_id',...
        ' FROM ',quant,'..estimator WHERE LOWER(estimator_name)=''',lower(estimator_name),'''']));
end

% Loop over parameters
parameter = xmltools(definition.get_tag('param'));
for i=1:parameter.nb_children()
    param = xmltools(parameter.keep_children(i));
    comment = wash_sql(param.get_tag_value('param'));
    name = param.get_attrib_value('param', 'name');
    x_value = str2double(param.get_attrib_value('param', 'x_value'));
    parameter_id = cell2mat(exec_sql('QUANT', ...
        ['SELECT parameter_id',...
        ' FROM ',quant,'..param_desc',...
        ' where parameter_name=''',name,'''',...
        ' and estimator_id=',num2str(estimator_id)]));
    if isempty(parameter_id)
        exec_sql('QUANT',sprintf(['INSERT INTO ',quant,'..param_desc',...
            ' (x_value, parameter_name, estimator_id, comment) ' ...
            'VALUES (%d, ''%s'', %d, ''%s'')'], ...
            x_value, ...
            name, ...
            estimator_id, ...
            comment));
    else
        exec_sql('QUANT',sprintf(['update ',quant,'..param_desc set x_value = %d,comment = ''%s''',...
            ' where parameter_id = %d'],x_value,comment,parameter_id));
    end
end

% Loop over contexts
context = xmltools(definition.get_tag('context'));
for i=1:context.nb_children()
    cont = xmltools(context.keep_children(i));
    comment = wash_sql(cont.get_tag_value('context'));
    name = cont.get_attrib_value('context', 'name');
    num = str2double(cont.get_attrib_value('context', 'num'));
    label     = cont.get_attrib_value('context', 'label');
    to_export = str2double(cont.get_attrib_value('context', 'to_export'));
    if isnan(to_export)
        to_export = 1;
    end
    context_id = cell2mat(exec_sql('QUANT', ...
        ['SELECT context_id',...
        ' FROM ',quant,'..context',...
        ' WHERE context_name=''',name,'''',...
        ' AND estimator_id=',num2str(estimator_id)]));
    if isempty(context_id)
        exec_sql('QUANT', sprintf(['INSERT INTO ',quant,'..context',...
            ' (context_name, estimator_id, context_num, comment,label,to_export) ' ...
            ' VALUES (''%s'', %d, %d, ''%s'',''%s'',%d)'],...
            name,...
            estimator_id,...
            num,...
            comment,...
            strrep(label,'''',''''''),...
            to_export));
    else
        exec_sql('QUANT',['update ',quant,'..context',...
            ' set context_num = ',num2str(num),',',...
            ' comment = ''',comment,''',',...
            ' label = ''',label,''' ,',...
            ' to_export = ',num2str(to_export),...
            ' where context_id = ',num2str(context_id)]);
    end
end

%loop over validation type
validation = xmltools(xmlfile.get_tag('validation'));
validation_type = xmltools(validation.get_tag('specific_validation'));
for k=1:validation_type.nb_children()
    validation_func = xmltools(validation_type.keep_children(k));
    pf  = validation_func.get_attrib_value('specific_validation','post');
    if ~isempty(pf)
        post_func = cell2mat(exec_sql('QUANT', ...
            ['SELECT book_id',...
            ' FROM ',quant,'..pre_post_validation_book',...
            ' WHERE estimator_id=',num2str(estimator_id),...
            ' AND matlab_func = ''',pf,''' ']));
        if isempty(post_func)
            vt = cell2mat(exec_sql('QUANT', ...
                ['SELECT validation_type_id',...
                ' from ',quant,'..validation_type',...
                ' where validation_type_name = ''POST_VALIDATION'' ' ]));
            exec_sql('QUANT', ...
                sprintf(['INSERT INTO ',quant,'..pre_post_validation_book',...
                ' ( matlab_func, estimator_id, validation_type_id)',...
                ' VALUES (''%s'', %d, %d)'],pf,estimator_id,vt));
        end
    end
end

% Meta-jobs
% *********
% Nettoyages de tables
exec_sql('QUANT',['DELETE FROM ',quant,'..meta_association WHERE estimator_id=',num2str(estimator_id)]);
exec_sql('QUANT',[ 'DELETE FROM ',quant,'..remove_job WHERE estimator_id=',num2str(estimator_id)]);
meta_job = xmltools(xmlfile.get_tag('metajob'));

% Boucle sur les meta-jobs
meta_job_id_list = nan(meta_job.nb_children(),1);
for i=1:meta_job.nb_children()
    mj = xmltools(meta_job.keep_children(i));
    setup = xmltools(mj.get_tag('setup'));
    perimeter_name = mj.get_attrib_value('perimeter', 'name');
    perimeter_id = cell2mat(exec_sql('QUANT',...
        ['SELECT perimeter_id FROM ',quant,'..perimeter WHERE LOWER(perimeter_name)=''',lower(perimeter_name),'''']));
    comment = wash_sql(mj.get_tag_value('comment'));
    varargin_ = mj.get_tag_value('varargin');
    modes = { 'run', 'check'};
    caracts = [];
    for m=1:length(modes)
        caracts.(modes{m}).frequency_id = cell2mat(exec_sql('QUANT',...
            ['SELECT frequency_id FROM ',quant,'..frequency',...
            ' WHERE frequency_name=''',setup.get_attrib_value(modes{m},'frequency'),'''']));
        caracts.(modes{m}).window_type_id = cell2mat(exec_sql('QUANT',...
            ['SELECT window_type_id FROM ',quant,'..window_type',...
            ' WHERE window_type_name=''',setup.get_attrib_value(modes{m},'window'),'''']));
        caracts.(modes{m}).width = setup.get_attrib_value(modes{m},'width');
        caracts.(modes{m}).date = setup.get_attrib_value(modes{m},'date');
    end
    
    meta_job_id = cell2mat(exec_sql('QUANT',['select meta_job_id from ',quant,'..meta_job',...
        ' where estimator_id = ',num2str(estimator_id),...
        ' and perimeter_id = ',num2str(perimeter_id),...
        ' and run_window_type_id = ',num2str(caracts.run.window_type_id),...
        ' and run_window_width = ',caracts.run.width,...
        ' and check_window_type_id = ',num2str(caracts.check.window_type_id),...
        ' and check_window_width = ',caracts.check.width,...
        ' and varargin = ''',varargin_,'''']));
    
    if isempty(meta_job_id)
        exec_sql('QUANT', sprintf(['INSERT INTO ',quant,'..meta_job',...
            ' (estimator_id, perimeter_id, comment, run_frequency_id,run_window_type_id, run_window_width, run_frequency_date, check_frequency_id, check_window_type_id,check_window_width, check_frequency_date, varargin) ',...
            ' VALUES (%d, %d, ''%s'', %d, %d, %s, ''%s'', %d, %d, %s, ''%s'', ''%s'')'], ...
            estimator_id, ...
            perimeter_id, ...
            comment, ...
            caracts.run.frequency_id, ...
            caracts.run.window_type_id, ...
            caracts.run.width, ...
            datestr(datenum(caracts.run.date, 'dd/mm/yyyy'), 'yyyy-mm-dd'), ...
            caracts.check.frequency_id, ...
            caracts.check.window_type_id, ...
            caracts.check.width, ...
            datestr(datenum(caracts.check.date, 'dd/mm/yyyy'), 'yyyy-mm-dd'), ...
            varargin_));
        meta_job_id = cell2mat(exec_sql('QUANT',['select meta_job_id from ',quant,'..meta_job',...
            ' where estimator_id = ',num2str(estimator_id),...
            ' and perimeter_id = ',num2str(perimeter_id),...
            ' and run_frequency_id = ',num2str(caracts.run.frequency_id),...
            ' and run_window_type_id = ',num2str(caracts.run.window_type_id),...
            ' and run_window_width = ',caracts.run.width,...
            ' and run_frequency_date = ''',datestr(datenum(caracts.run.date, 'dd/mm/yyyy'), 'yyyy-mm-dd'),'''',...
            ' and check_frequency_id = ',num2str(caracts.check.frequency_id),...
            ' and check_window_type_id = ',num2str(caracts.check.window_type_id),...
            ' and check_window_width = ',caracts.check.width,...
            ' and check_frequency_date = ''',datestr(datenum(caracts.check.date, 'dd/mm/yyyy'), 'yyyy-mm-dd'),'''',...
            ' and varargin = ''',varargin_,'''']));
        meta_job_id_list(i) = meta_job_id;
    else
        assert(length(meta_job_id)==1,'se_load_xml:load_meta_job',['Duplicated meta_job ',...
            '(estimator_id,perimeter_id) = (%d,%d)'],estimator_id,perimeter_id);
        exec_sql('QUANT',['update ',quant,'..meta_job',...
            ' set comment = ''',comment,''',',...
            ' run_frequency_id = ',num2str(caracts.run.frequency_id),',',...
            ' run_frequency_date = ''',datestr(datenum(caracts.run.date,'dd/mm/yyyy'),'yyyy-mm-dd'),''',',...
            ' check_frequency_id = ',num2str(caracts.check.frequency_id),',',...
            ' check_frequency_date = ''',datestr(datenum(caracts.check.date, 'dd/mm/yyyy'), 'yyyy-mm-dd'),'''',...
            ' where meta_job_id = ',num2str(meta_job_id)]);
        meta_job_id_list(i) = meta_job_id;
    end
    
    % Meta-associations
    % ****************
    domain = xmltools(mj.get_tag('domain'));
    % Boucle sur les m�ta-associations
    for j=1:domain.nb_children()
        dom = xmltools(domain.keep_children(j));
        domain_type = dom.get_attrib_value('domain','type');
        domain_type_id = cell2mat(exec_sql('QUANT',['SELECT domain_type_id',...
            ' FROM ',quant,'..domain_type',...
            ' WHERE LOWER(domain_type_name)=''',lower(domain_type),'''']));
        request = xmltools(dom.get_tag('domain'));
        request_type = request.get_attrib_value('request','type');
        request_type_id = cell2mat(exec_sql('QUANT',['SELECT request_type_id',...
            ' FROM ',quant,'..request_type',...
            ' WHERE LOWER(request_type_name)=''',lower(request_type),'''']));
        request_cmd  = strrep(strrep(dom.get_tag_value('request'), char(10), ' '), '''', '''''');
        exec_sql('QUANT', sprintf(['INSERT INTO ',quant,'..meta_association',...
            ' (estimator_id, domain_type_id, request_type_id, request,perimeter_id) ' ...
            'VALUES (%d, %d, %d, ''%s'', %d)'],estimator_id, domain_type_id, request_type_id, request_cmd,perimeter_id));
        
        % Remove job
        % **********
        removes = xmltools(dom.get_tag('removes'));
        remove = xmltools(removes.get_tag('remove'));
        for k=1:remove.nb_children()
            rem = xmltools(remove.keep_children(k));
            security_id = num2str(rem.get_attrib_value('remove','security_id'));
            if isempty(security_id)
                security_id = 'null';
            end
            trading_destination_id = num2str(rem.get_attrib_value('remove','trading_destination_id'));
            if isempty(trading_destination_id)
                trading_destination_id = 'null';
            end
            % on teste si la suppression existe d�j�
            remove_job_id = cell2mat(exec_sql('QUANT',...
                ['SELECT remove_job_id',...
                ' FROM ',quant,'..remove_job',...
                ' WHERE estimator_id=',num2str(estimator_id),...
                ' AND security_id=',num2str(security_id),...
                ' AND trading_destination_id=',num2str(trading_destination_id),...
                ' AND domain_type_id=',num2str(domain_type_id)]));
            if isempty(remove_job_id)
                exec_sql('QUANT', sprintf(['INSERT INTO ',quant,'..remove_job',...
                    ' (estimator_id, security_id, trading_destination_id, domain_type_id) ' ...
                    ' VALUES (%d, %s, %s, %d)'],estimator_id, security_id, trading_destination_id, domain_type_id));
            end
        end
    end
    
end

% Effa�age de tous les meta_job asssoci�s � l'estimateur = estimator_id et
% qui ne sont pas pr�sents dans le fichier xml.
if ~isempty(meta_job_id_list)
    str = sprintf('%d,',meta_job_id_list);
    exec_sql('QUANT',['delete from ',quant,'..meta_job',...
        ' where estimator_id = ',num2str(estimator_id),...
        ' and meta_job_id not in (',str(1:end-1),')']);
end


active_process = xmltools(xmlfile.get_tag('active_process'));
processes = xmltools(active_process.get_tag('process'));

for i = 1 : processes.nb_children()
    process = xmltools(processes.keep_children(i));
    name = process.get_attrib_value('process','name');
    is_active = str2double(process.get_attrib_value('process','is_active'));
    
    process_type_id = cell2mat(exec_sql('QUANT',['SELECT pt.process_type_id',...
        ' FROM ',quant,'..process_type pt',...
        ' WHERE upper(pt.process_type_name) = ''',upper(name),''' ']));
    
    process_id = cell2mat(exec_sql('QUANT', ...
        ['SELECT ep.process_id FROM ',quant,'..estimator_process ep, ',quant,'..process_type pt',...
        ' WHERE upper(pt.process_type_name)  = ''',upper(name),'''',...
        ' and pt.process_type_id = ep.process_type_id',... 
        ' and ep.estimator_id=',num2str(estimator_id)]));
    
    if isempty(process_id)
        exec_sql('QUANT',sprintf(['INSERT INTO ',quant,'..estimator_process',...
            ' (estimator_id, process_type_id, is_active) ' ...
            'VALUES (%d, %d, %d)'], ...
            estimator_id, ...
            process_type_id, ...
            is_active));
        
    else
        exec_sql('QUANT', sprintf(['UPDATE ',quant,'..estimator_process  set is_active = %d  '...
            'WHERE process_id = %d '],is_active,process_id));
    end
    
end

% Alimentation de la table event_action
event_actions = xmltools(xmlfile.get_tag('event_actions'));
event_actions = xmltools(event_actions.get_tag('event_action'));
for i = 1:event_actions.nb_children()
    event_action = xmltools(event_actions.keep_children(i));
    event = event_action.get_attrib_value('event_action','event');
    if ~strcmp(event,'')
        event_type = cell2mat(exec_sql('KGR',...
            ['select EVENTTYPE',... convert event into event_type
            ' from ',repository,'..CALEVENTTYPE',...
            ' where SHORTNAME = ''',event,'''']));
        if isempty(event_type)
            error('se_load_xml:unkown_event_name','Unkown event_name <%s>',event);
        end
    else
        event_type = nan;
    end
    context = event_action.get_attrib_value('event_action','context');
    if ~strcmp(context,'')
        context_id = cell2mat(exec_sql('QUANT',sprintf(['select context_id',... convert context_name into context_id
            ' from ',quant,'..context where context_name = ''%s'' and estimator_id = %d'],context,estimator_id)));
        if isempty(context_id)
            error('se_load_xml:unkown_context_name','Unkown context_name <%s> for estimator <%s>',context,estimator_name)
        end
    else
        context_id = nan;
    end
    default_ranking = event_action.get_attrib_value('event_action','default_ranking');
    if strcmp(default_ranking,'')
        default_ranking = 'null';
    end
    action = event_action.get_attrib_value('event_action','action');
    if ~any(strcmp(action,{'B','H','C'}))
        error('se_load_xml:unkown_action','Unkown action <%s>',action);
    end
    query = sprintf(['select count(*) from ',quant,'..event_action ea,',quant,'..context cont,',quant,'..estimator est',...
        ' where  ea.event_type = %d',...
        ' and ea.context_id = %d',...
        ' and ea.estimator_id = %d'],event_type,context_id,estimator_id);
    query = strrep(query,' = NaN',' is null');
    nb_event_action = cell2mat(exec_sql('QUANT',query));
    if nb_event_action > 0
        upd_query = sprintf(['update ',quant,'..event_action set action = ''%s'', default_ranking = %s',...
            ' where  event_type = %d',...
            ' and context_id = %d',...
            ' and estimator_id = %d'],...
            action,default_ranking,event_type,context_id,estimator_id);
        upd_query = strrep(upd_query,' = NaN',' is null');
        exec_sql('QUANT',upd_query);
    else
        ins_query = sprintf(['insert into ',quant,'..event_action',...
            '  (event_type, context_id, estimator_id, default_ranking, action)',...
            ' values (%d,%d,%d,%s,''%s'')'],event_type,context_id,estimator_id,default_ranking,action);
        ins_query = strrep(ins_query,'NaN','null');
        exec_sql('QUANT',ins_query);
    end
end
