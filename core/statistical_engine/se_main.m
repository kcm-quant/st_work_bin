function varargout = se_main(mode, varargin)
% SE_MAIN - main function of the statistical engine
%
% se_main('run')
% se_main('check')
%   while(1); se_main('run'); end;
%   while(1); se_main('check'); end;
%
% Examples:
% se_main('run', 'jm', 'local', 'main-date', datestr(now-7, 'dd/mm/yyyy'),...
% 'estimator_id', -1); % -1 wont work this estimator does not exist
%
% See also se_run, se_check
%
% Author   'edarchimbaud@cheuvreux.com'
% Version  '1.0'
% Reviewer ''
% Date     '24/10/2008'
%
% TODO: Gérer les remove_date

global st_version;

sv = st_version.bases.quant;

opt = options({ ...
    'jm', 'local', ...
    'main-date', datestr(now, 'dd/mm/yyyy'), ...
    'estimator_id', -1 ...
    'job-size', 20, ...
    'session-num', 0, ...
    'exec_phase', NaN, ...
    }, varargin);

 varargout = {{}};


if ischar(opt.get('jm'))
    switch lower(opt.get('jm'))
        case 'local'
            if verLessThan('matlab', '8.2')
                jm = findResource('scheduler','type','local');
            else
                jm = parcluster;
            end
        case 'thmlb'
            jm = findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL','tlhmlb003');
        case 'tcmlb'
            jm = findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL','tcmlb001');
    end
else
    jm = opt.get('jm');
end


job_size = opt.get('job-size');

%mail
address = exec_sql('QUANT', sprintf('SELECT owner from %s..estimator GROUP BY owner', sv));



switch (mode)
    case 'perimeter'
        try
            main_date = opt.get('main-date');
            % Perimeters to be updated
            perimeter_id = se_db('get_perimeter', datestr(datenum(main_date, 'dd/mm/yyyy'), 'yyyymmdd'));
            if isempty(perimeter_id)
                return
            end
            
            for i=1:size(perimeter_id, 1)
                try
                    request = exec_sql('QUANT', sprintf('SELECT request FROM %s..perimeter WHERE perimeter_id=%d', sv, perimeter_id(i)));
                    if strcmpi(request{1}, ' ') || isempty(request{1})
                        continue;
                    end
                    params = se_get_params_perim(request{1});
                    out = feval(request{1}, 'update','params',params{:});
                    
                    
                    if (out.is_successfully_update)
                        perimeter_info = exec_sql('QUANT', sprintf('SELECT run_frequency_date, run_frequency_id FROM %s..perimeter WHERE perimeter_id=%d', sv, perimeter_id(i)));
                        se_db('update_perimeter', perimeter_id(i), perimeter_info{1}, ...
                            perimeter_info{2}, main_date);
                    else
                        %ITRS
                        perimeter_name = cell2mat(exec_sql('QUANT', sprintf('SELECT perimeter_name FROM %s..perimeter WHERE perimeter_id=%d', sv, perimeter_id(i)))); 
                        st_log(sprintf('%s-PERIMETER-PERIMETER_UPDATE: WARNING : Fail to update the perimeter : %s' , datestr(now , 'yyyymmdd'), upper(perimeter_name)));

                    end
                catch e
                    st_log(sprintf('%s\n', e.message));
                    
                    %ITRS
                    st_log(sprintf('%s-PERIMETER-PERIMETER_UPDATE: ERROR : UNKOWN ERROR Fail to update perimeters' , datestr(now , 'yyyymmdd')))
                     
                    error_message = se_stack_error_message(e);
                    %envoie du mail
                    identification_err = sprintf('ATTENTION : erreur NON detectee lors de l''UPDATE du perimetre, a etudier...\n ->se_meta_update n''a pas tourne !\n%s',error_message);
                    subject =  sprintf('Erreur de mise a jour du perimetre : %s',upper(request{1}));
                    
                    for j=1:length(address)
                        se_sendmail( address{j},subject,identification_err)
                    end
                    
                end
            end
            se_meta_update('all');
            
        catch e
            st_log(sprintf('%s\n', e.message));
            
            %ITRS
            st_log(sprintf('%s-PERIMETER-PERIMETER_UPDATE: ERROR : Unknown error while updating SE perimeters' , datestr(now , 'yyyymmdd'))) ;
            
            subject =  sprintf('Perimeter:Erreur lancement SE from %s',hostname());
            error_message = se_stack_error_message(e);
            for j=1:length(address)
                se_sendmail( address{j},subject,error_message)
            end
        end
        
        varargout = {};
        
    case 'run'
        
        job_list = {}; % pour se_last_job
        
        try
            % Jobs to be launched selection
            run_job = se_db('get_run_job', datestr(datenum(opt.get('main-date'), 'dd/mm/yyyy'), 'yyyymmdd'));
            if isempty(run_job)
                return
            end
            
            active_estimator_id =  se_db('get_active_estimators','process','run','exec_phase',opt.get('exec_phase')); %récupération des estimateurs dont le process "run" dans la table estimator_process est actif
            if ~isempty(active_estimator_id)
                if opt.get('estimator_id') >-1
                    active_estimator_id = intersect(active_estimator_id,opt.get('estimator_id'));
                end
                
                idx = arrayfun(@(idx) find([run_job(:).estimator_id]== idx), active_estimator_id, 'Uni', false );
                run_job = run_job([idx{:}]);
                if isempty(run_job)
                    return
                end
                
                % Daily
                for i=1:job_size:size(run_job, 1)
                    %On d?coupe la liste des jobs en tranche de 100 jobs
                    nxt_idx = min(i+job_size-1, size(run_job, 1));
                    st_log(sprintf('Launching jobs from %d to %d\n', i, nxt_idx));
                    
                    job_list{end+1} = se_run('distrib', 'job_id', {run_job(i:nxt_idx).job_id}, 'job_manager', jm, 'run-date', opt.get('main-date'), ...
                        'session-num', opt.get('session-num')); %#ok<AGROW>
                end
            else
                st_log('<se_main :run> : no run will be executed, please check xml files for each estimator')
                 
                %ITRS
                st_log(sprintf('%s-START_SE-LAUNCH_RUN: WARNING : None run will be executed, please check xml files(tag:active_process) for each estimator ' , datestr(now , 'yyyymmdd'))) ;             
            end
        catch e
            st_log(sprintf('%s\n', e.message));
            
            %ITRS
            st_log(sprintf('%s-START_SE-LAUNCH_RUN: CRITICAL : Unknown error while launching RUN jobs' , datestr(now , 'yyyymmdd'))) ;
           
            subject =  sprintf('Run:Erreur lancement SE from %s',hostname());
            error_message = se_stack_error_message(e);
            for j=1:length(address)
                se_sendmail( address{j},subject,error_message)
            end
        end
        
        varargout = {job_list};
        
    case 'check'
        
        job_list = {}; % pour se_last_job
        
        try
            % Jobs to be launched selection
            check_run = se_db('get_check_job', datestr(datenum(opt.get('main-date'), 'dd/mm/yyyy'), 'yyyymmdd'));
            if isempty(check_run)
                return
            end
            
            active_estimator_id =  se_db('get_active_estimators','process','check','exec_phase',opt.get('exec_phase'));%récupération des estimateurs dont le process "check" dans la table estimator_process est actif
            if ~isempty(active_estimator_id)
                estimator_id = opt.get('estimator_id');
                if (estimator_id >-1)
                    if (ismember(estimator_id, active_estimator_id))
                        active_estimator_id = estimator_id;
                    end
                end
                
                idx = arrayfun(@(idx) find([check_run(:).estimator_id] == idx), active_estimator_id, 'Uni', false );
                check_run = check_run([idx{:}]);
                
                if isempty(check_run)
                    return
                end
%                 
%                 tmp = unique([check_run.job_id]);
%                 
%                 run_id = {};
%                 for i=tmp
%                     for k =0:1
%                         idx = find([check_run(:).job_id] == i);
%                         idx = idx(find([check_run(idx).is_last_run] == k));
%                         if ~isempty(idx)
%                             run_id{end+1} = [check_run(idx).run_id];
%                         end
%                     end
%                 end
%                 
                
                [~,~,idx_job] = unique([[check_run.job_id]',[check_run.is_last_run]'],'rows');
                run_id = accumarray(idx_job,[check_run.run_id]',[],@(x){x});
                
                % Daily
                for i=1:job_size:length(run_id)
                    %On d?coupe la liste des jobs en tranche de 100 jobs
                    nxt_idx = min(i+job_size-1, length(run_id));
                    st_log(sprintf('Launching check jobs from %d to %d\n', i, nxt_idx));
                    job_list{end+1} = se_check('distrib', 'run_id', {run_id{i:nxt_idx}}, 'job_manager', jm, 'check-date', opt.get('main-date'), ...
                        'session-num', opt.get('session-num')); %#ok<AGROW>
                    
                end
                
            else
                st_log('<se_main :check> : none check will be executed, please check xml files for each estimator')
                
                %ITRS
                st_log(sprintf('%s-START_SE-LAUNCH_CHECK: WARNING : None check will be executed, please check xml(tag:active_process)files for each estimator ' , datestr(now , 'yyyymmdd'))) ;   
                
            end
        catch e
            st_log(sprintf('%s\n', e.message));
            
            %ITRS
            st_log(sprintf('%s-START_SE-LAUNCH_CHECK: CRITICAL : Unknown error while launching CHECK jobs ' , datestr(now , 'yyyymmdd'))) ;
     
            
            subject =  sprintf('Check:Erreur lancement SE from %s',hostname());
            error_message = se_stack_error_message(e);
            for j=1:length(address)
                se_sendmail( address{j},subject,error_message)
            end
            
        end
        
    
        varargout = {job_list};
        
end

end

