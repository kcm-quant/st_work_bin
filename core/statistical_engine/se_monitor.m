function out=se_monitor(mode, varargin)
% SE_MONITOR - Monitoring of distributed tasks / errors
%
%
% Destroy
%   se_monitor('destroy', 'session-num', 1, 'jm', 'thmlb')
%   se_monitor('destroy', 'name', '4cf2feb1af54365099f30ec54dd9ec62')
%   se_monitor('destroy', 'status', 'finished', 'jm', 'thmlb')
%   se_monitor('destroy', 'session-num', 7, 'jm', 'thmlb')
%
% Display
%   se_monitor('disp-mdce', 'jm', 'thmlb')
%
% OLD
% se_monitor('cancell-job', 'obj', {21, 28, 32, 33, 37, 64, 68, 75, 76,...
% 107, 123, 159, 184})
% se_monitor('disp', 'obj', 'mdce-jobs')
% se_monitor('disp', 'obj', 'se-jobs', 'from', '01/11/2008', 'to', '07/11/2008', 'status', 'ended', 'est-name', 'se_run_volume_threshold')
% se_monitor('disp', 'obj', 'se-jobs')
% se_monitor('disp', 'obj', 'se-jobs', 'status', 'running')
% se_monitor('disp', 'obj', 'avg-perf')
% se_monitor('debug', 'from', 1, 'to', 50)
%
% See also se_main
%
% Author   'edarchimbaud@cheuvreux.com'
% Version  '1.0'
% Reviewer ''
% Date     '29/10/2008'

global st_version

opt = options({ ...
    'jm', 'local', ...
    'name', '', ...
    'status', '', ...
    'session-num', NaN, ...
    'obj', NaN, ...
    'from', NaN, ...
    'to', NaN, ...
    'est-name', NaN}, ...
    varargin);


if ischar(opt.get('jm'))
    switch lower(opt.get('jm'))
        case 'local'
            jm = findResource('scheduler','type','local');
        case 'tcmlb'
            jm = findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL','tcmlb001');
        case 'thmlb'
            jm = findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL','tlhmlb003');
    end
else
    jm = opt.get('jm');
end



switch lower(mode)
    
    case 'destroy'
        
        name = opt.get('name');
        status = opt.get('status');
        session_num = opt.get('session-num');
        job = findJob(jm);
       
        for i=1:length(job)
            n = get(job(i), 'name');
            s = get(job(i), 'state');
            if strcmpi(n, name) || strcmpi(s, status)
                destroy(job(i));
                st_log('Destroying job %s [%s]\n', n, s);
            end
        end
        
        if ~isnan(session_num)
            rslt = exec_sql('QUANT', ...
                sprintf(['SELECT CONVERT(VARCHAR(11), MIN(beg_date), 100), CONVERT(VARCHAR(8), MIN(beg_time), 8) '...
                ' FROM %s..job_status WHERE session_num=%d'], st_version.bases.quant, session_num));
            if ~strcmpi(rslt{1}, 'null')
                time1 = datenum([rslt{1} ' ' rslt{2}], 'mmm dd yyyy HH:MM:SS');
                
                for i=1:length(job)
                    n = get(job(i), 'name');
                    s = get(job(i), 'state');
                    t = get(job(i), 'submitTime');
                    time2 = datenum([t(5:10) t(end-4:end) ' ' t(12:19)], 'mmm dd yyyy HH:MM:SS');
                    if time2 >= time1 - datenum(0,0,0,0,5,0)
                        destroy(job(i));
                        st_log('Destroying job %s [%s]\n', n, s);
                    end
                end
            end
        end
        
        
    case 'disp-mdce'
        
        job = findJob(jm);
        for i=1:length(job)
            fprintf('Job %d\tName: %s\tState: %s\tUser: %s\n', ...
                i, get(job(i), 'name'), get(job(i), 'state'), get(job(i), 'username'));
        end
        
        
    case 'cancell-job'
        
        jobs = opt.get('obj');
        for i=1:length(jobs)
            query_update = sprintf('UPDATE %s..job SET next_run=''29990101'' WHERE job_id=%d', st_version.bases.quant, jobs{i});
            exec_sql('QUANT', query_update);
            query_update = sprintf('UPDATE %s..last_run SET next_check_date=''29990101'' WHERE job_id=%d', st_version.bases.quant, jobs{i});
            exec_sql('QUANT', query_update);
            st_log(sprintf('Job >>%d<< has been cancelled\n ', jobs{i}));
        end
      

    case'disp'
        
        switch lower(opt.get('obj'))
                      
                
            case 'error'
               rslt = exec_sql('QUANT', ...
                   sprintf('SELECT job_or_run_id, gravity_level, is_run, stamp_date, stamp_time, message FROM %s..error', st_version.bases.quant));
               disp(sprintf('Job or run ID\tGravity level\tIs run\tDate\tTime\tMessage'));
               for i=1:size(rslt, 1)
                   disp(sprintf('%d\t%d\t%d\t%s\t%s\t%s', rslt{i, 1}, rslt{i, 2}, rslt{i, 3}, rslt{i, 4}, ...
                       rslt{i, 5}, rslt{i, 6}));
               end
               out = struct('job_or_run_id', {rslt{:, 1}}, 'gravity_level', {rslt{:, 2}}, ...
                   'is_run', {rslt{:, 3}}, 'stamp_date', {rslt{:, 4}}, 'stamp_time', {rslt{:, 5}}, ...
                   'message', {rslt{:, 6}});

               
            case 'se-jobs'
                condition={}
                if ~isnan(opt.get('from'))
                    condition{end+1} = sprintf(' AND beg_date>=''%s''', ...
                        datestr(datenum(opt.get('from'), 'dd/mm/yyyy'), 'yyyy-mm-dd'));
                end
                if ~isnan(opt.get('to'))
                    condition{end+1} = sprintf(' AND end_date<=''%s''', ...
                        datestr(datenum(opt.get('to'), 'dd/mm/yyyy'), 'yyyy-mm-dd'));
                end
                if ~isnan(opt.get('status'))
                    condition{end+1} = sprintf(' AND status=''%s''', opt.get('status'));
                end
                rslt = exec_sql('QUANT', sprintf(['SELECT job_or_run_id, is_run, beg_date, beg_time, status, job_name ' ...
                    'FROM %s..job_status WHERE 0=0' condition{:}], st_version.bases.quant));
                disp(sprintf('Job/Run ID\tIs run\tBeg. date\tBeg. time\tJob name'));
                for i=1:size(rslt, 1)
                    disp(sprintf('%d\t%d\t%s\t%s\t%s\t%s', rslt{i, 1}, rslt{i, 2}, rslt{i, 3}, rslt{i, 4}, ...
                        rslt{i, 5}, rslt{i, 6}));
                end
                

            case 'avg-perf'
                job = exec_sql('QUANT', sprintf(['SELECT beg_date, beg_time, end_date, end_time, job_or_run_id, is_run, ' ...
                    'job_name FROM %s..job_status WHERE status=''ended'''], st_version.bases.quant));
                day = zeros(size(job, 1), 1);
                time = zeros(size(job, 1), 1);
                for i=1:size(job, 1)
                    beg_date = datenum(job{i, 1}, 'yyyy-mm-dd');
                    beg_time = datenum(job{i, 2}, 'HH:MM:SS');
                    end_date = datenum(job{i, 3}, 'yyyy-mm-dd');
                    end_time = datenum(job{i, 4}, 'HH:MM:SS');
                    beg_ = beg_date + (beg_time - floor(beg_time));
                    end_ = end_date + (end_time - floor(end_time));
                    day(i) = floor(end_) - floor(beg_);
                    time(i) = end_ - beg_;
                end
                if ~isempty(day)
                    disp(sprintf('Average time per job: %d day(s) and %s', mean(day), datestr(mean(time), 'HH:MM:SS')));
                end
                
               
            otherwise
                error(sprintf('se_monitor: obj <%s> unknown', opt.get('obj')));
        end
                
    
    case 'debug'
        condition = {};
        if ~isnan(opt.get('from'))
           condition{end+1} = sprintf(' AND e.error_id>=%d', opt.get('from')) ;
        end
        if ~isnan(opt.get('to'))
           condition{end+1} = sprintf(' AND e.error_id<=%d', opt.get('to')) ;
        end
        rslt = exec_sql('QUANT', ...
            sprintf(['SELECT j.job_id, d.security_id, d.trading_destination_id, e.message' ...
            ' FROM %s..error e, %s..job j, %s..domain_security d' ...
            ' WHERE e.is_run=1 AND e.job_or_run_id=j.job_id AND j.domain_id=d.domain_id', ...
            condition{:}], st_version.bases.quant, st_version.bases.quant, st_version.bases.quant));
        disp(sprintf('Job ID\tSecurity ID\tTrading destination ID\tMessage'))
        for i=1:size(rslt, 1)
            disp(sprintf('%d\t%d\t%d\t%s', rslt{i, 1}, rslt{i, 2}, rslt{i, 3}, rslt{i, 4}));
        end
                
        
    otherwise
        error('se_monitor: mode <%s> unknown', mode);
        
end
end