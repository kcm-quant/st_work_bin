global st_version
quant = st_version.bases.quant;

%% R�cup�ration des erreurs
txt = exec_sql('QUANT', [ 'select err.job_or_run_id, err.is_run, job.status, job.job_name, err.stamp_date, err.message from ' ...
' ',quant,'..error err, ' ...
' ',quant,'..job_status job ' ...
' where ' ...
' err.stamp_date>''20091009'' and ' ...
' err.job_or_run_id = job.job_or_run_id and err.is_run = job.is_run and ' ...
' err.stamp_date>=job.beg_date and err.stamp_date<=job.end_date' ] ) 
colnames = { 'err.job_or_run_id', 'err.is_run', 'job.status', 'job.job_name', 'err.stamp_date', 'err.message'}
%% parsing
messages = txt(:,end);
msg_table = {};
for m=1:length(messages)
	[t,n]=regexp(messages{m}, 'MESSAGE: (?<message>[^\[]+)', 'tokens', 'names');
	msg = n.message;
	[t,n]=regexp(messages{m}, '\[STACK[0-9]+:[^:]+:(?<location>[^\]]+)\]', 'tokens', 'names');
	a=cellflat( cellfun(@(c)regexp(c,':', 'split'),t,'uni',false));
	msg_table =cat(1, msg_table, ...
		cat(2,repmat({strtrim(msg)},numel(a)/2,1),reshape(a,2,numel(a)/2)'));
end
%% J'ai tout r�cup�r�
[fnames, tmp, fdx] = unique( msg_table(:,2));
[nlines, tmp, ldx] = unique( msg_table(:,3));
count = sparse(fdx,ldx,1);
fprintf('\n\n')
for c=1:size(count,1)
	fprintf('%s - ', fnames{c});
	for r=find(count(c,:)>0)
		fprintf( '%s (%d:%3.3f) - ', nlines{r}, count(c,r), count(c,r)/size(msg_table,1)*100);
	end
	fprintf('\n');
end
%% En fait je ne m'int�resse qu'� la premi�re erreur
first_messages = txt(:,end);
first_msg_table = {};
for m=1:length(first_messages)
	[t,n]=regexp(first_messages{m}, 'MESSAGE: (?<message>[^\[]+)', 'tokens', 'names');
	msg = n.message;
	[t,n]=regexp(first_messages{m}, '\[STACK[0-9]+:[^:]+:(?<location>[^\]]+)\]', 'tokens', 'names');
	a=regexp(t{1},':', 'split');
	first_msg_table =cat(1, first_msg_table, ...
		cat(2, txt(m,1:5), {strtrim(msg)},a{1}));
end
%% Liste des erreurs
fprintf('\nERRORS\n\n')
[msg, mdx] = unique(first_msg_table(:,6));
for m=1:length(msg)
    if first_msg_table{mdx(m),2}==1
        jor = 'run/job';
    else
        jor = 'check/run';
    end
    fprintf('%s -> cat %s_id_%d/%s.txt <- %s (line:%s)\n', msg{m}, jor, first_msg_table{mdx(m),1}, first_msg_table{mdx(m),4}, ...
        first_msg_table{mdx(m),7}, first_msg_table{mdx(m),8});
end

%% V�rification
fprintf('\nERRORS\n\n')
[msg, mdx] = unique(first_msg_table(:,6));
for m=1:length(msg)
    if first_msg_table{mdx(m),2}==1
        jor = 'run/job';
        cmdn = 'run';
        refn = 'job';
        acc  = {'[' , ']'};
    else
        jor = 'check/run';
        cmdn = 'check';
        refn = 'run';
        acc  = {'{' , '}'};
    end
    fprintf([ '\n%%%% ERROR <%s>\n%% %s -> cat %s_id_%d/%s.txt <- %s (line:%s)\n' ...
        'se_%s(''local'', ''%s_id'', ' , acc{1}, '%d', acc{2}, ', ''job_name'', ''%s-II'', ''%s-date'', ''%s'', ''debug'', 1)\n' ], ...
        msg{m}, msg{m}, jor, first_msg_table{mdx(m),1}, first_msg_table{mdx(m),4}, ...
        first_msg_table{mdx(m),7}, first_msg_table{mdx(m),8}, ...
        cmdn, refn, first_msg_table{mdx(m),1}, first_msg_table{mdx(m),4},  ...
        cmdn, datestr( datenum( first_msg_table{mdx(m),5}, 'yyyy-mm-dd'), 'dd/mm/yyyy') );
end
%% ERREURS � Classer
%  % Attempt to reference field of non-structure array. -> cat check/run_id_188923/f7e82c468a3fbd6f4309a8cfd8590020.txt <- se_check (line:93)
% % Attempted to access data.value(:,1); index out of bounds because size(data.value)= -> cat check/run_id_347926/ecb40014410a0c8436cc3ad022b50d09.txt <- se_run_heatmap (line:32)
% % CAT arguments dimensions are not consistent. -> cat run/job_id_7398/bf3b3888a61d81b096c03668762e6fbb.txt <- st_seasonnality/exec (line:318)
%  % Empty set returned by read_dataset used with gi:process4market_impact_01b -> cat run/job_id_7896/3dc1915c186253c30b235068143ae379.txt <- se_run_mi (line:18)
% % Index exceeds matrix dimensions. -> cat run/job_id_7479/bf3b3888a61d81b096c03668762e6fbb.txt <- trading_time (line:27)
% NODATA_FILTER: Empty set returned by read_dataset used with gi:seasonnality -> cat run/job_id_3277/ebded4f31df417a0e3ed596fbe659cea.txt <- se_run_volume_curve (line:40)
% NODATA_FILTER: No data for this day. -> cat run/job_id_1367/6040f7e25d16e763b1ca4ee5f380d610.txt <- st_learnmax/exec (line:46)
% NODATA_FILTER: No data for this job. -> cat run/job_id_2587/3dc1915c186253c30b235068143ae379.txt <- se_run_volatility_curve (line:24)
% NODATA_ROOT: KT empty, moins de 10 transactions par jour -> cat run/job_id_5879/5c2cd4f18e238c6d8422bc3eeed0e81c.txt <- cycle_bu_I (line:161)
% NODATA_ROOT: KT empty, probably last <10> days data empty -> cat check/run_id_369342/1ae1d15eb21b68c940c8d069fd8828de.txt <- cycle_I (line:200)
% NODATA_ROOT: KT empty, probably last <50> days data empty -> cat run/job_id_7915/3dc1915c186253c30b235068143ae379.txt <- cycle_I (line:200)
% NODATA_ROOT: No data for this (checked) day. -> cat check/run_id_353039/19a8a843f41f4fe425d7fba5cf41f4dc.txt <- se_check_volatility_curve (line:27)
% NODATA_ROOT: last <50> days data 2/3 empty -> cat run/job_id_6584/5c2cd4f18e238c6d8422bc3eeed0e81c.txt <- cycle_I (line:215)
% NOT_ENOUGH_DATA: Multiple change in trading hours change results in not enough data -> cat run/job_id_3550/6040f7e25d16e763b1ca4ee5f380d610.txt <- st_seasonnality/exec (line:137)
% NOT_ENOUGH_DATA: Not enough data for this security to work on it -> cat run/job_id_7432/6040f7e25d16e763b1ca4ee5f380d610.txt <- se_run_cfv (line:34)
% NOT_ENOUGH_DATA: Trading hours change results in Not enough data -> cat run/job_id_7386/bf3b3888a61d81b096c03668762e6fbb.txt <- st_seasonnality/exec (line:256)
% NO_DATA_FILTER:No data -> cat check/run_id_368589/a74fa96706cad9266c43f0e08bf5e2de.txt <- se_cfv_get_data (line:19)
% NO_DATA_FILTER:no data -> cat check/run_id_368352/37b2022a96db2e2270ad7308013b974a.txt <- se_check_volume_curve (line:45)
% % Non-singleton dimensions of the two input arrays must match each other. -> cat run/job_id_5511/3dc1915c186253c30b235068143ae379.txt <- st_normalize_iday/exec (line:49)
% % Stored procedure "candles_1min" not found. Specify owner.objectname or use sp_help to check whether the object exists (sp_help may produce lots of output). -> cat run/job_id_3646/3dc1915c186253c30b235068143ae379.txt <- exec_sql (line:82)
% % ETRANGE -[ Subscripted assignment dimension mismatch. -> cat run/job_id_4683/5c2cd4f18e238c6d8422bc3eeed0e81c.txt <- st_data (line:713)
% % The name "Inf" is illegal in this context. Only constants, constant expressions, or variables allowed here.  Column names are illegal. -> cat run/job_id_6320/8d6ef4ae9fb06e828fab0811efad0cde.txt <- exec_sql (line:82)
% % ETRANGE -> There is a NaN among parameters -> cat run/job_id_6879/3dc1915c186253c30b235068143ae379.txt <- se_run (line:101)
% % There is a zero in continuous phase : may be a too illiquid stock to build a volume curve, or wrong trading hours, or intraday auction -> cat run/job_id_7511/bf3b3888a61d81b096c03668762e6fbb.txt <- st_seasonnality/exec (line:369)
% % Unable to read MAT-file /disk01/matlab/st_repository/get_PLcycle/MRW.L.mat File may be corrupt. -> cat run/job_id_7236/5c2cd4f18e238c6d8422bc3eeed0e81c.txt <- get_PLcycle (line:47)
% X must be a non-empty vector. -> cat run/job_id_7444/3dc1915c186253c30b235068143ae379.txt <- parse_args (line:146)
% je ne reconnais pas le RIC -> cat run/job_id_4127/6040f7e25d16e763b1ca4ee5f380d610.txt <- se_run_heatmap (line:22)
% more than one security id for this code, please specify your source (IDN_SELECTFEED;TOPFLX) -> cat run/job_id_7337/8d6ef4ae9fb06e828fab0811efad0cde.txt <- get_repository (line:647)
% no such security_id : <362> in repository..security_market -> cat run/job_id_7464/bf3b3888a61d81b096c03668762e6fbb.txt <- get_repository (line:683)
% no such security_id : <8116> in repository..security_market -> cat check/run_id_368341/60c0826572b0bf769de8a73647925408.txt <- get_repository (line:683)
% not enough data with significant Market Impact to build a model -> cat run/job_id_7895/3dc1915c186253c30b235068143ae379.txt <- st_simple_mi/exec (line:40)
% se_check : Trading_hours changed -> cat check/run_id_368304/b4865f50e5daba490b7d688d143883aa.txt <- vc_check (line:31)
% 

%%
% select top 10 job.* from ',quant,'..job job, ',quant,'..job job_ref, ',quant,'..domain dom, ',quant,'..domain dom_ref where
% job_ref.job_id = 3277 and job.job_id > 3277 and
% dom_ref.domain_type_id = dom.domain_type_id and
% job_ref.estimator_id = job.estimator_id and
% job_ref.domain_id = dom_ref.domain_id and job.domain_id = dom.domain_id 
% order by job.job_id

%% ERROR <Attempt to reference field of non-structure array.>
% Attempt to reference field of non-structure array. -> cat check/run_id_188923/f7e82c468a3fbd6f4309a8cfd8590020.txt <- se_check (line:93)
se_check('local', 'run_id', {188923}, 'job_name', 'f7e82c468a3fbd6f4309a8cfd8590020-II', 'check-date', '12/10/2009', 'debug', 1)

%% ERROR <Attempted to access data.value(:,1); index out of bounds because size(data.value)=>
% Attempted to access data.value(:,1); index out of bounds because size(data.value)= -> cat check/run_id_347926/ecb40014410a0c8436cc3ad022b50d09.txt <- se_run_heatmap (line:32)
se_check('local', 'run_id', {347926}, 'job_name', 'ecb40014410a0c8436cc3ad022b50d09-II', 'check-date', '12/10/2009', 'debug', 1)

%% ERROR <CAT arguments dimensions are not consistent.>
% CAT arguments dimensions are not consistent. -> cat run/job_id_7398/bf3b3888a61d81b096c03668762e6fbb.txt <- st_seasonnality/exec (line:318)
se_run('local', 'job_id', [7398], 'job_name', 'bf3b3888a61d81b096c03668762e6fbb-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <Empty set returned by read_dataset used with gi:process4market_impact_01b>
% Empty set returned by read_dataset used with gi:process4market_impact_01b -> cat run/job_id_7896/3dc1915c186253c30b235068143ae379.txt <- se_run_mi (line:18)
se_run('local', 'job_id', [7896], 'job_name', '3dc1915c186253c30b235068143ae379-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <Index exceeds matrix dimensions.>
% Index exceeds matrix dimensions. -> cat run/job_id_7479/bf3b3888a61d81b096c03668762e6fbb.txt <- trading_time (line:27)
se_run('local', 'job_id', [7479], 'job_name', 'bf3b3888a61d81b096c03668762e6fbb-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <NODATA_FILTER: Empty set returned by read_dataset used with gi:seasonnality>
% NODATA_FILTER: Empty set returned by read_dataset used with gi:seasonnality -> cat run/job_id_3277/ebded4f31df417a0e3ed596fbe659cea.txt <- se_run_volume_curve (line:40)
se_run('local', 'job_id', [3277], 'job_name', 'ebded4f31df417a0e3ed596fbe659cea-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <NODATA_FILTER: No data for this day.>
% NODATA_FILTER: No data for this day. -> cat run/job_id_1367/6040f7e25d16e763b1ca4ee5f380d610.txt <- st_learnmax/exec (line:46)
se_run('local', 'job_id', [1367], 'job_name', '6040f7e25d16e763b1ca4ee5f380d610-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <NODATA_FILTER: No data for this job.>
% NODATA_FILTER: No data for this job. -> cat run/job_id_2587/3dc1915c186253c30b235068143ae379.txt <- se_run_volatility_curve (line:24)
se_run('local', 'job_id', [2587], 'job_name', '3dc1915c186253c30b235068143ae379-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <NODATA_ROOT: KT empty, moins de 10 transactions par jour>
% NODATA_ROOT: KT empty, moins de 10 transactions par jour -> cat run/job_id_5879/5c2cd4f18e238c6d8422bc3eeed0e81c.txt <- cycle_bu_I (line:161)
se_run('local', 'job_id', [5879], 'job_name', '5c2cd4f18e238c6d8422bc3eeed0e81c-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <NODATA_ROOT: KT empty, probably last <10> days data empty>
% NODATA_ROOT: KT empty, probably last <10> days data empty -> cat check/run_id_369342/1ae1d15eb21b68c940c8d069fd8828de.txt <- cycle_I (line:200)
se_check('local', 'run_id', {369342}, 'job_name', '1ae1d15eb21b68c940c8d069fd8828de-II', 'check-date', '12/10/2009', 'debug', 1)

%% ERROR <NODATA_ROOT: KT empty, probably last <50> days data empty>
% NODATA_ROOT: KT empty, probably last <50> days data empty -> cat run/job_id_7915/3dc1915c186253c30b235068143ae379.txt <- cycle_I (line:200)
se_run('local', 'job_id', [7915], 'job_name', '3dc1915c186253c30b235068143ae379-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <NODATA_ROOT: No data for this (checked) day.>
% NODATA_ROOT: No data for this (checked) day. -> cat check/run_id_353039/19a8a843f41f4fe425d7fba5cf41f4dc.txt <- se_check_volatility_curve (line:27)
se_check('local', 'run_id', {353039}, 'job_name', '19a8a843f41f4fe425d7fba5cf41f4dc-II', 'check-date', '12/10/2009', 'debug', 1)

%% ERROR <NODATA_ROOT: last <50> days data 2/3 empty>
% NODATA_ROOT: last <50> days data 2/3 empty -> cat run/job_id_6584/5c2cd4f18e238c6d8422bc3eeed0e81c.txt <- cycle_I (line:215)
se_run('local', 'job_id', [6584], 'job_name', '5c2cd4f18e238c6d8422bc3eeed0e81c-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <NOT_ENOUGH_DATA: Multiple change in trading hours change results in not enough data>
% NOT_ENOUGH_DATA: Multiple change in trading hours change results in not enough data -> cat run/job_id_3550/6040f7e25d16e763b1ca4ee5f380d610.txt <- st_seasonnality/exec (line:137)
se_run('local', 'job_id', [3550], 'job_name', '6040f7e25d16e763b1ca4ee5f380d610-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <NOT_ENOUGH_DATA: Not enough data for this security to work on it>
% NOT_ENOUGH_DATA: Not enough data for this security to work on it -> cat run/job_id_7432/6040f7e25d16e763b1ca4ee5f380d610.txt <- se_run_cfv (line:34)
se_run('local', 'job_id', [7432], 'job_name', '6040f7e25d16e763b1ca4ee5f380d610-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <NOT_ENOUGH_DATA: Trading hours change results in Not enough data>
% NOT_ENOUGH_DATA: Trading hours change results in Not enough data -> cat run/job_id_7386/bf3b3888a61d81b096c03668762e6fbb.txt <- st_seasonnality/exec (line:256)
se_run('local', 'job_id', [7386], 'job_name', 'bf3b3888a61d81b096c03668762e6fbb-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <NO_DATA_FILTER:No data>
% NO_DATA_FILTER:No data -> cat check/run_id_368589/a74fa96706cad9266c43f0e08bf5e2de.txt <- se_cfv_get_data (line:19)
se_check('local', 'run_id', {368589}, 'job_name', 'a74fa96706cad9266c43f0e08bf5e2de-II', 'check-date', '12/10/2009', 'debug', 1)

%% ERROR <NO_DATA_FILTER:no data>
% NO_DATA_FILTER:no data -> cat check/run_id_368352/37b2022a96db2e2270ad7308013b974a.txt <- se_check_volume_curve (line:45)
se_check('local', 'run_id', {368352}, 'job_name', '37b2022a96db2e2270ad7308013b974a-II', 'check-date', '12/10/2009', 'debug', 1)

%% ERROR <Non-singleton dimensions of the two input arrays must match each other.>
% Non-singleton dimensions of the two input arrays must match each other. -> cat run/job_id_5511/3dc1915c186253c30b235068143ae379.txt <- st_normalize_iday/exec (line:49)
se_run('local', 'job_id', [5511], 'job_name', '3dc1915c186253c30b235068143ae379-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <Stored procedure "candles_1min" not found. Specify owner.objectname or use sp_help to check whether the object exists (sp_help may produce lots of output).>
% Stored procedure "candles_1min" not found. Specify owner.objectname or use sp_help to check whether the object exists (sp_help may produce lots of output). -> cat run/job_id_3646/3dc1915c186253c30b235068143ae379.txt <- exec_sql (line:82)
se_run('local', 'job_id', [3646], 'job_name', '3dc1915c186253c30b235068143ae379-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <Subscripted assignment dimension mismatch.>
% Subscripted assignment dimension mismatch. -> cat run/job_id_4683/5c2cd4f18e238c6d8422bc3eeed0e81c.txt <- st_data (line:713)
se_run('local', 'job_id', [4683], 'job_name', '5c2cd4f18e238c6d8422bc3eeed0e81c-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <The name "Inf" is illegal in this context. Only constants, constant expressions, or variables allowed here.  Column names are illegal.>
% The name "Inf" is illegal in this context. Only constants, constant expressions, or variables allowed here.  Column names are illegal. -> cat run/job_id_6320/8d6ef4ae9fb06e828fab0811efad0cde.txt <- exec_sql (line:82)
se_run('local', 'job_id', [6320], 'job_name', '8d6ef4ae9fb06e828fab0811efad0cde-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <There is a NaN among parameters>
% There is a NaN among parameters -> cat run/job_id_6879/3dc1915c186253c30b235068143ae379.txt <- se_run (line:101)
se_run('local', 'job_id', [6879], 'job_name', '3dc1915c186253c30b235068143ae379-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <There is a zero in continuous phase : may be a too illiquid stock to build a volume curve, or wrong trading hours, or intraday auction>
% There is a zero in continuous phase : may be a too illiquid stock to build a volume curve, or wrong trading hours, or intraday auction -> cat run/job_id_7511/bf3b3888a61d81b096c03668762e6fbb.txt <- st_seasonnality/exec (line:369)
se_run('local', 'job_id', [7511], 'job_name', 'bf3b3888a61d81b096c03668762e6fbb-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <Unable to read MAT-file /disk01/matlab/st_repository/get_PLcycle/MRW.L.mat File may be corrupt.>
% Unable to read MAT-file /disk01/matlab/st_repository/get_PLcycle/MRW.L.mat
% File may be corrupt. -> cat run/job_id_7236/5c2cd4f18e238c6d8422bc3eeed0e81c.txt <- get_PLcycle (line:47)
se_run('local', 'job_id', [7236], 'job_name', '5c2cd4f18e238c6d8422bc3eeed0e81c-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <X must be a non-empty vector.> ???
% X must be a non-empty vector. -> cat run/job_id_7444/3dc1915c186253c30b235068143ae379.txt <- parse_args (line:146)
se_run('local', 'job_id', [7444], 'job_name', '3dc1915c186253c30b235068143ae379-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <je ne reconnais pas le RIC>
% je ne reconnais pas le RIC -> cat run/job_id_4127/6040f7e25d16e763b1ca4ee5f380d610.txt <- se_run_heatmap (line:22)
se_run('local', 'job_id', [4127], 'job_name', '6040f7e25d16e763b1ca4ee5f380d610-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <more than one security id for this code, please specify your source (IDN_SELECTFEED;TOPFLX)>
% more than one security id for this code, please specify your source (IDN_SELECTFEED;TOPFLX) -> cat run/job_id_7337/8d6ef4ae9fb06e828fab0811efad0cde.txt <- get_repository (line:647)
se_run('local', 'job_id', [7337], 'job_name', '8d6ef4ae9fb06e828fab0811efad0cde-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <no such security_id : <362> in repository..security_market>
% no such security_id : <362> in repository..security_market -> cat run/job_id_7464/bf3b3888a61d81b096c03668762e6fbb.txt <- get_repository (line:683)
se_run('local', 'job_id', [7464], 'job_name', 'bf3b3888a61d81b096c03668762e6fbb-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <no such security_id : <8116> in repository..security_market>
% no such security_id : <8116> in repository..security_market -> cat check/run_id_368341/60c0826572b0bf769de8a73647925408.txt <- get_repository (line:683)
se_check('local', 'run_id', {368341}, 'job_name', '60c0826572b0bf769de8a73647925408-II', 'check-date', '12/10/2009', 'debug', 1)

%% ERROR <not enough data with significant Market Impact to build a model>
% not enough data with significant Market Impact to build a model -> cat run/job_id_7895/3dc1915c186253c30b235068143ae379.txt <- st_simple_mi/exec (line:40)
se_run('local', 'job_id', [7895], 'job_name', '3dc1915c186253c30b235068143ae379-II', 'run-date', '12/10/2009', 'debug', 1)

%% ERROR <se_check : Trading_hours changed>
% se_check : Trading_hours changed -> cat check/run_id_368304/b4865f50e5daba490b7d688d143883aa.txt <- vc_check (line:31)
se_check('local', 'run_id', {368304}, 'job_name', 'b4865f50e5daba490b7d688d143883aa-II', 'check-date', '12/10/2009', 'debug', 1)