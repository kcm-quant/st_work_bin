function out = se_proc_validation_run(varargin)
%SE_PROC_VALIDATION_RUN - this function executes all the process of validation for SE_runs
%
%exemple :
%  se_proc_validation_run(varargin)
% job_with_recent_run
%  se_proc_validation_run('estimators','Placement en cycle','first-validation',0)
%  se_proc_validation_run('estimators','paSSive split','refresh' ,1 ,'first-validation',1)

global st_version
quant = st_version.bases.quant;
q_data_base = st_version.bases.quant_data;

%Si ce n'est pas specifi� la proc�dure fait une validation des tous les
%estimateurs
est_name = exec_sql('QUANT' , sprintf([ ...
    'SELECT estimator_name ' ...
    'FROM ' ...
    '%s.. estimator est '],quant));

%Options
opt = options( {'estimators',est_name...
    'first-validation', '' , ...
    'domain_type', '', ...
    'refresh', 0 , ...
    'translate-to-quant-data', 0, ...
    'session' , nan , ... 
    'exec_phase'  , nan
    }, varargin);

estimators = opt.get('estimators');
if ischar(estimators)
    estimators = {estimators};
end
temp = cellfun(@(x)se_db('get_estimator' , 'estimator_name' , x),estimators);
estimator_id = [temp.estimator_id];



%% PRE-VALIDATION

% pre-validation des estimateurs dont le process de PRE-VALIDATION est actif
active_pre =  se_db('get_active_estimators','process','pre_validation','exec_phase',opt.get('exec_phase'));
active_pre = intersect(active_pre,estimator_id);
for i =1 : length(active_pre)
    estimator_db = se_db('get_estimator', 'estimator_id' , active_pre(i));
    try
        feval(str2func(['se_pre_validation_',strrep(lower(estimator_db.estimator_name),' ','_')]));
    catch e
        subject = sprintf('Echec de la pre-validation de <%s> en environnement <%s>',estimator_db.estimator_name,st_version.my_env.target_name);
        message = [e.message '\nStack :\n' se_stack_error_message(e)];
        recipient = exec_sql('QUANT',sprintf(['select owner from ',quant,'..estimator where estimator_id = %d'],active_pre(i)));
        se_sendmail([recipient,regexp(st_version.my_env.se_admin,',','split')],subject,message);
    end
end
%% VALIDATION COMMUNE

%Retrait des estimateurs pour lesquels la "VALIDATION" n'est pas active
active_est_validation =  se_db('get_active_estimators','process','validation','exec_phase',opt.get('exec_phase'));
if ~isempty(active_est_validation) 
    if (~isempty(estimator_id))
        active_est_validation = intersect (estimator_id, active_est_validation);
    end   
else
    st_log('se_proc_validation: no estimator_id found for validation \n' );
    return
end

for i =1 : length(active_est_validation)
    
    %Si ce n'est pas specifi� la procedure fait une validation par
    %type de domain
    dmn_type = exec_sql('QUANT' , sprintf([ ...
        'SELECT  ' ...
        'distinct dt.domain_type_name  ' ...
        'FROM   ' ...
        '%s..estimator est,  ' ...
        '%s..job jb,  ' ...
        '%s..domain dm ,   ' ...
        '%s..domain_type dt  ' ...
        'WHERE  ' ...
        'est.estimator_id = %d AND  ' ...
        'jb.estimator_id= est.estimator_id AND  ' ...
        'dm.domain_id = jb.domain_id AND  ' ...
        'dt.domain_type_id = dm.domain_type_id '],quant ,quant, quant, quant, active_est_validation(i) ));
    
    dmn_type_str=dmn_type{1};
    for h =2:length(dmn_type)
        dmn_type_str = sprintf('%s;%s',dmn_type_str,dmn_type{h});
    end
    opt.set('domain_type', dmn_type_str);
    domain = tokenize(opt.get('domain_type'),';');
    
    %v�rification s'il y a des runs d�j� valid�s.
    nb_v = exec_sql('QUANT' , sprintf([ ...
        'SELECT count(1) ' ...
        'FROM ' ...
        '%s.. estimator est, '...
        '%s..estimator_runs est_r '...
        'WHERE ' ...
        'est.estimator_id = %d AND ' ...
        'est_r.estimator_id = est.estimator_id AND '...
        'est_r.is_valid =1 '],quant,quant, active_est_validation(i)));
    
    if opt.get('refresh')
        st_log('se_proc_validation_run: REFRESH validation\n');
        opt.set('first-validation',1);
        
    else
        if ~isempty(opt.get('first-validation')) %v�rification si c la premi�re validation de l'estimateur ou pas
            st_log('se_proc_validation_run: first validation\n');
            if nb_v{1}>0 && opt.get('first-validation')
                st_log('se_proc_validation_run: it is not the first validation since I already have %d validated runs on this estimator\n', nb_v{1});
                return;
            elseif ~opt.get('first-validation') &&   nb_v{1}==0
                st_log('se_proc_validation_run: it is the first validation for your estimator, you have to apply "first_validation" mode', nb_v{1});
                return;
            end
        else
            if nb_v{1}>0
                opt.set('first-validation',0);
            else
                opt.set('first-validation',1);
            end
        end
            
    end
    
    %%%< Validation commune � tous les estimateurs
    for j= 1:length(domain)
        
        validation_type = cell2mat(exec_sql('QUANT', sprintf('SELECT is_check_validation FROM %s..estimator where estimator_id = %d', quant ,active_est_validation(i))));
        
        switch validation_type
            case 0
                %Validation � partir des RUNS:run_quality
                run_to_valid = se_validation_run(active_est_validation(i), 'refresh' , opt.get('refresh') , ...
                    'first-validation', opt.get('first-validation'), 'domain_type',domain{j} , 'session' , opt.get('session') ); 
                se_validation_run_base( run_to_valid.estimator, run_to_valid );
                
            case 1
                %VALIDATION A PARTIR DES CHECK: check_quality              
                 se_validation_check(active_est_validation(i),'domain_type',domain{j},'session',opt.get('session'));
        end
        
        
    end
    %%%>
    
    opt.set('first-validation','');
    
end

%% POST-VALIDATION

% post-validation des estimateurs dont le process de POST-VALIDATION est actif
active_post =  se_db('get_active_estimators','process','post_validation','exec_phase',opt.get('exec_phase'));
active_post = intersect(active_post,estimator_id);
for i =1 : length(active_post)
    estimator_db = se_db('get_estimator', 'estimator_id' , active_post(i));
    try
        feval(str2func(['se_post_validation_',strrep(lower(estimator_db.estimator_name),' ','_')]));
    catch e
        subject = sprintf('Echec de la post-validation de <%s> en environnement <%s>',estimator_db.estimator_name,st_version.my_env.target_name);
        message = [e.message '\nStack :\n' se_stack_error_message(e)];
        recipient = exec_sql('QUANT',sprintf(['select owner from ',quant,'..estimator where estimator_id = %d'],active_post(i)));
        se_sendmail([recipient,regexp(st_version.my_env.se_admin,',','split')],subject,message);
    end
end
%% MISE � jour de la base QUANT_DATA
%%%
if opt.get('translate-to-quant-data')
    estimator_id = cell2mat(exec_sql('QUANT', sprintf( 'SELECT estimator_id FROM %s..estimator where upper(estimator_name) = ''%s'' ',quant,upper(active_estimator_id(i)))));
    try
        for i= 1 : length(estimator_id)
            exec_sql( 'QUANT', sprintf('%s..upd_quant_data %d ',q_data_base,estimator_id(i)));
        end
    catch er
        if isempty(strfind( er.message, 'JZ0R2'))
            st_log(sprintf('%s\n', er.message));
            error_message = se_stack_error_message(er);
            if ~ispc()
                error_message = regexprep(error_message, '\n', '\r');
            end
            
            %envoie de mail
            identification_err = sprintf('%s%s%s', 'ECHEC DANS LE PROCESSUS DE MISE A JOUR DE QUANT_DATA:',blank_char, error_message);
            subject = sprintf('%s : Echec de mise a jour de QUANT_DATA pour l''estimateur %s' , hostname() , upper(estimator_id));
            %mail
            address = exec_sql('QUANT', sprintf('SELECT owner from %s..estimator WHERE upper(estimator_name) = (''%s'')', quant , upper(estimator_id)));
            for j=1:length(address)
                se_sendmail( address{j},subject,identification_err)
            end
        end
    end
end

out = {};
end