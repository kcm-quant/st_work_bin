function se_quant_validation(mode,varargin)
%SE_QUANT_VALIDATION - permet de valider ou devalider une liste de runs
%
%
% exemple : se_quant_validation('validation','run_id', [1; 2])
%           se_quant_validation('devalidation','run_id', [1; 2])

% Author: dancr@cheuvreux.com
% Date: 05/08/2010
% Version: 1.0


global st_version;
sv = st_version.bases.quant;

opt = options({'run_id', NaN, ...
    'output' , 1 , ...
    'estimator', ''}, ...
    varargin);

switch mode
    case  'validation'
        
        if opt.get('output')
            sprintf('ATTENTION un certains nombre de runs seront valides sur la base QUANT, l''environnement en cours est :: %s', upper(st_version.my_env.target_name) )
        end
        
        run_id = opt.get('run_id');
        run2valid = regexprep(convs('str' , run_id),{'['  ';' ']'}, {'\t'  ',' '\t'});
        
        %Dévalidation nécessaire pour éviter de doublons de validation
        %pour un mm job
        job2devalid = cell2mat(exec_sql('QUANT',sprintf('select job_id from %s..estimator_runs where run_id in(%s)',sv,run2valid)));
        job2devalid = regexprep(convs('str' , job2devalid),{'['  ';' ']'}, {'\t'  ',' '\t'});
        run2devalid = cell2mat(exec_sql('QUANT', sprintf(' select run_id from %s..estimator_runs where job_id in (%s) and is_valid = 1 ' ...
            ,sv, job2devalid )));
        
        %dévalidation
        if ~isempty(run2devalid)
            se_quant_validation('devalidation','run_id', run2devalid , 'output' , 0)
        end
        %validation
        exec_sql('QUANT', sprintf([ 'UPDATE %s..estimator_runs '...
            'SET is_valid = 1,devalidation_type is null , validation_type =1 '...
            'WHERE run_id in (%s) '],sv,run2valid));
        
        if opt.get('output')
            st_log('< %s >: end of validation of %d runs\n',mfilename, length(run_id));
        end
        
        
    case 'devalidation'
        
        if opt.get('output')
            sprintf('ATTENTION un certains nombre de runs seront devalides sur la base QUANT, l''environnement en cours est :: %s', upper(st_version.my_env.target_name) )
        end
        
        run_id = opt.get('run_id');
        run2devalid = regexprep(convs('str' , run_id),{'['  ';' ']'}, {'\t'  ',' '\t'});
        
        exec_sql('QUANT', sprintf([ 'UPDATE %s..estimator_runs '...
            'SET is_valid = 0, devalidation_type = 1 , validation_type = null '...
            'WHERE run_id in (%s) '],sv,run2devalid));
        
        if opt.get('output')
            st_log('< %s >: end of devalidation of %d runs\n',mfilename, length(run_id));
        end
        
        
end
