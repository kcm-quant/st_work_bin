function idxes2del = se_remove_perim(domain_sec_ids)
% SE_REMOVE_PERIM - patch pour retirer massivement des trading_destination
%       d'un perimetre
%
% r = perim_FOBU('virtual');
% se_remove_perim(r.domain_sec_ids)
%

global st_version
repository = st_version.bases.repository;

if isempty(domain_sec_ids)
    idxes2del=[];
    return
end
    
% par d�faut on retire tous les security_id
idxes2del = true(length(domain_sec_ids), 1); 

for i=1:length(domain_sec_ids)
    sec_ids = cat(1,domain_sec_ids{i}.security_id);
    if isempty(sec_ids)
        continue
    end
    td_ids=unique(cat(1,domain_sec_ids{i}.trading_destination_id));
    in_txt = sprintf('%d,', sec_ids);
    in_txt(end) = [];
    % test sur les sec_id morts
    sec2keep = cell2mat(exec_sql('KGR',['select distinct security_id',...
        ' from ',repository,'..security_historic where end_date is null and security_id in (',in_txt,')']));
    idxes2del(i)=isempty(sec2keep);
end

end

% 
% 
% % SE_REMOVE_PERIM - patch pour retirer massivement des trading_destination
% %       d'un perimetre
% %
% % r = perim_FOBU('virtual');
% % se_remove_perim(r.domain_sec_ids)
% %
% 
% % < pr�paration de la liste des securitiy_id et de la chaine de caract�re
% % permettant de faire des requetes sur l'ensemble de ces security_id
% 
% % TO DO : juste un sec id !!!
% sec_ids = NaN(size(domain_sec_ids));
% for i=1:length(sec_ids)
%     sec_ids(i) = domain_sec_ids{i}.security_id;
% end
% in_txt = sprintf('%d,', sec_ids);
% in_txt(end) = [];
% % >
% 
% 
% idxes2del = true(length(domain_sec_ids), 1); % par d�faut on retire tous les security_id
% 
% if isempty(domain_sec_ids)
%     return;
% end
% 
% % < On ne veut pas retirer des perim�tres les security_id qui ne sont pas "morts"
% sec2keep = cell2mat(exec_sql('BSIRIUS', sprintf(['select distinct security_id from repository..security_historic where end_date is null and security_id in (%s)'], in_txt)));
% for i = 1 : length(sec2keep)
%     idxes2del(sec2keep(i) == sec_ids) = false;
% end
% % >
% 
% % < Valeurs expir�es mais pas not�es comme tel : ces lignes deviendront
% % inutiles � partir du 23/02/2009
% hard_list = [12959 16831 16874 267];
% for i = 1 : length(hard_list)
%     idxes2del(hard_list(i) == sec_ids) = true;
% end
% % >
% 
% % < on retire les trading_destination 20 et 25 (tokyo et NY)
% sec2del = cell2mat(exec_sql('BSIRIUS', sprintf(['select distinct security_id from repository..security_market '...
%         ' where trading_destination_id =25 and security_id in (%s)'], in_txt)));
% 
% for i = 1 : length(sec2del)
%     idxes2del(sec2del(i) == sec_ids) = true;
% end
% % >
