function varargout = se_run(mode, varargin)
% SE_RUN - do the evaluation of the run
%
% se_run('local', 'job_id', 1, 'job_name', 'test')
% se_run('distrib', 'job_id', 1)
% se_run('local', 'job_id', 1, 'job_name', 'test', 'run-date', '10/12/2008', 'debug', 1)
%
% See also se_check
%
% Author   'Edouard d'Archimbaud'
% Version  '1.0'
% Reviewer ''
% Date     '22/10/2008'

global st_version;

varargout = {};

opt = options({ ...
    'job_id', 0, ...
    'dt_remove', '', ...
    'job_manager', '', ...
    'run-date', '', ...
    'varargin_', '', ...
    'job_name', '', ...
    'session-num', 0, ...
    'FileDependencies', {}, ...
    'debug', false}, ...
    varargin);

switch(mode)
    
    case 'local'
        %D?but de la gestion des jobs multiples
        if iscell(opt.get('job_id'))
            job_id = opt.get('job_id');
            for i=1:length(job_id)
                if opt.get('debug')
                    varargout{end+1} = se_run('local', 'job_id', job_id{i}, 'job_name', opt.get('job_name'), ...
                        'run-date', opt.get('run-date'), 'debug', opt.get('debug'), 'session-num', opt.get('session-num'));
                else
                    se_run('local', 'job_id', job_id{i}, 'job_name', opt.get('job_name'), ...
                        'run-date', opt.get('run-date'), 'debug', opt.get('debug'), 'session-num', opt.get('session-num'));
                end
            end
            return;
        end
        % D?bit de la gestion des jobs simples
        try
            if ~opt.get('debug') % qd on est en debug, on n'?crit pas
                job_status_id = se_db('add_job_status', opt.get('job_id'), 'running', 1, ...
                    datestr(floor(now)), datestr(now, 'HH:MM:SS'), opt.get('job_name'), opt.get('session-num'));
                
                dir_path = fullfile(getenv('st_repository'), 'log', 'run', sprintf('job_id_%d', opt.get('job_id')));
                if isempty(dir(dir_path))
                    mkdir(dir_path);
                end
                log_path = fullfile(dir_path, sprintf('%s.txt', opt.get('job_name')));
                st_log('$out$', log_path);
            end
            
            job = se_db('get_job', 'job_id', opt.get('job_id'));
            estimator = se_db('get_estimator', 'estimator_id', job.estimator_id);
            domain_security = se_db('get_domain_security', 'domain_id', job.domain_id);
            
            window_type = se_db('get_window_type', 'window_type_id', job.run_window_type_id);
            
            if job.run_frequency_id==4 % asOfDate
                run_date = job.run_frequency_date;
            else
                % ce devrait ?tre le dernier jour ouvr?
                w = weekday(datenum(opt.get('run-date'), 'dd/mm/yyyy'));
                run_date = datestr(datenum(opt.get('run-date'), 'dd/mm/yyyy') - (w==1)*2-(w==7), 'dd/mm/yyyy');
            end
            
            
            % Coeur du calcul
            data = feval(estimator.matlab_run, [domain_security.security_id], [domain_security.trading_destination_id], ...
                window_type.window_type_name, job.run_window_width, run_date, opt.get('dt_remove'), ...
                job.varargin);
            if opt.get('debug')
                varargout{end+1} = data;
            end
            [data,b] = final_check(data);
            if b
                error('Data returned by %s is empty!',estimator.matlab_run);
            end
            if ~opt.get('debug')
                to_sql(data, job, opt.get('session-num'), run_date);
                se_db('update_next_run', job.job_id, job.run_frequency_date, job.run_frequency_id, run_date);
                se_db('update_job_status', 'ended', datestr(floor(now)), datestr(now, 'HH:MM:SS'), job_status_id);
                st_log('$close$');
                if ~st_version.se_spec.keep_all_log
                    delete(log_path);
                    if length(dir(dir_path)) == 2
                        rmdir(dir_path);
                    end
                end
            end
            
        catch e
            error_message = {};
            error_message{end+1} = ['MESSAGE: ' e.message];
            for i=1:length(e.stack)
                error_message{end+1} = [' [STACK' sprintf('%d', i) ': ' e.stack(i).name ':' sprintf('%d', e.stack(i).line) ']' ];
            end
            if ~opt.get('debug')
                se_db('add_error', opt.get('job_id'), 1, 1, datestr(floor(now)), datestr(now, 'HH:MM:SS'), [error_message{:}]);
                se_db('update_job_status', 'cancelled', datestr(floor(now)), datestr(now, 'HH:MM:SS'), job_status_id);
            end
            st_log('ERROR:%s\n', e.message);
            if ~opt.get('debug')
                st_log('$close$');
            end
            if opt.get('debug')
                rethrow(e); % pour d?truire le job qui a plant?
            end
        end
        
    case 'distrib'
        % Fonction lan?ant un run distribu? en local
        if ~isempty(opt.get('job_manager'))
            % On cr?e un nom pour le job
            job_name = hash(now,'md5');
            jm = opt.get('job_manager');
            job = createJob(jm, 'name', job_name, 'FileDependencies', opt.get('FileDependencies'));
            
            if ~strcmpi(jm.Type, 'local')
                set(job, 'RestartWorker', true);
                set(job, 'Timeout', 60 * 60 * 24); % Time out � 24h pour supprimer les jobs trop longs
            end
            
            task_n = createTask(job, str2func('se_run'), 0, {'local', 'job_id', opt.get('job_id'), ...
                'date_remove', opt.get('dt_remove'), 'varargin_', opt.get('varargin_'), ...
                'job_name', job_name, 'run-date', opt.get('run-date'), ...
                'session-num', opt.get('session-num')});
            
            if ~strcmpi(jm.Type, 'local')
                set(task_n,'MaximumNumberOfRetries',0)
            end
            
            submit(job);
            
            % pour se_last_job
            varargout = {job};
        else
            error('Error in distribution: no job manager');
        end
        
    otherwise
        error('se_run:unkown_mode','mode <%s> unknown', mode);
end


function [data,b] = final_check(data)
if iscell(data)
    b = false(length(data),1);
    for d = 1:length(data)
        [data{d}, b(d)] = st_data('isempty', data{d});
    end
    b = any(b);
else
    [data, b] = st_data('isempty', data);
end


function to_sql(data,job,session_num,run_date)
% Ecriture des r�sultats du calcul en base
global st_version
param = se_db('get_param_desc', 'estimator_id', job.estimator_id);
% Loop on the different contexts
if ~iscell(data)
    data = {data};
end
for d = 1:length(data)
    for i = 1:numel(data{d}.colnames)
        run_quality = data{d}.info.run_quality(i);
        if isnan(run_quality)
            run_quality = 0;
        end
        context = se_db('get_context', 'estimator_id', job.estimator_id, 'context_name', data{d}.colnames{i});
        assert(~isempty(context),'Error in function of evaluation');
        [~, idx1, idx2] = intersect(reshape(lower(data{d}.rownames),[],1), reshape(lower({param.parameter_name}),[],1));
        param_id = vertcat(param(idx2).parameter_id);
        val = data{d}.value(idx1, i);
        if any(isnan(val))
            error('se_run:nan_param', 'SE_JOB_PARAMS: There is a NaN among parameters');
        else
            % job_id, estimator_id, domain_id, run_quality, context_id, window_end_date, last_run_date, stamp_date, comment, session_num
            query = sprintf('%s..ins_estimator_runs %d, %d, %d, %d, %d , ''%s'', ''%s'', ''%s'',''%s'',null,null,%d ', ...
                st_version.bases.quant, ...
                job.job_id, job.estimator_id, job.domain_id, run_quality, ...
                context.context_id, datestr(datenum(run_date, 'dd/mm/yyyy'), 'yyyymmdd'), ...
                datestr(datenum(run_date, 'dd/mm/yyyy'), 'yyyymmdd'), ...
                datestr(datenum(run_date, 'dd/mm/yyyy'), 'yyyymmdd'),'',session_num);
            run_id = cell2mat(exec_sql('QUANT', query));
            for j=1:length(val)
                query = sprintf('INSERT %s..param_value (run_id, parameter_id, value) VALUES (%d,%d,%f)', ...
                    st_version.bases.quant, ...
                    run_id, param_id(j), val(j));
                exec_sql('QUANT', query);
            end
            
        end
    end
end