function se_sendmail(recipient, subject, content)
% SE_SENDMAIL - sends mail
%
% ex : se_sendmail('rburgot@smtpnotes', 'subject', 'Body of the message')
%
% Si votre matlab est sous Windows vous aurez besoin de la ligne 
% de commande suivante :
% setpref('Internet','SMTP_Server','smtpnotes');
% setpref('Internet','E_mail','rburgot@smtpnotes');
%
% Inutile, je crois :
% les préférence internet suivantes :
%   SMTP_Username SMTP_Password SMTP_port
% La configuration du proxy :
%    dans File/preference/Web, le host est paproxy, le port est 7171

if ischar(recipient)
    recipient = {recipient};
end
if ispc()
    sendmail(recipient, subject, content);
else
    for i = 1 : length(recipient)
        system(sprintf('echo -e "%s" | mail -s "%s" %s', content,subject, recipient{i}));
    end
end
end