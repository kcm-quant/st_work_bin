function error_message = se_stack_error_message(e)
% SE_STACK_ERROR_MESSAGE - compose une chaine de caract?res ? partir d'une
% erreur matlab afin de produire un message tel que ceux affich?s dans la
% table error de la base quant du statistical engine

error_message = {};
error_message{end+1} = ['MESSAGE: ' e.identifier ': ' e.message];
for i=1:length(e.stack)
    error_message{end+1} = [' [STACK' sprintf('%d', i) ': ' e.stack(i).name ':' ...
        sprintf('%d', e.stack(i).line) ']' ];
end
error_message = [error_message{:}];
end
