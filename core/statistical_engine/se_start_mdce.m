function se_start_mdce(varargin)
% SE_START_MDCE - Fonction << MAIN >> du SE. Ne pas confondre avec SE_MAIN.
%
%
% See also se_install
%
% Author   'Edouard d'Archimbaud'
% Version  '2.0'
% Reviewer 'Dana Croize','Matthieu Lasnier'
% Date     '12/12/2009'
%
% Exemple:
% se_start_mdce('exec_phase',2)
%

global st_version

quant = st_version.bases.quant;

opt = options({ ...
    'jm', 'local', ...
    'exec_phase',NaN, ...
    }, varargin );

if ~isnan(opt.get('exec_phase'))
    minmax = [min(opt.get('exec_phase')) max(opt.get('exec_phase')) ];
else
    minmax = cell2mat(exec_sql('QUANT', sprintf('SELECT min(exec_order), max(exec_order) FROM %s.. estimator', quant)));
end

try
    for j= min(minmax): max(minmax)
        
        %Cr�ation du fichier se_start_mdce_DATE.txt
        st_log('$out$', fullfile(getenv('st_repository'), 'log', sprintf('se_start_mdce_%s.txt' , datestr(now,'ddmmyyyy_HHMM'))));
        
        opt.set('exec_phase' , j );
        % On teste si le SE est <<allum�>> ou pas.
        xml  = xmltools('st_work.xml');
        se = xmltools(xml.get_tag('SE'));
        if ~se.get_attrib_value('SE','active')
            exit
        end

        decay = st_version.my_env.decay;
        jobmanager = st_version.my_env.jobmanager;
        
        %V�rification du nombre de workers MDCE/MDCS
        switch lower(jobmanager)
            case 'local'
                jm = findResource('scheduler','type','local');
            case 'tcmlb001'
                jm = findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL','tcmlb001');
                
                %ITRS
                nb_failed_workers = 16 - (jm.NumberOfIdleWorkers + jm.NumberOfBusyWorkers) ;
                if nb_failed_workers>0
                    st_log(sprintf('%s-MECANIC-NB_WORKER: WARNING : %d workers are not working ' , datestr(now , 'yyyymmdd'), nb_failed_workers))
                end
            case 'tlhmlb003'
                jm = findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL','tlhmlb003');
                
                %ITRS
                nb_failed_workers = 8 - (jm.NumberOfIdleWorkers + jm.NumberOfBusyWorkers) ;
                if nb_failed_workers>0
                    st_log(sprintf('%s-MECANIC-NB_WORKER: WARNING : %d workers are not working ' , datestr(now , 'yyyymmdd'), nb_failed_workers))
                end
        end
        
        
        % G�n�ration du session_num
        last_session_num = cell2mat(exec_sql('QUANT',['SELECT MAX(session_num) from ',quant,'..session']));
        if isnan(last_session_num)
            session_num = 1;
        else
            session_num = 1 + last_session_num;
        end

        %Insertion du num�ro de la session dans la table session
        session_id = se_db('add_session',session_num, datestr(floor(now)), datestr(now, 'HH:MM:SS'));
        
        
        % Destruction des anciens jobs
        se_monitor('destroy', 'status', 'finished', ...
            'jm', findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL',jobmanager));
        se_monitor('destroy', 'status', 'failed', ...
            'jm', findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL',jobmanager));
        se_kill_daemon();
        
        
        % G�n�ration de la date du jour
        now_ = now - decay;
        w = weekday(now_);
        main_date = datestr(now_ - (w==1)*2-(w==7), 'dd/mm/yyyy');
        
        if (opt.get('exec_phase')==1) %Mise � jour des p�rim�tres une seule fois par lancement du SE, lors de la premiere phase d'execution
            se_main('perimeter', 'main-date', main_date, 'session-num', session_num, ...
                'jm', findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL',jobmanager),'exec_phase' ,opt.get('exec_phase'));
        end
        job_list_run = se_main('run', 'main-date', main_date, 'session-num', session_num, ...
            'jm', findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL',jobmanager),'exec_phase' ,opt.get('exec_phase'));
        job_list_check = se_main('check', 'main-date', main_date, 'session-num', session_num, ...
            'jm', findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL',jobmanager), 'exec_phase' ,opt.get('exec_phase'));
        
        %fermeture du fichier  se_start_mdce_DATE.txt
        st_log('SE_START_MDCE ended at %s\n', datestr(now));
        st_log('$close$');
        
        if ~iscell(job_list_run)
            job_list_run = {job_list_run};
        end
        if ~iscell(job_list_check)
            job_list_check = {job_list_check};
        end
        job_list = {job_list_run{:}, job_list_check{:}};
        
        se_last_job(job_list);
        
        %Mise � jour de la table session (fin de la session en cours)
        se_db('update_session', datestr(floor(now)), datestr(now,'HH:MM:SS'), session_id);
        clear session_id
    end

    %iCheck
    try
%     arrayfun ( @( a ) se_integrity_check ( a, false ), se_integrity_load_db () )
    catch e
        stack_str = se_stack_error_message(e);
        sendmail(st_version.my_env.se_admin,['iCheck error session_num = <',...
            num2str(session_num),'> env = <',upper(st_version.my_env.target_name),'>'],...
            sprintf([e.message,'\n%s'],stack_str));
    end
    %< Archive dans last_associations
    % 1. efface last_associations
    st_log('Deleting the "last_association" table...\n');
    exec_sql('QUANT',['DELETE FROM ',quant,'..last_association']);
    st_log('Done.\n');
    % 2. archive dans last_associations
    st_log('Archiving the "association" table into "last_association"...\n');
    exec_sql('QUANT',[ ...
        'insert into ',quant,'..last_association (association_id ,	context_id,	rank, security_id, trading_destination_id, estimator_id, job_id, varargin, from_meta) ' ...
        ' select association_id , context_id, rank, security_id, trading_destination_id, estimator_id, job_id, varargin, from_meta from ',quant,'..association ']);
    st_log('... FIN\n');
    %>
catch e
    if exist('session_id','var')
        se_db('update_session', datestr(floor(now)), datestr(now,'HH:MM:SS'), session_id);
    end
    if ~exist('session_num','var')
        session_num = nan;
    end
    stack_str = se_stack_error_message(e);
    sendmail(st_version.my_env.se_admin,['INTERRUPTION BRUTALE SESSION NUM <',...
        num2str(session_num),'> SE <',...
        upper(st_version.my_env.target_name),'>'],...
        sprintf([e.message,'\n%s'],stack_str));
end
quit