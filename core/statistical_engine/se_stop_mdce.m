% SE_STOP_MDCE - stop the SE
%
%
% See also se_main, se_stop
%
% Author   'Edouard d'Archimbaud'
% Version  '1.0'
% Reviewer ''
% Date     '12/11/2008'


% R�cup�ration des donn�es
xml  = xmltools('st_work.xml');
envs = xmltools(xml.get_tag('ENVS'));
all_env = xmltools(envs.get_tag('ENV'));
for i=1:all_env.nb_children()
    env = xmltools(all_env.keep_children(i));
    if strcmpi(env.get_attrib_value('env','hostname'), hostname())
        jobmanager = env.get_attrib_value('env','jobmanager');
    end
end


se_monitor('destroy', 'status', 'queued', ...
    'jm', findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL',jobmanager));
se_monitor('destroy', 'status', 'pending', ...
    'jm', findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL',jobmanager));
se_monitor('destroy', 'status', 'failed', ...
    'jm', findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL',jobmanager));
se_monitor('destroy', 'status', 'finished', ...
    'jm', findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL',jobmanager));
se_monitor('destroy', 'status', 'running', ...
    'jm', findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL',jobmanager));

clear all;

exit;
