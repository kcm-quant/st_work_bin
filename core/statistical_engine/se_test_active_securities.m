function out = se_test_active_securities(as_of_date, send_a_mail)
% se_test_active_securities(get_as_of_date )
%
global st_version

if nargin <2
    send_a_mail = false;
end

sv = st_version.bases.quant;

comp = cell2mat(exec_sql('QUANT', sprintf(['SELECT DISTINCT ds.security_id ' ...
    ' FROM %s..perimeter_job pj, %s..domain_security ds, %s..job j ' ...
    ' WHERE pj.is_active=1 and pj.job_id = j.job_id and j.domain_id = ds.domain_id'], ...
            sv, sv, sv)));

% comp = comp(1:100);
        
sec_infos = se_test_security(comp, as_of_date);
idx2del = cellfun(@isempty, sec_infos, 'uni', true);
sec_infos(idx2del) = [];

if ~isempty(sec_infos); %,sgalzin@smtpnotes
    out = {'rburgot@smtpnotes', 'Inconsistencies in some securities of active jobs', ...
        cat(2, sec_infos{:}, ... % � mettre au d�but du message pour un test : sprintf('Bonjour, ce mail est un test pour l''automatisation de la notification d''incoh�rences dans la composition des indices. Merci de me r�pondre pour me confirmer que vous avez bien re�u ce message et n''h�sitez pas � me dire si ces messages vous paraissent incompr�hensibles. J''aurai de plus besoin des adresses de chacun dans l''�quipe BO referentiel, puisque l''adresse referential.trading@smtpnotes ne semble pas fonctionner. Voici le corps des futurs messages :\n\n')
        sprintf('\nP.S.: It is a system generated message from Cheuvreux Quantitative Research.\nIf you need any information, please contact ITDev.RechQuant')), ...
        1};
    if send_a_mail
        se_sendmail(out{1:end-1});
    end
end

