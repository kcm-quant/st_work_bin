function out = se_test_lmi(as_of_date)
% SE_TEST_LMI - looks at the consistuents of the local main indices for
% inconsistencies. If any found, then it sends email to warn people
%
% ex : se_test_lmi(get_as_of_date())

% TODO: retrieve this from quant database + add email adresses of the whole
% team BO referential (the common address referential.trading@smtpnotes)
% does not seem to work

% RECIPIENTS = {'ntounsi@smtpnotes', 'rburgot@smtpnotes', 'clehalle@smtpnotes', 'dcroize@smtpnotes'};
% 'referential.trading@smtpnotes' -> ne semble pas fonctionner


CHECKED_PERIMETERS = {'lmi', 'lmi components', 'lmi components mtd', ...
    'lmi mtd'};

% TODO before adding america and asia fonstions like se_test_security 
% have to be improved in order to use trading_daily_ameri

global st_version
quant = st_version.bases.quant;

% < retrieving the list of indices which have to be tested for
% inconsistencies
params = sprintf('''%s'',', CHECKED_PERIMETERS{:});
params = exec_sql('QUANT', sprintf(['select parameters ' ... 
    ' from ',quant,'..perimeter where perimeter_name in (%s)'], params(1:end-1)));
indices_list = {};
for i = 1 : length(params)
    tmp = eval(params{i});
    idx = strmatch('index', tmp(1:2:end), 'exact');
    indices_list = cat(2, indices_list, tmp{2*idx});
end
indices_list = unique(indices_list);
% >

out = {};
for i = 1 : length(indices_list)
    comp = get_repository('index-comp', indices_list{i}, 'recent_date_constraint', false);
    sec_infos = se_test_security(comp, as_of_date);
    idx2del = cellfun(@isempty, sec_infos, 'uni', true);
    sec_infos(idx2del) = [];
    if ~isempty(sec_infos); %,sgalzin@smtpnotes
        out(end + 1, :) = {'rburgot@smtpnotes', sprintf('Inconsistencies in <%s> consistuents', indices_list{i}), ...
            cat(2, sec_infos{:}, ... % � mettre au d�but du message pour un test : sprintf('Bonjour, ce mail est un test pour l''automatisation de la notification d''incoh�rences dans la composition des indices. Merci de me r�pondre pour me confirmer que vous avez bien re�u ce message et n''h�sitez pas � me dire si ces messages vous paraissent incompr�hensibles. J''aurai de plus besoin des adresses de chacun dans l''�quipe BO referentiel, puisque l''adresse referential.trading@smtpnotes ne semble pas fonctionner. Voici le corps des futurs messages :\n\n')
            sprintf('\nP.S.: It is a system generated message from Cheuvreux Quantitative Research.\nIf you need any information, please contact ITDev.RechQuant')), ...
            1};
    end
end



end