function out = se_test_run_quality(varargin)
% SE_TEST_RUN_QUALITY - to get easily run qualities...
% Returns a st_data
%
% se_test_run_quality('run-date', '08/12/2008', 'job_id',1)
% se_test_run_quality('run-date', '08/12/2008', 'job_id',(1:2))
% se_test_run_quality('run-date', '08/12/2008', 'estimator_id', 2)
% 
% Date: 18/12/2008
% Author: edarchimbaud@cheuvreux.com
% Version: 1.0

opt = options({'run-date', '10/12/2008', ...
    'job_id', 1, ...
    'estimator_id', NaN}, ...
    varargin);


if isnan(opt.get('estimator_id'))
    job = opt.get('job_id');
else
    job = cell2mat(exec_sql('QUANT', sprintf('SELECT job_id FROM job WHERE estimator_id=%d', opt.get('estimator_id'))));
end


rslt = arrayfun(@(i)se_run('local', 'job_id', i, 'job_name', 'test', 'run-date', opt.get('run-date'), 'debug', 1), job);

security_id = arrayfun(@(i)rslt(i).info.security_id, 1:length(rslt));
trading_destination_id = arrayfun(@(i)rslt(i).info.td_info.trading_destination_id, 1:length(rslt));
run_quality = arrayfun(@(i)rslt(i).info.run_quality, 1:length(rslt));
security_key = cellfun(@(i)rslt(i).info.security_key, num2cell(1:length(rslt)), 'UniformOutput', false);

out = st_data('init', 'title', 'Run qualities', 'value', [security_id' trading_destination_id' run_quality'], ...
    'date', (1:length(security_id))', 'colnames', { 'security_id', 'trading_destination_id' , 'run_quality'});
data.rownames = security_key';


end