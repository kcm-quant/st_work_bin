function msg = se_test_security(security_id, as_of_date)
% SE_TEST_SECURITY - regroupement d'information sur un security_id pour
% savoir ce qui lui arrive
%
% out = se_test_security(110)
% out = se_test_security([110, 362])


msg = {se_test_security_unit(security_id(1), as_of_date)};
for i = 2 : length(security_id)
    msg{end+1} = se_test_security_unit(security_id(i), as_of_date);
end

end

function msg = se_test_security_unit(security_id, as_of_date)
sec_info = get_security_informations_bufferized(security_id, as_of_date);
% TODO :
% ici il faudrait exploiter les info et les résumés pour en faire
% quelquechose de compréhensible.
% Pour l'instant on se contente de filtrer les info importantes et
% compréhensibles
sec_info = get_severe_info(sec_info.summary, 4, true);
msg = get_msg_from_summary_struct(sec_info);
if ~isempty(msg)
    ric = cell2mat(get_repository( 'sec_id2ric', security_id));
    if isempty(ric)
        ric_str = ' no ric in security_source';
    else
        ric_str = sprintf('ric: <%s>', ric);
    end
    sname = cell2mat(get_repository('sec_id2name', security_id));
    if isempty(sname)
        sname = 'NO NAME STOCK!';
    end
    msg = sprintf('%s (security_id: <%d>, %s):\n%s', sname, ...
        security_id, ric_str, msg);
end
end