function se_update_old_association(job_id)
% SE_UPDATE_OLD_ASSOCIATION - cr�e une association pour les job orphelins
%
% job_id = cell2mat(exec_sql('QUANT', sprintf('select distinct er.job_id from %s..estimator_runs er where er.is_valid=1 and er.job_id not in (select job_id from %s..association)  and er.job_id in (select job_id from %s..perimeter_job where is_active=1)', st_version.bases.quant, st_version.bases.quant, st_version.bases.quant)));
% se_update_old_association(job_id)


STEP = 1000;


global st_version;
sv = st_version.bases.quant;

for i=1:length(job_id)
    
    rslt = exec_sql('QUANT', sprintf(['SELECT j.estimator_id, d.domain_type_variable, d.domain_type_rank, ma.request, rt.request_type_name ' ...
        ' FROM %s..domain d, %s..job j, %s..meta_association ma, %s..request_type rt ' ...
        ' WHERE j.job_id=%d AND j.domain_id=d.domain_id AND ma.estimator_id=j.estimator_id AND ma.request_type_id=rt.request_type_id ' ...
        ' AND ma.domain_type_id=d.domain_type_id'], sv, sv, sv, sv, job_id(i)));
    
    rslt_unique = unique_cell_vert(rslt);
    
    for j=1:size(rslt_unique, 1)
        
        domain_type_variable = regexprep(rslt_unique{j, 2}, '''', '''''');
        
        try
            query = regexprep(rslt_unique{j, 4}, '\$r', num2str(rslt_unique{j, 3} + STEP));
            query = regexprep(query, '\$v', domain_type_variable);
            query = regexprep(query, '\$e', num2str(rslt_unique{j, 1}));
            query = regexprep(query, '\$j', num2str(job_id(i)));
            query = regexprep(query, '\$b', sv);
            st_log('Mise � jour de l''association du job <%d>\n', job_id(i));
            switch lower(rslt_unique{j, 5})
                case 'matlab'
                    eval(query);
                case 'sql'
                    exec_sql('QUANT', query);
                otherwise
                    error('se_meta_update: request_type_name <%s> doesn''t exist', lower(request_type));
            end
        catch
            query = regexprep(rslt_unique{j, 4}, '\$r', num2str(rslt_unique{j, 3} + STEP + 20 + randi(100)));
            query = regexprep(query, '\$v', domain_type_variable);
            query = regexprep(query, '\$e', num2str(rslt_unique{j, 1}));
            query = regexprep(query, '\$j', num2str(job_id(i)));
            query = regexprep(query, '\$b', sv);
            st_log('Mise � jour de l''association du job <%d>\n', job_id(i));
            switch lower(rslt_unique{j, 5})
                case 'matlab'
                    eval(query);
                case 'sql'
                    exec_sql('QUANT', query);
                otherwise
                    error('se_meta_update: request_type_name <%s> doesn''t exist', lower(request_type));
            end
        end
        
    end
    
end



    function out = unique_cell_vert(data)
        
        out = {};
        
        for ii=1:size(data, 1)
            is_in = false;
            for jj=size(out, 1)
                is_equal = true;
                for kk=1:size(out, 2)
                    if data{ii, kk} ~= out{jj, kk}
                        is_equal = false;
                    end
                end
                if size(out, 2) == 0
                    is_equal = false;
                end
                if is_equal
                    is_in = true;
                end
            end
            if ~is_in
                if size(out, 1) == 0
                    out = {data{ii, :}};
                else
                    out = {out; {data{ii, :}}};
                end
            end
        end
        
    end


end