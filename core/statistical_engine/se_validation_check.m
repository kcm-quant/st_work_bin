function data = se_validation_check(estimator , varargin)
% SE_VALIDATION_CHECK - Short_one_line_description
%
%
%
% Examples:
% se_validation_check('volume curve', 'domain_type', 'representative index', 'session', 906);
%
%
%
% See also:
%
%
%   author   : 'dcroize@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '21/06/2011'

global st_version
q_base = st_version.bases.quant;

opt = options( { ...
    'time', '' , ...
    'plot', false, ...
    'session', nan , ...
    'mail', '', ...
    'first-validation', 0 , ...
    'estimator-id', [], ...
    'domain_type', 'mono instrument' ,...
    'refresh',0 , ...
    'DB_validation' , 1 }, varargin);


%R�cuperation du nom de l'estimateur si besoins est
if ischar(estimator)
    estimator_name = estimator;
else
    estimator_name =cell2mat( exec_sql('QUANT', sprintf( 'select estimator_name from %s.. estimator where estimator_id = %d' , q_base, estimator)));
end


if ~isnan(opt.get('session'))
    session_num = opt.get('session');
else
    session_num = cell2mat(exec_sql('QUANT', sprintf('SELECT max(session_num) from %s..session ',q_base)));
end

%Validation sur les runs du type de domain choisit
domain_type = opt.get('domain_type');
dmn_type = sprintf('dt.domain_type_name = ''%s'' AND ' ,domain_type );


%R�cup�ration des jobs � traiter
quals = exec_sql('QUANT', sprintf( [ ...
    'SELECT  distinct lr.job_id, lr.context_id, lr.last_run_date, lr.last_run_id, lr.last_valid_run_id, est_r.run_quality '...
    'FROM  '...
    '%s..estimator est , '...
    '%s..context ctxt,  '...
    '%s..domain dm ,  '...
    '%s..domain_type dt , '...
    '%s..job jb,  '...
    '%s..perimeter_domain pd ,  '...
    '%s..last_run lr,  '...
    '%s..association ass, '...
    '%s..estimator_runs est_r '...
    'WHERE '...
    'upper(est.estimator_name) = upper(''%s'') AND '...
    'jb.estimator_id = est.estimator_id AND '...
    'pd.is_active = 1 AND  '...
    '%s '...  %dmn_type
    'dm.domain_type_id = dt.domain_type_id AND  '...
    'dm.domain_id = jb.domain_id AND  '...
    'jb.domain_id = pd.domain_id AND '...
    'jb.job_id = lr.job_id AND '...
    'est.estimator_id = ctxt.estimator_id AND '...
    'est.validation_context_num = ctxt.context_num AND  '...
    'ctxt.context_id = lr.context_id AND '...
    'isnull(lr.last_run_id,-1) <>isnull(lr.last_valid_run_id,-2)  AND ' ...
    'lr.job_id = ass.job_id AND  '...
    'lr.context_id = ass.context_id AND '...
    'lr.last_run_id = est_r.run_id '],q_base,q_base, q_base, q_base,q_base, q_base,q_base,q_base,q_base, ...
    upper(estimator_name) , dmn_type ));

colnames = {  'job_id', 'context_id', 'last_run_date', 'last_run_id' , 'last_valid_run_id', 'quality' };


try
    
    if (~isempty(quals))
        
        job2update_validation_data = st_data('from-cell', 'job2update_validation_data', colnames, quals);
        job_ids = st_data('cols' , job2update_validation_data , 'job_id');
        
        
        %**********************************************************
        %UPDATE VALIDATION
        %***********************************************************
        
        job2update = [];
        job_first_validation = [];
        check_last_run_error =[];
        check_last_and_valid_error =[];
        missing_last_run = [];
        job_run_quality_validation = [];
        
        %Liste des runs valides de l'estimateur
        last_valid = exec_sql('QUANT', sprintf( [ ...
            'SELECT run.estimator_id, run.run_id, run.job_id, run.run_quality, run.stamp_date ' ...
            'FROM ' ...
            '%s..estimator es, '...
            '%s..estimator_runs run , '...
            '%s..context ctx  ' ...
            'WHERE upper(es.estimator_name) = ''%s'' AND ' ...
            'run.is_valid = 1 AND ' ...
            'es.estimator_id = run.estimator_id AND ' ...
            'ctx.context_num = es.validation_context_num AND ' ...
            'run.context_id  = ctx.context_id ' ...
            'ORDER BY   run.job_id  '], q_base ,q_base , q_base , upper(estimator_name )));
        
        colnames = { 'lst_estimator', 'lst_run'  'lst_job_id','lst_quality','stamp_date' };
        data_lv = st_data('from-cell', 'Les Last_valid', colnames , last_valid);
        min_lst_valid = min(st_data('cols',data_lv ,'lst_quality'));
        
        
        for i = 1 : length(job_ids)
            %R�cuperation des valeurs du check sur le last_run
            %et le check sur le last_valid_run
            
            last_run_id  = st_data('cols' ,st_data('where', job2update_validation_data, sprintf('{job_id}==%d',job_ids(i))) ,'last_run_id');
            valid_run_id = st_data('cols' ,st_data('where', job2update_validation_data, sprintf('{job_id}==%d',job_ids(i))) ,'last_valid_run_id');
            
            run_ids = [last_run_id valid_run_id ];
            
            %R�cuperation des check_quality
            check_last_run_id  = NaN;
            check_valid_run_id = NaN;
            
            for j = 1 : 2
                SQL_query = sprintf([ ...
                    'SELECT cq.*  ' ...
                    'FROM %s..check_quality cq ' ...
                    'WHERE  ' ...
                    'cq.run_id = %d and ' ...
                    'session_num = %d  ' ] ,q_base ,run_ids(j) , session_num);
                
                switch j
                    case 1
                        if ~isnan(last_run_id)
                            check_last_run_id =  exec_sql('QUANT',SQL_query);
                            if isempty(check_last_run_id)
                                check_last_run_id = NaN;
                            end
                        else
                            check_last_run_id =  NaN;
                        end
                        
                    case 2
                        if ~isnan(valid_run_id)
                            check_valid_run_id =  exec_sql('QUANT',SQL_query);
                            if isempty(check_valid_run_id)
                                check_valid_run_id = NaN;
                            end
                        else
                            check_valid_run_id =  NaN;
                        end
                end
                
            end
            
            if ~isnan(last_run_id) && ~isnan(valid_run_id)
                %/************************************************
                % last_run et last_valid existent tous les deux
                %*************************************************/
                
                % Cas 1 : check_last_run et check_valid_run existent tous les deux
                %Si check_last_run > check_valid_run : on d�valide valid_run et on valide last_run
                % Si check_last_run < check_valid_run : rien ne change
                %if ~isnan(check_last_run_id) && ~isnan(check_valid_run_id)
                if iscell(check_last_run_id) && iscell(check_valid_run_id)
                    check_last_run_id_data = st_data('from-cell', 'check_last_run_id_data', {'check_quality_id', 'last_run_id', 'value', 'stamp_date', 'comment', 'session_num'}, check_last_run_id) ;
                    check_valid_run_id_data = st_data('from-cell', 'check_valid_run_id_data',{'check_quality_id', 'valid_run_id', 'value', 'stamp_date', 'comment', 'session_num'}, check_valid_run_id);
                    if st_data('cols' ,check_last_run_id_data , 'value')>st_data('cols' ,check_valid_run_id_data , 'value')
                        res =  cell2mat(exec_sql('QUANT', sprintf([...
                            'SELECT est_r.job_id, est_r.session_num '...
                            'FROM %s..estimator_runs est_r ' ...
                            'WHERE est_r.run_id = %d '],q_base ,st_data('cols' ,check_last_run_id_data , 'last_run_id'))));
                        job2update = [job2update ; res] ;
                        
                        %else RIEN NE CHANGE
                        
                    end
                    
                    % Cas 2 : check_last_run existe et pas check_valid_run
                    % d�validation du valid_run et validation du last_run
                    %
                elseif iscell(check_last_run_id) && ~iscell(check_valid_run_id)
                    check_last_run_id_data = st_data('from-cell', 'check_last_run_id_data', {'check_quality_id', 'last_run_id', 'value', 'stamp_date', 'comment', 'session_num'}, check_last_run_id) ;
                    res =  cell2mat(exec_sql('QUANT', sprintf([...
                        'SELECT est_r.job_id, est_r.session_num '...
                        'FROM %s..estimator_runs est_r ' ...
                        'WHERE est_r.run_id = %d '],q_base ,st_data('cols' ,check_last_run_id_data , 'last_run_id'))));
                    
                    job2update = [job2update ; res] ;
                    
                    
                    %Cas 3 : check_last_run n'existe pas et check_valid_run existe
                    %Remont� d'une erreur car cela est ANORMAL!!!!
                elseif ~iscell(check_last_run_id) && iscell(check_valid_run_id)
                    check_last_run_error =  [check_last_run_error ;job_ids(i)] ;
                    
                    
                    %Cas 4 : check_last_run et check_valid_run n'existent pas tous les deux
                    %Remont� d'un warning ET VALIDATION SUR LES RUN_QUALITY
                elseif ~iscell(check_last_run_id) && ~iscell(check_valid_run_id)
                    run_qual_last_run_id = se_db('get_estimator_run', 'job_id', job_ids(i) , 'run_id' ,last_run_id );
                    run_qual_valid_run_id = se_db('get_estimator_run', 'job_id', job_ids(i) , 'run_id' ,valid_run_id );
                    
                    switch domain_type
                        case  'mono instrument'
                            if (getfield(run_qual_last_run_id, 'run_quality')>=getfield(run_qual_valid_run_id , 'run_quality'))
                                job_run_quality_validation = [job_run_quality_validation ;job_ids(i) getfield(run_qual_last_run_id , 'session_num')] ;
                            end
                            
                        otherwise
                            job_run_quality_validation = [job_run_quality_validation ;job_ids(i) getfield(run_qual_last_run_id , 'session_num') ] ;
                    end
                    
                    check_last_and_valid_error = [check_last_and_valid_error ; job_ids(i)];
                end
                
                
            elseif ~isnan(last_run_id) && isnan(valid_run_id)
                %/***************************************
                % last_run existe mais pas de last_valid
                %**************************************/
                %Cas des jobs jamais valid�s dans le pass�
                %Validation sur run_quality comme fait habituellement
                switch (domain_type)
                    
                    case 'mono instrument'
                        if (isempty(last_valid))
                            error(sprintf('<%s> :Jobs are not yet validated, you have to execute a "first validation" ',mfilename))
                        end
                        
                        %Comparaison last_run_quality > min_last_valid
                        this_jb = st_data('where',job2update_validation_data, sprintf('{job_id}==%d & {quality}>= %d ',job_ids(i), min_lst_valid));
                        
                    otherwise
                        this_jb = st_data('where',job2update_validation_data, sprintf('{job_id}==%d',job_ids(i)));
                end
                
                if ~st_data('isempty-nl', this_jb)
                    this_jb_session = cell2mat(exec_sql('QUANT', sprintf([...
                        'SELECT session_num FROM %s.. estimator_runs where run_id = %d' ], q_base, st_data('cols', this_jb , 'last_run_id'))));
                    job_first_validation = [job_first_validation ; st_data('cols', this_jb ,'job_id') this_jb_session];
                end
                
                
            elseif isnan(last_run_id)
                %/******************
                % last_run n'existe
                %********************/
                %Envoie d'un mail d'ERREUR , � �tudier....
                missing_last_run = [missing_last_run ; job_ids(i)];
            end
            
        end
        
        
        %Pour les jobs [check_last_run_error], ATTENTION seuls ceux qui n'ont
        %pas tourn� lors de la derni�re session sont � remonter(pour les autres cela est normal 
        %et ces derniers sont mis � jour dans la session suivante)
        
        if ~isempty(check_last_run_error)
        check_last_run_error_str = regexprep(convs('str' , check_last_run_error),{'['  ';' ']'}, {'\t'  ',' '\t'});
        
        res1= cell2mat(exec_sql('QUANT', sprintf([... 
            'SELECT job_id ' ...
            'FROM %s..estimator_runs  ' ...
            'WHERE job_id in (%s) AND ' ...
            'session_num = %d ' ], q_base ,check_last_run_error_str, session_num)));
        
        check_last_run_error = setdiff(check_last_run_error , res1);
        end
        
        %VALIDATION et MISE A JOUR DE LA BASE DE DONNEES
        st_log('$off$') %D�sactivation du st_log pour �viter les erreurs de type JZ0R2
        if opt.get('DB_validation')
            all_job2valid =  [job2update ; job_first_validation ; job_run_quality_validation];
            if ~isempty(all_job2valid)
                for i = 1 : size(all_job2valid,1)
                    try
                        exec_sql('QUANT', sprintf( ' exec %s..upd_validation %d , 1 , %d ', q_base , all_job2valid(i, 1) , all_job2valid(i, 2) ) )
                    catch er
                        if isempty(strfind( er.message, 'JZ0R2'))
                            rethrow( er);
                        end
                    end
                end
            end
        end
        st_log('$on$') %R�sactivation du st_log
        
        %GENERATION DE RAPPORTS D'ERREURS
        if ~isempty(job2update)
            job2update = job2update(:,1);
        end
        
        if ~isempty(job_first_validation)
            job_first_validation = job_first_validation(:,1);
        end
        
        if ~isempty(job_run_quality_validation)
            job_run_quality_validation = job_run_quality_validation(:,1);
        end
        
        info_validation = struct('job2update', job2update, 'first_validation',job_first_validation , ...
            'check_last_run_error' ,check_last_run_error, ...
            'check_last_and_valid_error' ,check_last_and_valid_error, ...
            'last_run_not_exist' , missing_last_run ,...
            'job_run_quality_validation' , job_run_quality_validation);
        se_validation_check_report(estimator_name, info_validation , domain_type , 'session-num' , session_num)
    end
    
catch er
    if isempty(strfind( er.message, 'JZ0R2'))
        rethrow( er);
    end
    
    st_log(sprintf('ERROR : %s\n', e.message));
    error_message = se_stack_error_message(e);
    if ~ispc()
        error_message = regexprep(error_message, '\n', '\r');
    end
    %envoie de mail
    identification_err = sprintf('%s%s%s', 'ECHEC DANS LA VALIDATION A PARTIR DES CHECKS POUR L''ESTIMATEUR SUIVANT:',sprintf('%s%s',upper(mail{k, 3}),blank_char),error_message);
    subject = sprintf('Echec dans la validation � partir des checks pour l estimateur: %s %s' ,upper(mail{k, 3}),hostname());
    
    se_sendmail( mail{k, 2},subject,identification_err)
    
end



end