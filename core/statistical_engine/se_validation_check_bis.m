function se_validation_check_bis(estimator , varargin)
global st_version
quant = st_version.bases.quant;

opt = options( { ...
    'time', '' , ...
    'plot', false, ...
    'session', nan , ...
    'mail', '', ...
    'first-validation', 0 , ...
    'domain_type', 'mono instrument' ,...
    'refresh',0 , ...
    'DB_validation' , 1 }, varargin);


%R�cuperation du nom de l'estimateur si besoins est
if ischar(estimator)
    estimator_name = estimator;
else
    estimator_name =cell2mat( exec_sql('QUANT', sprintf( 'select estimator_name from %s.. estimator where estimator_id = %d' , quant, estimator)));
end


if ~isnan(opt.get('session'))
    session_num = opt.get('session');
else
    session_num = cell2mat(exec_sql('QUANT', sprintf('SELECT max(session_num) from %s..session ',quant)));
end

%Validation sur les runs du type de domain choisit
domain_type = opt.get('domain_type');
dmn_type = sprintf('dt.domain_type_name = ''%s'' AND ' ,domain_type );


%R�cup�ration des jobs � traiter
quals = exec_sql('QUANT', sprintf( [ ...
    'SELECT  distinct lr.job_id, lr.context_id, lr.last_run_date, lr.last_run_id, lr.last_valid_run_id, est_r.run_quality '...
    'FROM  '...
    '%s..estimator est , '...
    '%s..context ctxt,  '...
    '%s..domain dm ,  '...
    '%s..domain_type dt , '...
    '%s..job jb,  '...
    '%s..perimeter_domain pd ,  '...
    '%s..last_run lr,  '...
    '%s..association ass, '...
    '%s..estimator_runs est_r '...
    'WHERE '...
    'upper(est.estimator_name) = upper(''%s'') AND '...
    'jb.estimator_id = est.estimator_id AND '...
    'pd.is_active = 1 AND  '...
    '%s '...  %dmn_type
    'dm.domain_type_id = dt.domain_type_id AND  '...
    'dm.domain_id = jb.domain_id AND  '...
    'jb.domain_id = pd.domain_id AND '...
    'jb.job_id = lr.job_id AND '...
    'est.estimator_id = ctxt.estimator_id AND '...
    'est.validation_context_num = ctxt.context_num AND  '...
    'ctxt.context_id = lr.context_id AND '...
    'isnull(lr.last_run_id,-1) <>isnull(lr.last_valid_run_id,-2)  AND ' ...
    'lr.job_id = ass.job_id AND  '...
    'lr.context_id = ass.context_id AND '...
    'lr.last_run_id = est_r.run_id '],quant,quant, quant, quant,quant, quant,quant,quant,quant, ...
    upper(estimator_name) , dmn_type ));

colnames = {  'job_id', 'context_id', 'last_run_date', 'last_run_id' , 'last_valid_run_id', 'quality' };


try
    
    if (~isempty(quals))
        
        job2update_validation_data = st_data('from-cell', 'job2update_validation_data', colnames, quals);
        
        % get last_run_qual, last_valid_qual, check_last_run, check_last_valid
        
        run_str = sprintf('%d,',[st_data('cols',job2update_validation_data,'last_valid_run_id');...
            st_data('cols',job2update_validation_data,'last_run_id')]);
        run_str = strrep(run_str,'NaN,','');
        
        res = exec_sql('QUANT',sprintf(['select run_id,value',...
            ' from ',quant,'..check_quality',...
            ' where run_id in (%s)',...
            ' and session_num = %d'],run_str(1:end-1),session_num));
        
        check_last_run = nan(length(job2update_validation_data.date),1);
        check_last_valid = nan(length(job2update_validation_data.date),1);
        if ~isempty(res)
            [~,a,b] = intersect(st_data('cols',job2update_validation_data,'last_run_id'),cell2mat(res(:,1)));
            check_last_run(a) = cell2mat(res(b,2));
            [~,a,b] = intersect(st_data('cols',job2update_validation_data,'last_valid_run_id'),cell2mat(res(:,1)));
            check_last_valid(a) = cell2mat(res(b,2));
        end
        
        res = exec_sql('QUANT',sprintf(['select run_id,run_quality',...
            ' from ',quant,'..estimator_runs',...
            ' where run_id in (%s)'],run_str(1:end-1)));
        
        last_run_qual = nan(length(job2update_validation_data.date),1);
        last_valid_run_qual = nan(length(job2update_validation_data.date),1);
        [~,a,b] = intersect(st_data('cols',job2update_validation_data,'last_run_id'),cell2mat(res(:,1)));
        last_run_qual(a) = cell2mat(res(b,2));
        [~,a,b] = intersect(st_data('cols',job2update_validation_data,'last_valid_run_id'),cell2mat(res(:,1)));
        last_valid_run_qual(a) = cell2mat(res(b,2));
        
        
        job2update_validation_data = ...
            st_data('add-col',job2update_validation_data,[last_run_qual,last_valid_run_qual,check_last_run,check_last_valid],...
            'last_run_qual;last_valid_run_qual;check_last_run;check_last_valid');
        
        valid_last_run = false(length(job2update_validation_data.date),1);
        
        last_run_id         = st_data('cols',job2update_validation_data,'last_run_id');
        valid_run_id        = st_data('cols',job2update_validation_data,'last_valid_run_id');
        check_last_run      = st_data('cols',job2update_validation_data,'check_last_run');
        check_last_valid    = st_data('cols',job2update_validation_data,'check_last_valid');
        last_run_qual       = st_data('cols',job2update_validation_data,'last_run_qual');
        last_valid_run_qual = st_data('cols',job2update_validation_data,'last_valid_run_qual');
        
        %/************************************************
        % last_run et last_valid existent tous les deux
        %*************************************************
        
        % Cas 1 : check_last_run et check_valid_run existent tous les deux
        %     a. Si check_last_run > check_valid_run : on d�valide valid_run et on valide last_run
        %     b. Si check_last_run < check_valid_run : On ne change rien
        valid_last_run(~isnan(last_run_id) & ~isnan(valid_run_id) & (check_last_run > check_last_valid)...
            ... ~isnan(check_last_run) & ~isnan(check_last_valid) : REDONDANT AVEC (CHECK_LAST_RUN > CHECK_LAST_VALID)
            ) = true;
        job2update = st_data('cols',job2update_validation_data,'job_id',...
            ~isnan(last_run_id) & ~isnan(valid_run_id) & (check_last_run > check_last_valid));
        
        % Cas 2 : check_last_run existe et pas check_valid_run
        % d�validation du valid_run et validation du last_run
        valid_last_run(~isnan(last_run_id) & ~isnan(valid_run_id) & ~isnan(check_last_run) & isnan(check_last_valid)) = true;
        job2update = [st_data('cols',job2update_validation_data,'job_id',...
            ~isnan(last_run_id) & ~isnan(valid_run_id) & ~isnan(check_last_run) & isnan(check_last_valid));...
            job2update];
        
        % Cas 3 : check_last_run n'existe pas et check_valid_run existe
        % Remont� d'une erreur car cela est ANORMAL!!!!
        check_last_run_error = st_data('cols',job2update_validation_data,'job_id',...
            ~isnan(last_run_id) & ~isnan(valid_run_id) & isnan(check_last_run) & ~isnan(check_last_valid));
        
        % Cas 4 : check_last_run et check_valid_run n'existent pas tous les deux
        % Remont� d'un warning ET VALIDATION SUR LES RUN_QUALITY
        switch domain_type
            case  'mono instrument'
                valid_last_run(~isnan(last_run_id) & ~isnan(valid_run_id) & isnan(check_last_run) & isnan(check_last_valid) & ...
                    last_run_qual > last_valid_run_qual) = true;
                job_run_quality_validation = st_data('cols',job2update_validation_data,'job_id',...
                    ~isnan(last_run_id) & ~isnan(valid_run_id) & isnan(check_last_run) & isnan(check_last_valid) & ...
                    last_run_qual > last_valid_run_qual);
            otherwise
                valid_last_run(~isnan(last_run_id) & ~isnan(valid_run_id) & isnan(check_last_run) & isnan(check_last_valid)) = true;
                job_run_quality_validation = st_data('cols',job2update_validation_data,'job_id',...
                    ~isnan(last_run_id) & ~isnan(valid_run_id) & isnan(check_last_run) & isnan(check_last_valid));
        end
        check_last_and_valid_error = st_data('cols',job2update_validation_data,'job_id',...
            ~isnan(last_run_id) & ~isnan(valid_run_id) & isnan(check_last_run) & isnan(check_last_valid));
        
        
        %***************************************
        % last_run existe mais pas de last_valid
        %***************************************
        % Cas des jobs jamais valid�s dans le pass�
        % Validation sur run_quality comme fait habituellement
        switch domain_type
            case  'mono instrument'
                valid_last_run(~isnan(last_run_id) & isnan(valid_run_id) & last_run_qual > last_valid_run_qual) = true;
                job_first_validation = st_data('cols',job2update_validation_data,'job_id',...
                    ~isnan(last_run_id) & isnan(valid_run_id) & last_run_qual > min(last_valid_run_qual));
            otherwise
                valid_last_run(~isnan(last_run_id) & isnan(valid_run_id)) = true;
                job_first_validation = st_data('cols',job2update_validation_data,'job_id',...
                    ~isnan(last_run_id) & isnan(valid_run_id));
        end
        
        
        % Pour les jobs [check_last_run_error], ATTENTION seuls ceux qui n'ont
        % pas tourn� lors de la derni�re session sont � remonter(pour les autres cela est normal
        % et ces derniers seront mis � jour dans la session suivante)
        if ~isempty(check_last_run_error)
            check_last_run_error_str = sprintf('%d,',check_last_run_error);
            res1= cell2mat(exec_sql('QUANT', sprintf([...
                'SELECT job_id ' ...
                'FROM ',quant,'..estimator_runs  ' ...
                'WHERE job_id in (%s) AND ' ...
                'session_num = %d ' ],check_last_run_error_str(1:end-1),session_num)));
            check_last_run_error = setdiff(check_last_run_error , res1);
        end
        
        %VALIDATION et MISE A JOUR DE LA BASE DE DONNEES
        if opt.get('DB_validation') && any(valid_last_run)
            valid_last_run_str = sprintf('%d,',st_data('cols',job2update_validation_data,'last_run_id',valid_last_run));
            job_sess_num = exec_sql('QUANT',sprintf(['select job_id,session_num from ',quant,'..estimator_runs',...
                ' where run_id in (%s)'],valid_last_run_str(1:end-1)));
            if ~isempty(job_sess_num)
                for i = 1 : size(job_sess_num,1)
                    exec_sql('QUANT', sprintf(['exec ',quant,'..upd_validation %d , 1 , %d '],...
                        job_sess_num{i,1},job_sess_num{i,2}));
                end
            end
        end
        
        % CREATION DU RAPPORT D'ERREURS
        info_validation = struct('job2update', job2update, 'first_validation',job_first_validation , ...
            'check_last_run_error' ,check_last_run_error, ...
            'check_last_and_valid_error' ,check_last_and_valid_error, ...
            'last_run_not_exist' , st_data('cols',job2update_validation_data,'job_id',isnan(last_run_id)) ,...
            'job_run_quality_validation' , job_run_quality_validation);
        se_validation_check_report(estimator_name, info_validation , domain_type , 'session-num' , session_num)
    end
    
catch er
    st_log(sprintf('ERROR : %s\n', er.message));
    error_message = se_stack_error_message(er);
    if ~ispc()
        error_message = regexprep(error_message, '\n', '\r');
    end
    %envoie de mail
    identification_err = sprintf('%s%s\n%s', 'ECHEC DANS LA VALIDATION A PARTIR DES CHECKS POUR L''ESTIMATEUR SUIVANT: ',upper(estimator_name),error_message);
    subject = sprintf('Echec dans la validation � partir des checks pour l estimateur: %s %s' ,upper(estimator_name),hostname());
    
    sendmail(st_version.my_env.se_admin,subject,identification_err)
end