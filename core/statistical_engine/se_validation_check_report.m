function data = se_validation_check_report(estimator_name, info , domain_type, varargin)
% SE_VALIDATION_CHECK_REPORT - Short_one_line_description
%
%
%
% Examples:
%
% 
% 
%
% See also:
%    
%
%   author   : 'dcroize@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '05/07/2011'


global st_version
q_base = st_version.bases.quant;

opt = options( { ...
    'mail',regexp(st_version.my_env.se_admin, '[,;]', 'split' ), ...
    'is-run', 1 ...
    'estimator-id', [] ...
    'session-num', NaN ...
    }, varargin);

mail = opt.get('mail');


 job2update = info.job2update ;
 job_first_validation = info.first_validation;
 check_last_run_error = info.check_last_run_error;
 check_last_and_valid_error = info.check_last_and_valid_error;
 missing_last_run = info.last_run_not_exist ;
 job_run_quality_validation = info.job_run_quality_validation;


% Table des jobs valid�s
val = {job2update,job_first_validation};
uer = {'REFRESH VALIDATION','FIRST VALIDATION'};
tbl = MARGINAL_TABLE( 6, uer, val, 'Nb Job','job');
rep.wiki.job_v = tbl.wiki;
rep.html.job_v = tbl.html;
rep.mail.job_v = tbl.mail;

%Jobs pour lesquels le check sur le last_run tombe en erreur.
val = {check_last_run_error};
uer = {'ERROR: Check failed on last_run (but succeeded on last_valid)'};
tbl = MARGINAL_TABLE( 6, uer, val, 'Nb Job','job');
rep.wiki.job_check_lr_error = tbl.wiki;
rep.html.job_check_lr_error = tbl.html;
rep.mail.job_check_lr_error = tbl.mail;


%Jobs pour lesquels le check sur le last_run et valid_run tombent en erreur.
val = {check_last_and_valid_error};
uer = {'WARNING: Check failed on last_run and valid_run'};
tbl = MARGINAL_TABLE( 6, uer, val, 'Nb Job','job');
rep.wiki.job_check_lr_vr_error = tbl.wiki;
rep.html.job_check_lr_vr_error = tbl.html;
rep.mail.job_check_lr_vr_error = tbl.mail;

%Jobs pour lesquels le check sur le last_run et valid_run tombent en
%erreur, mais poour lesquels la validation sur les run_qality a permis de
%mettre � jour la validation
val = {job_run_quality_validation};
uer = {'REFRESH VALIDATION on RUN_QUALITY: Check failed on last_run and valid_run'};
tbl = MARGINAL_TABLE( 6, uer, val, 'Nb Job','job');
rep.wiki.job_check_lr_vr_error_run_validation = tbl.wiki;
rep.html.job_check_lr_vr_error_run_validation = tbl.html;
rep.mail.job_check_lr_vr_error_run_validation = tbl.mail;


%Jobs pour lesquels le last_run tombe en erreur
val = {missing_last_run};
uer = {'WARNING: Missing last_run'};
tbl = MARGINAL_TABLE( 6, uer, val, 'Nb Job','job');
rep.wiki.job_missing_lr = tbl.wiki;
rep.html.job_missing_lr = tbl.html;
rep.mail.job_missing_lr = tbl.mail;


% Ecriture dans une page HTML
fdir = fullfile(getenv('st_repository'), 'reports', 'SE', q_base);
if isempty( dir( fdir))
    mkdir( fdir);
end
% Cr?ation du path
fname = fullfile( fdir, sprintf('SE_validation_check_%s_%s.html', upper(estimator_name), domain_type, datestr(today,'yyyy_mm_dd'), estimator_name));

fid = fopen( fname, 'w');
fprintf(fid, '<html>\n<body>\n');
fprintf(fid, '<h1><a name="top"/>Contents</h1>\n');
% fprintf(fid, '<ul>\n');
% fprintf(fid, '<li><a href="#nb">Nb of jobs for the estimator</a></li>\n');
% 
% fprintf(fid, '</ul>\n');
% fprintf(fid, '<h1><a name="nb"/>Nb of jobs for the estimator</h1><a href="#top">top</a><p/>\n');
% fprintf(fid, '%s\n', rep.html.nb{:});
fprintf(fid, '<h1><a name="job_v"/>CHECK_QUALITY VALIDATION: Validated jobs</h1><a href="#top">top</a><p/>\n');
fprintf(fid, '%s\n', rep.html.job_v{:});
fprintf(fid, '<h1><a name="job_check_lr_vr_error"/>WARNING: Check failed on last_run and valid_run</h1><a href="#top">top</a><p/>\n');
fprintf(fid, '%s\n', rep.html.job_check_lr_vr_error{:});
fprintf(fid, '<h1><a name="job_check_lr_vr_error_run_validation"/>RUN_QUALITY VALIDATION: </h1><a href="#top">top</a><p/>\n');
fprintf(fid, '%s\n', rep.html.job_check_lr_vr_error_run_validation{:});
fprintf(fid, '<h1><a name="job_check_lr_error"/>ERROR: Check failed on last_run (but succeeded on last_valid)</h1><a href="#top">top</a><p/>\n');
fprintf(fid, '%s\n', rep.html.job_check_lr_error{:});
fprintf(fid, '<h1><a name="job_missing_lr"/>Missing last_run</h1><a href="#top">top</a><p/>\n');
fprintf(fid, '%s\n', rep.html.job_missing_lr{:});
fprintf(fid, '</body>\n</html>');
fclose(fid);

web(['file://' fname]);


if ~isempty(mail)
    txt = [
        sprintf('CHECK_QUALITY VALIDATION: Validated jobs\n') ...
        sprintf('=========================================\n') ...
        sprintf('%s\n', rep.mail.job_v{:}) ...
        sprintf('WARNING: Check failed on last_run and valid_run\n') ...
        sprintf('================================================\n') ...
        sprintf('%s\n', rep.mail.job_check_lr_vr_error{:}) ...
        sprintf('RUN_QUALITY VALIDATION\n') ...
        sprintf('========================\n') ...
        sprintf('%s\n', rep.mail.job_check_lr_vr_error_run_validation{:}) ...
        sprintf('ERROR: Check failed on last_run(but succeeded on last_valid)\n') ...
        sprintf('============================================================\n') ...
        sprintf('%s\n', rep.mail.job_check_lr_error{:}) ...
        sprintf('Missing last_run\n') ...
        sprintf('==================\n') ...
        sprintf('%s\n', rep.mail.job_missing_lr{:})];
    %
    path = fullfile(getenv('st_repository'), 'reports', 'SE', 'check_mail.txt');
    fid = fopen(path, 'w');
    fprintf(fid, '%s', txt);
    fclose(fid);
    sendmail(mail,...
        sprintf('CHECK validation Report SE [%d] <%s %s> from %s',...
        opt.get('session-num'), estimator_name,domain_type, hostname()),...
        txt)
end


end

function tbl = MARGINAL_TABLE(i, uer, value ,nb_txt, line_txt)
all_sec = repmat({[]}, length(uer),1);
for r=1:length(uer)
    all_sec{r} = value{r}; %all_sec{r} = unique( cell2mat(ers(edx==r,4)));
end
tbl_sec = cellfun(@length, all_sec);

% ?criture dans une table
tbl.wiki = { '{| border="1"' , '|-' };
tbl.wiki{end+1} = sprintf('! Reason || %s || %s', nb_txt, line_txt);
tbl.wiki{end+1} = '|-';
tbl.html = { '<table border=1>' };
tbl.html{end+1} = sprintf('<tr><td><b>Reason</b></td><td><b>%s</b></td><td><b>%s</td></tr>', nb_txt, line_txt);
tbl.mail = { '' , '' };
tbl.mail{end+1} = sprintf('%100s%20s%200s', 'Reason', nb_txt, line_txt);
tbl.mail{end+1} = '';
for r=1:length(uer)
    a = sprintf('%d, ', all_sec{r});
    tbl.wiki{end+1} = [ '|', strtrim(uer{r}), '||',sprintf('%d ', tbl_sec(r)),'||', a(1:end-2)];
    tbl.wiki{end+1} = '|-';
    tbl.html{end+1} = [ '<tr><td><b>', strtrim(uer{r}), '</b></td>', sprintf('<td>%d</td>', tbl_sec(r)), '<td>', a(1:end-2), '</td></tr>'];
    tbl.mail{end+1} = sprintf('%40s%20d%%%200s', strtrim(uer{r}), tbl_sec(r), a(1:end-2));
end
tbl.wiki{end+1} = '|}';
tbl.html{end+1} = '</table>';
tbl.mail{end+1} = '';
end