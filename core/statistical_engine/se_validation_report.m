function rep = se_validation_report( estimator_name, info , domain_type ,varargin)
%SE_VALIDATION_REPORT
%
%
%
%exemple
% se_validation_report

global st_version
q_base = st_version.bases.quant;

opt = options( { ...
    'mail', '', ...
    'is-run', 1 ...
    'estimator-id', [] ...
    }, varargin);

mail = opt.get('mail');

jb_better_lr = info.better_lr;
jb_worst_lr= info.worst_lr;
jb_better_min_lv = info.better_than_min_last_valid;
jb_worst_min_lv = info.worst_than_min_last_valid;
jb_orphelin = [];
jb_inactive = [];
run_validated = info.validated_runs;

%Numero de la Session
session_num = cell2mat(exec_sql('QUANT', sprintf([
    ' SELECT ' ...
    ' max(session_num)  ' ...
    ' FROM ' ...
    ' %s.. job_status ' ],q_base)));

% Tableau recapitulatif des jobs
jobs = exec_sql('QUANT', sprintf([
    'SELECT status , count(js.job_or_run_id)  ' ...
    'FROM ' ...
    '%s..estimator est , ' ...
    '%s..job jb , ' ...
    '%s..job_status  js , ' ...
    '%s..domain dm , ' ...
    '%s..domain_type dt ' ...
    'WHERE ' ...
    'upper(est.estimator_name) = ''%s'' AND ' ...
    'js.session_num = %d AND ' ...
    'js.is_run = 1 AND ' ...
    'jb.estimator_id = est.estimator_id AND ' ...
    'jb.job_id = js.job_or_run_id AND ' ...
    'upper(dt.domain_type_name) = ''%s'' AND ' ...
    'dm.domain_type_id = dt.domain_type_id AND ' ...
    'jb.domain_id = dm.domain_id ' ...
    'group by status ' ],q_base,q_base, q_base,q_base, q_base,upper(estimator_name),session_num,upper(domain_type))  );

if ~isempty(jobs)
    data_jobs = st_data('from-cell', 'title', {'status','nb_of_job'}, jobs);
    idx = cellfun(@(a)strcmp(a, 'ended') ,data_jobs.codebook.book );
    ended_jb  = data_jobs.value(idx,2);
    all_jb    = sum(data_jobs.value(:,2));
    jobs = {estimator_name,ended_jb ,all_jb-ended_jb };
else
    jobs = {estimator_name,0 ,0 };
end

jobs = cat(2, jobs, repmat({0},1,4));
jobs{1,4} = length(jb_better_lr)+ length(jb_better_min_lv);
jobs{1,5} = length(jb_worst_lr)+ length(jb_worst_min_lv);
jobs{1,6} = length(jb_orphelin);


rep.wiki.nb = cat(1, { '{| border="1" '; '|-' ; '! Estimator !! Nb jobs(ended) !! Jobs en erreur(cancelled/killed) !! Jobs valides !! Jobs non valides !! Jobs orphelins ' }, ...
    cellfun(@(c1,c2,c3,c4,c5,c6)sprintf('|-\n | %s || %d || %d ||%d || %d || %d ', c1,c2,c3,c4,c5,c6), jobs(:,1), jobs(:,2), jobs(:,3), jobs(:,4),jobs(:,5),jobs(:,6), 'uni', false));
rep.wiki.nb{end+1} = '|}';

rep.html.nb = cat(1, {'<table border="1">'; '<tr><td><b>Estimator</b></td><td><b>Nb jobs(ended)</b></td><td><b>Nb jobs en erreur(cancelled/killed)</b></td><td><b>Jobs valides</b></td><td><b>Jobs non validés</b></td><td><b>Jobs orphelins</b></td></tr>' }, ...
    cellfun(@(c1,c2,c3,c4,c5,c6)sprintf('<tr><td><b>%s</b></td><td> %d</td><td> %d</td><td> %d</td><td> %d</td><td> %d</td></tr>', c1,c2,c3,c4,c5,c6), jobs(:,1), jobs(:,2), jobs(:,3), jobs(:,4), jobs(:,5), jobs(:,6), 'uni', false));
rep.html.nb{end+1} = '</table>';

rep.mail.nb = cat(1, {''; sprintf('%20s%20s', 'Estimator', 'Nb jobs(ended)', 'Nb job en erreur(cancelled/killed)' , 'Jobs valides ' , ' Jobs non valides' ,' Jobs orphelins' ) }, ...
    cellfun(@(c1,c2,c3,c4,c5,c6)sprintf('%20s%20d%20d%20d%20d%20d', c1,c2,c3,c4,c5,c6), jobs(:,1), jobs(:,2), jobs(:,3), jobs(:,4), jobs(:,5), jobs(:,6), 'uni', false));
rep.mail.nb{end+1} = '';

% Table des job validés
val = {jb_better_lr,jb_better_min_lv}
uer = {'REFRESH VALIDATION: For those jobs, "Last_run" quality is better than its "last_valid" quality','FIRST VALIDATION: For those jobs, "Last_run" quality is better than min of "last_valid" values'};
tbl = MARGINAL_TABLE( 6, uer, val, 'Nb Job','job_valides');
rep.wiki.job_v = tbl.wiki;
rep.html.job_v = tbl.html;
rep.mail.job_v = tbl.mail;

% Table des job non validés
val = {jb_worst_lr,jb_worst_min_lv}
uer = {'REFRESH VALIDATION: For those jobs, "Last_run" quality is not as good as its "last_valid" quality','FIRST VALIDATION: For those jobs, "Last_run" quality is worse than min of "last_valid" values'};
tbl = MARGINAL_TABLE( 6, uer, val, 'Nb Job','job_non_valides');
rep.wiki.job_nv = tbl.wiki;
rep.html.job_nv = tbl.html;
rep.mail.job_nv = tbl.mail;

% Table des jobs orphelins forcement dévalidés
val = {jb_orphelin , jb_inactive};
uer = {'DEVALIDATION: Jobs orphelins systematiquement dévalidés' , 'DEVALIDATION: Jobs devenus inactifs'};
tbl = MARGINAL_TABLE( 6, uer, val, 'Nb Job','job_devalides');
rep.wiki.job_o = tbl.wiki;
rep.html.job_o = tbl.html;
rep.mail.job_o = tbl.mail;


% Table des runs validés
valid_runs = se_get_run_frm_job ('last-run' , jb_better_min_lv) ;
last_runs  =  se_get_run_frm_job('last-run',jb_better_lr);
if ~isempty( valid_runs) && ~isempty( last_runs)
    st_log('%d valid runs\n', length(  valid_runs.date));
    val = { st_data('cols' , last_runs , 'run_id'), st_data('cols', valid_runs, 'run_id')} ;
    uer = {'REFRESH VALIDATION','FIRST VALIDATION'};
    tbl = MARGINAL_TABLE( 6, uer, val, 'Nb run','run_valides');
    rep.wiki.run_v = tbl.wiki;
    rep.html.run_v = tbl.html;
    rep.mail.run_v = tbl.mail;
else
    rep.wiki.run_v = {''};
    rep.html.run_v = {''};
    rep.mail.run_v = {''};
    warning('se_validation_report:runs', 'no valid run');
end
% Table des domains validés
valid_domains = (se_get_dom_or_sec_frm_job ('domain_id' , jb_better_min_lv) ); 
last_doms     = (se_get_dom_or_sec_frm_job ('domain_id',jb_better_lr));
if ~isempty( valid_domains) && ~isempty( last_doms)
    st_log('%d valid domains\n', length(  valid_domains.date));
    val = {st_data('cols' , last_doms , 'domain_id'), st_data('cols', valid_domains, 'domain_id')} ;
    uer = {'REFRESH VALIDATION','FIRST VALIDATION'};
    tbl = MARGINAL_TABLE( 6, uer, val, 'Nb run','domain_valides');
    rep.wiki.dom_v = tbl.wiki;
    rep.html.dom_v = tbl.html;
    rep.mail.dom_v = tbl.mail;
else
    rep.wiki.dom_v = {''};
    rep.html.dom_v = {''};
    rep.mail.dom_v = {''};
    warning('se_validation_report:domains', 'no valid domain');
end
% Ecriture dans une page HTML
fdir = fullfile(getenv('st_repository'), 'reports', 'SE', q_base);
if isempty( dir( fdir))
    mkdir( fdir);
end
% Cr?ation du path
est_name = strrep(estimator_name , ' ','_');
fname = fullfile( fdir, sprintf('SE_validation_%s_%s_%s.html', upper(est_name), domain_type, datestr(today,'yyyy_mm_dd')));

fid = fopen( fname, 'w');
fprintf(fid, '<html>\n<body>\n');
fprintf(fid, '<h1><a name="top"/>Contents</h1>\n');
fprintf(fid, '<ul>\n');
fprintf(fid, '<li><a href="#nb">Nb of jobs for the estimator</a></li>\n');

fprintf(fid, '</ul>\n');
fprintf(fid, '<h1><a name="nb"/>Nb of jobs for the estimator(in the last session)</h1><a href="#top">top</a><p/>\n');
fprintf(fid, '%s\n', rep.html.nb{:});
fprintf(fid, '<h1><a name="job_v"/>Validated jobs(in the last session)</h1><a href="#top">top</a><p/>\n');
fprintf(fid, '%s\n', rep.html.job_v{:});
fprintf(fid, '<h1><a name="job_nv"/>Unchanged jobs(in the last session)</h1><a href="#top">top</a><p/>\n');
fprintf(fid, '%s\n', rep.html.job_nv{:});
fprintf(fid, '<h1><a name="job_o"/>Unvalidated jobs(in the last session)</h1><a href="#top">top</a><p/>\n');
fprintf(fid, '%s\n', rep.html.job_o{:});
fprintf(fid, '<h1><a name="run_v"/>Validated runs</h1><a href="#top">top</a><p/>\n');
fprintf(fid, '%s\n', rep.html.run_v{:});
fprintf(fid, '<h1><a name="dom_v"/>Validated domains</h1><a href="#top">top</a><p/>\n');
fprintf(fid, '%s\n', rep.html.dom_v{:});
fprintf(fid, '</body>\n</html>');
fclose(fid);

web(['file://' fname]);

% Ecriture pour le mail
mail = cell2mat(exec_sql('QUANT', sprintf('SELECT  owner FROM %s..estimator WHERE upper(estimator_name) = ''%s''', q_base , upper(estimator_name))));
 
if ~isempty(mail)
    txt = [sprintf('Récapitulatif du "Status" des jobs\n') ...
        sprintf('=========================================\n') ...
        sprintf('%s\n', rep.mail.nb{:}) ...
        sprintf('Validated jobs\n') ...
        sprintf('===================\n') ...
        sprintf('%s\n', rep.mail.job_v{:}) ...
        sprintf('Unchanged jobs\n') ...
        sprintf('==============\n') ...
        sprintf('%s\n', rep.mail.job_nv{:}) ...
        sprintf('Unvalidated jobs\n') ...
        sprintf('=================\n') ...
        sprintf('%s\n', rep.mail.job_o{:}) ...
        sprintf('Validated runs\n') ...
        sprintf('==============\n') ...
        sprintf('%s\n', rep.mail.run_v{:}) ...
        sprintf('Validated domains\n') ...
        sprintf('==================\n') ...
        sprintf('%s\n', rep.mail.dom_v{:})];
    %
    path = fullfile(getenv('st_repository'), 'reports', 'SE', 'mail.txt');
    fid = fopen(path, 'w');
    fprintf(fid, '%s', txt);
    fclose(fid)
        
    if (ispc())
        idx = findstr('@', mail);
        lgn = mail(1:idx-1);
        send_with_blat('install','sender', sprintf('%s',lgn))
        send_with_blat('file',sprintf('%s',lgn), sprintf('"Rapport %s SE - %s/%s from %s"','validation', estimator_name,domain_type, hostname()),sprintf('%s',path))
    else
        system(sprintf('more %s | mail -s "Rapport %s SE - %d %s/%s from %s" %s', path, 'validation', session_num, estimator_name, domain_type , hostname(), mail));
    end
    
end
end


function tbl = MARGINAL_TABLE(i, uer, value ,nb_txt, line_txt)
all_sec = repmat({[]}, length(uer),1);
for r=1:length(uer)
    all_sec{r} = value{r}; %all_sec{r} = unique( cell2mat(ers(edx==r,4)));
end
tbl_sec = cellfun(@length, all_sec);

% ?criture dans une table
tbl.wiki = { '{| border="1"' , '|-' };
tbl.wiki{end+1} = sprintf('! Reason || %s || %s', nb_txt, line_txt);
tbl.wiki{end+1} = '|-';
tbl.html = { '<table border=1>' };
tbl.html{end+1} = sprintf('<tr><td><b>Reason</b></td><td><b>%s</b></td><td><b>%s</td></tr>', nb_txt, line_txt);
tbl.mail = { '' , '' };
tbl.mail{end+1} = sprintf('%100s%20s%200s', 'Reason', nb_txt, line_txt);
tbl.mail{end+1} = '';
for r=1:length(uer)
    a = sprintf('%d, ', all_sec{r});
    tbl.wiki{end+1} = [ '|', strtrim(uer{r}), '||',sprintf('%d ', tbl_sec(r)),'||', a(1:end-2)];
    tbl.wiki{end+1} = '|-';
    tbl.html{end+1} = [ '<tr><td><b>', strtrim(uer{r}), '</b></td>', sprintf('<td>%d</td>', tbl_sec(r)), '<td>', a(1:end-2), '</td></tr>'];
    tbl.mail{end+1} = sprintf('%40s%20d%%%200s', strtrim(uer{r}), tbl_sec(r), a(1:end-2));
end
tbl.wiki{end+1} = '|}';
tbl.html{end+1} = '</table>';
tbl.mail{end+1} = '';
end