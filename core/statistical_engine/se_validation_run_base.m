function varargout = se_validation_run_base( estimator , run_to_valid, varargin )
%SE_VALIDATION_RUN_BASE - this function validates RUNS in QUANT
% inputs : 1: estimator's name
%          2: struct that contains all RUNS that had to be validated for
%          an estimator
%
%
%exemple:
% run_to_valid = se_validation_run('Volatility curve','first-validation',0);
% se_validation_run_base( 'Volatility curve', run_to_valid)

global st_version
q_base = st_version.bases.quant;

opt = options( { ...
    'first-validation', 0 , ...
    'refresh',0 , ...
    'session', NaN , ...
    },varargin);

[runs_val ,jobs_val ] = deal(run_to_valid.run_valid , run_to_valid.job_id );

if isfield(run_to_valid,'info')
    domain_t = run_to_valid.domain_type;
    info = run_to_valid.info;
else
    domain_t = [];
    run_to_valid.info = [];
end

if ispc()
    blank_char = sprintf('\n');
else
    blank_char = sprintf('\\n');
end


try
    %Si l'option refresh est utilisée : Dévalidation de tous les jobs de
    %l'estimateur en question (pour le domain_type specifié)
    if (opt.get('refresh')==1)
        st_log('se_validation_run: validation off for %d runs\n', size(quals,1));
        %UPDATE de la table estimator_runs
        exec_sql('QUANT', sprintf( [ ...
            'UPDATE %s..estimator_runs  ' ...
            'SET is_valid = 0  ' ...
            'WHERE job_id in(select distinct est_r.job_id ' ...
            'from ' ...
            '%s..estimator est, ' ...
            '%s..job jb, ' ...
            '%s..estimator_runs est_r, ' ...
            '%s..domain_type dt,  ' ...
            '%s..domain dm    ' ...
            'where ' ...
            'upper(est.estimator_name) = (''%s'') AND ' ...
            'jb.estimator_id = est.estimator_id  AND ' ...
            '%s ' ...
            'dm.domain_type_id = dt.domain_type_id AND  ' ...
            'dm.domain_id = jb.domain_id AND  ' ...
            'est_r.job_id = jb.job_id AND ' ...
            'est_r.is_valid = 1) '],q_base , q_base, q_base, q_base , q_base,q_base, upper(estimator),domain_t))
        
        %UPDATE de la table last_run
        exec_sql('QUANT', sprintf( [ ...
            'UPDATE %s..last_run  '...
            'SET last_valid_run_id = null ' ...
            'WHERE job_id in(select distinct lr.job_id ' ...
            'from ' ...
            '%s..last_run lr, ' ...
            '%s..job jb, ' ...
            '%s..estimator est, ' ...
            '%s..domain_type dt,  ' ...
            '%s..domain dm  ' ...
            'where ' ...
            'upper(est.estimator_name) = (''%s'') AND ' ...
            'jb.estimator_id = est.estimator_id  AND ' ...
            '%s ' ...
            'dm.domain_type_id = dt.domain_type_id AND  ' ...
            'dm.domain_id = jb.domain_id AND  ' ...
            'lr.job_id = jb.job_id AND ' ...
            'last_valid_run_id is not null) '],q_base, q_base, q_base,q_base ,q_base,q_base,upper(estimator),domain_t))
        
    end
    
    
    %%VALIDATION des runs dans la base QUANT et dévalidation si besoin est
    %%LOOP sur les JOB_Id
    
    st_log('$off$') %Désactivation du st_log pour éviter les erreurs de type JZ0R2
    for i = 1 : length(jobs_val)
        try
            if isnan(opt.get('session'))
                exec_sql('QUANT', sprintf( ' exec %s..upd_validation %d , 1 ', q_base , jobs_val(i) ))
            else
                exec_sql('QUANT', sprintf( ' exec %s..upd_validation %d , 1 , %d ', q_base , jobs_val(i) , opt.get('session') ) )
            end
        catch er
            if isempty(strfind( er.message, 'JZ0R2'))
                rethrow( er);
            end
        end            
    end
    st_log('$on$') %Résactivation du st_log
    
    if ~isempty(runs_val)
        st_log('< %s >: end of devalidation of %d runs\n', mfilename , length(runs_val));
        info.validated_runs = runs_val;
    else
        st_log(sprintf('<%s> 0 runs to validate in the base.\n', mfilename));
        info.validated_runs = [];
        
    end
    
    %REPORTING
    st_log('se_validation_run_base: reporting...\n');
    se_validation_report(estimator, info ,domain_t);
    
catch e
    st_log(sprintf('%s\n', e.message));
    error_message = se_stack_error_message(e);
    if ~ispc()
        error_message = regexprep(error_message, '\n', '\r');
    end
    
    %ENVOI DE MAIL
    identification_err = sprintf('%s%s%s', 'ECHEC DANS LE PROCESSUS DE VALIDATION:',blank_char, error_message);
    subject = sprintf('%s : Echec de mise a jour des tables lors du processus de validation pour l''estimateur %s' , hostname() , upper(estimator));
    %mail
    address = exec_sql('QUANT', sprintf('SELECT owner from %s..estimator WHERE upper(estimator_name) = (''%s'')', q_base , upper(estimator)));
    for j=1:length(address)
        se_sendmail( address{j},subject,identification_err)
    end
    
end

varargout = {};
end