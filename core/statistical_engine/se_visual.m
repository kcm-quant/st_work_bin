function varargout = se_visual( mode, varargin)
% SE_VISUAL - interractions visuelles avec le SE
%
%  runs = se_db('list_run', 101, 'input-field', 'run_id')
%  r_info = se_visual('run-info', runs(1).run_id)
%  se_visual('monitor-run', r_info)

switch lower( mode)
    case 'monitor-run'
        info       = varargin{1};
        fname      = sprintf('%s - %d', info.estimator.estimator_name, info.run.run_id);
        func_name  = info.estimator.matlab_show;
        get_focus( fname);
        check_vals = [info.checks.value];
        check_dts  = { info.checks.stamp_date };
        check_dtn  = datenum( check_dts, 'dd/mm/yyyy');
        run_dts    = info.run.creation_date;
        run_dtn    = datenum( run_dts, 'dd/mm/yyyy');
        for c=1:length(check_vals)
            plot( check_dtn(c), check_vals(c), 'or', 'markerfacecolor', [.67 0 .12], ...
                'ButtonDownFcn', sprintf('se_visual(''visual-check'', %d,''%s'')', info.run.run_id, func_name));
            hold on
        end
         ax = axis;
         plot( [run_dtn, run_dtn], [ax(3)*.8 ax(4)*1.2], ':k');
         ax_ = axis;
     
         plot( ax_(1:2), repmat(info.run.run_quality,1,2), '-k', 'linewidth',2);
         ax_ = axis;
         plot( run_dtn, info.run.run_quality, 'ok', 'markerfacecolor', 'k', ...
                 'ButtonDownFcn', 'se_visual(''visual-run'')');
         axis([ax_(1:2), ax_(3)*.9 ax_(4)*1.1]);
         
         hold off
         attach_date('init', 'date', ax(1:2), 'format', 'dd/mm/yyyy');
         title( fname);
         userdata(gca, 'set', 'info', info);
         
    case  'visual-run' 
        info = userdata(gca, 'get', 'info');
        
        feval( info.estimator.matlab_show, ...
            'run', 'security_id', [info.domain.security_id], ...
            'trading-destinations',[info.domain.trading_destination_id ], ...
            'window_width', info.job.run_window_width ,'window_type', info.job.run_window_type_id , ...
            'as_of_date', info.run.creation_date );
        
    case  'visual-check' 
         varargin{:}
    case 'run-info'
        run_id = varargin{1};
        % characteristiques du run
        run_char = se_db('list_run', run_id, 'input-field', 'run_id');
        job_char = se_db('list_job', run_char.job_id, 'input-field', 'job_id');
        est_char = se_db('list_estimator', job_char.estimator_id, 'input-field', 'estimator_id');
        dom_char = se_db('list_domain_security', job_char.domain_id , 'input-field', 'domain_id');
        try
            % list des checks
            checks = se_db('list_check', run_id, 'input-field', 'run_id');
        catch er
            st_log('STOP because of: %s\n', er.message);
            return
        end
        varargout = { struct('run', run_char, 'checks', checks, 'job', job_char, 'estimator', est_char, 'domain', dom_char )};
        
    otherwise
        error('se_visual:mode', 'mode <%s> unknown', mode);
        
end