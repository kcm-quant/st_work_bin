function [from, to] = se_window_spec2from_to(window_type, window_width, as_of_date)
if ischar(as_of_date)
    to = datenum(as_of_date, 'dd/mm/yyyy');
else
    to = as_of_date;
end
window_type = lower(window_type);
switch window_type
    case 'day'
        from = to - window_width + 1;
    case 'week'
        from = to - 7 * window_width + 1;
    case {'month', 'year'}
        from = addtodate(to, -window_width, window_type) + 1;
    otherwise
        error('window_spec2from_to:exec', 'unknown window_type : <%s>', window_type);
end