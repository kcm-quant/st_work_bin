% STATISTICAL_ENGINE
%
% se_from_xml      - chargement des descriptions dans les tables
% se_db_estimator  - parcours les tables en MATLAB
% evaluation_run   - lancement du run d'un job
% evaluation_chek  - lancement du check d'un job
% launch_se        - qui boucle sur les evaluation_*
%