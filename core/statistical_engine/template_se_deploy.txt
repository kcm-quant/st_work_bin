<?xml version="1.0" encoding="ISO-8859-1"?>
<estimator>
    <definition name="[GRAPH NAME]">
        <description> </description>
        <matlab run="[SE_RUN_FUNCTION]"/>
        <matlab check="[SE_CHECK_FUNCTION]"/>
        <output type="table"/>
        <params>
            <param name="[PARAM1_NAME]" x_value="1">[PARAM1_COMMENT]</param>
            <param name="[PARAM2_NAME]" x_value="2">[PARAM2_COMMENT]</param>
            [... ATTENTION A INCREMENTER x_value A CHAQUE FOIS]
        </params>
        <contexts>
            <context name="[CONTEXT1_NAME]" num="[CONTEXT1_NAME]">[CONTEXT1_COMMENT]</context>
			<context name="[CONTEXT2_NAME]" num="[CONTEXT2_NAME]">[CONTEXT2_COMMENT]</context>
			[... ATTENTION A INCREMENTER num A CHAQUE FOIS]
        </contexts>
    </definition>
    <jobs>
        <job group="[LETTRE DECRIVANT LE GROUPE]">
            <comment> </comment>
            <run frequency="[daily / weekly / monthly / asOfDate]" window="[day / week / month / year]" width="[NOMBRE DE JOURS]" date="[DATE DE DEBUT DE RUN dd/mm/yyyy]"/>
            <check frequency="[daily / weekly / monthly / asOfDate]" window="[day / week / month / year]" width="[NOMBRE DE JOURS]" date="[DATE DE DEBUT DE CHECK dd/mm/yyyy]"/>
        </job>
    </jobs>
    <domains>
        <domain name="[PERMIETER NAME]" group="[LETTRE DECRIVANT LE GROUPE, voir le job]">
            <txt>[DOMAIN COMMENT]</txt>
            <security id="[SECURITY ID]" td="[TRADING_DESTINATION]" />
        </domain>
		<domain name="[PERMIETER NAME]" group="[LETTRE DECRIVANT LE GROUPE, voir le job]">
            <txt>[DOMAIN COMMENT]</txt>
            <security id="[SECURITY ID]" td="[TRADING_DESTINATION]" />
        </domain>
		[...]
				
		<domain name="[PERMIETER NAME]" group="[LETTRE DECRIVANT LE GROUPE, voir le job]" type='index'>
            <index code="[INDEX CODE 1]"/>
				<tradingdestination id="[TRADING DESTINATION 1]"/>
				<tradingdestination id="[TRADING DESTINATION 2]"/>
			</index>
			<index code="[INDEX CODE 2]"/>
				<tradingdestination id="[TRADING DESTINATION 1]"/>
				<tradingdestination id="[TRADING DESTINATION 2]"/>
			</index>
        </domain>
		[...]
		
        <indice code="[INDEX CODE]" group="[LETTRE DECRIVANT LE GROUPE, voir le job]">
            <txt></txt>
            <tradingdestination id="[TRADING DESTINATION 1]"/>
			<tradingdestination id="[TRADING DESTINATION 2]"/>
        </indice>
        <indice code="[INDEX CODE]" group="[LETTRE DECRIVANT LE GROUPE, voir le job]">
            <txt></txt>
            <tradingdestination id="[TRADING DESTINATION 1]"/>
			<tradingdestination id="[TRADING DESTINATION 2]"/>
        </indice>
		[...]

    <associations>
        <association security_id="[SECURITY ID]" td_id="[TRADING DESTINATION ID]" group="[LETTRE DECRIVANT LE GROUPE, voir le job]" defaultcontext="[NOM DU CONTEXT PAR DEFAUT, voir les contextes]" rank="[RANG]" varargin="[ARGUMENT varargin_ DE se_run]"/>
		<association security_id="[SECURITY ID]" td_id="[TRADING DESTINATION ID]" group="[LETTRE DECRIVANT LE GROUPE, voir le job]" defaultcontext="[NOM DU CONTEXT PAR DEFAUT, voir les contextes]" rank="[RANG]" varargin="[ARGUMENT varargin_ DE se_run]"/>
		[...]
    </associations>
</estimator>
