function est_params = se_run_<<ESTIMATOR_NAME>>(security_id, trading_destination_id, window_type, window_width, as_of_day, dt_remove, varargin_)
% SE_RUN_<<ESTIMATOR_NAME>> - function that interface <<ESTIMATOR_NAME>>
%
% use:
%  est_params = se_run_<<ESTIMATOR_NAME>>(security_id, trading_destination_id, window_type, window_width, as_of_day)
% example:
%  est_params = se_run_<<ESTIMATOR_NAME>>('FTE.PA',{}, 'day', 60, '07/04/2008', [])
%
% See also se_check_<<ESTIMATOR_NAME>> 

data = read_dataset( sprintf('gi:<<GRAPH_NAME>>/window:char%s"%s|%d', char(167), window_type, window_width) , ...
    'security_id',security_id, 'from', as_of_day, 'to', as_of_day,  ...
    'remove',dt_remove, ...
    'trading-destinations', trading_destination_id);

if isempty(data)
    error('No data for this job!')
end

est_params = st_data('keep', data, '<<PARAM1>>;<<PARAM2>>;<<...>>');
est_params.value = est_params.value';
est_params.rownames = { '<<PARAM1>>'; '<<PARAM2>>'; '<<...>>' };
est_params.colnames = { '<<CONTEXT1>>', '<<CONTEXT2>>' , '<<...>>' };
est_params.info.run_quality = st_data('col', data, '<<QUALITY_COL>>');


 