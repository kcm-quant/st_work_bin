function out = time_to_report_error( estimator_name , varargin )
% TIME_TO_REPORT_ERROR 
%
%
%exemple:
% time_to_report_error( 'Volume curve', 'is-run' , 1 )
% time_to_report_error('Aggressivity Level','is-run',1)
%


global st_version
q_base = st_version.bases.quant;

opt = options( { ...
    'is-run', 1 ...
    'estimator-id', [] ...
    }, varargin);

is_run = opt.get('is-run');


 if is_run
    
    ers = exec_sql('QUANT', sprintf([ 'SELECT  ' ...
        '		convert(date,e.stamp_date),    ' ...
        '		convert(time,e.stamp_time), 	  ' ...
        '		es.estimator_name, 	  ' ...
        '		ds.security_id, 	  ' ...
        '		ds.trading_destination_id, 	  ' ...
        '		e.job_or_run_id, 	  ' ...
        '		e.is_run, 	  ' ...
        '		e.message,   ' ...
        '	    1  ' ...
        '	FROM 	  ' ...
        '		%s..error e, 	  ' ...
        '		%s..domain_security ds, 	  ' ...
        '		%s..estimator es,   ' ...
        '		%s..job j,   ' ...
        '		%s..job_status js   ' ...
        '	WHERE 	  ' ...
        '     js.is_run=1 AND 	  ' ...
        '     e.job_or_run_id=j.job_id AND 	  ' ...
        '     e.is_run=js.is_run AND 	  ' ...
        '     e.job_or_run_id=js.job_or_run_id AND 	  ' ...
        '     convert(date,e.stamp_date)=js.end_date AND 	  ' ...
        '     convert(time,e.stamp_time)=js.end_time AND 	  ' ...
        '     j.domain_id=ds.domain_id 	AND  	  ' ...
        '     es.estimator_id=j.estimator_id AND	  ' ...
        '     js.session_num =(SELECT MAX(session_num) FROM %s..session ) AND ' ...
        '     upper(es.estimator_name) =''%s'' ' ...
        '	ORDER BY 	  ' ...
        '		es.estimator_name, 	ds.trading_destination_id, 	ds.security_id    '], ...
        q_base, q_base, q_base, q_base, q_base,q_base, upper(estimator_name)) );
    
else
    
    ers = exec_sql('QUANT', sprintf([ 'SELECT  ' ...
        '		convert(date,e.stamp_date),    ' ...
        '		convert(time,e.stamp_time), 	  ' ...
        '		es.estimator_name, 	  ' ...
        '		ds.security_id, 	  ' ...
        '		ds.trading_destination_id, 	  ' ...
        '		e.job_or_run_id, 	  ' ...
        '		e.is_run, 	  ' ...
        '		e.message,   ' ...
        '	    1  ' ...
        '	FROM 	  ' ...
        '		%s..error e, 	  ' ...
        '		%s..domain_security ds, 	  ' ...
        '		%s..estimator es,   ' ...
        '		%s..estimator_runs er,   ' ...
        '		%s..job_status js   ' ...
        '	WHERE 	  ' ...
        '     js.is_run=0 AND   ' ...
        '     e.job_or_run_id=er.run_id AND   ' ...
        '     e.is_run=js.is_run AND   ' ...
        '     e.job_or_run_id=js.job_or_run_id AND   ' ...
        '     convert(date,e.stamp_date)=js.end_date AND   ' ...
        '     convert(time,e.stamp_time)=js.end_time AND   ' ...
        '     er.domain_id=ds.domain_id 	AND    ' ...
        '     es.estimator_id=er.estimator_id AND   ' ...
        '     js.session_num =(SELECT MAX(session_num) FROM %s..session ) AND ' ...
        '     upper(es.estimator_name) =''%s'' ' ...
        '	ORDER BY 	  ' ...
        '		es.estimator_name, 	ds.trading_destination_id, 	ds.security_id    '], ...
        q_base, q_base, q_base, q_base, q_base,q_base, upper(estimator_name)) );
 end

 out = ~isempty(ers);
    
end