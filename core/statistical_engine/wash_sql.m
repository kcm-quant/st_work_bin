function arg = wash_sql(arg)
% WASH_SQL - wash SQL arguments
%
%     s = wash_sql('CAC40: domaine de l''indice')
%
% See also sql_exec
%
% Author   'Edouard d'Archimbaud'
% Reviewer ''
% Date     '21/10/2008'


% Replace patterns like ' with ''
idx = regexp(arg, '[^''][''][^'']');
while ~isempty(idx)
    arg = [arg(1:idx) '''''' arg(idx+2:end)];
    idx = regexp(arg, '[^''][''][^'']');
end

