function clean_dev(directory2clean, dir4data, del_surround_files, txt_bytes_treshold)
% clean_dev('C:\Dev\st_work\dev', 'C:\Dev\st_work_data\dev', true)
% clean_dev('C:\Dev\st_work\methodo', 'C:\Dev\st_work_data\methodo', true)
% clean_dev('C:\Dev\st_work\external', 'C:\Dev\st_work_data\external', true)
if nargin <= 2
    del_surround_files = false;
end
if nargin <= 3
    txt_bytes_treshold = 50000;
end
warning('off', 'MATLAB:DELETE:FileNotFound')
delete(fullfile(directory2clean, '*.asv'));
if del_surround_files
    delete(fullfile(directory2clean, '.MySCMServerInfo'));
end
warning('on', 'MATLAB:DELETE:FileNotFound')
sdir = dir(directory2clean);
sdir(1:2) = [];
for i = 1 : length(sdir)
    curr_file_or_dir = fullfile(directory2clean, sdir(i).name);
    curr_file_or_dir4data = fullfile(dir4data, sdir(i).name);
    if sdir(i).isdir % recursive call for subdirectories
        clean_dev(curr_file_or_dir, curr_file_or_dir4data, del_surround_files, txt_bytes_treshold);
    else % then this is a file
        file_extension = strfind(sdir(i).name, '.');
        if isempty(file_extension)
            switch lower(sdir(i).name)
                case 'makefile'
                    % Currently nothing to do
                otherwise
                    move_data();
            end
            continue;
        end
        file_extension = sdir(i).name(file_extension(end)+1:end);
        switch file_extension
            case {'m', 'M', 'mexw32', 'tcl', 'rc', 'bat', 'c', 'h', 'cpp', ...
                    'hpp', 'cs', 'hs', 'dsp', 'dsw', 'ncb', 'opt', 'plg', ...
                    'csproj', 'sln', 'vssscc', 'ini', 'vcproj', 'vspscc', ...
                    'exp', 'sql', 'vdproj'}
                % Currently nothing to do
            case 'txt'
                if sdir(i).bytes > txt_bytes_treshold
                    move_data();
                end
            case 'mat'
                if isempty(strfind(curr_file_or_dir, 'compilation'))
                    % is it a grpah or some data?
                    var_in_file = load(curr_file_or_dir);
                    var_names = fieldnames(var_in_file);
                    if length(var_names) == 1 && strcmp(var_names{1}, 'gr')
                        try
                            gr = var_in_file.gr.upgrade();
                            gr.clear_outputs();
                            gr.init();
                        catch
                            move_data();
                            clear var_in_file;
                            clear var_names;
                            continue;
                        end
                        save(curr_file_or_dir, 'gr');
                    else
                        move_data();
                    end
                    clear var_in_file;
                    clear var_names;
                end
            otherwise
                move_data();
        end
    end
end

    function move_data
        st_log('Moving file <%s> to <%s>\n', curr_file_or_dir, curr_file_or_dir4data);
        if ~exist(dir4data, 'dir')
            st_log('Creating directory : <%s>\n', dir4data);
            mkdir(dir4data);
        end
        movefile(curr_file_or_dir, curr_file_or_dir4data, 'f');
    end
end