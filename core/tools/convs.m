function z = convs(mode, v)
% CONVS - conversions de tout le monde en chaine de caractères et
%           inversement
%
%   s = convs('str', @(x)x+1)
%   f = convs('param', s)
%   s = convs('param', '"12-01-2007')
%
%   s = convs('safe_str', {'a', 'a1', 'b', 'b'})
%   f = convs('param', s)
%
%   s = convs('safe_str', struct('a', 1, 'b', 'b'))
%   f = convs('param', s) 
%
%   s = convs('safe_str', {'a', {'a1', 'a2'}, 'b', 'b'})
%   f = convs('param', s) 
%
%   s = convs('safe_str', {'a', {'a1'; 'a2'}, 'b', 'b'})
%   f = convs('param', s) 
%
%   s = convs('safe_str', struct('a', {'a1', 'a2'}, 'b', 'b'))
%   f = convs('param', s) 
%
% author   : 'clehalle@cheuvreux.com'
% reviewer : 'rburgot@cheuvreux.com'
% version  : '1'
% date     : '29/08/2008'
%
% See also opt2str str2opt

switch lower(mode)
    case 'str'
        switch class(v)
            case 'function_handle'
                %< A function handle
                z = func2str( v);
                if z(1) ~= '@'
                    z = ['@' z];
                end
                %>
            case 'char'
                %< A char
                % nothing to be done
                z = ['"' v];
                %>
            case 'double'
                %< Double
                % loop on matrix rows
                if isempty(v)
                    z = '[]';
                elseif numel(v)==1
                    z = num2str(v,16);
                else
                    z = '[';
                    for r=1:size(v,1)
                        z = [ z num2str(v(r,:)) ' ; '];
                    end
                    z(end-2)=']';
                    z(end-1:end) = '';
                    z = strrep(z,'     ', ' ');
                end
                %>
            case 'cell'
                %< cellarray
                if size(v,1) > 0
                    z = '{ ';
                    for r=1:size(v,1)
                        tmp = '';
                        for c=1:size(v,2)
                            tmp = [tmp convs('str', v{r,c}) '~'];
                        end
                        z = [ z tmp '; '];
                    end
                    z(end-1)='}';
                    z(end) = '';
                else
                    z = '{}';
                end
                %>
            case 'logical'
                if v
                    z = 'true';
                else
                    z = 'false';
                end
            otherwise
                z = class(v);
        end
    case 'param'
        if v(1)=='"'
            %< string
            if length(v)>1 && v(2) == char(165)
                z = eval(v(3:end));
            else
                z = v(2:end);
            end
        elseif v(1)=='{'
            %< cellarray
            v = v(3:end-2);            
            idx = regexp(v, '~[^}]');
            idx = [0, idx, length(v)+1];
            t_v = arrayfun(@(b,e)v(b+1:e-1), idx(1:end-1), idx(2:end), 'uni', false);
            idx_beg_cell = cellfun(@(s)length(regexp(s, '\{')), t_v);
            idx_end_cell = cellfun(@(s)length(regexp(s, '~}')), t_v);
            exclude_idx  = [];
            i = 1;
            while i <= length(idx_beg_cell)
                if idx_beg_cell(i) ~= idx_end_cell(i)
                    next_i = (i-1) + find(idx_end_cell(i:end) == idx_beg_cell(i), 1, 'first');
                    temp_str = sprintf('%s~', t_v{i:next_i});
                    t_v{i} = temp_str(1:end-1);
                    exclude_idx = horzcat(exclude_idx, i+1:next_i);
                    i = next_i + 1;
                else
                    i = i + 1;
                end
            end
            t_v = t_v(setdiff(1:numel(t_v), exclude_idx));
            is_column_vector = (find(cellfun(@(s)s(1)==';', t_v)) ==  numel(t_v) - 1);
            z   = cellfun(@(s)convs('param', s), t_v, 'uni', false);
            if is_column_vector
                z = z';
            end
        elseif v(1) == '@'
            %z = str2func(v(2:end));
            eval(['z=' v ';']);
        elseif v(1) == char(165)
            %< struct
            z = list2struct( convs('param', v(2:end)));
        elseif v(1) == ';'
            z = convs('param', v(3:end));
        else
            z = eval( v);
        end
    case 'safe_str'
        switch class(v)
            case 'char'
                %< A char
                % nothing to be done
                z = ['"' v];
                %>
            case 'double'
                %< Double
                % loop on matrix rows
                if isempty(v)
                    z = '[]';
                elseif numel(v)==1
                    z = num2str(v);
                else
                    z = '[';
                    for r=1:size(v,1)
                        z = [ z num2str(v(r,:)) ' ; '];
                    end
                    z(end-2)=']';
                    z(end-1:end) = '';
                    z = strrep(z,'     ', ' ');
                end
                %>
            case 'logical'
                if v
                    z = 'true';
                else
                    z = 'false';
                end
            case 'function_handle'
                %< A function handle
                z = func2str( v);
                if z(1) ~= '@'
                    z = ['@' z];
                end
                %>
            case 'cell'
                %< cellarray
                if size(v,1) > 0
                    z = '{ ';
                    for r=1:size(v,1)
                        tmp = '';
                        for c=1:size(v,2)
                            tmp = [tmp convs('safe_str', v{r,c}) '~'];
                        end
                        z = [ z tmp '; '];
                    end
                    z(end-1)='}';
                    z(end) = '';
                else
                    z = '{}';
                end
                %>
            case 'struct'
                [m n] = size(v);
                if m > 1 && n > 1
                    error('convs:check_args', 'Conversion to string is not yet implemented for struct array with two dimensions');
                end
                z = [char(165) convs('safe_str', struct2list(v))];
            otherwise
                error('convs:safe_str', 'unhandled data type : <%s>', class(v));
        end
    otherwise
        error('convs:mode', 'mode <%s> unknwon', mode);
end