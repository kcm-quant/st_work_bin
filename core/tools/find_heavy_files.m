function list_heavy_files = find_heavy_files(directory, treshold_bytes)
% find_heavy_files('C:\Dev\st_work', 100000)
sdir = dir(directory);
list_heavy_files = {};
sdir(1:2) = [];
for i = 1 : length(sdir)
    if sdir(i).isdir
        list_heavy_files = cat(1, list_heavy_files, find_heavy_files(fullfile(directory, sdir(i).name), treshold_bytes));
    elseif sdir(i).bytes > treshold_bytes
        list_heavy_files = cat(1, list_heavy_files, fullfile(directory, sdir(i).name));
    end
end