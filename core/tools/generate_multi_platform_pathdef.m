function str = generate_multi_platform_pathdef()
% GENERATE_MULTI_PLATFORM_PATHDEF - permet a partir du pathdef.m ORIGINAL 
% de matlab de g�n�rer un fichier pathdef multiplateforme. c'est � dire
% dans lequel il n'y a pas �crit en dur de '\' ou '/', mais un pathdef qui 
% sait trouver tout seul s'il est sur unix ou windows et donc utiliser le
% caract�re ad�quate.
%
% Dans la mesure o� tr�s peu de fonctions sont disponible au moment o� le
% pathdef est utilis�, nous avons choisit d'utiliser la fonction matlabroot
% pour identifier s'il est sur unix ou windows. Ce choix n'est peut �tre
% pas id�al, mais semble fonctionner pour l'instant.
%
%

fid = fopen('pathdef.m');
fsep = filesep;
str = {};

% reading pathdef and making the wtring to be written
while 1
    tline = fgetl(fid);
    idxes = strfind(tline, '''');
    if ~isempty(idxes)
        str2change = tline((idxes(1)+1):(idxes(2)-1));
        idxes_fsep = strfind(str2change, fsep);
        tmp = '[';
        for i = 1 : length(idxes_fsep) - 1
            tmp = [tmp ' fsep ''' str2change((idxes_fsep(i)+1):(idxes_fsep(i+1)-1)) ''''];
        end
        tmp = [tmp ' fsep ''' str2change((idxes_fsep(end)+1) : end) ''']'];
        new_tline = [tline(1:(idxes(1)-1)) tmp tline((idxes(2)+1):end)];
    elseif strcmp(tline, 'p = [...') % TODO something less dirty
        str{end+1} = 'mr = matlabroot();';
        str{end+1} = 'if mr(1) == ''/''';
        str{end+1} = sprintf('\tfsep = ''/'';');
        str{end+1} = 'else';
        str{end+1} = sprintf('\tfsep = ''\\'';');
        str{end+1} = 'end';
        new_tline = tline;
    else
        new_tline = tline;
    end
    str{end+1} = new_tline;
    if ~ischar(tline)
        break;
    end
end
fclose(fid)

% Writing str in pathdef
file2edit = [matlabroot fsep 'toolbox' fsep 'local' fsep 'pathdef.m'];
[fid, message]=fopen(file2edit, 'w+');
if fid == -1
    error('generate_multi_platform_pathdef:exec', 'The file <%s> is probably read-only, please change this before asking me to work', file2edit);
end
fprintf(fid, '%s\n', str{:});
fclose(fid);

edit('pathdef.m');