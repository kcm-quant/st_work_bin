function dico = get_dico_user_mail(str)
% GET_DICO_USER_MAIL - dictionary between mail and username
%
% Please put your username and begining of mail inside 
%
%
% Examples:
%
% get_dico_user_mail()
% get_dico_user_mail('robur')
% get_dico_user_mail('rburgot@cheuvreux.com')
%
% get_dico_user_mail('antha')
% get_dico_user_mail('athabault@cheuvreux.com')
%
% get_dico_user_mail('dcroize@cheuvreux.com')
% 
% See also:
%    
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '08/03/2011'

dico = struct('user', {'robur',   'dancr',   'chleh',    'solar',     'stpel',  'sivla',      'marow',   'vilec',   'vilec',     'midan', 'jofer',      'nijos',   'malas',     'malas',         'lopel',     'benca',  'namay', 'stgal',   'lawag',     'gedes',     'erich',        'antha',     'juraz',          'lopel',       'edarc',   'thdel',    'heben' , 'edarc',        'mamag',    'test',     'parei',   'rober'   , 'sizha',   'ut0thq',   'hahar',  'wehua',  'elber',    'simap_perf', 'malab'}, ...
    'mail_begining', {'rburgot',  'dcroize', 'clehalle', 'slaruelle', 'spelin', 'svlasceanu', 'mrowley', 'vleclerc', 'vleclercq', 'mdang', 'jfernandez', 'njoseph', 'mlasnier', 'mlasnier-ext', 'lpelissier', 'bcarre', 'nmayo', 'sgalzin', 'lwagalath', 'gdeschaux', 'echauffeteau', 'athabault', 'jrazafinimanana','lpelissier',  'dcroize', 'clehalle', 'spelin', 'edarchimbaud', 'clehalle', 'clehalle', 'njoseph', 'clehalle', 'rburgot', 'clehalle', 'hharti', 'whuang', 'clehalle', 'njoseph',    'clehalle'});

if nargin < 1
    return
end

idx_at = str == '@';
if any(idx_at)
    str(find(idx_at, 1, 'first'):end) = [];
end

ind = strmatch(str, {dico.user}, 'exact');
if ~isempty(ind)
    dico = dico(ind).mail_begining;
    return;
end

ind = strmatch(str, {dico.mail_begining}, 'exact');
if ~isempty(ind)
    dico = dico(ind).user;
    return;
end

error('get_dico_user_mail:exec', 'Unknown user or mail : <%s>', str);
end