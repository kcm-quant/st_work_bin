function index = index_from_grid(dt,grid,varargin)
% Takes a grid and a vector of date and gives the interval number for each
% date.
% For coherence purposes with trading_intraday_volume default is close left
% and open right intervals -> [beg,end[
% Dates outside the grid get a NaN.
% Example:
% temp = rand(20,1)*12;[index_from_grid(temp,1:10),temp]
% temp = round(rand(20,1)*12);[index_from_grid(temp,1:10,'bornes',']]'),temp]

if size(dt,2) > 1
    dt = dt(:);
end
if size(grid,2) > 1
    grid = grid(:);
end
reverted_order = false;

if ~isempty(varargin)
    opt = options({'bornes','[['},varargin);
    bornes = opt.get('bornes');
    if strcmp([bornes(1),bornes(end)],']]')
        reverted_order = true;
    elseif strcmp([bornes(1),bornes(end)],'[]') || strcmp([bornes(1),bornes(end)],'][')
        error('index_from_grid:bornes','Not a partition');
    end
end

if reverted_order
    [~,I] = sort([grid;dt],'descend');
else
    [~,I] = sort([grid;dt]);
end
index = [ones(length(grid),1);zeros(length(dt),1)];
index(I) = cumsum(index(I));
index(index==0) = NaN;
index(index==length(grid)) = NaN;
index = index(end-length(dt)+1:end);

if reverted_order
    index = length(grid) - index;
end
