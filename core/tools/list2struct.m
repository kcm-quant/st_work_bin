function s = list2struct( lst)
% LIST2STRUCT - 
%
% lst = {'a', 1, 'b', 2}
% list2struct( lst)
%
% See also options stuct2list

s = struct(lst{:});