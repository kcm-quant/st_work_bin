function [z, varargout] = makedoc( mode, varargin)
% MAKEDOC - automatic documentation generation
%
% makedoc code % -> m2html do not put this in Examples
%  makedoc ocamaweb intervalle_confiance_target_close_3 % ocamaweb needs to be installed
%
% TODO: 
% - si je trouve .plot_fun = dans le code alors j'embarque mon propre
%     code
% - faire la doc sur le wiki
%
% Examples:
% 
%  % we reduce the scope of makedoc in order to avoid too long
%  % documentation
%  global st_version
%  stval = st_version.all_libs;
%  st_version.all_libs = stval(1);
%  
%  makedoc find-boxes % -> { '[type] names' ... }
%  makedoc latest past 7
%  makedoc userdoc st_tickdb
%  makedoc name-userdoc st_tickdb
%  makedoc find-shadows
%  makedoc( 'find-tags', which( 'st_depfun'))
%  makedoc('check_help', 'st_data') % do not use makedoc infinite recursion
% 
%
%  st_version.all_libs = stval;
% 
% See also m2html st_depfun check_help_quality
%
% author  = 'clehalle@cheuvreux.com'
% reviewer  = 'rburgot@cheuvreux.com'
% date '08/03/2011'
% version = '2.1'

global st_version


switch lower(mode)
    case 'builddocsearch'
        builddocsearchdb(fullfile(st_version.my_env.st_work, 'doc','mmqr_toolbox'));
    case 'check_help' % 
        [z, varargout{1}] = check_help_quality(varargin{1});
    case 'ocamaweb'
        m_name = varargin{1};
        [d,f,x] = fileparts( m_name);
        fname  = which(m_name);
        [s, em] = system( sprintf( 'ocamaweb "%s"', fname));
        if ispc()
            pdf_file = fullfile( st_version.my_env.ocamaweb_dest, [ f '.pdf']);
            winopen( pdf_file);
        end
    
    case 'find-tags'
        %<* Lire les tags
        fid=fopen( varargin{1});
        opt = options({ 'author', { '=', '''', '''', 'NONE' }, ...
            'reviewer', { '=', '''', '''', 'NONE'}, ...
            'version', { '=', '''', '''', 'NONE'} }, ...
            varargin(2:end) );
        lst = opt.get();
        keys  = lst(1:2:end);
        codes = lst(2:2:end);
        dec_code = cellfun(@(c)c{1}, codes, 'uni', false);
        bef_code = cellfun(@(c)c{2}, codes, 'uni', false);
        aft_code = cellfun(@(c)c{3}, codes, 'uni', false);
        def_val  = cellfun(@(c)c{4}, codes, 'uni', false);
        my_reg   = cellfun(@(k,d,b,a)[k '[^' d ']*' d '[^' b ']*' b '([^' a ']+)' a ], keys, dec_code, bef_code, aft_code, 'uni', false);
        found_k  = [];
        for k=1:length(keys)
            found_k.(keys{k}) = def_val{k};
        end
        while 1
            tline = fgetl(fid);
            if ~ischar(tline), break, end
            for mr=1:length( my_reg)
                tok = regexp(tline, my_reg{mr}, 'tokens');
                if ~isempty( tok)
                    found_k.(keys{mr}) = tok{1}{1};
                    break
                end
            end
        end
        fclose(fid);
        z = found_k;
        %>*
    case 'find-shadows'
        %<* Retrouver les doublons
        all_mfiles = fuf( fullfile(getenv('st_work'), '*.m'), 'detail');
        fnames = cellfun(@(c)c{1},regexp( all_mfiles, '\\([^\\]+)$', 'tokens'));
        f_shad = @(r,c)struct('reference', r, 'shadows', c);
        shadows = f_shad({},{});
        nb_shadows = 0;
        checked = repmat(false,length(fnames),1);
        for f=1:length(fnames)
%             if ~findstr( fnames{f}, '\external\')
%                def_version = '1.0';
%             else
%                 author = 'none';
%             end
            
            if ~strcmpi( fnames{f}, 'contents.m') && ~checked(f)
                idx = strmatch( fnames{f}, fnames, 'exact');
                if length( idx)>1
                    nb_shadows = nb_shadows+1;
                    shadows(end+1) = f_shad( fnames{f}, { all_mfiles(idx)});
                    st_log('file <%s> has %d shadows:\n', all_mfiles{f}, length(idx)-1);
                    st_log('     - %s\n', all_mfiles{setdiff(idx, f)});
                    checked(idx) = true;
                end
            end
        end
        st_log('\n%d shadowed functions found!\n', nb_shadows);
        makedoc('shadows2html', shadows);
        z = shadows;
        %>*
    case 'code'
        %<* Makedoc using h2html
        all_libs = st_version.all_libs;
        help_location = fullfile(st_version.my_env.st_work, 'doc','mmqr_toolbox');
        m2hdir = fullfile('doc','mmqr_toolbox', 'functions');
%         all_libs = cellfilter(@(s)isempty(strfind(s,'m2html')), all_libs); % sans le r?pertoire ad hoc
% TODO better
    all_libs = all_libs(1:min(56, length(all_libs)));
    %%%%%%
        all_libs = cellfun(@(s)s(2:end),strrep(all_libs, getenv('st_work'), ''), 'uniformoutput', false);
        
        cwd = pwd;
        cd( getenv('st_work'));
        m2html('mfiles', all_libs, 'htmldir', m2hdir, 'save', 'on', 'graph','on', 'todo', 'on');
        m2hmat = load(fullfile(m2hdir, 'm2html.mat'));
        cd(cwd);
        [m2hmat.mdir, ~, ind_m2dir] = unique(m2hmat.mdirs);
        
        %< Creating the view by directories
        init_offset = 1;
        s = '\n\t<tocitem target="functions\\index.html">View by directory tree\n';
        curr_dir = {};
        for i = 1 : length(m2hmat.mdir)
            prev_dir = cat(2, curr_dir{:});
            while ~isempty(prev_dir) && ~isempty(curr_dir) && ...
                    ~(length(prev_dir) < length(m2hmat.mdir{i}) && ...
                    strcmp(m2hmat.mdir{i}(1:length(prev_dir)), prev_dir)) % then we have to move up in dir tree
                tab_str = repmat('\t', 1, init_offset+length(curr_dir));
                s = cat(2, s, sprintf('%s</tocitem>\\n', tab_str));
                curr_dir(end) = [];
            end
            if isempty(curr_dir)
                curr_dir = m2hmat.mdir(i); % reinit
            else
                curr_dir{end+1} = m2hmat.mdir{i}(length(prev_dir)+1:end); % moving down
            end
            tab_str = repmat('\t', 1, init_offset+length(curr_dir));
            s = cat(2,s, sprintf('%s<tocitem target="functions\\\\%s\\\\index.html">\\n\\t%s%s\\n', ...
                    tab_str, text_to_printable_text(cat(2, curr_dir{:})), ...
                    tab_str, text_to_printable_text(cat(2, curr_dir{:}))));
            tab_str = repmat('\t', 1, init_offset+length(curr_dir)+1);
            for j = find(i == ind_m2dir)
                s = cat(2, s, sprintf('%s<tocitem target="functions\\\\%s.html">%s</tocitem>\\n', ...
                    tab_str, text_to_printable_text(m2hmat.mfiles{j}(1:end-2)), ...
                    text_to_printable_text(m2hmat.mfiles{j}(1:end-2))));
            end
        end 
        while ~isempty(curr_dir)  % then we have to move up in dir tree
            tab_str = repmat('\t', 1, init_offset+length(curr_dir));
            s = cat(2, s, sprintf('%s</tocitem>\\n', tab_str));
            curr_dir(end) = [];
        end
        s = cat(2, s, '\n\t</tocitem>\n');
        
        dir_view_flag = '<!-- DIRECTORY_VIEW: DO NOT EDIT, DO NOT MOVE, DO NOT DELETE -->';
        wholefile_s = fileread(fullfile(help_location, 'helptoc.xml'));
        ind_toks = strfind(wholefile_s, dir_view_flag);
        fid = fopen(fullfile(help_location, 'helptoc.xml'), 'w');
        fprintf(fid, text_to_printable_text(wholefile_s(1:ind_toks(1)+length(dir_view_flag)-1)));
        fprintf(fid, s);
        fprintf(fid, text_to_printable_text(wholefile_s(ind_toks(2):end)));
        fclose(fid);
        %>
        
        %<* Creating the view by categories
        %<looking for categories as subdirectories of any of add_plus
        % directories
        this_add_plus_categories = cell(1, length(st_version.add_plus));
        for i = 1 : length(st_version.add_plus)
            this_add_plus_categories{i} = cellflat(cellfun(...
                @(c)regexp(c, [st_version.add_plus{i} '\\([^\\]*)'], 'tokens'), ...
                m2hmat.mdirs, 'uni', false));
        end
        categories = unique(cat(2, this_add_plus_categories{:}));
        %>
        %< building the string to write
        dir_categories = fullfile(help_location, 'functions', 'categories');
        if isempty(dir(dir_categories))
            mkdir(dir_categories);
        end
        addpath(dir_categories); %so that the auto-generated mfiles can be executed in order to be published
        s = '\n\t<tocitem target="functions\\categories\\category_index_publisher.html">View by category\n';
        tab_str = '\t\t';
        for i = 1 : length(categories)
            s = cat(2, s, sprintf('%s<tocitem target="functions\\\\categories\\\\%s_publisher.html">%s</tocitem>\\n', ...
                    tab_str, categories{i}, categories{i}));
            s_this_cat = sprintf('%%%%%%%% Category : %s\\n', categories{i});
            for j = 1 : length(st_version.add_plus)
                s_this_cat = cat(2, s_this_cat, ...
                    sprintf('\\n%%%%%%%% Meta-category : %s\\n%%%%\\n',...
                    st_version.add_plus{j}));
                idx_this_cat = find(strcmp(this_add_plus_categories{j},categories{i}));
                for k =1:length(idx_this_cat)
                    one_liner = text_to_printable_text(...
                        regexprep(m2hmat.h1line{idx_this_cat(k)},...
                        m2hmat.name{idx_this_cat(k)},'', 'ignorecase', 'once'));
                    if isempty(one_liner)
                        one_liner = '- This function has no one-liner...';
                    end
                    s_this_cat = cat(2,s_this_cat, ...
                        sprintf('%%%% * <../%s.html %s> %s\\n',...
                        text_to_printable_text(m2hmat.mfiles{idx_this_cat(k)}(1:end-2)),...
                        text_to_printable_text(m2hmat.name{idx_this_cat(k)}),...
                        one_liner ) );
                end
            end
            filename_ = fullfile(dir_categories, [categories{i} '_publisher']);
            fid = fopen([filename_ '.m'], 'w');
            fprintf(fid, s_this_cat);
            fclose(fid);
            publish_demos('single_file', ...
                'file_in', filename_, ...
                'dir_out', fullfile(help_location, 'functions', 'categories'), ...
                'open_html', false);
        end
        filename_ = fullfile(dir_categories, 'category_index_publisher');
        fid = fopen([filename_ '.m'], 'w');
        fprintf(fid, '%%%% Categories\n%%\n');
        for i = 1 : length(categories)
            fprintf(fid, '%% * <./%s_publisher.html %s>\n', categories{i}, categories{i});
        end
        fclose(fid);
        publish_demos('single_file', ...
            'file_in', filename_, ...
            'dir_out', fullfile(help_location, 'functions', 'categories'), ...
            'open_html', false);
        rmpath(dir_categories);
        s = cat(2, s, '\n\t</tocitem>\n');
        %>
        
        %< writing the string infile helptoc
        %
        categories_view_flag = '<!-- CATEGORY_VIEW: DO NOT EDIT, DO NOT MOVE, DO NOT DELETE -->';
        wholefile_s = fileread(fullfile(help_location, 'helptoc.xml'));
        ind_toks = strfind(wholefile_s, categories_view_flag);
        fid = fopen(fullfile(help_location, 'helptoc.xml'), 'w');
        fprintf(fid, text_to_printable_text(wholefile_s(1:ind_toks(1)+...
            length(categories_view_flag)-1)));
        fprintf(fid, s);
        fprintf(fid, text_to_printable_text(wholefile_s(ind_toks(2):end)));
        fclose(fid);
        %>
        %>*
        
        m2hmat.fp_filename = cell(length(m2hmat.mfiles), 1);
        for i = 1 : length(m2hmat.mfiles)
            m2hmat.fp_filename{i} = fullfile(st_version.my_env.st_work, ...
                m2hmat.mfiles{i});
        end

        %< checking the help of functions
        all_help_problems = cell(length(m2hmat.name), 1);
        is_function = false(length(m2hmat.name), 1);
        info = cell(length(m2hmat.name), 1);
        for i = 1:length(m2hmat.name)
            is_function(i) = ~is_script(fileread(m2hmat.fp_filename{i}));
            if is_function(i)
                st_log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n\n\n');
                st_log('Checking help for function : <%s>', m2hmat.name{i});
                st_log('\n\n\n\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
                [all_help_problems{i} , info{i}] = makedoc('check_help',  m2hmat.name{i});
                if isempty(info{i}.author)
                    info{i}.author = get_most_surround_commiter(m2hmat.fp_filename{i});
                end
                info{i}.author = strrep(info{i}.author, '@cheuvreux.com', '');
            else
                info{i} = struct('author', get_most_surround_commiter(m2hmat.fp_filename{i}));
            end
        end
        %>
        
        %< Creating an mfile to be published to report the errors found
        fid = fopen(fullfile(help_location, 'help_error_report.m'), 'w');
        %         info = cat(1, info{:});
        authors = cell(length(info), 1);
        for i = 1 : length(info)
            authors{i} = get_dico_user_mail(get_dico_user_mail(    info{i}.author));
        end
        [u_authors, ~, ind_a] = unique(authors);
%         try
        fprintf(fid, '%%%% TO BE CORRECTED\n');
        for k = 1 : length(u_authors)
            fprintf(fid, '%%%% %s\n', u_authors{k});
            for i = 1 : length(m2hmat.name)
                if is_function(i) && ~isempty(all_help_problems{i}) && (ind_a(i) == k)
                    fprintf(fid, '%% <matlab:edit_and_check_out(''%s.m'') %s>\n%%\n', ...
                        m2hmat.name{i}, m2hmat.name{i});
                    for j = 1 : length(all_help_problems{i})
                        fprintf(fid, '%% * %s : "%s"\n', all_help_problems{i}(j).error_type, ...
                            strrep(regexprep(all_help_problems{i}(j).error_message, sprintf('\n'), sprintf('\n%%')), '[<|>]', '"'));
                    end
                    fprintf(fid, '%%\n');
                end
            end
        end
        fclose(fid);
        

        
        publish_demos('single_file', ...
            'file_in', 'help_error_report', ...
            'dir_out', help_location, 'open_html', false)
        
        %< * Identifying forbidden files in surround
        lff = cat(2, list_forbidden_files(get_ls_from_surround()), ...
            list_forbidden_files(get_ls_from_surround('QuantitativeResearch/st_work/usr/dev')));
        bad_guy = cell(size(lff));
        for i = 1 : length(lff)
            bad_guy{i} = get_most_surround_commiter(lff{i});
        end
        fid = fopen(fullfile(help_location, 'forbidden_files_report.m'), 'w');
        [u_bad_guy, ~, ind_bad_guy] = unique(bad_guy);
%         try
        fprintf(fid, ['%%%% TO BE REMOVED\n%%\n%% To remove the files you only have to click on the link "Remove ..." '...
            'But some of these links wont work because of space characters, then you have to use surround manually. '...
            'Perhaps you think these autogenerated messages are not relevant, in such a case please ask for an Exception to Romain\n']);
        for k = 1 : length(u_bad_guy)
            fprintf(fid, '%%\n%%%% %s\n%%\n', u_bad_guy{k});
            for i = 1 : length(lff)
                if (ind_bad_guy(i) == k)
                    fprintf(fid, ['%% * <matlab:verctrl(''remove'',' ...
                        'strrep(''%s'',[st_version.my_env.sscm_branch,''/st_work''],' ...
                        'st_version.my_env.st_work),0) Remove %s>\n'], lff{i}, lff{i});
                end
            end
        end
        fclose(fid);
        %> *
        
        publish_demos('single_file', ...
            'file_in', 'forbidden_files_report', ...
            'dir_out', help_location, 'open_html', false)
        
       
        
%         catch ME
%             fclose(fid);
%             rethrow(ME);
%         end
        %>

         %< Refreshes doc 
        rmpath(help_location);
        addpath(help_location);
        builddocsearchdb(help_location);
        %>
        
        % Correctly documented functions :
        % m2hmat.name(cellfun(@isempty, all_help_problems, 'uni', true)&is_function)
        
        
%         mdot(fullfile('doc', 'm2html', 'm2html.mat') , fullfile('doc', 'm2html', 'm2html.dot'));
        
        %>*
    case 'name-userdoc'
        %<* Name userdoc file
        % From the .m file
        fname  = which(varargin{1});
        [d, f] = fileparts( fname);
        d = strrep(d, getenv('st_work'), '');
        if d(1)=='\'
            d(1)='';
        end
        d = strrep(d, '\', '_');
        z = fullfile( getenv( 'st_work'), 'doc', 'userguide', [d '_' f '.html']);
        %>*
    case 'userdoc'
        %<* Extract userdoc for Boxes
        fname  = which(varargin{1});
        if isempty( fname)
            % J'ai un probl?me car je cherche ? faire le makedoc d'un
            % fichier qui n'est pas dans le path...
            z = {};
            st_log('<%s> not in the matlab path\n', varargin{1});
            return
        end
        dname = makedoc('name-userdoc', fname);
        fdate = dir( which( fname));
        ddate = dir( dname);
        if ~isempty( ddate) && fdate.datenum<=ddate.datenum
            z = {};
            st_log('<%s> unchanged on disk\n', fname);
            return
        end
        
        [d, f] = fileparts( fname);
        d = tokenize( d, '\\');
        state = 'open';
        oneliner = '';
        params   = '';
        plotf    = '';
        learnf   = '';
        fid = fopen( fname, 'r');
        while 1
            tline = fgetl(fid);
            if ~ischar(tline), break, end
            switch state
                case 'open'
                    if ~isempty( strfind( tline, 'function'))
                        state = 'oneliner';
                    end
                case 'oneliner'
                    tl = strtrim( tline);
                    if ~isempty( tl) && tl(1)=='%'
                        oneliner = sprintf('%s<<RETURN>>%s', oneliner, tl(2:end));
                    else
                        state = 'precode';
                    end
                case 'precode'
                    if ~isempty( strfind( tline, '''learn'''))
                        [tmp, tmp, cmt] = EXTRACT_P( tline, ',');
                        if isempty( strfind( cmt, '[]'))
                            learnf = '<<LEARN/>>';
                        end
                    end
                    if ~isempty( strfind( tline, '''plot'''))
                        [tmp, tmp, cmt] = EXTRACT_P( tline, ',');
                        if isempty( strfind( cmt, '[]'))
                            plotf = '<<PLOT/>>';
                        end
                    end
                    
                    if ~isempty( strfind( tline, 'this.params'))
                        state = 'params';
                        params = EXTRACT_P( tline);
                    end
                case 'params'
                    if ~isempty( strfind( tline, 'end')) || ~isempty( strfind( tline, 'this.memory'))
                        if ~isempty( params)
                            params = sprintf('<<PARAMS>>\n%s<</PARAMS>>', params);
                        end
                        state = 'done';
                    else
                        params = sprintf('%s\n%s', params, EXTRACT_P( tline));
                    end
                case 'done'
                    break
                otherwise
                    % nothing to do
            end
        end
        fclose( fid);
        
        ftype = d{end};
        
        z = { '<<BOXE>>', ...
            sprintf('<<FNAME>>%s%s%s<</FNAME>>',  f, learnf, plotf), ...
            oneliner, ...
            params, ...
            '<</BOXE>>' };
        
        opt = options({'BOXE', [ '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' ...
            '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">' ...
            '<LINK REL=StyleSheet HREF="../userguide.css" TYPE="text/css">' ...
            '<head></head>' '<body>' ], ...
            'FNAME' , '<h1>', '/FNAME', '</h1>', ...
            'LEARN/', '&nbsp;<font color=#770000 size=+1>@</font>', ...
            'PLOT/' , '&nbsp;<font color=#007700 size=+1>[~]</font>', ...
            'PARAMS', '<ul>', '/PARAMS', '</ul>', ...
            'RETURN', '<br/>', ...
            'PARAM', '<li>', '/PARAM', '</li>', ...
            'PNAME' , '<b>', '/PNAME', '</b>', ...
            '/BOXE', '</body></html>' });
            
        z = template_file( z, opt, [], 0);
        
        fod = fopen( dname, 'w');
        fprintf(fod, '%s', z{:});
        fclose(fod);
        %>*
    case 'find-boxes'
        %<* Find boxes
        if ispc()
            sep = ';';
        else
            sep = ':';
        end
        dirs = tokenize(genpath(getenv('st_work')), sep);
        idx  = cellfun(@(d)~isempty(strfind(d,fullfile(getenv('st_work'),'compilations')))||~isempty(strfind(d,fullfile(getenv('st_work'),'doc'))), ...
            dirs);
        dirs(idx) = [];
        if ~isempty( getenv('st_externals'))
            e_dirs = tokenize(genpath(getenv('st_externals')), ';');
            dirs = cellflat( { dirs, e_dirs});
        end
        dirs(cellfun(@(x)isempty(x),regexp(dirs, 'boxes'))) = [];
        boxes = {};
        areas = {};
        for d=1:length(dirs)
            this_dir  = dirs{d};
            st_log('working on dir <%s>...\n', strrep(this_dir, getenv('st_work'), ''));
            if ispc()
               sep = '\\';
            else
                sep = '/';
            end
            this_area = tokenize( this_dir, sep);
            if isempty( this_area)
                this_area = '[misc] ';
            else
                this_area = ['[' this_area{end} '] '];
            end
            
            all_funcs = dir( fullfile(dirs{d}, 'st_*.m'));
            
            all_funcs([all_funcs.isdir]) = [];
            all_funcs = cellfun(@(n)n(1:end-2), {all_funcs.name}, 'uni', false);
            
            cellfun( @(f)makedoc('userdoc', f), all_funcs, 'uni', false);
            
            boxes{end+1} = all_funcs;
            areas{end+1} = repmat({this_area}, 1, length(all_funcs));
        end
        boxes = cellflat( boxes);
        areas = cellflat( areas);
        [areas, idx] = sort( areas);
        boxes = boxes(idx);
        [ureas, tmp, udx] = unique(areas);
        for a=1:length(ureas)
            adx = a==udx;
            boxes(adx) = sort(boxes(adx));
        end
        z = cellfun(@(x,y)[x y], areas, boxes, 'uni', false);
        %>*
    case 'latest'
        %<* Oneliners for latests files
        all_dirs     = tokenize(genpath(getenv('st_work')),';');
        all_dirs(strmatch(fullfile(getenv('st_work'),'external'), all_dirs)) = [];
        all_dirs(strmatch(fullfile(getenv('st_work'),'compilations'), all_dirs)) = [];
        opt          = options({'past', 7}, varargin);
        past         = opt.get('past');
        if ischar( past)
            past = str2double( past);
        end
        build_doc    = @(a,p,f,t,d)struct('author', a, 'function', f, 'doc', t, 'path', p, 'date', d);
        doc_elements = build_doc({},{},{},{},{});
        for d=1:length(all_dirs)
            this_dir = all_dirs{d};
            st_log('working into <%s>\n', this_dir);
            all_files = dir( fullfile(this_dir,'*.m'));
            all_fnames = { all_files.name};
            all_isdir  = [all_files.isdir];
            all_dates  = [all_files.datenum];
            all_fnames(all_isdir | (all_dates<today-past)) = [];
            all_dates(all_isdir  | (all_dates<today-past))  = [];
            for f=1:length(all_fnames)
                this_fname = fullfile( this_dir, all_fnames{f});
                txt = help( this_fname);
                doc_elements(end+1) = build_doc( GET_AUTHOR( this_dir), strrep(this_dir,getenv('st_work'),''), all_fnames{f}, txt, datestr(all_dates(f),'dd/mm/yy'));
            end
        end
        st_log('---\n%d files modified since %s found\n', length(doc_elements), datestr(today-past,'dd/mm/yyyy'));
        
        authors = { doc_elements.author};
        [a,idx] = sort(authors);
        doc_elements = doc_elements(idx);
        
        this_docdir  = fullfile(getenv('st_work'), 'doc','latest');
        if isempty(dir(this_docdir))
            mkdir( this_docdir);
        end
        makedoc('table2htmlfile', makedoc('struct2html', doc_elements), fullfile( this_docdir, 'latest-table.html'), ...
            'h1', sprintf('<h1>Latest files since %s</h1>\n', datestr( today-past,'dd/mm/yyyy')) );
        makedoc('funcs2htmlfile', doc_elements, fullfile( this_docdir, 'latest-funcs.html'), ...
            'h1', sprintf('<h1>Latest files since %s</h1>\n', datestr( today-past,'dd/mm/yyyy')) );
        z            = doc_elements;
        %>*
    case 'table2htmlfile'
        %<* Embbdeding html code into a file 
        table4html = varargin{1};
        this_docfile = varargin{2};
        opt = options({'h1', ''}, varargin(3:end));
        h1  = opt.get('h1');
        st_log('Printing results into <%s>\n', this_docfile);
        fod          = fopen( this_docfile, 'w');
        fprintf( fod, '<html>\n<body>\n');
        fprintf( fod, '%s', h1);
        
        fprintf( fod, '%s\n', table4html{:});
        
        fprintf( fod, '</body>\n</html>');
        fclose( fod);
        %>*
    case 'funcs2htmlfile'
        %<* list of funcs into file
        doc_elements = varargin{1};
        this_docfile = varargin{2};
        opt = options({'h1', ''}, varargin(3:end));
        h1  = opt.get('h1');
        st_log('Printing results into <%s>\n', this_docfile);
        
        docs = { doc_elements.doc };
        idx  = cellfun(@isempty, docs)';
        empty_docs   = doc_elements(idx);
        doc_elements = doc_elements(~idx);
        
        % open file
        fod          = fopen( this_docfile, 'w');
        fprintf( fod, '<html>\n<body>\n');
        fprintf( fod, '%s', h1);
        
        % contents
        fprintf( fod, '<ul>\n');
        fprintf( fod, '<li><a href="#docs">%d files with oneliners</a></li>\n', length(doc_elements));
        fprintf( fod, '<li><a href="#empty">%d files without oneliners</a></li>\n', length(empty_docs));
        fprintf( fod, '</ul>\n');
        
        % Real docs
        fprintf( fod, '<h2><a name="docs"/>%d recent files with oneliners</h2>\n', length(doc_elements));
        for e=1:length(doc_elements)
            fprintf( fod, '<p><table border=0 bgcolor="#b7efc1"><tr><td width="1024"><b>%s</b> - by <i>%s</i> the %s<br/>\n', ...
                doc_elements(e).function, doc_elements(e).author, doc_elements(e).date);
            fprintf( fod, '<pre>%s</pre><br/>\n', doc_elements(e).doc);
            fprintf( fod, 'Into [<i>%s</i>]\n</td></tr></table></p>\n', doc_elements(e).path);
        end
        
        % empty docs
        fprintf( fod, '<h2><a name="empty"/>%d recent files without any doc!</2>\n', length(empty_docs));
        table4html = makedoc('struct2html', empty_docs);
        fprintf( fod, '%s\n', table4html{:});
        
        % close file
        fprintf( fod, '</body>\n</html>');
        fclose( fod);
        %>*
    case 'struct2html'
        %<* Pretty print of a struct
        tbl = varargin{1};
        %< field names
        fs = fieldnames(tbl);
        th = sprintf('<th>%s</th>', fs{:});
        %>
        %< field values
        tr = arrayfun(@TD_FIELDS, tbl,'uni', false);
        %>
        z = cat(1,{'<table>'}, th,tr',{'</table>'});
        %>*
    case 'shadows2html'
        %<* Pretty print of the shadows
        shad = varargin{1};
        shadowfile = fullfile(getenv('st_work'), 'doc', 'userguide', 'shadows.html');
        fod          = fopen( shadowfile, 'w');
        fprintf( fod, '<html>\n<body>\n');
        fprintf( fod, '<h1>shadows found the %s</h1>\n', datestr(today,'dd/mm/yy'));
        fprintf( fod, '<table border=1>\n');
        fprintf( fod, '<tr><th>filename</th><th>nature</th><th>locations</th></tr>\n');
        
        for s=1:length(shad)
            %fprintf( fod, '<tr><td>%s</td><td>', shad(s).reference);
            this_ref = shad(s).reference;
            this_path = which( this_ref);
            for f=1:length(shad(s).shadows)
                this_file = shad(s).shadows{f};
                st_idx = strfind(this_file, getenv('st_work'));
                neutral_file = this_file(st_idx+length(getenv('st_work'))+1:end);
                nats = tokenize(neutral_file, '\\');
                if ismember(nats{1}, { 'usr'})
                    this_nat = [nats{2} ':' nats{3}];
                else
                    this_nat = nats{1};
                end
                if strcmpi(this_file, this_path)
                    this_file = ['<font color="#FF0000">*</font>' this_file];
                end
                if f==1
                    this_ref = sprintf('<td rowspan=%d>%s</td>', length(shad(s).shadows), this_ref);
                else
                    this_ref = '';
                end
                fprintf( fod, '<tr>%s<td>%s</td><td>%s</td></tr>', this_ref, this_nat, this_file);
            end
        end
        fprintf( fod, '</table>\n');
        fprintf( fod, '</body>\n</html>');
        fclose( fod);
        %>*
    otherwise
        error('makedoc:mode', 'mode <%s> unknown', mode);
end

%%** Internals

%<* Get author from path
function str = GET_AUTHOR( p)
str = regexprep(p, '.*\\dev\\([^\\])+\\.*', '$1');
if isempty(str) || ~isempty(strfind(str,'\'))
    str = 'unkown';
end
%>*

%<* One struct to html
function cs = TD_FIELDS( fs)
cs = struct2cell( fs);
cs = sprintf('<td>%s</td>', cs{:});
cs = sprintf('<tr>%s</tr>', cs);
%>

%<* Extract potential params help from the line
function [str, pname, cmt] = EXTRACT_P( aline, sepp)
if nargin<2
    sepp='%';
end
str = '';
b = strfind( aline, '''');
if isempty( b)
    return
end
aline = aline(b+1:end);
b = strfind( aline, '''');
if isempty( b)
    return
end
pname = aline(1:b-1);
aline = aline(b+1:end);
b = strfind( aline, sepp);
if isempty( b)
    cmt = 'no comment!';
else
    cmt = strtrim(aline(b+1:end));
    if ~isempty( cmt) && cmt(1)=='-'
        cmt = strtrim( cmt(2:end));
    end
end
str = sprintf('<<PARAM>><<PNAME>>%s<</PNAME>> %s<</PARAM>>', pname, cmt);

    
%>*