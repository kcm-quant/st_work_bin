function o = ocamaweb( fname, varargin)
% OCAMAWEB - all for literate programming in MATLAB
%
% exemple:
%   ocamaweb ocamaweb_demo
% or:
%   ocamaweb ocamaweb_demo pdf

if nargin < 2
    external_pdf = false;
elseif strcmpi( varargin{1}, 'pdf')
    external_pdf = true;
end
in_error = false;    

ocw_bin  = getenv('OCAMAWEB');
ocw_dest = getenv('OCAMAWEB_DEST');

fname = which( fname);

pwd = cd;
cd( ocw_bin);
nfname = strrep(fname,'\','/');
[~, pfname, ~] = fileparts( nfname);
if external_pdf
    fprintf('ocamaweb transformation...\n');
    [a,o] = dos(sprintf('ocamaweb "%s" "%s.tex"', nfname, fullfile(ocw_dest, pfname)));
    if ~isempty(strfind(lower(o), 'error'))
        fprintf('\n\n==>It seems that an error occurred...\n');
        in_error = true;
    else
        cd( ocw_dest);
        fprintf('latex compilation...\n');
        [a,o] = dos(sprintf('latex --src -interaction=nonstopmode "%s.tex"',  fullfile(ocw_dest, pfname)));
        fprintf('latex compilation (again)...\n');
        [a,o] = dos(sprintf('latex --src -interaction=nonstopmode "%s.tex"',  fullfile(ocw_dest, pfname)));
    end
    fprintf('Done\n');
else
    fprintf('ocamaweb transformation...\n');
    [a,o] = dos(sprintf('ocamaweb "%s"', nfname));
    fprintf('Done\n');
end
cd(pwd);
if ~isempty(strfind(lower(o), ' error '))
    fprintf('\n\n==>It seems that an error occurred...\n');
else
    [d,f] = fileparts( fname);
    winopen( fullfile( ocw_dest, [f '.pdf']));
end
