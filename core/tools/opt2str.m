function str = opt2str( opt)
% OPT2STR convert une variable de type 'option' en une cha�ne de caract�res
%
% author   : 'mdang@cheuvreux.com'
% reviewer : 'rburgot@cheuvreux.com'
% version  : '1'
% date     : '29/08/2008'
%
% See also str2opt convs
assoc = opt.get();
assoc(2:2:end) = cellfun( @(v)convs('str',v), assoc(2:2:end), 'uni', false);
str   = sprintf(['%s' char(167)], assoc{1:end-1});
str   = sprintf('%s%s', str, assoc{end});