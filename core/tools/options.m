function o = options( varargin)
% OPTIONS - options management
%  ce n'est pas case sensitive
%  tr�s utile pour g�rer les param�tres optionnels des fonctions
%
% opt = options({'alpha', 1, 'beta', randn(5,2), 'gamma', 'azerty'}, {'alpha', -1})
% opt.get('alpha')
% opt.amend({'alpha', 6, 'beta', 9, 'garzol', 12, 'test', @(x)x+3})
% opt.eval('test', 2)
% % opt.info()
%
%         set: @options/set      - set('key', value)
%         get: @options/get      - get('key') -> value
%     try_get: @options/try_get  - try_get('key', def_value)
%     get_idx: @options/get_idx  - get_idx('key') ->idx (for internal use)
%       amend: @options/amend    - amend( { lst }) -> set(lst{k}, lst{k+1})
%        eval: @options/eval_    - eval( 'key-func', fargin) -> func( fargin{:})
%      visual: @options/visual   - visual() -> GUI
%     compare: @options/compare  - compare( opt2) -> [diff_ref, diff_comp]
%      update: @options/update_  - update() -> updated interface (TBI)
%      remove: @options/remove_  - remove( 'key')
%     isempty: @options/isempty_ - isempty()
%       clone: @options/clone    - clone()
%        size: @options/size     - size()
%    get_keys: @options/get_keys - get_keys()
%
% See also userdata struct2list list2struct template_file


% When j_options will be fully fonctionnal

o = j_options(varargin{:});
clear varargin;
return;

% Les lignes 36 � 46 ne doivent pas �tre effac�es, elles ne sont pas l�
% seulement pour l'histoire mais aussi car �trangement cela ne fonctionne
% pas sans.
this.assoc = varargin{1}; %#ok<UNRCH> % as explained above
if nargin>1
    % valeurs optionnelles
    opt_plus = varargin{2};
    cellfun(@(k,v)set(k,v), opt_plus(1:2:length(opt_plus)-1), opt_plus(2:2:length(opt_plus)) , 'uniformoutput', false);
    clear varargin opt_plus
end
o = struct('set', @set, 'get', @get, 'try_get', @try_get, 'get_idx', @get_idx, ...
    'amend', @amend, 'eval', @eval_, 'visual', @visual, 'compare', @compare, ...
    'update', @update_, 'remove', @remove_, 'get_no_err_no_ddot', @get_no_err_no_ddot, ...
    'isempty', @isempty_, 'clone', @clone);


%%** Embedded functions
%<* Compare with another option
    function [lst_ref, lst_comp] = compare( opt, k, v, vu)
        % for each key
        lst_ref  = {};
        lst_comp = {};
        for k=1:2:length(this.assoc)-1
            try
                v  = opt.get( this.assoc{k});
                if strcmpi( class(v), 'function_handle')
                    vu = functions( v);
                else
                    vu = v;
                end
                vr = this.assoc{k+1};
                if strcmpi( class(vr), 'function_handle')
                    vr = functions( vr);
                end
                if ~isequal( vu,vr)
                    % not equal
                    lst_ref  = cat(1,lst_ref , this.assoc(k:k+1));
                    lst_comp = cat(1,lst_comp, {this.assoc{k}, v});
                end
            catch
                % key not present
                lst_ref  = cat(1,lst_ref , this.assoc(k:k+1));
                lst_comp = cat(1,lst_comp, {this.assoc{k}, []});
                lasterr('');
            end
        end
    end
%>*
%<* Remove
    function v = remove_( k, i)
        try
            i = get_idx(k);
            v = this.assoc{2*i};
            this.assoc(2*i-1:2*i) = [];
        catch
            % nothing to do
            st_log('options: key <%s> is not present...\n', k);
            lasterr('');
        end
    end
%>*
%<* Update
% To be implemented
    function k = update_( k)
        k = k+1;
    end
%>*
%<* Get the index
    function idx = get_idx( k)
        idx = strmatch( lower(k), lower(this.assoc(1:2:length(this.assoc)-1)), 'exact');
    end
%>*
%<* Get the index
    function idx = get_idx_ddot( k)
        idx = strmatch( lower(k), lower(ctokenize(this.assoc(1:2:length(this.assoc)-1), 1, ':')), 'exact');
    end
%>*
%<* Try get
    function [v, idx] = try_get(k, v)
        idx = get_idx( k);
        if ~isempty(idx)
            [v, idx] = get(k, v);
        end
    end
%>*
%<* Get value from key
    function [v, idx] = get( k, varargin)
        if nargin==0
            v   = this.assoc;
            idx = 1:2:length(this.assoc)-1;
            return
        end
        idx = get_idx(k);
        if isempty(idx)
            idx = get_idx_ddot( k);
            if isempty( idx)
                error('options:key', 'key <%s> not available', k);
            end
        end
        if length(idx)>1
            warning('options:key', 'key <%s> has more that one occurence, I take the first one', k);
            idx = idx(1);
        end
        if nargin<2
            v   = this.assoc{idx*2};
        else
            v   = this.assoc{idx*2}(varargin{:});
        end
    end
%>*
%<* Set value from key
    function [v, idx, a] = set(k, v, varargin)
        idx = get_idx(k);
        if isempty(idx)
            this.assoc{end+1} = k;
            this.assoc{end+1} = v;
        elseif length(idx)>1
            error('options:key', 'key <%s> has more that one occurence, impossible to set its value', k);
        else
            this.assoc{idx*2} = v;
        end
        if nargin>2
            for a=1:2:length(varargin)-1
                [k, k] = set(varargin{a}, varargin{a+1});
                idx = [idx;k];
            end
        end
        if nargin>2
            v = {v, varargin{2:2:end-1}};
        end
    end
%>*
%<* Modify by a cellarray
    function amend( lst)
        cellfun(@(k,v)set(k,v), lst(1:2:length(lst)-1), lst(2:2:length(lst)) , 'uniformoutput', false);
    end
%>*
%<* Evaluation of a function
    function [h, v, str_fct] = eval_( fct, varargin)
        v = get(fct);
        str_fct = fct;
        if ischar( v)
            str_fct = [ str_fct ':' v];
        end
        st_log('Evaluate <%s>...\n', str_fct);
        h = feval( v, varargin{:});
    end
%>*
%<* Visual modif
    function h = visual( title, prompt,def,answ)
        if nargin < 1
            title = '';
        else
            title = sprintf('%s: ', title);
        end
        prompt = this.assoc(1:2:end-1);
        if ~isempty(prompt)
            def    = cellfun(@(v)convs('str',v),this.assoc(2:2:end),'uni',false);
            answ   = inputdlg(prompt,sprintf('%sparameters', title), 1, ...
                def);
            if isempty( answ)
                st_log('nothing done\n');
                h = false;
            else
                def    = cellfun(@(s)convs('param',s),answ,'uni',false);
                this.assoc(2:2:end) = def;
                h = true;
            end
        else
            warning('options:visual', 'You are trying to modify an empty option.\nHave a look to why you are doing such a thing.\n')
        end
    end
%<* returns [] if key does not exist, and does not check for ':' in assoc
    function v = get_no_err_no_ddot(k, idx)
        idx = strmatch( lower(k), lower(this.assoc(1:2:length(this.assoc)-1)), 'exact');
        if isempty(idx)
            v = [];
        else
            v = this.assoc{idx*2};
        end
    end
%>*
%<* returns true if this.assoc is empty
    function b = isempty_()
        b = isempty(this.assoc);
    end
%>*
%<* returns a clone
    function h = clone()
        h = options(this.assoc);
    end
%>*
end

function o = j_options(varargin)
%<* pas de check � la cr�ation
this = struct('content', [], 'hT', java.util.Properties);
if nargin > 0
    this.content = varargin{1}(2:2:end);
    for i = 1 : 2 : length(varargin{1})
        this.hT.put(varargin{1}{i}, (i+1)/2);
    end
else
    this.content = {};
end
%>*

if nargin>1
    opt_plus = varargin{2};
    cellfun(@(k,v)j_set(k,v), opt_plus(1:2:length(opt_plus)-1), opt_plus(2:2:length(opt_plus)) , 'uniformoutput', false); % inline amend
    clear varargin opt_plus
end
clear i

o = struct('set', @j_set, 'get', @j_get, 'try_get', @j_try_get, 'get_idx', @j_get_idx, ...
    'amend', @j_amend, 'eval', @j_eval_, 'visual', @j_visual, 'compare', @j_compare, ...
    'update', @j_update_, 'remove', @j_remove_, 'get_no_err_no_ddot', @j_get_no_err_no_ddot, ...
    'isempty', @j_isempty_ , 'clone', @j_clone, 'contains_key', @(k)this.hT.containsKey(k), ...
    'size', @()this.hT.size(), 'get_keys', @j_get_keys);

%<* Get the index
    function idx = j_get_idx( k)
        idx = this.hT.get(k);
    end
%>*
    % eponym
    function b=j_isempty_
        b=this.hT.isEmpty();
    end

%<* Get the index
    function idx = j_get_idx_ddot( k, this_assoc, i, keys, curr_key, str_end)
        keys = this.hT.keys();
        idx = 1;
        while keys.hasMoreElements()
            curr_key = keys.nextElement();
            str_end = strfind(curr_key, ':');
            if ~isempty(str_end) && strcmp(curr_key(1:str_end - 1), k)
                return;
            end
            idx = idx + 1;
        end
        idx = [];
    end
%>*

%<* Try get
    function [v, idx] = j_try_get(k, v)
        idx = j_get_idx( k);
        if ~isempty(idx)
            [v, idx] = j_get(k);
        end
    end
%>*

%<* Get value from key
    function [v, idx, hTsize, keys, i] = j_get( k, varargin)
        if nargin==0
            hTsize = this.hT.size();
            v = cell(2 * hTsize, 1);
            idx = 1 : 2 : 2 * hTsize;
            keys = this.hT.keys();
            i = 1;
            while keys.hasMoreElements()
                v{2*i-1} = keys.nextElement();
                v{2*i}   = this.content{this.hT.get(v{2*i-1})}; % inline get_idx
                i = i + 1;
            end
            return;
        end
        idx = this.hT.get(k);  % inline get_idx
        if isempty(idx)
            idx = j_get_idx_ddot( k);
            if isempty( idx)
                error('options:key', 'key <%s> not available', num2str(k));
            end
        end
        if length(idx)>1
            warning('options:key', 'key <%s> has more that one occurence, I take the first one', k);
            idx = idx(1);
        end
        if nargin<2
            v   = this.content{idx};
        else
            v   = this.content{idx}(varargin{:});
        end
        idx = 2 * idx; % so nice.............. just for history as many things in the code of j_options
    end
%>*

%<* returns [] if key does not exist, and does not check for ':' in assoc
    function v = j_get_no_err_no_ddot(k)
        v = this.hT.get(k); % inline get_idx
        if isempty(v)
            return;
        end
        v = this.content{v};
    end
%>*

%<* returns a clone
    function h = j_clone()
        h = j_options(j_get());
    end
%>*

%<* Set value from key
    function [v, idx, a] = j_set(k, v, varargin)
        idx = this.hT.get(k); % inline get_idx
        if isempty(idx)
            idx = length(this.content) + 1;
            this.hT.put(k, idx);
            this.content{idx} = v;
            idx = 2 * idx;
        else
            this.content{idx} = v;
        end
        if nargin>2
            for a=1:2:length(varargin)-1
                [k, k] = j_set(varargin{a}, varargin{a+1});
                idx = [idx;k];
            end
            v = {v, varargin{2:2:end-1}};
        end
    end
%>*

%<* Visual modif
    function h = j_visual( title, prompt, vals,def,answ)
        if nargin < 1
            title = '';
        else
            title = sprintf('%s: ', title);
        end
        prompt = j_get();
        vals = prompt(2:2:end);
        prompt = prompt(1:2:end-1);
        if ~isempty(prompt)
            def    = cellfun(@(v)convs('str',v),vals,'uni',false);
            answ   = inputdlg(prompt,sprintf('%sparameters', title), 1,def);
            for def = 1 : length(answ)
                j_set(prompt{def}, convs('param', answ{def}));
            end
            h = true; % TODO une vraie comparaison?
        else
            warning('options:visual', 'You are trying to modify an empty option.\nHave a look to why you are doing such a thing.\n')
        end
    end
%>

%<* Modify by a cellarray
    function j_amend( lst)
        cellfun(@(k,v)j_set(k,v), lst(1:2:length(lst)-1), lst(2:2:length(lst)) , 'uniformoutput', false);
    end
%>*

%<* Evaluation of a function
    function [h, v, str_fct] = j_eval_( fct, varargin)
        v = j_get(fct);
        str_fct = fct;
        if ischar( v)
            str_fct = [ str_fct ':' v];
        end
        st_log('Evaluate <%s>...\n', str_fct);
        h = feval( v, varargin{:});
    end
%>*

%<* Compare with another option : TODO better when someone will use it
    function [lst_ref, lst_comp] = j_compare( opt, k, v, vu)
        % for each key
        lst_ref  = {};
        lst_comp = {};
        this_assoc = j_get();
        for k=1:2:length(this_assoc)-1
            try
                v  = opt.get( this_assoc{k});
                if strcmpi( class(v), 'function_handle')
                    vu = functions( v);
                else
                    vu = v;
                end
                vr = this_assoc{k+1};
                if strcmpi( class(vr), 'function_handle')
                    vr = functions( vr);
                end
                if ~isequal( vu,vr)
                    % not equal
                    lst_ref  = cat(1,lst_ref , this_assoc(k:k+1));
                    lst_comp = cat(1,lst_comp, {this_assoc{k}, v});
                end
            catch
                % key not present
                lst_ref  = cat(1,lst_ref , this_assoc(k:k+1));
                lst_comp = cat(1,lst_comp, {this_assoc{k}, []});
                lasterr('');
            end
        end
    end
%>*

%<* Remove
    function v = j_remove_( k, i)
        i = this.hT.get(k); % inline get_idx
        if isempty(i)
            st_log('options: key <%s> is not present...\n', k);
        else
            this.hT.remove(k);
            v = this.content{i};
            this.content{i} = [];
        end
    end
%>*

%<* Update
% To be implemented
    function k = j_update_( k)
        k = k+1;
    end
%>*

%<* get_keys
    function c = j_get_keys()
        ks = this.hT.keySet;
        if ks.isEmpty
            c = {};
            return;
        end
        c = cell(ks.size(), 1);
        it = ks.iterator();
        j = 1;
        while it.hasMoreElements()
            c{j} = it.nextElement();
            j = j + 1;
        end
    end
%>*
end