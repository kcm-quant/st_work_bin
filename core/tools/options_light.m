function s = options_light(defaults, varargin_)
% OPTIONS_LIGHT - A map using a structure 
% The aim is just the same as options but with limited features and
% increased performances. It will also refuse to build options that have no
% default values, an example of this kind of error would be : options_light({},{'opt2', 'val2changed'});
%
% Examples:
%
% s = options_light({'opt1', 'val1', 'opt2', 'val2'},{'opt2', 'val2changed'});
%
% s.opt1
% s.opt2
% 
% % Performance test, something like 10 times faster
% tic
% for i = 1 : 1000
%   s = options_light({'opt1', 'val1', 'opt2', 'val2'},{'opt2', 'val2changed'});
%   s.opt1;
%   s.opt2;
% end
% toc
%
% tic
% for i = 1 : 1000
%   s = options({'opt1', 'val1', 'opt2', 'val2'},{'opt2', 'val2changed'});
%   s.get('opt1');
%   s.get('opt2');
% end
% toc
% 
% See also: options
%    
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '25/02/2011'
if nargin == 1
    varargin_ = [];
end
for i = 2 : 2 : length(defaults)
    if iscell(defaults{i})
        defaults{i} = defaults(i);
    end
end
s = struct(defaults{:});


for i = 1 : 2 : length(varargin_)
    s.(varargin_{i}) = varargin_{i+1};
end

end