function str = pprint( o)
% PPRINT - pretty print 'any' objct
%
% A terminer...
switch class(o)
    case 'double'
        [n,p] = size(o);
        str_o = num2str(o(1,min(4,p)));
        if n>1 || p>4
            str_o = [str_o '...'];
        end
        str = sprintf('double(%d,%d)= %s', n,p, str_o);
    otherwise
        str = 'unknown type...';
end
        