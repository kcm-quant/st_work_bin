function st_compile( mode, varargin)
% ST_COMPILE - embedded compilation
%
% st_compile('gr2exe', fname [, 'append', more_fnames])
% st_compile( 'exe', func_name)
%

switch lower( mode)
    case 'exe'
        fdir = fullfile( getenv('st_work'), 'compilations');
        if isempty( dir(fdir))
            mkdir( fdir);
        end
        fname = varargin{1};
        fdir = fullfile( fdir, fname);
        if isempty( dir(fdir))
            mkdir( fdir);
        end
        cwd = pwd;
        cd( fdir);
        mcc( '-m', fname);
        cd( cwd);
    case 'gr2exe'
        %<* Compile a grapher
        % Fonction built by gr.compile( fname)
        opt = options({'append', {}}, varargin(2:end));
        fdir = fullfile( getenv('st_work'), 'compilations');
        if isempty( dir(fdir))
            mkdir( fdir);
        end
        fname = varargin{1};
        fdir = fullfile( getenv('st_work'), 'compilations', fname);
        if isempty( dir(fdir))
            mkdir( fdir);
        end
        startu = which('startup');
        movefile( startu, [ startu '-current'], 'f');
        cwd = pwd;
        cd( fdir);
        app = opt.get('append');
        app_f = {};
        if ~isempty(app)
            if ischar(app)
                app = { app };
            end
            app_f = {};
            for a=1:length(app)
                app_f{end+1} = '-a';
                app_f{end+1} = app{a};
            end
        end
        mcc( '-m', fname, '-v', '-a', [fname, '.mat'], app_f{:});
        cd( cwd);
        movefile( [ startu '-current'], startu, 'f');
        %>*
    otherwise
        error('st_compile:mode', 'exec mode <%s> unknown', mode);
end
