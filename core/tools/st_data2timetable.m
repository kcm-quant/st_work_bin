function tt = st_data2timetable(data)
try
    time = round(mod(data.date,1)*24*3600-...
        1e-6*st_data('cols',data,'microseconds'));
catch
    time = floor(mod(data.date,1)*24*3600);
end
y = year(data.date);
m = month(data.date);
dy = day(data.date);
h = hour(time/24/3600);
mi = minute(time/24/3600);
s = floor(second(time/24/3600));
try
%     ms = floor(st_data('cols',data,'microseconds')/1e3);
    ms = zeros(length(time), 1);
catch
    ms = zeros(length(time), 1);
end
tt = array2timetable(data.value,'RowTimes',datetime(y,m,dy,h,mi,s,ms),...
    'VariableNames',data.colnames);
tt.Properties.UserData = data.info;
% openvar('tt')