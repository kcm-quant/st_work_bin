function st_data_explorer(data, varargin)
% ST_DATA_EXPLORER - GUI designed to explore st_data objects
%
%
%
% Examples:
%
%
%
%
% See also: st_data_editor
%
%
%   author   : 'athabault@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '13/05/2011

opt = options_light({'invert_colors', false,...
    'rowstripping', false,...
    'datestr_ms', false,...
    'sortenable',false},...
    varargin);

invert_colors = opt.invert_colors;
date_color_rowstripping= opt.rowstripping;
if opt.datestr_ms;
    datestr_func = @datestr_ms;
else
    datestr_func = @datestr;
end

if st_data('check', data)
    
    
    
    figure_name = ['st_data_explorer - ' data.title];
    h = findobj('Tag', figure_name);
    if isempty(h)
        % Create a figure that will have a uitable
        h = figure('Name', figure_name,...  % Title figure
            'NumberTitle', 'off',... % Do not show figure number
            'MenuBar', 'none',...
            'Tag', figure_name);      % Hide standard menu bar menus
        
    else
        figure(h);
    end
    
    %< Gestion rownames
    rownames = 'numbered';
    if isfield(data, 'rownames')
        rownames = data.rownames;
    end
    %>
    %< Gestion colnames
    
    % All column contain numeric data except the .date
    numeric = {'numeric'};
    colfmt = cat(2,'char', numeric(ones(size(data.colnames))));
    % Disallow editing values (but this can be changed)
    coledit = false(1,length(data.colnames)+1);
    % Set columns all the same width (must be in pixels)
    colwdt =  num2cell([160 60*ones(1,size(coledit,2)-1)]); % 'auto'
    
    % Dates
    id_finite_dates=isfinite(data.date);
    st_data_editor_dates_cell = cell(length(data.date),1);
    
    if any(id_finite_dates)
        idx_finite_dates=find(id_finite_dates);
        st_data_editor_dates_ = datestr_func( data.date(id_finite_dates));
        for i = 1:length(idx_finite_dates)
            st_data_editor_dates_cell{idx_finite_dates(i)} = st_data_editor_dates_(i,:);
        end
    end
    if any(~id_finite_dates)
        st_data_editor_dates_cell(~id_finite_dates)={'NaN'};
    end
    %>
    
    %< Gestion codebooks
    data_cdk = {};
    cols_cdk = {};
    cols_numeric = data.colnames;
    if isfield(data, 'codebook') && codebook('iscodebook', data.codebook)
        
        [cols_cdk, cols_numeric] = intersect_and_diff(data.colnames, {data.codebook.colname});
        if ~isempty(cols_cdk)
            
            data_cdk = st_data('col_cdk', data, cols_cdk);
        end
    end
    
    %>
    % Retour au colnames
    colnames = cat(2, '.date', cols_cdk, cols_numeric);
    
    uitable_data = cat(2,...
        st_data_editor_dates_cell,...
        data_cdk,...
        num2cell(st_data('col', data, cols_numeric)));
    
    %< Color definition
    bgcolor = [1 1 1];
    fgcolor = [0 0 0];
    fg_color_str = 'black';
    
    if invert_colors
        bgcolor = [0 0 0];
        fgcolor = [1 1 1];
        fg_color_str = 'white';
    end
    
    if date_color_rowstripping
        [~,~,idx] = unique(uitable_data(:,1));
        %     uitable_data(mod(idx,2)==0, 1) = ...
        %         cellfun(@(c) sprintf('<HTML><FONT color="blue">%s</FONT></HTML>', c),...
        %         uitable_data(mod(idx,2)==0, 1), 'uni', false);
        uitable_data(mod(idx,2)==1, 1:(1+size(cols_cdk,2))) = ...
            cellfun(@(c) sprintf('<HTML><FONT color="teal">%s</FONT></HTML>', c),...
            uitable_data(mod(idx,2)==1, 1:(1+size(cols_cdk,2))), 'uni', false);
        
        uitable_data(mod(idx,2)==1, (2+size(cols_cdk,2)):end) = ...
            cellfun(@(c) sprintf('<HTML><FONT color="teal">%g</FONT></HTML>', c),...
            uitable_data(mod(idx,2)==1, (2+size(cols_cdk,2)):end), 'uni', false);
        
        
        uitable_data(mod(idx,2)==0, 1:(1+size(cols_cdk,2))) = ...
            cellfun(@(c) sprintf('<HTML><FONT color="%s">%s</FONT></HTML>',fg_color_str, c),...
            uitable_data(mod(idx,2)==0, 1:(1+size(cols_cdk,2))), 'uni', false);
        
        uitable_data(mod(idx,2)==0, (2+size(cols_cdk,2)):end) = ...
            cellfun(@(c) sprintf('<HTML><FONT color="%s">%g</FONT></HTML>',fg_color_str, c),...
            uitable_data(mod(idx,2)==0, (2+size(cols_cdk,2)):end), 'uni', false);
    end
    %>
    
    % Create a uitable on the left side of the figure
    htable = uitable('Parent', h,...
        'Units', 'normalized',...
        'Position', [0 0 1 1],...
        'BackgroundColor', bgcolor,...
        'ForegroundColor', fgcolor,...
        'Data',  uitable_data,...
        'RearrangeableColumns', 'on',...
        'RowName', rownames,...
        'ColumnName', colnames,...
        'ColumnFormat', colfmt,...
        'ColumnWidth', colwdt,...
        'ColumnEditable', coledit);
    
    if opt.sortenable
        jscrollpane = findjobj(htable);
        jtable = jscrollpane.getViewport.getView;
        
        % Now turn the JIDE sorting on
        jtable.setSortable(true);		% or: set(jtable,'Sortable','on');
        jtable.setAutoResort(true);
        jtable.setMultiColumnSortable(true);
        jtable.setPreserveSelectionsAfterSorting(true);
    end
    
else
    warning('st_data:check', 'This object is not a correct st_data');
end

end