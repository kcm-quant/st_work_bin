function varargout = st_depfun( mode, fun)
% ST_DEPFUN - depfun for st_work
%
% [dp, ext, bad] = st_depfun('fun', 'st_data')
% [dp, ext, bad, nodes, enr] = st_depfun('graph', 'preprocess4market_impact_01b.mat')
%
% See also depfun FileDependencies makedoc 

% author  = 'clehalle@cheuvreux.com'
% version = '1.0'
global st_version

switch lower(mode)
    case 'graph'
        %<* Appel sur un graphe
        st_log('Graph loading (can be long)...\n');
        gr = load( fun);
        gr = gr.gr;
        nodes = gr.get('graph');
        nodes(cellfun(@isempty, nodes))=[];
        nodes_fun = cellfun(@(n)n.get('st_function'), nodes, 'uni', false);
        enr = gr.get('enrichments');
        enr_fun = repmat({''}, 1,length(enr));
        for e=1:length(enr)
            enr_fun{e} = func2str( enr(e).content.get('fun'));
        end
        st_log('Calling <st_depfun> in <fun> mode...\n');
        [dp, ext, bad] = st_depfun( 'fun', {nodes_fun{:}, enr_fun{:} });
        varargout = { dp, ext, bad, nodes_fun, enr_fun};
        %>*
    case 'fun'
        %<* Appel sur une liste de fonctions
        if ~iscell( fun)
            fun = { fun };
        end
        nok = 0;
        dp  = depfun( fun{:}, '-quiet');
        idx = cellfun(@isempty, regexp(dp, strrep(getenv('st_work'), '\', '\\')));
        dp(idx) = [];
        tags = repmat({[]}, length(dp), 1);
        for d=1:length(dp)
            tags{d} = makedoc( 'find-tags', dp{d});
            if lower( tags{d}.version(end)) ~= 'b' || strcmpi( tags{d}.reviewer, 'none')
                st_log('function <%s>\n ==> has no reviewer or no reviewer version...\n',  dp{d});
            end
        end
        varargout = { dp };
        idx = cellfun(@(x)~isempty(x), regexp(dp, strrep(fullfile( getenv('st_work'), 'external'), '\', '\\')));
        if sum(idx)>0
            nok = nok+sum(idx);
            warning( 'st_depfun:externals', '%d dependencies are in the <external> subdir...', nok);
            st_log('- %s\n', dp{idx});
            if nargout>1
                varargout{end+1} = dp(idx);
            end
            dp(idx) = [];
        elseif nargout>1
                varargout{end+1} = [];
        end
        if ~isfield( st_version, 'prod_dirs')
            error('st_depfun:startup', 'you do not have the lateste version of the official startup file...');
        end
        
        idx = cellfun(@isempty, regexp(dp, strrep(fullfile( getenv('st_work'), st_version.prod_dirs{1} ), '\', '\\')));
        for p=2:length(st_version.prod_dirs)
            idx = idx & ...
                cellfun(@isempty, regexp(dp, strrep(fullfile( getenv('st_work'), st_version.prod_dirs{p} ), '\', '\\')));
        end
        if sum(idx)>0
            warning( 'st_depfun:prod', '%d dependencies are not in the official production subdirs...', sum(idx));
            nok = nok + sum(idx);
            st_log('- %s\n', dp{idx});
            if nargout>2
                varargout{end+1} = dp(idx);
            end
        elseif nargout>2
            varargout{end+1} = [];
        end
        if nok>0
           warning( 'st_depfun:review', '%d files to review before going to production...', nok);
        end
        %>*
    otherwise
        error('st_depfun:mode', 'unknown mode <%s>', mode);
end