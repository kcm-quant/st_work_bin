function fs = st_patch( mode)
% ST_PATCH - gestion des patchs
%  la config des patch est g�r�e dans la variables st_version du startup.m
%
%  les getenv n�cessaires sont:
%   - matlab_home
%   - st_work
%   - matlab_save_plot
% 
% st_patch archive
% st_patch install
%
% see also startup
global st_version

zip_fname = 'st_patch.zip';

to_be_patched = st_version.patch_files;
switch lower(mode)
    case 'archive'
        %<* Archive patched files
        all_files = {to_be_patched.files};
        all_files = [ all_files{:}];
        filepos = cellfun(@(f)which(f), all_files, 'uniformoutput', false);
        zip( fullfile( getenv('st_work'), 'core', 'init', zip_fname), filepos);
        %>*
    case 'install'
        %<* Install patched files
        fprintf('unzipping files to the ''temp'' dir...\n');
        unzip( fullfile( getenv('st_work'), 'core', 'init', zip_fname), getenv('temp'));
        fprintf('copying them to their destinations...\n');
        for p=1:length(to_be_patched)
            cellfun(@(f) movefile( fullfile(getenv('temp'), f), to_be_patched(p).destination, 'f'), to_be_patched(p).files);
        end
        %>*
    otherwise
        error('st_patch:mode', 'ptch mode <%s> unknown', mode);
end