function opt = str2opt( str)
% STR2OPT renvoie une variable de type 'options' � partir d'une cha�ne de
% caract�res. Cette cha�ne est le r�sultat de la fonction 'opt2str'
%
% author   : 'mdang@cheuvreux.com'
% reviewer : 'rburgot@cheuvreux.com'
% version  : '1'
% date     : '29/08/2008'
%
% See also opt2str convs 
t_str = tokenize(str, char(167));
def   = cellfun( @(s)convs('param',s), t_str(2:2:end), 'uni',false);
t_str(2:2:end) = def;
opt = options(t_str);
