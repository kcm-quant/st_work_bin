function lst = struct2list( struct_)
% STRUCT2LIST - convert a struct to a list for options
%
% s = struct('a',1,'b',2)
% list2struct(struct2list( s))
%
% s = struct('b', {1,2}, 'c', 'a')
% list2struct(struct2list( s))
%
% s = struct('b', {{1,2}}, 'c', 'a')
% list2struct(struct2list( s))
%
% s = struct('b', {{1,2}}, 'c', {1,2})
% s.b
% s.c
% list2struct(struct2list( s))
% s.b
% s.c
%
%
% s = struct('b', {1;2}, 'c', 'a')
% list2struct(struct2list( s))
%
% s = struct('b', {{1;2}}, 'c', {1;2})
% s.b
% s.c
% list2struct(struct2list( s))
% s.b
% s.c
%
% See also struct options list2struct

[m n] = size(struct_);
if m >1 && n > 1
    error('struct2list:check_args', 'Conversion to list is not yet implemented for such struct size : <%d*%d> input must have only one dimension', m, n);
end

fn = fieldnames( struct_);
lst = cell(1,length(fn)*2);
lst(1:2:end-1) = fn;
tmp = struct2cell( struct_);
for i = 1 : length(fn)
    lst{2*i} = tmp(i, :);
end