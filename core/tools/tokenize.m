function tk = tokenize( str, varargin)
% TOKENIZE - tokenize a string, returns a cellarray
% 
% examples:
%  tokenize( 'alpha;beta' , ';')   -> { 'alpha', 'beta' }
%  tokenize( 'alpha;beta;', ';')   -> { 'alpha', 'beta' }
%  tokenize( 'alpha;beta', ';', 2) -> 'beta' 
if nargin<2
    sep = ';';
else
    sep = varargin{1};
end

pat = ['([^' sep ']+)'];
tk  = regexp( str, pat, 'match');
if nargin>2
    tk = tk{varargin{2}};
end