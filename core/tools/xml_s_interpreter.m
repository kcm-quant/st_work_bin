function rslt = xml_s_interpreter(mode, xml_s, varargin)
% XML_S_INTERPRETER - fonctions pour l'interpr�tation des STRUCTURES (pas
% l'objet) renvoy�es par le parsing d'un fichier xml (� l'aide de la fonction
% xmltools puis en appliquant la m�thode this � ce r�sultat)
%
% see also xmltools
try
    xml_s.tag;
catch
    xml_s = xml_s.this();
end
if ~strcmp(mode, 'xml_s')
    rslt = feval([mode '_interpreter'], mode, xml_s, varargin{:});
    return
end
switch xml_s.tag
    case '#document'
        rslt = xml_s_interpreter(mode, xml_s.children, varargin{:});
    case '#comment'
        rslt = [];
    case 'str_function'
        if length(xml_s.attribs) ~= 1
            error('xml_struct:func', 'A fonction cannot but have one unique attribute');
        end
        if isempty(xml_s.children)
            error('xml_struct:func:children', 'A function has to have some children : its inputs');
        end
        children_cellstr = arrayfun(@(i)xml_s_interpreter(mode, xml_s.children(i), varargin{:}), 1:length(xml_s.children), 'uni', false);
        switch xml_s.attribs.key
            case 'name'
                children_cellstr = sprintf('%s, ', children_cellstr{:});
                rslt = [xml_s.attribs.value '(' children_cellstr(1:end-2) ')'];
            case 'str'
                rslt = sprintf(xml_s.attribs.value, children_cellstr{:});
            otherwise
                error('xml_struct:func:attribs', 'Unknown attribute key : <%s>', xml_s.attribs.key);
        end
    case 'str_variable'
        if length(xml_s.attribs) ~= 1
            error('xml_struct:variable', 'A variable cannot have more than one attribute');
        end
        if ~isempty(xml_s.children)
            error('xml_struct:variable:children', 'A variable cannot but have one unique attribute');
        end
        opt = options({'specific_name', '', ...
            'specific_call', 'error(specific_call:not_specified)'}, varargin);
        switch xml_s.attribs.key
            case 'name'
                rslt = xml_s.attribs.value;
            case opt.get('specific_name')
                rslt = sprintf(opt.get('specific_call'), xml_s.attribs.value);
            otherwise
                error('xml_struct:variable:attribs', 'Unknown attribute key : <%s>', xml_s.attribs.key);
        end
    otherwise
        idx = strmatch('str_spec_tag', varargin);
        if ~isempty(idx) && strcmp(xml_s.tag, varargin{idx+1})
            idx_att = strmatch('name', {xml_s.attribs.key});
            idx_spec_str = strmatch('str_spec_str', varargin);
            if ~isempty(idx_spec_str)
                rslt = sprintf(varargin{idx_spec_str+1}, xml_s.attribs(idx_att).value);
                return;
            end
        end
        error('xml_s_interpreter:tag', 'Unknown tag : <%s>', xml_s.tag);
end
end


function rslt = set_params_interpreter(mode, xml_s, varargin)
if ~strcmp(mode, 'set_params')
    rslt = feval([mode '_interpreter'], mode, xml_s, varargin);
end
switch xml_s.tag
    case '#document'
        rslt = set_params_interpreter(mode, xml_s.children, varargin{:});
    case '#comment'
        rslt = [];
    case 'graph'
        opt = options({'xml_name', '', 'gi_name', '', 'add_small_test', false}, varargin);
        gi_name = opt.remove('gi_name');
        xml_name = [gi_name '.xml'];
        if isempty(gi_name)
            error('gi_tools:xml2set_params:check_args', 'estimator_name has to be specified');
        end
        common_gi_params = {'security_id', 'trading_destination_id', 'from', 'to'};
        xml_s = xmltools(xml_s);
        % < On fixe les valeurs par d�faut
        gp = xml_s.get_tag('graph_params');
        gp = gp.children;
        graph_params = {};
        if ~isempty(gp) && ~isempty(gp.children) % si la section existe et contient quelquechose
            nb_params = length(gp.children);
            sod = '';
            graph_params = cell(nb_params, 1);
            part_mk_str  = cell(nb_params, 1);
            to_be_hashed = false(nb_params, 1);
            found_mk_info  = false;
            for i = 1 : nb_params
                tmp = set_params_interpreter(mode, gp.children(i));
                [part_sod_str, graph_params{i}, part_mk_str{i}] = tmp{:};
                sod = [sod, part_sod_str];
                if isempty(part_mk_str{i})
                    to_be_hashed(i) = true;
                else
                    found_mk_info = true;
                end
            end
            sod(end) = [];
            has_some_graph_params = true;
        else
            has_some_graph_params = false;
            sod = '...';
        end
        % >
        
        tmp = intersect(common_gi_params, graph_params);
        if ~isempty(tmp)
            error('interpreter_set_params:graph_params', ...
                ['Error in your xml file the following graph_param names are' ...
                ' forbidden as these names are already used as parameters for' ...
                ' all graph_indicators : \n%s'], sprintf('\t<%s>\n', tmp{:}));
        end
        
        % < Mode Masterkey
        tag_mk = xml_s.get_tag('masterkey');
        if has_some_graph_params
            if ~found_mk_info && ~isempty(tag_mk.children) && ~isempty(tag_mk.children.children)
                mk = xml_s_interpreter(mode, tag_mk.children.children, ...
                    'specific_name', 'graph_param', 'specific_call', 'opt4gi.remove(''%s'')', ...
                    'str_spec_tag', 'graph_param', 'str_spec_str', 'opt4gi.remove(''%s'')');
                mk = ['rslt = ' mk ';'];
            elseif ~isempty(tag_mk.children)
                error('gi_tools:xml2set_params:masterkey', ...
                    ['You cannot have a way to make a masterkey either in the section graph_params and in the section masterkey.\n' ...
                    'You should either delete these informations from the section graph_params or delete the whole section masterkey']);
            else
                mk = sprintf('rslt = fullfile(...\n');
                % < Options qui poss�dent une fonction donnant un nom de r�pertoire sp�cifique
                tmp = find(~to_be_hashed)';
                for i = tmp(1:end-1)
                    mk = sprintf('%s%s, ...\n', mk, part_mk_str{i});
                end
                if ~isempty(tmp)
                    mk = sprintf('%s%s ...\n', mk, part_mk_str{tmp(end)});
                end
                % >
                % < options qui seront hash�es
                if any(to_be_hashed)
                    mk = [mk 'hash(convs(''safe_str'', {'];
                    tmp = find(to_be_hashed)';
                    for i = tmp(1:end-1)
                        mk = [mk sprintf('opt4gi.remove(''%s''),...\n', graph_params{i})];
                    end
                    mk = [mk sprintf('opt4gi.remove(''%s'')}), ''MD5'')', graph_params{tmp(end)})];
                end
                % >
                mk = [mk ');'];
            end
        else
            mk = 'rslt = '''';';
        end
        % >
        
        % < Set params � proprement parler...
        spn = xml_s.get_tag('nodes');
        spn = spn.children.children;
        sp = '';
        for i = 1 : length(spn)
            str = set_params_interpreter(mode, spn(i));
            sp = sprintf('%s%s\n\n', sp, str);
        end
        % >
        
        % < On va parcourir la chaine de caract�res sp avec deux buts :
        % 1) On veut v�rifier que tous les param�tres du graph sont
        % effectivement utilis�s
        % 2) Pour les param�tres qui ne sont utilis�s qu'une fois on peut
        % faire un remove plut�t qu'un get, pour les autres, on veut faire
        % un remove qui stocke la valeur dans une variable, puis les acc�s
        % � ce param�tre doivent se faire � l'aide de cette variable
        graph_params = cat(1, graph_params, common_gi_params');
        curr_tmp_var_number = 1;
        for i = 1 : length(graph_params)
            tmp = sprintf('opt4gi.get(''%s'')', graph_params{i}); % la chaine de caract�re que l'on veut voir apparaitre au moins une fois
            tmp_rep = sprintf('opt4gi.remove(''%s'')', graph_params{i});
            idxes = strfind(sp, tmp);
            if isempty(idxes) % le param�tre de graphe ne permet de changer aucun param�tre de boite, c'est une erreur
                error('interpreter_set_params:set_params', ...
                    'Error in your xml file the following graph_param is not used : <%s>', graph_params{i});
            elseif length(idxes) == 1 % alors on peut directement remplacer la chaine de carat�re par la m�me mais en remplacant get par remove
                sp = strrep(sp, tmp, tmp_rep);
            else
                curr_tmp_var = sprintf('tmp_%d', curr_tmp_var_number);
                sp = strrep(sp, tmp, curr_tmp_var);
                sp = sprintf('%s = %s;\n%s', curr_tmp_var, tmp_rep, sp);
                curr_tmp_var_number = curr_tmp_var_number + 1;
            end
        end
        % >
        
        %*< On remplit le template avec les sp�cificit�s r�colt�es
        template_fullpath = which('set_params_template.m');
        template_pathstr = fileparts(template_fullpath);
        final_file = fullfile(template_pathstr, ['set_params_' gi_name '.m']);
        %< Ecriture de la chaine de carat�re
        str = template_file(template_fullpath, ...
            options({'GI_NAME', gi_name, 'U_GI_NAME', upper(gi_name), ...
            'XML_FILE_NAME', which(xml_name), ...
            'AUTHOR', sprintf('''%s''', which('gi_tools')), ...
            'REVIEWER', sprintf('''%s''', hostname), ...
            'DATE', sprintf('''%s''', datestr(today, 'dd/mm/yyyy')), ...
            'SDO', sod, 'MASTERKEY', mk, ...
            'SET_PARAMS', sp}), [], 0);
        str = sprintf('%s\n', str{:});
        str = indentmcode(str);
        %>
        %< On �crit cette chaine dans le fichier
        [set_params_fid, message] = fopen(final_file, 'w');
        if set_params_fid == -1
            error('gi_tools:xml2set_params', ...
                'I cannot open the file <%s>\nI got the following error : <%s>', ...
                final_file, message);
        end
        fprintf(set_params_fid, text_to_printable_text(str));
        fclose(set_params_fid);
        %>
        %*>
        
        % Si l'on veut pouvoir �x�cuter la fonction que l'on vient
        % d'�crire, il faut demander � matlab de reconstruire ses liens
        % vers les fonctions, d'o� les deux lignes qui suivent
        rehash
        clear functions
        
        % < Tests, v�rifications
        % On �dite le fichier pour que l'utilisateur le voit
        if opt.get('add_small_test')
            edit(final_file);
            
            % On appelle la fonction en mode masterkey (et sans arguments)
            % afin que l'utilisateur puisse v�rifier la clef par d�faut
            tmp = sprintf('set_params_%s(''masterkey'', options({''security_id'', NaN}))', gi_name);
            st_log(['!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n' ...
                'TEST OPTIONS PAR DEFAUT ET MODE MASTERKEY :\n' ...
                'Voici la clef que l''on obtient dans le cas o� l''on choisit les options par d�faut : \n\t<%s> \n' ...
                'Elle a �t� obtenue en utilisant la commande : \n\t<%s>\n' ...
                '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n'], ...
                eval(tmp), tmp);
        end
        rslt = {};
        % >
    case 'graph_param'
        att = xml_s.attribs;
        att_keys    = {att.key};
        att_values  = {att.value};
        idx_name    = strmatch('name', att_keys);
        idx_def_val = strmatch('def_val', att_keys);
        name        = att_values{idx_name};
        sod_str     = sprintf('''%s'', %s, ...\n', name, att_values{idx_def_val});
        att_keys([idx_name idx_def_val])   = [];
        att_values([idx_name idx_def_val]) = [];
        if isempty(att_keys)
            rslt = {sod_str, name, ''};
            return;
        elseif length(att_keys) > 1
            error('gi_tools:xml2set_params:graph_params:z_set_params_att2set_def_val', ...
                'Don''t know what to do with this number of parameters');
        end
        switch att_keys{1}
            case 'func'
                mk_str = sprintf('%s(opt4gi.remove(''%s''))', att_values{1}, name);
            case 'func_str'
                mk_str = sprintf(att_values{1}, sprintf('opt4gi.remove(''%s'')', name));
            otherwise
                error('gi_tools:xml2set_params:graph_params:z_set_params_att2set_def_val', ...
                    'Don''t know what to do with such attribute : <%s>', att_keys{1});
        end
        rslt = {sod_str, name, mk_str};
    case 'node'
        xml_o = xmltools(xml_s);
        att = xml_o.get_root_attributes_as_options();
        str = sprintf('gr.set_node_param(  ''%s'', ...\n', att.get('name'));
        %         val = xml_s.value;
        public_params = xml_o.select_where_attrib('public', 'true');
        public_params = public_params.children;
        if ~isempty(public_params) && all(strcmp('param', {public_params.tag}))
            for i = 1 : length(public_params)
                curr_param = xmltools(public_params(i));
                cp_name = curr_param.get_root_attrib('name');
                curr_param = curr_param.this();
                if ~isempty(curr_param.children)
                    str = sprintf('%s''%s'', %s, ...\n', str, cp_name, ...
                        xml_s_interpreter('xml_s', curr_param.children, ...
                        'specific_name', 'graph_param', 'specific_call', 'opt4gi.get(''%s'')', ...
                        'str_spec_tag', 'graph_param', 'str_spec_str', 'opt4gi.get(''%s'')'));
                else
                    str = sprintf('%s''%s'', %s, ...\n', str, cp_name, ...
                        sprintf('opt4gi.get(''%s'')', cp_name));
                end
            end
            str(end-5:end) = [];
        end
        %         if ~isempty(val)
        %             if ~isempty(xml_s)
        %                 str = sprintf('%s, ...\n', str);
        %             end
        %             if strcmp(val, 'strd_set_params')
        %                 str = sprintf(['%s''RIC:char'', opt4gi.get(''security_id''), ...\n' ...
        %                     '''trading-destinations:cell'', opt4gi.get(''trading_destination_id''), ...\n' ...
        %                     '''from:dd/mm/yyyy'', opt4gi.get(''from''), ...\n' ...
        %                     '''to:dd/mm/yyyy'', opt4gi.get(''to'')'], str);
        %             end
        %         end
        rslt = [str ');'];
    otherwise
        xml_s_interpreter('xml_s', xml_s, varargin{:});
end
end