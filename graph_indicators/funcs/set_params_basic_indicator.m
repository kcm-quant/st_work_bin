function h = set_params_basic_indicator(gr, opt4gi)
% 
%
% opt = {'window:time', datenum(0,0,0,0,5,0), ...
%       'step:time', datenum(0,0,0,0,5,0),...
%       'bins4stop:b', false};
% data = read_dataset(['gi:basic_indicator/' opt2str(options(opt))], 'security_id', 110, 'from', '01/10/2008', 'to', '01/03/2009', 'trading-destinations', {});
%
%
% author   : 'rburgot@cheuvreux.com'
% reviewer : 'clehalle@cheuvreux.com'
% version  : '1'
% date     : '20/08/2008'

% attention ce graph indicator subit un traitement particulier dans get_gi,
% il faut donc synchroniser specific_default_options dans les deux
% fichiers...

specific_default_options = {...
    'window:time', datenum(0,0,0,0,15,0),... % window time on which the aggregation is made
    'step:time', datenum(0,0,0,0,15,0), ... % frequency at which aggregated statistics are sampled
    't_acc:time', datenum(0,0,0,0,11,0), ... % required accuracy on trading times
    'bins4stop:b', true, ... % do you want stop auction as separated bins or do you it to be included in other bins
    'given_trdt:cell', {}, ... % please do not use
    'security_id', NaN, ... % 
    'from', '01/01/1899', ...
    'to', '01/01/1899', ...
    'trading_destination_id', {NaN}... % if empty, all trading destinations are kept
    'source:char', 'tick4bi', ... % used to know on which data to apply the aggregation
    'volume_by_td:b', false, ... %please do not use
    };

if nargin == 1
    try
        opt4gi = gr.get_global('set_params_visual_memory');
    catch ME
        if strcmp(ME.identifier, 'options:key') && strcmp(ME.message, 'key <set_params_visual_memory> not available')
            opt4gi = options(specific_default_options);
        else
            rethrow(ME);
        end
    end
    opt4gi.visual();
    gr.set_global('set_params_visual_memory', opt4gi.clone());
else
    opt4gi = options(specific_default_options, opt4gi.get());
end

%< Masterkey mode
if ischar( gr) 
    switch lower(gr)
        case 'masterkey'
            sec_dt = 1/(24*3600);
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
            v_by_td = opt4gi.remove('volume_by_td:b');
            h = fullfile( ['t_acc=' num2str(round(opt4gi.remove('t_acc:time')/sec_dt)) 's'], ...
                ['width=' num2str(round(opt4gi.remove('window:time')/sec_dt)) 's'], ...
                ['step=' num2str(round(opt4gi.remove('step:time')/sec_dt)) 's'], ...
                ['source=' opt4gi.remove('source:char')], ...
                ['bins4stop=' num2str(opt4gi.remove('bins4stop:b'))], ...
                hash(convs('safe_str', opt4gi.remove('given_trdt:cell')), 'MD5'));
            if v_by_td
                h = fullfile('v_by_td', h);
            end
            if ~opt4gi.isempty()
                lst = opt4gi.get();
                error('set_params_basic_indicator:masterkey:check_args', ...
                    'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
            end
        otherwise
            error('set_params_basic_indicator:mode', 'mode <%s> unknown', gr);
    end
    return
end
%>

% la suite du code est inutilisée en raison du traitement particulier dans
% get_gi

error('set_params_basic_indicator:exec', 'unreachable code was reached');

gr.set_node_param(  'st read dataset', 'RIC:char', opt4gi.remove('security_id'), ...
    'source:char', opt4gi.get('source:char'), ...
    'trading-destinations:cell', opt4gi.remove('trading_destination_id'), ...
    'from:dd/mm/yyyy', opt4gi.remove('from'), ...
    'to:dd/mm/yyyy', opt4gi.remove('to'));

lst = opt4gi.get();
gr.set_node_param(  'st basic indicator', lst{:});
h = [];

end