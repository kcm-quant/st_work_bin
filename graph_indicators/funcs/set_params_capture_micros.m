function h = set_params_capture_micros(gr, opt4gi)
% SET_PARAMS_CAPTURE_MICROS
%
% author   : 'dcroize@cheuvreux.com'
% reviewer : 
% version  : '1'
% date     : '12/02/2009'


sdo = {
   'window:time', datenum(0,0,0,0,5,0),...
    'step:time', datenum(0,0,0,0,5,0), ...
    'window:char', 'day|20', ...
    'security_id', 'SEC.NAME', ...
    'from', '01/01/1899', ...
    'to', '01/01/1899', ...
    'trading_destination_id', { NaN}, ...
%     'raw:data', 0, ...
    };


if nargin == 1
    try
        opt4gi = gr.get_global('set_params_visual_memory');
    catch ME
        if strcmp(ME.identifier, 'options:key') && strcmp(ME.message, 'key <set_params_visual_memory> not available')
            opt4gi = options(sdo);
        else
            rethrow(ME);
        end
    end
    opt4gi.visual();
    gr.set_global('set_params_visual_memory', opt4gi.clone());
else
    opt4gi = options(sdo, opt4gi.get());
end

%< Masterkey mode
if ischar( gr)
    switch lower(gr)
        case 'masterkey'
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
            opt4gi.remove('raw:data');
             h = fullfile(['window_spec=' strrep(opt4gi.remove('window:char'), '|', '_')], ...
                ['width=' num2str(round(opt4gi.remove('window:time'))) 's'], ...
                ['step=' num2str(round(opt4gi.remove('step:time'))) 's']);
            
            if ~opt4gi.isempty()
                lst = opt4gi.get();
                error('set_params_capture_micros:masterkey:check_args', ...
                    'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
            end
            
        otherwise
            error('set_params_capture_micros:mode', 'mode <%s> unknown', gr);
    end
    return
end
%>

mwc_  = opt4gi.remove('window:char');
from_ = opt4gi.remove('from');
sec_  =opt4gi.remove('security_id');
td_   = opt4gi.remove('trading_destination_id');
to_   = opt4gi.remove('to');

subgi_call = ['gi:basic_indicator/' opt2str(opt4gi)];
st_log( 'subgi call <%s>...\n', subgi_call);

gr.set_node_param(  'st read dataset', 'RIC:char', sec_, ...
    'trading-destinations:cell', td_, ...
    'from:dd/mm/yyyy',from_, ...
    'to:dd/mm/yyyy', to_, ...
    'window:char',mwc_, ...
    'source:char', subgi_call, ...
    'last_formula','[{volume}, {vwap}, {nb_trades}, {volume_sell}, {vwap_sell}, {nb_sell}, {vwas}, {vol_GK}]',...
    'where_formula:char', '~{auction}'); % You might have something to do here as explained just below

% raw_d =  opt4gi.remove('raw:data');
% gr.set_node_param( 'capture microstructure', 'raw:data', raw_d);
h = [];
end
