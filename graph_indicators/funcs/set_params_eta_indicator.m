function h = set_params_eta_indicator(gr, opt4gi)
% SET_PARAMS_ETA_INDICATOR 
%
%
% author   : 'dcroize@cheuvreux.com'
% reviewer : ''
% version  : '2'
% date     : '05/01/2009'
specific_default_options = {...
    'security_id', NaN, ...
    'from', '01/01/1899', ...
    'to', '01/01/1899', ...
    'trading_destination_id', {NaN}...
    'window:char', 'day|60',... % options obligatoires
    };

if nargin == 1
    opt4gi = options(specific_default_options);
    opt4gi.visual();
else
    opt4gi = options(specific_default_options, opt4gi.get());
end

%< Masterkey mode
if ischar( gr)
    switch lower(gr)
        case 'masterkey'
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
            %             if opt4gi.isempty()
            %                 h = '';
            %             else
            %                 h = hash(opt2str(opt4gi), 'MD5');
            h = fullfile(['window_char=' strrep( opt4gi.remove('window:char'), '|', '_')]) ;
            
            if ~opt4gi.isempty()
                lst = opt4gi.get();
                error('set_params_eta_indicator:masterkey:check_args', ...
                    'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
            end
            % end
        otherwise
            error('set_params_eta_indicator:mode', 'mode <%s> unknown', gr);
    end
    return
end
%>

gr.set_node_param(  'st read dataset', 'RIC:char', opt4gi.remove('security_id'), ...
    'trading-destinations:cell', opt4gi.remove('trading_destination_id'), ...
    'from:dd/mm/yyyy', opt4gi.remove('from'), ...
    'to:dd/mm/yyyy', opt4gi.remove('to'),... 
    'window:char', opt4gi.remove('window:char'));


h = [];
end
