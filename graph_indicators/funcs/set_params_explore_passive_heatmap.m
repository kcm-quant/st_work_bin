function h = set_params_explore_passive_heatmap(gr, opt4gi)
% author : 'clehalle@cheuvreux.com'
% version  : '1'
% date     : '05/02/2009'
%
% data = read_dataset('gi:explore_passive_heatmap/window_inertia§30', ...
%                'security_id', 110, 'from', '02/01/2009', 'to', '02/01/2009', 'trading-destinations', {} )
specific_default_options = {...
    'security_id', NaN, ...
    'from', '02/01/2009', ...
    'to', '02/01/2009', ...
    'window_inertia', 30, ...
    'trading_destination_id', { } ...
    'proba', .95 ...
    };

if nargin == 1
    try
        opt_   = gr.get_global('set_params_visual_memory');
        opt4gi = options(specific_default_options,opt_.get() );
    catch ME
        if strcmp(ME.identifier, 'options:key') && strcmp(ME.message, 'key <set_params_visual_memory> not available')
            opt4gi = options(specific_default_options);
        else
            rethrow(ME);
        end
    end
    opt4gi.visual();
    gr.set_global('set_params_visual_memory', opt4gi.clone());
else
    opt4gi = options(specific_default_options, opt4gi.get());
end

%< Masterkey mode
if ischar( gr) 
    switch lower(gr)
        case 'masterkey'
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
            h =  fullfile(['inertia=' num2str(opt4gi.remove('window_inertia'))], ...
                ['proba=' num2str(opt4gi.remove('proba'))]);
            if ~opt4gi.isempty()
                lst = opt4gi.get();
                error('set_params_explore_passive_heatmap:masterkey:check_args', ...
                    'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
            end
        otherwise
            error('set_params_explore_passive_heatmap:mode', 'mode <%s> unknown', gr);
    end
    return
end
%>

gr.set_node_param(  'read dataset', 'RIC:char', opt4gi.remove('security_id'), ...
    'trading-destinations:cell', opt4gi.remove('trading_destination_id'), ...
    'from:dd/mm/yyyy', opt4gi.remove('from'), ...
    'window:char', ['day|' num2str(opt4gi.remove('window_inertia'))], ...
    'to:dd/mm/yyyy', opt4gi.remove('to'));

gr.set_node_param('market shares', 'proba', opt4gi.remove('proba'));

h = [];

end