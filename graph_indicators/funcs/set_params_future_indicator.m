function h = set_params_future_indicator(gr, opt4gi)
% TEMPLATE FILE for enrichment set_params in a graph indicator
%
% author   : 'rburgot@cheuvreux.com'
% reviewer : 'clehalle@cheuvreux.com'
% version  : '1'
% date     : '20/08/2008'
specific_default_options = {...
    ... ***********************************************************************
    ...                 PUT YOUR SPECIFIC OPTIONS HERE
    ... ***********************************************************************
    'security_id', 'CAC40', ...
    'from', '02/10/2008', ...
    'to', '02/10/2008', ...
    'trading_destination_id', {}, ...
    'step:time', 5/(24*60), ...
    'window_spec:char', 'day|180' ...
    };



if nargin == 1
    try
        opt4gi = gr.get_global('set_params_visual_memory');
    catch ME
        if strcmp(ME.identifier, 'options:key') && strcmp(ME.message, 'key <set_params_visual_memory> not available')
            opt4gi = options(specific_default_options);
        else
            rethrow(ME);
        end
    end

    opt4gi.visual();
    pause(1e-1);
    gr.set_global('set_params_visual_memory', opt4gi.clone());
else
    opt4gi = options(specific_default_options, opt4gi.get());
end


%< Masterkey mode
if ischar( gr)
    switch lower(gr)
        case 'masterkey'
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
            h = fullfile(['step=' num2str(round(opt4gi.remove('step:time')*3600*24)) 's'], ...
                ['window_spec=' strrep(opt4gi.remove('window_spec:char'), '|', '_')]);
            
            if ~opt4gi.isempty()
                lst = opt4gi.get();
                error('set_params_future_indicator:masterkey:check_args', ...
                    'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
            end
        otherwise
            error('set_params_future_indicator:mode', 'mode <%s> unknown', gr);
    end
    return
end
%>

security_id = opt4gi.get('security_id');
from = opt4gi.get('from');
to = opt4gi.get('to');


source4future = sprintf(['gi:basic_indicator/source:char' char(167) '"index' ...
    char(167) 'window:time' char(167) '%d' ...
    char(167) 'step:time' char(167) '%d'], opt4gi.get('step:time'), opt4gi.get('step:time'));


gr.set_node_param( 'future read dataset', ...
    'RIC:char', security_id, ...
    'trading-destinations:cell', {}, ...
    'formula:char', '[{open},{close},{vol_GK}]', ...
    'source:char', source4future, ...
    'window:char', opt4gi.get('window_spec:char'), ...
    'from:dd/mm/yyyy', from, ...
    'to:dd/mm/yyyy', to, ...
    'code2id:char', 'index');


source4comp = sprintf(['gi:basic_indicator/window:time' char(167) '%d' ...
    char(167) 'step:time' char(167) '%d'], opt4gi.get('step:time'), opt4gi.get('step:time'));


index_comp = get_repository('index-comp', security_id, 'recent_date_constraint', false);
% index_comp = [110, 26];

gr.set_node_param( 'comp read dataset', ...
    'RIC:char', index_comp', ...
    'trading-destinations:cell', {}, ...
    'formula:char', '[{open},{close},{vol_GK}]', ...
    'source:char', source4comp, ...
    'window:char', opt4gi.get('window_spec:char'), ...
    'from:dd/mm/yyyy', from, ...
    'to:dd/mm/yyyy', to, ...
    'where_formula:char', '~{auction}');



%**************************************************************************
% SET YOUR SPECIFIC OPTIONS IN THE RELEVANT ST_NODES : HERE AND JUST ABOVE
%       IN ST_READ_DATASET IF NEEDED
%**************************************************************************

h = [];
end
