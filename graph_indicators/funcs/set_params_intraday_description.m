function h = set_params_intraday_description(gr, opt4gi)
% SET_PARAMS_INTRADAY_DESCRIPTION - eponym
%
% data = read_dataset('gi:intraday_description/history�"day|300�indicator-type�fused', 'security_id', 'FTE.PA', 'from', '01/09/2008', 'to', '01/09/2008', 'trading-destinations', {} )
%
% author   : 'clehalle@cheuvreux.com'
% reviewer : 'to define'
% version  : '1'
% date     : '24/09/2008'
specific_default_options = {...
    'indicator-type', 'fused', ...
    'history', 'day|300', ...
    'security_id', NaN, ...
    'from', '01/01/1899', ...
    'to', '01/01/1899', ...
    'trading_destination_id', { 'MAIN' }...
    };

if nargin == 1
    try
        opt4gi = gr.get_global('set_params_visual_memory');
    catch ME
        if strcmp(ME.identifier, 'options:key') && strcmp(ME.message, 'key <set_params_visual_memory> not available')
            opt4gi = options(specific_default_options);
        else
            rethrow(ME);
        end
    end
    opt4gi.visual();
    gr.set_global('set_params_visual_memory', opt4gi.clone());
else
    opt4gi = options(specific_default_options, opt4gi.get());
end

%< Masterkey mode
if ischar( gr) 
    switch lower(gr)
        case 'masterkey'
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
            h = fullfile( ['history=' strrep( opt4gi.remove('history'),'|', '_')], ...
                ['type=' opt4gi.remove('indicator-type')]) ;
            if ~opt4gi.isempty()
                lst = opt4gi.get();
                error('set_params_basic_indicator:masterkey:check_args', ...
                    'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
            end
        otherwise
            error('set_params_basic_indicator:mode', 'mode <%s> unknown', gr);
    end
    return
end
%>

gr.set_node_param(  'basic indicators', 'RIC:char', opt4gi.remove('security_id'), ...
    'trading-destinations:cell', opt4gi.remove('trading_destination_id'), ...
    'from:dd/mm/yyyy', opt4gi.remove('from'), ...
    'window:char', opt4gi.remove('history'), ...
    'to:dd/mm/yyyy', opt4gi.remove('to'));

h = [];

end