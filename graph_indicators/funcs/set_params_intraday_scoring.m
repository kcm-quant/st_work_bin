function rslt = set_params_intraday_scoring(gr, opt4gi)
% SET_PARAMS_INTRADAY_SCORING - function to set parameters in the graph,
% made according to file : C:\st_work\bin\graph_indicators\funcs\intraday_scoring_set_params.xml
%
% author   : 'C:\st_work\bin\graph_indicators\gi_tools.m'
% reviewer : 'PADEV929'
% version  : '1'
% date     : '12/11/2008'

sdo = {...
    'history', 'day|300', ...
    'nb-dim', 10, ...
    'security_id', NaN, ...
    'from', '01/01/1899', ...
    'to', '01/01/1899', ...
    'trading_destination_id', {NaN}...
    };

if nargin == 1
    try
        opt4gi = gr.get_global('set_params_visual_memory');
    catch ME
        if strcmp(ME.identifier, 'options:key') && strcmp(ME.message, 'key <set_params_visual_memory> not available')
            opt4gi = options(specific_default_options);
        else
            rethrow(ME);
        end
    end
    opt4gi.visual();
    gr.set_global('set_params_visual_memory', opt4gi.clone());
else
    opt4gi = options(sdo, opt4gi.get());
end

%< Masterkey mode
if ischar( gr)
    switch lower(gr)
        case 'masterkey'
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
            rslt = fullfile(...
                ['history=' strrep( opt4gi.remove('history'),'|', '_')], ...
                ['nb-dim=' num2str( opt4gi.remove('nb-dim'))] ...
                );
            if ~opt4gi.isempty()
                lst = opt4gi.get();
                error('set_params_intraday_scoring:masterkey:check_args', ...
                    'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
            end
        otherwise
            error('set_params_intraday_scoring:mode', 'mode <%s> unknown', gr);
    end
    return;
end
%>

gr.set_node_param(  'basic indicators', ...
    'window:char', opt4gi.get('history'), ...
    'RIC:char', opt4gi.get('security_id'), ...
    'trading-destinations:cell', opt4gi.get('trading_destination_id'), ...
    'from:dd/mm/yyyy', opt4gi.get('from'), ...
    'to:dd/mm/yyyy', opt4gi.get('to'));

gr.set_node_param(  'PCA score', ...
    'nb-dimensions', opt4gi.get('nb-dim'));



rslt = [];
end
