function h = set_params_mi_characts(gr, opt4gi)
% SET_PARAMS_MI_CHARACTS
%
% use:
%  data = read_dataset('gi:mi_characts/imbalance_threshold�.25�window:char�"day|90', 'security_id', 'FTE.PA', 'from', '01/04/2008', 'to', '05/04/2008', 'trading-destinations', {'MAIN'})
specific_default_options = {
    'window:time', datenum(0,0,0,0,15,0),...
    'step:time', datenum(0,0,0,0,0,30), ...
    'window:char', 'day|180', ...
    'imbalance_threshold', 0.25, ...
    'security_id', 'RIC.CODE', ... % "FTE.PA
    'from', '01/01/1899', ... % "01/02/2008
    'to', '01/01/1899', ...   % "01/06/2008
    'trading_destination_id', {NaN}... % {}
    };

if nargin == 1
    try
        lst = gr.get_global('set_params_visual_memory');
        opt4gi = options( specific_default_options, lst.get());
    catch ME
        if strcmp(ME.identifier, 'options:key') && strcmp(ME.message, 'key <set_params_visual_memory> not available')
            opt4gi = options(specific_default_options);
        else
            rethrow(ME);
        end
    end
    opt4gi.visual();
    gr.set_global('set_params_visual_memory', opt4gi.clone());
else
    opt4gi = options(specific_default_options, opt4gi.get());
end

%< Masterkey mode
if ischar( gr)
    switch lower(gr)
        case 'masterkey'
            sec_dt = 1/(24*3600);
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
            h = fullfile(['window_spec=' strrep(opt4gi.remove('window:char'), '|', '_')], ...
                ['width=' num2str(round(opt4gi.remove('window:time')/sec_dt)) 's'], ...
                ['step=' num2str(round(opt4gi.remove('step:time')/sec_dt)) 's'], ...
                ['imb=' num2str(opt4gi.remove('imbalance_threshold')) ]);
            if ~opt4gi.isempty()
                lst = opt4gi.get();
                error('set_params_basic_indicator:masterkey:check_args', ...
                    'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
            end
        otherwise
            error('set_params_indicateur_mi:mode', 'mode <%s> unknown', gr);
    end
    return
end
%>
ric_  = opt4gi.remove('security_id');
td_   = opt4gi.remove('trading_destination_id');
from_ = opt4gi.remove('from');
to_   = opt4gi.remove('to');
wind_ = opt4gi.remove('window:char');

imb_tresh = opt4gi.remove('imbalance_threshold');

gr.set_node_param( 'st read dataset', 'RIC:char', ric_, ...
    'trading-destinations', td_, ...
    'from:dd/mm/yyyy', from_, ...
    'to:dd/mm/yyyy', to_, ...
    'window:char', wind_,...
    'source:char', ['gi:basic_indicator/' opt2str(opt4gi)]);

gr.set_node_param( 'mi characts', 'imbalance-threshold', imb_tresh);

h = [];
end