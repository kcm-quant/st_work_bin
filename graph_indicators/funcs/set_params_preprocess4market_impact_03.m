function h = set_params_preprocess4market_impact_03(gr, opt4gi)
% TEMPLATE FILE for enrichment set_params in a graph indicator
%       with an intraday box
%
% use:
%  data = read_dataset('gi:preprocess4market_impact_03/imbalance_add_threshold�.7�window:char�"day|120', 'security_id', 'FTE.PA', 'from', '10/12/2009', 'to', '10/12/2009', 'trading-destinations', {})

specific_default_options = {
    'window:time', datenum(0,0,0,0,15,0),...
    'step:time', datenum(0,0,0,0,0,30), ...
    'window:char', 'day|90', ...
    'imbalance_add_threshold',0.7 ,...
    'security_id', 'RIC.CODE', ... % "FTE.PA
    'from', '01/01/1899', ... % "01/02/2008
    'to', '01/01/1899', ...   % "01/06/2008
    'trading_destination_id', {NaN}... % {}
    };

if nargin == 1
    try
        lst = gr.get_global('set_params_visual_memory');
        opt4gi = options( specific_default_options, lst.get());
    catch ME
        if strcmp(ME.identifier, 'options:key') && strcmp(ME.message, 'key <set_params_visual_memory> not available')
            opt4gi = options(specific_default_options);
        else
            rethrow(ME);
        end
    end
    opt4gi.visual();
    gr.set_global('set_params_visual_memory', opt4gi.clone());
else
    opt4gi = options(specific_default_options, opt4gi.get());
end

%< Masterkey mode
if ischar( gr)
    switch lower(gr)
        case 'masterkey'
            sec_dt = 1/(24*3600);
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
            h = fullfile(['window_spec=' strrep(opt4gi.remove('window:char'), '|', '_')], ...
                ['width=' num2str(round(opt4gi.remove('window:time')/sec_dt)) 's'], ...
                ['step=' num2str(round(opt4gi.remove('step:time')/sec_dt)) 's'], ...
                ['imb_add=' num2str(opt4gi.remove('imbalance_add_threshold')) ]);
            
            if ~opt4gi.isempty()
                lst = opt4gi.get();
                error('set_params_preprocess4market_impact_03:masterkey:check_args', ...
                    'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
            end
        otherwise
            error('set_params_preprocess4market_impact_03:mode', 'mode <%s> unknown', gr);
    end
    return
end
%>
ric_  = opt4gi.remove('security_id');
td_   = opt4gi.remove('trading_destination_id');
from_ = opt4gi.remove('from');
to_   = opt4gi.remove('to');
wind_ = opt4gi.remove('window:char');

                        
imb_add_tresh = opt4gi.remove('imbalance_add_threshold');

gr.set_node_param( 'st read dataset', 'RIC:char', ric_, ...
    'trading-destinations:cell', td_, ...
    'from:dd/mm/yyyy', from_, ...
    'to:dd/mm/yyyy', to_, ...
    'window:char', wind_,...
    'source:char', ['gi:basic_indicator/' opt2str(opt4gi)]);

gr.set_node_param('preprocess MI','imbalance_add_threshold',imb_add_tresh,...
                  'window:time', opt4gi.get('window:time'),...
                  'step:time', opt4gi.get('step:time'));


h = [];
end