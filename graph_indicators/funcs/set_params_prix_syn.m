function h = set_params_prix_syn(gr, opt4gi)
% TEMPLATE FILE for enrichment set_params in a graph indicator
%
% author   : 'rburgot@cheuvreux.com'
% reviewer : 'clehalle@cheuvreux.com'
% version  : '1'
% date     : '20/08/2008'

specific_default_options = {...
... ***********************************************************************
...                 PUT YOUR SPECIFIC OPTIONS HERE
... ***********************************************************************
    'security_id', get_repository('index-comp', 'CAC40', 'recent_date_constraint', false), ...
    'from', '01/10/2008', ...
    'to', '01/10/2008', ...
    'trading_destination_id', {}, ...
    'step:time', datenum(0,0,0,0,5,0)  , ...
    'window:time', datenum(0,0,0,0,5,0), ...
    'window_spec:char', 'day|180' ...
    };


if nargin == 1
    try
        opt4gi = gr.get_global('set_params_visual_memory');
    catch ME
        if strcmp(ME.identifier, 'options:key') && strcmp(ME.message, 'key <set_params_visual_memory> not available')
            opt4gi = options(specific_default_options);
        else
            rethrow(ME);
        end
    end
    opt4gi.visual();
    gr.set_global('set_params_visual_memory', opt4gi.clone());
else
    opt4gi = options(specific_default_options, opt4gi.get());
end

%< Masterkey mode
if ischar( gr) 
    switch lower(gr)
        case 'masterkey'
            sec_dt = 1/(24*3600);
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
            h = fullfile(['step=' num2str(round(opt4gi.remove('step:time')/sec_dt)) 's'], ...
                ['window=' num2str(round(opt4gi.remove('window:time')/sec_dt)) 's'], ...
                ['window_spec=' strrep(opt4gi.remove('window_spec:char'), '|', '_')], ...
                ['A=' num2str(opt4gi.remove('A:b'))]);
            
... ***********************************************************************
...                 REMOVE YOUR SPECIFIC OPTIONS FROM opt4gi HERE
... ***********************************************************************
                if ~opt4gi.isempty()
                    lst = opt4gi.get();
                    error('set_params_prix_syn:masterkey:check_args', ...
                        'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
                end
        otherwise
            error('set_params_prix_syn:mode', 'mode <%s> unknown', gr);
    end
    return
end
%>

opt_str = opt2str(options({ ...
    'step:time', opt4gi.get('step:time'), ...
    'window:time', opt4gi.get('window:time'), ...
    }));

gr.set_node_param(  'st read dataset', 'RIC:char', opt4gi.remove('security_id'), ...
    'trading-destinations:cell', opt4gi.remove('trading_destination_id'), ...
    'from:dd/mm/yyyy', opt4gi.remove('from'), ...
    'to:dd/mm/yyyy', opt4gi.remove('to'), ...
    'source:char', ['gi:basic_indicator/'  opt_str], ...
    'where_formula:char', '~{auction}', ...
    'formula:char', '[{vwap}]', ...
    'window:char', 'day|180' ...
    ); % You might have something to do here as explained just below


gr.set_node_param(  'st\_prix\_syn', ...
    'A:b', opt4gi.get('A:b') ...
    ); % You might have something to do here as explained just below

    

% On lance le learning mode
gr.learn('st\_prix\_syn');
gr.set_memory('st read dataset', 'end_of_prod', false);

%**************************************************************************
% SET YOUR SPECIFIC OPTIONS IN THE RELEVANT ST_NODES : HERE AND JUST ABOVE
%       IN ST_READ_DATASET IF NEEDED
%**************************************************************************

h = [];
end
