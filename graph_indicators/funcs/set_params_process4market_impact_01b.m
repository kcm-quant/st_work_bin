function h = set_params_process4market_impact_01b(gr, opt4gi)
% SET_PARAMS_PROCESS4MARKET_IMPACT_01B - 
%    to be used by the graph indicator <process4market_impact_01b>
%
%  params:
%  - window:char is the nb of days to be used by preprocess4market_impact
%  - my-window is the nb of days asked to preprocess4market_impact
%  - imbalance_threshold is for preprocess4market_impact too
%  - usual 'rho>12', ...
%  - spread-weight 0.8
%
% use:
%  data = read_dataset('gi:process4market_impact_01b/window:char�"day|60', 'security_id', 'FTE.PA', 'from', '01/01/2008', 'to', '01/05/2008', 'trading-destinations', {})
specific_default_options = {
    'window:time', datenum(0,0,0,0,15,0),...
    'step:time', datenum(0,0,0,0,0,30), ...
    'my-window:char', 'day|60', ...
    'window:char', 'day|120', ...
    'security_id', 'SEC.NAME', ...
    'from', '01/01/1899', ...
    'to', '01/01/1899', ...
    'trading_destination_id', { NaN}, ...
    'usual', 'rho>12', ...
    'imbalance_threshold', 0.35, ...
    'spread-weight', 0.8 ...
    };

if nargin == 1
    try
        lst = gr.get_global('set_params_visual_memory');
        opt4gi = options( specific_default_options, lst.get());
    catch ME
        if strcmp(ME.identifier, 'options:key') && strcmp(ME.message, 'key <set_params_visual_memory> not available')
            opt4gi = options(specific_default_options);
        else
            rethrow(ME);
        end
    end
    opt4gi.visual();
    gr.set_global('set_params_visual_memory', opt4gi.clone());
else
    opt4gi = options(specific_default_options, opt4gi.get());
end

%< Masterkey mode
if ischar( gr)
    switch lower(gr)
        case 'masterkey'
            sec_dt = 1/(24*3600);
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
            h = fullfile(['window_spec=' strrep(opt4gi.remove('window:char'), '|', '_')], ...
                ['width=' num2str(round(opt4gi.remove('window:time')/sec_dt)) 's'], ...
                ['step=' num2str(round(opt4gi.remove('step:time')/sec_dt)) 's'], ...
                ['usual=' strrep(opt4gi.remove('usual'), '>', '_gt_')], ...
                ['my_window_spec=' strrep(opt4gi.remove('my-window:char'), '|', '_')], ...
                ['imb=' num2str(opt4gi.remove('imbalance_threshold')) ] , ...
                sprintf('spread-weight=%3.2f', opt4gi.remove('spread-weight') )  );
            if ~opt4gi.isempty()
                lst = opt4gi.get();
                error('set_params_basic_indicator:masterkey:check_args', ...
                    'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
            end
        otherwise
            error('set_params_indicateur_mi:mode', 'mode <%s> unknown', gr);
    end
    return
end
%>
gr.set_node_param( 'usual', 'thresholds:char', opt4gi.remove('usual'), 'plot:b', 0 );
gr.set_node_param( 'MI model', 'spread-weight', opt4gi.remove('spread-weight'), ...
    'plot:b', 0 );

mwc_  = opt4gi.remove('my-window:char');
from_ = opt4gi.remove('from');
sec_  =opt4gi.remove('security_id');
td_   = opt4gi.remove('trading_destination_id');
to_   = opt4gi.remove('to');
subgi_call = ['gi:preprocess4market_impact_02/' opt2str(opt4gi)];
st_log( 'subgi call <%s>...\n', subgi_call);
%fprintf('graph call:\n <gi:process4market_impact_01b/%s>\n', opt2str(options({'window:char', mwc_})));
gr.set_node_param( 'st read dataset', 'RIC:char', sec_ , ...
    'trading-destinations:cell', td_, ...
    'from:dd/mm/yyyy', from_, ...
    'to:dd/mm/yyyy', to_, ...
    'window:char', mwc_, ...
    'source:char', subgi_call);
h = [];
end