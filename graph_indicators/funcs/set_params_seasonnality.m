function h = set_params_seasonnality(gr, opt4gi)
% author   : 'rburgot@cheuvreux.com'
% reviewer : 'clehalle@cheuvreux.com'
% version  : '1'
% date     : '20/08/2008'
specific_default_options = {'mode_season', 'std', ...
    'seas_func', @nanmedian, ...
    'step:time', datenum(0,0,0,0,5,0), ...
    'filter_outlier:char', 'pca', ...
    'window_spec:char', 'day|180', ...
    'security_id', NaN, ...
    'from', '01/01/1899', ...
    'to', '01/01/1899', ...
    'trading_destination_id', {NaN}...
    'from_buffer_frequency', 'day|1', ...
    };
if nargin == 1
    try
        opt4gi = gr.get_global('set_params_visual_memory');
    catch ME
        if strcmp(ME.identifier, 'options:key') && strcmp(ME.message, 'key <set_params_visual_memory> not available')
            opt4gi = options(specific_default_options);
        else
            rethrow(ME);
        end
    end
    opt4gi.visual();
    gr.set_global('set_params_visual_memory', opt4gi.clone());
else
    opt4gi = options(specific_default_options, opt4gi.get());
end

%< Masterkey mode
if ischar( gr)
    switch lower(gr)
        case 'masterkey'
            sec_dt = 1/(24*3600);
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
            opt4gi.remove('from_buffer_frequency');
            fo = opt4gi.remove('filter_outlier:char');
            if opt4gi.contains_key('context_selection:char')
                cs = opt4gi.remove('context_selection:char');
            end
            if opt4gi.contains_key('force_unique_intraday_auction:b')
                fuia = opt4gi.remove('force_unique_intraday_auction:b');
            end
            if opt4gi.contains_key('convert_ifix2_to_ifix1:b')
                ifix2_to_ifix1 = opt4gi.remove('convert_ifix2_to_ifix1:b');
            end
            
            if strcmp(fo, 'pca')
                h = fullfile(['step=' num2str(round(opt4gi.remove('step:time')/sec_dt)) 's'], ...
                    ['mode_season=' strrep(opt4gi.remove('mode_season'), '|', '_and_')], ...
                    ['seas_func=' func2str(opt4gi.remove('seas_func'))], ...
                    ['window_spec=' strrep(opt4gi.remove('window_spec:char'), '|', '_')] ...
                    );
            else
                h = fullfile(['step=' num2str(round(opt4gi.remove('step:time')/sec_dt)) 's'], ...
                    ['mode_season=' strrep(opt4gi.remove('mode_season'), '|', '_and_')], ...
                    ['seas_func=' func2str(opt4gi.remove('seas_func'))], ...
                    ['window_spec=' strrep(opt4gi.remove('window_spec:char'), '|', '_')], ...
                    ['filter_outlier=' fo] ...
                    );
            end
            
            if opt4gi.contains_key('norm_fun')
                h = fullfile(h,strcat('norm_fun=',hash(func2str(opt4gi.remove('norm_fun')),'MD5'),'MD5'));
            end
            if exist('cs','var')
                h = fullfile(h,sprintf('context_selection=%s',cs));
            end
            if exist('fuia','var')
                h = fullfile(h,sprintf('force_unique_intraday_auction=%d',fuia));
            end
            if exist('ifix2_to_ifix1','var')
                h = fullfile(h,sprintf('convert_ifix2_to_ifix1=%d',ifix2_to_ifix1));
            end
            if ~opt4gi.isempty()
                lst = opt4gi.get();
                error('set_params_basic_indicator:masterkey:check_args', ...
                    'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
            end
        otherwise
            error('set_params_indicateur_mi:mode', 'mode <%s> unknown', gr);
    end
    return
end

%>
option_cell = {'bins4stop:b', false, 'step:time', opt4gi.get('step:time'),...
    'window:time', opt4gi.get('step:time')};

if opt4gi.contains_key('context_selection:char')
    option_cell = [option_cell,{'context_selection:char',opt4gi.get('context_selection:char')}];
end

if opt4gi.contains_key('force_unique_intraday_auction:b')
    option_cell = [option_cell,{'force_unique_intraday_auction:b',opt4gi.get('force_unique_intraday_auction:b')}];
end
if opt4gi.contains_key('convert_ifix2_to_ifix1:b')
    option_cell = [option_cell,{'convert_ifix2_to_ifix1:b',opt4gi.get('convert_ifix2_to_ifix1:b')}];
end
opt_aux_str = opt2str(options(option_cell));

gr.set_node_param(  'st read dataset', 'RIC:char', opt4gi.get('security_id'), ...
    'trading-destinations:cell', opt4gi.get('trading_destination_id'), ... % TODO check if trading_destination filtering really works...
    'from:dd/mm/yyyy', opt4gi.get('from'), ...
    'to:dd/mm/yyyy', opt4gi.get('to'), ...
    'source:char', ['gi:basic_indicator/' opt_aux_str], ...
    'formula:char', '[{volume},{vwap},{opening_auction},{intraday_auction},{closing_auction},{stop_auction}]',...
    'window:char', opt4gi.get('window_spec:char'), ...
    'step:char', opt4gi.get('from_buffer_frequency'));

gr.set_node_param(  'st seasonnality', 'mode_season', opt4gi.get('mode_season'), ...
    'seas_func', opt4gi.get('seas_func'), 'filter_outlier:char', opt4gi.get('filter_outlier:char'));

if opt4gi.contains_key('norm_fun')
    gr.set_node_param('st seasonnality', 'norm_fun', opt4gi.get('norm_fun'));
end

h = [];
end
