function h = set_params_spread_curve_simple(gr, opt4gi)
% SET_PARAMS_SPREAD_CURVE_SIMPLE
%
% author   : 'dcroize@cheuvreux.com'
% reviewer : 
% version  : '1'
% date     : '28/01/2009'

sdo = {
   'window:time', datenum(0,0,0,0,15,0),...
    'step:time', datenum(0,0,0,0,5,0), ...
    'window:char', 'day|12', ...
    'security_id', 'SEC.NAME', ...
    'from', '01/01/1899', ...
    'to', '01/01/1899', ...
    'trading_destination_id', { NaN}, ...
    };

if nargin == 1
    try
        opt4gi = gr.get_global('set_params_visual_memory');
    catch ME
        if strcmp(ME.identifier, 'options:key') && strcmp(ME.message, 'key <set_params_visual_memory> not available')
            opt4gi = options(sdo);
        else
            rethrow(ME);
        end
    end
    opt4gi.visual();
    gr.set_global('set_params_visual_memory', opt4gi.clone());
else
    opt4gi = options(sdo, opt4gi.get());
end

%< Masterkey mode
if ischar( gr) 
    switch lower(gr)
        case 'masterkey'
            sec_dt = 1/(24*3600);
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
             h = fullfile(['window_spec=' strrep(opt4gi.remove('window:char'), '|', '_')], ...
                ['width=' num2str(round(opt4gi.remove('window:time')/sec_dt)) 's'], ...
                ['step=' num2str(round(opt4gi.remove('step:time')/sec_dt)) 's']);
            if ~opt4gi.isempty()
                lst = opt4gi.get();
                error('set_params_spread_curve_simple:masterkey:check_args', ...
                    'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
            end
        otherwise
            error('set_params_spread_curve_simple:mode', 'mode <%s> unknown', gr);
    end
    return;
end
%>


mwc_  = opt4gi.remove('window:char');
from_ = opt4gi.remove('from');
sec_  =opt4gi.remove('security_id');
td_   = opt4gi.remove('trading_destination_id');
to_   = opt4gi.remove('to');
subgi_call = ['gi:basic_indicator/' opt2str(opt4gi)];
st_log( 'subgi call <%s>...\n', subgi_call);


gr.set_node_param( 'st read dataset', 'RIC:char', sec_ , ...
    'trading-destinations:cell', td_, ...
    'from:dd/mm/yyyy', from_, ...
    'to:dd/mm/yyyy', to_, ...
    'window:char', mwc_, ...
    'source:char', subgi_call, ...
'where_formula:char', '~{auction}');
h = [];

end
