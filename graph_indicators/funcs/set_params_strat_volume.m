function h = set_params_strat_volume(gr, opt4gi)
% TEMPLATE FILE for enrichment set_params in a graph indicator
%
% author   : 'rburgot@cheuvreux.com'
% reviewer : 'clehalle@cheuvreux.com'
% version  : '1'
% date     : '20/08/2008'
specific_default_options = {...
    ... ***********************************************************************
    ...                 PUT YOUR SPECIFIC OPTIONS HERE
    ... ***********************************************************************
    'security_id', 110, ...
    'from', '10/10/2008', ...
    'to', '10/10/2008', ...
    'trading_destination_id', 4 ...
    'step:time', datenum(0,0,0,0,5,0), ...
    'window:time', datenum(0,0,0,0,5,0), ...
    'window_spec:char', 'day|180', ...
    };


if nargin == 1
    try
        opt4gi = gr.get_global('set_params_visual_memory');
    catch ME
        if strcmp(ME.identifier, 'options:key') && strcmp(ME.message, 'key <set_params_visual_memory> not available')
            opt4gi = options(specific_default_options);
        else
            rethrow(ME);
        end
    end
    opt4gi.visual();
    gr.set_global('set_params_visual_memory', opt4gi.clone());
else
    opt4gi = options(specific_default_options, opt4gi.get());
end

%< Masterkey mode
if ischar( gr)
    switch lower(gr)
        case 'masterkey'
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
            sec_dt = 1/(24*3600);
            h = fullfile(['step=' num2str(round(opt4gi.remove('step:time')/sec_dt)) 's'], ...
                ['window=' num2str(round(opt4gi.remove('window:time')/sec_dt)) 's'], ...
                ['window_spec=' strrep(opt4gi.remove('window_spec:char'), '|', '_')]);
            
            if ~opt4gi.isempty()
                lst = opt4gi.get();
                error('set_params_strat_volume:masterkey:check_args', ...
                    'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
            end
            
        otherwise
            error('set_params_strat_volume:mode', 'mode <%s> unknown', gr);
    end
    return
end
%>

ric = opt4gi.remove('security_id');
td = opt4gi.remove('trading_destination_id');
from = opt4gi.remove('from');
to = opt4gi.remove('to');
opt_str = opt2str(options({'bins4stop:b', false, 'step:time', opt4gi.get('step:time'), 'window:time', opt4gi.get('window:time')}));

gr.set_node_param(  'read dataset', ...
    'RIC:char', ric, ...
    'trading-destinations:cell', td, ...
    'from:dd/mm/yyyy', from, ...
    'to:dd/mm/yyyy', to, ...
    'source:char', ['gi:basic_indicator/' opt_str], ...
    'window:char', opt4gi.get('window_spec:char'));



%**************************************************************************
% SET YOUR SPECIFIC OPTIONS IN THE RELEVANT ST_NODES : HERE AND JUST ABOVE
%       IN ST_READ_DATASET IF NEEDED
%**************************************************************************

h = [];
end
