function rslt = set_params_<<GI_NAME>>(gr, opt4gi)
% SET_PARAMS_<<U_GI_NAME>> - function to set parameters in the graph, 
% made according to file : <<XML_FILE_NAME>>
%
% author   : <<AUTHOR>>
% reviewer : <<REVIEWER>>
% version  : '1'
% date     : <<DATE>>

sdo = {...
    <<SDO>>
    'security_id', NaN, ...
    'from', '01/01/1899', ...
    'to', '01/01/1899', ...
    'trading_destination_id', {NaN}...
    };

if nargin == 1
    try
        opt4gi = gr.get_global('set_params_visual_memory');
    catch ME
        if strcmp(ME.identifier, 'options:key') && strcmp(ME.message, 'key <set_params_visual_memory> not available')
            opt4gi = options(sdo);
        else
            rethrow(ME);
        end
    end
    opt4gi.visual();
    pause(0.1); % weird but very important, do not delete!!!!
    gr.set_global('set_params_visual_memory', opt4gi.clone());
else
    opt4gi = options(sdo, opt4gi.get());
end

%< Masterkey mode
if ischar( gr) 
    switch lower(gr)
        case 'masterkey'
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
            <<MASTERKEY>>
            if ~opt4gi.isempty()
                lst = opt4gi.get();
                error('set_params_<<GI_NAME>>:masterkey:check_args', ...
                    'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
            end
        otherwise
            error('set_params_<<GI_NAME>>:mode', 'mode <%s> unknown', gr);
    end
    return;
end
%>

<<SET_PARAMS>>

rslt = [];
end