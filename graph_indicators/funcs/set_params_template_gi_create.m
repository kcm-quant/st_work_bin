function h = set_params_<<GI_NAME>>(gr, opt4gi)
% TEMPLATE FILE for enrichment set_params in a graph indicator
%
% author   : 'rburgot@cheuvreux.com'
% reviewer : 'clehalle@cheuvreux.com'
% version  : '1'
% date     : '20/08/2008'
specific_default_options = {...
... ***********************************************************************
...                 PUT YOUR SPECIFIC OPTIONS HERE
... ***********************************************************************
    'security_id', NaN, ...
    'from', '01/01/1899', ...
    'to', '01/01/1899', ...
    'trading_destination_id', {NaN}...
    };


if nargin == 1
    try
        opt4gi = gr.get_global('set_params_visual_memory');
    catch ME
        if strcmp(ME.identifier, 'options:key') && strcmp(ME.message, 'key <set_params_visual_memory> not available')
            opt4gi = options(specific_default_options);
        else
            rethrow(ME);
        end
    end
    opt4gi.visual();
    pause(0.1); % weird but very important, do not delete!!!!
    gr.set_global('set_params_visual_memory', opt4gi.clone());
else
    opt4gi = options(specific_default_options, opt4gi.get());
end

%< Masterkey mode
if ischar( gr) 
    switch lower(gr)
        case 'masterkey'
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
            if opt4gi.isempty()
                h = '';
            else
                h = hash(opt2str(opt4gi), 'MD5');
... ***********************************************************************
...                 REMOVE YOUR SPECIFIC OPTIONS FROM opt4gi HERE
... ***********************************************************************
                if ~opt4gi.isempty()
                    lst = opt4gi.get();
                    error('set_params_<<GI_NAME>>:masterkey:check_args', ...
                        'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
                end
            end
        otherwise
            error('set_params_<<GI_NAME>>:mode', 'mode <%s> unknown', gr);
    end
    return
end
%>

gr.set_node_param(  'st read dataset', 'RIC:char', opt4gi.remove('security_id'), ...
    'trading-destinations:cell', opt4gi.remove('trading_destination_id'), ...
    'from:dd/mm/yyyy', opt4gi.remove('from'), ...
    'to:dd/mm/yyyy', opt4gi.remove('to')); % You might have something to do here as explained just below

%**************************************************************************
% SET YOUR SPECIFIC OPTIONS IN THE RELEVANT ST_NODES : HERE AND JUST ABOVE
%       IN ST_READ_DATASET IF NEEDED
%**************************************************************************

h = [];
end