function h = set_params_volatility2fpm(gr, opt4gi)
% author   : 'rburgot@cheuvreux.com'
% reviewer : 'clehalle@cheuvreux.com'
% version  : '1'
% date     : '20/08/2008'
specific_default_options = { ...
    'quant', [0.9; 0.8; 0.7; 0.6; 0.5], ...
    'time_scales', datenum(0,0,0,0,[5,10,20,40],0), ... => parameter for the whole graph
    'step:time', datenum(0,0,0,0,0,30), ... => parameter for the whole graph
    'vol_width:time', datenum(0,0,0,0,15,0), ...=> parameter for the whole graph
    'q_step', 0.02, ...
    'smooth_volatility', 0, ...
    'window:char', 'day|14',...
    'security_id', NaN, ...
    'from', '01/01/1899', ...
    'to', '01/01/1899', ...
    'trading_destination_id', {NaN}...
    };
if nargin == 1
    try
        opt4gi = gr.get_global('set_params_visual_memory');
    catch ME
        if strcmp(ME.identifier, 'options:key') && strcmp(ME.message, 'key <set_params_visual_memory> not available')
            opt4gi = options(specific_default_options);
        else
            rethrow(ME);
        end
    end
    opt4gi.visual();
    gr.set_global('set_params_visual_memory', opt4gi.clone());
else
    opt4gi = options(specific_default_options, opt4gi.get());
end

%< Masterkey mode
if ischar( gr)
    switch lower(gr)
        case 'masterkey'
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
            h = fullfile(...
                hash(convs('safe_str', {opt4gi.remove('quant'),...
                opt4gi.remove('time_scales'),...
                opt4gi.remove('step:time'),...
                opt4gi.remove('vol_width:time'),...
                opt4gi.remove('q_step'),...
                opt4gi.remove('smooth_volatility'),...
                opt4gi.remove('window:char')}), 'MD5'));
            if ~opt4gi.isempty()
                lst = opt4gi.get();
                error('set_params_basic_indicator:masterkey:check_args', ...
                    'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
            end
        otherwise
            error('set_params_indicateur_mi:mode', 'mode <%s> unknown', gr);
    end
    return
end
%>

step = opt4gi.get('step:time');
v_width = opt4gi.get('vol_width:time');

gr.set_node_param(  'st read dataset 1', 'RIC:char', opt4gi.get('security_id'), ...
    'trading-destinations:cell', opt4gi.get('trading_destination_id'), ...
    'from:dd/mm/yyyy', opt4gi.get('from'), ...
    'to:dd/mm/yyyy', opt4gi.get('to'), ...
    'window:char', opt4gi.get('window:char'), ...
    'source:char', ['gi:basic_indicator/' opt2str(options({'window:time', v_width, 'step:time', step}))]);

gr.set_node_param(  'st read dataset 2', 'RIC:char', opt4gi.remove('security_id'), ...
    'trading-destinations:cell', opt4gi.remove('trading_destination_id'), ...
    'from:dd/mm/yyyy', opt4gi.remove('from'), ...
    'to:dd/mm/yyyy', opt4gi.remove('to'), ...
    'window:char', opt4gi.remove('window:char'), ...
    'source:char', ['gi:basic_indicator/' opt2str(options({'window:time', step, 'step:time', step}))]);

lst = opt4gi.get();
gr.set_node_param(  'st volatility2fpm', lst{:}, 'plot:b', 0);
h = [];

end