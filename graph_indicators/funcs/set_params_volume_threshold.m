function h = set_params_volume_threshold(gr, opt4gi)
% SET_PARAMS_VOLUME_THRESHOLD - set_param enrichment for
%      volume_threshold.mat
%
%  example:
%    gr= st_grapher('load', 'volume_threshold');
%    set_params_volume_threshold( gr)
%    set_params_volume_threshold( gr, options( { 'security_id', 'FTE.PA', 'trading_destination_id', { 'MAIN' }, 'from', '01/06/2008', 'to', '12/06/2008', 'window:char', 'day|16', 'remove-date-from-tickdb', { '08/06/2008', '09/06/2008'}}) )
%
%    set_params_volume_threshold( 'masterkey', options( { 'security_id', 'FTE.PA', 'trading_destination_id', { 'MAIN' }, 'from', '01/06/2008', 'to', '12/06/2008', 'window:char', 'day|16'}) )
%    read_dataset('gi:volume_threshold', 'security_id', 'FTE.PA', 'trading-destinations', { 'MAIN' }, 'from', '01/06/2008', 'to', '02/06/2008' )
%
% , 'window:char', 'day|16', 'remove-date-from-tickdb', { '08/06/2008', '09/06/2008'}

% author   : 'clehalle@cheuvreux.com'
% reviewer : 'edarchimbaud@cheuvreux.com'
% version  : '1'
% date     : '23/10/2008'
specific_default_options = { ...
    'truncation-quantile', 0.05, ...
    'threshold-quantile' , 0.25, ...
    'remove-date-from-tickdb', {}, ...
    'window:char', 'day|14',... % options obligatoires
    'security_id', NaN, ...
    'from', '01/01/1899', ...
    'to'  , '01/01/1899', ...
    'trading_destination_id', {NaN} ...
    };
if nargin == 1
    try
        opt4gi = gr.get_global('set_params_visual_memory');
    catch ME
        if strcmp(ME.identifier, 'options:key') && strcmp(ME.message, 'key <set_params_visual_memory> not available')
            opt4gi = options(specific_default_options);
        else
            rethrow(ME);
        end
    end
    opt4gi.visual();
    gr.set_global('set_params_visual_memory', opt4gi.clone());
else
    opt4gi = options(specific_default_options, opt4gi.get());
end

%< Masterkey mode
if ischar( gr)
    switch lower(gr)
        case 'masterkey'
            opt4gi.remove('security_id');
            opt4gi.remove('trading_destination_id');
            opt4gi.remove('from');
            opt4gi.remove('to');
            rm_dates = opt4gi.remove('remove-date-from-tickdb');
            if isempty(rm_dates)
                rm_dates = '_none_';
            elseif iscell( rm_dates)
                rm_dates = sprintf('%s;', rm_dates{:});
                rm_dates(end) ='';
            end
            h = fullfile( ['truncation_quantile=' num2str( opt4gi.remove('truncation-quantile'))], ...
                ['threshold_quantile=' num2str( opt4gi.remove('threshold-quantile'))], ...
                ['remove_dates_from_tickdb=', rm_dates ], ...
                ['window_char=' strrep( opt4gi.remove('window:char'), '|', '_')]) ;
            if ~opt4gi.isempty()
                lst = opt4gi.get();
                error('set_params_basic_indicator:masterkey:check_args', ...
                    'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
            end
        otherwise
            error('set_params_indicateur_mi:mode', 'mode <%s> unknown', gr);
    end
    return
end
%>
            
gr.set_node_param(  'tickdb', ...
    'RIC:char', opt4gi.remove('security_id'), ...
    'trading-destinations:cell', opt4gi.remove('trading_destination_id'), ...
    'from:dd/mm/yyyy', opt4gi.remove('from'), ...
    'to:dd/mm/yyyy', opt4gi.remove('to'), ...
    'window:char', opt4gi.remove('window:char'));
rm_dates = opt4gi.remove('remove-date-from-tickdb');
if isempty(rm_dates)
    gr.set_node_param(  'tickdb', 'source:char', 'btickdb');
else
    if iscell( rm_dates)
        rm_dates = sprintf('%s;', rm_dates{:});
        rm_dates = ['"', rm_dates(1:end-1)];
    end
    gr.set_node_param(  'tickdb', 'source:char', [ 'btickdb/remove�' rm_dates ]);
end

gr.set_node_param(  'learn max', ...
    'quantile-level', opt4gi.remove('truncation-quantile'), ...
    'quantile:double', opt4gi.remove('threshold-quantile'), ...
    'learn-mode', 'exec' );

h = [];

end