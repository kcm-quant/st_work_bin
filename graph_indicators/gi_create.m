function gi_create()
% GI_CREATE - interface pour la cr�ation d'un indicateur graphique 
%               (graph_indicators) il est fortement d�conseill� de cr�er un
%               indicateur graphique sans passer par cette fonction
%
% ex : gi_create
%
%   les indicateurs graphiques peuvent �tre r�cup�r�s gr�ce � read_dataset
%   et bien s�r st_read_dataset
%
% See also read_dataset st_read_dataset

%<* choose a name for your indicator
base_dir = fullfile(getenv('st_work'), 'bin', 'graph_indicators'); % TODO : more complex if some in libs and other somewhere else
[gi_name, fname] = GI_CHOOSE_NAME(base_dir);
if isempty(gi_name) || isempty(fname)
    return;
end
%>*

%<* Choose the features you want to add to your indicator
GI_CHOOSE_FEATURES(base_dir, gi_name, fname);
%>*

end

% helps to choose a name for your indicator, by checking if that name does not already exist 
function [gi_name, fname] = GI_CHOOSE_NAME(base_dir)
base_dir = fullfile(base_dir, 'projects');
forbidden_characters = '\/:*?"<>|��';
MY_WARNDLG(['Please be sure that you just get the whole content of $st_work$'...
    '\methodo\graph_indicators\projects from Surround']);
gi_name_is_correct = false;
has_forbidden_characters = false;
while ~gi_name_is_correct
    gi_name = inputdlg('Choose a name for your indicator');
    if isempty(gi_name)
        fname = [];
        return;
    end
    gi_name =  gi_name{:};
    for i = 1 : length(forbidden_characters)
        if ~isempty(strfind(gi_name, forbidden_characters(i)))
            has_forbidden_characters = true;
            break;
        end
    end
    if ~has_forbidden_characters
        if length(gi_name) > 3 && strcmpi(gi_name(end-3:end), '.mat')
            gi_name = gi_name(1:end-4);
        end
        fname = fullfile(base_dir, [gi_name '.mat']);
        if exist(fname, 'file')
            REQ_USER_ACTION(errordlg('An indicator with that name already exists, please choose another name'));
        else
            gi_name_is_correct = true;
        end
    else
        REQ_USER_ACTION(errordlg(sprintf('An indicator name cannot contain any of the following characters : %s', forbidden_characters)));
        has_forbidden_characters = false;
    end
end
end


function GI_CHOOSE_FEATURES(base_dir, gi_name, fname)
choose_list = {'No', ...                                                                            case 1
    'I have a function to insert (which takes a st_data as an input and returns a st_data)', ...    case 2
    'I have a box to insert', ...                                                                   case 3
    'I want a template for intraday computations', ...                                              case 4
    };
mode = listdlg('Name', 'Do you want something more?', 'ListString', choose_list, ...
    'ListSize', [300 150], 'SelectionMode','single');
if isempty(mode)
    return;
end
gr = st_grapher;
%<* Adding the necessary box : st_read_dataset
id1 = gr.add_node( 'st_function', 'st_read_dataset', 'title', 'st read dataset');
node1 = gr.get_node('st read dataset');
node1.set('position', [20 40 -5 5]);
gr.set_global('root', id1);
%>*
switch mode
    % TODO more than this, :
    % help user to specify the data he wants
    case 1
        % nothing to do
    case {2, 3, 4}
        if mode == 2
            node_name = inputdlg('What''s the name of your function?');
            if isempty(node_name)
                return;
            end
            node_name = strrep(node_name{:}, '_', ' ');
            function_name = 'st_generic';
        elseif mode == 3
            function_name = inputdlg('What''s the name of your box?');
            if isempty(function_name)
                return;
            end
            function_name = function_name{:};
            node_name = strrep(function_name, '_', ' ');
        elseif mode == 4
            function_name = ['st_intraday_' gi_name ];
            st_intraday_fid = fopen(fullfile(base_dir, 'boxes', [function_name '.m']), 'w');
            template_file(fullfile(base_dir, 'boxes', 'st_intraday_template.m'), ...
                options({'GI_NAME', gi_name}), ...
                st_intraday_fid);
            fclose(st_intraday_fid);
            node_name = strrep(function_name, '_', ' ');
            edit(fullfile(base_dir, 'boxes', [function_name '.m']));
        end
        id = gr.add_node( 'st_function', function_name, 'title', node_name);
        node = gr.get_node(node_name);
        node.set('position', node1.get('position') + [30 30 0 0]);
        gr.init_node(id);
        gr.set_global('target', id);
        if mode == 2
            gr.set_node_param(node_name, 'mode:char', 'st_data');
            gr.set_node_param(node_name, 'func:@', str2func(function_name));
        end
        gr.connect(id1, 1, id, 1);
    otherwise
        error('win_graph:GI_CHOOSE_FEATURES', 'this feature seems not to be implemented yet');
end
gr.set_global('filename', fname);
gr.set_global('title', gi_name);
%<* set params template => necessary feature
set_params_func = ['set_params_' gi_name];
set_params_fid = fopen(fullfile(base_dir, 'funcs', [set_params_func '.m']), 'w');
if mode == 4
    template_file(fullfile(base_dir, 'funcs', 'set_params_template_intraday.m'), ...
        options({'GI_NAME', gi_name, 'NODE_NAME', ['''' node_name '''']}), ...
        set_params_fid);
else
    template_file(fullfile(base_dir, 'funcs', 'set_params_template_gi_create.m'), ...
        options({'GI_NAME', gi_name}), ...
        set_params_fid);
end
fclose(set_params_fid);
gr.set_enrichment('set params', 'fun', str2func(set_params_func), ...
    'position', [20 50 -10 -15], 'display', true);
gr.set_enrichment('delete bufferized values', 'fun', DELETE_GI_FUNC(gi_name), ...
    'position', [20 50 -17 -22], 'display', true);
%>
gr.init();
% if mode == 4
%     rehash; % this line and the next one may seem strange but they are necessary to avoid a matlab bug (or feature )
%     clear([set_params_func '.m']);
%     gr.run_enrichment('set params');
% end
save( fname, 'gr');
win_graph('show', gr);
end

function MY_WARNDLG(msg)
REQ_USER_ACTION(warndlg(msg, 'Very important message for your safety'));
end

function REQ_USER_ACTION(h)
while ishandle(h)
    pause(0.2);
end
end

% A function (for an enrichment) which enables to delete the
% bufferized graph indicator values
function fun = DELETE_GI_FUNC(gi_name)
fun = @func;
    function h = func(varargin)
        MY_WARNDLG('Are you really sure that you want to delete all the data that you bufferized for this indicator?');
        try
            dir_name = fullfile(getenv('st_repository'), 'get_gi', gi_name);
            rmdir(dir_name, 's');
            st_log('%s deleted\n', dir_name);
        catch ME
            mess = ME.message;
            if length(mess) > 18 && strcmp(mess(end-18:end), 'is not a directory.')
                st_log('Nothing to delete\n');
            else
                error('DELETE_GI_FUNC:exec', mess);
            end
        end
        h = [];
    end
end