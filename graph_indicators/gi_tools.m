function rslt = gi_tools(mode, varargin)
% GI_TOOLS - Outil facilitant la cr�ation des graph indicator
%
%
%
% gi_tools('xml2set_params', 'test_new_xml2set_params')
%
% La suite de l'aide fait r�f�rence � de vieux modes qui ne devraient plus
% �tre utilis�s :
%
% deux exemples sans cons�quences :
% gi_tools('xml2set_params_old', 'xml_name', 'test_xml2set_params.xml', 'gi_name', 'estimateur_test');
% gi_tools('xml2set_params_old', 'xml_name', 'test_xml2set_params_2.xml', 'gi_name', 'estimateur_test');
%
% ATTENTION la commande suivante est dangeureuse!!!!!!!!
% gi_tools('test-xml2set_params_old');
%
% data = read_dataset('gi:eta_indicator', 'security_id', 110, 'from', '14/06/2008', 'to', '14/07/2008', 'trading-destinations', {});
%
% See also gi_create

switch mode
    case 'xml2set_params'
        rslt = {};
        xml_o = xmltools([varargin{1} '.xml']);
        xml_s_interpreter('set_params', xml_o, 'gi_name', varargin{1});
% A la suite, de vieux modes qui ne peuvent �tre utilis�s que si'lon
% r�cup�re la version 3 (sur surround, version ant�rieure au 19/11/2008) de xml_s_interpreter. Pour les
% fichiers xml, demandez � Romain
    case 'xml2set_params_old'
        rslt = {};
        opt = options({'xml_name', '', 'gi_name', '', 'add_small_test', true}, varargin);
        gi_name = opt.get('gi_name');
        xml_name = opt.get('xml_name');
        if isempty(gi_name)
            error('gi_tools:xml2set_params_old:check_args', 'estimator_name has to be specified');
        elseif isempty(xml_name)
            error('gi_tools:xml2set_params_old:check_args', 'xml_name has to be specified');
        end
        xml_o = xmltools(xml_name);
        lst = opt.get();
        xml_s_interpreter(xml_o, lst{:});
    case 'test-xml2set_params_old'
        %         answer = questdlg('This test will modify some of your mfiles and data. Are you sure that you know what you are doing?','Message for your safety','Yes','No','No');
        %         if isempty(answer) || ~strcmp(answer, 'Yes')
        %             st_log('Ok then we don''t do anything\n');
        %             return;
        %         end
        
%         gi_tools('test-unitaire-xml2set_params_old', {['gi:basic_indicator/' opt2str(options({'t_acc:time', '�datenum(0,0,0,2,0,0)', 'step:time', '�datenum(0,0,0,0,0,30)', 'window:time', '�datenum(0,0,0,4,0,0)'}))], 'security_id', 110, 'from', '14/07/2008', 'to', '14/07/2008', 'trading-destinations', {}});
%         gi_tools('test-unitaire-xml2set_params_old', {['gi:seasonnality/' opt2str(options({'step:time', '�datenum(0,0,0,0,1,0)', 'window_spec:char', 'day|30'}))], 'security_id', 110, 'from', '14/07/2008', 'to', '14/07/2008', 'trading-destinations', {}});
%         gi_tools('test-unitaire-xml2set_params_old', {'gi:eta_indicator', 'security_id', 110, 'from', '14/07/2008', 'to', '14/07/2008', 'trading-destinations', {}});
%         gi_tools('test-unitaire-xml2set_params_old', {'gi:intraday_scoring/history�"day|30�nb-dim�5', 'security_id', 'FTE.PA', 'from', '01/09/2008', 'to', '01/09/2008', 'trading-destinations', {}});
%         gi_tools('test-unitaire-xml2set_params_old', {['gi:volatility2fpm/' opt2str(options({'quant', [0.9; 0.7; 0.5], 'time_scales', datenum(0,0,0,0,[10,20,40],0), 'step:time', datenum(0,0,0,0,1,0),'vol_width:time', datenum(0,0,0,0,20,0), 'q_step', 0.05, 'smooth_volatility', 1, 'window:char', 'day|20'}))], 'security_id', 110, 'from', '14/07/2008', 'to', '14/07/2008', 'trading-destinations', {}}, false);

%         gi_tools('test-unitaire-xml2set_params_old', {'gi:intraday_description/history�"day|30', 'security_id', 'FTE.PA', 'from', '14/07/2008', 'to', '14/07/2008', 'trading-destinations', {}});
        gi_tools('test-unitaire-xml2set_params_old', {'gi:process4market_impact_01b/window:char�"day|60', 'security_id', 'FTE.PA', 'from', '14/07/2008', 'to', '14/07/2008', 'trading-destinations', {}});
        read_dataset('gi:process4market_impact_01b/window:char�"day|60', 'security_id', 'FTE.PA', 'from', '14/07/2008', 'to', '14/07/2008', 'trading-destinations', {});
    case 'test-unitaire-xml2set_params_old'
        idx1 = strfind(varargin{1}{1}, ':');
        idx1 = idx1(1) + 1;
        idx2 = strfind(varargin{1}{1}, '/');
        if isempty(idx2)
            idx2 = length(varargin{1}{1});
        else
            idx2 = idx2(1) -1;
        end
        estimator_name = varargin{1}{1}(idx1:idx2);
        set_param_func_name = ['set_params_' estimator_name];
        set_param_func_filename = [set_param_func_name '.m'];
        
        % On r�cup�re le r�pertoire de bufferisation afin d'aller le
        % renommer, pour �tre sur que les donn�es que nous avons ont bien
        % �t� calcul�es par les codes actuellement pr�sents sur la machine
        buffer_dir = read_dataset(varargin{1}{:}, 'from_buffer:cmd', 'get_dir_buffer');
        make_dir_old(buffer_dir);
        
        st_log('Testing xml2set_params_old mode of gi_tools for estimator : <%s>\n',estimator_name);
        % Trois points de comparaison
        % 1) On r�cup�re la clef g�n�r�e par l'ancien set_params pour les
        % options par d�faut
        def_key = feval(set_param_func_name, 'masterkey', options());
        
        % 2) On r�cup�re la clef g�n�r�e par l'ancien set_params pour les
        % options sp�cifi�es dans ce cas pr�cis
        speckey = feval(set_param_func_name, 'masterkey', str2opt(varargin{1}{1}(idx2+2:end)));
        
        % 3) On r�cup�re des donn�es avec l'ancien set_params
        data = read_dataset(varargin{1}{:});
        
        % On renomme l'ancien set_params, on v�rifie qu'il n'est plus dans
        % le path, et on construit le nouveau
        
        f_path = fullfile(getenv('st_work'), 'bin', 'graph_indicators', 'funcs', set_param_func_filename);
        make_dir_old(f_path);
        if ~isempty(which(set_param_func_filename))
            error('gi_tools:test-unitaire-xml2set_params_old', 'The file <%s> should not exist!!!', which(set_param_func_filename));
        end
        gi_tools('xml2set_params_old', 'xml_name', [estimator_name '_set_params.xml'], 'gi_name', estimator_name, 'add_small_test', false);
        
        % 1) On r�cup�re la clef g�n�r�e par le nouveau set_params pour les
        % options par d�faut
        if ~strcmp(def_key, feval(set_param_func_name, 'masterkey', options()))
            error('On a pas les m�mes clefs pour les options par d�faut')
        end
        
        % 2) On r�cup�re la clef g�n�r�e par le nouveau set_params pour les
        % options sp�cifi�es dans ce cas pr�cis
        if ~strcmp(speckey, feval(set_param_func_name, 'masterkey', str2opt(varargin{1}{1}(idx2+2:end))))
            error('On a pas les m�mes clefrs pour les options sp�cifi�es ici')
        end
        
        % on efface le r�pertoire les plus r�cent correspondant � la clef
        % fournie
        rmdir(buffer_dir, 's'); % cette fois on peut effacer puisque ce sont les donn�es que nous avons calcul�s
        
        % 3) On r�cup�re des donn�es avec le nouveau set_params
        data2 = read_dataset(varargin{1}{:});
        
        % comparaison des data obtenus
        tmp = data.value(1:end);
        idx = ~isfinite(tmp);
        tmp(idx) = [];
        tmp2 = data2.value(1:end);
        tmp2(idx) = [];
        if ~all(tmp == tmp2)
            error('Le recalcul ne donne pas les m�mes donn�es');
        end
        st_log('Test of xml2set_params_old mode of gi_tools for estimator : <%s> PASSED\n', estimator_name);
        
        % On remet les trucs qu'on a chang� � leur place
        % on efface les donn�es que l'on vient de calculer
        rmdir(read_dataset(varargin{1}{:}, 'from_buffer:cmd', 'get_dir_buffer'), 's');
        % on restore l'ancien fichier set_param_estimator_name.m
        restore_old_dir(f_path);
        % on remet � leur place les donn�es anciennement bufferis�es
        restore_old_dir(buffer_dir);
    otherwise
        error('gi_tools:choose_mode', 'Mode : <%s> unknown', mode);
end
end

function make_dir_old(dir_)
if exist(dir_)
    movefile(dir_, [dir_ '_old'], 'f');
end
end


function restore_old_dir(dir_)
if exist(dir_)
    movefile([dir_ '_old'], dir_, 'f');
end
end