function out = check_doublet_histo_association

global st_version
quant = st_version.bases.quant;

res = exec_sql('QUANT',['select rank,isnull(security_id,-1),isnull(trading_destination_id,-1),',...
    ' estimator_id,isnull(varargin,''''),job_id,isnull(beg_date,''3000-01-01''),isnull(end_date,''3000-01-01'')',...
    ' from ',quant,'..histo_association']);

beg_date = datenum(res(:,end-1),'yyyy-mm-dd');
end_date = datenum(res(:,end),'yyyy-mm-dd');

[~,~,idx_vararg] = unique(res(:,5));
[un,idx_un,idx] = unique([cell2mat(res(:,1:4)),idx_vararg],'rows');
beg_date = accumarray(idx,beg_date,[],@(x){x});
end_date = accumarray(idx,end_date,[],@(x){x});
idx_doublons = [];
for i = 1:length(un)
    [~,I] = sort([end_date{i};beg_date{i}]);
    temp = [-ones(length(end_date{i}),1);ones(length(beg_date{i}),1)];
    if any(cumsum(temp(I)) > 1)
        idx_doublons(end+1) = i;
    end
end
out = res(idx_un,1:5);
out = out(idx_doublons,:);