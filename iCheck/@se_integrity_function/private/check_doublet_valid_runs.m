function out = check_doublet_valid_runs

global st_version
quant = st_version.bases.quant;

run_id = cell2mat(exec_sql('QUANT',['select run_id from ',quant,'..valid_runs']));
res1 = exec_sql('QUANT',sprintf(['select job_id, estimator_id, context_id,'...
    ' isnull(validation_date,''3000-01-01''), isnull(devalidation_date,''3000-01-01'')',...
    ' from ',quant,'..valid_runs',...
    ' where run_id < %d'],median(run_id)));
res2 = exec_sql('QUANT',sprintf(['select job_id, estimator_id, context_id,'...
    ' isnull(validation_date,''3000-01-01''), isnull(devalidation_date,''3000-01-01'')',...
    ' from ',quant,'..valid_runs',...
    ' where run_id >= %d'],median(run_id)));
res = [res1;res2];

val_date = datenum(res(:,end-1),'yyyy-mm-dd');
de_val_date = datenum(res(:,end),'yyyy-mm-dd');

[un,idx_un,idx] = unique(cell2mat(res(:,1:3)),'rows');
val_date = accumarray(idx,val_date,[],@(x){x});
de_val_date = accumarray(idx,de_val_date,[],@(x){x});
idx_doublons = [];
for i = 1:length(un)
    [~,I] = sort([de_val_date{i};val_date{i}]);
    temp = [-ones(length(de_val_date{i}),1);ones(length(val_date{i}),1)];
    if any(cumsum(temp(I)) > 1)
        idx_doublons(end+1) = i;
    end
end
out = res(idx_un,1:3);
out = out(idx_doublons,:);