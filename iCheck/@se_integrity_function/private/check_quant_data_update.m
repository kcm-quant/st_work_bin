function out = check_quant_data_update()

global st_version;
base_quant_data = st_version.bases.quant_data;
base_quant = st_version.bases.quant;

out = {};

res = exec_sql('QUANT', sprintf( [ 
'SELECT distinct qr.estimator_id , est.estimator_name , est.owner ' ...
'FROM ' ...
'%s.. quant_reference qr, %s..estimator_runs er , %s..estimator est ' ... 
'WHERE ' ...
'qr.run_id=er.run_id and er.is_valid=0 AND ' ...
'est.estimator_id =qr.estimator_id '] ,base_quant_data,base_quant, base_quant ));


if ~isempty(res)
    data = st_data('from-cell', ' ', {'estimator_id' ,'estimator_name' ,'owner'}, res);
    
    estimator = unique (st_data('cols',data,'estimator_id' ));
    out = cell(length(estimator),2);
    
    for i= 1: length(estimator)
        tmp= st_data('where',data , sprintf('{estimator_id} == %d' ,estimator(i)));
        book1 = codebook( 'get', tmp.codebook, 'owner');
        book2 = codebook( 'get', tmp.codebook, 'estimator_name');
        out(i , : ) = { book2{i} , book1 } ;
    end
end


end