function out = check_update_perimeter()

global st_version;
q_base = st_version.bases.quant;
dir_path = fullfile(getenv('st_repository'), 'log', 'perim_update');
cd (dir_path)

perimeter_name = exec_sql('QUANT' , sprintf('SELECT perimeter_name from %s..perimeter' , q_base) );
out = {};

for i = 1 :length(perimeter_name)
    fid = fopen(sprintf('%s_update.txt' , strrep(perimeter_name{i},' ','_')));
    if fid>0
        C = textscan(fid, '%[^\n]');
        is_not_ok = sum(cellfun(@(c)~isempty(strfind(c,'ERROR')), C{1}));
        fclose(fid);
        if is_not_ok
            out(end+1 , : ) = {strrep(perimeter_name{i},' ','_')};
        end
    end
end
end


