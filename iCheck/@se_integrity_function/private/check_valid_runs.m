function out = check_valid_runs

global st_version
quant = st_version.bases.quant;

run_id = exec_sql('QUANT',['select run_id from ',quant,'..estimator_runs where is_valid = 1']);
valid_run_id = exec_sql('QUANT',['select run_id from ',quant,'..valid_runs',...
    ' where devalidation_date is null']);

vals_run_id = cell2mat(run_id);
vals_valid_run_id = cell2mat(valid_run_id);
out1 = run_id(~ismember(vals_run_id,vals_valid_run_id));
out2 = valid_run_id(~ismember(vals_valid_run_id,vals_run_id));
out = {out1,out2};