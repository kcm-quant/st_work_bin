function out = icheck_association_valid

global st_version
quant = st_version.bases.quant;

% Les nouvelles associations historis�es sont-elles des job valides ?

% Dates de la derni�re session
% res = exec_sql('QUANT',['select id,beg_time,beg_date from ',quant,'..session']);
% res(any(cellfun(@(x)strcmp(x,'null'),res),2),:) = [];
% res = [cell2mat(res(:,1)),abs(mod(datenum(res(:,2),'HH:MM:SS'),1)-datenum(0,0,0,20,0,0))<datenum(0,0,0,0,1,0),datenum(res(:,3),'yyyy-mm-dd')];
% res(res(:,2)==0,:) = [];
% id_max = max(res(:,1));
% res_dates = exec_sql('QUANT',sprintf('select beg_date,end_date from quant..session where id >= %d',id_max));
% res_dates(cellfun(@(x)strcmp(x,'null'),res_dates)) = [];
% dates = unique(res_dates);
% 
% 
% date_str = regexprep(sprintf('''%s'',',dates{:}),',$','');
% job_id_assoc = exec_sql('QUANT',sprintf(['select distinct job_id from ',quant,'..histo_association',...
%     ' where beg_date in (%s)',...
%     ' and end_date is null'],date_str));

job_id_assoc = exec_sql('QUANT',['select distinct job_id from ',quant,'..histo_association',...
    ' where end_date is null']);
job_str = regexprep(sprintf('%d,',job_id_assoc{:}),',$','');
job_id_valid = exec_sql('QUANT',sprintf('select job_id from ',quant,'..valid_runs where job_id in (%s) and devalidation_date is null',job_str));

out  = job_id_assoc(~ismember(cell2mat(job_id_assoc),cell2mat(job_id_valid)));