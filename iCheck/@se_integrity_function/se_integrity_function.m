function output_value = se_integrity_function ( mode, varargin )
	try
		fhandle = str2func ( mode );
		output_value = fhandle ( varargin{:} );
	catch e
		warning ( sprintf ( 'se_integrity_function:%s', mode ), 'Error in call to function ''%s'' :\n %s\n', mode, e.message );
		output_value = {};
	end