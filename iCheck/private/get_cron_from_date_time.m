function cron_regexp = get_cron_from_date_time ( datexp, timexp )
	if ( isempty ( datexp ) )
		datexp = 'weekdays';
	end
	if ( isempty ( timexp ) )
		timexp = 'morning';
	end
	
	switch ( lower ( datexp ) )
		case 'weekdays'
			datexp = '(Mon|Tue|Wed|Thu|Fri) \d{2}/\d{2}/\d{4}';
		case 'weekends'
			datexp = '(Sat|Sun) \d{2}/\d{2}/\d{4}';
		case 'everyday'
			datexp = '(Mon|Tue|Wed|Thu|Fri|Sat|Sun) \d{2}/\d{2}/\d{4}';
		case { 'monday', 'mondays' }
			datexp = 'Mon \d{2}/\d{2}/\d{4}';
		case { 'tuesday', 'tuesdays' }
			datexp = 'Tue \d{2}/\d{2}/\d{4}';
		case { 'wednesday', 'wednesdays' }
			datexp = 'Wed \d{2}/\d{2}/\d{4}';
		case { 'thursday', 'thursdays' }
			datexp = 'Thu \d{2}/\d{2}/\d{4}';
		case { 'friday', 'fridays' }
			datexp = 'Fri \d{2}/\d{2}/\d{4}';
		case { 'saturday', 'saturdays' }
			datexp = 'Sat \d{2}/\d{2}/\d{4}';
		case { 'sunday', 'sundays' }
			datexp = 'Sun \d{2}/\d{2}/\d{4}';
	end
	
	switch ( lower ( timexp ) )
		case 'morning'
			timexp = '08:00';
		case 'evening'
			timexp = '20:00';
	end
	
	cron_regexp = [ datexp ' ' timexp ];
