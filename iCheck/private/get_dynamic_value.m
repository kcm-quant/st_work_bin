function dynamic_property = get_dynamic_value ( property, check )
%	try
		dynamic_property = struct ( 'instance', [], 'value', [] );
		dynamic_property.instance = parse_for_expressions ( property.value, check );
		switch ( lower ( property.type ) )
			case 'sql'
				output_value = exec_sql ( 'QUANT', dynamic_property.instance );
			case 'matlab'
				%the output_value variable must be used explicitly in the
				%matlab code.
				eval ( dynamic_property.instance );
%			case 'value'
			case 'null'
				output_value = [];
			otherwise % 'value' is default
				eval ( if3 ( regexp ( dynamic_property.instance, '^[-+]?\d*([\.]\d+)?$' ), sprintf ( 'output_value = %s;', dynamic_property.instance ), 'output_value = dynamic_property.instance;' ) );
%				warning ( 'get_dynamic_property_value:wrong_property_type', 'I don''t know how to instanciate a property with type "%s".', property.type );
		end
		dynamic_property.value = output_value;
%	catch e
%		warning ( 'get_dynamic_value:error_in_eval', 'An error has occured while evaluating property ''%s'' :\n%s\n\ncheck_output has the following value :\n', property.name, e.message );
%		disp ( check.output );
%	end