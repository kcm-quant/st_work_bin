function property = get_property ( varargin ) % (name, type, value) or (xml_element)
	switch nargin 
		case 1
			try
				if ( isfield ( varargin{ 1 }, 'tag' ) )
					varargin{ 1 } = xmltools ( varargin{ 1 } );
				end
				if ( varargin{ 1 }.nb_children () )
					element = varargin{ 1 }.this ();
					tag = element.children ( 1 ).tag;
					property = struct ( 'name', varargin{ 1 }.get_attrib_value ( tag, 'name' ), 'type', varargin{ 1 }.get_attrib_value ( tag, 'type' ), 'value', varargin{ 1 }.get_tag_value ( tag ) );
				else
%					property = struct ( 'name', '', 'type', '', 'value', '' );
					property = {};
				end
			catch e
				error ( 'Error using get_property :\nArgument must be an xml_element with optional ''type'' and ''name'' attributes.' );
			end
		case 3
			property = struct ( 'name', varargin{ 1 }, 'type', varargin{ 2 }, 'value', varargin{ 3 } );
		otherwise
			error( 'Error using get_property :\nWrong number of input arguments' );
	end
