function property = get_property_definitions ( varargin ) % (xml_element, [tag name])
	if ( varargin{ 1 }.nb_children () )
		switch nargin
			case 1
				xml_element = varargin{ 1 };
				element = xml_element.this ();
				tag = element.children ( 1 ).tag ( 1 : end - 1 );
			case 2
				xml_element = varargin{ 1 };
				tag = varargin{ 2 };
		end
		properties = xmltools ( xml_element.get_tag ( tag ) );
		nb = properties.nb_children ();
        if ( nb > 0 )
            property ( nb, 1 ) = struct ( 'name', '', 'type', '', 'value', '' );
            for i = 1 : nb
                property ( i ) = get_property ( xmltools ( properties.keep_children ( i ) ) );
            end
        else
    		property = [];
        end
	else
		property = [];
	end

