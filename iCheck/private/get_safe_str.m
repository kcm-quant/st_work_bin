function safe = get_safe_str ( mode, str )
	%escapes str for special characters (depending on the context specified by mode)
	if ( isempty ( str ) )
		safe = '';
	else
		switch lower ( mode )
			case 'sql'
				safe = regexprep ( str, '''', '''''' );
			case 'itrs'
				safe = regexprep ( str, '(^[\\\s]*|[\\\s]*$)', '' );%delete leading and trailing whitespace
				safe = regexprep ( safe, '([\\\f\\\n\\\r\\\t\\\v]+| {4,})', '|' );%replace tabulations by | (pipe) character
				safe = regexprep ( safe, '[:;]', '\\$1' );%escape special seperation characters
			case 'file_name'
				safe = regexprep ( str, { '[\\<\\>\\:\\"\\/\\\\\\|\\?\\*]', '\\s' }, { '-', '_' } );
			case 'url'
				safe = urlencode ( str );
			case 'regexp'
				safe = regexprep ( str, '[\$\^\{\}\[\]\(\)\|\.\?\*\+\\]','\\$0' );
			case 'email'
                if ( ispc () )
                    safe = sprintf(str);
                else
                    safe = regexprep ( str, '\n','\\n' );
                end
		end
	end