	function value = get_value_for_expression ( expression, check )
		global st_version;
		expression = regexprep ( expression, '''' , '''''' );
		try
			evalc ( ['value_from_matlab_expression=eval(''' expression ''');'] );
%			value_from_matlab_expression = eval ( expression );
		catch e
			value_from_matlab_expression = [];
			warning ( 'parse_for_expressions:error_in_eval', 'evalc(''value_from_matlab_expression=eval(''''''%s'''''');'') generated an error :\n%s', expression, e.message );
		end
		if ( ~ischar ( value_from_matlab_expression ) )
%			value_from_matlab_expression = evalc ( 'disp ( value_from_matlab_expression );' );
            value_from_matlab_expression = regexprep(convs('str',value_from_matlab_expression),{'~',';','"|{|}'},{'\t','\n',''});
		end
		value = value_from_matlab_expression;
	end
