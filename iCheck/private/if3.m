function value = if3 ( condition, if_true, if_false )
	if ( condition )
		value = if_true;
	else
		value = if_false;
	end
	