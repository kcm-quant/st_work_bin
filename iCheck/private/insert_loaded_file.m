function loaded_file_id = insert_loaded_file ( file, is_load_ok )
	global st_version;
	current_datetime = datestr ( now, 'mmm dd yyyy HH:MM:SS:FFFAM' );
	modified_datetime = dir ( file );
	modified_datetime = datestr ( modified_datetime.datenum, 'mmm dd yyyy HH:MM:SS:FFFAM' );
	f = fopen ( file, 'r' );
	md5 = hash ( fread ( f ), 'md5' );
	fclose ( f );
	loaded_file_id = cell2mat ( exec_sql ( 'QUANT', sprintf ( '%s..ins_loaded_file ''%s'', %d, ''%s'', ''%s'', ''%s''', st_version.bases.quant, current_datetime, is_load_ok, file, modified_datetime, md5 ) ) );
