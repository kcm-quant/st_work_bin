function parsed_value = parse_for_expressions ( value, check )
	global st_version;
	if ( ~ischar ( value ) )
		parsed_value = parse_for_expressions ( convs ( 'str', value ), check );
	else
		value_from_matlab_expression = '';
		%replaces defined matlab shortcuts
		nb = size ( check.shortcut, 1 );
		names = {nb};
		values = {nb};
		%TODO : this should be done in a global var !
		for i = 1 : nb
			names{ i } = sprintf('\\$\\{%s\\}\\$', check.shortcut ( i ).name);
			switch lower ( check.shortcut ( i ).type )
				case 'matlab'
					eval ( check.shortcut ( i ).value );
					values{ i } = if3 ( ischar ( output_value ), output_value, convs ( 'str', output_value ) );
				case 'value'
					values{ i } = get_value_for_expression ( check.shortcut ( i ).value, check );
			end
		end
		parsed_value = regexprep ( value, names, values );
		%looks for more matlab code to replace
		more_expressions = regexp ( parsed_value, '(?<=\$\{)[^\$]*(?=\}\$)', 'match', 'warnings' );
		nb = numel ( more_expressions );
		if (nb > 0 )
			more_values = { nb };
			for n = 1 : nb
				more_values{ n } = get_value_for_expression ( more_expressions{ n }, check );
				more_expressions{ n } = regexptranslate ( 'escape', [ '${' more_expressions{ n } '}$' ] );
			end
			parsed_value = regexprep ( parsed_value, more_expressions, more_values );
		end
	end
end