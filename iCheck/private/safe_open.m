function fhandle = safe_open ( full_file_name, mode )
	% this function returns a handle on the file whose path is
	% provided. if the path does not exist but is a subdirectory of the parent of st_work,
	% the necessary directories are created. if the file does not exist, the file
	% might or might not be created depending on the mode (@see fopen).
	global st_version;
	try
        [ dir_path, fname, fext ] = fileparts ( full_file_name );
        file_name = [ fname, fext ];
		if ( ~isdir ( dir_path ) )
			old_dir = pwd;
			base_path = fileparts ( st_version.my_env.st_work );
			if ( strcmpi ( substr ( dir_path, 0, length ( base_path ) ), base_path ) )
				cd ( base_path );
				if ( ~isdir ( dir_path ) )
					mkdir ( dir_path );
				end
				cd ( old_dir );
			else
				warning ( 'do_alarm:fopen_fullpath', 'Error with dir_path : the dir_path must exist, or if it must be created on-the-fly it must be in the same brotherhood as st_work (''%s'' is not a subdirectory of ''%s'').', dir_path, base_path );
				fhandle = -1;
			end
		end
		if ( isdir ( dir_path ) )
			fhandle = fopen ( fullfile ( dir_path, file_name ), mode );
			if ( fhandle < 0 )
				warning ( 'do_alarm:fopen_fullpath', 'Unable to get a valid file handle with fopen ( ''%s'', ''%s'' );', fullfile ( dir_path, file_name ), mode );
			end
		end
	catch e
		warning ( 'do_alarm:fopen_fullpath', 'Error while creating path to or opening file %s :\n%s', fullfile ( dir_path, file_name ), e.message );
		fhandle = -1;
	end
end