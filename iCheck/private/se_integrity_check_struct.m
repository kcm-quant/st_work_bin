function se_integrity_check_struct ( check, force_check )
% private function called by se_integrity_check in order to do the
% specified check (argument must be the expected struct).
	global st_version;
	
	function is_above = is_above_iterative_limit ( nb, alarm_type )
		switch ( alarm_type )
			case { 'database', 'log', 'report', 'itrs' }
				is_above = false;
			case 'mail'
				is_above = nb > 100;
			case 'sms'
				is_above = nb > 10;
		end
	end
	
	function do_alarm ( alarm, check_id, check, severity )
		
		function html = update_html ( html, scopes, message, varargin )
			if ( iscell ( html ) )%TODO : try to correct this when calling update_html recursively using regexp match as html...
				html = html{:};
			end
			if ( isempty ( varargin ) )
				level = 1;
			else
				level = varargin{ 1 };
			end
			tabs = repmat ( sprintf ( '\t' ), 1, level + 2 );
			if ( level <= size ( scopes, 1 ) )
				meta_marker = '<!--${%%s_section=%s}$-->';
				marker_name = [ scopes{ level, 2 } '_' scopes{ level, 3 } ];
				marker = sprintf ( meta_marker, marker_name );
				if ( strcmpi ( scopes{ level, 2 }, 'date' ) )
					title = datestr ( str2double ( scopes{ level, 3 } ), 'dd/mm/yyyy' );
				else
					title = scopes{ level, 3 };
				end
				header = sprintf ( '%s<div id = "%s" class = "%s" >\n%s\t<h2>%s</h2>\n%s\t%s\n', tabs, marker_name, scopes{ level, 2 }, tabs, title, tabs, sprintf ( marker, 'begin' ) );
				footer = sprintf ( '%s\t%s\n%s</div>\n', tabs, sprintf ( marker, 'end' ), tabs );
				if ( regexp ( html, get_safe_str ( 'regexp', sprintf ( marker, 'end' ) ) ) )
					%the section was found
					inner_html = update_html ( regexp ( html, sprintf ( '(?<=%s).*(?=%s)', get_safe_str ( 'regexp', sprintf ( marker, 'begin' ) ), get_safe_str ( 'regexp', sprintf ( marker, 'end' ) ) ), 'match', 'once' ), scopes, message, level + 1 );
html = regexprep ( html, sprintf ( '(?<=%s)(.*)(?=%s)', get_safe_str ( 'regexp', sprintf ( marker, 'begin' ) ), get_safe_str ( 'regexp', sprintf ( marker, 'end' ) ) ), get_safe_str( 'regexp', inner_html ) );
% 					switch ( lower ( scopes{ level, 1 } ) )
% 						case 'prepend'
% 							html = regexprep ( html, sprintf ( '(?<=%s)(.*)(?=%s)', get_safe_str ( 'regexp', sprintf ( marker, 'begin' ) ), get_safe_str ( 'regexp', sprintf ( marker, 'end' ) ) ), [ get_safe_str( 'regexp', inner_html ) '$1' ] );
% 						case 'replace'
% 							html = regexprep ( html, sprintf ( '(?<=%s)(.*)(?=%s)', get_safe_str ( 'regexp', sprintf ( marker, 'begin' ) ), get_safe_str ( 'regexp', sprintf ( marker, 'end' ) ) ), get_safe_str( 'regexp', inner_html ) );
% 						otherwise %'append'
% 							html = regexprep ( html, sprintf ( '(?<=%s)(.*)(?=%s)', get_safe_str ( 'regexp', sprintf ( marker, 'begin' ) ), get_safe_str ( 'regexp', sprintf ( marker, 'end' ) ) ), [ '$1' get_safe_str( 'regexp', inner_html ) ] );
% 					end
				else
					%the section is not found
					inner_html = [ header update_html( '', scopes, message, level + 1 ) footer ];
					if ( isempty ( html ) )
						html = inner_html;
					else
						if ( level <= 1 )
							parent_name = 'se_content';
							parent_marker = sprintf ( sprintf ( meta_marker, parent_name ), 'end' );
							expr = sprintf ( '(^.*)(?=%s)(.*$)', get_safe_str ( 'regexp', parent_marker ) );
							html = regexprep ( html, expr, [ '$1' get_safe_str( 'regexp', inner_html ) '$2' ] );
						else
							html = if3 ( strcmpi ( scopes{ level, 1 }, 'prepend' ), [ inner_html html ], [ html inner_html ] );
						end
					end
				end
			else
				html = sprintf ( '%s\t%s\n', tabs, message );
			end
		end
		
		name = get_dynamic_value ( alarm.name, check );
		name = parse_for_expressions ( name.value, check );
		destination = get_dynamic_value ( alarm.destination, check );
		destination = parse_for_expressions ( destination.value, check );
		message = get_dynamic_value ( alarm.message, check );
		message = parse_for_expressions ( message.value, check );
		alarm_severity = get_dynamic_value ( alarm.severity, check );
		if ( isempty ( alarm_severity.value ) )
			alarm_severity.value = 0;
		else
			alarm_severity.value = str2double ( parse_for_expressions ( alarm_severity.value, check ) );	
		end
		
		switch ( alarm.type )
			case 'database'
				alarm_id = cell2mat ( exec_sql ( 'QUANT', sprintf ( '%s..ins_int_alarm %d, %d, ''%s'', %d', st_version.bases.quant, alarm.id, check_id, get_safe_str ( 'sql', name ), alarm_severity.value ) ) );
				exec_sql ( 'QUANT', sprintf ( 'update %s..int_alarm set destination=''%s'', message=''%s'' where int_alarm_id=%d', st_version.bases.quant, get_safe_str ( 'sql', destination ), get_safe_str ( 'sql', message ), alarm_id ) );
				for k = 1 : size ( alarm.scope, 1 )
					tmp = get_dynamic_value ( alarm.scope ( k ), check );
					save_object_property ( 'int_alarm', 'scope', alarm_id, get_property ( alarm.scope ( k ).name, alarm.scope ( k ).type, parse_for_expressions ( tmp.value, check ) ) );
				end
			case 'mail'
                if (~isempty(st_version.my_env.icheck_debug) && st_version.my_env.icheck_debug )
                    se_sendmail ( regexp ( parse_for_expressions ( '${debug_emails}$', check ), '[,;]', 'split' ), get_safe_str ( 'email', [ '[DEBUG] ' name ] ), get_safe_str ( 'email', sprintf ( '** DEBUG MODE ON **\ndestination :\n%s\nmessage :\n%s', destination, message ) ) );
                else
					se_sendmail ( regexp ( destination, '[,;]', 'split' ), get_safe_str ( 'email', name ), get_safe_str ( 'email', message ) );
                end
                %%write file to log
%                 log_temp = fullfile(st_version.my_env.log,'buffer.log');
%                 fid = fopen(log_temp,'w');
%                 fprintf(fid,message);
%                 fclose(fid);
                
%TODO : tell romain about -a swithc for iso latin 1 chars :
%system ( sprintf ( 'echo "%s" | mail -a ISO-8859-1 -s "%s" -c sgalzin@cheuvreux.com %s', message, name, destination ) );
			case 'sms'
				message = sprintf ( '%s\n%s', name, message );
				if ( size ( message, 1 ) > 160 )
					warning ( 'do_alarm:sms', 'The message is longer than 160 characters => only the first 160 characters will be sent. Original message :\n%s', message );
					message = mesage(1:160);
				end
				se_sendmail ( cellfun ( @(a)[a '@sms.oleane.com'], regexp ( destination, '[,;]', 'split' ), 'UniformOutput', false ), '', message );
			case 'log'
				f = fopen_fullpath ( fullfile ( getenv ( 'st_repository' ), 'iCheck', 'log' ), destination , 'a+' );
				if ( f > -1 )
					fprintf ( f, '%s', message );
					fclose ( f );
				end
			case 'report'
				nb_scopes = size ( alarm.scope, 1 );
				scopes = cell ( nb_scopes, 3 );
				for k = 1 : nb_scopes
					tmp = get_dynamic_value ( alarm.scope ( k ), check );
					tmp = parse_for_expressions ( tmp.value, check );
					tmp = regexp ( tmp, ':', 'split' );
					scopes{ k, 2 } = alarm.scope ( k ).name;
					if ( size ( tmp, 2 ) == 1 )
						scopes{ k, 1 } = 'append';
						scopes{ k, 3 } = tmp{ 1 };
					else
						scopes{ k, 1 } = tmp{ 1 };
						scopes{ k, 3 } = tmp{ 2 };
					end
				end
%TODO : try-catch for file
				f = fopen( fullfile ( st_version.my_env.www, destination ), 'r' );
				frewind ( f );
				html = fread ( f, 'char=>int' );
				html = char ( reshape ( html, 1, length ( html ) ) );
				fclose(f);
				f = fopen( fullfile ( st_version.my_env.www, destination ), 'w' );
				fwrite ( f, update_html ( html, scopes, message ) );
				fclose(f);
			case 'itrs'
				f = fopen_fullpath ( st_version.my_env.itrs, destination, 'a+' );
				if ~isempty ( alarm_severity )
					severity = alarm_severity.value;
				end
				if ( f > -1 )
					fprintf ( f, '%s:%s;%5.4f;%s;\n', datestr ( now, 'yyyy/mm/dd HH:MM:SS' ), name, severity, get_safe_str ( 'itrs', message ) );
					fclose ( f );
				end
		end
	end

	if ( ~strcmpi ( check.active_to, 'null' ) )
		warning ( 'se_integrity_check:running_inactive_check', 'The (manual) request for an inactive check has been made (%s).', check.mnemonic );
	end
    
    current_datetime = now;
	fprintf ( '%s : check %s :        has cron_regexp of %s.\n', datestr ( current_datetime, 'mmm dd yyyy HH:MM:SS:FFFAM' ), check.mnemonic, check.cron_regexp );
    frequency = force_check;

	if ( strcmpi ( check.last_verification, 'null' ) ) 
		frequency = true;
		fprintf ( '%s : check %s :  OK1 : has never been launched => let''s check for triggers.\n', datestr ( current_datetime, 'mmm dd yyyy HH:MM:SS:FFFAM' ), check.mnemonic );
	else
		last_verification = datevec ( check.last_verification );

        % frequency verification
        test_datetime = datevec ( current_datetime );
        %compare the current datetime to the last_verification datetime at
        %minute precision => put seconds to zero :
        test_datetime ( 6 ) = 0;
		while ( ~frequency && datenum ( test_datetime ) > datenum ( last_verification ) )
			if ( regexp ( datestr ( datenum ( test_datetime ), 'ddd dd/mm/yyyy HH:MM' ), check.cron_regexp ) )
				frequency = true;
				fprintf ( '%s : check %s :  OK1 : was last launched on %s and could have been launched on %s => let''s check for triggers.\n', datestr ( current_datetime, 'mmm dd yyyy HH:MM:SS:FFFAM' ), check.mnemonic, check.last_verification, datestr ( datenum ( test_datetime ), 'mmm dd yyyy HH:MM' ) );
			end
			% one-minute increments
			test_datetime ( 5 ) = test_datetime ( 5 ) - 1;
		end
	end
	if ( ~frequency )
		fprintf ( '%s : check %s : NOK1 : last launched on %s, it''s not time yet.\n', datestr ( current_datetime, 'mmm dd yyyy HH:MM:SS:FFFAM' ), check.mnemonic, check.last_verification );
	end

	% trigger verification
	triggered = false;
    trigger_condition = struct ();
	if ( frequency && ~force_check )
		%TODO : look at triggers
		nb = size ( check.trigger, 1 );
		if ( nb > 0 )
			% there are triggers configured for this check
			for i = 1 : nb
				current_trigger = get_dynamic_value ( check.trigger ( i ), check );
				switch ( lower ( check.trigger ( i ).type ) )
					case 'all'
						where = '';
					case { 'value', 'sql' }
						values='';
						if ( iscell ( current_trigger.value ) )
							for j = 1 : size ( current_trigger.value, 1 )
								values = sprintf ( '%s,%d', values, current_trigger.value{ j } );
							end
						else
							for j = 1 : size ( current_trigger.value, 1 )
								values = sprintf ( '%s,%d', values, current_trigger.value( j ) );
							end
						end
						where = sprintf ( ' and tbil.id_column_id in (%s)', values(2:end));
					otherwise
						where = ' and tbil.id_column_id=-1';
				end
				ids = exec_sql ( 'QUANT', sprintf ( 'select tbil.id_column_id from %s..trigger_buffer_id_list tbil, %s..trigger_buffer tb where tb.trigger_buffer_id = tbil.trigger_buffer_id and tb.table_name=''%s'' and datetime>convert(datetime, ''%s'')%s', st_version.bases.quant, st_version.bases.quant, check.trigger ( i ).name, datestr ( check.last_verification, 'mmm dd yyyy HH:MM:SS:FFFAM'), where ) );
				if ( ~isempty ( ids ) )
					triggered = true;
					triggered_ids='';
					for j = 1 : length ( ids )
						triggered_ids = sprintf ( '%s, %d', triggered_ids, ids{ j } );
					end
					trigger_condition.(check.trigger ( i ).name) = sprintf ( 'in (%s)', triggered_ids ( 3 : end ) );
				else
					trigger_condition.(check.trigger ( i ).name) = '=-99999';%no id should be equal to this number
				end
			end
		else
			% this check does not use triggers
			triggered = true;
		end
	end
	
    error = false;
    error_msg = [];
	% do instanciation
	if ( triggered || force_check )
		fprintf ( '%s : check %s :  OK2 : will be launched (check forced = %d).\n', datestr ( current_datetime, 'mmm dd yyyy HH:MM:SS:FFFAM' ), check.mnemonic, force_check );
		try
			begin_dt = now;
			check.trigger_condition = trigger_condition;
			dynamic_request = get_dynamic_value ( check.request, check );
			check.output = dynamic_request.value;
			end_dt = now;
			if ( ~isempty ( dynamic_request.value ) )
				dynamic_severity = get_dynamic_value ( check.severity, check );
				try
					check_id = cell2mat ( exec_sql ( 'QUANT', sprintf ( '%s..ins_int_check %d, ''%s'', ''%s'', %d', st_version.bases.quant, check.id, datestr ( begin_dt, 'mmm dd yyyy HH:MM:SS:FFFAM' ), datestr ( end_dt, 'mmm dd yyyy HH:MM:SS:FFFAM' ), dynamic_severity.value ) ) );
					exec_sql ( 'QUANT', sprintf ( 'update %s..int_check set request=''%s'' where int_check_id=%d', st_version.bases.quant, get_safe_str ( 'sql', dynamic_request.instance ), check_id ) );
					for i = 1 : size ( check.alarm, 1 )
						try
							if ( check.alarm ( i ).active )
								iterative = false;
								nb_alarms = size ( dynamic_request.value, 1 );
								if ( strcmpi ( check.alarm ( i ).scrutation, 'iterative' ) )
									if ( is_above_iterative_limit ( nb_alarms, check.alarm ( i ).type ) )
										warning ( 'integrity_check:above_iterative_limit', 'There are too many alarms (total = %d) for alarm n� %d (type %s) to be iterative => alarm will be launched as a bundle.', nb_alarms, check.alarm ( i ).id, check.alarm ( i ).type );
									else
										iterative = true;
									end
								end
								if ( iterative )
									for j = 1 : nb_alarms
										check.output = {dynamic_request.value{j,:}};
										do_alarm ( check.alarm ( i ), check_id, check, dynamic_severity.value );
									end
								else
									check.output = dynamic_request.value;
									do_alarm ( check.alarm ( i ), check_id, check, dynamic_severity.value );
								end
							end
						catch alarm_error
				            error = true;
                            error_msg = [error_msg,'\n\n------------------------------------\n',sprintf('Continuing execution after an error occured in alarm n�%d (type=%s) for check %s :\n%s', check.alarm ( i ).id, check.alarm ( i ).type, check.name, alarm_error.message),text_to_printable_text(se_stack_error_message ( lasterror ))];
							warning ( 'integrity_check:do_alarm', 'Continuing execution after an error occured in alarm n�%d (type=%s) for check %s :\n%s', check.alarm ( i ).id, check.alarm ( i ).type, check.name, alarm_error.message );
						end
					end
				catch check_insert_error
                    error_msg = [error_msg,'\n\n------------------------------------\n',sprintf('Error occured while inserting check ''%s'', check will be skipped and execution will continue.\n%s', check.name, check_insert_error.message),text_to_printable_text(se_stack_error_message ( lasterror ))];
		            error = true;
					warning ( 'integrity_check:insert_check', 'Error occured while inserting check ''%s'', check will be skipped and execution will continue.\n%s', check.name, check_insert_error.message );
				end
			end
		catch e
            error = true;
            error_msg = [error_msg,'\n\n------------------------------------\n',sprintf('an error occurred while instanciating check "%s" :\n%s', check.mnemonic, e.message ),text_to_printable_text(se_stack_error_message ( lasterror ))];
			warning ( 'integrity_check_instanciation:error_in_instanciation', 'an error occurred while instanciating check "%s" :\n%s', check.mnemonic, e.message );
		end
		try
			exec_sql ( 'QUANT', sprintf ( 'update %s..int_check_def set last_instanciation_dt=convert(datetime, ''%s'') where int_check_def_id=%d', st_version.bases.quant, datestr ( begin_dt, 'mmm dd yyyy HH:MM:SS:FFFAM' ), check.id ) );
			end_dt = if3 ( isempty ( end_dt ), now (), end_dt );
			exec_sql ( 'QUANT', sprintf ( 'update %s..int_check_def set last_output_dt=convert(datetime, ''%s'') where int_check_def_id=%d', st_version.bases.quant, datestr ( end_dt, 'mmm dd yyyy HH:MM:SS:FFFAM' ), check.id ) );
		catch check_update_error
			warning ( 'integrity_check:update_check', 'Error occured while updating check ''%s'' => dates will not be updated.\n%s', check.name, check_update_error.message );
		end
	else
		fprintf ( '%s : check %s : NOK2 : will not be launched (force_check = %d, triggered = %d).\n', datestr ( current_datetime, 'mmm dd yyyy HH:MM:SS:FFFAM' ), check.mnemonic, force_check, triggered );
	end
	%update last_verification_dt : this is done at the end of the code in
	%order to not update if there is an error.
	if ( ~error )
		exec_sql ( 'QUANT', sprintf ( 'update %s..int_check_def set last_verification_dt=convert(datetime, ''%s'') where int_check_def_id=%d', st_version.bases.quant, datestr ( current_datetime, 'mmm dd yyyy HH:MM:SS:FFFAM' ), check.id ) );
    else
        se_sendmail ( regexp ( st_version.my_env.se_admin, ',', 'split' ), sprintf ( 'SYSTEM ERROR in iCheck for ''%s''', check.name ), sprintf(error_msg) ); %#ok<LERR>
	end
end

% check structure :
% 	id
%	shortcut
% 	mnemonic
% 	name
% 	cron_regexp
% 	comment
% 	active_from
% 	active_to
% 	last_verification
% 	last_instanciation
% 	last_output
%	trigger_condition
%		[trigger_name_1]
%		[trigger_name_2]
%		[etc.]
% 	trigger ()
% 		name
% 		type
% 		value
% 	request
% 		name
% 		type
% 		value
% 	severity
% 		name
% 		type
% 		value
% 	alarm () : int_alarm_id, int_alarm_def_id, name, destination, message
% 		id
% 		type
% 		scrutation
% 		active
% 		destination
% 			name
% 			type
% 			value
% 		name
% 			name
% 			type
% 			value
% 		message
% 			name
% 			type
% 			value
% 		scope ()
% 			name
% 			type
% 			value
% 	