function out = se_test_assoc_index_without_valid(base)

global st_version;
base = st_version.bases.quant;

res = exec_sql('QUANT', sprintf( [ 'SELECT jb.job_id,jb.estimator_id, dm.domain_name , est.estimator_name ' ...
               'FROM %s..association ass , %s..job jb , %s..domain dm , %s.. estimator est ' ...
               'WHERE  ' ...
               'ass.job_id =jb.job_id AND ' ...
               'dm.domain_type_id >1 AND ' ...
               'dm.domain_id = jb.domain_id AND ' ...
               'est.estimator_id = jb.estimator_id AND ' ...
               'jb.job_id not in(  ' ...       
               'SELECT ass.job_id  ' ...
               'FROM %s..association ass,%s..job jb, %s..domain dm , %s..estimator_runs est_r ' ...
               'WHERE ' ...
               'ass.job_id =jb.job_id AND ' ...
               'dm.domain_type_id >1 AND ' ...
               'dm.domain_id = jb.domain_id AND ' ...
               'jb.job_id  = est_r.job_id AND ' ...
               'est_r.is_valid = 1) '] , base ,base ,base ,base ,base ,base ,base , base))
           
data = st_data('from-cell', ' ', {'job_id' , 'estimator','name' ,'estimator_name'}, res);

estimator = unique (st_data('cols',data,'estimator' ));
out = {};

for i= 1: length(estimator)
   
    tmp= st_data('where',data , sprintf('{estimator} == %d' ,estimator(i)));
    book1 = codebook( 'get', tmp.codebook, 'name');
    list = book1{1};
    
   for j = 2 : length(book1)
    list = sprintf('%s ; %s ',list,book1{j})    ;  
   end
   
   book2 = codebook( 'get', tmp.codebook, 'estimator_name');  
   out(end + 1 , : ) = { book2{i} , list } ;
end




end