function warn_if_unexpected_children ( xml_node, expected_children )
	if ( xml_node.nb_children () )
		if ( ischar ( expected_children ) )
			warn_if_unexpected_children ( xml_node, { expected_children } );
		end
		%TODO : verify the below bit of code with Charles
		bizarre = xml_node.keep_where_attrib ( '', '' );
		if ( isempty ( bizarre.children ( 1 ).children ( 1 ).tag ) )
			nb = 0;
		else
			nb = length ( bizarre.children ( 1 ).children );
		end
		if ( isempty ( expected_children ) && nb > 0 )
			xml_node = xml_node.this ();
			warning ( 'xml_parsing:unexpected_children', 'The <%s> element contains children whereas none are expected (elements is supposed to be empty).', xml_node.children.tag );
		else
			expected_children = unique ( [ expected_children; '#comment' ] );
			found_children = cell ( nb, 1 );
			for i = 1 : nb
				found_children{ i } = bizarre.children ( 1 ).children ( i ).tag ;
			end
			found_children = unique ( found_children );
			if ( max ( ~ismember ( found_children, expected_children ) ) )
				xml_node=xml_node.keep_children ( 1 );
				warning ( 'xml_parsing:unexpected_children', 'The <%s> element contains unknown children :\nexpected : %s\nfound : %s\n=> These children elements might not be parsed.', xml_node.children.tag, convs ( 'str', expected_children ), convs ( 'str', found_children ) );
			end
		end
	end
	
% %TODO : write equivalent of warn_if_unexpected_children for attributes
% %function warn_if_unexpected_attributes ( xml_node, expected_attributes )
% %end
% 
% %TODO : make a more generic check : see if element has children, or a value
% %such as <element>value</element> (no children here), etc. basically, write
% %something close to a dtd conforming check in order to warn about
% %grammatical errors in the xml. maybe this could be generic enough to
% %replace the next function (which checks attributes) ?
% %TODO : add cardinality info (mandatory/optional, 1, many) : ?, 1, *, +
% %TODO : have 2 modes : strict / forward-compatible, strict mode is
% %strict, forward-compatible doesn't alert in case an unexpected child
% %is found (i.e. one which is not referenced), all other cardinality
% %problems still generate an alert.
% 
% function attrib_value = get_optional_value_for_attrib ( xml_node, attrib_name, default_value )
% 	node = xml_node.this ();
% 	tag = node.children.tag;
% 	attrib_value = xml_node.get_attrib_value ( tag, attrib_name );
% 	if ( isempty ( attrib_value ) )
% 		warning ( 'xml_parsing:optional_attribute_not_set', 'Attribute ''%s'' is not set for element <%s>\n=> the default value "%s" will be used.', attrib_name, tag, default_value );
% 		attrib_value = default_value;
% 	end
% 
% function element_value = get_optional_value_for_element ( xml_node, default_value )
% 	node = xml_node.this ();
% 	tag = node.children.tag;
% 	element_value = xml_node.get_tag_value ( tag );
% 	if ( ~isempty ( element_value ) )
% 		element_value = parse_for_expressions ( element_value{ end } );
% 	end
% 	if ( isempty ( element_value ) )
% 		warning ( 'xml_parsing:optional_element_not_set', 'Element <%s> has no value\n=> the default value "%s" will be used.', tag, default_value );
% 		element_value = default_value;
% 	elseif ( length ( element_value ) > 1 )
% 		warning ( 'xml_parsing:multiple_elements_overwrite_single_value', 'Element <%s> is set multiple times\n=> only the last occurence will be used.', tag );
% 	end
