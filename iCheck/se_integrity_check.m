% varargin{ i } : the check struct or the check mnemonic
% varargin{ i+1 } : an optional logical value which may be specified :
%					- false : check only launched if triggers are waiting in the buffer.
%					- true  : check is launched even if no triggers are present (equivalent to the first launch).
			
function se_integrity_check ( varargin )
	if ( nargin == 0 )
		arrayfun ( @( a ) se_integrity_check ( a, false ), se_integrity_load_db () );
	else
		if ( size ( varargin{ 1 }, 1 ) > 1 )
			error ( 'struct arrays of arguments not accepted, please enter checks or check mnemonics one after the other as arguments.' );
		end
		
		% find out if the next argument is a force_check
		if ( nargin > 1 && islogical ( varargin{ 2 } ) )
			force_check = varargin{ 2 };
			next = 3;
		else
			force_check = false;
			next = 2;
		end
		
		% launch check depending on if check is a mnemonic or a struct
		if ischar ( varargin{ 1 } )
			se_integrity_check ( se_integrity_load_db ( varargin{ 1 } ), force_check );
		elseif isstruct ( varargin{ 1 } )
			se_integrity_check_struct ( varargin{ 1 }, force_check );
		else
			error ( 'Unknown argument type for se_integrity_check ( { check_struct | mnemonic_char } [, force_check_int ] ) : first argument must be either a check struct or a char with the mnemonic. ' );
		end
		
		% recursive launch for the next checks
		if ( next <= nargin )
			se_integrity_check ( varargin{ next : end } );
		end
	end

%TODO :
% 		- find the check in db
% 		- load to check struct
% 		- these should be calls to sub functinos, in se_integrity_load maybe...
% 		- then launch the check without focusing on triggers / cron_regexp
% 		: se_integrity_single_check
		
		