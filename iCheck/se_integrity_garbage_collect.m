function se_integrity_garbage_collect ()
	global st_version;
	check = se_integrity_load_db ();
	clear ( 'se_integrity_garbage_collection' );
	garbage_collection = struct();
	
	%step 1 : for each trigger target, look at the most ancient
	%last_instanciation among the different checks using that target and
	%save the date
	nb_checks = size ( check, 1 );
	for i = 1 : nb_checks
		nb_triggers = size ( check ( i ).trigger, 1 );
		for j = 1 : nb_triggers
			if ( ~isfield ( garbage_collection, check ( i ).trigger ( j ).name ) )
				garbage_collection.(check ( i ).trigger ( j ).name) = datenum ( check ( i ).last_instanciation );
			else
				if ( datenum ( check ( i ).last_instanciation ) < garbage_collection.(check ( i ).trigger ( j ).name) )
					garbage_collection.(check ( i ).trigger ( j ).name) = datenum ( check ( i ).last_instanciation );
				end
			end
		end
	end
	
	%step 2 : garbage collect any triggers anterior to the most ancient
	%last instanciation found in step 1
	trigger = exec_sql ( 'QUANT', sprintf ( 'select trigger_buffer_id, table_name from %s..trigger_buffer', st_version.bases.quant ) );
	nb_triggers = size ( trigger, 1 );
	for i = 1 : nb_triggers
		where = '';
		if ( isfield ( garbage_collection, trigger{ i, 2 } ) )
			where = sprintf ( ' and datetime<convert(datetime, ''%s'')', datestr ( garbage_collection.(trigger{ i, 2 }), 'mmm dd yyyy HH:MM:SS:FFFAM' ) );
		end
		exec_sql ( 'QUANT', sprintf ( 'delete from %s..trigger_buffer_id_list where trigger_buffer_id=%d%s', st_version.bases.quant, trigger{ i, 1 }, where ) );
	end
	exec_sql ( 'QUANT', sprintf ( 'delete from %s..trigger_buffer where trigger_buffer_id not in (select trigger_buffer_id from %s..trigger_buffer_id_list group by trigger_buffer_id)', st_version.bases.quant, st_version.bases.quant ) );
	
end
