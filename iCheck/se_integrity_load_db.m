%TODO : try / catch errors due to database corruption, check
%st_version.my_env.verbosity_level and do warnings / fprintf accordingly

function check = se_integrity_load_db ( varargin )
	global st_version;
	% if the mnemonics are given in varargin, the last check_def with this
	% mnemonicy will be loaded whether active or not.
	% Otherwise, all the active checks will be loaded.
	if ( nargin == 0 )
		where = 'cd.active_to_dt is NULL';
	else
		where = 'cd.mnemonic in ( ';
		for i = 1 : nargin
			where = sprintf ( '%s%s''%s''', where, if3 ( i == 1, '', ', ' ), varargin{ i } );
		end
		where = sprintf ( '%s ) and cd.int_check_def_id in (select max(check_def.int_check_def_id) from %s..int_check_def check_def group by check_def.mnemonic)', where, st_version.bases.quant );
	end
	check_result = exec_sql ( 'QUANT', sprintf ( 'select cd.int_check_def_id, cd.loaded_file_id, cd.mnemonic, cd.name, cd.cron_regexp, cd.comment, cd.active_from_dt, cd.active_to_dt, (select max(last_verification_dt) from %s..int_check_def icd where icd.mnemonic=cd.mnemonic) last_verification_dt, (select max(last_instanciation_dt) from %s..int_check_def icd where icd.mnemonic=cd.mnemonic) last_instanciation_dt, (select max(last_output_dt) from %s..int_check_def icd where icd.mnemonic=cd.mnemonic) last_output_dt, severity.type severity_type, severity.value severity_value, request.type request_type, request.value request_value from %s..int_check_def cd, %s..obj_property severity, %s..obj_property request where severity.obj_id=cd.int_check_def_id and request.obj_id=cd.int_check_def_id and severity.obj_name=''int_check_def'' and request.obj_name=''int_check_def'' and severity.xml_element_name=''severity'' and request.xml_element_name=''request'' and %s', st_version.bases.quant, st_version.bases.quant, st_version.bases.quant, st_version.bases.quant, st_version.bases.quant, st_version.bases.quant, where ) );
	nb_checks = size ( check_result, 1 );
	if ( nb_checks )
		check ( nb_checks, 1 ) = struct ( 'id', [], 'loaded_file_id', [], 'shortcut', struct ( 'name', {}, 'type', {}, 'value', {} ), 'mnemonic', [], 'name', [], 'cron_regexp', [], 'comment', [], 'active_from', [], 'active_to', [], 'last_verification', [], 'last_instanciation', [], 'last_output', [], ...
			'trigger', struct ( 'name', {}, 'type', {}, 'value', {} ), 'request', [], 'severity', [], 'output', [], ...
			'alarm', struct ( 'id', {}, 'type', {}, 'scrutation', {}, 'active', {}, 'destination', {}, 'name', {}, 'message', {}, 'severity', {}, 'scope', {} ) );
		for i = 1 : nb_checks
			check ( i ).id = check_result{ i, 1 };
			check ( i ).loaded_file_id = check_result{ i, 2 };
			check ( i ).mnemonic = check_result{ i, 3 };
			check ( i ).name = check_result{ i, 4 };
			check ( i ).cron_regexp = check_result{ i, 5 };
			check ( i ).comment = check_result{ i, 6 };
			check ( i ).active_from = check_result{ i, 7 };
			check ( i ).active_to = check_result{ i, 8 };
			check ( i ).last_verification = if3 ( strcmpi ( check_result{ i, 9 }, 'null' ), '01/01/2000 00:00:00', check_result{ i, 9 } );
			check ( i ).last_instanciation = if3 ( strcmpi ( check_result{ i, 10 }, 'null' ), '01/01/2000 00:00:00', check_result{ i, 10 } );
			check ( i ).last_output = if3 ( strcmpi ( check_result{ i, 11 }, 'null' ), '01/01/2000 00:00:00', check_result{ i, 11 } );
			check ( i ).severity = get_property ( '', check_result{ i, 12 }, check_result{ i, 13 } );
			check ( i ).request = get_property ( '', check_result{ i, 14 }, check_result{ i, 15 } );
			check ( i ).output = '';
			shortcut_result = exec_sql ( 'QUANT', sprintf ( 'select name, type, value from %s..obj_property where obj_name=''loaded_file'' and obj_id=%d and xml_element_name=''shortcut''', st_version.bases.quant, check ( i ).loaded_file_id ) );
			nb_shortcuts = size ( shortcut_result, 1 );
			if ( nb_shortcuts )
				check ( i ).shortcut ( nb_shortcuts, 1 ) = struct ( 'name', [], 'type', [], 'value', [] );
				for j = 1 : nb_shortcuts
					check ( i ).shortcut ( j ) = get_property ( shortcut_result{ j, 1 }, shortcut_result{ j, 2 }, shortcut_result{ j, 3 } );
				end
			end
			trigger_result = exec_sql ( 'QUANT', sprintf ( 'select name, type, value from %s..obj_property where obj_name=''int_check_def'' and obj_id=%d and xml_element_name=''trigger''', st_version.bases.quant, check ( i ).id ) );
			nb_triggers = size ( trigger_result, 1 );
			if ( nb_triggers )
				check ( i ).trigger ( nb_triggers, 1 ) = struct ( 'name', [], 'type', [], 'value', [] );
				for j = 1 : nb_triggers
					check ( i ).trigger ( j ) = get_property ( trigger_result{ j, 1 }, trigger_result{ j, 2 }, trigger_result{ j, 3 } );
				end
			end
			alarm_result = exec_sql ( 'QUANT', sprintf ( 'select alarm.int_alarm_def_id, type.name type_name, scrutation.name scrutation_name, alarm.is_active, destination.type destination_type, destination.value destination_value, name.type name_type, name.value name_value, message.type message_type, message.value message_value, severity.type severity_type, severity.value severity_value from %s..ref_int_alarm_type type, %s..ref_int_alarm_scrutation scrutation, %s..obj_property destination, %s..obj_property name, %s..obj_property message, %s..int_alarm_def alarm left join (select obj_id, type, value from %s..obj_property where obj_name=''int_alarm_def'' and xml_element_name=''severity'') severity on (severity.obj_id=alarm.int_alarm_def_id) where alarm.int_alarm_type_id=type.int_alarm_type_id and alarm.int_alarm_scrutation_id=scrutation.int_alarm_scrutation_id and alarm.int_alarm_def_id=destination.obj_id and destination.obj_name=''int_alarm_def'' and destination.xml_element_name=''destination'' and alarm.int_alarm_def_id=name.obj_id and name.obj_name=''int_alarm_def'' and name.xml_element_name=''name'' and alarm.int_alarm_def_id=message.obj_id and message.obj_name=''int_alarm_def'' and message.xml_element_name=''message'' and alarm.int_check_def_id=%d', st_version.bases.quant, st_version.bases.quant, st_version.bases.quant, st_version.bases.quant, st_version.bases.quant, st_version.bases.quant, st_version.bases.quant, check ( i ).id ) );
			nb_alarms = size ( alarm_result, 1 );
			if ( nb_alarms )
				check ( i ).alarm ( nb_alarms, 1 ) = struct ( 'id', [], 'type', [], 'scrutation', [], 'active', [], 'destination', [], 'name', [], 'message', [], 'severity', [], 'scope', struct ( 'name', {}, 'type', {}, 'value', {} ) );
				for j = 1 : nb_alarms
					check ( i ).alarm ( j ).id = alarm_result{ j, 1 };
					check ( i ).alarm ( j ).type = alarm_result{ j, 2 };
					check ( i ).alarm ( j ).scrutation = alarm_result{ j, 3 };
					check ( i ).alarm ( j ).active = alarm_result{ j, 4 };
					check ( i ).alarm ( j ).destination = get_property ( '', alarm_result{ j, 5 }, alarm_result{ j, 6 } );
					check ( i ).alarm ( j ).name = get_property ( '', alarm_result{ j, 7 }, alarm_result{ j, 8 } );
					check ( i ).alarm ( j ).message = get_property ( '', alarm_result{ j, 9 }, alarm_result{ j, 10 } );
					check ( i ).alarm ( j ).severity = get_property ( '', alarm_result{ j, 11 }, alarm_result{ j, 12 } );
					scope_result = exec_sql ( 'QUANT', sprintf ( 'select name, type, value from %s..obj_property where obj_name=''int_alarm_def'' and obj_id=%d and xml_element_name=''scope''', st_version.bases.quant, check ( i ).alarm ( j ).id ) );
					nb_scopes = size ( scope_result, 1 );
					if ( nb_scopes )
						check ( i ).alarm ( j ).scope ( nb_scopes, 1 ) = struct ( 'name', [], 'type', [], 'value', [] );
						for k = 1 : nb_scopes
							check ( i ).alarm ( j ).scope ( k ) = get_property ( scope_result ( k, 1 ), scope_result ( k, 2 ), scope_result ( k, 3 ) );
						end
					end
				end
			end
		end
	end
