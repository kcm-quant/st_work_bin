%TODO : check st_version.my_env.verbosity_level and do warnings / fprintf accordingly

%TODO : ask if the best practice is still to use se_db for inserts.
% if yes, add the following lines to se_db :
%         case 'add_int_check_def'
%         % Add an integrity check definition to the int_check_def table.
%         % (%TODO mnemonic, name, cron_regexp, comment, active_from_dt, active_to_dt)
%         %TODO query = sprintf('%s..ins_int_check_def ''%s'', ''%s'',
%         ''%s'', ''%s'', ''%s''',...
%             st_version.bases.quant, varargin{1}, varargin{2}, varargin{3}, varargin{4}, varargin{5});
%         out = cell2mat(exec_sql('QUANT', query));
%        
%        
%         case 'add_int_alarm_def'
%         % Add an integrity alarm definition to the int_alarm_def table.
%         % (%TODOmnemonic, name, cron_regexp, comment, active_from_dt, active_to_dt)
%         %TODO query = sprintf('%s..ins_int_check_def ''%s'', ''%s'', ''%s'', ''%s'', ''%s'', ''%s''',...
%             st_version.bases.quant, varargin{1}, varargin{2}, varargin{3}, varargin{4}, varargin{5}, varargin{6});
%         out = cell2mat(exec_sql('QUANT', query));

%TODO : one day, see if unexpected children may be more precise and do some sort of xml / dtd validation in the process...

function se_integrity_load_xml ( file )
	global st_version;
	try
		%see if the file has changed since the last successful load
		date_loaded = [];
		loaded_file_id = 0;
		try
			f = fopen ( file, 'r' );
			md5 = hash ( fread ( f ), 'md5' );
			fclose ( f );
			date_loaded = cell2mat ( exec_sql ( 'QUANT', sprintf ( 'select convert(varchar, max(load_dt), 109) from %s..loaded_file where file_name=''%s'' and file_md5=''%s'' and is_load_ok=1', st_version.bases.quant, file, md5 ) ) );
		catch problem
			warning ( 'integrity_check:xml_load_warning', 'Problem retrieving information on previous loads of this XML file so it will be (tentatively) reloaded.\n%s\n', problem.message );
		end
		
		%if file has changed, it must be loaded and saved
		if ( isempty ( date_loaded ) || strcmpi ( date_loaded, 'NULL' ) )
			fprintf('loading integrity checks from xml file ''%s''.\n', file );
			integrity = get_struct_from_xml ( file );
			fprintf('saving integrity checks definitions to the database.\n' );

			loaded_file_id = insert_loaded_file ( file, -1 );
			save_struct_to_db ( integrity, loaded_file_id );
			try
				exec_sql ( 'QUANT', sprintf ( 'update %s..loaded_file set is_load_ok=1 where loaded_file_id=%d', st_version.bases.quant, loaded_file_id ) );
			catch problem
				warning ( 'integrity_check:xml_load_warning', 'Problem declaring load (file load ok but file NOT added correctly to the ''loaded_file'' table) => the file will be reloaded next time (although it should not have been necessary).\n%s\n', problem.message );
			end

		%else file has not changed, warn that the previous version will be used
		else
			warning ( 'integrity_check:xml_load_warning', 'File ''%s'' has not changed since it was last successfully loaded (successful load on %s). Therefore it will not be parsed and saved to the database over again (loaded_file.is_load_ok=0)', file, datestr ( datenum ( date_loaded, 'mmm dd yyyy HH:MM:SS:FFFAM' ), 'mmm dd yyyy, HH:MM:SS' ) );
			insert_loaded_file ( file, 0 );
		end
	catch e
		try
			if ( loaded_file_id == 0 )
				insert_loaded_file ( file, -1 );
			end
		catch problem
			warning ( 'integrity_check:xml_load_warning', 'Problem loading and declaring load (file load NOT ok, file NOT added correctly to the ''loaded_file'' table).\n%s\n', problem.message );
		end
		warning ( 'integrity_check:xml_load_error', 'Problem loading xml file to database :\n%s\n=> attempt to load the previous version stored in the database.', e.message );
	end

function integrity = get_struct_from_xml ( file )
	integrity = struct ( 'shortcut', [], 'check', [] );
	
	xml = xmltools( file );
	warn_if_unexpected_children ( xml, { 'integrity' } );%warn_if_unexpected_children ( xml, { 'integrity1' } );
	int = xmltools ( xml.get_tag ( 'integrity' ) );
	warn_if_unexpected_children ( int, { 'shortcuts'; 'check' } );%warn_if_unexpected_children ( integrity, { 'shortcuts?'; 'check*' } );

	shortcuts = xmltools ( int.get_tag ( 'shortcuts' ) );
	warn_if_unexpected_children ( shortcuts, { 'shortcut' } );%warn_if_unexpected_children ( shortcuts, { 'shortcut*' } );
	integrity.shortcut = get_property_definitions ( shortcuts, 'shortcut' );

	checks = xmltools ( int.get_tag ( 'check' ) );
	nb_checks = checks.nb_children ();
	
	if ( nb_checks )
		check ( nb_checks, 1 ) = struct ( 'xml', [], 'mnemonic', [], 'name', [], 'active', [], 'cron_regexp', [], 'comment', [], ...
			'trigger', struct ( 'name', {}, 'type', {}, 'value', {} ), 'request', [], 'severity', [], ...
			'alarm', struct ( 'xml', {}, 'type', {}, 'scrutation', {}, 'active', {}, 'destination', {}, 'name', {}, 'message', {}, 'severity', {}, 'scope', {} ) );
	end
	
	for i = 1 : nb_checks
		check ( i ).xml = xmltools ( checks.keep_children ( i ) );
		warn_if_unexpected_children ( check ( i ).xml, { 'comment'; 'triggers'; 'request'; 'severity'; 'alarm' } );%warn_if_unexpected_children ( check ( i ).xml, { 'comment?'; 'triggers?'; 'request1'; 'severity1'; 'alarm+' } );
		check ( i ).name = check ( i ).xml.get_attrib_value ( 'check', 'name' );
		check ( i ).active = check ( i ).xml.get_attrib_value ( 'check', 'is_active' );
        if isempty ( regexp ( check ( i ).active, '[01]', 'once' ) )
            warning ( 'xml_parsing:unexpected_value', 'The is_active attribute should contain ''0'' for inactive or ''1'' for active (current value = ''%s'' ). Any value other than 1 is considered as meaning inactive.', check ( i ).active );
        end
		check ( i ).cron_regexp = check ( i ).xml.get_attrib_value ( 'check', 'cron_regexp' );
		if ( isempty ( check ( i ).cron_regexp ) )
			check ( i ).cron_regexp = get_cron_from_date_time ( check ( i ).xml.get_attrib_value ( 'check', 'datexp' ), check ( i ).xml.get_attrib_value ( 'check', 'timexp' ) );
		end
		
		check ( i ).comment = check ( i ).xml.get_tag_value ( 'comment' );

		triggers = xmltools ( check ( i ).xml.get_tag ( 'triggers' ) );
		warn_if_unexpected_children ( triggers, { 'trigger' } );%warn_if_unexpected_children ( triggers, { 'trigger*' } );
		check ( i ).trigger = get_property_definitions ( triggers, 'trigger' );
		
		check ( i ).request = get_property ( check ( i ).xml.get_tag ( 'request' ) );

		check ( i ).severity = get_property ( check ( i ).xml.get_tag ( 'severity' ) );
		
		alarms = xmltools ( check ( i ).xml.get_tag ( 'alarm' ) );
		nb_alarms = alarms.nb_children ();
		if ( nb_alarms > 0 )
			check ( i ).alarm ( nb_alarms, 1 ) = struct ( 'xml', [], 'type', [], 'scrutation', [], 'active', [], 'destination', [], 'name', [], 'message', [], 'severity', [], 'scope', struct ( 'name', {}, 'type', {}, 'value', {} ) );
			for j = 1 : nb_alarms
				check ( i ).alarm ( j ).xml = xmltools ( alarms.keep_children ( j ) );
	%TODO : could the following warning go outside the loop ? i.e. check for
	%unexpected children for all the alarms thanks to the xml_tools library ?
				warn_if_unexpected_children ( check ( i ).alarm ( j ).xml, { 'destination'; 'name'; 'message'; 'severity'; 'scopes' } );%warn_if_unexpected_children ( alarm, { 'destination1'; 'name1'; 'message1' } );
				check ( i ).alarm ( j ).type = check ( i ).alarm ( j ).xml.get_attrib_value ( 'alarm', 'type' );
				check ( i ).alarm ( j ).scrutation = check ( i ).alarm ( j ).xml.get_attrib_value ( 'alarm', 'scrutation' );
				check ( i ).alarm ( j ).active = check ( i ).alarm ( j ).xml.get_attrib_value ( 'alarm', 'is_active' );
				if isempty ( regexp ( check ( i ).alarm ( j ).active, '[01]', 'once' ) )
					warning ( 'xml_parsing:unexpected_value', 'The is_active attribute should contain ''0'' for inactive or ''1'' for active (current value = ''%s'' ). Any value other than 1 is considered as meaning inactive.', check ( i ).alarm ( j ).active );
				end
				check ( i ).alarm ( j ).destination = get_property ( check ( i ).alarm ( j ).xml.get_tag ( 'destination' ) );
				check ( i ).alarm ( j ).name = get_property ( check ( i ).alarm ( j ).xml.get_tag ( 'name' ) );
				check ( i ).alarm ( j ).message = get_property ( check ( i ).alarm ( j ).xml.get_tag ( 'message' ) );
				check ( i ).alarm ( j ).severity = get_property ( check ( i ).alarm ( j ).xml.get_tag ( 'severity' ) );
				scopes = xmltools ( check ( i ).alarm ( j ).xml.get_tag ( 'scopes' ) );
				if ( scopes.nb_children () )
					warn_if_unexpected_children ( scopes, { 'scope' } );
					check ( i ).alarm ( j ).scope = get_property_definitions ( scopes, 'scope' );
				end
			end
		end
		integrity.check = check;
	end

function save_struct_to_db ( integrity, loaded_file_id )
    global st_version;
	check = integrity.check;
    current_datetime = datestr ( now, 'mmm dd yyyy HH:MM:SS:FFFAM' );
	try
		for i = 1 : size ( integrity.shortcut, 1 )
			save_object_property ( 'loaded_file', 'shortcut', loaded_file_id, integrity.shortcut ( i ) );
		end

		%get the names of the checks to be deactivated (i.e. the ones
		%redefined by this file) and deactivate them.
		check_names = '';
		for i = 1 : size ( check, 1 )
			check_names = sprintf ( '%s, ''%s''', check_names, check ( i ).name(1:min(32,end)) );
		end
		exec_sql ( 'QUANT', sprintf ( 'update %s..int_check_def set active_to_dt=convert(datetime,''%s'') where active_to_dt is NULL and name in (%s)', st_version.bases.quant, current_datetime, check_names(2:end) ) );
		
		for i = 1 : size ( check, 1 )
			%TODO : replace by a call to se_db ?
			check_id = cell2mat ( exec_sql ( 'QUANT', sprintf('%s..ins_int_check_def %d, ''%s'', ''%s'', ''%s'', ''%s'', ''%s''', st_version.bases.quant, loaded_file_id, get_safe_str ( 'sql', check ( i ).name ), get_safe_str ( 'sql', check ( i ).cron_regexp ), get_safe_str ( 'sql', check ( i ).comment ), current_datetime, if3 ( strcmpi ( check ( i ).active, '1' ), 'NULL', current_datetime ) ) ) );
			save_object_property ( 'int_check_def', 'trigger', check_id, check ( i ).trigger );
			save_object_property ( 'int_check_def', 'request', check_id, check ( i ).request );
			save_object_property ( 'int_check_def', 'severity', check_id, check ( i ).severity );
			for j = 1 : size ( check ( i ).alarm, 1 )
				try
					alarm_id = cell2mat ( exec_sql ( 'QUANT', sprintf ( '%s..ins_int_alarm_def %d, ''%s'', ''%s'', %d', st_version.bases.quant, check_id, get_safe_str ( 'sql', check ( i ).alarm ( j ).type ), get_safe_str ( 'sql', check ( i ).alarm ( j ).scrutation ), if3 ( strcmpi ( check ( i ).alarm ( j ).active, '1' ), 1, 0 ) ) ) );
					save_object_property ( 'int_alarm_def', 'destination', alarm_id, check ( i ).alarm ( j ).destination );
					save_object_property ( 'int_alarm_def', 'name', alarm_id, check ( i ).alarm ( j ).name );
					save_object_property ( 'int_alarm_def', 'message', alarm_id, check ( i ).alarm ( j ).message );
					save_object_property ( 'int_alarm_def', 'severity', alarm_id, check ( i ).alarm ( j ).severity );
					save_object_property ( 'int_alarm_def', 'scope', alarm_id, check ( i ).alarm ( j ).scope );
				catch alarm_error
					rethrow ( alarm_error )
					%TODO : what should we do, continue without this
					%specific alarm or stop and rollback ? we could also
					%save a hard-coded non-failing alarm and continue...
				end
			end
		end
	catch check_error
		fprintf ( 'se_integrity_load_xml - An error has occured in se_integrity_load_xml/save_struct_to_db : attempting to "roll back" database to correspond to the previous successfully loaded XML version...\n' );
		try
			exec_sql ( 'QUANT', sprintf ( '%s..int_rollback %d', st_version.bases.quant, loaded_file_id ) );
			fprintf ( 'se_integrity_load_xml - rollback of loaded_file_id=%d has SUCCEEDED !\n', loaded_file_id );
		catch rollback_error
			fprintf ( 'se_integrity_load_xml - rollback of loaded_file_id=%d has FAILED !\nplease roll back manually.\n%s', loaded_file_id, rollback_error.message );
		end
        rethrow ( check_error );
	end
	