function o = st_coint_portfolio( varargin)
% ST_COINT_PORTFOLIO - coint�gration d'un portefeuille (niveau 1)
%
% entr�es :
% 1- le portefeuille
% 2- l'historique
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  @plot_ );
%%** Init       
function init( varargin)
    this.params = options({'var-level', .95,  ... % niveau de VaR
        'horizon (nb)', 10, ... % nb of steps
        'preprocessing:@', @(x)log(x), ... % preprocessing of data
        'learning-phase (nb)', 400 , ... % nb of steps
        'baskets-size', 5, ... % size of the coint baskets
        'basket-threshold', 0.01, ... % threshold on any basket value to keep it
        'plot:b', 1 }, ... % double plot or not
        varargin );
    this.memory = options({'mix', []});
end

%%** Exec
% La logique g�n�rale est la suivante :
% (1) Je cherche des paniers coint�gr�s de 5 valeurs pour chaque ligne du
%     portefeuille.
% (2) Je soustrai la droite de march� via le coint�gr� du CAC repr�sent�
%     dans le portefeuille (2 fois 5 de valeurs)
% (3) Sur ce qui me reste, je 
function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    portfolio = inputs{1};
    history   = inputs{2};
    %futur     = inputs{3};
    
    preproc   = this.params.get('preprocessing:@');
    
    %< Droite de march�
    % A ce stade approxim�e par la moyenne des titres.
    % Plus tard pourra �tre dans le panier de r�f�rence.
    market_index_history = mean(history.value,2);
    %market_index_futur   = mean(  futur.value,2);
    %>
    %< Nonempty portfolio
    idx_eps = abs(portfolio.value) < eps;
    portfolio.value(idx_eps)    = []; 
    portfolio.colnames(idx_eps) = [];
    portfolio_sum               = sum(abs(portfolio.value));
    portfolio_weights           = portfolio.value / portfolio_sum;
    idx_too_small               = abs(portfolio_weights)< ...
        sum(abs(portfolio_weights)) / length(portfolio_weights) / 20;
    portfolio_weights(idx_too_small) = [];
    portfolio.value(idx_too_small)   = []; 
    portfolio.colnames(idx_too_small)   = []; 
    portfolio_weights           = portfolio_weights(:);
    p_weights                   = portfolio_weights;
    portfolio_c_value           = sum(portfolio.value);
    history = st_data('keep', history, sprintf('%s;', portfolio.colnames{:}));
    D = length(history.date);
    H = this.params.get('learning-phase (nb)')-1;
    history = st_data('from-idx', history, (D-H:D)');
    %futur   = st_data('keep', futur  , sprintf('%s;', portfolio.colnames{:}));
    %>
    vr     = this.params.get('horizon (nb)');
    b_size = this.params.get( 'baskets-size');
    basket_th = this.params.get( 'basket-threshold');
    nbV    = length(portfolio.colnames);
    %< Recherche des coint�gr�s
    b_all_instruments      = zeros(nbV,nbV);    % a row by basket, a column by instrument
    basket_positions       = repmat(nan,nbV,1); % a row by basket
    ratio_positions        = repmat(nan,nbV,1); % a row by basket

    for n=1:nbV
        this_index          = repmat(true,nbV,1);
        this_index(n)       = false;
        this_instrument     = feval( preproc, history.value(:,n));
        other_instruments   = feval( preproc, history.value(:,this_index));
        [comp, b_all, z, z_std, DF_v, DF_p, z_sharpe] = FIND_BCOINT( this_instrument, other_instruments, ...
            min([b_size, size(other_instruments,2)]));
        b_all_instruments(n,this_index) = b_all;
        b_all_instruments(n,n) = -1;
        basket_positions(n) = DF_v;
        ratio_positions(n)  = z_sharpe;
    end
    %>
    b_all = b_all_instruments';
    %< Exclusion du CAC
    nb_coint_index  = b_size*2;
    this_instrument = feval( preproc, market_index_history(D-H:D,:) );
    portfolio_instr = feval( preproc, history.value );
    [comp, b_index, z, z_std, DF_v, DF_p, z_sharpe_index] = FIND_BCOINT( this_instrument, portfolio_instr, ...
        min([nb_coint_index, size(portfolio_instr,2)]));
    b_index      = b_index(:);
    alpha_market = MINABS( portfolio_weights, b_index);
    portfolio_weights_new = portfolio_weights - alpha_market * b_index;
    % je valide que je diminue la masse
    if sum(abs(portfolio_weights_new)) < sum(abs(portfolio_weights))
        portfolio_weights = portfolio_weights_new;
    else
        alpha_market = 0;
    end
    % alpha_market, b_index, z_sharpe_index
    replication = [];
    replication.history = st_data('init', 'title', 'Replication', ...
        'value', alpha_market * history.value * b_index, ...
        'date', history.date, 'colnames', {'Removed index'} );
    replication.info    = struct('weight',  alpha_market * b_index, ...
        'status', -sign(alpha_market * z_sharpe_index), ...
        'alpha', alpha_market, 'z', z_sharpe_index );
    replication.portfolio_elements = history.colnames ;
    %>
    %< Redistribution du portefeuille
    % Parmi les paniers coint�gr�s
    somme_max = [0.1 0.2 0.3 0.5 0.7];
    alpha_0   = randn(size(b_all,2),1);
    alpha_0   = alpha_0 / sum(abs(alpha_0));
    % R�sidu de la projection L1
    f_crit = @(x)sum(abs(portfolio_weights-b_all*x));
    optim_options = optimset('Display','off');
    warning off
    %< Boucle d'optimisation
    % Pour chaque somme maximum, je vais r�cup�rer l'optimum.
    % Parmi tous les optima ainsi d�tect�s, je prend celui qui laisse la
    % plus petite masse de favorables dans le portfeuille.
    best_portfolio_residual_mass = sum( abs( portfolio_weights));
    best_choice = [];
    for s=1:length(somme_max)
        st_log('st_coint_portfolio: optim loop %d/%d\n', s, length(somme_max));
        c = somme_max(s);
        [alpha, fval, exitflag ] = fmincon(f_crit, c*alpha_0, [],[],[],[],[],[], ...
            @(b)CONFUN(b,c),optim_options);
        ind_favorables = alpha .* ratio_positions < 0;
        weight_without_fav = portfolio_weights - b_all(:,ind_favorables) * alpha(ind_favorables);
        if sum( abs( weight_without_fav)) < best_portfolio_residual_mass
            best_portfolio_residual_mass = sum( abs( weight_without_fav));
            best_choice = alpha;
            st_log('  best config found : residual mass = %f\n', best_portfolio_residual_mass);
        end
    end
    %>
    warning on
    if ~isempty( best_choice)
        for a=1:length(best_choice)
            % je fabrique chaque panier de r�plication
            this_alpha = best_choice(a) * b_all(:,a);
            replication.history.value = [ replication.history.value, history.value * this_alpha];
            replication.history.colnames{end+1} = sprintf( 'Basket %d', a);
            replication.info(end+1)   = struct('weight', this_alpha , ...
                'status', -sign( best_choice(a) * ratio_positions(a)), ...
                'alpha', best_choice(a), 'z', ratio_positions(a) );

            % (status > 0) => favorable
        end
        % j'enl�ve les trop petits
        last_vals = replication.history.value(end,2:end);
        idx_null  = [false, abs(last_vals) < basket_th];
        replication.history.value(:,idx_null)  = [];
        replication.history.colnames(idx_null) = [];
        replication.info(idx_null)             = [];
        best_choice(idx_null(2:end))           = 0;
        % je fabrique le r�sidu
        portfolio_weights = portfolio_weights - b_all * best_choice(:);
        replication.history.value = [replication.history.value, history.value * portfolio_weights];
        replication.history.colnames{end+1} = 'Remaining' ;
        replication.info(end+1)   = struct('weight', portfolio_weights , ...
                'status', 0, ...
                'alpha', NaN , 'z', NaN );
    end
    %>
    
    % g�n�ration des historiques
    data1 = replication.history;
    data1.info.replication = replication.info;
    data1.info.original.weights  = p_weights;
    data1.info.original.colnames = portfolio.colnames;
    
    % g�n�ration de la description des pond�rations
    data2 = st_data('init', 'title', 'Weights', 'value', [p_weights(:), data1.info.replication.weight], ...
        'date', (1:length(portfolio.colnames))', 'rownames', portfolio.colnames, ...
        'colnames', { 'Original portfolio', data1.colnames{:} });
    data2.info.status = [0, [data1.info.replication.status]];
    data2.info.long_short = sign([NaN, [data1.info.replication.alpha ]]);
    data2.info.alpha      = [alpha_market, [data1.info.replication.alpha ]];
    data2.info.portfolio_value = portfolio_c_value;
    data2.info.portfolio_coef  = portfolio_c_value / (history.value(end,:) * p_weights);
    data2.info.portfolio_basket_value = sum(replication.history.value(end,2:end-1)) * data2.info.portfolio_coef;
    data2.info.baskets_value   = replication.history.value(end,:) * data2.info.portfolio_coef;
    
    % positions des stop/loss
    vals      = history.value * data2.value;
    var_level = this.params.get( 'var-level');
    alpha     = norminv(1-(1-var_level)/2);
    data2.info.portfolio_stops = repmat(vals(end,:),2,1) + [ alpha * std( vals); -alpha * std(vals)];
    st_log('Risky slice = %3.2f / %3.2f = %3.2f%%\n', ...
        data2.info.portfolio_coef * sum(-diff(data2.info.portfolio_stops(:,3:end-1))), ...
        data2.info.portfolio_basket_value, abs(100*data2.info.portfolio_coef * sum(-diff(data2.info.portfolio_stops(:,3:end-1))) / data2.info.portfolio_basket_value));
    
    % sortie
    outputs = { data1, data2 };
    
    if this.params.get('plot:b')
        opt = options(varargin);
        plot_focus( opt.get('title'));
        plot_( outputs);
    end
end

%%** Plot
function h = plot_( inputs, varargin)
    g = findobj( gcf, 'type', 'uipanel');
    status  = arrayfun(@(i)inputs{1}.info.replication(i).status, 1:length(inputs{1}.colnames)-1);
    alpha   = arrayfun(@(i)inputs{1}.info.replication(i).alpha , 1:length(inputs{1}.colnames)-1);
    fav_idx = status > 0;
    vals    = bsxfun(@times, inputs{1}.value(:,1:end-1), sign(alpha)) ;
    m_vals  = mean( vals);
    c_vals  = bsxfun( @plus, vals, -m_vals);
    s_vals  = std( vals);
    r_vals  = bsxfun( @times, c_vals, 1./s_vals);
    x_vals  = 1:size(c_vals,2);
    txt = {};
    for t=1:length( inputs{1}.colnames)-1
        if inputs{1}.info.replication(t).alpha > 0
            ptype = 'long';
        else
            ptype = 'short';
        end
        txt{end+1} = sprintf('%s (%s)', inputs{1}.colnames{t}, ptype);
    end
    stxt = {};
    for t=1:length( inputs{1}.colnames)-1
        if inputs{1}.info.replication(t).alpha > 0
            ptype = 'L';
        else
            ptype = 'S';
        end
        stxt{end+1} = ptype;
    end

    if false
        %< Portfolio and remaining
        subplot(5,1,1);
        val_p = sum(inputs{1}.value,2); % valeur totale du portfolio
        boxplot( [inputs{1}.value(:,end), val_p], 'orientation', 'horizontal', ...
            'labels', { inputs{1}.colnames{end} 'Original portfolio' } );
        hold on
        plot( [inputs{1}.value(end,end), val_p(end)], [1 2], 'dk', 'markerfacecolor', [0 0 0], 'markeredgecolor', [0 0 0])
        hold off
        %>
        %< Basket decomposition
        subplot(5,1,2:5);
        boxplot( c_vals, 'orientation', 'horizontal', ...
            'labels', txt );
        hold on
        plot( c_vals(end,fav_idx), x_vals(fav_idx) , 'dk', 'markerfacecolor', [0 .67 .12], 'markeredgecolor', [0 .67 .12])
        plot( c_vals(end,~fav_idx),x_vals(~fav_idx), 'dk', 'markerfacecolor', [.67 0 .12], 'markeredgecolor', [.67 0 .12])
        hold off
        ax = axis;
        z  = arrayfun(@(i)inputs{1}.info.replication(i).z, 1:length(inputs{1}.colnames)-1);
        text( repmat(ax(2), size(x_vals)), x_vals, tokenize(sprintf(' %3.2f;', z),';') ); % m_vals
        %>
    end
    weights   = [inputs{1}.info.replication.weight];
    if size(weights,2)<=1
        warning on
        subplot(3,1,1,'v6');
        cla;
        subplot(3,1,2:3,'v6');
        cla;
        warning off
        h = gcf;
        return
    end
    
    o_weights = inputs{1}.info.original.weights;
    
    magic_matrix = repmat(0,size(weights,1)+2, size(weights,2)*3+6);
    magic_matrix(3:end,1:2) = [abs(o_weights).*(o_weights<0), o_weights.*(o_weights>0)];
    for c=1:size(weights,2)
        magic_matrix(3:end,1+[3*c,3*c+1]) = [abs(weights(:,c)).*(weights(:,c)<0), weights(:,c).*(weights(:,c)>0)];
    end
    for c=1:size(weights,2)
        magic_matrix(1,1+[3*c,3*c+1]) = sum(magic_matrix(3:end,1+[3*c,3*c+1]));
    end
    % overall
    magic_matrix(3:end,end-2) = abs(sum(bsxfun(@times, weights(:,1:end-1),  fav_idx),2));
    magic_matrix(3:end,end-1) = abs(sum(bsxfun(@times, weights(:,1:end-1), ~fav_idx),2));
    magic_matrix(3:end,end)   = abs(sum(weights(:,end-1:end) ,2));
    
    n1 = size(weights,1);
    n2 = size(weights,2);
    
    warning on
    subplot(3,1,1,'v6');
    warning off
    boxplot( r_vals , 'orientation', 'vertical','labels', stxt);
    hold on
    plot( x_vals(fav_idx) , r_vals(end,fav_idx), 'dk', 'markerfacecolor', [0 .67 .12], 'markeredgecolor', [0 .67 .12])
    plot( x_vals(~fav_idx),r_vals(end,~fav_idx), 'dk', 'markerfacecolor', [.67 0 .12], 'markeredgecolor', [.67 0 .12])
    hold off
    ax = axis;
    axis([-.5 length(fav_idx)+3.1 ax(3:4)]);
    set(gca,'ytick',[],'yticklabel',[]); 
    ylabel('Relative positions');
    
    warning off
    subplot(3,1,2:3,'v6');
    warning on
    g = load( fullfile( fileparts( which(mfilename) ), 'cm4portfolio')); % charger cm
    cm = g.cm;
    imagesc( log(magic_matrix));
    colormap( (cm))
    hold on
    plot([0.5 0.5 2.5 2.5 0.5], 2.5 + [n1 0 0 n1 n1], 'k');
    for c=1:n2
        plot(3*c+[0.5 0.5 2.5 2.5 0.5], 2.5 + [n1 0 0 n1 n1], 'k');
        plot(3*c+[0.5 0.5 2.5 2.5 0.5], 0.5 + [1 0 0 1 1], 'k');
        text(3*c+[1,2],[1,1], { 'S', 'L'});
        text(3*c+1.5,2, inputs{1}.colnames{c},'horizontalalignment','center','fontsize',7);
    end
    text(1.5,2, 'Portfolio' ,'horizontalalignment','center');
    text(3*n2+3.5+1.5,1, 'Overall', 'horizontalalignment','center');
    text(3*n2+2.5+1.5,2, '+', 'horizontalalignment','center');
    text(3*n2+3.5+1.5,2, '-', 'horizontalalignment','center');
    text(3*n2+4.5+1.5,2, '0', 'horizontalalignment','center');
    
    %< Proportions en masse
    % calcul puis affichage
    norm_w = 1./sum(magic_matrix(3:end,1:2),2);
    overall_pct = bsxfun(@times, magic_matrix(3:end,end-2:end), norm_w);
    overall_txt = arrayfun( @NB2STR , overall_pct, 'uni', false);
    baskets_pct = bsxfun(@times, magic_matrix(3:end,4:end-3), norm_w);
    baskets_txt = arrayfun( @NB2STR , baskets_pct, 'uni', false);
    
    for c=1:n1
        text(3*n2+[2,3,4]+2, c+[2, 2, 2], overall_txt(c,:) , 'horizontalalignment','center', 'fontsize',8);
        for r=1:size(baskets_pct,2)
            if mod(r,3)~=0
               text(r+[0,0,0]+3, c+[2, 2, 2], baskets_txt(c,r) , 'horizontalalignment','center', 'fontsize',8);
            end
        end
    end
    %>
    plot(3*(n2+1) + [0.5 0.5 3.5 3.5 0.5], 2.5 + [n1 0 0 n1 n1], 'k');
    hold off
    ax = axis;
    axis(ax + [-.5 1.5 -.5 .5]);
    set(gca,'xtick',[],'xticklabel',[]);
    set(gca,'ytick',[],'yticklabel',[]);
    
    text(repmat(0,1,n1),2+(1:n1), inputs{1}.info.original.colnames, 'horizontalalignment','right');
    
    set(gcf, 'menubar','none')
    h = gcf;
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

%%** Basket coint
function [comp, b_all, z, z_std, DF_v, DF_p, z_sharpe] = FIND_BCOINT( y, x, nb)
    [b,se,pval,comp] = ...
        stepwisefit(x, y, 'display', 'off', 'maxiter', nb);
    x_trk = x(:,comp);
    b_all = zeros(size(comp));
    % j'ajoute une colonne de 1
    x_trk = cat(2,ones(size(x_trk,1),1),x_trk);
    % r�gression
    b     = inv(x_trk'*x_trk)*x_trk'*y;
    % dispatch des coef dans une ligne o� tout le monde � son indice
    b_all(comp) = b(2:end);
    % r�sidus
    z     = -y+x_trk*b;
    z_std = std(z);
    % diff�rentiel de la r�gression
    w     = diff(z);
    zf    = z(1:end-1);
    % j'ajoute une colonne de 1
    z1    = cat(2,ones(size(zf,1),1),zf); 
    % r�gression de delta z sur z
    g     = inv(z1'*z1)*z1'*w;
    % r�sidus
    r     = w-z1*g;
    s     = sqrt(mean(r.^2)/var(zf)/size(zf,1));
    DF_v  = g(2)/s;
    % sharpe du dernier z
    z_sharpe = z(end)/z_std;
    DF_p  = 0;
end

%%** Min abs
% le min est n�cessairement � un coin
function lambda = MINABS( w,b)
    fm = @(L)sum(abs(L*b-w));
    w_s_b = w./b;
    [tmp, ldx] = min( arrayfun(fm, w_s_b'));
    lambda = w_s_b(ldx);
end

%%** Contrainte de masse
function [c ceq] = CONFUN(b, seuil)
    c = sum(abs(b)) - seuil;
    ceq = 0;
end

function s = NB2STR( n)
    if n>=.05
        s = sprintf('%2.1f', round(10*n)/10);
    else
        s = '';
    end
end

end