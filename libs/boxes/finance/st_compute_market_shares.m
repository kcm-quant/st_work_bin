function o = st_compute_market_shares( varargin)
% ST_compute_market_shares
%
% See also st_node 
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({'names:td+turnover+phase', 'trading_destination_id;Turnover;day phase', ... % variables to work with (NOT USED!!!)
        'proba', .95, ...
        },varargin );
    this.memory = options({'n', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    data = keep_intraday_volume( 'period', inputs{1}, 'proba', this.params.get('proba'));
    %rownames = arrayfun(@(x)get_repository( 'td2em', x), st_data('col', data, 'trading_destination_id')); %td_id2td_name / , 'uni', false);
    %rownames = { rownames.name };
    %rownames = arrayfun(@(x)get_repository( 'td_id2td_name', x), st_data('col', data, 'trading_destination_id'), 'uni', false);
    %data.rownames = rownames;
    outputs = { data };
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end