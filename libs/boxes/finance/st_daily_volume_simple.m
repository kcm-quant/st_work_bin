function o = st_daily_volume_simple( varargin)
% ST_TARGET_CLOSE - module de recopie des entr?es en sorties
%   premier st_module r?alis?
%
% Le prototype d'un module est simple:
% - une m?thode  .init( varargin)
% - une m?thode  .exec( {input}  , varargin) -> {output}
% - une m?thode .learn( {{input}}, varargin) -> {} ou ![] (cst)
% - une m?thode  .plot( {output} , varargin)  -> h ou ![] (cst)
% - une m?thode .end_of_prod() -> true/false
% - une m?thode .get/.set qui permet de r?cup?r?r/modifier n'importe quoi
%
% d3 = read_dataset('gi:opt_trading', 'security_id',110, 'from', '01/10/2008', 'to', '01/10/2008', 'trading-destinations', {})
%
% L'acc?s ? ces m?thodes est verrouill? au niveau de st_node
%
% See also st_node
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
    'exec', @exec, ...
    'learn', [], ...
    'get', @get_, ...
    'set', @set_, ...
    'end_of_prod', @eop, ...
    'parallel', 1, ...
    'plot',  @plot_ );
%%** Init
    function init( varargin)
        this.params = options({ ...
            'security-id', NaN, ...
            'trading-destination-id', NaN ...
            }, varargin);
        this.memory = options({'n', 0});
    end

%%** Exec
    function outputs = exec( inputs, varargin)
        
        if st_data('isempty-nl', inputs{1}) ...
                || st_data('isempty-nl', inputs{2})
            outputs{1} = st_data('empty-init');
            return;
        end
        
        
        security_id = this.params.get('security-id');
        
        
        data = inputs{1};
        volume_proportion = inputs{2};
        as_of_date_num = unique(floor(data.date));
       
        dt = inputs{1}.date - unique(floor(inputs{1}.date));        
        data_tmp = st_data('cols', data, 'volume');
               
        
        outputs{1} = st_data('init', ...
            'title', 'Daily volume estimation', ...
            'date', as_of_date_num + dt, ...
            'value', cumsum(data_tmp), ...
            'colnames', {'daily_volume'});
        out.info= data.info;


        
    end

% < plot function
    function h=plot_(outputs, varargin)
        h = st_plot(outputs, varargin{:});
    end
% >

%%** End of production
    function b = eop
        b = true;
    end

%%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

end