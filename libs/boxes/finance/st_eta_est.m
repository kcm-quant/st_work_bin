function o = st_eta_est( varargin)
% ST_ETA_ETA -
%

this.params  = {};
this.memory  = {};

o = struct('init', @init, ...
    'exec', @exec, ...
    'learn', @learn, ...
    'end_of_prod', @eop, ...
    'get', @get_, ...
    'set', @set_, ...
    'plot',  @plot_ );
%%** Init
    function init( varargin)
        this.params = options({'plot:b', 1  ...                     % - automatic plot
            'autosave', 1 ...
            }, varargin);
    end

%%** Exec
    function outputs = exec( inputs, varargin)
        outputs = inputs;
        data = inputs{1} ;
        eta = [];
        dts = [];
        
        dt_days = floor(data.date);
        days = unique( dt_days);
        for d=1:length(days)
            idx_today = dt_days==days(d);
            this_data = st_data('from-idx', data, idx_today);
            
            if ~st_data('isempty-nl', this_data) && size(this_data.value, 1) > 20
                prices = st_data('col', this_data, 'price');
                dp     = diff(prices);
                prices(1) = [];
                prices(dp==0) = [];
                dp(dp==0)=[];
                
                % le tick est ici exprim� en num�raire
                tmp = unique(round(prices(prices > 1e-8)*1e8));
                tick = pgcd([unique(abs(diff(tmp(tmp > 0 & isfinite(tmp))))); tmp])*1e-8;
                Nc = sum( (sign(dp(1:end-1))==sign(dp(2:end))) & abs(abs(dp(2:end))-tick)<1e-8);
                Na = sum( (sign(dp(1:end-1))~=sign(dp(2:end))) & abs(abs(dp(2:end))-tick)<1e-8);
                
                if Na==0
                    this_eta = NaN;
                else
                    this_eta = ((Nc - Na)/Na +1)/2; %*tick;
                end
                
                X = prices - tick*(.5 - this_eta) * sign(dp);
                this_sigma = sqrt( sum( diff(log(X)).^2) * 256)  ;
                bad_sigma  = sqrt( sum( diff(log(prices)).^2) * 256)  ;
                
                data_gk = est_volatility('gk:1', this_data, 'window:date', datenum(0,0,0,0,10,0), ...
                    'step:date',  datenum(0,0,0,0,1,0), 'input-names:char', 'price');
                gk_sigma   = sqrt( mean( (data_gk.value).^2  * 8.5 * 6 * 256)) / 10000 * 100;
                
                %Calcul du eta pr chaque jours
                eta = [eta; Nc, Na, this_eta, this_sigma, bad_sigma, gk_sigma, mean(prices), length(prices), size(this_data.value,1), tick];
                dts = [dts; days(d)];
                
                utick=tick;
            else
                fprintf('...Empty data for day\n');
            end
        end
        
        data_eta = st_data( 'init', 'title', data.title, 'value', eta, ...
            'date', dts, ...
            'colnames', {'Nc', 'Na','\eta', '\hat\sigma', '\sigma','\sigma_{GK}', 'Mean price', 'nb changes', 'Nb trades', 'Tick size' }, ...
            'info', data.info);
        
        %valeur en sortie la valeur moyenne de la viscosit� sur une periode
        %donn�e
        outputs{1} = st_data('apply-formula', data_eta, '[mean({\eta}) mean({Mean price})]' , {'mean_eta' 'mean_price'}, 'st_data');
        
        if this.params.get('plot:b')
            eta = data_eta.value;
            stock = data_eta.title;
            stock_ric = data_eta.title(1:findstr('-',data_eta.title)-1);
            fname = sprintf('Eta / sigma - %s', stock);
            h = figure('name', fname, 'visible', 'off');
            utick = min(eta(:,end));
            tick = eta(:,end)/utick;
            plot(data_eta.date,tick.*eta(:,3));
            hold on
            plot(data_eta.date,tick.*eta(:,3),'.');
            idxm = tick<1.5;
            eta_f = ((sum(eta(idxm,1))-sum(eta(idxm,2)))/sum(eta(idxm,2))+1)/2;
            plot(data_eta.date([1 end]),[eta_f, eta_f], '--k', 'linewidth',2);
            plot(data_eta.date([1 end]),[1 1], ':r');
            hold off
            stitle('%s - mean price = %5.2f', stock,st_data( 'cols', outputs{1}, 'mean_price'));
            set(gca,'xticklabel',[]);
            ylabel('\eta')
            ax = axis;
            axis([data_eta.date([1 end])' ax(3:4)]);
            attach_date( 'init', 'date', data_eta.date)
            
            date_run = datestr(max(data_eta.date),'dd_mm_yyyy');
            if this.params.get('autosave')
                if isempty( dir(fullfile( getenv('st_repository'), 'plots','eta_indicator')))
                    mkdir( fullfile( getenv('st_repository'), 'plots','eta_indicator'));
                    if isempty( dir(fullfile( getenv('st_repository'), 'plots','eta_indicator',sprintf('%s',date_run ))))
                        mkdir( fullfile( getenv('st_repository'), 'plots','eta_indicator',sprintf('%s',date_run )));
                    end
                else
                    if isempty( dir(fullfile( getenv('st_repository'), 'plots','eta_indicator',sprintf('%s',date_run ))))
                        mkdir( fullfile( getenv('st_repository'), 'plots','eta_indicator',sprintf('%s',date_run )));
                    end
                end
                saveas( gcf, fullfile( getenv('st_repository'), 'plots','eta_indicator',sprintf('%s',date_run ),sprintf('viscosite_%d.png',inputs{1,1}.info.security_id ) ), 'png');
                
            end
        end
        
    end

    function h = plot_( inputs, varargin)
        read_dataset( 'plot-tickdb', inputs{1});
        h = gcf;
    end

%%** End of production
    function b = eop
        b = true;
    end
%%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

end