function o = st_liquidity_indic( varargin)
% ST_LIQUIDITY_INDIC - calcul d'indicateur de liquidit� sur un titre donn�
% Indicateurs utilis�s pour la construction de la fiche de liquidit�
%
% author   : 'dcroize@cheuvreux.com'
% reviewer : 
% version  : '1'
% date     : '23/12/2008'

this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({
        'plot:b', 1, ... 
        'index:name' , '', ...
        'autosave', 1, ...
        'from','01/01/1899', ...
        'to','01/01/1899', ...
        'window:char', 'day|60', ...
        },varargin );
    this.memory = options({'liquidity_indic', {}});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs,idx] = st_data('isempty', inputs);
    data_day = inputs{1};
    data_day_pr_place = st_data('where', inputs{1}, sprintf('{Trading_destination}==%d', min(st_data('cols', data_day , 'trading_destination')))); 
        
    %Statistiques de variation    
    %volatility daily German Klass
    hlco    = st_data('cols', data_day_pr_place , 'High;Low;Close;Open');
    s       = est_volatility('gk:ohlc', hlco(:,[4 1 2 3]));
    vol_day = sqrt(mean(s.^2) * 256);
    
    %volatlity close/close
    vol_cc = std(diff(hlco(:,3))./hlco(1:end-1,3)) * sqrt(256);
    
    %Rendements annuels
    price = st_data('cols', data_day_pr_place , 'Close');
    mu    = (mean(price(2:end)./price(1:end-1))^256-1) * 100;      %rendement an pourcent
    mu_h  = ((price(end)/price(1))^(256/length(data_day_pr_place.date))-1)*100; %rendement an pourcent
       
    first_p    = price(1);
    last_price = price(end);
    mean_price = mean(price);
     
    title_ric = data_day.title(1:findstr('(',data_day.title)-1);
    
    %Historique Trading destination 
    out       = transversal_TDrepartition(data_day , 0,'',title_ric );
    data_Td   = out{1};
    data_Td.value = [data_Td.value; zeros(6- size(data_Td.value,1),8)] %%ATTENTION C ECRIT EN DUR EN FONCTION DU MAX DE CONTEXTES DECIDE
    data_Td.date  = [1:6]                       %%ATTENTION C ECRIT EN DUR EN FONCTION DU MAX DE CONTEXTES DECIDE
    
    vals      = zeros(6,7);
    vals(1,:) = [vol_day*100 vol_cc*100 mu mu_h first_p last_price mean_price];
    data_Td   = st_data('add-col', data_Td, vals, 'vol_day;vol_cc;mu;mu_h;first_p;last_price;mean_price');
    outputs{1} =  data_Td;
    this.memory.set('liquidity_indic', data_Td);
    
    %Boxplot utile pour un domain correspondant � un indice seulement...
    
        
        
end

%%** Plot interne
function h = plot_(inputs, varargin)  
    h = figure('visible', 'off');
    dts  = inputs{1}.date;
    hlco = st_data('col', inputs{1}, 'High;Low;Close;Open');
    a1=subplot(4,1,1:2);
    candle(hlco(:,1), hlco(:,2), hlco(:,3), hlco(:,4), [.67 0 .12]);
    ax = axis;
    axis([1 size(hlco,1) ax(3:4)]);
    set(gca,'xticklabel',[]);
    title(inputs{1}.title);
    ylabel('OHLC');
    a2=subplot(4,1,3);
    s = est_volatility('gk:ohlc', hlco(:,[4 1 2 3])) * sqrt(256) *100;
    plot(s,'linewidth',2,'color',[0 .67 .12]);
    set(gca,'xticklabel',[]);
    ylabel('\sigma_{GK} (pct)');
    a3=subplot(4,1,4);
    g = stem( st_data('col', inputs{1}, 'Volume'));
    set(g,'marker', 'none', 'linewidth', 2);
    ylabel('Volume');
    linkaxes([a1,a2,a3],'x');
    set(gca,'xticklabel',[]);
    t = get(gca,'xtick');
    text(t,repmat(0,size(t)),datestr(inputs{1}.date(t),'dd/mm'), ...
        'HorizontalAlignment','right','rotation',90, 'fontsize', 8);
    h = gcf;
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end