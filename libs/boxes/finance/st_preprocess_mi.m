function o = st_preprocess_mi( varargin)
% ST_PREPROCESS_MI - extract data for market impact computation
%
% See also st_node
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
    'exec', @exec, ...
    'learn', [], ...
    'get', @get_, ...
    'set', @set_, ...
    'end_of_prod', @eop, ...
    'plot',  []);
%%** Init
    function init( varargin)
        this.params = options(...
            {'colname:char', 'volume;volume_sell;vwap;vwap_sell;open;vwas', ... % colname to use
            'lag_after_opening',datenum(0,0,0,0,30,0), ... % function to use for agregation
            'lag_before_closing',datenum(0,0,0,0,15,0),...
            'criterion_median_threshold',0.05,...
            'imbalance_add_threshold',0.7 ,...
            'q_dS_threshold',0.85,...
            'window:time', datenum(0,0,0,0,15,0),...
            'step:time', datenum(0,0,0,0,0,30)}, ... % function to use for difference
            varargin );
        this.memory = options({'n', []});
    end

%%** Exec
    function outputs = exec( inputs, varargin)
        
        NB_DAYS_MIN_TO_COMPUTE_PREPROCESS=20;
        window_time=this.params.get('window:time');
        step_time=this.params.get('step:time');
        
        %%%%%%% test
        [outputs, b] = st_data('isempty', inputs);
        if b
            return
        end
        
        %%%%%%% test de last date
        last_official_date = inputs{1}.info.data_datestamp;
        is_last_official_date = floor(inputs{1}.date(end))==last_official_date;
        if ~is_last_official_date
            outputs{1}.value=[];
            outputs{1}.date=[];
            outputs{1}.colnames=[];
            return
        end
        
        %%%%%%% create data
        colname  = this.params.get('colname:char');
        colname_cell  = tokenize(colname,';');
        
        try
            u_data={};
            for i_col=1:length(colname_cell)
                [u_data_tmp, outputs_tmp, inputs{1}, has_changed] = safer_bin_separator(outputs{1}, inputs{1}, colname_cell{i_col});
                u_data_tmp=st_data('transpose',u_data_tmp);
                u_data=cat(2,u_data,u_data_tmp);
            end
        catch
            error('st_preprocess_mi:error in aggregating data');
        end
        
        if size(u_data{1}.value,2)<NB_DAYS_MIN_TO_COMPUTE_PREPROCESS
            outputs{1}.value=[];
            outputs{1}.date=[];
            outputs{1}.colnames=[];
            return
        end
        
        % static data
        bin_hours=transpose(cellfun(@(c)(datenum(c,'HH:MM:SS:FFF')),u_data{1}.rownames));
        bin_hours=bin_hours-floor(bin_hours);
        
        hours=last_official_date+bin_hours;
        volume=u_data{strcmpi(colname_cell,'volume')}.value;
        volume_sell=u_data{strcmpi(colname_cell,'volume_sell')}.value;
        open=u_data{strcmpi(colname_cell,'open')}.value;
        vwap=u_data{strcmpi(colname_cell,'vwap')}.value;
        vwap_sell=u_data{strcmpi(colname_cell,'vwap_sell')}.value;
        vwas=u_data{strcmpi(colname_cell,'vwas')}.value;
        volume_buy=volume-volume_sell;
        vwap_buy=(vwap-(volume_sell./volume).*vwap_sell).*(volume./volume_buy);
        vwap_buy(volume_sell==0)=vwap(volume_sell==0);
        
        spread=10000*vwas./vwap;
        imbalance_volume=volume_sell./volume;
        dS=10000*(vwap-open)./open;
        dS_buy=10000*(vwap_buy-open)./open;
        dS_sell=10000*(vwap_sell-open)./open;
        coherence=1*((volume==0)|(imbalance_volume>=0.5 & dS<=0)|(imbalance_volume<0.5 & dS>0));
        
        % static median data
        critere_median=mean(sum(volume==0)/size(volume,1));
        if critere_median<=this.params.get('criterion_median_threshold')
            median_func=@(x)(nanmedian(x,2));
        else
            median_func=@(x)(nanmean(x,2));
        end
        volume_curve=median_func(volume(:,1:end-1)); % on ne prend pas en compte le jour OK
        spread_curve=nanmedian(spread(:,1:end-1),2);
        sur_volume=volume-repmat(volume_curve,1,size(volume,2));
        
        % handling 0 value
        %imbalance_volume(volume==0)=0;
        vwap_buy(volume_buy==0)=0;
        vwap_sell(volume_sell==0)=0;
        dS(volume==0)=0;
        dS_buy(volume_buy==0)=0;
        dS_sell(volume_sell==0)=0;
        
        % delta data
        step_by_win=uint16(window_time/step_time);
        idx_valid=uint16(1:size(dS,1));
        repmat_lag_start=repmat(nan,step_by_win,size(dS,2));
        
        dS_lag=cat(1,repmat_lag_start,dS(idx_valid(1):idx_valid(end-step_by_win),:));
        volume_buy_lag=cat(1,repmat_lag_start,volume_buy(idx_valid(1):idx_valid(end-step_by_win),:));
        volume_sell_lag=cat(1,repmat_lag_start,volume_sell(idx_valid(1):idx_valid(end-step_by_win),:));
        vwap_buy_lag=cat(1,repmat_lag_start,vwap_buy(idx_valid(1):idx_valid(end-step_by_win),:));
        vwap_sell_lag=cat(1,repmat_lag_start,vwap_sell(idx_valid(1):idx_valid(end-step_by_win),:));
        delta_volume=cat(1,repmat_lag_start,volume(idx_valid(step_by_win+1):idx_valid(end),:)-volume(idx_valid(1):idx_valid(end-step_by_win),:));
        delta_volume_buy=cat(1,repmat_lag_start,volume_buy(idx_valid(step_by_win+1):idx_valid(end),:)-volume_buy(idx_valid(1):idx_valid(end-step_by_win),:));
        delta_volume_sell=cat(1,repmat_lag_start,volume_sell(idx_valid(step_by_win+1):idx_valid(end),:)-volume_sell(idx_valid(1):idx_valid(end-step_by_win),:));
        delta_volume_detrend=cat(1,repmat_lag_start,sur_volume(idx_valid(step_by_win+1):idx_valid(end),:)-sur_volume(idx_valid(1):idx_valid(end-step_by_win),:));
        imbalance_ajout_buy=delta_volume_buy./(delta_volume_buy+delta_volume_sell);
        imbalance_ajout_sell=delta_volume_sell./(delta_volume_buy+delta_volume_sell);
        
        rho_v=bsxfun(@times,delta_volume,1./volume_curve);
        rho_dv=bsxfun(@times,delta_volume_detrend,1./volume_curve);
        
        %%%%%%% data selection
        % trading time handling
        trtime_info=trading_time(u_data{1}.info.td_info,last_official_date);
        start_trading_hours=trtime_info.opening;
        end_trading_hours=min(trtime_info.closing_auction,trtime_info.closing);
        limit_hours_start=start_trading_hours+window_time+this.params.get('lag_after_opening');
        limit_hours_end=end_trading_hours-this.params.get('lag_before_closing');
        % idx
        idx_good_data=repmat(bin_hours>=limit_hours_start & bin_hours<limit_hours_end,1,size(dS,2)) & coherence==1;
        idx_ajout_buy=idx_good_data & ...
            delta_volume_detrend>0 & delta_volume>0 & ...
            delta_volume_buy>0 & delta_volume_buy>delta_volume_sell &...
            imbalance_ajout_buy>this.params.get('imbalance_add_threshold') & imbalance_ajout_buy<=1;
        
        idx_ajout_sell=idx_good_data &...
            delta_volume_detrend>0 & delta_volume>0 & ...
            delta_volume_sell>0 & delta_volume_sell>delta_volume_buy & ...
            imbalance_ajout_sell>this.params.get('imbalance_add_threshold')  & imbalance_ajout_sell<=1;
        
        %%% ATTENTION, pour des actions tres peu liquide, dS_la_limit va
        %%% valoir 0 -> sum(idx_mi)=0 -> pas de data du preprocess
        dS_lag_limit=quantile(abs(dS(idx_good_data)),this.params.get('q_dS_threshold'));
        spread_pondere=nansum(volume_curve(2:end-1)./sum(volume_curve(2:end-1)).*spread_curve(2:end-1));
        
        idx_restreint= abs(dS_lag)<dS_lag_limit & ...
            (volume_buy==0 | volume_sell==0 | (vwap_buy-vwap_sell)>-1*(spread_pondere)/10000*vwap_sell) & ...
            (volume_buy_lag==0 | volume_sell_lag==0 | (vwap_buy_lag-vwap_sell_lag)>-1*(spread_pondere)/10000*vwap_sell_lag);
        
        idx_mi=(idx_ajout_buy | idx_ajout_sell) & idx_restreint;
        
        %%%%%%% create ouput
        idx_keep=find(idx_mi(:,end));
        
        delta_dS=dS-dS_lag;
        delta_dS(idx_ajout_sell)=-delta_dS(idx_ajout_sell);
        delta_dS=delta_dS(idx_keep,end);
        
        delta_dS_side=dS_buy-dS_lag;
        delta_dS_side(idx_ajout_sell)=-(dS_sell(idx_ajout_sell)-dS_lag(idx_ajout_sell));
        delta_dS_side=delta_dS_side(idx_keep,end);
        
        rho_v=rho_v(idx_keep,end);
        rho_dv=rho_dv(idx_keep,end);
        
        spread_p=repmat(spread_pondere,length(idx_keep),1);
        
        outputs{1}.value = cat(2,delta_dS,delta_dS_side,rho_v,rho_dv,spread_p);
        outputs{1}.date = hours(idx_keep);
        outputs{1}.colnames = {'delta_dS','delta_dS_side','rho_v','rho_dv','spread_p'};
        
    end


%%** End of production
    function b = eop
        b = true;
    end

%%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

end