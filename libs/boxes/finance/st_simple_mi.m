function o = st_simple_mi( varargin)
% ST_SIMPLE_MI - simple market impact model
%
% See also
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
    'exec', @exec, ...
    'learn', [], ...
    'get', @get_, ...
    'set', @set_, ...
    'end_of_prod', @eop, ...
    'plot',  @plot_ );
%%** Init
    function init( varargin)
        this.params = options({'estimation-mode', 'sequencial', ... % estimation mode
            'mi_mode', 'simple', ... % estimation mode
            'colnames:char', 'rho;dS;2psi', ... % colnames to use for rho,dS and spread
            'spread-weight', 0.8,  ... % weight of the spread
            'plot:b', 1 ... % autoplot
            },varargin );
        this.memory = options({'params', []});
    end

%%** Exec
    function outputs = exec( inputs, varargin)
        
        %%% global
        DELTA_R=0.15;
        
        [outputs, b] = st_data('isempty', inputs);
        if b
            outputs{end+1} = st_data('empty');
            error('st_simple_mi:no_data', 'NOT_ENOUGH_DATA: no data coming out from preprocessing');
        end
        % * inputs : one input with all synchronized variables in it
        [outp, q, params] = market_impact(this.params.get('mi_mode'), inputs{1}, ...
            'delta-r', DELTA_R, ...
            'spread-weight', this.params.get('spread-weight'), ...
            'colnames', this.params.get('colnames:char'), ...
            'overvolume', false);
        
        if isempty(q)
            error('st_simple_mi:no_data', 'NOT_ENOUGH_DATA: not enough data with significant Market Impact to build a model');
        end
        
        this.memory.set('params', params);
        
        %<* Build output
        outputs = outp;
        quality = q;
        
        outputs{1}.info.run_quality = tanh(10/quality);
        %>*
        
        %<* Plot if needed
        if this.params.get('plot:b')
            opt = options(varargin);
            h = plot_focus( opt.get('title'));
            plot_( outputs,this.params.get('mi_mode'));
        end
        %>*
    end

%%** Plot
    function h = plot_( inputs, varargin)
        h = gcf;
        set(h,'Visible','on')
        
        params = this.memory.get('params');
        
        f_MI = @(r)params.s0 + (r>0).*( params.kappa .* r.^params.gamma );
        
        rsp = st_data('col', inputs{2}, this.params.get('colnames:char'));
        if ~isempty(varargin)
            mode=varargin{1};
            if strcmpi(mode,'simple_03')
                rho  = rsp(:,strcmpi(inputs{2}.colnames,'rho_v'));
                dS  = rsp(:,strcmpi(inputs{2}.colnames,'delta_dS_side'));
                psi = rsp(:,strcmpi(inputs{2}.colnames,'spread_p'));
            else
                rho = rsp(:,1);
                dS  = rsp(:,2);
                psi = rsp(:,3);
            end
        else
            rho = rsp(:,1);
            dS  = rsp(:,2);
            psi = rsp(:,3);
            
        end
        
        rs = linspace(min(rho),max(rho),100);
        dS_hat =  f_MI( rs);
        
        %f_r2p = @(r)r./(r+1)*100;
        f_r2p = @(r)r*100;
        

        subplot(3,3,1);
        fill( params.s0_x, params.s0_y/max(params.s0_y),'r', 'facecolor', [.67 0 .12]);%, 'edgecolor', 'none');
        ax = axis;
        hold on
        plot([params.s0 params.s0] , ax(3:4), 'k', 'linewidth',2);
        text( params.s0, mean(ax(3:4)), sprintf(' s_0=%3.2f', params.s0), 'horizontalalignment', 'left', 'fontweight', 'bold');
        hold off
        %xlabel('s_0');
        
        subplot(3,3,2);
        fill( params.kappa_x, params.kappa_y/max(params.kappa_y),'r', 'facecolor', [.67 0 .12]);%, 'edgecolor', 'none');
        ax = axis;
        hold on
        plot([params.kappa params.kappa] , ax(3:4), 'k', 'linewidth',2);
        text( params.kappa, mean(ax(3:4)), sprintf(' \\ka8ppa=%3.2f', params.kappa), 'horizontalalignment', 'left', 'fontweight', 'bold');
        hold off
        %xlabel('\kappa');
        title( inputs{2}.title);
        
        subplot(3,3,3);
        fill( params.gamma_x, params.gamma_y/max(params.gamma_y),'r', 'facecolor', [.67 0 .12]);%, 'edgecolor', 'none');
        ax = axis;
        hold on
        plot([params.gamma params.gamma] , ax(3:4), 'k', 'linewidth',2);
        text( params.gamma, mean(ax(3:4)), sprintf(' \\gamma=%3.2f', params.gamma), 'horizontalalignment', 'left', 'fontweight', 'bold');
        hold off
        %xlabel('\gamma');
        
        subplot(3,3,4:9);
        plot(f_r2p(rho), dS, '.', 'linewidth', 2);
        hold on
        plot(f_r2p(rs), dS_hat, '-r', 'linewidth', 2);
        hold off
        hline(0,'-k');hline(params.s0,'-k','s0');hline(params.kappa+params.s0,'-k','K+s0');vline(100,'-k');
        ax = axis;
        axis([0 f_r2p(rs(end)) ax(3:4)]);
        xlabel('Pct volume market');
        ylabel('Market Impact');
        
    end

%%** End of production
    function b = eop
        b = true;
    end

%%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

end