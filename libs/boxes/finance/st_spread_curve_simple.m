function o = st_spread_curve_simple( varargin)
% ST_SPREAD_CURVE_SIMPLE - fonction qui renvoie le profil intra day de
% la fourchette bid-ask pour un titre sur une p�riode donn�e
%

this.params  = {};
this.memory  = {};

o = struct('init', @init, ...
    'exec', @exec, ...
    'learn', @learn, ...
    'end_of_prod', @eop, ...
    'get', @get_, ...
    'set', @set_, ...
    'plot',  @plot_ );
%%** Init
    function init( varargin)
        this.params = options({'plot:b', 1  ...                     % - automatic plot
            'autosave', 1 ...
            }, varargin);
    end

%%** Exec
    function outputs = exec( inputs, varargin)
        outputs = inputs;
        data = inputs{1} ;
       
        rpdata = st_data('iday2array', data, 'vwas');
        rwdata = st_data('iday2array', data, 'vwap');
        rpdata.value = rpdata.value./rwdata.value*10000;
        spread_curve = nanmedian(rpdata.value,2);
        date_run = datestr(max(data.date),'dd_mm_yyyy');
        
        end_of_first_slice = rpdata.date(2);
        alpha = std(log(rpdata.value(~isnan(rpdata.value))));
        
        data_info =  st_data( 'init', 'title', 'mes infos heure de quotation/interval de confiance', 'value',[end_of_first_slice;alpha], ...
            'date', [-2,-1]', 'colnames', {'value'},'rownames', {'end_of_first_slice','alpha'}');
         
        data_f = st_data( 'init', 'title', 'My vwas(spread)curve', 'value', spread_curve, ...
            'date', (1:size(spread_curve,1))', 'colnames', {'value'});
        
        slice_num = 1;
        data_f.rownames = cell(length(data_f.date), 1);
        for i=1:size(spread_curve,1)
            data_f.rownames{i}  =  MAKE_PARAM_NAME(slice_num);
            slice_num = slice_num + 1;
        end
        data_out = st_data('stack', data_info, data_f) ;
        
        %construction du run quality
        moy_curve = nanmedian(log(rpdata.value),2); %== C
        var_i = alpha^2;
        deg_lib = size(spread_curve,1);
        nb_j = size(rpdata.value,2);
        
        p_chi=[];
        for i=1:nb_j
            Qj = sum((1/sqrt(var_i).*(moy_curve - log(rpdata.value(:,i)))).^2);
            p_chi =[p_chi; 1- chi2cdf(Qj,deg_lib)] ;
        end
        
        %hypoth�se jours iid
        quality_run = (prod(p_chi))^(1/nb_j);
        data_out.info.run_quality = quality_run;
        data_out.info.moy_curve = moy_curve;
        data_out.info.var_i = var_i;
        data_out.info.deg_lib = deg_lib;
        
        outputs = {data_out};
        st_log('st_spread_curve_simple: end of run quality...\n');
              
    end

    function h = plot_( inputs, varargin)
        h = [];
    end

%%** End of production
    function b = eop
        b = true;
    end
%%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

end

function str = MAKE_PARAM_NAME(i)
str = num2str(i);
str = ['slice' repmat('0', 1, 3-length(str)) str];
end