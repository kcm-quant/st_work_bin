function o = st_trend( varargin)
% ST_VWAP_TREND - module de recopie des entr?es en sorties
%   premier st_module r?alis?
%
% Le prototype d'un module est simple:
% - une m?thode  .init( varargin)
% - une m?thode  .exec( {input}  , varargin) -> {output}
% - une m?thode .learn( {{input}}, varargin) -> {} ou ![] (cst)
% - une m?thode  .plot( {output} , varargin)  -> h ou ![] (cst)
% - une m?thode .end_of_prod() -> true/false
% - une m?thode .get/.set qui permet de r?cup?r?r/modifier n'importe quoi
%
% d3 = read_dataset('gi:opt_trading', 'security_id',110, 'from', '01/10/2008', 'to', '01/10/2008', 'trading-destinations', {})
%
% L'acc?s ? ces m?thodes est verrouill? au niveau de st_node
%
% See also st_node
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
    'exec', @exec, ...
    'learn', [], ...
    'get', @get_, ...
    'set', @set_, ...
    'end_of_prod', @eop, ...
    'parallel', 1, ...
    'plot',  @plot_ );
%%** Init
    function init( varargin)
        this.params = options({ ...
            'security-id', NaN, ...
            'trading-destination-id', NaN ...
            }, varargin);
        this.memory = options({'n', 0});
    end

%%** Exec
    function outputs = exec( inputs, varargin)
        
        
        for m=1:length(inputs)
            if st_data('isempty-nl', inputs{m})
                outputs{1} = st_data('empty-init');
                return;
            end
        end
        
        
        % Load data
        % %%%%%%%%%
        
        data = inputs{1};
        volatility_curve = inputs{2};
        
        
        
        % Apprentissage trend
        % %%%%%%%%%%%%%%%%%%%
        
        from_num = min(floor(data.date));
        to_num = max(floor(data.date)) - 1;
        as_of_date = unique(floor(volatility_curve.date));
        
        dt = (from_num:to_num);
        diff_vwap_tmp = [];
        
        for i=1:length(dt)
            
            data_tmp = st_data('extract-day', data, datestr(dt(i), 'dd/mm/yyyy'), 'format', 'dd/mm/yyyy');
            
            if st_data('isempty-nl', data_tmp)
                continue;
            end
            
            vwap_tmp = st_data('cols', data_tmp, 'vwap');
            diff_vwap_tmp = [diff_vwap_tmp; arrayfun(@(x) (vwap_tmp(x) - vwap_tmp(1)) / (x-1), 1:length(vwap_tmp))']; %#ok<AGROW>
            
        end
        
        alpha_bar = nanmean(diff_vwap_tmp);
        nu = nanstd(diff_vwap_tmp);
        
        
        
        % Estimation du trend
        % %%%%%%%%%%%%%%%%%%%
        
        data_tmp = st_data('extract-day', data, as_of_date, 'format', 'dd/mm/yyyy');
        
        if st_data('isempty-nl', data_tmp)
            outputs{1} = st_data('empty-init');
            return;
        end
        
        
        vwap = st_data('keep-cols', data_tmp, 'vwap');
        
        diff_vwap = st_data('init', ...
            'title', 'Diff vwap', ...
            'date', vwap.date, ...
            'value', arrayfun(@(x) vwap.value(x) - vwap.value(1), 1:length(vwap.value))', ...
            'colnames', 'vwap');
       
        tmp = my_intersect({ ...
            st_data('keep-cols', diff_vwap, 'vwap'), ...
            st_data('keep-cols', data_tmp, 'vwap'), ...
            st_data('keep-cols', volatility_curve, 'vol_GK'), ...
            });
        
        diff_vwap_val = tmp{1}.value(:);
        vwap_val = tmp{2}.value(:);
        vol_GK_val = tmp{3}.value(:);
        
        sigma_tmp = 1e-4 * vol_GK_val * vwap_val(1);
        
        alpha_mean = (alpha_bar * sigma_tmp .^ 2 + nu ^ 2 * diff_vwap_val) ./ (sigma_tmp .^ 2 + nu ^ 2 * (1:length(sigma_tmp))');
        alpha_std = sqrt(nu ^ 2 * sigma_tmp .^ 2 ./ (sigma_tmp .^ 2 + nu ^ 2 * (1:length(sigma_tmp))'));
        
        % Pour le mettre en point de base
        alpha_mean = alpha_mean * 1e4 / median(vwap_val);
        alpha_std = alpha_std * 1e4 / median(vwap_val);
        
        outputs{1} = st_data('init', ...
            'title', 'Trend', ...
            'date', tmp{1}.date, ...
            'value', [alpha_mean, alpha_std], ...
            'colnames', {'trend_mean', 'trend_std'} ...
            );
        
        
        
        
        % Local function
        % %%%%%%%%%%%%%%
        
        function cell_st_data = my_intersect(cell_st_data)
            
            EPS = datenum(0,0,0,0,1,0);
            
            my_dates2intersect = cell(length(cell_st_data), 1);            
            idx2keep = cell(length(cell_st_data), 1);
           
            for j = 1 : length(my_dates2intersect)
                my_dates2intersect{j} = EPS * round(cell_st_data{j}.date / EPS);
            end
         
            [tmp,idx2keep{:}] = chx_intersect(my_dates2intersect{:});
            
            for j = 1 : length(cell_st_data)
                cell_st_data{j} = st_data('from-idx', cell_st_data{j}, idx2keep{j});
            end
        end
        
        
    end

% < plot function
    function h=plot_(outputs, varargin)
        h = st_plot(outputs, varargin{:});
    end
% >

%%** End of production
    function b = eop
        b = true;
    end

%%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

end