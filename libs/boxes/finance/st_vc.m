function o = st_vc( varargin)
% ST_VC - module qui fait des saisonnalit� sur  du volume
% uniquement pour l'instant
%
% sert pour le graph indicator vc
%
% opt = {'seas_func', @nanmedian, ...
%     'step:time', datenum(0,0,0,0,5,0),...
%     'window_spec:char', sprintf('%s|%d', 'day', 180),...
%     'mode_season', 'gaussian curve'};
%
% data = read_dataset(['gi:vc/' opt2str(options(opt))], ...
%     'security_id', 110,...
%     'from', '01/01/2009',...
%     'to', '01/01/2009', ...
%     'trading-destinations',[]);
%
%
% author   : 'rburgot@cheuvreux.com'
% reviewer : ''
% version  : '1.1'
% date     : '27/08/2008'
%
% See also st_node gi_create from_buffer

this.params = {};
this.memory = {};

o = struct('init', @init, ...
    'exec', @exec, ...
    'learn', [], ...
    'get', @get_, ...
    'set', @set_, ...
    'parallel', true, ...
    'end_of_prod', @eop, ...
    'plot',  [] );

clear varargin;

%%** Init
    function init( varargin)
        this.params = options({...
            'mode_season', 'std', ...
            'seas_func', @nanmedian
            }, varargin);
        this.memory = {};
    end

%%** Exec
    function outputs = exec( inputs, varargin)
        % cas des courbes valeurs
        MINIMAL_NB_OBS_4_STOCKS = 60; % le contexte principal n'est calcul� que si'lon dispose d'au moins cette quantit� de donn�es
        ADD_NB_OBS_4_DATA_FILTERING = 20; % les donn�es ne sont filtr�es pas ACP que si'lon dispose d'au moins MINIMAL_NB_OBS_4_STOCKS + ADD_NB_OBS_4_DATA_FILTERING jours de donn�es
        MINIMAL_NEW_HOURS_TRADING_DAYS = 20; % si les horaires de quotation ont chang� depuis au moins ce nombre de jours, alors on calcule une courbe de volume d�grad�e en utilisant les anciennes et les nouvelles donn�es
        % cas des courbes indice ou groupe de cotation
        MINIMAL_NB_OBS_4_INDEX = 200;
        % tol�rance en minutes pour un d�calage d'horaire de cotation
        TH_CHANGE_TOL =  datenum(0,0,0,0,5,1);
        % constante pour le score minimal par acp
        PCA_CL = 0.1;
        
        
        mode = this.params.get('mode_season');
        colnames = 'Usual day';
        
        switch mode
            case 'static_agg'
                [outputs{1}, b] = st_data('isempty', inputs{1});
                if b
                    return
                end
                
                data = bin_separator(st_data('keep', inputs{1}, 'volume'));
                if length(data.date) < MINIMAL_NB_OBS_4_STOCKS + ADD_NB_OBS_4_DATA_FILTERING
                    error('st_vc:exec', 'NOT ENOUGH DATA');
                end
                prop = bsxfun(@times, data.value, 1./sum(data.value, 2));
                s = median(prop);
                s = s / sum(s);
                list_mode = {{'agg_zeros'}, {'ranksum'}, {'nbfixed', 2}, {'nbfixed', 3}, {'nbfixed', 6}, {'nbfixed', 12}};
                
                other_vc = [];
                for i = 1 : length(list_mode)
                    tmp = [prop(:, 1) vc_agg(list_mode{i}{1}, prop(:, 2:end-1), list_mode{i}{2:end}) prop(:, end)];
                    other_vc(:, end+1) = median(tmp);
                    other_vc(:, end) = other_vc(:, end)/sum(other_vc(:, end));
                end
                
                fixing_booleans = logical(st_data('cols', st_data('from-idx', inputs{1}, 1:size(data.value, 2)), 'opening_auction;intraday_auction;closing_auction'));
                
                outputs = {st_data('init', ...
                    'date', mod(datenum(data.colnames, 'HH:MM:SS:FFF'), 1), ...
                    'colnames', {'Usual day', 'opening_auction', 'intraday_auction', 'closing_auction'}, ...
                    'value', cat(2, s', fixing_booleans), ...
                    'info', inputs{1}.info, ...
                    'model', struct('is_static', true, 'name', 'static_agg', 'forecast', @vc_forecast, 'other_vc', other_vc))};
                
            case 'gaussian curve'
                [outputs{1}, b] = st_data('isempty', inputs{1});
                if b
                    return
                end
                data = bin_separator(st_data('keep', inputs{1}, 'volume'));
                
                if length(data.date) >= MINIMAL_NB_OBS_4_STOCKS + ADD_NB_OBS_4_DATA_FILTERING
                    
                    fixing_booleans = logical(st_data('cols', st_data('from-idx', inputs{1}, 1:size(data.value, 2)), 'opening_auction;intraday_auction;closing_auction'));
                    
                    if any(fixing_booleans(:, 2))
                        error('st_vc:exec', 'Unable to work with an intraday auction');
                    end
                    
                    outputs = {make_model(data, fixing_booleans, colnames, repmat((1:length(fixing_booleans))', 1, 2))};
                    
                    % TODO refaire proprement cette agr�gation qui est
                    % compliqu�e � faire dans le cas o� il y un auction de
                    % milieu de journ�e
                    
                    % vals is on continuous trading phase
                    vals = data.value(:, ~any(fixing_booleans, 2));
                    
                    % --- 15-minutes modele
                    [out_vals, bins_idx] = continuous_bin_agg(vals, 3);
                    tmp_data = data;
                    
                    if any(fixing_booleans(:,1)) && any(fixing_booleans(:,3))
                        tmp_data.value = [data.value(:, 1) out_vals data.value(:, end)];
                        tmp_data.colnames = data.colnames([1;(1+bins_idx(:, 2));length(data.colnames)]);
                        tmp_fixing_booleans = [fixing_booleans(1, :);false(size(out_vals, 2), size(fixing_booleans, 2));fixing_booleans(end, :)];
                        outputs{1}.model.other_models = make_model(tmp_data, tmp_fixing_booleans, colnames,...
                            [1,1;1+bins_idx;length(fixing_booleans),length(fixing_booleans)]);
                    elseif any(fixing_booleans(:,3))
                        tmp_data.value = [out_vals data.value(:, end)];
                        tmp_data.colnames = data.colnames([bins_idx(:, 2);length(data.colnames)]);
                        tmp_fixing_booleans = [false(size(out_vals, 2),size(fixing_booleans, 2));fixing_booleans(end, :)];
                        outputs{1}.model.other_models = make_model(tmp_data, tmp_fixing_booleans, colnames,...
                            [bins_idx;length(fixing_booleans),length(fixing_booleans)]);
                    elseif any(fixing_booleans(:,1))
                        tmp_data.value = [data.value(:, 1) out_vals];
                        tmp_data.colnames = data.colnames([1;(1+bins_idx(:, 2))]);
                        tmp_fixing_booleans = [fixing_booleans(1, :);false(size(out_vals, 2),size(fixing_booleans, 2))];
                        outputs{1}.model.other_models = make_model(tmp_data, tmp_fixing_booleans, colnames,...
                            [1,1;1+bins_idx]);
                    else
                        tmp_data.value = out_vals;
                        tmp_data.colnames = data.colnames(bins_idx(:, 2));
                        tmp_fixing_booleans = false(size(out_vals, 2), size(fixing_booleans, 2));
                        outputs{1}.model.other_models = make_model(tmp_data, tmp_fixing_booleans, colnames,...
                            bins_idx);
                    end
                    
                    
                    % --- 30-minutes modele
                    [out_vals, bins_idx] = continuous_bin_agg(vals, 6);
                    tmp_data = data;
                    
                    if any(fixing_booleans(:,1)) && any(fixing_booleans(:,3))
                        tmp_data.value = [data.value(:, 1) out_vals data.value(:, end)];
                        tmp_data.colnames = data.colnames([1;(1+bins_idx(:, 2));length(data.colnames)]);
                        tmp_fixing_booleans = [fixing_booleans(1, :);false(size(out_vals, 2), size(fixing_booleans, 2));fixing_booleans(end, :)];
                        outputs{1}.model.other_models(end+1) = make_model(tmp_data, tmp_fixing_booleans, colnames,...
                            [1,1;1+bins_idx;length(fixing_booleans),length(fixing_booleans)]);
                    elseif any(fixing_booleans(:,3))
                        tmp_data.value = [out_vals data.value(:, end)];
                        tmp_data.colnames = data.colnames([bins_idx(:, 2);length(data.colnames)]);
                        tmp_fixing_booleans = [false(size(out_vals, 2),size(fixing_booleans, 2));fixing_booleans(end, :)];
                        outputs{1}.model.other_models(end+1) = make_model(tmp_data, tmp_fixing_booleans, colnames,...
                            [bins_idx;length(fixing_booleans),length(fixing_booleans)]);
                    elseif any(fixing_booleans(:,1))
                        tmp_data.value = [data.value(:, 1) out_vals];
                        tmp_data.colnames = data.colnames([1;(1+bins_idx(:, 2))]);
                        tmp_fixing_booleans = [fixing_booleans(1, :);false(size(out_vals, 2),size(fixing_booleans, 2))];
                        outputs{1}.model.other_models(end+1) = make_model(tmp_data, tmp_fixing_booleans, colnames,...
                            [1,1;1+bins_idx]);
                    else
                        tmp_data.value = out_vals;
                        tmp_data.colnames = data.colnames(bins_idx(:, 2));
                        tmp_fixing_booleans = false(size(out_vals, 2), size(fixing_booleans, 2));
                        outputs{1}.model.other_models(end+1) = make_model(tmp_data, tmp_fixing_booleans, colnames,...
                            bins_idx);
                    end
                    %out.mode.forecast = @vc_forecast;
                    
                else
                    error('st_vc:exec', 'NOT_ENOUGH_DATA: Not enough data for this security to work on it');
                end
                
                
            case 'gaussian curve 2'
                [outputs{1}, b] = st_data('isempty', inputs{1});
                if b
                    return
                end
                
                % on ne conserve que les jours avec les m�mes horaires
                % cela est trait� par la boite en amont
                % -> utilisation de safer_bin_separator non obligatoire...
                [data,no_use,no_use,has_changed]=safer_bin_separator(outputs{1}, inputs{1},'volume');
                %data = bin_separator(st_data('keep', inputs{1},'volume'));
                
                if length(data.date) >= MINIMAL_NB_OBS_4_STOCKS + ADD_NB_OBS_4_DATA_FILTERING
                    
                    fixing_booleans = logical(st_data('cols', st_data('from-idx', inputs{1}, 1:size(data.value, 2)), 'opening_auction;intraday_auction;closing_auction'));
                    
                    % ////////////
                    % ///  TEST ON MID FIXING AUCTION
                    % ////////////
                    
                    if any(fixing_booleans(:, 2))
                        error('st_vc:exec', 'TITLE_NOT_SUITABLE:Unable to work with an intraday auction');
                    end
                    
                    outputs = {make_model(data, fixing_booleans, colnames, repmat((1:length(fixing_booleans))', 1, 2))};
                    
                    % TODO refaire proprement cette agr�gation qui est
                    % compliqu�e � faire dans le cas o� il y un auction de
                    % milieu de journ�e
                    
                    vals = data.value(:, ~any(fixing_booleans, 2));
                    
                    % --- 15-minutes modele
                    [out_vals, bins_idx] = continuous_bin_agg(vals, 3);
                    tmp_data = data;
                    
                    if any(fixing_booleans(:,1)) && any(fixing_booleans(:,3))
                        tmp_data.value = [data.value(:, 1) out_vals data.value(:, end)];
                        tmp_data.colnames = data.colnames([1;(1+bins_idx(:, 2));length(data.colnames)]);
                        tmp_fixing_booleans = [fixing_booleans(1, :);false(size(out_vals, 2), size(fixing_booleans, 2));fixing_booleans(end, :)];
                        outputs{1}.model.other_models = make_model(tmp_data, tmp_fixing_booleans, colnames,...
                            [1,1;1+bins_idx;length(fixing_booleans),length(fixing_booleans)]);
                    elseif any(fixing_booleans(:,3))
                        tmp_data.value = [out_vals data.value(:, end)];
                        tmp_data.colnames = data.colnames([bins_idx(:, 2);length(data.colnames)]);
                        tmp_fixing_booleans = [false(size(out_vals, 2),size(fixing_booleans, 2));fixing_booleans(end, :)];
                        outputs{1}.model.other_models = make_model(tmp_data, tmp_fixing_booleans, colnames,...
                            [bins_idx;length(fixing_booleans),length(fixing_booleans)]);
                    elseif any(fixing_booleans(:,1))
                        tmp_data.value = [data.value(:, 1) out_vals];
                        tmp_data.colnames = data.colnames([1;(1+bins_idx(:, 2))]);
                        tmp_fixing_booleans = [fixing_booleans(1, :);false(size(out_vals, 2),size(fixing_booleans, 2))];
                        outputs{1}.model.other_models = make_model(tmp_data, tmp_fixing_booleans, colnames,...
                            [1,1;1+bins_idx]);
                    else
                        tmp_data.value = out_vals;
                        tmp_data.colnames = data.colnames(bins_idx(:, 2));
                        tmp_fixing_booleans = false(size(out_vals, 2), size(fixing_booleans, 2));
                        outputs{1}.model.other_models = make_model(tmp_data, tmp_fixing_booleans, colnames,...
                            bins_idx);
                    end
                    
                    
                    % --- 30-minutes modele
                    [out_vals, bins_idx] = continuous_bin_agg(vals, 6);
                    tmp_data = data;
                    
                    if any(fixing_booleans(:,1)) && any(fixing_booleans(:,3))
                        tmp_data.value = [data.value(:, 1) out_vals data.value(:, end)];
                        tmp_data.colnames = data.colnames([1;(1+bins_idx(:, 2));length(data.colnames)]);
                        tmp_fixing_booleans = [fixing_booleans(1, :);false(size(out_vals, 2), size(fixing_booleans, 2));fixing_booleans(end, :)];
                        outputs{1}.model.other_models(end+1) = make_model(tmp_data, tmp_fixing_booleans, colnames,...
                            [1,1;1+bins_idx;length(fixing_booleans),length(fixing_booleans)]);
                    elseif any(fixing_booleans(:,3))
                        tmp_data.value = [out_vals data.value(:, end)];
                        tmp_data.colnames = data.colnames([bins_idx(:, 2);length(data.colnames)]);
                        tmp_fixing_booleans = [false(size(out_vals, 2),size(fixing_booleans, 2));fixing_booleans(end, :)];
                        outputs{1}.model.other_models(end+1) = make_model(tmp_data, tmp_fixing_booleans, colnames,...
                            [bins_idx;length(fixing_booleans),length(fixing_booleans)]);
                    elseif any(fixing_booleans(:,1))
                        tmp_data.value = [data.value(:, 1) out_vals];
                        tmp_data.colnames = data.colnames([1;(1+bins_idx(:, 2))]);
                        tmp_fixing_booleans = [fixing_booleans(1, :);false(size(out_vals, 2),size(fixing_booleans, 2))];
                        outputs{1}.model.other_models(end+1) = make_model(tmp_data, tmp_fixing_booleans, colnames,...
                            [1,1;1+bins_idx]);
                    else
                        tmp_data.value = out_vals;
                        tmp_data.colnames = data.colnames(bins_idx(:, 2));
                        tmp_fixing_booleans = false(size(out_vals, 2), size(fixing_booleans, 2));
                        outputs{1}.model.other_models(end+1) = make_model(tmp_data, tmp_fixing_booleans, colnames,...
                            bins_idx);
                    end
                    
                    % ////////////
                    % ///  TEST ON 30 minutes Liquidity
                    % ////////////
                    
                    % We need a minimum of liquidity to use gaussian curve estimation
                    % We need at least 90 % of days where
                    % 80% of the 30 minutes slice has volume > 0
                    
                    nb_days=size(tmp_data.value,1);
                    nb_days_valid_liquidity=sum(sum(tmp_data.value>0,2)/size(tmp_data.value,2)>0.8);
                    
                    if nb_days_valid_liquidity<=0.9*nb_days
                        error('st_vc:exec', 'TITLE_NOT_SUITABLE:Unable to work on an illiquid stock');                        
                    end
                                        
                elseif has_changed
                    error('st_vc:exec', 'NOT_ENOUGH_DATA: Multiple change in trading hours change results in not enough data');
                else
                    error('st_vc:exec', 'NOT_ENOUGH_DATA: Not enough data for this security to work on it');
                end
                
                
                
            case 'log_reg'
                [outputs{1}, b] = st_data('isempty', inputs{1});
                if b
                    return
                end
                data = bin_separator(st_data('keep', inputs{1}, 'volume'));
                
                if length(data.date) >= MINIMAL_NB_OBS_4_STOCKS + ADD_NB_OBS_4_DATA_FILTERING
                    
                    fixing_booleans = logical(st_data('cols', st_data('from-idx', inputs{1}, 1:size(data.value, 2)), 'opening_auction;intraday_auction;closing_auction'));
                    
                    if any(fixing_booleans(:, 2))
                        error('st_vc:exec', 'Unable to work with an intraday auction');
                    end
                    
                    outputs = {make_model_log_reg(data, fixing_booleans, colnames, repmat((1:length(fixing_booleans))', 1, 2))};
                    
                    
                    vals = data.value(:, ~any(fixing_booleans, 2));
                    
                    [out_vals, bins_idx] = continuous_bin_agg(vals, 3);
                    tmp_data = data;
                    % TODO refaire proprement cette agr�gation qui est
                    % compliqu�e � faire dans le cas o� il y un auction de
                    % milieu de journ�e
                    tmp_data.value = [data.value(:, 1) out_vals data.value(:, end)];
                    tmp_data.colnames = data.colnames([1;(1+bins_idx(:, 2));length(data.colnames)]);
                    tmp_fixing_booleans = [fixing_booleans(1, :);false(size(out_vals, 2), size(fixing_booleans, 2));fixing_booleans(end, :)];
                    outputs{1}.model.other_models = make_model_log_reg(tmp_data, tmp_fixing_booleans, colnames, [1,1;1+bins_idx;length(fixing_booleans),length(fixing_booleans)]);
                    
                    
                    [out_vals, bins_idx] = continuous_bin_agg(vals, 6);
                    tmp_data = data;
                    % TODO refaire proprement cette agr�gation qui est
                    % compliqu�e � faire dans le cas o� il y un auction de
                    % milieu de journ�e
                    tmp_data.value = [data.value(:, 1) out_vals data.value(:, end)];
                    tmp_data.colnames = data.colnames([1;(1+bins_idx(:, 2));length(data.colnames)]);
                    tmp_fixing_booleans = [fixing_booleans(1, :);false(size(out_vals, 2), size(fixing_booleans, 2));fixing_booleans(end, :)];
                    outputs{1}.model.other_models(end+1) = make_model_log_reg(tmp_data, tmp_fixing_booleans, colnames, [1,1;1+bins_idx;length(fixing_booleans),length(fixing_booleans)]);
                    
                    out.mode.forecast = @vc_forecast;
                else
                    error('st_vc:exec', 'NOT_ENOUGH_DATA: Not enough data for this security to work on it');
                end
            case 'static_quant'
                [outputs{1}, b] = st_data('isempty', inputs{1});
                if b
                    return
                end
                data = bin_separator(st_data('keep', inputs{1}, 'volume'));
                prop = bsxfun(@times, data.value, 1./sum(data.value, 2));
                vwap = bin_separator(st_data('keep', inputs{1}, 'vwap'));
                vwap = vwap.value;
                for i = 1 : size(vwap, 1)
                    vwap(i, :) = fill_nan_v(vwap(i, :));
                end
                vwap_day = sum(prop .* vwap, 2);
                q = fminsearch(@(q)my_perf(prop, vwap, vwap_day, q), 0.5);
                s = quantile(prop, q);
                s = s / sum(s);
                fixing_booleans = logical(st_data('cols', st_data('from-idx', inputs{1}, 1:length(s)), ...
                    'opening_auction;intraday_auction;closing_auction'));
                outputs = {st_data('init', ...
                    'date', mod(datenum(data.colnames, 'HH:MM:SS:FFF'), 1), ...
                    'colnames', {'Usual day', 'opening_auction', 'intraday_auction', 'closing_auction'}, ...
                    'value', cat(2, s', fixing_booleans), ...
                    'info', inputs{1}.info, ...
                    'model', struct('is_static', true, 'name', 'std', 'forecast', @vc_forecast, 'my_params', x))};
            case 'med_vc'
                [outputs{1}, b] = st_data('isempty', inputs{1});
                if b
                    return
                end
                data = bin_separator(st_data('keep', inputs{1}, 'volume'));
                prop = bsxfun(@times, data.value, 1./sum(data.value, 2));
                cum_prop = cumsum(prop, 2);
                seas = mean(prop);
                sum_med_prop = sum(seas);
                seas = seas / sum_med_prop;
                seasrel2beg = seas;
                prop_rel2beg = prop;
                for i = 1 : size(prop, 2)
                    prop_rel2beg(:, i) = prop(:, i) ./ sum(prop(:, 1:i), 2);
                    seasrel2beg(:, i)  = seas(:, i) ./ sum(seas(:, 1:i), 2);
                end
                x = fminsearch(@(x)make_allprop_gb(cum_prop, prop_rel2beg, seas, seasrel2beg, x(1:2), x(3)),[0.8 1.2, 0])
                fixing_booleans = logical(st_data('cols', st_data('from-idx', inputs{1}, 1:length(seas)), ...
                    'opening_auction;intraday_auction;closing_auction'));
                outputs = {st_data('init', ...
                    'date', mod(datenum(data.colnames, 'HH:MM:SS:FFF'), 1), ...
                    'colnames', {'Usual day', 'opening_auction', 'intraday_auction', 'closing_auction'}, ...
                    'value', cat(2, seas', fixing_booleans), ...
                    'info', inputs{1}.info, ...
                    'model', struct('is_static', false, 'name', 'gb', 'forecast', @vc_forecast, 'my_params', x))};
                return
            case 'opt_crit'
                [outputs{1}, b] = st_data('isempty', inputs{1});
                if b
                    return
                end
                data = bin_separator(st_data('keep', inputs{1}, 'volume'));
                prop = bsxfun(@times, data.value, 1./sum(data.value, 2));
                vwap = bin_separator(st_data('keep', inputs{1}, 'vwap'));
                vwap = vwap.value;
                for i = 1 : size(vwap, 1)
                    vwap(i, :) = fill_nan_v(vwap(i, :));
                end
                vwap_day = sum(prop .* vwap, 2);
                vals = median(prop);
                vals = vals / sum(vals);
                o = optimset(@fmincon);
                o = optimset(o, 'TolFun', 1e-10, 'MaxFunEvals', 1e4);
                turnover = vwap_day.*sum(data.value, 2);
                v = fmincon(@(v)my_perf_w(turnover, vwap, vwap_day, vals, v), ones(2, 1), [], [], [], [], zeros(2, 1), inf(2, 1), [], o);
                vals(2:67) = v(1) * vals(2:67);
                vals(68:end-1) = v(2) * vals(68:end-1);
                vals(end) = 1 - sum(vals(1:end-1));
                fixing_booleans = logical(st_data('cols', st_data('from-idx', inputs{1}, 1:length(vals)), ...
                    'opening_auction;intraday_auction;closing_auction'));
                outputs = {st_data('init', ...
                    'date', mod(datenum(data.colnames, 'HH:MM:SS:FFF'), 1), ...
                    'colnames', {'Usual day', 'opening_auction', 'intraday_auction', 'closing_auction'}, ...
                    'value', cat(2, vals', fixing_booleans), ...
                    'info', inputs{1}.info, ...
                    'model', struct('is_static', true, 'name', 'std', 'forecast', @vc_forecast))};
            case 'a'
                [outputs{1}, b] = st_data('isempty', inputs{1});
                if b
                    return
                end
                data = bin_separator(st_data('keep', inputs{1}, 'volume'));
                prop = bsxfun(@times, data.value, 1./sum(data.value, 2));
                vals = median(prop);
                vals = vals / sum(vals);
                tmp = 0.2*vals(end);
                vals(end) = vals(end) + tmp;
                vals(1:67) = vals(1:67) - tmp/67;
                fixing_booleans = logical(st_data('cols', st_data('from-idx', inputs{1}, 1:length(vals)), ...
                    'opening_auction;intraday_auction;closing_auction'));
                outputs = {st_data('init', ...
                    'date', mod(datenum(data.colnames, 'HH:MM:SS:FFF'), 1), ...
                    'colnames', {'Usual day', 'opening_auction', 'intraday_auction', 'closing_auction'}, ...
                    'value', cat(2, vals', fixing_booleans), ...
                    'info', inputs{1}.info, ...
                    'model', struct('is_static', true, 'name', 'std', 'forecast', @vc_forecast))};
            case {'std', 'std_ne'}
                if isstruct(inputs{1})
                    [outputs{1}, b] = st_data('isempty', inputs{1});
                    if b
                        return
                    end
                    data = bin_separator(st_data('keep', inputs{1}, 'volume'));
                    seas_func = this.params.get('seas_func');
                    % On passe de volumes � proportions
                    data.value = data.value ./ repmat(sum(data.value, 2), 1, length(data.colnames));
                    fixing_booleans = logical(st_data('cols', st_data('from-idx', inputs{1}, 1:size(data.value, 2)), 'opening_auction;intraday_auction;closing_auction'));
                    idx2del = any(data.value < 0, 2);
                    if sum(idx2del)
                        warning('cv_dirty_index:exec', 'There are negative volumes in your data!!!');
                        data.value(idx2del, :) = [];
                        data.date(idx2del, :) = [];
                    end
                    if length(data.date) >= MINIMAL_NB_OBS_4_STOCKS + ADD_NB_OBS_4_DATA_FILTERING && strcmpi(this.params.get('filter_outlier:char'), 'pca')
                        % On extrait les noisy days
                        pca_engine = pca('nb_simul', 200 ,...
                            'transformation', @(x)log(1 + x),...TODO something better than + 1?
                            'mode_nb_factors', 'bootstrap', ...
                            'normalized', false);
                        pca_engine.estimate(data.value(:, 1:end-1));
                        scores = pca_engine.compute_score(data.value(:, 1:end-1));
                        nd = scores <= PCA_CL; % MAGIC NUMBER
                        if length(data.date) - sum(nd) < MINIMAL_NB_OBS_4_STOCKS
                            s_scores = sort(scores);
                            nd = scores <= s_scores(end-MINIMAL_NB_OBS_4_STOCKS);
                        end
                        %< d�sormais les noisy day sont ceux o� nd est vrai
                        vals = seas_func(data.value(~nd, :))';
                        %>
                    elseif length(data.date) >= MINIMAL_NB_OBS_4_STOCKS % MAGIC NUMBER
                        vals = seas_func(data.value)';
                        nd = false(size(data.date, 1), 1);
                    elseif length(inputs) > 1 && ~st_data('isempty-nl', inputs{2})
                        th_changed
                    else
                        error('st_vc:exec', 'NOT_ENOUGH_DATA: Not enough data for this security to work on it');
                    end
                    % renormalisation pour avoir une somme qui fait 1
                    vals = vals ./ repmat(sum(vals), length(data.colnames), 1);
                    %< mise en forme des donn�es
                    outputs = {st_data('init', 'title', data.title, 'info', data.info, ...
                        'date', mod(datenum(data.colnames, 'HH:MM:SS:FFF'), 1), ...
                        'colnames', cat(2, colnames, {'opening_auction', 'intraday_auction', 'closing_auction'}), ...
                        'value', cat(2, vals, fixing_booleans), ...
                        'attach_format', 'HH:MM', 'model', struct('is_static', true, 'name', 'std', 'forecast', @vc_forecast))};
                    %>
                    % mise en forme des donn�es pour l'intervalle de confiance
                    whole_prop = data.value(~nd, :);
                else
                    whole_prop = [];
                    whole_money = [];
                    has_changed = false;
                    volume = cell(length(inputs{1}), 1);
                    for i = 1 : length(inputs{1})
                        if ~st_data('isempty-nl', inputs{1}{i})
                            volume{i} = bin_separator(st_data('keep', inputs{1}{i}, 'volume'));
                        end
                    end
                    
                    % On v�rifie que ces titres ont bien les m�mes horaires
                    % de quotation
                    
                    u_colnames = {volume{1}.colnames};
                    u_idx = {1};
                    u_idx_len = 1;
                    
                    for i = 2 : length(volume)
                        if ~st_data('isempty-nl',  volume{i})
                            for j = 1 : length(u_colnames)
                                if length(intersect(u_colnames{j}, volume{i}.colnames)) == length( volume{i}.colnames)
                                    u_idx{j}(end+1) = i;
                                    u_idx_len(j) = u_idx_len(j) + 1;
                                    break;
                                elseif j == length(u_colnames)
                                    u_colnames{end+1} = volume{i}.colnames;
                                    u_idx{end+1} = i;
                                    u_idx_len(end+1) = 1;
                                end
                            end
                        end
                    end
                    
                    % on prend seulement les titres qui ont les m�mes
                    % horaires, on choisit les horaires qui correspondent �
                    % un maximum de titres
                    
                    [tmp, idx_max_stocks] = max(u_idx_len);
                    volume = volume(u_idx{idx_max_stocks});
                    inputs{1} = inputs{1}(u_idx{idx_max_stocks});
                    
                    for i = 1 : length(inputs{1})
                        if ~st_data('isempty-nl', inputs{1}{i})
                            vwap = st_data('keep', inputs{1}{i}, 'vwap');
                            vwap = bin_separator(vwap);
                            whole_prop = [whole_prop; volume{i}.value./repmat(sum(volume{i}.value, 2), 1, size(volume{i}.value, 2))];
                            whole_money = [whole_money; nansum(vwap.value.*volume{i}.value, 2)];
                        end
                    end
                    if size(whole_prop, 1) < MINIMAL_NB_OBS_4_INDEX
                        error('st_seasonnality:exec', 'NOT_ENOUGH_DATA: Not enough data for this index to work on it');
                    end
                    vals = weighted_median(whole_prop, whole_money);
                    vals = (vals ./ sum(vals))';
                    %< mise en forme des donn�es
                    outputs = {st_data('init', 'title', 'Courbe indice', ...
                        'date', inputs{1}{1}.date(1:size(vals, 1)), ...
                        'colnames', {'Usual day', 'opening_auction', 'intraday_auction', 'closing_auction'}, ...
                        'value', cat(2, vals, st_data('cols', st_data('from-idx', inputs{1}{1}, 1:size(vals, 1)), 'opening_auction;intraday_auction;closing_auction')), ...
                        'attach_format', 'HH:MM', 'info', inputs{1}{1}.info, ...
                        'model', struct('is_static', true, 'name', 'std', 'forecast', @vc_forecast))};
                end
                
                % < Maintenant que l'on a une courbe de volume on s'attaque aux
                % intervalles de confiance
                if strcmpi(mode, 'std')
                    outputs = vc_ci(outputs, whole_prop, vals);
                end
                % >
                
                % < g�n�ration des contextes relatifs aux semaines ou le
                % d�calage horaire avec NY est diff�rent
                % le 5 mars 2009 (datenum(2009,3,5,8,30,0)) est une date
                % parmi d'autre durant laquelle le d�calage horaire avec NY
                % est "normal"
                % TODO : faire plus propre
                
                
                if ~isfield( outputs{1}.info.td_info(1), 'global_zone_name')
                    outputs{1}.info.td_info(1).global_zone_name = 'europe';
                end
                if strcmpi(outputs{1}.info.td_info(1).global_zone_name, 'europe')
                    
                    modified_curves = vc_time_rescaling('std', outputs{1});
                    
                    msy = st_data('cols', modified_curves, 'March special ny');
                    msy = msy / sum(msy);
                    if any(msy< 0)
                        error('st_vc:exec', 'Problem with context March special ny');
                    end
                    outputs{1} = st_data('add-col', outputs{1}, ...
                        msy, 'March special ny' ); % , 'October special ny'
                end
                
                
            case 'parametric'
                [outputs{1}, b] = st_data('isempty', inputs{1});
                if b
                    return
                end
                data = bin_separator(st_data('keep', inputs{1}, 'volume'));
                % On passe de volumes � proportions
                data.value = data.value ./ repmat(sum(data.value, 2), 1, length(data.colnames));
                fixing_booleans = logical(st_data('cols', st_data('from-idx', inputs{1}, 1:size(data.value, 2)), 'opening_auction;intraday_auction;closing_auction'));
                idx2del = any(data.value < 0, 2);
                if sum(idx2del)
                    warning('cv_dirty_index:exec', 'There are negative volumes in your data!!!');
                    data.value(idx2del, :) = [];
                    data.date(idx2del, :) = [];
                end
                if length(data.date) >= MINIMAL_NB_OBS_4_STOCKS
                    data = create_parametric_volume_curve('data_vc','data',data,'fixing_booleans',fixing_booleans);
                else
                    error('st_vc:exec', 'NOT_ENOUGH_DATA: Not enough data for this security to work on it');
                end
                %< mise en forme des donn�es
                outputs = {st_data('init', 'title', data.title, 'info', data.info, ...
                    'date',data.date, ...
                    'colnames', data.colnames, ...
                    'value', data.value, ...
                    'attach_format', 'HH:MM', 'model', struct('is_static', true, 'name', 'std', 'forecast', @vc_forecast))};
                %>
                
                
            case 'parametric_new'
                [outputs{1}, b] = st_data('isempty', inputs{1});
                if b
                    return
                end
                data = bin_separator(st_data('keep', inputs{1}, 'volume'));
                % On passe de volumes � proportions
                data.value = data.value ./ repmat(sum(data.value, 2), 1, length(data.colnames));
                fixing_booleans = logical(st_data('cols', st_data('from-idx', inputs{1}, 1:size(data.value, 2)), 'opening_auction;intraday_auction;closing_auction'));
                idx2del = any(data.value < 0, 2);
                if sum(idx2del)
                    warning('cv_dirty_index:exec', 'There are negative volumes in your data!!!');
                    data.value(idx2del, :) = [];
                    data.date(idx2del, :) = [];
                end
                if length(data.date) >= MINIMAL_NB_OBS_4_STOCKS
                    data = create_parametric_curve_new(data,fixing_booleans);
                else
                    error('st_vc:exec', 'NOT_ENOUGH_DATA: Not enough data for this security to work on it');
                end
                %< mise en forme des donn�es
                outputs = {st_data('init', 'title', data.title, 'info', data.info, ...
                    'date',data.date, ...
                    'colnames', data.colnames, ...
                    'value', data.value, ...
                    'attach_format', 'HH:MM', 'model', struct('is_static', true, 'name', 'std', 'forecast', @vc_forecast))};
                %>
                
                
            case 'fidessa'
                [outputs{1}, b] = st_data('isempty', inputs{1});
                if b
                    return
                end
                data = bin_separator(st_data('keep', inputs{1}, 'volume'));
                if length(data.date) < MINIMAL_NB_OBS_4_STOCKS + ADD_NB_OBS_4_DATA_FILTERING
                    error('st_vc:exec', 'NOT ENOUGH DATA');
                end
                fixing_booleans = logical(st_data('cols', st_data('from-idx', inputs{1}, 1:size(data.value, 2)), 'opening_auction;intraday_auction;closing_auction'));
                hours = inputs{1}.date(1:length(data.colnames))-floor(inputs{1}.date(1));
                
                % estimation des parametres du model suivant :
                % cumsum(volume(t))/sum(volume(t)) = t.*(b(1)+b(2)*t+(1-b(1)-b(2))*t.^2)
                % ou t appartient a [0,1]
                
                op_cl_time = exec_sql('BSIRIUS',sprintf('select opening,closing from repository..quotation_group_trading_hours where trading_destination_id = %d'...
                    ,inputs{1}.info.td_info(1).trading_destination_id));
                op_cl_time = {['00000100 ' op_cl_time{1,1}],['00000100 ' op_cl_time{1,2}]};
                op_cl_time = datenum(op_cl_time,'yyyymmdd HH:MM:SS');
                
                t = hours(~any(fixing_booleans,2));
                t = (t-op_cl_time(1))/(op_cl_time(2)-op_cl_time(1));
                t = repmat(t,1,length(data.date));
                T1 = t-t.^3;
                T2 = t.^2-t.^3;
                X = data.value(:,~any(fixing_booleans,2))';
                X = cumsum(X)./repmat(sum(X),size(X,1),1);
                X = X-t.^3;
                T1 = T1(:);
                T2 = T2(:);
                X  = X(:);
                b = regress(X,[T1,T2]);
                t = [0;t(:,1)];
                s = diff(t.*(b(1)+b(2)*t+(1-b(1)-b(2))*t.^2));
                
                % estimation volume ouverture et volume fermeture
                
                t_fermeture = 385/390;
                t_ouverture = 1.25/390;
                
                s = [t_ouverture*(b(1)+b(2)*t_ouverture+(1-b(1)-b(2))*t_ouverture^2);...
                    s;...
                    1-t_fermeture*(b(1)+b(2)*t_fermeture+(1-b(1)-b(2))*t_fermeture^2)];
                s = s/sum(s);
                outputs = {st_data('init', ...
                    'date', hours, ...
                    'colnames', {'Usual day', 'opening_auction', 'intraday_auction', 'closing_auction'}, ...
                    'value', [s,fixing_booleans], ...
                    'info', inputs{1}.info, ...
                    'model', struct('is_static', true, 'name', 'std', 'forecast', @vc_forecast))};
                
                
            otherwise
                error('st_vc:exec', 'mode_seas <%s> unknown', mode);
        end
        
        % < Calcul du run_quality
        if isstruct(inputs{1})
            if strcmp(mode,'gaussian curve 2')
                dates_run=unique(floor(inputs{1}.date));
                data_tmp=st_data('extract-day', inputs{1}, dates_run(end));
                default_forecast_params={'composite 1',2,0.1,0.2,'curve_ref',30,'emaLength',4,'quant0',1,...
                    'meanNb05m',10,'pctMedian05m',0,'start_slice',6,'end_slice',length(data_tmp.date)-6};
                first_date_check=find(dates_run>=dates_run(end)-29);
                quality_in_sample=[];
                for i_date=first_date_check:length(dates_run)
                    data_day=st_data('extract-day', inputs{1}, dates_run(i_date));
                    forecast=vc_forecast(outputs{1},data_day,default_forecast_params{:});
                    res=vc_check('cvdyn_indicator', forecast,data_day);
                    quality_in_sample=cat(1,quality_in_sample,res);
                end
                outputs{1}.info.mat_run_quality=quality_in_sample(:,1);
                outputs{1}.info.run_quality = nanmedian(quality_in_sample(:,1));
            else
                outputs{1}.info.run_quality = vc_check('vwap_slippage', outputs{1}, inputs{1});
            end
        else
            if strcmp(mode,'gaussian curve 2')
                tmp_check = NaN(length(inputs{1}),1);
                for i_inputs = 1 : length(inputs{1})
                    dates_run=unique(floor(inputs{1}{i_inputs}.date));
                    data_tmp=st_data('extract-day', inputs{1}{i_inputs}, dates_run(end));
                    default_forecast_params={'composite 1',2,0.1,0.2,'curve_ref',30,'emaLength',4,'quant0',1,...
                        'meanNb05m',10,'pctMedian05m',0,'start_slice',6,'end_slice',length(data_tmp.date)-6};
                    first_date_check=find(dates_run>=dates_run(end)-30);
                    quality_in_sample=[];
                    for i_date=first_date_check:length(dates_run)
                        data_day=st_data('extract-day', inputs{1}{i_inputs}, dates_run(i_date));
                        forecast = vc_forecast(outputs{1}, data_day, default_forecast_params{:});
                        res=vc_check('cvdyn_indicator', forecast, st_data('extract-day', inputs{1}, dates_run(i_date)));
                        quality_in_sample=cat(1,quality_in_sample,res);
                    end
                    tmp_check(i) = nanmean(quality_in_sample(:,1));
                end
                outputs{1}.info.run_quality = nanmean(tmp_check);
            else
                tmp_check = NaN(length(inputs{1}), length(outputs{1}.colnames) - 3);
                for i = 1 : length(inputs{1})
                    tmp_check(i, :) = vc_check('vwap_slippage', outputs{1}, inputs{1}{i});
                end
                outputs{1}.info.run_quality = nanmean(tmp_check);
            end
        end
        % >
        
        function th_changed
            % colnames = 'Trading hours changed';
            colnames = 'Usual day'; % Pour m�moire, avant il y avait �crit 'Trading hours changed' ici afin de changer de contexte...
            if length(data.date) >= MINIMAL_NEW_HOURS_TRADING_DAYS
                old_data = bin_separator(st_data('keep', inputs{2}, 'volume'));
                old_fixing_booleans = logical(st_data('cols', st_data('from-idx', inputs{2}, 1:size(old_data.value, 2)), 'opening_auction;intraday_auction;closing_auction'));
                if length(old_data.date) < MINIMAL_NB_OBS_4_STOCKS - MINIMAL_NEW_HOURS_TRADING_DAYS
                    if length(inputs) > 2 && ~st_data('isempty-nl', inputs{3})
                        error('st_vc:exec', 'NOT_ENOUGH_DATA: Multiple change in trading hours change results in not enough data');
                    else
                        error('st_vc:exec', 'NOT_ENOUGH_DATA: Not enough data for this security to work on it');
                    end
                end
                old_data.value = old_data.value ./ repmat(sum(old_data.value, 2), 1, length(old_data.colnames));
                [additionnal_bins, idx_add] = setdiff(data.colnames(~any(fixing_booleans, 2)), old_data.colnames(~any(old_fixing_booleans, 2)));
                [removed_bins, idx_removed] = setdiff(old_data.colnames(~any(old_fixing_booleans, 2)), data.colnames(~any(fixing_booleans, 2)));
                for i = 1 : size(fixing_booleans, 2)
                    if any(fixing_booleans(:, i)) ~= any(old_fixing_booleans(:, i)) || ...
                            ( any(fixing_booleans(:, i)) && ...
                            abs(datenum(data.colnames(fixing_booleans(:, i)), 'HH:MM:SS:FFF')-datenum(old_data.colnames(old_fixing_booleans(:, i)), 'HH:MM:SS:FFF')) >= TH_CHANGE_TOL )
                        error('st_vc:exec', 'Fixing hours changed too much');
                    end
                end
                if (~isempty(additionnal_bins) && ~isempty(removed_bins)) || (isempty(additionnal_bins) && isempty(removed_bins))
                    if length(idx_add) == length(idx_removed)
                        for i = 1 : length(idx_add)
                            if abs(datenum(data.colnames(additionnal_bins{i}), 'HH:MM:SS:FFF')-datenum(old_data.colnames(removed_bins{i}), 'HH:MM:SS:FFF')) >= TH_CHANGE_TOL
                                error('st_vc:exec', 'Trading hours changed and don''t know how to handle it');
                            end
                        end
                        data.value = [old_data.value;data.value];
                        data.date = [old_data.date;data.date];
                        vals = seas_func(data.value)';
                    else
                        error('st_vc:exec', 'Trading hours changed and doesn''t know how to handle it');
                    end
                elseif ~isempty(additionnal_bins)
                    [inter_bins, idx_inter] = intersect(old_data.colnames(~any(old_fixing_booleans, 2)), data.colnames(~any(fixing_booleans, 2)));
                    if any(diff(idx_inter)>1)
                        error('st_vc:exec', 'Trading hours changed and don''t know how to handle it');
                    end
                    if sum(diff(idx_add)>1) > 1
                        error('st_vc:exec', 'Trading hours changed and don''t know how to handle it');
                    end
                    has_bins_before = idx_add(1) == 1;
                    has_bins_after = idx_add(end) == sum(~any(fixing_booleans(:, 1:3), 2));
                    if has_bins_after && has_bins_before
                        if sum(diff(idx_add)>1) == 0 || sum(diff(idx_add)>1) > 1
                            error('st_vc:exec', 'Trading hours changed and don''t know how to handle it');
                        end
                        idxsep = find(diff(idx_add)>1);
                        idx_bins_before = any(fixing_booleans(:, 1)) + idx_add;
                        idx_bins_before = idx_bins_before(1:idxsep);
                        idx_bins_after  = any(fixing_booleans(:, 1)) + idx_add;
                        idx_bins_after = idx_bins_after(idxsep+1:end);
                    elseif has_bins_after
                        if sum(diff(idx_add)>1) > 0
                            error('st_vc:exec', 'Trading hours changed and don''t know how to handle it');
                        end
                        idx_bins_after = sum(any(fixing_booleans(:, 1:2))) + idx_add;
                        idx_bins_before = [];
                    elseif has_bins_before
                        if sum(diff(idx_add)>1) > 0
                            error('st_vc:exec', 'Trading hours changed and don''t know how to handle it');
                        end
                        idx_bins_before  = any(fixing_booleans(:, 1)) + idx_add;
                        idx_bins_after = [];
                    else
                        error('st_vc:exec', 'Trading hours changed and don''t know how to handle it');
                    end
                    
                    % < On fait les intervalles de confiance
                    % sur les 'vieilles donn�es'
                    tmp = seas_func(old_data.value)';
                    tmp = tmp ./ sum(tmp);
                    tmp = vc_ci({st_data('extract-day', inputs{2}, floor(inputs{2}.date(end)))}, old_data.value, tmp);
                    data.info.ci = tmp{1}.info.ci;
                    % >
                    
                    if has_bins_before
                        prop_before = median(sum(data.value(:, idx_bins_before), 2)./sum(data.value, 2));
                    else
                        prop_before = 0;
                    end
                    if has_bins_after
                        prop_after = median(sum(data.value(:, idx_bins_after), 2)./sum(data.value, 2));
                    else
                        prop_after = 0;
                    end
                    
                    old_data_cvals = old_data.value(:, ~any(old_fixing_booleans, 2));
                    
                    idx_mid_fixing = find(fixing_booleans(:, 2));
                    if ~isempty(idx_mid_fixing)
                        old_data_mid_fixing =  old_data.value(:, old_fixing_booleans(:, 2));
                    end
                    
                    old_data.value = [old_data.value(:, old_fixing_booleans(:, 1)) * (1-prop_after-prop_before), ...
                        repmat(prop_before / length(idx_bins_before), size(old_data.value, 1), length(idx_bins_before)), ...
                        old_data_cvals(:, idx_inter) * (1-prop_after-prop_before), ...
                        repmat(prop_after/ length(idx_bins_after), size(old_data.value, 1), length(idx_bins_after)), ...
                        old_data.value(:, old_fixing_booleans(:, 3)) * (1-prop_after-prop_before) ];
                    
                    if ~isempty(idx_mid_fixing)
                        old_data.value = [old_data.value(:, 1:idx_mid_fixing-1) old_data_mid_fixing old_data.value(:, idx_mid_fixing:end)];
                    end
                    
                    data.value = [old_data.value;data.value];
                    vals = seas_func(data.value)';
                elseif ~isempty(removed_bins)
                    old_data.value(:, idx_removed) = [];
                    data.value = [old_data.value;data.value];
                    data.date = [old_data.date;data.date];
                    vals = seas_func(data.value)';
                else
                    error('st_vc:exec', 'Assumed unreachable code was reached');
                end
            else
                error('st_vc:exec', 'NOT_ENOUGH_DATA: Trading hours change results in Not enough data');
            end
            nd = false(size(data.value, 1), 1);
        end
    end


    function outputs = vc_ci(outputs, whole_prop, vals)
        % TODO : faire mieux en cas de courbe indice => la variance
        % des volumes relatifs est tr�s corr�l�e au nombre de
        % transaction moyen
        
        if ~isfield(outputs{1}.info, 'ci') || ~isfield(outputs{1}.info.ci, 'alpha')
            % Param�tres de s lois betas
            MIN_BETA = 1;
            MIN_ALPHA = 3;
            % >
            % on retire les auctions
            idx_continuous = ~any(st_data('cols', outputs{1}, {'opening_auction','intraday_auction','closing_auction'}), 2);
            
            % on passe en volume cumul� sur le continu
            rel_volume = whole_prop(:, idx_continuous);
            rel_volume = rel_volume ./ repmat(sum(rel_volume, 2), 1, size(rel_volume, 2));
            rel_volume = cumsum(rel_volume, 2);
            rel_volume(:, end) = [];
            
            % ici la courbe de volume cumul�e "moyenne" (plut�t m�diane mais
            % bon...)
            c_vals = vals(idx_continuous)';
            if (any(c_vals == 0) || any(~isfinite(c_vals))) && strcmp(mode, 'std')
                trdt = trading_time(outputs{1}.info.td_info, floor(outputs{1}.date(end)));
                if isfinite(trdt.intraday_stop) && isfinite(trdt.intraday_resumption)...
                        && trdt.intraday_stop <= min(mod(outputs{1}.date(c_vals == 0), 1)) && ...
                        max(mod(outputs{1}.date(c_vals == 0), 1)) <= trdt.intraday_resumption
                    st_log('La pause d�jeuner est tol�r�e...');
                else
                    error('st_vc:checking_data', 'There is a zero in continuous phase : may be a too illiquid stock to build a volume curve, or wrong trading hours, or intraday auction');
                end
            end
            c_vals = c_vals ./ sum(c_vals);
            m = cumsum(c_vals);
            m(:, end) = [];
            
            v = mean((rel_volume-repmat(m, size(rel_volume, 1), 1)).^2);
            
            tmp = [ones(length(v), 1) log(m.*(1-m))'];
            tmp = (tmp'*tmp)^-1*tmp'*log(v)';
            
            beta = tmp(2);
            
            alpha = 0.25^(1-beta)/exp(tmp(1))-1;
            
            if beta <= MIN_BETA
                tmp = (v./(m.*(1-m)));
                alpha = median(1./tmp) - 1;
                beta = 1;
            end
            if alpha <= MIN_ALPHA
                beta = MIN_BETA;
            end
            
            %
            explicit_q = 0.1*(1:9);
            % Petit calcul de qualit�. TODO mettre �a en m�moire pour permettre
            % un plot
            % Pour la qualit�, pourquoi ne pas faire un test Chi2 ou
            % kolomogorov?
            quant = NaN(length(m), length(explicit_q));
            for j = 1 : 9
                %                     quant(:, j) = betainv(explicit_q(j), alpha.*m, alpha.*(1-m));
                quant(:, j) = betainv(explicit_q(j), m.*((alpha+1).*(m.*(1-m)/0.25).^(1-beta)-1), (1-m).*((alpha+1).*(m.*(1-m)/0.25).^(1-beta)-1));
                outputs{1}.info.ci.dist(:, j) = quant(:, j) - m';
                outputs{1}.info.ci.m_dist(:, j) = mean(abs(outputs{1}.info.ci.dist(:, j)));
                outputs{1}.info.ci.max_dist(:, j) = max(abs(outputs{1}.info.ci.dist(:, j)));
            end
            quant(1, :) = 0;
            quant(end, :) = 1;
            for j = 1:length(explicit_q)
                success(:, j) = mean(rel_volume'<repmat(quant(:, j), 1, size(rel_volume, 1)), 2);
            end
            nb_val2exclude_from_check = 0;
            %         check_val = mean(mean(abs(success(nb_val2exclude_from_check+1:end-nb_val2exclude_from_check, :)-repmat(0.1*(1:9), size(success, 1)-2*nb_val2exclude_from_check, 1))));
            check_val = max(0, 1-10*max(mean(abs(success(nb_val2exclude_from_check+1:end-nb_val2exclude_from_check, :)-repmat(explicit_q, size(success, 1)-2*nb_val2exclude_from_check, 1)))));
            
            % intervalles diff�renci�s
            %                 rel_volume = whole_prop(:, idx_continuous);
            %                 rel_volume = rel_volume ./ repmat(sum(rel_volume, 2), 1, size(rel_volume, 2));
            %                 m = diff([0 m 1]);
            %                 quant = NaN(length(m), length(explicit_q));
            %                 for j = 1 : 9
            %                     quant(:, j) = betainv(explicit_q(j), m.*((alpha+1).*(m.*(1-m)/0.25).^(1-beta)-1), (1-m).*((alpha+1).*(m.*(1-m)/0.25).^(1-beta)-1));
            %                 end
            %                 success = [];
            %                 for j = 1:length(explicit_q)
            %                     success(:, j) = mean(rel_volume'<repmat(quant(:, j), 1, size(rel_volume, 1)), 2);
            %                 end
            %                 nb_val2exclude_from_check = 0;
            %                 %         check_val = mean(mean(abs(success(nb_val2exclude_from_check+1:end-nb_val2exclude_from_check, :)-repmat(0.1*(1:9), size(success, 1)-2*nb_val2exclude_from_check, 1))));
            %                 check_val = max(0, 1-10*max(mean(abs(success(nb_val2exclude_from_check+1:end-nb_val2exclude_from_check, :)-repmat(explicit_q, size(success, 1)-2*nb_val2exclude_from_check, 1)))));
            
            outputs{1}.info.ci.alpha = max(alpha, MIN_ALPHA);
            outputs{1}.info.ci.beta  = beta;
            outputs{1}.info.ci.run_quality = check_val;
        end
    end

    function [out_vals, bins_idx] = continuous_bin_agg(vals, nb_bin2agg)
        out_vals = [];
        bins_idx = [];
        nb_bins = size(vals, 2);
        for i = 1 : nb_bin2agg : nb_bins
            bins_idx(end+1, :) = [i,min(i+nb_bin2agg-1, nb_bins)];
            out_vals(:, end+1) = sum(vals(:, bins_idx(end, 1):bins_idx(end, 2)), 2);
        end
    end

    function out = make_model(data, fixing_booleans, colnames, bins_idx)
        logData=log(data.value);
        logDataQuant0=logData;
        
        %%% zero transformation quantile
        fixed_quantile0=0.1;
        quant0=NaN(size(logData,2),1);
        for i=1:size(logData,2)
            non_zero_data=find((isfinite(logData(:,i))==1)*1 +(logData(:,i)>0)*1==2) ;
            quant0(i)=min(quantile(logData(non_zero_data,i),fixed_quantile0),mean(logData(isfinite(logData(:,i)),i)));
            logDataQuant0(~ismember(1:size(logData,1),non_zero_data),i)=quant0(i);
        end
        
        %%% zero transformation 1
        logData(~isfinite(logData))=0;
        
        %%% cov & historical computation
        historicalCov=cov(logData);
        historicalMean=transpose(mean(logData));
        historicalCovQuant0=cov(logDataQuant0);
        historicalMeanQuant0=transpose(mean(logDataQuant0));
        
        %%% scaling du volume total journ�e
        medianDailyVolumeAll=exp(mean(log(sum(data.value,2))));
        medianDailyVolume30=exp(mean(log(sum(data.value(max(1,end-29):end,:),2))));
        medianDailyVolume60=exp(mean(log(sum(data.value(max(1,end-59):end,:),2))));
        
        ratioScalingAllMeanQuant0=medianDailyVolumeAll/sum(exp(historicalMeanQuant0));
        ratioScaling30MeanQuant0=medianDailyVolume30/sum(exp(historicalMeanQuant0));
        ratioScaling60MeanQuant0=medianDailyVolume60/sum(exp(historicalMeanQuant0));
        
        
        historicalMeanQuant0ScalingAll=historicalMeanQuant0+log(ratioScalingAllMeanQuant0);
        historicalMeanQuant0Scaling30=historicalMeanQuant0+log(ratioScaling30MeanQuant0);
        historicalMeanQuant0Scaling60=historicalMeanQuant0+log(ratioScaling60MeanQuant0);
        
        prop_close_moy = 0;
        prop_close_med =0;
        if any(fixing_booleans(:, 3))
            prop_close_moy =mean(data.value(:, fixing_booleans(:, 3)) ./ sum(data.value, 2));
            prop_close_med = median(data.value(:, fixing_booleans(:, 3))) / median(sum(data.value, 2));
        end
        
        qlev = (0.05:0.05:0.95)';
        
        out = st_data('init', 'title', data.title, 'info', data.info, ...
            'date', mod(datenum(data.colnames, 'HH:MM:SS:FFF'), 1), ...
            'colnames', cat(2, colnames, {'opening_auction', 'intraday_auction', 'closing_auction'}), ...
            'value', cat(2, historicalMean, fixing_booleans), ...
            'valueQuant0', cat(2, historicalMeanQuant0, fixing_booleans), ...
            'valueQuant0ScalingAll', cat(2, historicalMeanQuant0ScalingAll, fixing_booleans), ...
            'valueQuant0Scaling30', cat(2, historicalMeanQuant0Scaling30, fixing_booleans), ...
            'valueQuant0Scaling60', cat(2, historicalMeanQuant0Scaling60, fixing_booleans), ...
            'attach_format', 'HH:MM', ...
            'model', struct('is_static', false, 'name', 'gc', 'forecast', [], ...
            'cov', historicalCov, ...
            'covQuant0', historicalCovQuant0, ...
            'quant', struct('q', quantile(data.value, qlev), 'qlevel', qlev), ...
            'quant0', struct('q', fixed_quantile0, 'qlevel', quant0),...
            'critere', struct('mean_nb_0', mean(100*sum(data.value==0,2)/size(data.value,2)),...
            'nb_period_median0',sum(median(data.value)==0)), ...
            'varlog', var(logData), ...
            'prop', struct('prop_close_moy', prop_close_moy, 'prop_close_med', prop_close_med, 'med_prop', median(bsxfun(@times, data.value, 1./sum(data.value, 2)))), ...
            'bins_idx', bins_idx));
    end

    function out = make_model_log_reg(data, fixing_booleans, colnames, bins_idx)
        logData=log(data.value);
        logData(~isfinite(logData))=0;
        %         historicalCov=cov(logData);
        
        Y = log(sum(data.value, 2));
        X = logData;
        vals = NaN(size(data.value, 2)-1, size(data.value, 2));
        o = ones(size(Y));
        X = [o X];
        for i = 1 : size(data.value, 2)-1
            vals(i, 1:i+1) = X(:, 1:i+1) \ Y;% plot(X(:, 1:i)*vals(i, 1:i)', Y, '+')
            tmp = log(mean(exp(Y))/mean(exp(X(:, 1:i+1) *vals(i, 1:i+1)')));
            vals(i, 1) = vals(i, 1) + tmp;
        end
        
        prop_close_moy = mean(data.value(:, fixing_booleans(:, 3)) ./ sum(data.value, 2));
        prop_close_med = median(data.value(:, fixing_booleans(:, 3))) / median(sum(data.value, 2));
        
        qlev = (0.05:0.05:0.95)';
        
        
        % < modele correspodant � l'estimateur closing fixing � des fins de
        % comparaison
        Y = data.value(:, end);
        idx = (Y == 0);
        Y(idx, :) = [];
        cf_coeff = [];
        for i = 1 : size(data.value, 2)-1
            X = sum(data.value(:, 1:i), 2);
            X(idx, :) = [];
            cf_coeff(end+1) = log(X) \ log(Y);
        end
        % >
        
        out = st_data('init', 'title', data.title, 'info', data.info, ...
            'date', mod(datenum(data.colnames, 'HH:MM:SS:FFF'), 1), ...
            'colnames', cat(2, colnames, {'opening_auction', 'intraday_auction', 'closing_auction'}), ...
            'value', cat(2, NaN(size(fixing_booleans, 1), 1), fixing_booleans), ...
            'attach_format', 'HH:MM', ...
            'model', struct('is_static', false, 'name', 'gc', 'forecast', [], ...
            'reg_coeff', vals, ...
            'cf_coeff', cf_coeff, ...
            'quant', struct('q', quantile(data.value, qlev), 'qlevel', qlev), ...
            'varlog', var(logData), ...
            'prop', struct('mean_log_eod', NaN, 'prop_close_moy', prop_close_moy, 'prop_close_med', prop_close_med, 'med_prop', median(bsxfun(@times, data.value, 1./sum(data.value, 2)))), ...
            'bins_idx', bins_idx));
    end

    function perf = make_allprop_gb(cum_prop, rel_prop, seas, seas_rel, modif_bounds, modif_conf)
        p = rel_prop;
        for i = 1 : size(rel_prop, 1)
            p(i, :) = prop_gloubiboulga(rel_prop(i, :), seas, seas_rel, modif_bounds, modif_conf);
        end
        perf = mean(max(abs(cumsum(p, 2)-cum_prop), [], 2));
    end

    function p = prop_gloubiboulga(rel_prop, seas, seas_rel, modif_bounds, modif_conf)
        p = seas;
        for i = 3 : length(seas)-1
            p(i) = min(1-sum(p(1:i-1)), max(modif_bounds(1)*seas(i), min(modif_bounds(2)*seas(i), p(i) + modif_conf * (seas_rel(i-1)-rel_prop(i-1)) * sum(seas(1:i-1)))));
        end
        p(end) = 1 - sum(p(1:end-1));
    end

    function p = my_perf(prop, vwap, vwaps, q)
        s = quantile(prop, q);
        s = s / sum(s);
        my_vwaps = sum(bsxfun(@times, s, vwap), 2);
        p = mean(abs(1-my_vwaps./vwaps));
    end

    function p = my_perf_w(weight, vwap, vwaps, seas, v)
        seas(2:67) = v(1) * seas(2:67);
        seas(68:end-1) = v(2) * seas(68:end-1);
        seas(end) = 1 - sum(seas(1:end-1));
        my_vwaps = sum(bsxfun(@times, seas, vwap), 2);
        p = sum(abs(1-my_vwaps./vwaps).*weight)/sum(weight);
    end


    function data = create_parametric_curve_new(data,fixing_booleans)
        FUNC_FIDESSA=@(x)(11*x.^3-4*x.^2-4*x+3);
        
        slice_time=mod(datenum(data.colnames, 'HH:MM:SS:FFF'), 1);
        slice_time_renorm=(slice_time-slice_time(1))./(slice_time(end)-slice_time(1));
        vc_value_fidessa=repmat(0,size(slice_time,1),1);
        
        idx_open=find(fixing_booleans(:,1));
        idx_close=find(fixing_booleans(:,3));
        if ~isempty(idx_open)
            vc_value_fidessa(idx_open)=max(0.01,min(0.1,median(data.value(:,idx_open(1)))));
        end
        if ~isempty(idx_close)
            vc_value_fidessa(idx_close)=max(0.04,min(0.2,median(data.value(:,idx_close(1)))));
        end
        
        idx_na=~fixing_booleans(:,1) & ~fixing_booleans(:,3);
        fidessa_na=(1-sum(vc_value_fidessa))*FUNC_FIDESSA(slice_time_renorm(idx_na))/sum(FUNC_FIDESSA(slice_time_renorm(idx_na)));
        vc_value_fidessa(idx_na)=fidessa_na;
        vc_value_fidessa=vc_value_fidessa/sum(vc_value_fidessa);
        
        data.value=cat(2,vc_value_fidessa,fixing_booleans);
        data.date=slice_time;
        data.colnames={'Usual day','opening_auction','intraday_auction','closing_auction'};
    end














%%** End of production
    function b = eop
        b = true;
    end

%%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

end