function o = st_vc_perf( varargin)
% ST_VC_PERF - module qui �value la qualit� de la pr�vision des proportions
% de volume
%
% L'acc�s � ces m�thodes est verrouill� au niveau de st_node
%
% See also st_node
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
    'exec', @exec, ...
    'learn', [], ...
    'get', @get_, ...
    'set', @set_, ...
    'end_of_prod', @eop, ...
    'parallel', true, ...
    'plot',  [] );
%%** Init
    function init( varargin)
        this.params = options({'mode', 'slippage'}, varargin);
        this.memory = options({});
    end

%%** Exec
    function outputs = exec( inputs, varargin)
        vc = inputs{1};
        oos_data = inputs{2};
        perf  = NaN(length(oos_data), 1);
        mode  = this.params.get('mode');
        dates = NaN(length(oos_data), 1);
        forecast = [];
        for i = 1 : length(oos_data)
            if ~isempty(oos_data{i})
                if ~vc.model.is_static || isempty(forecast)
                    forecast = vc.model.forecast(oos_data{i});
                end
                perf(i)  = vc_check(mode, forecast, oos_data{i});
                dates(i) = floor(oos_data{i}.date(1));
            end
        end
        idx2del = ~isfinite(perf);
        perf(idx2del)  = [];
        dates(idx2del) = [];
        outputs = {st_data('init', 'value', perf, 'date', dates, 'colnames', mode, 'title', vc.title, 'info', vc.info)};
    end

%%** End of production
    function b = eop
        b = true;
    end

%%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

end