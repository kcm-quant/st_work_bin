function o = st_volatility( varargin)
% ST_VOLATILITY - estimateur de volatilité (intra day)
%
% See also st_node est_volatility 
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({'estimator:char', 'gk:1', ... % estimation type
                           'window:date', datenum(0,0,0,0,10,0), ... % window size
                           'step:date',  datenum(0,0,0,0,1,0), ...   % step size
                           'input-names:char', 'price'}, ... % input names
                           varargin);
    this.memory = options({'n', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    outputs = inputs;
    for o=1:length(outputs)
        outputs{o} = est_volatility( this.params.get('estimator:char'), inputs{o}, ...
            'window:date', this.params.get('window:date'), ...
            'step:date'  , this.params.get('step:date'), ...
            'input-names:char', this.params.get('input-names:char'));
    end
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end