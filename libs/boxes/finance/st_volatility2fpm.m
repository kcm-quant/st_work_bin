function o = st_volatility2fpm( varargin)
% ST_VOLATILITY2FPM - calibrage (je n'ose pas utiliser le mot estimation)
%       et test de param�tres permettant de connaitre la distance �
%       laquelle placer un ordre limite pour avoir une certaine probabilit�
%       d'�x�cution dans un horizon temporel donn�
%
% author   : 'rburgot@cheuvreux.com'
% reviewer : ''
% version  : '1'
% date     : '20/08/2008'
%
% See also st_node

this.params = {};
this.memory  = {};

o = struct('init', @init, ...
    'exec', @exec, ...
    'learn', [], ...
    'get', @get_, ...
    'set', @set_, ...
    'end_of_prod', @eop, ...
    'plot', @plot_ );
%%** Init
    function init( varargin)
        this.params = options({'plot:b', 1, ...
            'quant', [0.9; 0.8; 0.7; 0.6; 0.5], ...
            'time_scales', datenum(0,0,0,0,[5,10,20,40],0), ... => parameter for the whole graph_indicator
            'step:time', datenum(0,0,0,0,0,30), ... => parameter for the whole graph_indicator
            'q_step', 0.02, ...
            'smooth_volatility', 0, ...
            'params2test', [], ...
            }, varargin);
        this.memory = options({'gdata', [], 'str_from', '', 'str_to', ''});
    end

%%** Exec
    function outputs = exec( inputs, varargin)
        % inputs{1} = volatilit� 15 min tous les 'step:time'
        % inputs{2} = donn�es placement 30 secondes tous les 'step:time'
        %   l'input 2 doit ensuite �tre agr�g� pour respecter time_scales
        
        [inputs, b] = st_data('isempty', inputs);
        if b
            outputs = {st_data('log', inputs{1}, 'eieo')};
            return;
        end
        
        prop_obs_vol = mean(isfinite(st_data('cols', inputs{1}, 'vol_GK')));
        if prop_obs_vol < 0.9
           error('st_volatility2fpm:exec', 'INAPPROPRIATE_METHOD: Too illiquid stock to use this estimator'); 
        end
        
        % < On ne sait pas travailler s'il y a une pause � midi
        trdt = trading_time(inputs{1}.info.td_info, floor(inputs{1}.date(end)));
        if isfinite(trdt.intraday_stop_auction) || isfinite(trdt.intraday_stop_fixing)...
                || isfinite(trdt.intraday_stop) || isfinite(trdt.intraday_resumption_auction) ...
                || isfinite(trdt.intraday_resumption_fixing) || isfinite(trdt.intraday_resumption)
            error('st_volatility2fpm:exec', ...
                'INAPPROPRIATE_METHOD: this estimator has problems with stock having an intraday auction or a lunch break');
        end
        % >
        
        %< On retire les jours qui contiennent un auction stop de volatilit�
        days = unique(floor(inputs{1}.date(logical(st_data('cols', inputs{1}, 'stop_auction')))));
        if ~isempty(days)
            for i = 1 : length(inputs)
                idx = ismember(floor(inputs{i}.date), days);
                inputs{i} = st_data('log', st_data('from-idx', inputs{i}, ~idx), 'warn', sprintf('<%d> days have been removed from this data has they had a stop auction', length(days)));
            end
        end
        %>
        %< On retire les auctions
        for i = 1 : length(inputs)
            inputs{i} = st_data('where', inputs{i} , '~{auction}');
        end
        %>

        [outputs, b] = st_data('isempty', inputs);
        if b
            outputs = {st_data('log', inputs{1}, 'eieo')};
            return;
        end

        %< On ne conserve que les colonnes qui nous int�ressent
        outputs{1} = st_data('keep', outputs{1} , 'vol_GK');
        outputs{2} = st_data('apply-formula', outputs{2} , '[{low}, {high}, {ask_open}, {bid_open}]');
        %>

        nb_params = 4; % MAGIC NUMBER
        quant = this.params.get('quant');
        time_scales = this.params.get('time_scales');
        vol_time_convert = sqrt(datenum(0,0,0, 0,10,0)); % la volatilit� quel que soit sa fen�tre est exprim�e en vol 10 minutes en bp
        % Cr�ation des variables qui permettront la r�gression pour obtenir
        % la relation (proba,horizon)<->lambda
        %<*
        q_step = this.params.get('q_step');
        if quant(1) > quant(end)
            qc = quant(1) : - q_step : quant(end);
        else
            qc = quant(end) : - q_step : quant(1);
        end
        to_be_regress = zeros(length(time_scales) * length(qc), nb_params);
        k = 1;
        lg_ts = log(time_scales) / 100;
        for i = 1 : length(time_scales)
            for j = 1 : length(qc)
                to_be_regress(k, :) = [1, qc(j), qc(j) * lg_ts(i), lg_ts(i)];
                k = k + 1;
            end
        end
        %>*

        step = this.params.get('step:time');
        %<* Calculs effectif des quantiles du rapport : rendement_max / volatilit�
        outputs{2}.date = outputs{2}.date - step;
        
        % TODO comprendre pourquoi si l'on fait varier window:time de
        % basic_indicator, on a pas toujours les m�mes jours manquants...
        % Alors les 3 lignes comment�es suivantes suffiront
        % plut�t que les 6 lignes apr�s les commentaires...
%         idx_to_del = mod(outputs{1}.date(1), 1) > mod(outputs{2}.date, 1);
%         outputs{2}.date(idx_to_del) = [];
%         outputs{2}.value(idx_to_del, :) = [];
        half_sec_dt = 5.78703703703704e-006;
        [dt, idx, pdx] = intersect( round(outputs{1}.date / half_sec_dt) * half_sec_dt, round(outputs{2}.date / half_sec_dt) * half_sec_dt);
        outputs{1}.date = dt;
        outputs{2}.date = dt;
        outputs{1}.value = outputs{1}.value(idx,:);
        outputs{2}.value = outputs{2}.value(pdx,:);
        %>*

        if this.params.get('smooth_volatility')
            vol = ewma('id-fix-no0-squared', outputs{1});
        else
            vol = outputs{1};
        end
        
        %< d�coupage par jours
        % TODO quelquechose de plus efficace?
        vol = bin_separator(vol, 'explicit_colnames', false, 'intersect_hours', true);
        data = bin_separator(outputs{2}, 'explicit_colnames', false, 'intersect_hours', true);
        outputs(2) = [];
        %>
        
        lm_ = [];
        nb_days = length(vol.date);
        params2test = this.params.get('params2test');
        if nb_days < 5 && isempty(params2test)
            outputs{1} = st_data('log', outputs{1}, 'dieo', 'Not enough data for computation');
            outputs = outputs(1);
            return;
        end
        
        g_data = cell(length(time_scales), 1);
        for i = 1 : length(time_scales)
            %< Agr�gation des donn�es depuis step:time vers time_scales
            nb_bins = time_scales(i) / step;
            if round(nb_bins) - 0.01 < nb_bins && round(nb_bins) + 0.01 > nb_bins
                nb_bins = round(nb_bins);
            else
                error('st_volatility2fpm:check_args', 'Time_scales should be a multiple of ''step:time''');
            end

            idx = 1 : nb_bins;
            nb_ts_data_per_days = size(vol.value, 2) - nb_bins;
            ts_data = zeros(nb_days*nb_ts_data_per_days, 4);
            l = 1;
            % TODO quelquechose de plus efficace? => bufferiser les donn�es
            % aux diff�rentes �chelles
            for j = 1 : nb_ts_data_per_days
                d_1_v___idx = data{1}.value(:, idx);
                d_3_v___j = data{3}.value(:, j);
                d_2_v___idx = data{2}.value(:, idx);
                d_4_v___j = data{4}.value(:, j);
                for k = 1 : nb_days
                    ts_data(l, :) = [1e4*(1 - min(d_1_v___idx(k, :))/d_3_v___j(k)), ...
                        -1e4*(1 - max(d_2_v___idx(k, :))/d_4_v___j(k)), d_3_v___j(k), d_4_v___j(k)];
                    l = l +  1;
                end
                idx = idx + 1;
            end
            %>

            %< Data cleaning :
            ts_vol = vol.value(:, 1 : nb_ts_data_per_days);
            ts_vol = ts_vol(:) * sqrt(time_scales(i)) / vol_time_convert;
            %TODO report it, at least warning
            idx_to_be_deleted = ts_vol <= 0 | ~isfinite(ts_vol) | any(ts_data == 1e4, 2) | any(~isfinite(ts_data), 2);
            ts_vol(idx_to_be_deleted) = [];
            ts_data(idx_to_be_deleted, :) = [];
            %>

            g_data{i} = [ts_data, ts_vol];
            lm_ = [lm_; quantile([ts_data(:, 1) ./ ts_vol; ts_data(:, 2) ./ ts_vol], (1-qc)')];
            for j = 1 : length(qc)
                outputs{1}.info.quantiles(i, j) = lm_(end - length(qc) + j);
            end
        end
        %>*

        if isempty(params2test)
            % R�gression des quantiles de rm/vol sur (proba,horizon) %
            % attention ce n'est pas une r�gression quantile! mais ptet que j'y
            % reviendrais
            %<
            maxlm = max(lm_); % TODO :pourquoi est-ce que j'ai �cris �a?
            idx_to_be_deleted = lm_ < maxlm / 20; % pourquoi est-ce que j'ai �cris �a?
            lm_(idx_to_be_deleted) = []; % pourquoi est-ce que j'ai �cris �a?
            to_be_regress(idx_to_be_deleted, :) = []; % pourquoi est-ce que j'ai �cris �a?
            outputs{1}.value = regress(lm_, to_be_regress)';
            %>

            %< Mise en forme
            outputs{1}.date = outputs{1}.info.data_datestamp;
            outputs{1}.colnames = {'a', 'b', 'c', 'd'};
            %>

            %< test de ce qui vient d'�tre fitt�
            for i = 1 : length(time_scales)
                for j = 1 : length(quant)
                    outputs{1}.info.lambda(i, j) = max(outputs{1}.value(1) + outputs{1}.value(2) * quant(j) + ...
                        outputs{1}.value(3) * quant(j) * log(time_scales(i)) / 100 + outputs{1}.value(4) * log(time_scales(i)) / 100, 0);
                    tmp_dist = floor([g_data{i}(:, 5) .* g_data{i}(:, 3); g_data{i}(:, 5) .* g_data{i}(:, 4)] * outputs{1}.info.lambda(i, j)/ (10000*outputs{1}.info.min_step_price));
                    tmp_dist(tmp_dist == 0) = -1;
                    outputs{1}.info.test(i, j) = mean( [g_data{i}(:, 1) .* g_data{i}(:, 3) ; g_data{i}(:, 2) .* g_data{i}(:, 4)] >  tmp_dist *(10000*outputs{1}.info.min_step_price) );
                end
            end
            outputs{1}.info.run_quality = max(0, 1 - 10*max(max(abs(outputs{1}.info.test - repmat(quant', length(time_scales), 1))))); % MAGIC NUMBER 10 => test si l'erreur est inf�rieure � 0.1
            %>

            %< set mem to pot if asked
            this.memory.set('g_data', g_data);
            this.memory.set('str_from', datestr(inputs{1}.date(1), 'dd/mm/yyyy'));
            this.memory.set('str_to', datestr(inputs{1}.date(end), 'dd/mm/yyyy'));
            %>

            %<* Plot if needed
            if this.params.get('plot:b')
                opt = options(varargin);
                h = plot_focus( opt.get('title'));
                plot_( outputs);
            end
            %>*
        else
            % <* test des param�tres fournit dans params2test
            % < code utilis� pour la calibration aggressivit� vers proba
            if length(params2test) > 1
                dist2test = params2test{2}; % les distances � tester, c'est � dire que l'on va regarder
                % quelle est la proportion de distances conseill�es qui sont au dela du spread plus cette quantit�
            else
                dist2test = 0.01 * (1:8);
            end
            if length(params2test) > 2
                spreadreldist2test = params2test{3}; % les distances � tester, c'est � dire que l'on va regarder
                % quelle est la proportion de distances conseill�es qui sont au dela du spread plus cette quantit�
            else
                spreadreldist2test = 2.^(-1:3);
            end
             % >
            params2test = params2test{1}; % param�tres � tester
            
            for i = 1 : length(time_scales)
                for j = 1 : length(quant)
                    outputs{1}.info.lambda(i, j) = max(params2test(1) + params2test(2) * quant(j) + ...
                        params2test(3) * quant(j) * log(time_scales(i)) / 100 + params2test(4) * log(time_scales(i)) / 100, 0);
                    tmp_dist = floor([g_data{i}(:, 5) .* g_data{i}(:, 3); g_data{i}(:, 5) .* g_data{i}(:, 4)] * outputs{1}.info.lambda(i, j)/ 100);
                    tmp_dist(tmp_dist == 0) = -1;
                    outputs{1}.info.test(i, j) = mean( [g_data{i}(:, 1) .* g_data{i}(:, 3) ; g_data{i}(:, 2) .* g_data{i}(:, 4)] / 100 >  tmp_dist );
                    
                    % < code utilis� pour la calibration aggressivit� vers proba
                    spread = g_data{i}(:, 3) - g_data{i}(:, 4);
                    spread = [spread; spread]; 
                    tmp_dist = tmp_dist / 100;% / 100 car tmp_dist est exprim� en centimes
                    lspread = length(spread);
                    % eps sera utilis� pour avoir des intervalles de la
                    % forme ] , ] et non pas [ , [
                    outputs{1}.info.calibration.rel2spread(i, j, :) = histc(tmp_dist./spread,[-inf,0,spreadreldist2test,inf]+eps([0,0,spreadreldist2test,0]))/lspread;
                    outputs{1}.info.calibration.spread_plus_dist(i, j, :) = histc(tmp_dist-spread,[-inf,0,dist2test,inf]+eps([0,0,dist2test,0]))/lspread;
                    % >
                end
            end
            outputs{1}.info.calibration.rel2spread(:, :, end) = []; %on retire le dernier qui sera toujours nul puisque cela correspond � une �galit� � l'infini
            outputs{1}.info.calibration.spread_plus_dist(:, :, end) = []; %on retire le dernier qui sera toujours nul puisque cela correspond � une �galit� � l'infini
            outputs{1}.info.calibration.dist2test = [-inf,0,dist2test,inf];
            outputs{1}.info.calibration.spreadreldist2test = [-inf,0,spreadreldist2test,inf];
            
            outputs{1}.value = max(0, 1 - 10*max(max(abs(outputs{1}.info.test - repmat(quant', length(time_scales), 1)))));
            % >*
        end
        outputs = outputs(1);
    end

%%** End of production
    function b = eop
        b = true;
    end

%%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

%%** plot
    function h = plot_(inputs, varargin)
        h = gcf;
        sdata = inputs{1};
        g_data = this.memory.get('g_data');
        str_from = this.memory.get('str_from');
        str_to = this.memory.get('str_to');
        if ~isempty(g_data) && ~st_data('isempty-nl', inputs)
            time_scales = this.params.get('time_scales');
            quant = this.params.get('quant');
            q_step = this.params.get('q_step');
            if quant(1) > quant(end)
                qc = quant(1) : - q_step : quant(end);
            else
                qc = quant(end) : - q_step : quant(1);
            end
            sec_dt = datenum(0,0,0,0,0,1);

            %< plot des quantiles
            subplot(2,3,1); plot(sdata.info.quantiles);
            set(gca, 'xtick', 1:length(time_scales));
            set(gca, 'xticklabel', num2str(round(time_scales'/ sec_dt)));
            xlabel('T en secondes');ylabel('\lambda_{T, \theta}', 'rotation', 0);
            title([sdata.info.security_key ' du ' str_from ' au ' str_to]);
            for i = 1 : length(qc)
                text(length(time_scales), sdata.info.quantiles(length(time_scales), i), sprintf('\\theta = %2.2f', qc(i)), 'interpreter', 'tex');
            end
            subplot(2,3,2); plot(qc, sdata.info.quantiles');
            legend(tokenize(sprintf('T = %2.0f sec:', time_scales/ sec_dt), ':'));
            xlabel('\theta');ylabel('\lambda_{T, \theta}', 'rotation', 0);
            %>
            
            %< Plot des quantiles fitt�s
            quantiles_fit = [];
            for i = 1 : length(qc)
                quantiles_fit(:, i) = max(sdata.value(1) + sdata.value(2) * qc(i) + ...
                        sdata.value(3) * qc(i) * log(time_scales) / 100 + sdata.value(4) * log(time_scales) / 100, 0);
            end
            subplot(2,3,4); plot(quantiles_fit);
            set(gca, 'xtick', 1:length(time_scales));
            set(gca, 'xticklabel', num2str(round(time_scales'/ sec_dt)));
            xlabel('T en secondes');ylabel('\lambda_{T, \theta}', 'rotation', 0);
            for i = 1 : length(qc)
                text(length(time_scales), quantiles_fit(length(time_scales), i), sprintf('\\theta = %2.2f', qc(i)), 'interpreter', 'tex');
            end
            subplot(2,3,5); plot(qc, quantiles_fit');
            legend(tokenize(sprintf('T = %2.0f sec:', time_scales/ sec_dt), ':'));
            xlabel('\theta');ylabel('\lambda_{T, \theta}', 'rotation', 0);
            %>
            
            %< Plot de test in-sample
            pos_for_2nd = get(subplot(2,3,2), 'position');
            h1 = subplot(2,3,6);
            curr_pos = get(h1,  'position');
            plot(time_scales / sec_dt, sdata.info.test);xlabel('Horizon temporel en secondes');
            ylabel('proba de d�passer la barri�re');
            legend(cat(2, tokenize(sprintf('%2.1f:', quant), ':')), 'location', 'NorthOutside');
            hold on;
            plot(repmat([time_scales(1);time_scales(end)] / sec_dt, 1, length(quant)), repmat(quant, 1, 2)', ':k');
            hold off;
            set(h1, 'position', [curr_pos(1) curr_pos(2) curr_pos(3) pos_for_2nd(4)])
            %>
        else
            st_log('Faites tourner le graphe sur des donn�es non-vides pour obtenir un plot');
        end
    end
end