function o = st_volatility_curve_simple( varargin)
% ST_VOLATILITY_CURVE_SIMPLE - fonction qui renvoie le profil intra day de
% la volatilit� pour un titre sur une p�riode donn�e
%

this.params  = {};
this.memory  = {};

o = struct('init', @init, ...
    'exec', @exec, ...
    'learn', @learn, ...
    'end_of_prod', @eop, ...
    'get', @get_, ...
    'set', @set_ , ...
    'plot',  @plot_ );

%%** Init
    function init( varargin)
        this.params = options({'plot:b', 1  ...                     % - automatic plot
            'autosave', 1 ...
            'quantiles', 2 ...
            }, varargin);
    end

%%** Exec
    function outputs = exec( inputs, varargin)
        outputs ={{}};
        data = inputs{1} ;
        
        if ~st_data('isempty-nl', data)
            rsdata = st_data('iday2array',data, 'vol_GK');
            rsdata.value(rsdata.value<=0)=NaN;
            volatility_curve = nanmedian(rsdata.value,2);

            end_of_first_slice = rsdata.date(1);
            %alpha = sum((std(log(rsdata.value))).^2)/size(rsdata,2);
            
            %Retrait des valeurs volatilit� �gales � NaN et zero pour le
            %calcul de la variance
            log_value = log ( rsdata.value );
            log_curve = nanmedian ( log_value, 2 );
            alphas = nanvar ( log_value, 0, 2 );
            alpha = mean ( alphas );
            
            data_info =  st_data( 'init', 'title', 'mes infos heure de quotation/interval de confiance', 'value',[end_of_first_slice;alpha], ...
                'date', [-2,-1]', 'colnames', {'value'},'rownames', {'end_of_first_slice','alpha'}');
            
            data_f = st_data( 'init', 'title', 'My volatility curve', 'value', volatility_curve, ...
                'date', rsdata.date, 'colnames', {'value'});
            st_log('st_volatility_curve_simple:volatility curve in st_data  ...\n');
            
            slice_num = 1;
            data_f.rownames = cell(length(data_f.date), 1);
            for i=1:size(volatility_curve,1)
                data_f.rownames{i}  =  MAKE_PARAM_NAME(slice_num);
                slice_num = slice_num + 1;
            end
            data_out = st_data('stack', data_info, data_f) ;
            
            %construction du run quality
            %hypoth�se jours iid
            nb_j = size ( rsdata.value, 2 );
            Qj = ( log_value - repmat ( log_curve, 1, nb_j ) ) .^2 ./ repmat ( alphas, 1, nb_j );
            deg_vec = sum ( ~isnan ( Qj ), 1 );
            data_out.info.run_quality = prod ( 1 - chi2cdf ( nansum ( Qj, 1 ), deg_vec ) ) ^ ( 1 / nb_j );
            data_out.info.moy_curve = log_curve;
            data_out.info.var_i = alpha;
            data_out.info.var_vec = alphas;
            data_out.info.deg_lib = deg_vec;
            
            outputs = {data_out};
            st_log('st_volatility_curve_simple: end of run quality...\n');
        else
            st_log('st_volatility_curve_simple: There is no available data to build volatility curve...\n');
        end
    end

    function h = plot_( inputs, varargin)
        h = [];
    end
%%** End of production
    function b = eop
        b = true;
    end
%%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

end

function str = MAKE_PARAM_NAME(i)
str = num2str(i);
str = ['slice' repmat('0', 1, 3-length(str)) str];
end