function o = st_vwap_trend( varargin)
% ST_VWAP_TREND - module de recopie des entr?es en sorties
%   premier st_module r?alis?
%
% Le prototype d'un module est simple:
% - une m?thode  .init( varargin)
% - une m?thode  .exec( {input}  , varargin) -> {output}
% - une m?thode .learn( {{input}}, varargin) -> {} ou ![] (cst)
% - une m?thode  .plot( {output} , varargin)  -> h ou ![] (cst)
% - une m?thode .end_of_prod() -> true/false
% - une m?thode .get/.set qui permet de r?cup?r?r/modifier n'importe quoi
%
% d3 = read_dataset('gi:opt_trading', 'security_id',110, 'from', '01/10/2008', 'to', '01/10/2008', 'trading-destinations', {})
%
% L'acc?s ? ces m?thodes est verrouill? au niveau de st_node
%
% See also st_node
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
    'exec', @exec, ...
    'learn', [], ...
    'get', @get_, ...
    'set', @set_, ...
    'end_of_prod', @eop, ...
    'parallel', 1, ...
    'plot',  @plot_ );
%%** Init
    function init( varargin)
        this.params = options({ ...
            'security-id', NaN, ...
            'trading-destination-id', NaN, ...
            'order-volume', [1e5, 1e5], ...
            'lambda', 1e-10 ...
            'arbitrage', 20, ...
            'trend', 1 ...
            }, varargin);
        this.memory = options({'n', 0});
    end

%%** Exec
    function outputs = exec( inputs, varargin)
        
        for m=1:length(inputs)
            if st_data('isempty-nl', inputs{m})
                if m == 1
                    error('Volume Curve Input Empty!')
                end
                if m == 2
                    error('Volatility Curve Input Empty!')
                end
                if m == 3
                    error('Market Impact Input Empty!')
                end
                if m == 4
                    error('Daily Volume Input Empty!')
                end
                outputs{1} = st_data('empty-init');
                return;
            end
        end
        
        
        EPS_1 = 1e-2;
        ITER_MAX = 1000;
        
        
        security_id = this.params.get('security-id');
        trading_destination_id = this.params.get('trading-destination-id');
        order_volume = this.params.get('order-volume');
        lambda = this.params.get('lambda');
        arbitrage = this.params.get('arbitrage');
        
                
        % Volume curve
        
        volume = [inputs{1}.value(1); inputs{1}.value(7:end-1); inputs{1}.value(2)];
        %volume = volume ./ sum(volume);
        
        
        N = size(volume, 1);
        as_of_date = datestr(unique(floor(inputs{1}.date)), 'dd/mm/yyyy');
        dt = [1:N+1]';
        
        % Volatility curve
        
        volatility = inputs{2}.value;        
        
        % Market impact
        
        kappa = inputs{3}.value(2);
        gamma = inputs{3}.value(3);
        
        % Volume march�
        
        market_volume = inputs{4}.value(end);
        
        
       % cd C:\Libs\volume_curves\src
        a = mexVolumeCurves('VwapTrend', N, order_volume, arbitrage, arbitrage, lambda, kappa, gamma,...
            volume, volatility, market_volume(end));
        mid = a(:, 1);
        up = a(:,2);
        down = a(:, 3);
        
        % Calcul de la courbe de trading
        % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
%         st_log(sprintf('computing cash_neutral trading curve on %s\n', as_of_date));
%         
%         x_final = linspace(1, 0, N+1)';
%         
%         for j=1:N-1
%             
%             if j == 1
%                 x_prev = x_final;
%             else
%                 x_prev = linspace( ...
%                     x_final(j), ...
%                     0, ...
%                     N+1 - (j-1))';
%             end
%             
%             err = realmax;
%             err_prev = realmax;
%             nb_iter = 0;
%             
%             
%             
%             while err > EPS_1 && nb_iter < ITER_MAX && err <= err_prev
%                 
%                 err_prev = err;
%                 
%                                 
%                 a = get_coef(j);
%                 b = get_const( ...
%                     j, ...
%                     0, ...
%                     x_final(j), ...
%                     x_final(end));
%                 
%                 if det(a) ~= 0 && ~any(any(isnan(a))) && ~any(any(isinf(a)))
%                     x = real(pinv(a) * b);
%                 else
%                     x = x_prev;
%                 end
%                 
%                 idx = (x_prev ~= 0);
%                 err = nanmean(abs((x(idx) - x_prev(idx)) ./ x_prev(idx)));
%                 x_prev = x;
%                 nb_iter = nb_iter + 1;
%                 
%             end
%             
%             x_final(1 + j) = x(2);
%             
%         end
%         
%         
%         
%         
%         
%         % Calcul de l'enveloppe up
%         % %%%%%%%%%%%%%%%%%%%%%%%%
%         
%         st_log(sprintf('computing cash_neutral wrapping up on %s\n', as_of_date));
%         
%         x_final_up = linspace(1, 0, N+1)';
%         
%         for j=1:N-1
%             
%             if j == 1
%                 x_prev = x_final_up;
%             else
%                 x_prev = linspace( ...
%                     x_final(j), ...
%                     0, ...
%                     N+1 - (j-1))';
%             end
%             
%             err = realmax;
%             err_prev = realmax;
%             nb_iter = 0;
%             
%             
%             
%             while err > EPS_1 && nb_iter < ITER_MAX && err <= err_prev
%                 
%                 err_prev = err;
%                 
%                 
%                 
%                 a = get_coef(j);
%                 b = get_const( ...
%                     j, ...
%                     arbitrage, ...
%                     x_final(j), ...
%                     x_final(end));
%                 
%                 if det(a) ~= 0 && ~any(any(isnan(a))) && ~any(any(isinf(a)))
%                     x = real(pinv(a) * b);
%                 else
%                     x = x_prev;
%                 end
%                 
%                 idx = (x_prev ~= 0);
%                 err = nanmean(abs((x(idx) - x_prev(idx)) ./ x_prev(idx)));
%                 x_prev = x;
%                 nb_iter = nb_iter + 1;
%                 
%             end
%             
%             x_final_up(1 + j) = x(2);
%             
%         end
%         
%         
%         
%         
%         % Calcul de l'enveloppe down
%         % %%%%%%%%%%%%%%%%%%%%%%%%%%
%         
%         st_log(sprintf('computing cash_neutral wrapping down on %s\n', as_of_date));
%         
%         x_final_down = linspace(1, 0, N+1)';
%         
%         for j=1:N-1
%             
%             if j == 1
%                 x_prev = x_final_down;
%             else
%                 x_prev = linspace( ...
%                     x_final(j), ...
%                     0, ...
%                     N+1 - (j-1))';
%             end
%             
%             err = realmax;
%             err_prev = realmax;
%             nb_iter = 0;
%             
%             
%             while err > EPS_1 && nb_iter < ITER_MAX && err <= err_prev
%                 
%                 err_prev = err;
%                 
%                 
%                 a = get_coef(j);
%                 b = get_const( ...
%                     j, ...
%                     -arbitrage, ...
%                     x_final(j), ...
%                     x_final(end));
%                 
%                 if det(a) ~= 0 && ~any(any(isnan(a))) && ~any(any(isinf(a)))
%                     x = real(pinv(a) * b);
%                 else
%                     x = x_prev;
%                 end
%                 
%                 idx = (x_prev ~= 0);
%                 err = nanmean(abs((x(idx) - x_prev(idx)) ./ x_prev(idx)));
%                 x_prev = x;
%                 nb_iter = nb_iter + 1;
%                 
%             end
%             
%             x_final_down(1 + j) = x(2);
%             
%         end
        
        
        
        
        
        colnames = cell(1, 3);
        
        %value = [x_final, x_final_up, x_final_down]; %, mid, up, down];
        
        value = [ mid, up, down];
        colnames{1} = sprintf('trading_curve');
        colnames{2} = sprintf('wrapping_up');
        colnames{3} = sprintf('wrapping_down');        
        
        %colnames{4} = sprintf('mid');
        %colnames{5} = sprintf('up');
       % colnames{6} = sprintf('down');
        
        
        outputs{1} = st_data('init', ...
            'title', 'Trading curve', ...
            'date', dt, ...
            'value', value, ...
            'colnames', colnames);
        outputs{1}.info.security_id = security_id;
        outputs{1}.info.trading_destination_id = trading_destination_id;
        outputs{1}.info.market_volume = market_volume(end);
        
        
        
        
        
        
        % Fonctions annexes
        % %%%%%%%%%%%%%%%%%
        
        function coef = get_coef(n)
            
            dn =  n - 1;
            
            coef = zeros(N+1 - dn, N+1 - dn);
            
            % Diagonale
            
            coef(1, 1) = 1;
            coef(N+1 - dn, N+1 - dn) = 1;
            
            for i=2:N - dn
                coef(i, i) = ...
                    + kappa * gamma * (gamma + 1) * order_volume ^ (gamma + 1) / market_volume(i + dn) ^ gamma * ( ...
                    (x_prev(i-1) - x_prev(i)) ^ (gamma - 1) / volume(i-1 + dn) ^ gamma ...
                    + (x_prev(i) - x_prev(i+1)) ^ (gamma - 1) / volume(i + dn) ^ gamma ...
                    ) ...
                    + lambda * 2 * order_volume ^ 2 * volatility(i + dn) ^ 2 ...
                    + lambda * 2 * order_volume ^ 2 * volatility_trend(i + dn) ^ 2;
            end
            
            % Upper diagonal
            
            for i=2:N - dn
                coef(i, i + 1) = ...
                    - kappa * gamma * (gamma + 1) * order_volume ^ (gamma + 1) / market_volume(i + dn) ^ gamma * ( ...
                    (x_prev(i) - x_prev(i+1)) ^ (gamma - 1) / volume(i + dn) ^ gamma ...
                    );
            end
            
            % Lower diagonal
            
            for i=2:N - dn
                coef(i, i - 1) = ...
                    - kappa * gamma * (gamma + 1) * order_volume ^ (gamma + 1) / market_volume(i + dn) ^ gamma * ( ...
                    (x_prev(i-1) - x_prev(i)) ^ (gamma - 1) / volume(i-1 + dn) ^ gamma ...
                    );
            end
        end
        
        
        
         function const = get_const(n, da, c_1, c_2)
            
            dn =  n - 1;
            
            const = zeros(N+1 - dn, 1);
            
            % Premier terme = solution optimal l� o� on est
            const(1) = c_1;
            const(end) = c_2;
            
            for i=2:N - dn
                const(i) = ...
                    - kappa * (1 - gamma) * (gamma + 1) * order_volume ^ (gamma + 1) / market_volume(i + dn) ^ gamma * ( ...
                    - (x_prev(i-1) - x_prev(i)) ^ (gamma) / volume(i-1 + dn) ^ gamma ...
                    + (x_prev(i) - x_prev(i+1)) ^ (gamma) / volume(i + dn) ^ gamma ...
                    ) ...
                    - order_volume * trend(i + dn) ...
                    + lambda  * 2 * order_volume ^ 2 * sum(volume(i + dn : end)) * volatility(i + dn) ^ 2 ...
                    + lambda  * 2 * order_volume ^ 2 * sum(volume(i + dn : end)) * volatility_trend(i + dn) ^ 2;
            end
            
            % Terme d'opportunit� d'arbitrage
            if volatility(dn + 2) > 0
                const(2) = const(2) + da * order_volume;
            end
            
        end
        
        
        
    end

% < plot function
    function h=plot_(outputs, varargin)
        h = st_plot(outputs, varargin{:});
    end
% >

%%** End of production
    function b = eop
        b = true;
    end

%%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

end