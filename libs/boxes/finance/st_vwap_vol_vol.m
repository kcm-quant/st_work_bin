function o = st_vwap_vol_vol( varargin)
% st_vwap_vol_vol - calcul g�n�rique
%
% See also st_node 
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({ 'input-names:char', '', ... % name of the inputs to use
        'window', '100', ... % window size
        'step',   '20' , ... % step size
        'duration-type', 'points' ... % duration-type (points*/time)
        }, ... 
        varargin );
    this.memory = options({'function', build_st_function( 'init', @(pv)[sum(pv(:,1).*pv(:,2))/sum(pv(:,2)),sqrt(((max(pv(:,1))-min(pv(:,1))).^2/2-(2*log(2)-1)*(pv(1,1)-pv(end,1)).^2)/mean(pv(:,1)).^2)*10000,sum(pv(:,2))], 'date-fun', @(t)t(end), 'colnames-fun', 'VWAP;\sigma GK;Volume', 'title-fun', @(i)i, 'fname', 'st_vwap_vol_vol', 'title', '') });
end

%%** Exec
function outputs = exec( inputs, varargin)
    this_st_function = this.memory.get('function');
    outputs  = {};
    i_names = this.params.get('input-names:char');
    w       = this.params.get('window');
    s       = this.params.get('step');
    m       = this.params.get('duration-type');
    for i=1:length(inputs)
        try
            data = st_data('keep', inputs{i}, i_names);
            if w==0
                outputs{end+1} = this_st_function.apply( data);
            else
                outputs{end+1}  = this_st_function.sliding( data, w, s, m);
            end
        catch
            lasterr('');
        end
    end

end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end
