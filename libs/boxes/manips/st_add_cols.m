function o = st_add_cols( varargin)
% ST_ADD_COLS - ajout de colonnes
%
% See also st_node
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
    'exec', @exec, ...
    'learn', [], ...
    'get', @get_, ...
    'set', @set_, ...
    'end_of_prod', @eop, ...
    'plot',  [] );
%%** Init
    function init( varargin)
        this.params = options({'colnames:char', '1:volume'}, ... % cols of input n to add to last input, format is n:name;etc
            varargin );
        this.memory = options({'n', 0});
    end

%%** Exec
    function outputs = exec( inputs, varargin)
        [outputs, b] = st_data('isempty', inputs);
        if b
            return
        end
        outputs  = outputs(end);
        colnames = tokenize(this.params.get('colnames:char'),';');
        for c=1:length(colnames)
            ncname     = tokenize(colnames{c},':');
            n          = str2double( ncname{1});
            cname      = ncname{2};
            vals       = st_data('col', inputs{n}, cname);
            outputs{1} = st_data('add-col', outputs{1}, vals, cname);
        end
    end

%%** End of production
    function b = eop
        b = true;
    end

%%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

end