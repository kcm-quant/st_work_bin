function o = st_basic_indicator( varargin)
% ST_BASIC_INDICATOR - indicateur agr�g� basique � l'intention des
%       estimateurs les plus courants
%
% read_dataset('gi:basic_indicator', 'security_id', 110, 'from', '21/10/2008', 'to', '21/10/2008', 'trading-destinations', {});
% read_dataset('gi:basic_indicator/source:char�"index', 'security_id', 'CAC40', 'from', '21/10/2008', 'to', '21/10/2008', 'trading-destinations', {});
% read_dataset('gi:basic_indicator/source:char�"index�window:time�1�step:time�1', 'security_id', 'CAC40', 'from', '21/10/2008', 'to', '21/10/2008', 'trading-destinations', {});
% read_dataset('gi:basic_indicator/volume_by_td:b�true', 'security_id', 110, 'from', '10/02/2009', 'to', '10/02/2009', 'trading-destinations', {});
%
% author   : 'rburgot@cheuvreux.com'
% reviewer : ''
% version  : '1.2'
% date     : '20/08/2008'

this.params = {};
this.memory  = {};

o = struct('init', @init, ...
    'exec', @exec, ...
    'learn', [], ...
    'get', @get_, ...
    'set', @set_, ...
    'end_of_prod', @eop, ...
    'plot',  [] );
%%** Init
    function init( varargin)
        this.params = options({},varargin ); % hidden params : 'window:time', 'step:time', 't_acc:time', 'given_trdt:cell', 'bins4stop', 'volume_by_td:b'
        this.memory = options({'n', 0});
    end

%%** Exec
    function outputs = exec( inputs, varargin)
        [outputs, b] = st_data('isempty', inputs);
        if b
            return;
        else
            data = outputs{1};
            if ~isfield(data.info, 'min_step_price')
                try
                    prices = st_data('cols', data, {'price'; 'bid'; 'ask'});
                catch
                    prices = st_data('cols', data, 'price');
                end
                prices = prices(:);
                tmp = unique(round(prices(prices > 1e-8)*1e8));
                data.info.min_step_price = pgcd([unique(abs(diff(tmp(tmp > 0 & isfinite(tmp))))); tmp])*1e-8;% MAGIC NUMBER 1e-8; % MAGIC NUMBER 1e-8
                if ~isfinite(data.info.min_step_price)
                    error('st_basic_indicator:exec', 'Strange data!!!');
                end
            end
        end
        
        time_accuracy = this.params.get('t_acc:time');
        window = this.params.get('window:time');
        step   = this.params.get('step:time');
        curr_day = floor(data.date(1));
        
        if window == 1 && step == window
            daily_indic = true;
        else
            daily_indic = false;
        end
        
        source_mode = this.params.get('source:char');
        v_by_td = this.params.get('volume_by_td:b');
        
        switch source_mode
            case {'tick4bi', 'tick4cfv'}
                %< On isole les donn�es de fixing
                if ~daily_indic
                    if this.params.get('bins4stop:b')
                        idx_auctions = st_data('apply-formula', data, '{auction}|{opening_auction}|{intraday_auction}|{closing_auction}'); %data.value(:, 6) | data.value(:, 7) | data.value(:, 8) | data.value(:, 9);
                    else
                        idx_auctions = st_data('apply-formula', data, '{opening_auction}|{intraday_auction}|{closing_auction}'); %data.value(:, 7) | data.value(:, 8) | data.value(:, 9);
                    end
                    idx_auctions = idx_auctions.value;
                    if all(idx_auctions)
                        warning('st_accum_volume:exec', 'Please have a closer look to day :<%s> for <%s>.\nThe whole day was an auction!\nAs a consequence this day will be deleted from indicateur_volume', datestr(floor(data.date(1))), data.title)
                        outputs = {st_data('log', data, 'gieo', 'No continuous phase trading in these data.')};
                        return;
                    end
                    data_na = st_data('from-idx', data, ~idx_auctions);
                    data_a  = st_data('from-idx', data, idx_auctions);
                else
                    if this.params.get('bins4stop:b')
                        idx_auctions =  st_data('apply-formula', data, '{auction}|{opening_auction}|{intraday_auction}|{closing_auction}');
                    else
                        idx_auctions = st_data('apply-formula', data, '{opening_auction}|{intraday_auction}|{closing_auction}');
                    end
                    idx_auctions = idx_auctions.value;
                    data_a = st_data('from-idx', data, idx_auctions);
                    data.info.fixing_turnover = sum(st_data('cols', data_a, 'volume').*st_data('cols', data_a, 'price'));
                    data_na = st_data('from-idx', data, ~idx_auctions);
                end
                %>
                [grid, trdt, main_td] = get_time_grid('disjoint', data_na, 'is_gmt:bool', false, ...
                    'window:time', window, 'step:time', step, 'given_trdt', this.params.get('given_trdt:cell'), ...
                    'default_date', floor(inputs{1}.date(1)));
                if ~daily_indic
                    %< On traite les donn�es concernant la phase d'�change en continu
                    
                    vals = st_data('cols', data_na, 'price;volume;bid;ask;sell;buy;trading_destination_id'); %data_na.value(:, 1:5);
                    vals = arrayfun(@(inf,sup)sliding_func(vals(inf:sup,:), window), grid(:,1), grid(:,2), 'uni', false);
                    vals = cat(1, vals{:});
                    
                    tmp = abs(sum(vals(:, 1)) / sum(st_data('cols', data_na, 'volume')) - 1);
                    
                    if sum(vals(:, 1))==0
                        st_log(['Please have a closer look to day :<%s> for <%s>. \n\t' ...
                            'Have a special look to continuous phase trading hours %f %% of the volume is out of what we thought to be the hours of the continuous phase.\n\t' ...
                            'As a consequence this day will be deleted from basic_indicator.\n'], datestr(floor(data.date(1))), data.title, 100 * tmp);
                        outputs = {st_data('log', data, 'gieo', 'There is no data in trading phase time !!!.')};
                        return;
                    end
                    
                    if abs(window - step) < datenum(0,0,0,0,0,1) % si step = window, alors on regarde la proportion de volume perdues en ignorant les donn�es en dehors de ce qui est pour nous les horaires de trading en continu. Si cette proportion est trop grande, on rejette les donn�es
                        if tmp > (time_accuracy/(trdt.closing_auction-trdt.opening)) || ~isfinite(tmp)
                            st_log(['Please have a closer look to day :<%s> for <%s>. \n\t' ...
                                'Have a special look to continuous phase trading hours %f %% of the volume is out of what we thought to be the hours of the continuous phase.\n\t' ...
                                'As a consequence this day will be deleted from basic_indicator.\n'], datestr(floor(data.date(1))), data.title, 100 * tmp);
                            outputs = {st_data('log', data, 'gieo', 'There are too much data flagged ''continuous phase'' which are not in what we thought to be the hours of the continuous phase.')};
                            return;
                        end
                    end
                    % si step~=window, alors on regarde au moins si les horaires ne d�bordent pas trop
                    if trdt.closing_auction + time_accuracy < mod(data_na.date(end), 1)
                        st_log(['Please have a closer look to day :<%s> for <%s>. \n\t' ...
                            'Have a special look to the end of the continuous phase trading. It is supposed to be <%s>, but this particular day, it was <%s>.\n\t' ...
                            'As a consequence this day will be deleted from basic_indicator.\n'], datestr(floor(data.date(1))), data.title, datestr(trdt.closing_auction, 'HH:MM:SS'), datestr(data_na.date(end), 'HH:MM:SS'));
                        outputs = {st_data('log', data, 'gieo', sprintf('The end of the continuous phase is supposed to be : <%s> but this set of data ends at : <%s>', datestr(trdt.closing_auction, 'HH:MM:SS'), datestr(data_na.date(end), 'HH:MM:SS')))};
                        return;
                    elseif trdt.opening - time_accuracy > mod(data_na.date(1), 1)
                        st_log(['Please have a closer look to day :<%s> for <%s>. \n\t' ...
                            'Have a special look to the begining of the continuous phase trading. It is supposed to be <%s>, but this particular day, it was <%s>.\n\t' ...
                            'As a consequence this day will be deleted from basic_indicator.\n'], datestr(floor(data.date(1))), data.title, datestr(trdt.opening, 'HH:MM:SS'), datestr(data_na.date(1), 'HH:MM:SS'));
                        outputs = {st_data('log', data, 'gieo', sprintf('The beginning of the continuous phase is supposed to be : <%s> but this set of data begins at : <%s>', datestr(trdt.opening, 'HH:MM:SS'), datestr(data_na.date(1), 'HH:MM:SS')))};
                        return;
                    end
                else
                    % S'il n'y a pas d'auction de fermeture, alors la fin
                    % du continu est trdt.closing et pas
                    % trdt.closing_auction.
                    % Ceci n'est utilis� que pour la volatilit�
                    vals = sliding_func(st_data('cols', data_na, 'price;volume;bid;ask;sell;buy;trading_destination_id'), min(trdt.closing, trdt.closing_auction)-max(trdt.opening, trdt.opening_auction));
                end
                %<
                
                if v_by_td
                    td_ids = [data_na.info.td_info.trading_destination_id];
                    vol_and_td = st_data('cols', data_na, 'volume;trading_destination_id;bid;ask;bid_size;ask_size');
                    rslt = arrayfun(@(inf,sup)sliding_v_by_td(vol_and_td(inf:sup,:), td_ids), grid(:,1), grid(:,2), 'uni', false);
                    vals = cat(2, vals, cat(1, rslt{:}));
                    v_by_td_colns = cat(2, tokenize(sprintf('volume4td_%d;', td_ids)), ...
                        tokenize(sprintf('vwas4td_%d;', td_ids)), tokenize(sprintf('bbo_size4td_%d;', td_ids)));
                    dts  = grid(:,3);
                else
                    %<* On traite les donn�es concernant les auctions
                    [a_vals, a_dts] = agg_and_check_auctions(data_a, trdt, time_accuracy, floor(data.date(1)), main_td);
                    %>
                    
                    if any(any(a_vals == -1))
                        mess = sprintf('There is a problem with an auction \n\tAs a consequence this day will be deleted from basic_indicator.\n');
                        st_log(mess);
                        outputs = {st_data('log', data, 'gieo', mess)};
                        return;
                    end
                    
                    %< On rassemble auction et continu
                    vals = [vals; a_vals];
                    dts  = [grid(:,3); a_dts'];
                    %>
                    %                 else
                    %                     data.date  = grid(:,3);
                    %                     data.value = vals;
                    %                 end
                end
                [data.date, idx_sorted] = sort(dts);
                data.value = vals(idx_sorted, :);
                %<* on met en forme
                data.colnames = {'volume', 'vwap', 'nb_trades', ... % available data in auctions
                    'volume_sell', 'vwap_sell', 'nb_sell', 'vwas', 'vol_GK', ...
                    'open','high','low','close', ...
                    'bid_open', 'bid_high', 'bid_low','bid_close', ...
                    'ask_open', 'ask_high', 'ask_low','ask_close', ...
                    'mean_price', ...
                    'auction', 'opening_auction', 'closing_auction', 'intraday_auction', 'stop_auction'};% available data in auctions
                %>*
                if v_by_td
                    data.colnames = cat(2, data.colnames, v_by_td_colns);
                end
                if length(data.date) > 1 && round(data.date(end)*24*3600) == round(data.date(end-1)*24*3600) % in the table quotation group it happens that closing = closing_auction = closing fixing
                    data.date(end) = data.date(end) + 1/(24*720); % plus five seconds
                end
            case 'index'
                if window ~= 1 || step ~= 1
                    hours = mod(data.date, 1);
                    bod_h = round(min(hours)/datenum(0,0,0,1,0,0))*datenum(0,0,0,1,0,0);
                    eod_h = round(max(hours)/datenum(0,0,0,1,0,0))*datenum(0,0,0,1,0,0);
                    tps_last  = bod_h + window : step : eod_h;
                    
                    sec_dt = 1/(24*360);
                    bound_inf = round((tps_last - window)/sec_dt)*sec_dt;
                    bound_sup = round(tps_last/sec_dt)*sec_dt;
                    
                    idx_first = (length(hours)+1)*ones(length(bound_inf), 1);
                    idx_last  = zeros(length(bound_inf), 1);
                    for i = 1 : length(idx_first)
                        idx = (hours >= bound_inf(i) & hours < bound_sup(i));
                        tmp_idx_first = find(idx, 1, 'first');
                        if ~isempty(tmp_idx_first)
                            idx_first(i) = tmp_idx_first;
                            idx_last(i)  = find(idx, 1, 'last');
                        end
                    end
                    
                    vals = arrayfun(@(inf,sup)index_sliding_func(data.value(inf:sup,:), window), idx_first, idx_last, 'uni', false);
                    vals = cat(1, vals{:});
                    
                    data.date = bound_sup' + floor(data.date(1));
                else
                    open        = data.value(1);
                    close       = data.value(end);
                    high        = max(data.value);
                    low         = min(data.value);
                    mean_price = mean(data.value);
                    vol_GK = sqrt(	( (high-low).^2/2 - (2*log(2)-1)*(close-open).^2 ) / ( mean_price.^2 * window/datenum(0,0,0,0,10,0) )  ) * 10000;
                    
                    vals = [open high low close vol_GK];
                    data.date = floor(data.date(1));
                end
                data.colnames = {'open', 'high', 'low', 'close', 'vol_GK'};
                data.value = vals;
                
                
            case 'future'
                
                % - on exclut les cross pour les stats
                if any(strcmp(data.colnames,'cross'))
                    data=st_data('from-idx',data,~data.value(:,strcmp(data.colnames,'cross')));
                end
                % - on conserve uniquement ces colonnes
                colnames_keep='price;size;bid;ask;bid_size;ask_size';
                data = st_data( 'keep-cols', data, colnames_keep);
                % - on regle le probleme des horaires, dans data c'est en GMT
                % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!ATTENTION !!!!!!!!!!!!!
                % c'est juste g�r� pour le future CAC 40 actuellement!!!!!
                % et le future EUROSTOXX 50
                if strcmp(data.info.future_name,'CAC 40')
                    trdt_info=get_repository('tdinfo',get_sec_id_from_ric('TOTF.PA'));
                    data.date=timezone('convert','init_place_id', 'GMT',...
                        'final_place_id',trdt_info(1).timezone,...
                        'dates',data.date);
                    data.info.localtime=true;
                elseif strcmp(data.info.future_name,'EURO STOXX 50')
                    trdt_info=get_repository('tdinfo',get_sec_id_from_ric('VOWG_p.DE'));
                    data.date=timezone('convert','init_place_id', 'GMT',...
                        'final_place_id',trdt_info(1).timezone,...
                        'dates',data.date);
                    data.info.localtime=true;
                end
                
                if window ~= 1 || step ~= 1
                    hours = mod(data.date, 1);
                    
                    
                    % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!ATTENTION !!!!!!!!!!!!!
                    % les bornes sont g�r� uniquement pour le future CAC 40
                    % et EUROSTOXX 50
                    if strcmp(data.info.future_name,'CAC 40')
                        bod_h = round(mod(datenum('08:00','HH:MM'),1)/datenum(0,0,0,1,0,0))*datenum(0,0,0,1,0,0)-datenum(0,0,0,0,0,10);
                        eod_h = round(mod(datenum('22:00','HH:MM'),1)/datenum(0,0,0,1,0,0))*datenum(0,0,0,1,0,0)+datenum(0,0,0,0,0,10);
                    elseif strcmp(data.info.future_name,'EURO STOXX 50')
                        bod_h = round(mod(datenum('08:00','HH:MM'),1)/datenum(0,0,0,1,0,0))*datenum(0,0,0,1,0,0)-datenum(0,0,0,0,0,10);
                        eod_h = round(mod(datenum('22:00','HH:MM'),1)/datenum(0,0,0,1,0,0))*datenum(0,0,0,1,0,0)+datenum(0,0,0,0,0,10);
                    else
                        bod_h = round(min(hours)/datenum(0,0,0,1,0,0))*datenum(0,0,0,1,0,0);
                        eod_h = round(max(hours)/datenum(0,0,0,1,0,0))*datenum(0,0,0,1,0,0);
                    end
                    
                    
                    tps_last  = bod_h + window : step : eod_h;
                    
                    sec_dt = 1/(24*360);
                    bound_inf = round((tps_last - window)/sec_dt)*sec_dt;
                    bound_sup = round(tps_last/sec_dt)*sec_dt;
                    
                    idx_first = (length(hours)+1)*ones(length(bound_inf), 1);
                    idx_last  = zeros(length(bound_inf), 1);
                    for i = 1 : length(idx_first)
                        idx = (hours >= bound_inf(i) & hours < bound_sup(i));
                        tmp_idx_first = find(idx, 1, 'first');
                        if ~isempty(tmp_idx_first)
                            idx_first(i) = tmp_idx_first;
                            idx_last(i)  = find(idx, 1, 'last');
                        end
                    end
                    vals = arrayfun(@(inf,sup)sliding_func(data.value(inf:sup,:), window), idx_first, idx_last, 'uni', false);
                    vals = cat(1, vals{:});
                    dts_vals = bound_sup' + floor(data.date(1));
                else
                    error('st_basic_indicator:future', 'this window or step is not handle');
                end
                
                tmp = abs(sum(vals(:, 1)) / sum(st_data('cols', data, 'size')) - 1);
                
                if sum(vals(:, 1))==0
                    st_log(['Please have a closer look to day :<%s> for <%s>. \n\t' ...
                        'Have a special look to continuous phase trading hours %f %% of the volume is out of what we thought to be the hours of the continuous phase.\n\t' ...
                        'As a consequence this day will be deleted from basic_indicator.\n'], datestr(floor(data.date(1))), data.title, 100 * tmp);
                    outputs = {st_data('log', data, 'gieo', 'There is no data in trading phase time !!!.')};
                    return;
                end
                
                data.value = vals;
                data.date = dts_vals;
                %<* on met en forme
                data.colnames = {'volume', 'vwap', 'nb_trades', ... % available data in auctions
                    'volume_sell', 'vwap_sell', 'nb_sell', 'vwas', 'vol_GK', ...
                    'open','high','low','close', ...
                    'bid_open', 'bid_high', 'bid_low','bid_close', ...
                    'ask_open', 'ask_high', 'ask_low','ask_close', ...
                    'mean_price', ...
                    'auction', 'opening_auction', 'closing_auction', 'intraday_auction', 'stop_auction'};% available data in auctions
                
        end
        if any(~isfinite(data.date))
            error('st_basic_indicator:exec', 'There is a NaN in dates');
        end
        outputs = {data};
    end

%%** End of production
    function b = eop
        b = true;
    end

%%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

end

function rslt = index_sliding_func(vals, window)
if isempty(vals)
    rslt = [NaN, NaN, NaN, NaN, NaN];
    return;
end
open        = vals(1);
close       = vals(end);
high        = max(vals);
low         = min(vals);
mean_price = mean(vals);
vol_GK = sqrt(	( (high-low).^2/2 - (2*log(2)-1)*(close-open).^2 ) / ( mean_price.^2 * window/datenum(0,0,0,0,10,0) )  ) * 10000;

rslt = [open high low close vol_GK];
end

function rslt = sliding_v_by_td(vals, td_ids)
% vals doit contenir : volume, trading_destination, bid, ask, bid_size, ask_size
traded_volume = zeros(1, length(td_ids));
vwas = NaN(1, length(td_ids));
bbo_size = NaN(1, length(td_ids));
if isempty(vals)
    rslt = [traded_volume, vwas, bbo_size];
    return;
end
idx_valid_bbo = vals(:, 4) > 0 & vals(:, 3)  > 0 & isfinite(vals(:, 4)) & isfinite(vals(:, 3)) & vals(:, 4) > vals(:, 3);
for i = 1 : length(td_ids)
    idx4td = vals(:, 2)==td_ids(i);
    traded_volume(i) = sum(vals(idx4td, 1));
    idx4td = idx4td & idx_valid_bbo;
    if any(idx4td)
        vwas(i) = sum((vals(idx4td, 4) - vals(idx4td, 3)) .* vals(idx4td, 1)) / sum(vals(idx4td, 1));
        bbo_size(i) = weighted_median((vals(idx4td, 5) + vals(idx4td, 6))/2, vals(idx4td, 1));
    end
end
rslt = [traded_volume, vwas, bbo_size];
end

function rslt = sliding_func(vals, window)
% expected data
% data.colnames = {'volume', 'vwap', 'nb_trades', ... % available data in auctions
%             'volume_sell', 'vwap_sell', 'nb_sell', 'vwas', 'vol_GK', ...
%             'open','high','low','close', ...
%             'bid_open', 'bid_high', 'bid_low','bid_close', ...
%             'ask_open', 'ask_high', 'ask_low','ask_close', ...
%             'auction', 'opening_auction', 'closing_auction', 'intraday_auction', 'stop_auction'};% available data in auctions
if isempty(vals)
    rslt = [0, NaN, 0, ...
        0, NaN, 0, NaN, NaN, ...
        NaN, NaN, NaN, NaN, ...
        NaN, NaN, NaN, NaN, ...
        NaN, NaN, NaN, NaN, ...
        NaN,...
        0,0,0,0,0];
    return;
end
% input data : price;volume;bid;ask;sell;buy;trading_destination_id...
price       = vals(:, 1);
volume_     = vals(:, 2);
volume      = sum(volume_);
open        = price(1);
close       = price(end);
high        = max(price);
low         = min(price);

bid         = vals(:, 3);
ask         = vals(:, 4);

idx_valid_ask = ask > 0 & isfinite(vals(:, 3));
idx_valid_bid = bid > 0 & isfinite(vals(:, 4));

idx_valid_bbo = idx_valid_ask & idx_valid_bid & ask > bid;

identified_bos = xor(vals(:, 5), vals(:, 6));
is_sell = ( idx_valid_bbo & price < (bid+ask)/2 ) | ( idx_valid_bid & price <= bid );
is_buy  = ( idx_valid_bbo & price > (bid+ask)/2 ) | ( idx_valid_ask & price >= ask );
is_sell(identified_bos) = vals(identified_bos, 5);
is_buy(identified_bos) = vals(identified_bos, 6);
identified_bos = xor(is_sell, is_buy);

if ~all(identified_bos)% find(~xor(vals(:, 5), vals(:, 6))); vals(find(~xor(vals(:, 5), vals(:, 6))), :)
    is_sell = double(is_sell);
    is_sell(~identified_bos) = 0.5;
end

volume_sell = sum(volume_.*is_sell);
bid(bid==0) = NaN;
ask(ask==0) = NaN;

idx_valid_bbo = ask > 0 & bid  > 0 & isfinite(ask) & isfinite(bid) & ask > bid;
ask = ask(idx_valid_bbo);
bid = bid(idx_valid_bbo);

bid         = min(price(idx_valid_bbo), bid);
ask         = max(price(idx_valid_bbo), ask);

if any(idx_valid_bbo)
    vwas = sum(max(abs(vals(idx_valid_bbo, 4)-price(idx_valid_bbo)), ...
        abs(price(idx_valid_bbo) - vals(idx_valid_bbo, 3))).*volume_(idx_valid_bbo))...
        / sum(volume_(idx_valid_bbo)); % correction pour les carnets updat�s o� la liquidit� ..
    % consom�e par le deal est retir�e avant d'enregistrer le spread
    o_bid = bid(1);
    c_bid = bid(end);
    h_bid = max(bid);
    l_bid = min(bid);
    o_ask = ask(1);
    c_ask = ask(end);
    h_ask = max(ask);
    l_ask = min(ask);
else
    vwas  = NaN;
    o_bid = NaN;
    c_bid = NaN;
    h_bid = NaN;
    l_bid = NaN;
    o_ask = NaN;
    c_ask = NaN;
    h_ask = NaN;
    l_ask = NaN;
end

nb_trades = size(vals, 1);
mean_price = mean(price);

if nb_trades > 1
    vol_GK = sqrt(	( (high-low).^2/2 - (2*log(2)-1)*(close-open).^2 ) / ( mean_price.^2 * window/datenum(0,0,0,0,10,0) )  ) * 10000;
else
    vol_GK = NaN;
end

if volume == 0
    rslt = [volume, NaN, nb_trades, ...
        volume_sell, NaN, sum(is_sell), vwas, vol_GK, ...
        open, high, low, close, ...
        o_bid, h_bid, l_bid, c_bid, ...
        o_ask, h_ask, l_ask, c_ask,...
        mean_price, ...
        0,0,0,0,0];
elseif volume_sell == 0
    rslt = [volume, sum(volume_ .* price) / volume, nb_trades, ...
        volume_sell, NaN, sum(is_sell), vwas, vol_GK, ...
        open, high, low, close, ...
        o_bid, h_bid, l_bid, c_bid, ...
        o_ask, h_ask, l_ask, c_ask,...
        mean_price, ...
        0,0,0,0,0];
else
    rslt = [volume, sum(volume_ .* price) / volume, nb_trades, ...
        volume_sell, sum(volume_ .* is_sell .* price) / volume_sell, sum(is_sell), vwas, vol_GK, ...
        open, high, low, close, ...
        o_bid, h_bid, l_bid, c_bid, ...
        o_ask, h_ask, l_ask, c_ask,...
        mean_price, ...
        0,0,0,0,0];
end


if any(rslt < 0)
    error('st_basic_indicator:exec:sliding_func', 'Strange data!!!');
end
tmp = rslt;
tmp([4:8 13:length(rslt)]) = [];
if any(tmp == 0) || any(~isfinite(tmp))
    error('st_basic_indicator:exec:sliding_func', 'Strange data!!!');
end
end
