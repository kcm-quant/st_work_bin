function o = st_daily_relative( varargin)
% ST_DAILY_RELATIVE - op�re chaque jour par une statistique sur ce jour
%
% See also st_data  
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({'operator', @(v,s)v./s , ... % operator on (value,stat)
        'statistic', 'mean' }, ... % statistic to use
         varargin );
    this.memory = options({'n', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    outputs  = inputs(1);
    oper     = this.params.get('operator');
    stat     = this.params.get('statistic');
    [udays, tmp, ddx] = unique( floor( inputs{1}.date));
    new_vals  = repmat( nan, size(inputs{1}.value));
    all_stats = repmat( nan, length( udays), 1);
    for d=1:length(udays)
        this_day  = (ddx==d);
        all_stats(d) = feval( stat, inputs{1}.value(this_day,:));
        new_vals(this_day,:) = feval( oper, inputs{1}.value(this_day,:), all_stats(d));
    end
    outputs{1}.value = new_vals;
    if isfield( outputs{1}, 'info')
        info = outputs{1}.info;
    else
        info = [];
    end
    info.daily_relative_stats = all_stats;
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end