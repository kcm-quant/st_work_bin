function o = st_dynacc( varargin)
% ST_DYNACC - accumulation dyamique
%
% See also st_node 
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({});
    this.memory = options({'data', []});
end

%%** Exec
function outputs = exec( inputs, varargin)
    data = this.memory.get('data');
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    if isempty( data)
        data = inputs{1};
    else
        data.value = [data.value; inputs{1}.value];
        data.date  = [data.date ; inputs{1}.date ];
    end
    this.memory.set('data', data);
    outputs = { data };
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end