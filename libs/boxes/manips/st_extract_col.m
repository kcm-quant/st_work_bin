function o = st_extract_col( varargin)
% ST_EXTRACT_COL - extraction de colonnes
%
% See also st_data  
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({'colnames', '#rand' }, ... % colnums to extract / begin by a & means grep
         varargin );
    this.memory = options({'n', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    outputs  = inputs;
    colnames = this.params.get('colnames');
    for i=1:length(inputs)
        if colnames(1)=='#'
            switch lower( colnames((2:end)))
                case 'rand'
                    r    = ceil(rand*length(inputs{i}.colnames));
                    outputs{i}.value    = outputs{i}.value(:,r);
                    outputs{i}.colnames = outputs{i}.colnames(r);
                otherwise
                    error('st_extract_col:exec:buildin', 'buildin function <%s> unknown', colnames);
            end
        else
            outputs{i} = st_data('keep', inputs{i}, colnames);
        end
    end
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end