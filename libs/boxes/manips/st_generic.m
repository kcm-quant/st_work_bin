function o = st_generic( varargin)
% ST_GENERIC - calcul g�n�rique
%
% See also st_node 
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({'func:@', @(x)x, ... % function to be compute
        'colnames:char', '', ... % new colnames ('' => keep the same)
        'input-names:char', '', ... % name of the inputs
        'nonans:b', 1, ... % specify what to do with nans
        'mode:char', 'vals', ... % Three modes are currently available : 'vals', 'st_data', inputs. If 'vals', the function is applied on extracted columns, if st_data the function is applied on each st_data of inputs after keeping only the specified columns (in this case, as well as for 'inputs' mode, param 'colnames' and 'nonans' are ignored); if 'inputs', then the function is applied directly on inputs
        },varargin );
    this.memory = options({'n', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs,idx] = st_data('isempty', inputs);
    if ~idx
        f_c      = this.params.get('func:@');
        inames   = this.params.get('input-names:char');
        colnames = this.params.get('colnames:char');
        mode     = this.params.get('mode:char');
        colnames = tokenize(colnames,';');
        switch lower(mode)
            case 'vals'
                for o=1:length(outputs)
                    if isempty( inames)
                        vals = inputs{o}.value;
                    else
                        vals = st_data('cols', inputs{o}, inames);
                    end
                    outputs{o}.value = feval(f_c, vals);
                    nd = size(outputs{o}.value,1);
                    if size(outputs{o}.date,1)>nd
                        outputs{o}.date = outputs{o}.date(end-nd+1:end);
                    elseif size(outputs{o}.date,1)<nd
                        outputs{o}.date = (1:nd)';
                    end
                    if this.params.get('nonans:b')
                        idx = ~all(isfinite( outputs{o}.value) & arrayfun(@(x)imag(x)<eps,outputs{o}.value),2);
                        outputs{o}.value(idx,:)=[];
                        outputs{o}.date(idx,:) =[];
                    end
                    if ~isempty( colnames)
                        outputs{o}.colnames = colnames;
                    end
                end
            case 'st_data'
                for o=1:length(outputs)
                    data = st_data('keep-cols', outputs{o}, inames);
                    outputs{o} = feval(f_c, data);
                end
            case 'inputs'
                outputs = {feval(f_c, outputs)};
            case 'input1_to_all_output'
                outputs = feval(f_c, outputs{1});
            otherwise
                error('Error in st_generic, mode <%s> unknown', lower(mode));
        end
    end
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end