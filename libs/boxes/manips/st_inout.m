function o = st_inout( varargin)
% ST_INOUT - module de recopie des entr�es en sorties
%   premier st_module r�alis�
%
% Le prototype d'un module est simple:
% - une m�thode  .init( varargin)
% - une m�thode  .exec( {input}  , varargin) -> {output}
% - une m�thode .learn( {{input}}, varargin) -> {} ou ![] (cst)
% - une m�thode  .plot( {output} , varargin)  -> h ou ![] (cst)
% - une m�thode .end_of_prod() -> true/false
% - une m�thode .get/.set qui permet de r�cup�r�r/modifier n'importe quoi
%
% L'acc�s � ces m�thodes est verrouill� au niveau de st_node
%
% See also st_node 
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({});
    this.memory = options({'n', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    n = this.memory.set('n', this.memory.get('n')+1);
    outputs = inputs;
    for o=1:length(outputs)
        outputs{o}.n = n;
    end
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end