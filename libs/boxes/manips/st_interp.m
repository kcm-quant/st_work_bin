function o = st_interp( varargin)
% ST_INTERP - interpolation
% of a variable of inputs{1} by a couple of variables of input{2}
% the result is added to inputs{1}
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({'var-to-interp', 'VWAP', ... % name of the variable to interpolate (inputs{1})
        'X-f(X)-names', 'X;\int f(X)', ... % names of the pair (X,f(X)) to use
        'interp-mode', 'nearest' ... % param for interp1
        }, varargin );
    this.memory = options({'n', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    outputs    = inputs(1);
    x_name     = this.params.get('var-to-interp');
    xf_names   = this.params.get('X-f(X)-names');
    x_vals     = st_data( 'col', inputs{1}, x_name);
    xf_vals    = st_data( 'col', inputs{2}, xf_names);
    [xu, udx]  = unique( xf_vals(:,1));
    fu         = xf_vals(udx,2);
    x_interp   = interp1(xu, fu, x_vals, this.params.get('interp-mode') );
    f_name     = tokenize( xf_names, ';', 2);
    outputs{1} = st_data('add-col', outputs{1}, x_interp, sprintf('%s@%s', f_name, x_name));
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end