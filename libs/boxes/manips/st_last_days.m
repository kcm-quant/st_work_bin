function o = st_last_days( varargin)
% ST_LAST_DAYS - select last days
%
% See also st_read_dataset 
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({'nb-of-days:num', 1, ... % number of days to split
        'uses-data_datestamp:bool', 0 ... % uses info.data_datestamp or not
        },varargin );
    this.memory = options({'n', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs,ise] = st_data('isempty', inputs);
    if ise
        outputs{end+1} = st_data('empty');
    else
        nb_days = this.params.get( 'nb-of-days:num');
        uses_datestamp = this.params.get( 'uses-data_datestamp:bool');
        if uses_datestamp
            last_official_date = inputs{1}.info.data_datestamp;
            idx = floor( inputs{1}.date) > last_official_date - nb_days;
        else
            idx = floor( inputs{1}.date) > max(inputs{1}.date) - nb_days;
        end
        outputs = { st_data('from-idx', inputs{1}, idx), ...
            st_data('from-idx', inputs{1}, ~idx) };
        if uses_datestamp
            outputs{2}.info.data_datestamp = last_official_date - nb_days+1;
        end
    end
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end