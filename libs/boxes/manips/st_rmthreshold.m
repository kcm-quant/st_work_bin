function o = st_rmthreshold( varargin)
% ST_RMTHRESHOLD - retire les lignes correspondant � une threshold
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({'thresholds:char', '-1', ... % format= colname&gt;value;colname&lt;value;colname=value
        'effect:char', 'remove'}, ... % remove*/NaN
        varargin );
    this.memory = options({'n', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    outputs = inputs; % un seul signal en entr�e
    thres   = regexp( this.params.get('thresholds:char'), '(?<colname>[^(<>=)]*)(?<comp>[<>=])(?<value>[^;]+);?', 'names');
    idx = repmat(0,size(inputs{1}.date));
    for t=1:length(thres)
        if isempty( thres(t).colname)
            col = inputs{1}.value;
        else
            col  = st_data('col', inputs{1}, thres(t).colname);
        end
        vals = str2double( thres(t).value);
        switch thres(t).comp
            case '>'
                idx = idx | any(col>vals,2);
            case '<'
                idx = idx | any(col<vals,2);
            case '='
                idx = idx | any(abs(col-vals)<eps,2);
            otherwise
                error('st_rmthreshold:compare', 'compoarison operator [%s] unknown', thres(t).comp);
        end
    end
    st_log('%d dates removed.\n', sum(idx));
    outputs{1}.value(idx,:) = [];
    outputs{1}.date(idx,:)  = [];
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end