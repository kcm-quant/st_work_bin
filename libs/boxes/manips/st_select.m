function o = st_select( varargin)
% ST_SELECT - sélectionne suivant une valeur
%  for one input (first) only
%
% See also st_node
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
    'exec', @exec, ...
    'learn', [], ...
    'get', @get_, ...
    'set', @set_, ...
    'end_of_prod', @eop, ...
    'plot',  [] );
%%** Init
    function init( varargin)
        this.params = options({'colnames:char', 'normalized volume;dS', ... % new colnames ('' => keep the same) ; warning: respect de la casse des noms de variable!
            'selection-type:char', 'quantile;threshold', ... % type of selection quantile*|threshold
            'selection-direction:char', '+;-', ... % direction of selection (values to KEEP)
            'values:num', '#[.5, .01]' } ... % value to apply
            ,varargin );
        this.memory = options({'n', 0});
    end

%%** Exec
    function outputs = exec( inputs, varargin)
        [outputs, b] = st_data('isempty', inputs);
        if b
            return
        end
        colnames = tokenize(this.params.get( 'colnames:char'),';');
        sel_type = tokenize(this.params.get( 'selection-type:char'),';');
        sel_dir  = tokenize(this.params.get( 'selection-direction:char'),';');
        sel_vals = this.params.get( 'values:num');
        if ischar( sel_vals)
            sel_vals = eval( sel_vals(2:end));
        end
        for c=1:length(colnames)
            st_log('st_select: selection %d for sequence %s:%s:%s:%f\n', ...
                c, colnames{c}, sel_type{c}, sel_dir{c}, sel_vals(c));
            switch lower(sel_type{c})
                case 'quantile'
                    thr = quantile( st_data('col', outputs{1}, colnames{c}), sel_vals(c));
                case 'threshold'
                    thr = sel_vals(c);
                otherwise
                    error('st_select:exec:type', 'selection type mode <%s> unknown', sel_type{c});
            end
            switch sel_dir{c}
                case { '+', '>' }
                    req = sprintf('{%s}>%f', colnames{c}, thr);
                case { '-', '<' }
                    req = sprintf('{%s}<%f', colnames{c}, thr);
                case { '=', '==' }
                    req = sprintf('abs({%s}-%f)<eps', colnames{c}, thr);
                case { '+=', '>=' }
                    req = sprintf('{%s}>=%f', colnames{c}, thr);
                case { '-=', '<=' }
                    req = sprintf('{%s}<=%f', colnames{c}, thr);
                otherwise
                    error('st_select:exec:direction', 'direction mode <%s> unknown', sel_dir{c});
            end
            outputs{1} = st_data('where', outputs{1}, req);
        end
    end

%%** End of production
    function b = eop
        b = true;
    end

%%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

end