function o = st_slicer( varargin)
% ST_SLICER - calcul g�n�rique
%
% See also st_node build_slicer read_dataset slicer_lib 
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({ ...
        'slicer-name', 'mavg' , ... % - name of the slicer (in slicer_lib)
        'colnames'   , 'price', ... % - name of the colname to apply
        'window'     , 100, ...     % - slider window width
        'step'       , 10, ...      % - slider step
        'type'       , 'p', ...     % - slider type 'p' (points) or 't' (time)
        },varargin );
    this.memory = options({'n', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs,idx] = st_data('isempty', inputs);
    % * inputs{1} any signal with the give column
    if ~idx
        slicer_name = this.params.get('slicer-name');
        colnames    = this.params.get('colnames');
        windoww     = this.params.get('window');
        step        = this.params.get('step');
        stype       = this.params.get('type');
        my_slicer   = slicer_lib( slicer_name);
        outputs     = { my_slicer.sliding( st_data( 'keep', inputs{1}, colnames), ...
            windoww, step, stype) } ;
        % * outputs{1} the sliced signal
    end
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end