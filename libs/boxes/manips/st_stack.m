function o = st_stack( varargin)
% ST_STACK - empilage de signaux
%
% See also st_data
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({ } ...
        ,varargin );
    this.memory = options({'n', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    % Il me faut le premier signal non vide
    i=1;
    while i<=length(inputs)
        [outputs, b] = st_data('isempty', inputs(i));
        if ~b
            break
        end
        i=i+1;
    end
    if b
        % si je n'en ai pas: je retourne du vide
        return;
    end
    % sinon tout va bien � partir de i
    outputs  = inputs(i); % en fait outputs est d�j� �gal � inputs(i), mais c'est plus lisible comme �a
    for j=i+1:length(inputs)
        [tmp, b] = st_data('isempty', inputs(j));
        if ~b
            % st_log('st_stack: adding %d points to %d\n', size(outputs{1}.value,1), size(inputs{j}.value,1));
            outputs{1} = st_data('stack', outputs{1}, inputs{j});
        end
    end
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end