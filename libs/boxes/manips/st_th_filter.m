function o = st_th_filter( varargin)
% ST_TH_FILTER - module qui filtre ou re-traite les donn�es pour lesquels
% les horaires de trading ont chang�s
% ce module s'attend � n'avoir qu'un seul input
% en output, il ressort les donn�es qui correspondent
%
% See also st_node

this.params = {};
this.memory  = {};

o = struct('init', @init, ...
    'exec', @exec, ...
    'learn', [], ...
    'get', @get_, ...
    'set', @set_, ...
    'parallel', false, ...
    'end_of_prod', @eop, ...
    'plot',  [] );
%%** Init
    function init( varargin)
        this.params = options({});
        this.memory = options({'n', 0});
    end

%%** Exec
    function outputs = exec( inputs, varargin)
        if length(inputs) > 1
            error('st_th_filter:exec:check_args', 'Too much inputs, only one input can be handled');
        end
        [outputs, b] = st_data('isempty', inputs);
        outputs{2} = [];
        outputs{3} = [];
        if b
            return
        end
        EPS_DT      = datenum(0,0,0,0,0,1);
        
        bins_hours  = round(mod(outputs{1}.date, 1)/EPS_DT)*EPS_DT;
        hours       = unique(bins_hours);
        nb_hours    = length(hours);
        nb_days     = length(unique(floor(outputs{1}.date)));
        if nb_hours * nb_days ~= length(outputs{1}.date)
            % s'il s'agit juste d'un changement d'horaire des fixings, nous
            % allons simplement corriger cela � la vol�e
            [tmp, idx] = st_data('where', outputs{1}, '{closing_auction}');
            outputs{1}.date(idx) = floor(outputs{1}.date(idx)) + mod(outputs{1}.date(find(idx, 1, 'last')), 1);
            
            [tmp, idx] = st_data('where', outputs{1}, '{opening_auction}');
            outputs{1}.date(idx) = floor(outputs{1}.date(idx)) + mod(outputs{1}.date(find(idx, 1, 'last')), 1);
            
            bins_hours  = round(mod(outputs{1}.date, 1)/EPS_DT)*EPS_DT;
            hours       = unique(bins_hours);
            nb_hours    = length(hours);
            nb_days     = length(unique(floor(outputs{1}.date)));
            if nb_hours * nb_days ~= length(outputs{1}.date)
                [outputs{1}, outputs{2}] = th_filter(outputs{1});
                [outputs{2}, outputs{3}] = th_filter(outputs{2}); % en cas de multiples changements d'horaires
            end
        end
    end

    function [data, d_before] = th_filter(data)
        d_before = [];
        [data, b] = st_data('isempty', data);
        if b
            return
        end
        days = floor( data.date);
        udays = unique(days);
        hours = mod(data.date(udays(end) == days), 1);
        nb_per_day = length(hours);
        
        for i = length(udays)-1 : - 1 : 1
            idx4day = udays(i) == days;
            if sum(idx4day) ~= nb_per_day || any(mod(data.date(idx4day), 1) ~= hours);
                idx_before_hourchange =  data.date < udays(i) + 1;
                d_before = data;
                d_before.value(~idx_before_hourchange, :) = [];
                d_before.date(~idx_before_hourchange, :) = [];
                data.value(idx_before_hourchange, :) = [];
                data.date(idx_before_hourchange, :) = [];
                return;
            end
        end
    end

%%** End of production
    function b = eop
        b = true;
    end

%%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

end