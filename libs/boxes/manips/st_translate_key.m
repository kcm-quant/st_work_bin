function o = st_translate_key( varargin)
% ST_TRANSLATE_KEY - 
% 
%
% See also autocorr
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({ 'key-from', 'info.likely_lags', ... % on input 2
        'key-to', 'likely_lags'}, ... % on input 1
         varargin );
    this.memory = options({'n', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    outputs = inputs(1);
    key_from = tokenize(this.params.get('key-from'), '.');
    key_to   = this.params.get('key-to');
    key      = inputs{2};
    for k=1:length(key_from)
        key = key.(key_from{k});
    end
    eval( [ 'outputs{1}.' key_to '=key;' ]);
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end