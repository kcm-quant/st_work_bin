function o = st_cond_bplot( varargin)
% ST_COND_BPLOT - conditionnal box plot
%
% une seule entr�e (la premi�re est prise en compte).
%
% See also st_plot st_plot_bubble st_box_plot
this.params = {};
this.memory = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  @plot_ );
       
function init( varargin)
    this.params = options({'colnames:char','dS#vol_gk;dS#Normalized volume',... % colnames to use (y#x)
        'nb-quantiles', 50, ... % nb of quantiles to use
        'plot:b', 1 }, ... % auto plot
        varargin);
    
    this.memory = options({});
end

function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    outputs = inputs;
    if this.params.get('plot:b')
        opt = options(varargin);
        plot_focus( opt.get('title'));
        plot_( outputs);
    end
end

function h = plot_(inputs, varargin)
    colnames = tokenize(this.params.get( 'colnames:char' ), ';');
    nb_q    = this.params.get( 'nb-quantiles');
    [n,p]   = subgrid( length(colnames));
    for c=1:length(colnames)
        cs     = tokenize( colnames{c}, '#');
        y_val  = st_data('col', inputs{1}, cs{1});
        x_val  = st_data('col', inputs{1}, cs{2});
        q_vals = quantile( x_val, linspace(0,1, nb_q));
        x_idx  = repmat(1, size(x_val));
        for q=2:length(q_vals)-1
            x_idx = x_idx + (x_val>q_vals(q));
        end
        
        subplot(n,p,c);
        pq = (q_vals(1:end-1)+q_vals(2:end))/2;
        tq = tokenize(sprintf(' %3.2f;', pq));
        dq = diff(q_vals);
        dq(dq<eps) = 0.1;
        boxplot(y_val, x_idx, 'labels', [], ...
            'positions', pq, 'width', dq);
        set(gca,'xticklabel',[]);
        ax = axis;
        axis([q_vals(1)-dq(1)/4, q_vals(end)+dq(end)/4, ax(3:4)]);
        text( pq, repmat(ax(3),size(pq)), tq, 'rotation',-90, 'fontsize',8);
        ylabel( cs{1});
        stitle('%s - %s', inputs{1}.title, cs{2});
    end
    h = gcf;
end
%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end