function o = st_iday_distrib( varargin)
% ST_IDAY_DISTRIB - analyse d'une distribution intra day
%  Le seul effet est de fusionner tous les jour et de trier par date
%
% See also st_node 
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  @plot_ );
%%** Init       
function init( varargin)
    this.params = options({ 'transform:func', @log, ... % - optional transform to apply to the data
        'plot:b', 0, ... % - automatic plot
        'colname:char', '' }, ... % - colname to apply on
        varargin );
    this.memory = options({'n', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    outputs = inputs;
    for i=1:length(inputs)
        [outputs{i}.date, idx] = sort( mod(outputs{i}.date,1));
        outputs{i}.value = outputs{i}.value(idx,:);
        outputs{i}.attach_format = 'HH:MM';
    end
    if this.params.get('plot:b')
        opt = options(varargin);
        plot_focus( opt.get('title'));
        plot_( outputs);
    end
end

%%** Plot
function h = plot_( inputs, varargin)
    data   = inputs{1};
    transf = this.params.get( 'transform:func');
    if ~isempty( transf)
        nb_prows = 2;
    else
        nb_prows = 1;
    end
    colname = this.params.get('colname:char');
    if isempty( colname)
        val = data.value(:,1);
    else
        val = st_data('col', data, colname);
    end
    dts     = data.date;
    mima    = quantile(dts, [0 1]);

    % toutes les journ�es
    % moyenne
    [ddts, tmp, tdx] = unique(dts);
    sATS = accumarray( tdx, val)./accumarray( tdx, ones(size(tdx)));
    lATS = accumarray( tdx, log(val))./accumarray( tdx, ones(size(tdx)));
    % donn�es brutes
    a1=subplot(nb_prows,2,1);
    plot(dts, val, '.', 'color', [.67 0. 0.12])
    hold on
    plot(ddts, sATS, 'color', [0 .12 .67], 'linewidth',2);
    hold off
    title( data.colnames{1});
    ax = axis;
    axis([mima ax(3:4)]);
    attach_date('init', 'date', mima, 'format', 'HH:MM');
    % densit� ATS
    a2=subplot(nb_prows,2,2);
    [x,y] = ksdensity(val);
    fill(x,y, 'r', 'facecolor',[.67 0 .12], 'edgecolor', 'none');
    linkaxes([a1,a2],'y');
     
    if nb_prows == 2
        % log ATS
        tvals = feval( transf, val);
        a3=subplot(nb_prows,2,3);
        plot(mod(data.date,1), tvals, '.', 'color', [.67 0. 0.12])
        hold on
        plot(ddts, lATS, 'color', [0 .12 .67], 'linewidth',2);
        hold off
        ax = axis;
        axis([mima ax(3:4)]);
        attach_date('init', 'date', mima, 'format', 'HH:MM');
        stitle( '%s [ %s ]', func2str( transf), data.colnames{1});
        % densit� log ATS
        a4=subplot(nb_prows,2,4);
        [x,y] = ksdensity( tvals);
        fill(x,y, 'r', 'facecolor',[.67 0 .12], 'edgecolor', 'none');
        linkaxes([a3,a4],'y');
    end
    h = gcf;
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end