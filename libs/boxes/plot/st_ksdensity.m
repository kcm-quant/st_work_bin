function o = st_ksdensity( varargin)
% ST_KSDENSITY - apply ksdensity
%
% See also st_plot st_box_plot ksdensity
this.params = {};
this.memory = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  @plot_ );
       
function init( varargin)
    this.params = options({'colnames:char','dS',... % colnames to use 
        'kernel', 'epanechnikov' , ... % name of the kernel
        'plot:b', 1 }, ... % auto plot
        varargin);
    
    this.memory = options({});
end

function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    outputs = inputs;
    if this.params.get('plot:b')
        opt = options(varargin);
        plot_focus( opt.get('title'));
        plot_( outputs);
    end
end

function h = plot_(inputs, varargin)
    colnames = tokenize(this.params.get( 'colnames:char' ), ';');
    kername  = this.params.get( 'kernel');
    [n,p]    = subgrid( length(colnames));
    for c=1:length(colnames)
        vals  = st_data('col', inputs{1}, colnames{c});
        [y,x] = ksdensity( vals, 'kernel', kername);
        qq    = [0 2.5 25 50 75 97.5 100]/100;
        qs    = quantile(vals, qq);
        for q=1:length(qs)
            st_log('Quantile %3.2f%% for %s is %3.2f\n', qq(q)*100, colnames{c}, qs(q));
        end
        
        subplot(n,p,c);
        fill( x, y, 'r', 'edgecolor', [.67 0 .12], 'facecolor', (1-(1- [.67 0 .12])/5));
        stitle('%s - %s', inputs{1}.title, colnames{c});
    end
    h = gcf;
end
%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end