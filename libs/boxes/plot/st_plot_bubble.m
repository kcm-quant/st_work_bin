function o = st_plot_bubble( varargin)
% ST_PLOT_BUBBLE - plot 4D  or scatter plot
%
% See also plot_bubble
this.params  = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'end_of_prod', @eop, ...
           'get', @get_, ...
           'set', @set_, ...
           'plot',  @plot_ );
%%** Init       
function init( varargin)
    this.params = options({'center-names', 'dB;\sigma GK#dB;dB', ... % - eponym le sÚparateur est #
        'radius-names', 'Volume (pct);Volume (pct)# ', ... % - eponym le sÚparateur est # (ne pas oublier espace)
        'nb-points', 12, ... % to trace ellipsoids
        'coef-radius', 0, ... % 0 mean auto
        'plot:b', 1 ... % automated-plot
        }, varargin); 
    this.memory = options({});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    outputs = inputs;
    if this.params.get('plot:b')
        opt = options(varargin);
        h = plot_focus( opt.get('title'));
        plot_( outputs);
    end
end

%%** Plot
function h = plot_( inputs, varargin)
    h = gcf;
    u = findobj(h, 'Type', 'uipanel');
    cr = this.params.get('coef-radius');
    if numel(cr)==1 && cr(1)==0
        cr = [];
    end
    all_r_names = tokenize(this.params.get('radius-names'),'#');
    all_c_names = tokenize(this.params.get('center-names'),'#');
    if numel(all_r_names)~=numel(all_c_names) && numel(all_r_names)~=0
        error('st_plot_bubble:params', 'nb of radius =/= nb of centers');
    end
    A = length(all_c_names);
    [n,p] = subgrid( A);
    for a=1:A
        subplot(n,p, a);
        c_names = all_c_names{a};
        if a>length(all_r_names)
            r_names = '';
        else
            r_names = all_r_names{a};
        end
        if isempty( r_names) || strcmp(r_names,' ')
            xy_names = tokenize( c_names, ';');
            vals = st_data('col', inputs{1}, c_names);
            plot( vals(:,1), vals(:,2), '.', 'color', [.67 0 .12]);
            xlabel(xy_names{1}); ylabel( xy_names{2});
            title( inputs{1}.title);
        else
            plot_bubble( inputs{1}, 'center-names', c_names, ...
                'radius-names', r_names , 'nb-points', this.params.get('nb-points'), ...
                'coef-radius', cr, 'panel', u);
        end
    end
end

%%** End of production
function b = eop
    b = true;
end
%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end