function o = st_plot_generic( varargin)
% ST_PLOT_GENREIC - plot generique
%
% See also st_plot 
this.params  = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'end_of_prod', @eop, ...
           'get', @get_, ...
           'set', @set_, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({ 'fuse-mode:char', 'none', ... % - fuse all signals demux/no*/base/mean/min-max/reg/normalize/all
        'add-keys', 'attach_format,''HH:MM''', ... % - keys to add
        'regression', 'Volume (pct)~\sigma GK;dB', ... % - optional regression sous la forme tilde
        'plot:b', 1 ... % automated-plot
        }, varargin); 
    this.memory = options({});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    try
        % demux param is for compatibility
        demux = this.params.get('demux:b');
        if demux
            fuse_mode = 'demux';
        else
            fuse_mode = 'none';
        end
    catch
        lasterr('');
    end
    try
        fuse_mode = this.params.get('fuse-mode:char');
    catch
        lasterr('');
        st_log('st_plot_generic:params - please reinit this node and choose a <fuse-mode> rather than a <demux> value (which is obsolete)\n');
    end
    switch lower(fuse_mode)
        case 'demux'
            outputs = cellflat(cellfun(@(d)st_data('demux',d), inputs, 'uni', false));
        case { 'no', 'none'}
            outputs = inputs;
        case 'base'
            outputs = { st_data('interp', inputs{:}) };
            idx     = any(isnan(outputs{1}.value),2);
            outputs{1}.value(idx,:) = [];
            outputs{1}.date(idx,:) = [];
            outputs{1}.value = bsxfun( @times, 100*outputs{1}.value, 1./ outputs{1}.value(1,:));
        case 'min-max'
            outputs = { st_data('interp', inputs{:}) };
            outputs{1}.value = bsxfun( @minus, outputs{1}.value, nanmin(outputs{1}.value));
            outputs{1}.value = bsxfun( @times, outputs{1}.value, 1./ nanmax(outputs{1}.value));
        case 'normalize'
            outputs = { st_data('interp', inputs{:}) };
            outputs{1}.value = bsxfun( @minus, outputs{1}.value, nanmean(outputs{1}.value));
            outputs{1}.value = bsxfun( @times, outputs{1}.value, 1./ nanstd(outputs{1}.value));
        case 'mean'
            outputs = { st_data('interp', inputs{:}) };
            outputs{1}.value = bsxfun( @minus, outputs{1}.value, nanmean(outputs{1}.value));
        case 'all'
            outputs = { st_data('interp', inputs{:}) };
        case 'reg'
            outputs = { st_data('interp', inputs{:}) };
            error('st_plot_generic:fuse_mode', 'fuse mode <%s> not available yet', fuse_mode);
        otherwise
        error('st_plot_generic:fuse_mode', 'fuse mode <%s> unknown', fuse_mode);
    end
    keys2add = tokenize(this.params.get('add-keys'));
    if ~isempty( keys2add)
        for k=1:2:length(keys2add)-1
            val = keys2add{k+1};
            val = eval([ val ';']);
            for ot=1:length(outputs)
                outputs{ot}.(keys2add{k}) = val;
            end
        end
    end
    %< Regression
    reg_names = this.params.get('regression');
    if ~isempty( reg_names)
        % je vais archiver les composantes de mes variabels dans un vecteur
        % ligne [output , col]
        yx_names = tokenize( reg_names, '~');
        y_name   = yx_names{1};
        x_names  = tokenize( yx_names{2}, ';');
        y_coords = [NaN NaN];
        x_coords = repmat(NaN,length(x_names), 2);
        x_vals   = [];
        for o=1:length(outputs)
            try
                [y_val, y_idx] = st_data('col', outputs{o}, y_name);
                y_coords = [o, y_idx];
                st_log('Y variable <%s> found\n', y_name);
            catch
                lasterr('');
            end
            for x=1:length(x_names)
                try
                    [x_val, x_idx] = st_data('col', outputs{o}, x_names{x});
                    x_coords(x,:) = [o, x_idx];
                    x_vals = [x_vals, x_val];
                    st_log('X variable <%d:%s> found\n', x, x_names{x});
                catch
                    lasterr('');
                end
            end
        end
        x_vals = cat(2, x_vals, ones(size(x_vals,1),1));
        [b, b_int, r, r_int, stats] = regress( y_val, x_vals);
        y_hat = y_val - r;
        colname = '';
        for c=1:length(x_names)
            colname = sprintf('%s+ %5.4f %s', colname, b(c), x_names{c});
        end
        colname = sprintf('%s ~%s + %5.4f', y_name, colname(2:end), b(end));
        outputs{y_coords(1)} = st_data('add-col', outputs{y_coords(1)}, y_hat, colname);
    end
    %>
    if this.params.get('plot:b')
        opt = options(varargin);
        h = plot_focus( opt.get('title'));
        u = findobj(h, 'Type', 'uipanel');
        st_plot( outputs, 'panel', u);
    end
end

%%** End of production
function b = eop
    b = true;
end
%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end