function o = st_score_plot( varargin)
% ST_SCORE_PLOT - 2D plot associated with intra day visualization
%
%  The first input is the only one plotted
%
% See also sc_plot 
this.params  = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'end_of_prod', @eop, ...
           'get', @get_, ...
           'set', @set_, ...
           'plot',  @plot_ );
%%** Init       
function init( varargin)
    this.params = options({'x-colname', 'Chx Score', ... % eponym
        'y-colname', 'Slippage', ... % eponym
        'secid-colname', 'Security', ... % column containing security ids
        'begin-time-colname', '', ... % column name for begin time (optional)
        'end-time-colname', '', ... % column name for end time (optional)
        'side-colname', '', ... % column name for side buy=0, sell=1 (optional)
        'info-colnames', '', ... % columns to use as info (on plot and on title)
        'info-preprocess', @(x)[''], ... % to build the title
        'info-plot-id', [], ... % to use on plot
        'plot:b', 1 ... % automated-plot
        }, varargin); 
    this.memory = options({});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    outputs = inputs;
    if this.params.get('plot:b')
        opt = options(varargin);
        h = plot_focus( opt.get('title'));
        plot_( outputs);
    end
end

%%** Plot
function h = plot_( inputs, varargin)
    h = gcf;
    lst = this.params.get();
    sc_plot( inputs{1}, lst{:});
end

%%** End of production
function b = eop
    b = true;
end
%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end