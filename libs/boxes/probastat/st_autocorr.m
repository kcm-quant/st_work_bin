function o = st_autocorr( varargin)
% ST_AUTOCORR
%
% See also autocorr
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  @plot_ );
%%** Init       
function init( varargin)
    this.params = options({ 'colnames', '', ... % column to work on
        'max-lags', '', ... % Positive, scalar integer indicating the number of lags of the ACF to compute
        'MA(N)', 2 , ... % Nonnegative integer scalar indicating the number of lags beyond which the theoretical ACF is effectively 0.
        'plot:b', 1}, ...
         varargin );
    this.memory = options({'n', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    outputs  = inputs([1 1]);
    colnames = this.params.get('colnames');
    if isempty( colnames)
        vals = inputs{1}.value;
    else
        vals = st_data('col', inputs{1}, colnames);
    end
    mlag = this.params.get( 'max-lags');
    if isempty( mlag)
        mlag = min([20, size(vals,1)-1]);
    end
    man    = this.params.get( 'MA(N)');
    ACF    = repmat(nan, mlag+1, size(vals,2));
    bounds = repmat(nan, 2, size(vals,2));
    likely_lags = repmat(nan, 1, size(vals,2));
    for c=1:size(vals,2)
        [ACF(:,c), lags, bounds(:,c)] = autocorr(vals(:,c), mlag, man);
        likely_lags(c) = find( ACF(:,c)<bounds(1,c) & ACF(:,c)>bounds(2,c), 1, 'first')-1;
    end
    dlag = median( diff( inputs{1}.date));
    
    outputs{1}.value = ACF;
    outputs{1}.date  = lags * dlag;
    outputs{1}.title = 'ACF';
    
    outputs{2}.value = bounds;
    outputs{2}.date  = [1;2];
    outputs{2}.rownames = { 'Max bound' , 'Min Bound'};
    outputs{2}.title     = 'ACF Bounds';
    if isfield( outputs{2}, 'info')
        info = outputs{2}.info;
    else
        info = [];
    end
    info.likely_lags = likely_lags;
    outputs{2}.info = info;
    
    if this.params.get('plot:b')
        opt = options(varargin);
        plot_focus( opt.get('title'));
        plot_( outputs);
    end
end

%%** Plot
function h = plot_( inputs, varargin)
    nC = size( inputs{1}.value, 2);
    [n,p] = subgrid( nC);
    d0 = -inputs{1}.date(2)/2;
    for c=1:nC
        subplot(n,p,c);
        stem( inputs{1}.date, inputs{1}.value(:,c), 'linewidth', 2);
        hold on
        plot( [d0 max(inputs{1}.date) nan d0 max(inputs{1}.date)], ...
            [inputs{2}.value(1,c) inputs{2}.value(1,c) nan inputs{2}.value(2,c) inputs{2}.value(2,c)], ...
            'r', 'linewidth', 2);
        hold off
        if c==1
            title( inputs{1}.title);
        end
        ax = axis;
        axis([d0 max(inputs{1}.date) ax(3:4)]);
        legend( inputs{1}.colnames(c));
        attach_date( 'init', 'date', [d0 max(inputs{1}.date)], 'format', 'HH:MM:SS');
    end
    h = gcf;
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end