function o = st_baselearn( varargin)
% ST_BASELEARN - learning a quantile level
this.params  = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', @learn, ...
           'end_of_prod', @eop, ...
           'get', @get_, ...
           'set', @set_, ...
           'plot',  @plot_ );
%%** Init       
function init( varargin)
    this.params = options({'quantile:double', .99 ... % - eponym
        'colname:char', 'volume', ... % - name of the column to work on
        'output-type', 'Empirical quantile', ... % - type of threshold to apply to the output
        'plot:b', 1 ... % - automatic plot
        }, varargin); 
    this.memory = options({'quantile-level', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    ql = this.memory.get('quantile-level');
    outputs = repmat( {inputs{1}}, 1, 3) ;
    % la premi�re sortie est juste augment�e du champ threshold
    outputs{1}.threshold = ql;
    % s�lection
    thres = st_data('col', ql, this.params.get('output-type'));
    % la deuxi�me sortie est un fitrage haut
    outputs{2} = st_data('where', inputs{1}, [ '{' this.params.get('colname:char') '}>' num2str( thres)]);
    % la troisi�me sortie un filtrage bas
    outputs{3} = st_data('where', inputs{1}, [ '{' this.params.get('colname:char') '}<=' num2str( thres)]);
    % statistiques pour info
    vals = st_data('col', inputs{1}, this.params.get('colname:char'));
    for c=1:length(ql.colnames)
        st_log('    st_baselearn: %3.2f%% remaining vs %3.2f%% (over %d) in mode %s\n', sum(vals>ql.value(1,c))/length(vals)*100, 100*(1-this.params.get('quantile:double')), length(vals), ql.colnames{c});
    end
    % automated plot
    if this.params.get('plot:b')
        opt = options(varargin);
        plot_focus( opt.get('title'));
        plot_( outputs);
    end
end

%%** Learn
function outputs = learn( inputs, varargin)
    colname = this.params.get('colname:char');
    vals    = cellfun(@(c)st_data('col', c{1}, colname)', inputs, 'uni', false);
    vals    = [vals{:}]';
    
    p_alpha    = this.params.get('quantile');
    [m, sigma] = estimationRobuste('gaussien', log(vals), 'quantile-level', .25, 'troncate-level', 1.96);
    qln        = exp( m+norminv( p_alpha)*sigma);
    
    qs = [quantile( vals, p_alpha), qln ];
    ql = st_data('init', 'title', 'Thresholds', ...
        'colnames', { 'Empirical quantile' , 'Log-normal based quantile' }, ...
        'date', NaN, 'value', qs, 'rownames', { colname });
    this.memory.set('quantile-level', ql);
end


function h = plot_( inputs, varargin)
    read_dataset( 'plot-tickdb', inputs{1});
    h = gcf;
end

%%** End of production
function b = eop
    b = true;
end
%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end