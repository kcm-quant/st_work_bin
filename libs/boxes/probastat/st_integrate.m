function o = st_integrate( varargin)
% ST_INTEGRATE - intégration par une variable
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({'f(X)-variable', '1/\sigma', ... % function to integrate
        'X-variable', 'X', ... % variable to integrate
        'nonans:b', 1}, ... % specify what to do with nans
        varargin );
    this.memory = options({'n', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    outputs  = inputs(1);
    f_X_name = this.params.get('f(X)-variable');
      X_name = this.params.get('X-variable');
    f_X      = st_data('col', inputs{1}, f_X_name);
      X      = st_data('col', inputs{1},   X_name);
    [X, idx] = sort(X);
    f_X      = f_X(idx);
    int_f_X  = cumsum( f_X(2:end) .* diff(X));
    outputs{1}.value    = [int_f_X, X(2:end)];
    outputs{1}.date     = X(2:end);
    if isfield( outputs{1}, 'attach_format')
        outputs{1} = rmfield( outputs{1}, 'attach_format');
    end
    outputs{1}.colnames = { sprintf('\\int f(%s)', X_name); X_name };
end

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end