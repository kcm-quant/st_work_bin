function o = st_learnmax( varargin)
% ST_LEARNMAX - learning a quantile level
%
% See also estimationRobuste ScoreMaxLog
this.params  = {};
this.memory  = {};
% Author   'Dana Croize'
% Reviewer 'Charles-Albert Lehalle'
% Date     '22/07/2008'
% Version  '3.0'
o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', @learn, ...           
           'end_of_prod', @eop, ...
           'get', @get_, ...
           'set', @set_, ...
           'plot',  @plot_ );
%%** Init       
function init( varargin)
    this.params = options({ ...
        'quantile:double', .999 ...          % - quantile level (proba threshold to use, combined with quantile-double)
        'quantile-type', 'proba', ...        % - quantile type (proba*/value) ex (.95/1.96)
        'colname:char', 'volume;price', ...  % - name of the column to work on (volume;price)
        'preprocess'  , @(x)x(:,1)/mean(x(:,1)), ... % - preprocess @(x)x(:,1)/mean(x(:,1)) : divide by ATS
        'learn-mode'  , 'learn', ...         % - learning mode (learn*/exec), exec means: new learn at each exec
        'threshold-type', 'Max based quantile', ... % - type of threshold to apply to the output (Empirical quantile/Log-normal based quantile/Max based quantile*)
        'test-threshold', [], ...            % - threshold to test (for plot and output): max priority if filled
        'method', 'none', ...                % - to bootstrap reults (none*/boots)
        'quantile-level', 0.05, ...          % - quantile level (estimationrobuste, 1st step std estmation)
        'robustness:double', [], ...         % - robustness (flat size)
        'plot:b', 1  ...                     % - automatic plot
        }, varargin);
    try
        this.memory.get('quantile-level');
    catch er
        this.memory = options({'quantile-level', [], 'Bootstrap' , [], ...
            'Capital',[],'ATS',[]});
    end
    
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        error('NODATA_FILTER: No data for this day.')
    end
    %<* Mode de learn
    % Si on est en mode 'exec', on apprend � la voll�e
    learn_mode = this.params.get('learn-mode');
    if strcmpi( learn_mode, 'exec')
        learn( inputs, varargin{:});
    end
    %>*
    Nb_days = length(unique( floor( inputs{1}.date)));
    % Je r�cup�re les param�tres internes
    ql  = this.memory.get('quantile-level');
    ATS = this.memory.get('ATS');
    if isempty( ql)
        error('st_learnmax:exec', 'mode <learn> needed first');
    end
    outputs = { inputs{1}, [], []};
    % * output3 : idem avec le champ threshold
    qq = st_data('keep', ql, 'Observed Max;Max based quantile');
    % je ne concerve que les seuils et je les met au bon format
    qq.value = qq.value * ATS;
    % * output2 : ceux qui d�passent
    colname   = this.params.get('colname:char');
    vals      = st_data('col', inputs{1}, colname);
    vals      = vals(:,1);
    q_lev     = this.params.get('test-threshold');
    if isempty( q_lev)
        q_lev     = st_data('col', ql, this.params.get('threshold-type')) * ATS;
    end
    idx       = vals<q_lev;
    inputs{1}.value(idx,:) = [];
    inputs{1}.date(idx)    = [];
    outputs{2} = inputs{1};
    Nb_kept    = length( outputs{2}.date);
    
    if ~isempty( this.params.get('test-threshold'))
        qq = st_data('add-col', qq, q_lev, 'External thres.');
    end
    outputs{1}.threshold = qq;
    if isfield( outputs{1}, 'info')
        info = outputs{1}.info;
    else
        info = [];
    end

    info.learnmax = struct('nb_total', length( outputs{1}.date), 'nb_out', Nb_kept);
    outputs{1}.info = info;
    % * outputs1 : les thresholds
    % destination: le STATISTICAL ENGINE!
    idx = cellfun(@(n)strmatch(n,ql.colnames,'exact'), { 'Observed max', 'Empirical quantile', ...
        'Log-normal based quantile',  'Max based quantile', 'Max based opt1', 'Max based opt2'}, ...
        'uni', false);
    idx = [idx{cellfun(@(x)~isempty(x),idx)}];
    ql.value(idx) = ql.value(idx)*ATS;
    ql.colnames = regexprep( ql.colnames, 'Max based quantile', 'Threshold');
    ql.colnames = regexprep( ql.colnames, 'Max based opt1', 'Threshold opt 1');
    ql.colnames = regexprep( ql.colnames, 'Max based opt2', 'Threshold opt 2');
    
    ql = st_data('add-col', ql, ATS, 'ATS');
    ql = st_data('add-col', ql, Nb_kept/Nb_days, 'run_quality');
    outputs{3} = ql;
    % pour le statistical engine:
    outputs = outputs([3 2 1 ]);
    
    % automated plot
    if this.params.get('plot:b')
        opt = options(varargin);
        plot_focus( opt.get('title'));
        plot_( outputs);
    end
end

%%** Learn
function outputs = learn( inputs, varargin) 
    %<* R�cup�ration des donn�es
    colname  = this.params.get('colname:char');
    if iscell( inputs{1})
        vals     = cellfun(@(c)st_data('col', c{1}, colname), inputs, 'uni', false);
        vals     = cat(1,vals{:});
        nb_days  = length(inputs{1});
    else
        vals = st_data('col', inputs{1}, colname);
        nb_days = length(unique(floor(inputs{1}.date)));
    end
    ATS     = mean( vals(:,1));
    Capital = mean( vals(:,1).*vals(:,2));
    fprep   = this.params.get('preprocess');
    vals    = feval( fprep, vals); % application du preprocess
    %>*
    %<* Travail sur les log
    vals(vals<=0)=[];
    vals = log(vals);
    %>*
    %<* Param�tre de la loi des volumes
    % Il s'agit des log(volumes/ATS).
    % on prend l'estimation ROBUSTE.
    [m, sigma] = estimationRobuste('gaussien', vals, ...
        'quantile-level', this.params.get('quantile-level'), ...
        'troncate-level', 1.96);
    %>*
    %<* Gestion du seuil
    % Pour les mod�les param�trique
    p_alpha    = this.params.get('quantile:double');
    q_type = this.params.get('quantile-type');
    switch lower( q_type)
        case 'proba'
            % rien � faire
            alpha  = norminv( p_alpha);
        case 'value'
            alpha   = p_alpha;
            p_alpha = normcdf( p_alpha);
        otherwise
            error('st_learnmax:quantile_type', 'quantile type <%s> unknow', q_type);
    end
    % m�thode 'gaussian'
    %qln = exp( m+p_alpha*sigma);
    % m�thode 'empirical'
    
    % stockage de max/quantile empirique/quantile theorique
    qs  = [exp(max( vals)), exp(quantile( vals, p_alpha)), exp( m+alpha*sigma) ];
    
    %<* Loi des max
    
    scores_and_values = ScoreMaxLog(ceil(size(vals,1)/nb_days), m, sigma);
    scores_and_values(:,2) = exp( scores_and_values(:,2));
    % j'ai un vrai probl�me sur le choix de la bonne fronti�re
    % je vais utiliser deux crit�res:
    % 1. sup�rieur � p_alpha
    % 2. premier plateau d'au moins flat
    L = size(scores_and_values,1);
    rob = this.params.get( 'robustness:double');
    if isempty( rob)
        chosen_score = find( scores_and_values(:,1)>=p_alpha*100, 1, 'first');
    else
        flat_size = (1-rob) * L;
        evals     = exp(vals);
        flat_vals = diff(arrayfun( @(v)sum(evals>v), scores_and_values(:,2)))==0;
        flat_pos  = bin_find( flat_vals);
        flat_idx  = flat_pos(flat_pos(:,2)>=flat_size,1);
        chosen_score = flat_idx(find( flat_idx>p_alpha*L,1));
        if isempty( chosen_score)
            st_log('st_learnmax: Problem with chosen_score...\n');
            chosen_score = L;
        end
    end
    
    if chosen_score < L-1
        chosen_score = chosen_score:chosen_score+2;
    elseif chosen_score < L
        chosen_score = [chosen_score chosen_score:chosen_score+1];
    elseif chosen_score == L
        chosen_score = repmat(chosen_score,1,3);
    else
        st_log('No operation threshold found, max used\n');
        if isempty( scores_and_values)
            % max des donn�es
            warning('st_learnmax:learn', 'I should not be here, please call Dana to report this unespected warning!!!\n');
            scores_and_values = [NaN, exp(max(vals))];
            chosen_score = 1;
        else
            % max des simulations (si il existe)
            chosen_score = size(scores_and_values,1);
        end
        chosen_score = repmat(chosen_score,1,3);
    end
    qs = [qs, scores_and_values(chosen_score,2)'];
    %>*
    %<* Calcul des scores ex ante
    % Nombre de points qui d�passent, pct quotidien, score, signed_score
    nb_upper  = sum( vals>=qs(4) );
    pct_upper = nb_upper / nb_days;
    sc_upper  = normcdf((pct_upper - p_alpha) / sqrt( p_alpha*(1-p_alpha)/nb_days));
    sn_upper  = 2*(sc_upper-.5);
    qs = [ qs, nb_upper, pct_upper, sc_upper, sn_upper];
    %>*
    %<* stockage des r�sultats
    t_colname = tokenize( colname, ';');
    ql = st_data('init', 'title', 'Thresholds', ...
        'colnames', { 'Observed max', 'Empirical quantile' , 'Log-normal based quantile', ...
        'Max based quantile', 'Max based optsort1', 'Max based opt2', ...
        'Nb upper points', 'Daily upper points', 'Risk score', 'Signed score' }, ...
        'date', NaN, 'value', qs, 'rownames', t_colname(1), 'info', inputs{1}.info );
    this.memory.set('quantile-level', ql);
    
    this.memory.set('ATS', ATS);
    this.memory.set('Capital', Capital);
    %>*
    

    if strcmpi( this.params.get('method'), 'boots')
        %<* Bootstrap
        % Optionnel pour valider les calculs de moyenne et �cart type.
        % Attention: probl�me avec la fonction bootstrap.
        val1  = bootstrp(100,@estimationRobuste, 'gaussien', vals, 'quantile-level', .25, 'troncate-level', 1.96);
        Boots = st_data('init','title','BootstrapData','colnames',{'Moyenne','Variance'},'date',(1:100)','value', val1);
        this.memory.set('Bootstrap', Boots);
        this.memory.set('taille', length(vals));
        %>*
    end     
    
    
end

%%** Dedicated Plot
function h = plot_( inputs, varargin)
    read_dataset( 'plot-tickdb', inputs{3});
    h = gcf;
end

%%** End of production
function b = eop
    b = true;
end
%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end