function o = st_normalize_iday( varargin)
% ST_NORMALIZE_IDAY - normalise des grandeurs intra day
%
% See also st_node 
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  @plot_ );
%%** Init       
function init( varargin)
    this.params = options({ 'colname:char', 'volume', ... % colname to use
        'ref:fun', @(x)median(x,2), ... % function to use for agregation
        'diff:fun', @(x,y)(x-y)./y }, ... % function to use for difference
        varargin );
    this.memory = options({'n', []});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs(1));
    if b
        this.memory.set('n', []);
        return
    end
    colname  = this.params.get( 'colname:char');
    u_data   = st_data('iday2array', inputs{1}, colname);
    u_slice  = feval(this.params.get( 'ref:fun'), u_data.value);
    if length(inputs)>1
        [outputs, b] = st_data('isempty', inputs(2));
        if b
            this.memory.set('n', []);
            return
        end
        v_data   = st_data('iday2array', inputs{2}, colname);
        u_value = v_data.value; % si il y a une autre source de donn�es, c'est sur elle qu'on fait de calcul
        u_date  = v_data.date;
        u_colnames = v_data.colnames;
        titles = sprintf('%s and %s', inputs{1}.title, inputs{2}.title);
    else
        u_value = u_data.value; % sinon sur soi
        u_date  = u_data.date;
        u_colnames = u_data.colnames;
        titles = sprintf('%s and itself', inputs{1}.title);
    end
    try
        mat_vals = bsxfun( this.params.get( 'diff:fun'), u_value, u_slice);
    catch
        error('st_normalize_iday:exec', 'DATA_HETEROGENEITY: size problems between %s (%d,%d) and (%d,%d)', ...
            titles, size(u_value,1), size(u_value,2), size(u_slice,1), size(u_slice,2));
    end
    outputs{1}.value = mat_vals(:);
    dts      = bsxfun( @plus, u_date, datenum( u_colnames, 'dd/mm/yyyy')');
    outputs{1}.date  = dts(:);
    outputs{1}.colnames = { sprintf('Normalized %s', colname) };
    this.memory.set('n', u_data);
end

%%** plot
function h = plot_( inputs, varargin)
    data = this.memory.get( 'n');
    if isempty( data)
        st_log('st_normalize_iday:plot nothing to plot\n');
    else
        histocurves( 'init', data);
        attach_date('init', 'date', data.date([1 end]), 'format', 'HH:MM');
        title( data.title);
    end
    h = gcf;
end
%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end