function o = st_regress( varargin)
% ST_REGRESS - boite de regression : le premier input doit contenir les
%               donn�es sur lesquels on fait la r�gression (apprentissage)
%               Le deuxi�me input contient les donn�es sur lesquelles on
%               fait la pr�diction. �videmment les deux inputs doivent �tre
%               identiques dans leur structure, et il serait pr�f�rable
%               que les donn�es d'apprentissage soit strictement
%               ant�rieures aux donn�es de pr�diction.
%               st_memory a �t� pr�vu pour vous assiter dans cette t�che
%
%               Remarques au sujet du mode de r�gression 'func' :
%                   Tout d'abord c'est un peu l'aventure, rien ne vous
%                   assure que ce que vous faites a la moindre chance de
%                   fonctionner. Ensuite, purement pour l'utilisation, vous
%                   devez imp�rativement renseigner les parametres
%                   'forecast:func' et 'criterion:func'.
%
%               Exemple pour le mode func, si vous utilisez les param�tres suivants : 
%                   'regress-type:char'     =           func
%                   'forecast:func'         =           @(X,p)p'*X
%                   'criterion:func'        =           @(Y,Y_hat)sum((Y-Y_hat).^2)
%                   'first_guess:double'    =           1
%               En fait, vous �tes en train de faire une r�gression lin�aire
%               sans constante, sauf que dans ce mode, l'estimation va
%               prendre plus de temps, vous n'aurez aucune statistique de
%               test ou de qualit� de l'estimation, et vous n'avez m�me pas
%               l'assurance d'obtenir une estimation.
%
% See also st_memory st_simplereglin st_dynacc_scatter

this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  @plot_);
       
    %%** Init
    function init( varargin)
        this.params = options({ 'x-names:char', 'spread', ... % name of the X variable
                                'y-name:char', 'vol', ... % name of the Y variable
                                'regress-type:char', 'linear', ... % regression types : linear/median/robust/tobit/func (func means that you want a function to be minimized, then you will have to give the functionnal formula for forecast ('forecast:func'), as well as the criterion to minimize ('criterion:func'))
                                'add-const:b', 1, ... % Add a constant to the regression? Useless if regress_type = func
                                'bootstrap:b', 0, ... % Should the regression be bootstrapped? Works only if regress-type = linear
                                'plot:b', 1 ... % Do you want a plot to be drawn each time you press next?
                                'nb_quantiles', 20, ... % The if X and Y are scalars, then mean of Y over quantiles of X will be plotted, how many quantiles do u want?
                                'pre_transformation:func', @(x)x, ... % Transformation on Y and X before doing anything
                                'final_transformation:func', @(x)x, ... % Transformation on Y and Y_forecast before plotting it
                                'forecast:func', @(X,p)p'*X, ... % Useless unless you chose func for the param 'regress-type:char'. But if you did, that will be the function for forecasting. First argument(matrix) is X, second argument is parameters
                                'criterion:func', @(Y,Y_hat,X)sum((Y-Y_hat).^2), ... % Useless unless you chose func for the param 'regress-type:char'. But if you did, that will be the criterion to minimize, first argument : forecasts, second argument values to be forecasted, third argument X
                                'first_guess:double', [], ... % idem. These values should be first guess of parameters. It will be used to initialize optimization
                                }, varargin );
        this.memory = options({ 'coeffs', {}, 'stats', {},...
                                'residuals_out', [], 'Y_hat', [], ...
                                'model', {}, 'last_X_and_Y_in', []});
    end

    %%** Exec
    function outputs = exec( inputs, varargin)
        [outputs, b] = st_data('isempty', inputs);
        if b
            return;
        end
        outputs = cell(floor(length(inputs)/2), 1);
        for i = 1 : floor(length(inputs)/2)
            if ~isempty(inputs{2*i-1})
                % Informations sur le mod�le utilis� pour cette estimation
                model = options(this.params.get()); % Est il n�cessaire de recr�er une structure options pour faire une recopie?

                % Estimation
                [X_in, Y_in, dts] = GET_X_Y_dts(inputs{2*i-1}, model);
                [bls, stats] = REGRESS_(Y_in, X_in, model);
                ADD2MEM_CELL('stats',  stats, i);
                ADD2MEM_CELL('coeffs',  bls, i);
                this.memory.set('last_X_and_Y_in', [X_in, Y_in]);

                % Prediction
                [X, Y, dts] = GET_X_Y_dts(inputs{2*i}, model);
                Y_hat = FORECAST_(X, bls, model);
                ADD2MEM_STDATA('Y_hat', {'value', Y_hat, 'date', dts, 'colnames', {'forecast'}, 'title', 'forecast'}, i);
                ADD2MEM_STDATA('residuals_out', {'value', Y - Y_hat, 'date', dts, 'colnames', {'residuals'}, 'title', 'residuals'}, i);

                % Outputs
                outputs{i} = st_horzcat(inputs{2*i}, st_data('init', 'value', Y_hat, 'date', dts, 'colnames', {'forecast'}, 'title', 'forecast'));

                % Reporting
                % TODO ???

                % On stocke les informations sur le mod�le qui a �t� utilis�
                % pour cette estimation
                ADD2MEM_CELL('model',  model, i);
            end
        end
        % Plots
        if this.params.get('plot:b') && ~all(cellfun(@(o)isempty(o)||isempty(o.value), outputs, 'uni', true))
            opt = options(varargin);
            plot_focus( opt.get('title'));
            plot_( outputs);
        end
    end

    %%** End of production
    function b = eop
        b = true;
    end

    %%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

    function h = plot_( inputs, varargin)
        nb_subplots_vert = 2 * length(inputs);
        nb_subplots_horz = 1;
        plots_drawn = 0;
        for i = 1 : length(inputs)
            input = inputs{i};
            
            if length(tokenize(this.params.get( 'x-names:char'))) == 1
                nb_subplots_horz = 3 * length(inputs);
                plots_drawn = plots_drawn + 1;
                subplot(nb_subplots_vert, nb_subplots_horz, plots_drawn);
                if ~isempty(input.date)
                    % Nuage de points
                    vals = st_data('cols', input, [this.params.get( 'x-names:char') ';' this.params.get( 'y-name:char') ';' 'forecast']);
                    plot(vals(:, 1), vals(:, 2), '+', vals(:, 1), vals(:, 3), 'r');
                else
                    cla;
                end
                
                plots_drawn = plots_drawn + 1;
                subplot(nb_subplots_vert, nb_subplots_horz, plots_drawn);
                if ~isempty(input.date)
                    % Boxplots de Y sur des quantiles de X
                    nb_q = this.params.get('nb_quantiles');
                    quantiles_of_X  = [quantile(vals(:, 1), 1/nb_q:1/nb_q:1-1/nb_q), Inf];
                    subsamples_of_Y = cell(nb_q, 1);
                    mean_X_in_q = zeros(nb_q, 1);
                    nb_X_in_q = zeros(nb_q, 1);
                    for j = 1 : size(vals, 1)
                        k = find(repmat(vals(j, 1), 1, nb_q) < quantiles_of_X, 1);
                        subsamples_of_Y{k}(end + 1) = vals(j, 2);
                        mean_X_in_q(k) = mean_X_in_q(k) + vals(j, 1);
                        nb_X_in_q(k) = 1 + nb_X_in_q(k);
                    end
                    mean_X_in_q = mean_X_in_q ./ nb_X_in_q;
                    % Not very nice but unavoidable if one wants to use
                    % matlab's boxplots
                    min_size = min(nb_X_in_q);
                    ss_of_Y = zeros(min_size, nb_q);
                    for j = 1 : nb_q
                        ss_of_Y(:, j) = subsamples_of_Y{j}(1:min_size);
                    end
                    % boxplot
                    slope     = (nb_q - 1) / (mean_X_in_q(nb_q) - mean_X_in_q(1));
                    intercept = 1 - slope * mean_X_in_q(1);
                    boxplot(ss_of_Y, 'labels', arrayfun(@(x)num2str(x, '%2.1f'), mean_X_in_q, 'uni', false), ...
                                        'positions', slope * mean_X_in_q + intercept);
                else
                    cla;
                end
                
                plots_drawn = plots_drawn + 1;
                subplot(nb_subplots_vert, nb_subplots_horz, plots_drawn);
                if ~isempty(input.date)
                    v = conditionnal_expectation_kernel(vals(:, 2), vals(:, 1));
                    vals_in = this.memory.get('last_X_and_Y_in');
                    v_in = conditionnal_expectation_kernel(vals_in(:, 2), vals_in(:, 1));
                    plot(v(:, 1), v(:, 2), 'g', v_in(:, 1), v_in(:, 2), 'r');
                else
                    cla;
                end
            end

            % Y et sa pr�diction
            plots_drawn = plots_drawn + 1;
            subplot(nb_subplots_vert, nb_subplots_horz, plots_drawn); 
            if ~isempty(input.date)
                vals = feval(this.params.get('final_transformation:func'), ...
                st_data('cols', input, [this.params.get( 'y-name:char') ';' 'forecast']));
                plot(input.date, vals(:, 1), '-+',input.date, vals(:, 2), '-+'); 
            else
                cla;
            end
            legend({this.params.get( 'y-name:char');'forecast'})

            % Distribution des residus dans\hors �chantillon d'apprentissage
            plots_drawn = plots_drawn + 1;
            subplot(nb_subplots_vert, nb_subplots_horz, plots_drawn);
            r_in = this.memory.get('stats');
            r_in = r_in{end, i}.residuals;
            r_out = vals(:, 1) - vals(:, 2);
            if ~isempty(r_in) && ~isempty(input.date)
                [f_in,x_in]=ksdensity(r_in);
                [f_out,x_out]=ksdensity(r_out);

                plot(x_in,f_in,'r',x_out,f_out,'b');legend({'residuals in sample'; 'residuals out of sample'});
            else
                cla;
            end
        end
    end


    % petites fonctions pour empiler les infos dans la m�moire
    function ADD2MEM_CELL(field, data2add, i)
        f = this.memory.get(field);
        if isempty(f) || length(f) < i
            f(1, i) = {[]};
        elseif ~isempty(f{end, i})
            f(end + 1, :) = cell(1, length(f(end, :)));
        end
        f{end, i} = data2add;
        this.memory.set(field, f);
    end

    function ADD2MEM_STDATA(field, fields_list, i)
        field_value = this.memory.get(field);
        if ~isempty(field_value) && length(field_value) >= i
            field_value{i} = st_data('stack', field_value{i}, ...
                            st_data('init', fields_list{:}));
        else
            field_value{i} = st_data('init', fields_list{:});
        end
        this.memory.set(field, field_value);
    end
end

function [X, Y, dts] = GET_X_Y_dts(input, model)
    f = model.get('pre_transformation:func');
    X = feval(f, st_data('col', input, model.get( 'x-names:char')));
    Y = feval(f, st_data('col', input, model.get( 'y-name:char')));
    dts = input.date;
end

function [X_all, const_str] = ADD_CONST(X, model)
    % Add a constant to the model?
    const_str = 'on';
    if model.get('add-const:b')
        X_all = [ones(size(X,1),1) X];
    else
        X_all = X;
        const_str = 'off';
    end
end

function [bls, stats] = REGRESS_(Y, X, model)
    [X_all, const_str] = ADD_CONST(X, model);
    % Which regression to use?
    switch(lower(model.get('regress-type:char')))
        case 'linear'
            stats = simplereglin( Y, X, 'bootstrap', model.get( 'bootstrap'), ...
                'const', model.get('add-const:b'), 'hat', true, 'residuals', true);
            bls = stats.a;
        case 'median'
            bls = rq_fnm(X_all,Y,0.5);
            stats.residuals = Y - X_all*bls;
        case 'robust'
            % TODO if one uses it, add wfun and tune in the parameters
            % (below it is empty)
            bls = robustfit(X,Y, [], [], const_str);
            stats.residuals = Y - X_all*bls;
        case 'tobit'
            info.maxit=10000;  
            info.limit=0;
            stats = tobit(Y,X_all,info); 
            bls = stats.beta;
            stats.residuals = stats.resid;
            stats = rmfield(stats, 'resid');
        case 'func'
            f = model.get('forecast:func');
            first_guess = model.get('first_guess:double');
            if isempty(first_guess)
                error('Error in st_regress, in such a mode, you have to give a first guess for parameters. Please fill the parameter ''first_guess:double''');
            end
            [N, P] = size(first_guess);
            if N == 1 && P > 1
                first_guess = first_guess';
            elseif N > 1 && P > 1
                error('Error in st_regress, the first guess should be a vector. Please correct the parameter ''first_guess:double''');
            end
            g = model.get('criterion:func');
            bls = fminunc(@(p)g(Y, f(X, p), X),first_guess);
            stats.residuals = Y - f(X, bls);
    end
end

function Y_hat = FORECAST_(X, bls, model)
    X_all = ADD_CONST(X, model);
    % Which regression to use?
    switch(lower(model.get('regress-type:char')))
        case {'linear', 'median', 'robust'}
            Y_hat = X_all*bls;
        case 'tobit' % ??? TODO check this !!!
            Y_hat = max(X_all*bls, 0);
        case 'func'
            f = model.get('forecast:func');
            Y_hat = f(X, bls);
    end
end