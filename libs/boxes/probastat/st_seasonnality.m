function o = st_seasonnality( varargin)
% ST_SEASONNALITY - module qui fait des saisonnalit? sur  du volume
% uniquement pour l'instant
%
% sert pour le graph indicator seasonnality
%
% opt = {'seas_func', @nanmedian, ...
%     'step:time', datenum(0,0,0,0,5,0),...
%     'window_spec:char', sprintf('%s|%d', 'day', 180),...
%     'mode_season', 'jmc'};
%
% data = read_dataset(['gi:seasonnality/' opt2str(options(opt))], ...
%     'security_id', 110,...
%     'from', '01/01/2009',...
%     'to', '01/01/2009', ...
%     'trading-destinations',[]);
%
%
% author   : 'rburgot@cheuvreux.com'
% reviewer : ''
% version  : '1.1'
% date     : '27/08/2008'
%
% See also st_node gi_create from_buffer

this.params = {};
this.memory = {};

o = struct('init', @init, ...
    'exec', @exec, ...
    'learn', [], ...
    'get', @get_, ...
    'set', @set_, ...
    'parallel', true, ...
    'end_of_prod', @eop, ...
    'plot',  [] );

clear varargin;

%%** Init
    function init( varargin)
        this.params = options({...
            'mode_season', 'std', ...
            'seas_func', @(x)nanmedian(x),...
            'norm_fun', @(x,varargin)bsxfun(@rdivide,x,sum(x))
            }, varargin);
        this.memory = {};
    end

%%** Exec
    function outputs = exec( inputs, varargin)
        % < MAGIC NUMBERS
        % cas des courbes valeurs
        MINIMAL_NB_OBS_4_STOCKS = 60; % le contexte principal n'est calcul? que si'lon dispose d'au moins cette quantit? de donn?es
        ADD_NB_OBS_4_DATA_FILTERING = 20; % les donn?es ne sont filtr?es pas ACP que si'lon dispose d'au moins MINIMAL_NB_OBS_4_STOCKS + ADD_NB_OBS_4_DATA_FILTERING jours de donn?es
        MINIMAL_NEW_HOURS_TRADING_DAYS = 20; % si les horaires de quotation ont chang? depuis au moins ce nombre de jours, alors on calcule une courbe de volume d?grad?e en utilisant les anciennes et les nouvelles donn?es
        % cas des courbes indice ou groupe de cotation
        MINIMAL_NB_OBS_4_INDEX = 400;
        % tol?rance en minutes pour un d?calage d'horaire de cotation
        TH_CHANGE_TOL =  datenum(0,0,0,0,5,1);
        % Param?tres de s lois betas
        MIN_BETA = 1;
        MIN_ALPHA = 3;
        % >
        % < Gestion du nommage (aussi utilis? pour extraction des valeurs des indicatrices) des fixings
        FIXING_COLN = {'opening_auction' , 'intraday_auction', 'closing_auction'};
        % >
        
        % < Patch pour les donn?es erron?es sur les US sur le mois de
        % Novembre (Rappel : volume de fixing ? z?ro, consid?r?s comme du
        % volume continu)
        if isstruct(inputs{1})
            if inputs{1}.info.td_info(1).place_id == 3
                % on va supprimer les donn?es du mois de novembre 2012
                idxdata2del = inputs{1}.date >= datenum(2012,10,31) & inputs{1}.date <= datenum(2012,11,31);
                inputs{1} = st_data('from-idx', inputs{1}, ~idxdata2del);
            end
        else
            if inputs{1}{1}.info.td_info(1).place_id == 3
                for i = 1 : length(inputs{1})
                    % on va supprimer les donn?es du mois de novembre 2012
                    idxdata2del = inputs{1}{i}.date >= datenum(2012,10,31) & inputs{1}{i}.date <= datenum(2012,11,31);
                    inputs{1}{i} = st_data('from-idx', inputs{1}{i}, ~idxdata2del);
                end
            end
        end
        % >
        
        mode = this.params.get('mode_season');
        colnames = 'Usual day';
        alpha = false;
        switch mode
            case 'std'
                if isstruct(inputs{1})
                    [outputs, b] = st_data('isempty', inputs);
                    if b
                        return
                    end
                    input_copy = inputs{1};
                    [data, outputs{1}, inputs{1}, has_changed] = safer_bin_separator(outputs{1}, inputs{1}, 'volume');
                    seas_func = this.params.get('seas_func');
                    % On passe de volumes aux proportions
                    m = length(data.colnames);
                    data.value = bsxfun(@rdivide, data.value, sum(data.value, 2));
                    fixing_booleans = logical(st_data('cols',inputs{1},FIXING_COLN,1:m));
                    idx2del = any(data.value < 0, 2);
                    if any(idx2del)
                        data.value(idx2del, :) = [];
                        data.date(idx2del, :) = [];
                    end

                    if length(data.date) >= MINIMAL_NB_OBS_4_STOCKS + ADD_NB_OBS_4_DATA_FILTERING
                        % On extrait les noisy days
                        pca_engine = pca('nb_simul', 200 ,...
                            'transformation', @(x)log(1 + x),...TODO something better than + 1?
                            'mode_nb_factors', 'bootstrap', ...
                            'normalized', false);
                        pca_engine.estimate(data.value(:, 1:end-1));
                        scores = pca_engine.compute_score(data.value(:, 1:end-1));
                        nd = scores <= 0.1; % MAGIC NUMBER
                        if length(data.date) - sum(nd) < MINIMAL_NB_OBS_4_STOCKS
                            s_scores = sort(scores);
                            nd = scores <= s_scores(end-MINIMAL_NB_OBS_4_STOCKS);
                        end
                        %< d?sormais les noisy day sont ceux o? nd est vrai
                        vals = seas_func(data.value(~nd, :))';
                        %>
                        
                        %                         % < juste un test
                        %                         data.value(:, 1) = [];
                        %                         data.value(:, end) = [];
                        %                         a = [];
                        %                         b = [];
                        %                         m = [];
                        %                         alpha = [];
                        %                         for i = 1 : size(data.value, 2)-1
                        %                             tmp = betafit(sum(data.value(:, 1:i), 2)./sum(data.value, 2));
                        %                             a(i) = tmp(1);
                        %                             b(i) = tmp(2);
                        %                         end
                        %                         alpha = a+b;
                        %                         m = a./(a+b);
                        %                         plot(m.*(1-m), 1./(alpha+1), '+');
                        %                         plot(log(m.*(1-m)), log(1./(alpha+1)), '+');
                        % >
                    elseif length(data.date) >= MINIMAL_NB_OBS_4_STOCKS % MAGIC NUMBER
                        vals = seas_func(data.value)';
                        nd = false(size(data.date, 1), 1);
                    else
                        colnames = 'Usual day'; % Pour m?moire, avant il y avait ?crit 'Trading hours changed' ici afin de changer de contexte...
                        if has_changed
                            if length(data.date) >= MINIMAL_NEW_HOURS_TRADING_DAYS
                                idx_new_trading_hours = input_copy.date >= floor(data.date(1));
                                input_copy.value(idx_new_trading_hours, :) = [];
                                input_copy.date(idx_new_trading_hours, :) = [];
                                [old_data, input_copy, input_copy, has_changed] = safer_bin_separator(input_copy, input_copy, 'volume');
                                old_fixing_booleans = logical(st_data('cols', st_data('from-idx', input_copy, 1:size(old_data.value, 2)), FIXING_COLN));
                                if length(old_data.date) < MINIMAL_NB_OBS_4_STOCKS - MINIMAL_NEW_HOURS_TRADING_DAYS
                                    if has_changed
                                        error('st_seasonnality:exec', 'NOT_ENOUGH_DATA: Multiple change in trading hours change results in not enough data');
                                    else
                                        error('st_seasonnality:exec', 'NOT_ENOUGH_DATA: Trading hours change implies not enough data for this security to work on it');
                                    end
                                end
                                old_data.value = bsxfun(@rdivide, old_data.value, sum(old_data.value, 2));
                                [additionnal_bins, idx_add] = setdiff(data.colnames(~any(fixing_booleans(:, 1:3), 2)), old_data.colnames(~any(old_fixing_booleans(:, 1:3), 2)));
                                [removed_bins, idx_removed] = setdiff(old_data.colnames(~any(old_fixing_booleans(:, 1:3), 2)), data.colnames(~any(fixing_booleans(:, 1:3), 2)));
                                if (~isempty(additionnal_bins) && ~isempty(removed_bins)) || (isempty(additionnal_bins) && isempty(removed_bins))
                                    % Deux cas de figure :
                                    % - soit aucun intervalle du continu n'a
                                    % ete modifie dans ce cas on va juste
                                    % verifier que ce n'est pas
                                    % un ajout ou un retrait de fixing
                                    % - soit des intervalles ont ete
                                    % ajoutes et d'autres retires, auquel
                                    % cas on va assimiler ceux qui ont moins
                                    % de TH_CHANGE_TOL d'ecart
                                    if length(idx_add) == length(idx_removed)
                                        for i = 1 : length(idx_add)
                                            if abs(datenum(data.colnames(additionnal_bins{i}), 'HH:MM:SS:FFF')-datenum(old_data.colnames(removed_bins{i}), 'HH:MM:SS:FFF')) >= TH_CHANGE_TOL
                                                error('st_seasonnality:exec', 'INAPPROPRIATE_METHOD: Trading hours changed, same number of bins but not the same hours, dont know what to do please help me');
                                            end
                                        end
                                        for i = 1 : length(FIXING_COLN)
                                            if (any(fixing_booleans(:, i)) ~= any(old_fixing_booleans(:, i)))
                                                error('st_seasonnality:exec', sprintf('INAPPROPRIATE_METHOD: The %s has been added or removed, so the whole price formation process may have been modified, please have a careful look', FIXING_COLN{i}));
                                            end
                                            % inutile de verifier que les
                                            % horaires de fixing ont ete
                                            % modifies car si c'est le cas,
                                            % safer_bin_separator les a
                                            % corriges et donc a ce niveau
                                            % on en est pas conscient
                                            % normalement
                                            if any(fixing_booleans(:, i)) && abs(datenum(data.colnames(fixing_booleans(:, i)), 'HH:MM:SS:FFF')-datenum(old_data.colnames(old_fixing_booleans(:, i)), 'HH:MM:SS:FFF')) >= TH_CHANGE_TOL
                                                sec_datenum = datenum(0,0,0,0,0,1);
                                                time_diff_sec = round(abs(datenum(data.colnames(fixing_booleans(:, i)), 'HH:MM:SS:FFF')-datenum(old_data.colnames(old_fixing_booleans(:, i)), 'HH:MM:SS:FFF'))/ sec_datenum);
                                                error('st_seasonnality:exec', 'UNEXPECTED: It was assumed this error was unreachable because of safer_bin_separator.m');
                                            end
                                        end
                                        % Le seul cas de figure traite ici
                                        % devrait etre celui ou il y a
                                        % moins de 5 minutes d'ecart entre
                                        % les anciens intervalles et les
                                        % nouveaux (qu'ils soient fixing ou non)
                                        data.value = [old_data.value;data.value];
                                        data.date = [old_data.date;data.date];
                                        vals = seas_func(data.value)';
                                    else
                                        error('st_seasonnality:exec', 'INAPPROPRIATE_METHOD: Trading hours changed, some bins have been added, some others removed, I don''t know what to do.');
                                    end
                                elseif ~isempty(additionnal_bins)
                                    % Des intervalles du continu ont ete ajoutes
                                    [inter_bins, idx_inter] = intersect(old_data.colnames(~any(old_fixing_booleans(:, 1:3), 2)), data.colnames(~any(fixing_booleans(:, 1:3), 2)));
                                    if any(diff(idx_inter)>1) % les intervalles 
                                        error('st_seasonnality:exec', 'INAPPROPRIATE_METHOD: Trading hours changed and I just dont understand the difference');
                                    end
                                    if sum(diff(idx_add)>1) > 1
                                        error('st_seasonnality:exec', 'INAPPROPRIATE_METHOD: Trading hours changed and don''t know how to handle it');
                                    end
                                    has_bins_before = idx_add(1) == 1;
                                    has_bins_after = idx_add(end) == sum(~any(fixing_booleans(:, 1:3), 2));
                                    if has_bins_after && has_bins_before
                                        if sum(diff(idx_add)>1) == 0 || sum(diff(idx_add)>1) > 1
                                            error('st_seasonnality:exec', 'INAPPROPRIATE_METHOD: Trading hours changed and don''t know how to handle it');
                                        end
                                        idxsep = find(diff(idx_add)>1);
                                        idx_bins_before = any(fixing_booleans(:, 1)) + idx_add;
                                        idx_bins_before = idx_bins_before(1:idxsep);
                                        idx_bins_after  = any(fixing_booleans(:, 1)) + idx_add;
                                        idx_bins_after = idx_bins_after(idxsep+1:end);
                                    elseif has_bins_after
                                        if sum(diff(idx_add)>1) > 0
                                            error('st_seasonnality:exec', 'INAPPROPRIATE_METHOD: Trading hours changed and don''t know how to handle it');
                                        end
                                        idx_bins_after = any(fixing_booleans(:, 1)) + idx_add;
                                        idx_bins_before = [];
                                    elseif has_bins_before
                                        if sum(diff(idx_add)>1) > 0
                                            error('st_seasonnality:exec', 'INAPPROPRIATE_METHOD: Trading hours changed and don''t know how to handle it');
                                        end
                                        idx_bins_before  = any(fixing_booleans(:, 1)) + idx_add;
                                        idx_bins_after = [];
                                    else
                                        error('st_seasonnality:exec', 'INAPPROPRIATE_METHOD: Trading hours changed and don''t know how to handle it');
                                    end
                                    idx_continuous = ~any(old_fixing_booleans, 2);
                                    % on passe en volume cumul? sur le continu
                                    rel_volume = old_data.value(:, idx_continuous);
                                    rel_volume = rel_volume ./ repmat(sum(rel_volume, 2), 1, size(rel_volume, 2));
                                    rel_volume = cumsum(rel_volume, 2);
                                    rel_volume(:, end) = [];
                                    
                                    % ici la courbe de volume cumul?e "moyenne" (plut?t m?diane mais
                                    % bon...)
                                    c_vals = seas_func(old_data.value(:, idx_continuous));
                                    c_vals = c_vals ./ sum(c_vals);
                                    m = cumsum(c_vals);
                                    m(:, end) = [];
                                    
                                    v = mean((rel_volume-repmat(m, size(rel_volume, 1), 1)).^2);
                                    
                                    tmp = [ones(length(v), 1) log(m.*(1-m))'];
                                    tmp = (tmp'*tmp)^-1*tmp'*log(v)';
                                    
                                    beta = tmp(2);
                                    if ~isfinite(beta)
                                        tmp = (v./(m.*(1-m)));
                                        alpha = median(1./tmp) - 1;
                                        beta = 1;
                                    else
                                        
                                        alpha = 0.25^(1-beta)/exp(tmp(1))-1;
                                    end
                                    if has_bins_before
                                        prop_before = median(sum(data.value(:, idx_bins_before), 2)./sum(data.value, 2));
                                    else
                                        prop_before = 0;
                                    end
                                    if has_bins_after
                                        prop_after = median(sum(data.value(:, idx_bins_after), 2)./sum(data.value, 2));
                                    else
                                        prop_after = 0;
                                    end
                                    
                                    old_data.value = [old_data.value(:, old_fixing_booleans(:, 1)) * (1-prop_after-prop_before), ...
                                        repmat(prop_before / length(idx_bins_before), size(old_data.value, 1), length(idx_bins_before)), ...
                                        old_data.value(:, idx_inter) * (1-prop_after-prop_before), ...
                                        repmat(prop_after/ length(idx_bins_after), size(old_data.value, 1), length(idx_bins_after)), ...
                                        old_data.value(:, old_fixing_booleans(:, 3)) * (1-prop_after-prop_before) ];
                                    
                                    data.value = [old_data.value;data.value];
                                    vals = seas_func(data.value)';
                                elseif ~isempty(removed_bins)
                                    old_data.value(:, idx_removed) = [];
                                    data.value = [old_data.value;data.value];
                                    data.date = [old_data.date;data.date];
                                    vals = seas_func(data.value)';
                                else
                                    error('st_seasonnality:exec', 'INAPPROPRIATE_METHOD: Assumed unreachable code was reached');
                                end
                            else
                                error('st_seasonnality:exec', 'NOT_ENOUGH_DATA: Trading hours change results in Not enough data')
                            end
                        else
                            error('st_seasonnality:exec', 'NOT_ENOUGH_DATA: Not enough data for this security to work on it');
                        end
                        nd = false(size(data.value, 1), 1);
                    end
                    % renormalisation pour avoir une somme qui fait 1
                    norm_fun = this.params.get('norm_fun');
                    vals = norm_fun(vals,fixing_booleans);
                    %< mise en forme des donn?es
                    outputs = {st_data('init', 'title', data.title, 'info', rmfield(data.info,'data_log'), ...
                        'date', mod(datenum(data.colnames, 'HH:MM:SS:FFF'), 1), ...
                        'colnames', cat(2, colnames, FIXING_COLN), ...
                        'value', cat(2, vals, fixing_booleans), ...
                        'attach_format', 'HH:MM')};
                    %>
                    
                    % mise en forme des donn?es pour l'intervalle de confiance
                    whole_prop = data.value(~nd, :);
                else
                    whole_prop = [];
                    whole_money = [];
                    has_changed = false;
                    volume = cell(length(inputs{1}), 1);
                    for i = 1 : length(inputs{1})
                        [volume{i}, inputs{1}{i}, inputs{1}{i}, tmp_has_changed] = safer_bin_separator(inputs{1}{i}, inputs{1}{i}, 'volume');
                        has_changed = has_changed || tmp_has_changed;
                    end
                    for i = 1 : length(inputs{1})
                        if ~isempty(whole_prop) && size(whole_prop,2)~=size(volume{i}.value,2)
                            error('st_seasonnality:std', 'DATA_HETEROGENEITY: impossible to stack the whole perimeter of <%s>', volume{i}.title);
                        end
                        if ~st_data('isempty-nl', inputs{1}{i})
                            vwap = st_data('keep', inputs{1}{i}, 'vwap');
                            vwap = bin_separator(vwap);
                            whole_prop = [whole_prop; volume{i}.value./repmat(sum(volume{i}.value, 2), 1, size(volume{i}.value, 2))];
                            whole_money = [whole_money; nansum(vwap.value.*volume{i}.value, 2)];
                        end
                    end
                    if size(whole_prop, 1) < MINIMAL_NB_OBS_4_INDEX
                        if has_changed
                            error('st_seasonnality:exec', 'NOT_ENOUGH_DATA: Trading hours change results in Not enough data');
                        end
                        error('st_seasonnality:exec', 'NOT_ENOUGH_DATA: Not enough data for this index to work on it');
                    end
                    seas_func =  this.params.get('seas_func');
                    if strcmp(func2str(seas_func), 'nanmedian')
                        vals = weighted_median(whole_prop, whole_money);
                    else
                        vals = seas_func(whole_prop);
                    end
                    fixing_booleans  = st_data('cols', st_data('from-idx', inputs{1}{1}, 1:size(vals, 2)), FIXING_COLN);
                    norm_fun = this.params.get('norm_fun');
                    vals = norm_fun(vals',fixing_booleans);
                    %< mise en forme des donn?es
                    outputs = {st_data('init', 'title', 'Courbe indice', ...
                        'date', inputs{1}{1}.date(1:size(vals, 1)), ...
                        'colnames', [{'Usual day'}, FIXING_COLN], ...
                        'value', cat(2, vals, fixing_booleans), ...
                        'attach_format', 'HH:MM', 'info', inputs{1}{1}.info)};
                end
                
                % TODO : faire mieux en cas de courbe indice => la variance
                % des volumes relatifs est tr?s corr?l?e au nombre de
                % transaction moyen
                
                % Maintenant que l'on a une courbe de volume on s'attaque aux
                % intervalles de confiance
                
                % on retire les auctions
                idx_continuous = ~any(st_data('cols', outputs{1}, FIXING_COLN), 2);
                
                % on passe en volume cumul? sur le continu
                rel_volume = whole_prop(:, idx_continuous);
                rel_volume = rel_volume ./ repmat(sum(rel_volume, 2), 1, size(rel_volume, 2));
                rel_volume = cumsum(rel_volume, 2);
                rel_volume(:, end) = [];
                
                % ici la courbe de volume cumul?e "moyenne" (plut?t m?diane mais
                % bon...)
                c_vals = vals(idx_continuous)';
                if any(c_vals == 0) || any(~isfinite(c_vals))
                    if iscell(inputs{1})
                        last_date = floor(inputs{1}{1}.date(end));
                        [~, ~, trdt] = trading_time_interpret(inputs{1}{1}.info.td_info, last_date);
                    else
                        last_date = floor(inputs{1}.date(end));
                        [~, ~, trdt] = trading_time_interpret(inputs{1}.info.td_info,last_date);
                    end
                    trdt = trdt.null_context_dump;
                    if length(trdt.end_date)>1
                        assert(all(diff(trdt.end_date)>0));
                        idx2pick = find(last_date<trdt.end_date, 1, 'first');
                        fns = fieldnames(trdt);
                        for kk = 1 : length(fns)
                            trdt.(fns{kk}) = trdt.(fns{kk})(idx2pick);
                        end
                    end
                    
                    end_morning_continuous = nanmin([trdt.intraday_stop_auction, trdt.intraday_stop, trdt.intraday_resumption_auction]);
                    start_morning_continuous = trdt.opening;
                    if isfinite(end_morning_continuous) && isfinite(trdt.intraday_resumption)...
                            && end_morning_continuous <= min(mod(outputs{1}.date(c_vals == 0), 1)) && ...
                            max(mod(outputs{1}.date(c_vals == 0), 1)) <= trdt.intraday_resumption
                        
                        st_log('st_seasonnality:exec', 'La pause d?jeuner est tol?r?e, mais je ne garantie pas le r?sultat...');
                        
                    elseif isfinite(start_morning_continuous) && isfield(outputs{1},'info') && ~isempty(outputs{1}.info.td_info) && ...
                            any(ismember(outputs{1}.info.td_info(1).trading_destination_id,[26 31])) && ...
                            c_vals(1)==0 && all(c_vals(2:end))
                        st_log('st_seasonnality:exec', 'La premier intervalle nul est tol?r? sur l australie et la thailande, mais je ne garantie pas le r?sultat... ');
                    else
                        if ~isempty(inputs{1}) && iscell(inputs{1}) && isfield(inputs{1}{1},'info') && isfield(inputs{1}{1}.info,'td_info') && ...
                                isfield(inputs{1}{1}.info.td_info,'short_name') && strcmp(inputs{1}{1}.info.td_info(1).short_name,'ASE')
                           st_log('st_seasonnality:exec','Greek stocks patch for error: There is a zero in continuous phase : may be a too illiquid stock to build a volume curve, or wrong trading hours, or intraday auction');
                        else
                           error('st_seasonnality:checking_data', 'INAPPROPRIATE_METHOD/REPOSITORY: There is a zero in continuous phase : may be a too illiquid stock to build a volume curve, or wrong trading hours, or intraday auction');
                        end
                    end
                end
                c_vals = c_vals ./ sum(c_vals);
                m = cumsum(c_vals);
                m(:, end) = [];
                
                if ~alpha
                    v = mean((rel_volume-repmat(m, size(rel_volume, 1), 1)).^2);
                    
                    tmp = [ones(length(v), 1) log(m.*(1-m))'];
                    tmp = (tmp'*tmp)^-1*tmp'*log(v)';
                    
                    beta = tmp(2);
                    
                    alpha = 0.25^(1-beta)/exp(tmp(1))-1;
                    
                    if beta <= MIN_BETA || ~isfinite(beta)
                        tmp = (v./(m.*(1-m)));
                        alpha = median(1./tmp) - 1;
                        beta = 1;
                    end
                    if alpha <= MIN_ALPHA
                        beta = MIN_BETA;
                    end
                end
                
                
                
                %
                explicit_q = 0.1*(1:9);
                % Petit calcul de qualit?. TODO mettre ?a en m?moire pour permettre
                % un plot
                quant = NaN(length(m), length(explicit_q));
                for j = 1 : 9
                    %                     quant(:, j) = betainv(explicit_q(j), alpha.*m, alpha.*(1-m));
                    quant(:, j) = betainv(explicit_q(j), m.*((alpha+1).*(m.*(1-m)/0.25).^(1-beta)-1), (1-m).*((alpha+1).*(m.*(1-m)/0.25).^(1-beta)-1));
                    outputs{1}.info.ci.dist(:, j) = quant(:, j) - m';
                    outputs{1}.info.ci.m_dist(:, j) = mean(abs(outputs{1}.info.ci.dist(:, j)));
                    outputs{1}.info.ci.max_dist(:, j) = max(abs(outputs{1}.info.ci.dist(:, j)));
                end
                quant(1, :) = 0;
                quant(end, :) = 1;
                for j = 1:length(explicit_q)
                    success(:, j) = mean(rel_volume'<repmat(quant(:, j), 1, size(rel_volume, 1)), 2);
                end
                nb_val2exclude_from_check = 0;
                %         check_val = mean(mean(abs(success(nb_val2exclude_from_check+1:end-nb_val2exclude_from_check, :)-repmat(0.1*(1:9), size(success, 1)-2*nb_val2exclude_from_check, 1))));
                check_val = max(0, 1-10*max(mean(abs(success(nb_val2exclude_from_check+1:end-nb_val2exclude_from_check, :)-repmat(explicit_q, size(success, 1)-2*nb_val2exclude_from_check, 1)))));
                
                
                % intervalles diff?renci?s
                %                 rel_volume = whole_prop(:, idx_continuous);
                %                 rel_volume = rel_volume ./ repmat(sum(rel_volume, 2), 1, size(rel_volume, 2));
                %                 m = diff([0 m 1]);
                %                 quant = NaN(length(m), length(explicit_q));
                %                 for j = 1 : 9
                %                     quant(:, j) = betainv(explicit_q(j), m.*((alpha+1).*(m.*(1-m)/0.25).^(1-beta)-1), (1-m).*((alpha+1).*(m.*(1-m)/0.25).^(1-beta)-1));
                %                 end
                %                 success = [];
                %                 for j = 1:length(explicit_q)
                %                     success(:, j) = mean(rel_volume'<repmat(quant(:, j), 1, size(rel_volume, 1)), 2);
                %                 end
                %                 nb_val2exclude_from_check = 0;
                %                 %         check_val = mean(mean(abs(success(nb_val2exclude_from_check+1:end-nb_val2exclude_from_check, :)-repmat(0.1*(1:9), size(success, 1)-2*nb_val2exclude_from_check, 1))));
                %                 check_val = max(0, 1-10*max(mean(abs(success(nb_val2exclude_from_check+1:end-nb_val2exclude_from_check, :)-repmat(explicit_q, size(success, 1)-2*nb_val2exclude_from_check, 1)))));
                
                
                
                
                outputs{1}.info.ci.alpha = max(alpha, MIN_ALPHA);
                outputs{1}.info.ci.beta  = beta;
                outputs{1}.info.ci.run_quality = check_val;
                
                
%                 % < generation des contextes relatifs aux semaines ou le
%                 % decalage horaire avec NY est different
%                 % le 5 mars 2009 (datenum(2009,3,5,8,30,0)) est une date
%                 % parmi d'autres durant laquelle le decalage horaire avec NY
%                 % est "normal"
%                 % TODO : faire plus propre
%                 if ~isfield( outputs{1}.info.td_info(1), 'global_zone_name')
%                     outputs{1}.info.td_info(1).global_zone_name = 'europe';
%                 end
                if (~isfield(outputs{1}.info.td_info(1),'global_zone_name') ||...
                        isnumeric(outputs{1}.info.td_info(1).global_zone_name)) &&...
                        isfield(outputs{1}.info.td_info(1),'timezone')
                    % On conserve la cha?ne de caract?re qui pr?c?de le symbole '/' : e.g. 'Europe/paris' -> 'Europe'
                    outputs{1}.info.td_info(1).global_zone_name = ...
                        regexprep(outputs{1}.info.td_info(1).timezone,'^([^/]+)/([^/]+)$','$1');
                end
                is_field_timezeone = isfield(outputs{1}.info.td_info(1),'timezone');
                if is_field_timezeone
                    is_johanesbourg = strcmp(outputs{1}.info.td_info(1).timezone,'Africa/Johannesburg');
                    is_istanbul = strcmp(outputs{1}.info.td_info(1).timezone,'Asia/Istanbul');
                else
                    is_johanesbourg = false;
                    is_istanbul = true;
                end
                
                if strcmpi(outputs{1}.info.td_info(1).global_zone_name, 'europe') || is_johanesbourg || is_istanbul
                    modified_curves = vc_time_rescaling('std', outputs{1});
                    
                    msy = st_data('cols', modified_curves, 'March special ny');
                    msy = msy / sum(msy);
                    if any(msy< 0)
                        error('st_seasonnality:exec', 'Problem with context March special ny');
                    end
                    outputs{1} = st_data('add-col', outputs{1}, ...
                        msy, 'March special ny' ); % , 'October special ny'
                end
                if isstruct(inputs{1})
                    outputs{1}.info.run_quality = vc_check('vwap_slippage', outputs{1}, inputs{1});
                else
                    tmp_check = NaN(length(inputs{1}), length(outputs{1}.colnames) - 3);
                    for i = 1 : length(inputs{1})
                        tmp_check(i, :) = vc_check('vwap_slippage', outputs{1}, inputs{1}{i});
                    end
                    outputs{1}.info.run_quality = nanmean(tmp_check);
                end
                % >
            case 'jmc'
                [outputs, b] = st_data('isempty', inputs);
                if b
                    return
                end
                % Un seul contexte pour l'instant
                [data, outputs{1}, inputs{1}, has_changed] = safer_bin_separator(outputs{1}, inputs{1}, 'volume');
                % On passe de volumes ? proportions
                data.value = data.value ./ repmat(sum(data.value, 2), 1, length(data.colnames));
                cum_prop = cumsum(data.value, 2);
                
                m = mean(cum_prop);
                v = var(cum_prop);
                
                tmp = [ones(length(v), 1) log(m.*(1-m))'];
                tmp(end, :) = [];
                tmp = (tmp'*tmp)^-1*tmp'*(log(v(1:end-1))');
                
                a = exp(tmp(1));
                e = tmp(2);
                
                explicit_q = 0.1*(1:9);
                % Petit calcul de qualit?. TODO mettre ?a en m?moire pour permettre
                % un plot
                quant = NaN(length(m), length(explicit_q));
                for j = 1 : 9
                    quant(:, j) = betainv(explicit_q(j), (a.*(m.*(1-m)).^(e-1)).^-1.*m, (a.*(m.*(1-m)).^(e-1)).^-1.*(1-m));
                    outputs{1}.info.ci.dist(:, j) = quant(:, j) - m';
                    outputs{1}.info.ci.m_dist(:, j) = mean(abs(outputs{1}.info.ci.dist(:, j)));
                    outputs{1}.info.ci.max_dist(:, j) = max(abs(outputs{1}.info.ci.dist(:, j)));
                end
                quant(1, :) = 0;
                quant(end, :) = 1;
                for j = 1:length(explicit_q)
                    success(:, j) = mean(cum_prop'<repmat(quant(:, j), 1, size(data.value, 1)), 2);
                end % plot(success)
                nb_val2exclude_from_check = 0;
                %         check_val = mean(mean(abs(success(nb_val2exclude_from_check+1:end-nb_val2exclude_from_check, :)-repmat(0.1*(1:9), size(success, 1)-2*nb_val2exclude_from_check, 1))));
                check_val = max(0, 1-10*max(mean(abs(success(nb_val2exclude_from_check+1:end-nb_val2exclude_from_check, :)-repmat(explicit_q, size(success, 1)-2*nb_val2exclude_from_check, 1)))));
                
                outputs = {st_data('init', 'title', data.title, 'info', data.info, ...
                    'date', mod(datenum(data.colnames, 'HH:MM:SS:FFF'), 1), ...
                    'colnames', cat(2, colnames, FIXING_COLN), ...
                    'value', cat(2, mean(data.value)', st_data('cols', st_data('from-idx', inputs{1}, 1:size(data.value, 2)), FIXING_COLN)), ...
                    'attach_format', 'HH:MM')};
                
                outputs{1}.info.ci.a = a;
                outputs{1}.info.ci.e = e;
                outputs{1}.info.ci.run_quality = check_val;
            otherwise
                error('st_seasonnality:exec', 'mode_seas <%s> unknown', mode);
        end
    end

%%** End of production
    function b = eop
        b = true;
    end

%%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

end


%% An old version, deprecated november the 12th 2008
%
%
% function o = st_seasonnality( varargin)
% % ST_SEASONNALITY - module qui fait des saisonnalit? sur  du volume
% % uniquement pour l'instant
% %
% % sert pour le graph indicator seasonnality
% %
% % data = read_dataset(['gi:seasonnality/' opt2str(options({...
% %       'mode_season','dirichlet','step:time',datenum(0,0,0,0,5,0)}))], ...
% %       'security_id', 276, 'from', '01/08/2008', 'to', '01/08/2008', ...
% %       'remove',[], 'trading-destinations',{});
% %
% %
% % author   : 'rburgot@cheuvreux.com'
% % reviewer : ''
% % version  : '1.1'
% % date     : '27/08/2008'
% %
% % See also st_node gi_create from_buffer
%
% this.params = {};
% this.memory = {};
%
% o = struct('init', @init, ...
%     'exec', @exec, ...
%     'learn', [], ...
%     'get', @get_, ...
%     'set', @set_, ...
%     'end_of_prod', @eop, ...
%     'plot',  [] );
%
% clear varargin;
%
% %%** Init
%     function init( varargin)
%         this.params = options(varargin);
%         this.memory = {};
%     end
%
% %%** Exec
%     function outputs = exec( inputs, varargin)
%         %< On retire les jours qui contiennent un auction stop de volatilit?
%         if ~isempty(inputs{1}.date)
%             days = unique(floor(inputs{1}.date(logical(st_data('cols', inputs{1}, 'stop_auction')))));
%             if ~isempty(days)
%                 idx = ismember(floor(inputs{1}.date), days);
%                 inputs{1} = st_data('log', st_data('from-idx', inputs{1}, ~idx), 'warn', sprintf('<%d> days have been removed from this data has they had a stop auction', length(days)));
%             end
%         end
%         %>
%         [outputs, b] = st_data('isempty', inputs);
%         if b
%             return
%         end
%         mode = this.params.get('mode_season');
%         switch mode
% %             case 'polya'
% %                 % J'aurai aim? ajouter les d?nominations suivantes
% %                 %, 'DCM', 'beta-binomiale', 'hyperg?om?trique n?gative'}
% %                 % Mais si je fais ?a et qu'on les utilise alors on peut
% %                 % avoir ? recalculer des choses d?j? bufferiz?es alors je
% %                 % m'abstiens
% %                 %
% %                 % Un peu d'histoire sur 'polya', 'DCM', 'beta-binomiale',
% %                 % 'hyperg?om?trique n?gative' :
% %                 % Il s'agit apr?s la distribution dirichlet du mod?le le
% %                 % plus simple pour mod?liser des proportions. 'polya' et
% %                 % 'DCM' coreespondent au m?me mod?le, de m?me
% %                 % 'beta-binomiale', 'hyperg?om?trique n?gative'
% %                 % correpondent au m?me mod?le, qui est un cas particulier
% %                 % de la distribution polya
% %                 %
% %                 % Pour l'impl?mentation de l'estimation des param?tres de
% %                 % la ditribution polya (et aussi des maths sur polya):
% %                 % cf. Estimating a Dirichlet ditribution de Thomas P. Minka
% %                 data = bin_separator(st_data('keep', outputs{1}, 'volume'));
% %                 vals = polya('fit', data.value)';
% %                 data.info.seasonnality.precision = sum(vals);
% %                 vals = vals / data.info.seasonnality.precision;
% %                 colnames = {'Usual day'};
% %             case 'dirichlet'
% %                 % Pour l'impl?mentation de l'estimation des param?tres
% %                 % cf. Estimating a Dirichlet ditribution de Thomas P. Minka
% %                 data = bin_separator(st_data('keep', outputs{1}, 'volume'));
% %                 data.value = data.value ./ repmat(sum(data.value, 2), 1, length(data.colnames));
% %                 vals = dirichlet('fit', data.value)';
% %                 data.info.seasonnality.precision = sum(vals);
% %                 vals = vals / data.info.seasonnality.precision;
% %                 colnames = {'Usual day'};
% %                 % La pr?cision ne peut pas ?tre utilis?e en pr?diction
% %                 % temps r?el puisque le volume fin de journ?e n'est pas
% %                 % connu
% %             case 'simple'
% %                 data = bin_separator(st_data('keep', outputs{1}, 'volume'));
% %                 seas_func = this.params.get('seas_func');
% %                 % On passe de volumes ? proportions
% %                 data.value = data.value ./ repmat(sum(data.value, 2), 1, length(data.colnames));
% %                 vals = seas_func(data.value)';
% %                 colnames = {'Usual day'};
% %             case 'prod'
% %                 data = outputs{1};
% %                 data.value = st_data('cols', data, 'volume') .* st_data('cols', data, 'vwap');
% %                 data.colnames = {'capital'};
% %                 data = bin_separator(st_data('keep', data, 'capital'));
% %                 seas_func = this.params.get('seas_func');
% %                 vals = seas_func(data.value)'; % pas la peine de renormaliser pour avoir une somme qui fait 1 ce sera fait en dessous
% %                 colnames = {'Usual day'};
%             case 'std'
%                 % Un seul contexte pour l'instant
%                 data = bin_separator(st_data('keep', outputs{1}, 'volume'));
%                 seas_func = this.params.get('seas_func');
%                 % On passe de volumes ? proportions
%                 data.value = data.value ./ repmat(sum(data.value, 2), 1, length(data.colnames));
%                 idx2del = any(data.value < 0, 2);
%                 if sum(idx2del)
%                     warning('cv_dirty_index:exec', 'There are negative volumes in your data!!!');
%                     data.value(idx2del, :) = [];
%                     data.date(idx2del, :) = [];
%                 end
%                 if length(data.date) > 20
%                     % On extrait les noisy days
%                     pca_engine = pca('nb_simul', 200 ,...
%                         'transformation', @(x)log(1 + x),...TODO something better than + 1?
%                         'mode_nb_factors', 'bootstrap', ...
%                         'normalized', false);
%                     pca_engine.estimate(data.value(:, 1:end-1));
%                     nd = pca_engine.compute_score(data.value(:, 1:end-1)) <= 0.1; % MAGIC NUMBER
%                     %< d?sormais les noisy day sont ceux o? nd est vrai
%                     vals = seas_func(data.value(~nd, :))';
%                     %>
%                 elseif length(data.date) > 1
%                     vals = seas_func(data.value)';
%                 else
%                     vals = data.value';
%                 end
%                 rel_volume = cumsum(data.value, 2);
%                 m = cumsum(vals, 2);
%                 v = var(rel_volume);
%                 tmp = (v./(m.*(1-m)));
%                 alpha = trimmean(1./tmp, 10) - 1;
%                 colnames = 'Usual day';
% %             case 'witching_day'
% %                 % pour l'instant on ne fait que de la saisonnalit? de volume
% %                 % ? changer, le but est de faire un graphe g?n?rique...
% %                 % la pr?sence deux boites de donn?es en entr?e est inutilis?
% %                 % pour l'instant, mais ? terme le but est de permettre de faire
% %                 % des saisonnalit? en m?me temps sur volume (avec auctions) et
% %                 % sur le spread et la volatilit? (qui n'ont pas de sens au
% %                 % moment des auctions, ou alors pas le m?me)
% %                 data = bin_separator(st_data('keep', outputs{1}, 'volume'));
% %                 seas_func = this.params.get('seas_func');
% %
% %                 % >* On va mettre de c?t? les withcing days
% %                 % TODO
% %                 % Attention pour les titres du SBF120 il faut
% %                 % appartenir aux indices FTSE si on veut que cela ait
% %                 % un sens de regarder les witching_days....
% %                 if ~isempty(strfind(data.title,'.PA')) % TODO BETTER : not really robust....
% %                     wd = witching_days('FCE', data.date);
% %                     wd = logical(wd); % si l'on veut avoir des courbes diff?rente suivant que c'est un withcing day, un double ou un triple, alors il faudra changer ?a entre autres
% %                 else
% %                     error('st_seasonnality:exec', 'mode witching_day is currently implemented only for CAC40 stocks');
% %                 end
% %                 %<*
% %
% %                 % On passe de volumes ? proportions
% %                 data.value = data.value ./ repmat(sum(data.value, 2), 1, length(data.colnames));
% %                 idx2del = any(data.value < 0, 2);
% %                 if sum(idx2del)
% %                     warning('cv_dirty_index:exec', 'There are negative volumes in your data!!!');
% %                     data.value(idx2del, :) = [];
% %                     data.date(idx2del, :) = [];
% %                 end
% %                 if length(data.date) > 20
% %                     % >* On extrait les noisy days parmis les non witching days
% %                     pca_engine = pca('nb_simul', 200 ,...
% %                         'transformation', @(x)log(1+x),... TODO it wont work if there are some zeros in volumes
% %                         'mode_nb_factors', 'bootstrap', ...
% %                         'normalized', false);
% %                     data_od = data.value(~wd, :);
% %                     pca_engine.estimate(data_od(:, 1:end-1));
% %                     nd = pca_engine.compute_score(data_od(:, 1:end-1)) <= 0.1; % MAGIC NUMBER
% %                     %<* d?sormais les noisy day sont (parmi data_od) ceux o? nd est vrai
% %
% %                     %cr?ation des 3 contextes ordinary days, witching days, noisy days
% %                     vals = [seas_func(data_od(~nd, :)); seas_func(data.value(wd, :));seas_func(data_od(nd, :))]';
% %                 else
% %                     vals = [seas_func(data.value);NaN(2, length(data.colnames))]';
% %                 end
% %                 colnames = {'Usual day', 'Witch day', 'Noisy day'};
%             otherwise
%                 error('st_seasonnality:exec', 'mode_seas <%s> unknown', this.params.get('mode_seas'));
%         end
%         % renormalisation pour avoir une somme qui fait 1
%         vals = vals ./ repmat(sum(vals), length(data.colnames), 1);
%         %< mise en forme des donn?es
%         data.info.seasonnality.mode_season = mode;
%         outputs = {st_data('init', 'title', data.title, 'info', data.info, ...
%             'date', mod(datenum(data.colnames, 'HH:MM:SS:FFF'), 1), ...
%             'colnames', cat(2, colnames, {'opening_auction', 'intraday_auction', 'closing_auction'}), ...
%             'value', cat(2, vals, st_data('cols', st_data('from-idx', inputs{1}, 1:size(vals, 1)), 'opening_auction;intraday_auction;closing_auction')), ...
%             'attach_format', 'HH:MM:SS')};
%         outputs{1}.info.run_quality = vc_check('vwap_slippage', outputs{1}, inputs{1});
%         %>
%     end
%
% %%** End of production
%     function b = eop
%         b = true;
%     end
%
% %%** get/set
%     function o = get_( name)
%         o = this.(name);
%     end
%     function value = set_( name, value)
%         this.(name) = value;
%     end
%
% end
%
