function o = st_simplereglin( varargin)
% ST_SIMPLEREGLIN - regression lineaire simple
%
%  input 1 : X
%  input 2 : Y (sauf s'il s'agit d'un nom de colonne de input 1)
%
% See also simplereglin
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', @learn, ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  @plot_ );
%%** Init       
function init( varargin)
    this.params = options({ 'X-colnames', '', ... % column to work on
        'Y-colname', '', ... % colnum to work on (if no input 2, it's in input 1)
        'constant', 1, ... % use of a constant
        'bootstrap', 0 , ... % bootstrap
        'plot-info', 'residuals' }, ... % info to plot just after learning a/stats/residuals*
         varargin );
    this.memory = options({'model', [], 'skel', [], 'xnames', []});
end

%%** Learn
function outputs = learn( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    outputs = {};
    inputs1 = cellfun(@(c)c{1}  , inputs, 'uni', false);
    inputs1 = st_data('stack', inputs1{:});
    inputs2 = cellfun(@(c)c{end}, inputs, 'uni', false);
    inputs2 = st_data('stack', inputs2{:});
    %<* X and Y
    xnames  = this.params.get( 'X-colnames');
    if isempty( xnames)
        X = inputs1.value;
        xnames = sprintf('%s;', inputs1.colnames{:});
        xnames(end) = '';
    else
        X = st_data('col', inputs1, xnames);
    end
    yname   = this.params.get( 'Y-colname');
    if isempty( yname)
        if length( inputs{1})<2
            error('st_simplereglin:learn', 'if no Y colname is specified, I need two inputs');
        else
            Y     = inputs2.value(:,1);
            yname = inputs2.colnames{1};
            if length( inputs2.colnames)>1
                warning( 'st_simplereglin:learn', 'I use only the first Y colnum <%s>\n', yname);
            end
        end
    else
        Y = st_data('col', inputs2, yname);
    end
    %>*
    boot  = this.params.get( 'bootstrap');
    if boot == 0
        boot = [];
    else
        st_log('Bootstraping %d times... this can be long...\n', boot);
    end
    const = this.params.get( 'constant');
    result = simplereglin( Y, X, 'bootstrap', boot, 'const', const, 'hat', true, 'residuals', true);
    
    this.memory.set('model', result);
    input2 = rmfield( inputs2, {'value', 'date'});
    input2.colnames = { yname };
    this.memory.set('skel' , input2);
    this.memory.set('xnames', xnames);
end

%%** Exec
function outputs = exec( inputs, varargin)
    outputs = inputs;
    outputs{1} = this.memory.get('skel') ; 
    result  = this.memory.get('model');
    xnames  = this.params.get( 'X-colnames');
    if isempty( xnames)
        X = inputs{1}.value;
        xnames = sprintf('%s;', inputs{1}.colnames{:});
        xnames(end) = '';
    else
        X = st_data('col', inputs{1}, xnames);
    end
    y_hat = simplereglin( result, X);
    outputs{1}.value = y_hat;
    outputs{1}.date  = inputs{1}.date;
    outputs{1}.colnames = cellfun(@(c)['Estimate for ' c], outputs{1}.colnames, 'uni', false);
end

%%** Plot
function h = plot_( inputs, varargin)
    plot_info = this.params.get( 'plot-info');
    if isempty( inputs)
        result = this.memory.get('model');
        skel   = this.memory.get('skel');
        xnames = this.memory.get('xnames');
        if isfield( result, 'plot')
            switch lower(plot_info)
                case { 'a', 'stats' }
                    result.plot( plot_info, 'figure', gcf);
                case 'residuals'
                    [y,x] = ksdensity( result.overall.residuals, 'kernel', 'epanechnikov');
                    subplot(1,2,1);
                    fill(x,y,'c','edgecolor', 'none', 'facecolor', [.67 0 .12]);
                    stitle('Residuals for %s\nstd gain: %3.2f%%', ...
                        skel.colnames{1}, 100-(std(result.overall.residuals)/result.overall.y_std)*100 );
                    subplot(1,2,2);
                    stem(result.overall.a(result.overall.const+1:end), 'linewidth', 2);
                    text( (2-result.overall.const):length(result.overall.a)-result.overall.const, ...
                        result.overall.a(result.overall.const+1:end)', ...
                        tokenize(xnames), 'rotation', 90);
                    ax = axis;
                    axis([.5 length(result.overall.a)-result.overall.const+.5 ax(3:4)]);
                otherwise
                    error('st_simplereglin:plot', 'plot info mode <%s> unknown', plot_info);
            end
        else
            [y,x] = ksdensity( result.residuals, 'kernel', 'epanechnikov');
            subplot(1,2,1);
            fill(x,y,'c','edgecolor', 'none', 'facecolor', [.67 0 .12]);
            stitle('Residuals for %s\nstd gain: %3.2f%%', ...
                skel.colnames{1}, 100-(std(result.residuals)/result.y_std)*100 );
            subplot(1,2,2);
            stem(result.a(result.const+1:end), 'linewidth', 2);
            text( (2-result.const):length(result.a)-result.const, result.a(result.const+1:end), tokenize(xnames), 'rotation', 90);
            ax = axis;
            axis([.5 length(result.a)-result.const+.5 ax(3:4)]);
        end
    else
        g = findobj( gcf, 'type', 'uipanel');
        st_plot( inputs, 'panel', g);
    end
    h = gcf;
end


%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end