function o = st_test_niveau( varargin)
% ST_TEST_NIVEAU - test de changement de niveau (moyenne)
%   je renvoie qq chose qui est homog�ne � la proba que les moyennes soient
%   diff�rentes (i.e. 1-p de ce que retourne ttest2) ou qu'il y ait deux
%   droite
%
% See also ttest2
this.params = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'get', @get_, ...
           'set', @set_, ...
           'end_of_prod', @eop, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({'colnames:char', '' , ... % colnums to extract / begin by a & means grep
        'windows', '[100 30 100]' , ... % - width of the 3 windows (in points), si c'est vide, alors je prend tout le monde
        'step', 10 , ... % - step size
        'test-type:char', 'level', ... % - type of test : level*/regression
        'plot:b', 0 }, ... % - affichage automatique
         varargin );
    this.memory = options({'n', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    [outputs, b] = st_data('isempty', inputs);
    if b
        return;
    end
    outputs  = inputs;
    colnames = this.params.get('colnames:char');
    if isempty( colnames)
        data = inputs{1};
    else
        data = st_data('keep', inputs{1}, colnames);
    end
    w     = this.params.get('windows');
    step  = this.params.get('step');
    N     = size(data.value,1);
    testy = this.params.get( 'test-type:char');
    if isempty( w)
        scale = (4*step:step:N-4*step)';
        p_val = 1-arrayfun(@(c)MYTEST( testy, data.value(1:c-step,:), data.date(1:c-step,:), ...
            data.value(c+step:end,:), data.date(c+step:end,:) ), scale);
        dts   = data.date(scale);
    else
        w     = eval( w);
        scale = (1:step:N-sum(w))';
        p_val = 1-arrayfun(@(l0,l1,r0,r1)MYTEST( testy, data.value(l0:l1,:), data.date(l0:l1,:), ...
            data.value(r0:r1,:), data.date(r0:r1,:) ), scale, scale+w(1), scale+w(1)+w(2), scale+sum(w));
        dts   = data.date(scale+w(1)+floor(w(3)/2));
    end
    outputs{1}.date  = dts;
    outputs{1}.value = p_val;
    outputs{1}.colnames = cellfun(@(c)['P value of ' c], outputs{1}.colnames, 'uni', false);
    
    if this.params.get('plot:b')
        opt = options(varargin);
        plot_focus( opt.get('title'));
        h = findobj(gcf,'type', 'uipanel');
        st_plot( outputs, 'panel', h);
    end
end

%%** Embedded
%<* TTEST2 for p-value
function p = MYTEST( tt, d1, t1, d2, t2, h)
    switch lower(tt)
        case 'level'
            [h,p] = ttest2( d1,d2);
        case 'regression'
            h = test_regress( '2vs1', [d1;d2], [t1;t2], length(t1), 'verbose', 0 );
            p = h.stats.p;
        otherwise
            error('st_test_niveau:mytest:type', 'test-type <%s> unknwon', tt);
    end
end
%>*

%%** End of production
function b = eop
    b = true;
end

%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end