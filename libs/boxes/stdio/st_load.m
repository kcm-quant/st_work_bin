function o = st_load( varargin)
% ST_LOAD - load a mat file containing a cellarray of st_data
%
% See also st_data
this.params  = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'end_of_prod', @eop, ...
           'get', @get_, ...
           'set', @set_, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({'mat-file', 'C:\st_work\methodo\ATS\data\ATS-SBF120-01.mat' }, ... % - eponym
        varargin);
    this.memory = options({'n', 0, 'data', [], 'eop', false});
end

%%** Exec
function outputs = exec( inputs, varargin)
    data = this.memory.get('data');
    if isempty( data)
        data = st_data('ope-load', this.params.get('mat-file'));
        this.memory.set('data', data);
    end
    n = this.memory.set('n', this.memory.get('n')+1);
    outputs = data(n) ;
    if n>=length(data)
        this.memory.set('n', 0);
        this.memory.set('eop', true);
    end
end

%%** End of production
function b = eop
    b = this.memory.get('eop') ;
end
%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end