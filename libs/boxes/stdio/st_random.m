function o = st_random( varargin)
% ST_RANDOM - module de production random
%   deuxi�me st_module r�alis�
%
% Le prototype d'un module est simple:
% - une m�thode  .init( varargin)
% - une m�thode  .exec( {input}  , varargin) -> {output}
% - une m�thode .learn( {{input}}, varargin) -> {} ou ![] (cst)
% - une m�thode  .plot( {output} , varargin)  -> h ou ![] (cst)
% - une m�thode .get/.set
% - une m�thode .end_of_prod() -> true/false
%
% L'acc�s � ces m�thodes est verrouill� au niveau de st_node
%
% See also st_node 
this.params  = {};
this.memory  = {};

o = struct('init', @init, ...
           'exec', @exec, ...
           'learn', [], ...
           'end_of_prod', @eop, ...
           'get', @get_, ...
           'set', @set_, ...
           'plot',  [] );
%%** Init       
function init( varargin)
    this.params = options({'generate:@', @(n,p) exp(cumsum(randn(n,p))*.02), ... % - 'generate' fonction de g�n�ration des donn�es
        'nb-rows:int', 100, ... % - 'nb-rows' eponym
        'nb-cols:int', 2}, varargin); % - 'nb-cols', eponym
    this.memory = options({'n', 0});
end

%%** Exec
function outputs = exec( inputs, varargin)
    n = this.memory.set('n', this.memory.get('n')+1);
    outputs = { st_data('init', 'title', 'Randomly generated data', ...
                 'date', (1:this.params.get('nb-rows'))', ...
                 'value', this.params.eval( 'generate', this.params.get('nb-rows'), this.params.get('nb-cols')), ...
                 'colnames', tokenize( sprintf('V-%d;', 1:this.params.get('nb-cols')) ,';')) };
end

%%** End of production
function b = eop
    b = true;
end
%%** get/set
function o = get_( name)
    o = this.(name);
end
function value = set_( name, value)
    this.(name) = value;
end

end