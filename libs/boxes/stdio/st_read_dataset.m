function o = st_read_dataset( varargin)
% ST_READ_DATASET - lecture des donn?es gr?ce ? read_dataset
%
%n'en a rien ? faire de ses imputs!!!=> ne devrait pas
%                   pouvoir ?tre connect? en entr?e
%
% See also read_dataset

this.params  = {};
this.memory  = {};
clear varargin;

o = struct('init', @init, ...
    'exec', @exec, ...
    'learn', [], ...
    'end_of_prod', @eop, ...
    'get', @get_, ...
    'set', @set_, ...
    'plot', []);
%%** Init
    function init( varargin)
        this.params = options({'RIC:char', 'FTE.PA', ... % - code Reteurs
            'from:dd/mm/yyyy', '25/06/2007', ... % - premi?re date inclue
            'to:dd/mm/yyyy', '29/06/2007', ... % - derni?re date inclue
            'source:char', 'btickdb', ... % - mode de read_dataset ? utiliser
            'formula:char', '', ... % formule ? donner ? read_dataset pour qu'il l'applique lui m?me
            'where_formula:char', '', ... % formule ? donner ? read_dataset pour qu'il l'applique lui m?me
            'post_formula:func', [], ... % fonction ? appliquer aux donn?es renvoy?es par read_dataset
            'window:char', 'day|1', ... % sp?cification de la fen?tre temporelle n?cessaire ? la construction de l'indicateur sur une journ?e
            'trading-destinations:cell', {} ... % trading dest: main;chix
            'code2id:char', 'security', ... % comment passe-t-on du security_id (qui peut �tre un index_id ou un future_id) � la clef
            'cell_source:b', true, ... % si l'on donne comme security_id un tableau, est-ce parce que l'on souhaite un cell aray de st_data ou non?
            'step:char', 'day|1', ...
            'oos_data:char', 'none', ...
            'force_unique_intraday_auction:b', false...
            }, varargin);
        this.memory = options({'n', 1, 'data', [], 'end_of_prod', false, 'gi_params', [], 'my_calendar', []});
    end

%%** Exec
    function outputs = exec( inputs, varargin)
        src_name = this.params.get('source:char');
        cell_source = this.params.get('cell_source:b');
        oos_data = this.params.get('oos_data:char');
        if strcmpi( src_name, 'daily') || strcmpi( src_name, 'bdaily')
            daily = true;
        else
            daily = false;
        end
        securities = this.params.get('RIC:char');
        if ischar(securities)
            if strcmp(src_name, 'index')
                [tmp,securities] = get_repository( 'index-key', securities);
            elseif strcmp(src_name, 'future')
                securities=get_repository('fut_name2fut_id',securities);
            else
                code2id = this.params.get('code2id:char');
                switch code2id
                    case 'security'
                        securities = get_sec_id_from_ric(securities);
                    case 'index'
                        [tmp,securities] = get_repository( 'index-key', securities);
                    otherwise
                        error('st_read_dataset:exec', 'Unknown code2id mode : <%s>', code2id);
                end
            end
        end
        if cell_source
            nb_sec = length(securities);
        else
            nb_sec = 1;
        end
        if length(src_name) < 17 ||  ~strcmp(src_name(1:17), [char(165) 'dontWantAnyData' char(165)])
            data = this.memory.get('data');
            window_spec = tokenize(this.params.get('window:char'), '|');
            window_spec = {lower(window_spec{1}), str2double(window_spec{2})};
            step_spec = tokenize(this.params.get('step:char'), '|');
            step_spec = {lower(step_spec{1}), str2double(step_spec{2})};
            if step_spec{2} > window_spec{2}
                error('st_read_dataset:exec', 'step > window is not yet implemented');
            end
            if ~strcmp(window_spec{1}, 'day') || ~strcmp(step_spec{1}, 'day')
                error('st_read_dataset:exec', 'only ''day'' is currently recognized for specifying a step or a window');
            end
            if isempty(data)
                %< Lecture et m?morisation
                td = this.params.get('trading-destinations:cell');
                if ischar( td)
                    td = tokenize(td, ';');
                end
                if isempty( td)
                    td = {};
                end
                data = cell(1, nb_sec);
                from_ = this.params.get('from:dd/mm/yyyy');
                from_window = se_window_spec2from_to(window_spec{:}, from_);
                switch oos_data
                    case 'day'
                        to_ = datestr(datenum(this.params.get('to:dd/mm/yyyy'), 'dd/mm/yyyy') + step_spec{2}, 'dd/mm/yyyy');
                    case 'none'
                        to_ = this.params.get('to:dd/mm/yyyy');
                    otherwise
                        error('st_read_dataset:exec', 'Out of sample data is currently implemented only on a ''day'' mode');
                end
                for i = 1 : nb_sec
                    if cell_source
                        tmp_sec = securities(i);
                        if isnumeric(td) && length(td) == length(securities)
                            tmp_td = td(i);
                        else
                            tmp_td  = td;
                        end
                    else
                        tmp_sec = securities;
                        tmp_td  = td;
                    end
                    data{i} = read_dataset( src_name, 'security_id', tmp_sec,...
                        'from', datestr(from_window, 'dd/mm/yyyy'), ...
                        'to', to_, ...
                        'output-mode', 'day', ...
                        'last_formula', this.params.get('formula:char'), ...
                        'where_formula', this.params.get('where_formula:char'), ...
                        'trading-destinations', tmp_td,'today_allowed',this.params.try_get('today_allowed',false));
                    if ~isempty(this.params.get('post_formula:func'))
                        st_log('Applying your post formula function to the data\n');
                        pf = make_if_safe(this.params.get('post_formula:func'));
                        data{i} = cellfun(pf, data{i}, 'uni', false);
                        st_log('Post formula function applied\n');
                    end
                    %                     if length(src_name) < 3 || ~strcmp(src_name(1:3), 'gi:') && ~daily
                    %                         st_log('Converting time from gmt to local timezone\n');
                    %                         data{i} = cellfun(@(d)timezone('convert', 'data', d), data{i}, 'uni', false);
                    %                         st_log('Time converted\n');
                    %                     end
                end
                this.memory.set('data', data);
                this.memory.set('my_calendar', datenum(from_, 'dd/mm/yyyy'):step_spec{2}:datenum(to_, 'dd/mm/yyyy'));
                %>
            end
            %< Agregation des donn?es avant de les envoyer
            if  nargin>0 && ~isempty(inputs)
                n = inputs{1}.info.n;
            else
                n = this.memory.get('n');
            end
            % TODO implement for any window_type
            % TODO r?crire ?a m?me pour window_type=day ?a ne fonctionne
            % pas, puisque les we et les date_remove sont filtr?s
            out_ = cell(1, nb_sec);
            for i = 1 : nb_sec
                if daily
                    % si je suis en daily
                    first_date  = datenum( this.params.get('from:dd/mm/yyyy'), 'dd/mm/yyyy') ;
                    this_window = (data{i}.date >= se_window_spec2from_to(window_spec{:}, first_date+n)) & ...
                        (data{i}.date <= first_date+n) ;
                    out_{i} = st_data('sort', st_data('from-idx', data{i}, this_window), '.date');
                else
                    % si je suis en intra day
                    switch window_spec{1}
                        case 'day'
                            out_{i} = data{i}{n};
                            last_j_not_empty = n;
                            for j = n + 1 : n + window_spec{2} - 1
                                if ~st_data('isempty-nl', data{i}{j})
                                    if st_data('isempty-nl', out_{i})
                                        out_{i} = data{i}{j};
                                    else
                                        out_{i}.value = [out_{i}.value; data{i}{j}.value];
                                        out_{i}.date  = [out_{i}.date; data{i}{j}.date];
                                        last_j_not_empty = j;
                                    end
                                end
                            end
                            try
                                out_{i}.info = data{i}{last_j_not_empty}.info; % to have the freshest information
                            catch
                            end
                            if window_spec{2}>1 && ~isempty(out_{i})
                                out_{i}.title = sprintf('%s to %s', out_{i}.title, ...
                                    datestr(datenum(this.params.get('from:dd/mm/yyyy'), 'dd/mm/yyyy')+n-1, 'dd/mm/yyyy'));
                            end
                        otherwise
                            error('st_read_dataset:exec', 'This window_type is not yet implemented : <%s>', window_spec{1});
                    end
                end
            end
            %>
            for i = 1 : nb_sec
                if isempty(out_{i})
                    out_{i} = st_data('empty-init');
                end
            end
            out_{1}.info.data_datestamp = datenum(this.params.get('from:dd/mm/yyyy'), 'dd/mm/yyyy')+n-1;
            out_3 = {};
            switch oos_data
                case 'none'
                case 'day'
                    if daily
                        error('st_read_dataset:exec', 'Out of sample data for daily figures is not currently implemented');
                    end
                    if nb_sec > 1
                        out_3 = cell(1, nb_sec);
                        for i = 1 : nb_sec
                            out_3{i} = data{i}{n + window_spec{2}:n + window_spec{2} + step_spec{2} - 1};
                        end
                    else
                        out_3 = {data{1}(n + window_spec{2}:n + window_spec{2} + step_spec{2} - 1)};
                    end
                otherwise
                    error('st_read_dataset:exec', 'Out of sample data is currently implemented only on a ''day'' mode');
            end
            if nb_sec > 1
                outputs = {  out_ , st_data('init', 'value', [], 'date', [], 'colnames', [], 'info', struct('n', n)), out_3};
            else
                outputs = cat(2, out_, st_data('init', 'value', [], 'date', [], 'colnames', [], 'info', struct('n', n)), out_3);
            end
            %< gestion de eop et n
            if  nargin > 0 && ~isempty(inputs)
                this.memory.set('end_of_prod', true);
            else
                if step_spec{2} == 1
                    my_calendar = this.memory.get('my_calendar');
                    wd = weekday(my_calendar(n));
                    if wd < 6
                        n = n + 1;
                    elseif wd == 6
                        n = n + 3;
                    elseif wd == 7;
                        n = n + 2;
                    end
                else
                    n = n + step_spec{2};
                end
                if daily % uniquement pour des donn?es daily
                    if first_date+n > max(data{1}.date)
                        % Endless loop
                        n = 1;
                        this.memory.set('end_of_prod', true);
                    else
                        this.memory.set('end_of_prod', false);
                    end
                else
                    % TODO generalize to any window_type
                    if n + window_spec{2} - 1 > length(data{1})
                        % Endless loop
                        n = 1;
                        this.memory.set('end_of_prod', true);
                    else
                        this.memory.set('end_of_prod', false);
                    end
                end
                this.memory.set('n', n);
            end
        else
            outputs = {{}};
            this.memory.set('end_of_prod', true);
        end
        %>
    end

%%** End of production
    function b = eop
        b = this.memory.get('end_of_prod');
    end
%%** get/set
    function o = get_( name)
        o = this.(name);
    end
    function value = set_( name, value)
        this.(name) = value;
    end

%%** s?curisation des fonctions utilisateur
    function secu_fct = make_if_safe(pf)
        secu_fct = @my_pf;
        function data = my_pf(data)
            if ~isempty(data) && isfield(data, 'value') && ~isempty(data.value)
                data = pf(data);
            end
        end
    end
end