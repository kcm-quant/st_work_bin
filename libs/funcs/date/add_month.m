function incr_date = add_month(date, incr)
% Add incr months to date

incr_date = datetime(year(date),month(date)+incr,day(date));
if ~isdatetime(date)
    incr_date = datenum(incr_date);
end