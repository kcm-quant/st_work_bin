function str = datestr_ms(time,day_into_date)
% DATESTR -  with fixed format and full (up to microseconds) precision
%
% a=734412;b=31226;c=461349
% sec1 = 1/(24*3600);
% datestr_ms(a + b*sec1 + c*sec1*1e-6)
% % se vautre pour raison de pr?cision, m?me le nouveau code
%
% a=734412;b=32400;c=665932
% sec1 = 1/(24*3600);
% datestr_ms( a + b*sec1 + c*sec1*1e-6)
% % se vautre si on utilise le vieux code comment? ci dessous 
% 
% % microseconds = mod(time, 1e-3/(24*3600))*1e6*24*3600;
% % time_millisec_acc = time-microseconds/(1e6*24*3600);
% % rounded_tmicros = round(microseconds*1e3)/1e3;
% % idx2be_corrected = rounded_tmicros >= 1e3;
% % microseconds(idx2be_corrected) = 0;
% % time_millisec_acc = time_millisec_acc + idx2be_corrected/(1e3*24*3600);

if nargin<=1
    day_into_date=true;
end
sec1 = 1/(24*3600);
day_ = floor(time);
time_no_day = mod(time,1);
secs_ = floor(time_no_day/sec1);
microsecs = round(mod(time_no_day, sec1)*1e6*24*3600);
millisecs = floor(microsecs/1e3);
microseconds = microsecs - millisecs*1e3;

time_millisec_acc = day_ + secs_*sec1 + +millisecs*sec1*1e-3;

if day_into_date
    default_unknown='??/??/???? ??:??:??.???.???';
    time_format='dd/mm/yyyy HH:MM:SS.FFF';
else
    default_unknown='??:??:??.???.???';
    time_format='HH:MM:SS.FFF';
end

idx=isfinite(time_millisec_acc);
str=repmat(default_unknown,size(time_millisec_acc,1),1);
str(idx,:)=[datestr(time_millisec_acc(idx), time_format),repmat('.',size(time_millisec_acc(idx))),num2str(microseconds(idx),'%03d')];

end