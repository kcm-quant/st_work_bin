function out = get_bi_time_grid(varargin)
% get_bi_time_grid
% gives the grid time as in the basic indicator program
% example : simple call -> output in local time
% out = get_bi_time_grid('security_id',get_sec_id_from_ric('VOWG_p.DE'),'date','08/06/2010');


opt  = options( {'security_id',NaN,...
    'date',NaN,...
    'tdinfo',[],...
    'step_time',datenum(0,0,0,0,5,0),...
    'window_time',datenum(0,0,0,0,5,0),...
    'is_gmt:bool',false}, varargin);


% - on r�cup�re les donn�es d'entr�es
security_id=opt.get('security_id');
date=opt.get('date');
date_numeric=datenum(date,'dd/mm/yyyy');
trdt_info=opt.get('tdinfo');
step_time=opt.get('step_time');
window_time=opt.get('window_time');
is_gmt=opt.get('is_gmt:bool');


% - on cr�e un fake st_data pour get_time_grid
data_na_fake=st_data('init','value',1,...
    'date',date_numeric,'colnames',{'a'});
if isempty(trdt_info)
    trdt_info=get_repository('tdinfo',security_id);
end
data_na_fake.info.td_info=trdt_info;

% - on r�cup�re la grille du continue
[grid, trdt, main_td] = get_time_grid('disjoint',data_na_fake,...
    'back_to_gmt:bool',is_gmt,...
    'window:time', window_time,...
    'step:time',step_time);
vals=zeros(size(grid,1),3);

% - on r�cup�re la grille des auctions
[a_vals, a_dts] = agg_and_check_auctions(st_data('empty-init'),trdt,1,date_numeric, main_td);
if isempty(a_vals)
    error('get_bi_time_grid: due to the trading_times, the bi grid cant be build');
end
a_vals=a_vals(:,[size(a_vals,2)-3 size(a_vals,2)-1 size(a_vals,2)-2]);

if is_gmt
    a_dts=timezone('back_to_gmt','dates',a_dts,'init_place_id',trdt_info(1).place_id);
end

% - on met en forme la sortie
if ~isempty(a_dts) && any(ismember(a_dts,grid(:,3)))
    idx_2slide=ismember(a_dts,grid(:,3));
    a_dts(idx_2slide)=a_dts(idx_2slide)-datenum(0,0,0,0,0,1);
end
dts  = [grid(:,3); a_dts'];

vals  = [vals; a_vals];
[dts, idx_sorted] = sort(dts);
vals = vals(idx_sorted, :);

out=st_data('init',...
    'title','Basic indicator time grid',...
    'date',dts,...
    'value',vals,...
    'colnames',{'opening_auction','intraday_auction','closing_auction'});
out.info.localtime=~is_gmt;
out.info.local_place_timezone_id=trdt_info(1).place_id;
if is_gmt
    out.info.place_timezone_id=0;
else
    out.info.place_timezone_id=out.info.local_place_timezone_id;
end
out.info.data_datestamp=date_numeric;



end





