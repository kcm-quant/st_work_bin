function [rslt, trading_times, main_td] = get_time_grid(mode, data, varargin)
% get_time_grid(mode, data, varargin) - r�cup�ration d'une grille temporelle
%                 Le r�sultat renvoy� est un tableau � trois colonnes.
%                 Le nombre de lignes est �gal au nombre d'intervalles.
%                 La premi�re colonne contient l'indice (dans data) du d�but de
%                 l'intervalle, la deuxi�me colonne contient l'indice de la
%                 derni�re observation appartenant � l'intervalle, enfin,
%                 la troisi�me colonne contient l'heure (et la date) de fin de
%                 l'intervalle
%                 S'il n'y a aucune donn�e qui tombe dans un intervalle,
%                 alors la valeurs dans la deuxi�me colonne sera < � celle
%                 de la premi�re
%
%       data ne doit contenir qu'une seule journ�e
%
% TODO profile optimize add modes...
%
% author   : 'rburgot@cheuvreux.com'
% reviewer : ''
% version  : '1'
% date     : '20/08/2008'
%
% See also trading_time

opt = options({ 'window:time', datenum(0,0,0,0,15,0), ...
                'step:time', datenum(0,0,0,0,15,0), ...
                'offset:time', 0, ...
                'post_offset:time', 0, ... % not used currently?
                'is_gmt:bool', true, ...
                'back_to_gmt:bool', false, ...
                'regularize_grid:bool', true, ...
                'given_trdt', {}, ... un cell-array qui contient dans les num�ros impairs le nom du champs dont la valeur est chang�e, et dans les num�ros pairs la nouvelle valeur. Les valeurs non modifi�ees peuvent �tre trouv�es en lignes 56 et plus. Un astuce pour utiliser les horaires par d�faut consiste � utiliser un cell-array du genre {'a', 0}
                'default_date', NaN, ...
                }, varargin);

switch lower(mode)
    case 'disjoint' % TODO profile optimize
        wind = opt.get('window:time');
        step = opt.get('step:time');
        if opt.get('is_gmt:bool')
            dts = timezone('convert', 'data', data);
            dts = dts.date;
        else
            dts = data.date;
        end
        days = unique(floor(dts));
        nb_days = length(days);
        if nb_days > 1
            error('get_time_grid:exec', 'Unable to work on more than one day');
        elseif nb_days==0
            days = opt.get('default_date'); % juste pour ne pas planter lors de l'appel � trading time deux lignes plus loin, et ne pas utiliser today
            nb_days=1;
        end
        if isempty(opt.get('given_trdt'))
            [trading_times, main_td] = trading_time(data.info.td_info, days(1));
        else
            error('get_time_grid:unreachable code'); % => given_trdt is unused?
%             trading_times = struct('opening_auction', datenum(0,0,0,7,15,0), ...
%                 'opening_fixing', datenum(0,0,0,9,0,0), ...
%                 'opening', datenum(0,0,0,9,0,0), ...
%                 'intraday_stop_auction', NaN, ...
%                 'intraday_stop_fixing', NaN, ...
%                 'intraday_stop', NaN, ...
%                 'intraday_resumption_auction', NaN, ...
%                 'intraday_resumption_fixing', NaN, ...
%                 'intraday_resumption', NaN, ...
%                 'closing_auction', datenum(0,0,0,17,30,0), ...
%                 'closing_fixing', NaN, ...
%                 'closing', datenum(0,0,0,17,35,0), ...
%                 'post_opening', datenum(0,0,0,17,35,0), ...
%                 'post_closing', datenum(0,0,0,17,40,0) ...
%                 );
        end
        if wind == 1 && step == 1
            if isempty(data.date)
                rslt = [1, length(data.date),opt.get('default_date')+1-1/(24*3600)];
            else
                rslt = [1, length(data.date), floor(data.date(1))+1-1/(24*3600)];
            end
            return;
        end
        if length(trading_times) > 1
            error('get_time_grid:exec', 'Unable to work with more than one quotation_group');
        end
        eod_h = mod([trading_times.closing_auction], 1);
        if ~isfinite(eod_h)
            eod_h = mod([trading_times.closing], 1); % BEQUILLE : 
            % pour faire une grille j'ai besoin d'un d�but et d'une fin.
            % habituellement j'utilise 
            % - debut : opening qui est cens� �tre l'horaire
            % de fin du fixing du matin, et qui a l'air d'�tre toujours
            % renseign� (...???)
            % fin : closing_auction qui est cens� �tre l'horaire
            % de debut du fixing du soir. SAUF que dans le cas o� il n'y a
            % pas de fixing le soir, alors cet horaire n'est pas renseign�.
            % Dans ce cas, il faut utiliser closing
        end
        bod_h = mod([trading_times.opening], 1);
        float_str_eps = 1/(24*3600);
        step_grid = [];
        if opt.get('regularize_grid:bool')
            tmp = step / datenum(0,0,0,0,5,0);
            if round(tmp) >= tmp -  float_str_eps / datenum(0,0,0,0,5,0) ...
                    && round(tmp) <= tmp +  float_str_eps / datenum(0,0,0,0,5,0) ...
                && (round(tmp) == 1 || round(tmp) == 2 || round(tmp) == 3 || round(tmp) == 4 || round(tmp) == 6 || round(tmp) == 12)
                tmp = round((opt.get('offset:time') + bod_h + wind) / step) * step;
                step_grid = tmp : step : eod_h;
                if step_grid(end) < eod_h - float_str_eps || step_grid(end) > eod_h + float_str_eps
                    if (eod_h - step_grid(end) > step / 2)
                        step_grid(end + 1) = eod_h;
                    else
                        step_grid(end) = eod_h;
                    end
                    if step == wind
                        wind = repmat(wind, length(step_grid), 1);
                        wind(end) = step_grid(end) - step_grid(end-1);
                    end
                end
            else
%                 warning('continuous_time_grid:exec', 'I refuse to regularize time_grid with a step which is not 5, 10, 20 or 30 minutes.\n Here step = <%d>\n', step);
            end
        end
        if isempty(step_grid)
            step_grid = opt.get('offset:time') + bod_h + wind : step : (eod_h+float_str_eps);
        end
        nb_per_day = length(step_grid);
        dtu = zeros(nb_days * nb_per_day, 1);
        for i = 1 : nb_days
            dtu((i - 1) * nb_per_day + 1 : i * nb_per_day) = days(i) + step_grid';
        end
        
        sec_dt = 1/(24*360);
        bound_inf = round((dtu - wind)/sec_dt)*sec_dt;
        bound_sup = round(dtu/sec_dt)*sec_dt;
        
        bin_tol = 0;%2/(24*60); % too often, the last trades in continuous phase happens somes seconds after the closing hours and it may represent much volume
        bound_sup(end) = bound_sup(end) + bin_tol; 
        bound_inf(1) = bound_inf(1) - bin_tol; 
        
        nb = length(dts) + 1;
        
        idx_first = nb*ones(length(bound_inf), 1);
        idx_last  = zeros(length(bound_inf), 1);
        
        for i = 1 : length(idx_first)
            idx = (dts >= bound_inf(i) & dts < bound_sup(i));
            tmp_idx_first = find(idx, 1, 'first');
            if ~isempty(tmp_idx_first)
                idx_first(i) = tmp_idx_first;
                idx_last(i)  = find(idx, 1, 'last');
            end
        end
        
        bound_sup(end) = bound_sup(end) - bin_tol; 
        
        rslt = [idx_first, idx_last, bound_sup];
        if opt.get('back_to_gmt:bool') && opt.get('is_gmt:bool')
            rslt(:, 3) = timezone('back_to_gmt', 'dates', rslt(:, 3), 'init_place_id', data.info.td_info(1).place_id);
            % remplace data.info{1}.place_id
        end
    otherwise
        error('Error in get_time_grid, mode : <%s> unknown', mode);
end