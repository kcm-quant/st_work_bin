function b = is_busday(calendar, varargin)
% IS_BUSDAY - prend en argument une chaine de charatere correspondant � un 
%   calendrier, et renvoie un vecteur de booleen de la m�me taille
%   indiquant s'il s'agit d'un jour ouvr� pour le calendrier consid�r�
%
%   actuellement, seul le calendrier TARGET est impl�ment�
% 
%   les arguments suivants sont au choix : soit un seul argument qui est un
%   vecteur de dates; soit 4 arguments : y, m, d, wd qui sont :year, month,
%   day_in_month, day_in_week
%
%   ptet il vaudrait mieux l'impl�menter avec isbusday mais l� tout de
%   suite j'ai pas envie...
%
%   autre remarque : ne fonctionne qu'� partir de l'ann�e 2000
%
% ex :
%   is_busday('TARGET', datenum(2008,5,1,0,0,0):datenum(2008,5,8,0,0,0)) % 1er mai
%   is_busday('TARGET', datenum(2008,3,20,0,0,0):datenum(2008,3,25,0,0,0)) % vendredi saint et lundi de p�ques
%
% See also isbusday

if length(varargin) == 1
    [y, m, d] = datevec(varargin{1});
    wd = weekday(varargin{1});
elseif length(varargin) == 4
    [y, m, d, wd] = varargin{:};
else
    error('is_buday:exec', 'wrong number of input, can be 2 or 5');
end

switch lower(calendar)
    case 'target'
        b = is_target_busday(y, m, d, wd);
    otherwise
        error('is_busday:exec', 'Calendar <%s> not yet implemented', calendar);
end

end

function b = is_target_busday(y, m, d, wd) % year, month, day_in_month, day_in_week
yd = datenum(y, m, d) - datenum(y, 1, 1) + 1; % inline de yearday
easterMonday = [...
    115; 106;  91; 111; 103;  87; 107;  99;  84; 103;...  // 2000-2009
    95; 115; 100;  91; 111;  96;  88; 107;  92; 112;...   // 2010-2019
    104;  95; 108; 100;  92; 111;  96;  88; 108;  92;...   // 2020-2029
    112; 104;  89; 108; 100;  85; 105;  96; 116; 101;...   // 2030-2039
    93; 112;  97;  89; 109; 100;  85; 105;  97; 109;...   // 2040-2049
    101;  93; 113;  97;  89; 109;  94; 113; 105;  90;...   // 2050-2059
    110; 101;  86; 106;  98;  89; 102;  94; 114; 105;...   // 2060-2069
    90; 110; 102;  86; 106;  98; 111; 102;  94; 114;...   // 2070-2079
    99;  90; 110;  95;  87; 106;  91; 111; 103;  94;...   // 2080-2089
    107;  99;  91; 103;  95; 115; 107;  91; 111; 103;...   // 2090-2099
    88; 108; 100;  85; 105;  96; 109; 101;  93; 112;...   // 2100-2109
    97;  89; 109;  93; 113; 105;  90; 109; 101;  86;...   // 2110-2119
    106;  97;  89; 102;  94; 113; 105;  90; 110; 101;...   // 2120-2129
    86; 106;  98; 110; 102;  94; 114;  98;  90; 110;...   // 2130-2139
    95;  86; 106;  91; 111; 102;  94; 107;  99;  90;...   // 2140-2149
    103;  95; 115; 106;  91; 111; 103;  87; 107;  99;...   // 2150-2159
    84; 103;  95; 115; 100;  91; 111;  96;  88; 107;...   // 2160-2169
    92; 112; 104;  95; 108; 100;  92; 111;  96;  88;...   // 2170-2179
    108;  92; 112; 104;  89; 108; 100;  85; 105;  96;...   // 2180-2189
    116; 101;  93; 112;  97;  89; 109; 100;  85; 105 ...   // 2190-2199
    ];
em = easterMonday(y-1999);
if size(em, 1) ~= size(y, 1)
    em = em';
end
b = ~(wd == 7 | wd == 1 ... week end
    | (d == 1 & m == 1) ... New Year's Day
    | yd == em-3 ... Good Friday
    | yd == em ... Easter Monday
    | (d == 1 & m == 5) ... Labour day
    | (d == 25 & m == 12) ... Christmas
    | (d == 31 & m == 12 & y == 2001) ); % December 31st, 1998, 1999, and 2001 only
end