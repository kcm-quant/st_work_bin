function bool = is_context_march_special_ny(varargin)
% is_context_march_special_ny : return a boolean whereas the volume curve
% from the day "date" has to be considered with a context march special ny
%
% trdt_info=get_repository('tdinfo',110);
% bool1=is_context_march_special_ny('security_id',110,'date','10/03/2010');
% bool1b=is_context_march_special_ny('security_id',110,'date',datenum('10/03/2010','dd/mm/yyyy'):datenum('30/03/2010','dd/mm/yyyy'));
% bool2=is_context_march_special_ny('place_timezone',trdt_info(1).timezone,'date','30/03/2010');

opt  = options( {'security_id',NaN,...
    'width',50,...
    'date','',...
    'place_timezone',NaN}, varargin);

sec_id=opt.get('security_id');
width=opt.get('width');
date=opt.get('date');
place_timezone=opt.get('place_timezone');

%- test suivant l'entr�e de date
if ischar(date)
    dates_num=datenum(date,'dd/mm/yyyy');
else
    dates_num=date;
end


place_time_zone_us = cell2mat(exec_sql('BSIRIUS', 'select place_id from repository..place where name = ''NEW YORK STOCK MARKET'''));
place_time_zone_us = unique(cell2mat(exec_sql('BSIRIUS', sprintf('select isnull(place_timezone, place_id) from repository..execution_market where place_id = %d', place_time_zone_us))));
%%% - dans le cas ou on connait pas la place_id
if isnan(place_timezone)
    trdt_info=get_repository('tdinfo',sec_id);
    place_timezone=trdt_info(1).timezone;
end


bool=repmat(false,length(dates_num),1);

for i=1:length(dates_num)
    
    [from,to] = se_window_spec2from_to('day',width,dates_num(i));
    
    offset_period = timezone('get_offset','init_place_id', place_time_zone_us,'final_place_id',place_timezone,'dates',from:to);
    usual_offset=nanmedian(offset_period);
    
    bool(i)=(offset_period(end)~=usual_offset);
end




end

