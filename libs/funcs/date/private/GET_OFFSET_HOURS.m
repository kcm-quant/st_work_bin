function offset_hours = GET_OFFSET_HOURS(initial_timezone, final_timezone, date4offset)
date4offset = datestr(date4offset, 'yyyy-mm-dd');
offset_hours = G_O_H(final_timezone, date4offset) - G_O_H(initial_timezone, date4offset);
end

function offset_hours = G_O_H(timezone, date4offset)
global st_version
if strcmp(timezone,'GMT')
    offset_hours=0;
    return
end

repository = st_version.bases.repository;
end_req = sprintf(' TIMEZONE = ''%s''', timezone);
offset_hours = cell2mat(exec_sql('KGR',...
    sprintf(['select OFFSET',...
    ' from ',repository,'..TIMEZONE'...
    ' where BEGINDATE < ''%s'' and isnull(ENDDATE, ''%s'') >= ''%s'' and %s'],...
    date4offset, date4offset, date4offset, end_req)));
offset_hours = offset_hours(1);

end