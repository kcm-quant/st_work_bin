function varargout = persist_bufferize(fname, fargs)
% gestion centralis� de la m�moire de certaines fonctions au comportement 
% d�terministe... Attention si l'on l'utilise pour des fonctions dont les
% outputs d�pendent du jour et que Matlab n'a pas �t� relanc� entre temps.
% Comment �a c'est sale?
%
% author   : 'rburgot@cheuvreux.com'
% reviewer : ''
% version  : '1'
% date     : '21/08/2008'
global st_version
persistent memory

if st_version.my_env.verbosity_level > 10
    t0 = clock;
end

key = hash(sprintf('%s_%s_%d', fname, convs('safe_str', fargs), nargout), 'MD5');
try
    varargout = memory.get(key);
catch ME
    if isempty(memory) || memory.size() > 1000
        memory = options();
        varargout = cell(1, nargout);
        [varargout{:}] = persist_bufferize(fname, fargs);
        return;
    end
    if strcmp(ME.message, sprintf('key <%s> not available', key))
        varargout = cell(1, nargout);
        [varargout{:}] = feval(fname, fargs{:});
        memory.set(key, varargout);
    else
        rethrow(ME);
    end
end
if st_version.my_env.verbosity_level > 10
    st_log('persist_bufferize:fname=<%s> nb_keys=<%d> (%5.2f sec)\n', fname, memory.size(), etime(clock, t0));
end