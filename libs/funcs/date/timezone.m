function rslt = timezone(mode, varargin)
% timezone - permet de recuperer le temps (en numero de serie matlab) qui separe deux timezones,
% ou convertit des horaires d'une timezone en une autre
%
% Modes :
% - get_offset: Arguments -> initial_timezone and final_timezone
% returns offset between those to time-zones.
% - convert: Arguments -> initial_timezone, final_timezone and vector of dates
% returns dates offset according to mode <get_offset>
% - convert_stdata: Arguments -> final_timezone and an st_data (containing an info.td_info field)
% returns an st_data whose vecteur dates haven been offset according to mode <convert>
% back_to_gmt: Arguments -> an st_data
% returns the result of mode <convert_stdata> with final_timezone = GMT.
%
% Examples:
% datestr(timezone('convert','initial_timezone','America/New_York','final_timezone','Europe/Paris','dates',[datenum(2009,3,5,8,30,0),datenum(2009,3,12,8,30,0), datenum(2009,4,1,8,30,0)]))
% timezone('get_offset','initial_timezone','America/New_York','final_timezone','Europe/Paris','dates',[datenum(2009,3,5,8,30,0),datenum(2009,3,12,8,30,0), datenum(2009,4,1,8,30,0)]))
% data = read_dataset('btickdb', 'security', 'FTE.PA', 'from','28/03/2008', 'to', '04/04/2008');
%   data = timezone('back_to_gmt', 'data', data);
% % or
%   data.date = timezone('convert','initial_timezone','Europe/Paris','final_timezone','GMT','dates',data.date);
%
% author   : 'rburgot@keplercheuvreux.com'
% reviewer : 'sixin', 'nijos', 'malas'
% version  : '3'
% date     : '18/05/2010'
%
%
% See also trading_phase_definition

% TODO:
% - Finir de recoder tous les autres modes :
% + convert_stdata
% + back_to_gmt

opt = options_light({ 'initial_timezone','GMT','final_timezone','Europe/Paris',...
    'dates', [],'data',[]}, varargin);

switch mode
    case 'get_offset'
        data = opt.data;
        if st_data('isempty-nl', data)
            rslt = GET_OFFSET_DATENUM(opt.dates, opt.initial_timezone, opt.final_timezone);
        else
            rslt = GET_OFFSET_DATENUM(data.date, 'GMT', data.info.td_info(1).timezone);
        end
    case 'convert' % INLINE : get_offset
        % Le but etait initialement de convertir des horaires gmt vers les
        % horaires locaux
        rslt = opt.data;
        if st_data('isempty-nl', rslt)
            dts = opt.dates;
            if ~isempty(dts)
                rslt = dts + GET_OFFSET_DATENUM(dts, opt.initial_timezone, opt.final_timezone);
            else
                rslt = [];
            end
        elseif ~isfield(rslt, 'info') || ~isfield(rslt.info, 'td_info')
            % TODO est-ce vraiment ce qu'il faut faire?
            % on ne convertit pas les horaires des donn?es d'indice....
        else
            %             if rslt.info.td_info(1).trading_destination_id == 53 && rslt.info.td_info(1).place_id <= 0 % BEQUILLE : corriger zone horaire CHIX et retirer ?a/LOndres
            %                 rslt.date = rslt.date + GET_OFFSET_DATENUM(rslt.date, 0, 18);% BEQUILLE
            %             elseif any(rslt.info.td_info(1).trading_destination_id==[52 61] ) && rslt.info.td_info(1).place_id <= 0 % BEQUILLE : corriger zone horaire CHIX et retirer ?a/Paris Frankfort
            %                 rslt.date = rslt.date + GET_OFFSET_DATENUM(rslt.date, 0, 1);% BEQUILLE
            %             else% BEQUILLE
            rslt.date = rslt.date + GET_OFFSET_DATENUM(rslt.date,'GMT',rslt.info.td_info(1).final_timezone); % magic number : 0 = GMT par d?faut; magic number : {1, } pourquoi? bah pasque si ya plusieurs trading destination appartenant ? des timezones diff?rentes, il faut bien en choisir une de toute fa?on, esp?rons au moins que la premi?re corresponde au march? principal; % magic number {, 2} pour avoir le place_id
            %             end% BEQUILLE
        end
        
        
        
    case 'convert_stdata'
        
        % Aims : transform input st_data into final_place_id'
        % Inputs : use 'data' and 'final_place_id'
        % Handle old version : "localtime" + "td_info"
        %       and new version : "local_place_timezone_id" and "place_timezone_id"
        % init_place_id : will be equal to data.info.place_timezone_id
        % final_place_id : numeric, or 'local'
        
        % TO DO !
        % check if there is 'security_id' in columns, if several security..then can't use td?_info...localtime==0...
        
        % !!!!!!!!!!!! BEWARE !!!!!!!!
        % If the st_data .date contains the numeric date part, time precision could be altered !!!
        % If you care at microseconds...you have to work with a st_data with hours part in .date and day part in a colnames "day.date with only hours !!!!
        % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
        
        % ------------------------- PARAMS
        [rslt, b] = st_data('isempty',opt.data);
        assert(~b);
        
        final_timezone = opt.final_timezone;
        colnames2offset = {};
        
        % ------------------------ CHECK
        new_version=isfield(rslt, 'info') && ...
            isfield(rslt.info, 'localtime') && ...
            isfield(rslt.info, 'place_timezone_id') && ...
            isfield(rslt.info, 'local_place_timezone_id');
        
        old_version=isfield(rslt, 'info') && ...
            isfield(rslt.info, 'localtime') && ...
            isfield(rslt.info, 'td_info');
        
        assert(new_version|old_version,'timezone:convert_stdata','Cannot be converted, bad inputs in .info')
        assert(~isnumeric(final_timezone)&&~strcmp(final_timezone,'local'),'timezone:convert_stdata', 'final_place_id MUST be a numeric value or "local"');
        
        %---------------------- COMPUTE
        % -- Find initial timezone
        if new_version
            local_timezone = ...
                get_repository('place_id2timezone',rslt.info.local_place_timezone_id);
            init_timezone = ...
                get_repository('place_id2timezone',rslt.info.place_timezone_id);
            rslt.info = rmfield(rslt.info,{'local_place_timezone_id','place_timezone_id'});
        elseif old_version
            % If localtime is 0 then date are in GMT...
            local_timezone = rslt.info.td_info(1).timezone;
            if rslt.info.localtime == 0
                init_timezone = 'GMT';
            else
                init_timezone = local_timezone;
            end
        end
        % -- Find final timezone
        if ~isnumeric(final_timezone) && strcmp(final_timezone,'local')
            final_timezone = local_timezone;
        end
        
        % -- Transform time
        if ~strcmp(init_timezone,final_timezone)
            
            if all(rslt.date<=1)
                if ismember({'day'},rslt.colnames)
                    day_=st_data('col',rslt,'day');
                    offset_date=get_offset_from_date(rslt.date+day_,init_timezone,final_timezone);
                    rslt.date = rslt.date + offset_date;
                    % the day part may have been modified
                    rslt.value(:,strcmp(rslt.colnames,'day'))=floor(day_+rslt.date);
                elseif isfield(rslt.info, 'data_datestamp')
                    day_=rslt.info.data_datestamp;
                    offset_date=get_offset_from_date(rslt.date+day_,init_timezone,final_timezone);
                    rslt.date = rslt.date + offset_date;
                    % the day part may have been modified
                    if length(unique(floor(rslt.date+ day_)))>1
                        error('timezone:convert_stdata', '"day" part has several, this can not be handled in .info, you should add a colnames "day"');
                    end
                    rslt.info.data_datestamp=floor(day_+rslt.date(1));
                else
                    error('timezone:convert_stdata', '.date has no day part + no "day" colnames + no .info.data_datestamp');
                end
            else
                offset_date=get_offset_from_date(rslt.date,init_timezone,final_timezone);
                rslt.date = rslt.date + offset_date;
            end
            
            % we can also apply the date offset at some colnames !
            if any(abs(offset_date)>0)
                
                if isfield(rslt.info, 'timezone_colnames2offset')
                    colnames2offset=rslt.info.timezone_colnames2offset;
                end
                
                if ~iscellstr(colnames2offset)
                    error('timezone:convert_stdata', 'input "colnames2offset" has to be a cell of strings');
                end
                
                id=ismember(rslt.colnames,colnames2offset);
                
                if any(id)
                    rslt.value(:,id)=rslt.value(:,id)+repmat(offset_date,1,sum(id));
                end
                
            end
        end
        
        % --transform output
        rslt.info.localtime = strcmp(local_timezone,final_timezone);
        rslt.info.timezone = final_timezone;
        rslt.info.local_timezone = local_timezone;
        
        
    case 'back_to_gmt'  % INLINE : get_offset
        rslt = opt.data;
        if st_data('isempty-nl', rslt)
            dts = opt.dates;
            if ~isempty(dts)
                rslt = dts + GET_OFFSET_DATENUM(dts,opt.initial_timezone,'GMT'); % magic number : 0 = GMT par d?faut;
            else
                rslt = [];
            end
        else
            rslt.date = rslt.date + GET_OFFSET_DATENUM(rslt.date, rslt.info.td_info(1).timezone,'GMT'); % magic number : 0 = GMT par d?faut; magic number : {1, } pourquoi? bah pasque si ya plusieurs trading destination appartenant ? des timezones diff?rentes, il faut bien en choisir une de toute fa?on, esp?rons au moins que la premi?re corresponde au march? principal; % magic number {, 2} pour avoir le place_id
        end
    otherwise
        error('timezone:exec', 'unknown mode <%s>', mode);
end
end

function dts = GET_OFFSET_DATENUM(dts, initial_timezone, final_timezone)
[days, ~, idxes_d] = unique(floor(dts));
if length(days) > 1    
    offsets_d_daten = arrayfun(@(d)GET_OFFSET_HOURS( initial_timezone, final_timezone,d), ...
        days, 'uni', true) * datenum(0,0,0,0,0,1);
else
    offsets_d_daten = GET_OFFSET_HOURS(initial_timezone, final_timezone,days)* datenum(0,0,0,0,0,1);
end
dts = offsets_d_daten(idxes_d);
end


function offset = get_offset_from_date(dts, initial_timezone, final_timezone)
% P�riode de validit� [BEGINDATE, ENDDATE[
date_range = [min(floor(dts)), max(floor(dts))];

if strcmp(initial_timezone, 'GMT')
    o_ini = zeros(length(dts), 1);
else
    offset_init = exec_sql('KGR',...
        sprintf(['select BEGINDATE, ENDDATE, OFFSET from KGR..TIMEZONE'...
        ' where BEGINDATE <= ''%s'' and isnull(ENDDATE, ''3000-01-01'') > ''%s''',...
        ' and TIMEZONE = ''%s'' order by BEGINDATE'],...
        datestr(date_range(2), 'yyyymmdd'),...
        datestr(date_range(1), 'yyyymmdd'), initial_timezone));
    idx_ini = index_from_grid(floor(dts),...
        [datenum(offset_init(:, 1), 'yyyy-mm-dd')',...
        datenum(offset_init(end, 2), 'yyyy-mm-dd')]);
    o_ini = cell2mat(offset_init(idx_ini, 3));
end

if strcmp(final_timezone, 'GMT')
    o_fin = zeros(length(dts), 1);
else
    offset_final = exec_sql('KGR',...
        sprintf(['select BEGINDATE, ENDDATE, OFFSET from KGR..TIMEZONE'...
        ' where BEGINDATE <= ''%s'' and isnull(ENDDATE, ''3000-01-01'') > ''%s''',...
        ' and TIMEZONE = ''%s'' order by BEGINDATE'],...
        datestr(date_range(2), 'yyyymmdd'),...
        datestr(date_range(1), 'yyyymmdd'), final_timezone));
    idx_fin = index_from_grid(floor(dts),...
        [datenum(offset_final(:, 1), 'yyyy-mm-dd')',...
        datenum(offset_final(end, 2), 'yyyy-mm-dd')]);
    o_fin = cell2mat(offset_final(idx_fin, 3));
end
offset = (o_fin - o_ini)*datenum(0,0,0,0,0,1);
end