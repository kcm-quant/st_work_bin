function [rslt, main_td] = trading_time(tdinfo, date4hours)
% trading_time - permet de connaitre les horaires qui d�finissent un
%       journ�e de trading
%
% trading_time(get_repository('tdinfo', 110), datenum(2010,1,1,0,0,0))
%
% author   : 'rburgot@cheuvreux.com'
% reviewer : ''
% version  : '1.2'
% date     : '20/08/2008'
%
% CONTIENT DES BEQUILLES
%
% tests pour le passage aux horaires contextualis�s
% 
% 
% tdinfo = struct('trading_destination_id', 6, 'quotation_group', 'MB1');
% trdt = trading_time(tdinfo, now);
% datestr(trdt.opening)
% tdinfo = struct('trading_destination_id', 6, 'quotation_group', 'null');
% trdt = trading_time(tdinfo, now);
% datestr(trdt.opening)
% tdinfo = struct('trading_destination_id', 6, 'quotation_group', 'MB1');
% trdt = trading_time(tdinfo, datenum(2008,1,1,0,0,0));
% datestr(trdt.opening)
% tdinfo = struct('trading_destination_id', 6, 'quotation_group', 'null');
% trdt = trading_time(tdinfo, datenum(2008,1,1,0,0,0));
% datestr(trdt.opening)
%
%
% tdinfo = struct('trading_destination_id', 1, 'quotation_group', 'PA');
% trdt = trading_time(tdinfo, now);
% datestr(trdt.closing_fixing)
% trdt = trading_time(tdinfo, datenum(2004,1,1,0,0,0));
% datestr(trdt.closing_fixing)
%
% See also accum_data

global st_version

 if ischar(date4hours)
    date4hours = datenum(date4hours, 'dd/mm/yyyy');
end

colnames = {'opening_auction', 'opening_fixing', 'opening', 'intraday_stop_auction', ...
    'intraday_stop_fixing', 'intraday_stop', 'intraday_resumption_auction', ...
    'intraday_resumption_fixing', 'intraday_resumption', 'closing_auction', ...
    'closing_fixing', 'closing', 'post_opening', 'post_closing'};
str_cols = sprintf('%s, ', colnames{:});
str_cols(end-1:end) = '  ';

% < BEQUILLE : TODO : mieux, trouver le vrai march� primaire, un jour...
if isempty( tdinfo)
    error('trading_time:td', 'NODATA_TD: trading destination info and tick data do not seem to be compatible');
end
tdinfo = tdinfo(1);
main_td = tdinfo.trading_destination_id;
% >


	


my_req = sprintf(['select top 1 convert(varchar, end_date, 113), %s ' ...
    ' from %s..trading_hours_master where trading_destination_id = %d and ' ...
    ' quotation_group = %%s and context_id is null and ' ...
    ' ''%s'' <= isnull(end_date, getdate()) order by isnull(end_date, getdate())'], ...
    str_cols, st_version.bases.repository, main_td, datestr(date4hours, 'yyyymmdd'));


if strcmp('null', tdinfo.quotation_group)
    request_rslt = exec_sql('BSIRIUS',sprintf(my_req, 'null'));
elseif ~isempty(strmatch(tdinfo.quotation_group,{'qg_FCHI', 'qg_GDAXI', 'qg_MIB30', 'qg_AEX', 'qg_SSMI', 'qg_FTSE'}, 'exact'))
       request_rslt = { 'null', 'null', 'null', '09:00:00', 'null', 'null', 'null', 'null', ...% BEQUILLE : � retirer
        'null', 'null', 'null', 'null', '22:00:00', 'null', 'null'};% BEQUILLE : � retirer
else
    request_rslt = exec_sql('BSIRIUS',sprintf(my_req, ['''' tdinfo.quotation_group '''']));
    if isempty( request_rslt)
        request_rslt = exec_sql('BSIRIUS',sprintf(my_req, 'null'));
    end
end

if isempty( request_rslt)
    error('trading_time:exec', 'Unable to find any trading hours for trading_destination = <%d> and quotation_group = <%s>', ...
        tdinfo.trading_destination_id, tdinfo.quotation_group);
end

request_rslt(:, 1) = []; % la date s�lectionn�e en base n'est l� que pour aider � debugger

% mise en forme des donn�es
rslt = cellfun(@safe_datenum, ...
    request_rslt, ...
    'uni', false);

tmp = arrayfun(@(i){colnames{i},rslt(:,i)},1:length(colnames), 'uni', false);
tmp = [tmp{:}];
rslt = struct(tmp{:});

% if tdinfo.trading_destination_id == 18
%     rslt.closing_auction = datenum(0,0,0,17,20,0);
% end

% pas d'horaires d'auction pr�sentes pour NYSE
if main_td == 25
%     rslt.opening = datenum(0,0,0,5,30,0); % cela doit d�pendre de timezone...
%     rslt.closing = datenum(0,0,0,12,0,0);
    rslt.opening_auction = rslt.opening;
    rslt.closing_auction = rslt.closing;
end

function d = safe_datenum(str)
if strcmp(str, 'null')
    d = NaN;
else
    d = mod(datenum(str, 'HH:MM:SS'), 1);
end