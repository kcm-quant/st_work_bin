function [phase_th, phase_id, trading_hours_base_dump, ...
    from_phase_id_to_phase_name, extract_phase, change_dates, PHASE_TYPE, ...
    plot_phase_th, th_event] = ...
    trading_time_interpret(tdinfo, date_, varargin)
% trading_time_interpret - permet de connaitre les horaires qui d�finissent un
%       journ�e de trading, on doit aussi pouvoir obtenir une grille de
%       temps qui soit la m�me sur toute une p�riode et quel que soient les
%       horaires de trading
%
% [phase_th, phase_id, trading_hours_base_dump, from_phase_id_to_phase_name, extract_phase] = ...
%       trading_time_interpret(get_repository('tdinfo', 110), datenum(2010,1,1,0,0,0));
%
% [phase_th, phase_id, trading_hours_base_dump, from_phase_id_to_phase_name, extract_phase] = ...
%       trading_time_interpret(get_repository('tdinfo', 10748), datenum(2010,1,1,0,0,0));
% 
% phase_th
% phase_id
% trading_hours_base_dump
% from_phase_id_to_phase_name(0:6)
% extract_phase(phase_th, {'CONTINUOUS'})
%
% ex d'utilisation dans make_grid : 
% [phase_th,phase_id,trading_hours_base_dump] = ...
%             trading_time_interpret(data.info.td_info, ...
%                                     floor(data.date(1)), 'force_unique_intraday_auction', fuia);
%         
%         CONT_PHASE      = phase_id.CONTINUOUS;
%         OPEN_FIX_PHASE  = phase_id.OPEN_FIXING;
%         ID_FIX_PHASE1   = phase_id.ID_FIXING1;
%         ID_FIX_PHASE2   = phase_id.ID_FIXING2;
%         CLOSE_FIX_PHASE = phase_id.CLOSE_FIXING;
%         VOL_FIX_PHASE   = phase_id.VOL_FIXING;
%
% astuce, pure aide � l'interpr�tation et r�cup�ration des id des phase : 
% ex d'utilisation dans formula factory : 
% [no_use,phase_id,no_use,from_phase_id_to_phase_name] = trading_time_interpret([],[]);
%
% Example :
% 
% [phase_th, phase_id, trading_hours_base_dump, ...
%     from_phase_id_to_phase_name, extract_phase, change_dates] = ...
%     trading_time_interpret(get_repository('tdinfo', 366), '05/05/2011', 'continuous_duration_tolerance', datenum(0,0,0,0,2,0))
% % change_dates will be the list of dates when there has been a change in
% % begining or the end of the continuous phase that has been greater than 'continuous_duration_tolerance'
%
%
% [phase_th, phase_id, trading_hours_base_dump, ...
%     from_phase_id_to_phase_name, extract_phase, change_dates] = ...
%     trading_time_interpret(get_repository('tdinfo', 456339), '05/05/2011', 'continuous_duration_tolerance', datenum(0,0,0,0,2,0))
% % brazil change its trading time between summer and winter to be closer to NY time
%
% % Un exemple de demi journ�e
% [phase_th, phase_id, trading_hours_base_dump, ...
%     from_phase_id_to_phase_name, extract_phase, change_dates] = ...
%     trading_time_interpret(get_repository('tdinfo', 110), '23/12/2010', ...
%     'context_selection', 'contextualised');
% p = extract_phase(phase_th, {'CONTINUOUS'});[datestr(p.begin, 'HH:MM:SS') ' -> ' datestr(p.end, 'HH:MM:SS')] 
% [phase_th, phase_id, trading_hours_base_dump, ...
%     from_phase_id_to_phase_name, extract_phase, change_dates] = ...
%     trading_time_interpret(get_repository('tdinfo', 110), '24/12/2010', ...
%     'context_selection', 'contextualised');
% p = extract_phase(phase_th, {'CONTINUOUS'});[datestr(p.begin, 'HH:MM:SS') ' -> ' datestr(p.end, 'HH:MM:SS')] 
% 
% % Un exemple d'ajout de fixing intraday
% phase_th = trading_time_interpret(get_repository('tdinfo', get_repository('ric2sec_id', {'VOD.L'})), '18/08/2011', 'context_selection', 'contextualised');
% {phase_th.phase_name}'
% phase_th = trading_time_interpret(get_repository('tdinfo', get_repository('ric2sec_id', {'VOD.L'})), '19/08/2011', 'context_selection', 'contextualised');%     'context_selection', 'contextualised');
% {phase_th.phase_name}'
%[phase_th, phase_id, trading_hours_base_dump, ...
%   from_phase_id_to_phase_name, extract_phase, change_dates, plot_phase_th] = ...
%   trading_time_interpret(get_repository('tdinfo', 305972), '05/05/2011', 'continuous_duration_tolerance', datenum(0,0,0,0,2,0), 'context_selection', 'span_all')
% plot_phase_th(phase_th)
%
% % Begin end matrix for markets havin a pause :
% [phase_th, ~, ~, ~, ~, ~, ~, plot_phase_th] = trading_time_interpret(get_repository('tdinfo', 6069), '05/05/2011');
% plot_phase_th(phase_th)
% [phase_th, ~, ~, ~, ~, ~, ~, plot_phase_th] = trading_time_interpret(get_repository('tdinfo', 290637), '01/04/2012');
% plot_phase_th(phase_th)
%
% See also : formula_factory, formula_compute_auction
% 
% author   : 'rburgot@cheuvreux.com'
% reviewer : 'athabault@cheuvreux.com'
% version  : '1.3'
% date     : '15/12/2010'

v = version(); % pour g�rer le legacy de unique

PHASE_TYPE = struct('CONTINUOUS', 0, 'FIXING', 1, 'PERIODIC_FIXING', 2, 'AT_LAST', 3, 'AFTER_HOURS', 4, 'CLOSED', 5);
PHASES_NAME = {'CONTINUOUS', 'SECOND_CONTINUOUS', 'OPEN_FIXING', ...
        'ID_FIXING1', 'ID_FIXING2', 'CLOSE_FIXING', 'VOL_FIXING', ...
        'TRADING_AT_LAST', 'TRADING_AFTER_HOURS', 'MARKET_CLOSED',...
        'PERIODIC_AUCTION'};
PHASE_NUM = num2cell(0:(length(PHASES_NAME)-1));
tmp = cat(1, PHASES_NAME, PHASE_NUM);
phase_id = struct(tmp{:});

if nargout > 3
    from_phase_id_to_phase_name = @(id)fpitpn(id, PHASES_NAME);
end
if nargout > 4
    extract_phase = @ep;
end

if isempty(tdinfo)
    phase_th = [];
    trading_hours_base_dump = [];
    change_dates= []; 
    return;
end

% < Options Management
opt  = options_light({'context_selection', 'null', ... % ['null']/'contextualised'/'span_all' should we consider the specific hours for some special days? 'null' => No, just ordinary trading time but taking care about trading time changes / 'contextualised' => Yes, each day may have specific trading times/ 'span_all' : all the existing phase in any context will be defined for each days, for each phase, the minimum (resp max) of the beginings (resp ends) over each context would be used as the begining (resp end) of the phase, fixing hours will be taken from the null context
    'force_unique_intraday_auction', true, ... % [true]/false to know wether you want to consider there is only one intraday fixing
    'continuous_duration_tolerance', NaN, ... % [NaN]/numeric matlab time : is a tolerance on the end and begin of the continuous phase hour above which a change intrading time will be considered
    }, varargin);
fuia = opt.force_unique_intraday_auction;
cdt  = opt.continuous_duration_tolerance;
% >

global st_version
persistent trdt_memory

plot_phase_th = @plot_phase_th_;
    function plot_phase_th_(pth)
        for kk = 1 : length(pth)
            if size(pth(kk).begin_end, 1) == 1
                st_log('phase <%s> from <%s> to <%s>\n', pth(kk).phase_name, ...
                    datestr(round(pth(kk).begin*(24*60))/(24*60), 'HH:MM'), ...
                    datestr(round(pth(kk).end*(24*60))/(24*60), 'HH:MM') );
            else
                st_log('phase <%s> from <%s> to <%s>, then from <%s> to <%s>\n', pth(kk).phase_name, ...
                    datestr(round(pth(kk).begin_end(1, 1)*(24*60))/(24*60), 'HH:MM'), ...
                    datestr(round(pth(kk).begin_end(1, 2)*(24*60))/(24*60), 'HH:MM'), ...
                    datestr(round(pth(kk).begin_end(2, 1)*(24*60))/(24*60), 'HH:MM'), ...
                    datestr(round(pth(kk).begin_end(2, 2)*(24*60))/(24*60), 'HH:MM') );
            end
        end
    end
if ischar(date_)
    date_ = datenum(date_, 'dd/mm/yyyy');
end

%< TODO grilles sur plusieurs destinations
tdid = tdinfo(1).trading_destination_id;
quotation_group = tdinfo(1).quotation_group;
quotation_group_str4req = quotation_group;
if ~strcmp(quotation_group_str4req, 'null')
    quotation_group_str4req = ['''' quotation_group_str4req ''''];
end

%< TODO better then MAGIC NUMBERS  : this should be dependent on the destination, as the continuous phase on taipei, shenzen, for instance is not very continuous, TODO a dictionnary
if ~ismember(tdid, [22 118]) % MAGIC NUMBERS td_id taipei and shenzen
%     PHASES_NAME = {'CONTINUOUS', 'SECOND_CONTINUOUS', 'OPEN_FIXING', ...
%         'ID_FIXING1', 'ID_FIXING2', 'CLOSE_FIXING', 'VOL_FIXING', ...
%         'TRADING_AT_LAST', 'TRADING_AFTER_HOURS', 'MARKET_CLOSED'};
    PHASE_TYPE_ID = [0 0 1 1 1 1 1 3 4 5];
else
    PHASE_TYPE_ID = [2 2 1 1 1 1 1 3 4 5];
end
% PHASE_TYPE = struct('CONTINUOUS', 0, 'FIXING', 1, 'PERIODIC_FIXING', 2, 'AT_LAST', 3, 'AFTER_HOURS', 4, 'CLOSED', 5);
%>

if ~isempty(trdt_memory) ...
        && trdt_memory.trading_destination_id == tdid ...
        && strcmp(quotation_group, trdt_memory.quotation_group) ...
        && strcmp(opt.context_selection, trdt_memory.context_selection)
    null_context_dump = trdt_memory.null_context_dump;
    other_context_dump = trdt_memory.other_context_dump;
    ocd_woe = trdt_memory.ocd_woe;
else
    null_context_dump = generic_structure_request('KGR', sprintf(['select * from %s..trading_hours_master ' ...
        ' where trading_destination_id = %d and context_id is null and quotation_group = ' quotation_group_str4req], st_version.bases.repository, tdid));
    if isempty(null_context_dump)
        null_context_dump = generic_structure_request('KGR', sprintf(['select * from %s..trading_hours_master ' ...
            ' where trading_destination_id = %d and context_id is null and quotation_group is null'], st_version.bases.repository, tdid));
        if isempty(null_context_dump)
            error('trading_time_interpret:check_args', ...
                'REPOSITORY: No trading hours for td <%d> and quotation_group <%s>', ...
                tdid, quotation_group_str4req);
        end
    end

    if ~strcmp(opt.context_selection, 'null') || nargout > 8
        other_context_dump = generic_structure_request('KGR', sprintf([
            ' select ' ...
            ' 	734504+datediff(day, ''20110101'', cebg.EVENTDATE) as "event_date", ' ...
            ' 	734504+datediff(day, ''20110101'', isnull(thm.end_date, ''22000101'')) as "end_datenum", ' ...
            '   cet.SHORTNAME as "short_name", ' ...
            ' 	thm.* ' ...
            ' from  ' ...
            '   ',st_version.bases.repository,'..CALEVENTTYPE cet, '...
            ' 	',st_version.bases.repository,'..CALEVENTBYGROUP cebg,  ' ...
            ' 	',st_version.bases.repository,'..quotation_group qg,   ' ...
            ' 	',st_version.bases.repository,'..trading_hours_master thm ' ...
            ' where  ' ...
            ' 	cebg.QUOTATIONGROUPID = qg.quotation_group_id ' ...
            ' 	 ' ...
            '   and cet.EVENTTYPE = cebg.EVENTTYPE ' ...
            ' 	 ' ...
            ' 	and thm.quotation_group = qg.quotation_group ' ...
            ' 	and thm.trading_destination_id = qg.trading_destination_id ' ...
            ' 	and thm.context_id = cebg.EVENTTYPE ' ...
            ' 	and cebg.EVENTDATE <= isnull(thm.end_date, ''22000101'') ' ... % MAGIC NUMBER : 2200 is the end of the world + this condition is too weak , we'll have to filter after that
            ' 	 ' ...
            ' 	and qg.trading_destination_id = %d ' ...
            ' 	and qg.quotation_group = %s order by event_date, end_datenum'], round(tdid), quotation_group_str4req));
        % All contexts but without taking events into account
        ocd_woe = generic_structure_request('KGR', sprintf([
            ' select ' ...
            ' 	734504+datediff(day, ''20110101'', isnull(thm.end_date, ''22000101'')) as "end_datenum", '...
            '   thm.*, isnull(thm.context_id, 0) as "no_nan_context" ' ...
            ' from  ' ...
            ' 	',st_version.bases.repository,'..trading_hours_master thm ' ...
            ' where  ' ...
            ' 	thm.trading_destination_id = %d ' ...
            ' 	and thm.quotation_group = %s order by isnull(thm.context_id, 0), end_datenum '], round(tdid), quotation_group_str4req));
    else
        ocd_woe = [];
        other_context_dump = [];
    end
    hoc  = (~isempty(ocd_woe));
    heac = (~isempty(other_context_dump));
    if heac
        if strtok(v, '.') > '7' %v(1)
            [~, idx_filter, ~] = unique([other_context_dump.event_date other_context_dump.context_id], 'rows', 'first', 'legacy');
        else
            [~, idx_filter, ~] = unique([other_context_dump.event_date other_context_dump.context_id], 'rows', 'first');
        end
        other_context_dump = sub_struct(other_context_dump, idx_filter);
    end
    if hoc
        if strtok(v, '.') > '7' %v(1)
            [~, idx_filter, ~] = unique([ocd_woe.context_id ocd_woe.end_datenum], 'rows', 'first', 'legacy');
        else
            [~, idx_filter, ~] = unique([ocd_woe.context_id ocd_woe.end_datenum], 'rows', 'first');
        end
        ocd_woe = sub_struct(ocd_woe, idx_filter);
    end
    % < * just a little bit of preprocessing data format
    fns = fieldnames(null_context_dump);
    
    tmp_fns = setdiff(fns, {'end_date', 'quotation_group', 'trading_destination_id', 'context_id', 'default_context', 'end_datenum', 'event_date', 'event_name'});
    for i = 1 : length(tmp_fns)
        for j = 1 : length(null_context_dump.(tmp_fns{i}))
            null_context_dump.(tmp_fns{i}){j} = safe_datenum(null_context_dump.(tmp_fns{i}){j}, 'HH:MM:SS', NaN, @(x)mod(x, 1));
        end
        null_context_dump.(tmp_fns{i}) = cell2mat(null_context_dump.(tmp_fns{i}));
        if heac
            for j = 1 : length(other_context_dump.(tmp_fns{i}))
                other_context_dump.(tmp_fns{i}){j} = safe_datenum(other_context_dump.(tmp_fns{i}){j}, 'HH:MM:SS', NaN, @(x)mod(x, 1));
            end
            other_context_dump.(tmp_fns{i}) = cell2mat(other_context_dump.(tmp_fns{i}));
        end
        if hoc
            for j = 1 : length(ocd_woe.(tmp_fns{i}))
                ocd_woe.(tmp_fns{i}){j} = safe_datenum(ocd_woe.(tmp_fns{i}){j}, 'HH:MM:SS', NaN, @(x)mod(x, 1));
            end
            ocd_woe.(tmp_fns{i}) = cell2mat(ocd_woe.(tmp_fns{i}));
        end
    end
    for j = 1 : length(null_context_dump.end_date)
        null_context_dump.end_date{j} = safe_datenum(null_context_dump.end_date{j}, 'yyyy-mm-dd', Inf);
    end
    null_context_dump.end_date = cell2mat(null_context_dump.end_date);
    %< The equivalent of this for other_context would be the filter talked
    %about lines 150 to 154
    [~, idx] = sort(null_context_dump.end_date);
    for i = 1 : length(fns)
         null_context_dump.(fns{i}) = null_context_dump.(fns{i})(idx);
    end
    %>
    % > *
    trdt_memory = struct('trading_destination_id', tdid, ...
        'quotation_group', quotation_group, ...
        'null_context_dump', null_context_dump, ...
        'other_context_dump', other_context_dump, ...
        'context_selection', opt.context_selection, ...
        'ocd_woe', ocd_woe);
end

%< ditionnaire d'interpr�tation des horaires (num_phase, cell des champs pour pr�sence si any non null, d�but phase, fin phase)
if ~fuia %force_unique_intraday_auction
    dico_trdt = {phase_id.CONTINUOUS, {'opening'}, {'opening'}, {'closing_auction', 'closing'};...
        phase_id.OPEN_FIXING, {'opening_auction', 'opening_fixing'}, {'opening_auction', 'opening_fixing'}, {'opening_fixing'};...
        phase_id.ID_FIXING1, {'intraday_stop_auction'}, {'intraday_stop_auction'}, {'intraday_stop_fixing'};...
        phase_id.ID_FIXING2, {'intraday_resumption_auction'}, {'intraday_resumption_auction'}, {'intraday_resumption_fixing'};...
        phase_id.CLOSE_FIXING, {'closing_auction','closing_fixing'}, {'closing_auction', 'closing_fixing'}, {'closing_fixing'};...
        ... phase_id.TRADING_AT_LAST, {'trading_at_last_opening', 'trading_at_last_closing'}, {'trading_at_last_opening'}, {'trading_at_last_closing'};...
        ... phase_id.TRADING_AFTER_HOURS, {'trading_after_hours_opening','trading_after_hours_closing'}, {'trading_after_hours_opening'}, {'trading_after_hours_closing'}
        };
else
    dico_trdt = {phase_id.CONTINUOUS, {'opening'}, {'opening'}, {'closing_auction', 'closing'};...
        phase_id.OPEN_FIXING, {'opening_auction', 'opening_fixing'}, {'opening_auction', 'opening_fixing'}, {'opening_fixing'};...
        phase_id.ID_FIXING2, {'intraday_stop_auction', 'intraday_resumption_auction'}, {'intraday_stop_auction', 'intraday_resumption_auction'}, {'intraday_stop_fixing', 'intraday_resumption_fixing'};...
        phase_id.CLOSE_FIXING, {'closing_auction','closing_fixing'}, {'closing_auction', 'closing_fixing'}, {'closing_fixing'};...
        ... phase_id.TRADING_AT_LAST, {'trading_at_last_opening', 'trading_at_last_closing'}, {'trading_at_last_opening'}, {'trading_at_last_closing'};...
        ... phase_id.TRADING_AFTER_HOURS, {'trading_after_hours_opening','trading_after_hours_closing'}, {'trading_after_hours_opening'}, {'trading_after_hours_closing'}
        };
end
    function phase_th_o = dump2phase_th(scd)
        phase_th_o = struct('phase_id', {}, 'phase_name', {}, 'begin', {}, 'end', {}, 'phase_type_id', {}, 'begin_end', []);
        for i_p = 1 : size(dico_trdt, 1)
            if isfinite(scd.(dico_trdt{i_p, 2}{1})) || isfinite(scd.(dico_trdt{i_p, 2}{end}))
                phase_th_o(end+1) = struct('phase_id', dico_trdt{i_p, 1}, ...
                    'phase_name', fpitpn(dico_trdt{i_p, 1}, PHASES_NAME), ...
                    'begin', isnull(scd.(dico_trdt{i_p, 3}{1}),...
                    scd.(dico_trdt{i_p, 3}{end})), ...
                    'end', isnull(scd.(dico_trdt{i_p, 4}{1}),...
                    scd.(dico_trdt{i_p, 4}{end})), ...
                    'phase_type_id', PHASE_TYPE_ID(dico_trdt{i_p, 1}+1), ...
                    'begin_end', []);
                phase_th_o(end).begin_end = [phase_th_o(end).begin phase_th_o(end).end];
            end
        end
        if ~strcmp(quotation_group_str4req, 'span_all')
            % For the continuous auction, we will change the matrix
            % begin_end in orde to make obvious the pause in trading
            [~, ~, idx_pause_continuous] = intersect([phase_id.ID_FIXING1 phase_id.ID_FIXING2], ...
                [phase_th_o.phase_id]);
            if ~isempty(idx_pause_continuous)
                idx_cont_phase = (phase_id.CONTINUOUS == [phase_th_o.phase_id]);
                if any(idx_cont_phase)
                    phase_th_o(idx_cont_phase).begin_end = repmat(phase_th_o(idx_cont_phase).begin_end, 2, 1);
                    phase_th_o(idx_cont_phase).begin_end(1, 2) = min([phase_th_o(idx_pause_continuous).begin scd.intraday_stop]);
                    phase_th_o(idx_cont_phase).begin_end(2, 1) = max([phase_th_o(idx_pause_continuous).end scd.intraday_resumption]);
                end
            end
        end
    end
%>

change_dates = [];
if ~isempty(null_context_dump.end_date)
    if isfinite(cdt) && length(null_context_dump.end_date) > 1
        % < if a tolerance for a change in trading time has been given then
        % here we'll havbe a look for dates which have continuous trading
        % time changes greater than this tolerance
        idx4c = find(cell2mat(dico_trdt(:, 1)) == phase_id.CONTINUOUS);
        if size(null_context_dump.end_date, 1) > 1
            dbe = NaN(size(null_context_dump, 1), 3); % date, begin of continuous trading, end of continuous trading
            for i = 1 : length(null_context_dump.end_date)
                dbe(i, :) = [null_context_dump.end_date(i), ...
                    isnull(null_context_dump.(dico_trdt{idx4c, 3}{1})(i), null_context_dump.(dico_trdt{idx4c, 3}{end})(i)), ...
                    isnull(null_context_dump.(dico_trdt{idx4c, 4}{1})(i), null_context_dump.(dico_trdt{idx4c, 4}{end})(i))];
            end
            change_dates = dbe([any(abs(diff(dbe(:, 2:end))) > cdt, 2); false], 1);
        end
        % >
    end
    if ~isempty(other_context_dump)
        idx_th_event_type = find(other_context_dump.event_date == date_);
    else
        idx_th_event_type = [];
    end
    if length(idx_th_event_type) > 1
        error('REPOSITORY: There can''t be two different trading_time for this day');
    elseif isempty(idx_th_event_type)
        th_event = struct('event_type', [], 'event_name', '');
    else
        th_event = struct('event_type', other_context_dump.context_id(idx_th_event_type), ...
            'event_name', other_context_dump.short_name(idx_th_event_type));
    end
    switch opt.context_selection
        case 'null'
            selected_context_dump = sub_struct(null_context_dump, find(date_<null_context_dump.end_date, 1, 'first'));
            phase_th = dump2phase_th(selected_context_dump);
        case 'contextualised'
            if strcmp(quotation_group_str4req, 'null')
                warning('trading_time_interpret:TODO', 'TODO: check with Thierry what should be done in this case, is this really working? dont think so, there is no event from a null quotation_group up to now');
            end
            if isempty(other_context_dump)
                selected_context_dump = sub_struct(null_context_dump, find(date_<null_context_dump.end_date, 1, 'first'));
            else
                if isempty(idx_th_event_type)
                    selected_context_dump = sub_struct(null_context_dump, find(date_<null_context_dump.end_date, 1, 'first'));
                elseif length(idx_th_event_type) > 1
                    error('trading_time_interpret:check_database', 'REPOSITORY: Two trading hours contexts for a given day is a problem');
                else
                    selected_context_dump = sub_struct(other_context_dump, idx_th_event_type);
                end
            end
            phase_th = dump2phase_th(selected_context_dump);
        case 'span_all'
            if isempty(ocd_woe)
                selected_context_dump = sub_struct(null_context_dump, find(date_<null_context_dump.end_date, 1, 'first'));
                phase_th = dump2phase_th(selected_context_dump);
            else
                phase_th = dump2phase_th(sub_struct(ocd_woe, find((date_<ocd_woe.end_datenum) ...
                    & (ocd_woe.no_nan_context == 0), 1, 'first'))); 
%                 plot_phase_th(phase_th)
                all_phases = [phase_th.phase_id];
                u_contexts = setdiff(unique(ocd_woe.no_nan_context), 0);
                for i = 1 : length(u_contexts) % ocd_woe.(tmp_fns{1})
                    tmp_phase_th = dump2phase_th(sub_struct(ocd_woe, find((date_<ocd_woe.end_datenum) ...
                    & (ocd_woe.no_nan_context == u_contexts(i)), 1, 'first'))); 
%                     plot_phase_th(tmp_phase_th)
                    this_context_phases = [tmp_phase_th.phase_id];
                    [~, id1, id2] = intersect(all_phases, this_context_phases);
                    for j = 1 : length(id1)
                        phase_th(id1(j)).end = max(phase_th(id1(j)).end,tmp_phase_th(id2(j)).end);
                        phase_th(id1(j)).begin = min(phase_th(id1(j)).begin,tmp_phase_th(id2(j)).begin);
                    end
                    [~, id2] = setdiff(this_context_phases, all_phases);
                    if ~isempty(id2)
                        phase_th = cat(2, phase_th, tmp_phase_th(id2));
                        all_phases = [phase_th.phase_id];
                    end
                end
%                 plot_phase_th(phase_th)
                selected_context_dump = struct('message', ['You can find you info in phase_th. ' ...
                    ' If you really need this variable, then you will have to implement ' ...
                    ' a fonction transforming bask from phase_th struct to dump data']);
            end
        otherwise
            error('trading_time_interpret:check_args', 'Unknown context_selection <%s>', context_selection);
    end
else
    error('trading_time_interpret:check_output', ...
        'REPOSITORY: No trading hours defined for trading_destination_id <%d> and quotation_group <%s> for date <%s>', ...
        tdid, quotation_group, datestr(date_, 'dd/mm/yyyy'));
end

if length(selected_context_dump) > 1
    error('trading_time_interpret:check_output', ...
        'Unexpected result : trading_time_interpret has more than one trading_hours_base_dump');
end

if nargout > 2
    trading_hours_base_dump = struct('selected_context_dump', selected_context_dump, ...
        'null_context_dump', trdt_memory.null_context_dump, ...
        'other_context_dump', trdt_memory.other_context_dump);
end

end

function d = safe_datenum(str, format, def_val, func)
if strcmp(str, 'null')
    d = def_val;
else
    d = datenum(str, format);
    if nargin > 3 && ~isempty(func)
        d = func(d);
    end
end
end

function phase_name = fpitpn(phase_id, PHASES_NAME)
    phase_name = PHASES_NAME(phase_id+1);
end

function phase_th = ep(phase_th, phase_names2be_extracted)
    phase_th(~ismember({phase_th.phase_name}, ...
        phase_names2be_extracted)) = [];
    if length(phase_th) ~= length(phase_names2be_extracted)
        missing_phase = setdiff(phase_names2be_extracted, {phase_th.phase_name});
        error('extract_phase:check_args', ...
            ['You are trying to extract a phase which is not supposed to exist.' ...
            ' Missing phases are : <%s>'], sprintf('%s/', missing_phase{:}));
    end
end

function ss = sub_struct(s, idx)
ss = s;
fns = fieldnames(s);
for i = 1 : length(fns)
    ss.(fns{i}) = ss.(fns{i})(idx);
end
end