function rslt = witching_days(mode, dts)
% WITCH_DAYS - permet de retrouver dans une liste de date ceux qui
%       correspondent aux �ch�ances de produits d�riv�s sur action
%       rslt = witch_days(mode, dts) dts est un vecteur de dates au format
%       datenum rslt est un vecteur qui fait la m�me taille et qui vaut
%       soit 0 s'il ne s'agit pas d'une date d'�ch�ance de future, soit 3
%       si c'est une �ch�ance de future semestrielle, soit 2 si c'est une
%       �ch�ance de future trimestrielle, soit 1 si c'est une �ch�ance de
%       future qui n'est pas trimestrielle (janvie f�vrier avril mai
%       juillet, etc)
% ex :
% dts = (datenum(2006, 1,1, 0,0,0) : datenum(2009, 1,1, 0,0,0))';
% wd = witching_days('FCE', dts);
% datestr(dts(wd~=0))
% datestr(dts(wd==2))
% datestr(dts(wd==3))
% % rendez-vous sur euronext pour v�rifier
%
% Attention cela ne fonctionne pas pour les ann�es ant�rieures � 2000
% (parceque je me dis que �a ne sert � rien)
%
% See also nweekdate is_busday
[n m] = size(dts);
if m > n
    dts = dts';
end
switch mode
    case {'FCE', 'CAC40'}
        % 17h45 = last trading time for FCE future and options both occurs on the third friday of each month. there are monthly, quarterly and semestrials expiries
        % 17h30 = last trading time for American style stock options (i mean option on a stock, not stock-options) also occurs on the third friday of each month. there are monthly, quarterly and semestrials expiries
        % 17h30 = last trading time for European style stock options also occurs on the third friday of each month. NO semestrials there are monthly, and quarterly expiries, but
        % si je donne ces dates et que je n'apporte pas l'information �
        % l'utilisateur de cette fonction c'est parceque j'ai trouv� des
        % informations contradictoires � ce sujet : 16h30 pour la premi�re,
        % et m�me 16h pour le future FCE ce dernier �tant ptet l'info la
        % plus r�cente...
        [y, m] = datevec(dts);
        uym = unique([y, m], 'rows');
%         w_d = nweekdate(3, 6, uym(:, 1), uym(:, 2));  %Doesn't work anymore without the financial toolbox
        w_d = third_fridays(uym(:, 1), uym(:, 2)); % on r�cup�re la date des 3 eme vendredi du mois
        %< On v�rifie que ce sont des businnes day, et sinon on les ajuste
        %en previous
        not_bus_day = ~is_busday('target',w_d);
        while any(not_bus_day) 
            w_d(not_bus_day) = w_d(not_bus_day) - 1;
            not_bus_day = ~is_busday('target',w_d);
        end
        %>
        rslt = ismember(floor(dts), w_d);
        rslt = rslt .* ( 1 + ~mod(m, 3) + ~mod(m, 6)); % on rajoute l'information �c�hance trimestriel et semestriel
    otherwise
        error('witching_days:exec', 'mode <%s> unknown', mode);
end
if m > n
    rslt = rslt';
end
end

function d_3f = third_fridays(y,m)
% THIRD_FRIDAYS date of third friday of the month m of year y *
%(between 01/01/2000 and 31/01/2040)

d_3f = NaN(length(y),1);

third_fridays_20000101_2040101 = ...
    [730506;730534;730562;730597;730625;730653;730688;730716;730744;730779;730807;730835;730870;730898;730926;730961;730989;731017;731052;731080;731115;731143;731171;731206;731234;731262;731290;731325;731353;731388;731416;731444;731479;731507;731535;731570;731598;731633;731661;731689;731717;731752;731780;731808;731843;731871;731906;731934;731962;731997;732025;732053;732088;732116;732144;732179;732207;732235;732270;732298;732333;732361;732389;732417;732452;732480;732508;732543;732571;732606;732634;732662;732697;732725;732753;732788;732816;732844;732879;732907;732935;732970;732998;733026;733061;733089;733117;733152;733180;733208;733243;733271;733306;733334;733362;733397;733425;733453;733488;733516;733544;733579;733607;733635;733670;733698;733733;733761;733789;733824;733852;733880;733908;733943;733971;734006;734034;734062;734097;734125;734153;734188;734216;734244;734279;734307;734335;734370;734398;734426;734461;734489;734524;734552;734580;734608;734643;734671;734699;734734;734762;734797;734825;734853;734888;734916;734944;734979;735007;735035;735070;735098;735133;735161;735189;735224;735252;735280;735308;735343;735371;735406;735434;735462;735497;735525;735553;735588;735616;735651;735679;735707;735735;735770;735798;735826;735861;735889;735924;735952;735980;736015;736043;736071;736099;736134;736162;736197;736225;736253;736288;736316;736344;736379;736407;736435;736470;736498;736526;736561;736589;736624;736652;736680;736715;736743;736771;736806;736834;736862;736897;736925;736953;736988;737016;737044;737079;737107;737135;737170;737198;737226;737261;737289;737324;737352;737380;737415;737443;737471;737499;737534;737562;737597;737625;737653;737688;737716;737744;737779;737807;737842;737870;737898;737926;737961;737989;738024;738052;738080;738115;738143;738171;738206;738234;738262;738297;738325;738353;738388;738416;738444;738479;738507;738542;738570;738598;738626;738661;738689;738717;738752;738780;738815;738843;738871;738906;738934;738962;738997;739025;739053;739088;739116;739144;739179;739207;739235;739270;739298;739326;739361;739389;739424;739452;739480;739515;739543;739571;739606;739634;739669;739697;739725;739753;739788;739816;739844;739879;739907;739942;739970;739998;740033;740061;740089;740117;740152;740180;740215;740243;740271;740306;740334;740362;740397;740425;740453;740488;740516;740544;740579;740607;740635;740670;740698;740733;740761;740789;740824;740852;740880;740915;740943;740971;741006;741034;741062;741097;741125;741153;741188;741216;741244;741279;741307;741342;741370;741398;741433;741461;741489;741517;741552;741580;741615;741643;741671;741706;741734;741762;741797;741825;741860;741888;741916;741944;741979;742007;742035;742070;742098;742133;742161;742189;742224;742252;742280;742315;742343;742371;742406;742434;742462;742497;742525;742560;742588;742616;742644;742679;742707;742735;742770;742798;742833;742861;742889;742924;742952;742980;743015;743043;743071;743106;743134;743162;743197;743225;743253;743288;743316;743344;743379;743407;743435;743470;743498;743533;743561;743589;743624;743652;743680;743715;743743;743771;743806;743834;743862;743897;743925;743960;743988;744016;744051;744079;744107;744135;744170;744198;744233;744261;744289;744324;744352;744380;744415;744443;744471;744506;744534;744562;744597;744625;744653;744688;744716;744751;744779;744807;744835;744870;744898;744926;744961;744989;745024;745052;745080;745115];

[y_3f, m_3f] = datevec(third_fridays_20000101_2040101);
for i = 1:length(y)
    d_3f(i) = third_fridays_20000101_2040101(y_3f==y(i) & m_3f==m(i));
end

end

% function d = nweekdate(n,wkd,y,m,g)
% %NWEEKDATE Date of specific occurrence of weekday in month.
% %   D = NWEEKDATE(N,WKD,Y,M,G) returns the serial date for a specific 
% %   occurrence of a given weekday, in a given year and month.
% %   N is the nth occurrence of the desired weekday, (an integer from 1 to 5),
% %   WKD is the weekday, (1 through 7 equal Sunday through Saturday), Y is the
% %   year, M is the month, and G is an optional specification of another weekday 
% %   that must fall in the same week as WKD. The default value is G = 0.
% %  
% %   For example, to find the first Thursday in May 1997:
% %
% %   d = nweekdate(1,5,1997,5) returns d = 729511 which is
% %       the serial date corresponding to May 01, 1997. 
% %   
% %   See also FBUSDATE, LBUSDATE, LWEEKDATE.
%  
% %   Copyright 1995-2006 The MathWorks, Inc.
% %   $Revision: 2$   $Date: 02/03/2011 04:21:12 PM$
%  
% if nargin < 4 
%   error('finance:nweekdate:missingInputs','Please enter N, WKD, Y, and M.') 
% end 
%  
% if nargin < 5
%   g = 0;
% end
%  
% if any(any(n > 5 | n < 1)) 
%   error('finance:nweekdate:invalidOccurrence','N must be integer value of 1 through 5.') 
% end 
%  
% if any(any(m > 12 | m < 1)) 
%   error('finance:nweekdate:invalidMonth','M must be greater than 0 and less than 13.') 
% end 
%  
% if any(any(wkd > 7 | wkd < 1))
%   error('finance:nweekdate:invalidWeekday','WKD must be integer value of 1 through 7.')
% end 
%  
% if any(any(g > 7 | g < 0))
%   error('finance:nweekdate:invalidWeekdayConstraint','G must be integer value of 0 through 7.')
% end 
%  
%  
% % Scalar expansion
% if length(n)==1, n = n(ones(size(g))); end
% if length(wkd)==1, wkd = wkd(ones(size(n))); end
% if length(y)==1, y = y(ones(size(wkd))); end
% if length(m)==1, m = m(ones(size(y))); end
% if length(g)==1, g = g(ones(size(m))); end
% if length(n)==1, n = n(ones(size(g))); end
% if length(wkd)==1, wkd = wkd(ones(size(n))); end
% if length(y)==1, y = y(ones(size(wkd))); end
% if length(m)==1, m = m(ones(size(y))); end
% if checksiz([size(n);size(wkd);size(y);size(m);size(g)],mfilename)
%   return
% end
%   
% d = zeros(size(n));
% for i = 1:length(n(:))
%   prelim1 = -ones(42,1);
%   prelim2 = prelim1;
%   firsd = datenum(y(i),m(i),1);
%   lastd = eomdate(y(i),m(i));
%   stard = weekday(firsd);
%   numdays = length(firsd:lastd);
%   prelim1(stard:numdays+stard-1) = firsd:lastd; 
%   prelim2(stard:numdays+stard-1) = weekday(firsd:lastd);
%   dayind = find(prelim2 == wkd(i));
%   tind = ceil(dayind/7);
%   mint = min(tind);
%   if g(i) == 0 | wkd(i) < g(i)
%     minat = mint;
%   else
%     adayind = find(prelim2 == g(i));
%     atind = ceil(adayind/7);
%     minat = min(atind);
%   end
%   if mint ~= minat
%     if max(tind) < n(i)+1
%       d(i) = 0;
%     else
%       d(i) = prelim1(dayind(n(i)+1));
%     end
%   else
%     if max(tind) < n(i) | length(dayind) < n(i)
%       d(i) = 0;
%     else
%       d(i) = prelim1(dayind(n(i)));
%     end
%   end
% end
% end