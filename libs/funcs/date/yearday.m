function yd = yearday(y, m, d)
% YEARDAY - renvoie le num�ro du jour dans l'ann�e
%
% ses arguments sont : y, m, d l'ann�e, le mois, et le jour consid�r�s, ils
% peuvent �tre des vecteurs
%
% ex : 
% yearday(2008, 3,1) % = 31 + 29 + 1 car 2008 est bissextile
%
% See also weekday day
yd = datenum(y, m, d) - datenum(y, 1, 1) + 1;