function rslt = any2strreq(any_)
% ANY2STRREQ - formatage de valeurs deans une requ�te : rajoute des
%       guillemets autour des chaines de carat�res, et convertit les
%       num�riques en chaine de charact�re
if ischar(any_)
    rslt = ['''' any_ ''''];
elseif isnumeric(any_)
    rslt = num2str(any_);
elseif iscell(any_)
    rslt = cellfun(@any2strreq, any_, 'uni', false);
else
    error('any2strreq:exec', 'unhandled type : <%s>', class(any_))
end