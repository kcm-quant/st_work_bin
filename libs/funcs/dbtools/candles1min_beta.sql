create procedure candles1min_beta
(
  @date                   					date,
  @security_id            					int,
  @trading_destination_id 					int = null,
  @stamp_time_spec     					    varchar(32) = null,
  @phase_spec			                    varchar(32) = null,
  -- 1 si vous voulez du disjonctif, ou 2 si vous voulez le nom des phases cod� en deux lettres
  @source_table           					varchar(64) = null
)
as
begin

-- gestion des utilisations particuli�res

  declare @td_condition varchar(32)
  select  @td_condition = ''
  if ( @trading_destination_id <> null )
  begin
  	select  @td_condition = ' and trading_destination_id = ' + convert( char(16), @trading_destination_id)
  end
  
  declare @stamp_time_select varchar(128)
  select @stamp_time_select = " datediff( second, '00:00:00', begin_time ) + 60.0 as end_time, " -- fin de l'intervalle en nombre de secondes depuis le d�bnut de la journ�e
  if ( @stamp_time_spec <> null )
  begin
  	select @stamp_time_select = 'begin_time, ' 
  end
  
  declare @phase_desc varchar(512)
  select  @phase_desc = "
    p.auction,
    p.opening_auction,
    p.intraday_auction,
    p.closing_auction,
    p.trading_at_last,
    p.trading_after_hours,
    p.cross,"
  if ( @phase_spec <> null )
  begin
  	select @phase_desc = "t.phase,"
  end
  
  if ( @source_table = null )
  begin
    declare @dbase varchar(32)
    declare @dtable varchar(32)

    select @dtable = 'deal_archive' + z.global_zone_suffix
      from tick_db..tickdb_feeder_zone z, repository..place_timezone ptz, repository..security s
     where s.security_id = @security_id
       and ptz.place_id = s.place_id
       and z.global_zone_id = ptz.global_zone_id

    select @dbase = 'tick_db_' + substring( convert( char(3), datepart(month,@date) + 100 ), 2, 2 ) + "d_" + convert(varchar(8),datepart(year,@date))
 
    select @source_table = @dbase + ".." + @dtable
  end
  
-- cr�ation des table temporaire de travail
  create table #trading_1min
   (
    security_id            int     not null,
    trading_destination_id int     not null,
    date                   date    not null,
    phase                  char(2) not null,
    begin_time             time    not null,
    volume                 float   not null,
    turnover               float   not null,
    turnover_overbid       float   not null,
    turnover_overask       float   not null,
    nb_deal                int     not null,
    volume_overbid         float   not null,
    volume_overask         float   not null,
    open_microseconds      int     not null,  -- microseconds from begin_time
    close_microseconds     int     not null,  -- microseconds from begin_time
    open_price             float   not null,
    high_price             float   not null,
    low_price              float   not null,
    close_price            float   not null,
    open_ask               float   not null,
    open_bid               float   not null,
    average_spread_numer   float   null,
    average_spread_denom   float   null,
    sum_price              float   not null
   )

  create table #phase
   ( phase               char(2),
     auction             int,
     opening_auction     int,
     intraday_auction    int,
     closing_auction     int,
     trading_at_last     int,
     trading_after_hours int, 
     cross               int)

  insert into #phase values ('C',  0, 0, 0, 0, 0, 0, 0)
  insert into #phase values ('OA', 1, 1, 0, 0, 0, 0, 0)
  insert into #phase values ('IA', 1, 0, 1, 0, 0, 0, 0)
  insert into #phase values ('CA', 1, 0, 0, 1, 0, 0, 0)
  insert into #phase values ('XA', 1, 0, 0, 0, 0, 0, 0)
  insert into #phase values ('TL', 0, 0, 0, 0, 1, 0, 0)
  insert into #phase values ('TH', 0, 0, 0, 0, 0, 1, 0)
  insert into #phase values ('CR', 0, 0, 0, 0, 0, 0, 1)

-- cr�ation des donn�es
  exec (
   "insert into #trading_1min  (
                security_id,
                trading_destination_id,
                date,
                phase,
                begin_time,
                volume,
                turnover,
                turnover_overbid,
                turnover_overask,
                nb_deal,
                volume_overbid,
                volume_overask,
                open_microseconds,
                close_microseconds,
                open_price,
                high_price,
                low_price,
                close_price,
                open_ask,
                open_bid,
                average_spread_numer,
                average_spread_denom,
                sum_price)
       select d.security_id,
              trading_destination_id,
              d.date,
              p.phase,
              begin_time = dateadd( second, - datepart( second, d.time ), d.time ),

              volume = sum( d.size * 1.0 ),
              turnover = sum( d.size * d.price),
              turnover_overbid = sum( d.size * d.price * d.overbid),
              turnover_overask = sum( d.size * d.price * d.overask),
              nb_deal = sum( 1 ),

              volume_overbid = sum( d.size * 1.0 * d.overbid ),
              volume_overask = sum( d.size * 1.0 * d.overask ),

              open_microseconds = min( datepart( second, d.time ) * 1000000.0 + d.microseconds ),  -- cannot directly get open price, retrieving firstly the timestamp (as negative value)
              close_microseconds = max( datepart( second, d.time ) * 1000000.0 + d.microseconds ),  -- cannot directly get close price, retrieving firstly the timestamp (as negative value)

              open_price = convert( float, 0 ),
              high_price = max( d.price ),
              low_price = min( d.price ),
              olose_price = convert( float, 0 ),
              open_ask = convert( float, 0 ),
              open_bid = convert( float, 0 ),

              average_spread_numer =  sum( case 
                      when d.overbid = 1 and d.ask<>0 and d.bid<>0 and d.bid<=d.ask then d.size * ( d.ask - d.price )
                      when d.overask = 1 and d.ask<>0 and d.bid<>0 and d.bid<=d.ask then d.size * ( d.price - d.bid )
                   end ),
              average_spread_denom = sum( case 
                      when d.overbid = 1 then d.size * 1.0
                      when d.overask = 1 then d.size * 1.0
                   end ),

              sum_price = sum( price )
         from " + @source_table + "  d, #phase p
        where d.date = @date
          and d.security_id = @security_id
          and d.auction = p.auction
          and d.opening_auction = p.opening_auction
          and d.closing_auction = p.closing_auction
          and d.intraday_auction = p.intraday_auction
          and d.trading_after_hours = p.trading_after_hours
          and d.trading_at_last = p.trading_at_last
          and d.cross = p.cross
          and d.size > 0
          " + @td_condition + "

        group by d.security_id,
                 d.trading_destination_id,
                 d.date,
                 p.phase,
                 dateadd( second, - datepart( second, d.time ), d.time )" )


  /* retrieve open/close */

    exec (
    "update #trading_1min
        set open_price = do.price,
            open_bid   = do.bid,
            open_ask   = do.ask,
            close_price = dc.price
       from #trading_1min t, " + @source_table + " do, " + @source_table + " dc
      where 
            t.date = @date
        and t.security_id = @security_id
 
        and do.date = @date
        and do.security_id = @security_id
        and t.trading_destination_id = do.trading_destination_id
        and do.time = dateadd( second, t.open_microseconds / 1000000, t.begin_time )
        and do.microseconds = t.open_microseconds % 1000000 
 
        and dc.date = @date
        and dc.security_id = @security_id
        and t.trading_destination_id = dc.trading_destination_id
        and dc.time = dateadd( second, t.close_microseconds / 1000000, t.begin_time )
        and dc.microseconds = t.close_microseconds % 1000000" )

-- s�lection des donn�es utiles pour l'utilisation sp�cifique
  exec (
  "select
        " + @stamp_time_select + "
         open_microseconds,
         close_microseconds,
         volume,
         turnover,
         turnover_overbid,
         turnover_overask,
         nb_deal,
         volume_overbid,
         volume_overask,
         open_price,
         high_price,
         low_price,
         close_price,
         open_ask,
         open_bid,
         average_spread_numer,
         average_spread_denom,
         sum_price,
         " + @phase_desc + "
         t.trading_destination_id
    from #trading_1min t, #phase p
    where t.phase = p.phase
   order by begin_time, close_microseconds")
   
   -- dans le cas d'une description de la phase avec deux lettres, 
   -- la jointure sur phase est bien entendue inutile
   
end