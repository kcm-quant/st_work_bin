function c = current_mnemonic()
global st_version
c = horzcat(st_version.connection_driver{:, 2});