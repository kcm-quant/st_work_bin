function d = exec_sql( c, str_req)
% EXEC_SQL - exec SQL queries
%
% ex :
%  global st_version
%  exec_sql('QUANT', sprintf('select * from %s..estimator', st_version.bases.quant))
%
% See also userdatabase
global st_version
persistent warn_sqlite

setdbprefs('NullStringRead','null')

try
    [c, logic_cname] = get_database_object(c);
    
    if ~isempty( st_version.regexp)
        reg_fields = fields(st_version.regexp);
        idx_cname = strmatch(upper(logic_cname), reg_fields, 'exact');
        for i = 1:length(idx_cname)
            regexp_struct = st_version.regexp.(reg_fields{idx_cname(i)});
            str_req_ = str_req;
            str_req = regexprep(str_req, regexp_struct.pattern, regexp_struct.replace);
            if ~strcmp( str_req_, str_req)
                exec_sql_st_log('exec_sql: <%s> -> <%s> in <%s>\n', regexp_struct.pattern, regexp_struct.replace, str_req);
            end
        end
    end
    
    if strcmp(st_version.connection_driver, 'sqlite')
        str_req = strrep(str_req, 'repository..', '');
        str_req = strrep(str_req, 'MARKET_DATA..', '');
        str_req = strrep(str_req, 'isnull', 'ifnull');
        if isempty(warn_sqlite)
            warn_sqlite = true;
            exec_sql_st_log('exec_sql: replacing instances of repository.. by '''' and isnull by ifnull...\n');
        end
    end
    
catch ME
    exec_sql_st_log(my_se_stack_err(ME));
    rethrow(ME);
end

try
    curs = exec( c, str_req);
    if ~isempty(curs.Message)
        exec_sql_st_log(curs.Message);
        if ~isempty(regexp(curs.Message,'(Invalid connection\.|Connection reset|Invalid or closed connection)$','once')) || ...
                ~isempty(regexp(curs.Message,'deadlock'))
            exec_sql_st_log('Invalid/closed connection or Deadlock process. We''ll try to reconnect\n');
            %< This log does not seem to work, TODO
            c_fields = fieldnames(c);
            try
                for i = 1 : length(c_fields)
                    try
                        if ischar(c.(c_fields{i}))
                            exec_sql_st_log('\t%s = %s\n', c_fields{i}, c.(c_fields{i}));
                        else
                            exec_sql_st_log('\t%s : %s\n', c_fields{i}, class(c.(c_fields{i})));
                        end
                    catch end
                end
            catch e % TODO more explicit? but not what is just below
                %                     error('exec_sql:check_args', 'SQL: something unexpected happened : <%s>, <%s>', e.identifier, e.message);
            end
            %>
            is_reconnected = false;
            for nb_retry = 1 : 5
                exec_sql_st_log(sprintf('Invalid/closed connection or Deadlock process. We''ll try to reconnect for the %dth time in %d minutes\n', nb_retry, nb_retry));
                pause(nb_retry * 60);
                userdatabase('reset'); % TODO : c'est un peu brutal de refaire toutes les connections, faire plus malin
                
                try
                    [c, logic_cname] = get_database_object(logic_cname);
                    curs = exec( c, str_req);
                catch ME
                    my_se_stack_err(ME);
                    continue;
                end
                is_reconnected = true;
                break;
            end
            if ~is_reconnected
                error('exec_sql:exec', 'SQL: Reconnection seems impossible\n curs.message : <%s>', curs.Message);
            end
        end
        if ~isempty(curs.Message)
            d = message_parsing(curs, str_req);
        else
            d = fetch( curs);
            d = d.Data;
            close(curs);
        end
    else
        d = fetch( curs);
        d = d.Data;
        close(curs);
    end
    if iscell(d) && numel(d)==1 && ischar( d{1}) && strcmpi(d{1}, 'no data') ...
            || ( isnumeric(d) && numel(d) == 1 && d == 0)
        d = {};
    end
    
catch ME
    exec_sql_st_log(my_se_stack_err(ME));
    rethrow(ME);
end

setdbprefs('NullStringRead','')

end

function s = my_se_stack_err(ME)
s = text_to_printable_text(se_stack_error_message(ME));
end

function exec_sql_st_log(text2log)
% V1
st_log([text2log '\n']);
%V2
% global st_version
% dir2log = fullfile(st_version.my_env.st_repository, 'log', 'dbtools');
% if ~exist(dir2log, 'dir')
%     mkdir(dir2log);
% end
% f2log = fullfile(dir2log, 'exec_sql.txt');
% st_log(text2log);
% try
%     st_log('$out+$', f2log);
%     st_log(['EXEC_SQL_LOG: ' datestr(now, 'dd/mm/yyyy HH:MM:SS.FFF') ' : ' text2log '\n']);
%     st_log('$close$');
% catch
%     st_log('$close$');
% end % just to avoit that several matlab or worker have an error because trying to write in the same file
end

function [c, logic_cname] = get_database_object(c)
global st_version
if ischar( c)
    logic_cname = c;
else
    if isempty( c.URL)
        logic_cname = c.Instance;
    else
        full_cname  = tokenize(c.URL, ':');
        logic_cname = full_cname{4};
        warning('I do not know how is <%s>!!!', logic_cname);
    end
end
if iscell(c)
    c = userdatabase( c{:});
elseif ischar(c)
    for i = 1 : size(st_version.connection_driver, 1)
        if ~isempty(strmatch(c, st_version.connection_driver{i, 2}))
            c = userdatabase( st_version.connection_driver{i, 1}, c);
            break;
        end
    end
    if ischar(c)
        error('exec_sql:exec', 'Unable to find a driver for this connection.\n');
    end
end
end

function d = message_parsing(curs, str_req)
if ~any(strcmpi( str_req, {'begin tran', 'commit tran'})) && ...
        isempty(strfind(strrep(lower(str_req), '  ', ' '), 'create table')) &&...
        ~strcmp(curs.Message(1:min(end,5)),'JZ0R2')
    error('exec:sql', 'SQL: %s', curs.Message);
else
    d= {};
    return;
end
end