function out = exec_sql_table(c, req_str, varargin)
% EXEC_SQL_TABLE - Queries results into table
% For old Matlab version, defaults back to struct
% 
% After the second argument, you can give some values to be inserted in the
% request via a sprintf
%
% Examples :
% s = exec_sql_table('MARKET_DATA', 'select max(date) as max_date, min(date) as min_date from MARKET_DATA..trading_daily where security_id = 2');
%
% See also exec_sql
% 
%   author   : 'mlasnier@keplercheuvreux.com'
%   date     :  '11/03/2020'

drf = setdbprefs('DataReturnFormat');
cleanup = onCleanup(@()set_pref_back(drf));
setdbprefs('DataReturnFormat', 'structure');
if ~isempty(varargin)
    req_str = sprintf(req_str, varargin{:});
end
out = exec_sql(c, req_str);
if ~verLessThan('matlab', '9.0')
    if isempty(out)
        out = table();
    else
        out = struct2table(out);
    end
end


function set_pref_back(drf)
setdbprefs('DataReturnFormat', drf);