function data = export_to_sqlite_db(mode, varargin)
% EXPORT_TO_SQLITE_DB - create or insert in sqlite db  the content of a st_data
%
%
%
% Examples:
% the_map =  {'test', '%f','FLOAT';'test_num', '%d','INTEGER';'txt_col', '''%s''', 'VARCHAR(64)'}
% data = st_data( 'init', 'title', 'test_data', 'value', cumsum(randn(100,2)), ...
% 'date', (1:100)', 'colnames', { 'test', 'test_num' })
% data = st_data('add-col', data, repmat({'vincent'}, 100,1),{'txt_col;'})
% export_to_sqlite_db('create','table_name', 'simap_sequences', 'data', data,'parameter_map',the_map,'db_filename', fullfile(pwd,'test.db'))
% change_connection
% change_connections('homolo');export_to_sqlite_db('insert','table_name', 'simap_sequences', 'data', data,'parameter_map',the_map,'db_filename', fullfile(pwd,'test.db'))
%
% See also: userdatabase
%    
%
%   author   : 'vilec'
%   reviewer : ''
%   version  : '1'
%   date     :  '22/07/2011'

opt = options({ 'data',[],...
                'parameter_map',[],...
                'db_filename', '',...
                'table_name','',...
                },varargin);

            
if ~check_input_parameters(opt)
    return;
else
    data = opt.get('data');
    st_parameter_map  = opt.get('parameter_map');
    fullfilename = opt.get('db_filename');
    data_cols = ['date'; data.colnames'];
    col_to_export = st_parameter_map(:,1);
   [subset_to_export , index_data, index_map]= intersect(data_cols, col_to_export);
end

% open connection to the db file 

c = userdatabase('jdbc_sqlite', 'export_sql' , fullfilename );

switch lower(deblank(mode))
    case 'create'
        sql_sentence = create_table_string(mode, data , opt.get('parameter_map'),  opt.get('table_name') ) ;
        exec_sql('export_sql',sql_sentence);
    case 'insert'
        sql_sentence = create_table_string(mode, data , opt.get('parameter_map'),  opt.get('table_name') ) ;
        insert_sql = '';
        sql_insert_data = build_insertion_string(sql_sentence,data_cols, col_to_export,st_parameter_map,data);
        sql_insert = ['exec_sql(''export_sql'', sprintf(sql_sentence ,' sql_insert_data,'));'];
        for i = 1 :length(data.date)
            eval(sql_insert);
        end
        
    otherwise
        error('export_to_sqlite_db:mode', 'MODE: <%s> unknown', mode);
        
end

c = userdatabase('jdbc_sqlite', 'export_sql' , fullfilename );
end


function sql_insert = build_insertion_string(sql_sentence,data_cols, col_to_export, st_parameter_map, data)
sql_insert = ''
[subset_to_export , index_data, index_map]= intersect(data_cols, col_to_export);
get_data_string = 'st_data(''col'',  data, ''%s'',i),';
get_data_string_cdk = 'char(st_data(''col_cdk'',  data, ''%s'',i)),';

for i = 1 : length(subset_to_export)
    is_cdk_col = false;
    current_col = data_cols{index_data(i)};
    if isfield(data,'codebook')
        %we have a codebook, we need to be cautious !!!
        cdk_cols =  {data.codebook.colname};
        if ~isempty(find(strcmp(current_col, cdk_cols)))
            is_cdk_col = true;
        end
    end
    if ~is_cdk_col
        sql_insert = [sql_insert,sprintf(get_data_string, current_col)];
    else
        sql_insert = [sql_insert,sprintf(get_data_string_cdk, current_col)];
    end
    
end

sql_insert(end) = [];
end

function is_valid = check_input_parameters(opt)
is_valid = true; 
st_data = opt.get('data');
st_parameter_map  = opt.get('parameter_map');


        if isempty(st_data)
            is_valid  = false;
            error('export_to_sqlite_db:data', 'please provide a st_data as ''data'' parameter');
        else
            data_cols = ['date'; st_data.colnames'];
        end
        
        if isempty(st_parameter_map)
            is_valid  = false;
            error('export_to_sqlite_db:data', 'please provide a parameter_map as ''parameter_map'' parameter');
        else
            % valid structure for the parameter map is a cell array with
            % colname, sql_format, and MATLAB_format, for isntance {'col_1', '%s', 'VARCAHR(32')}
            
            col_to_export = st_parameter_map(:,1);
            subset_to_export = intersect(data_cols, col_to_export);
            if isempty(subset_to_export) 
                error('export_to_sqlite_db:colnames', 'columns in the input data do not match thenames in the parameters map');
                is_valid  = false;
            end
        end
        
        if isempty(opt.get('table_name'))
                error('export_to_sqlite_db:table_name', 'please sepcify the name of the table to create or modify');
                is_valid  = false;
        end
            
        if isempty(opt.get('db_filename'))
                error('export_to_sqlite_db:db_filename', 'please specify the name of sqlite db filename');
                is_valid  = false;
        end
        
end

function sql_sentence = create_table_string(mode, data, st_parameter_map, table_name)
% valid structure for the parameter map is a cell array with
% colname, sql_format, and MATLAB_format, for isntance {'col_1', '%s', 'VARCAHR(32')}

data_cols = ['date'; data.colnames'];
col_to_export = st_parameter_map(:,1);
[subset_to_export , ~, index_map]= intersect(data_cols, col_to_export);


switch lower(deblank(mode))
    case 'create'
        sql_sentence = sprintf('CREATE TABLE %s ', table_name);
        col_string = '(';
        for i = 1 : length(subset_to_export)
            col_string = [col_string ,sprintf('%s %s,', st_parameter_map{index_map(i), 1}, st_parameter_map{index_map(i), 3})];
        end
        %replace last comma by the closing bracket ) 
        col_string(end) = ')';
        sql_sentence =[sql_sentence col_string];
    case 'insert'
        sql_sentence = sprintf('INSERT OR REPLACE INTO %s ', table_name);
        col_string = '(';
        for i = 1 : length(subset_to_export)
            col_string = [col_string ,sprintf('%s,', st_parameter_map{index_map(i), 1})];
        end
        %replace last comma by the closing bracket )
        col_string(end) = ')';
        sql_sentence =[sql_sentence col_string ' VALUES '];
        values_string = '(';
        for i = 1 : length(subset_to_export)
            values_string = [values_string ,sprintf('%s,', st_parameter_map{index_map(i), 2})];
        end
        values_string(end) = ')';
        sql_sentence = [sql_sentence , values_string];
    otherwise
        error('export_to_sqlite_db:mode', 'MODE: <%s> unknown', mode);
end

end