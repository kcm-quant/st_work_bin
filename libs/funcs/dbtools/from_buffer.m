function datas = from_buffer( fct, fmode, varargin)
% FROM_BUFFER - daily buffer management
% use:
%  data = from_buffer( 'fname', 'fmode', 'from', '01/04/2008', 'to', '30/04/2008', 'remove', {}, opt)
%
% remove can be a cellarray of chars, an array of date or a char with ';' as
% separator.
%
% fname function must:
% - have a 'masterkey' mode
% - accept 'day' option to specify the day to work on
%
% example:
%  data = from_buffer( 'get_tick', 'ft', 'from', '01/04/2008', 'to', '05/04/2008', 'security_id', 110)
%  data = from_buffer( 'get_tick', 'ft', 'from', '01/04/2008', 'to', '05/04/2008', 'security_id', 110, 'output-mode', 'all')
%  data = from_buffer( 'get_tick', 'ft', 'from', '01/04/2008', 'to', '05/04/2008', 'security_id', 110, 'output-mode', 'day')
%
%  data = from_buffer( 'get_gi', 'indicateur_mi', 'security_id', 110, 'from', '05/05/2008', 'to', '07/05/2008', 'trading-destinations', {} )
% in this case the 'to' key will be fruitfully transmitted to 'get_gi'
%
% author   : 'rburgot@cheuvreux.com'
% reviewer : {'ecepeda@ext.keplercheuvreux.com','mlasnier@keplercheuvreux.com'}
% version  : '2.1'
% date     : '23/01/2014'
%
% See also get_tick get_gi

if strcmp(fct,'get_tick') && strcmp(fmode, 'ft')
    datas = get_tick('ft', varargin{:});
    return
end

if strcmp(fct,'get_basic_indicator_v2') && strcmp(fmode, 'smallest_step')
    datas = get_basic_indicator_v2('smallest_step_mysql', varargin{:});
    if isempty(datas.date)
        datas = get_basic_indicator_v2('smallest_step', varargin{:});
    end
    i_wf = 2*find(strcmp(varargin(1:2:end),'where_formula'));
    i_lf = 2*find(strcmp(varargin(1:2:end),'last_formula'));
    if ~isempty(i_wf)
        datas = st_data('where', datas, varargin{i_wf});
    end
    if ~isempty(i_lf)
        datas = st_data('apply-formula', datas, varargin{i_lf});
    end
    i_om = 2*find(strcmp(varargin(1:2:end),'output-mode'),1,'last');
    if ~isempty(i_om) && strcmp(varargin{i_om},'day')
        datas = {datas};
    end
    return
end

global st_version
repository = st_version.my_env.st_repository;
if isempty(repository)
    error('from_buffer:check_st_repository_value', 'Environnement variable st_repository is empty, from_buffer do not agree to work with it');
end
ror_has_priority = ~isfield(st_version.my_env,'ror_has_priority') || st_version.my_env.ror_has_priority;
if ~isempty(st_version.my_env.read_only_repository)
    hror = tokenize(st_version.my_env.read_only_repository); % if you have an error here, then you should get the latest version of st_work in surround
    path2ror = hror{1};
    if length(hror) == 1
        hror = true;
    else
        hror = ismember(fct, hror(2:end));
    end
else
    hror = false;
end
if strcmpi( fmode, 'btickdb')
    error('from_buffer:submode', 'from_buffer should not be called on <get_tick:btickdb>');
end

%<* Key management
fct_master_key = feval( fct, 'masterkey', fmode, varargin{:});
masterkey = fullfile(repository, fct, fct_master_key);
if hror
    ror_dir = fullfile( path2ror, fct, fct_master_key);
end
%>*
%<* Dates management
opt = options( {'from', '', 'to', '', 'remove', {}, 'format-date', 'dd/mm/yyyy', ...
    'output-mode', 'all', 'where_formula', '', 'last_formula', '', 'info_field_to_st_data', ''...
    'from_buffer:cmd', '', 'frequency', 1}, varargin);

% < key test 4 sec_id
is_sec_id_and_which_one = opt.get('security_id');
if ~isnumeric(is_sec_id_and_which_one);
    is_sec_id_and_which_one = false;
end
% >

from_buffer_cmd = opt.get('from_buffer:cmd');
freq = opt.get('frequency');
% Valeurs possible pour from_buffer:cmd (toujours des chaines de caract?re)
% - show_mode : indique que l'on ne souhaite pas les donn?es, mais
%   simplement ouvrir le graphes en mode visuel, avec les bons param?tres
% - get_dir_buffer : indique que l'on souhaite connaitre le r?pertoire dans
% lequel seront bufferis?es les donn?es
if strcmp(from_buffer_cmd, 'get_dir_buffer')
    datas = masterkey;
    return;
end

mode_day    = strcmpi(opt.get('output-mode'), 'day'); % TODO: more than one mode
format_date = opt.get('format-date');
% dt_from     = datenum( opt.get('from'), format_date );
% dt_to       = datenum( opt.get('to'), format_date );
dt_from = opt.get('from');
if ~isnumeric( dt_from)
    dt_from = datenum(dt_from, format_date);
end
dt_to = opt.get('to');
if ~isnumeric( dt_to)
    dt_to   = datenum( dt_to, format_date);
end
where_formula = opt.remove('where_formula');
has_where_formula_to_apply = ~isempty(where_formula);
last_formula = opt.remove('last_formula');
has_formula_to_apply = ~isempty(last_formula);
if length(dt_from) > 1 % On peut demander un ensemble de dates pr?d?termin?s gr?ce ? un vecteur de dates num?rique transmis dans from
    dt_all = dt_from;
    dt_to = dt_from(end);
else
    dt_all     = dt_from:freq:dt_to;
    if length(dt_all) > 2 % il faudrait trouver une bonne mani?re de faire ?a, pour l'instant je le fais comme ?a...
        wk_all     = weekday(dt_all);
        if freq == 1
            dt_all( wk_all==1 | wk_all==7 ) = [];
        end
    end
end
if mode_day
    if freq == 1
        agenda = dt_from:dt_to;
    else
        agenda = dt_all;
    end
    datas  = arrayfun(@(c)struct('value', [], 'date', [], 'colnames', {{}}, ...
        'title', '', 'info', struct('data_datestamp', c)), agenda, 'uni', false)';
else
    datas = [];
end
dt_remove  = opt.get('remove');
if ~isempty( dt_remove)
    if ~isnumeric( dt_remove)
        if ischar( dt_remove)
            dt_remove = tokenize( dt_remove, ';');
        end
        dt_remove = datenum( dt_remove, format_date );
    end
    dt_all = setdiff(dt_all , dt_remove);
end
%>*
if isempty( dt_all)
    if length(dt_from)==1 && dt_from>dt_to
        st_log('from_buffer: you asked for no date because from <%s> is greater than to <%s>...\n', ...
            datestr( dt_from, 'dd/mm/yyyy'), datestr(dt_to, 'dd/mm/yyyy') );
    else
        st_log('from_buffer: you asked for no date...\n');
    end
end
st_log('from_buffer: working on %d dates from <%s> to <%s> \n\tfct = <%s>\n\tfmode = <%s>\n\tmasterkey = <%s>\n',...
    length( dt_all), datestr(dt_all(1), format_date), datestr(dt_all(end), format_date), fct, fmode, masterkey);
filesep_   = filesep();
fct_handle = str2func(fct);
curs = NaN;
% <* gestion de l'option info_field_to_st_data qui permet de ranger dans un
% st_data un champs qui se trouve par exemple dans .info
iftpisd = opt.get('info_field_to_st_data');
iftsd   = ~isempty(iftpisd);
ifields_data = [];
% >

if strcmp(fct, 'get_gi')
    show_mode = strcmp(opt.get('from_buffer:cmd'), 'show_mode');
    if freq ~= 1
        if ~isempty(strfind(fmode, char(47)))
            fmode = [fmode char(167) 'from_buffer_frequency' char(167) '"day|' num2str(freq)];
        else
            fmode = [fmode char(47) 'from_buffer_frequency' char(167) '"day|' num2str(freq)];
        end
    end
else
    show_mode = false;
end

%**************************************************************************
%******************************** DEBUT  **********************************
%tout ce qui vient sera remplace par une fonction
%qui cree le dossier (si ~existe) et  cherche le fichier .mat
%**************************************************************************
% < On va regarder toutes les dates qu'il va falloir reg?n?rer, et on va le
% faire en g?rant aussi le to qui est pass? ? fmode car certaines fonction
% l'utilisent, come get_gi
these_days  = cellstr(datestr(dt_all,'yyyy_mm_dd'));
has2compute =  verify_repository(masterkey,dt_all);

% < Patch pour get_tick/ft en base
if strcmp(fct,'get_tick') && strcmp(fmode,'ft')
    has2compute = false(length(has2compute),1);
end
% Fin du patch >

if hror && (ror_has_priority || any(has2compute))
    is_in_ror =  ~verify_repository(ror_dir,dt_all);
    if ~ror_has_priority
        is_in_ror = is_in_ror & has2compute;
        % the variable is_in_ror is not eponym now but rather means is_in_ror_and_not_in_local_repository
    end
else
    is_in_ror = false(size(has2compute));
end

if any(is_in_ror)
    st_log('from_buffer: You''ve set a read_only_repository <%s> in st_work.xml which contains some of the data asked. It will be used first\n', ror_dir);
end
has2compute = has2compute & ~is_in_ror;
%kepchTODO: Speed up the following call to dir.
% if ~exist(masterkey,'dir') %pour eviter error when save(file)
% 	st_log('from_buffer: creating dir <%s>\n', masterkey);
% 	mkdir(masterkey);
% 	dir_content = {};
% else
% 	dir_content = dir_custom(masterkey);
% end
% % dir_content = {dir_content.name};
% dir_content = strrep(dir_content, '.mat', '')';
%
% [tmp, itd, idc] = intersect(these_days, dir_content);
% has2compute = true(size(these_days, 1), 1);
% if ~isempty(tmp)
%     has2compute(itd) = false(length(itd), 1);
% end


% if hror && (ror_has_priority || any(has2compute))
%     % kepchTODO: Speed up the following call to dir.
% 	if ~exist(ror_dir,'dir')
% 		ror_content = {};
% 	else
% 		ror_content = dir_custom(ror_dir);
% 	end
%
% %     ror_content = {ror_content.name};
%     ror_content = strrep(ror_content, '.mat', '')';
%     [tmp, itd, idc] = intersect(these_days, ror_content);
%     is_in_ror = false(size(these_days, 1), 1);
%     if ~isempty(tmp)
%         is_in_ror(itd) = true;
%     end
%     if ~ror_has_priority
%        is_in_ror = is_in_ror & has2compute;
%        % the variable is_in_ror is not eponym now but rather means is_in_ror_and_not_in_local_repository
%     end
% else
%     is_in_ror = false(size(has2compute));
% end

%has2compute sortie vecteur avec valeurs logiques 1 si in repo 0 sinon
%**************************************************************************
%********************************* FIN  ***********************************
%tout ce qui est passe' sera remplace' par une fonction
%qui cre'e le dossier (si ~exist) et  cherche le fichier .mat
%**************************************************************************
% >
these_days_my_format  = datestr( dt_all, format_date);
len_dt_all = length(dt_all);
my_memory4extract_days = [];
for d=1:len_dt_all
    %<* For each date
    this_date = dt_all(d);
    this_day  = these_days_my_format(d, :);
    this_folder  = [masterkey, filesep_, char(pathtofile(this_date))];
    this_file = [this_folder , filesep_, these_days{d}, '.mat'];
    
    loaded = false;
    has2stack = true;
    if ~has2compute(d) && ~show_mode
        try
            if is_in_ror(d)
                %load( [ror_dir, filesep_, these_days{d}, '.mat'], 'data');
                load([ror_dir, filesep_, char(pathtofile(this_date)), filesep_, these_days{d}, '.mat'], 'data');
            else
                load( this_file, 'data');
            end
            if ~isempty(data) && issparse(data.value)
                data.value = full(data.value);
            end
            loaded = true;
            
            %> Patch : Correction des volumes Bid-ask pour les titres US
            try
                is_america_ny = strcmpi(data.info.td_info(1).timezone,'America/New_york');
                is_day_greater_21jul2010 = this_date > datenum(2010,7,21);
                is_day_lower_31_mai_2012 = this_date < datenum(2012,5,31);
                is_field_patch_us = isfield(data.info,'patch_us');
                is_ftc_get_tick_ft = strcmp(fct,'get_tick')&strcmp(fmode,'ft');
                if is_america_ny && is_day_greater_21jul2010 && ~is_field_patch_us && is_ftc_get_tick_ft && is_day_lower_31_mai_2012
                    is_col_bid_ask_size = ismember(data.colnames,{'bid_size','ask_size'});
                    data.value(:,is_col_bid_ask_size) = data.value(:,is_col_bid_ask_size)*100;
                    data.info.patch_us = true;
                elseif is_ftc_get_tick_ft && is_america_ny && is_field_patch_us &&...
                        (~is_day_lower_31_mai_2012 || ~is_day_greater_21jul2010)
                    is_col_bid_ask_size = ismember(data.colnames,{'bid_size','ask_size'});
                    data.value(:,is_col_bid_ask_size) = data.value(:,is_col_bid_ask_size)/100;
                    data.info.patch_us = true;
                    data.info = rmfield(data.info,'patch_us');
                end
            catch %#ok<CTCH>
            end
            %> Fin du patch.
            
        catch er
            st_log('from_buffer [mat]: Error during the load of <%s>:\n<<%s>>\nI will try to recreate it...\n', this_file, er.message);
            has2compute(d) = true;
        end
        
    end
    if ~loaded
        % < Gestion du to et du dt_all ? transmettre ? la fonction fct afin
        % qu'elle puis si elle le d?sire faire ses calculs sur toute une
        % p?riode, et pas seulement jour par jour.
        % Rq : de toute fa?on from_buffer va l'appeller sur chaque jour, il
        % faut donc que 'fct' stocke cela dans une variable persistente
        % N.B.: si vous choississez d'utiliser cette feature, prenez soin
        % de v?rifier que 'fct' calcule bien tous les jours qui lui sont
        % transmis dans dt_all et pas plus. Autrement votre variable
        % persistente pourrait se remplir petit ? petit
        %
        % Une autre possibilit? est de ne pas faire cela dans votre
        % fonction mais de laisser from_buffer le faire.
        % Pour cela vous devez rajouter ? votre st_data un champs
        % has_several_days ? true.
        % Dans ce cas from_buffer s'occupera de d?couper votre st_data
        % Attention n?anmoins, vous devez travailler de day ? to, et pas de
        % from ? to
        if isempty(my_memory4extract_days)
            if d==1 || ~has2compute(d-1)
                idx_next_to = find(~has2compute(d:end), 1, 'first') - 1;
                if isempty(idx_next_to)
                    idx_next_to = len_dt_all - d + 1;
                end
                opt.set('to', datestr(dt_all(d-1+idx_next_to), 'dd/mm/yyyy'));
                opt.set('dt_all', dt_all(d:d+idx_next_to-1));
            end
            % >
            opt.set('day', this_day);
            lst = opt.get();
            if strcmp(fct,'get_tick')
                iter = iterator(fct_handle, fmode, varargin{:});
                data = iter.get_data(fct_handle, fmode, lst{:});
            else
                data = fct_handle(fmode, lst{:});
            end
            if iscell(data)
                assert(length(data)==1,'from_buffer','Load data for multiple days instead of just one')
                data = data{1};
            end
            if ~isempty(data) && issparse(data.value) % matlab convertit tout seul en sparse mais ne sait pas toujours s'en d?brouiller
                data.value = full(data.value);
            end
            if isfield(data, 'has_several_days')
                my_memory4extract_days = rmfield(data, 'has_several_days');
                my_memory4extract_days = gmt2local_time(fct, my_memory4extract_days);
                if ~mode_day % alors on conserve les donn?es d?j? empil?es et on les ajoute directement ? datas
                    data = apply_formulas(my_memory4extract_days, has_where_formula_to_apply, where_formula, has_formula_to_apply, last_formula);
                    if isempty( datas)
                        datas = data;
                        curs  = size(data.value,1)+1;
                    else
                        s1 = size(data.value,1)-1;
                        if curs+s1>size(datas.value,1)
                            datas.value = cat(1,datas.value, nan(s1+1-size(datas.value,1),size(datas.value,2)));
                            datas.date  = cat(1,datas.date, nan(s1+1-size(datas.value,1),1));
                        end
                        datas.value(curs:curs+s1,:) = data.value;
                        datas.date(curs:curs+s1,:)  = data.date;
                        curs = curs+s1+1;
                        if isfield( datas, 'rownames')
                            if isfield( data, 'rownames')
                                datas.rownames = cat(1,datas.rownames(:),data.rownames(:));
                            else
                                datas = rmfield( datas, 'rownames');
                                st_log('     from_buffer: removing <rownames> field from fused data\n');
                            end
                        end
                    end
                    has2stack = false;
                end
                data = st_data('extract-day', my_memory4extract_days, this_date);
                save_data(data, this_file, this_folder, show_mode);
            else
                data = gmt2local_time(fct, data);
                save_data(data, this_file, this_folder ,show_mode);
            end
        else
            % la conversion en horaires locaux a ?t? faite lors de la
            % r?cup?ration de my_memory4extract_days
            % Ainsi que l'empilage, il ne reste plus qu'? sauvegarder la
            % journ?e
            data = st_data('extract-day', my_memory4extract_days, this_date);
            if d<len_dt_all && ~has2compute(d+1)
                my_memory4extract_days = [];
            end
            if ~mode_day
                has2stack = false;
            end
            save_data(data, this_file, this_folder,  show_mode);
        end
    end
    % < Testing for bad reference : mixing different security_id
    % because of same RIC...
    if length(is_sec_id_and_which_one)==1 && is_sec_id_and_which_one && isfield(data, 'info') && isfield(data.info, 'security_id')
        assert(is_sec_id_and_which_one==data.info.security_id || ...
            (is_sec_id_and_which_one == 31 && data.info.security_id == 1512703) || ...
            (is_sec_id_and_which_one == 137 && data.info.security_id ==  1823563), ...
            'REPOSITORY: Confusing different security_id');
        % might either be because you are on windows whose
        % directories names are not case sensitive, or the same
        % ric has been given to two different stocks (not at the
        % same time),  or...
    end
    % >
    
    
    %<Mapping des destinations de trading : Cheuvreux -> Kepler
    has_to_map = kech_td_mapping_needed(data);
    if has_to_map
%     if (strcmp(fct,'get_tick') || strcmp(fct,'get_basic_indicator_v2') || (strcmp(fct,'get_gi') && strcmp(fmode,'basic_indicator')))...
%             && ~isempty(data) && ~isempty(data.value) && (~isfield(data.info,'version') || ...
%             (~strcmp(data.info.version,'kepche_1') && ~strcmp(data.info.version,'matgen_1.0') && ~strcmp(data.info.version,'matgenNet_1.0')))
%         [td_id_ch,~,idx_td_id_ch] = unique(st_data('cols',data,'trading_destination_id'));
%         td_id_kgr = get_trading_destination_mapping(td_id_ch,data.info.security_id);
%         idx_keep_td_info = ismember([data.info.td_info.trading_destination_id],-td_id_kgr(td_id_kgr<0));
%         keep_td_info = data.info.td_info(idx_keep_td_info(:));
%         if ~isempty(keep_td_info)
%             for t = 1:length([keep_td_info.trading_destination_id])
%                 keep_td_info(t).trading_destination_id = -keep_td_info(t).trading_destination_id;
%             end
%         end
%         data.value(:,strcmp(data.colnames,'trading_destination_id')) = td_id_kgr(idx_td_id_ch);
%         new_td_info = get_repository('tdinfo',data.info.security_id,'trading_destination_id',td_id_kgr(td_id_kgr>0));
%         missing_fields = setdiff(fieldnames(new_td_info),fieldnames(keep_td_info));
%         keep_td_info = merge_struct(keep_td_info,...
%             cell2struct(cell([size(keep_td_info),length(missing_fields)]),missing_fields(:),3));
%         if isfield(keep_td_info, 'true_place_id')
%             keep_td_info = rmfield(keep_td_info,'true_place_id');
%         end
%         data.info.td_info = [new_td_info;keep_td_info];
%         data.info.version = 'kepche_1';
        data = st_data_get_trading_destination_mapping(data);
    end
    
    if isempty( data) || isempty(data.value)
        
        if st_version.my_env.verbosity_level > 9;
            st_log('nothing in the buffer for %s. Potential reasons below :\n <<<%s>>>', ...
                this_day, st_data('log2str', data));
        else
            st_log('nothing in the buffer for %s\n', this_day);
        end
        if mode_day
            %< Mode DAY
            if isempty( data)
                data = st_data('empty-init');
            end
            curs = find( this_date==agenda, 1);
            data.info.data_datestamp = this_date;
            datas{curs} = data;
            %>
        end
    elseif has2stack % si 'lon a re?u tout un bloc de donn?es, l'empilage a d?j? ?t? fait si le mode_day est actif
        data = gmt2local_time(fct, data);
        data = apply_formulas(data, has_where_formula_to_apply, where_formula, has_formula_to_apply, last_formula);
        if isempty(data.date)
            if mode_day
                %< Mode DAY
                curs = find( this_date==agenda, 1);
                data.info.data_datestamp = this_date;
                datas{curs} = data;
                %>
            end
            continue;
        end
        if mode_day
            %< Mode DAY
            curs = find( this_date==agenda, 1);
            data.info.data_datestamp = this_date;
            datas{curs} = data;
            %>
        else
            %< Mode ALL
            if isempty( datas)
                datas = data;
                curs  = size(data.value,1)+1;
                if length(dt_all) > 1
                    datas.value = cat(1,data.value, nan(size(data.value,1)*ceil(length(dt_all)*1.1),size(data.value,2)));
                    datas.date  = cat(1,data.date, nan(size(data.value,1)*ceil(length(dt_all)*1.1),1));
                    if isfield( data, 'rownames')
                        datas.rownames = data.rownames;
                    end
                end
            else
                s1 = size(data.value,1)-1;
                if curs+s1>size(datas.value,1)
                    datas.value = cat(1,datas.value, nan((s1+1)*5,size(datas.value,2)));
                    datas.date  = cat(1,datas.date, nan((s1+1)*5,1));
                end
                datas.value(curs:curs+s1,:) = data.value;
                datas.date(curs:curs+s1,:)  = data.date;
                curs = curs+s1+1;
                if isfield( datas, 'rownames')
                    if isfield( data, 'rownames')
                        datas.rownames = cat(1,datas.rownames(:),data.rownames(:));
                    else
                        datas = rmfield( datas, 'rownames');
                        st_log('     from_buffer: removing <rownames> field from fused data\n');
                    end
                end
                
                % Union des champs td_info si existants
                if isfield(datas.info, 'td_info')&&isfield(data.info, 'td_info')
                    [~,a,b] = union([datas.info.td_info.trading_destination_id],...
                        [data.info.td_info.trading_destination_id]);
                    
                    pot_miss_flds = {'execution_market_id','global_zone_name','default_timezone'};
                    for w = 1:length(pot_miss_flds)
                        flds_name = pot_miss_flds{w};
                        if ~isfield(datas.info.td_info,flds_name) && isfield(data.info.td_info,flds_name)
                            [datas.info.td_info.(flds_name)] = deal([]);
                        end
                        if isfield(datas.info.td_info,flds_name) && ~isfield(data.info.td_info,flds_name)
                            [data.info.td_info.(flds_name)] = deal([]);
                        end
                    end
                    datas.info.td_info = [datas.info.td_info(a);data.info.td_info(b)];
                end
                
                % Gestion de l'empilage des codebooks
                
                if isfield( datas, 'codebook') && ~isempty(datas.codebook)
                    if isfield( data, 'codebook') && ~isempty(data.codebook)
                        codebooks_colnames = {datas.codebook.colname};
                        codebook_colnames  = { data.codebook.colname};
                        for c=1:length(codebooks_colnames)
                            col_id = strfind( codebook_colnames, codebooks_colnames{c});
                            if length(col_id)~=1
                                datas = rmfield( datas, 'codebook');
                                st_log('     from_buffer: removing <codebook> field from fused data\n');
                                break
                            else
                                col_id = col_id{1};
                            end
                            datas.codebook(c).book = cat(1, datas.codebook(c).book, ...
                                data.codebook(col_id).book);
                        end
                    else
                        datas = rmfield( datas, 'codebook');
                        st_log('     from_buffer: removing <codebook> field from fused data\n');
                    end
                end
                
                
            end
            if iftsd && ~isempty(data.info.(iftpisd))
                if isempty(ifields_data)
                    ifields_data = st_data('init', 'value', data.info.(iftpisd), ...
                        'date', this_date, 'colnames', {iftpisd}, 'title', 'extracted_fields');
                else
                    ifields_data.value(end+1, :) = data.info.(iftpisd);
                    ifields_data.date(end+1, :)  = this_date;
                end
            end
            %>
        end
    end
    %>*
end
if ~mode_day
    if isnan( curs)
        datas = st_data('log', data, 'eieo');
    else
        datas.value(curs:end,:)=[];
        datas.date(curs:end,:) =[];
        if ~st_data('isempty-nl', data)
            datas.title = sprintf('%s -> %s', datas.title, data.title);
        else
            datas.title = sprintf('%s -> ??????', datas.title);% TODO better
        end
        datas.info.data_datestamp = dt_to;
    end
    if iftsd
        datas.info.(iftpisd) = ifields_data;
    end
else
    %     datas(cellfun(@isempty, datas, 'uni', true)) = [];
end

if exist('iter','var')==1
    iter.clean_persistent();
end

% if (iscell(datas)&&all(cellfun(@(c)st_data('isempty-nl',c),datas))) || (~iscell(datas)&&st_data('isempty-nl',datas))
%     if ischar(fct)
%         st_log('from_buffer:%s Some of your data are empty.\n', fct);
%         if ~iscell(datas) && isfield(datas, 'info') &&  isfield(datas.info, 'data_log')
%             st_log('from_buffer:Reasons might be : <%s>\n', sprintf('%s\n\t', datas.info.data_log.message));
%         end
%     else
%         st_log('from_buffer:%s Some of your data are empty.\n', func2str(fct));
%     end
% % Too useless to be kept I agree to work on it if someone wants : robur
% % furthermore note that you can have some details if setting the
% % verbosity_level to more than 10
%     try
%         % probl?me probable
%         tds = get_repository( 'any-to-sec-td', 'security_id', opt.get('security_id'), 'trading-destinations', opt.get('trading-destinations') );
%         td_min = min([ tds.trading_destination_id]);
%         qr = exec_sql( 'BSIRIUS', sprintf( 'select quotation_group from repository..security_market where security_id = %d and trading_destination_id = %d', ...
%             opt.get('security_id'), td_min));
%         if isempty( qr)
%             st_log('from_buffer: sec_id=%d td=%s from="%s" to="%s" seems to be empty because no quotation group is defined for the minimal td <%d>\n', ...
%                 opt.get('security_id'), convs( 'str', opt.get('trading-destinations')), datestr(dt_from, 'dd/mm/yyyy'),  datestr(dt_to, 'dd/mm/yyyy'), ...
%                 td_min);
%         else
%             hq = exec_sql( 'BSIRIUS', sprintf('select * from repository..quotation_group_trading_hours where trading_destination_id = %d and quotation_group = ''%s''', ...
%                 td_min, regexprep(qr{1},'null','')));
%             if isempty( hq)
%                 st_log('from_buffer: sec_id=%d td=%s from="%s" to="%s" seems to be empty because no trading hour is defined for the minimal td <%d> and the quotation group <%s>"\n', ...
%                     opt.get('security_id'), convs( 'str', opt.get('trading-destinations')), datestr(dt_from, 'dd/mm/yyyy'),  datestr(dt_to, 'dd/mm/yyyy'), ...
%                     td_min, qr{1});
%             else
%                 oc =  exec_sql( 'BSIRIUS', strrep(sprintf('select opening,closing from repository..quotation_group_trading_hours where trading_destination_id = %d and quotation_group = ''%s''', ...
%                     td_min, qr{1}),'= ''null''','is null'));
%                 if strcmpi( oc{1,1}, 'null') || strcmpi( oc{1,2}, 'null')
%                     st_log('from_buffer: sec_id=%d td=%s from="%s" to="%s" seems to be empty because no opening or closing hour is defined for the minimal td <%d> and the quotation group <%s>"\n', ...
%                         opt.get('security_id'), convs( 'str', opt.get('trading-destinations')), datestr(dt_from, 'dd/mm/yyyy'),  datestr(dt_to, 'dd/mm/yyyy'), ...
%                         td_min, qr{1});
%                 else
%                     st_log('from_buffer: sec_id=%d td=%s from="%s" to="%s" <<mysterious>> data error...\n', ...
%                         opt.get('security_id'), convs( 'str', opt.get('trading-destinations')), datestr(dt_from, 'dd/mm/yyyy'),  datestr(dt_to, 'dd/mm/yyyy'));
%                 end
%             end
%         end
%     catch e
%         st_log('Oops, I made an error while trying to understand why your data is empty.\n');
%     end
% end

end

function data = gmt2local_time(fct, data)
% recalage de la timezone en local
if strcmpi(fct, 'get_tick') && ~(isempty(data) || isempty(data.value)) && ...
        ~(isfield(data, 'info') && isfield(data.info, 'localtime') && data.info.localtime)
    st_log('Adjusting timezone to local...\n');
    decalage  = timezone('get_offset', 'final_timezone', data.info.td_info(1).timezone, 'dates', floor( data.date(1)));
    data.date = data.date + decalage;
    data.info.localtime = true;
end
end

function data = apply_formulas(data, has_where_formula_to_apply, where_formula, has_formula_to_apply, last_formula)
% On applique les formules
if has_where_formula_to_apply
    data = st_data('where', data, where_formula);
    % Attention, en sortie d'un where les donn?es peuvent ?tre
    % vides
end
if has_formula_to_apply
    data = st_data('apply-formula', data, last_formula);
end
end

% fonction pour suavegarder le fichier, en faisant attention aux acc?s
% consurentiels
function save_data(data, this_file, this_folder , show_mode) %#ok<INUSL> % data is used in save( this_file, 'data');
% < le mode show qui n'existe que dans le cas d'un appel ? get_gi
% sert a afficher le graphe afin de le debugger, mais renvoie des
% donn?es vides, il ne faut surtout pas le bufferizer

     if ~exist(this_folder,'dir')
         mkdir(this_folder);
     end
if ~show_mode
    ftxt = [ this_file '.txt'];
    dtxt = dir( ftxt);
    ok2save = true;
    if ~isempty( dtxt)
        % j'ai un probl?me, qq un est en train d'?cire le m?me
        % fichier
        dmat = dir( this_file);
        if ~isempty( dmat)
            % et le fichier mat est d?j? l?!
            if dmat.datenum > dtxt.datenum %+ datenum(0,0,0, 0,10,0)
                % depuis tr?s longtemps
                st_log('from_buffer [mat]: MATLAB waited too long between txt and mat file, I will (try to) rewrite <%s>...', this_file);
                try
                    % j'essaie de l'effacer (pour le r? ?crire)
                    delete( this_file);
                    delete( ftxt);
                    st_log('Deleted...\n');
                catch er
                    st_log('Impossible to delete it <%s>...\n', er.message);
                    ok2save = false;
                end
            else
                % fichier txt pr?sent mais pas le mat
                st_log('from_buffer [mat]: txt file is there, I will not write the mat file <%s> on my own...\n', this_file);
                ok2save = false;
            end
        else
            st_log('from_buffer [mat]: txt file is there, I will not write the mat file <%s> on my own...\n', this_file);
            ok2save = false;
        end
    end
    if ok2save
        txt = 101;
        save( ftxt, 'txt', '-ASCII')
        save( this_file, 'data');
        try
            delete( ftxt);
        catch er
            st_log('from_buffer [mat]: impossible to delete the txt file <%s>...\n', ftxt);
        end
    end
end
% >
end

function path = pathtofile(date_)
path = datestr(date_,'yyyy_mm');
path(:,5) = filesep;
path  = cellstr(path);
end

function has2compute =  verify_repository(repository,dt)
path_ = pathtofile(dt);
[un,~,idx] = unique(path_);
has2compute = true(length(dt),1);
for i = 1:length(un)
%     if ~exist(fullfile(repository,un{i}),'dir')
%         mkdir(fullfile(repository,un{i}));
%     else
    if exist(fullfile(repository,un{i}),'dir')
        filenames = strcat(un{i},filesep,cellstr(datestr(dt(idx==i),'yyyy_mm_dd')),'.mat');
        tmp_has2compute = true(length(filenames),1);
        for j = 1:length(filenames)
            if exist(fullfile(repository,filenames{j}),'file')
                tmp_has2compute(j) = false;
            end
        end
        has2compute(idx==i) = tmp_has2compute;
    end
end
end

% 
% if ~exist(path_,'dir')
%     st_log('from_buffer: creating dir <%s>\n', path_);
%     mkdir(path_);
%     return
% end
% for i = 1 : numel(dt)
%     path_ = fullfile(repository,pathtofile(dt(i)));
%     %creation preventive
%     
%     filename = fullfile(path_,[datestr(dt(i),'yyyy_mm_dd'),'.mat']);
%     if exist(filename,'file')
%         has2compute(i) = false;
%     end
% end
% end