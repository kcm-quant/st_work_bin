function data = fx_adjustment(data)
sec_str = join(unique(data.security_id));
dt = data.date;
if iscellstr(dt) %#ok<ISCLSTR>
    dt = datenum(dt, 'yyyy-mm-dd');
elseif isdatetime(dt)
    dt = datenum(dt, 'yyyy-mm-dd');
end
q = "select distinct q.security_id,h.DATE,h.VALUE"+...
    " from KGR..HISTO_SECURITY_QUANT q, KGR..HISTOCURRENCYTIMESERIES h"+...
    sprintf(" where q.security_id in (%s)", sec_str)+...
    " and q.CCY = h.CCY"+...
    " and h.CCYREF = 'EUR'"+...
    sprintf(" and h.DATE between '%s' and '%s'",...
    datestr(min(dt), 'yyyy-mm-dd'), datestr(max(dt), 'yyyy-mm-dd'));
fx = exec_sql_table('MARKET_DATA', q);
fx.DATE = datenum(fx.DATE, 'yyyy-mm-dd 00:00:00.0');

idx_adj = ismember(data.Properties.VariableNames, {'turnover','open_prc',...
    'high_prc','low_prc','close_prc','open_turnover','close_turnover',...
    'intraday_turnover','cross_turnover','auction_turnover',...
    'dark_turnover','midclose_turnover','end_turnover','off_turnover',...
    'last_before_close_prc','qty_1er_limit','tick_size','periodic_turnover',...
    'trading_at_last_turnover'});
if any(idx_adj)
    data{:, idx_adj} = joint(fx{:, {'security_id', 'DATE'}}, fx.VALUE,...
        [data.security_id, dt]).*data{:, idx_adj};
end


function s = join(x)
if isnumeric(x)
    s = sprintfc('%d', x);
    s = sprintf('%s,', s{:});
    s(end) = [];
    return
end
is_num = cellfun(@isnumeric, x);
if all(is_num)
    s = join(cell2mat(x));
elseif all(~is_num)
    s = sprintf('%s,', x{:});
    s(end) = [];
else
    s = strcat(join(x(is_num)), ',', join(x(~is_num)));
end