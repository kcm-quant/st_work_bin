function [rslt, rslt_plus] = gen_query(mode, varargin)
% GEN_QUERY - function qui g�n�re une requ�te et formate le r�sultat
%
% ex :
%   gen_query('s', 'request-table', 'repository..security_source', ...
%       'input-fields', {'security_id', 'source_id'}, 'input-values', {110, 2})
%
% modes : - simple/s
%
% MODE simple/s
%
% Arguments quasi-non optionnels :
%   - request-fields = les noms de colonnes � s�lectionner, ce seront aussi
%       les noms des champs de la structure si 'lon choisit d'avoir le
%       r�sultat sous forme de structure.
%   - input-fields   = les champs sur lesquels portent une clause where
%       c'est une chaine de charact�res (s'il n'y en a qu'un) ou un
%       cell-array de strings s'il y en a plusieurs
%   - input-values   = les valeurs d�sir�es dans la clause where
%       selon le cas, cela peut-�tre une chaine de charact�res, un numeric,
%       ou un cell-array contenant numeric et chaines de caract�res
%   - request-table  = 'base..table' sur laquelle porte la req�te
%   - server         = 'BSIRIUS', 'GANDALF', etc
%
% Arguments optionnels
%   - output-type = 'struct/cell'     suivant que l'on d�sire avoir le
%       r�sultat dans un struct-array (le nom des champs est alors le nom
%       des colonnes s�lectionn�es)
%   - request-opt = chaine de charact�re que l'on rajoute � la fin de la
%       requ�te permet par exemple de rajouter un order by
%
% See also exec_sql userdatabase any2strreq

switch lower(mode)
    case {'s', 'simple'}
    opt = options({ 'request-fields', '*', ...
        'input-fields', '', ...
        'input-values', '',...
        'output-type', 'struct', ...
        'request-table', 'repository..security_source', ...
        'request-opt', '' ,...
        'server', 'BSIRIUS',...
        }, varargin);

    request_fields = opt.get('request-fields');
    input_f        = opt.get('input-fields');
    request_table  = opt.get('request-table');
    base_and_table = tokenize(request_table, '..');

    if length(base_and_table) ~= 2
        error('gen_query:exec', 'request-table must be of the form : ''base..table''.\nYou only gave request-table = <%s>', request_table);
    end
    % >* R�cup�ration de la connection
    c = userdatabase('sybase:j', opt.get('server'));
    % <*
    
    % >* Formattage de la requ�te
    if iscell( request_fields)
        request_fields = CELL2STR_SEPARATOR(request_fields, ',');
    end
    if isempty(input_f) % pas de clause where
        str_where = '';
    else % il y a une clause where
        input_v = any2strreq(opt.get('input-values'));
        if iscell(input_f)
            str_where = arrayfun(@(i)sprintf('%s=%s',input_f{i},input_v{i}),1:length(input_f), 'uni', false);
            str_where = sprintf('where %s', CELL2STR_SEPARATOR(str_where, ' and '));
        else
            str_where = sprintf('where %s=%s', input_f, input_v);
        end
    end
    % <*

    % >* requ�te
    rslt= exec_sql(c, sprintf('select %s from %s %s %s',  request_fields, request_table, str_where, opt.get('request-opt') ));
    % <*

    % >* formattage du r�sultat
    switch lower(opt.get('output-type'))
        case 'struct'
            if strcmp(strtrim(request_fields), '*') || strcmp(strtrim(request_fields), 'distinct *')
                request_fields = exec_sql(c, sprintf('%s..sp_columns %s', base_and_table{:}));
                request_fields = lower(strtrim(request_fields(:, 4)));
            else % on a sp�cifi� les colonnes � r�cup�rer
                request_fields = lower(strtrim(tokenize( request_fields, ',')));
                request_fields{1} = strtrim(strrep( request_fields{1}, 'distinct ', ''));
            end
            if ~isempty(rslt)
                tmp = arrayfun(@(i){request_fields{i},rslt(:,i)},1:length(request_fields), 'uni', false);
                tmp = [tmp{:}];
                rslt = struct(tmp{:});
            else
                tmp = arrayfun(@(i){request_fields{i},{}},1:length(request_fields), 'uni', false);
                tmp = [tmp{:}];
                rslt = struct(tmp{:});
            end
        case { 'list', 'cell', 'cellarray'}
            % nothing to do
            rslt_plus = strtrim(tokenize( request_fields, ','));
        otherwise
            error('se_db_estimator:list_output_type', 'output mode <%s> unknown', opt.get('output-type'));
    end
    % <*
    otherwise
        error('gen_query:exec', 'mode <%s> unknown', mode);
end

function str = CELL2STR_SEPARATOR(cell_, sep_)
str = sprintf(['%s' sep_], cell_{:});
str(end-length(sep_)+1:end)='';