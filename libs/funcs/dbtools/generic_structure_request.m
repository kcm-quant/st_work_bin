function out = generic_structure_request(c, req_str, varargin)
% GENERIC_STRUCTURE_REQUEST - Queries results as a struct
%
% After the second argument, you can give some values to be inserted in the
% request via a sprintf
%
% Examples :
% s = generic_structure_request('MARKET_DATA', 'select max(date) as max_date, min(date) as min_date from MARKET_DATA..trading_daily where security_id = 2');
%
% See also exec_sql
% 
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : 'mlasnier@keplercheuvreux.com'
%   date     :  '12/02/2020'

drf = setdbprefs('DataReturnFormat');
cleanup = onCleanup(@()set_pref_back(drf));
setdbprefs('DataReturnFormat', 'structure');
if ~isempty(varargin)
    req_str = sprintf(req_str, varargin{:});
end
out = exec_sql(c, req_str);


function set_pref_back(drf)
setdbprefs('DataReturnFormat', drf);