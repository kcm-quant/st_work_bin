function [Dbid,Dask] = get_Dbid_Dask(data,smallest_tick,tick_opt)
% GET_dBID_dASK - Helper function to get delta bid and delta ask sizes between trades
%
% Inputs:
% - data: st_data coming out of get_tick 
% - smallest_tick: Smallest price increments. Used to transform prices into integers.
% - tick_opt: Flag to use the "presence of limit beyond first limit" option
% (see "Adopted conventions" below)
% 
% Adopted conventions:
% * dBID or dASK not null if previous trade is OVER_BID or OVER_ASK.
% * If the tick_opt flag is true, all the orderbook limits beyond
%   the first limits are supposed to be located on each tick increment away
%   from the first limit and have sizes nanmean(bid_size) and nanmean(ask_size).
%
% see also: read_dataset('tick4ri',...

if nargin < 3
    tick_opt = false;
end

assert(all(ismember({'bid','ask','over_bid','over_ask','bid_size','ask_size'},data.colnames)),...
    'get_Dbid_Dask:MISSING_COLUMNS','The following columns are missing <%s>',...
    join(',',setdiff({'bid','ask','over_bid','over_ask','bid_size','ask_size'},data.colnames)))

[un_td,~,idx_td] = unique(data.value(:,strcmp(data.colnames,'trading_destination_id')));
Dbid = zeros(length(idx_td),1);
Dask = zeros(length(idx_td),1);
for i = 1:length(un_td)
    volume = data.value(idx_td == i,strcmp(data.colnames,'volume'));
    L = length(volume);
    if L < 2
        continue
    end
    bid = round(data.value(idx_td == i,strcmp(data.colnames,'bid'))/smallest_tick);
    ask = round(data.value(idx_td == i,strcmp(data.colnames,'ask'))/smallest_tick);
    over_bid = data.value(idx_td == i,strcmp(data.colnames,'over_bid'))==1;
    over_ask = data.value(idx_td == i,strcmp(data.colnames,'over_ask'))==1;
    bid_size = data.value(idx_td == i,strcmp(data.colnames,'bid_size'));
    ask_size = data.value(idx_td == i,strcmp(data.colnames,'ask_size'));
    post_bid_size = bid_size;
    post_ask_size = ask_size;
    post_bid_size(over_bid) = max(bid_size(over_bid) - volume(over_bid),0);
    post_ask_size(over_ask) = max(ask_size(over_ask) - volume(over_ask),0);
    
    if tick_opt
        tick = pgcd([pgcd(unique(bid)),pgcd(unique(ask))])*smallest_tick;
        usual_bid_size = nanmean(bid_size); %overbid|overask?
        usual_ask_size = nanmean(ask_size); %overbid|overask?
    end
    
    % ... & bid(2:end)<ask(2:end), we need the spread to be > 0
    ask_widening_spread   = find(ask(1:end-1) < ask(2:end) & bid(2:end)<ask(2:end));
    ask_tightening_spread = find(ask(1:end-1) > ask(2:end) & bid(2:end)<ask(2:end));
    bid_widening_spread   = find(bid(1:end-1) > bid(2:end) & bid(2:end)<ask(2:end));
    bid_tightening_spread = find(bid(1:end-1) < bid(2:end) & bid(2:end)<ask(2:end));
    
    temp_Dbid = bid_size(2:end) - post_bid_size(1:end-1);
    temp_Dask = ask_size(2:end) - post_ask_size(1:end-1);
    
    if ~tick_opt
        temp_Dask(ask_widening_spread) = -post_ask_size(ask_widening_spread);
        temp_Dbid(bid_widening_spread) = -post_bid_size(bid_widening_spread);
        temp_Dask(ask_tightening_spread) = ask_size(ask_tightening_spread+1);
        temp_Dbid(bid_tightening_spread) = bid_size(bid_tightening_spread+1);
    else
        delta_ask_nb_ticks = max(floor(abs(ask(2:end)-ask(1:end-1))/tick)*smallest_tick,1);
        delta_bid_nb_ticks = max(floor(abs(bid(2:end)-bid(1:end-1))/tick)*smallest_tick,1);
        
        temp_Dask(ask_widening_spread) = -post_ask_size(ask_widening_spread)-(delta_ask_nb_ticks(ask_widening_spread)-1)*usual_ask_size;
        temp_Dbid(bid_widening_spread) = -post_bid_size(bid_widening_spread)-(delta_bid_nb_ticks(bid_widening_spread)-1)*usual_bid_size;
        temp_Dask(ask_tightening_spread) = ask_size(ask_tightening_spread+1)+(delta_ask_nb_ticks(ask_tightening_spread)-1)*usual_ask_size;
        temp_Dbid(bid_tightening_spread) = bid_size(bid_tightening_spread+1)+(delta_bid_nb_ticks(bid_tightening_spread)-1)*usual_bid_size;
    end
    
    % Unidentified trades are followed by zero Dask and Dbid (By convention).
    temp_Dbid(~over_bid(1:end-1)&~over_ask(1:end-1)) = 0;
    temp_Dask(~over_bid(1:end-1)&~over_ask(1:end-1)) = 0;
    % Cross spreads are not reliable enough to compute Dask and Dbid.
    temp_Dbid(bid(2:end)>=ask(2:end)) = 0;
    temp_Dask(bid(2:end)>=ask(2:end)) = 0;
    
    Dask(idx_td == i) = [0;temp_Dask];
    Dbid(idx_td == i) = [0;temp_Dbid];
end


function str = join(separator,list)
if isempty(list)
    str = '';
    return
end
if isnumeric(list)
    str = sprintf(['%d',separator],list);
elseif iscellstr(list)
    str = sprintf(['%s',separator],list{:});
else
    error('join:wronginputargs','Second argument should be either numeric or cellstr')
end
str(end-length(sprintf(separator))+1:end) = [];