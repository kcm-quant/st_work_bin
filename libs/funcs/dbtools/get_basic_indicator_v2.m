function data = get_basic_indicator_v2(mode, varargin)
% GET_BASIC_INDICATOR_V2 - Short_one_line_description
%
%
%
%
% Examples:
% opt = options({ 'window:time', datenum(0,0,0,0,15,0),... % the sliding windows' width in matlab time format
%             'step:time', datenum(0,0,0,0,15,0), ...  % duration between any two rows
%             't_acc:time', datenum(0,0,0,0,11,0), ... % test for trading hours inconsistencies, the higher the less exigent are the test. Nanfor no tests
%             'bins4stop:b', true, ...                 % if true, you do not want to aggregates volatility stop fixing without continuous phase but rather have a separate bin
%             'source:char', 'tick4bi', ...            % controls the source of aggregation. tick4bi the only one for now
%             'output-mode', 'all', ...                % ['all']/'day' in the first case you get only one st_data, in the second case, a cell array of st_data, one cell for each calendar day
%             'context_selection:char', 'null', ...    % ['null']/'contextualised'/'span_all' see trading_time_interpret 'contextualised' will give you heterogenuous data from one day to another because it will take into account the specific trading hours of this day, 'span_all' will try to take every phases that can exist any day and take min of begins and max of ends
%             'ind_set:char', 'std', ...               % ['std']/'small_set'/'bench_price' the set of indictors you want to retrieve
%             'grid_mode', 'time_and_phase', ...       % ['time_and_phase']/'time' the mode of make_grid that should be usedd
%             });
%
%  d = read_dataset('gi:basic_indicator', 'security_id', 110, 'trading-destinations', {}, 'from', '23/12/2010', 'to', '24/12/2010')
%  d.attach_format = 'HH:MM';st_plot(st_data('keep', d, 'volume')); % => no data for Christmas as the hald day has been filtered
%  close all;
%
%  opt = options({'context_selection:char', 'contextualised'})
%  d = read_dataset(['gi:basic_indicator/' opt2str(opt)], 'security_id', 110, 'trading-destinations', {}, 'from', '23/12/2010', 'to', '24/12/2010')
%  d.attach_format = 'HH:MM';st_plot(st_data('keep', d, 'volume')); % => This time there are some data on a time grid adapted to this specific day
%  close all;
%
% % To understnad the following exemple, you have to know that 15/07/2011
% % is a derivatives expiry date and on these dates, there is an extra
% % intraday auction that do not exist on other days
% d = read_dataset('gi:basic_indicator', 'security_id', 12058, 'trading-destinations', {}, 'from', '15/07/2011', 'to', '15/07/2011')  % => is empty because an unexpected auction happens
% d.info.th_event
% assert(st_data('isempty-nl', d))
% opt = options({'context_selection:char', 'contextualised'})
% d = read_dataset(['gi:basic_indicator/' opt2str(opt)], 'security_id', 12058, 'trading-destinations', {}, 'from', '15/07/2011', 'to', '15/07/2011')  % => is not empty because thanks to contextualisation we know there is an intraday_auction because of derivative expiry
% d.info.th_event
% assert(sum(st_data('col', d, 'auction'))==3)
% opt = options({'context_selection:char', 'span_all'})
% d = read_dataset(['gi:basic_indicator/' opt2str(opt)], 'security_id', 12058, 'trading-destinations', {}, 'from', '14/07/2011', 'to', '14/07/2011')
% d.info.th_event
% d2 = read_dataset(['gi:basic_indicator/' opt2str(opt)], 'security_id', 12058, 'trading-destinations', {}, 'from', '15/07/2011', 'to', '15/07/2011')
% d2.info.th_event
% assert((~st_data('isempty-nl', d)) && (length(d.date)==length(d2.date)) && (st_data('col', st_data('from-idx', d, logical(st_data('col', d, 'intraday_auction'))), 'volume')==0))
%
% opt = options({'context_selection:char', 'contextualised'})
%  d = read_dataset(['gi:basic_indicator/' opt2str(opt)], 'security_id', 12058, 'trading-destinations', {}, 'from', '15/07/2011', 'to', '15/07/2011') % => this time there is some ...
%
%  opt = options({'grid_mode', {'time'}, 'ind_set:char', 'small_set'})
%  d = read_dataset(['gi:basic_indicator/' opt2str(opt)], 'security_id', 110, 'trading-destinations', {}, 'from', '23/12/2010', 'to', '24/12/2010')
%  d.attach_format = 'HH:MM';st_plot(st_data('keep', d, 'volume')); % => This time there are some data on a time grid adapted to this specific day
%  close all;
%
% Getting today's data :
% opt = options({ 'window:time', datenum(0,0,0,0,15,0),... % the sliding windows' width in matlab time format
%               'step:time', datenum(0,0,0,0,15,0), ...  % duration between any two rows
%               't_acc:time', datenum(0,0,0,0,11,0), ... % test for trading hours inconsistencies, the higher the less exigent are the test. Nanfor no tests
%               'bins4stop:b', false, ...                 % if true, you do not want to aggregates volatility stop fixing without continuous phase but rather have a separate bin
%               'today_allowed', true});
%           
% d = read_dataset(['gi:basic_indicator/' opt2str(opt)], 'security_id', 110, ...
%     'trading-destinations', {}, 'from', today, ...
%     'to',today)
% % but we just don't care about the date you asked : 
% d = read_dataset(['gi:basic_indicator/' opt2str(opt)], 'security_id', 110, ...
%     'trading-destinations', {}, 'from', 0, ...
%     'to',NaN)
% % or if you don't event ask for a day :
% d = read_dataset(['gi:basic_indicator/' opt2str(opt)], 'security_id', 110, ...
%     'trading-destinations', {})
% 
%
% opt = options({ 'window:time', datenum(0,0,1,0,0,0),...
%             'step:time', datenum(0,0,1,0,0,0), ...  
%             't_acc:time', datenum(0,0,0,0,11,0), ...
%             });
% d = read_dataset(['gi:basic_indicator/' opt2str(opt)], 'security_id', 110, 'trading-destinations', {}, 'from', '24/05/2013', 'to', '24/05/2013')
% st_data_explorer(d)
%
% See also: read_dataset, st_data
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : 'athabault@cheuvreux.com'
%   version  : '1'
%   date     :  14/01/2011

global st_version

if isfield(st_version.my_env, 'switch_full_tick2tbt2') && st_version.my_env.switch_full_tick2tbt2
    error('get_basic_indictor_v2:check_init_env', ...
        'Forbidden st_version.my_env.switch_full_tick2tbt2 = 1 as long as this allow for false empty data instead of errors.')
end

NB_SEC_SMALLEST_STEP = 30; % Any modification would imply also something to change in candles30sec
NB_DAYS_BLOCK_COMPUTE = 60;

% < KP_HACK : if no kingpin specified and we want daily data, then 
% use kingpin = 00:00:00 => there is more todo : this creates an empty bin
% at the begining of the day
idx_kingpin = find(strcmp('kingpin:time', varargin((strcmp('masterkey', mode)+1):2:end)), 1);
% >

switch lower(deblank(mode))
    case 'masterkey'
        internal_buffer = []; %if asked for a master_key this means that
        % remaining data in the buffer (if any wont ever be used)
        
        %<* Return the generic+specific dir
        fmode = varargin{1};
        fmode = regexprep( fmode, '[ |*|\\|/|:|?|"|<|>|\|]', '_');
        opt4gi = options({'security_id', NaN, ...
            'window:time', datenum(0,0,0,0,15,0),...
            'step:time', datenum(0,0,0,0,15,0), ...
            't_acc:time', datenum(0,0,0,0,11,0), ...
            'bins4stop:b', false, ...
            'bins4pa:b', false,...
            'context_selection:char', 'null', ...
            'source:char', 'tick4bi', ...
            'ind_set:char', 'std', ...
            'grid_mode', 'time_and_phase', ...
            'kingpin:time', datenum(0,0,0,12,0,0), ...
            }, varargin(2:end));
        
        assert(ismember(opt4gi.get('source:char'),{'tick4bi','get_full_candles','tick4ri','tick4bi+junk_lse_data'}),...
            'get_basic_indicator:check_args',...
            'MODE: Not yet implemented for other sources than ''tick4bi'',''get_full_candles'',''tick4ri''');
        
        sec_id = opt4gi.remove('security_id');
        if ~isnumeric( sec_id) && numel(sec_id)==1
            error('get_tick:secid', 'I need an unique numerical security id');
        end
        switch varargin{1}
            case 'basic_indicator'
                sec_dt = 1/(24*3600);
                td = opt4gi.remove('trading-destinations');
                if ~isempty(td) && ((iscell(td) && strcmpi(td{1},'MAIN')) || (ischar(td) && strcmpi(td,'MAIN')))
                    td = get_repository('maintd',sec_id);
                end
                opt4gi.remove('from');
                opt4gi.remove('to');
                %< getting values of handled parameters
                ta = opt4gi.remove('t_acc:time');
                w = opt4gi.remove('window:time');
                st = opt4gi.remove('step:time');
                s = opt4gi.remove('source:char');
                b = opt4gi.remove('bins4stop:b');
                pa = opt4gi.remove('bins4pa:b');
                c = opt4gi.remove('context_selection:char');
                is = opt4gi.remove('ind_set:char');
                gm = opt4gi.remove('grid_mode');
                if ischar(gm) && strcmp(gm, 'time_and_phase')
                    gm_str = '';
                else
                    gm_str = hash(convs('safe_str', gm),'MD5');
                end
                kp = opt4gi.remove('kingpin:time');
                % < KP_HACK : if no kingpin specified and we want daily data, then 
                % use kingpin = 00:00:00
                if isempty(idx_kingpin) && abs(w-1)<1/(24*3600) ...
                        && abs(st-1)<1/(24*3600) 
                    kp = 0;
                end
                % >
                if kp ~= datenum(0,0,0,12,0,0)
                    add_cellstr = {['kingpin=', datestr(round(kp*24*3600)/(24*3600), 'HH_MM_SS')]};
                else
                    add_cellstr = {};
                end
                %>
                
                %                 if ~opt4gi.isempty()
                %                     lst = opt4gi.get();
                %                     error('set_params_basic_indicator:masterkey:check_args', ...
                %                         'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
                %                 end
                
                data = fullfile(fmode, get_repository('kech-security-td-key',sec_id,td), ...
                    ['t_acc=' num2str(round(ta/sec_dt)) 's'], ...
                    ['width=' num2str(round(w/sec_dt)) 's'], ...
                    ['step=' num2str(round(st/sec_dt)) 's'], ...
                    ['source=' s], ...
                    ['bins4stop=' num2str(b)], ...
                    ['bins4pa=' num2str(pa)], ...
                    ['context_sel=' c], ...
                    ['ind_set=' is], gm_str, add_cellstr{:});
                
            case 'smallest_step'
                data = fullfile(fmode, get_repository( 'kech-security-key', sec_id));
            otherwise
                error('get_basic_indicator:check_args', 'MODE: Unknown mode <%s>', varargin{1})
        end
        %>*
    case 'basic_indicator'
        idx_day = find(strcmp('day', varargin(1:2:end)));
        % < We will compute 6 months at a call, not more
        idx_from = find(strcmp('from', varargin(1:2:end)));
        idx_to = find(strcmp('to', varargin(1:2:end)));
        if isnumeric(varargin{2*idx_from}) && length(varargin{2*idx_from}) > 1 % in case some specific dates are asked
            dt_day = datenum(varargin{2*idx_day}, 'dd/mm/yyyy');
            dt_from = varargin{2*idx_from}(varargin{2*idx_from}>=dt_day);
            dt_from = dt_from(1:min(NB_DAYS_BLOCK_COMPUTE, end));
            dt_to = dt_from(end);
            varargin{2*idx_from} = dt_from;
            varargin{2*idx_to} = dt_to;
        else % this is a really from to period but for now, from_buffer is only asking "day"
            varargin{2*idx_from} = varargin{2*idx_day};
            varargin{2*idx_to} = datestr(min(datenum(varargin{2*idx_to}, 'dd/mm/yyyy'), ...
                datenum(varargin{2*idx_day}, 'dd/mm/yyyy')+NB_DAYS_BLOCK_COMPUTE), 'dd/mm/yyyy');
        end
        % >
        
        
        opt = options({ 'window:time', datenum(0,0,0,0,15,0),...
            'step:time', datenum(0,0,0,0,15,0), ...
            't_acc:time', datenum(0,0,0,0,11,0), ...
            'bins4stop:b', false, ...
            'bins4pa:b', false, ...
            'source:char', 'tick4bi', ...
            'output-mode', 'all', ...
            'context_selection:char', 'null', ...
            'ind_set:char', 'std', ...
            'grid_mode', 'time_and_phase', ...
            'kingpin:time', datenum(0,0,0,12,0,0), ...
            'force_unique_intraday_auction:b',true,...
            'convert_ifix2_to_ifix1:b',false,...
            }, varargin);
        
        width           = opt.get('window:time');
        step            = opt.get('step:time');
        cs              = opt.get('context_selection:char');
        kp              = opt.get('kingpin:time');
        fuia            = opt.get('force_unique_intraday_auction:b');
        ifix2_to_ifix1  = opt.get('convert_ifix2_to_ifix1:b');
        
        % < KP_HACK : if no kingpin specified and we want daily data, then
        % use kingpin = 00:00:00
        if isempty(idx_kingpin) && abs(width-1)<1/(24*3600) ...
                && abs(step-1)<1/(24*3600)
            kp = 0;
        end
        % >
        
        %< Data source and financial intrument type management
        switch opt.get('source:char')
            case 'tick4bi+junk_lse_data'
                if strcmp(varargin{2*idx_from}, varargin{2*idx_to}) ...
                        && strcmp(varargin{2*idx_from}, datestr(today, 'dd/mm/yyyy'))
                    data2agg = get_tick('ft', varargin{:}, 'today_allowed', true);
                    data2agg.info.data_datestamp = today;
                    data2agg = {st_data('where', data2agg, '~({cross}|{trading_at_last}|{trading_after_hours})')};
                elseif width > NB_SEC_SMALLEST_STEP/(24*3600) && step > NB_SEC_SMALLEST_STEP/(24*3600)
                    data2agg = from_buffer('get_basic_indicator_v2', 'smallest_step', ...
                        varargin{:}, 'output-mode', 'day', ...
                        'where_formula', '~{cross}&~between({phase},7,9)'); % todo MAGIc number phase < 7 excludes trading_at_last and_after_hours
                else
                    data2agg = from_buffer('get_tick', 'ft', varargin{:}, 'output-mode', 'day', ...
                        'where_formula', '~({cross}|{trading_at_last}|{trading_after_hours})'); % todo MAGIc number phase < 7 excludes trading_at_last and_after_hours
                end
                data2agg = st_data('filter-td', data2agg, opt.get('trading-destinations'));
                if isfield(data2agg{1},'info') && isfield(data2agg{1}.info,'td_info') &&...
                        data2agg{1}.info.td_info(1).trading_destination_id==34
                    idx_lse = between(floor(mod(data2agg{1}.date,1)*24*60),8*60,16*60+35);
                    data2agg{1} = st_data('from-idx',data2agg{1},idx_lse);
                end
            case 'tick4bi'
                % HELP MEMORY : if you are stuck here because of Out of
                % memory, then decreasing NB_DAYS_BLOCK_COMPUTE might be a
                % solution
                if strcmp(varargin{2*idx_from}, varargin{2*idx_to}) ...
                        && strcmp(varargin{2*idx_from}, datestr(today, 'dd/mm/yyyy'))
                    data2agg = get_tick('ft', varargin{:}, 'today_allowed', true);
                    data2agg.info.data_datestamp = today;
                    data2agg = {st_data('where', data2agg, '~({cross}|{trading_at_last}|{trading_after_hours})')};
                elseif width > NB_SEC_SMALLEST_STEP/(24*3600) && step > NB_SEC_SMALLEST_STEP/(24*3600)
                    data2agg = from_buffer('get_basic_indicator_v2', 'smallest_step', ...
                        varargin{:}, 'output-mode', 'day', ...
                        'where_formula', '~{cross}&~between({phase},7,9)'); % todo MAGIc number phase < 7 excludes trading_at_last and_after_hours
                else
                    data2agg = from_buffer('get_tick', 'ft', varargin{:}, 'output-mode', 'day', ...
                        'where_formula', '~({cross}|{trading_at_last}|{trading_after_hours})'); % todo MAGIc number phase < 7 excludes trading_at_last and_after_hours
                end
                % < filter trading-destinations
                data2agg = st_data('filter-td', data2agg, opt.get('trading-destinations'));
                % >
                %                 data2agg = get_basic_indicator_v2('simep_candles', varargin{:});
                %                 for i = 1 : length(data2agg)
                %                     data2agg{i} = st_data('where', data2agg{i}, '~{cross}&({phase}<7)'); % phase < 7 excludes trading_at_last and_after_hours
                %                 end
            case {'tick4resilience_imbalance','tick4ri'}
                data2agg = read_dataset('tick4ri',varargin{:},'output-mode','day');
            case 'get_full_candles' % Circuit-breaker to get aggregate data from get_full_candles
                if width >= 60/(24*3600) && step >= 60/(24*3600) % Mode valid iff width and step > 60 sec
                    data_candles = {get_candles_1min(varargin{:})};
                else
                    data_candles = from_buffer('get_tick', 'ft', varargin{:}, 'output-mode', 'day', ...
                        'where_formula', '~({cross}|{trading_at_last}|{trading_after_hours})'); % todo MAGIc number phase < 7 excludes trading_at_last and_after_hours
                end
                % recopie du code de smallest-step
                
                [data2agg, ~, idx_dumped] = ... % yes ~ won't work with matlab version < 2010a and this is intended : please upgrade matlab
                    st_data('agg', data_candles, ...
                    'colnames',{'volume', 'turnover', 'nb_trades', 'volume_sell', ...
                    'turnover_sell', 'nb_sell', 'vwas', 'open','high','low','close', ...
                    'bid_open', 'bid_high', 'bid_low','bid_close', ...
                    'ask_open', 'ask_high', 'ask_low','ask_close', ...
                    'sum_price', 'time_close', 'time_open'},...
                    'no_empty_bins', true, ...
                    'grid_mode', { 'time',...
                    'phase', ...
                    'dark', ...
                    'cross',...
                    'trading_destination_id'},...
                    'grid_options', ...
                    {'time_type', 'sec',...
                    'window', 60, ...
                    'step', 60,...
                    'kingpin', datenum(0,0,0,12,0,0),...
                    'exceed_begin', true, ...
                    'exceed_end', true, ...
                    }...
                    );
                if any(idx_dumped{1})
                    error('get_basic_indicator_V2:check_exec', 'Some idx have been thrown away');
                end
                data2agg = data2agg{1};
                if isempty(data2agg)
                    data2agg = st_data('empty-init');
                end
                data2agg.info.data_datestamp = datenum(varargin{2*idx_day}, 'dd/mm/yyyy');
                data2agg = st_data('filter-td', data2agg, opt.get('trading-destinations'));
                data2agg = {data2agg};
            otherwise
                error('get_basic_indicator:check_args', ...
                    'MODE: Not yet implemented for other sources than ''tick4bi'''); % TODO
        end
        %>
        
        
        %< aggr?gation
        ind_set = opt.get('ind_set:char');
        switch ind_set
            case 'std'
                rc_cols = {'volume', 'vwap', 'nb_trades', ...
                    'vwas', 'vol_GK', 'volume_sell', 'vwap_sell', 'nb_sell', ...
                    ...'volume_buy', 'vwap_buy', 'nb_buy',...
                    'open','high','low','close', ...
                    'bid_open', 'bid_high', 'bid_low','bid_close', ...
                    'ask_open', 'ask_high', 'ask_low','ask_close', ...
                    'auction', 'opening_auction', 'closing_auction', ...
                    'intraday_auction', 'stop_auction','periodic_auction',...
                    'mean_price',...
                    'time_open','time_close'};
            case 'std+phase'
                rc_cols = {'volume', 'vwap', 'nb_trades', ...
                    'vwas', 'vol_GK', 'volume_sell', 'vwap_sell', 'nb_sell', ...
                    'open','high','low','close', ...
                    'bid_open', 'bid_high', 'bid_low','bid_close', ...
                    'ask_open', 'ask_high', 'ask_low','ask_close', ...
                    'auction', 'opening_auction', 'closing_auction', ...
                    'intraday_auction', 'stop_auction', 'mean_price',...
                    'time_open','time_close','phase'};
            case 'std+dark'
                rc_cols = {'volume', 'vwap', 'nb_trades', ...
                    'vwas', 'vol_GK', 'volume_sell', 'vwap_sell', 'nb_sell', ...
                    'open','high','low','close', ...
                    'bid_open', 'bid_high', 'bid_low','bid_close', ...
                    'ask_open', 'ask_high', 'ask_low','ask_close', ...
                    'auction', 'opening_auction', 'closing_auction','volume_dark','turnover_dark', ...
                    'intraday_auction', 'stop_auction', 'mean_price', 'time_open','time_close',...
                    'nb_trades_dark'};
            case 'small_set'
                rc_cols = {'volume', 'vwap', 'nb_trades', ...
                    'vwas', 'vol_GK', 'time_open','time_close', 'phase'};
            case 'bench_price'
                rc_cols = {'volume', 'vwap', 'nb_trades', ...
                    'vwas', 'vol_GK', 'time_open','time_close', 'phase', ...
                    'open','high','low','close','twap', 'mean_price'};
            case 'imbalance_resilience'
                rc_cols = {'vwap','volume','open','close','high','low','imbalance_numer',...
                    'imbalance_denom','replenishment_numer','replenishment_denom',...
                    'imbalance_rel','replenishment_rel','imbalance_abs','replenishment_abs',...
                    'vol_GK','nb_trades'};
            case 'std+td_id+dark'
                rc_cols = {'volume', 'vwap', 'nb_trades', ...
                    'vwas', 'vol_GK', 'volume_sell', 'vwap_sell', 'nb_sell', ...
                    ...'volume_buy', 'vwap_buy', 'nb_buy',...
                    'open','high','low','close', ...
                    'bid_open', 'bid_high', 'bid_low','bid_close', ...
                    'ask_open', 'ask_high', 'ask_low','ask_close', ...
                    'auction', 'opening_auction', 'closing_auction', ...
                    'volume_dark','turnover_dark','nb_trades_dark',...
                    'intraday_auction', 'stop_auction','periodic_auction',...
                    'mean_price',...
                    'time_open','time_close','trading_destination_id'};
            case 'std+td_id'
                rc_cols = {'volume', 'vwap', 'nb_trades', ...
                    'vwas', 'vol_GK', 'volume_sell', 'vwap_sell', 'nb_sell', ...
                    ...'volume_buy', 'vwap_buy', 'nb_buy',...
                    'open','high','low','close', ...
                    'bid_open', 'bid_high', 'bid_low','bid_close', ...
                    'ask_open', 'ask_high', 'ask_low','ask_close', ...
                    'auction', 'opening_auction', 'closing_auction', ...
                    'intraday_auction', 'stop_auction','periodic_auction',...
                    'mean_price',...
                    'time_open','time_close','trading_destination_id'};
            otherwise
                error('get_basic_indicator_v2:check_args', 'Unknown set of indicators : <%s>', ind_set);
        end
        
        gm = opt.get('grid_mode');
        b4s  = opt.get('bins4stop:b');
        b4pa = opt.get('bins4pa:b');
        if ischar(gm) && strcmp(gm, 'time_and_phase')
            add_opt = {'vol_stop_slice', b4s, ...
                'periodic_auction', b4pa, ...
                'try2correct_auction_flag', false, ... datenum(0,0,0,0,15,0)*(~b4s), ... % this means that if you dont want specific slices for volatility fixing, then we'll try to identify if this fixing was a regular one, badly flagged
                'context_selection', cs,...
                'force_unique_intraday_auction',fuia,'convert_ifix2_to_ifix1',ifix2_to_ifix1};
        else
            add_opt = {};
        end
        [data, ~, idx_dumped] =...   % yes ~ won't work with matlab version < 2010 a and this is intended : please upgrade matlab
            st_data('agg', data2agg, ...
            'colnames', rc_cols(~strcmp(rc_cols,'phase')), ... Pour �viter le doublement de 'phase', qui est ajout� par d�faut
            'grid_mode', gm,...
            'grid_options', ...
                [...
                    {'time_type', 'matlab',...
                    'window', opt.get('window:time'), ...
                    'step', opt.get('step:time'),...
                    'kingpin', kp},add_opt
                ]...
            );
        %>
        
        %< checking emptiness once for all
        ind_nonempty = find(cellfun(@(c)~st_data('isempty-nl', c), data));
        [n, k] = size(ind_nonempty);
        if n > k
            ind_nonempty = ind_nonempty';
        end
        %>
        
        %<* Inconsistencies in trading time filters if asked by user =
        % t_acc:time finite
        t_acc = opt.get('t_acc:time');
        ind_forced_empty = [];
        if ischar(gm) && strcmp(gm, 'time_and_phase') ...
                && isfinite(t_acc) && ~isempty(ind_nonempty)
            [~,phase_id, ~, phase_id2phase_name, extract_phase] = trading_time_interpret([],[]);
            
            % <* Filter for unexpected phases or out of expected time data
            % in a fixing phase
            message_log_unexpected = sprintf('There is a problem with an unexpected auction \n\tAs a consequence this day will be deleted from basic_indicator.\n');
            message_log_phase_time = 'There is a problem with the hours of an auction (%s) theoretical_times =[%s,%s] observed_times =[%s,%s]\n\tAs a consequence this day will be deleted from basic_indicator.\n';
            i = ind_nonempty(1);
            [vals, ind] = st_data('cols', data{i}, {'phase','time_open','time_close'});
            phase = vals(:, 1); ind_phase = ind(1);
            bs    = vals(:, 2); ind_bs    = ind(2);
            es    = vals(:, 3); ind_es    = ind(3);
            
            %< just the same as below in the for loop but enable not to
            %search for the position of phase col at each loop step : done
            %once
            expected_phases_id = [data{i}.info.phase_th.phase_id];
            [observed_phases_id, ind_phase_in_data]  = unique(phase);
            idx2del = ismember(observed_phases_id, [phase_id.VOL_FIXING, phase_id.CONTINUOUS, phase_id.PERIODIC_AUCTION]);
            observed_phases_id(idx2del) = [];
            ind_phase_in_data(idx2del)  = [];
            
            if ~all(ismember(observed_phases_id, expected_phases_id))
                ind_forced_empty = cat(2, ind_forced_empty, i);
                data{i} = double_log(data{i}, message_log_unexpected);
            elseif (~strcmp(cs, 'span_all'))
                for j = 1 : length(observed_phases_id)
                    p = extract_phase(data{i}.info.phase_th, phase_id2phase_name(observed_phases_id(j)));
                    if mod(bs(ind_phase_in_data(j)), 1) < p.begin-t_acc || ...
                            mod(es(ind_phase_in_data(j)), 1) > p.end+t_acc
                        ind_forced_empty = cat(2, ind_forced_empty, i);
                        data{i} = double_log(data{i}, sprintf(message_log_phase_time, ...
                            p.phase_name, ...
                            datestr_ms(p.begin),datestr_ms(p.end), ...
                            datestr_ms(bs(ind_phase_in_data(j))),...
                            datestr_ms(es(ind_phase_in_data(j)))));
                    end
                end
            end
            %>
            
            for i = ind_nonempty(2:end)
                phase = data{i}.value(:, ind_phase);% this is possible thanks to the fact that st_data('agg' will return homogenuous colnames
                bs    = data{i}.value(:, ind_bs);
                es    = data{i}.value(:, ind_es);
                % just a copy-paste of what is above
                expected_phases_id = [data{i}.info.phase_th.phase_id];
                [observed_phases_id, ind_phase_in_data]  = unique(phase);
                idx2del = observed_phases_id==phase_id.VOL_FIXING;
                observed_phases_id(idx2del) = [];
                ind_phase_in_data(idx2del)  = [];
                
                if ~all(ismember(observed_phases_id, expected_phases_id))
                    ind_forced_empty = cat(2, ind_forced_empty, i);
                    data{i} = double_log(data{i}, message_log_unexpected);
                elseif (~strcmp(cs, 'span_all'))
                    for j = 1 : length(observed_phases_id)
                        p = extract_phase(data{i}.info.phase_th, phase_id2phase_name(observed_phases_id(j)));
                        if mod(bs(ind_phase_in_data(j)), 1) < p.begin-t_acc || ...
                                mod(es(ind_phase_in_data(j)), 1) > p.end+t_acc
                            ind_forced_empty = cat(2, ind_forced_empty, i);
                            data{i} = double_log(data{i}, sprintf(message_log_phase_time, ...
                                p.phase_name, ...
                                datestr_ms(p.begin),datestr_ms(p.end), ...
                                datestr_ms(bs(ind_phase_in_data(j))),...
                                datestr_ms(es(ind_phase_in_data(j)))));
                        end
                    end
                end
            end
            % >*
            
            ind_nonempty = setdiff(ind_nonempty, ind_forced_empty);
            ind_forced_empty = [];
            
            % < Filter for "too much dumped data" because of "it was out of
            % trading_time"
            for i = ind_nonempty
                if any(idx_dumped{i})
                    % < How much volume did we dump?
                    volume = strcmp('volume', data2agg{i}.colnames); % we cannot assume that the cols will allways be in the same order
                    volume = data2agg{i}.value(:, volume);
                    dumped_prop = sum(volume(idx_dumped{i}))/sum(volume);
                    % >
                    
                    % < what is the duration of a day?
                    ind_cont_inphase_th = find([data{i}.info.phase_th.phase_id] == phase_id.CONTINUOUS, 1);
                    if ~isempty(ind_cont_inphase_th)
                        trading_day_duration = data{i}.info.phase_th(ind_cont_inphase_th).end ...
                            - data{i}.info.phase_th(ind_cont_inphase_th).begin;
                    else
                        trading_day_duration = max([data{i}.info.phase_th.end]) ...
                            - min([data{i}.info.phase_th.begin]);
                    end
                    % >
                    
                    % < Is the proportion of dumped volume greater than the
                    % ratio of accuracy divided by trading day duration?
                    if dumped_prop > t_acc/trading_day_duration
                        message_log = sprintf(['Please have a closer look to day :<%s> for <%s>. \n\t' ...
                            'Have a special look to continuous phase trading hours %f %% of ' ...
                            'the volume is out of what we thought to be the hours of the continuous phase.\n\t' ...
                            'As a consequence this day will be deleted from basic_indicator.\n'], ...
                            datestr(floor(data2agg{i}.date(1))), data2agg{i}.title, 100 * dumped_prop);
                        ind_forced_empty = cat(2, ind_forced_empty, i);
                        data{i} = double_log(data{i}, message_log);
                    end
                    % >
                end
            end
            % >
        end
        %>*
        
        ind_nonempty = setdiff(ind_nonempty, ind_forced_empty);
        
        %< keeping only the same cols as in basic_indicator
        if ~isempty(ind_nonempty) && ischar(ind_set) && strcmp(ind_set, 'std')
            full_cols = cat(2, rc_cols, 'begin_slice', 'end_slice');
            [data{ind_nonempty(1)}, ind_cols] = st_data('keep', data{ind_nonempty(1)}, full_cols);
            for i = ind_nonempty(2:end)
                data{i}.value = data{i}.value(:, ind_cols); % this is possible thanks to the fact that st_data('agg' will return homogenuous colanmes
                data{i}.colnames = full_cols;
            end
        end
        %>
        
        %< Si on a pas output-mode = 'day' alors on empile les donn?es
        if ~strcmp(opt.get('output-mode'), 'day')
            data = st_data('fast-stack', data);  % this is possible thanks to the fact that st_data('agg' will return homogenuous colanmes
        end
        %>
    case 'simep_candles'
        opt = options(varargin);
        
        sec_id = opt.get('security_id');
        from_  = opt.get('from');
        to_    = opt.get('to');
        
        %< destination sans avoir ? d?termnier ici la liste des destinations
        tdi = get_repository('tdinfo', sec_id);
        %>
        %< r?cup?ration du RIC primaire TODO changer cela pour demander un
        %security_id
        %>
        ric_primary = exec_sql('BSIRIUS', sprintf(['select reference from repository..security_source ' ...
            ' where security_id = %d and trading_destination_id = %d and source_id = 2'], sec_id, tdi(1).trading_destination_id));
        
        if length(ric_primary) ~= 1
            error('get_basic_indicator_v2:simep_candles', 'REPOSITORY: NO primary ric or more than one');
        end
        ric_primary = ric_primary{1};
        
        td_spec = opt.get('trading-destinations');
        if isempty(td_spec)
            td_list = sprintf('%d,', [tdi.trading_destination_id]);
            td_list(end) = [];
        elseif isnumeric(td_spec)
            if length(td_spec) > 1
                error(); % as the path in simep repository will be all_td
            end
            td_list = sprintf('%d,', td_spec);
            td_list(end) = [];
            tdi = tdi([tdi.trading_destination_id]==td_spec);
        elseif length(td_spec) == 1 && strcmpi(td_spec{1}, 'main')
            td_list = num2str(tdi(1).trading_destination_id);
            tdi = tdi(1);
        elseif length(td_spec) == 1
            idx2keep = strcmpi(td_spec, {tdi.short_name});
            tdi = tdi(idx2keep);
            td_list = num2str(tdi.trading_destination_id);
        else
            error(); % as the path iun simep repository will be all_td
            %             tdnames = {tdi.short_name};
            %             id2keep = ismember(lower({tdi.short_name}), lower(td_spec));
        end
        
        [status, result] = system(sprintf('%s %s %s %s %s,%s 00:00:10:000000 %d', ...
            st_version.my_env.python_home, ...
            fullfile(st_version.my_env.st_sim, 'simep', 'scenarii', 'candles_generator.py'), ...
            ric_primary, ...
            td_list, ...
            from_([7:10 4:5 1:2]), ... % from dd/mm/yyyy to yyyymmdd
            to_([7:10 4:5 1:2]), ...% from dd/mm/yyyy to yyyymmdd
            st_version.my_env.cluster_size_per_worker));
        if status
            error('get_basic_indicator_v2:simep_candles', 'Error Message from simep : <%s>', result);
        end
        
        
        sched = datenum(from_, 'dd/mm/yyyy'):datenum(to_, 'dd/mm/yyyy');
        if length(tdi) > 1
            td_path = 'all';
        else
            td_path = sprintf('%1.3d', tdi.trading_destination_id);
        end
        rep = fullfile(st_version.my_env.st_repository, 'simep_scenarii', 'ROB', 'CandleAgent', [strrep(ric_primary, '.', '_') '_' td_path]);
        filen = '72e022fdee361bd52e370e43157248d6.mat'; % TODO better
        fs = filesep;
        data = cell(length(sched), 1);
        info = struct('security_id', sec_id, ...
            'security_key', get_repository( 'security-key', sec_id), ...
            'td_info', tdi , ...
            'localtime', true, 'data_log', [], 'data_datestamp', NaN);
        
        if ~tdi(1).localtime
            sched_offset = timezone('get_offset','initial_timezone','GMT','final_timezone', tdi(1).timezone, 'dates',sched);
        else
            sched_offset = zeros(size(sched));
        end
        
        for i = 1 : length(sched)
            try
                data{i} = load([rep fs 'DAY_' datestr(sched(i), 'yyyymmdd') fs filen]);
                data{i} = data{i}.A000;
                data{i}.date = data{i}.date + sched_offset(i) + sched(i);
            catch ME
                %                 st_log(text_to_printable_text(se_stack_error_message(ME)));
                data{i} = st_data('empty-init');
            end
            data{i}.info = info;
            data{i}.info.data_datestamp = sched(i);
        end
        
    case 'smallest_step'
%         server_market_data = get_server_from_mnemonic(st_version.my_env.target_name,st_version.bases.market_data);
%         server_market_data_ref = get_server_from_mnemonic('production',st_version.bases.market_data);
%          if ~strcmp(server_market_data,server_market_data_ref)
%             error('get_tick:check', 'You should be connected to production server.');
%          end
%          c = userdatabase( 'sqlserver', 'MARKET_DATA');
%          a=tokenize(c.URL, '/:');
%          if ~ strcmp(a{4}, 'KECH-DB03') 
%              %~( strcmp(a{4}, '172.29.100.64') || strcmp(a{4}, 'LUIDBQ01') || strcmp(a{4}, 'LUIDBC01') )
%              error('get_tick:check', 'You should be connected to production server.');
%          end
        opt = options({'day', NaN, 'security_id', NaN, 'date_format:char', 'dd/mm/yyyy'}, varargin);
        format_date = opt.get('date_format:char');
        sec_id = opt.get('security_id');
        if ~isnumeric( sec_id) && numel(sec_id)==1
            error('get_tick:secid', 'I need an unique numerical security id');
        end
%         td_info = get_repository( 'trading-destination-info', sec_id);
        day_    = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        if ismember(weekday(day_), [1 7])
            data = [];
            return;
        end
%         
%         data_in_td_daily = exec_sql('MARKET_DATA', ['select * from MARKET_DATA..trading_daily' td_info(1).global_zone_suffix ...
%             ' where security_id = ' num2str(sec_id) ' and date = ''' datestr(day_, 'yyyymmdd') ''' ']);
%         
%         if isempty(data_in_td_daily)
%             data = [];
%             return;
%         else
%             error('TICK:get_basic_indicator_v2 samllest_step check', 'You are not authorized to perform this request.');
%         end
%         
        idx_from = find(strcmp('from', varargin(1:2:end)));
        idx_day  = find(strcmp('day', varargin(1:2:end)));
        if ~isempty(idx_from)
            idx_to   = find(strcmp('to', varargin(1:2:end)));
            varargin{2*idx_to} = varargin{2*idx_day};
            varargin{2*idx_from} = varargin{2*idx_day};
        end
        %         if st_version.my_env.retrieve_full_tick
        %             try
        data_ft = read_dataset('ft', varargin{:});
%                     catch ME
%                         if ( strcmp(ME.identifier, 'MATLAB:unassignedOutputs') && ...
%                                 strcmp(ME.message, sprintf(['Error using ==> system_dependent\n' ...
%                                 'One or more output arguments not assigned during call to "feature".'])) ) ...
%                                 || ( strcmp(ME.identifier, 'MATLAB:Java:GenericException') && ...
%                                 strcmp(ME.message(1:69), sprintf(['Java exception occurred: \n' ...
%                                 'java.lang.OutOfMemoryError: Java heap space'])) ) ...
%                                 || false % todo memory error : ex : read_dataset('ft', 'security_id', 13381, 'from', '11/11/2010', 'to', '11/11/2010')
%                             rethrow(ME);
%                             data_ft = get_candles(varargin{:}); % not used as it is untested
%                         else
%                             rethrow(ME);
%                         end
%                     end
%                 else
%                     data_ft = {get_candles(varargin{:})};
%                 end
        [data, ~, idx_dumped] = ... % yes ~ won't work with matlab version < 2010a and this is intended : please upgrade matlab
            st_data('agg', data_ft, ...
            'colnames',{'volume', 'turnover', 'nb_trades', 'volume_sell', ...
            'volume_buy', 'vwap_buy', 'nb_buy',...
            'turnover_sell', 'nb_sell', 'vwas', 'open','high','low','close', ...
            'bid_open', 'bid_high', 'bid_low','bid_close', ...
            'ask_open', 'ask_high', 'ask_low','ask_close', ...
            'sum_price', 'time_close', 'time_open'},...
            'no_empty_bins', true, ...
            'grid_mode', { 'time',...
            'phase', ...
            'dark', ...
            'cross',...
            'trading_destination_id'},...
            'grid_options', ...
            {'time_type', 'sec',...
            'window', NB_SEC_SMALLEST_STEP, ...
            'step', NB_SEC_SMALLEST_STEP,...
            'kingpin', datenum(0,0,0,12,0,0),...
            'exceed_begin', true, ...
            'exceed_end', true, ...
            });
        
        
        if any(idx_dumped{1})
            error('get_basic_indicator_V2:check_exec', 'Some idx have been thrown away');
        end
        data = data{1};
        if isempty(data)
            data = st_data('empty-init');
        elseif ~isfield(data.info, 'min_step_price') && ~st_data('isempty-nl', data)
            try
                prices = st_data('cols', data_ft, {'price'; 'bid'; 'ask'});
            catch
                prices = st_data('cols', data_ft, 'price');
            end
            prices = prices(:);
            tmp = unique(round(prices(prices > 1e-8)*1e8));
            data.info.min_step_price = pgcd([unique(abs(diff(tmp(tmp > 0 & isfinite(tmp))))); tmp])*1e-8;% MAGIC NUMBER 1e-8; % MAGIC NUMBER 1e-8
            if ~isfinite(data.info.min_step_price)
                error('st_basic_indicator:exec', 'Strange data!!!');
            end
        end
        data.info.data_datestamp = datenum(varargin{2*idx_day}, 'dd/mm/yyyy');
    
    case 'smallest_step_mysql'
        % Retrieval of Smallest Step data from the MySQL database
        persistent res old_ref %#ok<TLEV>
        conv_date = @(d,f)datestr(datenum(d,f),'yyyy-mm-dd');
        security_id = varargin{2*find(strcmp(varargin(1:2:end),'security_id'))};
        from = varargin{2*find(strcmp(varargin(1:2:end),'from'))};
        to = varargin{2*find(strcmp(varargin(1:2:end),'to'))};
        day = varargin{2*find(strcmp(varargin(1:2:end),'day'))};
        format_date = varargin{2*find(strcmp(varargin(1:2:end),'format-date'))};
        if isempty(res) || isempty(old_ref) || old_ref(1)~=security_id || ...
                ~between(datenum(day,format_date),old_ref(2),old_ref(3))
            old_ref = [security_id, datenum(from,format_date), datenum(to,format_date)];
            [from,to] = deal(conv_date(from,format_date),conv_date(to,format_date));
            query = sprintf(['select Id, StampDate, SecurityId, TradeDate, File',...
                ' from %s',...
                ' where SecurityId = %d and TradeDate between ''%s'' and ''%s'''],...
                st_version.tables.ss_file_table,security_id,from,to);
            res = read_mysql_matfile(query);
        end
        if isempty(res)
            data = st_data('empty-init');
            return
        end
        day = conv_date(day,format_date);
        idx_date = find(strcmp(res(:,4),day));
        if ~any(idx_date)
            data = st_data('empty-init');
        else
            data = res{idx_date(1),5};
            [~,I] = sort(data.date);
            data = st_data('from-idx',data,I);
        end
        has_to_map = kech_td_mapping_needed(data);
        if has_to_map
            data = st_data_get_trading_destination_mapping(data);
        end
        
        
    otherwise
        error('get_basic_indicator_v2:mode', 'mode <%s> unknown', mode);
end
end

function data = get_candles(varargin)
opt = options({'security_id', NaN, 'day', '??/??/????'}, varargin);
sec_id = opt.get('security_id');
date_n = datenum(opt.get('day'), 'dd/mm/yyyy');

drf=setdbprefs('DataReturnFormat');
setdbprefs('DataReturnFormat', 'numeric');
t1 = clock;
try
    try
        vals = exec_sql('BSIRIUS', sprintf('candles30sec ''%s'', %d', ...
            datestr(date_n, 'yyyymmdd'), sec_id));
        % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        % !!! Si vous ?tes arr?t? dans la debugger parceque vous avez       !!!
        % !!! interrompu Matlab (Ctrl+C), alors vous devriez ?x?cuter la    !!!
        % !!! ligne ci-dessous                                              !!!
        setdbprefs('DataReturnFormat', drf);%                               !!!
        % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    catch ME
        if strcmp(ME.identifier, 'exec:sql') && strcmp(ME.message(1:end-1), ...
                sprintf(['SQL: Stored procedure ''candles30sec'' not found. ' ...
                'Specify owner.objectname or use sp_help to check ' ...
                'whether the object exists (sp_help may produce lots of output).']))
            install_candles30sec_sp();
            vals = exec_sql('BSIRIUS', sprintf('candles30sec ''%s'', %d', ...
                datestr(date_n, 'yyyymmdd'), sec_id));
            % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            % !!! Si vous ?tes arr?t? dans la debugger parceque vous avez       !!!
            % !!! interrompu Matlab (Ctrl+C), alors vous devriez ?x?cuter la    !!!
            % !!! ligne ci-dessous                                              !!!
            setdbprefs('DataReturnFormat', drf);%                               !!!
            % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            % !!!!
        else
            rethrow(ME);
        end
    end
catch ME
    setdbprefs('DataReturnFormat', drf);
    rethrow(ME);
end
st_log('end of fetch (%5.2f sec)\n', etime(clock, t1));
info = struct('security_id', sec_id, ...
    'security_key', get_repository( 'security-key', sec_id), ...
    'td_info', get_repository( 'trading-destination-info', sec_id) , ...
    'localtime', true , 'data_log', []);
if ~isempty(vals)
    data = st_data('init', 'title', info.security_key, 'value', vals(:, 2:end), 'date', date_n + vals(:, 1)/(24*3600), ...
        'colnames', {'time_open', 'time_close', 'volume', 'turnover', 'turnover_overbid','turnover_overask'...
        'nb_trades', 'volume_overbid','volume_overask', ...
        'open','high','low','close','open_ask','open_bid','average_spread_numer','average_spread_denom', ...
        'sum_price','auction','opening_auction','intraday_auction','closing_auction','trading_at_last',...
        'trading_after_hours','cross','dark', 'trading_destination_id'});
    
    % < On rajoute les colonnes manquantes : TODO les rajouter dans la
    % procedure stockee?
    % TODO si on fait ?a, revoir aussi le calcul du spread
    v = st_data('cols', data, {'open_ask','open_bid','high','low'});
    close_quote = [v(2:end, 1:2); NaN, NaN];
    hl_ask = [max(max(v(:, 1),close_quote(:, 1)), v(:, 3)) min(v(:, 1),close_quote(:, 1))];
    hl_bid = [max(v(:, 2),close_quote(:, 2)) min(min(v(:, 1),close_quote(:, 1)), v(:, 4))];
    
    nb_sell = st_data('cols', data, {'nb_trades', 'volume_overbid','volume_overask', 'volume'});
    nb_sell = nb_sell(:, 1).* (nb_sell(:, 2)+0.5*nb_sell(:, 4)-(nb_sell(:, 2)+nb_sell(:, 3))) ./ nb_sell(:, 4);
    
    data.colnames = cat(2, data.colnames, 'nb_sell', 'bid_high', 'bid_low',...
        'ask_high', 'ask_low','ask_close', 'bid_close');
    data.value = cat(2, data.value, nb_sell, hl_bid, hl_ask, close_quote);
    % >
else
    data = st_data('empty-init');
end
info.data_log = data.info.data_log;
data.info = info;
data.info.localtime = data.info.td_info(1).localtime;
end

function data = get_candles_1min(varargin)
opt = options({'security_id', NaN, 'day', '??/??/????'}, varargin);
sec_id = opt.get('security_id');
date_n = datenum(opt.get('day'), 'dd/mm/yyyy');

t1 = clock;
vals = from_buffer('get_full_candles', 'fc', 'security_id', ...
    sec_id, 'from', date_n, 'to', date_n);
st_log('end of fetch (%5.2f sec)\n', etime(clock, t1));
info = struct('security_id', sec_id, ...
    'security_key', get_repository( 'security-key', sec_id), ...
    'td_info', get_repository( 'trading-destination-info', sec_id) , ...
    'localtime', true , 'data_log', []);
% First 2 columns modification
vals.value(:,2) = vals.date;
vals.value(:,1) = vals.date-datenum(0,0,0,0,1,0);
if ~isempty(vals) && ~isempty(vals.date)
    data = st_data('init', 'title', info.security_key, ...
        'value',[vals.value(:,1:end-1),zeros(length(vals.date),1),vals.value(:,end)],...
        'date', vals.date, ...
        'colnames', {'time_open', 'time_close', 'volume', 'turnover', 'turnover_overbid','turnover_overask'...
        'nb_trades', 'volume_overbid','volume_overask', ...
        'open','high','low','close','open_ask','open_bid','average_spread_numer','average_spread_denom', ...
        'sum_price','auction','opening_auction','intraday_auction','closing_auction','trading_at_last',...
        'trading_after_hours','cross','dark', 'trading_destination_id'});
    
    % < On rajoute les colonnes manquantes : TODO les rajouter dans la
    % procedure stockee?
    % TODO si on fait ?a, revoir aussi le calcul du spread
    v = st_data('cols', data, {'open_ask','open_bid','high','low'});
    close_quote = [v(2:end, 1:2); NaN, NaN];
    hl_ask = [max(max(v(:, 1),close_quote(:, 1)), v(:, 3)) min(v(:, 1),close_quote(:, 1))];
    hl_bid = [max(v(:, 2),close_quote(:, 2)) min(min(v(:, 1),close_quote(:, 1)), v(:, 4))];
    
    nb_sell = st_data('cols', data, {'nb_trades', 'volume_overbid','volume_overask', 'volume'});
    nb_sell = nb_sell(:, 1).* (nb_sell(:, 2)+0.5*nb_sell(:, 4)-(nb_sell(:, 2)+nb_sell(:, 3))) ./ nb_sell(:, 4);
    
    data.colnames = cat(2, data.colnames, 'nb_sell', 'bid_high', 'bid_low',...
        'ask_high', 'ask_low','ask_close', 'bid_close');
    data.value = cat(2, data.value, nb_sell, hl_bid, hl_ask, close_quote);
    % >
else
    data = st_data('empty-init');
end
info.data_log = data.info.data_log;
data.info = info;
data.info.localtime = data.info.td_info(1).localtime;
end

function install_candles30sec_sp()
fid = fopen('candles30sec.sql', 'r');
tline = {fgetl(fid)};
while ischar(tline{end})
    tline{end+1} = fgetl(fid);
end
fclose(fid);
tline(end) = [];
exec_sql('BSIRIUS', sprintf('%s\n', tline{:}));
end

function data = double_log(data, message)
st_log(text_to_printable_text(message));
data = st_data('log', data, 'gieo', message);
end