function data = get_boat( mode, varargin)
% GET_BOAT - interface pour les donn�es publi�es sur BOAT
%
% internal uses:
% - get_boat('masterkey', 'boat', 'security_id', 110 )
% - get_boat('boat', 'security_id', 18, 'day', '04/01/2010' )
% - get_boat('boat_new', 'security_id', 107509, 'day', '04/04/2012' )
% - get_boat('trading_daily', 'security_id', [26 107509], 'day', '04/04/2012','day_start','25/03/2012')
%
% See also get_tick from_buffer read_dataset


switch lower(mode)
    
    case 'masterkey'
        %<* Return the generic+specific dir
        fmode = varargin{1};
        fmode = regexprep( fmode, '[ |*|\\|/|:|?|"|<|>|\|]', '_');
        opt = options({'security_id', 110}, varargin(2:end));
        sec_id = opt.get('security_id');
        if ~isnumeric( sec_id) && numel(sec_id)==1
            error('get_tick:secid', 'I need an unique numerical security id');
        end
        if strcmp(varargin{1}, 'spread and tick')
            data = fullfile(fmode, get_repository( 'security-td-key', sec_id, opt.get('trading-destinations')));
        else
            data = fullfile(fmode, get_repository( 'security-key', sec_id));
        end
        %>*    
    
    case 'boat'
        %<* Read into the tickdb
        opt = options({'security_id', 110, 'day', '01/04/2008',...
            'date_format:char', 'dd/mm/yyyy' ...
            }, varargin);
        %< Lecture des options
        format_date = opt.get('date_format:char');
        sec_id = opt.get('security_id');
        if ~isnumeric( sec_id) && numel(sec_id)==1
            error('get_tick:secid', 'I need an unique numerical security id');
        end
        day_ = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        %>
        this_day = datestr( day_, 'yyyymmdd');
        vals = exec_sql( 'GANDALF:tick_db', sprintf([ 'select ' ...
            ' (convert(varchar(10),date,111) + '' '' + convert(varchar(8),time,108)), ' ... 1
            ' trading_destination_code, ' ... 2
            ' (convert(varchar(10),origin_date,111) + '' '' + convert(varchar(8),origin_time,108)), ' ... 3
            ' convert(varchar(8),feedtime,108), ' ... 4
            ' deal_id, ' ... 5
            ' trading_destination_id, ' ...
            ' price, ' ...
            ' size, ' ...
            ' out_of_spread, ' ...
            ' negociated, ' ...
            ' correction, ' ...
            ' cancellation, ' ...
            ' previous_day_late, ' ...
            ' late ' ...
            ' from tick_db..deal_boat  ' ...
            ' where  ' ...
            ' security_id = %d and  ' ...
            ' date=''%s'' '], sec_id, this_day));
        if isempty(vals)
            data = st_data('empty-init');
            st_log(sprintf('%s is empty!', this_day));
        else
            num_vals = cellfun(@(n)double(n),vals(:,5:14),'unif', true);
            num_vals(isnan(num_vals(:,2)),2) = 0;
            dts = datenum( vals(:,1), 'yyyy/mm/dd HH:MM:SS');
            [cb_vals, cdk] = codebook('build', vals(:,2));
            cdk.colname = 'destination code';
            this_day = floor(dts(1));
            cdk.book = cellfun(@(c)sprintf('%d/%s', this_day, c), cdk.book , 'uni', false);
            cb_vals = cb_vals + 100*this_day;
            data = st_data('init', 'title', sprintf( 'Boat Data for %s', get_security_name(sec_id)), ...
                'date', dts, ...
                'value', [num_vals, cb_vals], ...
                'colnames', { 'deal id', 'trading destination id', 'price', 'size', 'out of spread', 'negociated', 'correction', ...
                'cancellation', 'previous day late', 'late', 'destination code'});
            data.codebook = cdk;
        end
        
            
    case 'boat_new'
        
        t0 = clock;
        %<* Read into the tickdb
        opt = options({'security_id', 110, 'day', '01/04/2008',...
            'date_format:char', 'dd/mm/yyyy' ...
            }, varargin);
        %< Lecture des options
        format_date = opt.get('date_format:char');
        sec_id = opt.get('security_id');
        if ~isnumeric( sec_id) && numel(sec_id)==1
            error('get_boat:secid', 'I need an unique numerical security id');
        end
        day_ = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        %>
        td_info = get_repository( 'trading-destination-info', sec_id);
        
        %< TESTS
        if day_<734779 % 03 october 2011 : data are only available starting this date on that format
            error('get_boat:day', 'No data before 3nd October 2011');
        end
        this_day = datestr( day_, 'yyyymmdd');
        %>
        
        %< SQL
        select_str = [  'select ' ...
            ' date,datediff(millisecond, ''00:00:00'', time),' ...
            ' publication_date,datediff(millisecond, ''00:00:00'', publication_time), ' ...
            ' venue,price,size,'...
            ' currency,delayed,execution_type,trade_type ' ...
            ' from '];
        deal_name=['deal_boat_trs' td_info(1).global_zone_suffix ];
        main_req = sprintf('%s..%s', get_basename4day( day_), deal_name);
        
        end_req = sprintf(['  where date = ''%s'' ' ...
            '   and security_id = %d   ' ...
            '  order by date,time ' ], ...
            this_day, sec_id);
        
        
        sql_req = [select_str main_req end_req];
        st_log('just before fetch (%5.2f sec)\n', etime(clock, t0));
        
        t1 = clock;
        vals = exec_sql('BSIRIUS', sql_req);
        st_log('end of fetch (%5.2f sec)\n', etime(clock, t1));
        %>
        
        
        if isempty(vals)
            data = [];
            return;
        end
        
        %< Dates and times
        sec1 = datenum(0,0,0,0,0,1);
        date=datenum(vals(1,1), 'yyyy-mm-dd')*ones(size(vals,1),1);
        date_time=cell2mat(vals(:,2)) * sec1/1000;
        time_diff_publication_date=datenum(vals(:,3), 'yyyy-mm-dd')-date;
        time_diff_publication_time=round(cell2mat(vals(:,4))/1000)*sec1-date_time;
        vals_out=time_diff_publication_date+time_diff_publication_time;
        %>
        
        col_out={'time_diff_publication','price','size','delayed'};
        vals_out=cat(2,vals_out,cellfun(@(c)(c*1),vals(:,[6 7 9])));
        
        %< cdk data
        num_cdk=[5 8 10 11];
        name_cdk={'venue','currency','execution_type','trade_type'};
        cdk=[];
        
        for i_cdk=1:length(num_cdk)
            cdk=codebook('stack',cdk,codebook('new',vals(:,num_cdk(i_cdk)),name_cdk{i_cdk}));
            vals_out=cat(2,vals_out,...
                codebook('extract','names2ids',cdk,name_cdk{i_cdk},cellfun(@strtrim, vals(:,num_cdk(i_cdk)),'uni', false)));
        end
        %>
        
        %< output building
        t4 = clock;
        data = st_data( 'init', ...
            'title', sprintf( 'BOAT : %s - %s', get_repository( 'security-key', sec_id), datestr(day_, 'dd/mm/yyyy')), ...
            'value', vals_out, ...
            'date', date+date_time, ...
            'colnames', cat(2,col_out,name_cdk));
        data.info =  ...
            struct('security_id', sec_id, ...
            'security_key', get_repository( 'security-key', sec_id), ...
            'td_info', td_info , ...
            'sql_request', sql_req, ...
            'data_log', data.info.data_log, ...
            'localtime',false,...
            'place_timezone_id', 0,...
            'local_place_timezone_id',td_info(1).place_id) ;
        data.codebook=cdk;
        
        st_log('end of repository (%5.2f sec)\n', etime(clock, t4));
        %>

        
     case 'trading_daily'
        t0 = clock;
        %<* Read into the tickdb
        opt = options({'security_id', [26 110],... % is iselpty -> all security !
            'day', '04/04/2012',...
            'day_start','01/04/2012',...
            'venue',{},...% restrict venues
            'date_format:char', 'dd/mm/yyyy' ...
            }, varargin);
        %< Lecture des options
        format_date = opt.get('date_format:char');
        sec_id = opt.get('security_id');
        if ~isnumeric( sec_id)
            error('get_boat:secid', 'I need numerical security id list');
        end
        day_ = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        day_start_ = opt.get('day_start');
        
        if isempty(day_start_)
            day_start_=day_;
        elseif ischar( day_start_)
            day_start_ = datenum( day_start_, format_date);
        end
        
        venue = opt.get('venue');
       if ~iscellstr(venue)
          error('get_boat:venue', 'venue has to be a cellarray'); 
       end
        %>
        
        %< TESTS
        if day_<day_start_
            error('get_boat:day', 'bad inputs day - day_start');
        end
        if day_<734779 % 03 october 2011 : data are only available starting this date on that format
            error('get_boat:day', 'No data before 3nd October 2011');
        end
        start_day = datestr( day_start_, 'yyyymmdd');
        this_day = datestr( day_, 'yyyymmdd');
        %>
        
        %< SQL
        select_str = [  'select ' ...
            ' date,security_id,venue,' ...
            ' count(*),sum(size),sum(size*price)' ...
            ' from '];
        deal_name=['deal_boat_trs'];
        end_req = sprintf(['  where date >= ''%s'' ' ...
            ' and date <= ''%s'' ' ],start_day,this_day);
        if ~isempty(sec_id)
            sec_char = sprintf('%d,',sec_id);
            sec_char(end) = [];
            end_req=[end_req sprintf(' and security_id in (%s) ',sec_char)];
        end
        if ~isempty(venue)
            venue_char = cellfun(@(c)(sprintf(['''%s'','],c)),venue,'uni',false);
            venue_char=[venue_char{:}];
            venue_char(end) = [];
            end_req=[end_req sprintf(' and venue in (%s) ',venue_char)];
        end
        end_req=[end_req ' group by  date,security_id,venue'];
   
        year_month_list=unique(cat(2,year((day_start_:day_)'),month((day_start_:day_)')),'rows');
      
        %< extract
        vals=[];
        st_log('just before fetch (%5.2f sec)\n', etime(clock, t0));
        t1 = clock;
        for i_=1:size(year_month_list,1)
            main_req_tmp=sprintf('%s..%s', get_basename4day( datenum(year_month_list(i_,1),year_month_list(i_,2),1)), deal_name);
            sql_req_tmp = [select_str main_req_tmp end_req];
            vals_tmp = exec_sql('BSIRIUS', sql_req_tmp);
            if ~isempty(vals_tmp)
                vals = cat(1,vals,vals_tmp);
            end
        end
        st_log('end of fetch (%5.2f sec)\n', etime(clock, t1));
        %>
        if isempty(vals)
            data = [];
            return;
        end
        
        %< Dates and times
        date=datenum(vals(:,1), 'yyyy-mm-dd');
        %>
        col_out={'security_id','nb_trades','volume','turnover'};
        vals_out=cellfun(@(c)(c*1),vals(:,[2 4 5 6]));
        
        %< cdk data
        num_cdk=[3];
        name_cdk={'venue'};
        cdk=[];
        
        for i_cdk=1:length(num_cdk)
            cdk=codebook('stack',cdk,codebook('new',vals(:,num_cdk(i_cdk)),name_cdk{i_cdk}));
            vals_out=cat(2,vals_out,...
                codebook('extract','names2ids',cdk,name_cdk{i_cdk},cellfun(@strtrim, vals(:,num_cdk(i_cdk)),'uni', false)));
        end
        %>
        
        [~,idx_sort]=sortrows(cat(2,date,vals_out(:,[1 3])),[1 2 -3]);
        
        %< output building
        t4 = clock;
        data = st_data( 'init', ...
            'title', sprintf( 'BOAT daily stats : %s - %s', start_day, this_day), ...
            'value', vals_out(idx_sort,:), ...
            'date', date(idx_sort), ...
            'colnames', cat(2,col_out,name_cdk));
        data.info =  ...
            struct( 'sql_request', sql_req_tmp, ...
            'data_log', data.info.data_log, ...
            'localtime',false,...
            'place_timezone_id', 0,...
            'local_place_timezone_id',NaN) ;
        data.codebook=cdk;
        
        st_log('end of repository (%5.2f sec)\n', etime(clock, t4));
        %>       
        
        
        
        
end
