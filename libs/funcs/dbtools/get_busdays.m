function bds = get_busdays( varargin )
% get_busdays
% Matlab busdays function, with trading holidays removed (when  more than
% 90% of tds in input are on holidays)
%


opt = options({'mode','trading_destination_id',...
    'code', 1, 'from', '01/01/2014', 'to', '30/06/2014'}, varargin);

hdays = get_holidays(varargin{:});
bds = busdays(datenum(opt.get('from'),'dd/mm/yyyy'),datenum(opt.get('to'),'dd/mm/yyyy'),'daily'); %pb: enlevera les jours f�ri�s US-UK
bds = setdiff(bds, hdays);

end
