function rate = get_currency_rate(ccy_id,ccy_ref_id,varargin)
% GET_CURRENCY_RATE - Grab historical currency rates btw to dates
%
%
%
% Examples:
% get_currency_rate('currency_pair',ccy_id,ccy_ref_id,'from',datestr(today-1,'dd/mm/yyyy'),'to',datestr(today-8,'dd/mm/yyyy'))
%
%
%
% See also:
% get_repository, read_dataset
%
%   author   : 'mlasnier@keplercheuvreux.com'
%   reviewer : 'mlasnier'
%   version  : '2'
%   date     :  08/02/2017

opt = options({'to',datestr(today-1,'dd/mm/yyyy'),'from',datestr(today-8,'dd/mm/yyyy')},varargin);

assert(~iscellstr(ccy_ref_id),'Reference currency has to be an str')
if ~iscellstr(ccy_id)
    ccy_id = {ccy_id};
end

beg_date = datenum(opt.get('from'),'dd/mm/yyyy');
end_date = datenum(opt.get('to'),'dd/mm/yyyy');
ccy_str = join(unique(ccy_id));

% Data query
res = exec_sql('KGR',['select DATE,CCY,VALUE'...
    ' from KGR..HISTOCURRENCYTIMESERIES '...
    ' where SOURCEID = 1 and ATTRIBUTEID = 43',...
    sprintf(' and CCY in (%s) and CCYREF = ''%s''',ccy_str,ccy_ref_id)...
    sprintf(' and DATE between ''%s'' and ''%s''',datestr(beg_date,'yyyy-mm-dd'),datestr(end_date,'yyyy-mm-dd'))]);
%

rate = struct();
rate.date = datenum(res(:,1),'yyyy-mm-dd');
rate.ccy = res(:,2);
rate.currency_rate = cell2mat(res(:,3));


function str = join(list)
str = sprintf('''%s'',',list{:});
str = str(1:end-1);
