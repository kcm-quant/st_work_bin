function data = get_dark_otc(mode, varargin)
% GET_DARK_OTC - Short_one_line_description
%
% !!!!!!!!!!! IMPORTANT REMARK : DATA ARE ONLY AVAILABLE STARTING 3rd Octobre 2011 !!!!!!!!
%
% %-- Extract the link between, database name / matlab name and trading destiantion id of each DP
% data=get_dark_otc('info');
% %-- Get BOAT Deal for One security on One day
% data=get_dark_otc('boat', 'security_id', 107509, 'day', '04/04/2012' );
% %-- Get ENX Deal for One security on One day
% data=get_dark_otc('enx', 'security_id', 107509, 'day', '04/04/2012' );
% %-- Get BOAT aggregate data on a daily basis, for a list of security id on the period 'day_start' to 'day'
% data=get_dark_otc('td_boat', 'security_id', [26 107509], 'day','04/04/2012','day_start','25/03/2012');
% %-- only blink on all stocks
% data=get_dark_otc('td_boat', 'security_id', [], 'day','05/04/2012','day_start','01/04/2012','venue_name',{'BLNK'});
% data=get_dark_otc('td_boat', 'security_id', [26 107509], 'day','04/04/2012','day_start','25/03/2012','venue_name',{'BLNK'});
% data=get_dark_otc('td_boat', 'indice_id', [1], 'day','04/04/2012','day_start','25/03/2012');
% %-- Get ENX aggregate data on a daily basis, for a list of security id on the period 'day_start' to 'day'
% data=get_dark_otc('td_enx', 'security_id', [26 107509 10735], 'day','04/04/2012','day_start','03/04/2012');
% data=get_dark_otc('td_enx', 'security_id', [26 107509 10735], 'day','04/04/2012','day_start','25/03/2012');
% data=get_dark_otc('td_enx', 'indice_id', [1], 'day','04/04/2012','day_start','25/03/2012');
% %-- Get All dark volume (BOAT, ENX, MID POINT of MTF) on an aggregated daily view
% data=get_dark_otc('trading_daily', 'security_id', [26 172371 10735], 'day_start','01/04/2012', 'day','05/04/2012');
% data=get_dark_otc('trading_daily', 'indice_id', [1], 'day_start','01/04/2012', 'day','05/04/2012');
%
% Examples:
%
%
% See also:
%
%
%   author   : 'nijos'
%   reviewer : ''
%   date     :  '11/04/2012'
%
%   last_checkin_info : $Header: get_dark_otc.m: Revision: 15: Author: malas: Date: 03/26/2013 03:31:59 PM$


switch lower(mode)
    
    
    case 'info'
        %--------------------------------------------------------------------------
        %---- GLOBAL NAMES
        %--------------------------------------------------------------------------
        
        data=[];
        data.boat_name.database_name={'OTC','SI','NXEU',...
            'XPOS','XUBS','BLOX',...
            'BLNK','LIQU','BLKX'};
        data.boat_name.cdk_name={'Boat OTC','Boat SI','Nomura NX',...
            'Posit','UBS MTF','Instinet BlockMatch',...
            'Blink MTF','Liquidnet','Block Cross'};
        data.boat_name.trading_destination_id=[-1,-2,-3,...
            131,151,153,...
            92,-4,-5];
        
        data.enx_name.database_name={'XSMP','OTC','XVTX',...
            'MTAA','XETR','XLON','174220 V S'};
        data.enx_name.cdk_name={'Smartpool','Euronext OTC','Virtx OTC',...
            'MTAA','Xetra OTC','XLON','174220 V S'};
        data.enx_name.trading_destination_id=[128,-6,-7,...
            -8,-9,-10,-11];
        
    case 'boat'
        
        t0 = clock;
        %<* Read into the tickdb
        opt = options({'security_id', [], ...
            'day', '',...
            'date_format:char', 'dd/mm/yyyy' ...
            }, varargin);
        %< Lecture des options
        format_date = opt.get('date_format:char');
        sec_id = opt.get('security_id');
        if ~isnumeric( sec_id) && numel(sec_id)==1
            error('get_dark_otc:secid', 'I need an unique numerical security id');
        end
        day_ = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        %>
        td_info = get_repository( 'trading-destination-info', sec_id);
        
        %< TESTS
        if day_<734779 % 03 october 2011 : data are only available starting this date on that format
            error('get_dark_otc:day', 'No data before 3nd October 2011');
        end
        this_day = datestr( day_, 'yyyymmdd');
        %>
        
        %< SQL
        select_str = [  'select ' ...
            ' date,datediff(millisecond, ''00:00:00'', time),' ...
            ' publication_date,datediff(millisecond, ''00:00:00'', publication_time), ' ...
            ' venue,price,size,'...
            ' currency,delayed,execution_type,trade_type ' ...
            ' from '];
        deal_name=['deal_boat_trs' td_info(1).global_zone_suffix ];
        main_req = sprintf('%s..%s', get_basename4day( day_), deal_name);
        
        end_req = sprintf(['  where date = ''%s'' ' ...
            '   and security_id = %d   ' ...
            '  order by date,time ' ], ...
            this_day, sec_id);
        
        
        sql_req = [select_str main_req end_req];
        st_log('just before fetch (%5.2f sec)\n', etime(clock, t0));
        
        t1 = clock;
        vals = exec_sql('BSIRIUS', sql_req);
        st_log('end of fetch (%5.2f sec)\n', etime(clock, t1));
        %>
        
        
        if isempty(vals)
            data = [];
            return;
        end
        
        %< Dates and times
        sec1 = datenum(0,0,0,0,0,1);
        [uni_date,~,idx_in_date]=unique(vals(:,1));
        uni_date=datenum(uni_date, 'yyyy-mm-dd');
        date=uni_date(idx_in_date);
        date_time=cell2mat(vals(:,2)) * sec1/1000;
        
        [uni_publi_date,~,idx_in_date]=unique(vals(:,3));
        uni_publi_date=datenum(uni_publi_date, 'yyyy-mm-dd');
        publi_date=uni_publi_date(idx_in_date);
        
        vals_out=(publi_date+cell2mat(vals(:,4)) * sec1/1000)-(date+date_time);
        col_out={'time_diff_publication'};
        %>
        
        %<---- quotation_unit and currency handling
        %// reference of this security_id in our databse !
        data_ref_curr=get_repository('sec_id_rate2ref','security_id',sec_id,...
            'from',datestr(day_,'dd/mm/yyyy'),...
            'to',datestr(day_, 'dd/mm/yyyy'),...
            'currency_id_ref',58);
        if st_data('isempty-nl',data_ref_curr)
            error('get_dark_otc:currency', 'No information on the security currency !');
        end
        data_ref_curr=st_data('col',data_ref_curr,'currency_id;quotation_unit;rate2ref');
        
        %// value of this infos in boat data
        boat_currency=vals(:,8);
        boat_quotation_unit=ones(size(boat_currency));
        %/ GBP / GBX
        boat_quotation_unit(strcmpi(boat_currency,'gbx'))=1/100;
        boat_currency(strcmpi(boat_currency,'gbx'))={'GBP'};
        boat_currency(strcmpi(boat_currency,'GBp'))={'GBP'};
        %/ ILS / ILa
        boat_quotation_unit(strcmpi(boat_currency,'ila'))=1/100;
        boat_currency(strcmpi(boat_currency,'ila'))={'ILS'};
        %/ ZAR / Zac
        boat_quotation_unit(strcmpi(boat_currency,'zac'))=1/100;
        boat_currency(strcmpi(boat_currency,'zac'))={'ZAR'};
        uni_boat_currency=unique(boat_currency);
        uni_currency_id=get_repository('currency_short_name2id',uni_boat_currency);
        if any(~isfinite(uni_currency_id))
            error('get_dark_otc:currency', 'At least one of the boat currency is unknwon in the database <%s>',uni_boat_currency(find(~isfinite(uni_currency_id),1,'first')));
        end
        %// transform boat data to the reference currency id and quotation_unit
        %// TO DO BETTER IF NEEDED : get the rate ...
        if any(~ismember(uni_currency_id,data_ref_curr(1)))
            error('get_dark_otc:currency', 'Boat reported a different currency for this securityid !! <%d>',security_id);
        end
        multiplier_to_ref=boat_quotation_unit/data_ref_curr(2);
        col_out=cat(2,col_out,{'price_multiplier_to_ref','price','size'});
        vals_out=cat(2,vals_out,multiplier_to_ref,cell2mat(vals(:,6)).*multiplier_to_ref,cell2mat(vals(:,7)));
        % attention on ne renormalize pas le size !
        
        %---->
        %// other numerical value
        col_out=cat(2,col_out,{'delayed'});
        vals_out=cat(2,vals_out,cellfun(@(c)(c*1),vals(:,[9])));
        
        %< cdk data
        info_ = get_dark_otc('info');
        BOAT_VENUE_NAME=info_.boat_name.database_name;
        BOAT_VENUE_NAME_4CDK=info_.boat_name.cdk_name;
        
        num_cdk=[5 10 11];
        name_cdk={'venue_name','execution_type','trade_type'};
        cdk=[];
        
        for i_cdk=1:length(num_cdk)
            cdk_tmp=codebook('new',vals(:,num_cdk(i_cdk)),name_cdk{i_cdk});
            vals_out=cat(2,vals_out,...
                codebook('extract','names2ids',cdk_tmp,name_cdk{i_cdk},cellfun(@strtrim, vals(:,num_cdk(i_cdk)),'uni', false)));
            if strcmp(name_cdk{i_cdk},'venue_name')
                [id idx_in]=ismember(cdk_tmp.book,BOAT_VENUE_NAME);
                if any(id)
                    cdk_tmp.book(id)=BOAT_VENUE_NAME_4CDK(idx_in(id));
                end
            end
            cdk=codebook('stack',cdk,cdk_tmp);
        end
        %>
        
        %< output building
        t4 = clock;
        data = st_data( 'init', ...
            'title', sprintf( 'BOAT : %s - %s', get_repository( 'security-key', sec_id), datestr(day_, 'dd/mm/yyyy')), ...
            'value', vals_out, ...
            'date', date+date_time, ...
            'colnames', cat(2,col_out,name_cdk));
        data.info =  ...
            struct('security_id', sec_id, ...
            'security_key', get_repository( 'security-key', sec_id), ...
            'td_info', td_info , ...
            'sql_request', sql_req, ...
            'data_log', data.info.data_log, ...
            'localtime',false,...
            'place_timezone_id', 0,...
            'local_place_timezone_id',td_info(1).place_id) ;
        data.codebook=cdk;
        
        st_log('end of repository (%5.2f sec)\n', etime(clock, t4));
        %>
        
        
    case 'enx'
        
        t0 = clock;
        %<* Read into the tickdb
        opt = options({'security_id', [], ...
            'day', '',...
            'date_format:char', 'dd/mm/yyyy' ...
            }, varargin);
        %< Lecture des options
        format_date = opt.get('date_format:char');
        sec_id = opt.get('security_id');
        if ~isnumeric( sec_id) && numel(sec_id)==1
            error('get_dark_otc:enx', 'I need an unique numerical security id');
        end
        sec_isin='';
        try
            sec_isin=get_repository('sec_id2isin',sec_id);
            sec_isin=sec_isin{1};
        catch end
        
        if isempty(sec_isin) || ~ischar(sec_isin)
            error('get_dark_otc:enx', 'ISIN could not be find');
        end
        
        day_ = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        %>
        td_info = get_repository( 'trading-destination-info', sec_id);
        
        %< TESTS
        if day_<734779 % 03 october 2011 : data are only available starting this date on that format
            error('get_dark_otc:day', 'No data before 3nd October 2011');
        end
        this_day = datestr( day_, 'yyyymmdd');
        %>
        
        %< SQL
        select_str = [  'select ' ...
            ' origin_date,datediff(millisecond, ''00:00:00'', origin_time),' ...
            ' date,datediff(millisecond, ''00:00:00'', time), ' ...
            ' venue,price,size,'...
            ' currency,late,price_multiplier ' ...
            ' from '];
        deal_name=['deal_enx_trs' td_info(1).global_zone_suffix ];
        main_req = sprintf('%s..%s', get_basename4day( day_), deal_name);
        
        end_req = sprintf(['  where origin_date = ''%s'' ' ...
            '  and isin = ''%s ''  ' ...
            '  order by date,time ' ], ...
            this_day, sec_isin);
        
        
        sql_req = [select_str main_req end_req];
        st_log('just before fetch (%5.2f sec)\n', etime(clock, t0));
        
        t1 = clock;
        vals = exec_sql('BSIRIUS', sql_req);
        st_log('end of fetch (%5.2f sec)\n', etime(clock, t1));
        %>
        
        
        if isempty(vals)
            data = [];
            return;
        end
        
        %< Dates and times
        sec1 = datenum(0,0,0,0,0,1);
        [uni_date,~,idx_in_date]=unique(vals(:,1));
        uni_date=datenum(uni_date, 'yyyy-mm-dd');
        date=uni_date(idx_in_date);
        date_time=cell2mat(vals(:,2)) * sec1/1000;
        
        [uni_publi_date,~,idx_in_date]=unique(vals(:,3));
        uni_publi_date=datenum(uni_publi_date, 'yyyy-mm-dd');
        publi_date=uni_publi_date(idx_in_date);
        
        vals_out=(publi_date+cell2mat(vals(:,4)) * sec1/1000)-(date+date_time);
        col_out={'time_diff_publication'};
        %>
        
        
        %<---- quotation_unit and currency handling
        %// reference of this security_id in our databse !
        data_ref_curr=get_repository('sec_id_rate2ref','security_id',sec_id,...
            'from',datestr(day_,'dd/mm/yyyy'),...
            'to',datestr(day_, 'dd/mm/yyyy'),...
            'currency_id_ref',58);
        if st_data('isempty-nl',data_ref_curr)
            error('get_dark_otc:currency', 'No information on the security currency !');
        end
        data_ref_curr=st_data('col',data_ref_curr,'currency_id;quotation_unit;rate2ref');
        
        %// value of this infos in boat data
        enx_currency=vals(:,8);
        enx_quotation_unit=cell2mat(vals(:,10));
        %/ GBP / GBX
        enx_quotation_unit(strcmpi(enx_currency,'gbx'))=1/100*enx_quotation_unit(strcmpi(enx_currency,'gbx'));
        enx_currency(strcmpi(enx_currency,'gbx'))={'GBP'};
        enx_currency(strcmpi(enx_currency,'GBp'))={'GBP'};
        %/ ILS / ILa
        enx_quotation_unit(strcmpi(enx_currency,'ila'))=1/100*enx_quotation_unit(strcmpi(enx_currency,'ila'));
        enx_currency(strcmpi(enx_currency,'ila'))={'ILS'};
        %/ ZAR / Zac
        enx_quotation_unit(strcmpi(enx_currency,'zac'))=1/100*enx_quotation_unit(strcmpi(enx_currency,'zac'));
        enx_currency(strcmpi(enx_currency,'zac'))={'ZAR'};
        [uni_enx_currency,~,idx_curr_in_uni]=unique(enx_currency);
        uni_currency_id=get_repository('currency_short_name2id',uni_enx_currency);
        if any(~isfinite(uni_currency_id))
            error('get_dark_otc:currency', 'At least one of the boat currency is unknwon in the database <%s>',uni_enx_currency(find(~isfinite(uni_currency_id),1,'first')));
        end
        enx_currency_id=uni_currency_id(idx_curr_in_uni);
        
        %// transform enx data to the reference currency id and quotation_unit
        multiplier_to_ref=enx_quotation_unit/data_ref_curr(2);
        %// TO DO BETTER IF NEEDED : get the rate ...
        multiplier_currency_to_ref=ones(size(enx_quotation_unit));
        
        if any(~ismember(uni_currency_id,data_ref_curr(1)))
            %error('get_dark_otc:currency', 'ENX reported a different currency for this securityid !! <%d>',security_id);
            tmp_rate2ref=get_repository('currency_rate2ref',...
                'from',datestr(day_,'dd/mm/yyyy'),...
                'to',datestr(day_,'dd/mm/yyyy'),...
                'currency_id',uni_currency_id,...
                'currency_id_ref',data_ref_curr(1));
            if ~st_data('isempty-nl',tmp_rate2ref)
                [id idx]=ismember(cat(2,date,enx_currency_id),st_data('col',tmp_rate2ref,'day;currency_id'),'rows');
                multiplier_currency_to_ref(id)=st_data('col',tmp_rate2ref,'rate2ref',idx(id));
            end
            clear tmp_rate2ref;
        end
        
        col_out=cat(2,col_out,{'price_multiplier_to_ref','price','size'});
        vals_out=cat(2,vals_out,multiplier_to_ref.*multiplier_currency_to_ref,cell2mat(vals(:,6)).*multiplier_to_ref.*multiplier_currency_to_ref,cell2mat(vals(:,7)));
        
        %---->
        %// other numerical value
        
        col_out=cat(2,col_out,{'late'});
        vals_out=cat(2,vals_out,cellfun(@(c)(c*1),vals(:,[9])));
        
        %< cdk data
        info_ = get_dark_otc('info');
        ENX_VENUE_NAME=info_.enx_name.database_name;
        ENX_VENUE_NAME_4CDK=info_.enx_name.cdk_name;
        
        num_cdk=[5 8];
        name_cdk={'venue_name','currency'};
        cdk=[];
        
        for i_cdk=1:length(num_cdk)
            cdk_tmp=codebook('new',vals(:,num_cdk(i_cdk)),name_cdk{i_cdk});
            vals_out=cat(2,vals_out,...
                codebook('extract','names2ids',cdk_tmp,name_cdk{i_cdk},cellfun(@strtrim, vals(:,num_cdk(i_cdk)),'uni', false)));
            if strcmp(name_cdk{i_cdk},'venue_name')
                [id idx_in]=ismember(cdk_tmp.book,ENX_VENUE_NAME);
                if any(id)
                    cdk_tmp.book(id)=ENX_VENUE_NAME_4CDK(idx_in(id));
                end
            end
            cdk=codebook('stack',cdk,cdk_tmp);
        end
        %>
        
        %< output building
        t4 = clock;
        data = st_data( 'init', ...
            'title', sprintf( 'BOAT : %s - %s', get_repository( 'security-key', sec_id), datestr(day_, 'dd/mm/yyyy')), ...
            'value', vals_out, ...
            'date', date+date_time, ...
            'colnames', cat(2,col_out,name_cdk));
        data.info =  ...
            struct('security_id', sec_id, ...
            'security_key', get_repository( 'security-key', sec_id), ...
            'td_info', td_info , ...
            'sql_request', sql_req, ...
            'data_log', data.info.data_log, ...
            'localtime',false,...
            'place_timezone_id', 0,...
            'local_place_timezone_id',td_info(1).place_id) ;
        data.codebook=cdk;
        
        st_log('end of repository (%5.2f sec)\n', etime(clock, t4));
        %>
        
        
        
        
    case 'td_boat'
        
        t0 = clock;
        
        info_ = get_dark_otc('info');
        BOAT_VENUE_NAME=info_.boat_name.database_name;
        BOAT_VENUE_NAME_4CDK=info_.boat_name.cdk_name;
        
        %< ----------------------- Lecture des options
        
        opt = options({'security_id', [],... % is isempty -> all security !
            'indice_id',[],...
            'day', '',...
            'day_start','',...
            'venue_name',{},...% restrict venues
            'add_rate_to_euro',true,...
            'date_format:char', 'dd/mm/yyyy' ...
            }, varargin);
        
        format_date = opt.get('date_format:char');
        sec_id = opt.get('security_id');
        indice_id = opt.get('indice_id');
        
        if ~isnumeric( sec_id) || ~isnumeric(indice_id)
            error('get_dark_otc:secid', 'I need numerical security id list');
        end
        
        if ~isempty( sec_id) && ~isempty(indice_id)
            error('get_dark_otc:secid', 'security or indice id !! ');
        end
        
        day_ = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        day_start_ = opt.get('day_start');
        
        if isempty(day_start_)
            day_start_=day_;
        elseif ischar( day_start_)
            day_start_ = datenum( day_start_, format_date);
        end
        
        venue = opt.get('venue_name');
        if ~iscellstr(venue)
            error('get_dark_otc:td_boat', 'venue has to be a cellarray');
        end
        
        if ~isempty(venue) && ...
                any(~ismember(venue,BOAT_VENUE_NAME))
            error('get_dark_otc:td_boat', 'At least one venue is not recognized');
        end
        %>
        
        %< ----------------------- TESTS
        if day_<day_start_
            error('get_dark_otc:day', 'bad inputs day - day_start');
        end
        if day_<734779 % 03 october 2011 : data are only available starting this date on that format
            error('get_dark_otc:day', 'No data before 3nd October 2011');
        end
        start_day = datestr( day_start_, 'yyyymmdd');
        this_day = datestr( day_, 'yyyymmdd');
        %>
        
        %< ----------------------- SQL
        
        %----- SQL info for deal boat aggregation
        %/
        sh_prefix='sh';
        sh_table_name=['security_historic'];
        sh_base_name='repository';
        %/
        deal_prefix='dbt';
        deal_name='deal_boat_trs';
        %/
        ind_prefix='ind';
        ind_table_name=['indice_component_master'];
        ind_base_name='repository';
        
        
        select_req_name={'date','security_id','venue','currency','nb','volume','turnover','ref_quotation_unit','ref_currency_id'};
        select_req = sprintf([  'select ' ...
            ' %s = %s.date,%s = %s.security_id,%s = %s.venue,%s = %s.currency,' ...
            ' %s = count(*),%s = sum(%s.size),%s = sum(%s.size*%s.price) ,'...
            ' %s=ISNULL(min(%s.quotation_unit),1), %s=min(%s.currency_id)'],...
            select_req_name{1},deal_prefix,...
            select_req_name{2},deal_prefix,...
            select_req_name{3},deal_prefix,...
            select_req_name{4},deal_prefix,...
            select_req_name{5},...
            select_req_name{6},deal_prefix,...
            select_req_name{7},deal_prefix,deal_prefix,...
            select_req_name{8},sh_prefix,...
            select_req_name{9},sh_prefix);
        
        if ~isempty(indice_id)
            select_req_name=cat(2,select_req_name,{'indice_id'});
            select_req=[select_req ...
                sprintf(' , %s = %s.indice_id',select_req_name{end},ind_prefix)];
        end
        
        end_req = sprintf(['  where %s.date >= ''%s'' ' ...
            ' and %s.date <= ''%s'' ' ],...
            deal_prefix,start_day,...
            deal_prefix,this_day);
        
        if ~isempty(sec_id)
            sec_char = sprintf('%d,',sec_id);
            sec_char(end) = [];
            end_req=[end_req ...
                sprintf(' and %s.security_id in (%s) ',...
                deal_prefix,sec_char)];
        elseif ~isempty(indice_id)
            sec_char = sprintf('%d,',indice_id);
            sec_char(end) = [];
            end_req=[end_req ...
                sprintf(' and %s.indice_id in (%s) ',ind_prefix,sec_char) ...
                sprintf(' and %s.security_id = %s.security_id  ',ind_prefix,deal_prefix) ...
                sprintf(' and %s.begin_date<= %s.date  ',ind_prefix,deal_prefix) ...
                sprintf(' and ( %s.end_date >= %s.date or %s.end_date is null ) ',ind_prefix,deal_prefix,ind_prefix ) ...
                ];
        end
        
        if ~isempty(venue)
            venue_char = cellfun(@(c)(sprintf(['''%s'','],c)),venue,'uni',false);
            venue_char=[venue_char{:}];
            venue_char(end) = [];
            end_req=[end_req ...
                sprintf(' and %s.venue in (%s) ',...
                deal_prefix,venue_char)];
        end
        
        end_req=[end_req ...
            sprintf(' group by  %s.date,%s.security_id,%s.venue,%s.currency',...
            deal_prefix,deal_prefix,deal_prefix,deal_prefix)];
        
        if ~isempty(indice_id)
            end_req=[end_req sprintf(',%s.indice_id',ind_prefix)];
        end
        
        
        % deal doat depends on the month, we have to perform several request....
        year_month_list=unique(cat(2,year((day_start_:day_)'),month((day_start_:day_)')),'rows');
        
        %----- SQL
        %< extract
        vals=[];
        st_log('just before fetch (%5.2f sec)\n', etime(clock, t0));
        t1 = clock;
        for i_=1:size(year_month_list,1)
            
            from_req_tmp=sprintf('%s..%s %s ',get_basename4day( datenum(year_month_list(i_,1),year_month_list(i_,2),1)), deal_name,deal_prefix);
            
            if ~isempty(indice_id)
                from_req_tmp=[sprintf(' %s..%s %s, ',ind_base_name,ind_table_name,ind_prefix) from_req_tmp];
            end
            
            from_req_tmp=[from_req_tmp ...
                sprintf([' left join %s..%s %s ' ...
                ' on ( ' ...
                ' %s.security_id=%s.security_id ' ...
                ' and %s.begin_date<= ''%s'' ' ...
                ' and ( %s.end_date >= ''%s'' or %s.end_date is null ) ' ...
                ' ) ' ],...
                sh_base_name,sh_table_name,sh_prefix,...
                deal_prefix,sh_prefix,...
                sh_prefix,start_day,...
                sh_prefix,this_day,sh_prefix)];
            
            sql_req_tmp = [select_req ' from ' from_req_tmp ' ' end_req];
            vals_tmp = exec_sql('BSIRIUS', sql_req_tmp);
            if ~isempty(vals_tmp)
                vals = cat(1,vals,vals_tmp);
            end
        end
        st_log('end of fetch (%5.2f sec)\n', etime(clock, t1));
        %>
        if isempty(vals)
            data = [];
            return;
        end
        
        %< Dates and times
        date=datenum(vals(:,1), 'yyyy-mm-dd');
        %>
        if ~isempty(indice_id)
            col_out={'security_id','indice_id','nb_trades','volume','turnover','ref_quotation_unit','ref_currency_id'};
            vals_out=cellfun(@(c)(c*1),vals(:,[2 10 5 6 7 8 9]));
        else
            col_out={'security_id','nb_trades','volume','turnover','ref_quotation_unit','ref_currency_id'};
            vals_out=cellfun(@(c)(c*1),vals(:,[2 5 6 7 8 9]));
        end
        
        %< cdk data
        num_cdk=[3];
        name_cdk={'venue_name'};
        cdk=[];
        
        for i_cdk=1:length(num_cdk)
            cdk_tmp=codebook('new',vals(:,num_cdk(i_cdk)),name_cdk{i_cdk});
            vals_out=cat(2,vals_out,...
                codebook('extract','names2ids',cdk_tmp,name_cdk{i_cdk},cellfun(@strtrim, vals(:,num_cdk(i_cdk)),'uni', false)));
            col_out=cat(2,col_out,name_cdk{i_cdk});
            if strcmp(name_cdk{i_cdk},'venue_name')
                [id idx_in]=ismember(cdk_tmp.book,BOAT_VENUE_NAME);
                if any(id)
                    cdk_tmp.book(id)=BOAT_VENUE_NAME_4CDK(idx_in(id));
                end
            end
            cdk=codebook('stack',cdk,cdk_tmp);
        end
        %>
        
        %<add boat currency and quotation unit info
        boat_currency=vals(:,4);
        boat_quotation_unit=ones(size(boat_currency));
        %/ GBp / GBX
        boat_quotation_unit(strcmpi(boat_currency,'gbx'))=1/100;
        boat_currency(strcmpi(boat_currency,'gbx'))={'GBP'};
        boat_currency(strcmpi(boat_currency,'GBp'))={'GBP'};
        %/ ILS / ILa
        boat_quotation_unit(strcmpi(boat_currency,'ila'))=1/100;
        boat_currency(strcmpi(boat_currency,'ila'))={'ILS'};
        %/ ZAR / Zac
        boat_quotation_unit(strcmpi(boat_currency,'zac'))=1/100;
        boat_currency(strcmpi(boat_currency,'zac'))={'ZAR'};
        
        [uni_boat_currency,~,idx_in]=unique(boat_currency);
        uni_currency_id=get_repository('currency_short_name2id',uni_boat_currency);
        if any(~isfinite(uni_currency_id))
            error('get_dark_otc:currency', 'At least one of the boat currency is unknwon in the database <%s>',uni_boat_currency(find(~isfinite(uni_currency_id),1,'first')));
        end
        boat_currency_id=uni_currency_id(idx_in);
        vals_out=cat(2,vals_out,boat_currency_id,boat_quotation_unit);
        col_out=cat(2,col_out,{'boat_currency_id','boat_quotation_unit'});
        
        
        %< create output add day
        vals_out=cat(2,date,vals_out);
        col_out=cat(2,'day',col_out);
        
        data = st_data( 'init', ...
            'title', sprintf( 'BOAT daily stats : %s - %s', start_day, this_day), ...
            'value', vals_out, ...
            'date', date, ...
            'colnames', col_out);
        data.info =  ...
            struct( 'sql_request', sql_req_tmp, ...
            'data_log', data.info.data_log, ...
            'localtime',false,...
            'place_timezone_id', 0,...
            'local_place_timezone_id',NaN) ;
        data.codebook=cdk;
        data=st_data('from-idx',data,isfinite(st_data('col',data,'security_id')));
        %//renormalize unknown currency onthe referential...
        id_renorm=~isfinite(st_data('col',data,'ref_currency_id')) & isfinite(st_data('col',data,'boat_currency_id'));
        if any(id_renorm)
            data.value(id_renorm,strcmp(data.colnames,'ref_currency_id'))=data.value(id_renorm,strcmp(data.colnames,'boat_currency_id'));
        end
        id_renorm=~isfinite(st_data('col',data,'ref_quotation_unit')) & isfinite(st_data('col',data,'boat_quotation_unit'));
        if any(id_renorm)
            data.value(id_renorm,strcmp(data.colnames,'ref_quotation_unit'))=data.value(id_renorm,strcmp(data.colnames,'boat_quotation_unit'));
        end
        
        %// HANDLE CURRENCY AND QUOTATIOn UNIT ISSUE
        if any(st_data('col',data,'ref_currency_id')~=st_data('col',data,'boat_currency_id'))
            % st_data_explorer(st_data('from-idx',data,st_data('col',data,'ref_currency_id')~=st_data('col',data,'boat_currency_id')))
            error('get_dark_otc:td_boat', 'Boat publish in other currency than the reference !!');
        end
        multiplier_to_ref=st_data('col',data,'boat_quotation_unit')./st_data('col',data,'ref_quotation_unit');
        % on ne renormalize que le turnover, pas le volume !
        data.value(:,strcmp(data.colnames,'turnover'))=data.value(:,strcmp(data.colnames,'turnover')).*multiplier_to_ref;
        data=st_data('remove',data,{'boat_currency_id','boat_quotation_unit'});
        data=st_data('modif-cols',data,{'ref_currency_id','ref_quotation_unit'},'names',{'currency_id','quotation_unit'});
        
        if opt.get('add_rate_to_euro')
            uni_currency_id=unique(st_data('col',data,'currency_id'));
            tmp_rate_to_euro=get_repository('currency_rate2ref',...
                'from',datestr(day_start_,'dd/mm/yyyy'),...
                'to',datestr(day_,'dd/mm/yyyy'),...
                'currency_id',uni_currency_id,...
                'currency_id_ref',58);
            add_rate_to_euro=NaN(size(data.value,1),1);
            if ~st_data('isempty-nl',tmp_rate_to_euro)
                [id idx]=ismember(st_data('col',data,'day;currency_id'),st_data('col',tmp_rate_to_euro,'day;currency_id'),'rows');
                add_rate_to_euro(id)=st_data('col',tmp_rate_to_euro,'rate2ref',idx(id));
                add_rate_to_euro=add_rate_to_euro.*st_data('col',data,'quotation_unit');
            end
            clear tmp_rate_to_euro;
            data=st_data('add-col',data,add_rate_to_euro,'rate_to_euro');
        end
        
        data=st_data('remove',data,{'currency_id','quotation_unit'});
        
        %// FINAL AGGREGATION
        if ~isempty(indice_id)
            g_by_variables={'day','security_id','indice_id','venue_name'};
        else
            g_by_variables={'day','security_id','venue_name'};
        end
        
        g_by_formula=['['...
            'min({rate_to_euro}),' ...
            'sum({nb_trades}),' ...
            'sum({volume}),' ...
            'sum({turnover})' ...
            ']'];
        g_by_formula_colnames={...
            'rate_to_euro' ...
            'nb_trades' ...
            'volume' ...
            'turnover' ...
            };
        b_by_4stdata=cellfun(@(c)([',{' c '}']),g_by_variables,'uni',false);
        b_by_4stdata=[b_by_4stdata{:}];
        b_by_4stdata(1)=[];
        
        info_keep=data.info;
%         data=st_data('group-by-nan',...
%             data,...
%             b_by_4stdata,...
%             'apply-formula',g_by_formula);
%         data.colnames=cat(2,g_by_variables,g_by_formula_colnames);
%         data.info=info_keep;
        
        %%% Same treatment but a less time consumming version of it:
        g_colnames = tokenize(regexprep(b_by_4stdata,'(\{|\})',''),',');
        [g_un,~,g_idx] = unique(st_data('cols',data,g_colnames),'rows');
        formulae = tokenize(regexprep(g_by_formula,'(\[|\])',''),',');
        value = nan(size(g_un,1),size(g_un,2)+length(formulae));
        value(:,1:size(g_un,2)) = g_un;
        f_colnames = cell(1,length(formulae));
        for i = 1:length(formulae)
            f_colnames(i) = regexp(formulae{i},'{([^}]+)}','match');
            formulae{i} = strrep(formulae{i},f_colnames{i},'x');
            fun = str2func(['@(x)',formulae{i},'']);
            value(:,size(g_un,2)+i) = accumarray(g_idx,st_data('cols',data,f_colnames{i}(2:end-1)),[],fun);
        end
        f_colnames = regexprep(f_colnames,'({|})','');
        cdk_keep = data.codebook;
        data = st_data('init','value',value,'date',(1:size(value,1))',...
            'colnames',[g_colnames,f_colnames]);
        data.codebook = cdk_keep;
        data.info=info_keep;
        data.date = st_data('cols',data,'day');
        %%%
        
        % [~,idx_sort]=sortrows(cat(2,date,vals_out(:,[1 3])),[1 2 -3]);
        
        
    case 'td_enx'
        
        t0 = clock;
        info_ = get_dark_otc('info');
        ENX_VENUE_NAME=info_.enx_name.database_name;
        ENX_VENUE_NAME_4CDK=info_.enx_name.cdk_name;
        
        
        %<* Read into the tickdb
        opt = options({'security_id', [],... % is iselpty -> all security !
            'indice_id',[],...
            'day', '',...
            'day_start','',...
            'venue_name',{},...% restrict venues
            'add_rate_to_euro',true,...
            'date_format:char', 'dd/mm/yyyy' ...
            }, varargin);
        %< Lecture des options
        format_date = opt.get('date_format:char');
        sec_id = opt.get('security_id');
        indice_id = opt.get('indice_id');
        
        if ~isnumeric( sec_id) || ~isnumeric(indice_id)
            error('get_dark_otc:secid', 'I need numerical security id list');
        end
        
        if ~isempty( sec_id) && ~isempty(indice_id)
            error('get_dark_otc:secid', 'security or indice id !! ');
        end
        
        day_ = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        day_start_ = opt.get('day_start');
        
        if isempty(day_start_)
            day_start_=day_;
        elseif ischar( day_start_)
            day_start_ = datenum( day_start_, format_date);
        end
        
        venue = opt.get('venue_name');
        if ~iscellstr(venue)
            error('get_dark_otc:venue', 'venue has to be a cellarray');
        end
        if ~isempty(venue) && ...
                any(~ismember(venue,ENX_VENUE_NAME))
            error('get_dark_otc:td_boat', 'At least one venue is not recognized');
        end
        %>
        
        %< TESTS
        if day_<day_start_
            error('get_dark_otc:day', 'bad inputs day - day_start');
        end
        if day_<734779 % 03 october 2011 : data are only available starting this date on that format
            error('get_dark_otc:day', 'No data before 3nd October 2011');
        end
        start_day = datestr( day_start_, 'yyyymmdd');
        this_day = datestr( day_, 'yyyymmdd');
        %>
        
        %< --------------------- SQL
        % enx
        enx_prefix='enx';
        enx_table_name=['deal_enx_trs'];
        year_month_list=unique(cat(2,year((day_start_:day_)'),month((day_start_:day_)')),'rows');
        enx_base_name=cell(size(year_month_list,1),1);
        for i_=1:size(year_month_list,1)
            enx_base_name{i_}=get_basename4day(datenum(year_month_list(i_,1),year_month_list(i_,2),1));
        end
        
        % security_dictionnary
        
        %// ATTENTION IL peut y avoir un probleme de currency !!!!!
        %/
        secdictio_prefix='secdic';
        secdictio_table_name=['security_dictionary'];
        secdictio_base_name='repository';
        %/
        sh_prefix='sh';
        sh_table_name=['security_historic'];
        sh_base_name='repository';
        %/
        sec_prefix='s';
        sec_table_name=['security'];
        sec_base_name='repository';
        %/
        ind_prefix='ind';
        ind_table_name=['indice_component_master'];
        ind_base_name='repository';
        
        
        select_req_name={'date','security_id','venue','currency','price_multiplier','nb','volume','turnover','ref_quotation_unit','ref_currency_id'};
        select_str = sprintf([  'select ' ...
            ' %s = %s.origin_date,%s = %s.security_id,%s = %s.venue,%s = %s.currency,%s = %s.price_multiplier,' ...
            ' %s = count(*),%s = sum(%s.size),%s = sum(%s.size*%s.price) ,'...
            ' %s=ISNULL(min(%s.quotation_unit),1), %s=min(%s.currency_id)'],...
            select_req_name{1},enx_prefix,...
            select_req_name{2},secdictio_prefix,...
            select_req_name{3},enx_prefix,...
            select_req_name{4},enx_prefix,...
            select_req_name{5},enx_prefix,...
            select_req_name{6},...
            select_req_name{7},enx_prefix,...
            select_req_name{8},enx_prefix,enx_prefix,...
            select_req_name{9},sh_prefix,...
            select_req_name{10},sh_prefix);
        
        if ~isempty(indice_id)
            select_req_name=cat(2,select_req_name,{'indice_id'});
            select_str=[select_str ...
                sprintf(' , %s = %s.indice_id',select_req_name{end},ind_prefix)];
        end
        
        end_req = sprintf(['  where %s.origin_date >= ''%s'' ' ...
            ' and %s.origin_date <= ''%s'' ' ],...
            enx_prefix,start_day,enx_prefix,this_day);
        if ~isempty(venue)
            venue_char = cellfun(@(c)(sprintf(['''%s'','],c)),venue,'uni',false);
            venue_char=[venue_char{:}];
            venue_char(end) = [];
            end_req=[end_req sprintf(' and %s.venue in (%s) ',enx_prefix,venue_char)];
        end
        if ~isempty(sec_id)
            sec_char = sprintf('%d,',sec_id);
            sec_char(end) = [];
            end_req=[end_req sprintf(' and %s.security_id in (%s) ',secdictio_prefix,sec_char)];
        elseif ~isempty(indice_id)
            sec_char = sprintf('%d,',indice_id);
            sec_char(end) = [];
            end_req=[end_req ...
                sprintf(' and %s.indice_id in (%s) ',ind_prefix,sec_char) ...
                sprintf(' and %s.security_id = %s.security_id  ',ind_prefix,secdictio_prefix) ...
                sprintf(' and %s.begin_date<= %s.origin_date  ',ind_prefix,enx_prefix) ...
                sprintf(' and ( %s.end_date >= %s.origin_date or %s.end_date is null ) ',ind_prefix,enx_prefix,ind_prefix ) ...
                ];
        end
        
        % secditcio
        end_req=[end_req ...
            sprintf(' and  %s.reference=%s.isin and %s.dictionary_id = 5 ',secdictio_prefix,enx_prefix,secdictio_prefix) ...
            sprintf(' and  %s.security_id=%s.security_id and %s.security_id=%s.security_id ',secdictio_prefix,sh_prefix,secdictio_prefix,sec_prefix) ...
            sprintf(' and  %s.end_date is null and %s.place_id = %s.primary_place_id ',sh_prefix,sec_prefix,sec_prefix) ...
            ];
        
        end_req=[end_req ...
            sprintf(' group by  %s.origin_date,%s.security_id,%s.venue,%s.currency,%s.price_multiplier',...
            enx_prefix,secdictio_prefix,enx_prefix,enx_prefix,enx_prefix)];
        
        if ~isempty(indice_id)
            end_req=[end_req sprintf(',%s.indice_id',ind_prefix)];
        end
        
        %< extract
        vals=[];
        st_log('just before fetch (%5.2f sec)\n', etime(clock, t0));
        t1 = clock;
        for i_=1:size(year_month_list,1)
            main_req_tmp=sprintf('%s..%s %s, %s..%s %s, %s..%s %s, %s..%s %s',...
                enx_base_name{i_},enx_table_name,enx_prefix,...
                secdictio_base_name,secdictio_table_name,secdictio_prefix,...
                sh_base_name,sh_table_name,sh_prefix,...
                sec_base_name,sec_table_name,sec_prefix);
            if ~isempty(indice_id)
                main_req_tmp=[main_req_tmp ...
                    sprintf(', %s..%s %s',ind_base_name,ind_table_name,ind_prefix)];
            end
            sql_req_tmp = [select_str ' from ' main_req_tmp ' ' end_req];
            vals_tmp = exec_sql('BSIRIUS', sql_req_tmp);
            if ~isempty(vals_tmp)
                vals = cat(1,vals,vals_tmp);
            end
        end
        st_log('end of fetch (%5.2f sec)\n', etime(clock, t1));
        %>
        if isempty(vals)
            data = [];
            return;
        end
        
        %< Dates and times
        date=datenum(vals(:,1), 'yyyy-mm-dd');
        %>
        
        if ~isempty(indice_id)
            col_out={'security_id','indice_id','nb_trades','volume','turnover','ref_quotation_unit','ref_currency_id'};
            vals_out=cellfun(@(c)(c*1),vals(:,[2 11 6 7 8 9 10]));
        else
            col_out={'security_id','nb_trades','volume','turnover','ref_quotation_unit','ref_currency_id'};
            vals_out=cellfun(@(c)(c*1),vals(:,[2 6 7 8 9 10]));
        end
        
        %< cdk data
        num_cdk=[3];
        name_cdk={'venue_name'};
        cdk=[];
        
        for i_cdk=1:length(num_cdk)
            cdk_tmp=codebook('new',vals(:,num_cdk(i_cdk)),name_cdk{i_cdk});
            vals_out=cat(2,vals_out,...
                codebook('extract','names2ids',cdk_tmp,name_cdk{i_cdk},cellfun(@strtrim, vals(:,num_cdk(i_cdk)),'uni', false)));
            col_out=cat(2,col_out,name_cdk{i_cdk});
            if strcmp(name_cdk{i_cdk},'venue_name')
                [id idx_in]=ismember(cdk_tmp.book,ENX_VENUE_NAME);
                if any(id)
                    cdk_tmp.book(id)=ENX_VENUE_NAME_4CDK(idx_in(id));
                end
            end
            cdk=codebook('stack',cdk,cdk_tmp);
        end
        %>
        
        %<add enx_ currency and quotation unit info
        enx_currency=vals(:,4);
        enx_quotation_unit=cell2mat(vals(:,5));
        %/ GBp / GBX
        enx_quotation_unit(strcmpi(enx_currency,'gbx'))=1/100;
        enx_currency(strcmpi(enx_currency,'gbx'))={'GBP'};
        enx_currency(strcmpi(enx_currency,'GBp'))={'GBP'};
        %/ ILS / ILa
        enx_quotation_unit(strcmpi(enx_currency,'ila'))=1/100;
        enx_currency(strcmpi(enx_currency,'ila'))={'ILS'};
        %/ ZAR / Zac
        enx_quotation_unit(strcmpi(enx_currency,'zac'))=1/100;
        enx_currency(strcmpi(enx_currency,'zac'))={'ZAR'};
        
        [uni_enx_currency,~,idx_in]=unique(enx_currency);
        uni_currency_id=get_repository('currency_short_name2id',uni_enx_currency);
        if any(~isfinite(uni_currency_id))
            error('get_dark_otc:currency', 'At least one of the boat currency is unknwon in the database <%s>',uni_enx_currency(find(~isfinite(uni_currency_id),1,'first')));
        end
        enx_currency_id=uni_currency_id(idx_in);
        vals_out=cat(2,vals_out,enx_currency_id,enx_quotation_unit);
        col_out=cat(2,col_out,{'enx_currency_id','enx_quotation_unit'});
        
        
        %< create output add day
        vals_out=cat(2,date,vals_out);
        col_out=cat(2,'day',col_out);
        
        data = st_data( 'init', ...
            'title', sprintf( 'BOAT daily stats : %s - %s', start_day, this_day), ...
            'value', vals_out, ...
            'date', date, ...
            'colnames', col_out);
        data.info =  ...
            struct( 'sql_request', sql_req_tmp, ...
            'data_log', data.info.data_log, ...
            'localtime',false,...
            'place_timezone_id', 0,...
            'local_place_timezone_id',NaN) ;
        data.codebook=cdk;
        data=st_data('from-idx',data,isfinite(st_data('col',data,'security_id')));
        %//renormalize unknown currency onthe referential...
        id_renorm=~isfinite(st_data('col',data,'ref_currency_id')) & isfinite(st_data('col',data,'enx_currency_id'));
        if any(id_renorm)
            data.value(id_renorm,strcmp(data.colnames,'ref_currency_id'))=data.value(id_renorm,strcmp(data.colnames,'enx_currency_id'));
        end
        id_renorm=~isfinite(st_data('col',data,'ref_quotation_unit')) & isfinite(st_data('col',data,'enx_quotation_unit'));
        if any(id_renorm)
            data.value(id_renorm,strcmp(data.colnames,'ref_quotation_unit'))=data.value(id_renorm,strcmp(data.colnames,'enx_quotation_unit'));
        end
        
        %// HANDLE CURRENCY AND QUOTATIOn UNIT ISSUE
        multiplier_to_ref=st_data('col',data,'enx_quotation_unit')./st_data('col',data,'ref_quotation_unit');
        multiplier_currency_to_ref=ones(size(multiplier_to_ref));
        if any(st_data('col',data,'ref_currency_id')~=st_data('col',data,'enx_currency_id'))
            % st_data_explorer(st_data('from-idx',data,st_data('col',data,'ref_currency_id')~=st_data('col',data,'enx_currency_id')))
            %error('get_dark_otc:td_boat', 'Boat publish in other currency than the reference !!');
            
            [uni_curr_enx_ref]=unique(st_data('col',data,'enx_currency_id;ref_currency_id',...
                st_data('col',data,'ref_currency_id')~=st_data('col',data,'enx_currency_id')),'rows');
            
            for i_c=1:size(uni_curr_enx_ref,1)
                tmp_rate2ref=get_repository('currency_rate2ref',...
                    'from',datestr(min(st_data('col',data,'day')),'dd/mm/yyyy'),...
                    'to',datestr(max(st_data('col',data,'day')),'dd/mm/yyyy'),...
                    'currency_id',uni_curr_enx_ref(i_c,1),...
                    'currency_id_ref',uni_curr_enx_ref(i_c,2));
                if ~st_data('isempty-nl',tmp_rate2ref)
                    [id idx]=ismember(st_data('col',data,'day;enx_currency_id;ref_currency_id'),...
                        cat(2,st_data('col',tmp_rate2ref,'day;currency_id'),uni_curr_enx_ref(i_c,2)*ones(size(tmp_rate2ref.value,1),1)),'rows');
                    multiplier_currency_to_ref(id)=st_data('col',tmp_rate2ref,'rate2ref',idx(id));
                end
            end
            clear tmp_rate2ref;
        end
        
        data.value(:,strcmp(data.colnames,'turnover'))=data.value(:,strcmp(data.colnames,'turnover')).*multiplier_to_ref.*multiplier_currency_to_ref;
        data=st_data('remove',data,{'enx_currency_id','enx_quotation_unit'});
        data=st_data('modif-cols',data,{'ref_currency_id','ref_quotation_unit'},'names',{'currency_id','quotation_unit'});
        
        if opt.get('add_rate_to_euro')
            uni_currency_id=unique(st_data('col',data,'currency_id'));
            tmp_rate_to_euro=get_repository('currency_rate2ref',...
                'from',datestr(day_start_,'dd/mm/yyyy'),...
                'to',datestr(day_,'dd/mm/yyyy'),...
                'currency_id',uni_currency_id,...
                'currency_id_ref',58);
            add_rate_to_euro=NaN(size(data.value,1),1);
            if ~st_data('isempty-nl',tmp_rate_to_euro)
                [id idx]=ismember(st_data('col',data,'day;currency_id'),st_data('col',tmp_rate_to_euro,'day;currency_id'),'rows');
                add_rate_to_euro(id)=st_data('col',tmp_rate_to_euro,'rate2ref',idx(id));
                add_rate_to_euro=add_rate_to_euro.*st_data('col',data,'quotation_unit');
            end
            clear tmp_rate_to_euro;
            data=st_data('add-col',data,add_rate_to_euro,'rate_to_euro');
        end
        
        data=st_data('remove',data,{'currency_id','quotation_unit'});
        
        %// FINAL AGGREGATION
        g_by_variables={'day','security_id','venue_name'};
        if ~isempty(indice_id)
            g_by_variables={'day','security_id','indice_id','venue_name'};
        end
        
        g_by_formula=['['...
            'min({rate_to_euro}),' ...
            'sum({nb_trades}),' ...
            'sum({volume}),' ...
            'sum({turnover})' ...
            ']'];
        g_by_formula_colnames={...
            'rate_to_euro' ...
            'nb_trades' ...
            'volume' ...
            'turnover' ...
            };
        b_by_4stdata=cellfun(@(c)([',{' c '}']),g_by_variables,'uni',false);
        b_by_4stdata=[b_by_4stdata{:}];
        b_by_4stdata(1)=[];
        
        info_keep=data.info;
        data=st_data('group-by-nan',...
            data,...
            b_by_4stdata,...
            'apply-formula',g_by_formula);
        data.colnames=cat(2,g_by_variables,g_by_formula_colnames);
        data.info=info_keep;
        %>
        
        
        
        
    case 'trading_daily'
        
        data=[];
        
        %< -----------------------------------------------  Read inputs
        opt = options_light({'security_id', [],... % is iselpty -> all security !
            'indice_id',[],...
            'day', '',...
            'day_start','',...
            'boat_venue_name',{},...% restrict venues
            'enx_venue_name',{},...
            }, varargin);
        
        sec_id = opt.security_id;
        indice_id = opt.indice_id;
        day = opt.day;
        day_start = opt.day_start;
        boat_venue_name = opt.boat_venue_name;
        enx_venue_name = opt.enx_venue_name;
        %>*
        
        info_ = get_dark_otc('info');
        
        %< ---------------------------------------------- extract Data
        %< boat
        data_boat=get_dark_otc('td_boat',...
            'security_id',sec_id,...
            'indice_id',indice_id,...
            'day',day,...
            'day_start',day_start,...
            'venue_name',boat_venue_name,...
            'add_rate_to_euro',true);
        if ~st_data('isempty-nl',data_boat)
            data_boat=st_data('modif-cols',data_boat,...
                'venue_name;nb_trades;volume;turnover',...
                'names','venue;dark_nb_trades;dark_volume;dark_turnover');
            td_id=NaN(size(data_boat.value,1),1);
            [id idx]=ismember(st_data('col_cdk',data_boat,'venue'),info_.boat_name.cdk_name);
            td_id(id)=info_.boat_name.trading_destination_id(idx(id));
            data_boat=st_data('add-col',data_boat,td_id,'trading_destination_id');
        end
        
        %< enx
        data_enx=get_dark_otc('td_enx',...
            'security_id',sec_id,...
            'indice_id',indice_id,...
            'day',day,...
            'day_start',day_start,...
            'venue_name',enx_venue_name,...
            'add_rate_to_euro',true);
        if ~st_data('isempty-nl',data_enx)
            data_enx=st_data('modif-cols',data_enx,...
                'venue_name;nb_trades;volume;turnover',...
                'names','venue;dark_nb_trades;dark_volume;dark_turnover');
            td_id=NaN(size(data_enx.value,1),1);
            [id idx]=ismember(st_data('col_cdk',data_enx,'venue'),info_.enx_name.cdk_name);
            td_id(id)=info_.enx_name.trading_destination_id(idx(id));
            data_enx=st_data('add-col',data_enx,td_id,'trading_destination_id');
        end
        
        %< td_daily_dark
        data_td_daily_dark=get_trading_daily('base',...
            'security_id',sec_id,...
            'indice_id',indice_id,...
            'day_start',day_start,...
            'day',day,...
            'td_include_all_destination',false,...
            'td_fields',{'dark_nb_deal','dark_volume','dark_turnover'},...
            'add_codebook',true,...
            'add_rate_to_euro',true);
        if ~st_data('isempty-nl',data_td_daily_dark)
            td_id=st_data('col',data_td_daily_dark,'trading_destination_id');
            data_td_daily_dark=renormalize_tddaily_dark(data_td_daily_dark,'trading_destination_id_cdk');
            data_td_daily_dark=st_data('remove',data_td_daily_dark,'trading_destination_id;security_id_cdk');
            data_td_daily_dark=st_data('modif-cols',data_td_daily_dark,...
                'trading_destination_id_cdk;dark_nb_deal',...
                'names','venue;dark_nb_trades');
            data_td_daily_dark=st_data('add-col',data_td_daily_dark,td_id,'trading_destination_id');
            data_td_daily_dark=st_data('from-idx',data_td_daily_dark,...
                st_data('col',data_td_daily_dark,'dark_nb_trades')>0);
        end
        
        %< ---- Outputs
        data=st_data('extended_stack',{data_boat,data_enx,data_td_daily_dark});
        % st_data_explorer(data)
        if ~st_data('isempty-nl',data)
            if ~ismember('rate_to_euro',data.colnames)
                data=st_data('add-col',data,NaN(size(data.date)),'rate_to_euro');
            end
            
            [~,idx_sort]=sortrows(st_data('col',data,'day;security_id;venue'),[1 2 3]);
            data=st_data('from-idx',data,idx_sort);
        end
        
        
end
end

function out=renormalize_tddaily_dark(data,td_col_name)


if ~st_data('isempty-nl',data ) &&...
        ismember(td_col_name,data.colnames) && ...
        isfield(data,'codebook') && ...
        ismember(td_col_name,{data.codebook.colname})
    
    cdk_td=codebook('extract','codebook',data.codebook,td_col_name);
    max_td_value=max(cdk_td.colnum)+1000;
    
    % Normalize 'CHI-X'
    normalize_names_2find={'CHI-X','EURONEXT','OMX','TURQUOISE','BATS'};
    normalize_names={'Chi-delta','EURONEXT','OMX','Turquoise Dark','Bats Dark'};
    for i_n=1:length(normalize_names_2find)
        id_in_cdk=cellfun(@(c)(~isempty(strfind(c,normalize_names_2find{i_n}))),cdk_td.book);
        if any(id_in_cdk)
            data.value(ismember(data.value(:,strcmp(data.colnames,td_col_name)),...
                cdk_td.colnum(id_in_cdk)),strcmp(data.colnames,td_col_name))=max_td_value;
            cdk_td.book(id_in_cdk)=[];
            cdk_td.colnum(id_in_cdk)=[];
            cdk_td=codebook('stack',cdk_td,codebook('new',normalize_names(i_n),td_col_name,max_td_value));
        end
        max_td_value=max_td_value+1;
    end
    data.codebook(ismember({data.codebook.colname},td_col_name))=[];
    data.codebook=codebook('stack',data.codebook,cdk_td);
end

out=data;

end



% % function [main_req_out select_str_out select_str_name_out]=get_rate_to_euro_add_info(mode,td_prefix,sechisto_prefix,currencyp_prefix,curencytm_prefix)
% %
% %
% % main_req_out=sprintf([...
% %     ' left join repository..security_historic %s ' ...
% %     ' on ( ' ...
% %     ' %s.security_id = %s.security_id ' ...
% %     ' and %s.begin_date<= %s.date ' ...
% %     ' and ( %s.end_date >= %s.date or %s.end_date is null ) ' ...
% %     ' ) ' ...
% %     ' left join repository..currency_pair %s ' ...
% %     ' on ( ' ...
% %     ' (%s.currency_id = %s.ref_currency_id  and %s.currency_id = 58 ) or ' ...
% %     ' (%s.currency_id = 58 and %s.currency_id = 58 and %s.ref_currency_id = 31 ) ' ...
% %     ' ) ' ...
% %     ' left join repository..histo_currency_time_serie %s ' ...
% %     ' on ( ' ...
% %     ' %s.date = %s.date and ' ...
% %     ' %s.currency_pair_id = %s.currency_pair_id and ' ...
% %     ' %s.attribut_id = 1 and %s.time_serie_id = 1 ) '],...
% %     sechisto_prefix,...
% %     sechisto_prefix,td_prefix,...
% %     sechisto_prefix,td_prefix,...
% %     sechisto_prefix,td_prefix,sechisto_prefix,...
% %     currencyp_prefix,...
% %     sechisto_prefix,currencyp_prefix,currencyp_prefix,...
% %     sechisto_prefix,currencyp_prefix,currencyp_prefix,...
% %     curencytm_prefix,...
% %     td_prefix,curencytm_prefix,...
% %     curencytm_prefix,currencyp_prefix,...
% %     curencytm_prefix,curencytm_prefix);
% %
% % select_str_name_out={'rate_to_euro'};
% %
% % switch mode
% %     case 'base'
% %         select_str_out=sprintf(' (case when %s.currency_id <> 58 then isnull(%s.quotation_unit,1)/convert(float, %s.value) when %s.currency_id = 58  then isnull(%s.quotation_unit, 1) end) ',...
% %             sechisto_prefix,sechisto_prefix,curencytm_prefix,sechisto_prefix,sechisto_prefix);
% %     case 'agg'
% %         select_str_out=sprintf(' rate_to_euro = (case when %s.currency_id <> 58 then isnull(%s.quotation_unit,1)/convert(float, %s.value) when %s.currency_id = 58  then isnull(%s.quotation_unit, 1) end) ',...
% %             sechisto_prefix,sechisto_prefix,curencytm_prefix,sechisto_prefix,sechisto_prefix);
% %     otherwise
% %         error('get_rate_to_euro_add_info: mode <%s> is unknown ',mode);
% % end
% %
% % end

