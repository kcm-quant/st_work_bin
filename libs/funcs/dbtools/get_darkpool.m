function data = get_darkpool( mode, varargin)
% GET_DARKPOOL - interface pour les donn�es de type dark pool (donc un peu
%   exotiques)
%
% internal uses:
% - get_darkpool('masterkey', 'em', 'security_id', 110 )
% - get_darkpool('em', 'security_id', 110, 'day', '01/04/2008' )
% - get_darkpool('euromillenium', 'security_id', 'FTE', 'from', '01/04/2008', 'to', '30/04/2008'  )
%
% get_darkpool('em', 'security_id', 110, 'day', '20/10/2008' )
% get_darkpool('masterkey', 'em', 'security_id', 110 )
% get_darkpool('euromillenium', 'security_id', 'FTE.PA', 'from', '20/10/2008', 'to', '21/10/2008'  )
%
% See also get_tick from_buffer read_dataset


switch lower(mode)
    case 'euromillenium'
         %<* intra day data
        opt   = options( { 'source', '',  'trading-destinations', {}, ...
            'initial-colnames', {}, 'final-colnames', {}, 'where_f-td', '', 'filter-sec', true}, varargin);
        lst = opt.get();
        kodes = get_repository( 'any-to-sec-td', lst{:});
        code  = kodes.security_key;
        sid   = kodes.security_id;
        o_tdi = kodes.trading_destination_id;
        opt.set('security_id', sid);
        st_log('get_tick:data looking for data for <%s>...\n', code);
        lst   = opt.get();
        data = from_buffer( 'get_darkpool', 'em', lst{:}) ;
        if opt.get('filter-sec')
            data = st_data('where', data, sprintf('{sec id}==%d', sid));
        end
        %>
    case 'masterkey'
        %<* Return the generic+specific dir
        fmode = varargin{1};
        fmode = regexprep( fmode, '[ |*|\\|/|:|?|"|<|>|\|]', '_');
        data = fmode;
        %>*
    case 'em'
        %<* Read into the tickdb
        opt = options({'security_id', 110, 'day', '01/04/2008',...
            'date_format:char', 'dd/mm/yyyy' ...
            }, varargin);
        %< Lecture des options
        format_date = opt.get('date_format:char');
        sec_id = opt.get('security_id');
        if ~isnumeric( sec_id) && numel(sec_id)==1
            error('get_tick:secid', 'I need an unique numerical security id');
        end
        day_ = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        %>
        this_day = datestr( day_, 'yyyymmdd');
        vals = exec_sql('BSIRIUS', sprintf([ 'select    security_id , '...
            '   convert(char(10),date,103)  , '...
            '    ping_orders , '...
            '   ping_volume , '...
            '    resident_orders , '...
            '    resident_volume , '...
            '    total_orders , '...
            '   total_volume  , '...
            '   exposed_liquidity , '...
            '    executed_orders , '...
            '    executed_volume  from tick_db..euro_millennium_daily where date = ''%s'' '], this_day));
        if isempty(vals)
            data = st_data('empty-init');
            st_log(sprintf('%s is empty!', this_day));
        else
            data = st_data('init', 'title', 'EUROMILLENNIUM', ...
            'date', datenum( vals(:,2), 'dd/mm/yyyy'), ...
            'value', cell2mat(vals(:,[ 1 3:end])), ...
            'colnames', { 'sec id', 'nb ping', 'volume ping', 'nb resident', 'volume resident', 'nb order', 'volume order', ...
            'exposed', 'nb exec', 'volume exec'});
        end
end