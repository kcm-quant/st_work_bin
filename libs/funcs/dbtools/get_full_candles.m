function data = get_full_candles(mode, varargin)
% d=get_full_candles('fc', 'security_id', 13375, 'day', '09/07/2009')
% d=from_buffer('get_full_candles', 'fc', 'security_id', 13375, 'from', '09/07/2009', 'to', '09/07/2009')
switch mode
    case 'masterkey'
        %<* Return the generic+specific dir
        fmode = varargin{1};
        opt = options(varargin(2:end));
        sec_id = opt.get('security_id');
        data = fullfile(fmode, get_repository( 'security-key', sec_id));
        %>*
    case 'fc'
        opt = options(varargin);
        date_n = datenum(opt.get('day'), 'dd/mm/yyyy');
        if date_n >= today
            dbs = dbstack;
            if date_n > today || ~opt.try_get('today_allowed',false) || ismember('from_buffer.m',{dbs.file})
                error('Data''s not available yet');
            else
                proc_name = 'candles1min_beta_sameday';
            end
        else
            proc_name = 'candles1min_beta';
        end
        sec_id = opt.get('security_id');
        drf=setdbprefs('DataReturnFormat');
        setdbprefs('DataReturnFormat', 'numeric');
        t1 = clock;
        try
            vals = exec_sql('BSIRIUS', sprintf('%s ''%s'', %d', proc_name,datestr(date_n, 'yyyymmdd'), sec_id));
            % Si vous �tes arr�t� dans la debugger parceque vous avez
            % interrompu Matlab (Ctrl+C), alors vous devriez �x�cuter la
            % ligne ci-dessous
            setdbprefs('DataReturnFormat', drf);
        catch ME
            setdbprefs('DataReturnFormat', drf);
            rethrow(ME);
        end
        st_log('end of fetch (%5.2f sec)\n', etime(clock, t1));
        info = struct('security_id', sec_id, ...
            'security_key', get_repository( 'security-key', sec_id), ...
            'td_info', get_repository( 'trading-destination-info', sec_id) , ...
            'localtime', true , 'data_log', []);
        if ~isempty(vals)
            data = st_data('init', 'title', info.security_key, 'value', vals(:, 2:end), 'date', date_n + vals(:, 1)/(24*3600), ...
                'colnames', {'ms_open', 'ms_close', 'volume', 'turnover', 'turnover_overbid','turnover_overask'...
                'nb_deal', 'volume_overbid','volume_overask', ...
                'open','high','low','close','open_ask','open_bid','average_spread_numer','average_spread_denom', ...
                'sum_price','auction','opening_auction','intraday_auction','closing_auction','trading_at_last','trading_after_hours','cross', 'trading_destination_id'});
        else
            data = st_data('empty-init');
        end
%         d=st_data('apply-formula', data, '(({high}-{low}).^2)/2-(2*log(2)-1)*({close}-{open}).^2');d.value
        info.data_log = data.info.data_log;
        data.info = info;
        data.info.localtime = data.info.td_info(1).localtime;
end