function data = get_future( mode, varargin)
% GET_FUTURE - version de get_tick pour le future CAC40
% NOTES on trading-destinations
%    it  refer to future_maturity_id
%    to use the front future, just put it as empty !
%    to use all the maturity, write 'all'
%    to use some specifics maturity_id sur [269 156]...
% internal uses:
% get_future('masterkey', 'futurefull', 'security_id','CAC 40' )
% get_future('futurefull', 'security_id','CAC 40','day','16/04/2010')
% data1=get_future('filter-maturity', 'security_id','CAC 40','trading-destinations',[],'from','10/03/2010','to','15/03/2010');
% data2=get_future('filter-maturity', 'security_id','CAC 40','trading-destinations','all','from','10/03/2010','to','15/03/2010');
% data3=get_future('filter-maturity', 'security_id','CAC 40','trading-destinations',269,'from','10/03/2010','to','15/03/2010');
% data4=read_dataset('future', 'security_id','CAC 40','from','10/03/2010','to','15/03/2010');

% ATTENTION IL Y A BEAUCOUP DE TODO
%
% See also from_buffer read_dataset 



switch lower(mode)
    
    case 'masterkey'
        %<* Return the generic+specific dir
        opt = options({'security_id', ''}, varargin(2:end));
        fname = opt.get('security_id');
        if isnumeric(fname)
            fname=get_repository('fut_id2fut_name',fname);
        else
            all_fname=unique(exec_sql('BSIRIUS', sprintf('select future_name from repository..future')));
            if ~any(ismember(fname,all_fname))
                error('get_future:This future name is not in the database');
            end
        end
        data = fullfile( varargin{1}, fname); % TODO
        %>*
        
    case 'futurefull'
        
        t0 = clock;
        
        %<* Read into the tickdb
        opt = options({'security_id', '', 'day', '01/04/2008',...
            'date_format:char', 'dd/mm/yyyy' ...
            }, varargin);
        %< Lecture des options
        format_date = opt.get('date_format:char');
        fname = opt.get('security_id');
        if isnumeric(fname)
            fname=get_repository('fut_id2fut_name',fname);
        end
        day_ = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        %>
        this_day = datestr( day_, 'yyyymmdd');
        
        
        future_maturity_list=exec_sql('BSIRIUS', sprintf(['select future_maturity_id,future_id,maturity_code, ' ...
            ' maturity_date,begin_date,end_date,is_generic' ...
            ' from repository..future_maturity '...
            ' where future_id in (select future_id from repository..future where future_name=''%s'') '...
            ' and begin_date<=''%s'' ' ...
            ' and end_date>=''%s'' ' ...
            ' order by future_maturity_id '],fname,this_day,this_day));
        
        % on r�cuperere dans les donn�s bufferis�es, toutes les maturit�s du
        % future actuellement enregistr�e
        
        future_maturity_id_list=cell2mat(future_maturity_list(:,1));
        
        if isempty(future_maturity_id_list)
            data = [];
            return
        end
        future_maturity_id_char=vect_to_char(future_maturity_id_list);
        
        % < requete
        select_str = [  'select date,datediff(millisecond,''00:00:00'',time),microseconds, ' ...
            ' future_maturity_id, price,size,bid,ask,'...
            ' bid_size,ask_size, '...
            ' cross,strategy ' ...
            ' from '];
        colnames={'future_maturity_id', 'price','size','bid','ask','bid_size','ask_size',...
            'cross','strategy'};
        deal_name='future_deal';
        main_req = sprintf('%s..%s', get_basename4day( day_), deal_name);
        %>
        end_req = sprintf(['  where date = ''%s'' ' ...
            '   and future_maturity_id in %s   ' ...
            '  order by date,time,microseconds ' ], ...
            this_day, future_maturity_id_char);
        
        sql_req = [select_str main_req end_req];
        st_log('just before fetch (%5.2f sec)\n', etime(clock, t0));
        
        t1 = clock;
        vals = exec_sql('BSIRIUS', sql_req);
        st_log('end of fetch (%5.2f sec)\n', etime(clock, t1));
        
        if isempty(vals)
            data = [];
            return;
        end
        
        sec1 = datenum(0,0,0,0,0,1);
        
        date_time = datenum(vals(1,1), 'yyyy-mm-dd') + round(cell2mat(vals(:,2))/1000)*sec1 + ...
            cell2mat(vals(:,3))*sec1*1e-6;
        vals        = vals(:,4:end);
        data = cellfun(@(x)double(x),vals);
        
        %< Passage en heure locale
        %         if ~td_info(1).localtime
        %             decalage  = timezone('get_offset','final_place_id', td_info(1).place_id, 'dates', floor( date_time(1)));
        %             date_time = date_time + decalage;
        %         end
        
        data = st_data( 'init', 'title', sprintf( '%s - %s',fname, datestr(day_, 'dd/mm/yyyy')), ...
            'value', data, ...
            'date', date_time,...
            'colnames', colnames);
        
        
        data.info.future_name=fname;
        data.info.future_maturity_list=future_maturity_list;
        data.info.sql_request=sql_req;
        data.info.localtime=false;
        data.info.place_timezone_id=0;
        data.info.local_place_timezone_id=NaN;
        
    case 'filter-maturity'
        
        opt   = options( { 'security_id', '',  'trading-destinations', {},'output-mode',''}, varargin);
        st_log('get_future:looking for data ...\n');
        % - on force ici � regarder les data par jours
        output_mode_initial=opt.get('output-mode');
        opt.set('output-mode','day');
        lst   = opt.get();
        data = from_buffer( 'get_future', 'futurefull', lst{:}) ;
        if isempty( data)
            return
        end
        td_ids=opt.get('trading-destinations');
        future_id=opt.get('security_id');
        
        if strcmp(output_mode_initial,'day')
            data_out=cell(length(data),1);
        else
            data_out=st_data('empty-init');
        end
        
        for c=1:length(data)
            if st_data('isempty-no_log', data{c})
                % nothing to do
            else
                % On note le place_id initial car les donn�es sont
                % timestamp�es en heures locales de cette place
                day=floor(data{c}.date(1));
                
                % suivant la donn�es des future maturity id
                if isempty(td_ids)
                    % - on doit cherch� la maturity_id front
                    front_maturity_id=get_repository('future-maturity-front',future_id,day);
                    fut_maturity_id=data{c}.value(:,strcmp('future_maturity_id',data{c}.colnames));
                    idx_to_keep=ismember(fut_maturity_id,front_maturity_id);
                    if any(idx_to_keep)
                        data_tmp=st_data('from-idx',data{c},idx_to_keep);
                    else
                        data_tmp=[];
                    end
                elseif strcmp(td_ids,'all')
                    % - on renvoit TOUT
                    data_tmp=data{c};
                elseif all(isnumeric(td_ids))
                    fut_maturity_id=data{c}.value(:,strcmp('future_maturity_id',data{c}.colnames));
                    idx_to_keep=ismember(fut_maturity_id,td_ids);
                    if any(idx_to_keep)
                        data_tmp=st_data('from-idx',data{c},idx_to_keep);
                    else
                        data_tmp=[];
                    end
                else
                    data_tmp=[];
                end
                
                
                if strcmp(output_mode_initial,'day')
                    data_out{c}=st_data('from-idx',data{c},idx_to_keep);
                else
                    data_out=st_data('stack',data_out,data_tmp);
                end
                
            end
            
        end
        
        data=data_out;
        
        
end


end

function char_list=vect_to_char(double_vec)
char_list=[];
for i=1:length(double_vec)
    if i<length(double_vec)
        char_list=[char_list,'',num2str(double_vec(i)),'',','];
    else
        char_list=[char_list,'',num2str(double_vec(i)),''];
    end
end
char_list=['(',char_list,')'];
end







% ---------------------------------------- OLD VERSION

% switch lower(mode)
%     case 'future'
%          %<* intra day data
%         opt   = options( { 'source', '',  'trading-destinations', {}, ...
%             'initial-colnames', {}, 'final-colnames', {}, 'where_f-td', ''}, varargin);
%         sec_id = opt.get('security_id');
%         fname = get_repository( 'future-key', sec_id);
%         st_log('get_tick:data looking for data for <%s>...\n', fname);
%         lst   = opt.get();
%         data = from_buffer( 'get_future', 'futurefull', lst{:});
%         %>
%     case 'masterkey'
%         %<* Return the generic+specific dir
%         opt = options({'security_id', -1}, varargin(2:end));
%         fname = get_repository( 'future-key', opt.get('security_id'));
%         data = fullfile( varargin{1}, fname); % TODO
%         %>*
%     case 'futurefull'
%         %<* Read into the tickdb
%         opt = options({'security_id', -1, 'day', '01/04/2008',...
%             'date_format:char', 'dd/mm/yyyy' ...
%             }, varargin);
%         %< Lecture des options
%         format_date = opt.get('date_format:char');
%         sec_id = opt.get('security_id');
%         fname = get_repository( 'future-key', sec_id);
%         day_ = opt.get('day');
%         if ischar( day_)
%             day_ = datenum( day_, format_date);
%         end
%         %>
%         this_day = datestr( day_, 'yyyymmdd');
%         vals = exec_sql('GANDALF:tick_db', sprintf([ 'select '...
%             '   convert(char(10),date,103)  , '...
%             '    time , '...
%             '   price, '...
%             '   size, '...
%             '   cross, '...
%             '   strategy, '...
%             '   bid, '...
%             '   ask, '...
%             '   bid_size, '...
%             '   ask_size '...
%             '  from tick_db..future_deal where date = ''%s'' and future_maturity_id=-%d '... % TODO remove this - when ready
%             ' order by date,time'], this_day, sec_id));
%         dt_and_time = vals(:, 1:2);
%         vals = cellfun(@(x)double(x),vals(:,3:end));
%         if ~isempty(vals)
%             data = st_data('init', 'title', fname, ...
%                 'date', datenum( dt_and_time(:,1), 'dd/mm/yyyy') + mod( datenum(dt_and_time(:,2), 'HH:MM:SS'),1), ...
%                 'value', [vals,(vals(:,1)<(vals(:,5)+vals(:,6))/2),false(size(vals,1),6),4*ones(size(vals,1),1)], ...
%                 'colnames', { 'price', 'volume', 'cross', 'strategy', 'bid', 'ask', 'bid_size', 'ask_size', ...
%                 'sell', 'auction','opening_auction','intraday_auction','closing_auction','trading_at_last','trading_after_hours', ...
%                 'trading_destination_id'}, ... % ces dernieres colonnes sont invent?es pour permettre un traitement identique ? full_tick
%                 'info', [] );
%         else
%             data = st_data('empty');
%             data.title = fname;
%         end
%         data.info.td_info = get_repository('future-tdi', sec_id); % TODO
%         data.info.security_key = fname;% TODO
% end
%
%



% select * from repository..future_maturity where future_id=5
% select * from repository..future_maturity_source  where future_id=5
% select top 20 * from tick_db_03d_2010..future_deal

