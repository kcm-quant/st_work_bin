function varargout = get_gi(mode, varargin)
% GET_GI - get graph indicator
%
% get_gi('masterkey',['indicateur_mi/b',char(167),'"',char(167),'datenum(0,0,0,0,15,0)',char(167),'a',char(167),'1'], 'security_id', 110, 'trading-destinations', {} )
% [a,b,c] = get_gi( 'parse-args', 'indicateur_mi/b?"?datenum(0,0,0,0,15,0)?a?1', 'security_id', 110, 'trading-destinations', {} )
% data = get_gi( 'indicateur_mi', 'security_id', 110, 'day', '05/05/2008', 'to', '07/05/2008', 'trading-destinations', {} )
% data = from_buffer( 'get_gi', 'indicateur_mi', 'security_id', 110, 'from', '05/05/2008', 'to', '07/05/2008', 'trading-destinations', {} )
% data = from_buffer( 'get_gi', 'indicateur_mi', 'security_id', 110, 'from', '05/05/2008', 'to', '07/05/2008', 'trading-destinations', { 'main'} )
% data = from_buffer( 'get_gi', 'indicateur_mi/window:time?"?datenum(0,0,0,0,15,0)?step:time?"?datenum(0,0,0,0,15,0)', 'security_id', 110, 'from', '05/05/2008', 'to', '07/05/2008', 'trading-destinations', {} )
% data = from_buffer( 'get_gi', 'indicateur_mi/window:time?"?datenum(0,0,0,0,15,0)?step:time?"?datenum(0,0,0,0,5,0)', 'security_id', 110, 'from', '05/05/2008', 'to', '07/05/2008', 'trading-destinations', {} )
%
% author   : 'rburgot@cheuvreux.com'
% reviewer : 'clehalle@cheuvreux.com'
% version  : '1.1'
% date     : '20/08/2008'
%
% See also gi_create get_tick read_dataset get_repository from_buffer
persistent full_opt_storage
persistent curr_graph

switch mode
    case 'masterkey'
        %<* Masterkey generation
        % To know where buffer has to be stored
        opt = options( varargin(2:end) );
        [gi_name, opt4graph, params_mkey] = parse_args( varargin{1});
        sec_id = opt.get('security_id');
        td_id = opt.get('trading-destinations');
        if (iscell(td_id) && strcmpi(td_id{1},'MAIN')) || (ischar(td_id) && strcmpi(td_id,'MAIN'))
            td_id = get_repository('maintd',sec_id);
        end
        sec_mkey = get_sectd_key(sec_id,td_id,opt4graph);
        varargout = { fullfile( gi_name, params_mkey, sec_mkey) };
        %>*
    case 'parse-args'
        [a,b,c] = parse_args(varargin{1});
        varargout = {a,b,c};
    otherwise
        opt4me = options( {'format-date', 'dd/mm/yyyy', 'trading-destinations', {},...
            'from_buffer:cmd', '', 'empty_if_error', false}, varargin);
        from_date = opt4me.get('day');
        to_date   = opt4me.get('to');
        sec_id    = opt4me.get('security_id');
        td_id     = opt4me.get('trading-destinations');
        if (iscell(td_id) && strcmpi(td_id{1},'MAIN')) || (ischar(td_id) && strcmpi(td_id,'MAIN'))
            td_id = get_repository('maintd',sec_id);
        end
        [gi_name, opt4graph] = parse_args( mode);
        sec_td_key = get_sectd_key(sec_id, td_id, opt4graph);
        %<* I have the day in my storage
        if isempty( full_opt_storage)
            full_opt_storage = options();
        end
        first_key = [mode ':' sec_td_key];
        if full_opt_storage.contains_key(first_key)
            opt_storage = full_opt_storage.get(first_key);
            if opt_storage.contains_key(from_date)
                varargout = { opt_storage.remove(from_date) };
                if opt_storage.isempty()
                    full_opt_storage.remove(first_key);
                end
                return
            elseif ~opt_storage.isempty()
                error('get_gi:exec', 'Some bufferized data have still been unused, You might try a clear get_gi, perhaps this state is due to the way you debugged');
            end
        else
            opt_storage = options();
            full_opt_storage.set(first_key, opt_storage);
        end
        from_buffer_dt_all = opt4me.get('dt_all');
        fb_cmd = opt4me.get('from_buffer:cmd');
        eie = opt4me.get('empty_if_error');
        %>*
        switch gi_name
            
            case 'basic_indicator'
                
                is_source_char_future=false;
                if opt4graph.contains_key('source:char') && strcmp(opt4graph.get('source:char'), 'future')
                    is_source_char_future=true;
                end
                
                % cas particulier pour le future
                if is_source_char_future
                    specific_default_options = {... % ? synchrniser avec set_params_basic_indicator
                        'window:time', datenum(0,0,0,0,15,0),...
                        'step:time', datenum(0,0,0,0,15,0), ...
                        't_acc:time', datenum(0,0,0,0,11,0), ...
                        'bins4stop:b', false, ...
                        'given_trdt:cell', {}, ...
                        'source:char', 'future', ...
                        'volume_by_td:b', false, ...
                        };
                    opt4graph = options(specific_default_options, opt4graph.get());
                    n1 = st_read_dataset();
                    n2 = st_basic_indicator();
                    opt4graph.set('security_id', sec_id);
                    opt4graph.set('trading_destination_id', td_id);
                    lst1 = {'RIC:char', opt4graph.remove('security_id'), ...
                        'source:char', opt4graph.get('source:char'), ...
                        'trading-destinations:cell', opt4graph.remove('trading_destination_id')};
                    lst = opt4graph.get();
                    n2.init(lst{:});
                    
                    for i = 1 : length(from_buffer_dt_all)
                        this_date = datestr( from_buffer_dt_all(i), 'dd/mm/yyyy');
                        n1.init(lst1{:}, 'from:dd/mm/yyyy', this_date, ...
                            'to:dd/mm/yyyy', this_date);
                        day_ = n1.exec();
                        day_ = n2.exec(day_(1));
                        day_ = day_{:};
                        opt_storage.set(this_date, day_ );
                    end
                    
                else  % si ce n'est pas un future
                    tdinfo = get_repository('tdinfo', sec_id);
                    specific_default_options = {... % ? synchrniser avec set_params_basic_indicator
                        'window:time', datenum(0,0,0,0,15,0),...
                        'step:time', datenum(0,0,0,0,15,0), ...
                        't_acc:time', datenum(0,0,0,0,11,0), ...
                        'bins4stop:b', true, ...
                        'given_trdt:cell', {}, ...
                        'source:char', 'tick4bi', ...
                        'volume_by_td:b', false, ...
                        };
                    opt4graph = options(specific_default_options, opt4graph.get());
                    switch lower(tdinfo(1).global_zone_name)
                        case 'europe'
                            n1 = st_read_dataset();
                            n2 = st_basic_indicator();
                            opt4graph.set('security_id', sec_id);
                            opt4graph.set('trading_destination_id', td_id);
                            lst1 = {'RIC:char', opt4graph.remove('security_id'), ...
                                'source:char', opt4graph.get('source:char'), ...
                                'trading-destinations:cell', opt4graph.remove('trading_destination_id')};
                            lst = opt4graph.get();
                            n2.init(lst{:});
                            
                            for i = 1 : length(from_buffer_dt_all)
                                this_date = datestr( from_buffer_dt_all(i), 'dd/mm/yyyy');
                                n1.init(lst1{:}, 'from:dd/mm/yyyy', this_date, ...
                                    'to:dd/mm/yyyy', this_date,'today_allowed',opt4me.try_get('today_allowed',false));
                                day_ = n1.exec();
                                day_ = n2.exec(day_(1));
                                day_ = day_{:};
                                opt_storage.set(this_date, day_ );
                            end
                        case {'america', 'asia'}
                            for i = 1 : length(from_buffer_dt_all)
                                if from_buffer_dt_all(i) == today && opt4me.try_get('today_allowed',false)
                                    this_date = datestr( from_buffer_dt_all(i), 'dd/mm/yyyy');
                                    opt_storage.set(this_date, agg_candles(get_full_candles('fc', 'security_id', ...
                                    sec_id, 'day',this_date,'today_allowed',opt4me.try_get('today_allowed',false)), td_id, ...
                                    opt4graph.get('window:time'), opt4graph.get('step:time'), opt4graph.get('t_acc:time'), opt4graph.get('bins4stop:b')) );                                
                                else
                                    this_date = datestr( from_buffer_dt_all(i), 'dd/mm/yyyy');
                                    opt_storage.set(this_date, agg_candles(from_buffer('get_full_candles', 'fc', 'security_id', ...
                                        sec_id, 'from', this_date, 'to', this_date), td_id, ...
                                        opt4graph.get('window:time'), opt4graph.get('step:time'), opt4graph.get('t_acc:time'), opt4graph.get('bins4stop:b')) );
                                end
                            end
                        otherwise
                            error('get_gi:basic_indicator:exec', 'Unknown zone : <%s>', lower(tdinfo(1).global_zone_name))
                    end
                end
                
            otherwise
                %<* I will have to build all data
                % From day to 'to'
                has2load = true;
                if strcmp(fb_cmd, 'persist_graph')
                    % je le fais de cette mani�re car sinon il faudrait
                    % avoir la capacit� � idetifier tous les param�tres
                    % susceptibles de modifier les donn�es en entr�e.
                    % Puisque 'lon a pas cette c�pacit�, on se repose sur
                    % l'utilisateur, qui choisit ou non d'utiliser ce mode
                    curr_graph_key = hash(convs('safe_str', {gi_name, sec_id, td_id, from_date, to_date}), 'MD5');
                    if ~isempty(curr_graph) && strcmp(curr_graph_key, curr_graph.key)
                        has2load = false;
                        gr = curr_graph.gr;
                    end
                end
                if has2load
                    if isempty(curr_graph) || ~strcmp(curr_graph.gi_name, gi_name)
                        graph_path = fullfile(getenv('st_work'), 'bin', 'graph_indicators', 'projects', [gi_name '.mat']);
                        try
                            load(graph_path);
                        catch er
                            gr_path = which([gi_name '.mat']);
                            graph_path_dev = fullfile(getenv('st_work'), 'usr', 'methodo', 'graph_indicators', 'projects', [gi_name '.mat']);
                            if strcmp(gr_path, graph_path_dev)
                                load(gr_path);
                            elseif ~isempty(gr_path) && ~strcmp(gr_path, graph_path)
                                error('get_gi:loading_graph', 'Please move your graph from <%s> to <%s>', gr_path, graph_path);
                            else
                                rethrow(er);
                            end
                        end
                    else
                        gr = curr_graph.gr;
                    end
                    gr.set_global('verbosity', 0);
                    gr.init();
                    
                end
                %< Set generic parameters
                opt4graph.set('from', from_date);
                opt4graph.set('to'  , to_date);
                opt4graph.set('security_id', sec_id);
                opt4graph.set('trading_destination_id', td_id);
                %>
                gr.run_enrichment('set params', opt4graph);
                % < getsion du mode show
                if strcmp(fb_cmd, 'show_mode')
                    win_graph('show', gr);
                    varargout = {[]};
                    return;
                end
                % >
                first_iter = true;
                
                while first_iter || ~gr.global_end_of_prod()
                    st_log('get_gi : %s working on <%s>...\n', gi_name, sec_td_key);
                    if eie
                        try
                            NEXT
                        catch ME
                            if ~strcmp(ME.identifier, 'MATLAB:nomem')
                                % Si ce n'est pas un erreur de m�moire,
                                % alors on renvoi du vide
                                curr_datestamp = from_buffer_dt_all(1);
                                this_date = datestr( curr_datestamp, 'dd/mm/yyyy');
                                st_log('get_gi : Day <%s> has been computed and is empty because of an error\n', this_date);
                                day_ = [];
                            elseif first_iter
                                % si c'est une erreur m�moire qui se
                                % produit d�s la premi�re it�ration, alors
                                % on devrait essayer de ne travailler que
                                % sur to_ plut�t que de day_ � to_, mais ce
                                % n'est pas encore impl�ment�
                                rethrow(ME);
                            else
                                % Si ce n'est pas la premi�re it�ration,
                                % alors on renvoie d�j� les dates qui ont
                                % �t� calcul�es et puis on verra bien si
                                % l'erreur m�moire se reproduit
                                % output asked date
                                varargout = { opt_storage.remove(from_date) };
                                if opt_storage.isempty()
                                    full_opt_storage.remove(first_key);
                                end
                                %>*
                                return;
                            end
                        end
                    else
                        NEXT
                    end
                    % store data for next demands of from_buffer
                    idx_this_date = from_buffer_dt_all == curr_datestamp;
                    if any(idx_this_date)
                        opt_storage.set(this_date, day_ );
                        from_buffer_dt_all(idx_this_date) = [];
                    else
                        warning('get_gi:exec', 'The graph runnned on a not required day');
                    end
                    first_iter = false;
                end
                if ~isempty(from_buffer_dt_all)
                    error('get_gi:exec', 'Not all of the required dates have been computed');
                end
                if strcmp(fb_cmd, 'persist_graph')
                    curr_graph.gr = gr;
                    curr_graph.key = curr_graph_key;
                    curr_graph.gi_name = gi_name;
                end
        end
        % output asked date
        varargout = { opt_storage.remove(from_date) };
        if opt_storage.isempty()
            full_opt_storage.remove(first_key);
        end
        %>*
end
    function NEXT
        gr.next();
        % root output HAS to give me a numeric info.data_datestamp field
        root_out1 = gr.get_root_outputs(1);
        if iscell(root_out1) % can be a cell-array if more than one security was asked
            curr_datestamp = root_out1{1}.info.data_datestamp;
        else
            curr_datestamp = root_out1.info.data_datestamp;
        end
        this_date = datestr( curr_datestamp, 'dd/mm/yyyy');
        st_log('get_gi : Day <%s> has been computed\n', this_date);
        day_ = gr.get_target_outputs(); % multiple bufferize not allowed, have to implement a 'flag' in nodes for that
        day_ = day_{:}; % multiple outputs not allowed currently...
    end
end
%%** Embedded functions

%%* Parse char of opts
function [gi_name, opt, mkey] = parse_args(str)
idx = strfind( str, '/');
if isempty( idx)
    gi_name = str;
    opt = options();
else
    gi_name = str(1:idx(1)-1);
    opt = str2opt( str(idx(1)+1:end));
end
mkey = feval([ 'set_params_' gi_name], 'masterkey', opt);
end
function sec_td_key = get_sectd_key(sec_id, td_id, opt4graph)
if ~ischar(sec_id) && length(sec_id) > 1
    if opt4graph.contains_key('source:char') && strcmp(opt4graph.get('source:char'), 'index')
        sec_td_key = sec_id;
    else
        % Alors on fait une clef d?guelasse
        sec_td_key = hash(convs('safe_str', {sort(sec_id), sort(td_id)}), 'MD5');
    end
else
    if opt4graph.contains_key('source:char') && strcmp(opt4graph.get('source:char'), 'index')
        sec_td_key = get_repository( 'index-key', sec_id);
    elseif opt4graph.contains_key('source:char') && strcmp(opt4graph.get('source:char'), 'future')
        %         sec_td_key = get_repository( 'future-key', sec_id);
        sec_td_key = get_repository( 'future-maturity-key', sec_id, td_id);
    else
        sec_td_key = get_repository( 'security-td-key', sec_id, td_id);
    end
end
end
