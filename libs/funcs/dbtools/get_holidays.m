function hdays = get_holidays( varargin )
% get_holidays
% provides trading holidays for a number of trading destinations
% (if more than 90% of tds are on holidays)
%

opt = options({'mode','trading_destination_id',...
    'code', 1, 'from', '01/01/2014', 'to', '30/06/2014'}, varargin);

% dates
SQL_frm = datestr(datenum(opt.get('from'), 'dd/mm/yyyy'), 'yyyy-mm-dd'); 
SQL_too = datestr(datenum(opt.get('to'), 'dd/mm/yyyy'), 'yyyy-mm-dd'); 

% récupération des trading destinations
if strcmpi(opt.get('mode'), 'trading_destination_id')
    tdid = opt.get('code');
elseif strcmpi(opt.get('mode'), 'index')
    idxid = exec_sql('KGR', sprintf('select INDEXID from KGR..[INDEX] where INDEXNAME = ''%s'' ',  opt.get('code')));
    tdid = cell2mat(exec_sql('KGR',sprintf(['select distinct trading_destination_id from KGR..security_market '...
        ' where security_id in (select distinct security_id from KGR..indice_component where INDEXID = ''%s'') '...
        ' and ranking = 1 '],idxid{1})));
elseif strcmpi(opt.get('mode'), 'security')
    strsec = sprintf('%d,', opt.get('code'));
    strsec(end) = [];
    tdid = cell2mat(exec_sql('KGR',sprintf(['select distinct trading_destination_id from KGR..security_market '...
        ' where security_id in (%s) '...
        ' and ranking = 1 '],strsec)));
end

strtdid = sprintf('%d,', tdid);
strtdid(end) = [];

% récupération des jours fériés pour l'ensemble de ces trading_destinations
hdays = exec_sql('KGR', sprintf(['select EVENTDATE,count(distinct SCOPEID)  from KGR..CALEVENT where '...
    ' EVENTDATE between ''%s'' and ''%s'' '...
    ' and EVENTTYPE = 4 '...
    ' and SCOPETYPE = 2  '...
    ' and SCOPEID in (%s) '...
    ' group by EVENTDATE '], SQL_frm, SQL_too, strtdid));

nbtd = size(tdid,1);
T = 0.9;

if isempty(hdays)
    hdays = [];
else
    hdays2keep = cell2mat(hdays(:,2))>=floor(T*nbtd);
    hdays = cellfun(@(c) datenum(c, 'yyyy-mm-dd'), hdays(hdays2keep,1));
end
end