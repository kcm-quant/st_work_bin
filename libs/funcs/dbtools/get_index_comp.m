function sec_ids = get_index_comp(index, varargin)
% GET_INDEX_COMP - define some sets of securities, according to indices,
% quotation_groups, securities traded on a destination or other criteria,
% and makes some set diff unions, etc
% 
% Examples : 
%
% get_index_comp('SLI')
% get_index_comp('SBF120\CAC40') % SBF120 excluding CAC40
% get_index_comp('FTSE 100 UNION FTSE 250') % eponym
% get_index_comp('CAC40excluding276') % removing TOTAL from the CAC40
% get_index_comp('DJ INDUSTRIAL intersect S&P500 STOCK INDEX') % set intersection
% get_index_comp([char(36) '13:SET0 UNION ' char(36) '13:SET1']) % r?union de deux groupes de quotation
% get_index_comp([char(163) '26:100']) % les 100 titres trait?s en australie ayant le plus grand nombre de deal sur le dernier mois
% get_index_comp([char(163) char(163) '26:100']) % les 100 titres trait?s en australie ayant le plus petit nombre de deal sur le dernier mois
% get_index_comp([char(163) '26:all']) % tous les titres trait?s en australie y compris ceux que l'on enregistre pas mais en excluant ceux pour lesquels Cheuvreux ne s'autorise pas ? traiter en Australie
% get_index_comp([char(181) '26:100']) % les 100 titres ayant le plus grand nombre de deals sur l'australie et pour lesquels l'australie est consid?r?e comme march? primaire
% get_index_comp('fobu') % les titres europ?ens pour lesquels on enregistre le carnet d'ordre
% get_index_comp('safer_fobu') % les titres europ?ens pour lesquels on enregistre le carnet d'ordre et qui ont de plus des informations de r?f?rentiel indiquant sur quel march? ils sont trait?s et qui sont des actions
% get_index_comp([char(169) '30:100']) % les titres qui sont dans trading_daily (et trading_daily_ameri) et qui ont eu en moyenne plus de 100 trades sur les 30 derniers jours (calendaires)
% get_index_comp([char(169) '30:100:_ameri']) % les titres qui sont dans trading_daily_ameri et qui ont eu en moyenne plus de 100 trades sur les 30 derniers jours (calendaires)
% get_index_comp([char(169) '30:100: ']) % les titres qui sont dans trading_daily et qui ont eu en moyenne plus de 100 trades sur les 30 derniers jours (calendaires)
% See also : get_repository
%
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '11/05/2011'

%
% ? faire un jour ls compositions historiques
% un version d?grad? pourrait s'appuyer sur les  modifications des
% p?rim?tres:
% 
% select 
% d.domain_id,
% min(er.stamp_date) "first_run_date", 
% max(er.stamp_date) "last_run_date",
% min(err.stamp_date), 
% max(err.stamp_date)
% from 
% quant..domain d, 
% quant..estimator_runs er,
% quant..error err,
% quant..job j
% where 
% ( d.domain_name like 'lmi mtd/%')  
% and er.domain_id = d.domain_id
% and j.domain_id  =* d.domain_id
% and j.job_id *= err.job_or_run_id
% and err.is_run = 1
% and d.domain_type_variable = 'FTSE 100'
% group by d.domain_id, er.domain_id,j.domain_id
% order by first_run_date

global st_version
repository = st_version.bases.repository;
market_data = st_version.bases.market_data;

idx = regexp(index,' intersect ','ignorecase','split');
if length(idx)==2
    sec_ids = intersect(get_index_comp(idx{1},varargin{:}),get_index_comp(idx{2},varargin{:}));
    return
end

idx = regexp(index, '\S\\\S', 'ONCE');
if ~isempty(idx)
    sec_ids = setdiff(get_index_comp(index(1:idx),varargin{:}), get_index_comp(index(idx+2:end),varargin{:}));
    return
end

idx = regexp(index, '\S UNION \S', 'ONCE');
if ~isempty(idx)
    sec_ids = union(get_index_comp(index(1:idx),varargin{:}), get_index_comp(index(idx+8:end),varargin{:}));
    return
end

idx = regexp(index, '\Sexcluding\S', 'ONCE');
if ~isempty(idx)
    sec_ids = setdiff(get_index_comp(index(1:idx),varargin{:}), str2double(index(idx+10:end)));
    return
end

index = regexp(index, '(?<=\w) +ex +(?=\w)', 'split');
if length(index)>1
    sec_ids = setdiff(get_index_comp(index{1}),get_index_comp(index{2}));
    return
else
    index = index{1};
end

if index(1) == char(36)
    % kepche TODO :  cette fonctionnalit? n'a pas ?t? test?e.
    td_id = tokenize(index(2:end), ':');
    qg = td_id{2};
    td_id = str2double(td_id{1});
    sec_ids = cell2mat(exec_sql('KGR',['select convert(int,SYMBOL6)',...
        ' from ',repository,'..SECURITY s,',repository,'..EXCHANGEMAPPING e',...
        ' where e.EXCHANGE = ',num2str(td_id),...
        ' and s.EXCHGID = e.EXCHGID',...
        ' and s.MKTGROUP = ''',qg,'''',...
        ' and s.STATUS = ''A''']));
    return
end

if index(1) == char(163)
    % kepche TODO : cette fonctionnalit? n'a pas ?t? test?e.
    td_id = tokenize(index(2:end), ':');
    if strcmp(td_id{2}, 'all')
        td_id = str2double(td_id{1});
        sec_ids = cell2mat(exec_sql('KGR', ...
            ['select security_id',...
            ' from ',repository,'..security_market',...
            ' where trading_destination_id = ',num2str(td_id),' and ranking = 1']));
        return
    else
        if td_id{1}(1) == char(163)
            td_id{1}(1) = [];
            order_in_req = 'asc';
        else
            order_in_req = 'desc';
        end
        nb_stock = str2double(td_id{2});
        td_id = str2double(td_id{1});
        asof = get_as_of_date();
        sec_ids = cell2mat(exec_sql('KGR', ...
            ['select top ',num2str(nb_stock),' tdbtd.security_id, sum(tdbtd.nb_deal) as sumnbdeal',...
            ' from ',market_data,'..trading_daily',get_repository('tdid2global_zone_suffix', td_id),' tdbtd,',...
            ' ',repository,'..security s ' ...
            ' where tdbtd.trading_destination_id = ',num2str(td_id),...
            ' and tdbtd.date >= ''',datestr(asof-30, 'yyyy-mm-dd'),'''',...
            ' and tdbtd.date <= ''',datestr(asof, 'yyyy-mm-dd'),'''',...
            ' and tdbtd.security_id = s.security_id',...
            ' and s.security_type = 1',...
            ' group by tdbtd.security_id order by sumnbdeal ',order_in_req]));
        sec_ids = sec_ids(:, 1);
        return
    end
end

if index(1) == char(181)
    % kepche TODO : cette fonctionnalit? n'a pas ?t? test?e.
    td_id = tokenize(index(2:end), ':');
    if td_id{1}(1) == char(163)
        td_id{1}(1) = [];
        order_in_req = 'asc';
    else
        order_in_req = 'desc';
    end
    nb_stock = td_id{2};
    td_id = td_id{1};
    asof = get_as_of_date();
    sec_ids = cell2mat(exec_sql('KGR', ...
        ['select top ',nb_stock,' tdbtd.security_id, sum(nb_deal) as sumnbdeal',...
        ' from tick_db..trading_daily',get_repository('tdid2global_zone_suffix', td_id),' tdbtd,',...
        ' repository..security s, repository..security_market sm ' ...
        ' where tdbtd.trading_destination_id = ',num2str(td_id),...
        ' and tdbtd.date >= ''',datestr(asof-30, 'yyyymmdd'),'''',...
        ' and tdbtd.date <= ''',datestr(asof, 'yyyymmdd'),'''',...
        ' and tdbtd.security_id = sm.security_id ' ...
        ' and sm.trading_destination_id = ',td_id,...
        ' and sm.ranking = 1',...
        ' and tdbtd.security_id = s.security_id',...
        ' and s.security_type = 1',...
        ' and s.security_id = sm.security_id',...
        ' group by sm.security_id, tdbtd.security_id order by sumnbdeal ' order_in_req]));
    sec_ids = sec_ids(:, 1);
    return
end


if index(1) == char(169)
    nbdays = tokenize(index(2:end), ':');
    minavgdeal = nbdays{2};
    if length(nbdays) == 2
        nbdays = str2double(nbdays{1});
        asof = get_as_of_date();
        sec_ids1 = cell2mat(exec_sql('KGR', ...
            ['select tdbtd.security_id, avg(nb_deal) as sumnbdeal',...
            ' from Market_data..trading_daily tdbtd ',...
            ' where tdbtd.date >= ''',datestr(asof-nbdays, 'yyyymmdd'),'''',...
            ' and tdbtd.date <= ''',datestr(asof, 'yyyymmdd'),'''',...
            ' group by tdbtd.security_id having avg(nb_deal) >= ' minavgdeal]));
        sec_ids2 = cell2mat(exec_sql('KGR', ...
            ['select tdbtd.security_id, avg(nb_deal) as sumnbdeal',...
            ' from Market_data..trading_daily_ameri tdbtd ',...
            ' where tdbtd.date >= ''',datestr(asof-nbdays, 'yyyymmdd'),'''',...
            ' and tdbtd.date <= ''',datestr(asof, 'yyyymmdd'),'''',...
            ' group by tdbtd.security_id having avg(nb_deal) >= ' minavgdeal]));
        sec_ids = union(sec_ids1(:, 1), sec_ids2(:, 1));
        return
    else
        suffix = nbdays{3};
        nbdays = str2double(nbdays{1});
        asof = get_as_of_date();
        sec_ids = cell2mat(exec_sql('KGR', ...
            ['select tdbtd.security_id',...
            ' from Market_data..trading_daily' suffix ' tdbtd ',...
            ' where tdbtd.date >= ''',datestr(asof-nbdays, 'yyyymmdd'),'''',...
            ' and tdbtd.date <= ''',datestr(asof, 'yyyymmdd'),'''',...
            ' group by tdbtd.security_id having avg(nb_deal) >= ' minavgdeal]));
        return
    end
end


if strcmp(index, 'SLI')
     sec_ids = get_repository('index-comp', 'SWISS LEADER PR INDEX', varargin{:});
    return
end

if strcmpi(index, 'fobu')
    error('getindexcomp:deprecatedmode','This mode is deprecated');
end

if strcmpi(index, 'safer_fobu')
    error('getindexcomp:deprecatedmode','This mode is deprecated');
end

sec_ids = get_repository('index-comp', index, varargin{:});

end