function data = get_index_data(mode, varargin)
% GET_INDEX_DATA - version de get_tick pour les indices
%
% Examples:
% get_index_data('indexfull', 'security_id', 'CAC40', 'day','11/11/2011')
% today_data=get_index_data('indexfull','security_id','CAC40','day',datestr(today,'dd/mm/yyyy'),'today_allowed',true)
% read_dataset('index', 'security_id', 'CAC40', 'from','10/11/2011','to','11/11/2011')
%
%
% See also: read_dataset
%
%   author   : 'njoseph@cheuvreux.com'
%   reviewer : ''
%   date     :  '05/12/2011'
%
%   last_checkin_info : $Header: get_index_data.m: Revision: 3: Author: malas: Date: 06/07/2012 11:40:53 AM$


switch lower(mode)
    
     case 'masterkey'
        %<* Return the generic+specific dir
        opt = options({'security_id', ''}, varargin(2:end));
        sec_id = opt.get('security_id');
        [~,sid,index_name] = get_repository( 'index-key', sec_id);
        opt.set('security_id', sid);
        index_name = strrep(index_name,'/','');
        data = fullfile( varargin{1}, index_name);
        %>*
       
    case 'indexfull'
        
        %<* Read into the tickdb
        opt = options({'security_id', '', 'day', '01/04/2008',...
            'today_allowed',false,...
            'date_format:char', 'dd/mm/yyyy' ...
            }, varargin);
        %< Lecture des options
        format_date = opt.get('date_format:char');
        sec_id = opt.get('security_id');
        [~,sec_id,index_name] = get_repository( 'index-key', sec_id);

        day_ = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        %>
        this_day = datestr( day_, 'yyyymmdd');
        %< check dates
        if day_ >= today 
            dbs = dbstack;
            if day_ == today && opt.get('today_allowed') && ~ismember('from_buffer.m', {dbs.file})
                % ok you are allowed to go further
            else
                error('get_index_data:check_args', 'TODAY : Input parameters do not allow you to extract today data');
            end
        end
        %>
        index_tdinfo=get_repository('index_tdinfo',sec_id);  
        if isempty(index_tdinfo.global_zone_suffix)
            error('get_index_data:no_data','No data for security_id = %d',sec_id)
        end
        
        %< server / table name
        if floor(day_) == today
            table_name = [ 'indice_day' index_tdinfo.global_zone_suffix ];
            server_name='tick_db';
        elseif day_<datenum(2010,4,29)
            table_name = [ 'indice' index_tdinfo.global_zone_suffix ];
            server_name='tick_db';
        else
            table_name=['indice' index_tdinfo.global_zone_suffix ];
            server_name=get_basename4day( day_);
        end
        %>
        sql_req=sprintf([ 'select '...
            '   convert(char(10),date,103),time,price,tickprice_id '...
            '  from %s..%s where date = ''%s'' and indice_id=%d '],...
            server_name,table_name,this_day, sec_id);
        
        t0=clock;
        vals = exec_sql('BSIRIUS', sql_req);
        st_log('get_index_data : extract duration (%5.2f sec)\n', etime(clock, t0));
        
        % < timezone handle
        local_place_timezone_id=index_tdinfo(1).timezone;
        if isempty(local_place_timezone_id)
            local_place_timezone_id=index_tdinfo(1).default_timezone;
        end
        
        if ~isempty(vals)
            
            %< Date handling
            date_time=datenum( vals(:,1), 'dd/mm/yyyy') + mod( datenum(vals(:,2), 'HH:MM:SS'),1);
            vals(:,1:2) = [];               % effacer date/time des valeurs
            
            %< If not recorded in localtime, then convert
            if ~index_tdinfo(1).localtime
                decalage  = timezone('get_offset',...
                    'final_timezone',local_place_timezone_id ,...
                    'initial_timezone',index_tdinfo(1).default_timezone,...
                    'dates', floor(date_time));
                date_time = date_time + decalage;
            end
            %>
            
            %< Will we be able to distinguish two updates thanks to
            % tickprice_id in Matlab? if not, lets make a matlab tickprice_id
            tickprice_id=cell2mat(vals(:,2));
            if eps(max(tickprice_id)) >= 0.5
                tickprice_id=(1:length(tickprice_id))';
            end
            %>
            
            data = st_data('init', 'title', sprintf( '%s - %s', index_name, datestr(day_, 'dd/mm/yyyy')), ...
                'date', date_time, ...
                'value', [cellfun(@(x)double(x),vals(:,1)),tickprice_id], ...
                'colnames', {'price','tickprice_id'}, ...
                'info', [] );
        else
            data = [];
            return;
        end
        
        % // info
        data.info=struct('sql_request', sql_req, ...
            'security_id', sec_id, ...
            'security_type', 'index', ...
            'security_key', index_name, ...
            'td_info', index_tdinfo , ...
            'localtime', true,...
            'local_place_timezone_id',local_place_timezone_id,...
            'place_timezone_id',local_place_timezone_id,...
            'data_datestamp',day_);
        
        %%% test
        %%% [datestr(data.date(1)) datestr(data.date(end))]
        %%% timezone('get_offset','final_place_id',1,'init_place_id',local_place_timezone_id,'dates',day_)/datenum(0,0,0,1,0,0)
        
end
end