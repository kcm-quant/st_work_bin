function [ idata ] = get_index_prices( indexname, dt_from, dt_to )
% get_index_prices gets historical index prices
% from Market..INDICE_DAILY table
%
% ATTENTION : BLOOMBERG fournit des donn�es pour les week ends et jours
% f�ri�s, elles sont supprim�es ici par une intersection avec trading_daily
%

global st_version
market_data = st_version.bases.market_data;

str_dt_from = datestr(datenum(dt_from, 'yyyy/mm/dd'), 'yyyymmdd');
str_dt_to = datestr(datenum(dt_to, 'yyyy/mm/dd'), 'yyyymmdd');

indexid = exec_sql('KGR',...
    sprintf('select INDEXID from KGR..[INDEX] where INDEXNAME like ''%s'' ',indexname) );

res = exec_sql('MARKET_DATA',...
    sprintf(['select DATE, indice_id, OPEN_PRC, HIGH_PRC, LOW_PRC, CLOSE_PRC '...
    'from ',market_data,'..INDICE_DAILY '...
    'where DATE between ''%s'' and ''%s'' and INDEXID = ''%s'' order by DATE '],...
    str_dt_from, str_dt_to, indexid{1}));

idata = st_data('init','title', indexname,...
    'value',cell2mat(res(:, 2:end)), 'date', cellfun(@(c) datenum(c, 'yyyy-mm-dd'), res(:,1)),...
    'colnames', {'indice_id', 'open', 'high', 'low', 'close'});

td_id = cell2mat(exec_sql('KGR',...
    sprintf(['select td.trading_destination_id from KGR..[INDEX] ind, KGR..trading_destination td '...
    'where ind.INDEXID = ''%s'' and td.short_name = ind.PRIMARYEXCHID '], indexid{1})));
fprintf('*** Warning: removing weekends and market holidays from bloomberg data:\n intersection with dates in trading daily for trading destination %d ***\n', td_id);
if isempty(td_id)
    td_id = 1;
end
fprintf('******* Warning: index primary exchange not available: trading destination forced to %d *******\n', td_id);

ref_dates = exec_sql('MARKET_DATA',...
    sprintf(['select distinct date from ',market_data,'..trading_daily',...
    ' where date between ''%s'' and ''%s'' and trading_destination_id = %d order by date'], str_dt_from, str_dt_to, td_id));
ref_dates = cellfun(@(c) datenum(c, 'yyyy-mm-dd'), ref_dates);

idata = st_data('from-idx', idata, ismember(idata.date, ref_dates));

end

function DEMO

change_connections('production_copy')

idata = get_index_prices( 'DJ STOXX SMALL 200', '2007/01/01', '2013/01/01' )
idata = get_index_prices( 'DJ STOXX MID 200', '2007/01/01', '2013/01/01' )
idata = get_index_prices( 'DJ STOXX LARGE 200', '2007/01/01', '2013/01/01' )
datestr(min(idata.date))

% Comparaison avec acc�s direct � Market_data..HISTOINDEXTIMESERIES
d= DataDaily.index('INDEXNAME=DJ STOXX LARGE 200')
d= DataDaily.index('INDEXNAME=DJ STOXX MID 200')
datestr(min(d.date))

end



