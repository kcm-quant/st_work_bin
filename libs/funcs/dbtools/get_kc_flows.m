function [data, data_by_month, data_by_month_account] = get_kc_flows(from_n, to_n, varargin)
% data = get_kc_flows(datenum(2022,1,1),datenum(2022,1,13));
% data = get_kc_flows(datenum(2022,1,1),datenum(2022,1,13),'filter_corporate',0,'keep_client_types',[],'dynamic_index','SXXP');
% data = get_kc_flows(datenum(2022,1,1),datenum(2022,1,13),'filter_corporate',0,'keep_client_types',[],'dynamic_index',[]);
% data = get_kc_flows(datenum(2022,1,1),datenum(2022,1,13),'mode','booking_channels','filter_corporate',0,'keep_client_types',[],'dynamic_index',[]);
%

opt = options({'mode', 'executions',... %'booking_channels'
    'client_ids', 'v2',...% crystal customer
    'filter_technical', 1,...% filter salesapp 
    'filter_corporate', 1,...% filter salesapp 
    'filter_QFD_exclusions', 1,... % filter QR 
    'keep_client_types', {'Banks-Brokers','Funds','Hedge funds','Retail'},... % filter QR
    'quant_perimeter', 1,...
    'dynamic_index','SXXP'},varargin);

from = datestr(from_n,'yyyy-mm-dd');
to = datestr(to_n,'yyyy-mm-dd');
mode = opt.get('mode');

switch mode
    case 'executions'
        % corpo_ids = {'@CORPO_Traders','BMC','BMC2','CGA','CGD','CGD2','CIE','DMZ','EPE','JFT2','JFT3','JSS','JSS2','LBR','PDS','PSC','PSR','PSR-T','PSU','PSU2','DME','FTD'};
        corpo_ids = {'@CORPO_Traders','BMC','BMC2','CGA','CGD','CGD2','CIE','DMZ','EPE','JFT2','JFT3','JSS','JSS2','LBR','PDS','PSC','PSR','PSR-T','PSU','PSU2','DME','FTD','SME'};
        corpo_str = sprintf('''%s'',',corpo_ids{:});
        corpo_str = corpo_str(1:end-1);

        if strcmpi(opt.get('client_ids'),'v1')
            cid_str = 'a.CrystalCode';
        elseif strcmpi(opt.get('client_ids'),'v2')
            cid_str = 'case when isnumeric(c.CLNID) = 1 then convert(bigint,c.EXTCLNID) else 0 end';
        elseif strcmpi(opt.get('client_ids'),'v3_temp')     
            cid_str = 'b.CrystalCode';
            % v4: remove jin with CLIENTIDMAP
        end

        req = sprintf(['SELECT	a.TRADEDATE_date DATE, '...
            'a.security_id, '...
            'a.CCY CCY, '...
            '%s ACCOUNTID, '... %a.CrystalCode
            'case when a.SIDE= ''BUY'' then 1 else -1 end SIDE, '...
            'case when b.SALESID in (%s) then ''Corporate'' else ''Brokerage'' end DESK,'...
            'sum(MATCHQTY) MATCHQTY, '...
            'sum(a.MATCHQTY*a.PRICE_NUM)   AMOUNT, '...
            'sum((a.MATCHQTY*a.PRICE_NUM)/(INFOCENTRE.dbo.Daily_ExchangeRate.Rate+0)) AMOUNT_EUR '...
            'FROM INFOCENTRE..ALLTRADE_QUANT a '...
            '		INNER JOIN INFOCENTRE.dbo.Daily_ExchangeRate ON (a.CCY=INFOCENTRE.dbo.Daily_ExchangeRate.QuotedCCy and a.TRADEDATE_date=INFOCENTRE.dbo.Daily_ExchangeRate.Date) '...
            '		INNER JOIN OMS..ALLORDER b on a.ORDID = b.ORDID '...
            '		INNER JOIN OMS..CLIENTIDMAP c on c.CLNID = b.CLNID '...
            'WHERE	a.TRADEDATE_date >= ''%s 00:00:00'' and a.TRADEDATE_date<=''%s 23:59:59''  '...
            'and a.DEALERID <> ''TEST'' '...
            'and a.STATUS <> ''C'' '...
            'group by a.TRADEDATE_date, a.security_id, a.CCY, %s,'...
            'case when b.SALESID in (%s) then ''Corporate'' else ''Brokerage'' end,'...
            'case when a.SIDE= ''BUY'' then 1 else -1 end'],...
            cid_str, corpo_str, from, to, cid_str, corpo_str);
        data = exec_sql_table('INFOCENTRE',req);

        uds = unique(data.DATE);
        udn = cell2mat(cellfun(@(c) datenum(c,'yyyy-mm-dd'), uds, 'uni', 0));
        data.date = joint(uds, udn, data.DATE);

        % cleanup data
        data = data(data.AMOUNT_EUR>0,:);
        data = data(~isnan(data.security_id),:);
        data = data(~isnan(data.ACCOUNTID),:);
        
        % quant perimeter
        if opt.get('quant_perimeter')
            hsq = exec_sql_table('KGR','select distinct security_id from KGR..HISTO_SECURITY_QUANT');
            data = data(ismember(data.security_id, hsq.security_id),:);
        end
        %% dynamic SXXP
        if ~isempty(opt.get('dynamic_index'))
            compo_histo = get_repository('index-comp',opt.get('dynamic_index'), [min(data.date), max(data.date)],{'type','dynamic'});
            data = select_index_compo_histo_data(data, data.date, compo_histo);
        end

    case 'booking_channels'
        req = sprintf(['SELECT	DealDate DATE, '...
            'ISIN, CotationPlace, TRADINGCURRENCY CCY, '...
            'CrystalCustomer ACCOUNTID, '...
            'ExecutionChannel, '...
            'ServiceCode, '...
            'case when SIDE= ''BUY'' then 1 else -1 end SIDE, '...
            'sum(VOLUME) MATCHQTY, '...
            'sum(TURNOVER) AMOUNT, '...
            'sum(TURNOVER_EUR) AMOUNT_EUR '...
            'FROM QUANT_work..TradePerChannel T '...
            'WHERE	DealDate >= ''%s 00:00:00'' and DealDate<=''%s 23:59:59''  '...
            'group by DealDate, ISIN, CotationPlace, TRADINGCURRENCY, CrystalCustomer, ExecutionChannel, ServiceCode, ' ...
            'case when SIDE= ''BUY'' then 1 else -1 end'], from, to);
        data = exec_sql_table('QUANT_work',req);

        uds = unique(data.DATE);
        udn = cell2mat(cellfun(@(c) datenum(c,'yyyy-mm-dd'), uds, 'uni', 0));
        data.date = joint(uds, udn, data.DATE);
        data.DESK = data.ServiceCode;

        % filter Kepler Cheuvreux technical account (used for transferring US-Canada trades to Europe > double counting)
        % data = data(~ismember(data.ACCOUNTID, [2146860452]),:);


        if ~isempty(opt.get('dynamic_index'))
            error('Matching of security_id is not coded yet. Please remove dynamic_index option in your call: ''dynamic_index'', []')
        end
end

%% client type x region
acc = exec_sql_table('KGR','select * from KGR..Account_Classification');

data.client_type = joint(acc.Id, acc.Type, data.ACCOUNTID);
data.client_region = joint(acc.Id, acc.Country, data.ACCOUNTID);
data.client_name = joint(acc.Id, acc.Name,data.ACCOUNTID);
data_by_month.no_filter = grouby_month(data);
data_by_month_account.no_filter = grouby_month_account(data);
%% filters
% filter management guidelines: Technical
if opt.get('filter_technical')
    if strcmpi(mode, 'booking_channels')
        data = data(~ismember(data.ServiceCode, 'TECH'),:);
    end
    data = data(~ismember(data.client_type,{'Technical'}),:);
end
% filter management guidelines: corporate
if opt.get('filter_corporate')
    if strcmpi(mode, 'booking_channels')
        data = data(~ismember(data.ServiceCode, 'CORP'),:);
       
    elseif strcmpi(mode, 'executions')
        data = data(~ismember(data.DESK, 'Corporate'),:);
    end
end

if (opt.get('filter_technical') && opt.get('filter_corporate'))
    data_by_month.salesapp = grouby_month(data);
    data_by_month_account.salesapp = grouby_month_account(data);
else
    data_by_month.salesapp = array2table([]);
    data_by_month_account.salesapp = array2table([]);
end

%% filter Quant reasearch
% filter Quant reasearch: client_types
if ~isempty(opt.get('keep_client_types'))
    data = data(ismember(data.client_type,opt.get('keep_client_types')),:);
end
% filter Quant reasearch: filter_QFD_exclusions
if opt.get('filter_QFD_exclusions')
    permanent_filter = exec_sql_table('KGR','select ID,convert(DATE,StampDate) DATE from QUANT_FLOW_DATA..DailyClientFilter');
    for f = 1:height(permanent_filter)
        if ismember(permanent_filter.ID(f),[951480556,2146849257,2146849258,2146859947])
            ix_f = ismember(data.ACCOUNTID, permanent_filter.ID(f));
        else
            ix_f = ismember(data.ACCOUNTID, permanent_filter.ID(f)) & data.date>=datenum(permanent_filter.DATE(f),'yyyy-mm-dd');
        end
        data = data(~ix_f,:);
    end
end

if ((~isempty(opt.get('keep_client_types'))) && opt.get('filter_QFD_exclusions') ...
     && opt.get('filter_technical') && opt.get('filter_corporate'))
    data_by_month.SA_QR = grouby_month(data);
    data_by_month_account.SA_QR = grouby_month_account(data);
else
    data_by_month.SA_QR = array2table([]);
    data_by_month_account.SA_QR = array2table([]);
end
%% group by month
    function data_by_month = grouby_month(data)
        date_month = datenum(year(data.date), month(data.date),1);
        data_month = data;
        data_month.date_month = date_month;
        [group, id1, id2, id3] = findgroups(data_month.date_month, data_month.client_type, data_month.client_region);
        func = @(q, t, te) [nansum(q), nansum(t), nansum(te)];
        result = splitapply(func, data_month.MATCHQTY, data_month.AMOUNT, data_month.AMOUNT_EUR, group);
        data_by_month = cell2table([num2cell(id1), id2, id3, num2cell(result)],...
            'VariableNames', {'date','client_type','client_region','MATCHQTY','AMOUNT','AMOUNT_EUR'});
        data_by_month.date = datestr(data_by_month.date,'yyyy-mm-dd');
        clear data_month
    end
    function data_by_month = grouby_month_account(data)
            date_month = datenum(year(data.date), month(data.date),1);
            data_month = data;
            data_month.date_month = date_month;
            [group, id0, id1, id11, id2, id3, id4] = findgroups(data_month.date_month, data_month.ACCOUNTID, ...
                data_month.DESK, data_month.client_name, ...
                data_month.client_type, data_month.client_region);
            func = @(q, t, te) [nansum(q), nansum(t), nansum(te)];
            result = splitapply(func, data_month.MATCHQTY, data_month.AMOUNT, data_month.AMOUNT_EUR, group);
            data_by_month = cell2table([num2cell(id0), num2cell(id1), id11, id2, id3, id4, num2cell(result)],...
                'VariableNames', {'date', 'ACCOUNTID', 'DESK', 'client_name', 'client_type','client_region','MATCHQTY','AMOUNT','AMOUNT_EUR'});
            data_by_month.date = datestr(data_by_month.date,'yyyy-mm-dd');
            clear data_month
        end
end

