function [out, sec_ids, idxes] = get_main_tds_for_index(index_name)
% GET_MAIN_TDS_FOR_INDEX - renvoie une sorte de
% trading_destination_principale pour un indice
% out(1, 1) est cette sorte de trading_destination_principale
% tandis que out(1, 2) est la proportion de titres appartenant � l'indice pour
% lesquels c'est effectivement la trading_destination_principale
% les lignes suivantes respecte le m�me sch�ma pour les autres
% trading_destination_principales des titres de l'indice
%
% out = get_main_tds_for_index('AEX') % out = get_main_tds_for_index('AMX')
% out = get_main_tds_for_index('ISEQ')
% out = get_main_tds_for_index('FTSE 100')
% out = get_main_tds_for_index('BEL20')
% out = get_main_tds_for_index('OMX')
% out = get_main_tds_for_index('ATX')
% out = get_main_tds_for_index('FTSE / JSE')
% out = get_main_tds_for_index('PSI 20')
% out = get_main_tds_for_index('WIG20')
% out = get_main_tds_for_index('CAC40')
% out = get_main_tds_for_index('DAX')
% out = get_main_tds_for_index('IBEX35')
% out = get_main_tds_for_index('MIB30')
% out = get_main_tds_for_index('SMI')
% out = get_main_tds_for_index('KFX')
% out = get_main_tds_for_index('HEX')
% out = get_main_tds_for_index('OBX')
%
% Un exemple plus clair et pas un liste :
% la premi�re colonne du premier output est le trading_destination_id
% la deuxi�me colonne du premier output 
% est la proportion de titres dans la trading_destination
% le deuxi�me output est la liste des security_id
% le troisi�me ouput sont les index (logiques) dans la liste des
% security_id pour chacune des trading_destination_id (cell array de m�me 
% longueur que le premier out dont les elements sont des boleens de m�me 
% longueur que le deuxi�me output)
%
% fonctionne aussi pour une liste de security_id :
% [out, sec_ids, idxes] = get_main_tds_for_index(get_repository('index-comp', 'CAC40'))
% pour le CAC40, la trading_destination_ primaire est Euronext PAris :
% get_td_name(out(1, 1))
% voici la liste des titres dont la trading_destination_primaire est Euronext PAris
% get_security_name(sec_ids(idxes{1}))
% mais il y aussi DEXIA
% get_security_name(sec_ids(idxes{2}))
% dont la destination primaire est Euronext Brussel
% get_td_name(out(2, 1))
% et ARCELORMITTAL
% get_security_name(sec_ids(idxes{3}))
% dont la destination primaire est EURONEXT AMSTERDAM
% get_td_name(out(3, 1))
%

if ischar(index_name)
    sec_ids  = get_repository('index-comp',index_name,'recent_date_constraint', false);
elseif isnumeric(index_name) && length(index_name) > 1
    sec_ids = index_name;
end
main_tds = NaN(length(sec_ids), 1);

for i = 1 : length(sec_ids)
    main_td4this_sec = get_repository('tdi', sec_ids(i));
    main_tds(i) = main_td4this_sec(1);
end

out = unique(main_tds);
out = [out NaN(length(out), 1)];
idxes = cell(size(out, 1), 1);
for i = 1 : size(out, 1)
    idxes{i} = main_tds==out(i, 1);
    out(i, 2) = sum(idxes{i});
end
out(:, 2) = out(:, 2) / sum(out(:, 2));
[out(:, 2), idx] = sort(out(:, 2), 1, 'descend');
out(:, 1) = out(idx, 1);
idxes = idxes(idx);