function nb_ob_updates = get_number_of_updates(security_id,date,trading_destination, max_limit)
% GET_NUMBER_OF_UPDATES - Retrieve the number of order book updates up to
% the max_limit
% ex :
% number of 1st limit update on ChiX :
% nb = get_number_of_updates(110,'20110830',61,1)
% number of 1st limit update on ENPA :
% nb = get_number_of_updates(110,'20110830',4,1)
% see also get_tickdb2, get_orderbook_tbt2
global st_version

nb_ob_updates =get_nb_ob_updates(st_version.my_env.tbt2_repository,security_id, date,trading_destination,0,1,max_limit);
