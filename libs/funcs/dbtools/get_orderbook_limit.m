function data = get_orderbook_limit( mode, varargin)
% GET_ORDERBOOK_LIMIT - version de get_tick pour le future CAC40
%%- NOTES on trading-destinations

% internal uses:
% data0=get_orderbook_limit('orderbook_by_destination', 'security_id',110,'trading_destination_id',4,'depth_max',1,'day','08/06/2010')
% data1=read_dataset('orderbook_limit','security_id',26,'trading_destination_id',4,'from','01/10/2010','to','01/10/2010');
% data2=read_dataset('orderbook_limit','security_id',26,'trading_destination_id',4,'from','01/10/2010','to','01/10/2010','depth_max',5);
% %--today data
% date_today=datestr(today,'dd/mm/yyyy');
% %-- all data
% data=get_orderbook_limit('orderbook_by_destination','security_id',110,'trading_destination_id',4,'depth_max',2,'day',date_today,'today_allowed',true);
% %-- selected data
% begin_time=0.4235;end_time=0.4240;place_timezone_id=1;
% data=get_orderbook_limit('orderbook_by_destination','security_id',110,'trading_destination_id',4,'depth_max',2,'day',date_today,'today_allowed',true,'begin_time',begin_time,'end_time',end_time,'place_timezone_id',place_timezone_id);
%
%
% See also from_buffer read_dataset



switch lower(mode)
    
    case 'masterkey'
        opt = options({'security_id', '','trading_destination_id','','depth_max',1}, varargin(2:end));
        security_id = opt.get('security_id');
        trading_destination_id = opt.get('trading_destination_id');
        depth_max = opt.get('depth_max');
        if ~isnumeric(security_id)
            error('get_orderbook_limit:security_id is not a number');
        end
        if ~isnumeric(trading_destination_id)
            error('get_orderbook_limit:trading_destination_id is not a number');
        end
        
        data = fullfile(varargin{1},sprintf('depth_max=%d',depth_max),get_repository('security-td-key', security_id, trading_destination_id));
        
    case 'orderbook_by_destination'
        
        t0 = clock;
        
        %<* Read into the tickdb
        opt = options({'security_id', '','trading_destination_id','', 'day', '01/04/2008',...
            'date_format:char', 'dd/mm/yyyy','depth_max',1, ...
            'today_allowed',false,...
            'begin_time',[],'end_time',[],'place_timezone_id',[],...
            }, varargin);
        %< Lecture des options
        format_date = opt.get('date_format:char');
        security_id = opt.get('security_id');
        trading_destination_id = opt.get('trading_destination_id');
        if ~isnumeric(security_id)
            error('get_orderbook_limit:security_id is not a number');
        end
        if ~isnumeric(trading_destination_id) || length(trading_destination_id)>1
            error('get_orderbook_limit:trading_destination_id input is bad');
        end
        depth_max = opt.get('depth_max');
        td_info = get_repository( 'trading-destination-info', security_id);
        day_ = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        %>
        this_day = datestr( day_, 'yyyymmdd');
        
        %< check dates
        if day_ >= today 
            dbs = dbstack;
            if day_ == today && opt.get('today_allowed') && ~ismember('from_buffer.m', {dbs.file})
                % ok you are allowed to go further
            else
                error('get_orderbook_limit:check_args', 'TODAY : Input parameters do not allow you to extract today data');
            end
        end
        %>
        %< check options for today
        begin_time = opt.get('begin_time');
        end_time = opt.get('end_time');
        place_timezone_id = opt.get('place_timezone_id');
        any_time_today_opt=~isempty(begin_time) || ~isempty(end_time) || ~isempty(place_timezone_id);
        all_time_today_opt=~isempty(begin_time) && ~isempty(end_time) && ~isempty(place_timezone_id);
        if any_time_today_opt
            if day_ ~= today
                error('get_orderbook_limit:check_args',' begin_time/end_time is only available for TODAY data');
            end
            if ~all_time_today_opt
                error('get_orderbook_limit:check_args',' begin_time/end_time/place_timezone_id are MANDATORY if any');
            end
            begin_time=mod(begin_time,1);
            end_time=mod(end_time,1);
        end
        %>
        
        % -----------------------------------------------------
        % -------------------------- handle timezone
        % -----------------------------------------------------
        
        local_place_timezone_id=td_info(1).place_id;
        recorded_place_timezone_id=td_info(1).default_timezone;
        
        
        % -----------------------------------------------------
        % -------------------------- recup?ration des updates
        % -----------------------------------------------------
        % < requete
        
        select_str = [  'select date,datediff(millisecond,''00:00:00'',time),microseconds,' ...
            ' side,depth,price,size,mmk_number,image ' ...
            ' from '];
        
        colnames={'side','depth','price','size','mmk_number','image'};
        
        if floor(day_) == today
            orderbook_name = [ 'orderbook_update_day' td_info(1).global_zone_suffix ];
            server_name='tick_db';
        else
            orderbook_name=['orderbook_update' td_info(1).global_zone_suffix ];
            
            server_name=get_basename4day( day_); 
            server_name(find(server_name=='d',1,'last'))='o';
        end
       
        
        main_req = sprintf('%s..%s', server_name, orderbook_name);
        %>
        end_req = sprintf(['  where date = ''%s'' ' ...
            '   and security_id=%d   ' ...
            '   and trading_destination_id=%d   ' ...
            '   and depth<%d   '], ...
            this_day, security_id,trading_destination_id,depth_max);
        
        if all_time_today_opt
            
            %- !!!!!!!!!!!!!!   BEWARE   !!!!!!!!!!!!!!!!!
            %- using begin_time and end_time could imply to loose
            %- information at the start of the computation...
            %- remember how the OB data are stored ...
            %- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            
            one_sec=1/(24*60*60);
            
            decalage_input2recorded  = timezone('get_offset',...
                'final_place_id',recorded_place_timezone_id ,...
                'init_place_id',place_timezone_id,...
                'dates', floor(day_));
            
            begin_time=max(0+one_sec,begin_time-one_sec+decalage_input2recorded);
            end_time=min(1-one_sec,end_time-one_sec+decalage_input2recorded);
            
            end_req=[end_req ...
                sprintf([' and time >= ''%s'' ' ...
                ' and time <= ''%s'' '], ...
                datestr(begin_time,'HH:MM:SS'),...
                datestr(end_time,'HH:MM:SS'))];
            
        end
        
        end_req=[end_req '  order by date,time,microseconds ']; 
        
        sql_req = [select_str main_req end_req];
        st_log('just before fetch (%5.2f sec)\n', etime(clock, t0));
        
        t1 = clock;
        vals = exec_sql('BSIRIUS', sql_req);
        st_log('end of fetch (%5.2f sec)\n', etime(clock, t1));
        
        if isempty(vals)
            data = [];
            return;
        end
        
        
        
        
        % -----------------------------------------------------
        % -------------------------- transformation des upadte
        % -----------------------------------------------------
        
        t2 = clock;
        
        %< If not recorded in localtime, then convert
        
        decalage  = timezone('get_offset',...
            'final_place_id',local_place_timezone_id ,...
            'init_place_id',recorded_place_timezone_id,...
            'dates', floor(day_));
        %>
        
        %------------------------
        %---- DATETIME Handling
        %------------------------

        sec1 = 1/(24*3600);
        
        datetime_matrix=cat(2,day_*ones(size(vals,1),1),...% DAY
            round(cell2mat(vals(:,2))/1000),...% SECONDS
            cell2mat(vals(:,3))...% MICROSECONDS
            );


        
        
        decalage_in_sec=round(decalage/sec1);
        datetime_matrix(:,2)=datetime_matrix(:,2)+decalage_in_sec;
 
        id_localdaybefore=datetime_matrix(:,2)<0;
        
        if any(id_localdaybefore)
            one_day_in_sec=86400;
            datetime_matrix(id_localdaybefore,1)=datetime_matrix(id_localdaybefore,1)-1;
            datetime_matrix(id_localdaybefore,2)=one_day_in_sec+datetime_matrix(id_localdaybefore,2);
        end
        
        [datetime_matrix_uni,~,datetime_idx_uni]=unique(datetime_matrix,'rows');
        
%         datess=datetime_matrix_uni(:,1)+sec1*datetime_matrix_uni(:,2)+1e-6*sec1*datetime_matrix_uni(:,3);
%         idx=transpose(1);
%         cat(2,arrayfun(@(a)(a),datetime_matrix_uni(idx,3),'uni',false),arrayfun(@(a)(datestr_ms(a)),datess(idx),'uni',false))
%       

        %------------------------
        %---- VALUES Handling
        %------------------------
        
        vals = cellfun(@(x)double(x),vals(:,4:end));
       
        %------------------------
        %---- OUT : by default
        %------------------------ 
        
        colnames_out={'day','seconds','microseconds','image'};
        for i_depth=1:depth_max
            colnames_out=cat(2,colnames_out,...
                sprintf('bid_price_%d',i_depth),...
                sprintf('bid_size_%d',i_depth),...
                sprintf('bid_nb_%d',i_depth),...
                sprintf('ask_price_%d',i_depth),...
                sprintf('ask_size_%d',i_depth),...
                sprintf('ask_nb_%d',i_depth));
        end
        
        clear datetime_matrix;
        out_data=st_data('init',...
            'title','orderbook data',...
            'date',sec1*datetime_matrix_uni(:,2)+1e-6*sec1*datetime_matrix_uni(:,3),...
            'colnames',colnames_out,...
            'value',cat(2,datetime_matrix_uni,NaN(size(datetime_matrix_uni,1),length(colnames_out)-3))...
            );
        
        
        %------------------------
        %---- OUT : add goods values
        %------------------------
        
        for i_side=1:2
            for i_depth=1:depth_max
                
                id_tmp=(vals(:,1)==i_side-1 & vals(:,2)==i_depth-1);
                idx_tmp_in_matrix=datetime_idx_uni(id_tmp);
                
                if length(idx_tmp_in_matrix)~=length(unique(idx_tmp_in_matrix))
                    error('get_orderbook_limit: impossible ???')
                end
                
                % side,depth,price,size,mmk_number,image
                
                if i_side==1
                    out_data.value(idx_tmp_in_matrix,...
                        [find(strcmp(out_data.colnames,sprintf('bid_price_%d',i_depth))) ...
                        find(strcmp(out_data.colnames,sprintf('bid_size_%d',i_depth))) ...
                        find(strcmp(out_data.colnames,sprintf('bid_nb_%d',i_depth)))...
                        find(strcmp(out_data.colnames,'image'))...
                        ])=vals(id_tmp,3:6);
                else
                    out_data.value(idx_tmp_in_matrix,...
                        [find(strcmp(out_data.colnames,sprintf('ask_price_%d',i_depth))) ...
                        find(strcmp(out_data.colnames,sprintf('ask_size_%d',i_depth))) ...
                        find(strcmp(out_data.colnames,sprintf('ask_nb_%d',i_depth)))...
                        find(strcmp(out_data.colnames,'image'))...
                        ])=vals(id_tmp,3:6);
                end
            end
        end
        
        %------------------------
        %---- OUT : fill_nan_values goods values
        %------------------------
        idx_col_2fillnan=find(~ismember(out_data.colnames,{'day','seconds','microseconds','image'}));
        
        if ~isempty(idx_col_2fillnan)
            for i_col=1:length(idx_col_2fillnan)
                out_data.value(:,idx_col_2fillnan(i_col))=...
                    fill_nan_v2(out_data.value(:,idx_col_2fillnan(i_col)),'mode','before');
            end
        end
        
        
        %------------------------
        %---- OUT : add .info, title...
        %------------------------ 
        
        ric='';
        try
            ric=get_ric_from_sec_id(security_id);
            ric=ric{1};
        catch
        end
        data = out_data;
        data.title=sprintf( 'Order book update %s - %s - number of limits : %d',ric,...
            datestr(day_, 'dd/mm/yyyy'),depth_max);
        data=st_data('add-col',data,...
            repmat(trading_destination_id,size(data.value,1),1),'trading_destination_id');
        
        
        data.info =  ...
            struct('security_id', security_id, ...
            'security_key', get_repository( 'security-key', security_id), ...
            'td_info', td_info , ...
            'depth_max',depth_max,...
            'sql_request', sql_req, ...
            'data_datestamp',day_,...
            'data_log', data.info.data_log, ...
            'localtime', true,...
            'place_timezone_id', local_place_timezone_id,...
            'local_place_timezone_id',local_place_timezone_id) ;
        
        st_log('end of update transformation (%5.2f sec)\n', etime(clock, t2));
   
end


end









