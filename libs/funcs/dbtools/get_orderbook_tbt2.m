function [data_td] = get_orderbook_tbt2( mode, varargin)
% GET_ORDERBOOKB_TBT2 - retrieve orderbook data in tbt2 files
% This function suppot only one day and one trading destination
%
%
% TODO :
%   TODO0 : function Book renvoyant l'ensemble des destination
%       pr?sentes car on ne peut pas se baser sur la liste de destinations
%       renvoy?es par
%       [sec_id, tdi] = get_repository( 'any-to-sec-td', 'security_id', opt.get('security') , ...
%            'trading-destinations', asked_td);
%       puisu'elle n'est pas complete. voir par exemple :
%       j=get_repository('tdinfo', 381) => j.trading_destination_id = 6 91
%       92 89 et il n'y pas 81 alors que cela traite sur Turquoise
%       Cette 'fonctionnalit?' (les destination pr?sentes dans
%       security_market sont probablement celles que l'on s'autorise ?
%       traiter)
%       ?tait d?j? pr?sente dans get_tick en mode filter-td mais pas ft.
%       ? voir si l'on souhaite conserver ce comportement.
%
%   TODO2 - r?cup?ration dans un fichier r?f?rentiel du jour
%       security_id, trading_destination_id, short_name, quotation_group, et GMT_offfset (du
%       primaire) et donc aussi quelle est la destination primaire
%   TODO3 - si on veut ?tre portable il faut les horaires de trading? dans
%   ce
%       cas il est peut ?tre inutile d'avoir le groupe de quotation
%   TODO4 le .info n'est jamais mis dans data dans certains cas ??
%
% Examples:
% data = get_orderbook_tbt2( 'ft', 'security', 'BAYG.DE', 'date', '04/11/2010', 'trading-destinations', 8 );
% Get only the ob update when the 1st limit has changed  : set the
% max_limit parameter to 1
% data = get_orderbook_tbt2( 'ft', 'security', 'TOTF.PA', 'date', '04/11/2010', 'trading-destinations', 4 ,'max_limit', 1);
% %retrieve first limits using last_formula
% data = get_orderbook_tbt2( 'ft', 'security', 'TOTF.PA', 'date', '04/11/2010', 'trading-destinations', 4,...
%'last_formula', '[{bidPrice1},{askPrice1}]' );
% %using time filtered 'begin-time' and 'end-time' are expressed in local
% time
% data = get_orderbook_tbt2( 'time-filtered', 'security', 'FTE.PA','date', ...
% '04/11/2010', 'trading-destinations', 4,'begin-time',0.5,'end-time',0.5 + 2 /24);
% 
% data are in LOCALTIME (from the main td point of view)
% See also : read_dataset
%
%
%   author   : 'vleclerc@cheuvreux.com'
%   reviewer : ''
%   version  : '1.1'
%   date     :  06/04/2011

global st_version;
if ~isfield( st_version.my_env, 'tbt2_repository')
    error('get_orderbook_tbt2:st_version', 'please get the default st_work.xml and restart matlab');
end

data_folder = st_version.my_env.tbt2_repository;

switch lower( mode)
    case 'ft'
        opt = options({ 'security', [], 'dt_to', [], 'trading-destinations', '', ...
            'where_formula', [], 'last_formula', [], 'date-format', 'dd/mm/yyyy', ...
            'day-into-date', true, 'output-mode', 'all','max_limit', 5}, varargin);
        
        % < gestion des dates qui seront dans le st_data ou dans le
        % cell_array de st_data renvoy?
        % we only use one day here
        date_format = opt.get('date-format');
        dt_all = opt.get('date');
        if ischar( dt_all)
            dt_all = datenum( dt_all, date_format);
        end
        
        if length( dt_all) > 1
            error('get_orderbook_tbt2:date this function only supports one day');
        end
        
        % dt_all est maintenant un jour
        % >
        
        % 1. r?f?rentiel et traduction td
        % only one td supported in this mode
        % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        asked_td = opt.get('trading-destinations');
        
        if isempty(asked_td) || length(asked_td) > 1
            error('get_orderbook_2:trading_destination', 'trading_destination: this function only supports one trading destination');
        end
        tic
        [sec_id, tdi] = get_repository( 'any-to-sec-td', 'security_id', opt.get('security') , ...
            'trading-destinations', asked_td);
        toc
        % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        d_into_date = opt.get('day-into-date');
        
        max_limit = opt.get('max_limit');
        
        coln = {'price', 'volume', 'sell', 'bid', 'ask',  ...
            'trading_destination_id', 'bid_size', 'ask_size', 'buy',...
            'cross', 'auction', 'opening_auction', 'closing_auction', 'trading_at_last', ...
            'trading_after_hours', 'intraday_auction', 'dark', 'order_book_id'};
        % < gestion du mode de sortie, souhaite-t-on un cell array avec un
        % st_data pour chaque jour ou bien veut on les empiler?
        output_mode_d = strcmpi(opt.get('output-mode'), 'day');
        if output_mode_d
            error('get_orderbook_tbt2:output-mode this function only supports one day, so this mode is not usable');
        end
        % >
        
        % < on ne peut pas demander un seul st_data m?langeant plusieurs
        % jours et ne contenant pas dans le champs date d'information sur
        % le jour
        
        % code d?fensif : ne defvrait jamais ?tre vrai du fait des
        % contr?les pr?c?dents.
        if ~output_mode_d && ~d_into_date && length(dt_all) > 1
            error('get_orderbook_tbt2:chec_args', ...
                'BAD_USE: you are asking for a unique st_data mixing several days but with a field date containing only hours');
        end
        % >
        % local time is the one of the asked trading destinaton 
        
        td_idx = find(ismember([tdi.trading_destination_id], [sec_id.trading_destination_id]));
        
        
        if isempty(td_idx)
             error('get_orderbook_tbt2:tradingdestination','trading-destination %d not found for sec_id %d ',asked_td,sec_id.security_id );
        end
            
        idxdest4time = td_idx(1);
        % >
        d = dt_all;
        
        % TODO2 rendra inutile cette ligne
        
        GMT_offset = timezone('get_offset','init_place_id', 0,... % MAGIC NUMBER 0 => GMT
            'final_place_id', tdi(idxdest4time).place_id, 'dates', d); % MAGIC NUMBER 1 => destination primaire
        if d_into_date
            dt_day = d;
        else
            dt_day = 0;
        end
        % < le st_data dans lequel on va accumuler les
        % trading_destinations
        title_ = sprintf('%s - %s', sec_id.security_key, datestr(d, 'dd/mm/yyyy') );

        % >

%         % < 2. r?cup data
            [~,data_td]   = loadOrderBook(data_folder , ...
                sec_id.security_id, datestr(d, 'yyyymmdd'),...
                sec_id.trading_destination_id, 0, 0, max_limit);% MAGIC NUMBER 0 => auctionType et 1=>trade only
            if tdi(td_idx).localtime
                dt_offset = 0;
            else
                dt_offset = GMT_offset;
            end
            
            data_td.date =   data_td.date + dt_offset;
        % >
        
        if ~(isempty(data_td) || isempty(data_td.value))
            % < 3. filtres
            where_formula = opt.get('where_formula');
            if ~isempty( where_formula)
                nb_lines = length(data_td.date);
                data_td = st_data('where', data_td, where_formula);
                st_log('get_orderbook_tbt2:where_formula application of <%s> reduce the dataset from %d to %d lines\n', ...
                    where_formula, nb_lines, length(data_td.date));
            end
            
            last_formula = opt.get('last_formula');
            if ~isempty( last_formula) && ~isempty( data_td.value)
                nb_cols = length(data_td.colnames);
                data_td = st_data('apply-formula', data_td, last_formula);
                st_log('get_orderbook_tbt2:last_formula application of <%s> changed the dataset from %d to %d cols\n', ...
                    last_formula, nb_cols, length(data_td.colnames));
            end
            % >
        end
        
        
        % 5. cr?ation du .info
        data_td.info = struct( 'td_info', ...
            rmfield(tdi(td_idx), {'execution_market_id', 'timezone', 'global_zone_name', ...
            'global_zone_suffix', 'default_timezone', 'localtime'}), ...
            'localtime', true, 'GMT_offset', GMT_offset , 'place_timezone_id', ...
            tdi(td_idx).place_id, 'local_place_timezone_id' , tdi(td_idx).place_id, ...
            'security_id',sec_id.security_id, 'data_datestamp', d, 'depth_max_available' , max(st_data('col', data_td, 'ob_limit_update')));
        data_td.title = sprintf('%s - %s -> %s', sec_id.security_key, datestr(dt_all, 'dd/mm/yyyy'), ...
                    datestr(dt_all, 'dd/mm/yyyy'));
       
    case 'time-filtered' % extract only a subset of the orderbook of the day
        opt = options({ 'security', [], 'date', [], 'trading-destinations', '', ...
            'begin-time',[],'end-time',[],'where_formula', [], 'last_formula', [], 'date-format', 'dd/mm/yyyy', ...
            'day-into-date', true, 'output-mode', 'all','max_limit', 5}, varargin);
        
        % < gestion des dates qui seront dans le st_data ou dans le
        % cell_array de st_data renvoy?
        % we only use one day here
        date_format = opt.get('date-format');
        dt_all = opt.get('date');
        if ischar( dt_all)
            dt_all = datenum( dt_all, date_format);
        end
        
        dt_start =  opt.get('begin-time');
        dt_end   =  opt.get('end-time');
        max_limit = opt.get('max_limit');
        
        if isempty(dt_start) && isempty(dt_end)
            error('get_orderbook_2: end-time', 'pleas eprovide at least one value for begin-time or end-time');
        end
        
        if isempty(dt_start)
            dt_start = 0;
            st_log('\nusing default value ( = 0 ) for start-time');
        end
        
        if isempty(dt_end)
            dt_end = 1;
            st_log('\nusing default value (  = 1 ) for end-time');
        end
        
        
        if dt_end <= dt_start
            error('get_orderbook_2: end-time', 'end-time : the end-time must be posterior to the begin time');
        end
        % dt_all est maintenant le calendrier sur lequel on va boucler
        % >
        
        % 1. r?f?rentiel et traduction td
        % TODO1 et TODO2 rendront inutiles ces appels de plus ils seront
        % d?pendants du jour et devront donc ?tre dans la boucle sur dt_all
        % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        asked_td = opt.get('trading-destinations');
        
        if isempty(asked_td) || length(asked_td) > 1
            error('get_orderbook_2:trading_destination', 'trading_destination: this function only supports one trading destination');
        end
        
        [sec_id, tdi] = get_repository( 'any-to-sec-td', 'security_id', opt.get('security') , ...
            'trading-destinations', asked_td);
        % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        d_into_date = opt.get('day-into-date');
        
        coln = {'price', 'volume', 'sell', 'bid', 'ask',  ...
            'trading_destination_id', 'bid_size', 'ask_size', 'buy',...
            'cross', 'auction', 'opening_auction', 'closing_auction', 'trading_at_last', ...
            'trading_after_hours', 'intraday_auction', 'dark', 'order_book_id'};
        % < gestion du mode de sortie, souhaite-t-on un cell array avec un
        % st_data pour chaque jour ou bien veut on les empiler?
        output_mode_d = strcmpi(opt.get('output-mode'), 'day');
        if output_mode_d
            data = cell(size(dt_all, 1), 1);
        else
            data = [];
        end
        % >
        
        % < on ne peut pas demander un seul st_data m?langeant plusieurs
        % jours et ne contenant pas dans le champs date d'information sur
        % le jour
        if ~output_mode_d && ~d_into_date && length(dt_all) > 1
            error('get_orderbook_tbt2:check_args', ...
                'BAD_USE: you are asking for a unique st_data mixing several days but with a field date containing only hours');
        end
        % >
        % < is prmary market in the list of destination or not : if yes
        % then local time is the one of the primary market. if not then
        % local time is the one of the first destination asked
        
        
        % TODO0 il ne faut pas se baser sur les destinations pr?sentes mais
        % celles qui sont enregistr?es? => pour les groupe de quotation on
        % ne disposera pas de l'information puisque c'est directement dans
        % security_market que ces informations ne sont pas pr?sentes
        
        td_idx = find(ismember([tdi.trading_destination_id], [sec_id.trading_destination_id]));
        idxdest4time = td_idx(1);
        % >
        d = dt_all;
        
        % TODO2 rendra inutile cette ligne
        
        GMT_offset = timezone('get_offset','init_place_id', 0,... % MAGIC NUMBER 0 => GMT
            'final_place_id', tdi(idxdest4time).place_id, 'dates', d); % MAGIC NUMBER 1 => destination primaire
        if d_into_date
            dt_day = d;
        else
            dt_day = 0;
        end
        % < le st_data dans lequel on va accumuler les
        % trading_destinations
        title_ = sprintf('%s - %s', sec_id.security_key, datestr(d, 'dd/mm/yyyy') );
        
        % >
            
        if tdi(td_idx).localtime
                dt_offset = 0;
        else
                dt_offset = GMT_offset;
        end
        
        % the time start and end are now in local time , see 163 in Mantis
        
        dt_start = dt_start - dt_offset;
        dt_end = dt_end - dt_offset;
        % < 2. r?cup data
        

        [dummy,data_td]   = loadOrderBook(data_folder , ...
                sec_id.security_id, datestr(d, 'yyyymmdd'),...
                sec_id.trading_destination_id, 0, 0,dt_start,dt_end, max_limit);% added time  filtration
            % clear dummy

            
            data_td.date =   data_td.date + dt_offset;
        % >
                
             if ~(isempty(data_td) || isempty(data_td.value))
            % < 3. filtres
            where_formula = opt.get('where_formula');
            if ~isempty( where_formula)
                nb_lines = length(data_td.date);
                data_td = st_data('where', data_td, where_formula);
                st_log('get_orderbook_tbt2:where_formula application of <%s> reduce the dataset from %d to %d lines\n', ...
                    where_formula, nb_lines, length(data_td.date));
            end
            
            last_formula = opt.get('last_formula');
            if ~isempty( last_formula) && ~isempty( data_td.value)
                nb_cols = length(data_td.colnames);
                data_td = st_data('apply-formula', data_td, last_formula);
                st_log('get_orderbook_tbt2:last_formula application of <%s> changed the dataset from %d to %d cols\n', ...
                    last_formula, nb_cols, length(data_td.colnames));
            end
            % >
        end
        
        
        % 5. cr?ation du .info
        data_td.info = struct( 'td_info', ...
            rmfield(tdi(td_idx), {'execution_market_id', 'timezone', 'global_zone_name', ...
            'global_zone_suffix', 'default_timezone', 'localtime'}), ...
            'localtime', true, 'GMT_offset', GMT_offset , 'place_timezone_id', ...
            tdi(td_idx).place_id, 'local_place_timezone_id' , tdi(td_idx).place_id, ...
            'security_id',sec_id.security_id, 'data_datestamp', d, 'depth_max_available' , max(st_data('col', data_td, 'ob_limit_update')));
        data_td.title = sprintf('%s - %s -> %s', sec_id.security_key, datestr(dt_all, 'dd/mm/yyyy'), ...
        datestr(dt_all, 'dd/mm/yyyy'));
    otherwise
        error('get_orderbook_tbt2:mode', 'MODE: mode <%s> unknown', mode);
end