function data = get_prefixing_theoretic(mode, varargin)
% GET_PREFIXING_THEORETIC - data about the expected volume/price of the fixing
%
% sec_ids = get_index_comp('DAX');
% whole_data = [];
% for i = 1 : length(sec_ids)
%     data = read_dataset('fixing', 'security_id', sec_ids(i), 'from', '16/06/2011', 'to', '28/06/2011');
%     theo_vol = st_data('where', data, '~isnan({theo_vol})&({trading_destination_id}==8)');
%     whole_data = cat(1, whole_data, [st_data('col', theo_vol, 'theo_vol;phase') theo_vol.date]);
% end
% nb_less_vol = accumarray(1+whole_data(2:end, 2), diff(whole_data(:, 1))<0, [], @sum)
% nb_obs = accumarray(1+whole_data(2:end, 2), ones(size(whole_data, 1)-1, 1), [], @sum)
% nb_less_vol./nb_obs
%
% sec_ids = get_index_comp('CAC40');
% whole_data = [];
% for i = 1 : length(sec_ids)
%     data = read_dataset('fixing', 'security_id', sec_ids(i), 'from', '16/06/2011', 'to', '28/06/2011');
%     theo_vol = st_data('where', data, '~isnan({theo_vol})&(ismember({trading_destination_id}, [2 3 4]))');
%     whole_data = cat(1, whole_data, [st_data('col', theo_vol, 'theo_vol;phase') theo_vol.date]);
% end
% nb_less_vol_c = accumarray(1+whole_data(2:end, 2), diff(whole_data(:, 1))<0, [], @sum)
% nb_obs_c = accumarray(1+whole_data(2:end, 2), ones(size(whole_data, 1)-1, 1), [], @sum)
% nb_less_vol_c./nb_obs_c
%
% sec_ids = get_index_comp('FTSE 100');
% whole_data = [];
% for i = 1 : length(sec_ids)
%     data = read_dataset('fixing', 'security_id', sec_ids(i), 'from', '16/06/2011', 'to', '28/06/2011');
%     theo_vol = st_data('where', data, '~isnan({theo_vol})&(ismember({trading_destination_id}, [13]))');
%     whole_data = cat(1, whole_data, [st_data('col', theo_vol, 'theo_vol;phase') theo_vol.date]);
% end
% nb_less_vol_f = accumarray(1+whole_data(2:end, 2), diff(whole_data(:, 1))<0, [], @sum)
% nb_obs_f = accumarray(1+whole_data(2:end, 2), ones(size(whole_data, 1)-1, 1), [], @sum)
% nb_less_vol_f./nb_obs_f
%
%
% Examples:
%
% data = get_prefixing_theoretic('all-dest', 'security_id', 107509, 'day', '01/07/2011')
% data = read_dataset('fixing', 'security_id', 107509, 'from', '01/07/2011', 'to', get_as_of_date('dd/mm/yyyy'))
%
% See also: read_dataset
%   
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '28/06/2011'

persistent state_list phase_dico;
if isempty(state_list)
    [state_list, phase_dico] = trading_state_interpret();
end

switch mode
    case 'masterkey'
        %<* Return the generic+specific dir
        fmode = varargin{1};
        fmode = regexprep( fmode, '[ |*|\\|/|:|?|"|<|>|\|]', '_');
        opt = options({'security_id', NaN}, varargin(2:end));
        sec_id = opt.get('security_id');
        if ~isnumeric( sec_id) && numel(sec_id)==1
            error('get_tick:secid', 'I need an unique numerical security id');
        end
        %>*
        data = fullfile(fmode, get_repository( 'security-key', sec_id));
    case 'all-dest'
        opt = options({'security_id', NaN}, varargin);
        
        % < preparing information about requested data
        day_    = opt.get('day');
        day_num = datenum(day_, 'dd/mm/yyyy');
        datestr_yyyymmdd = datestr(day_num, 'yyyymmdd');
        sec_id  = opt.get('security_id');
        tdi     = get_repository('tdinfo', sec_id, day_); % TODO do not remove day_ it is currently useless but one day we should have an historized referential
        req_fields = {'trading_destination_id', 'theo_price', 'theo_vol', 'surplus', 'image'};
        req_fields_str = sprintf(',%s ', req_fields{:});
        base_name = get_basename4day( day_num);
        % >
        
        % < Checking if we are asking some data that exist : not before the
        % first recorded date and not in the future (or today)
        if day_num >= today
            error('get_prefixing_theoretic:check_args', 'You should''nt ask about the future or the current day, asked date is : <%s>', datestr(day_num, 'dd/mm/yyyy'));
        elseif day_num < -inf % 734685 % Magic number 01 / 07/ 2011 the first date for records
            error('get_prefixing_theoretic:check_args', 'You should''nt ask data for a date before <July the 1st 2011> as no data has been recorded before that date, asked date is : <%s>', datestr(day_num, 'dd/mm/yyyy'));
        end
        % >
        
        % < requesting the data % BSIRIUS
        vals = exec_sql('BSIRIUS', ... 
            sprintf(['select datediff(second,''00:00:00'', time)+microseconds/1000000.0 %s from ' ...
            ' %s..theoretic_info%s where security_id = %d and date = ''%s'' order by time, microseconds' ...
            ], req_fields_str, base_name, tdi(1).global_zone_suffix, sec_id, datestr(day_num, 'yyyymmdd')));
        gmt_offset = timezone('get_offset','init_place_id', 0,... % MAGIC NUMBER 0 => GMT
                    'final_place_id', tdi(1).place_id, 'dates', day_num);    
        sec_key = get_repository( 'security-key', sec_id);
        
        if ~isempty(vals)
            vals = [cell2mat(vals(:, 1:end-1)) cellfun(@double, vals(:,end))];
            % >

            % < timestamps computation
            timestamps = vals(:, 1)/(24*3600);
            if ~tdi(1).localtime
                timestamps =  timestamps + gmt_offset;
            end
            % > 

            % < creating the st_data
            data = st_data('init', 'value', vals(:, 2:end), 'date', timestamps, ...
                'colnames', req_fields, 'title', ['theoretical fixing data for ' sec_key]);
        else
            data = st_data('empty-init');
        end
        % >
        
        % < adding the state of trading in order to know which kind of
        % auction we are talking about
        vals = exec_sql('BSIRIUS', ... % TODO change this for BSIRIUS when available in real databases
            sprintf(['select datediff(second, ''00:00:00'', time)+microseconds/1000000.0, trading_destination_id, trading_state, image from ' ...
            ' %s..trading_states%s where security_id = %d and date = ''%s'' order by time, microseconds' ... % TODO change database when changing server mnamonic to BSIRIUS if eventually it became relative to the trading date
            ], base_name, tdi(1).global_zone_suffix, sec_id, datestr_yyyymmdd));
        %>
        if ~isempty(vals)
        % < timestamps computation
            timestamps = cell2mat(vals(:, 1))/(24*3600);
            if ~tdi(1).localtime
                timestamps =  timestamps + gmt_offset;
            end
            % > 

            td_id = cell2mat(vals(:, 2));
            image = cellfun(@double, vals(:,end));
            associated_phase = NaN(size(td_id));

            % < Association from message to the trading phase
            % TODO optimize if needed
            for i = 1 : length(associated_phase)
                associated_phase(i) = phase_dico(strcmp(vals{i, 3}, state_list));
            end
            % >

            trading_state = st_data('init', 'date', timestamps, 'value', [td_id image associated_phase], ...
                'colnames', {'trading_destination_id', 'image', 'phase'}, 'title', '', ...
                'rownames', vals(:, 3));
        else
            trading_state = st_data('empty-init');
        end
        
        if ~st_data('isempty-nl', data) && ~isempty(vals)
            % < Attributing trading states to theoretic info to know what phase
            % it is about. TODO optimize if needed
            td_in_states   = st_data('col', trading_state, 'trading_destination_id');
            u_td_in_states = unique(td_in_states);
            td_in_preftheo = st_data('col', data, 'trading_destination_id');
            phase = NaN(size(data.date));
            for i = 1 : length(u_td_in_states)
                sub_states_this_td = st_data('from-idx', trading_state, td_in_states == u_td_in_states(i));
                if st_data('isempty-nl', sub_states_this_td)
                    continue;
                end
                idx_this_td_in_preftheo = td_in_preftheo == u_td_in_states(i);
                for j = 1 : length(sub_states_this_td.date)
                    phase(data.date >= sub_states_this_td.date(j) & idx_this_td_in_preftheo) = sub_states_this_td.value(j, 3); % MAGIC NUMBER : 3 for getting phase in trading state
                end
            end
            data = st_data('add-col', data, phase, 'phase');
        elseif isempty(vals)
            data = st_data('add-col', data, NaN(size(data.date)), 'phase');
        end
        % >
        
        % < adding info field
        data.info =  ...
            struct('security_id', sec_id, ...
            'security_key', sec_key, ...
            'td_info', tdi , ...
            'data_log', data.info.data_log, ...
            'data_datestamp', day_num, ...
            'trading_state', trading_state, ...
            'phase_dico',{transpose(cat(1,state_list,arrayfun(@(a)(a),phase_dico,'uni',false)))},...
            'GMT_offset', gmt_offset, ...
            'place_timezone_id', tdi(1).place_id, ...
            'localtime', true ) ; % TODO check with TP wether local time is true or not (Asia Ameri, etc...)
        % >
    otherwise
        error('get_prefixing_theoretic:exec', 'mode <%s> unknown', mode);
end
end

function [state_list, phase_dico] = trading_state_interpret()
% reference sur la signification de ces lettres http://padev012/SharedApplications/GL/GLPublisher.html
% dans la section sections: TRADING_PHASE:<PlatformName>
% au moment de l'�criture de ce code on avait : 
% variable 	description: liste s�par�e par des virgules des valeurs de TRADING_PHASE consid�r�es comme
% TR 	(Trading Regular)     <default value>
% TA 	(Trading AfterHours)
% TL 	(Trading at Last)
% TH 	(Trading after Hours)
%   	
% AO 	(Auction Opening) opening auction
% AR 	(Auction Resomption) auction de reprise sur suspension
% AC 	(Auction Closing) closing auction
% AI 	(Auction Intraday) intraday auction (xetra, sets expiry)
% AU 	(Auction Undetermined) auction
%   	
% CS 	(Closed Suspended) trading ferm� pour suspension
% CI 	(Closed Interdiction) trading ferm� pour indertiction de la valeur (�tat plus permanent que CS)
% CF 	(Closed Fixing) trading ferm� quelques secondes le temps du fixing
% CC 	(Closed Closed) trading ferm�, apr�s cl�ture
% CU 	(Closed Undefined)
% CP 	(Closed Preopen) trading ferm�, avant ouverture

persistent phase_id
if isempty(phase_id)
    [~,phase_id] = trading_time_interpret([],[]);
end

state_list = {'TR', 'TA', 'TL', 'TH', ...
    'AO', 'AR', 'AC', 'AI', 'AU', ...
    'CS', 'CI', 'CF', 'CC', 'CU', 'CP'};
phase_dico   = [phase_id.CONTINUOUS, phase_id.TRADING_AFTER_HOURS, phase_id.TRADING_AT_LAST, phase_id.TRADING_AFTER_HOURS, ...
    phase_id.OPEN_FIXING, phase_id.VOL_FIXING, phase_id.CLOSE_FIXING, phase_id.ID_FIXING2, phase_id.VOL_FIXING, ...
     phase_id.MARKET_CLOSED*ones(1, 6)];

end