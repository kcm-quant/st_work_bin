function data = get_price( mode, varargin)
% GET_PRICE - version de get_tick pour les indices
%
% internal uses:
% get_price('masterkey', 'pricefull', 'security_id', 'CAC40' )
% get_price('pricefull', 'security_id', 1, 'day', '20/10/2008' )
% get_price('index', 'security_id', 'CAC40', 'from', '20/10/2008', 'to', '21/10/2008'  )
%
% See also get_tick from_buffer read_dataset

switch lower(mode)
    case 'index'
         %<* intra day data
        opt   = options( { 'source', '',  'trading-destinations', {}, ...
            'initial-colnames', {}, 'final-colnames', {}, 'where_f-td', ''}, varargin);
        sec_id = opt.get('security_id');
        [a,sid,fname] = get_repository( 'index-key', sec_id);
        opt.set('security_id', sid);
        st_log('get_tick:data looking for data for <%s>...\n', fname);
        lst   = opt.get();
        data = from_buffer( 'get_price', 'pricefull', lst{:});
        %>
    case 'masterkey'
        %<* Return the generic+specific dir
        opt = options({'security_id', 110}, varargin(2:end));
        sec_id = opt.get('security_id');
        data = fullfile( varargin{1}, get_repository( 'index-key', sec_id));
        %>*
    case 'pricefull'
        %<* Read into the tickdb
        opt = options({'security_id', 1, 'day', '01/04/2008',...
            'date_format:char', 'dd/mm/yyyy' ...
            }, varargin);
        %< Lecture des options
        format_date = opt.get('date_format:char');
        sec_id = opt.get('security_id');
        [a,sec_id,index_name] = get_repository( 'index-key', sec_id);
        %         if ~isnumeric( sec_id) && numel(sec_id)==1
        %             error('get_tick:secid', 'I need an unique numerical index id');
        %         end
        day_ = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        %>
        this_day = datestr( day_, 'yyyymmdd');
        if day_<datenum(2010,4,29)
            nom_schema = 'tick_db';
        else
            nom_schema = get_basename4day( day_);
        end
        suffix_zone_code = get_repository('indice_id2global_zone_suffix',sec_id);
        if isempty(suffix_zone_code)
            error('get_price:no_data','No data for security_id = %d',sec_id)
        end
        vals = exec_sql('BSIRIUS', sprintf([ 'select '...
            '   convert(char(10),date,103)  , '...
            '    time , '...
            '   price '...
            '  from %s..indice%s where date = ''%s'' and indice_id=%d '], nom_schema,suffix_zone_code{1},this_day, sec_id));
        if ~isempty(vals)
            data = st_data('init', 'title', index_name, ...
                'date', datenum( vals(:,1), 'dd/mm/yyyy') + mod( datenum(vals(:,2), 'HH:MM:SS'),1), ...
                'value', cell2mat(vals(:,3)), ...
                'colnames', { 'price' }, ...
                'info', [] );
        else
            data = st_data('empty');
            data.title = index_name;
        end
        data.info.td_info.place_id = cell2mat(exec_sql('BSIRIUS', sprintf('select place_id from repository..indice where indice_id = %d', sec_id)));
        data.info.security_key = index_name;
end