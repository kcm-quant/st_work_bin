function varargout = get_repository( mode, varargin)
% GET_REPOSITORY
%
% %% corporate events
% 'cac'    s = get_repository('cac', 286, '20100901'); % get the list of corporate_action which imply a correction of the market data up to the date which is the third input format yyyymmdd
% 'dividend'
% 
% %% dates
% 'quotation_date'
% 'delisting_date'
% 
% %% perimeters
% 'quant_perimeter'
% 'quant_perimeter_tick'
% 'kech_coverage'
% 'brexit_range'
% 'td_id2sec_id'
% 'fixed_holidays'  fixed_holidays = get_repository('fixed_holidays', [datenum(2020,1,1),datenum(2021,12,31)]);
% 'holidays'        
%       cd = get_repository('holidays', [from,to]);
%       cd = get_repository('holidays', [from,to], td_id);
% 
% %% classifications
% 'capitalisation'
% 'style'
% 'sector'  r_vect = get_repository('sector', 'ICB_SuperSector', 110)
%           r_vect = get_repository('sector', 'kech_sector', 110)
%           r_vect = get_repository('sector', 'kech_analyst', 110)
% 
% %% references
% 'bloomberg'       get_repository('bloomberg', 'FP FP;RCF FP') % get info from bloomberg code
% {'ticker_bloomberg', 'sec_id2bbg'}   
%       latest_bbg = get_repository('sec_id2bbg', [110 276])
%       hist_bbg = get_repository('sec_id2bbg',[110,110,276,276],[datenum(2016,1,1),datenum(2021,6,30),datenum(2016,1,1),datenum(2021,6,30)])
% 'sec_id2ticker'   get_repository('sec_id2ticker', [110 45121]) % get ticker from security_id corresponding to main destination
% 'sec_and_td_id2ticker'
% 'bbg2bbg_quant'   tickers_quant = get_repository('bbg2bbg_quant',{'ABBN SE'; 'FP FP'; 'DBK GR';'ABBN VX'})
% 'bbg2sec_id'      bb = get_repository('bbg2sec_id',{'ABBN SW';'ABBN VX'}, {'20200120'; '20160101'})
% 'sec_id2name'     
%       latest_name = get_repository('sec_id2name', [276 11]) % get the name of the firm from the security_id
%       hist_name = get_repository('sec_id2name',[110,110,276,276],[datenum(2016,1,1),datenum(2021,6,30),datenum(2016,1,1),datenum(2021,6,30)])
% {'short_name','sec_id2short_name'}    short_name = get_repository('short_name',[2,110]);short_name = get_repository('short_name',{'AC FP','FTE FP','FTE FP'});
% 'sec_id2isin'
%       latest_isin = get_repository('sec_id2isin',[110,110,276,276]')
%       hist_isin = get_repository('sec_id2isin',[110,110,276,276]',[datenum(2016,1,1),datenum(2021,6,30),datenum(2016,1,1),datenum(2021,6,30)]')
% {'sedol_code','sec_id2sedol'}     
%       latest_sedol = get_repository('sedol_code', 110)
%       hist_sedol = get_repository('sec_id2sedol',[110,110,276,276],[datenum(2016,1,1),datenum(2021,6,30),datenum(2016,1,1),datenum(2021,6,30)])
% 'ric2sec_id'      get_repository('ric2sec_id', {'FTE.PA', 'TOTF.PA', 'blabla'}) % get security_id from RIC of the primary market
% 'sec_id2ric'      get_repository('sec_id2ric', [110 2]) % get RIC from security_id
% 'secid2security_id'
% 'sec_type' - OBSOLETE
%
% %% indices
% 'refindex'    get_repository('refindex','security_id',110,'ref_type','stoxx_sector','output_mode','name') % OBSOLETE
% 'index-comp'  get_repository('index-comp', 'CAC40') % get the components of an index See also get_index_comp
% 'index-list'
%
% %% trading destinations by security_id
% 'maintd'  
%       latest_main = get_repository('maintd', [2 6069 110]) % ->td_id of main td, ONLY WHEN THE STOCK IS STILL ACTIVE (otherwise ask maintd_last)
%       hist_main = get_repository('maintd',[110,110,276,276],[datenum(2016,1,1),datenum(2021,6,30),datenum(2016,1,1),datenum(2021,6,30)])
% 'maintd_last'             last_main = get_repository('maintd_last', [2 6069 110])
% 'maintd-zero-if-no-main'  get_repository('maintd-zero-if-no-main', [2 6069 110]) % ->td_id of main td
% 'security_id2timezone'
% 'security_id2global_zone_suffix'
% 'security-id'
% 'any-to-sec-td'
% { 'trading-destination', 'trading_destination', 'trading_destination_id','td', 'tdi' } get_repository( 'trading-destination', 110) - TO MIGRATE
% { 'trading-destination-info' , 'tdinfo' }     get_repository( 'tdinfo', 110) %TO MIGRATE
%
% %%  trading destinations info
% 'tdid2global_zone_suffix' get_repository('tdid2global_zone_suffix', 25) % suffixe pour les tables MARKET_DATA..trading_daily
% 'trading-destinations'    get_repository( 'trading-destinations')
% 'td_id2td_name'   get_repository('td_id2td_name', 4)
% 'td_id2td_name-unknown-if-zero'
% 'exchgid2trading_destination_id'
% 'place_id2timezone' - OBSOLETE
% 'td_id2exchgid'
% 
% %% keys (bufferization)
% 'kech-security-key'
% 'kech-security-td-key'
% 'security-td-key' get_repository( 'security-td-key', 'security_id', 'VOD.L')
% 'security-key'    get_repository( 'security-key', 110)
% 
% %% currency
% 'sec_currency'    get_repository('sec_currency', 110)
% {'currency','currency_id'}
% 'sec_id_rate2ref' get_repository('sec_id_rate2ref','security_id',[10735 110],'from','01/09/2012','to','06/09/2012')
% 
% %% others
% 'flextradeticker2sec_id' - TO MIGRATE
%
% See also from_buffer read_dataset

global st_version;
rdb = st_version.bases.repository;
repository = st_version.bases.repository;

if length(mode)>1 && mode(1)=='-'
    mode(1) = [];
end

switch lower( mode)
%% category: corporate events
    case {'cac', 'coporate_action_corrections'}
        if length(varargin{1}) > 1
            % Case when there are more than one stock
            sec_str = sprintf('%d,',varargin{1});
            sec_str(end) = [];
            
%             tmp_legacy = generic_structure_request('KGR', sprintf(['select 730486+datediff(day, ''20000101'', ce.EVENTDATE) as "event_date", ' ...
%                 ' ceta.ADJUSTTYPE adjust_type, ceta.DIVIDEND as dividend, isnull(ceta.PRICERATIO, 1) as "price_ratio", isnull(ceta.VOLUMERATIO, 1) as "volume_ratio",' ...
%                 'ce.SCOPEID as security_id' ...
%                 ' from %s..CALEVENT ce,%s..CALEVENTTRADINGADJUST ceta ' ...
%                 ' where ce.SCOPEID in (%s) and ceta.EVENTID = ce.EVENTID and ce.EVENTDATE <= ''%s''' ...
%                 ' and ce.EVENTTYPE = 51 and ce.SCOPETYPE = 1 '...
%                 ' order by ce.EVENTDATE'], ...
%                 rdb, rdb, sec_str,varargin{2})); % varargin{2}
            
            tmp = generic_structure_request('KGR', sprintf(['select date as event_date, ' ...
                ' ''S'' adjust_type, NULL as dividend, isnull(F1_agg, 1) as "price_ratio", 1/isnull(F0, 1) as "volume_ratio",' ...
                ' security_id' ...
                ' from MARKET_DATA..CORP_ACTION_ADJUSTMENT_FACTOR ' ...
                ' where security_id in (%s) and date <= ''%s''' ...
                ' and F1_agg <> 1 '...
                ' order by date'], ...
                sec_str, varargin{2}));
            
            
            if ~isempty(tmp)
                
                tmp.event_date = cellfun(@datenum, tmp.event_date);
                
                [~,~,idx_sec] = unique(tmp.security_id);
                
                % sub-function definitions for aggregation
                date_fun = @(i)tmp.event_date(i);
                price_ratio_fun = @(i)tmp.price_ratio(i);
                volume_ration_fun = @(i)tmp.volume_ratio(i);
                dividend_fun = @(i)tmp.dividend(i);
                cum_price_ratio_fun = @(i)flipud(cumprod(flipud(tmp.price_ratio(i))));
                cum_volume_ratio_fun = @(i)flipud(cumprod(flipud(tmp.volume_ratio(i))));
                cum_dividend_fun = @(i)flipud(cumsum(flipud(tmp.dividend(i))));
                security_id_fun = @(i)tmp.security_id(i);
                adjust_type_fun = @(i)tmp.adjust_type(i);
                
                % Aggregation
                gr_idx = accumarray(idx_sec,(1:length(idx_sec))',[],@(i){sort(i)});
                vals = cellfun(@(i){[date_fun(i),price_ratio_fun(i),volume_ration_fun(i),dividend_fun(i),...
                    cum_price_ratio_fun(i),cum_volume_ratio_fun(i),cum_dividend_fun(i),...
                    security_id_fun(i)]},gr_idx);
                vals = vertcat(vals{:});
                adjust_type = accumarray(idx_sec,(1:length(idx_sec))',[],@(i){adjust_type_fun(i)});
                adjust_type = vertcat(adjust_type{:});
                
                % Format output
                varargout{1} = st_data('init', 'date',vals(:,1), 'colnames', ...
                    {'price_ratio', 'volume_ratio', 'dividend',...
                    'cum_price_ratio', 'cum_volume_ratio', 'cum_dividend','security_id'}, ...
                    'value',vals(:,2:end), 'rownames', adjust_type);
            else
                varargout{1} = st_data('empty-init');
            end
        else
%             tmp_legacy = generic_structure_request('KGR', sprintf(['select 730486+datediff(day, ''20000101'', ce.EVENTDATE) as "event_date", ' ...
%                 ' ceta.ADJUSTTYPE adjust_type, ceta.DIVIDEND as dividend, isnull(ceta.PRICERATIO, 1) as "price_ratio", isnull(ceta.VOLUMERATIO, 1) as "volume_ratio"' ...
%                 ' from %s..CALEVENT ce,%s..CALEVENTTRADINGADJUST ceta ' ...
%                 ' where ce.SCOPEID = %d and ceta.EVENTID = ce.EVENTID and ce.EVENTDATE <= ''%s''' ...
%                 ' and ce.EVENTTYPE = 51 and ce.SCOPETYPE = 1 '...
%                 ' order by ce.EVENTDATE'], ...
%                 rdb, rdb, varargin{1}, varargin{2}));
            
             tmp = generic_structure_request('KGR', sprintf(['select date as event_date, ' ...
                ' ''S'' adjust_type, NULL as dividend, isnull(F1_agg, 1) as "price_ratio", 1/isnull(F0, 1) as "volume_ratio",' ...
                ' security_id' ...
                ' from MARKET_DATA..CORP_ACTION_ADJUSTMENT_FACTOR ' ...
                ' where security_id = %d and date <= ''%s''' ...
                ' and F1_agg <> 1 '...
                ' order by date'], ...
                varargin{1}, varargin{2}));
           
            
            if ~isempty(tmp)
                
                tmp.event_date = cellfun(@datenum, tmp.event_date);
                
                varargout{1} = st_data('init', 'date', tmp.event_date, 'colnames', ...
                    {'price_ratio', 'volume_ratio', 'dividend',...
                    'cum_price_ratio', 'cum_volume_ratio', 'cum_dividend'}, ...
                    'value', [tmp.price_ratio tmp.volume_ratio tmp.dividend , ...
                    flipud(cumprod(flipud(tmp.price_ratio))) ...
                    flipud(cumprod(flipud(tmp.volume_ratio))) ...
                    flipud(cumsum(flipud(tmp.dividend)))], 'rownames', tmp.adjust_type);
            else
                varargout{1} = st_data('empty-init');
            end
        end
        
    case {'dividend'}
        str = sprintf('%d,',unique(varargin{1}));
        str(end) = [];
        tmp = generic_structure_request('KGR', sprintf([...
            'select 730486+datediff(day, ''20000101'', ce.EVENTDATE) as event_date,ce.SCOPEID as security_id,',...
            'ceta.ADJUSTTYPE adjust_type,',...
            'ceta.DIVIDEND as dividend,',...
            'sum(log(ceta.DIVIDEND)) over(partition by ce.SCOPEID order by ce.EVENTDATE desc rows between unbounded preceding and current Row) as rev_cumsum_log_dividend,',...
            'isnull(ceta.PRICERATIO, 1) as price_ratio,',...
            'sum(log(isnull(ceta.PRICERATIO, 1))) over(partition by ce.SCOPEID order by ce.EVENTDATE desc rows between unbounded preceding and current Row) as rev_cumsum_log_price_rat,',...
            'isnull(ceta.VOLUMERATIO, 1) as volume_ratio,',...
            'sum(log(isnull(ceta.VOLUMERATIO, 1))) over(partition by ce.SCOPEID order by ce.EVENTDATE desc rows between unbounded preceding and current Row) as rev_cumsum_log_volume_rat',...
            ' from %s..CALEVENT ce,%s..CALEVENTTRADINGADJUST ceta',...
            ' where ce.SCOPEID in (%s)',...
            ' and ceta.EVENTID = ce.EVENTID',...
            ' and ce.EVENTDATE <= ''%s''',...
            ' and ce.EVENTTYPE = 52',...
            ' and ce.SCOPETYPE = 1',...
            ' order by ce.SCOPEID,ce.EVENTDATE'], ...
            rdb, rdb, str, varargin{2}));
        if ~isempty(tmp)
            varargout{1} = st_data('init', 'date', tmp.event_date, 'colnames', ...
                {'price_ratio', 'volume_ratio', 'dividend',...
                'cum_price_ratio', 'cum_volume_ratio', 'cum_dividend','security_id'}, ...
                'value', [tmp.price_ratio tmp.volume_ratio tmp.dividend , ...
                exp(tmp.rev_cumsum_log_price_rat),exp(tmp.rev_cumsum_log_volume_rat),...
                exp(tmp.rev_cumsum_log_dividend),tmp.security_id], 'rownames', tmp.adjust_type);
        else
            varargout{1} = st_data('empty-init');
        end
          
%% category: dates
    case 'quotation_date'
        query = ['select s.security_id, min(s.begin_date) from KGR..HISTO_SECURITY_QUANT s '...
            'where security_id in (%s) group by security_id'];
        sec = varargin{1};
        sec_str = join(',', unique(sec(~isnan(sec))));
        result = exec_sql('KGR', sprintf(query, sec_str));
        dt = cellfun(@(c) datenum(c,'yyyy-mm-dd'), result(:, 2));
        if ~isempty(result)
            varargout{1} = joint(cell2mat(result(:, 1)), dt, sec(:));
        else
            varargout{1} = joint(nan, {''}, sec(:));
        end
        
     case 'delisting_date'
        query = ['select s.security_id, max(s.end_date) from KGR..HISTO_SECURITY_QUANT s '...
            'where security_id in (%s) group by security_id'];
        sec = varargin{1};
        sec_str = join(',', unique(sec(~isnan(sec))));
        result = exec_sql('KGR', sprintf(query, sec_str));
        dt = cellfun(@(c) datenum(c,'yyyy-mm-dd'), result(:, 2));
        if ~isempty(result)
            varargout{1} = joint(cell2mat(result(:, 1)), dt, sec(:));
        else
            varargout{1} = joint(nan, {''}, sec(:));
        end
        
%% category: perimeters
    case 'quant_perimeter'
        if nargin>1
            from = datestr(varargin{1},'yyyymmdd');
            sec = exec_sql_table('KGR',sprintf('select distinct security_id from KGR..HISTO_SECURITY_QUANT where end_date >=''%s''',from));
        else
            sec = exec_sql_table('KGR','select distinct security_id from KGR..HISTO_SECURITY_QUANT where end_date =''29991231''');
        end
        sec = sec.security_id;
        varargout = { sec};
       
    case 'level_1_perimeter'
        if nargin>1
            from = datestr(varargin{1},'yyyymmdd');
            to = datestr(varargin{2},'yyyymmdd');
            sec = exec_sql_table('KGR',sprintf('select distinct security_id from KGR..HISTO_QUANT_PERIMETER where level = 1 and DATE >=''%s'' and DATE<=''%s'' order by security_id',from,to));
        else
            sec = exec_sql_table('KGR','select distinct security_id from KGR..QUANT_PERIMETER where level = 1 order by security_id');
        end
        sec = sec.security_id;
        varargout = { sec};

     case 'level_2_perimeter'
        if nargin>1
            from = datestr(varargin{1},'yyyymmdd');
            to = datestr(varargin{2},'yyyymmdd');
            sec = exec_sql_table('KGR',sprintf('select distinct security_id from KGR..HISTO_QUANT_PERIMETER where level = 2 and DATE >=''%s'' and DATE<=''%s'' order by security_id',from,to));
        else
            sec = exec_sql_table('KGR','select distinct security_id from KGR..QUANT_PERIMETER where level = 2 order by security_id');
        end
        sec = sec.security_id;
        varargout = { sec};

    case 'quant_perimeter_nb_changes'
        gz = varargin{1}; 
        if strcmpi(gz,'Europe')
            gz = 1;
        elseif strcmpi(gz,'US')
            gz = 2;
        end
        nb = exec_sql_table('KGR',sprintf(['select l.last_in_date, Count(q.security_id) n from KGR..HISTO_SECURITY_QUANT q '...
            'inner join (select distinct last_in_date from KGR..HISTO_SECURITY_QUANT where last_in_date >= dateadd (day, -20, getdate())) l '...
            'on l.last_in_date >= q.begin_date and l.last_in_date <= q.last_in_date '...
            'where q.primary_trading_destination_id in (select trading_destination_id from KGR..EXCHANGE_QUANT where GLOBALZONEID = %d) '...
            'group by l.last_in_date order by 1'], gz));
        varargout = { nb};
        
    case 'quant_perimeter_tick'
        td_remove = [8,15,49,80,102,103,135,163,220];
        td_rm = sprintf('%d,',td_remove);
        td_rm = td_rm(1:end-1);
        if nargin>1
            from = datestr(varargin{1},'yyyymmdd');
            sec = exec_sql_table('KGR',...
                sprintf('select distinct security_id from KGR..HISTO_SECURITY_QUANT where end_date >=''%s'' and primary_trading_destination_id not in (%s)',from,td_rm));
        else
            sec = exec_sql_table('KGR',...
                sprintf('select distinct security_id from KGR..HISTO_SECURITY_QUANT where end_date =''29991231'' and primary_trading_destination_id not in (%s)',td_rm));
        end
        sec = sec.security_id;
        varargout = { sec};

    case 'kech_coverage'
        if nargin>1
            from = datestr(varargin{1},'yyyymmdd');
            sec = exec_sql_table('KGR',sprintf('select r.security_id from KGR..HISTO_SECURITY_QUANT_REFERENCE r where r.reference_type_id = 7 and r.end_date >=''%s'' ',from));
        else
            sec = exec_sql_table('KGR','select r.security_id from KGR..HISTO_SECURITY_QUANT_REFERENCE r where r.reference_type_id = 7 and r.end_date = ''29991231'' ');
        end
        sec = sec.security_id;
        varargout = { sec};
        
    case 'brexit_range'
        security_id = varargin{1};
        EU_ISIN_PREFIX = ["AT","BE","BG","CY","CZ","DE","DK","EE","ES","FI",...
            "FR","GR","HR","HU","IE","IS","IT","LI","LT","LU","LV","MT","NL",...
            "NO","PL","PT","RO","SE","SI","SK"];
        
        isin = get_repository('sec_id2isin',security_id);
        isin(cellfun('isempty', isin)) = {''};
        isin = string(isin);
        
        range = repmat({'No isin'}, length(security_id), 1);
        idx_eu = ismember(cellfun(@(x)x(1:min(2, end)), isin, 'uni', false), EU_ISIN_PREFIX);
        range(idx_eu) = {'EU'};
        range(~cellfun('isempty', isin)&~idx_eu) = {'UK'};
        
        spec_EEA = {'GB0059822006','LU1598757687','GB00B03MLX29','FO0000000179','GB00BDSFG982'};
        range(ismember(isin, spec_EEA)) = {'EU'};
        varargout{1} = range;
        
    case 'td_id2sec_id'
        %         tdids = [1]
        tdids = varargin{1};
        ref_str = sprintf('%d,',unique(tdids(:)));
        ref_str(end) = [];
        
        data = exec_sql('KGR',...
            sprintf(['select distinct security_id from KGR..HISTO_SECURITY_QUANT '...
            'where primary_trading_destination_id in (%s) and end_date = ''29991231'' '], ref_str));
        security_id_output = cell2mat(data);
        
        varargout = {security_id_output};
        
    case 'fixed_holidays'
        fromto = varargin{1};
        ys = year(fromto(1)):year(fromto(2));
        hd = [];
        for s = 1:length(ys)
            hd = [hd; datenum(ys(s),1,1);...
                datenum(ys(s),4,2);datenum(ys(s),4,5);...
                datenum(ys(s),5,3);datenum(ys(s),5,13);datenum(ys(s),5,24);datenum(ys(s),5,31);...
                datenum(ys(s),8,30);...
                datenum(ys(s),12,24);datenum(ys(s),12,25);datenum(ys(s),12,28);datenum(ys(s),12,31)];
        end
        varargout = {hd};

    case 'holidays'
        fromto = varargin{1};
        if length(varargin)==1
            hd = exec_sql_table('KGR',sprintf('select * from KGR..TRADINGBUSINESSCALENDAR where CLOSEDDAY between ''%s'' and ''%s''',...
                datestr(fromto(1),'yyyymmdd'),datestr(fromto(2),'yyyymmdd')));
            if isempty(hd)
                clear cd
                hd.CLOSEDDAY = [];
                hd.closedday_num = [];
                hd.EXCHANGE = [];
            else
                hd.closedday_num = cellfun(@(c) datenum(c,'yyyy-mm-dd'), hd.CLOSEDDAY);
            end
        else
            td_id = varargin{2};
            from = fromto(1);
            to = fromto(2);
            hd = exec_sql_table('KGR', "select td_id=c.SCOPEID, c.EVENTDATE"+...
                " from KGR..CALEVENTTYPE t,"+...
                "KGR..CALEVENT c where t.EVENTTYPE = c.EVENTTYPE"+...
                sprintf(" and c.EVENTDATE between '%s' and '%s'",datestr(from, 'yyyy-mm-dd'),...
                datestr(to, 'yyyy-mm-dd'))+...
                " and SHORTNAME='market_closed'"+...
                " and c.SCOPETYPE='2'"+...
                sprintf(" and c.SCOPEID in (%s)", join_malas(',', unique(td_id))));
            if ~isempty(hd)
                hd.date = datetime(hd.EVENTDATE,'InputFormat','uuuu-MM-dd');
            end
        end
        varargout = {hd};

%% category: classifications
    case 'capitalisation'
        opts = options({'type', 'static'... 'dynamic'
            }, varargin(2:end));
        if strcmpi(opts.get('type'),'static')
            sec = varargin{1};
            capi = cell(length(sec),1);
            tmi = get_repository('index-comp','STOXX Europe Total Market Index'); %get_index_comp
            sxxp = get_repository('index-comp','DJ STOXX 600');
            lms = {get_repository('index-comp','DJ STOXX LARGE 200'),get_repository('index-comp','DJ STOXX MID 200'),get_repository('index-comp','DJ STOXX SMALL 200')};
            capi(ismember(sec, lms{1})) = {'LARGE'};
            capi(ismember(sec, lms{2})) = {'MID'};
            capi(ismember(sec, lms{3})) = {'SMALL'};
            capi(ismember(sec, tmi) & ~ismember(sec, sxxp)) = {'SMALLEST'};
            capi(~ismember(sec, tmi)) = {'SMALLEST ex TMI'};
            
        elseif strcmpi(opts.get('type'),'dynamic')
            sec_date = varargin{1};
            tmi = membership_vect('BKXP',sec_date);
            capi = cell(length(sec_date),1);
            v = membership_vect('LCXP',sec_date);
            capi(v>0) = {'LARGE'};
            v = membership_vect('MCXP',sec_date);
            capi(v>0) = {'MID'};
            v = membership_vect('SCXP',sec_date);
            capi(v>0) = {'SMALL'};
            capi(tmi>0 & cellfun(@isempty, capi)) = {'SMALLEST'};
            capi(~(tmi>0) & cellfun(@isempty, capi)) = {'SMALLEST ex TMI'};
            
        end
        varargout = {capi};
        
    case 'style'
        opts = options({'type', 'static'... 'dynamic'
            }, varargin(2:end));
        if strcmpi(opts.get('type'),'static')
            sec = varargin{1};
            styl = cell(length(sec),1);
            tmi = get_repository('index-comp','STOXX Europe Total Market Index'); %get_index_comp
            gv = {get_repository('index-comp','STOXX Europe TMI Value'),get_repository('index-comp','STOXX Europe TMI Growth')};
            styl(ismember(sec, gv{1})) = {'VALUE'};
            styl(ismember(sec, gv{2})) = {'GROWTH'};
            styl(ismember(sec, tmi) & ~ismember(sec, [gv{1};gv{2}])) = {'OTHERS'};
            styl(~ismember(sec, tmi)) = {'OTHERS ex TMI'};
            
        elseif strcmpi(opts.get('type'),'dynamic')
            sec_date = varargin{1};
            tmi = membership_vect('BKXP',sec_date);
            styl = cell(length(sec_date),1);
            v = membership_vect('STGP',sec_date);
            styl(v>0) = {'GROWTH'};
            v = membership_vect('STVP',sec_date);
            styl(v>0) = {'VALUE'};
            styl(tmi>0 & cellfun(@isempty, styl)) = {'OTHERS'};
            styl(~(tmi>0) & cellfun(@isempty, styl)) = {'OTHERS ex TMI'};
            
        end
        varargout = {styl};

    case 'sector'
        if nargin==2
            varargin = { 'ICB_SuperSector', varargin{1}}; %stoxx_sectorial_index
        end
        tickerref_cols = {'ICB_Industry','ICB_INDUSTRY_NAME';...
            'ICB_SuperSector','ICB_SUPERSECTOR_NAME';...
            'ICB_Sector','ICB_SECTOR_NAME';...
            'ICB_SubSector','ICB_SUBSECTOR_NAME'};
        tickerref_col = joint(tickerref_cols(:,1),tickerref_cols(:,2),varargin{1});
        sec = varargin{2};
        if length(sec)>1
            sector_data = arrayfun(@(c) get_repository('sector', varargin{1}, c), sec, 'uni', 0);
            % sector_data = vertcat(sector_data{:});
            varargout = { sector_data };
        else
            switch lower( varargin{1})
                case 'stoxx_sectorial_index'
                    vals = exec_sql( 'KGR', sprintf( ['select INDEXID, INDEXNAME from KGR..[INDEX] ',...
                        'where INDEXID in (select distinct INDEXID from KGR..indice_component where security_id = %d) ',...
                        'and INDEXNAME like ''%% PRICE EUR'' '] , varargin{2}) );
                    if isempty(vals)
                        varargout = { struct('index_id', [], 'index_name',[]) };
                    else
                        varargout = { struct('index_id', vals{1,1}, 'index_name',vals{1,2}) };
                    end
                    return
                    
                case 'kech_sector'
                    vals = exec_sql( 'KGR', sprintf( ['select c.classification_code from KGR..HISTO_SECURITY_QUANT_CLASSIFICATION c '...
                        'where security_id = %d and c.classification_type_id = 5 and c.end_date = ''29991231'' '], varargin{2}, varargin{1}));
                    if isempty(vals);varargout = { '' };else;varargout = vals;end
                    
                case 'kech_analyst'
                    vals = exec_sql( 'KGR', sprintf( ['select c.classification_code from KGR..HISTO_SECURITY_QUANT_CLASSIFICATION c '...
                        'where security_id = %d and c.classification_type_id = 6 and c.end_date = ''29991231'' '], varargin{2}, varargin{1}));
                    if isempty(vals);varargout = { '' };else;varargout = vals;end
                    
                otherwise
                    if strcmpi(varargin{1}, 'GICS_short')
                        sect_type = 'GICS_SECTOR';
                        short_sect_names = 1;
                    else
                        sect_type = varargin{1};
                        short_sect_names = 0;
                    end
                    vals = exec_sql( 'KGR', sprintf( ['select o.name from KGR..HISTO_SECURITY_QUANT_CLASSIFICATION c, '...
                        'KGR..SECURITY_QUANT_CLASSIFICATION_CODE o, KGR..SECURITY_QUANT_CLASSIFICATION_TYPE t '...
                        'where security_id = %d and c.classification_code = o.classification_code '...
                        'and c.classification_type_id = t.classification_type_id and o.classification_type_id = t.classification_type_id '...
                        'and end_date = ''29991231'' and t.name = ''%s'' '], varargin{2}, sect_type));

                    if short_sect_names
                        GICS_map = {'Communication Services','Comm Svcs';....
                            'Consumer Staples','Cons Staples';....
                            'Consumer Discretionary','Cons Discr';...
                            'Information Technology','Info Tech'};
                        uvals = unique(vals);
                        unchanged_sect = uvals(~ismember(uvals, GICS_map(:,1)));
                        GICS_map = [GICS_map; [unchanged_sect, unchanged_sect]];
                        vals = joint(GICS_map(:,1),GICS_map(:,2),vals);
                    end

                    if isempty(vals) % & ~isempty(tickerref_col{1})
%                         if ~isempty(regexp(tickerref_col{1},'ICB'))
%                             add_str = 'and BEGINDATE >=''20210401'' ';
%                         else
%                             add_str = ' ';
%                         end
%                         vals = exec_sql( 'KGR', sprintf( ['select distinct %s from KGR..TICKERREF t '...
%                             'where security_id = %d and ENDDATE is null %s'], tickerref_col{1}, varargin{2}, add_str)); 
%                         if length(vals)>1;vals = [];end
%                         if isempty(vals);varargout = { '' };else;varargout = vals;end
%                     elseif isempty(vals)
                        varargout = { '' };
                    else
                        varargout = vals;
                    end
            end
        end
                    
%% category: references
    case 'bloomberg'
        %<* Security id from Bloomberg code
        bcodes = tokenize(varargin{1});
        bcodes  = bbg_suffix_map(bcodes);
        f_ref = @(s,t,b,n)struct('security_id', s, 'trading_destination_id', t, 'bloomberg', b, 'name', n);
        ref   = f_ref({},{},{},{});
        for bcode = bcodes
            bcode = upper(strtrim(bcode{1}));
            vals = exec_sql( 'KGR', sprintf( ...
                [ 'select s.security_id, s.primary_trading_destination_id, r.reference, s.SECNAME from KGR..HISTO_SECURITY_QUANT s, KGR..HISTO_SECURITY_QUANT_REFERENCE r '...
                'where s.primary_trading_destination_id = r.trading_destination_id and s.security_id = r.security_id '...
                'and r.reference_type_id = 4 '...
                'and s.end_date = ''29991231'' and r.end_date = ''29991231'' '...
                'and r.reference  = ''%s'' '],  bcode));
            if isempty(vals)
                st_log('\nNot reference for <%s> as BLOOMBERG code...\n', bcode);
            else
                ref = cat(1,ref, f_ref(vals(:,1),vals(:,2),vals(:,3),vals(:,4)));
            end
        end
        if ~isempty( ref)
            st_log('\nReferences for <%s> as BLOOMBERG code:\n-----\n', varargin{1});
            st_log('%12s  %5s %12s    %s\n', 'sec id', 'td id', 'Code', 'Name');
            for r=1:length(ref)
                st_log('%12d  %5d %12s    %s\n', ref(r).security_id, ref(r).trading_destination_id, ref(r).bloomberg, ref(r).name);
            end
        end
        varargout = { ref};
        %>*
     
    case {'ticker_bloomberg', 'sec_id2bbg'}
        sec_id = varargin{1};
        [un_sec,~,idx_sec] = unique(sec_id);
        sec_nn = un_sec(~isnan(un_sec));
        sec_str = sprintf('%d,',sec_nn);
        
        if length(varargin)==1
            res = exec_sql('KGR',['select r.security_id, r.reference '...
                'from KGR..HISTO_SECURITY_QUANT s,  KGR..HISTO_SECURITY_QUANT_REFERENCE r '...
                'where s.primary_trading_destination_id = r.trading_destination_id and s.security_id = r.security_id '...
                'and s.end_date = ''29991231'' and r.end_date = ''29991231''  and r.reference_type_id = 4 '...
                'and r.security_id in (',sec_str(1:end-1),')']);
            [~,a,b] = intersect(un_sec,cell2mat(res(:,1)));
            sec_ticker = cell(length(un_sec),1);
            sec_ticker(a) = res(b,2);
            varargout{1} = sec_ticker(idx_sec);
%         else % dynamic / UNDER DEVELOPMENT
%             dates = varargin{2};
%             res = exec_sql_table('KGR',['select r.security_id, r.reference, r.begin_date, r.end_date '...
%                 'from KGR..HISTO_SECURITY_QUANT s,  KGR..HISTO_SECURITY_QUANT_REFERENCE r '...
%                 'where s.primary_trading_destination_id = r.trading_destination_id and s.security_id = r.security_id '...
%                 'and s.begin_date>=r.begin_date and s.end_date<=r.end_date and r.reference_type_id = 4 '...
%                 'and r.security_id in (',sec_str(1:end-1),') order by r.begin_date']);
            
        else
            dates = varargin{2};
            
            res = exec_sql_table('KGR',['select distinct r.security_id, r.begin_date, r.end_date, r.reference '...
                'from KGR..HISTO_SECURITY_QUANT_REFERENCE r, KGR..HISTO_SECURITY_QUANT s   '...
                'where s.primary_trading_destination_id = r.trading_destination_id and s.security_id = r.security_id '...
                'and s.end_date <= r.end_date and r.reference_type_id = 4 '... %and s.begin_date >= r.begin_date 
                'and r.security_id in (',sec_str(1:end-1),') '...
                'order by r.security_id, r.begin_date']);
            res.begin_date_num = cellfun(@(c) datenum(c,'yyyy-mm-dd'), res.begin_date);
            res.end_date_num = cellfun(@(c) datenum(c,'yyyy-mm-dd'), res.end_date);
            
            ix = arrayfun(@(v) find(res.security_id==sec_id(v) & res.begin_date_num<=dates(v) & res.end_date_num>dates(v)), (1:length(sec_id))');
            if size(sec_id,2)>1
            varargout{1} = res.reference(ix)';
            else
            varargout{1} = res.reference(ix);
            end
                        
        end
        
    case 'sec_id2ticker' %  security_id to ticker corresponding to main destination
        sec_ids = varargin{1};
        td_ids=nan(size(sec_ids));
        for i=1:length(sec_ids)
            kode = get_repository( 'any-to-sec-td', 'security_id',sec_ids(i),'trading-destinations',{'MAIN'});
            td_ids(i)=kode.trading_destination_id;
        end
        varargout = {get_repository('sec_and_td_id2ticker',sec_ids,td_ids)};
      
    case 'sec_and_td_id2ticker'
        %<* Bloomberg code from security_id and trading_destination_id
        sec_ids = varargin{1};
        if nargin>1
            td_ids=varargin{2};
            if ~all(size(sec_ids)==size(td_ids));
                throw(MException('st_repository:size', 'security_ids and td_ids arrays must have the same size'));
            end
        else throw(MException('st_repository:data', 'Please provide security ids AND trading_destination ids'));
        end
        f_ref = @(s,t,b,n)struct('security_id', s, 'trading_destination_id', t, 'bloomberg', b, 'name', n);
        ref   = f_ref({},{},{},{});
        for i = 1:length(sec_ids)
            sec_id=sec_ids(i);
            td_id=td_ids(i);
            vals = exec_sql( 'KGR', sprintf( ...
                [ 'select r.security_id, r.trading_destination_id, r.reference, s.SECNAME '...
                'from KGR..HISTO_SECURITY_QUANT s,  KGR..HISTO_SECURITY_QUANT_REFERENCE r '...
                'where s.security_id = r.security_id '...
                'and s.end_date = ''29991231'' and r.end_date = ''29991231''  and r.reference_type_id = 4' ...
                'and r.security_id = %d and r.trading_destination_id = %d'],  sec_id,td_id));
            
            if isempty(vals)
                st_log('\nNot reference for security_id %d ...\n', sec_id);
            else
                ref = cat(1,ref, f_ref(vals(:,1),vals(:,2),vals(:,3),vals(:,4)));
            end
        end
        if ~isempty( ref)
            st_log('%12s  %5s %12s    %s\n', 'sec id', 'td id', 'Code', 'Name');
            for r=1:length(ref)
                st_log('%12d  %5d %12s    %s\n', ref(r).security_id, ref(r).trading_destination_id, ref(r).bloomberg, ref(r).name);
            end
        end
        varargout = { ref};
        %>*
        
    case 'bbg2bbg_quant'
        if ~iscell(varargin{1})
            bcodes = tokenize(varargin{1});
        else
            bcodes = varargin{1};
        end
        bcodes = bbg_suffix_map(bcodes);
        varargout = {bcodes};
        
    case 'bbg2sec_id'
        if ~iscell(varargin{1})
            bcodes = tokenize(varargin{1});
        else
            bcodes = varargin{1};
        end
        bcodes  = bbg_suffix_map(bcodes);
        ref = NaN(size(bcodes));
        if nargin==2
            for t = 1:length(bcodes)
                bcode = upper(strtrim(bcodes{t}));
                vals = cell2mat(exec_sql( 'KGR', sprintf( ...
                    [ 'select distinct r.security_id from KGR..HISTO_SECURITY_QUANT_REFERENCE r '...
                    'where r.reference_type_id = 4 '...
                    'and r.end_date = ''29991231'' '...
                    'and r.reference  = ''%s'' '],  bcode)));
                if ~isempty(vals)
                    ref(t) = vals;
                end
            end
        else
            dates = varargin{2};
            for t = 1:length(bcodes)
                bcode = upper(strtrim(bcodes{t}));
                vals = cell2mat(exec_sql( 'KGR', sprintf( ...
                    [ 'select distinct r.security_id from KGR..HISTO_SECURITY_QUANT_REFERENCE r '...
                    'where r.reference_type_id = 4 '...
                    'and r.begin_date <= ''%s'' and r.end_date >= ''%s'' '...
                    'and r.reference  = ''%s'' '],  dates{t},  dates{t}, bcode)));
                if ~isempty(vals) & length(vals)==1
                    ref(t) = vals;
                end
            end
        end
        varargout = {ref};
        
    case 'sec_id2name' % get the firm name from a security_id
        sec = varargin{1};
        sec_str = join(',', unique(sec(~isnan(sec))));
        
        if length(varargin)==1
            query = ['select s.security_id, s.SECNAME from KGR..HISTO_SECURITY_QUANT s '...
                'where s.end_date = ''29991231''  and security_id in (%s)'];
            result = exec_sql('KGR', sprintf(query, sec_str));
            if ~isempty(result)
                varargout{1} = joint(cell2mat(result(:, 1)), result(:, 2),...
                    sec(:));
            else
                varargout{1} = joint(nan, {''}, sec(:));
            end
            
        else
            dates = varargin{2};
            
            res = exec_sql_table('KGR',['select r.security_id, r.begin_date, r.end_date, r.SECNAME '...
                'from KGR..HISTO_SECURITY_QUANT r   '...
                'where r.security_id in (',sec_str,') '...
                'order by r.security_id, r.begin_date']);
            res.begin_date_num = cellfun(@(c) datenum(c,'yyyy-mm-dd'), res.begin_date);
            res.end_date_num = cellfun(@(c) datenum(c,'yyyy-mm-dd'), res.end_date);
            
            ix = arrayfun(@(v) find(res.security_id==sec(v) & res.begin_date_num<=dates(v) & res.end_date_num>dates(v)), 1:length(sec));
            if size(sec,2)>1
                varargout{1} = res.SECNAME(ix)';
            else
                varargout{1} = res.SECNAME(ix);
            end
        end
        
    case {'short_name','sec_id2short_name'}
        %TODO: cas ou varargin{1} n'est pas numerique
        if isnumeric(varargin{1})
            sec_id = varargin{1};
            [un_sec,~,idx_sec] = unique(sec_id);
            sec_str = sprintf('%d,',un_sec);
            % Table Bloomberg
            bbg = get_repository('ticker_bloomberg',un_sec);
            bbg_str = sprintf('''%s'',',bbg{:});
            res_bloom = exec_sql('KGR',['select ref.CODEBLOOMBERG,ref.SHORT_NAME',...
                ' from KGR..TICKERREF ref',...
                ' where ref.CODEBLOOMBERG in (',bbg_str(1:end-1),')',...
                ' and ref.ENDDATE is null ']);
            [~,a,b] = intersect(un_sec',joint(bbg, un_sec', res_bloom(:,1)));
            sec_short_name_bloom = cell(length(un_sec),1);
            sec_short_name_bloom(a) = res_bloom(b,2);
            % Table Security
            res_sec = exec_sql('KGR',['select security_id,SECNAME',...
                ' from KGR..HISTO_SECURITY_QUANT ',...
                ' where security_id in (',regexprep(sec_str(1:end-1),'([^,]*)','''$1'''),')',...
                ' and end_date = ''29991231''']);
            [~,a,b] = intersect(un_sec,cell2mat(res_sec(:,1)));
            sec_short_name_sec = cell(length(un_sec),1);
            sec_short_name_sec(a) = res_sec(b,2);
            % Overloading
            sec_short_name = sec_short_name_sec;
            sec_short_name(~cellfun('isempty',sec_short_name_bloom)) = ...
                sec_short_name_bloom(~cellfun('isempty',sec_short_name_bloom));
            varargout{1} = sec_short_name(idx_sec);
        else
            error('get_repository:short_name','bad argument type');
        end
     
    case 'sec_id2isin'
        secids = varargin{1};
        ref_str = sprintf('%d,',unique(secids(:)));
        ref_str(end) = [];
        if length(varargin)==1
            data = exec_sql('KGR',...
                sprintf(['select s.ISIN,s.security_id from KGR..HISTO_SECURITY_QUANT s '...
                'where s.security_id in (%s) and s.end_date = ''29991231'' '], ref_str));
            
            [un_secids,~,idx_secids] = unique(secids);
            [~,a,b] = intersect(un_secids,cell2mat(data(:,2)));
            isins = cell(length(secids),1);
            for i=1:length(a)
                isins(idx_secids==a(i)) = data(b(i),1);
            end
            varargout = {isins};
        else
            dates = varargin{2};
            
            res = exec_sql_table('KGR',['select r.security_id, r.begin_date, r.end_date, r.ISIN '...
                'from KGR..HISTO_SECURITY_QUANT r   '...
                'where r.security_id in (',ref_str,') '...
                'order by r.security_id, r.begin_date']);
            res.begin_date_num = cellfun(@(c) datenum(c,'yyyy-mm-dd'), res.begin_date);
            res.end_date_num = cellfun(@(c) datenum(c,'yyyy-mm-dd'), res.end_date);
            
            ix = arrayfun(@(v) find(res.security_id==secids(v) & res.begin_date_num<=dates(v) & res.end_date_num>dates(v)), 1:length(secids), 'uni',0);
            ix(cellfun(@isempty,ix)) = {NaN};
            ix = cell2mat(ix);
            if size(secids,2)>1
                varargout{1} = res.ISIN(ix)';
            else
                varargout{1} = res.ISIN(ix);
            end
        end

%     case 'isin2sec_id'
%         isins = varargin{1};
%         u_isins = unique(isins);
%         ref_str = sprintf('''%s'',',u_isins{:});
%         ref_str(end) = [];
%         if length(varargin)==1
%             data = exec_sql('KGR',...
%                 sprintf(['select s.ISIN,s.security_id from KGR..HISTO_SECURITY_QUANT s '...
%                 'where s.ISIN in (%s) and s.end_date = ''29991231'' '], ref_str));
%             
%             [un_isins,~,idx_isins] = unique(isins);
%             [~,a,b] = intersect(un_isins,data(:,1));
%             sec_ids = NaN(length(isins),1);
%             for i=1:length(a)
%                 sec_ids(idx_isins==a(i)) = cell2mat(data(b(i),2));
%             end
%             varargout = {sec_ids};
%         else
%             dates = varargin{2};
%             
%             res = exec_sql_table('KGR',['select r.security_id, r.begin_date, r.end_date, r.ISIN '...
%                 'from KGR..HISTO_SECURITY_QUANT r   '...
%                 'where r.ISIN in (',ref_str,') '...
%                 'order by r.security_id, r.begin_date']);
%             res.begin_date_num = cellfun(@(c) datenum(c,'yyyy-mm-dd'), res.begin_date);
%             res.end_date_num = cellfun(@(c) datenum(c,'yyyy-mm-dd'), res.end_date);
%             
%             ix = arrayfun(@(v) find(strcmpi(res.ISIN,isins{v}) & res.begin_date_num<=dates(v) & res.end_date_num>dates(v)), 1:length(isins), 'uni',0);
%             if size(isins,2)>1
%                 varargout{1} = res.security_id(ix)';
%             else
%                 varargout{1} = res.security_id(ix);
%             end
%         end

    case 'sec_id2ccy'
        secids = varargin{1};
        ref_str = sprintf('%d,',unique(secids(:)));
        ref_str(end) = [];
        if length(varargin)==1
            data = exec_sql('KGR',...
                sprintf(['select s.CCY,s.security_id from KGR..HISTO_SECURITY_QUANT s '...
                'where s.security_id in (%s) and s.end_date = ''29991231'' '], ref_str));
            
            [un_secids,~,idx_secids] = unique(secids);
            [~,a,b] = intersect(un_secids,cell2mat(data(:,2)));
            isins = cell(length(secids),1);
            for i=1:length(a)
                isins(idx_secids==a(i)) = data(b(i),1);
            end
            varargout = {isins};
        else
            dates = varargin{2};
            
            res = exec_sql_table('KGR',['select r.security_id, r.begin_date, r.end_date, r.CCY '...
                'from KGR..HISTO_SECURITY_QUANT r   '...
                'where r.security_id in (',ref_str,') '...
                'order by r.security_id, r.begin_date']);
            res.begin_date_num = cellfun(@(c) datenum(c,'yyyy-mm-dd'), res.begin_date);
            res.end_date_num = cellfun(@(c) datenum(c,'yyyy-mm-dd'), res.end_date);
            
            ix = arrayfun(@(v) find(res.security_id==secids(v) & res.begin_date_num<=dates(v) & res.end_date_num>dates(v)), 1:length(secids));
            if size(secids,2)>1
                varargout{1} = res.CCY(ix)';
            else
                varargout{1} = res.CCY(ix);
            end
        end

    case {'sedol_code','sec_id2sedol'}
        sec_id = varargin{1};
        [un_sec,~,idx_sec] = unique(sec_id);
        sec_str = sprintf('%d,',un_sec);
        
        if length(varargin)==1
            res = exec_sql('KGR',['select s.security_id, s.SEDOL '...
                'from KGR..HISTO_SECURITY_QUANT s '...
                'where s.security_id in (',sec_str(1:end-1),') '...
                'and s.end_date = ''29991231'' and s.SEDOL is not null']);
            [~,a,b] = intersect(un_sec,cell2mat(res(:,1)));
            sec_ticker = cell(length(un_sec),1);
            sec_ticker(a) = res(b,2);
            varargout{1} = sec_ticker(idx_sec);
        else
            dates = varargin{2};
            
            res = exec_sql_table('KGR',['select r.security_id, r.begin_date, r.end_date, r.SEDOL '...
                'from KGR..HISTO_SECURITY_QUANT r   '...
                'where r.security_id in (',sec_str(1:end-1),') '...
                'order by r.security_id, r.begin_date']);
            res.begin_date_num = cellfun(@(c) datenum(c,'yyyy-mm-dd'), res.begin_date);
            res.end_date_num = cellfun(@(c) datenum(c,'yyyy-mm-dd'), res.end_date);
            
            ix = arrayfun(@(v) find(res.security_id==sec_id(v) & res.begin_date_num<=dates(v) & res.end_date_num>dates(v)), 1:length(sec_id));
            if size(sec_id,2)>1
                varargout{1} = res.SEDOL(ix)';
            else
                varargout{1} = res.SEDOL(ix);
            end
        end
    
    case 'ric2sec_id' % renvoie le sec_id ? partir du RIC Reuters d'un titre
        req = ['select r.security_id, r.reference from KGR..HISTO_SECURITY_QUANT_REFERENCE r '...
            'where r.end_date = ''29991231''  and r.reference_type_id = 5 and r.reference in (%s) '];
        
        ric = varargin{1};
        ric_str = sprintf('''%s'',',ric{:});
        ric_str(end)=[];
        sec = NaN(size(ric));
        result = exec_sql('KGR', sprintf(req, ric_str));
        if ~isempty(result)
            [~,a,b] = intersect(result(:,2),ric);
            sec(b) = cell2mat(result(a,1));
        end
        varargout = {sec};
        
    case 'sec_id2ric' % renvoie le RIC Reuters d'un titre a partir de son id
        ric_req = ['select r.security_id, r.reference '...
            'from KGR..HISTO_SECURITY_QUANT s,  KGR..HISTO_SECURITY_QUANT_REFERENCE r '...
            'where s.primary_trading_destination_id = r.trading_destination_id and s.security_id = r.security_id '...
            'and s.end_date = ''29991231'' and r.end_date = ''29991231''  and r.reference_type_id = 5 '...
            'and r.security_id in (''%s'') '];
        
        sec = varargin{1};
        sec_str = sprintf('%d,',sec);
        sec_str(end)=[];
        ric = cell(size(sec));
        result = exec_sql('KGR', sprintf(ric_req, sec_str));
        if ~isempty(result)
            [~,a,b] = intersect(cell2mat(result(:,1)),sec);
            ric(b) = result(a,2);
        else
            ric = [];
        end
        varargout = {ric};
        
    case 'secid2security_id'
        if ischar(varargin{1})
            varargin{1} = varargin(1);
        end
        secid_str = ['''',join(''',''',varargin{1}),''''];
        secid_security_id = exec_sql('KGR',['select reference,security_id',...
            ' from KGR..HISTO_SECURITY_QUANT_REFERENCE',...
            sprintf(' where reference in (%s) and reference_type_id=2',secid_str)]);
        varargout{1} = joint(secid_security_id(:,1),cell2mat(secid_security_id(:,2)),varargin{1});
        
    case 'sec_type'
        error('This mode does not exist anymore. idc_security_type is not maintained. Please change your code.')

%% category: indices
    case 'refindex' %************************TO MIGRATE************************%
        mtd = get_repository('maintd', varargin{1});
        vals = exec_sql( 'KGR', sprintf( ['select ref.INDEXID, ind.INDEXNAME ',...
            'from KGR..BENCHMARKDEFAULTINDEX ref, KGR..[INDEX] ind '...
            'where ref.PRIMARYEXCHANGE = %d and ref.SECTORTYPE is null and ref.GLOBALZONEID is null ',...
            'and ref.INDEXID = ind.INDEXID '] , mtd));
        if isempty(vals)
            varargout = { };
        else
            varargout = { struct('index_id', vals{1,1}, 'index_name',vals{1,2}) };
        end
        
    case 'index-comp'
        if length(varargin)>2
            varg = varargin{3};
            varargin = varargin(1:2);
        elseif length(varargin)>1
            varg = varargin{2};
            varargin = varargin(1);
        else
            varg = {};
        end
        opt = options({'type','static',... % dynamic
            'recent_date_constraint', true, 'max_nb_months', 2, ...
            'source', 'indice_component', 'with_weights', 0}, varg);
        
        if strcmpi(opt.get('type'),'static')
            if length(varargin)>1 % static but past date
                %%% Methode pas tres propre pour r�cup�rer la composition d'un
                %%% indice a une date dans le passe
                %%% -> On recupere la requete de la vue 'indice_component' que
                %%%    l'on modifie "legerement"...
                
                index_name = varargin{1};
                assert(isnumeric(varargin{2}),'Second input argument should be numeric')
                date = varargin{2};
                
                idx_code = exec_sql('KGR',['select SYMBOL2 from KGR..[INDEX] where INDEXNAME = ''',index_name,'''']);
                assert(length(idx_code)==1,'Wrong INDEXNAME [first input argument]');
                compo_histo_1d = get_index_compo_histo(idx_code, date, date);
                sec_ids = cellfun(@str2num, compo_histo_1d.member_histo.colnames);

                % % Recherche de l'INDEXID correspondant au nom de l'indice en entree
                % index_id = exec_sql('KGR',['select INDEXID from KGR..[INDEX] where INDEXNAME = ''',index_name,'''']);
                % assert(length(index_id)==1,'Wrong INDEXNAME [first input argument]');
                % index_id = char(index_id);
                % 
                % str = char(exec_sql('KGR',['select s.text from KGR.sys.objects o,KGR.sys.syscomments s',...
                %     ' where o.name = ''indice_component''',...
                %     ' and o.object_id = s.id'])); % Script de creation de la vue 'indice_component'
                % 
                % % Ajout d'un filtre sur la date
                % rep_str = sprintf('select MAX(DATE) from KGR..INDEXCOMPONENT where INDEXID=ic.INDEXID and DATE <= ''%s''',...
                %     datestr(date,'yyyy-mm-dd'));
                % rep_str_regexp = sprintf('select MAX\\(DATE\\) from KGR\\.\\.INDEXCOMPONENT where INDEXID=ic\\.INDEXID and DATE <\\= \\''%s\\''',...
                %     datestr(date,'yyyy-mm-dd'));
                % str = strrep(str,...
                %     'select MAX(DATE) from KGR..INDEXCOMPONENT where INDEXID=ic.INDEXID',...
                %     rep_str); %
                % assert(~isempty(regexp(str,rep_str_regexp, 'once')),'Unable to make replace string')
                % str = regexprep(str,'[\r\n ]+CREATE +view +\[dbo\]\.\[indice_component\][\r\n ]+as[\r\n ]+','');
                % str = strcat(str,' and a.INDEXID = ''',index_id,'''');
                % 
                % res = exec_sql('KGR',str);
                % assert(all(cellfun(@isnumeric,res(:,2))),'View ''indice_component'' has changed. Review this code');
                % sec_ids = cell2mat(res(:,2));
                
            else
                
                % TODO: change column name in KGR..[INDEX] SYMBOL2=INDEXCODEBLOOMBERG
                i_id = exec_sql('KGR',['select INDEXID from ' repository '..[INDEX] where INDEXNAME = ''',varargin{1},''' or SYMBOL2= ''',varargin{1},''' ']);
                if length(i_id)>1
                    error('Several indices. Please check name or code as input.')
                end
                
                if opt.get('recent_date_constraint')
                    dat = cell2mat(exec_sql('KGR', ...
                        sprintf('select distinct convert(date,DATE) from %s..INDEXCOMPONENT_QUANT where INDEXID=''%s''', repository,i_id{1})));      
                    %                 indexcode = exec_sql('KGR', sprintf('select SYMBOL2 from KGR..[INDEX] where INDEXNAME = ''%s'' ', varargin{1}));
                    %                 add_dat = exec_sql('KGR', sprintf(['select max(upd_date) from '...
                    %                     ' KGR..additional_indice_component where INDEXCODEBLOOMBERG = ''%s'' '], indexcode{1}));
                    
                    if isempty(dat) % && isempty(add_dat)
                        fprintf('Index not present in index component view.\n')
                        sec_ids = [];
                    else
                        dat = datenum(dat, 'yyyy-mm-dd');
                        if (today()-dat)>opt.get('max_nb_months')*30
                            error('%s - Latest index composition is too old (%s): please check or ask for no constraint on dates.\,', varargin{1}, datestr(dat,'dd/mm/yyyy'))
                        else
                            sec_ids = get_repository('index-comp', varargin{1}, {'recent_date_constraint', 0,...
                                'source', opt.get('source'), 'with_weights', opt.get('with_weights')});
                        end
                    end
                    
                else
                    if opt.get('with_weights')
                        sec_ids = cell2mat(exec_sql('KGR', ...
                            sprintf('select security_id, SECURITYRATIO from %s..INDEXCOMPONENT_QUANT where INDEXID=''%s'' order by security_id', repository,i_id{1})));
                    else
                        sec_ids = cell2mat(exec_sql('KGR', ...
                            sprintf('select distinct security_id from %s..INDEXCOMPONENT_QUANT where INDEXID=''%s'' order by security_id', repository,i_id{1})));
                    end
                end
            end
            
        elseif strcmpi(opt.get('type'),'dynamic')
            idx_code = varargin{1};
            dates = varargin{2};
            sec_ids = get_index_compo_histo(idx_code, dates(1), dates(2));
        end
        
        varargout = {sec_ids};        
     
    case 'index-list'
        index_name = exec_sql('KGR','select INDEXNAME from KGR..[INDEX]');
        varargout = {index_name};
        
%% category: trading destinations by security_id
    case 'maintd' % renvoie le td id de la detination primaire pour un sec_id
        sec = varargin{1};
        sec_str = sprintf('%d,',sec);
        sec_str(end)=[];
        
        if length(varargin)==1 || (length(varargin) > 1 && isequaln(varargin{2},'noerror'))
            req = ['select s.security_id, s.primary_trading_destination_id from KGR..HISTO_SECURITY_QUANT s '...
                'where s.security_id in (%s) and s.end_date = ''29991231'' '];
            tdid_ = cell(size(sec));
            result = exec_sql('KGR', sprintf(req, sec_str));
            if ~isempty(result)
                [~,a,b] = intersect(cell2mat(result(:,1)),sec);
                tdid_(b) = result(a,2);
            else
                tdid_ = [];
            end
            tdid = cell2mat(tdid_);
            
            if length(tdid)~=length(tdid_)
                % if you're stopped here in debugger, have a look at which elements
                % of the cell array tdid_ is empty to know which of your sec_id is
                % a problem
                if length(varargin) > 1 && isequaln(varargin{2},'noerror')
                    %non existing stock or TD -> nan
                    tdid_= fillcell(tdid_,cellfun(@isempty,tdid_),nan);
                    tdid = cell2mat(tdid_);
                else
                    msg= 'At least one of your security is not recorded in security_market or has no main td:%d';
                    missings= setdiff(sec ,uo_(result(:,1)) );
                    assert(false, sprintf(msg,missings));
                end
            end
            varargout = {tdid};
        else
            dates = varargin{2};
            
            res = exec_sql_table('KGR',['select r.security_id, r.begin_date, r.end_date, r.primary_trading_destination_id '...
                'from KGR..HISTO_SECURITY_QUANT r   '...
                'where r.security_id in (',sec_str,') '...
                'order by r.security_id, r.begin_date']);
            res.begin_date_num = cellfun(@(c) datenum(c,'yyyy-mm-dd'), res.begin_date);
            res.end_date_num = cellfun(@(c) datenum(c,'yyyy-mm-dd'), res.end_date);
            
            ix = arrayfun(@(v) find(res.security_id==sec(v) & res.begin_date_num<=dates(v) & res.end_date_num>dates(v)), 1:length(sec));
            if size(sec,2)>1
                varargout{1} = res.primary_trading_destination_id(ix)';
            else
                varargout{1} = res.primary_trading_destination_id(ix);
            end
        end
      
    case 'maintd_last' % renvoie le td id de la detination primaire pour un sec_id
        req = ['select s.security_id, s.primary_trading_destination_id from KGR..HISTO_SECURITY_QUANT s, '...
            '(select security_id, max(end_date) end_date from KGR..HISTO_SECURITY_QUANT group by security_id) m '...
            'where s.security_id in (%s) and s.security_id = m.security_id and s.end_date = m.end_date '];
        sec = varargin{1};
        sec_str = sprintf('%d,',sec);
        sec_str(end)=[];
        tdid_ = cell(size(sec));
        result = exec_sql('KGR', sprintf(req, sec_str));
        
        if ~isempty(result)
            [~,a,b] = intersect(cell2mat(result(:,1)),sec);
            tdid_(b) = result(a,2);
        else
            tdid_ = [];
        end
        
        tdid = cell2mat(tdid_);
        if length(tdid)~=length(tdid_)
            % if you're stopped here in debugger, have a look at which elements
            % of the cell array tdid_ is empty to know which of your sec_id is
            % a problem
            if length(varargin) > 1 && isequaln(varargin{2},'noerror')
                %non existing stock or TD -> nan
                tdid_= fillcell(tdid_,cellfun(@isempty,tdid_),nan);
                tdid = cell2mat(tdid_);
            else
                msg= 'At least one of your security is not recorded in security_market or has no main td:%d';
                missings= setdiff(sec ,uo_(result(:,1)) );
                assert(false, sprintf(msg,missings));
            end
        end
        varargout = {tdid};
        
    case 'maintd-zero-if-no-main'
        % renvoie le td id de la detination primaire pour un sec_id
        % renvoie 0 si pas de main d�fini pour un stock
        ric_req = ['select s.security_id, s.primary_trading_destination_id from KGR..HISTO_SECURITY_QUANT s '...
            'where s.security_id in (%s) and s.end_date = ''29991231'' '];
        sec = varargin{1};
        sec_str = sprintf('%d,',sec);
        sec_str(end)=[];
        tdid_ = zeros(size(sec));
        result = exec_sql('KGR', sprintf(ric_req, sec_str));
        if ~isempty(result)
            [~,a,b] = intersect(cell2mat(result(:,1)),sec);
            tdid_(b) = cell2mat(result(a,2));
        end
        varargout = {tdid_};
        
    case 'security_id2timezone'
        sec_list = varargin{1};
        un_sec_list = unique(sec_list);
        str = sprintf('%d,',un_sec_list);
        str(end) = [];
        sec_id_timezone = exec_sql('KGR',...
            ['select s.security_id,e.TIMEZONE '...
            'from KGR..HISTO_SECURITY_QUANT s, KGR..EXCHANGE_QUANT e where s.security_id in (',str,') and s.end_date = ''29991231'' '...
            'and s.primary_trading_destination_id = e.trading_destination_id']);
        varargout{1} = map(cell2mat(sec_id_timezone(:,1)),sec_id_timezone(:,2),reshape(sec_list,numel(sec_list),1));
        
    case 'security_id2global_zone_suffix'
        default_vals=' ';
        
        sec_id_list=unique(varargin{1});
        if size(varargin{1},2)>1
            sec_id_list=sec_id_list';
        end
        
        sec_id_char=sprintf('%d,',sec_id_list);
        sec_id_char(end)=[];
        
        vals=exec_sql('KGR',sprintf([' select  s.security_id, '...
            '(case when e.GLOBALZONEID = 2 then ''_ameri'' else   case when e.GLOBALZONEID = 3 '...
            'then ''_asia'' else   case when e.GLOBALZONEID = 1 then '' '' else  ''_unknown'' end end end ) '...
            'from  KGR..EXCHANGE_QUANT e, KGR..HISTO_SECURITY_QUANT s '...'...
            'where  s.security_id in (%s)  and s.end_date = ''29991231'' and s.primary_trading_destination_id = e.trading_destination_id '...
            'group by s.security_id, e.GLOBALZONEID '],sec_id_char));
        
        if ~isempty(vals)
            no_data=setdiff(sec_id_list,cell2mat(vals(:,1)));
        else
            no_data=sec_id_list;
        end
        
        if ~isempty(no_data)
            vals=cat(1,vals,...
                cat(2,arrayfun(@(a)(a),no_data,'uni',false),repmat({default_vals},size(no_data))));
        end
        
        varargout{1} = vals;
        
    case 'security-id'
        %<* Get security id
        [tmp, res] = get_repository('trading-destination', varargin{:});
        if numel( res)~=1
            error('get_repository:secid', 'REPOSITORY: the code has not exactly one security id');
        end
        varargout = { res};
        %>*
        
    case 'any-to-sec-td'
        %<* Convert code and trading destinations
        % To OUR main codec: security_id, [ td_id ]
        opt   = options( { 'source', '',  'trading-destinations', {}, ...
            'security_id', [] }, varargin);
        code  = opt.get('security_id');
        if isnumeric( code)
            sid  = code;
        else
            sid = get_repository('security-id',  code, 'source', opt.get('source'));
        end
        code = get_repository('security-key', sid);
        tdi  = get_repository('tdinfo', sid);
        if nargout >1
            tdi_all = tdi;
        end
        opt.set('security_id', sid);
        o_tdi = opt.get('trading-destinations');
        if isempty( o_tdi)
            o_tdi = [tdi.trading_destination_id];
        elseif isnumeric( o_tdi)
            % nothing to do
        elseif iscell( o_tdi) && isnumeric( o_tdi{1})
            o_tdi = cell2mat( o_tdi);
        elseif iscell( o_tdi) && ischar( o_tdi{1})
            tmp_o_tdi = o_tdi;
            o_tdi = repmat(nan, length(o_tdi), 1);
            for i = 1 : length(tmp_o_tdi)
                if strcmpi('main', tmp_o_tdi{i})
                    o_tdi(i) = tdi(1).trading_destination_id;
                    st_log('get_tick:TD according to me, main place is <%s>\n', tdi(1).short_name);
                    tdi(1) = [];
                else
                    idx = strmatch( lower(tmp_o_tdi{i}), lower({tdi.short_name}), 'exact');
                    if numel(idx)~=1
                        warning('get_tick:TD', 'trading destination <%s> unavailable', tmp_o_tdi{i});
                    else
                        o_tdi(i) = tdi(idx).trading_destination_id;
                    end
                end
            end
            o_tdi(~isfinite(o_tdi)) = [];
        else
            error('get_tick:trading_destination', 'BAD_USE: format of trading_dest unknown');
        end
        % < on ne veut surtout pas trier par trading_destination
        % croissante mais on souhaite s'assurer de l'unicit?
        [tmp, idx] = unique(o_tdi);
        o_tdi = o_tdi(sort(idx));
        % >
        varargout = { struct('security_id', sid, 'trading_destination_id', o_tdi, 'security_key', code) };
        if nargout > 1
            varargout = cat(2, varargout, { tdi_all});
        end
        %>*
        
    case { 'trading-destination', 'trading_destination', 'trading_destination_id', 'td', 'tdi' }
        %<* Get EXCHANGE from table EXCHANGEMAPPING
        repository = st_version.bases.repository;
        code = varargin{1};
        if ischar(code)
            opt = options( { 'source', nan}, varargin(2:end));
            source = opt.get('source');
            if ischar(source)
                error('get_repository:trading_destination', 'Argument source should be an integer');
            elseif isnan(source)
                res = exec_sql( 'KGR', sprintf( ...
                    [ 'select sm.trading_destination_id,ss.reference_type_id,td.trading_name, sm.security_id '...
                    'from ',repository,'..HISTO_SECURITY_QUANT s, '...
                    ,repository,'..HISTO_SECURITY_QUANT_MARKET sm, '...
                    ,repository,'..HISTO_SECURITY_QUANT_REFERENCE ss, '...
                    ,repository,'..EXCHANGE_QUANT td '...
                    'where s.security_id = ss.security_id and s.end_date = ''29991231'' '...
                    'and sm.security_id=ss.security_id  and sm.end_date = ''29991231'' '...
                    'and sm.trading_destination_id = td.trading_destination_id '...
                    'and ss.reference = ''%s''  and ss.end_date = ''29991231'' '...
                    'group by sm.trading_destination_id, ss.reference_type_id,td.trading_name, sm.security_id, '...
                    'case when td.trading_destination_id = s.primary_trading_destination_id then 1 else 0 end '...
                    'order by case when td.trading_destination_id = s.primary_trading_destination_id then 1 else 0 end desc, '...
                    'sm.trading_destination_id asc'], code));
                if isempty(res)
                    error('get_repository:no_result', 'REPOSITORY: no such a code');
                end
                if length(unique( cell2mat(res(:,4))))~=1
                    sources = unique(cell2mat(res(:,2)));
                    sources = sprintf('%d;', sources);
                    error('get_repository:ambiguity', 'REPOSITORY: more than one security id for this code, please specify your source (%s)', ...
                        sources(1:end-1));
                end
                sid = unique( cell2mat( res(:,4)));
                res = cell2mat( res(:,1));
                [~,un,~] = unique(res);
                res = res(sort(un));
            else
                res = exec_sql( 'KGR', sprintf( ...
                    [ 'select sm.trading_destination_id, sm.security_id '...
                    'from ',repository,'..HISTO_SECURITY_QUANT s, '...
                    ,repository,'..HISTO_SECURITY_QUANT_MARKET sm, '...
                    ,repository,'..HISTO_SECURITY_QUANT_REFERENCE ss, '...
                    ,repository,'..EXCHANGE_QUANT td '...
                    'where s.security_id = ss.security_id and s.end_date = ''29991231'' '...
                    'and sm.security_id=ss.security_id  and sm.end_date = ''29991231'' '...
                    'and sm.trading_destination_id = td.trading_destination_id '...
                    'and ss.reference_type_id = %d and ss.reference = ''%s''  and ss.end_date = ''29991231'' '...
                    'group by sm.trading_destination_id, ss.reference_type_id,td.trading_name, sm.security_id, '...
                    'case when td.trading_destination_id = s.primary_trading_destination_id then 1 else 0 end '...
                    'order by case when td.trading_destination_id = s.primary_trading_destination_id then 1 else 0 end desc, '...
                    'sm.trading_destination_id asc'], source,code));
                if isempty(res)
                    error('get_repository:no_result', 'REPOSITORY: no such a code on <%s>', source);
                end
                sid = unique(cell2mat(res(:,2)));
                if length(sid)~=1
                    error('get_repository:ambiguity', 'REPOSITORY: more than one security id for this code, please stop this!');
                end
                res = cell2mat(res(:,1));
                [~,un,~] = unique(res);
                res = res(sort(un));
            end
        elseif isnumeric(code)
            res = exec_sql( 'KGR', sprintf([...
                'select sm.trading_destination_id,case when sm.trading_destination_id = s.primary_trading_destination_id then 1 else 0 end ranking '...
                'from ',repository,'..HISTO_SECURITY_QUANT s,',repository,'..HISTO_SECURITY_QUANT_MARKET sm '...
                'where s.security_id = sm.security_id '...
                'and s.end_date = ''29991231'' and sm.end_date = ''29991231'' '...
                'and s.security_id = %d '...
                'group by sm.trading_destination_id, case when sm.trading_destination_id = s.primary_trading_destination_id then 1 else 0 end '...
                'order by ranking desc, sm.trading_destination_id asc'], code));
            if isempty(res)
                error('get_repository:no_result',['REPOSITORY: no such security_id : ',...
                    '<%d> in ',repository,'..security_market'], code);
            end
            sid = code;
            res = cell2mat(res(:,1));
            [~,un,~] = unique(res);
            res = res(sort(un));
        else
            error('get_repository:code', 'BAD_USE: code must be numeric or char');
        end
        varargout = { res, sid};
        %>*
        
    case { 'trading-destination-info' , 'tdinfo' } %************************TO MIGRATE************************%
        repository = st_version.bases.repository;
        opt = options({'trading_destination_id',[]},varargin(2:end));
        if isempty(opt.get('trading_destination_id'))
            [td, sec_id] = get_repository('trading-destination', varargin{1});
        else
            td = opt.get('trading_destination_id');
            sec_id = varargin{1};
        end
        
        in_txt = sprintf('%d,', td);
        in_txt = sprintf(' in (%s) ', in_txt(1:end-1));
        
        td_info = exec_sql('KGR',['select ',...
            'place_id,',...
            'exec_market_id,',...
            'EXCHGID,',...
            'TIMEZONE,',...
            'EXCHANGE,',...
            '/*fzone.zone*/null,',...
            'zone_suffix,',...
            'localtime_p,',...
            'default_timezone,',...
            'ranking,',...
            'TICK_SIZE_TABLE_ID,',...
            'MKTGROUP',...
            ' from ',repository,'..[get_mat_referential]',...
            ' where security_id = ',num2str(sec_id),...
            ' and EXCHANGE ',in_txt,...
            ' order by ranking']);
        
        if isempty(td_info)
            error('get_repository:tdinfo', 'REPOSITORY:No trading destination with enough information to work with it!');
        end
        td = cell2mat(td_info(:,5));
        price_scale = cell(length(td), 1);
        
        if ~isnan(td_info{1,11})
            price_scale{1} = cell2mat(exec_sql('KGR',['select MIN_,MAX_,VALUE',...
                ' from ',repository,'..TABLE_TICKSIZE',...
                ' where TABLEID = ',num2str(td_info{1,11}),...
                ' order by MIN_']));
        end
        
        varargout = { struct('place_id', td_info(:,1), 'trading_destination_id', num2cell(td(:)), 'execution_market_id', td_info(:,2), ...
            'short_name', td_info(:,3), 'timezone', td_info(:,4), 'quotation_group', td_info(:,12), ...
            'global_zone_name', td_info(:,6), ...
            'global_zone_suffix', td_info(:,7), ...
            'localtime', td_info(:,8), ...
            'default_timezone', td_info(:,9), ...
            'price_scale', price_scale)};
        
%% category: trading destinations info
    case 'tdid2global_zone_suffix'
        varargout = exec_sql('KGR',['select case when erc.GLOBALZONEID = 2 then ''_ameri'' else case when erc.GLOBALZONEID = 3 then ''_asia'' else '''' end end '...
            'from ',repository,'..EXCHANGE_QUANT erc where erc.trading_destination_id = ',num2str(varargin{1})]);
        
    case 'trading-destinations'
        res = exec_sql( 'KGR', ['select e.trading_destination_id, e.trading_name, er.EXCHGID '...
            'from KGR..EXCHANGE_QUANT e, KGR..EXCHANGE_REFCOMPL er '...
            'where e.trading_destination_id = er.trading_destination_id' ] );
        varargout = { struct('td', res(:,1), 'trading_destination_name', res(:,2), 'trading_destination_short', res(:,3) ) };
        
    case 'td_id2td_name'
        %<* Get trading destination name from trading destination id
        code = varargin{1};
        code_str = join(',', unique(code(~isnan(code))));
        res = exec_sql( 'KGR', sprintf( ...
            ['SELECT e.trading_destination_id, e.trading_name from KGR..EXCHANGE_QUANT e ',...
            ' WHERE e.trading_destination_id in (%s)'], code_str));
        if isempty( res)
            error('get_repository:key', 'REPOSITORY: no trading destination available for code <%d>', code);
        end
        if size(code,2)>1
            varargout{1} = joint(cell2mat(res(:,1)),res(:,2),code(:))';
        else
            varargout{1} = joint(cell2mat(res(:,1)),res(:,2),code(:));
        end
        varargout{1}(cellfun('isempty', varargout{1})) = {'unknown'};
        %>*
        
    case 'td_id2td_name-unknown-if-zero'
        %<* Get trading destination name from trading destination id
        code = varargin{1};
        res = exec_sql( 'KGR', sprintf( ...
            ['SELECT e.trading_name from KGR..EXCHANGE_QUANT e',...
            ' WHERE e.trading_destination_id=%d'], code));
        if isempty( res)
            res = {'Unknown'};
        end
        varargout = res(1);
        
    case 'exchgid2trading_destination_id'
        exchg_id = varargin{1};
        if ischar(exchg_id)
            exchg_id = {exchg_id};
        end
        exchg_id_str = join(''',''', unique(exchg_id));
        query = ['select EXCHGID, trading_destination_id from KGR..EXCHANGE_REFCOMPL',...
            sprintf(' where EXCHGID in (''%s'')', exchg_id_str)];
        res = exec_sql('KGR', query);
        if isempty(res)
            varargout{1} = nan(length(exchg_id), 1);
        else
            varargout{1} = joint(res(:,1), cell2mat(res(:,2)), reshape(exchg_id,[],1));
        end
        
    case 'place_id2timezone' %*********************OBSOLETE***************%
        place_id = varargin{1};
        varargout{1} = char(exec_sql('KGR',['select TIMEZONE',...
            ' from ',repository,'..EXCHANGEREFCOMPL ec,',repository,'..EXCHANGEMAPPING em',...
            ' where em.place_id = ',num2str(place_id),...
            ' and em.EXCHANGE = ec.EXCHANGE']));
    
    case 'td_id2exchgid'
        td_id_str = join(',', unique(varargin{1}));
        res = exec_sql('KGR', ['select trading_destination_id, EXCHGID from KGR..EXCHANGE_REFCOMPL',...
            sprintf(' where trading_destination_id in (%s)', td_id_str)]);
        varargout{1} = joint(cell2mat(res(:, 1)), res(:, 2),...
            reshape(varargin{1}, [], 1));
        varargout{1} = reshape(varargout{1}, size(varargin{1}));
        varargout{1}(cellfun('isempty', varargout{1})) = {''};

%% category: keys
    case 'kech-security-key'
        s = num2str(varargin{1});
        varargout = {fullfile(regexprep(s(max(end-1,1):end),'^0',''),s)};
    
    case 'kech-security-td-key'
        s = num2str(varargin{1});
        if isempty(varargin{2})
            varargout = {fullfile(regexprep(s(max(end-1,1):end),'^0',''),s,'all_td')};
        else
            assert(isnumeric(varargin{2}));
            varargout = {fullfile(regexprep(s(max(end-1,1):end),'^0',''),s,num2str(varargin{2}))};
        end
        
    case 'security-td-key'
        % , sec_id, td_id
        kodes = persist_bufferize('get_repository', {'any-to-sec-td', 'security_id', varargin{1}, 'trading-destinations', varargin{2}});
        code  = kodes.security_key;
        if isempty(varargin{2})
            varargout = { [ code, '_all_td'] };
        else
            o_tdi = sprintf('%d.', kodes.trading_destination_id);
            varargout = { [ code, '_', o_tdi(1:end-1)] };
        end
        
    case 'security-key'
        %<* Get security key
        % Unique string code to name directories
        code = varargin{1};
        if ischar( code)
            code = persist_bufferize('get_repository', {'security-id', varargin{:}});
        end
        varargout = {num2str(code)};
        
%% category: currency
    case 'sec_currency'
        sec_id = varargin{1};
        opts = options({'pm_only', true}, varargin(2:end));
        [un_sec,~,idx_sec] = unique(sec_id);
        sec_str = sprintf('%d,',un_sec);
        if ~opts.get('pm_only')
            fprintf('Warning: you asked for pm only=false BUT currency is only available on primary market.\n')
        end
        res = exec_sql('KGR',['select security_id, min(CCY), max(CCY) ',...
            ' from KGR..HISTO_SECURITY_QUANT ',...
            ' where security_id in (',sec_str(1:end-1),') ',...
            ' and end_date =''29991231'' '...
            ' group by security_id ']);
        idx2ccy =arrayfun(@(c) ~strcmpi(res{c,2}, res{c,3}), 1:size(res,1))';
        res(idx2ccy,2) = {''};
        
        sec_ccy = cell(length(un_sec),1);
        if ~isempty(res)
            [~,a,b] = intersect(un_sec,cell2mat(res(:,1)));
            sec_ccy(a) = res(b,2);
        end
        varargout{1} = sec_ccy(idx_sec);
        
    case {'currency','currency_id'}
        sec_ids = varargin{1};
        sec_str = regexprep(sprintf('%d,',sec_ids),',$','');
        res = exec_sql('KGR',sprintf(['select security_id,CCY from KGR..HISTO_SECURITY_QUANT',...
            ' where security_id in (%s)',...
            ' and end_date =''29991231'''],sec_str));
        [~,a,b] = intersect(sec_ids,cell2mat(res(:,1)));
        cur = cell(length(sec_ids),1);
        cur(a) = res(b,2);
        varargout{1} = cur;
        
    case 'sec_id_rate2ref'
        % get_repository('sec_id_rate2ref','security_id',[10735 110 19457],'from','01/09/2012','to','06/09/2012')
        % get_repository('sec_id_rate2ref','security_id',[12058 19457],'from','01/03/2007','to','06/09/2007')
        
        %--------------------------
        %-- recup inputs
        %--------------------------
        opt = options_light({
            'security_id',[],...
            'from','',...
            'to','',...
            'currency_ref','EUR',...
            },varargin);
        
        security_id=opt.security_id;
        from=opt.from;
        to=opt.to;
        currency_ref=opt.currency_ref;
        
        %--------------------------
        %-- extract
        %--------------------------
        security_id=unique(security_id);
        sec_id_char=sprintf('%d,',security_id);
        sec_id_char(end)=[];
        
        beg_date = datestr(datenum(from, 'dd/mm/yyyy'), 'yyyymmdd');
        end_date = datestr(datenum(to, 'dd/mm/yyyy'), 'yyyymmdd');
        
        vals_zone = exec_sql('MARKET_DATA', ...
            sprintf([' select  crd.DATE, sh.security_id, sh.CCY, 1 , min(crd.VALUE) ' ...
            ' from KGR..HISTO_SECURITY_QUANT sh, ' ...
            ' (select security_id, max(end_date) end_date from KGR..HISTO_SECURITY_QUANT group by security_id) e,  ' ...
            ' KGR..HISTOCURRENCYTIMESERIES crd  ' ...
            ' where  ' ...
            ' crd.CCYREF = ''%s'' and  ' ...
            ' crd.CCY = sh.CCY and '...
            ' crd.ATTRIBUTEID = 43 and crd.SOURCEID = 1 and ' ...
            ' crd.DATE>= ''%s''  and crd.DATE<= ''%s'' and ' ...
            ' sh.security_id in (%s) and '...
            ' sh.end_date = e.end_date and sh.security_id = e.security_id '... %''29991231''
            ' group by crd.DATE, sh.security_id, sh.CCY '], ...
            currency_ref, beg_date,end_date,...
            sec_id_char));
        
        if ~isempty(vals_zone)
            vals_zone(:,1)=arrayfun(@(a)(a),datenum(vals_zone(:,1),'yyyy-mm-dd'),'uni',false); %-734139
        end
        vals=vals_zone; %cat(1,vals,vals_zone);
        %         end
        
        if isempty(vals)
            out = [];
        else
            [~,idx_sort]=sortrows(vals(:, [2 1]),[1 2]);
            vals=vals(idx_sort,:);
            
            out = st_data('from-cell','security id rate to ref',...
                {'day','security_id','currency_id','quotation_unit','rate2ref'},vals);
            out.date = st_data('cols', out, 'day');
            % ATTENTION currency_id n'est plus un id mais un string (CCY) dans les tables
            % cf st_data('col_cdk', temp, 'currency')
        end
        
        varargout{1}=out;
        
%% category: others   
    case 'flextradeticker2sec_id' %************************TO MIGRATE************************%
        ticker = unique(varargin{1});
        split_ticker = cellfun(@(t)horzcat({t(1:end-3)}, {t(end-1:end)}), ticker, 'uni', false);
        % On �l�mine les tickers qui ne contiennent pas de "."
        ticker = ticker(cellfun('length',split_ticker)>=2);
        split_ticker = vertcat(split_ticker{cellfun('length',split_ticker)>=2});
        if size(split_ticker, 2) > 2
            split_ticker = horzcat(split_ticker(:,1), split_ticker(:,end));
        end
        is_conso = strcmp(split_ticker(:,2),'AG');
        security_id = nan(length(ticker),1);
        doublets = cell(2,0);
        missing_sym6 = cell(2,0);
        if any(~is_conso)
            temp = unique(split_ticker(~is_conso,1));
            str = sprintf('''%s'',',temp{:});
            str(end) = [];
            res = exec_sql('KGR',['select s.SYMBOL1,f.SUFFIX,ec.EXCHANGE,',...
                'case when s.SYMBOL6 is null then -1 else convert(int,s.SYMBOL6) end,s.PRIORITY',...
                ' from KGR..SECURITY s,KGR..FlextradeExchangeMapping f,KGR..EXCHANGEREFCOMPL ec',...
                ' where s.SYMBOL1 in (',str,')',...
                ' and s.STATUS = ''A''',...
                ' and f.EXCHANGE = ec.EXCHANGE',...
                ' and ec.EXCHGID = s.EXCHGID']);
            priority = cell2mat(res(:,5));
            key = res(:,1:2);
            value = cell2mat(res(:,4));
            % Keeps only keys present in the input
            is_key = map(split_ticker,ones(size(split_ticker,1),1),key);
            % If find no key, return the value for the first key
            if all(isnan(is_key))
                fprintf('The following ticker(s) has no key in KGR..SECURITY:\n%s\n', ticker{1});
                
                possible_values = cellfun(@(k1,k2)sprintf('%s.%s', k1, k2), key(:,1), key(:,2), 'uni', false);
                possible_values = sprintf('%s\n%s\n', possible_values{:});
                fprintf('Please re-try with one of the following values:\n%s', possible_values);
                
                priority = priority(1);
                key = key(1, :);
                value = value(1);
                
                security_id(~is_conso) = value;
            else
                priority(isnan(is_key)) = [];
                key(isnan(is_key),:) = [];
                value(isnan(is_key)) = [];
                
                % Remove "PRIORITY is NULL" if ticker has "PRIORITY = 1"
                idx_rm = ~isnan(map(key(priority==1,:),ones(sum(priority==1),1),key))&...
                    priority~=1;
                priority(idx_rm) = [];
                key(idx_rm,:) = [];
                value(idx_rm) = [];
                
                % Update security_id
                security_id(~is_conso) = map(key(priority==1,:),value(priority==1),split_ticker(~is_conso,:));
            end
            
            % Remove all "PRIORITY = 1 "
            idx_rm = priority==1;
            priority(idx_rm) = [];
            key(idx_rm,:) = [];
            value(idx_rm) = [];
            
            % Find doublets
            if ~isempty(key)
                [~,i_double,idx_double] = unique(cell_to_num(key),'rows');
                nb_double = accumarray(idx_double,1);
                if any(nb_double>1)
                    doublets = key(i_double(nb_double>1),:)';
                    % Remove the doublets from "priority", "key" and "value"
                    idx_doublets = nb_double(idx_double) > 1;
                    priority(idx_doublets) = [];
                    key(idx_doublets,:) = [];
                    value(idx_doublets) = [];
                end
            end
            
            % Find tickers with missing SYMBOL6
            if ~isempty(key)
                missing_sym6 = key';
            end
        end
        if any(is_conso)
            temp = unique(split_ticker(is_conso,1));
            str = sprintf('''%s'',',temp{:});
            str(end) = [];
            res = exec_sql('KGR',['select distinct PARENTCODE,',...
                'case when SYMBOL6 is null then -1 else convert(int,SYMBOL6) end',...
                ' from KGR..SECURITY',...
                ' where PARENTCODE in (',str,')',...
                ' and STATUS = ''A''']);
            if ~isempty(res)
                key = res(:,1);
                value = cell2mat(res(:,2));
                
                % Remove "SYMBOL6 = -1" if ticker has "SYMBOL6 != -1"
                idx_rm = value==-1&...
                    ~isnan(map(key(value~=-1),ones(sum(value~=-1),1),key));
                key(idx_rm) = [];
                value(idx_rm) = [];
                
                % Update security_id
                security_id(is_conso) = map(key(value~=-1),value(value~=-1),split_ticker(is_conso,1));
                
                % Remove all "SYMBOL6 = -1"
                idx_rm = value==-1;
                key(idx_rm) = [];
                value(idx_rm) = [];
                if ~isempty(key)
                    % Find doublets
                    [~,i_key,idx_key] = unique(key);
                    nb_key = accumarray(idx_key,1);
                    if any(nb_key>1)
                        doublets = [doublets,[key(i_key(nb_key>1))';repmat({'AG'},1,sum(nb_key>1))]];
                        % Remove the doublets from "key" and "value"
                        idx_doublets = nb_key(idx_key) > 1;
                        key(idx_doublets) = [];
                        value(idx_doublets) = [];
                    end
                    
                    % Find tickers with missing SYMBOL6
                    if ~isempty(key)
                        missing_sym6 = [missing_sym6,[key';repmat({'AG'},1,length(key))]];
                    end
                end
            end
        end
        
        % Doublets report
        if ~isempty(doublets)
            str = sprintf('%s.%s, ',doublets{:});
            fprintf('The following ticker(s) has/ve several entries in KGR..SECURITY:\n%s\n',str(1:end-2));
        end 
        % Missing SYMBOL6 report
        if ~isempty(missing_sym6)
            str = sprintf('%s.%s, ',missing_sym6{:});
            fprintf('The following ticker(s) has/ve no SYMBOL6 in KGR..SECURITY:\n%s\n',str(1:end-2));
        end
        
        varargout{1} = map(ticker,security_id,varargin{1});
        
    otherwise
        error('get_repository:mode', 'MODE: mode <%s> unknown', mode);
        
end

end


function Y = map(data1,X,data2)
% MAP maps the values of X with those of data1.
if ~iscell(data1)
    [data2,~,idx] = unique(data2,'rows');
    
    [~,a,b] = intersect(data1,data2,'rows');
    if ~iscell(X)
        Y = nan(length(data2),size(X,2));
    else
        Y = cell(length(data2),size(X,2));
    end
    Y(b,:) = X(a,:);
    
    Y = Y(idx,:);
else
    cell_idx = cell_to_num([data1;data2]);
    Y = map(cell_idx(1:size(data1,1),:),X,cell_idx(1+size(data1,1):end,:));
end

end


function varargout = cell_to_num(data)
idx_char = cellfun(@ischar,data(1,:));
not_idx_char = find(~idx_char);
data_char = mat2cell(data(:,idx_char),size(data(:,idx_char),1),ones(size(data(:,idx_char),2),1));
data_to_int = cell2mat(cellfun(@uniq,data_char,'uni',false));
data_to_cell = nan(size(data));
if ~isempty(not_idx_char)
    data_to_cell(:,not_idx_char) = cell2mat(data(:,not_idx_char));
end
if any(idx_char)
    data_to_cell(:,idx_char)  = data_to_int;
end
varargout{1} = data_to_cell;

    function varargout = uniq(data)
        [~,~,idx]      = unique(data);
        varargout{1}   = idx;
    end
end


function str = join(separator,list)
if isempty(list)
    str = '';
    return
end
if isnumeric(list)
    str = sprintf(['%d',separator],list);
elseif iscellstr(list)
    str = sprintf(['%s',separator],list{:});
else
    error('join:wronginputargs','Second argument should be either numeric or cellstr')
end
str(end-length(sprintf(separator))+1:end) = [];
end


function bbg_quant = bbg_suffix_map(bbg)

if ~iscell(bbg)
    bbg = tokenize(bbg);
end

% suffix
mp = exec_sql_table('KGR','select BBG_EXCH_CODE, isnull(BBG_EXCH_MAP, BBG_EXCH_CODE) BBG_suffix from KGR..EXCHANGE_QUANT where EXCHANGETYPE = ''M''');
ix = ~strcmpi(mp.BBG_EXCH_CODE, mp.BBG_suffix);
mp = [mp; [mp.BBG_EXCH_CODE(ix), mp.BBG_EXCH_CODE(ix)]];

% identify multiple mappings
l = cellfun(@length, mp.BBG_suffix);
mm = mp(l>2,:);
mpa = mp(l<=2,:);
for b = 1:size(mm.BBG_EXCH_CODE)
    mms = split(mm.BBG_suffix{b},',');
    mpa = [mpa; ...
        [repmat(mm.BBG_EXCH_CODE(b),length(mms),1), mms]];
end

% change suffix
bbg_s = cellfun(@(x) x(end-1:end), bbg, 'uni',0);
bbg_r = cellfun(@(x) x(1:end-2), bbg, 'uni',0);
if size(bbg_s,2)>1
    bbg_s_new = joint(mpa.BBG_suffix,mpa.BBG_EXCH_CODE,bbg_s');
else
    bbg_s_new = joint(mpa.BBG_suffix,mpa.BBG_EXCH_CODE,bbg_s);
end
bbg_s_new(cellfun(@isempty, bbg_s_new)) = bbg_s(cellfun(@isempty, bbg_s_new));

bbg_quant = arrayfun(@(x) sprintf('%s%s',bbg_r{x},bbg_s_new{x}), (1:length(bbg_r))', 'uni',0);

if size(bbg_s,2)>1
    bbg_quant = bbg_quant';
end

end


function v = membership_vect(idx_code,sec_date)
tic;
fprintf('Retrieving historical index components for stocks categories...\n')
idx_mmb = get_repository('index-comp',idx_code, [min(sec_date(:,2)), max(sec_date(:,2))],{'type','dynamic'});
toc;

mmb = reshape(idx_mmb.member_histo.value,[],1);
dts = reshape(repmat(idx_mmb.member_histo.date,1,size(idx_mmb.member_histo.value,2)),[],1);
us = cellfun(@str2num, idx_mmb.member_histo.colnames);
sec_ids = reshape(repmat(us,size(idx_mmb.member_histo.value,1),1),[],1);

v = joint([sec_ids, dts], mmb, sec_date);
v(isnan(v))=0;
end
