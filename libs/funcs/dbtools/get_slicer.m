function varargout = get_slicer(mode, varargin)
% GET_SLICER - get graph indicator
%
% get_slicer( 'masterkey', 'mavg/window�100�step�10�type�"p�colnames�"price', 'security_id', 110, 'trading-destinations', {} )
% [a,b,c] = get_slicer( 'parse-args', 'mavg/window�100�step�10�type�"p�colnames�"price', 'security_id', 110, 'trading-destinations', {} )
% data = get_slicer( 'mavg/window�100�step�10�type�"p�colnames�"price', 'security_id', 110, 'day', '01/01/2009', 'to', '30/01/2009', 'trading-destinations', {} )
% data = from_buffer( 'get_slicer', 'mavg/window�100�step�10�type�"p�colnames�"price', 'security_id', 110, 'from', '01/01/2009', 'to', '30/01/2009', 'trading-destinations', { 'main'} )
% data = from_buffer( 'get_slicer', 'mavg/window�100�step�10�type�"p�colnames�"price', 'security_id', 110, 'from', '01/01/2009', 'to', '30/01/2009', 'trading-destinations', {} )
%
% author   : 'clehalle@cheuvreux.com'
% reviewer : 'no one'
% version  : '0.1'
% date     : '24/02/2009'
%
% See also gi_create get_tick read_dataset get_repository from_buffer
persistent opt_storage

switch mode
    case 'masterkey'
        %<* Masterkey generation
        % To know where buffer has to be stored
        opt = options( varargin(2:end) );
        [slicer_name, opt4graph, params_mkey] = parse_args( varargin{1});
        sec_mkey = get_sectd_key(opt.get('security_id'), opt.get('trading-destinations'), opt4graph);
        varargout = { fullfile( slicer_name, params_mkey, sec_mkey) };
        %>*
    case 'parse-args'
        [a,b,c] = parse_args(varargin{1});
        varargout = {a,b,c};
    otherwise
        opt4me = options( {'format-date', 'dd/mm/yyyy', 'trading-destinations', {}, 'from_buffer:cmd', ''}, varargin);
        from_date = opt4me.get('day');
        % to_date   = opt4me.get('to');
        sec_id    = opt4me.get('security_id');
        td_id     = opt4me.get('trading-destinations');
        [slicer_name, opt4slicer] = parse_args( mode);
        % cette fonction doit �tre simplifi�e
        sec_td_key = get_sectd_key(sec_id, td_id, opt4slicer); % get_repository( 'security-td-key', sec_id, td_id);
        
        current_day = read_dataset( 'tickdb', 'security', sec_id, 'from', from_date, 'to', from_date, 'trading-destinations', td_id);
        [output,idx] = st_data('isempty', current_day);
        if idx
            % je ne travaille pas sur des donn�es vides
            varargout = {output};
            return
        end
        my_slicer = slicer_lib( slicer_name);
        colnames  = opt4slicer.remove('colnames');
        sliced_day = my_slicer.sliding( st_data( 'keep', current_day, colnames), ...
            opt4slicer.get('window'), opt4slicer.get('step'), opt4slicer.get('type'));
        if isfield( my_slicer, 'get_info');
            try
                sliced_day.info.slicer_info = my_slicer.get_info();
            catch er
                st_log('get_slicer: error encountered while evaluating the dedicated <get_info> method\n--->%s...\n', er.message);
            end
        end
        varargout = { sliced_day };
        
end

%%** Embedded functions

%%* Parse char of opts
function [slicer_name, opt, mkey] = parse_args(str)
idx = strfind( str, '/');
if isempty( idx)
    slicer_name = str;
    opt = options();
else
    slicer_name = str(1:idx(1)-1);
    opt = str2opt( str(idx(1)+1:end));
end
opt2check = options( opt.get());
sec_dt = 1/(24*3600);
w_type = opt2check.remove('type');
step   = opt2check.remove('step');
switch lower(w_type)
    case { 'p', 'point', 'points'}    % must be inline with st_function options
        step   = num2str(step);
        w_type = 'p';
    case { 'date', 'd' , 't', 'time'} % must be inline with st_function options
        step   = num2str(round(step/sec_dt));
        w_type = 't';
    otherwise
        error('get_slicer:sliding:mode', 'mode <%s> unknown (see st_function/build_slicer for more info)', w_type);
end
        
mkey = fullfile(['window=' num2str( opt2check.remove('window'))], ...
    ['step='  step], ...
    ['type=' w_type ], ...
    ['colnames=' opt2check.remove('colnames')]);
if ~opt2check.isempty()
    lst = opt2check.get();
    error('set_params_basic_indicator:masterkey:check_args', ...
        'There are some unhandled parameters, Please, find and remove the folowing ones :\n%s', sprintf('<%s>\n', lst{1:2:end}));
end
    

function sec_td_key = get_sectd_key(sec_id, td_id, opt4graph)
if ~ischar(sec_id) && length(sec_id) > 1
    if opt4graph.contains_key('source:char') && strcmp(opt4graph.get('source:char'), 'index')
        sec_td_key = sec_id;
    else
        % Alors on fait une clef d�guelasse
        sec_td_key = hash(convs('safe_str', {sort(sec_id), sort(td_id)}), 'MD5');
    end
else
    if opt4graph.contains_key('source:char') && strcmp(opt4graph.get('source:char'), 'index')
        sec_td_key = get_repository( 'index-key', sec_id);
    else
        sec_td_key = get_repository( 'security-td-key', sec_id, td_id);
    end
end