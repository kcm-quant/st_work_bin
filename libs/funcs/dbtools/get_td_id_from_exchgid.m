function td_id = get_td_id_from_exchgid(exchgid)
% GET_TD_ID_FROM_EXCHGID - Grabs trading destination id for corresponding exchgid
% td_id = get_td_id_from_exchgid({'SEPA','SEPA','toto','SEFE'})

if ~iscell(exchgid)
    get_td_id_from_exchgid({exchgid})
end

global st_version
repository = st_version.bases.repository;

exchgid_str = sprintf('''%s'',',exchgid{:});

exchgid_exchange = exec_sql('KGR',['select distinct EXCHGID,EXCHANGE',...
    ' from ',repository,'..EXCHANGEREFCOMPL',...
    ' where EXCHGID in (',exchgid_str(1:end-1),')']);

[un_exchgid,~,idx_exchgid] = unique(exchgid);

[~,a,b] = intersect(un_exchgid,exchgid_exchange(:,1));
temp = nan(length(un_exchgid),1);
temp(a) = cell2mat(exchgid_exchange(b,2));

td_id = temp(idx_exchgid);