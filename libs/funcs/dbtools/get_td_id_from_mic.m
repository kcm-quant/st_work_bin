function td_id = get_td_id_from_mic(mic_list)
% Trouve dans le r�f�rentiel la liste de trading destinations associ�e � la
% liste des code MIC en entr�e.
% 
un_mic = unique(mic_list);
str = to_str(un_mic);
mic2exchange = exec_sql('KGR',['select MIC,trading_destination_id,dark from KGR..MICMAPPING',...
    ' where MIC in (',str,')']);
temp = mapping(mic2exchange(:,1),cell2mat(mic2exchange(:,2:3)),un_mic);
missing_mic = un_mic(isnan(temp(:,1)));
if ~isempty(missing_mic)
    fprintf('Missing mic code in KGR..MICMAPPING: %s\n',to_str(missing_mic));
end
td_id = mapping(mic2exchange(:,1),cell2mat(mic2exchange(:,2:3)),mic_list);

function yi = mapping(x,y,xi)
% helper function
[un_xi,~,idx_xi] = unique(xi);
[~,a,b] = intersect(x,un_xi);
temp = nan(length(un_xi),size(y,2));
temp(b,:) = y(a,:);
yi = temp(idx_xi,:);

function str = to_str(in)
% Helper function
str = sprintf('''%s'',',in{:});
str(end) = [];