function rslt = get_td_name(td_id)
% GET_TD_NAME - permet de r�cup�rer le nom d'une trading_destination en
% fonction de son trading_destination_id
%
% ex : get_td_name(4)

global st_version
repository = st_version.bases.repository;

if ischar(td_id)
    % C'est donc qu'on a affaire � un EXCHGID
    td_id = exec_sql('KGR',['select top 1 EXCHANGE from ',repository,'..EXCHANGEMAPPING',...
        ' where EXCHGID = ''',td_id,'''']);
    rslt = get_td_name(td_id);
elseif length(td_id)>1
    rslt = {};
    for t = td_id(:)'
        rslt = cat(1, rslt, get_td_name(t));
    end
elseif iscell(td_id)
    rslt = get_td_name(cell2mat(td_id));
elseif isempty(td_id) || ~isfinite(td_id)
    rslt = 'toutes les destinations qui le traitent';
elseif isnumeric(td_id)
    % Kepche TODO, est ce le nom de la plateforme que l'on souhaite ?
    rslt = cell2mat(exec_sql('KGR',['select PLATFORM',...
        ' from ',repository,'..EXCHANGEREFCOMPL where EXCHANGE = ',num2str(td_id)]));
    if isempty(rslt)
        rslt = sprintf('Unknwon Exchange : %d', td_id);
    end
else
    error('get_td_name:check_args', 'Unable to process this argument');
end