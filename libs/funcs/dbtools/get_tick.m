function data = get_tick(mode, varargin)
% GET_TICK - Tick by tick data retrieval from the MySQL database
%
% *Notice for the guidance of the long-term users: GET_TICK has been deeply
%  reconstructed. This new version has nothing to do with older ones.
% 
% Examples
% get_tick('masterkey', 'btickdb', 'security_id', 110 )
% get_tick('ft', 'security_id', 110, 'from', '01/04/2008', 'to', '01/04/2008')
% get_tick('btickdb', 'security_id', 110, 'from', '01/04/2008', 'to', '30/04/2008' , 'source', 'topaggr', 'trading-destinations', {'main', 'chix'} )
% data = get_tick('btickdb', 'security_id',  110, 'from', '01/04/2008', 'to', '01/04/2008' , 'trading-destinations', {4, 51} )
% data = get_tick('mo_tick', 'security_id', 110, 'from', '02/02/2009', 'to', '02/02/2009','trading-destinations', {'MAIN'} )
% 
%
% See also read_dataset get_repository get_basename4day
%
% author: 'rburgot@cheuvreux.com'
% reviewer: 'clehalle@cheuvreux.com'
% Modifier: 'mlasnier@keplercheuvreux'
% version: '2.0'
% last version date: '08/02/2018'
% 

global st_version;

tick_db_name = st_version.bases.market_data;

switch mode
    case 'tbt2-deals-intraday'
        opt = options({'security', 'non sp?cifi?', 'day','??/??/????', 'format_date', 'dd/mm/yyyy','tradingDestinationId' , 4 ,'dir', 'Y:\tick_ged\','tradesOnly', 1}, ...
            varargin(1:end));
        if (opt.get( 'tradesOnly' ) == 0 )
            st_log('get_tick:For order book data, please use the mode ''tbt2-deals-ob-intraday'' and not the flag ''tradesOnly''');
            data = st_data('empty');
        else
            lst = opt.get();
            data=get_orderbook('read', lst{:});
        end
    case 'tbt2-deals-ob-intraday'
        opt = options({'security', 'non sp?cifi?', 'day','??/??/????', 'format_date', 'dd/mm/yyyy','tradingDestinationId' , 4 ,'dir', 'Y:/tick_ged/','tradesOnly', 0}, ...
            varargin(1:end));
        if (opt.get( 'tradesOnly' ) == 1 )
            st_log('get_tick:For trade data only, please use the mode ''tbt2-deals-intrada'' and not the flag ''tradesOnly''');
            data = st_data('empty');
        else
            lst = opt.get();
            [deals,obs]=get_orderbook('read', lst{:});
            data{1} = deals;
            data{2} = obs;
        end
    case 'btickdb'
        data = get_tick( 'filter-td', varargin{:}, ...
            'initial-colnames', 'price;volume;sell;bid;ask;bid_size;ask_size;trading_destination_id', 'final-colnames', {}, ...
            'where_f-td', '~({auction}|{trading_at_last}|{cross}|{trading_after_hours})');
    case 'tick4bi'
        data = get_tick( 'filter-td', varargin{:}, ...
            'initial-colnames', ['price;volume;bid;ask;sell;'...
            'auction;opening_auction;closing_auction;intraday_auction;trading_destination_id;bid_size;ask_size;buy'],...
            'final-colnames', {}, 'where_f-td', '~({trading_at_last}|{cross}|{trading_after_hours})');
    case 'tick4simap'
        data = get_tick( 'filter-td', varargin{:}, ...
            'initial-colnames', ['price;volume;bid;ask;sell;'...
            'auction;opening_auction;closing_auction;intraday_auction;trading_destination_id;bid_size;ask_size;buy;dark;deal_id'],...
            'final-colnames', {}, 'where_f-td', '~({trading_at_last}|{cross}|{trading_after_hours})');
        %-- add of columns 'tick_size'
        data=add_col_tick_size(data);
    case 'tick4cfv'
        data = get_tick( 'filter-td', varargin{:}, ...
            'initial-colnames', ['price;volume;bid;ask;sell;'...
            'auction;opening_auction;closing_auction;intraday_auction;trading_destination_id;buy'],...
            'final-colnames', {}, 'where_f-td', '~({trading_at_last}|{trading_after_hours})');
    case 'tick4resilience_imbalance'
        %%%% Purpose of this mode:
        % + Create Dbid and Dask for the calculation of the replenishment
        % + Create over_bid, over_ask columns for the imabalance
        %%%%
        opt = options( { 'source', '',  'trading-destinations', {}, ...
            'where_f-td', '', 'binary', 0, 'security_id', NaN,'smallest_tick','1e-6',...
            'tick_option', false, 'trade_side_definition', 'strict'}, varargin);
        smallest_tick = opt.get('smallest_tick');
        
        % Remark:
        % Dark deals are integrated into the calculation as they can
        % interact with the lit orderbook.
        data = get_tick( 'filter-td', varargin{:}, ...
            'initial-colnames','price;volume;bid;ask;bid_size;ask_size;trading_destination_id;dark;auction;closing_auction;intraday_auction;opening_auction;trading_after_hours;trading_at_last',...
            'final-colnames', {}, 'where_f-td', '~({trading_at_last}|{cross}|{trading_after_hours}|{auction})');
        formula = ['[round(({price}-{bid})/',smallest_tick,')==0&round(({ask}-{bid})/',smallest_tick,')>0,',...
            'round(({price}-{ask})/',smallest_tick,')==0&round(({ask}-{bid})/',smallest_tick,')>0]'];
        
        % formula_bid_ask does not apply to replenishment measures
        if strcmpi(opt.get('trade_side_definition'), 'strict')
            formula_bid_ask = ['[round(({price}-{bid})/',smallest_tick,')==0&round(({ask}-{bid})/',smallest_tick,')>0,',...
                'round(({price}-{ask})/',smallest_tick,')==0&round(({ask}-{bid})/',smallest_tick,')>0]'];
        elseif strcmpi(opt.get('trade_side_definition'), 'all')
            formula_bid_ask = ['[round(({ask}+{bid})/',smallest_tick,')>round(2*{price}/',smallest_tick,')&round(({ask}-{bid})/',smallest_tick,')>0,',...
                'round(({ask}+{bid})/',smallest_tick,')<round(2*{price}/',smallest_tick,')&round(({ask}-{bid})/',smallest_tick,')>0]'];
        end
        
        if iscell(data)
            for i = 1:length(data)
                if isempty(data{i}.date)
                    continue
                end
                over_bid_ask = st_data('apply-formula',data{i},formula,'output','value');
                data{i} = st_data('add-col',data{i},over_bid_ask,{'over_bid','over_ask'});
                [dbid,dask] = get_Dbid_Dask(data{i},str2double(smallest_tick),opt.get('tick_option'));
                data{i} = st_data('add-col',data{i},[dbid,dask],{'dbid','dask'});
                
                over_bid_ask = st_data('apply-formula',data{i},formula_bid_ask,'output','value');
                data{i}.value(:, cellfun(@(c) ~isempty(regexp(c, 'over_bid')),data{i}.colnames)) = over_bid_ask(:,1);
                data{i}.value(:, cellfun(@(c) ~isempty(regexp(c, 'over_ask')),data{i}.colnames)) = over_bid_ask(:,2);
            end
        else
            over_bid_ask = st_data('apply-formula',data,formula,'output','value');
            data = st_data('add-col',data,over_bid_ask,{'over_bid','over_ask'});
            [dbid,dask] = get_Dbid_Dask(data,str2double(smallest_tick),opt.get('tick_option'));
            data = st_data('add-col',data,[dbid,dask],{'dbid','dask'});
            
            over_bid_ask = st_data('apply-formula',data,formula_bid_ask,'output','value');
            data.value(:, cellfun(@(c) ~isempty(regexp(c, 'over_bid')),data.colnames)) = over_bid_ask(:,1);
            data.value(:, cellfun(@(c) ~isempty(regexp(c, 'over_ask')),data.colnames)) = over_bid_ask(:,2);
        end
        
    case 'tick_regrouped' % Calcul inspir? du travail de Robin Ferret
        % On regroupe les trades :
        % - qui ne sont ni auction, ni "at last", ni "after hours", ni
        %   cross, ni dark
        % - qui se trouvent sur une m?me destination de trading
        % - qui traitent au m?me prix
        % - qui sont s?par?s de moins de 100 ?sec
        % - qui sont dans le m?me sens. Les trades non-identifi?s ne sont
        %   pas regroup?s.
        data = get_tick( 'filter-td', varargin{:});
        if ~iscell(data)
            [~,~,idx_date] = unique(floor(data.date));
            if isempty(idx_date)
                data = {data};
            else
                data = accumarray(idx_date,(1:length(idx_date))',[],@(i){st_data('from-idx',data,sort(i))});
            end
        end
        L = length(data);
        for l = 1:L
            if isempty(data{l}) || isempty(data{l}.date)
                continue
            end
            info = data{l}.info;
            nb_microsec_agreg = 100;
            if ~any(strcmp(data{l}.colnames,'microseconds'))
                data{l} = st_data('add-col',data{l},floor(mod(mod(data{l}.date,1)*24*3600,1)*1e6),'microseconds');
            end
            idx_continuous = all(st_data('cols',data{l},'cross;auction;trading_at_last;trading_after_hours;dark')==0,2);
            data_continuous = st_data('from-idx',data{l},idx_continuous);
            data_not_continuous = st_data('from-idx',data{l},~idx_continuous);
            [un_td,~,idx_td] = unique(st_data('cols',data_continuous,'trading_destination_id'));
            nb_td = length(un_td);
            vals = cell(nb_td,1);
            date = cell(nb_td,1);
            for i = 1:nb_td
                sbpt = st_data('cols',data_continuous,'sell;buy;price;microseconds',idx_td==i);
                sbpt(:,3) = round((sbpt(:,3)-sbpt(1,3))*1e6);
                sbpt(:,4) = (sbpt(:,4)*1e-6+floor(mod(data_continuous.date(idx_td==i),1)*24*3600))/24/3600;
                sbpt(:,4) = cumsum([true;diff(sbpt(:,4))>=nb_microsec_agreg/24/3600/1e6]);
                sbpt(all(sbpt(:,1:2)==0,2),1:2) = nan;
                sbpt(:,1:3) = cumsum([true(1,3);~(abs(diff(sbpt(:,1:3),[],1))<1)],1);
                [~,ix_trade,idx_trade] = unique(sbpt,'rows');
                vals{i} = st_data('cols',data_continuous,'price;volume;sell;bid;ask;bid_size;ask_size;buy;microseconds',idx_td==i);
                date{i} = data_continuous.date(idx_td==i);
                vals{i} = [vals{i}(ix_trade,1),accumarray(idx_trade,vals{i}(:,2)),vals{i}(ix_trade,3:9),...
                    ones(length(ix_trade),1)*un_td(i)];
                date{i} = date{i}(ix_trade);
            end
            
            data{l} = st_data('init','date',cell2mat(date),'value',cell2mat(vals),'colnames',...
                {'price','volume','sell','bid','ask','bid_size','ask_size','buy',...
                'microseconds','trading_destination_id'});
            data{l}.colnames = [data{l}.colnames,{'cross','auction','opening_auction',...
                'closing_auction','trading_at_last','trading_after_hours','intraday_auction','dark'}];
            data{l}.value = [data{l}.value,zeros(length(data{l}.date),8)];
            data{l}.date = [data{l}.date;data_not_continuous.date];
            data{l}.value = [data{l}.value;st_data('cols',data_not_continuous,...
                ['price;volume;sell;bid;ask;bid_size;ask_size;',...
                'buy;microseconds;trading_destination_id;cross;auction;',...
                'opening_auction;closing_auction;trading_at_last;',...
                'trading_after_hours;intraday_auction;dark'])];
            [~,I] = sortrows([data{l}.date,st_data('cols',data{l},'microseconds')]);
            data{l} = st_data('from-idx',data{l},I);
            data{l}.info = info;
        end
        if ~any(strcmp(varargin,'output-mode')) || ~strcmp(varargin{find(strcmp(varargin,'output-mode'))+1},'day')
            data = st_data('stack',data{:});
        end
    case 'orig_mo_tick' % can work on Euronext from march 2009 to septmeber 2009 but otherwise it relies too much on the order book as well as overbid
        data = get_tick( 'filter-td', varargin{:}, ...
            'initial-colnames', ['price;volume;bid;ask;sell;'...
            'auction;opening_auction;closing_auction;intraday_auction;trading_destination_id;bid_size;ask_size;buy'],...
            'final-colnames', {}, 'where_f-td', '~({auction}|{trading_at_last}|{cross}|{trading_after_hours})');
        % data = st_data('from-idx', data, mod(data.date, 1) > datenum(0,0,0,11,49,58.8) & mod(data.date, 1) < datenum(0,0,0,12,1,0)); % permettait de faire un test
        % d'aggragation sur total 19/06/2009 ? une p?riode o? il y beaucoup
        % d'aggr?gations ? faire
        if isempty(data)
            return;
        end
        idx_td = strmatch('trading-destinations', varargin(1:2:end));
        if isempty(varargin{2*idx_td})
            error('get_tick:mo_tick:exec', 'You have to specify only one trading-destination');
        end
        
        epsilon =  0.00001; %Magic Number :  used to round prices given by Sybase
        sec_dt = datenum(0,0,0,0,0,1) / 5;
        
        sells     = st_data('col', data, 'sell');
        price     = round(st_data('col', data, 'price') / epsilon) * epsilon;
        volume    = st_data('col', data, 'volume');
        
        sizes = {st_data('col', data, 'ask_size');st_data('col', data, 'bid_size')};
        prices_ob = {round(st_data('col', data, 'ask') / epsilon) * epsilon;round(st_data('col', data, 'bid') / epsilon) * epsilon};
        secs    = data.date;
        diff_p  = diff(price);
        auctions_indic = st_data('col', data, 'auction;opening_auction;closing_auction;intraday_auction');
        td_id = st_data('col', data, 'trading_destination_id');
        
        seq_begins =  logical(diff(sells)) ... % ou bien on change de c?t? du carnet
            | (diff(secs) > sec_dt)... % Ou bien l'horodatage permet de conclure que ce n'est pas le m?me ordre agressif
            | any(diff([sizes{1}, sizes{2}, prices_ob{1}, prices_ob{2}]), 2)... % Ou bien le carnet d'ordre a ?t? updat? entre les deux transactions, ce qui n'est pas le cas s'il s'agit du m?me ordre march?
            ... % Les deux lignes suivantes ont pour seule fonction de s?parer les ordres stop de l'ordre march? qui les a d?clench?s par son ?x?cution. Je ne me souviens plus si cela est n?cessiare
            | ((diff_p > 0) & sells(1:end-1)) ... % ou bien on suivait une s?quence d'ordres de vente et le prix suivant est sup?rieur
            | ((diff_p < 0) & ~sells(1:end-1)) ... % ou bien on suivait une s?quence d'ordres d'achat et le prix suivant est inf?rieur
            | logical(diff(td_id)) ...
            | any(diff(auctions_indic), 2);
        
        seq_begins = [true; seq_begins];
        num_seq    = cumsum(seq_begins);
        
        acc_volume = accumarray(num_seq, volume);
        acc_price  = accumarray(num_seq, volume.*price) ./ acc_volume;
        
        acc_bid            = prices_ob{2}(seq_begins);
        acc_ask            = prices_ob{1}(seq_begins);
        acc_sell           = sells(seq_begins);
        acc_auctions_indic = auctions_indic(seq_begins, :);
        acc_td_id          = td_id(seq_begins);
        acc_bid_size       = sizes{2}(seq_begins);
        acc_ask_size       = sizes{1}(seq_begins);
        dts                = data.date(seq_begins);
        
        data.date  = dts;
        data.value = [acc_price,acc_volume,acc_bid,acc_ask,acc_sell, ...
            acc_auctions_indic,acc_td_id,acc_bid_size,acc_ask_size];
    case 'mo_tick'
        data = get_tick( 'filter-td', varargin{:}, ...
            'initial-colnames', 'price;volume;bid;ask;bid_size;ask_size;trading_destination_id;auction',...
            'final-colnames', {}, 'where_f-td', '~({auction}|{trading_at_last}|{cross}|{trading_after_hours})');
        % data = st_data('from-idx', data, mod(data.date, 1) > datenum(0,0,0,11,49,58.8) & mod(data.date, 1) < datenum(0,0,0,12,1,0)); % permettait de faire un test
        % d'aggragation sur total 19/06/2009 ? une p?riode o? il y beaucoup
        % d'aggr?gations ? faire et pour laquelle la m?thode na?ve
        % ci-dessus fonctionnait
        if st_data('isempty-nl', data)
            return
        end
        idx_td = find(strcmp('trading-destinations', varargin(1:2:end)));
        if isempty(varargin{2*idx_td})
            error('get_tick:mo_tick:exec', 'You have to specify only one trading-destination');
        end
        
        epsilon =  0.00001; %Magic Number :  used to round prices given by Sybase
        dt = datenum(0,0,0,0,0,1) * 15/1000; % 15 millisecond
        
        price     = round(st_data('col', data, 'price') / epsilon) * epsilon;
        volume    = st_data('col', data, 'volume');
        
        sizes = {st_data('col', data, 'ask_size');st_data('col', data, 'bid_size')};
        prices_ob = {round(st_data('col', data, 'ask') / epsilon) * epsilon;round(st_data('col', data, 'bid') / epsilon) * epsilon};
        secs    = data.date;
        
        seq_begins =  ... logical(diff(sells)) ... % ou bien on change de c?t? du carnet
            (diff(secs) > dt)... % Ou bien l'horodatage permet de conclure que ce n'est pas le m?me ordre agressif
            | any(diff([sizes{1}, sizes{2}, prices_ob{1}, prices_ob{2}]), 2); % Ou bien le carnet d'ordre a ?t? updat? entre les deux transactions, ce qui n'est pas le cas s'il s'agit du m?me ordre march?
        seq_begins = [true; seq_begins];
        
        diff_p  = [0;diff(price)];
        
        for i = 1 : length(seq_begins)
            % curr_moving_side sera 1 si le prix est en train de monter, -1
            % s'il descend, 0 si on ne sait pas encore
            if seq_begins(i)
                curr_moving_side = 0;
            else
                if curr_moving_side == 1 && diff_p(i) <= -epsilon  % le prix montait (ou stagnait mais a mont? dans la s?quence) et maintenant il baisse
                    curr_moving_side = -1;
                    seq_begins(i) = 1;
                elseif curr_moving_side == -1 && diff_p(i) >= epsilon % le prix baissait (ou stagnait mais a baiss? dans la s?quence) et maintenant il monte
                    curr_moving_side = 1;
                    seq_begins(i) = 1;
                elseif curr_moving_side == 0 % on ne connaissait pas encore le sens de la s?quence et on en est d?j? au deuxi?me ?l?ment de la s?quence
                    if diff_p(i) >= epsilon
                        curr_moving_side = 1;
                    elseif diff_p(i) <= -epsilon
                        curr_moving_side = -1;
                    end
                end
            end
        end
        
        num_seq    = cumsum(seq_begins);
        
        acc_volume = accumarray(num_seq, volume);
        acc_price  = accumarray(num_seq, volume.*price) ./ acc_volume;
        
        acc_bid            = prices_ob{2}(seq_begins);
        acc_ask            = prices_ob{1}(seq_begins);
        acc_bid_size       = sizes{2}(seq_begins);
        acc_ask_size       = sizes{1}(seq_begins);
        dts                = data.date(seq_begins);
        
        data.date          = dts;
        seq_num_begin      = find(seq_begins);
        data.value = [acc_price,acc_volume,acc_bid,acc_ask, ...
            acc_bid_size,acc_ask_size,price(seq_begins), ...
            [price(seq_num_begin(2:end)-1);price(end)]];
        data.value(:, end+1:end+3) = [data.value(:, 1)-data.value(:, end-1), data.value(:, end)-data.value(:, end-1),  1 + accumarray(num_seq, ~seq_begins)];
        data.colnames = {'price', 'volume', 'bid', 'ask', 'bis_size', 'ask_size', ...
            'first_price_of_seq', 'last_price_of_seq', 'average-first', 'last-first', 'nb_trades_4_this_mo'};
        % [max_pi, idx] = sort(st_data('cols', data, 'last-first'));
        % worst_idx = idx(end-9:end);
        % datestr(data.date(worst_idx), 'HH:MM:SS.FFF');
        % data2lookat = st_data('from-idx', data, worst_idx);
        % st_data('cols', data2lookat, 'last-first')
    case 'spread and tick'
        idx_td = strmatch('trading-destinations', varargin(1:2:end), 'exact');
        if isempty(varargin{2*idx_td})
            warning('get_tick:exec', ['Be aware that the tick size may not be the same on different trading-destinations.\n'...
                'In order to prenvent from analysing excessively high spread, only the main market and CHI-X will be kept (but this will be done with the dirty method td_id < 80)\n']);
        end
        
        idx_day = strmatch('day', varargin(1:2:end), 'exact');
        idx_from = strmatch('from', varargin(1:2:end), 'exact');
        varargin{2*idx_from} = varargin{2*idx_day};
        idx_to = strmatch('to', varargin(1:2:end), 'exact');
        varargin{2*idx_to} = varargin{2*idx_day};
        
        
        
        MY_EPS = 1e-8;
        LIST_MULT = [2 5 10 20 50 100];
        TRESHOLD = 0.995;
        
        
        data = st_data('where', get_tick( 'tick4bi', varargin{:}, 'last_formula', '', 'info_field_to_st_data', '', 'output-mode', 'all'), '~{auction}');
        if st_data('isempty-nl', data)
            return;
        end
%         data = st_data('where', data, '{trading_destination_id}<80');
        if st_data('isempty-nl', data)
            return;
        end
        data   = st_data('where', data, '{bid}<{ask}&{bid}~=0&({bid}>={price}|{ask}<={price})');
        exclude_till = ceil(length(data.date)/100);
        
        data = st_data('from-idx', data, (1:length(data.date))'>exclude_till);
        tmp = st_data('cols', data, 'ask;bid');
        tmp = tmp(1:end);
        tmp(tmp <= MY_EPS) = [];
        
        if isempty(tmp)
            data = st_data('log', data, 'gieo', 'No reliable quotes in data');
            return;
        end
        
        tick_size = MY_EPS*pgcd(round(tmp/MY_EPS));
        ld = length(data.date);
        
        data.info.excluded_prop = 1-length(data.date)/ld;
        %         for j = 1 : length(data)
        if ~st_data('isempty-nl', data)
            spread = full(st_data('apply-formula', data, 'max(abs({ask}-{price}),abs({bid}-{price}))', 'output', 'value'));
            data   = st_data('add-col', data, spread, 'spread');
            
%             ;
%             
%             spread(1:exclude_till) = [];
            
            nbts = round(spread / tick_size);
            
            distrib = accumarray(nbts, ones(length(tick_size), 1), [], @sum);
%             distrib = distrib / sum(distrib);
            
            mult = 1;
            for i = 1 : length(LIST_MULT)
                if sum(distrib(LIST_MULT(i):LIST_MULT(i):end))/ sum(distrib) > TRESHOLD
                    mult = LIST_MULT(i);
                    break;
                end
            end
            
            if mult ~= 1
                tick_size = tick_size * mult;
                idx2del = abs(round(spread/tick_size)*tick_size - spread) > MY_EPS;
                if any(idx2del)
                    warning('get_tick:spread_and_tick size:deleting <%d> deal among <%d> because it is not consistent.', sum(idx2del), length(idx2del));
%                     idx2del = exclude_till+find(idx2del);
                    data.date(idx2del) = [];
                    data.value(idx2del, :) = [];
                    if isfield(data, 'rownames')
                        data.rownames(idx2del) = [];
                    end
                end
                
                spread = st_data('cols', data, 'spread');
                data.info.excluded_prop2 = sum(idx2del)/length(idx2del);
%                 spread(1:exclude_till) = [];
                
%                 distrib = distrib / sum(distrib);
            else
                data.info.excluded_prop2 = 0;
            end
            
            nbts = round(spread / tick_size);
                
            distrib = accumarray(nbts, ones(length(tick_size), 1), [], @sum);
            
            data.info.min_step_price = tick_size;
            
            data.info.nbts_distrib = distrib;
            
            spread = st_data('cols', data, 'spread');
            volume = st_data('cols', data, 'volume');
            
            nbts = round(spread / tick_size);
            
            data.info.nb_ts_spread_vw_distrib = accumarray(nbts, volume, [], @sum) / sum(volume);
        end
        %         end
    case 'filter-td'
        %<* intra day data
        opt   = options( { 'source', '',  'trading-destinations', {}, ...
            'initial-colnames', {}, 'final-colnames', {}, 'where_f-td', '', 'binary', 0,...
            'trade_side_definition', 'strict'}, varargin);
        where_f_td = opt.get('where_f-td');
        formula = tokenize(opt.get('initial-colnames'));
        formula = sprintf('{%s},', formula{:});
        formula = sprintf('[%s]', formula(1:end-1));
        if strcmp(formula, '[]')
            formula = '';
        end
        opt.set('last_formula', formula);
        opt.set('where_formula', where_f_td);
        lst = opt.get();
        kodes = get_repository( 'any-to-sec-td', lst{:});
        code  = kodes.security_key;
        sid   = kodes.security_id;
        o_tdi = kodes.trading_destination_id;
        opt.set('security_id', sid);
        st_log('get_tick:data looking for data for <%s>...\n', code);
        lst   = opt.get();
        if ~st_version.my_env.switch_full_tick2tbt2
            data = get_tick('ft', lst{:});
            if ~isempty(where_f_td)
                data = cellfun(@(d)st_data('where', d, where_f_td),data,'uni',false);
            end
            if ~isempty(formula)
                data = cellfun(@(d)st_data('apply-formula', d, formula),data,'uni',false);
            end
        else
            data = get_tickdb2( 'ft', lst{:});
        end
        if isempty( data)
            return
        end
        % filter on trading destinations
        if ~iscell( data)
            no_cell = true;
            data = { data };
        else
            no_cell = false;
        end
        warn = false;
        for c=1:length(data)
            if st_data('isempty-no_log', data{c})
                % nothing to do
            else
                % On note le place_id initial car les donn?es sont
                % timestamp?es en heures locales de cette place
                main_timezone = data{c}.info.td_info(1).timezone;
                % fitrage du .info.td_info
                all_td_info      = [data{c}.info.td_info.trading_destination_id];
                if length( all_td_info) < length( o_tdi)
                    if ~warn
                        st_log('get_tick:td - your data and the DB..repository does not agree about available TD for sec id <%d>\n', opt.get('security_id'));
                    end
                    warn = true;
                    data{c}.info.td_info = get_repository( 'trading-destination-info', opt.get('security_id'));
                end
                try
                    keep_td_info_idx = arrayfun(@(td_)find(td_==all_td_info), o_tdi, 'uni', true);
                    % retrouver les trading destination demand?es dans td_info
                    data{c}.info.td_info = data{c}.info.td_info(keep_td_info_idx);
                catch ME
                    keep_td_info_idx = arrayfun(@(td_)find(td_==all_td_info), o_tdi, 'uni', false);
                    keep_td_info_idx = [keep_td_info_idx{:}];
                    data{c}.info.td_info = data{c}.info.td_info(keep_td_info_idx);
                end
                % filtrage des donn?es
                td_vals  = st_data('col', data{c}, 'trading_destination_id');
                idx_keep = ismember( td_vals, o_tdi);
                data{c}.value = data{c}.value(idx_keep,:);
                data{c}.date  = data{c}.date(idx_keep);
                if isfield( data{c}, 'rownames')
                    data{c}.rownames = data{c}.rownames(idx_keep);
                end
                icolnames = opt.get('initial-colnames');
                fcolnames = opt.get('final-colnames');
                % apr?s avoir filtr? les donn?es, si la trading_destination
                % principale n'a pas ?t? conserv?e, on peut avoir besoin de
                % changer l'heure
                if ~isempty(keep_td_info_idx) && ~any(keep_td_info_idx ==1)
                    data{c}.date = timezone('convert','initial_timezone',main_timezone,...
                        'final_timezone', data{c}.info.td_info(1).timezone, 'dates', data{c}.date);
                end
                if ~isempty( icolnames)
                    %< Filtrage des colnames
                    data{c} = st_data('keep', data{c}, icolnames);
                    if ~isempty( fcolnames)
                        if ischar( fcolnames)
                            fcolnames = tokenize( fcolnames, ';');
                        end
                        data{c}.colnames = fcolnames;
                    end
                    %>
                end
            end
            
            overb_col = cellfun(@(c) strcmpi(c, 'over_bid'), data{c}.colnames);
            overa_col = cellfun(@(c) strcmpi(c, 'over_ask'), data{c}.colnames);
                
            if strcmpi(opt.get('trade_side_definition'), 'all') && any(overb_col>0) && any(overa_col>0)
                MIN_PRICE_STEP = 1e-6;
                bid = st_data('cols', data{c}{1}, 'bid');
                ask = st_data('cols', data{c}{1}, 'ask');
                prc = st_data('cols', data{c}{1}, 'price');
                
                overb = (round((bid+ask)/MIN_PRICE_STEP) > round(2*prc/MIN_PRICE_STEP)) && ((ask-bid)>0);
                overa = (round((bid+ask)/MIN_PRICE_STEP) < round(2*prc/MIN_PRICE_STEP)) && ((ask-bid)>0);
                
                data{c}.value(:, overb_col) = overb;
                data{c}.value(:, overa_col) = overa;
            end
        end
        if no_cell
            data = data{1};
        end
        %>*
    case 'masterkey'
        %<* Return the generic+specific dir
        fmode = varargin{1};
        fmode = regexprep( fmode, '[ |*|\\|/|:|?|"|<|>|\|]', '_');
        opt = options({'security_id', NaN}, varargin(2:end));
        sec_id = opt.get('security_id');
        if ~isnumeric( sec_id) && numel(sec_id)==1
            error('get_tick:secid', 'I need an unique numerical security id');
        end
        if strcmp(varargin{1}, 'spread and tick')
            td_id = opt.get('trading-destinations');
            if (iscell(td_id) && strcmpi(td_id{1},'MAIN')) || (ischar(td_id) && strcmpi(td_id,'MAIN'))
                td_id = get_repository('maintd',sec_id);
            end
            key = get_repository('kech-security-td-key',sec_id,td_id);
            data = fullfile(fmode,key);
        else
            data = fullfile(fmode, get_repository( 'kech-security-key', sec_id));
            %data = fullfile(fmode, get_repository( 'security-key', sec_id));
        end
        %>*
        
    case 'ft'
        % HARD CODED VARIABLES
        BATCH_SIZE = 20;
        convert_date = @(date)datestr(datenum(date,'dd/mm/yyyy'),'yyyy-mm-dd');
        
        % Variables ini
        data = [];
        opt = options({'security_id',[],'from','','to',''},varargin);
        security_id = unique(opt.get('security_id'));
        if isempty(security_id)
            return
        end
        sec_str = sprintf('%d,',security_id);
        sec_str(end) = [];
        from = opt.get('from');
        if isempty(from)
            return
        end
        to = opt.get('to');
        if isempty(to)
            return
        end
        from = convert_date(from);
        to = convert_date(to);
        
        % Core of the code
        q = sprintf(['select Id, StampDate, SecurityId, TradeDate, File',...
            ' from %s.%s where SecurityId in (%s) and TradeDate between ''%s'' and ''%s'''],...
            st_version.bases.mat_file_db,st_version.tables.mat_file_table,...
            sec_str,from,to);
        res = read_mysql_matfile(q);
        date = (datenum(from,'yyyy-mm-dd'):datenum(to,'yyyy-mm-dd'))';
        date_security_id = [repmat(reshape(security_id,[],1),length(date),1),repmat(date,length(security_id),1)];
        is_data = joint([cell2mat(res(:,3)),datenum(res(:,4),'yyyy-mm-dd')],...
            true(size(res,1),1),date_security_id(between(weekday(date_security_id(:,2)),2,6),:));
        if any(isnan(is_data))
            idx = between(weekday(date_security_id(:,2)),2,6);
            idx(idx) = isnan(is_data);
            error('get_tick:ft','No data in QuantMatFile for security_id <%s> on date(s) <%s>',...
                join(', ',unique(date_security_id(idx,1))),...
                join(', ',cellstr(datestr(unique(date_security_id(idx,2)),'dd/mm/yyyy'))))
        end
        data = cell(length(date),length(security_id));
        for i = 1:length(security_id)
            data(:,i) = joint(datenum(res(cell2mat(res(:,3))==security_id(i),4),'yyyy-mm-dd'),...
                res(cell2mat(res(:,3))==security_id(i),5),date);
            for j = 1:size(data,2)
                has_to_map = kech_td_mapping_needed(data{j,i});
                if has_to_map
                    data{j,i} = st_data_get_trading_destination_mapping(data{j,i});
                end
            end
        end
        data(cellfun('isempty',data)) = {st_data('empty-init')};

    case 'ob'
        % TODO : something fully fonctionnal, which is able to write the stored_prodeure to be used.
        % Mais il est plus sage d'attendre les serveurs IQ qui permettront d'acc?der aux donn?es de n'importe quelle p?riode sans se soucier de la base dans laquelle elles sont stock?es
        % Il faudra aussi ?crire un mode permettant de synchroniser ces
        % donn?es avec les donn?es de type deal gr?ce au deal_id
        opt = options({'security', 'FTE.PA', 'from', '11/01/2008', 'to', '11/01/2008', ...
            'trading_destination_id', 4, 'date_format:char', 'dd/mm/yyyy', ...
            'conn_chain', {'batch_tick', 'batick'}}, varargin);
        try
            td_id  = opt.get('td_info');
            sec_id = opt.get('security_id');
        catch er
            error('get_tick:ob', 'You should not reach this point, report to clehalle or rburgot');
        end
        date_format = opt.get('date_format:char');
        date_from = opt.get('from');
        if ~isnumeric( date_from)
            date_from = datenum(date_from, date_format);
        end
        date_to = opt.get('to');
        if ~isnumeric( date_to)
            date_to   = datenum( date_to, date_format);
        end
        if (date_from ~= date_to)
            error('get_tick:ob', 'You are not allowed to ask for more than one day');
        elseif date_from < datenum(2007, 2, 1, 0, 0, 0) % TODO this should be dynamic... but the stored procedure is not dynamic either, so...
            error('get_tick:ob', 'Ordebook not yet implemented for this period');
        end
        c = userdatabase('sybase:j', 'BSIRIUS');
        if date_from < 733499
            sp_name = 'tmp_get_orderbook';
        else
            sp_name = 'tmp_get_orderbook_2';
            %vals = exec_sql(c, sprintf('exec temp_works..tmp_get_orderbook_2 %d, %d, ''%s''', ids(2), ids(1), datestr(date_to, 'yyyymmdd')));
        end
        st_log('retrieving <%s> orderbooks...\n', td_id{1,4});
        vals = exec_sql(c, sprintf('exec temp_works..%s %d, %d, ''%s''', sp_name, sec_id, td_id{1,1}, datestr(date_to, 'yyyymmdd')));
        vals_td = repmat(td_id{1,1}, size(vals,1),1);
        for td=2:size(td_id,1)
            st_log('retrieving <%s> orderbooks...\n', td_id{td,4});
            tals = exec_sql(c, sprintf('exec temp_works..%s %d, %d, ''%s''', sp_name, sec_id, td_id{td,1}, datestr(date_to, 'yyyymmdd')));
            vals = cat(1, vals, tals);
            vals_td = cat(1, vals_td, repmat(td_id{td,1}, size(tals,1),1));
        end
        if isempty(vals)
            data = [];
            return;
        end
        try
            dts  = date_to + mod(datenum( vals(:,1), 'HH:MM:SS'),1);
        catch
            lasterr('');
            dts  = date_to + mod(datenum( vals(:,1), 'yyyy-mm-dd HH:MM:SS.FFF'),1);
        end
        vals = [cellfun(@double, vals(:,2:end)), vals_td];
        idx_to_delete = any(isnan(vals), 2);
        prop_delete = mean(idx_to_delete);
        if prop_delete > 0
            warning('get_tick:ob', 'Deleting %.2f%% of the trades, as they have no orderbook data', 100 * prop_delete);
            vals(idx_to_delete, :) = [];
            if prop_delete > 0.5
                error('get_tick:ob', 'Too much missing value');
            end
        end
        data  = st_data('init', 'title', [ opt.get('security') ' Orderbook'], 'date', dts, 'info', struct('td_info', td_id), ...
            'value', vals, 'colnames', { 'deal_id', ...
            ...
            'Bid number 5', 'Bid size 5', 'Bid price 5', ...
            'Bid number 4', 'Bid size 4', 'Bid price 4', ...
            'Bid number 3', 'Bid size 3', 'Bid price 3', ...
            'Bid number 2', 'Bid size 2', 'Bid price 2', ...
            'Bid number 1', 'Bid size 1', 'Bid price 1', ...h
            ...
            'Ask number 1', 'Ask size 1', 'Ask price 1', ...
            'Ask number 2', 'Ask size 2', 'Ask price 2', ...
            'Ask number 3', 'Ask size 3', 'Ask price 3', ...
            'Ask number 4', 'Ask size 4', 'Ask price 4', ...
            'Ask number 5', 'Ask size 5', 'Ask price 5', ...
            'Trading destination' });
    case 'daily'
        %<* Read daily data
        opt = options({'security', 'EDF.PA', 'from','02/04/2007','to','02/04/2007', 'format_date', 'dd/mm/yyyy', 'db-connection', {'batch_tick','batick' } }, ...
            varargin(1:end));
        sec = opt.get('security');
        conn_chain = opt.get('db-connection');
        format_date = opt.get('date_format:char');
        frm = datenum(opt.get('from'), format_date);
        too   = datenum(opt.get('to'  ), format_date);
        if (frm ~= too)
            error('get_tick:daily', 'You are not allowed to ask for more than one day');
        end
        c = userdatabase('sybase:j', 'BSIRIUS');
        ids = get_td_and_sec_id_from_ric(sec);
        vals = exec_sql(c, sprintf( [ ...
            'select  date, trading_destination_id,volume, turnover, open_prc, high_prc, low_prc, close_prc, open_turnover, close_turnover, nb_deal, ' ...
            '        open_volume, close_volume,average_spread_numer, average_spread_denom  ' ...
            '   from ' tick_db_name '..trading_daily td ' ...
            '   where security_id = %d and trading_destination_id = %d and date = ''%s''' ...
            ], ids(2), ids(1), datestr(frm, 'yyyymmdd')));
        if ~isempty( vals)
            nvals = cell2mat(vals(:,2:end));
            data = st_data('init', 'date', datenum( vals(:,1), 'yyyy-mm-dd'), ...
                'value', [nvals(:,1:end-2), nvals(:,end-1)./nvals(:,end)] , 'colnames', { 'Trading_destination','Volume', 'Turnover', 'Open', 'High', 'Low', 'Close', 'Open (turnover)', 'Close (turnover)', 'Nb deals', 'Open volume', 'Close volume', 'Mean spread'}, ...
                'title', sec);
        else
            data = [];
        end
    otherwise
        if strcmpi(mode(1:3), 'gi:')
            graphname = mode(4:end);
            
        end
        error('get_tick', 'mode <%s> unknown', mode);
end



end






%--------------------------------------------------------------------------
%----- OTHER FUNCTION
%--------------------------------------------------------------------------

function out=add_col_tick_size(data)

[data b_data]=st_data('isempty',data);
out=data;

if b_data
    return
end

tick_size_val=NaN(size(data.value,1),1);

if any(ismember(data.colnames,'trading_destination_id')) && ...
        isfield(data,'info') && isfield(data.info,'td_info') && ...
        (~isempty(data.info.td_info) && isfield(data.info.td_info(1),'price_scale'))
    
    td_id_val=st_data('col',data,'trading_destination_id');
    price_val=st_data('col',data,'price');
    uni_td_id=unique(td_id_val);
    all_td_in_tdinfo=[data.info.td_info.trading_destination_id];
    
    for i_t=1:length(uni_td_id)
        id_in_tdinfo=ismember(all_td_in_tdinfo,uni_td_id(i_t));
        if any(id_in_tdinfo)
            tick_size_rule_matrix=data.info.td_info(id_in_tdinfo).price_scale;
            idx_in_val=find(td_id_val==uni_td_id(i_t));
            
            if ~isempty(tick_size_rule_matrix)
                nb_class_tick_size=size(tick_size_rule_matrix,1);
                if nb_class_tick_size==1
                    tick_size_val(idx_in_val)=tick_size_rule_matrix(2);
                else
                    for i_class=1:nb_class_tick_size-1
                        id_tmp=tick_size_rule_matrix(i_class,1)<=price_val(idx_in_val) &...
                            price_val(idx_in_val)<tick_size_rule_matrix(i_class+1,1);
                        if any(id_tmp)
                            tick_size_val(idx_in_val(id_tmp))=tick_size_rule_matrix(i_class,2);
                        end
                    end
                    id_tmp_end=price_val(idx_in_val)>=tick_size_rule_matrix(end,1);
                    if any(id_tmp_end)
                        tick_size_val(idx_in_val(id_tmp_end))=tick_size_rule_matrix(end,2);
                    end
                end
            end
        end
        
    end
end

out=st_data('add-col',out,tick_size_val,'tick_size');

end


function str = join(separator,list)
if isempty(list)
    str = '';
    return
end
if isnumeric(list)
    str = sprintf(['%d',separator],list);
elseif iscellstr(list)
    str = sprintf(['%s',separator],list{:});
else
    error('join:wronginputargs','Second argument should be either numeric or cellstr')
end
str(end-length(sprintf(separator))+1:end) = [];
end