function data = get_tick(mode, varargin)
% GET_TICK - Requesting and formatting tick by tick deal data
%
% % < Unit test
% data = get_tick('ft', 'security_id', 110, 'day', '17/05/2013' ); assert(isempty(data) || unique(floor(data.date))==datenum(2013,5,17))
% data = get_tick('ft', 'security_id', 110, 'day', '21/05/2013' ); assert(isempty(data) || unique(floor(data.date))==datenum(2013,5,21))
% data = get_tick('ft', 'security_id', 252268, 'day', '20/05/2013' ); assert(isempty(data) || unique(floor(data.date))==datenum(2013,5,20))
% data = get_tick('ft', 'security_id', 313484, 'day', '16/05/2013' ); assert(isempty(data) || unique(floor(data.date))==datenum(2013,5,16))
% data = get_tick('ft', 'security_id', 63345, 'day', '18/05/2013' ); assert(isempty(data) || unique(floor(data.date))==datenum(2013,5,18))
% 
% % > Unit test
% 
% internal uses:
% - get_tick('masterkey', 'ft', 'security_id', 110 )
% - get_tick('ft', 'security_id', 110, 'day', '01/04/2008' )
% - get_tick('btickdb', 'security_id', 'FTE', 'from', '01/04/2008', 'to', '30/04/2008' , 'source', 'topaggr', 'trading-destinations', {'main', 'chix'} )
%
% Examples :
%
% % Todays data : no buffer
% data = get_tick('filter-td', 'security_id', 110, 'from',today, 'to', today, 'trading-destinations', {})
%
% % Classical calls through read_dataset with bufferization 
% data = read_dataset('btickdb', 'security_id',  110, 'from', '01/04/2008', 'to', '01/04/2008' , 'trading-destinations', {4, 51} )
% data = read_dataset('mo_tick', 'security_id', 110, 'from', '02/02/2009', 'to', '02/02/2009','trading-destinations', {'MAIN'} )
% data = read_dataset('spread and tick', 'security_id', 6249, 'from', '31/07/2009', 'to', '31/07/2009','trading-destinations', {'CHIX'} )
% data = read_dataset('spread and tick', 'security_id', 6249, 'from', '31/07/2009', 'to', '31/07/2009','trading-destinations', {'MAIN'} )
% 
%
% See also read_dataset get_repository from_buffer get_basename4day
% author   : 'rburgot@cheuvreux.com'
% reviewer : 'clehalle@cheuvreux.com'
% version  : '1'
% date     : '20/08/2008'
% 
%   last_checkin_info : $Header: get_tick.m: Revision: 64: Author: robur: Date: 05/09/2013 10:23:47 AM$
global st_version;

tick_db_name = st_version.bases.market_data;

switch mode
    case 'tbt2-deals-intraday'
        opt = options({'security', 'non sp?cifi?', 'day','??/??/????', 'format_date', 'dd/mm/yyyy','tradingDestinationId' , 4 ,'dir', 'Y:\tick_ged\','tradesOnly', 1}, ...
            varargin(1:end));
        if (opt.get( 'tradesOnly' ) == 0 )
            st_log('get_tick:For order book data, please use the mode ''tbt2-deals-ob-intraday'' and not the flag ''tradesOnly''');
            data = st_data('empty');
        else
            lst = opt.get();
            data=get_orderbook('read', lst{:});
        end
    case 'tbt2-deals-ob-intraday'
        opt = options({'security', 'non sp?cifi?', 'day','??/??/????', 'format_date', 'dd/mm/yyyy','tradingDestinationId' , 4 ,'dir', 'Y:/tick_ged/','tradesOnly', 0}, ...
            varargin(1:end));
        if (opt.get( 'tradesOnly' ) == 1 )
            st_log('get_tick:For trade data only, please use the mode ''tbt2-deals-intrada'' and not the flag ''tradesOnly''');
            data = st_data('empty');
        else
            lst = opt.get();
            [deals,obs]=get_orderbook('read', lst{:});
            data{1} = deals;
            data{2} = obs;
        end
    case 'btickdb'
        data = get_tick( 'filter-td', varargin{:}, ...
            'initial-colnames', 'price;volume;sell;bid;ask;bid_size;ask_size;trading_destination_id', 'final-colnames', {}, ...
            'where_f-td', '~({auction}|{trading_at_last}|{cross}|{trading_after_hours})');
    case 'tick4bi'
        data = get_tick( 'filter-td', varargin{:}, ...
            'initial-colnames', ['price;volume;bid;ask;sell;'...
            'auction;opening_auction;closing_auction;intraday_auction;trading_destination_id;bid_size;ask_size;buy'],...
            'final-colnames', {}, 'where_f-td', '~({trading_at_last}|{cross}|{trading_after_hours})');
    case 'tick4simap'
        data = get_tick( 'filter-td', varargin{:}, ...
            'initial-colnames', ['price;volume;bid;ask;sell;'...
            'auction;opening_auction;closing_auction;intraday_auction;trading_destination_id;bid_size;ask_size;buy;dark;deal_id'],...
            'final-colnames', {}, 'where_f-td', '~({trading_at_last}|{cross}|{trading_after_hours})');
        %-- add of columns 'tick_size'
        data=add_col_tick_size(data);
    case 'tick4cfv'
        data = get_tick( 'filter-td', varargin{:}, ...
            'initial-colnames', ['price;volume;bid;ask;sell;'...
            'auction;opening_auction;closing_auction;intraday_auction;trading_destination_id;buy'],...
            'final-colnames', {}, 'where_f-td', '~({trading_at_last}|{trading_after_hours})');
    case 'orig_mo_tick' % can work on Euronext from march 2009 to septmeber 2009 but otherwise it relies too much on the order book as well as overbid
        data = get_tick( 'filter-td', varargin{:}, ...
            'initial-colnames', ['price;volume;bid;ask;sell;'...
            'auction;opening_auction;closing_auction;intraday_auction;trading_destination_id;bid_size;ask_size;buy'],...
            'final-colnames', {}, 'where_f-td', '~({auction}|{trading_at_last}|{cross}|{trading_after_hours})');
        % data = st_data('from-idx', data, mod(data.date, 1) > datenum(0,0,0,11,49,58.8) & mod(data.date, 1) < datenum(0,0,0,12,1,0)); % permettait de faire un test
        % d'aggragation sur total 19/06/2009 ? une p?riode o? il y beaucoup
        % d'aggr?gations ? faire
        if isempty(data)
            return;
        end
        idx_td = strmatch('trading-destinations', varargin(1:2:end));
        if isempty(varargin{2*idx_td})
            error('get_tick:mo_tick:exec', 'You have to specify only one trading-destination');
        end
        
        epsilon =  0.00001; %Magic Number :  used to round prices given by Sybase
        sec_dt = datenum(0,0,0,0,0,1) / 5;
        
        sells     = st_data('col', data, 'sell');
        price     = round(st_data('col', data, 'price') / epsilon) * epsilon;
        volume    = st_data('col', data, 'volume');
        
        sizes = {st_data('col', data, 'ask_size');st_data('col', data, 'bid_size')};
        prices_ob = {round(st_data('col', data, 'ask') / epsilon) * epsilon;round(st_data('col', data, 'bid') / epsilon) * epsilon};
        secs    = data.date;
        diff_p  = diff(price);
        auctions_indic = st_data('col', data, 'auction;opening_auction;closing_auction;intraday_auction');
        td_id = st_data('col', data, 'trading_destination_id');
        
        seq_begins =  logical(diff(sells)) ... % ou bien on change de c?t? du carnet
            | (diff(secs) > sec_dt)... % Ou bien l'horodatage permet de conclure que ce n'est pas le m?me ordre agressif
            | any(diff([sizes{1}, sizes{2}, prices_ob{1}, prices_ob{2}]), 2)... % Ou bien le carnet d'ordre a ?t? updat? entre les deux transactions, ce qui n'est pas le cas s'il s'agit du m?me ordre march?
            ... % Les deux lignes suivantes ont pour seule fonction de s?parer les ordres stop de l'ordre march? qui les a d?clench?s par son ?x?cution. Je ne me souviens plus si cela est n?cessiare
            | ((diff_p > 0) & sells(1:end-1)) ... % ou bien on suivait une s?quence d'ordres de vente et le prix suivant est sup?rieur
            | ((diff_p < 0) & ~sells(1:end-1)) ... % ou bien on suivait une s?quence d'ordres d'achat et le prix suivant est inf?rieur
            | logical(diff(td_id)) ...
            | any(diff(auctions_indic), 2);
        
        seq_begins = [true; seq_begins];
        num_seq    = cumsum(seq_begins);
        
        acc_volume = accumarray(num_seq, volume);
        acc_price  = accumarray(num_seq, volume.*price) ./ acc_volume;
        
        acc_bid            = prices_ob{2}(seq_begins);
        acc_ask            = prices_ob{1}(seq_begins);
        acc_sell           = sells(seq_begins);
        acc_auctions_indic = auctions_indic(seq_begins, :);
        acc_td_id          = td_id(seq_begins);
        acc_bid_size       = sizes{2}(seq_begins);
        acc_ask_size       = sizes{1}(seq_begins);
        dts                = data.date(seq_begins);
        
        data.date  = dts;
        data.value = [acc_price,acc_volume,acc_bid,acc_ask,acc_sell, ...
            acc_auctions_indic,acc_td_id,acc_bid_size,acc_ask_size];
    case 'mo_tick'
        data = get_tick( 'filter-td', varargin{:}, ...
            'initial-colnames', 'price;volume;bid;ask;bid_size;ask_size;trading_destination_id;auction',...
            'final-colnames', {}, 'where_f-td', '~({auction}|{trading_at_last}|{cross}|{trading_after_hours})');
        % data = st_data('from-idx', data, mod(data.date, 1) > datenum(0,0,0,11,49,58.8) & mod(data.date, 1) < datenum(0,0,0,12,1,0)); % permettait de faire un test
        % d'aggragation sur total 19/06/2009 ? une p?riode o? il y beaucoup
        % d'aggr?gations ? faire et pour laquelle la m?thode na?ve
        % ci-dessus fonctionnait
        if st_data('isempty-nl', data)
            return;
        end
        idx_td = strmatch('trading-destinations', varargin(1:2:end));
        if isempty(varargin{2*idx_td})
            error('get_tick:mo_tick:exec', 'You have to specify only one trading-destination');
        end
        
        epsilon =  0.00001; %Magic Number :  used to round prices given by Sybase
        dt = datenum(0,0,0,0,0,1) * 15/1000; % 15 millisecond
        
        price     = round(st_data('col', data, 'price') / epsilon) * epsilon;
        volume    = st_data('col', data, 'volume');
        
        sizes = {st_data('col', data, 'ask_size');st_data('col', data, 'bid_size')};
        prices_ob = {round(st_data('col', data, 'ask') / epsilon) * epsilon;round(st_data('col', data, 'bid') / epsilon) * epsilon};
        secs    = data.date;
        
        seq_begins =  ... logical(diff(sells)) ... % ou bien on change de c?t? du carnet
            (diff(secs) > dt)... % Ou bien l'horodatage permet de conclure que ce n'est pas le m?me ordre agressif
            | any(diff([sizes{1}, sizes{2}, prices_ob{1}, prices_ob{2}]), 2); % Ou bien le carnet d'ordre a ?t? updat? entre les deux transactions, ce qui n'est pas le cas s'il s'agit du m?me ordre march?
        seq_begins = [true; seq_begins];
        
        diff_p  = [0;diff(price)];
        
        for i = 1 : length(seq_begins)
            % curr_moving_side sera 1 si le prix est en train de monter, -1
            % s'il descend, 0 si on ne sait pas encore
            if seq_begins(i)
                curr_moving_side = 0;
            else
                if curr_moving_side == 1 && diff_p(i) <= -epsilon  % le prix montait (ou stagnait mais a mont? dans la s?quence) et maintenant il baisse
                    curr_moving_side = -1;
                    seq_begins(i) = 1;
                elseif curr_moving_side == -1 && diff_p(i) >= epsilon % le prix baissait (ou stagnait mais a baiss? dans la s?quence) et maintenant il monte
                    curr_moving_side = 1;
                    seq_begins(i) = 1;
                elseif curr_moving_side == 0 % on ne connaissait pas encore le sens de la s?quence et on en est d?j? au deuxi?me ?l?ment de la s?quence
                    if diff_p(i) >= epsilon
                        curr_moving_side = 1;
                    elseif diff_p(i) <= -epsilon
                        curr_moving_side = -1;
                    end
                end
            end
        end
        
        num_seq    = cumsum(seq_begins);
        
        acc_volume = accumarray(num_seq, volume);
        acc_price  = accumarray(num_seq, volume.*price) ./ acc_volume;
        
        acc_bid            = prices_ob{2}(seq_begins);
        acc_ask            = prices_ob{1}(seq_begins);
        acc_bid_size       = sizes{2}(seq_begins);
        acc_ask_size       = sizes{1}(seq_begins);
        dts                = data.date(seq_begins);
        
        data.date          = dts;
        seq_num_begin      = find(seq_begins);
        data.value = [acc_price,acc_volume,acc_bid,acc_ask, ...
            acc_bid_size,acc_ask_size,price(seq_begins), ...
            [price(seq_num_begin(2:end)-1);price(end)]];
        data.value(:, end+1:end+3) = [data.value(:, 1)-data.value(:, end-1), data.value(:, end)-data.value(:, end-1),  1 + accumarray(num_seq, ~seq_begins)];
        data.colnames = {'price', 'volume', 'bid', 'ask', 'bis_size', 'ask_size', ...
            'first_price_of_seq', 'last_price_of_seq', 'average-first', 'last-first', 'nb_trades_4_this_mo'};
        % [max_pi, idx] = sort(st_data('cols', data, 'last-first'));
        % worst_idx = idx(end-9:end);
        % datestr(data.date(worst_idx), 'HH:MM:SS.FFF');
        % data2lookat = st_data('from-idx', data, worst_idx);
        % st_data('cols', data2lookat, 'last-first')
    case 'spread and tick'
        idx_td = strmatch('trading-destinations', varargin(1:2:end), 'exact');
        if isempty(varargin{2*idx_td})
            warning('get_tick:exec', ['Be aware that the tick size may not be the same on different trading-destinations.\n'...
                'In order to prenvent from analysing excessively high spread, only the main market and CHI-X will be kept (but this will be done with the dirty method td_id < 80)\n']);
        end
        
        idx_day = strmatch('day', varargin(1:2:end), 'exact');
        idx_from = strmatch('from', varargin(1:2:end), 'exact');
        varargin{2*idx_from} = varargin{2*idx_day};
        idx_to = strmatch('to', varargin(1:2:end), 'exact');
        varargin{2*idx_to} = varargin{2*idx_day};
        
        
        
        MY_EPS = 1e-8;
        LIST_MULT = [2 5 10 20 50 100];
        TRESHOLD = 0.995;
        
        
        data = st_data('where', get_tick( 'tick4bi', varargin{:}, 'last_formula', '', 'info_field_to_st_data', '', 'output-mode', 'all'), '~{auction}');
        if st_data('isempty-nl', data)
            return;
        end
%         data = st_data('where', data, '{trading_destination_id}<80');
        if st_data('isempty-nl', data)
            return;
        end
        data   = st_data('where', data, '{bid}<{ask}&{bid}~=0&({bid}>={price}|{ask}<={price})');
        exclude_till = ceil(length(data.date)/100);
        
        data = st_data('from-idx', data, (1:length(data.date))'>exclude_till);
        tmp = st_data('cols', data, 'ask;bid');
        tmp = tmp(1:end);
        tmp(tmp <= MY_EPS) = [];
        
        if isempty(tmp)
            data = st_data('log', data, 'gieo', 'No reliable quotes in data');
            return;
        end
        
        tick_size = MY_EPS*pgcd(round(tmp/MY_EPS));
        ld = length(data.date);
        
        data.info.excluded_prop = 1-length(data.date)/ld;
        %         for j = 1 : length(data)
        if ~st_data('isempty-nl', data)
            spread = full(st_data('apply-formula', data, 'max(abs({ask}-{price}),abs({bid}-{price}))', 'output', 'value'));
            data   = st_data('add-col', data, spread, 'spread');
            
%             ;
%             
%             spread(1:exclude_till) = [];
            
            nbts = round(spread / tick_size);
            
            distrib = accumarray(nbts, ones(length(tick_size), 1), [], @sum);
%             distrib = distrib / sum(distrib);
            
            mult = 1;
            for i = 1 : length(LIST_MULT)
                if sum(distrib(LIST_MULT(i):LIST_MULT(i):end))/ sum(distrib) > TRESHOLD
                    mult = LIST_MULT(i);
                    break;
                end
            end
            
            if mult ~= 1
                tick_size = tick_size * mult;
                idx2del = abs(round(spread/tick_size)*tick_size - spread) > MY_EPS;
                if any(idx2del)
                    warning('get_tick:spread_and_tick size:deleting <%d> deal among <%d> because it is not consistent.', sum(idx2del), length(idx2del));
%                     idx2del = exclude_till+find(idx2del);
                    data.date(idx2del) = [];
                    data.value(idx2del, :) = [];
                    if isfield(data, 'rownames')
                        data.rownames(idx2del) = [];
                    end
                end
                
                spread = st_data('cols', data, 'spread');
                data.info.excluded_prop2 = sum(idx2del)/length(idx2del);
%                 spread(1:exclude_till) = [];
                
%                 distrib = distrib / sum(distrib);
            else
                data.info.excluded_prop2 = 0;
            end
            
            nbts = round(spread / tick_size);
                
            distrib = accumarray(nbts, ones(length(tick_size), 1), [], @sum);
            
            data.info.min_step_price = tick_size;
            
            data.info.nbts_distrib = distrib;
            
            spread = st_data('cols', data, 'spread');
            volume = st_data('cols', data, 'volume');
            
            nbts = round(spread / tick_size);
            
            data.info.nb_ts_spread_vw_distrib = accumarray(nbts, volume, [], @sum) / sum(volume);
        end
        %         end
    case 'filter-td'
        %<* intra day data
        opt   = options( { 'source', '',  'trading-destinations', {}, ...
            'initial-colnames', {}, 'final-colnames', {}, 'where_f-td', '', 'binary', 0}, varargin);
        where_f_td = opt.get('where_f-td');
        formula = tokenize(opt.get('initial-colnames'));
        formula = sprintf('{%s},', formula{:});
        formula = sprintf('[%s]', formula(1:end-1));
        if strcmp(formula, '[]')
            formula = '';
        end
        opt.set('last_formula', formula);
        opt.set('where_formula', where_f_td);
        lst = opt.get();
        kodes = get_repository( 'any-to-sec-td', lst{:});
        code  = kodes.security_key;
        sid   = kodes.security_id;
        o_tdi = kodes.trading_destination_id;
        opt.set('security_id', sid);
        st_log('get_tick:data looking for data for <%s>...\n', code);
        lst   = opt.get();
        if ~st_version.my_env.switch_full_tick2tbt2
            if opt.try_get('today_allowed',false) || ~isempty(opt.try_get('minTime','')) || ~isempty(opt.try_get('maxTime',''))
                if (isnumeric(opt.get('from')) && floor(opt.get('from')) ~= today) || ...
                   (ischar(opt.get('from'))    && datenum(opt.get('from'),'dd/mm/yyyy') ~= today) || ...
                   (isnumeric(opt.get('to'))   && floor(opt.get('to')) ~= today) || ...
                   (ischar(opt.get('to'))      && datenum(opt.get('to'),'dd/mm/yyyy') ~= today)
               
                    error('get_tick:ft', 'you should ask for today''s data only');
                else
                    %RMK : from_buffer applies those 2 functions : gmt2local_time and apply_formulas
                    data = get_tick('ft','day',today,lst{:});
                    if ~(isempty(data) || isempty(data.value)) && ~(isfield(data, 'info') && isfield(data.info, 'localtime') && data.info.localtime)
                        st_log('Adjusting timezone to local...\n');
                        decalage  = timezone('get_offset','initial_timezone','GMT',...
                            'final_timezone', data.info.td_info(1).timezone,'dates',floor( data.date(1)));
                        data.date = data.date + decalage;
                        data.info.localtime = true;
                    end
                    if ~isempty(where_f_td)
                        data = st_data('where', data, where_f_td);
                    end
                    if ~isempty(formula)
                        data = st_data('apply-formula', data, formula);
                    end
                    if strcmp(opt.try_get('output-mode',''),'day')
                        data = {data};
                    end
                end
            else
                data = from_buffer( 'get_tick', 'ft', lst{:});
            end
        else
            data = get_tickdb2( 'ft', lst{:});
        end
        if isempty( data)
            return
        end
        % filter on trading destinations
        if ~iscell( data)
            no_cell = true;
            data = { data };
        else
            no_cell = false;
        end
        warn = false;
        for c=1:length(data)
            if st_data('isempty-no_log', data{c})
                % nothing to do
            else
                % On note le place_id initial car les donn?es sont
                % timestamp?es en heures locales de cette place
                main_timezone = data{c}.info.td_info(1).timezone;
                % fitrage du .info.td_info
                all_td_info      = [data{c}.info.td_info.trading_destination_id];
                if length( all_td_info) < length( o_tdi)
                    if ~warn
                        st_log('get_tick:td - your data and the DB..repository does not agree about available TD for sec id <%d>\n', opt.get('security_id'));
                    end
                    warn = true;
                    data{c}.info.td_info = get_repository( 'trading-destination-info', opt.get('security_id'));
                end
                try
                    keep_td_info_idx = arrayfun(@(td_)find(td_==all_td_info), o_tdi, 'uni', true);
                    % retrouver les trading destination demand?es dans td_info
                    data{c}.info.td_info = data{c}.info.td_info(keep_td_info_idx);
                catch ME
                    keep_td_info_idx = arrayfun(@(td_)find(td_==all_td_info), o_tdi, 'uni', false);
                    keep_td_info_idx = [keep_td_info_idx{:}];
                    data{c}.info.td_info = data{c}.info.td_info(keep_td_info_idx);
                end
                % filtrage des donn?es
                td_vals  = st_data('col', data{c}, 'trading_destination_id');
                idx_keep = ismember( td_vals, o_tdi);
                data{c}.value = data{c}.value(idx_keep,:);
                data{c}.date  = data{c}.date(idx_keep);
                if isfield( data{c}, 'rownames')
                    data{c}.rownames = data{c}.rownames(idx_keep);
                end
                icolnames = opt.get('initial-colnames');
                fcolnames = opt.get('final-colnames');
                % apr?s avoir filtr? les donn?es, si la trading_destination
                % principale n'a pas ?t? conserv?e, on peut avoir besoin de
                % changer l'heure
                if ~isempty(keep_td_info_idx) && ~any(keep_td_info_idx ==1)
                    data{c}.date = timezone('convert','initial_timezone',main_timezone,...
                        'final_timezone', data{c}.info.td_info(1).timezone, 'dates', data{c}.date);
                end
                if ~isempty( icolnames)
                    %< Filtrage des colnames
                    data{c} = st_data('keep', data{c}, icolnames);
                    if ~isempty( fcolnames)
                        if ischar( fcolnames)
                            fcolnames = tokenize( fcolnames, ';');
                        end
                        data{c}.colnames = fcolnames;
                    end
                    %>
                end
            end
        end
        if no_cell
            data = data{1};
        end
        %>*
    case 'masterkey'
        %<* Return the generic+specific dir
        fmode = varargin{1};
        fmode = regexprep( fmode, '[ |*|\\|/|:|?|"|<|>|\|]', '_');
        opt = options({'security_id', NaN}, varargin(2:end));
        sec_id = opt.get('security_id');
        if ~isnumeric( sec_id) && numel(sec_id)==1
            error('get_tick:secid', 'I need an unique numerical security id');
        end
        if strcmp(varargin{1}, 'spread and tick')
            data = fullfile(fmode, get_repository( 'security-td-key', sec_id, opt.get('trading-destinations')));
        else
            data = fullfile(fmode, get_repository( 'kech-security-key', sec_id));
        end
        %>*
    case 'ft' % il est interdit d'utiliser un cell array pour appeller ce mode avec un autre d?nomination
%         server_market_data = get_server_from_mnemonic(st_version.my_env.target_name,st_version.bases.market_data);
%         server_market_data_ref = get_server_from_mnemonic('production',st_version.bases.market_data);
%         if ~strcmp(server_market_data,server_market_data_ref)
%             error('get_tick:check', 'You should be connected to production server.');
%         end
        c = userdatabase( 'sqlserver', 'MARKET_DATA');
        a=tokenize(c.URL, '/:');
        if ~strcmp(a{4}, 'LUIDBQ01')
            error('get_tick:check', 'You should be connected to production server.');
        end
        
        t0 = clock;
        %<* Read into the tickdb
        opt = options({'security_id', NaN, 'day', '??/??/????',...
            'date_format:char', 'dd/mm/yyyy' ,'minTime','','maxTime','','today_allowed',false...
            }, varargin);
        
        assert(isempty(opt.get('minTime')));
        assert(isempty(opt.get('maxTime')));
        
        %< Lecture des options
        format_date = opt.get('date_format:char');
        sec_id = opt.get('security_id');
        if ~isnumeric( sec_id) && numel(sec_id)==1
            error('get_tick:secid', 'I need an unique numerical security id');
        end
        td_info = get_repository( 'trading-destination-info', sec_id);
        day_    = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        if ismember(weekday(day_), [1 7])
            data = [];
            return;
        end
        
        data_in_td_daily = exec_sql('MARKET_DATA', ['select * from MARKET_DATA..trading_daily' td_info(1).global_zone_suffix ...
            ' where security_id = ' num2str(sec_id) ' and date = ''' datestr(day_, 'yyyymmdd') ''' ']);
        
        if isempty(data_in_td_daily)
            data = [];
            return;
        else
%             error('get_tick:check', 'You are not authorized to request these data, there might be a problem with mat files production.');
        end
        
%<* Get one day for one sec id
        
        
%         td_info = struct('localtime', 1, 'global_zone_suffix', '');
        
        
        
        if day_ >= today || ( (day_ == today - 1) && mod(now, 1)*24 < 8) % added this last condition for Kepler's env, hope yesterday's data will be in database by eight in the morning
            error('get_tick:check_args', 'Currently unvailable data');
        end
        
%         if ~isempty(opt.try_get('minTime','')) || ~isempty(opt.try_get('maxTime',''))
%             dbs = dbstack;
%             if ismember('from_buffer.m',{dbs.file})
%                 error('get_tick:check_args','You can''t use minTime or maxTime with the buffer');
%             end
%         end
        
        %>
        %< Requ?te
        %< Exception TD
        % Avant le 01/04/2007 les tables deal_archive ne contiennent pas le
        % champs trading_destination_id
        no_trading_destination_yet = (day_ < 733133); % datenum(2007,04,01)
        if no_trading_destination_yet
            slct_trading_destination = '';
        else
            % Kepche TODO remove it!!!!!!!! only for Yike until Referential
            % is handled in matlab framework
            slct_trading_destination = [',trading_destination_id'];
        end
        %>
        %< Exception IAD
        % intraday_auction (ench?re r?guli?re de milieu de
        % journ?e sur XETRA par ex) et dark (deal sp?ciaux dans les darks pools)
        % qui ne sont enregistr?s que depuis q3_2008
        no_iad_yet = (day_ < 733682); % datenum(2008,10,01)
        if no_iad_yet
            slct_iad = '';
        else
            slct_iad = ',intraday_auction,dark';
        end
        %>
        %< Exception Microseconde
        % Avant 2009, il n'y a pas de colonne microseconde
        no_micro = (day_ < 733774); % datenum(2009,01,01)
        if no_micro
            slct_micro = '';
        else
            slct_micro = ',microseconds';
        end
        %>
        
        select_str = [  'select date,datediff(millisecond, ''00:00:00'', time),price,size,overbid,bid,ask,'...
            'bid_size,ask_size,overask,'...
            '[cross],auction,opening_auction,closing_auction,'...
            'trading_at_last,trading_after_hours,interpolated,deal_id' slct_trading_destination slct_iad slct_micro ...
            ' from '];
        % ATTENTION: conserver date/time en 2 premi?res colonnes
        req_cols  = { 'date' , 'time', 'price' , 'volume' , 'sell' ,'bid' ,'ask',...
            'bid_size' ,'ask_size' ,'buy', 'cross', ...
            'auction' ,'opening_auction','closing_auction', ...
            'trading_at_last','trading_after_hours','interpolated','deal_id', 'trading_destination_id', 'intraday_auction', 'dark'};
        % Si l'on modifie l'ordre des colonnes, il faut le
        % r?percuter dans filter mode tickdb (et ptet ailleurs...)
        colnames_ = {   'date' , 'time', 'price', 'volume','sell','bid','ask', 'trading_destination_id',...
            'bid_size','ask_size','buy','cross',...
            'auction','opening_auction','closing_auction','trading_at_last','trading_after_hours',...
            'interpolated', 'deal_id', 'intraday_auction', 'dark'};
        idx_cols = cellfun(@(c)strmatch(c, req_cols, 'exact'), colnames_,'uni', true);
        %< Gestion des noms de bases
        % ATTENTION: pour un futur (qu'on esp?re) lointain il n'y a pas de
        % raison de se concentrer sur la premi?re TD... sauf que c'est la
        % primary place (ranking la plus faible dans security market).
        
%         first_req = ''; % a first request that can be tried as it could be a more efficient way to retrieve the data
%         
%         if floor(day_) == today
%             deal_name = [ 'deal_day' td_info(1).global_zone_suffix ];
%             main_req = [tick_db_name '..' deal_name];
%         else %comme avant
%             deal_name = [ 'deal_archive' td_info(1).global_zone_suffix ];    
%             try
%                 main_req = sprintf('%s..%s', get_basename4day( day_), deal_name);
%             catch ME
%                 if strcmp(ME.identifier, 'get_basename4day:range')
%                     data = [];
%                     st_log('end of read_dataset:exec_req:exec (%5.2f sec)\n', etime(clock, t0));
%                     return;
%                 else
%                     rethrow(ME);
%                 end
%             end
%             if day_ >= today - 2 % TODO_KEPCHE ?criture en base des jours trait?s pour chacun des Exchanges, tests ici qu'on l'a bien trait?, remont?e d'erreur sinon
%                 first_req = [tick_db_name '..deal_day' td_info(1).global_zone_suffix];
%             end
%         end
        %special case if minTime or maxTime set

%         minTime = opt.get('minTime');
%         if isnumeric(minTime)
%             minTime = datestr(mod(minTime,1));
%         end
%         
%         maxTime = opt.get('maxTime');
%         if isnumeric(maxTime)
%             maxTime = datestr(mod(maxTime,1)+1/(24*60));
%         end
        
        if ~td_info(1).localtime
            decalage  = timezone('get_offset','final_timezone', td_info(1).timezone, 'dates',  day_(1) );
            %             if ~isempty(minTime)
            %                 minTime = datestr(mod(datenum(minTime),1) - decalage);
            %             end
            %             if ~isempty(maxTime)
            %                 maxTime = datestr(mod(datenum(maxTime),1) - decalage);
            %             end
        end
        
        where_time = '';
        %         if ~isempty(minTime)
        %            where_time = sprintf('and time >= ''%s'' ',minTime);
        %         end
        %         if ~isempty(maxTime)
        %            where_time = [where_time sprintf(' and time <= ''%s'' ',maxTime)];
        %         end
        
        end_req = sprintf(['  where date = ''%s'' ' ...
            '   and security_id = %d   ' where_time ...
            '  order by date, time' slct_micro ', deal_id' ], ...
            datestr( day_, 'yyyymmdd'), ...
            sec_id);
        st_log('just before fetch (%5.2f sec)\n', etime(clock, t0));
        t1 = clock;
        
%         for i = 1 : 9
%             try % still unsure about how Alexandre will handle that kepche_TODO : remove it ?
%                 base_and_table = [tick_db_name '..deal_day' td_info(1).global_zone_suffix '_' num2str(i)];
% 
%                 tmp = exec_sql('TICK', ['select date, count(1) from ' base_and_table ' group by date']);
% 
%                 assert(size(tmp, 1)<3, 'get_tick:check', 'TICK: More than two days found in a deal_day table'); 
%                 if size(tmp, 1) == 2 % kepcheTODO safer than this? more warning? send mail about this and process anyway?
%                     [~, idxs] = sort(tmp(:, 1));
%                     if tmp{idxs(1), 2} < 1e-4 * tmp{idxs(2), 2}
%                         tmp = tmp(idxs(2), 1);
%                     else
%                         error('get_tick:check', 'TICK: More than one day found in a deal_day table and there are lots of deals');
%                     end
%                 elseif isempty(tmp)
%                     continue
%                 else
%                     tmp = tmp(1);
%                 end
% 
%     %             tmp = exec_sql('MARKET_DATA', ['select distinct date from ' base_and_table]);
%     %             assert(length(tmp)<2, 'get_tick:check', 'TICK: More than one day found in a deal_day table'); % TODO safer than this?
%                 days_found(i) = datenum(cell2mat(tmp), 'yyyy-mm-dd');
%                 if days_found(i) == day_
%                     found_table = true;
%                     break;
%                 end
%             catch 
%                 
%             end
%         end
        
        % kepcheTODO : more efficient/safer than this search for the right table?
        [~, ~, day_in_month] = datevec(day_);
        base_and_table = [tick_db_name '..deal_day' td_info(1).global_zone_suffix '_' num2str(day_in_month)];
        tmp = exec_sql('TICK', ['select top 1 date from ' base_and_table ]);
       % < this test was originally written to check for some problem that
        % happened due to misunderstanding with database admin who made our
        % process failing due to index changing.
        % the problem is that it can also happen because of holidays both
        % in the US and Canada
        % kepcheTODO use calendar to know whether this is a closed day?
        if isempty(tmp)
            data = [];
            return;
        end
%         assert(~isempty(tmp), 'get_tick:check', 'TICK: Empty table found');
        % >
        found_table = ( datenum(cell2mat(tmp), 'yyyy-mm-dd') == day_ );
        
        assert(found_table, 'get_tick:check', 'TICK: This day was not found : is it too late? Quickly ask for it')
        sql_req = [select_str base_and_table ' ' end_req]; %  ( index security_ind )
        vals = exec_sql('TICK', sql_req); 
        st_log('end of fetch (%5.2f sec)\n', etime(clock, t1));
        %>
        
        if isempty(vals)
            data = [];
            return;
        end
        
        t6 = clock;
        %< Correctif Microseconde
        if ~no_micro
            micro = vals(:,end);
            vals(:,end) = [];
        else
            micro = NaN;
        end
        %>
        %> Si l'on a pas r?cup?rer le champs trading_destination : YAKA
        % l'inventer, pour cela :
        % on exclue CHIX (execution_market_id = 50)
        % S'il reste plus d'une trading destination pource titre alors
        % on est dans la merde donc on raise une exception
        % S'il reste q'une, alors c'est celle l?, YAPLUKA l'ajouter au
        % r?sultat de la requ?te
        if no_trading_destination_yet
            possible_td = get_repository( 'trading-destination', sec_id);
            if isempty( possible_td)
                error('get_tick:trading_destination', 'no trading destination available');
            end
            vals(:, end + 1) = repmat({possible_td(1)}, size(vals, 1), 1);
        end
        if no_iad_yet
            vals(:, end + 1:end+2) = repmat({0}, size(vals, 1), 2);
        end
        st_log('end of first get_repository (%5.2f sec)\n', etime(clock, t6));
        
        
        
        t5 = clock;
        %< Creation du st_data
        
        %< Conversion de la date
        sec1 = datenum(0,0,0,0,0,1);
        mil1 = datenum(0,0,0,0,0,1)/1000;
        if no_micro
            date_time = datenum(vals(1,1), 'yyyy-mm-dd') + cell2mat(vals(:,2)) * mil1; % 1 et 2 sont en dur!
        else
            date_time = datenum(vals(1,1), 'yyyy-mm-dd') + round(cell2mat(vals(:,2))/1000)*sec1 + ...
                cell2mat(micro)*sec1*1e-6; % 1 et 2 sont en dur!
        end
        vals        = vals(:,idx_cols); % s?lection et ordonnancement des bonnes colonnes
        vals(:,1:2) = [];               % effacer date/time des valeurs
        colnames_   = colnames_(3:end); % effacer date/time des colonnes
        st_log('end of datenum (%5.2f sec)\n', etime(clock, t5));
        %>
        %< Passage en heure locale
        if ~td_info(1).localtime
            date_time = date_time + decalage;
        end
        %>
        
        %>
        
        t3 = clock;
        if size(vals, 1) > 100000
            for i = 1 : size(vals, 2)
                data(:, i) = sparse(cellfun(@(x)double(x),vals(:, 1)));
                vals(:, 1) = [];
            end
            clear vals
        else
            data = cellfun(@(x)double(x),vals);
        end
        st_log('end of conversion to double (%5.2f sec)\n', etime(clock, t3));
        
        
        t4 = clock;
        data = st_data( 'init', 'title', sprintf( '%s - %s', get_repository( 'security-key', sec_id), datestr(day_, 'dd/mm/yyyy')), ...
            'value', data, 'date', date_time, 'colnames', colnames_);
        
        
        data.info =  ...
            struct('security_id', sec_id, ...
            'security_key', get_repository( 'security-key', sec_id), ...
            'td_info', td_info , ...
            'sql_request', sql_req, ...
            'data_log', data.info.data_log, ...
            'localtime', true, ...
            'version', 'kepche_1') ;
        st_log('end of repository (%5.2f sec)\n', etime(clock, t4));
        %>
        %<* Quelques petites v?rifications
        %< volumes n?gatifs? => transactions ? annuler il faudrait
        % retrouver la transaction ? annuler, n?anmoins aucune information
        % en base ne nous permet de le faire, de plus du point de vue des
        % algos cette transaction a exist?e jusqu'? son annul.
        % Conclusion on va ici faire un traitement simplissimme qui
        % consiste ? retirer toutes les transactions ayant un volume
        % n?gatif (donc on fait comme si l'annulation ne s'?tait pas produite)
        % TODO : BETTER one day
        
        
        t8 = clock;
        volumes = st_data('cols', data, 'volume');
        idx = volumes <= 0;
        if any(idx)
            data = st_data('from-idx', data, ~idx);
            data = st_data('log', data, 'digo', sprintf('Les donn?es contenaient <%g> volume (r?partis sur <%d> annulations) qui ont ?t? purement et simplement retir?s des donn?es', sum(volumes(idx)), sum(idx)));
        end
        %>
        %> les prix n?gatifs ou nuls
        prices = st_data('cols', data, 'price');
        idx = prices <= 0;
        if any(idx)
            data = st_data('from-idx', data, ~idx);
            data = st_data('log', data, 'digo', sprintf('Les donn?es contenaient <%d> transactions ? un prix n?gatif ou nul qui ont ?t? purement et simplement retir?s des donn?es', sum(idx)));
        end
        %<
        %> TODO quelque chose pour les bid-ask nuls ou aberrants lors de la
        %phase continue...
        %<
        %> les intraday_auction et dark mis ? false sans r?fl?chir
        if no_iad_yet
            data = st_data('log', data, 'gigo', 'Peut -?tre y-t-il des intraday auction, mais ne disposant pas de l''information, ils ont tous ?t? mis ? false. idem pour les darks');
        end
        %<
        
        %< Will we be able to distinguish two deals thanks to 
        % deal_id in Matlab? if not, lets make a matlab_deal_id
        
        if eps(max(st_data('cols', data, 'deal_id'))) >= 0.5
            data = st_data('drop', data, 'deal_id');
            data = st_data('add-col', data, (1:length(data.date))', 'deal_id');
        end
        
%         %> Patch : Modification des tailles de bid-ask pour les titres US
%         is_america_ny = strcmpi(data.info.td_info(1).timezone,'America/New_york');
%         is_day_greater_21_jul_2010 = day_ > datenum(2010,7,21);
%         is_day_lower_31_mai_2012 = day_ < datenum(2012,5,31);
%         if is_america_ny && is_day_greater_21_jul_2010 && is_day_lower_31_mai_2012
%             is_col_bid_ask_size = ismember(data.colnames,{'bid_size','ask_size'});
%             data.value(:,is_col_bid_ask_size) = data.value(:,is_col_bid_ask_size)*100;
%             data.info.patch_us = true;
%         end
%         %> Fin du Patch
        
        %>
        st_log('end of last manipulation (%5.2f sec)\n', etime(clock, t8));
        st_log('end of get_tick:exec (%5.2f sec)\n', etime(clock, t0));
        %>*
    
    case 'ob'
        % TODO : something fully fonctionnal, which is able to write the stored_prodeure to be used.
        % Mais il est plus sage d'attendre les serveurs IQ qui permettront d'acc?der aux donn?es de n'importe quelle p?riode sans se soucier de la base dans laquelle elles sont stock?es
        % Il faudra aussi ?crire un mode permettant de synchroniser ces
        % donn?es avec les donn?es de type deal gr?ce au deal_id
        opt = options({'security', 'FTE.PA', 'from', '11/01/2008', 'to', '11/01/2008', ...
            'trading_destination_id', 4, 'date_format:char', 'dd/mm/yyyy', ...
            'conn_chain', {'batch_tick', 'batick'}}, varargin);
        try
            td_id  = opt.get('td_info');
            sec_id = opt.get('security_id');
        catch er
            error('read_dataset:ob', 'You should not reach this point, report to clehalle or rburgot');
        end
        date_format = opt.get('date_format:char');
        date_from = opt.get('from');
        if ~isnumeric( date_from)
            date_from = datenum(date_from, date_format);
        end
        date_to = opt.get('to');
        if ~isnumeric( date_to)
            date_to   = datenum( date_to, date_format);
        end
        if (date_from ~= date_to)
            error('read_dataset:exec_req:orderbook', 'You are not allowed to ask for mmore than one day');
        elseif date_from < datenum(2007, 2, 1, 0, 0, 0) % TODO this should be dynamic... but the stored procedure is not dynamic either, so...
            error('read_dataset:exec_req:orderbook', 'Ordebook not yet implemented for this period');
        end
        c = userdatabase('sybase:j', 'BSIRIUS');
        if date_from < 733499
            sp_name = 'tmp_get_orderbook';
        else
            sp_name = 'tmp_get_orderbook_2';
            %vals = exec_sql(c, sprintf('exec temp_works..tmp_get_orderbook_2 %d, %d, ''%s''', ids(2), ids(1), datestr(date_to, 'yyyymmdd')));
        end
        st_log('retrieving <%s> orderbooks...\n', td_id{1,4});
        vals = exec_sql(c, sprintf('exec temp_works..%s %d, %d, ''%s''', sp_name, sec_id, td_id{1,1}, datestr(date_to, 'yyyymmdd')));
        vals_td = repmat(td_id{1,1}, size(vals,1),1);
        for td=2:size(td_id,1)
            st_log('retrieving <%s> orderbooks...\n', td_id{td,4});
            tals = exec_sql(c, sprintf('exec temp_works..%s %d, %d, ''%s''', sp_name, sec_id, td_id{td,1}, datestr(date_to, 'yyyymmdd')));
            vals = cat(1, vals, tals);
            vals_td = cat(1, vals_td, repmat(td_id{td,1}, size(tals,1),1));
        end
        if isempty(vals)
            data = [];
            return;
        end
        try
            dts  = date_to + mod(datenum( vals(:,1), 'HH:MM:SS'),1);
        catch
            lasterr('');
            dts  = date_to + mod(datenum( vals(:,1), 'yyyy-mm-dd HH:MM:SS.FFF'),1);
        end
        vals = [cellfun(@double, vals(:,2:end)), vals_td];
        idx_to_delete = any(isnan(vals), 2);
        prop_delete = mean(idx_to_delete);
        if prop_delete > 0
            warning('read_dataset:exec_req:orderbook:exec', 'Deleting %.2f%% of the trades, as they have no orderbook data', 100 * prop_delete);
            vals(idx_to_delete, :) = [];
            if prop_delete > 0.5
                error('read_dataset:exec_req:orderbook:exec', 'Too much missing value');
            end
        end
        data  = st_data('init', 'title', [ opt.get('security') ' Orderbook'], 'date', dts, 'info', struct('td_info', td_id), ...
            'value', vals, 'colnames', { 'deal_id', ...
            ...
            'Bid number 5', 'Bid size 5', 'Bid price 5', ...
            'Bid number 4', 'Bid size 4', 'Bid price 4', ...
            'Bid number 3', 'Bid size 3', 'Bid price 3', ...
            'Bid number 2', 'Bid size 2', 'Bid price 2', ...
            'Bid number 1', 'Bid size 1', 'Bid price 1', ...h
            ...
            'Ask number 1', 'Ask size 1', 'Ask price 1', ...
            'Ask number 2', 'Ask size 2', 'Ask price 2', ...
            'Ask number 3', 'Ask size 3', 'Ask price 3', ...
            'Ask number 4', 'Ask size 4', 'Ask price 4', ...
            'Ask number 5', 'Ask size 5', 'Ask price 5', ...
            'Trading destination' });
    case 'daily'
        %<* Read daily data
        opt = options({'security', 'EDF.PA', 'from','02/04/2007','to','02/04/2007', 'format_date', 'dd/mm/yyyy', 'db-connection', {'batch_tick','batick' } }, ...
            varargin(1:end));
        sec = opt.get('security');
        conn_chain = opt.get('db-connection');
        format_date = opt.get('date_format:char');
        frm = datenum(opt.get('from'), format_date);
        too   = datenum(opt.get('to'  ), format_date);
        if (frm ~= too)
            error('read_dataset:exec_req:daily', 'You are not allowed to ask for more than one day');
        end
        c = userdatabase('sybase:j', 'BSIRIUS');
        ids = get_td_and_sec_id_from_ric(sec);
        vals = exec_sql(c, sprintf( [ ...
            'select  date, trading_destination_id,volume, turnover, open_prc, high_prc, low_prc, close_prc, open_turnover, close_turnover, nb_deal, ' ...
            '        open_volume, close_volume,average_spread_numer, average_spread_denom  ' ...
            '   from ' tick_db_name '..trading_daily td ' ...
            '   where security_id = %d and trading_destination_id = %d and date = ''%s''' ...
            ], ids(2), ids(1), datestr(frm, 'yyyymmdd')));
        if ~isempty( vals)
            nvals = cell2mat(vals(:,2:end));
            data = st_data('init', 'date', datenum( vals(:,1), 'yyyy-mm-dd'), ...
                'value', [nvals(:,1:end-2), nvals(:,end-1)./nvals(:,end)] , 'colnames', { 'Trading_destination','Volume', 'Turnover', 'Open', 'High', 'Low', 'Close', 'Open (turnover)', 'Close (turnover)', 'Nb deals', 'Open volume', 'Close volume', 'Mean spread'}, ...
                'title', sec);
        else
            data = [];
        end
    otherwise
        if strcmpi(mode(1:3), 'gi:')
            graphname = mode(4:end);
            
        end
        error('read_dataset:exec_req:exec', 'mode <%s> unknown', mode);
end
end






%--------------------------------------------------------------------------
%----- OTHER FUNCTION
%--------------------------------------------------------------------------

function out=add_col_tick_size(data)

[data b_data]=st_data('isempty',data);
out=data;

if b_data
    return
end

tick_size_val=NaN(size(data.value,1),1);

if any(ismember(data.colnames,'trading_destination_id')) && ...
        isfield(data,'info') && isfield(data.info,'td_info') && ...
        (~isempty(data.info.td_info) && isfield(data.info.td_info(1),'price_scale'))
    
    td_id_val=st_data('col',data,'trading_destination_id');
    price_val=st_data('col',data,'price');
    uni_td_id=unique(td_id_val);
    all_td_in_tdinfo=[data.info.td_info.trading_destination_id];
    
    for i_t=1:length(uni_td_id)
        id_in_tdinfo=ismember(all_td_in_tdinfo,uni_td_id(i_t));
        if any(id_in_tdinfo)
            tick_size_rule_matrix=data.info.td_info(id_in_tdinfo).price_scale;
            idx_in_val=find(td_id_val==uni_td_id(i_t));
            
            if ~isempty(tick_size_rule_matrix)
                nb_class_tick_size=size(tick_size_rule_matrix,1);
                if nb_class_tick_size==1
                    tick_size_val(idx_in_val)=tick_size_rule_matrix(2);
                else
                    for i_class=1:nb_class_tick_size-1
                        id_tmp=tick_size_rule_matrix(i_class,1)<=price_val(idx_in_val) &...
                            price_val(idx_in_val)<tick_size_rule_matrix(i_class+1,1);
                        if any(id_tmp)
                            tick_size_val(idx_in_val(id_tmp))=tick_size_rule_matrix(i_class,2);
                        end
                    end
                    id_tmp_end=price_val(idx_in_val)>=tick_size_rule_matrix(end,1);
                    if any(id_tmp_end)
                        tick_size_val(idx_in_val(id_tmp_end))=tick_size_rule_matrix(end,2);
                    end
                end
            end
        end
        
    end
end

out=st_data('add-col',out,tick_size_val,'tick_size');

end




