function data = get_tickdb2( mode, varargin)
% GET_TICKDB2 - interface for tickdb2 data
%
% le local time utilis? n'est pas celui du primary. Il est relatif ?
% l'ensemble des destinations extriates :
% en effet CHIX ?tant ? londres, les horaires du groupe de quotation CHIX
% sont en heures anglaises et donc si l'on ne prends que des donn?es CHIX
% et que l'on souhaite pouvoir les regarder vis ? vis des horaires de
% trading alors il faut pouvoir avoir ces donn?es en heures londonniennes
% globalement en faisant cela je m'assure que cela fonctionne comme cela
% fonctionnait auparavant et je m'assure aussi une coh?rence avec les bases
% de donn?es.
%
% ex :
% data = get_tickdb2( 'ft', 'security_id', 276, 'from', '04/07/2010', 'to', '07/07/2010', 'trading-destinations', {}, ...
%       'where_formula', '~({auction}|{trading_at_last}|{cross}|{trading_after_hours})', 'last_formula', '[{price},{volume}]', ...
%       'day-into-date', true);
%
%-  tests mode ft
%
% dest = {'MAIN', 'CHIX', 'TRQXLT', 'BATE'}
% date = '05/07/2010'
% perf = NaN(length(dest), 2);
% diff = [];
% diffidx = [];
% for i = 1 : length(dest)
%   t = clock;
%   data = get_tickdb2( 'ft', 'security_id', 276, 'from', date, 'to', date, 'trading-destinations', dest(i), ...
%       'where_formula', '~({auction}|{trading_at_last}|{cross}|{trading_after_hours})', ...
%       'last_formula', '[{price},{volume},{sell},{bid},{ask},{bid_size},{ask_size},{trading_destination_id}]', ...
%       'day-into-date', true); 
%   perf(i, 1) = etime(clock, t);
%   
%   t = clock;
%   data2 = get_tick( 'btickdb', 'security_id', 276, 'from', date, 'to', date, 'trading-destinations', dest(i));
%   perf(i, 2) = etime(clock, t);
%
%   [diff(end+1, :), diffidx(end+1, :)] = max(abs([data.date, data.value]-[data2.date, data2.value]));
% end
% 
%
%
% data = get_tickdb2( 'ft', 'security_id', 'TOTF.PA', 'from', '04/07/2010', 'to', '07/07/2010', 'trading-destinations', {'CHIX', 'BATE'}, ...
%       'where_formula', '~({auction}|{trading_at_last}|{cross}|{trading_after_hours})', ...
%       'last_formula', '[{price},{volume},{sell},{bid},{ask},{bid_size},{ask_size},{trading_destination_id}]', ...
%       'day-into-date', true);
% data2 = get_tick( 'btickdb', 'security_id', 276, 'from', '04/07/2010', 'to', '07/07/2010', 'trading-destinations', {'CHIX', 'BATE'});
% [m, i] = max(abs(data.value-data2.value))
% data.value(i(2)+(-1:1:1), :)
% data2.value(i(2)+(-1:1:1), :)
% % il y une diff?rence d?s le 2699eme deals mais ces deux deals se
% produisent pr?cisemment en m?me temps
% % select * from tick_db_07d_2010..deal_archive where security_id = 276 and trading_destination_id in (61, 89) and date = '20100705'
% % order by time, microseconds
% % la seule information que j'en retire c'est que si l'on veut pouvoir
% comparer les deux st_data alors il faut le faire en trading_destination
% par trading_destination et pas en les regroupant
%
% data = get_tickdb2( 'ft', 'security_id', 'TOTF.PA', 'from', '04/07/2010', 'to', '07/07/2010', 'trading-destinations', {}, ...
%       'where_formula', '~({auction}|{trading_at_last}|{cross}|{trading_after_hours})', ...
%       'last_formula', '[{price},{volume},{sell},{bid},{ask},{bid_size},{ask_size},{trading_destination_id}]', ...
%       'day-into-date', true);
% data2 = get_tick( 'btickdb', 'security_id', 276, 'from', '04/07/2010', 'to', '07/07/2010', 'trading-destinations', {});
%
% data = get_tickdb2( 'ft', 'security_id', 'TOTF.PA', 'from', '04/07/2010', 'to', '07/07/2010', 'trading-destinations', {}, ...
%       'where_formula', '~({auction}|{trading_at_last}|{cross}|{trading_after_hours})', ...
%       'last_formula', '[{price},{volume},{sell},{bid},{ask},{bid_size},{ask_size},{trading_destination_id}]', ...
%       'day-into-date', true, 'output-mode', 'day');
% data2 = get_tick( 'btickdb', 'security_id', 276, 'from', '04/07/2010', 'to', '07/07/2010', 'trading-destinations', {}, 'output-mode', 'day');
%
%
% tests deprecated modes
%
% data = get_tickdb2( 'ft-1day', 'security_id', 'TOTF.PA', 'day', '04/01/2010', 'trading-destinations', {}, ...
%       'where_formula', '~({auction}|{trading_at_last}|{cross}|{trading_after_hours})', 'last_formula', '[{price},{volume}]', ...
%       'day-into-date', true)
%
% data = get_tick( 'btickdb', 'security_id', 276, 'from', '04/01/2010', 'to', '04/01/2010', 'trading-destinations', {})
%
%
% data = get_tickdb2( 'ft-1day', 'security_id', 'TOTF.PA', 'day', '04/01/2010', 'trading-destinations', {'MAIN', 'CHIX'}, ...
%       'where_formula', '~({auction}|{trading_at_last}|{cross}|{trading_after_hours})', 'last_formula', '[{price},{volume}]', ...
%       'day-into-date', true)
%
% data = get_tick( 'btickdb', 'security_id', 276, 'from', '04/01/2010', 'to', '04/01/2010', 'trading-destinations', {})
%
% data = get_tickdb2( 'ft-1day-1td', 'security_id', 'TOTF.PA', 'day', '04/01/2010', 'trading-destination', 'MAIN', ...
%       'where_formula', '~({auction}|{trading_at_last}|{cross}|{trading_after_hours})', 'last_formula', '[{price},{volume}]', ...
%       'day-into-date', true)
%
% data = get_tickdb2( 'ft-1day-1td', 'security_id', 'TOTF.PA', 'day', '04/01/2010', 'trading-destination', 'CHIX')
%
% data are in LOCALTIME (from the main td point of view)
% TODO :
%   TODO0 : function loadOrderBook renvoyant l'ensemble des destination
%       pr?sentes car on ne peut pas se baser sur la liste de destinations
%       renvoy?es par 
%       [sec_id, tdi] = get_repository( 'any-to-sec-td', 'security_id', opt.get('security') , ...
%            'trading-destinations', asked_td);
%       puisu'elle n'est pas complete. voir par exemple :
%       j=get_repository('tdinfo', 381) => j.trading_destination_id = 6 91
%       92 89 et il n'y pas 81 alors que cela traite sur Turquoise
%       Cette 'fonctionnalit?' (les destination pr?sentes dans 
%       security_market sont probablement celles que l'on s'autorise ? traiter)
%       ?tait d?j? pr?sente dans get_tick en mode filter-td mais pas ft.
%       ? voir si l'on souhaite conserver ce comportement.
%
%   TODO1 - la gestion des codes (security_id ou ric) doit ?tre faite en
%       c++
%   TODO2 - r?cup?ration dans un fichier r?f?rentiel du jour
%       security_id, trading_destination_id, short_name, quotation_group, et GMT_offfset (du
%       primaire) et donc aussi quelle est la destination primaire
%   TODO3 - si on veut ?tre portable il faut les horaires de trading? dans ce
%       cas il est peut ?tre inutile d'avoir le groupe de quotation
%
% see also read_dataset

global st_version;
if ~isfield( st_version.my_env, 'tbt2_repository')
    error('get_tickdb2:st_version', 'please get the default st_work.xml and restart matlab');
end

data_folder = st_version.my_env.tbt2_repository;

switch lower( mode)
    case 'ft'
        opt = options({ 'security_id', [], 'from', [], 'to', [], 'trading-destinations', '', ...
            'where_formula', [], 'last_formula', [], 'date-format', 'dd/mm/yyyy', ...
            'day-into-date', true, 'output-mode', 'all'}, varargin);
        
        % < gestion des dates qui seront dans le st_data ou dans le
        % cell_array de st_data renvoy?
        date_format = opt.get('date-format');
        dt_from = opt.get('from');
        if ischar( dt_from)
            dt_from = datenum( dt_from, date_format);
        end
        if dt_from < datenum(2009,1,1,0,0,0)
            error('get_tickdb2:exec', 'No TBT2 files before January 2009 the 1st');
        end
        dt_to = opt.get('to');
        if ischar( dt_to)
            dt_to = datenum( dt_to, date_format);
        end
        dt_all = dt_from:1:dt_to; % MAGIC NUMBER 1 : cela a t -il un sens ici de mettre une autre fr?quence ?
%         if length(dt_all) > 1
%             wk_all     = weekday(dt_all);
% %             if freq == 1
%                 dt_all( wk_all==1 | wk_all==7 ) = [];
% %             end
%         end
        % dt_all est maintenant le calendrier sur lequel on va boucler
        % >
        
        % 1. r?f?rentiel et traduction td
        % TODO1 et TODO2 rendront inutiles ces appels de plus ils seront
        % d?pendants du jour et devront donc ?tre dans la boucle sur dt_all
        % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        asked_td = opt.get('trading-destinations');
        [sec_id, tdi] = get_repository( 'any-to-sec-td', 'security_id', opt.get('security_id') , ...
            'trading-destinations', asked_td);
        % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        d_into_date = opt.get('day-into-date');
        
        coln = {'price', 'volume', 'sell', 'bid', 'ask',  ...
                    'trading_destination_id', 'bid_size', 'ask_size', 'buy',...
                    'cross', 'auction', 'opening_auction', 'closing_auction', 'trading_at_last', ...
                    'trading_after_hours', 'intraday_auction', 'dark', 'order_book_id'};
        % < gestion du mode de sortie, souhaite-t-on un cell array avec un
        % st_data pour chaque jour ou bien veut on les empiler?
        output_mode_d = strcmpi(opt.get('output-mode'), 'day');
        if output_mode_d
            data = cell(size(dt_all, 1), 1);
        else
            data = [];
        end
        % >
        
        % < on ne peut pas demander un seul st_data m?langeant plusieurs
        % jours et ne contenant pas dans le champs date d'information sur
        % le jour
        if ~output_mode_d && ~d_into_date && length(dt_all) > 1
            error('get_tickdb2:chec_args', ...
                'BAD_USE: you are asking for a unique st_data mixing several days but with a field date containing only hours');
        end
        % >
        % < is prmary market in the list of destination or not : if yes
        % then local time is the one of the primary market. if not then
        % local time is the one of the first destination asked
        
        
        % TODO0 il ne faut pas se baser sur les destinations pr?sentes mais
        % celles qui sont enregistr?es? => pour les groupe de quotation on
        % ne disposera pas de l'information puisque c'est directement dans
        % security_market que ces informations ne sont pas pr?sentes
        td_idx = find(ismember([tdi.trading_destination_id], [sec_id.trading_destination_id]));
        idxdest4time = td_idx(1);
        % >
        for i = 1 : length(dt_all)
            d = dt_all(i);
            % TODO2 rendra inutile cette ligne
            GMT_offset = timezone('get_offset','init_place_id', 0,... % MAGIC NUMBER 0 => GMT
                'final_place_id', tdi(idxdest4time).place_id, 'dates', d); % MAGIC NUMBER 1 => destination primaire
            if d_into_date
                dt_day = d;
            else
                dt_day = 0; 
            end
            % < le st_data dans lequel on va accumuler les
            % trading_destinations
            title_ = sprintf('%s - %s', sec_id.security_key, datestr(d, 'dd/mm/yyyy') );
            data_d = st_data('init','title',title_,'value', [], ...
                    'date', [],...
                    'colnames', {});
            % >
            for j = 1 : length(td_idx)
                % < 2. r?cup data
%                 try % TODO remove it pour l'instant ?a permet d'?viter 
                    % l'erreur existant si le coupe sec_id td_id n'est pas dans l'univers
                    data_td   = loadOrderBook(data_folder , ...
                        sec_id.security_id, datestr(d, 'yyyymmdd'),...
                        sec_id.trading_destination_id(j), 0, 1, 5);% MAGIC NUMBER 0 => auctionType et 1=>trade only
                    % add unique deal_ids
                    deal_ids = (1 : 1 :size(data_td.value,1))';
                    data_td = st_data('add-col',data_td,deal_ids ,'deal_id');
                    
                    
                    if tdi(td_idx(j)).localtime
                        dt_offset = 0;
                    else
                        dt_offset = GMT_offset;
                    end
                    
                    data_td.date =   data_td.date + dt_offset;
%                 catch
%                     data_td = st_data('init','title',title_,'value', [], ...
%                         'date', [],...
%                         'colnames', coln);
%                 end
                % >
                
                if ~(isempty(data_td) || isempty(data_td.value))
                    % < 3. filtres
                    where_formula = opt.get('where_formula');
                    if ~isempty( where_formula)
                        nb_lines = length(data_td.date);
                        data_td = st_data('where', data_td, where_formula); 
                        st_log('get_tickdb2:where_formula application of <%s> reduce the dataset from %d to %d lines\n', ...
                            where_formula, nb_lines, length(data_td.date));
                    end

                    last_formula = opt.get('last_formula');
                    if ~isempty( last_formula) && ~isempty( data_td.value)
                        nb_cols = length(data_td.colnames);
                        data_td = st_data('apply-formula', data_td, last_formula);
                        st_log('get_tickdb2:last_formula application of <%s> changed the dataset from %d to %d cols\n', ...
                            last_formula, nb_cols, length(data_td.colnames));
                    end
                    % >
                end
                
                if ~isempty(data_td.value)
                    % < 4.concat?nation
                    data_d.value = [data_d.value; data_td.value];
                    data_d.date  = [data_d.date; data_td.date];
                    % >
                    if isempty(data_d.colnames)
                        data_d.colnames = data_td.colnames;
                    end
                end
            end
            [data_d.date, idx] = sort(data_d.date);
            data_d.value = data_d.value(idx, :);
            data_d.date = data_d.date + dt_day;

            % 5. cr?ation du .info
            data_d.info = struct( 'td_info', ...
                rmfield(tdi(td_idx), {'execution_market_id', 'timezone', 'global_zone_name', ...
                'global_zone_suffix', 'default_timezone', 'localtime'}), ...
                'localtime', true, 'GMT_offset', GMT_offset, 'security_id', sec_id.security_id , ...
                'place_timezone_id', tdi(1).place_id,'data_datestamp',dt_day);
            if output_mode_d
                data{i} = data_d; 
            elseif ~st_data('isempty-nl', data_d)
                if isempty(data)
                    data = data_d;
                    data.title = sprintf('%s - %s -> %s', sec_id.security_key, datestr(dt_all(1), 'dd/mm/yyyy'), ...
                        datestr(dt_all(end), 'dd/mm/yyyy'));
                else
                    data.value = [data.value;data_d.value];
                    data.date  = [data.date;data_d.date];
                end
            end
        end
    case 'ft-1day' % inutile mais permet de faire des tests sur le mode 'ft'
        opt = options({ 'security_id', [], 'day', [], 'trading-destinations', '', ...
            'where_formula', [], 'last_formula', [], 'date-format', 'dd/mm/yyyy', ...
            'day-into-date', true}, varargin);
        
        % 1. r?f?rentiel et traduction td
        day_ = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, opt.get('date-format'));
        end
        % TODO1 et TODO2 rendront inutiles ces appels
        [sec_id, tdi] = get_repository( 'any-to-sec-td', 'security_id', opt.get('security_id') , ...
            'trading-destinations', opt.get('trading-destinations'));
        % TODO2 rendra inutile cette ligne
        GMT_offset = timezone('get_offset','init_place_id', 0,... % MAGIC NUMBER 0 => GMT
            'final_place_id', tdi(1).place_id, 'dates', day_); % MAGIC NUMBER 1 => destination primaire
        td_idx = find(ismember([tdi.trading_destination_id], [sec_id.trading_destination_id]));
        if opt.get('day-into-date')
            dt_day = day_;
        else
            dt_day = 0; 
        end
        % < le st_data dans lequel on va tout accumuler
        title_ = sprintf('%s - %s', sec_id.security_key, datestr(day_, 'dd/mm/yyyy') );
        coln = {'price', 'volume', 'sell', 'bid', 'ask',  ...
                'trading_destination_id', 'bid_size', 'ask_size', 'buy',...
                'cross', 'auction', 'opening_auction', 'closing_auction', 'trading_at_last', ...
                'trading_after_hours', 'intraday_auction', 'dark', 'order_book_id'};
        data = st_data('init','title',title_,'value', zeros(0, 18), ...
                'date', zeros(0, 1),...
                'colnames', coln);
        % >
        for i = 1 : length(td_idx)
            % < 2. r?cup data
            try % TODO remove it pour l'instant ?a permet d'?viter
                % l'erreur existant si le coupe sec_id td_id n'est pas dans l'univers
                
                
                data_td   = loadOrderBook(data_folder , ...
                    sec_id.security_id, datestr(day_, 'yyyymmdd'),...
                    sec_id.trading_destination_id(i), 0, 1, 5);% MAGIC NUMBER 0 => auctionType et 1=>trade only
                
                if tdi(td_idx(i)).localtime
                    dt_offset = 0;
                else
                    dt_offset = GMT_offset;
                end
                
                data_td.dates = data_td.dates  + dt_offset;
            catch
                data_td = st_data('init','title',title_,'value', zeros(0, 18), ...
                    'date', zeros(0, 1),...
                    'colnames', coln);
            end
            % >
            
            % < 3. filtres
            where_formula = opt.get('where_formula');
            if ~isempty( where_formula)
                nb_lines = length(data_td.date);
                data_td = st_data('where', data_td, where_formula); 
                st_log('get_tickdb2:where_formula application of <%s> reduce the dataset from %d to %d lines\n', ...
                    where_formula, nb_lines, length(data_td.date));
            end

            last_formula = opt.get('last_formula');
            if ~isempty( last_formula) && ~isempty( data_td.value)
                nb_cols = length(data_td.colnames);
                data_td = st_data('apply-formula', data_td, last_formula);
                st_log('get_tickdb2:last_formula application of <%s> changed the dataset from %d to %d cols\n', ...
                    last_formula, nb_cols, length(data_td.colnames));
            end
            % >
            
            % < 4.concat?nation
            data.value = [data.value; data_td.value];
            data.date  = [data.date; data_td.date];
            % >
        end
        [data.date, idx] = sort(data.date);
        data.value = data.value(idx, :);
        data.date = data.date + dt_day;
        
        % 5. cr?ation du .info
        data.info = struct( 'td_info', ...
            rmfield(tdi(td_idx), {'execution_market_id', 'timezone', 'global_zone_name', ...
            'global_zone_suffix', 'default_timezone', 'localtime'}), ...
            'localtime', true, 'GMT_offset', GMT_offset );
        
    case 'ft-1day-1td' % inutile mais permet de faire des tests sur le mode 'ft-1day'
        opt = options({ 'security_id', [], 'day', [], 'trading-destination', '', ...
            'where_formula', [], 'last_formula', [], 'date-format', 'dd/mm/yyyy', ...
            'day-into-date', true}, varargin);
        
        % 1. r?f?rentiel et traduction td
        day_ = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, opt.get('date-format'));
        end
        % TODO1 et TODO2 rendront inutiles ces appels
        [sec_id, tdi] = get_repository( 'any-to-sec-td', 'security_id', opt.get('security_id') , ...
            'trading-destinations', { opt.get('trading-destination') });
        % TODO2 rendra inutile cette ligne
        GMT_offset = timezone('get_offset','init_place_id', 0,... % MAGIC NUMBER 0 => GMT
            'final_place_id', tdi(1).place_id, 'dates', day_); % MAGIC NUMBER 1 => destination primaire
        td_idx = sec_id.trading_destination_id == [tdi.trading_destination_id];
        if tdi(td_idx).localtime
            dt_offset = 0;
        else
            dt_offset = GMT_offset;
        end
        if opt.get('day-into-date')
            dt_day = day_;
        else
            dt_day = 0; 
        end
        % 2. r?cup data
        data      = loadOrderBook(data_folder, ...
            sec_id.security_id, datestr(day_, 'yyyymmdd'),sec_id.trading_destination_id, 0, 1, 5);% MAGIC NUMBER 0 => auctionType et 1=>trade only
        date.date = data.date + dt_offset;
          
         % 3. cr?ation du .info
        data.info = struct( 'td_info', ...
            rmfield(tdi(td_idx), {'execution_market_id', 'timezone', 'global_zone_name', ...
                'global_zone_suffix', 'default_timezone', 'localtime'}), ...
            'localtime', true, 'GMT_offset', GMT_offset ) ;
        
        % 5. filtres
        where_formula = opt.get('where_formula');
        if ~isempty( where_formula)
            nb_lines = length(data.date);
            data = st_data('where', data, where_formula); 
            st_log('get_tickdb2:where_formula application of <%s> reduce the dataset from %d to %d lines\n', ...
                where_formula, nb_lines, length(data.date));
        end
        
        last_formula = opt.get('last_formula');
        if ~isempty( last_formula) && ~isempty( data.value)
            nb_cols = length(data.colnames);
            data = st_data('apply-formula', data, last_formula);
            st_log('get_tickdb2:last_formula application of <%s> changed the dataset from %d to %d cols\n', ...
                last_formula, nb_cols, length(data.colnames));
        end
        
    otherwise
        error('get_tickdb2:mode', 'MODE: mode <%s> unknown', mode);
end