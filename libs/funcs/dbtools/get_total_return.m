function tr = get_total_return( sec_id, EN_dt_from, EN_dt_to )
% get_total_return
% tr = get_total_return( 12058, '2013/01/01', '2014/10/31')
%
%
%
% See Also: get_repository.m (mode 'dividend'), read_trading_daily_legacy.m

from = datenum(EN_dt_from, 'yyyy/mm/dd');
to = datenum(EN_dt_to, 'yyyy/mm/dd');
SQL_from = datestr(from, 'yyyymmdd');
SQL_to = datestr(to, 'yyyymmdd');

if length(sec_id)==1
%     data = read_trading_daily_legacy( 'source', 'security', 'code', sec_id, 'fields', 'close_prc',...
%         'from', EN_dt_from, 'to', EN_dt_to , 'trading-destinations', {'MAIN'}, 'cac', true );
    data = read_trading_daily(sec_id, from, to, 'compute_null', false, 'split_adj', true, 'output', 'st_data',...
        'field', {'close_prc'},'trading_destinations',{'MAIN'},'convert_in_euro', false);
    if isempty(data.date)
        tr = nan;
        return
    end
    div = get_repository('dividend', sec_id, SQL_to);    
    
    if ~isempty(div.value)
        data = st_data('add-col', data, joint(div.date, st_data('cols', div, 'dividend'), data.date), 'gross_dividend');
    else
        data = st_data('add-col', data, zeros(size(data.date)), 'gross_dividend');
    end
    
    df = 1+st_data('cols', data, 'gross_dividend')./st_data('cols', data, 'close_prc');
    df(isnan(df))=1;
    tr = prod(df,1)*st_data('cols', data, 'close_prc',length(data.date))/st_data('cols', data, 'close_prc',1)-1;
    
else
    data = read_trading_daily_legacy( 'source', 'security', 'code', sec_id, 'fields', 'close_prc',...
        'from', EN_dt_from, 'to', EN_dt_to , 'trading-destinations', {'MAIN'}, 'cac', true );
    if isempty(data.date)
        tr = nan;
        return
    end
    div = get_repository('dividend', sec_id, SQL_to);
    div_ratio = joint([div.date,div.value(:,strcmp(div.colnames,'security_id'))],...
        div.value(:,strcmp(div.colnames,'dividend')),...
        [data.date,data.value(:,strcmp(data.colnames,'security_id'))])./...
        data.value(:,strcmp(data.colnames,'close_prc'))+1;
    div_ratio(isnan(div_ratio)) = 1;
    [un_sec,ix_first,idx_sec] = unique(data.value(:,strcmp(data.colnames,'security_id')),'first');
    [~,ix_last] = unique(data.value(:,strcmp(data.colnames,'security_id')),'last');
    tr = data.value(ix_last,strcmp(data.colnames,'close_prc'))./data.value(ix_first,strcmp(data.colnames,'close_prc')).*...
        accumarray(idx_sec,div_ratio,[],@prod)-1;
    tr = joint(un_sec,tr,sec_id);
end

end

