function data = get_trading_daily(mode, varargin)
% GET_TRADING_DAILY - Short_one_line_description
%
%
% Examples:
% %- ALL SECURITY FROM AN INDEX: get  ALL + MAIN trading deatination id 
% data1=get_trading_daily('base','indice_id',[1],'day_start','23/03/2012','day','04/04/2012','trading_destination_id','main','td_include_all_destination',true,'add_rate_to_euro',true,'add_place_id',true);
% %- INDEX
% data_indice_mult_zone=get_trading_daily('base_indice','security_id',[1 845],'day_start','23/03/2012','day','04/04/2012','add_codebook',true);
% %- SECURITY : get each single destination without the "ALL" destination
% data=get_trading_daily('base','security_id',[2 107509 6069 87915],'day_start','23/03/2011','day','04/04/2012');
% %- SECURITY : get each single destination AND the "ALL" destination
% data=get_trading_daily('base','security_id',[2 107509 6069 87915],'day_start','23/03/2011','day','04/04/2012','td_include_all_destination',true);
% %- SECURITY : get only the main trading deatination id 
% data1=get_trading_daily('base','security_id',[2 107509 6069 87915],'day_start','23/03/2011','day','04/04/2012','trading_destination_id','main');
% %- SECURITY : get only the ALL trading deatination id 
% data1=get_trading_daily('base','security_id',[2 107509 6069 87915],'day_start','23/03/2011','day','04/04/2012','trading_destination_id','none','td_include_all_destination',true);
% %- SECURITY : get  ALL + MAIN trading deatination id 
% data1=get_trading_daily('base','security_id',[2 107509 6069 87915],'day_start','23/03/2011','day','04/04/2012','trading_destination_id','main','td_include_all_destination',true);
% %- Others
% data2=get_trading_daily('base','security_id',[2 107509],'day_start','23/03/2011','day','04/04/2012','trading_destination_id',[4 61],'add_rate_to_euro',true);
% data_indice=get_trading_daily('base_indice','security_id',1,'day_start','23/03/2011','day','04/04/2012');
% data_only_alltd=get_trading_daily('base','security_id',[2 107509],'day_start','23/03/2011','day','04/04/2012','td_include_all_destination',true,'trading_destination_id','none','add_rate_to_euro',true);
% data_mult_zone=get_trading_daily('base','security_id',[2 87915],'day_start','23/03/2012','day','04/04/2012','td_include_all_destination',true,'add_rate_to_euro',true,'add_place_id',true,'add_codebook',true);
% data_indice_mult_zone=get_trading_daily('base_indice','security_id',[1 845],'day_start','23/03/2012','day','04/04/2012','add_codebook',true);
%
%
%
%
% See also:
%
%
%   author   : 'nijos'
%   reviewer : ''
%   date     :  '10/04/2012'
%
%   last_checkin_info : $Header: get_trading_daily.m: Revision: 11: Author: malas: Date: 01/08/2013 11:37:12 AM$
%



TRADING_DAILY_FIELDS={...
    'volume','open_volume','intraday_volume','close_volume','cross_volume','auction_volume','dark_volume','off_volume','end_volume','midclose_volume',...
    'turnover','open_turnover','intraday_turnover','close_turnover','cross_turnover','auction_turnover','dark_turnover','off_turnover','end_turnover','midclose_turnover',...
    'nb_deal','open_nb_deal','intraday_nb_deal','close_nb_deal','auction_nb_deal','dark_nb_deal','midclose_nb_deal',...
    'open_prc', 'high_prc', 'low_prc','close_prc',...
    'deal_amount_var_log','deal_amount_avg_log',...
    'average_spread_numer','average_spread_denom','overbid_volume','overask_volume'};

TRADING_DAILY_INDICE_FIELDS={'open_prc', 'high_prc', 'low_prc','close_prc'};

ALL_TD_NUM=-1;

switch lower(deblank(mode))
    
    
    case 'base'
        
        
        t0 = clock;
        
        
        %-------------------------------------------------------------------
        %< Lecture des options + tests
        %-------------------------------------------------------------------
        
        opt = options({'security_id', [],... % if isempty -> all security !
            'indice_id',[],...
            'day', '',...
            'day_start','',...
            'trading_destination_id',[],...% if isempty -> all td id / if "none" : 'td_include_all_destination' has to be true
            'td_fields',{},...% if isempty -> TRADING_DAILY_FIELDS
            'td_include_all_destination',false,...
            'add_place_id',false,...
            'add_rate_to_euro',false,...
            'add_codebook',false,...
            'date_format:char', 'dd/mm/yyyy' ...
            }, varargin);
        
        format_date = opt.get('date_format:char');
        sec_id = opt.get('security_id');
        indice_id = opt.get('indice_id');
        
        if ~isnumeric(sec_id) || ~isnumeric(indice_id)
            error('get_trading_daily:secid', 'I need numerical security id list');
        end
        if ~isempty(sec_id) && ~isempty(indice_id) 
            error('get_trading_daily:secid', 'Only security or indice');            
        end
        day_ = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        day_start_ = opt.get('day_start');
        
        if isempty(day_start_)
            day_start_=day_;
        elseif ischar( day_start_)
            day_start_ = datenum( day_start_, format_date);
        end
        
        trading_destination_id = opt.get('trading_destination_id');
        if ~isnumeric( trading_destination_id) && (~ischar(trading_destination_id) || (ischar(trading_destination_id) && ~any(strcmpi(trading_destination_id,{'main','none'}))))
            error('get_trading_daily:trading_destination_id', 'trading_destination_id has to be a numerical array or "none"');
        end
        
        if day_<day_start_
            error('get_trading_daily:day', 'bad inputs day - day_start');
        end
        start_day = datestr( day_start_, 'yyyymmdd');
        this_day = datestr( day_, 'yyyymmdd');
        
        
        %< Handle trading info fileds
        td_fields= opt.get('td_fields');
        if isempty(td_fields)
            td_fields=TRADING_DAILY_FIELDS;
        end
        if ischar(td_fields)
            td_fields=tokenize(td_fields,';');
        end
        td_fields=setdiff(td_fields,{'date','security_id','trading_destination_id'});
        if ~all(ismember(td_fields,TRADING_DAILY_FIELDS))
            error('get_trading_daily:field','At least one asked fields is not in the databse');
        end
        td_fields=cat(2,{'date','security_id','trading_destination_id'},...
            td_fields);
        %>
        
        %-------------------------------------------------------------------
        %<  -------- SQL REQUEST + EXTRACT DATA
        %-------------------------------------------------------------------
        
        select_str='';
        select_names={};
        
        
        ind_prefix='ind';
        td_prefix='td';
        td_fieldschar=cellfun(@(c)([td_prefix '.' c ',']),td_fields,'uni',false);
        td_fieldschar=[td_fieldschar{:}];
        td_fieldschar(end)=[];
        
        %-----------------
        %-- "select" part
        select_str = [select_str td_fieldschar];
        select_names=cat(2,select_names,td_fields);
        
        %-----------------
        %-- "where" and "end" part
        
        where_req = sprintf(['  where %s.date >= ''%s'' ' ...
            ' and %s.date <= ''%s'' ' ],td_prefix,start_day,td_prefix,this_day);
        
        if ~isempty(sec_id)
            sec_char = sprintf('%d,',sec_id);
            sec_char(end) = [];
            where_req=[where_req sprintf(' and %s.security_id in (%s) ',td_prefix,sec_char)];
            zone_security_list=get_repository('security_id2global_zone_suffix',sec_id);
        elseif ~isempty(indice_id)
            sec_char = sprintf('%d,',indice_id);
            sec_char(end) = [];
            where_req=[where_req ...
                sprintf(' and %s.indice_id in (%s) ',ind_prefix,sec_char) ...
                sprintf(' and %s.security_id = %s.security_id  ',ind_prefix,td_prefix) ...
                sprintf(' and %s.begin_date<= %s.date  ',ind_prefix,td_prefix) ...
                sprintf(' and ( %s.end_date >= %s.date or %s.end_date=NULL ) ',ind_prefix,td_prefix,ind_prefix ) ...
                ];
            select_str=[select_str sprintf(',%s.indice_id',ind_prefix)];
            select_names=cat(2,select_names,'indice_id');
            zone_security_list=get_repository('indiceid2global_zone_suffix',indice_id);          
        else
            zone_security_list={};
            warning('get_trading_daily:input','All security only works for EUROPE !!!!');
        end
        if ~isempty(trading_destination_id)
            if all(isnumeric(trading_destination_id))
                td_char = sprintf('%d,',trading_destination_id);
                td_char(end) = [];
                if opt.get('td_include_all_destination')
                    td_char=sprintf('%s,%s',td_char,'NULL');
                end
                where_req=[where_req sprintf(' and %s.trading_destination_id in (%s) ',td_prefix,td_char)];
            elseif ischar(trading_destination_id) && ...
                    strcmpi(trading_destination_id,'none') && ...
                    opt.get('td_include_all_destination')
                td_char='NULL';
                where_req=[where_req sprintf(' and %s.trading_destination_id in (%s) ',td_prefix,td_char)];
            elseif  ischar(trading_destination_id) && ...
                    strcmpi(trading_destination_id,'main')
                % if 'main' is aked, request will go on all, and we will filter after ....
                if ~opt.get('td_include_all_destination')
                    where_req=[where_req sprintf(' and %s.trading_destination_id != NULL ',td_prefix)];
                end
            else
                error('get_trading_daily:field','bad input of trading_destination_id');
            end
        elseif ~opt.get('td_include_all_destination')
            where_req=[where_req sprintf(' and %s.trading_destination_id != NULL ',td_prefix)];
        end
        
        end_req=sprintf(' order by  %s.date,%s.security_id,%s.trading_destination_id',td_prefix,td_prefix,td_prefix);
        
        %-----------------
        %-- REMARK : there can be security_id of multiple zone
        %-- BOUCLE by zone
        vals={};
        uni_zone = {' '};
        if ~isempty(zone_security_list)
            uni_zone=unique(zone_security_list(:,2));
        end
        
        for i_zone=1:length(uni_zone)
            
            main_req_zone='';
            if ~isempty(indice_id)
                main_req_zone=[main_req_zone sprintf('repository..indice_component_master %s , ',ind_prefix)];
            end
            main_req_zone=[main_req_zone sprintf(' %s..%s%s %s ','tick_db','trading_daily',uni_zone{i_zone},td_prefix)];
            
            select_str_zone=select_str;
            select_zone_names={};
            
            %-----------------
            % -- add place id
            if opt.get('add_place_id')
                sec_prefix='sec';
                
                main_req_add=sprintf([...
                    ' left join repository..security %s ' ...
                    ' on ( ' ...
                    ' %s.security_id = %s.security_id ) '],...
                    sec_prefix,...
                    td_prefix,sec_prefix);
                
                main_req_zone=[main_req_zone main_req_add];
                select_str_zone=[select_str_zone ', '...
                    sprintf('%s.place_id',sec_prefix)];
                select_zone_names=cat(2,select_zone_names,'place_id');
                
            end
            
            
            %-----------------
            % -- add rate to euro
            if opt.get('add_rate_to_euro')
                sechisto_prefix='sh';
                currencyp_prefix='cp';
                curencytm_prefix='crd';
                
                main_req_add_rate2euro=sprintf([...
                    ' left join repository..security_historic %s ' ...
                    ' on ( ' ...
                    ' %s.security_id = %s.security_id ' ...
                    ' and %s.begin_date<= %s.date ' ...
                    ' and ( %s.end_date >= %s.date or %s.end_date=NULL ) ' ...
                    ' ) ' ...
                    ' left join repository..currency_pair %s ' ...
                    ' on ( ' ...
                    ' (%s.currency_id = %s.ref_currency_id  and %s.currency_id = 58 ) or ' ...
                    ' (%s.currency_id = 58 and %s.currency_id = 58 and %s.ref_currency_id = 31 ) ' ...
                    ' ) ' ...
                    ' left join repository..histo_currency_time_serie %s ' ...
                    ' on ( ' ...
                    ' %s.date = %s.date and ' ...
                    ' %s.currency_pair_id = %s.currency_pair_id and ' ...
                    ' %s.attribut_id = 1 and %s.time_serie_id = 1 ) '],...
                    sechisto_prefix,...
                    sechisto_prefix,td_prefix,...
                    sechisto_prefix,td_prefix,...
                    sechisto_prefix,td_prefix,sechisto_prefix,...
                    currencyp_prefix,...
                    sechisto_prefix,currencyp_prefix,currencyp_prefix,...
                    sechisto_prefix,currencyp_prefix,currencyp_prefix,...
                    curencytm_prefix,...
                    td_prefix,curencytm_prefix,...
                    curencytm_prefix,currencyp_prefix,...
                    curencytm_prefix,curencytm_prefix);
                
                main_req_zone=[main_req_zone main_req_add_rate2euro];
                select_str_zone=[select_str_zone ', '...
                    sprintf('(case when %s.currency_id <> 58 then isnull(%s.quotation_unit,1)/convert(float, %s.value) when %s.currency_id = 58  then isnull(%s.quotation_unit, 1) end)',...
                    sechisto_prefix,sechisto_prefix,curencytm_prefix,sechisto_prefix,sechisto_prefix)];
                select_zone_names=cat(2,select_zone_names,'rate_to_euro');
                
            end
            
            %-----------------
            %< ---------- extract data
            sql_req = ['select ' select_str_zone ' from ' main_req_zone where_req end_req];
            sql_req= strrep(sql_req,'tick_db..','MARKET_DATA..');
            sql_req= strrep(sql_req,'repository..','KGR..');
            vals_zone = exec_sql('MARKET_DATA', sql_req);
            
            %-----------------
            %< ---------- add data
            vals=cat(1,vals,vals_zone);
            
        end
        select_names=cat(2,select_names,select_zone_names);
        
        %-------------------------------------------------------------------
        %<  -------- TRANSFORM DATA + CREATE OUTPUT
        %-------------------------------------------------------------------
        
        if isempty(vals)
            data = [];
            return;
        end
        
        
        %< normalize some values
        if opt.get('add_rate_to_euro')
            id_rate_to_euro=strcmp(select_names,'rate_to_euro');
            rate_to_euro=vals(:,id_rate_to_euro);
            rate_to_euro(cellfun(@(c)(strcmpi('null',c)),rate_to_euro))={NaN};
            vals(:,id_rate_to_euro)=rate_to_euro;
        end
        if opt.get('add_place_id')
            id_=strcmp(select_names,'place_id');
            place_id=vals(:,id_);
            place_id(cellfun(@(c)(strcmpi('null',c)),place_id))={NaN};
            vals(:,id_)=place_id;
        end
        
        
        %< handle trading destination
        if opt.get('td_include_all_destination')
            id_all_td=cellfun(@(c)(isnan(c)),vals(:,strcmp(select_names,'trading_destination_id')));
            vals(id_all_td,strcmp(td_fields,'trading_destination_id'))={ALL_TD_NUM};
        end
        
        if ischar(trading_destination_id) && ...
                strcmpi(trading_destination_id,'main')
            % we need to filter the trading destination id
            uni_sec_id=unique(cell2mat(vals(:,strcmp(select_names,'security_id'))));
            uni_td_id_main=get_repository('td_id_main',uni_sec_id);
            keep_sec_td=cat(2,uni_sec_id(isfinite(uni_td_id_main)),uni_td_id_main(isfinite(uni_td_id_main)));
            if opt.get('td_include_all_destination')
                keep_sec_td=cat(1,keep_sec_td,...
                    cat(2,uni_sec_id,ALL_TD_NUM*ones(length(uni_sec_id),1)));
            end
            if isempty(keep_sec_td)
                data = [];
                warning('trading destination id main filter has not keep any data !!');
                return;
            end     
            id_2keep=ismember(cell2mat(vals(:,nth_output(@ismember,2,{'security_id','trading_destination_id'},select_names))),keep_sec_td,'rows');
            if ~any(id_2keep)
                data = [];
                warning('trading destination id main filter has not keep any data !!');
                return;
            end             
            vals=vals(id_2keep,:);
        end
        %< cdk columns
        cdk=[];
        %>
        
        %<numeric cols td
        col_out=td_fields(~strcmp(td_fields,'date'));
        [id_ idx_in_]=ismember(col_out,select_names);
        vals_out=cellfun(@(c)(c*1),vals(:,idx_in_(id_)));
        %>
        
        %<numeric cols add
        id_add=ismember(select_names,{'rate_to_euro','place_id'});
        if any(id_add)
            col_out=cat(2,col_out,select_names(id_add));
            vals_out=cat(2,vals_out,...
                cellfun(@(c)(c*1),vals(:,id_add)));
        end
        %>       
        
        %<numeric cols add
        if ~isempty(indice_id)
            id_add=ismember(select_names,{'indice_id'});
            col_out=cat(2,col_out,select_names(id_add));
            vals_out=cat(2,vals_out,...
                cellfun(@(c)(c*1),vals(:,id_add)));
        end
        %> 
        
        %<Dates and times
        date=datenum(vals(:,strcmp(select_names,'date')), 'yyyy-mm-dd');
        vals_out=cat(2,date,vals_out);
        col_out=cat(2,'day',col_out);
        %>
        
        %< output building
        data = st_data( 'init', ...
            'title', sprintf( 'trading_daily extract : %s - %s', start_day, this_day), ...
            'value', vals_out, ...
            'date', date, ...
            'colnames',col_out);
        data.info =  ...
            struct( 'sql_request', sql_req, ...
            'data_log', data.info.data_log, ...
            'localtime',false,...
            'place_timezone_id', 0,...
            'local_place_timezone_id',NaN) ;
        data.codebook=cdk;
        %>
        
        
        % add codebook
        if opt.get('add_codebook')
            
            if any(strcmp(data.colnames,'trading_destination_id'))
                
                tmp_4cdk=get_repository('id2cdk','trading_destination_id',unique(st_data('col',data,'trading_destination_id')));
                if opt.get('td_include_all_destination')
                    cdk_tmp=codebook('new',cat(1,{sprintf('All TD(%d)',ALL_TD_NUM)},tmp_4cdk.book), 'trading_destination_id_cdk',cat(1,ALL_TD_NUM,tmp_4cdk.colnum));
                else
                    cdk_tmp=codebook('new',tmp_4cdk.book, 'trading_destination_id_cdk',tmp_4cdk.colnum);
                end
                data=st_data('add-col',data,st_data('col',data,'trading_destination_id'),'trading_destination_id_cdk');
                
                if ~isfield(data,'codebook') || isempty(data.codebook)
                    data.codebook=cdk_tmp;
                else
                    [data.codebook col_merge]=codebook('stack',data.codebook,cdk_tmp);
                    if ~isempty(col_merge)
                        error('get_trading_daily: in "add_cdk" bad stack of codebook !!!!');
                    end
                end
            end
            
            
            if any(strcmp(data.colnames,'security_id'))
                
                tmp_4cdk=get_repository('id2cdk','security_id',unique(st_data('col',data,'security_id')));
                cdk_tmp=codebook('new',tmp_4cdk.book, 'security_id_cdk',tmp_4cdk.colnum);
                data=st_data('add-col',data,st_data('col',data,'security_id'),'security_id_cdk');
                
                if ~isfield(data,'codebook') || isempty(data.codebook)
                    data.codebook=cdk_tmp;
                else
                    [data.codebook col_merge]=codebook('stack',data.codebook,cdk_tmp);
                    if ~isempty(col_merge)
                        error('get_trading_daily: in "add_cdk" bad stack of codebook !!!!');
                    end
                end
            end
            
            
            if any(strcmp(data.colnames,'place_id'))
                
                tmp_4cdk=get_repository('id2cdk','place_id',unique(st_data('col',data,'place_id')));
                cdk_tmp=codebook('new',tmp_4cdk.book, 'place_id_cdk',tmp_4cdk.colnum);
                data=st_data('add-col',data,st_data('col',data,'place_id'),'place_id_cdk');
                
                if ~isfield(data,'codebook') || isempty(data.codebook)
                    data.codebook=cdk_tmp;
                else
                    [data.codebook col_merge]=codebook('stack',data.codebook,cdk_tmp);
                    if ~isempty(col_merge)
                        error('get_trading_daily: in "add_cdk" bad stack of codebook !!!!');
                    end
                end
            end
            
        end
        st_log('end of get_trading_daily:base:exec (%5.2f sec)\n', etime(clock, t0));
        
        
        
        
        
        
        
        
    case 'base_indice'
        
        
        t0 = clock;
        
        %-------------------------------------------------------------------
        %< Lecture des options + tests
        %-------------------------------------------------------------------
        opt = options({'security_id', [],... % if isempty -> all security !
            'day', '',...
            'day_start','',...
            'td_fields',{},...% if isempty -> TRADING_DAILY_INDICE_FIELDS
            'add_codebook',false,...
            'date_format:char', 'dd/mm/yyyy' ...
            }, varargin);
        
        %< Lecture des options
        format_date = opt.get('date_format:char');
        sec_id = opt.get('security_id');
        if ~isnumeric(sec_id)
            error('get_trading_daily:secid', 'I need numerical security id list');
        end
        if isempty(sec_id)
            error('get_trading_daily:secid', 'security id has to be non - empty');
        end
        day_ = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        day_start_ = opt.get('day_start');
        
        if isempty(day_start_)
            day_start_=day_;
        elseif ischar( day_start_)
            day_start_ = datenum( day_start_, format_date);
        end
        
        %<
        if day_<day_start_
            error('get_trading_daily:day', 'bad inputs day - day_start');
        end
        start_day = datestr( day_start_, 'yyyymmdd');
        this_day = datestr( day_, 'yyyymmdd');
        %>
        
        %< Handle trading info fileds
        td_fields= opt.get('td_fields');
        if isempty(td_fields)
            td_fields=TRADING_DAILY_INDICE_FIELDS;
        end
        if ischar(td_fields)
            td_fields=tokenize(td_fields,';');
        end
        td_fields=setdiff(td_fields,{'date','indice_id'});
        if ~all(ismember(td_fields,TRADING_DAILY_INDICE_FIELDS))
            error('get_trading_daily:field','At least one asked fields is not in the databse');
        end
        td_fields=cat(2,{'date','indice_id'},...
            td_fields);
        %>
        
        %-------------------------------------------------------------------
        %<  -------- SQL REQUEST + EXTRACT DATA
        %-------------------------------------------------------------------
        
        select_str='';
        select_names={};
        
        
        td_prefix='td';
        td_fieldschar=cellfun(@(c)([td_prefix '.' c ',']),td_fields,'uni',false);
        td_fieldschar=[td_fieldschar{:}];
        td_fieldschar(end)=[];
        
        %-----------------
        %-- "select" part
        select_str = [select_str td_fieldschar];
        select_names=cat(2,select_names,td_fields);
        
        %-----------------
        %-- "where" and "end" part
        
        sec_char = sprintf('%d,',sec_id);
        sec_char(end) = [];
        
        where_req = sprintf(['  where %s.date >= ''%s'' ' ...
            ' and %s.date <= ''%s'' ' ...
            ' and %s.indice_id in (%s) '],td_prefix,start_day,td_prefix,this_day,td_prefix,sec_char);
        
        end_req=sprintf(' order by  %s.date,%s.indice_id',td_prefix,td_prefix);
        
        
        zone_security_list=get_repository('indiceid2global_zone_suffix',sec_id);
        
        
        %-----------------
        %-- REMARK : there can be security_id of multiple zone
        %-- BOUCLE by zone
        vals={};
        uni_zone = {' '};
        if ~isempty(zone_security_list)
            uni_zone=unique(zone_security_list(:,2));
        end
        
        for i_zone=1:length(uni_zone)
            
            main_req_zone=sprintf(' %s..%s%s %s ','tick_db','indice_daily',uni_zone{i_zone},td_prefix);
            select_str_zone=select_str;
            select_zone_names={};
            
            %< ---------- extract data
            sql_req = ['select ' select_str_zone ' from ' main_req_zone where_req end_req];
            vals_zone = exec_sql('BSIRIUS', sql_req);
            
            vals=cat(1,vals,vals_zone);
            
        end
        select_names=cat(2,select_names,select_zone_names);
        
        %-------------------------------------------------------------------
        %<  -------- TRANSFORM DATA + CREATE OUTPUT
        %-------------------------------------------------------------------
        
        if isempty(vals)
            data = [];
            return;
        end
        
        %< cdk columns
        cdk=[];
        %>
        
        %<numeric cols
        col_out=td_fields(~strcmp(td_fields,'date'));
        [id_ idx_in_]=ismember(col_out,select_names);
        vals_out=cellfun(@(c)(c*1),vals(:,idx_in_(id_)));
        %>
        
        %<Dates and times
        date=datenum(vals(:,strcmp(select_names,'date')), 'yyyy-mm-dd');
        vals_out=cat(2,date,vals_out);
        col_out=cat(2,'day',col_out);
        %>
        
        %< output building
        
        data = st_data( 'init', ...
            'title', sprintf( 'trading_daily_indice extract : %s - %s', start_day, this_day), ...
            'value', vals_out, ...
            'date', date, ...
            'colnames',col_out);
        data.info =  ...
            struct( 'sql_request', sql_req, ...
            'data_log', data.info.data_log, ...
            'localtime',false,...
            'place_timezone_id', 0,...
            'local_place_timezone_id',NaN) ;
        data.codebook=cdk;
        %>
        
        % add codebook
        if opt.get('add_codebook')
            if any(strcmp(data.colnames,'indice_id'))
                
                tmp_4cdk=get_repository('id2cdk','indice_id',unique(st_data('col',data,'indice_id')));
                cdk_tmp=codebook('new',tmp_4cdk.book, 'indice_id_cdk',tmp_4cdk.colnum);
                data=st_data('add-col',data,st_data('col',data,'indice_id'),'indice_id_cdk');
                
                if ~isfield(data,'codebook') || isempty(data.codebook)
                    data.codebook=cdk_tmp;
                else
                    [data.codebook col_merge]=codebook('stack',data.codebook,cdk_tmp);
                    if ~isempty(col_merge)
                        error('get_trading_daily: in "add_cdk_td_id" bad stack of codebook !!!!');
                    end
                end
            end
            
        end
        st_log('end of get_trading_daily:base_indice:exec (%5.2f sec)\n', etime(clock, t0));
        
        
        
    otherwise
        error('get_trading_daily:mode', 'MODE: <%s> unknown', mode);
        
end

end