function td_kgr = get_trading_destination_mapping(td_id,sec_id)
% get_trading_destination_mapping returns the mapping of Cheuvreux
% destination to KGR destination as it appears in KGR..EXCHANGEMAPPING

persistent map
persistent time % TIME for timeout
global st_version
repository = st_version.bases.repository;

if isempty(map) || now-time>0.5 % Timeout d'une demi-journ�e
    map = cell2mat(exec_sql('KGR',['select trading_destination_id,EXCHANGE',...
        ' from ',repository,'..EXCHANGEMAPPING',...
        ' where trading_destination_id is not null and EXCHANGE is not null']));
    time = now;
end
td_kgr = joint(map(:,1),map(:,2),td_id);


% Y a-t-il des NaN dans td_kgr ? 
% Si oui v?rifions qu'il s'agit de donn?es US :
% - oui -> td_kgr = destination primaire
% - non -> on place un signe '-' devant la trading destination.

if any(isnan(td_kgr))
    if ~all(isnan(td_kgr))
        td_str = sprintf('%d,',unique(td_kgr(~isnan(td_kgr))));
        timezone_exch = exec_sql('KGR',['select TIMEZONE,EXCHANGE from ',repository,'..EXCHANGEREFCOMPL',...
            ' where EXCHANGE in (',td_str(1:end-1),')']);
    else
        prim_dest_id = cell2mat(exec_sql('KGR',['select trading_destination_id from ',repository,'..security_market',...
            ' where security_id = ',num2str(sec_id),...
            ' and ranking = 1']));
        if ~isempty(prim_dest_id)
            timezone_exch = exec_sql('KGR',['select TIMEZONE,EXCHANGE from ',repository,'..EXCHANGEREFCOMPL',...
                ' where EXCHANGE in (',num2str(prim_dest_id),')']);
        else
            error('get_trading_destination_mapping:missing_SYMBOL6','SYMBOL6 <%d> does not exist anymore in databases',sec_id)
        end
    end
    if any(strcmp(timezone_exch(:,1),'America/New_York'))
        % Get primary market
        prim_dest_id = cell2mat(exec_sql('KGR',['select trading_destination_id from ',repository,'..security_market',...
            ' where security_id = ',num2str(sec_id),...
            ' and ranking = 1']));
        td_kgr = prim_dest_id*ones(length(td_kgr),1); % <- in the US there is only one destination!
    else
        td_kgr(isnan(td_kgr)) = -td_id(isnan(td_kgr));
    end
end