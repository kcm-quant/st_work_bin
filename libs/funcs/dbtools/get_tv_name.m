function tv_name = get_tv_name(tv_id)
% GET_TV_NAME - get trading venue name
%
% Examples:
% tv_name=get_tv_name([-1 2 4 -2]);
% tv_name=get_tv_name([109 13 53]);
% See also:
%
%   author   : 'njoseph@cheuvreux.com'
%   reviewer : ''
%   date     :  '20/09/2011'
%
%   last_checkin_info : $Header: get_tv_name.m: Revision: 1: Author: nijos: Date: 09/21/2011 02:38:36 PM$

tv_name=cell(length(tv_id),1);
if any(isfinite(tv_id))
    idx_1=find(isfinite(tv_id));
    tv_info=exec_sql('BSIRIUS',sprintf(['select trading_venue_id, name ' ...
        ' from repository..trading_venue_info ' ...
        ' where trading_venue_id in %s '],num_to_char(tv_id(idx_1))));
    if ~isempty(tv_info)
        [id_2 idx_2]=ismember(cell2mat(tv_info(:,1)),tv_id');
        tv_name(idx_1(idx_2(id_2)))=tv_info(id_2,2);
    end
end

tv_name(cellfun(@(c)(isempty(c)),tv_name))={'Unknown'};

end




function value_list=num_to_char(value_vect)
value_list=[];
for i_occ=1:length(value_vect)
    if i_occ<length(value_vect)
        value_list=[value_list,'',num2str(value_vect(i_occ)),'',','];
    else
        value_list=[value_list,'',num2str(value_vect(i_occ)),''];
    end
end
value_list=['(',value_list,')'];
end