function index_list = index_regexp(pattern)
% INDEX_REGEXP - Filtre les noms des indices par une expression r�guli�re
index_list = get_repository('index-list');
index_list = index_list(~cellfun('isempty', regexpi(index_list, pattern)));