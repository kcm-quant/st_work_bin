function bool = kech_td_mapping_needed(data)
% R�sum� de la r�gle de filtrage ci-dessous. On conserve les cas o� :
% * La fonction appelante est get_tick, basic_indicator ou bien get_gi en
%   mode 'basic_indicator'
% * La version du st_data n'est ni kepche_1, ni matgen_1.0, ni
%   matgenNet_1.0 (le cas sans info sur la version est consid�r� comme
%   positif).

stack = dbstack;
if length(stack) <= 1
    bool = false;
    return
end
fct = stack(2).name;
if strcmp(fct,'from_buffer')
    fct = evalin('caller','fct');
    mode = evalin('caller','fmode');
elseif strcmp(fct,'get_gi')
    mode = evalin('caller','mode');
end

bool = ...
    (...
        strcmp(fct,'get_tick') || strcmp(fct,'get_basic_indicator_v2') || ...
            (strcmp(fct,'get_gi') && strcmp(mode,'basic_indicator'))...
    )  && ~isempty(data) && ~isempty(data.value) &&...
    (...
        ~isfield(data.info,'version') || ...
            (~strcmp(data.info.version,'kepche_1') && ~strcmp(data.info.version,'matgen_1.0') && ~strcmp(data.info.version,'matgenNet_1.0'))...
    );