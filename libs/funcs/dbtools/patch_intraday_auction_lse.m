function data_out = patch_intraday_auction_lse(data_in)
% PATCH_INTRADAY_AUCTION_LSE - correction de la colonne phase
% La correction n'est appliqu�e que si la valeur cote sur un des
% destinations de TD_ID_LIST.
% Correction des phase d'auction intraday en se basant sur l'information
% fournie par trading_time_interpret

% HARD CODED VARIABLES
TD_ID_LIST = [34,55];

% Creation d'un cache pour optimiser le nombre de requetes en base
persistent phase_id td_info security_id

if iscell(data_in)
    L = length(data_in);
    data_out = cell(L,1);
    for l = 1:L
        data_out{l} = patch_intraday_auction_lse(data_in{l});
    end
    return
end

if isempty(data_in) || isempty(data_in.date) || ~any(strcmp(data_in.colnames,'phase'))
    data_out = data_in;
    return
end

if isfield(data_in,'trading_destination_id')
    idx_lse = ismember(st_data('cols',data_in,'trading_destination_id'),TD_ID_LIST);
    if ~any(idx_lse)
        data_out = data_in;
        return
    end
else
    idx_lse = true(length(data_in.date),1);
end


if isempty(phase_id)
    [~,phase_id] = trading_time_interpret([],[]);
end

idx_ia_phase = ismember(st_data('cols',data_in,'phase'),[phase_id.ID_FIXING1,phase_id.ID_FIXING2]);
idx_lse = idx_lse&idx_ia_phase;
if ~any(idx_lse)
    data_out = data_in;
    return
end

data_lse = st_data('from-idx',data_in,idx_lse);
if isfield(data_lse,'info') && isfield(data_lse.info,'td_info') && ...
        ~isempty(data_lse.info.td_info)
    new_td_info = data_lse.info.td_info;
elseif isfield(data_lse,'info') && isfield(data_lse.info,'security_id')
    if isempty(security_id) || security_id~=data_lse.info.security_id || isempty(td_info)
        security_id = data_lse.info.security_id;
        new_td_info = get_repository('tdinfo', security_id);
    else
        new_td_info = td_info;
    end
else
    data_out = data_in;
    return
end
td_info = new_td_info;

if isempty(td_info) || ~any(td_info(1).trading_destination_id==TD_ID_LIST)
    data_out = data_in;
    return
end

[phase_th, ~, ~, ~, ~] = trading_time_interpret(td_info, floor(data_lse.date(1)),...
    'context_selection','span_all','force_unique_intraday_auction',false);
phase = phase_th(ismember([phase_th.phase_id],[phase_id.ID_FIXING1,phase_id.ID_FIXING2]));

for p = 1:length(phase)
    idx_phase = phase(p).begin<=mod(data_lse.date,1)&phase(p).end+2/24/60>=mod(data_lse.date,1);
    data_lse.value(idx_phase,strcmp(data_lse.colnames,'phase')) = phase(p).phase_id;
end

data_out = data_in;
data_out.value(idx_lse,:) = data_lse.value;