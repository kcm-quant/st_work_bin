function base = get_basename4day( day)
% GET_BASENAME - return the correct basename from the day to request
%   get_basename4day( datenum(2008,06,01))
%   get_basename4day( datenum(2008,02,01))
%   get_basename4day( datenum(2008,08,01))
%   get_basename4day( datenum(2008,08,31))
%   get_basename4day( datenum(2007,08,01))
%   get_basename4day( datenum(2009,08,01))
%   get_basename4day( today )

global st_version
persistent hard_table

if isfield(st_version.my_env, 'kepche_tick') && st_version.my_env.kepche_tick
    base = 'MARKETDATA';
    return;
end

if isempty( hard_table)
    hard_table_raw = { ...
    'tick_db_07d_2008', datenum(2008,07,01), datenum(2008, 07, 31) ; ...
    'tick_db_08d_2008', datenum(2008,08,01), datenum(2008, 08, 31) ; ...
    'tick_db_09d_2008', datenum(2008,09,01), datenum(2008, 09, 30) ; ...
    'tick_db_10d_2008', datenum(2008,10,01), datenum(2008, 10, 31) ; ...
    'tick_db_11d_2008', datenum(2008,11,01), datenum(2008, 11, 30) ; ...
    'tick_db_12d_2008', datenum(2008,12,01), datenum(2008, 12, 31) ; ...
    'tick_db_q1d_2008', datenum(2008,01,01), datenum(2008, 03, 31) ; ...
    'tick_db_q2d_2008', datenum(2008,03,01), datenum(2008, 06, 30) ; ...
    'tick_db_q1_2007' , datenum(2007,01,01), datenum(2007, 03, 31) ; ...
    'tick_db_q2_2007' , datenum(2007,04,01), datenum(2007, 06, 30) ; ...
    'tick_db_q3_2007' , datenum(2007,07,01), datenum(2007, 09, 30) ; ...
    'tick_db_q4_2007' , datenum(2007,10,01), datenum(2007, 12, 31) };
    hard_table = struct('name', { hard_table_raw(:,1) }, 'from', cell2mat( hard_table_raw(:,2)), 'to', cell2mat( hard_table_raw(:,3)) );
end

[y, m] = datevec(day);
if y>=2009
    if m>9
        m_str = '';
    else
        m_str = '0';
    end
    base = sprintf('tick_db_%s%dd_%d', m_str, m, y);
else
    idx  = (day >= hard_table.from) & (day <= hard_table.to);
    if sum( idx)==0
        error( 'get_basename4day:range', 'this date <%s> does not seem to be in any identified database', datestr(day, 'dd/mm/yyyy'));
    end
    base = hard_table.name{ idx};
end