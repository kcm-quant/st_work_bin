function data = read_dataset(mode, varargin)
% READ_DATASET - bufferization locale, requ�tage en base de donn�s,
%                   filtrage et mise en forme des donn�es
%
% NEW!!!
%
%  data = read_dataset('gi:basic_indicator/source:char�"future', 'security_id',-1, 'from', '15/01/2009', 'to', '18/01/2009', 'trading-destinations', {})
%  data = read_dataset('future', 'security_id', -1, 'from', '15/01/2009', 'to', '18/01/2009')
%  data = read_dataset('btickdb', 'security', 'ENEI.MI', 'from', '01/04/2008', 'source', 'idn_selectfeed',  ...
%               'to', '10/04/2008' , 'remove', '02/04/2008;03/04/2008' ,'trading-destinations', {  } )
%  data = read_dataset('btickdb', 'security_id', 12058, 'from', '01/04/2008', ...
%               'to', '10/04/2008' , 'remove', '02/04/2008;03/04/2008' ,'trading-destinations', { 'main' } )
%  data = read_dataset('gi:basic_indicator', 'security_id', 276, 'from', '05/05/2008', 'to', '07/05/2008', 'trading-destinations', {})
%  data = read_dataset('gi:volatility2fpm', 'security_id', 276, 'from', '05/05/2008', 'to', '07/05/2008', 'trading-destinations', {})
%  mav  = build_slicer( 'moving-avg', @(x)mean(x) )
%  data = read_dataset('slicer:mavg/window�100�step�10�type�"p�colnames�"price', 'security_id', 110, 'from', '01/01/2009', 'to', '30/01/2009', 'trading-destinations', {'MAIN'})
%
% exemples :
%
% mode orderbook :
% data0 = read_dataset('ob', 'security', 'FTE.PA', 'from', '28/01/2008', 'to','28/01/2008')
%
% mode full_tickdb :
%   tic;data1 = read_dataset('bfull_tickdb', 'security', 'TOTF.PA', 'from', '01/04/2007', 'to','31/12/2007');toc
%
% mode tickdb :
%   tic;data2 = read_dataset('tickdb', 'security', 'TOTF.PA', 'from', '01/04/2007', 'to','31/12/2007');toc
%   tic;data3 = read_dataset('btickdb', 'security', 'TOTF.PA', 'from', '01/04/2007', 'to','31/12/2007');toc % second call will be faster, but this costs some space disk. You can use 'tickdb' mode to disable this feature
%   tic;data4 = read_dataset('btickdb', 'security', 'TOTFpa.CHI', 'from', '01/04/2007', 'to','31/12/2007');toc % second call will be faster, but this costs some space disk. You can use 'tickdb' mode to disable this feature
%
% mode deal_and_orderbook :
% data0 = read_dataset('deal_and_orderbook', 'security', 'FTE.PA', 'from', '28/01/2008', 'to','28/01/2008')
%
% mode index
%  data = read_dataset('index', 'security_id', 'CAC40', 'from', '20/10/2008', 'to', '21/10/2008'  )
%
% mode darkpool
%  data = read_dataset('darkpool-em',  'security_id', 'FTE.PA', 'from', '20/10/2008', 'to', '21/10/2008' )
%  option: 'filter-sec' = true
%
% mode tca
% data = read_dataset('tca', 'security-id', 110, 'trading-destination-id', 4, 'from', '01/01/2009', 'to', '05/01/2009', 'exec-cond', 'VWAP')
% data = read_dataset('tca', 'from', '01/01/2009', 'to', '05/01/2009', 'exec-cond', 'VWAP')
%
% mode corporate exec
% data = read_dataset( 'corporate_exec', 'security_id', 18, 'operator_code', 103,  'from', '05/05/2009', 'to', '11/05/2009')
%
% mode orderbook closing auction
% data = read_dataset('close-ob', 'security-id', 110, 'trading-destination-id', 4, 'from', '01/01/2009', 'to', '05/01/2009')
%
% mode param�tres SE
%   read_dataset('se/market-impact', 'security_id', 110, 'trading-destinations', 4, 'from', '01/05/2009', 'to', '30/05/2009')
%   read_dataset('se/volume-curve', 'security_id', 110, 'trading-destinations', 4, 'from', '01/05/2009', 'to', '30/05/2009')
%   read_dataset('se/volatility-curve', 'security_id', 110, 'trading-destinations', 4, 'from', '01/05/2009', 'to', '30/05/2009')
%   read_dataset('se/closing-fixing', 'security_id', 110, 'trading-destinations', 4, 'from', '01/05/2009', 'to', '30/05/2009')
%
% mode courbes de trading
%   read_dataset('trading-curve/cash-neutral', 'security-id', [26, 110], 'trading-destination-id', [4, 4], 'from', '01/05/2009', 'to', '30/05/2009')
%
% mode boat (output-mode = day only)
%   data = read_dataset( 'boat', 'security_id', 18, 'from', '04/01/2010', 'to', '04/01/2010')
%
% mode daily (pas de buffer):
%   data5 = read_dataset('daily', 'security', 'NWG.L', 'from', '05/06/2007', 'trading-destinations', {'MAIN' })
%
% mode flowdec
%   data_fd = read_dataset('flowdec', 'security_id', 'FTE.PA', 'from', '20/03/2012', 'to', '27/03/2012' )
%
% - Bufferiser les donn�es (full_tickdb) les plus r�centes pour les indices :
%       'FTSE 350', 'SBF250', 'IBEX35', 'BEL20', 'DAX', 'MIB30' :
% read_dataset('bufferize')
%
%
% - Effacer les donn�es bufferis�e qui sont vides et qui sont dans le dossier st_repository\read_dataset\tickdb:
% read_dataset('clean', 'tickdb')
%
%
% mode get_se
% read_dataset(['se/' 'volume curve'],'mode' , 'security' , 'estimator','volume curve' , 'security_id' , 110,'trading_destination_id',4,'is_valid', 1 , 'backup_process','default')
% read_dataset(['se/' 'volume curve'],'mode' , 'run', 'run_id' , 1209553, 'backup_process','none' )
%
%
% see also from_buffer get_tick get_gi get_slicer get_darkpool get_boat flow_dec

global st_version

idx_slash = strfind( mode, '/');
if ~isempty( idx_slash)
    mode_params = mode(idx_slash(1)+1:end);
    mode = mode(1:idx_slash(1)-1);
else
    mode_params = '';
end

switch lower(mode)
    
    case 'se'
        %<* R�cup�ration des donn�es du SE
        
        %mode = 'security';
        
        opt = options({'mode' , 'none' , ...
            'security_id' , [] , ...
            'trading_destination_id' , [] , ...
            'varargin_', '', ...
            'output_mode', 'params', ...
            'backup_process', 'backup', ...
            'is_valid', 1 , ...
            'run_id' , -1 , ...
            'as_of_date' , datestr(today , 'dd/mm/yyyy'),...
            'retrieve_all_context', true}, varargin);
        
        mode = opt.get('mode');
        if strcmpi(mode, 'none')
            mode = 'security';
        end
        is_valid = opt.get('is_valid');
        backup = opt.get('backup_process');
        
        if opt.contains_key('as_of_date')
            as_of_date = opt.get('as_of_date');
        elseif opt.contains_key('from')
            as_of_date = opt.get('from');
        end
        
        switch mode
            case 'security'
                td = [];
                if opt.contains_key('trading-destinations')
                    td = opt.get('trading-destinations');
                elseif opt.contains_key('trading_destination_id')
                    td = opt.get('trading_destination_id');
                end
                
                
                data = {from_buffer( 'get_se', mode, 'estimator', mode_params, ...
                    'security_id', opt.get('security_id'), ...
                    'trading_destination_id', td, ...
                    'varargin_' , opt.get('varargin_') ,...
                    'is_valid', is_valid, ...
                    'backup_process', backup, ...
                    'output_mode', opt.get('output_mode'), ...
                    'from', as_of_date, ...
                    'to', as_of_date , ...
                    'as_of_date', as_of_date,...
                    'retrieve_all_context', true)};
                
            case 'run'
                
                data = {from_buffer( 'get_se', mode, 'estimator', mode_params, ...
                    'run_id', opt.get('run_id'), ...
                    'security_id', opt.get('security_id'), ...
                    'backup_process', backup, ...
                    'is_valid' , 0 , ...
                    'from', as_of_date, ...
                    'to', as_of_date , ...
                    'as_of_date', as_of_date,...
                    'retrieve_all_context', true)};
            otherwise
                error('<read_dataset/se> :: Input argument ''mode'' not assigned during call to read_dataset/se ');
                
        end
        %>*
        
        %<* Pour GED
    case {'fullfunc', 'atdatesfunc'}
        data = from_buffer( 'getFunctionalIndicatorsValues', mode, varargin{:});
    case {'optim'}
        data = from_buffer( 'getOptimizedStrategyLevels', 'optim', varargin{:});
    case {'filtered', 'returnwithstats'}
        data = from_buffer( 'getMarketData', mode, varargin{:});
    case {'fullindicator', 'atstrategydates', 'fullindicatorwithstats', 'atstrategydateswithstats'}
        data = from_buffer( 'getIndicatorsValues', mode, varargin{:});
    case 'fullstrategy'
        data = from_buffer( 'getStrategyAtStrategyDates', 'fullstrategy', varargin{:});
        %>*
        
    case 'close-ob'
        %<* Pour la target close
        data = from_buffer( 'get_close_ob', 'ob', varargin{:});
        %>*
    case 'boat'
        %<* Pour les donn�es boat
        data = from_buffer( 'get_boat', 'boat', varargin{:});
        if iscell(data)
            st_log('read_dataset:boat cleanning day bu day codebooks...\n');
            for d=1:length(data)
                data{d} = codebook( 'clean', data{d}, 'destination code');
            end
        else
            data = codebook( 'clean', data, 'destination code');
        end
        %>*
        
    case 'get_marketevent'
        %<* Pour la detection de manipulations
        data = from_buffer('get_marketevent', varargin{:});
        %>*
        
    case 'tca'
        %<* Pour l'analyse de performance
        data = from_buffer( 'get_tca', 'co', varargin{:});
        %>*
    case 'corporate_exec'
        %<* Pour les exec corporate
        data = from_buffer( 'get_corporate_exec', 'read', varargin{:});
        %>*
        
    case { 'em', 'euromillennium', 'darkpool-em'}
        %<* Pour les darkpool
        data = get_darkpool('euromillenium', varargin{:});
        %>*
    case 'exec'
        data = get_exec('orders', varargin{:});
        
    case 'seq_stats'
        %<* Pour l'analyse de performance
        data = from_buffer( 'get_exec_algo_stats', 'seq_stats', varargin{:});
        %>*
        
    case 'algo_vc_curve'
        %<* Pour r�cup�rer la cb de volume utilis� par l'algo
        data = from_buffer('search_se_validated_volume_curve', 'run', varargin{:});
        %>*
        
    case 'flowdec'
        %<*To get the flow decomposition
        data = from_buffer( 'flow_dec', 'dealbook',  varargin{:});
        %>*
        
    case 'index'
        %<* Pour les indices
        data = from_buffer( 'get_index_data', 'indexfull', varargin{:});
        %>*
    case 'future'
        %<* Pour les futures
        %data = get_future('future',  varargin{:});
        data = get_future('filter-maturity', varargin{:});
        %>*
    case 'orderbook_limit'
        %<* Pour r�cup�rer la cb de volume utilis� par l'algo
        data = from_buffer('get_orderbook_limit', 'orderbook_by_destination', varargin{:});
        %>*
    case 'fixing'
        data = from_buffer( 'get_prefixing_theoretic', 'all-dest', varargin{:});
    case { 'tickdb', 'btickdb' }
        %<* Pour tickdb (un peu filtr�)
        % appel � |get_tick|
        opt = options( { 'security', ''}, varargin);
        if ~isempty( mode_params)
            mode_params = str2opt( mode_params);
            lst_ = mode_params.get();
            opt.set( lst_{:});
        end
        lst = opt.get();
        data = get_tick('btickdb', 'security_id', opt.get('security') , lst{:} );
        %>*
    case {'tick4resilience_imbalance','tick4ri'}
        opt = options( varargin);
        if ~isempty( mode_params)
            mode_params = str2opt( mode_params);
            lst_ = mode_params.get();
            opt.set( lst_{:});
        end
        lst = opt.get();
        data = get_tick('tick4resilience_imbalance', lst{:} );
    case {'tick4bi','tick4simap','tick4cfv', 'mo_tick', 'spread and tick'}
        %<* Pour tickdb (avec auctions)
        % appel � |get_tick|
        opt = options( varargin);
        if ~isempty( mode_params)
            mode_params = str2opt( mode_params);
            lst_ = mode_params.get();
            opt.set( lst_{:});
        end
        lst = opt.get();
        data = get_tick(mode, lst{:} );
        %>*
    case {'ft', 'fulltick'}
        if ~st_version.my_env.switch_full_tick2tbt2
            opt = options(varargin);
            if ~isempty( mode_params)
                mode_params = str2opt( mode_params);
                lst_ = mode_params.get();
                opt.set( lst_{:});
            end
            lst = opt.get();
            data = get_tick('ft', lst{:});
        else
            idx_td  = find(strcmp('trading-destinations', varargin(1:2:end)));
            varargin{2*idx_td} = [];
            data = get_tickdb2( 'ft', varargin{:});
        end
    case {'fc', 'fullcandles'}
        %< R�cup�ration de candles 1 minutes, indispensables pour les
        %titres am�ricains les plus liquides, utilis� par basic indicator
        %pour tout ce qui est hors Europe
        opt = options( varargin);
        if ~isempty( mode_params)
            mode_params = str2opt( mode_params);
            lst_ = mode_params.get();
            opt.set( lst_{:});
        end
        lst = opt.get();
        if opt.try_get('today_allowed',false)
            if (isnumeric(opt.get('from')) && floor(opt.get('from')) ~= today) || ...
                    (ischar(opt.get('from'))    && datenum(opt.get('from'),'dd/mm/yyyy') ~= today) || ...
                    (isnumeric(opt.get('to'))   && floor(opt.get('to')) ~= today) || ...
                    (ischar(opt.get('to'))      && datenum(opt.get('to'),'dd/mm/yyyy') ~= today)
                
                error('read_dataset:fc','you should ask for today''s data only');
            else
                data = get_full_candles('fc','day',datestr(today,'dd/mm/yyyy'),lst{:});
                if ~isempty(opt.try_get('where_formula',''))
                    data = st_data('where',data,opt.get('where_formula'));
                end
                if ~isempty(opt.try_get('last_formula',''))
                    data = st_data('apply-formula',data,opt.get('last_formula'));
                end
                if strcmp(opt.try_get('output-mode',''),'day')
                    data = {data};
                end
            end
        else
            data = from_buffer( 'get_full_candles', 'fc', lst{:});
        end
        %>
    case { 'bdaily', 'daily' }
        opt   = options( { 'source', '',  'trading-destinations', {}, ...
            'security', '', 'from', 'dd/mm/yyyy','to', 'dd/mm/yyyy'}, varargin);
        try
            opt.get('security_id');
        catch
            opt.set('security_id', opt.get('security'));
        end
        if ~isempty( mode_params)
            mode_params = str2opt( mode_params);
            lst_ = mode_params.get();
            opt.set( lst_{:});
        end
        lst = opt.get();
        kodes = get_repository( 'any-to-sec-td', lst{:});
        td    = sprintf('trading_destination_id=%d or ', kodes.trading_destination_id);
        td    = ['( ' td(1:end-4) ' )'];
        frm   = datenum(opt.get('from'), 'dd/mm/yyyy');
        to    = datenum(opt.get('to'),   'dd/mm/yyyy');
        c = userdatabase('sybase:j', 'BSIRIUS');
        vals = exec_sql(c, sprintf( [ ...
            'select  date, trading_destination_id,volume, turnover, open_prc, high_prc, low_prc, close_prc, open_turnover, close_turnover, nb_deal, ' ...
            '        open_volume, close_volume,average_spread_numer, average_spread_denom  ' ...
            '   from tick_db..trading_daily td ' ...
            '   where security_id = %d and %s and date >= ''%s'' and  date <= ''%s''' ...
            ], kodes.security_id, td, datestr(frm, 'yyyymmdd'), datestr(to, 'yyyymmdd')));
        if ~isempty( vals)
            nvals = cell2mat(vals(:,2:end));
            tdk   = sprintf('%d ', kodes.trading_destination_id);
            data  = st_data('init', 'date', datenum( vals(:,1), 'yyyy-mm-dd'), ...
                'value', [nvals(:,1:end-2), nvals(:,end-1)./nvals(:,end)] , 'colnames', { 'Trading_destination','Volume', 'Turnover', 'Open', 'High', 'Low', 'Close', 'Open (turnover)', 'Close (turnover)', 'Nb deals', 'Open volume', 'Close volume', 'Mean spread'}, ...
                'title', [ kodes.security_key ' (' tdk(1:end-1) ')']);
        else
            data = [];
        end
    case {'bvolumes-intraday', 'bintraday-volumes', 'bivolumes', 'bivwap', 'biproportion', ...
            'plot-tickdb', 'plot-history+tick', 'plot-history', 'history', ...
            'tickdb:select', 'tickdbdanats', 'tickdbdana', 'volumes-intraday', 'intraday-volumes',...
            'ivolumes', 'ivwap', 'iVWAP', 'IVWAP', 'iproportion', 'tickdb:arch', 'cv_prod', 'bcv_prod', ...
            'blc', 'lc', 'liquidity consumers', 'bliquidity consumers', 'tickdb*'}
        data = old_read_dataset( mode, varargin{:});
        warning('read_dataset:mode', 'You are using a deprecated mode (%s), please call immediately Romain or Charles or Edouard...', ...
            mode);
    case 'full_tickdb'
        error('This mode is forbidden, use bfull_tickdb. You would get an error even if you delete this line and the previous one');
    case 'bufferize'
        securities =  get_index('comp', 'FTSE 350');
        securities = cat(1, securities,get_index('comp', 'SBF250'));
        securities = cat(1, securities, get_index('comp', 'IBEX35'));
        securities = cat(1, securities, get_index('comp', 'BEL20'));
        securities = cat(1, securities,get_index('comp', 'DAX'));
        securities = cat(1, securities, get_index('comp', 'MIB30'));
        for i = 1 : length(securities)
            read_dataset( 'bfull_tickdb', 'security', securities{i}, 'from', datestr(today - 90, 'dd/mm/yyyy'), 'to', datestr(today, 'dd/mm/yyyy'), 'get_data', false);
        end
        %     case {'export', 'import', 'synchronize'}
        %         import_export(mode, varargin{:});
    case 'clean'  % be carefull
        work_on_bdata_in_dir('clean_subdirs_recursive', fullfile(fullfile( getenv('st_repository'), 'read_dataset'), get_directory(varargin{1})));
    case {'ob', 'orderbook'}
        data = bufferize(mode, get_directory(mode), varargin{:});
        %     case 'tickdb'
        %         data = bufferize('full_tickdb', 'full_tickdb', varargin{:}, 'filter', 'tickdb');
    case 'deal_and_orderbook'
        data_deal = bufferize('bfull_tickdb', 'full_tickdb', varargin{:});
        data_ob = bufferize('ob', 'orderbooks', varargin{:});
        [C,IA,IB] = intersect(st_data('col', data_deal,'deal_id'), st_data('col', data_ob,'deal_id'));
        data = data_ob;
        data.date = data_ob.date(IB);
        data.value = [data_ob.value(IB, :), data_deal.value(IA, :)];
        data.colnames = {data_ob.colnames{:}, data_deal.colnames{:}};
    otherwise
        mode = [mode '/' mode_params];
        k = strfind(mode,':');
        sub_mode = mode(k+1:end);
        mode = mode(1:k-1);
        if ~isempty(k)
            switch mode
                case 'gi'
                    % GRAPH INDICATOR SPECIAL SUBMODE
                    if length(sub_mode) > 15 ...
                            && isfield(st_version.my_env, 'switch_basic_indicator_to_v2') ...
                            && st_version.my_env.switch_basic_indicator_to_v2 ...
                            && strcmp(sub_mode(1:15), 'basic_indicator')
                        idx = strfind(sub_mode, '/');
                        if isempty(idx)
                            varargin_ = {};
                        else
                            opt = str2opt(sub_mode(idx(1)+1:end));
                            varargin_ = opt.get();
                            opt_bis = options(varargin);
                            if opt.try_get('today_allowed', false) || opt_bis.try_get('today_allowed',false)
                                varargin2 = options(varargin);
                                today_str = datestr(today, 'dd/mm/yyyy');
                                varargin2.set('day', today_str);
                                varargin2.set('to', today_str);
                                varargin2.set('from', today_str);
                                varargin2.set('today_allowed',true);
                                varargin2 = varargin2.get();
                                
                                data = get_basic_indicator_v2('basic_indicator', ...
                                    varargin2{:}, varargin_{:});
                                
                                clear get_basic_indicator_v2; % to be able to refresh this data
                                if ~isempty(opt_bis.try_get('where_formula',''))
                                    data = st_data('where',data,opt_bis.get('where_formula'));
                                end
                                if ~isempty(opt_bis.try_get('last_formula',''))
                                    data = st_data('apply-formula',data,opt_bis.get('last_formula'));
                                end
                                if strcmp(opt_bis.try_get('output-mode',''),'day')
                                    data = {data};
                                end
                                
                                return;
                            end
                        end
                        data = from_buffer('get_basic_indicator_v2', 'basic_indicator', ...
                            varargin{:}, varargin_{:});
                        data = st_data('cac', data);
                    else
                        opt = options(varargin);
                        idx = strfind(sub_mode, '/');
                        opt2 = str2opt(sub_mode(idx(1)+1:end));
                        if opt.try_get('today_allowed',false) || opt2.try_get('today_allowed',false)
                            if (isnumeric(opt.get('from')) && floor(opt.get('from')) ~= today) || ...
                                    (ischar(opt.get('from'))    && datenum(opt.get('from'),'dd/mm/yyyy') ~= today) || ...
                                    (isnumeric(opt.get('to'))   && floor(opt.get('to')) ~= today) || ...
                                    (ischar(opt.get('to'))      && datenum(opt.get('to'),'dd/mm/yyyy') ~= today)
                                
                                error('read_dataset:gi','you should ask for today''s data only');
                            else
                                today_str = datestr(today,'dd/mm/yyyy');
                                opt.set('day',today_str);
                                opt.set('from',today_str);
                                opt.set('to',today_str);
                                opt.set('dt_all',today);
                                opt2 = opt.get();
                                
                                data = get_gi(sub_mode,opt2{:});
                                
                                if ~isempty(opt.try_get('where_formula',''))
                                    data = st_data('where',data,opt.get('where_formula'));
                                end
                                if ~isempty(opt.try_get('last_formula',''))
                                    data = st_data('apply-formula',data,opt.get('last_formula'));
                                end
                                if strcmp(opt.try_get('output-mode',''),'day')
                                    data = {data};
                                end
                            end
                        else
                            data = from_buffer( 'get_gi', sub_mode, varargin{:});
                        end
                    end
                case 'st_indicator'
                    % ST INDICATOR SPECIAL SUBMODE
                    data = call_st_indicator(sub_mode, varargin{:});
                case 'specific_call'
                    % SPECIFIC SPECIAL SUBMODE
                    % hummm... permet de faire n'importe quoi....
                    f_and_args = tokenize(sub_mode, '|');
                    data = feval(f_and_args{:}, varargin{:});
                case 'slicer'
                    % SLICER SPECIAL SUBMODE
                    data = from_buffer( 'get_slicer', sub_mode, varargin{:});
                otherwise
                    error('read_dataset:exec', 'submode <%s> unknown', mode(1:k-1));
            end
            %         elseif mode(1) == 'b'
            %             submode = mode(2:end);
            %             data = bufferize( submode, get_directory(mode(2:end)), varargin{:});
        else
            error('read_dataset:exec', 'mode <%s> unknown', mode);
            %             data = exec_req(mode, varargin{:});
        end
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% call to st_indicator : enables to make indicators, based on indicators
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function st_data = call_st_indicator(key, varargin)
opt = options({'date_format:char', 'dd/mm/yyyy'}, varargin);
format_ = opt.get('date_format:char');
date_from = datenum(opt.get('from'), format_);
date_to   = datenum(opt.get('to'  ), format_);
file_sep = filesep();
base_dir = [getenv('st_repository') file_sep 'indicators' file_sep key file_sep opt.get('security')];
try
    load([base_dir file_sep datestr(date_from, 'yyyy_mm_dd') '.mat']);
    st_data = data; % data vient du load ci-dessus
    for d = date_from + 1 : date_to
        load([base_dir file_sep datestr(d, 'yyyy_mm_dd') '.mat']);
        st_data.date  = [st_data.date;data.date];
        st_data.value = [st_data.value;data.value];
    end
catch
    indicator_list = st_indicator( 'list', 'secu-ind', opt.get('security'), key);
    indicator      = st_indicator( 'get', 'batch', 'secu', indicator_list, [date_from date_to], 0);
    data           = indicator.data;
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% import/export functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function import_export(mode, varargin)
% shared_data = 'G:\DEVELOPMENT\PARIS\TEAM_QUANT_RESEARCH\data';
% common_repository = fullfile(shared_data, 'get_tick', 'ft');
% my_repository = fullfile(getenv('st_repository'), 'get_tick', 'ft');
% if ~exist(my_repository, 'dir')
%     mkdir(my_repository);
% end
% switch mode
%     case 'export'
%         copy_only_missing_files(my_repository,common_repository);
%     case 'import'
%         my_sync_log_dir = fullfile(getenv('st_repository'), 'log', 'import');
%         if ~exist(my_sync_log_dir, 'dir')
%             mkdir(my_sync_log_dir);
%         end
%         st_log('$out$', fullfile(my_sync_log_dir,'sync_log.txt'));
%         try
%             % Import des donn�es de l'homologation vers le repository commun
%             li_from_homolo = fullfile(shared_data, 'get_tick', 'last_import_from_homolo_date.mat');
%             iip_filen = fullfile(shared_data, 'get_tick', 'import_in_progess.txt');
%             d_iip_filen = dir(iip_filen);
%             if isempty(d_iip_filen) || d_iip_filen(1).datenum < today
%                 delete(iip_filen);
%                 import_fh_should_be_done = true;
%                 if exist(li_from_homolo, 'file')
%                     lifhd = load(li_from_homolo);
%                     if today > lifhd.lifhd + 5
%                         delete(li_from_homolo);
%                     else
%                         import_fh_should_be_done = false;
%                     end
%                 end
%                 if import_fh_should_be_done
%                     save( iip_filen, 'iip_filen', '-ASCII');
%                     st_log('Import from homolo\n');
%                     copy_repository_from_homolo(common_repository, varargin{1});
%                     delete(iip_filen);
%                     lifhd = today;
%                     save(li_from_homolo, 'lifhd');
%                 end
%             end
%             % Synchronisation entre le repository commun et le repository local
%             lsd_filen = fullfile(getenv('st_repository'), 'get_tick', 'last_synchronization_date.mat');
%             if exist(lsd_filen, 'file')
%                 lsd = load(lsd_filen);
%                 if today <= lsd.lsd
%                     return;
%                 end
%                 delete(lsd_filen);
%             end
%             lsd = today;
%             save(lsd_filen, 'lsd');
%             st_log('Synchronization with common repository\n');
%             copy_only_missing_files(common_repository, my_repository);
%         catch ME
%             st_log('There is an error : \n\tidentifier:<%s>\n\tmessage:<%s>', ME.identifier, ME.message);
%         end
%         st_log('$close$');
%         copy_only_missing_files(common_repository,my_repository);
%     case 'synchronize'
%         copy_only_missing_files(fullfile(shared_data, 'get_tick', 'ft'), my_repository, true);
%     otherwise
%         error('read_dataset:import_export', 'mode <%s> unknown', mode);
% end
% end


% function copy_only_missing_files(source_, destination_, has_to_sync, filesep_) % copy only missing files or bigger files
% st_log('Working on <%s>\n', source_);
% if nargin < 4
%     filesep_ = filesep;
%     if nargin < 3
%         has_to_sync = false;
%     end
% end
% dir_source = dir(source_);
% dir_destination = dir(destination_);
% [intersection, inter_source, inter_destination] = intersect({dir_source.name}, {dir_destination.name});
% to_be_copied = setdiff(1:length(dir_source), inter_source);
% to_be_copied = [to_be_copied find([dir_source(inter_source).bytes] > [dir_destination(inter_destination).bytes] & ~[dir_source(inter_source).isdir])];
% st_log('\tThere are <%d> files/directory to be copied from the current directory\n', length(to_be_copied));
% arrayfun(@(k)my_copyfile([source_ filesep_ dir_source(k).name], [destination_ filesep_ dir_source(k).name]), to_be_copied);
% if has_to_sync
%     to_be_copied = setdiff(1:length(dir_destination), inter_destination);
%     to_be_copied = [to_be_copied find([dir_destination(inter_destination).bytes] > [dir_source(inter_source).bytes])];
%     st_log('\tThere are <%d> files/directory to be copied into the current directory\n', length(to_be_copied));
%     arrayfun(@(k)my_copyfile([destination_ filesep_ dir_destination(k).name], [source_ filesep_ dir_destination(k).name]), to_be_copied);
% end
% if ~isempty(inter_source)
%     to_be_checked = {dir_source([dir_source(inter_source).isdir]).name};
%     if ~isempty(to_be_checked)
%         if to_be_checked{1}(1) == '.'
%             if to_be_checked{2}(1) == '.'
%                 to_be_checked(1:2) = [];
%             else
%                 to_be_checked(1) = [];
%             end
%         end
%     end
%     st_log('\tThere are <%d> directories that we have to check for missing files\n', length(to_be_checked));
%     cellfun(@(fn)copy_only_missing_files([source_ filesep_ fn], [destination_ filesep_ fn], has_to_sync, filesep_), to_be_checked);
% end
% end

function [status,message,messageid] = my_copyfile(src, dest, nb_iter)
if nargin < 3
    nb_iter = 0;
end
if nb_iter > 9
    error('my_copyfile:exec', 'my_copyfile has already attempted 10 times to copy the file so its time to give up');
end
try
    copyfile(src, dest);
catch
    st_log('Someone else is trying to copy this file : <%s>, we''ll retry in one minute\n', src);
    pause(60);
    st_log('Lets retry');
    my_copyfile(src, dest, nb_iter + 1);
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function to get the directory to write/read in
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function directory_ = get_directory(mode)
switch mode
    %         case {'lc', 'liquidity consumers'}
    %             directory_ = 'lc';
    case 'tickdb'
        directory_ = fullfile('temp', 'tickdb');
    case 'full_tickdb'
        directory_ = 'full_tickdb';
    case 'cv_prod'
        directory_ = 'cv_prod';
    case { 'ob', 'orderbook'}
        directory_ = 'orderbooks';
    case {'daily'}
        directory_ = fullfile('temp', 'daily');
    otherwise
        error('read_dataset:exec', 'mode <%s> unknown', mode);
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fonction qui fait les requ�tes                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function data = exec_req(mode, varargin)
switch mode
    case 'tickdb'
        data = read_dataset('tickdb', varargin{:});
        if iscell(data) && ~isempty(data)
            data = data{:}; % 1 should be ok, but i'd rather throw error if something unexpected happens
        elseif isempty(data)
            data = [];
        end
    case 'full_tickdb'
        t0 = clock;
        %<* Read into the tickdb
        opt = options({'security_id', 110, 'from', '01/04/2008', 'to', '01/04/2008',...
            'connection', {'batch_tick','batick'},'date_format:char', 'dd/mm/yyyy', 'td_info', [] ...
            }, varargin);
        %< Lecture des options
        connection_lp = opt.get('connection');
        format_date = opt.get('date_format:char');
        sec_id = opt.get('security_id');
        frm = opt.get('from');
        if ischar( frm)
            frm = datenum( frm, format_date);
        end
        too = opt.get('to');
        if ischar( too)
            too = datenum( too, format_date);
        end
        if (frm ~= too)
            error('read_dataset:exec_req:full_tickdb', 'You are not allowed to ask for more than one day'); % because these data could be in several databases
        end
        %>
        %< Database connection
        c = userdatabase('sybase:j', 'BSIRIUS');
        %>
        %< Requ�te
        slct_trading_destination = 'trading_destination_id,';
        if too < 733133 % Avant le 01/04/2007 les tables deal_archive ne contiennent pas le champs trading_destiantion_id
            slct_trading_destination = '';
        end
        select_str = [  'select date,convert(char(8),time,108),price,size,overbid,bid,ask,'...
            slct_trading_destination 'bid_size,ask_size,overask,'...
            'cross,auction,opening_auction,closing_auction,'...
            'trading_at_last,trading_after_hours,interpolated,deal_id,isnull(buyer_id,''null''),isnull(seller_id,''null'')' ...
            ' from %s ( index security_ind )'];
        % Si l'on modifie l'ordre des colonnes, il faut le
        % r�percuter dans filter mode tickdb (et ptet ailleurs...)
        colnames_ = {    'price','volume','sell','bid','ask', 'trading_destination_id',...
            'bid_size','ask_size','buy','cross',...
            'auction','opening_auction','closing_auction','trading_at_last','trading_after_hours',...
            'interpolated', 'deal_id', 'tick_size'};
        quarter_str = datestr( frm,18);
        year_frm = year(frm);
        if strcmp(quarter_str,datestr( today,18)) && year_frm == year(today)
            main_req = {sprintf( select_str, 'deal')};% useless to check deal_day, as it is empty on BSIRIUS TODO if using SIRIUS change this.;sprintf( select_str, 'deal_day')};
        else
            quarter_num = str2double(quarter_str(2));
            previous_quarter = ['Q' num2str(quarter_num - 1)];
            previous_quarter_year = datestr( frm, 'yyyy');
            next_quarter = ['Q' num2str(quarter_num + 1)];
            next_quarter_year = previous_quarter_year;
            if quarter_num == 4
                next_quarter = 'Q1';
                next_quarter_year = num2str(year_frm + 1);
            elseif quarter_num == 1
                previous_quarter = 'Q4';
                previous_quarter_year = num2str(year_frm - 1);
            end
            main_req = {sprintf( select_str, sprintf('tick_db_%s_%s..deal_archive', ...
                lower(quarter_str), datestr( frm, 'yyyy')));...
                sprintf( select_str, sprintf('tick_db_%s_%s..deal_archive', ...
                lower(next_quarter), next_quarter_year));...
                sprintf( select_str, sprintf('tick_db_%s_%s..deal_archive', ...
                lower(previous_quarter), previous_quarter_year));...
                sprintf( select_str, 'tick_db..deal')};
            % Un petit test pour ne pas faire planter la requ�te
            % dans le cas o� la base archive suivante n'existe pas
            % encore
            try
                vals = exec_sql(c, sprintf('tick_db_%s_%s..sp_tablelist', lower(next_quarter), next_quarter_year));
            catch
                vals = {};
            end
            if isempty(strmatch('deal_archive', vals))
                main_req(2) = [];
            end
            if strcmp(previous_quarter, 'Q1') && strcmp(previous_quarter_year, '2007') % alors il faudrait faire une requete ne n�cessitant pas trading_destination... mais l� j'ai pas envie de faire l'effort...
                main_req(3) = [];
            end
        end
        end_req = sprintf(['  where date between ''%s'' and ''%s'' ' ...
            '   and security_id = %d   ' ...
            '  order by date, time, deal_id' ], ...
            datestr( frm, 'yyyymmdd'),datestr( too, 'yyyymmdd'), ...
            sec_id);
        for i = 1 : length(main_req)
            sql_req = [main_req{i} end_req];
            t1 = clock;
            vals = exec_sql(c, sql_req);
            if ~isempty(vals)
                break;
            end
        end
        st_log('end of fetch (%5.2f sec)\n', etime(clock, t1));
        if isempty(vals)
            data = [];
            st_log('end of read_dataset:exec_req:exec (%5.2f sec)\n', etime(clock, t0));
            return;
        end
        %>
        %> Si l'on a pas r�cup�rer le champs trading_destination : YAKA
        % l'inventer, pour cela :
        % on exclue CHIX (execution_market_id = 50)
        % S'il reste plus d'une trading destination pource titre alors
        % on est dans la merde donc on raise une exception
        % S'il reste q'une, alors c'est celle l�, YAPLUKA l'ajouter au
        % r�sultat de la requ�te
        if too < 733133
            td_info = opt.get('td_info');
            idx_td_NOT_CHIX = round(cell2mat(td_info(:, 3))) ~= 50;
            if sum(idx_td_NOT_CHIX) == 1
                td = td_info{idx_td_NOT_CHIX, 1};
            else
                error('read_dataset:exec_req:exec', 'Unable to decide a tradinig_destination for security_id <%d>', sec_id);
            end
            vals(:, end + 1) = vals(:, end); % on rajoute une colonne
            vals(:, 9 : end) = vals(:, 8 : end - 1);
            vals(:, 8) = repmat({td}, size(vals, 1), 1);
        end
        %< Nettoyage des donn�es, les volumes doivent �tre des entiers,
        % les prix doivent �tre sur une grille discrete de ticks
        % Ce tick peut �tre diff�rent suivant les trading_destination
        prices = [cell2mat(vals(:, 3)), cell2mat(vals(:, 6:7))];
        td_col = cell2mat(vals(:, 8));
        trading_destinations = unique(td_col);
        guessed_tick = zeros(length(trading_destinations), 1);
        idx_4_td = cell(length(trading_destinations), 1);
        my_eps = 0.0000000001;
        tick_size = zeros(length(td_col), 1);
        for i = 1 : length(trading_destinations)
            idx_4_td{i} = (td_col == trading_destinations(i));
            tmp = prices(idx_4_td{i}, :);
            tmp = abs(diff(sort(tmp(:))));
            tmp(tmp < my_eps) = [];
            if isempty(tmp)
                guessed_tick(i) = NaN;
            else
                guessed_tick(i) = round(min(tmp)/my_eps)*my_eps;
            end
            tick_size(idx_4_td{i}) = guessed_tick(i);
        end
        broker_ids = vals(:, end-1 : end);
        vals(:, end) = [];
        vals(:, end) = num2cell(tick_size);
        %< Creation du st_data
        data = st_data( 'from-matrix', vals, ...
            'title', opt.get('security'), ...
            'date', '{1}yyyy-mm-dd', ...
            'time', '{2}HH:MM:SS', ...
            'colnames',  colnames_...
            );
        if any(cellfun(@(c)~strcmp(c, 'null'), [broker_ids(:, 1) broker_ids(:, 2)], 'uni', true))
            data.info = struct( 'full_name', cell2mat(get_security_name(sec_id)), ...
                'security_id', sec_id, ...
                'td_info', {opt.get('td_info')}, ...
                'sql_request', sql_req, ...
                'broker_ids',struct('buyer_id', {broker_ids(:,1)},'seller_id', {broker_ids(:,2)}));
            % TODO les 'broker_ids' seraient mieux dans un
            % codebook, car ainsi on pourrait agr�ger les colonnes.
            % N�anmoins, cela pose au moins deux probl�mes :
            %   1) les codebook devraient �tre capable de
            %           s'agr�ger, or cela ne se ferait pas sans un coup CPU probablement cons�quent...
            %   2) les codebook devraient pouvoir s'appuyer sur
            %       deux colonnes et non pas une seule, car il y a
            %       �videmment de grosses redondances entre acheteurs
            %       et vendeurs
        else
            data.info = struct( 'full_name', get_security_name(sec_id), ...
                'security_id', sec_id, ...
                'td_info', {opt.get('td_info')}, ...
                'sql_request', sql_req);
        end
        %>
        st_log('end of read_dataset:exec_req:exec (%5.2f sec)\n', etime(clock, t0));
    case { 'ob', 'orderbook'}
        % TODO : something fully fonctionnal, which knows which stored_prodeure to use.
        % Mais il est plus sage d'attendre les serveurs IQ qui permettront d'acc�der aux donn�es de n'importe quelle p�riode sans se soucier de la base dans laquelle elles sont stock�es
        % Il faudra aussi �crire un mode permettant de synchroniser ces
        % donn�es avec les donn�es de type deal gr�ce au deal_id
        opt = options({'security', 'FTE.PA', 'from', '11/01/2008', 'to', '11/01/2008', ...
            'trading_destination_id', 4, 'date_format:char', 'dd/mm/yyyy', ...
            'conn_chain', {'batch_tick', 'batick'}}, varargin);
        try
            td_id  = opt.get('td_info');
            sec_id = opt.get('security_id');
        catch er
            error('read_dataset:ob', 'You should not reach this point, report to clehalle or rburgot');
        end
        date_format = opt.get('date_format:char');
        date_from = datenum(opt.get('from'), date_format);
        date_to   = datenum(opt.get('to'  ), date_format);
        if (date_from ~= date_to)
            error('read_dataset:exec_req:orderbook', 'You are not allowed to ask for mmore than one day');
        elseif date_from < datenum(2007, 2, 1, 0, 0, 0) % TODO this should be dynamic... but the stored procedure is not dynamic either, so...
            error('read_dataset:exec_req:orderbook', 'Ordebook not yet implemented for this period');
        end
        c = userdatabase('sybase:j', 'BSIRIUS');
        if date_from < 733499
            sp_name = 'tmp_get_orderbook';
        else
            sp_name = 'tmp_get_orderbook_2';
            %vals = exec_sql(c, sprintf('exec temp_works..tmp_get_orderbook_2 %d, %d, ''%s''', ids(2), ids(1), datestr(date_to, 'yyyymmdd')));
        end
        st_log('retrieving <%s> orderbooks...\n', td_id{1,4});
        vals = exec_sql(c, sprintf('exec temp_works..%s %d, %d, ''%s''', sp_name, sec_id, td_id{1,1}, datestr(date_to, 'yyyymmdd')));
        vals_td = repmat(td_id{1,1}, size(vals,1),1);
        for td=2:size(td_id,1)
            st_log('retrieving <%s> orderbooks...\n', td_id{td,4});
            tals = exec_sql(c, sprintf('exec temp_works..%s %d, %d, ''%s''', sp_name, sec_id, td_id{td,1}, datestr(date_to, 'yyyymmdd')));
            vals = cat(1, vals, tals);
            vals_td = cat(1, vals_td, repmat(td_id{td,1}, size(tals,1),1));
        end
        if isempty(vals)
            data = [];
            return;
        end
        try
            dts  = date_to + mod(datenum( vals(:,1), 'HH:MM:SS'),1);
        catch
            lasterr('');
            dts  = date_to + mod(datenum( vals(:,1), 'yyyy-mm-dd HH:MM:SS.FFF'),1);
        end
        vals = [cellfun(@double, vals(:,2:end)), vals_td];
        idx_to_delete = any(isnan(vals), 2);
        prop_delete = mean(idx_to_delete);
        if prop_delete > 0
            warning('read_dataset:exec_req:orderbook:exec', 'Deleting %.2f%% of the trades, as they have no orderbook data', 100 * prop_delete);
            vals(idx_to_delete, :) = [];
            if prop_delete > 0.5
                error('read_dataset:exec_req:orderbook:exec', 'Too much missing value');
            end
        end
        data  = st_data('init', 'title', [ opt.get('security') ' Orderbook'], 'date', dts, 'info', struct('td_info', td_id), ...
            'value', vals, 'colnames', { 'deal_id', ...
            ...
            'Bid number 5', 'Bid size 5', 'Bid price 5', ...
            'Bid number 4', 'Bid size 4', 'Bid price 4', ...
            'Bid number 3', 'Bid size 3', 'Bid price 3', ...
            'Bid number 2', 'Bid size 2', 'Bid price 2', ...
            'Bid number 1', 'Bid size 1', 'Bid price 1', ...
            ...
            'Ask number 1', 'Ask size 1', 'Ask price 1', ...
            'Ask number 2', 'Ask size 2', 'Ask price 2', ...
            'Ask number 3', 'Ask size 3', 'Ask price 3', ...
            'Ask number 4', 'Ask size 4', 'Ask price 4', ...
            'Ask number 5', 'Ask size 5', 'Ask price 5', ...
            'Trading destination' });
        %             Ancienne version utilisant la table orderbook et non pas
        %             orderbook update
        %             NE PAS EFFACER
        
        % Non seulement ce mode ne peut qu'�tre bufferize, mais de plus, il n'accepete pas de faire des requ�tes pour plus d'une journ�e
        %<* Read orderbooks from tickdb
        %             opt = options( { 'security', 'FTE.PA', 'from', '26/06/2007', 'to', '26/06/2007', ...
        %                 'base', 'tick_db', 'login', 'batch_tick', 'password', 'batick'}, varargin);
        %
        %             sec_id    = get_sec_id_from_ric(opt.get('security'));
        %             date_from = datenum(opt.get('from'), 'dd/mm/yyyy');
        %             date_to   = datenum(opt.get('to'  ), 'dd/mm/yyyy');
        %             if (date_from ~= date_to)
        %                 error('read_dataset:exec_req:orderbook', 'You are not allowed to ask for mmore than one day');
        %             end
        %             con = userdatabase('sybase', opt.get('base'), opt.get('login'), opt.get('password'));
        %             archive_name = sprintf('tick_db_%s_%s', lower(datestr( date_from,18)), datestr( date_from, 'yyyy'));
        %             full_tables_names = {sprintf('%s..deal_archive d,%s..orderbook_archive o', archive_name, archive_name)};
        %                 %'tick_db..deal d,tick_db..orderbook o'};
        %             for ftn = 1 : length(full_tables_names)
        %                 requet = sprintf(strcat('select d.time ,',...
        %                     ' d.price, d.size , d.bid , d.ask , d.bid_size , d.ask_size , d.overbid ,',...
        %                     ' o.bid_5 , o.ask_5 , o.bid_size_5 , o.ask_size_5 , ',...
        %                     ' o.bid_4 , o.ask_4 , o.bid_size_4 , o.ask_size_4 , ',...
        %                     ' o.bid_3 , o.ask_3 , o.bid_size_3 , o.ask_size_3 , ',...
        %                     ' o.bid_2 , o.ask_2 , o.bid_size_2 , o.ask_size_2 ',...
        %                     ' from %s where d.security_id = ', num2str(sec_id) ,...
        %                     ' and o.orderbook_id = d.orderbook_id and d.date between ''' , datestr(date_from, 'yyyymmdd') , ''' and ''' , datestr(date_from, 'yyyymmdd'), ''' ', ...
        %                     ' and d.cross = 0 ', ...
        %                     ' and d.auction = 0 ', ...
        %                     ' and d.trading_destination_id <= 50 ', ...
        %                     ' and trading_after_hours = 0 ',...
        %                     ' and d.trading_at_last = 0 ', ...
        %                     ' order by d.security_id, d.date, d.time'), full_tables_names{ftn});
        %
        %                 vals = exec_sql(con, requet);
        %
        %                 if ~(isempty( vals) || iscellstr(vals))
        %                     %< Jour existant
        %                     try
        %                         dts  = date_from + mod(datenum( vals(:,1), 'yyyy-mm-dd HH:MM:SS.FFF'),1);
        %                     catch
        %                         lasterr('');
        %                         dts  = date_from + mod(datenum( vals(:,1), 'HH:MM:SS'),1);
        %                     end
        %                     vals = cellfun(@double, vals(:,2:end));
        %                     idx_to_delete = any(isnan(vals), 2);
        %                     prop_delete = mean(idx_to_delete);
        %                     if prop_delete > 0
        %                         warning('Deleting %.2f%% of the trades, as they have no orderbook data', 100 * prop_delete);
        %                         vals(idx_to_delete, :) = [];
        %                     end
        %                     data  = st_data('init', 'title', [ opt.get('security') ' Orderbook'], 'date', dts, ...
        %                         'value', vals, 'colnames', { 'Price', 'Volume', 'Bid price', 'Ask price', 'Bid size', 'Ask size', ...
        %                         'Sell', 'Bid price 5', 'Ask price 5', 'Bid size 5', 'Ask size 5', ...
        %                         'Bid price 4', 'Ask price 4', 'Bid size 4', 'Ask size 4', ...
        %                         'Bid price 3', 'Ask price 3', 'Bid size 3', 'Ask size 3', ...
        %                         'Bid price 2', 'Ask price 2', 'Bid size 2', 'Ask size 2' });
        %                     break; % de la boucle qui essai la base archive et la base courante
        %                 elseif ftn == length(full_tables_names)
        %                     data = [];
        %                 end
        %             end
    case 'cv_prod'
        opt = options({'security', 'FTE.PA', 'from', '19/04/2007', 'to', '19/04/2007', 'connection', {'batch_tick','batick'}, ...
            }, varargin);
        security = opt.get('security');
        sec_id = get_sec_id_from_ric(security);
        z = cv_select_prod('info', security);
        c = userdatabase('sybase:j', 'BILBO');
        rslt = exec_sql(c, ['select * from market..trading_curve_definition '...
            'where index_id = ' num2str(sec_id)]);
        tmp = exec_sql(c, ['select * from trading_curve_definition '...
            'where index_type = ''M''']);
        c2 = userdatabase('sybase:j', 'BSIRIUS');
        for i = 1 : size(tmp, 1)
            tmp_2 = cell2mat(exec_sql(c2, ['select security_id from repository..indice_component '...
                'where indice_id = ' num2str(tmp{i, 2})]));
            if ismember(sec_id, tmp_2);
                rslt = cat(1, rslt, tmp(i, :));
            end
        end
        st_log(sprintf('%d curve(s) found for %s : \n', size(z, 1), security));
        for i = 1 : size(z, 1)
            st_log('\t%s\n', z{i, 4});
        end
        st_log('Choosing first curve before to_date : %d\t%d\t%s\t%s\t%s\t%s\t%d\t \n', z{1, :});
        if strcmp(security(end-2:end), '.MI')
            cg = 666;
        else
            cg = 1;
        end
        data = cv_select_prod_get_curve(z{1},cg);
    case {'lc', 'liquidity consumers'} % liquidity consumers and price impact
        %             nb_limites = 5; % Magic Number
        %             tick_size  = 0.01; % Magic Number : very important, used to round the prices given by Sybase !!!
        %             epsilon = tick_size / 100; % used to round prices given by Sybase
        %             data      = read_dataset('ob', varargin{:});
        %             if isempty(data)
        %                 return;
        %             end
        %             sells     = st_data('col', data, 'sell');
        %             price     = round(st_data('col', data, 'price') / epsilon) * epsilon;
        %             volume    = st_data('col', data, 'volume');
        %             a_s_coln  = ['Ask size;' sprintf('Ask size %d;', 2:nb_limites)]; a_s_coln(end) = [];
        %             b_s_coln  = ['Bid size;' sprintf('Bid size %d;', 2:nb_limites)]; b_s_coln(end) = [];
        %             sizes_cum = {cumsum(st_data('col', data, a_s_coln), 2);cumsum(st_data('col', data, b_s_coln), 2)};
        %             a_p_coln  = ['Ask price;' sprintf('Ask price %d;', 2:nb_limites)]; a_p_coln(end) = [];
        %             b_p_coln  = ['Bid price;' sprintf('Bid price %d;', 2:nb_limites)]; b_p_coln(end) = [];
        %             prices_ob = {round(st_data('col', data, a_p_coln) / epsilon) * epsilon;round(st_data('col', data, b_p_coln) / epsilon) * epsilon};
        %             secs    = data.date;
        %             diff_p  = diff(price);
        %
        %             seq_begins =  logical(diff(sells)) ... ou bien on change de c�t� du carnet
        %                 | (diff(secs) > 0)... Ou bien l'horodatage (Euronext j'esp�re) permet de conclure que ce n'est pas le m�me ordre agressif
        %                 | any(diff([sizes_cum{1}, sizes_cum{2}, prices_ob{1}, prices_ob{2}]), 2)... % Ou bien le carnet d'ordre a �t� updat� entre les deux transactions, ce qui n'est pas le cas s'il s'agit du m�me ordre march�
        %                 ... Les deux lignes suivantes ont pour seule fonction de s�parer les ordres stop de l'ordre march� qui les a d�clench�s par son �x�cution. Je ne me souviens plus si cela est n�cessiare
        %                 | ((diff_p > 0) & sells(1:end-1)) ... ou bien on suivait une s�quence d'ordres de vente et le prix suivant est sup�rieur
        %                 | ((diff_p < 0) & ~sells(1:end-1)); % ou bien on suivait une s�quence d'ordres d'achat et le prix suivant est inf�rieur
        %
        %             seq_begins = [true; seq_begins];
        %             num_seq    = cumsum(seq_begins);
        %
        %             acc_volume          = accumarray(num_seq, volume);
        %             acc_price           = accumarray(num_seq, volume.*price) ./ acc_volume;
        %             acc_sell            = sells(seq_begins);
        %             acc_price_impact    = accumarray(num_seq, price, [], @(x)max(x)-min(x)) .* (2 * ~acc_sell - 1);
        %
        %             acc_sizes_cum = {sizes_cum{1}(seq_begins, :), sizes_cum{2}(seq_begins, :)};
        %             acc_sizes     = {[acc_sizes_cum{1}(:, 1) diff(acc_sizes_cum{1}, 1, 2)], [acc_sizes_cum{2}(:, 1) diff(acc_sizes_cum{2}, 1, 2)]};
        %             acc_prices_ob = {prices_ob{1}(seq_begins, :), prices_ob{2}(seq_begins, :)};
        %             theo_price_impact = zeros(length(acc_sell), 1);
        %             theo_price        = zeros(length(acc_sell), 1);
        %             bugs_gl           = zeros(length(acc_sell), 1);
        %             fat_finger        = zeros(length(acc_sell), 1);
        %             very_fat_finger   = zeros(length(acc_sell), 1);
        %             hidden_volume     = zeros(length(acc_sell), 1);
        %             for i = 1 : length(acc_sell)
        %                 s = acc_sell(i);
        %                 % Calcul du price impact 'theorique', c'est � dire sans
        %                 % connaitre la liquidit� cach�e, et en supposant qu'il n'y a
        %                 % pas de bugs GL
        %                 idx_worst_price = find(acc_sizes_cum{ 1 + s}(i, :) >= acc_volume(i), 1, 'first');
        %                 if isempty(idx_worst_price)
        %                     theo_price_impact(i) = acc_prices_ob{ 1 + s}(i, nb_limites) - acc_prices_ob{ 1 + s}(i, 1) + (2 * ~s - 1) * tick_size;
        %                     fat_finger(i) = 1;
        %                     theo_price(i) = NaN;
        %                 else
        %                     theo_price_impact(i) = acc_prices_ob{ 1 + s}(i, idx_worst_price) - acc_prices_ob{ 1 + s}(i, 1);
        %                     if idx_worst_price > 1
        %                         theo_price(i) = (   sum(acc_sizes{ 1 + s}(i, 1:(idx_worst_price - 1)) .* acc_prices_ob{ 1 + s}(i, 1:(idx_worst_price - 1))) ...
        %                             + (acc_volume(i) - sum(acc_sizes{ 1 + s}(i, 1:(idx_worst_price - 1)))) * acc_prices_ob{ 1 + s}(i, idx_worst_price)...
        %                             ) / acc_volume(i);
        %                     else
        %                         theo_price(i) = acc_prices_ob{ 1 + s}(i, 1);
        %                     end
        %                 end
        %                 % Calcul de la quantit� th�oriquement �x�cut�e sur chaque
        %                 % limite => d�couverte de la quantit� cach�e r�v�l�e par l'ordre
        %                 % et mise en �vidence des bugs pour dire quel est la
        %                 % proportion d'ordres march� affect�s par les bugs GL
        %                 idx_deals   = find(num_seq == i);
        %                 seq_prices  = price(idx_deals(1));
        %                 seq_volumes = volume(idx_deals(1));
        %                 for j = 2 : length(idx_deals)
        %                     p = price(idx_deals(j));
        %                     if p ~= seq_prices(end)
        %                         seq_prices(end + 1) = p;
        %                         seq_volumes(end + 1) = volume(idx_deals(j));
        %                     else
        %                         seq_volumes(end) = seq_volumes(end) + volume(idx_deals(j));
        %                     end
        %                 end
        %                 if length(seq_prices) > nb_limites % ne sert que si quelqu'un a arrach� les 5 meilleures limites du carnet, pour �viter de bugger, mais les calculs seront faux
        %                     very_fat_finger(i) = true;
        %                     seq_prices(nb_limites + 1 : end) = [];
        %                     seq_volumes(nb_limites + 1 : end) = [];
        %                 end
        %                 tmp = acc_sizes_cum{ 1 + s}(i, 1:length(seq_prices));
        %                 tmp = [tmp(1) diff(tmp)];
        %                 if any( acc_prices_ob{ 1 + s}(i, 1:length(seq_prices)) ~= seq_prices ) ...
        %                         || any( seq_volumes(1:end-1) <  tmp(1:end-1))
        %                     bugs_gl(i) = true;
        %                 end
        %                 hidden_volume(i)  = sum(seq_volumes(1:end-1) - tmp(1:end-1)) + max(seq_volumes(end) - tmp(end), 0);
        %             end
        %
        %             tmp = mean(bugs_gl);
        %             if tmp > 0.0001
        %                 if tmp > 1.5 % records actuels : 9.7% pour TOTF.PA le 17/08/2007, 7.9% pour EDF le 24/09/2007, 6.8% pour EDF le 29/06/2007, 6.8% pour EDF le 17/08/2007
        %                     error('read_dataset:exec', 'Les bugs GL concernent %.2g%% des ordres march�s', 100* tmp);
        %                 elseif tmp > 0.02
        %                     warning('read_dataset:exec','Les bugs GL concernent %.2g%% des ordres march�s', 100* tmp);
        %                 else
        %                     st_log('Les bugs GL concernent %.2g%% des ordres march�s\n', 100* tmp);
        %                 end
        %             end
        %
        %             % DEBUG code, do not delete
        %             % cmp  = [(data.date - data.date(1))/datenum(0,0,0,0,0,1) sells price volume prices_ob{1} prices_ob{2} sizes_cum{1} sizes_cum{2}]
        %             % cmp2 = [(data.date(seq_begins) - data.date(1))/datenum(0,0,0,0,0,1) acc_sell acc_price acc_volume acc_prices_ob{1} acc_prices_ob{2} acc_sizes_cum{1} acc_sizes_cum{2}]
        %             % plot((theo_price_impact - acc_price_impact) .* sign(theo_price_impact))
        %             % idx = find((theo_price_impact - acc_price_impact) .* sign(theo_price_impact) < 0)
        %             % idx_in_data = find(num_seq == idx, 1, 'first')
        %             if any( (    (theo_price_impact - acc_price_impact) .* (2 * ~acc_sell - 1) < 0 ...
        %                     | (theo_price - acc_price) .* (2 * acc_sell - 1) > 10000*eps  ...
        %                     )...
        %                     & ~(bugs_gl | very_fat_finger))
        %                 warning('read_dataset:exec', 'Incoh�rence non pr�vue!\n'); % seul int�r�t alerter tout de m�me l'utilisateur qui fait un try catch sur la raison pour laquelle il n'a pas de donn�es
        %                 error('read_dataset:exec', 'Incoh�rence non pr�vue!');
        %                 % idx = find((theo_price_impact - acc_price_impact) .* sign(theo_price_impact) < 0 & ~bugs_gl)
        %                 % idx_in_data = find(num_seq == idx, 1, 'first')
        %             end
        %
        %             % Nous allons maintenant corriger les dates pour que les ordres
        %             % agressifs ne puissent pas �tre au m�me instant pr�cis, m�me s'ils
        %             % seront toujours dans la m�me seconde. Les ordres agressifs dans
        %             % la m�me seconde seront uniform�ment r�partis dans cette seconde.
        %             dates =  data.date(seq_begins);
        %             dates_to_be_corrected = find(diff(dates) == 0);
        %             sec_dt = datenum(0,0,0,0,0,1);
        %             i = 1;
        %             while i <= length(dates_to_be_corrected)
        %                 nb_in_same_sec = 2;
        %                 while i < length(dates_to_be_corrected) && dates_to_be_corrected(i + 1) - dates_to_be_corrected(i) == 1
        %                     nb_in_same_sec = nb_in_same_sec + 1;
        %                     i = i + 1;
        %                 end
        %                 for j = 1 : nb_in_same_sec - 1
        %                     dates(dates_to_be_corrected(i) - j + 2) = dates(dates_to_be_corrected(i) - j + 2) + sec_dt * ( 1 - j / nb_in_same_sec);
        %                 end
        %                 i = i + 1;
        %             end
        %
        %             data = st_data('init', 'value', [       acc_price acc_volume acc_sell acc_price_impact  theo_price_impact    theo_price     hidden_volume   acc_sizes{1}            acc_sizes{2}              acc_prices_ob{1}         acc_prices_ob{2}            fat_finger   very_fat_finger                           bugs_gl], ...
        %                 'colnames', cellflat({'price',  'volume',  'sell', 'pi',            'theo price impact', 'theo price', 'hidden volume', tokenize(a_s_coln, ';'), tokenize(b_s_coln, ';'), tokenize(a_p_coln, ';'), tokenize(b_p_coln, ';')   ,'fat finger', sprintf('%d limits deleted', nb_limites), 'bugs_GL'}),...
        %                 'date', dates, ...
        %                 'title', data.title);
    case 'daily'
        %<* Read daily data
        opt = options({'security', 'EDF.PA', 'from','02/04/2007','to','02/04/2007', 'format_date', 'dd/mm/yyyy', 'db-connection', {'batch_tick','batick' } }, ...
            varargin(1:end));
        sec = opt.get('security');
        conn_chain = opt.get('db-connection');
        format_date = opt.get('date_format:char');
        frm = datenum(opt.get('from'), format_date);
        too   = datenum(opt.get('to'  ), format_date);
        if (frm ~= too)
            error('read_dataset:exec_req:daily', 'You are not allowed to ask for more than one day');
        end
        c = userdatabase('sybase:j', 'BSIRIUS');
        ids = get_td_and_sec_id_from_ric(sec);
        vals = exec_sql(c, sprintf( [ ...
            'select  date, trading_destination_id,volume, turnover, open_prc, high_prc, low_prc, close_prc, open_turnover, close_turnover, nb_deal, ' ...
            '        open_volume, close_volume,average_spread_numer, average_spread_denom  ' ...
            '   from tick_db..trading_daily td ' ...
            '   where security_id = %d and trading_destination_id = %d and date = ''%s''' ...
            ], ids(2), ids(1), datestr(frm, 'yyyymmdd')));
        if ~isempty( vals)
            nvals = cell2mat(vals(:,2:end));
            data = st_data('init', 'date', datenum( vals(:,1), 'yyyy-mm-dd'), ...
                'value', [nvals(:,1:end-2), nvals(:,end-1)./nvals(:,end)] , 'colnames', { 'Trading_destination','Volume', 'Turnover', 'Open', 'High', 'Low', 'Close', 'Open (turnover)', 'Close (turnover)', 'Nb deals', 'Open volume', 'Close volume', 'Mean spread'}, ...
                'title', sec);
        else
            data = [];
        end
    otherwise
        error('read_dataset:exec_req:exec', 'mode <%s> unknown', mode);
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% R�cup�ration d'informations de type r�f�rentiel (ids, etc)              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function rslt = get_read_dataset_ids_from_reference(ref)
% ids = get_read_dataset_ids_from_reference(ref) - permet de r�cup�rer les
%                       informations utiles � read_dataset
%
% % ids{1} : security_id,
% % ids{2}(:, 1) : trading_destination_id (�ventuellement au pluriel)
% % ids{2}(:, 2) : place_id de chaque trading_destination -> permet de r�cup�rer d�calage horaire heure d'�t�/hiver
% % ids{2}(:, 3) : execution_market_id (numeric)
% % ids{2}(:, 4) : execution_market (char)
% % ids{2}(:, 5) : execution_market timezone (char  = GMT/MET/etc)
% % ids{3} : reference (toujours la m�me j'esp�re, afin de bufferizer toujours dans le m�me dossier)
%
% exemples :
% ids1 = get_read_dataset_ids_from_reference('FTE.PA')
% ids2 = get_read_dataset_ids_from_reference('FTE')
% ids3 = get_read_dataset_ids_from_reference(110)
c = userdatabase('sybase:j', 'BSIRIUS');
if ischar(ref)
    tmp = exec_sql(c, sprintf('select security_id from repository..security_source where reference = ''%s''  order by security_id' , ref));
    if isempty(tmp)
        error('read_dataset:get_read_dataset_ids_from_reference:exec', 'The security (%s) you are asking for does not exist in cheuvreux databases', ref);
    elseif length(unique(cell2mat(tmp(:, 1)))) ~= 1
        warning('read_dataset:get_read_dataset_ids_from_reference:exec', 'Be carefull, the reference you gave correponds to <%d> different securities', length(unique(cell2mat(tmp(:, 1)))));
    end
    rslt = tmp(1);
    tmp = grdifr(c, rslt{1});
    rslt(2) = tmp(2);
    rslt(3) = tmp(3);
elseif isnumeric(ref)
    rslt = {ref};
    tmp = grdifr(c, rslt{1});
    rslt(2) = tmp(2);
    rslt(3) = tmp(3);
else
    error('get_read_dataset_ids_from_reference:exec', 'This type of reference <%s> is not handeld', class(ref));
end
end

function rslt = grdifr(c, ref)
tmp = cell2mat(exec_sql(c, sprintf('select distinct trading_destination_id from repository..security_source where security_id = %d  order by trading_destination_id', ref)));
tmp(~isfinite(tmp)) = [];
if isempty(tmp)
    error('read_dataset:get_read_dataset_ids_from_reference:exec', 'No trading_destination_id for this reference <%s>', ref);
end
rslt{2} = cell(length(tmp), 1);
rslt{2}(:, 1) = num2cell(tmp);
tmp2 = cell(length(rslt{2}), 4);
for i = 1 : length(rslt{2})
    tmp = exec_sql(c, sprintf('select em.place_id, em.execution_market_id, em.short_name, ptz.timezone from repository..execution_market em, repository..trading_destination td, repository..place_timezone ptz where td.trading_destination_id = %d and td.execution_market_id = em.execution_market_id and ptz.place_id = em.place_id order by td.trading_destination_id' , rslt{2}{i, 1}));
    [n k] = size(tmp);
    if all([n k] == [1 4])
        tmp2(i, :) = tmp;
    else
        error('read_dataset:get_read_dataset_ids_from_reference:exec', 'Problem with trading_destination_id <%d>', rslt{2}{i});
    end
end
rslt{2}(:, 2:5) = tmp2;
tmp = exec_sql(c, sprintf('select reference from repository..security_source where security_id = %d and source_id = 2', ref));
if isempty(tmp)
    tmp = exec_sql(c, sprintf('select reference from repository..security_source where security_id = %d and defaut = 1 order by source_id', ref));
end
if isempty(tmp)
    tmp = exec_sql(c, sprintf('select reference from repository..security_source where security_id = %d order by source_id', ref));
end
rslt{end + 1} = tmp{1};
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fonctions de bufferization                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function data = bufferize(mode, directory_, varargin) % mode de bufferization le plus courant
env_st_rep = getenv('st_repository');
if isempty( env_st_rep)
    error('read_dataset:bufferize', 'environment variable <st_repository> must be defined');
end
opt = options({ 'security', 'FTE.PA', ...
    'from', '19/04/2007', 'to', '19/04/2007',...
    'date_format:char', 'dd/mm/yyyy', ...
    'get_data', true, ...
    'get_brokers_ids', true...
    'filter', [], ... DO NOT USE!!!!!!
    'force_req_if_empty', false, ...
    'I dont care about missing data', false, ...
    'keep_d_by_d', false, ...
    'last_formula', '',...
    }, varargin);
filter__ = opt.get('filter');
init_sec = opt.get('security');
opt.set('init_sec', init_sec);
frfie = opt.get('force_req_if_empty');
% on stocke last_formula et on en efface toute trace
% on aurait du faire le m�me  genre de chose pour filter
% mais il a un autre m�canisme (je me souviens plsu lequel)
%<
last_formula = opt.remove('last_formula');
if ~isempty(last_formula)
    ids = strmatch('last_formula', varargin(1:2:end), 'exact');
    varargin(2*ids-1:2*ids) = [];
end
%>
if ~isempty(filter__)
    filter__ = @(st_data)filter(filter__, st_data, init_sec, varargin{:});
else
    filter__ = @(x)x;
end
date_from = datenum(opt.get('from'), opt.get('date_format:char'));
date_to   = datenum(opt.get('to'  ), opt.get('date_format:char'));
%     profile on
ids = bufferize_ref( 'get_read_dataset_ids_from_reference', {opt.get('security')}); %ids = get_read_dataset_ids_from_reference(opt.get('security')); %
%     profile off
%     profile viewer
if ~strcmp(mode, 'tickdb') % c'est surement pas une bonne id�e de faire du sp�cifique comme �a, mais je ne vois plus comment m'en sortir autrment
    sec = ids{end};
    opt.set('security', sec);
else
    sec = opt.get('security');
end
opt.set('security_id', ids{1});
opt.set('td_info', ids{2});
if (date_to >= today - weekday(today))
    warning('read_dataset:bufferize:exec', 'You are not allowed to ask for this week''s data, data will be available only on next monday');
    date_to = today - weekday(today) - 1;
    if date_to < date_from
        error('read_dataset:current_weekday', 'Current'' week data are not available in BSIRIUS'); % DO not error message id, this is used in st_indicator
    end
end
has_to_return_data = opt.get('get_data');
base_dir  = fullfile(fullfile( env_st_rep, 'read_dataset'), directory_);
if ~exist(base_dir, 'dir')
    mkdir( base_dir);
end
base_dir  = fullfile( base_dir, strtrim(sec));
if ~exist(base_dir, 'dir')
    st_log('Creating directory %s \n', base_dir);
    mkdir(base_dir);
end
data = cell(date_to-date_from+1,1);
for d=date_from:date_to
    wd = weekday(d);
    if wd~=1 && wd~=7
        this_date  = datestr( d, 'yyyy/mm/dd');
        st_log('working on <%s:%s>...\n', sec, this_date);
        this_fname = fullfile( base_dir, [strrep( this_date, '/', '_') '.mat']);
        if ~isempty( dir( this_fname))
            if has_to_return_data
                load( this_fname);
                if isempty(last_formula)
                    data{d-date_from+1} = filter__(day_);
                else
                    data{d-date_from+1} = st_data('apply-formula', day_, last_formula);
                end
            end
            %                 st_log('data found in .mat file\n');
            if frfie && isempty(day_) % Si les donn�es sont vides et que l'on a demand� � faire la requ�te dans ce cas
                st_log('Nevertheles, as the file is empty, we will have a look to the databases\n');
                opt.set('from', this_date);
                opt.set('to',   this_date);
                opt.set('date_format:char', 'yyyy/mm/dd');
                lst = opt.get();
                day_ = exec_req( mode, lst{:});
                if ~isempty(day_)
                    save( this_fname, 'day_');
                    st_log('data was empty, but this time we got some data\n');
                    if has_to_return_data
                        if isempty(last_formula)
                            data{d-date_from+1} = filter__(day_);
                        else
                            data{d-date_from+1} = st_data('apply-formula', day_, last_formula);
                        end
                    end
                else
                    st_log('data was empty, new request also gave empty data\n');
                end
            end
        else
            opt.set('from', this_date);
            opt.set('to',   this_date);
            opt.set('date_format:char', 'yyyy/mm/dd');
            lst = opt.get();
            day_ = exec_req( mode, lst{:});
            if isempty(day_)
                bsirius_gap_nb_days = 1;
                
                if today - d < 186
                    % Un test minimal : on v�rifie si l'on aurait pas trois jours 'ouvr�s' de suite
                    % pour lesquels on a pas de donn�es, si tel est le cas,
                    % il se pourrait que la raison soit que BSIRIUS n'est
                    % pas � jour, et que donc en r�alit� il existerait des
                    % donn�es, auquel cas, on s'en voudrait de bufferizer
                    % des journ�es vides et de trainer �a
                    % Il est inutile de faire ce test si le jour demand�
                    % n'est pas dans les 6 derniers mois...
                    
                    for i = 1 : 4
                        if weekday(d-i)~=1 && weekday(d-i)~=7
                            this_fname2 = fullfile( base_dir, [strrep( datestr( d-i, 'yyyy/mm/dd'), '/', '_') '.mat']);
                            if ~isempty(dir( this_fname2))
                                load(this_fname2);
                                if isempty(day_)
                                    bsirius_gap_nb_days = bsirius_gap_nb_days + 1;
                                end
                            else
                                bsirius_gap_nb_days = bsirius_gap_nb_days + 1;
                            end
                        end
                    end
                    if bsirius_gap_nb_days > 2
                        for i = 1 : 4
                            if weekday(d-i)~=1 && weekday(d-i)~=7
                                this_fname2 = fullfile( base_dir, [strrep( datestr( d-i, 'yyyy/mm/dd'), '/', '_') '.mat']);
                                if ~isempty(dir( this_fname2))
                                    load(this_fname2);
                                    if isempty(day_)
                                        st_log('Deleting file : <%s>\n', this_fname2);
                                        delete(this_fname2);
                                    end
                                end
                            end
                        end
                        if has_to_return_data && ~opt.get('I dont care about missing data')
                            error('read_dataset:bufferize:exec', 'I cannot believe that <%s> was not traded for such a long time \n\t As a consequence the last bufferized empty days has been deleted.\n To avoid this error use option ''I dont care about missing data'' and set it to true (This way you will not have any error and you wont bufferize any empty data if you asked for along enough period, but you wont get the data), another way is to wait for three month and then ask a copy of this quarter''s tick database', init_sec);
                        end
                    end
                    day_ = [];
                end
                if bsirius_gap_nb_days <= 2
                    st_log('read_dataset:%s:bufferize - empty day to bufferize for <%s:%s>\n', mode, sec, this_date);
                else
                    st_log('read_dataset:%s:bufferize - empty day for <%s:%s> wont be bufferized\n', mode, sec, this_date);
                end
            else
                bsirius_gap_nb_days = 1;
                st_log('read_dataset:%s:bufferize - <%s:%s> bufferized \n', mode, sec, this_date);
            end
            % sauve dans le fichier
            if bsirius_gap_nb_days <= 2
                save( this_fname, 'day_');
            end
            if has_to_return_data
                if isempty(last_formula)
                    data{d-date_from+1} = filter__(day_);
                else
                    data{d-date_from+1} = st_data('apply-formula', day_, last_formula);
                end
            end
        end
    end
end
if isempty(data) || opt.get('keep_d_by_d')
    return;
end
idx_empty = cellfun( @(x)isempty(x), data);
data(idx_empty) = [];
if isempty(data)
    return;
end
clear day_;
get_broker_ids = isfield(data{1}, 'info') && isfield(data{1}.info, 'broker_ids') ...
    && isfield(data{1}.info.broker_ids, 'buyer_id') ...
    && isfield(data{1}.info.broker_ids, 'seller_id')...
    && opt.get('get_brokers_ids');
if get_broker_ids
    b_id = cellfun(@(d)d.info.broker_ids.buyer_id,data, 'uni', false);
    s_id = cellfun(@(d)d.info.broker_ids.seller_id,data, 'uni', false);
end
dts  = cellfun(@(d)d.date,data, 'uni', false);
vals = cellfun(@(d)d.value,data, 'uni', false);
data = data{end}; % end car on esp�re que ce faisant on aura le maximum de td_info dans full_tickdb
data.date  = cat(1, dts{:});
clear dts
data.value = cat(1, vals{:});
clear vals
if get_broker_ids
    data.info.broker_ids.buyer_id = cat(1, b_id{:});
    if length(data.info.broker_ids.buyer_id) ~= length(data.date)
        warning('read_dataset:bufferize:exec', 'Some buyer_id are missing, so i''rather drop this information');
        data.info = rmfield(data.info, 'broker_ids');
    else
        data.info.broker_ids.seller_id = cat(1, s_id{:});
    end
end
data.title = init_sec;
%>*
end

function data = bufferize_only_if_not_old_enough(mode, directory_, varargin) % Mode de bufferization laxiste, interdisant la connaissance du futur, mais ne d�sirant pas n�cessairement l'information la plus fraiche
env_st_rep = getenv('st_repository');
if isempty( env_st_rep)
    error('read_dataset:bufferize', 'environment variable <st_repository> must be defined');
end
opt = options({'security', 'FTE.PA', 'from', '19/04/2007', 'to', '19/04/2007', 'waitbar', [] , 'date_format:char', 'dd/mm/yyyy'}, varargin);
wb = opt.get('waitbar');
sec       = opt.get('security');
date_to   = datenum(opt.get('to'  ), opt.get('date_format:char'));
ids = get_read_dataset_ids_from_reference(sec); % ids{1} : security_id, ids{2} : trading_destination_id, ids{3} : reference (toujours la m�me j'esp�re afin de bufferizer toujours dans le m�me dossier)
sec = ids{3};
opt.set('security', ids);
if isempty( env_st_rep)
    error('read_dataset:bufferize', 'environment variable <st_repository> must be defined');
end
base_dir  = fullfile(fullfile( env_st_rep, 'read_dataset'), directory_);
dbd = dir(base_dir);
if isempty( dbd)
    mkdir( base_dir);
end
base_dir  = fullfile( base_dir, sec);
if ~exist(base_dir, 'dir')
    st_log('Creating directory %s \n', base_dir);
    mkdir(base_dir);
end

fnames = dir(base_dir);fnames = {fnames.name};
max_date = 0;
j = -1;
for i = 3 : length(fnames)
    tmp = datenum(strrep(strrep(fnames{end}, sec, ''), '.mat', ''), '_dd_mm_yyyy');
    if  tmp > max_date && tmp <= date_to
        max_date = tmp;
        j = i;
    end
end
if j ~= -1 && max_date <= date_to;
    tmp = load( fullfile(base_dir, fnames{j}));
    data{1} = tmp.day;
    clear tmp;
    st_log('data found in .mat file\n');
else
    this_date = datestr(date_to, 'dd/mm/yyyy');
    opt.set('from', this_date);
    opt.set('to',   this_date);
    opt.set('date_format:char', 'dd/mm/yyyy');
    lst = opt.get();
    day = exec_req( mode, lst{:});
    if isempty(day)
        st_log('read_dataset:%s:bufferize - empty day to bufferize for <%s:%s>\n', mode, sec, this_date);
    end
    save( fullfile( base_dir, [ sec '_' strrep( this_date, '/', '_') '.mat']) , 'day');
    data{1} = day;
    clear day vals;
end
idx_empty = cellfun( @(x)isempty(x), data);
data(idx_empty) = [];
dts  = cellfun(@(d)d.date,data, 'uni', false);
dts  = cat(1, dts{:});
vals = cellfun(@(d)d.value,data, 'uni', false);
vals = cat(1, vals{:});
if isempty(data)
    return;
end
data = data{1};
data.date  = dts;
data.value = vals;
if ~isempty( wb)
    close( wb);
end
%>*
end

%TODO surveiller le comportement de cette fonction
function rslt = bufferize_ref(fname, fargs)
persistent memory
key = hash(sprintf('%s_%s', fname, convs('safe_str', fargs)), 'MD5');
try
    rslt = memory.get(key);
catch ME
    if isempty(memory)
        memory = options();
        rslt = bufferize_ref(fname, fargs);
        return;
    end
    if strcmp(ME.message, sprintf('key <%s> not available', key))
        rslt = feval(fname, fargs{:});
        memory.set(key, rslt);
    else
        rethrow(ME);
    end
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function to filter data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function data = filter(mode, data, security,varargin)
if isempty(data)
    return;
end
switch mode
    case 'tickdb'
        %             opt = options({}); TODO????
        td = bufferize_ref( 'get_td_from_ric', {security});
        idx_lines = data.value(:, 6) == td ... trading destination dont le trading_destination_id est le plus petit
            & ~data.value(:, 10)... pas de cross
            & ~data.value(:, 11)... pas d'auction
            & ~data.value(:, 14)... pas de trading at last
            & ~data.value(:, 15); % pas de trading after hours
        info = data.info;
        data = rmfield(data, 'info'); % On retire le champs info (peut contenir les broker_ids dans le cas de l'espagne)
        data.date = data.date(idx_lines);
        data.value = data.value(idx_lines, [1:5, 7, 8]); % on ne garde que les colonnes Price Volume sell bid ask
        data.colnames = data.colnames([1:5, 7, 8]);
        for i = 1 : size(info.td_info, 1)
            if info.td_info{i, 1} == td
                data.info.td_info = info.td_info(i, :);
                break;
            end
        end
    otherwise
        error('read_dataset:filter:exec', 'mode <%s> unknown', mode);
end
end

function td = get_td_from_ric(ric)
td = cell2mat(exec_sql('BSIRIUS', sprintf('select trading_destination_id from repository..security_source where reference = ''%s'' and source_id = 2', ric)));
if isempty(td)
    error('read_dataset:get_td_from_ric:exec', 'No RIC (source_id = 2) found for this security');
end
end

function rslt = get_td_and_sec_id_from_ric(ric)
rslt = cell2mat(exec_sql('BSIRIUS', sprintf('select trading_destination_id ,security_id from repository..security_source where reference = ''%s'' and source_id = 2', ric)));
if isempty(td)
    error('read_dataset:get_td_from_ric:exec', 'No RIC (source_id = 2) found for this security');
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vieux modes                                                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function data = old_read_dataset( mode, varargin)
% READ_DATASET - import d'un ensemble de donn�es
%
% Deux types de mode existent, l'un permet de coder un import de donn�es et
% d'obtenir 'gratuitement' un syst�me de bufferization simple, l'autre type
% de mode permet de coder tous les imports dont le syst�me de bufferisation
% sp�cifique.
%
% Pour le premier type de mode, il suffira d'ajouter le caract�re b au
% d�but du mode (!! attention ce la signifie que le mode ne doit pas
% commencer par la lettre b sous peine de ne plus pour voir acc�der � ce
% mode hors bufferisation) pour pouvoir bufferiser les donn�es.
%
% Pour le second type de mode, il s'agit d'un switch case comme d'habitude
%
% Le premier type de mode est impl�ment� dans la sous-fonction 'exec_req',
% son syst�me de bufferisation commun est impl�ment� dans la
% sous-fonction 'bufferize', et le choix du nom du r�pertoire dans lequel
% bufferizer doit �tre impl�ment� dans 'get_directory'.
%
% Le deuxi�me type de mode doit �tre impl�ment� dans la sous-fonction 'old_read_dataset'
%
%
% - Modes partageant le m�me syst�me de bufferisation : 'tickdb',
%       'orderbook' (alias 'ob'), 'liquidity consumers' (alias 'lc')
%
%   data = read_dataset( 'tickdb', 'security', 'TOTF.PA', 'from', '26/06/2007', 'to', '26/06/2007')
%
%   data = read_dataset( 'orderbook', 'security', 'FTE.PA', 'from','02/06/2007', 'to', '12/06/2007' )
%
%   data = read_dataset( 'liquidity consumers', 'security', 'TOTF.PA', 'from', '26/06/2007', 'to', '26/06/2007')
%
%   data = read_dataset( 'daily', 'security', 'TOTF.PA', 'from', '26/06/2007', 'to', '26/06/2007')
%
%   read_dataset('tickdb', 'security', 'FTEpa.CHI', 'from', '14/01/2008','to', '14/01/2008')
%
% M�me chose mais en bufferisant les donn�es :
%
%   data = read_dataset( 'btickdb', 'security', 'TOTF.PA', 'from', '26/06/2007', 'to', '26/06/2007')
%
%   data = read_dataset( 'ob', 'security', 'FTE.PA', 'from','02/06/2007','to', '12/06/2007' ) % inutile de cahnger quoi que ce soit, ce mode bufferise tout le temps les donn�es
%
%   data = read_dataset( 'blc', 'security', 'TOTF.PA', 'from', '26/06/2007', 'to', '26/06/2007')
%
%   data = read_dataset( 'bdaily', 'security', 'TOTF.PA', 'from', '26/06/2007', 'to', '26/06/2007')
%
% - Modes Sp�cifiques :
%
%   data = read_dataset( 'tickdb:arch', 'security', 'SN.L', 'from', '6/02/2007', 'to', '10/02/2007')
%
%   read_dataset('plot-tickdb', data)
%
%   read_dataset('tickdb:select', {'indice_id', 'name'}, 'from repository..indice');
%
%   data = read_dataset( 'volumes-intraday', 'security', 'SN.L', 'from', '6/02/2007', 'to', '10/02/2007')
%   data = read_dataset( 'volumes-intraday', 'security', get_index('comp', 'CAC40'), 'from', '6/02/2007', 'to', '10/02/2007')
%
%   data = read_dataset( 'bvolumes-intraday', 'security', 'FTE.PA', 'from', '19/04/2007', 'to', '25/04/2007' )
%
%   data = read_dataset('daily', 'FTE.PA') % max days
%   data = read_dataset('daily', 'FTE.PA', 'day', '06/08/2007')
%   data = read_dataset('daily', 'FTE.PA', 'from', '06/08/2007', 'to', '16/08/2007')
%
%   data = read_dataset('history', 'FTE.PA', 'from', '02/07/2007', 'to', '02/07/2007', 'login', '***', 'password', '***' )
%   data = st_data('where', data, '{Remaining quantity}==0')
%   read_dataset('plot-history', data)
%   read_dataset( 'plot-history+tick', data, data_tick)
%
% See also get_index get_market_exec

%1 ere verrue : les modes qui ne peuvent pas �tre appel� sans bufferisation
if ~isempty(strmatch(mode, {'ob', 'orderbook'}, 'exact')) % => ce mode ne peut donc pas �tre appel� sans bufferisation
    data = bufferize(mode, get_directory(mode), varargin{:});
    % 2eme verrue les modes sp�cifiques, qui ont soit leur propore fonction
    % de bufferisation, soit un fonctionnement trop complexe pour rentrer
    % dans le sch�ma de bufferisation impl�ment� ici, soit pasque plus
    % personne ne les utilise et donc on ne fait pas l'effort de les porter
    % dans le nouveaux code
elseif ~isempty(strmatch(mode, {'bvolumes-intraday', 'bintraday-volumes', 'bivolumes', 'bivwap', 'biproportion', ...
        'plot-tickdb', 'plot-history+tick', 'plot-history', 'history', ...
        'tickdb:select', 'tickdbdanats', 'tickdbdana', 'volumes-intraday', 'intraday-volumes',...
        'ivolumes', 'ivwap', 'iVWAP', 'IVWAP', 'iproportion', 'tickdb:arch', 'tickdb*'}, 'exact'))
    if strcmpi( mode, 'tickdb*')
        mode = 'tickdb:arch';
    end
    
    data = old_old_read_dataset(mode, varargin{:});
    % Maintenant il s'agit des nouveaux modes, soit on veut bufferizer, alors le mode commence par un b
elseif mode(1) == 'b'
    mode = lower(mode(2:end));
    switch mode
        case 'cv_prod'
            data = bufferize_only_if_not_old_enough(mode, get_directory(mode), varargin{:});
        otherwise
            data = bufferize(mode, get_directory(mode), varargin{:});
    end
elseif strcmp(mode, 'clean')
    work_on_bdata_in_dir('clean_subdirs', fullfile(fullfile( getenv('st_repository'), 'read_dataset'), get_directory(varargin{1})));
else
    % Soit on ne veut pas bufferize, alors le nom du mode ne doit pas
    % commencer par un b !!!!!!!!!!!!!!!!!
    opt = options({'security', 'FTE.PA'}, varargin); % celui qui ne comprend pas pourquoi on se complique la vie, avec les trois lignes suivantes, n'a qu'� venir voir Romain, il va se faire recevoir...
    ids = get_read_dataset_ids_from_reference(opt.get('security'));
    opt.set('security', ids);
    lst = opt.get();
    data = exec_req(mode, lst{:});
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tr�s Tr�s vieux modes                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function data = old_old_read_dataset(mode, varargin)

turnover_only = true;
volume_only   = false;
fname_prefix  = 'bvi_';

switch lower(mode)
    case { 'ivwap', 'bivwap'}
        turnover_only = false;
        fname_prefix  = 'bwi_' ;
    case {'iproportion', 'biproportion'}
        volume_only   = true;
        fname_prefix  = 'bpi_';
end
switch lower(mode)
    case 'tickdb:arch'
        %<* Read into the tickdb
        opt = options({'table-name', 'deal', 'base-name', '', 'security', 'FTE.PA', ...
            'from', '19/04/2007', 'to', '19/04/2007', 'connection', {'seltick','obelix'}, ...
            'auctions', false }, varargin);
        connection_lp = opt.get('connection');
        %< Database connection
        base_name = opt.get('base-name');
        if isempty( base_name)
            if strcmpi( mode, 'tickdb')
                base_name = 'tick_db';
            elseif strcmpi( mode, 'tickdb:arch')
                warning('read_datset:tickdb', 'I will use a prod database');
                base_name = 'tick_db_arch';
            end
        end
        c   = userdatabase('sybase:j', 'BSIRIUS');
        %>
        frm = opt.get('from');
        if ischar( frm)
            frm = datenum( frm, 'dd/mm/yyyy');
        end
        too = opt.get('to');
        if ischar( too)
            too = datenum( too, 'dd/mm/yyyy');
        end
        
        select_str = 'select date,convert( char(8), time, 108),price,size,overbid,bid,ask from %s as D ( index security_ind ), repository..security_source as ID ';
        if strcmpi( mode, 'tickdb:arch')
            archive_name = 'tick_db..deal';
        else
            archive_name = sprintf('tick_db_%s_%s..deal_archive', lower(datestr( frm,18)), datestr( frm, 'yyyy'));
        end
        if strcmpi( mode, 'tickdb')
            if (frm == too) % Pour que btickdb aille taper tout seul dans les archives sans que l'on lui dise de le faire. On peut se permettre de faire les trois requetes dans la mesure o� s'il n'y a pas de donn�es la r�ponse sera instantan�e
                if strcmp(datestr( frm,18),datestr( today,18)) && year(frm) == year(today)
                    main_req = {sprintf( select_str, 'deal');sprintf( select_str, 'deal_day')};
                else
                    main_req = {sprintf( select_str, archive_name);sprintf( select_str, 'deal');sprintf( select_str, 'deal_day')};
                end
            else
                main_req = sprintf(select_str, opt.get('table-name') );
            end
        else
            main_req = sprintf(select_str, archive_name);
        end
        if opt.get('auctions')
            auc_str = '';
        else
            auc_str = ' and auction=0 ';
        end
        if frm < 733133 % avant le deuxi�me trimestre 2007 les tables ne contiennent pas le champs trading_destination_id
            trd_dest_str = '';
        else
            trd_dest_str = '   and isnull(D.trading_destination_id ,-1) <= 50 ';
        end
        end_req = sprintf(['  where ID.security_id= D.security_id ' ...
            '   and ID.reference = ''%s'' ' ...
            '   and ID.source_id = 2 ' ...
            '   and date >= ''%s'' ' ...
            '   and date <= ''%s'' ' ...
            auc_str ...
            trd_dest_str ...
            '  order by date, time ' ], opt.get('security'), datestr( frm, 'mmm dd yyyy'), ...
            datestr( too, 'mmm dd yyyy') );
        if iscell(main_req)
            for i = 1 : length(main_req)
                sql_req = [main_req{i} end_req];
                t0 = cputime;
                try
                    vals = exec_sql(c, sql_req);
                    if ~isempty(vals)
                        break;
                    end
                catch end
            end
            if isempty(vals)
                error('read_dataset:emptyset', 'the request got back an empty set');
            end
        else
            sql_req = [main_req end_req];
            t0 = cputime;
            vals = exec_sql(c, sql_req);
            if isempty(vals)
                error('read_dataset:emptyset', 'the request got back an empty set');
            end
        end
        st_log('end of fetch (%5.2f sec)\n', cputime-t0);
        data = st_data('from-matrix', vals, 'title', opt.get('security'), 'date', '{1}yyyy-mm-dd', ...
            'time', '{2}HH:MM:SS', 'colnames', {'price', 'volume', 'sell', 'bid', 'ask'} );
        data.info = struct('sql_request', sql_req);
    case 'do not use but do not delete' % future price impact : should use deal and orderbook_updates
        sdata = ['select security_id, trading_destination_id, date,'...
            ' convert(char(8),time,108), seq = datepart( ms, time),'...
            ' dsize, dprice, oprice, osize, onumber, oside, odepth'...
            ' from ('...
            ' select security_id, trading_destination_id, date, '...
            ' time, dsize= size, dprice=price, oprice is null,'...
            ' osize is null, onumber is null, oside is null, odepth is null'...
            ' from tick_db..deal'...
            ' union'...
            ' select security_id, trading_destination_id, date, time,'...
            ' dsize is null, dprice is null, oprice=price, osize=size,'...
            ' onumber=mmk_number, oside=side, odepth=depth'...
            ' from tick_db_q4_2007..orderbook_update'...
            ' where depth = 0 ) i'...
            ' where security_id = 110 and trading_destination_id = 4 and date = ''2007-12-17'''...
            ' order by time'...
            ' go'];
    case { 'bvolumes-intraday', 'bintraday-volumes', 'bivolumes', 'bivwap', 'biproportion' }
        %<* Bufderized volume intraday
        % Needs the |st_repository| env variable
        opt  = options({'security', 'FTE.PA', 'from', '19/04/2007', ...
            'to', '20/04/2007', 'fixed_nb_days', -1, 'nb_attempts', 0,...
            'data_filter', 'on','turnover only', turnover_only, ...
            'volume only', volume_only}, varargin);
        sec = opt.get('security');
        
        if iscell( sec)
            data = [];
            for s = 1:length(sec)
                opt.set('security', sec{s});
                lst = opt.get();
                data_unit = old_read_dataset( mode, lst{:});
                data = st_horzcat(data, check_data_length(...
                    data_unit, mode, opt),...
                    'default_value', 0, 'colnames2', @(x,y)[x ' de ' y]);
                opt.set('nb_attempts', 0);
            end
            return
        elseif sec(1) == '&'
            opt.set('security', get_index('comp', sec(2:end)));
            lst = opt.get();
            data = old_read_dataset(mode, lst{:});
            return
        end
        
        % je vais d'abord fabriquer mes titres
        date_from = datenum(opt.get('from'), 'dd/mm/yyyy');
        date_to   = datenum(opt.get('to')  , 'dd/mm/yyyy');
        dts       = cellstr( datestr(date_from:date_to, 'dd/mm/yyyy'));
        % ATTENTION: j'ai ajout� ici le mot clef ' - historique'
        titles    = tokenize(sprintf([ sec ' - historique x %s;'], dts{:}), ';');
        % puis r�cup�rer ce qui existe sur le repository
        
        fname = [ fname_prefix sec '.mat' ];
        try
            rep_data = st_data('ope-load', fname, titles);
            existing_file = true;
        catch
            lasterr('');
            st_log('No repository file for intraday volumes of <%s>\n', sec);
            rep_data = {};
            existing_file = false;
        end
        rep_titles = cellfun(@(c)c.title, rep_data, 'uni', false);
        % finalement je vais r�cup�rer les manquant dans la base
        mig_titles  = setdiff( titles, rep_titles);
        if ~isempty(mig_titles) % Voir avec Charles cette correction un peu brutale
            mig_dates   = datenum(regexprep( mig_titles, '.* x (.*)$', '$1'), 'dd/mm/yyyy');
            new_date_to =  max(mig_dates);
            opt.set('to' , datestr(new_date_to, 'dd/mm/yyyy'));
            if isempty( rep_titles)
                new_date_from = min(mig_dates);
                opt.set('from' , datestr( new_date_from, 'dd/mm/yyyy'));
            else
                rep_dates  = datenum(regexprep( rep_titles, '.* x (.*)$', '$1'), 'dd/mm/yyyy');
                new_date_from = max(rep_dates)+1;
                opt.set('from', datestr( new_date_from, 'dd/mm/yyyy'));
            end
            if new_date_to < new_date_from
                st_log('Nothing to load for <%s> from %s to %s.\n', ...
                    opt.get('security'), opt.get('from'), opt.get('to') );
                data_ope = rep_data;
            else
                lst = opt.get();
                mig_data   = old_read_dataset( mode(2:end), lst{:});
                if ~isempty(mig_data) % TODO better
                    % les d�couper en liste d'op�rations quotidiennes
                    mig_data   = st_data('data2ope', mig_data, 'days');
                    % et les mettre dans le repository
                    if existing_file
                        st_data( 'ope-add', fname, mig_data);
                    else
                        st_data( 'ope-save', fname, mig_data);
                    end
                    % puis recup�rer tout le monde
                    data_ope = cat(2, rep_data, mig_data);
                else
                    data_ope = rep_data;
                end
            end
        else
            data_ope = rep_data;
        end
        % et les stacker
        if isempty(data_ope) % TODO better
            data = {};
        else
            data = data_ope{1};
            if length(data_ope)>1
                vals = cellfun(@(c)c.value,data_ope,'uni',false);
                data.value = cat(1,vals{:});
                dts  = cellfun(@(c)c.date,data_ope,'uni',false);
                data.date  = cat(1,dts{:});
            end
            data.title = regexprep(data.title, '(.*) x .*$', '$1');
        end
        %>*
    case { 'volumes-intraday', 'intraday-volumes', 'ivolumes', 'ivwap', 'iVWAP', 'IVWAP', 'iproportion'}
        %<* Read intraday volumes
        opt  = options({'security', 'FTE.PA', 'from', '19/04/2007', ...
            'to', '20/04/2007', 'fixed_nb_days', -1, 'nb_attempts', 0,...
            'data_filter', 'on','turnover only', turnover_only, ...
            'volume only', volume_only}, varargin);
        sec_name = opt.get('security');
        if (iscell(sec_name))
            data = [];
            for s = 1:length(sec_name)
                opt.set('security', sec_name{s});
                data = st_horzcat(data, check_data_length(...
                    read_intraday_volumes(opt), mode, opt),...
                    'default_value', 0, 'colnames2', @(x,y)[x ' de ' y]);
                opt.set('nb_attempts', 0);
            end
        elseif sec_name(1) == '&'
            opt.set('security', get_index('comp', sec_name(2:end)));
            lst = opt.get();
            data = old_read_dataset(mode, lst{:});
        else
            data = check_data_length(read_intraday_volumes(opt), mode, opt);
        end
        %>*
    case { 'tickdbdana'}
        %<* Read into the tickdb
        opt = options({'table-name', 'deal', 'base-name', '', 'security', 'FTE.PA', ...
            'from', '19/04/2007', 'to', '19/04/2007', 'connection', {'batch_tick','batick'}, ...
            'auctions', false }, varargin);
        connection_lp = opt.get('connection');
        %< Database connection
        base_name = opt.get('base-name');
        if isempty( base_name)
            if strcmpi( mode, 'tickdbdana')
                base_name = 'tick_db';
            elseif strcmpi( mode, 'tickdb:arch')
                warning('read_datset:tickdb', 'I will use a prod database');
                base_name = 'tick_db_arch';
            end
        end
        c   = userdatabase('sybase:j', 'BSIRIUS');
        %>
        frm = opt.get('from');
        if ischar( frm)
            frm = datenum( frm, 'dd/mm/yyyy');
        end
        too = opt.get('to');
        if ischar( too)
            too = datenum( too, 'dd/mm/yyyy');
        end
        
        if strcmpi( mode, 'tickdbdana')
            main_req = sprintf( 'select date,convert( char(8), time, 108),price,size,overbid,bid,ask,cross from %s as D, repository..security_source as ID ', ...
                opt.get('table-name') );
            
        else
            main_req = sprintf('select date,time,price,size,overbid,bid,ask,cross from tick_db_%s_%s..deal_archive as D, repository..security_source as ID ', ...
                lower(datestr( frm,18)), datestr( frm, 'yyyy'));
        end
        if opt.get('auctions')
            auc_str = '';
        else
            auc_str = ' and auction=0 ';
        end
        sql_req = sprintf([main_req ...
            '  where ID.security_id= D.security_id ' ...
            '   and ID.reference = ''%s'' ' ...
            '   and ID.source_id = 2 ' ...
            '   and date >= ''%s'' ' ...
            '   and date <= ''%s'' ' ...
            auc_str ...
            '   and isnull(D.trading_destination_id ,-1) <= 50 '...
            '  order by date, time ' ], opt.get('security'), datestr( frm, 'mmm dd yyyy'), ...
            datestr( too, 'mmm dd yyyy') );
        t0 = cputime;
        vals = exec_sql(c, sql_req);
        if isempty(vals)
            error('read_dataset:emptyset', 'the request got back an empty set');
        end
        st_log('end of fetch (%5.2f sec)\n', cputime-t0);
        data = st_data('from-matrix', vals, 'title', opt.get('security'), 'date', '{1}yyyy-mm-dd', ...
            'time', '{2}HH:MM:SS', 'colnames', {'price', 'volume', 'sell', 'bid', 'ask', 'cross'} );
        data.info = struct('sql_request', sql_req);
        
    case { 'tickdbdanats'}
        %<* Read into the tickdb
        opt = options({'table-name','trading_daily', 'base-name', '', 'security', 'FTE.PA', ...
            'from', '19/04/2007', 'to', '19/04/2007', 'connection', {'batch_tick','batick'}, ...
            'auctions', false }, varargin);
        connection_lp = opt.get('connection');
        %< Database connection
        base_name = opt.get('base-name');
        if isempty( base_name)
            if strcmpi( mode, 'tickdbdanats')
                base_name = 'tick_db';
            elseif strcmpi( mode, 'tickdb:arch')
                warning('read_datset:tickdb', 'I will use a prod database');
                base_name = 'tick_db_arch';
            end
        end
        c   = userdatabase('sybase:j', 'BSIRIUS');
        %>
        frm = opt.get('from');
        if ischar( frm)
            frm = datenum( frm, 'dd/mm/yyyy');
        end
        too = opt.get('to');
        if ischar( too)
            too = datenum( too, 'dd/mm/yyyy');
        end
        
        if strcmpi( mode, 'tickdbdanats')
            main_req = sprintf( 'select sum(volume),sum(nb_deal) from %s as D, repository..security_source as ID', ...
                opt.get('table-name') );
            
        else
            %main_req = sprintf('select date,volume from tick_db_%s_%s..deal_archive as D, repository..security_source as ID ', ...
            %   lower(datestr( frm,18)), datestr( frm, 'yyyy'));
        end
        sql_req = sprintf([main_req ...
            '  where ID.security_id= D.security_id ' ...
            '   and ID.reference = ''%s'' ' ...
            '   and ID.source_id = 2 ' ...
            '   and date >= ''%s'' ' ...
            '   and date <= ''%s'' ' ...
            ], opt.get('security'), datestr( frm, 'mmm dd yyyy'), ...
            datestr( too, 'mmm dd yyyy') );
        t0 = cputime;
        vals = exec_sql(c, sql_req);
        if isempty(vals)
            error('read_dataset:emptyset', 'the request got back an empty set');
        end
        st_log('end of fetch (%5.2f sec)\n', cputime-t0);
        data = st_data('from-matrix', vals, 'title', opt.get('security'),'colnames', {'Sumvolume', 'SumNbDeal'} );
        data.info = struct('sql_request', sql_req);
        
        
        %>*
    case 'tickdb:select'
        %<* A select returns an st_data
        c        = userdatabase('sybase:j', 'BSIRIUS');
        colnames = varargin{1};
        req      = varargin{2};
        sql_req = ['select ' sprintf('%s,', colnames{:})];
        sql_req = [sql_req(1:end-1) ' ' req];
        vals    = exec_sql(c, sql_req);
        idc     = cellfun(@(x)isnumeric(x),vals);
        if all(idc(:))
            vals = cell2mat( vals);
        end
        data    = st_data('init', 'title', req, 'value', vals, 'date', (1:size(vals,1))', 'colnames', colnames);
        %>*
    case 'history'
        %<* Selection of history of cheuvreux orders
        ticker = varargin{1};
        opt    = options({ 'from', '16/08/2007', 'to', '16/08/2007', 'login', '***', 'password', '***'}, ...
            varargin(2:end) );
        
        if isnumeric( ticker)
            security_id = ticker;
        else
            sec_id = get_index('other-codes', ticker, 'topcac');
            security_id = sec_id.security_id;
        end
        dt_from     = DATE2SYBASE( opt.get('from'));
        dt_to       = DATE2SYBASE( opt.get('to'));
        
        c      = userdatabase('sybase:j', 'VEGA');
        
        vals = exec_sql(c, sprintf( [ ...
            '  select  ' ...
            '   s.name, ' ...
            '   c.security_id, ' ...
            '   c.market_id, ' ...
            '   o.market_order_id, ' ...
            '   o.ack_type, ' ...
            '   o.send_time, ' ...
            '   a.exec_time, ' ...
            '   o.side, ' ...
            '   o.limit_price, ' ...
            '   a.exec_id, ' ...
            '   a.quantity exec_quantity, ' ...
            '   a.price, ' ...
            '   a.remain_quantity, ' ...
            '   b.third_id ' ...
            '      from    market_stat..global_market_execution as a ' ...
            '   inner join market_stat..global_market_order     as o ' ...
            '    on a.security_code = o.security_code ' ...
            '    and a.market_id = o.market_id ' ...
            '    and a.order_internal_ref = o.order_internal_ref ' ...
            '        and a.order_market_ref = o.order_market_ref ' ...
            '        and a.security_code = o.security_code ' ...
            '   inner join market_stat..global_market_operator  as b ' ...
            '    on a.operator_code = b.user_sle_id ' ...
            '    and a.market_id = b.market_id ' ...
            '    and a.server_id = isnull(b.server_id,a.server_id) ' ...
            '    and a.file_date between b.begin_date and isnull(b.end_date, a.file_date) ' ...
            '   inner join market_stat..security_coefficient    as c  ' ...
            '    on a.security_code = c.security_code ' ...
            '    and ISNULL(c.reference_compl,''1'') = ISNULL(a.reference_compl,''1'') ' ...
            '    and a.file_date = c.date ' ...
            '    and a.market_id = c.market_id ' ...
            '   inner join market..security                     as s  ' ...
            '    on s.security_id = c.security_id ' ...
            '      where   ' ...
            '    b.business_source = ''R''  ' ...
            '   and c.security_type= 1  ' ...
            '   and o.limit_price = 0 ' ...
            '   and s.security_id = %d ' ...
            '   and o.ack_type not in (''E'' ,''G'' ,''C'', ''L'')  ' ...
            '   and a.file_date between ''%s'' and ''%s'' ' ...
            '   and o.send_date between ''%s'' and ''%s'' ' ...
            '   order by a.exec_time ' ], ...
            security_id, dt_from, dt_to, dt_from, dt_to ) );
        
        num_vals = cell2mat( vals(:,[3 9 11 12 13 14]));
        x_id     = cellfun(@(c)str2double(c), vals(:,10));
        dts      = datenum( vals(:,6), 'yyyy-mm-dd HH:MM:SS.FFF');
        dts_x    = datenum( vals(:,7), 'yyyy-mm-dd HH:MM:SS.FFF');
        [tmp, tmp, ack_t] = unique( vals(:,5));
        data     = st_data('init', 'title', vals{1,1}, 'date', dts_x, ...
            'value', [num_vals, dts_x - dts, cellfun(@(c)c=='B', vals(:,8)), ack_t, x_id], ...
            'colnames', { 'Market id', 'Limit price', 'Exec quantity', 'Price', 'Remaining quantity', 'Client id', 'delta t', 'Buy', 'Acknowledgement', 'Exec id' } );
        %>*
    case 'plot-history'
        %<* plot historical execs
        figure;
        data    = varargin{1};
        price   = st_data('col', data, 'Price');
        volume  = st_data('col', data, 'Exec quantity');
        buy     = st_data('col', data, 'Buy');
        cvolume = cumsum(volume .* price .* (2*buy-1));
        mima    = [min(data.date) max(data.date)];
        
        f  = uipanel('backgroundcolor', [238 238 208]/256, 'borderwidth', 0);
        ap = subplot(2,1,1); set(ap, 'parent', f);
        plot(data.date, price, '-', 'linewidth', 2, 'color', [0.67 0 .122]);
        hold on
        plot(data.date, price, '.', 'linewidth', 2, 'color', [0.67 0 .122]);
        hold off
        ax = axis;
        axis([mima ax(3:4)]);
        stitle( '%s from %s to %s', data.title, datestr(floor(mima(1)),'dd/mm/yy'), ...
            datestr(floor(mima(2)),'dd/mm/yy') );
        ylabel('Price');
        
        av = subplot(2,1,2); set(av, 'parent', f);
        stairs(data.date, cvolume, 'linewidth', 2);
        ylabel('Net Turnover');
        
        linkaxes([ap,av], 'x');
        set(ap,'xticklabel',[]);
        attach_date('init', 'date', mima, 'format', 'HH:MM:SS');
        %>*
    case 'plot-history+tick'
        %<* plot historical execs
        figure;
        
        data    = varargin{1};
        price   = st_data('col', data, 'Price');
        volume  = st_data('col', data, 'Exec quantity');
        buy     = st_data('col', data, 'Buy');
        cvolume = cumsum(volume .* price .* (2*buy-1));
        
        data_t    = varargin{2};
        data_t.date = data_t.date + datenum(0,0,0, 2,0,0);
        price_t   = st_data('col', data_t, 'Price');
        volume_t  = st_data('col', data_t, 'Volume');
        buy_t     = 1-st_data('col', data_t, 'Sell');
        cvolume_t = cumsum(volume_t .* price_t .* (2*buy_t-1));
        mean_tt   = mean( volume_t .* price_t);
        
        mima    = [min(data_t.date) max(data_t.date)];
        
        f  = uipanel('backgroundcolor', [238 238 208]/256, 'borderwidth', 0);
        ap = subplot(3,1,1); set(ap, 'parent', f);
        plot(data_t.date, price_t, '-k', 'linewidth', 2);
        hold on
        plot(data.date, price, '.', 'linewidth', 2, 'color', [0.67 0 .122]);
        hold off
        ax = axis;
        axis([mima ax(3:4)]);
        stitle( '%s from %s to %s', data.title, datestr(floor(mima(1)),'dd/mm/yy'), ...
            datestr(floor(mima(2)),'dd/mm/yy') );
        ylabel('Price');
        
        
        avm = subplot(3,1,2); set(avm, 'parent', f);
        stairs(data_t.date, cvolume_t, '-k', 'linewidth', 2);
        hold on
        plot([mima, NaN, mima], [mean_tt, mean_tt, NaN, -mean_tt, -mean_tt], ':r', 'linewidth', 2);
        hold off
        title('Net market turnover');
        
        av = subplot(3,1,3); set(av, 'parent', f);
        stairs(data.date, cvolume, 'linewidth', 2, 'color', [0.67 0 .122]);
        hold on
        plot([mima, NaN, mima], [mean_tt, mean_tt, NaN, -mean_tt, -mean_tt], ':r', 'linewidth', 2);
        hold off
        title('Net CA Cheuvreux turnover');
        
        linkaxes([ap,avm,av], 'x');
        set(ap,'xticklabel',[]);
        set(avm,'xticklabel',[]);
        attach_date('init', 'date', mima, 'format', 'HH:MM:SS');
        %>*
    case 'plot-tickdb'
        %<* Plot result of the tickdb
        data_all   = varargin{1};
        if ~iscell(data_all)
            data_all = {data_all};
        end
        opt    = options({'cumulated-volume', false}, varargin(2:end));
        
        
        for i = 1 : length(data_all)
            data = data_all{1,i};
            sell   = logical(st_data('col', data, 'sell'));
            price  = st_data('col', data, 'price');
            volume = st_data('col', data, 'volume');
            if any(strcmp(data.colnames,'dark'))
                dark=st_data('col', data, 'dark')==1;
            else
                dark=false(size(volume));
            end
            h1=subplot(2,length(data_all),2*i-1);
            plot( data.date(sell & ~dark), price(sell & ~dark), 'vr', 'MarkerEdgeColor', 'none', 'markerfacecolor', [.9 .2 .1]);
            hold on
            plot( data.date(~sell & ~dark), price(~sell & ~dark), '^g','MarkerEdgeColor', 'none', 'markerfacecolor', [.2 .2 .9]);
            if any(dark)
                hold on
                plot( data.date(dark), price(dark), 'o','color', 'k');
            end
            plot(data.date, cumsum( price.*volume)./cumsum(volume), '-', ...
                'linewidth',1, 'color', [.667, 0,.122]);
            stairs(data.date, st_data('col', data, 'bid'), 'color', [.5 .5 .8]);
            stairs(data.date, st_data('col', data, 'ask'), 'color', [.8 .5 .5]);
            hold off
            ylabel('Price');
            set(h1,'xticklabel',[],'xtick',[]);
            st_plot('menu', 'init', gca);
            stitle( '%s from %s to %s', data.title, datestr(data.date(1),'dd/mm/yyyy'), datestr(data.date(end),'dd/mm/yyyy'));
            
            if any(isfinite(price))
                x_tmp=[min(data.date), max(data.date)];
                if (x_tmp(2)==x_tmp(1))
                    x_tmp(2)=x_tmp(2)+1/(24*60);
                end
                y_tmp=[min(price)-0.05*(max(price)-min(price)), max(price)+0.05*(max(price)-min(price))];
                if (y_tmp(2)==y_tmp(1))
                    y_tmp(2)=y_tmp(2)*1.0001;
                end
                axis([x_tmp y_tmp]);
            end
            
            h2=subplot(2,length(data_all),2*i);
            if opt.get('cumulated-volume')
                plot( data.date, cumsum( volume), 'linewidth', 2, 'color', [.1 .3 .1]);
                ylabel('Cumulated volume');
            else
                stem( data.date(~dark), volume(~dark), 'color', [.1 .3 .1]);
                if any(dark)
                    hold on
                    stem( data.date(dark), volume(dark),'color', 'k');
                end
                ax=axis;
                hold on
                plot( data.date, cumsum(volume)/sum(volume)*ax(4), 'color', [.5 .8 .5]);
                hold off
                ylabel('Volume');
                if isfield( data, 'threshold')
                    thres = data.threshold;
                    idx_c = strmatch( 'volume', thres.rownames{1}, 'exact');
                    if ~isempty( idx_c)
                        thres = st_data('from-idx', thres, idx_c);
                        cm = hsv(size(thres.value,2));
                        hg = [];
                        hold on
                        for c=1:size(thres.value,2)
                            hg    = [hg; plot( [data.date(1), data.date(end)], [thres.value(1,c), thres.value(1,c)], 'r', 'color', cm(c,:) )];
                            idx_s = (volume>thres.value(1,c));
                            plot( data.date(idx_s), volume(idx_s), '.', 'color', cm(c,:));
                        end
                        hold off
                        legend( hg, thres.colnames);
                    end
                end
            end
            st_plot('menu', 'init', gca);
            
            linkaxes([h2,h1], 'x');
            ax = axis;
            axis([min(data.date), max(data.date), ax(3:4)]);
            attach_date('init', 'date', data.date, 'format', 'HH:MM:SS');
        end
        data = [h1,h2];
        %>*
    otherwise
        error('read_dataset:mode', 'mode <%s> unknown', mode);
end
end

function rslt = cv_select_prod_get_curve(varargin)
% varargin{1} : curve_id
% varargin{2} : cotation_group
c = userdatabase('sybase:j', 'BILBO');
vals = exec_sql(c, sprintf( [ 'select adjusted_volume, convert(varchar, end_time, 112), phase, '...
    'convert(varchar, end_time, 108), volume '...
    'from market..trading_curve_volume '...
    ' where curve_id = %d '...
    ' order by end_time, phase'], varargin{1}));
if isempty(vals)
    rslt = [];
    return;
end
% pas de trading at last
vals(strmatch('TA',vals(:,3)),:) = [];
% conversion des dates GMT : ATTENTION, ici le d�calage horaire des
% courbes n'est pas celui qui valait lorsque la courbe a �t�
% construite, mais celui qui vaut aujourd'hui => pour la conversion
% en horaires locaux on passe � la date d'aujourd'hui, mais ensuite
% on revient � la date de cr�ation de la courbe
dts = datenum(cellfun(@(x,y)[x ' ' y], repmat({datestr(now, 'yyyymmdd')}, size(vals, 1), 1), vals(:,4),'uni',false),'yyyymmdd HH:MM:SS');
dts = gmt('dec-fr', dts);
% On revient � la date de cr�ation de la courbe
tmp = exec_sql(c, ['select convert(varchar, stamp_date, 112) from market..trading_curve_definition where curve_id = ' num2str(varargin{1})]);
dts = floor(datenum(tmp{1}, 'yyyymmdd')) + mod(dts, 1);
opt_def_phases = trading_phase_definition('cotation_group', varargin{2});
eod_hour = opt_def_phases.get('eod_hour');
beod_hour = opt_def_phases.get('bod_hour');

%On retire tous les jours qui ne poss�de pas un fixing d'ouverture et
%un closing de fermeture
days = unique(floor(dts));
oa_days = unique(floor(dts(strcmp('OA',vals(:,3)))));
ca_days = unique(floor(dts(strcmp('CA',vals(:,3)))));
full_days = intersect(oa_days, ca_days);
days_to_be_excluded = setdiff(days, full_days);
for i = 1 : length(days_to_be_excluded)
    st_log(['Warning in read_dataset : excluding day ' datestr(days_to_be_excluded(i)) ' for ' sec_name_ ' as there is either no opening auction or no closing auction']);
    idx = (floor(dts) == days_to_be_excluded(i));
    dts(idx)     = [];
    vals(idx, :) = [];
end

% On va imposer l'horodatage de lopening Auction
idx = strmatch('OA',vals(:,3));
dts(idx,:) = floor(dts(idx,:)) + opt_def_phases.get('oa_hour');
% On va imposer l'horodatage du closing Auction
idx = strmatch('CA',vals(:,3));
dts(idx,:) = floor(dts(idx,:)) + opt_def_phases.get('ca_hour');
% volumes
value = [cellfun(@(x)x,vals(:,1)) cellfun(@(x)x,vals(:,5))]; % turnover et volume
% gestion du march� continu apr�s fermeture du march� continu : n'est
% pas cens� exister
idx = find( (mod(dts,1)>eod_hour) & strcmp(vals(:,3),'C '));
while ~isempty(idx)
    idx = setdiff(idx, idx + 1);
    value(idx-1, :) = value(idx, :) + value(idx-1, :);
    value(idx, :)   = [];
    dts(idx)        = [];
    vals(idx, :)    = [];
    idx = find( (mod(dts,1)>eod_hour) & strcmp(vals(:,3),'C '));
end
% gestion du march� continu avant ouverture march� continu : n'est pas
% cens� exister
idx = find( (mod(dts,1)<beod_hour) & strcmp(vals(:,3),'C '));
while ~isempty(idx)
    idx = setdiff(idx, idx - 1);
    value(idx+1, :) = value(idx, :) + value(idx+1, :);
    value(idx, :)   = [];
    dts(idx)        = [];
    vals(idx, :)    = [];
    idx = find( (mod(dts,1)<beod_hour) & strcmp(vals(:,3),'C '));
end
% On va interdire que deux dates correspondent � la m�me chose:
% En la mati�re j'ai tout vu : deux enregistrement horodat� � la m�me
% heure, deux closing auction dans une m�me journ�e mais � 5 minutes d'intervalle, etc...
%vu ce que l'on a fait ci-dessus, les deux cas explicit�s se ram�nent
%au m�me cas : identifier deux enregistrement horodat�s � la m�me heure
idx = find(diff(dts) == 0);
while ~isempty(idx)
    idx = setdiff(idx, idx - 1);
    value(idx+1, :) = value(idx, :) + value(idx+1, :);
    dts(idx)        = [];
    value(idx, :)   = [];
    vals(idx, :)    = [];
    idx = find(diff(dts) == 0);
end
name = exec_sql(c, ['select name from market..trading_curve_definition where curve_id = ' num2str(varargin{1})]);
rslt = st_data('init', 'title', name{:}, ...
    'date', dts, 'value', value(:, 2), ...
    'colnames', { 'proportion' }, 'plot_type', 'points');
end

function data_ = read_intraday_volumes(opt)
sec_name_ = opt.get('security');
dt_from  = datestr( datenum(opt.get('from'), 'dd/mm/yyyy'), 'yyyymmdd');
dt_to    = datestr( datenum(opt.get('to'), 'dd/mm/yyyy'), 'yyyymmdd');
c    = userdatabase('sybase:j', 'BSIRIUS');
t    = cputime;
vals = exec_sql(c, sprintf( [ 'select turnover, convert(varchar, date, 112), phase, '...
    'convert(varchar, end_time, 108), volume '...
    'from tick_db..trading_intraday_volume '...
    ' where security_id = %d '...
    '   and (date between ''%s'' and ''%s'') '...
    ' and isnull(trading_destination_id ,-1)<=50'...
    ' order by date, end_time, phase'], get_sec_id_from_ric(sec_name_), dt_from, dt_to));
t = cputime -t;
st_log('done %3.2f sec...\n', t);
if isempty(vals)
    data_ = [];
    return;
end
% pas de trading at last
vals(strmatch('TA',vals(:,3)),:) = [];
% conversion des dates GMT
dts = datenum(cellfun(@(x,y)[x ' ' y], vals(:,2), vals(:,4),'uni',false),'yyyymmdd HH:MM:SS');
dts = gmt('dec-fr', dts);
opt_def_phases = trading_phase_definition(opt.get());
eod_hour  = opt_def_phases.get('eod_hour');
beod_hour = opt_def_phases.get('bod_hour');


if (strcmp(opt.get('data_filter'), 'on'))
    %On retire tous les jours qui ne poss�dent pas un fixing d'ouverture et
    %un closing de fermeture � l'heure habituelle (histoire de retirer
    % les jours incomplets, mais aussi les demi-journ�es comme le 24/12)
    days = unique(floor(dts));
    oa_days = unique(floor(dts(strcmp('OA',vals(:,3)))));
    ca_days = unique(floor(dts(strcmp('CA',vals(:,3)))));
    full_days = intersect(oa_days, ca_days);
    days_to_be_excluded = setdiff(days, full_days);
    for i = 1 : length(days_to_be_excluded)
        st_log(['Warning in read_dataset : excluding day ' datestr(days_to_be_excluded(i)) ' for ' sec_name_ ' as there is either no opening auction or no closing auction']);
        idx = (floor(dts) == days_to_be_excluded(i));
        dts(idx)     = [];
        vals(idx, :) = [];
    end
    
    % Les interruptions de s�ance en cours de journ�e suivies d'un fixing
    % sont difficiles � prendre en compte, tant que nous n'avons pas de
    % mod�le sp�cifique pour les fixing. Dans ces conditions, les journ�es
    % avec fixing en milieu de journ�e seront exclues
    
    
    idx = (strcmp('OA',vals(:,3)) & (mod(dts,1)>beod_hour+datenum(0,0,0,0,1,0))) ...
        | (strcmp('CA',vals(:,3)) & (mod(dts,1)<eod_hour-datenum(0,0,0,0,1,0)));
    days_to_be_excluded = unique(floor(dts(idx)));
    for i = 1 : length(days_to_be_excluded)
        st_log(['Warning in read_dataset : excluding day ' datestr(days_to_be_excluded(i)) ' for ' sec_name_ ' as there is an auction in the continuous phase']);
        idx = (floor(dts) == days_to_be_excluded(i));
        dts(idx)     = [];
        vals(idx, :) = [];
    end
else
    %On rajoute un fixing d'ouverture et un fixing de fermeture avec
    %un volume nul pour tous les jours n'en poss�dant pas
    days    = unique(floor(dts));
    oa_days = unique(floor(dts(strcmp('OA',vals(:,3)))));
    days_without = setdiff(days, oa_days);
    for i = 1 : length(days_without)
        idx = find(floor(dts) == days_without(i), 1, 'first');
        tmp_hour = floor(dts(idx)) + opt_def_phases.get('oa_hour');
        dts      = [dts(1:(idx - 1)); tmp_hour; dts(idx:end)];
        if (idx ~= 1)
            tmp = vertcat(vals(1:(idx - 1), :), {0, datestr(tmp_hour, 'yyyymmdd'), 'OA', datestr(tmp_hour, 'HH:MM:SS'), 0});
        else
            tmp = {0, datestr(tmp_hour, 'yyyymmdd'), 'OA', datestr(tmp_hour, 'HH:MM:SS'), 0};
        end
        vals    = vertcat(tmp, vals(idx:end, :));
    end
    ca_days = unique(floor(dts(strcmp('CA',vals(:,3)))));
    days_without = setdiff(days, ca_days);
    for i = 1 : length(days_without)
        idx = find(floor(dts) == days_without(i), 1, 'last');
        tmp_hour = floor(dts(idx)) + opt_def_phases.get('ca_hour');
        dts      = [dts(1:idx); tmp_hour; dts(idx + 1:end)];
        tmp      = vertcat(vals(1:idx, :), {0, datestr(tmp_hour, 'yyyymmdd'), 'CA', datestr(tmp_hour, 'HH:MM:SS'), 0});
        if (idx ~= length(dts))
            vals = vertcat(tmp, vals(idx + 1:end, :));
        else
            vals = tmp;
        end
    end
    %Si l'on veut juste les consid�rer comme si c'�tait du march� continu :
    idx = (strcmp('OA',vals(:,3)) & (mod(dts,1)>beod_hour+datenum(0,0,0,0,1,0))) ...
        | (strcmp('CA',vals(:,3)) & (mod(dts,1)<eod_hour-datenum(0,0,0,0,1,0)));
    vals(idx,3) = {'C'};
end

% On va imposer l'horodatage de lopening Auction
idx = strmatch('OA',vals(:,3));
dts(idx,:) = floor(dts(idx,:)) + opt_def_phases.get('oa_hour');
% On va imposer l'horodatage du closing Auction
idx = strmatch('CA',vals(:,3));
dts(idx,:) = floor(dts(idx,:)) + opt_def_phases.get('ca_hour');
% volumes
value = [cellfun(@(x)x,vals(:,1)) cellfun(@(x)x,vals(:,5))]; % turnover et volume
% gestion du march� continu apr�s fermeture du march� continu : n'est
% pas cens� exister
idx = find( (mod(dts,1)>eod_hour) & strcmp(vals(:,3),'C '));
while ~isempty(idx)
    idx = setdiff(idx, idx + 1);
    value(idx-1, :) = value(idx, :) + value(idx-1, :);
    value(idx, :)   = [];
    dts(idx)        = [];
    vals(idx, :)    = [];
    idx = find( (mod(dts,1)>eod_hour) & strcmp(vals(:,3),'C '));
end
% gestion du march� continu avant ouverture march� continu : n'est pas
% cens� exister
idx = find( (mod(dts,1)<beod_hour) & strcmp(vals(:,3),'C '));
while ~isempty(idx)
    idx = setdiff(idx, idx - 1);
    value(idx+1, :) = value(idx, :) + value(idx+1, :);
    value(idx, :)   = [];
    dts(idx)        = [];
    vals(idx, :)    = [];
    idx = find( (mod(dts,1)<beod_hour) & strcmp(vals(:,3),'C '));
end
% On va interdire que deux dates correspondent � la m�me chose:
% En la mati�re j'ai tout vu : deux enregistrement horodat� � la m�me
% heure, deux closing auction dans une m�me journ�e mais � 5 minutes d'intervalle, etc...
%vu ce que l'on a fait ci-dessus, les deux cas explicit�s se ram�nent
%au m�me cas : identifier deux enregistrement horodat�s � la m�me heure
idx = find(diff(dts) == 0);
while ~isempty(idx)
    idx = setdiff(idx, idx - 1);
    value(idx+1, :) = value(idx, :) + value(idx+1, :);
    dts(idx)        = [];
    value(idx, :)   = [];
    vals(idx, :)    = [];
    idx = find(diff(dts) == 0);
end
if (opt.get('volume only'))
    data_ = st_data('init', 'title', [sec_name_ ' - historique'], ...
        'date', dts, 'value', value(:, 2), ...
        'colnames', { 'volume' }, 'plot_type', 'points');
elseif (opt.get('turnover only'))
    data_ = st_data('init', 'title', [sec_name_ ' - historique'], ...
        'date', dts, 'value', value(:, 1), ...
        'colnames', { 'turnover' }, 'plot_type', 'points');
else
    data_ = st_data('init', 'title', [sec_name_ ' - historique'], ...
        'date', dts, 'value', value, ...
        'colnames', { 'turnover', 'volume' }, 'plot_type', 'points');
end
data_.info.market = opt_def_phases.get('market');
data_.info.cotation_group = opt_def_phases.get('cotation_group');
end

function data = check_data_length(data, mode, opt)
if isempty(data)
    return;
end
opt.set('nb_attempts', opt.get('nb_attempts') + 1);
if ( opt.get('nb_attempts') <= 5)
    target_nb_days = opt.get('fixed_nb_days');
    if (target_nb_days > 0)
        days = unique(floor(data.date));
        nb_days = length(days);
        if (nb_days < target_nb_days)
            opt.set('from', datestr(datenum(opt.get('from'), 'dd/mm/yyyy') - 1.5 * (target_nb_days - nb_days) - 7, 'dd/mm/yyyy'));
            lst = opt.get();
            data = old_read_dataset(mode, lst{:});
        elseif (nb_days > target_nb_days)
            idx = find(floor(data.date) == days(end - target_nb_days), 1, 'last');
            data.value(1:idx, :) = [];
            data.date(1:idx, :)  = [];
        end
    end
else
    st_log(['Error in read_dataset: After five attempts, there is not enough data for ' opt.get('security')]);
    st_log([' As a consequence, you will only get ' num2str(length(unique(floor(data.date)))) ' days']);
end
end

%<* Date to sydate
function dt = DATE2SYBASE( dt)
dt = dt([7:10 4:5 1:2]);
end
%>*
