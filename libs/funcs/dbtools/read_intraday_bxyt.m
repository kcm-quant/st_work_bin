function [table_intraday, st_data_intraday] = read_intraday_bxyt(security_id, td_id, start_date, end_date)

% READ_TRADING_DAILY - Loading BXYT 5-min data
% Example:
% [table_intraday, st_data_intraday] = read_intraday_bxyt(110, 1, '2021-06-01', '2021-06-01');
% remark: consolidated : td_id = 0
%%

if isempty(td_id)
    td_id_use = 0;
elseif isnumeric(td_id)
    td_id_use = td_id;
elseif ismember(td_id, {'main', 'MAIN'})
    td_id_use = get_repository('maintd', security_id);
end

%%
    if count(py.sys.path, 'C:\st_sim\bin\libs\bxyt') == 0
        insert(py.sys.path, int32(0), 'C:\st_sim\bin\libs\bxyt');
    end

    intraday_data = py.get_intraday_bxyt.intraday_data(security_id, td_id_use, start_date, end_date);
    
    if length(intraday_data)==0
        table_intraday = [];
        st_data_intraday = [];
    else
        st = struct(intraday_data.to_dict());
        inStruct = structfun( @(x)py.list(x.values), st, 'UniformOutput', false);

        fields = fieldnames(inStruct);
        out = struct();
        for f = 1:numel(fields)
            fld = fields{f};
            data_value = cellstr(string(cell(inStruct.(fld))))';
            if strcmp(fld, 'date')
                out.(fld) = datetime(data_value);
            else
                out.(fld) = str2double(data_value);
            end
        end

        table_intraday      = struct2table(out);
        st_data_intraday    = st_data( 'init', 'title', 'BXYT 5min data', 'value', table_intraday{:, 2:end}, ...
                          'date', datenum(table_intraday.date), 'colnames', table_intraday.Properties.VariableNames(2:end));
    end
end