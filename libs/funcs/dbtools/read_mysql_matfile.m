function res = read_mysql_matfile(query)
% READ_MYSQL_MATFILE - Chargement de donn�es depuis la base MySql
% La colonne "File" est convertie automatiquement sous forme de st_data.
%
% Tests :
% res = read_mysql_matfile(['select TradeDate,SecurityId,File from QuantMatFile',...
%     ' where SecurityId = 110 and TradeDate between ''2018-02-01'' and ''2018-02-06''']);

colnames = regexp(query,'(?<=select +).*(?= +from)','match','once');
colnames = cellstr(regexp(colnames, ' *, *', 'split'));
file_col_num = find(strcmp(colnames, 'File'));

res = exec_sql('Matlab',query);
if isempty(res)
    res = reshape(res,[],length(colnames));
    return
end
if ~isempty(file_col_num)
    res(:,file_col_num) = cellfun(@mysql_data2st_data,res(:,file_col_num),'uni',false);
    keep_col_num = setdiff(1:size(res,2),file_col_num);
    try
        meta_data = cell2struct(res(:,keep_col_num)',colnames(keep_col_num));
    catch ME
        if strcmp(ME.identifier, 'MATLAB:Cell2Struct:InvalidFieldName')
            % Gestion des espaces
            colnames = strrep(colnames, ' ', '_');
            % Gestion des parenth�ses
            colnames = regexprep(colnames, '([\(|\)](?!\)*$))+', '_');
            colnames = regexprep(colnames, '\)+$', '');
            meta_data = cell2struct(res(:,keep_col_num)',colnames(keep_col_num));
        else
            rethrow(ME)
        end
    end
    for i = 1:size(res,1)
        if isempty(res{i,file_col_num})
            res{i,file_col_num} = st_data('empty-init');
        end
        res{i,file_col_num}.info.mysql_meta_data = meta_data(i);
    end
end
