function data = read_trading_daily(sec_list, start_date, end_date, varargin)
% READ_TRADING_DAILY - Reads data from table trading_daily
%
% This function reads the data from the SQL table
% MARKET_DATA..trading_daily. It can adjust the data for split or other
% corporate actions and convert the data in euro.
% The function can also fetch data for the main destination and compute
% consolidated data on desired a list of trading_destination_id.
%
%   Mandatory input variables:
%       * sec_list : a vector of security_id;
%       * start_date and end_date : beginning and end dates in datenum.
%
%   Optional input variables ("key, value" pairs):
%       * field : a cellarray of trading_daily fields to include or a str
%       split by ';'
%       * trading_destinations : a cellarray of trading_destination_id to
%                                to include. Main destination is refered as
%                                'MAIN'. The SQL table KGR..EXCHANGE_REFCOMPL
%                                provides the mapping between the
%                                trading_destination_id and the operating
%                                mic (see columns trading_destination_id
%                                and OPERATINGMIC).
%       * compute_null (default false) : flag (true or false). If true,
%                                        read_trading_daily computes 
%                                        the consolidated data which are
%                                        added as new lines with
%                                        trading_destination_id = NaN.
%       * convert_in_euro (default true) : flag for conversion from the
%                                          quotation currency to euro. If
%                                          true the following columns are
%                                          converted to euro:
%                                          turnover,open_prc,high_prc,
%                                          low_prc,close_prc,open_turnover,
%                                          close_turnover,intraday_turnover,
%                                          cross_turnover,auction_turnover,
%                                          dark_turnover,midclose_turnover,
%                                          end_turnover,off_turnover,
%                                          last_before_close_prc,
%                                          qty_1er_limit,tick_size,
%                                          periodic_turnover.
%       * split_adj (default false) : flag for split adjustment.
%       * output (default 'table') : If 'table', the output is a Matlab
%                                    table; if 'st_data', the output is an
%                                    "st_data".
%
%   Output:
%       The output is by default a Matlab table or an "st_data" (see option
%       "output" with one column for each asked field (see option "field"),
%       plus security_id, date, trading_destination_id and is_prim. The
%       output has one line per triplet (date, security_id, 
%       trading_destination_id). The consolidated data are added in lines
%       with trading_destination_id = NaN.
%       The "is_prim" column, either 0 or 1, indicates whether the
%       current line corresponds to the main destination.
%
% Example
% data = read_trading_daily([2, 110], datenum(2021, 1,1), datenum(2021, 2,8),...
%   'compute_null', true, 'split_adj', false);

% CONFIG FILE
functionFilePath = which('read_trading_daily.m');
parts = strsplit(functionFilePath, filesep);
repository = fullfile(parts{1:end-1});
config_file = [repository '\consolidated_data_config.json']; %C:\st_work\bin\libs\funcs\dbtools\

opt = options({'field', {'turnover', 'close_prc'}, 'trading_destinations',...
    {'MAIN', 17, 34, 159, 183, 189, 288, 289}, 'compute_null', false,...
    'convert_in_euro', true, 'split_adj', false, 'output', 'table'},...
    varargin);

field = opt.get('field');
if ischar(field)
    field = tokenize(field, ';');
end
    
out_field = field;
if opt.get('compute_null')
    conf = load_json(config_file);
    fn = intersect(fieldnames(conf), out_field);
    aux_field = cell(length(fn), 1);
    for f = 1:length(fn)
        if isfield(conf.(fn{f}), 'formula')
            dec = decode_formula(conf.(fn{f}).formula);
            aux_field{f} = regexp(dec, '(?<=#)[^#@]+(?=@)', 'match');
        end
    end
    field = setdiff(union(out_field, horzcat(aux_field{:})), 'is_prim');
else
    field = out_field;
end

td = opt.get('trading_destinations');

td_str = join(td(~strcmp(td, 'MAIN')));
if opt.get('compute_null') || ismember('MAIN', td(cellfun(@ischar, td)))
    pm = get_histo_primary_mkt(sec_list);
    pm = rename(pm, 'trading_destination_id', 'prim_id');
    pm{end+1:end+length(sec_list), :} = [reshape(sec_list,[], 1),...
        nan(length(sec_list),1), zeros(length(sec_list), 2)];
    if isempty(td_str)
        td_str = join(unique(pm.prim_id(~isnan(pm.prim_id))));
    else
        td_str = strcat(td_str, ',', join(unique(pm.prim_id(~isnan(pm.prim_id)))));
    end
end

sec_str = join(sec_list);
fld_str = join(field);

if ~isnan(opt.try_get('source_table', nan)) % Undocumented feature!
    q = sprintf("select date, security_id, trading_destination_id, %s", fld_str)+...
        sprintf(" from %s",opt.get('source_table'))+...
        sprintf(" where security_id in (%s)", sec_str)+...
        sprintf(" and date between '%s'", datestr(start_date, 'yyyy-mm-dd'))+...
        sprintf(" and '%s'", datestr(end_date, 'yyyy-mm-dd'))+...
        sprintf(" and trading_destination_id in (%s)", td_str);
else
    q = sprintf("select date, security_id, trading_destination_id, %s", fld_str)+...
        " from MARKET_DATA..trading_daily"+...
        sprintf(" where security_id in (%s)", sec_str)+...
        sprintf(" and date between '%s'", datestr(start_date, 'yyyy-mm-dd'))+...
        sprintf(" and '%s'", datestr(end_date, 'yyyy-mm-dd'))+...
        sprintf(" and trading_destination_id in (%s)", td_str);
end 
data = exec_sql_table('MARKET_DATA', q);
data.date = datenum(data.date, 'yyyy-mm-dd');

if opt.get('split_adj')
   % data = split_adjustment(data);
   fprintf('split_adj works only for st_data\n')
end
if opt.get('convert_in_euro')
	data = fx_adjustment(data);
end
if opt.get('compute_null') || ismember('MAIN', td(cellfun(@ischar, td)))    
    key = sortrows([data{:, {'security_id', 'date'}}, (1:height(data))', nan(height(data), 1);...
        pm{:, {'security_id', 'begin_date'}}, zeros(height(pm), 1), (1:height(pm))']);
    key(:, end) = previous_not_nan(key(:, end));
    try
        val = pm(key(key(:, end-1)~=0, end), :);
    catch
        error('read_trading_daily:unable_to_find_primary',['There is',...
            ' data in trading_daily for before stock''s ',...
            'HISTO_SECURITY_QUANT begin_date.\n',...
            'Correct the referential or ask data with a more recent starting date.'])
    end
    val.id = key(key(:, end-1)~=0, end-1);
    val = sortrows(val, 'id');
    val{val.security_id~=data.security_id, :} = nan;
    % val{val.end_date<=data.date, :} = nan;
    data.is_prim = data.trading_destination_id==val.prim_id;
end
if iscell(td) & length(td)==1 
if strcmpi(td{1},'MAIN')
    data = data(data.is_prim,:);
end
end
if opt.get('compute_null')    
    conf = load_json(config_file);
    [conso_data, ~, idx] = unique(data(:, {'security_id', 'date'}), 'rows');
    conso_data{:, 'trading_destination_id'} = nan;
    fn = intersect(fieldnames(conf), data.Properties.VariableNames);
    for i = 1:length(fn)
        conso_data.(fn{i}) = aggregate(idx, data, fn{i}, conf.(fn{i}));
    end
    conso_data{:, 'is_prim'} = false;
    data = [data; conso_data];
end

data = data(:, [{'date', 'security_id', 'trading_destination_id'}, out_field]);
data = sortrows(data, {'date', 'security_id', 'trading_destination_id'});
if strcmp(opt.get('output'), 'st_data')
    data = table2st_data(data);
    if opt.get('split_adj')
%         data = st_data('cac', data,'ignore-unknown-indic');
        data = st_data('trading_daily_cac', data);
    end
end

function s = join(x)

if isempty(x) 
    s='';
    return
end

if isnumeric(x)
    s = sprintfc('%d', x);
    s = sprintf('%s,', s{:});
    s(end) = [];
    return
end
is_num = cellfun(@isnumeric, x);
if all(is_num)
    s = join(cell2mat(x));
elseif all(~is_num)
    s = sprintf('%s,', x{:});
    s(end) = [];
else
    s = strcat(join(x(is_num)), ',', join(x(~is_num)));
end

function histo_prim = get_histo_primary_mkt(sec_list)
sec_str = join(sec_list);
q = "with t as (select security_id, trading_destination_id=primary_trading_destination_id,"+...
    "begin_date, end_date,lag_td_id = lag(primary_trading_destination_id, 1) over(partition by security_id order by begin_date),"+...
    "lead_td_id = lead(primary_trading_destination_id, 1) over(partition by security_id order by begin_date)"+...
    " from KGR..HISTO_SECURITY_QUANT"+...
    sprintf(" where security_id in (%s))", sec_str)+...
    " select security_id, trading_destination_id, date=begin_date,type='begin_date'"+...
    " from t"+...
    " where (trading_destination_id!=lag_td_id or lag_td_id is null)"+...
    " union all"+...
    " select security_id, trading_destination_id, date=end_date,type='end_date'"+...
    " from t"+...
    " where (trading_destination_id!=lead_td_id or lead_td_id is null)"+...
    " order by security_id, date, type desc";

histo_prim = exec_sql_table('KGR', q);
histo_prim.date = datenum(histo_prim.date, 'yyyy-mm-dd');
histo_prim = sortrows(histo_prim, {'security_id', 'date', 'trading_destination_id'});
temp = histo_prim(strcmp(histo_prim.type, 'begin_date'), {'security_id', 'trading_destination_id', 'date'});
temp.Properties.VariableNames{strcmp(temp.Properties.VariableNames, 'date')} = 'begin_date';
histo_prim = [temp, histo_prim(strcmp(histo_prim.type, 'end_date'), 'date')];
histo_prim.Properties.VariableNames{strcmp(histo_prim.Properties.VariableNames, 'date')} = 'end_date';

function t = rename(t, old_name, new_name)
t.Properties.VariableNames{strcmp(t.Properties.VariableNames, old_name)} = new_name;

function d=load_json(filename)
fid = fopen(filename);
str = fread(fid,inf,'*char')';
fclose(fid); 
d = jsondecode(str);

function v = aggregate(idx, data, name, s)
if isfield(s, 'function')
    v = accumarray(idx, data.(name), [], str2func(s.function), nan);
elseif isfield(s, 'formula')
    frml = decode_formula(s.formula);
    vname = regexp(frml, '(?<=#)[^#@]+(?=@)', 'match');
    temp = regexp(frml, '#[^#@]+@', 'split');
    temp(1:end-1) = cellfun(@(x, i)strcat(x, num2str(i)),...
        temp(1:end-1), num2cell(1:length(temp)-1),'uni', false);
    frml = strcat(temp{:});
    val = data{:, vname}; %#ok<NASGU>
    v = accumarray(idx, (1:length(idx))', [], eval(strcat('@(i){', frml, '}')));
    v(cellfun('isempty', v)) = {nan};
    assert(all(cellfun('length', v)==1), 'Formula <%s> returned a on-scalar value', s.formula)
    v = cell2mat(v);
end

function sdata = table2st_data(data)
sdata = struct();
sdata.title = 'Read trading daily';
sdata.colnames = setdiff(data.Properties.VariableNames, 'date','stable');
sdata.value = data{:, setdiff(data.Properties.VariableNames, 'date','stable')};
sdata.date = data.date;
sdata.info = struct('security_id', {unique(data.security_id)},...
    'data_log', '', 'td', []);