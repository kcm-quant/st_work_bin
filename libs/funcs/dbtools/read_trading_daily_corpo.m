function data_daily = read_trading_daily_corpo(varargin)
% READ_TRADING_DAILY - lecture de la table MARKET_DATA..trading_daily
%
% data = read_trading_daily_corpo( 'source', 'index', 'code', 'CAC40', 'fields', ...
%              'volume;intraday_volume', 'from', '2008/12/01', 'to', '2008/12/11' )
%
% data = read_trading_daily_corpo( 'source', 'security', 'code', 110, 'fields', ...
%              'volume;intraday_volume', 'from', '2008/12/01', 'to', '2008/12/11' , 'trading-destinations', {'MAIN'} )
%
% data = read_trading_daily_corpo( 'source', 'security', 'code', 110, 'fields', ...
%              'volume;intraday_volume', 'from', '01/12/2008', 'to', '31/12/2008', 'date-format', 'dd/mm/yyyy' )
%
% data = read_trading_daily_corpo( 'source', 'security', 'code', 110, 'fields', ...
%              'volume;intraday_volume', 'from', '2008/12/01', 'to', '2008/12/11', 'trading-destinations', {'CHI-X'} )
%
% [data_v, data_iv] = read_trading_daily_corpo( 'source', 'index', 'code', 'CAC40', 'fields', ...
%              'volume;intraday_volume', 'from', '2008/12/01', 'to', '2008/12/11', ...
%              'format', 'by-field' )
%
% Options :
% - 'td_null' : does not exist anymore
% - 'td_main_mtfs' : does not exist anymore
% - 'trading-destinations': {''}(default)/ci-dessous
%       {'MAIN'} 
%       ou {'Chi-X'} ou {'Turquoise'} ou {'Bats Europe'} ou {'Chi-X','Turquoise','Bats Europe'} 
%       ou {'MAIN-MTFS'}
%       ou {'NULL'}
%
%
% 'Chi-X' - 'CHIX' - trading_destination_id = 159
% 'Bats Europe' - 'BATE' - trading_destination_id = 189
% 'Turquoise' - 'TRQSE' - trading_destination_id = 183
% WARNING: Equiduct has been removed automatically from 'MAIN', 'MAIN-MTFS
% options
%
%
%

opt = options( { 'source', 'index', 'code', 'CAC40', 'fields', 'turnover;volume;close_prc', ...
    'from', '2021/01/01', 'to', '2021/01/31', 'date-format', 'yyyy/mm/dd', ...
    'trading-destinations', {'NULL'}, 'data_in_euros', false, 'cac', true}, varargin);

if strfind(opt.get('from'),'/')~= strfind(opt.get('date-format'),'/')
    opt = options( { 'source', 'index', 'code', 'CAC40', 'fields', 'volume;intraday_volume', ...
        'from', '01/12/2008', 'to', '31/12/2008', 'date-format', 'dd/mm/yyyy', ...
        'trading-destinations', {'NULL'}, 'data_in_euros', false, 'cac', false}, varargin);
end

%% Requets writing: common part
code      = opt.get('code');
dt_format = opt.get('date-format');
dt_from   = datestr( datenum( opt.get('from'), dt_format), 'yyyy-mm-dd');
dt_to     = datestr( datenum( opt.get('to'), dt_format), 'yyyy-mm-dd');

% récupération des champs
field       = opt.get('fields');
source      = opt.get('source');

param_main = {'field', field, 'convert_in_euro', opt.get('data_in_euros'), 'split_adj', opt.get('cac'), 'output', 'st_data'};


td_names    = opt.get('trading-destinations');
switch td_names{1}
    case 'NULL'
        param_add = {'compute_null', true};
    case 'MAIN'
        param_add = {'trading_destinations', {'MAIN'}};
    case {'MAIN-MTFS', ''}
        param_add = {'MAIN', 17, 34, 159, 183, 189, 288, 289}; % without Aquis
    case {'Chi-X', 'CHIX'}
        param_add = {'trading_destinations', {159, 288}};
    case {'Bats Europe', 'BATE'}
        param_add = {'trading_destinations', {189}};
    case {'Turquoise', 'TRQSE'}
        param_add = {'trading_destinations', {183, 289}};
    case {'Aquis', 'AQUIS'}
        param_add = {'trading_destinations', {276, 286}};
    otherwise
        if isnumeric([td_names{:}]) && isvector([td_names{:}])
            param_add = {'trading_destinations', td_names};
        else
            error('read_trading_daily_corpo:trading_destinations', 'no matching trading destination id found...');
        end
end


switch source
    case 'security'
        data_daily = read_trading_daily(code, dt_from, dt_to, param_main{:}, param_add{:});
    case 'index'
        sec_ids    = get_repository('index-comp', code);
        data_daily = read_trading_daily(sec_ids, dt_from, dt_to, param_main{:}, param_add{:});
    otherwise
       error('read_trading_daily_corpo:source', 'no matching source found...');
 
end

data_daily = st_data( 'remove', data_daily, 'is_prim');
if strcmp(td_names{1}, 'NULL')
    data_daily = st_data('from-idx', data_daily,  isnan(st_data('cols', data_daily, 'trading_destination_id')));
end



end