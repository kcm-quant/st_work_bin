function varargout = read_trading_daily_legacy( varargin)
% READ_TRADING_DAILY - lecture de la table MARKET_DATA..trading_daily
%
% data = read_trading_daily_legacy( 'source', 'index', 'code', 'CAC40', 'fields', ...
%              'volume;intraday_volume', 'from', '2008/12/01', 'to', '2008/12/11' )
%
% data = read_trading_daily_legacy( 'source', 'security', 'code', 110, 'fields', ...
%              'volume;intraday_volume', 'from', '2008/12/01', 'to', '2008/12/11' , 'trading-destinations', {'MAIN'} )
%
% data = read_trading_daily_legacy( 'source', 'security', 'code', 110, 'fields', ...
%              'volume;intraday_volume', 'from', '01/12/2008', 'to', '31/12/2008', 'date-format', 'dd/mm/yyyy' )
%
% data = read_trading_daily_legacy( 'source', 'security', 'code', 110, 'fields', ...
%              'volume;intraday_volume', 'from', '2008/12/01', 'to', '2008/12/11', 'trading-destinations', {'CHI-X'} )
%
% [data_v, data_iv] = read_trading_daily_legacy( 'source', 'index', 'code', 'CAC40', 'fields', ...
%              'volume;intraday_volume', 'from', '2008/12/01', 'to', '2008/12/11', ...
%              'format', 'by-field' )
%
% Options :
% - 'td_null' : does not exist anymore
% - 'td_main_mtfs' : does not exist anymore
% - 'trading-destinations': {''}(default)/ci-dessous
%       {'MAIN'} 
%       ou {'Chi-X'} ou {'Turquoise'} ou {'Bats Europe'} ou {'Chi-X','Turquoise','Bats Europe'} 
%       ou {'MAIN-MTFS'}
%       ou {'NULL'}
%
%
% 'Chi-X' - 'CHIX' - trading_destination_id = 159
% 'Bats Europe' - 'BATE' - trading_destination_id = 189
% 'Turquoise' - 'TRQSE' - trading_destination_id = 183
% WARNING: Equiduct has been removed automatically from 'MAIN', 'MAIN-MTFS
% options
%
%
%

opt = options( { 'source', 'index', 'code', 'CAC40', 'fields', 'volume;intraday_volume', ...
    'colnames', '', 'from', '2008/12/01', 'to', '2008/12/31', 'date-format', 'yyyy/mm/dd', ...
    'format', 'plain', 'trading-destinations', {'NULL'}, ...
    'td_null', false, 'td_main_mtfs', false,'trading_destination_id',nan,...
    'add_rate_to_euro', false, 'data_in_euros', false, 'force_order', false,...
    'cac', true, 'group_by_date', false, 'group_by_date_func', 'sum'}, varargin);
if strfind(opt.get('from'),'/')~= strfind(opt.get('date-format'),'/')
    opt = options( { 'source', 'index', 'code', 'CAC40', 'fields', 'volume;intraday_volume', ...
        'colnames', '', 'from', '01/12/2008', 'to', '31/12/2008', 'date-format', 'dd/mm/yyyy', ...
        'format', 'plain', 'trading-destinations', {'NULL'}, ...
        'td_null', false, 'td_main_mtfs', false,'trading_destination_id',nan,...
        'add_rate_to_euro', false, 'data_in_euros', false, 'force_order', false,...
        'cac', false, 'group_by_date', false, 'group_by_date_func', 'sum'}, varargin);
end

%% Requets writing: common part
code      = opt.get('code');
dt_format = opt.get('date-format');
dt_from   = datestr( datenum( opt.get('from'), dt_format), 'yyyymmdd');
dt_to     = datestr( datenum( opt.get('to'), dt_format), 'yyyymmdd');
FR_dt_from = datestr( datenum( opt.get('from'), dt_format), 'dd/mm/yyyy');
FR_dt_to = datestr( datenum( opt.get('to'), dt_format), 'dd/mm/yyyy');
% r�cup�ration des champs
fields    = opt.get('fields');
get_fldk  = tokenize( fields, ';');
if opt.get('group_by_date')
    t = [cell(size(get_fldk')), get_fldk', get_fldk'];
    t(:,1) = {opt.get('group_by_date_func')};
    t = reshape(t', 1, []);
    get_flds  = sprintf('%s(trd.%s) as %s, ', t{:});
else
    get_flds  = sprintf('trd.%s, ', get_fldk{:});
end
get_flds(end-1:end) = '';
% cr�ation des noms de champs
colnames  = tokenize(opt.get('colnames'), ';');
if isempty( colnames)
    colnames = get_fldk;
end
colnames = strtrim( colnames);
source = opt.get('source');
if opt.get('data_in_euros')
    table_name_compl = '_euro';
else
    table_name_compl = '';
end

%< gestion de la zone
switch lower(source)
    case 'index'
        table_name = ['trading_daily_rebuilt' table_name_compl];
    case {'security','security_histo'}
        if (length(code) == 1)
            tdinfo = get_repository('tdinfo', code);
            table_name = sprintf('trading_daily_rebuilt%s%s', table_name_compl, tdinfo(1).global_zone_suffix);
        else
            fprintf('Warning: several securities, taking global_zone_suffix from the first security.\n')
            tdinfo = get_repository('tdinfo', code(1));
            table_name = sprintf('trading_daily_rebuilt%s%s', table_name_compl, tdinfo(1).global_zone_suffix);
        end
end
%>

%< gestion des tds
if opt.get('td_null')
    error('read_trading_daily_legacy:td_null', 'td_null option replaced by ''trading-destinations'' {''NULL''}.')
end
if opt.get('td_main_mtfs')
        error('read_trading_daily_legacy:td_main_mtfs', 'td_main_mtfs option replaced by ''trading-destinations'' {''MAIN-MTFS''}.')
end
if all(~isnan(opt.get('trading_destination_id')))
        error('read_trading_daily_legacy:trading_destination_id', 'trading_destination_id option replaced by ''trading-destinations'' [a,b,...,n].')
end

td_names = opt.get('trading-destinations');
if strcmpi(td_names{1}, 'MAIN')
    if length(td_names)>1
        error('read_trading_daily_legacy:trading_destinations', 'if you ask for MAIN, ask for only MAIN, or option ''td_main_mtfs''...');
    end
    td_str = sprintf([' inner join KGR..security_market sm on sm.security_id = tt.security_id '...
        ' inner join MARKET_DATA..%s trd on trd.security_id=sm.security_id and case when trd.trading_destination_id = 51 then 18 else trd.trading_destination_id end = sm.trading_destination_id '...
        ' where sm.ranking = 1 and trd.trading_destination_id <>185 and trd.trading_destination_id is not null '...
        '  '],table_name);
      
elseif strcmpi(td_names{1}, 'MAIN-MTFS')
    td_main = cell2mat(exec_sql('KGR', ...
        sprintf('select distinct trading_destination_id from KGR..security_market where ranking = 1 ')));
    td_ids = [td_main; 159; 189; 183]; 
    td_ids = sprintf('%d,', td_ids);
    td_ids(end) = [];

    td_str = sprintf([' inner join MARKET_DATA..%s trd on trd.security_id = tt.security_id '...
        ' where trd.trading_destination_id <>185 and trd.trading_destination_id in (%s) and trd.trading_destination_id is not null '],table_name,td_ids); 
    
elseif strcmpi(td_names{1}, 'NULL')
    td_str = sprintf([' inner join MARKET_DATA..%s trd on trd.security_id = tt.security_id '...
        ' where trd.trading_destination_id is null '],table_name);

elseif strcmpi(td_names{1}, 'ALL')
    td_str = sprintf([' inner join MARKET_DATA..%s trd on trd.security_id = tt.security_id '...
        ' where trd.trading_destination_id is not null '],table_name);   

elseif strcmpi(td_names{1}, 'Chi-X') || strcmpi(td_names{1}, 'CHIX')    
    td_ids = '159';
    td_str = sprintf([' inner join MARKET_DATA..%s trd on trd.security_id = tt.security_id '...
        ' where trd.trading_destination_id in (%s) and trd.trading_destination_id is not null '], table_name, td_ids);

elseif strcmpi(td_names{1}, 'Bats Europe') || strcmpi(td_names{1}, 'BATE')
    td_ids = '189';
    td_str = sprintf([' inner join MARKET_DATA..%s trd on trd.security_id = tt.security_id '...
        ' where trd.trading_destination_id in (%s) and trd.trading_destination_id is not null '], table_name, td_ids); 
    
elseif strcmpi(td_names{1}, 'Turquoise') || strcmpi(td_names{1}, 'TRQSE')
    td_ids = '183';
    td_str = sprintf([' inner join MARKET_DATA..%s trd on trd.security_id = tt.security_id '...
        ' where trd.trading_destination_id in (%s) and trd.trading_destination_id is not null '], table_name, td_ids);     

elseif isnumeric([td_names{:}]) && isvector([td_names{:}])
    td_ids = sprintf('%d,', [td_names{:}]);
    td_ids(end) = [];
    td_str = sprintf([' inner join MARKET_DATA..%s trd on trd.security_id = tt.security_id '...
        ' where trd.trading_destination_id in (%s) and trd.trading_destination_id is not null '], table_name, td_ids);      
    
% elseif isvector(td_names)
%     td_ids = sprintf('%d,', td_names);
%     td_ids = sprintf('%d,', td_ids);
%     td_ids(end) = [];
%     td_str = sprintf([' inner join MARKET_DATA..%s trd on trd.security_id = tt.security_id '...
%         ' where trd.trading_destination_id in (%s) and trd.trading_destination_id is not null '], table_name, td_ids);

else
    td_ids = {};
    for t=1:length(td_names)
        td_ids = cat(1, td_ids, ...
            exec_sql('KGR', ...
            sprintf('select trading_destination_id from KGR..trading_destination where name like ''%%%s%%''', td_names{t})) );
    end
    if isempty(td_ids)
        error('read_trading_daily_legacy:trading_destinations', 'no matching trading destination id found...');
    end
    td_ids = sprintf(' %d,', td_ids{:});
    td_ids(end) = [];
    
    td_str = sprintf([' inner join MARKET_DATA..%s trd on trd.security_id = tt.security_id '...
        ' where trd.trading_destination_id in (%s) and trd.trading_destination_id is not null '], table_name, td_ids);
end
%>

%% Request writing: specific part
% with respect to the selected source
if opt.get('force_order')
    order_str = ' OPTION (FORCE ORDER) ';
else
    order_str = '';
end

switch lower( source)
    case 'index'
        if opt.get('group_by_date')
            req_txt = sprintf(['select convert(varchar(12),trd.date,102)as date, trd.trading_destination_id, '...
                ' %s from (select distinct cac.security_id as security_id, cac.name as name from KGR..indice_component comp '...
                ' inner join KGR..security_cac cac on comp.security_id = cac.security_id '...
                ' where INDEXID = (select INDEXID from KGR..[INDEX] where INDEXNAME =''%s'')) tt '...
                ' %s  and trd.date between ''%s'' and ''%s'' %s '...
                'group by date, trd.trading_destination_id'],...
                get_flds, code, td_str, dt_from, dt_to, order_str);
        else
            req_txt = sprintf(['select tt.name, convert(varchar(12),trd.date,102)as date, tt.security_id, trd.trading_destination_id, '...
                ' %s from (select distinct cac.security_id as security_id, cac.name as name from KGR..indice_component comp '...
                ' inner join KGR..security_cac cac on comp.security_id = cac.security_id '...
                ' where INDEXID = (select INDEXID from KGR..[INDEX] where INDEXNAME =''%s'')) tt '...
                ' %s  and trd.date between ''%s'' and ''%s'' %s '],get_flds, code, td_str, dt_from, dt_to, order_str);
        end
        
    case {'security'}
        sec_text_in = sprintf('%d,', code);
        sec_text_in(end) = [];
        if opt.get('group_by_date')
            req_txt = sprintf(['select convert(varchar(12),trd.date,102)as date, trd.trading_destination_id, '...
                ' %s from (select distinct cac.security_id as security_id, cac.name as name from KGR..security_cac cac '...
                ' where security_id in (%s)) tt '...
                ' %s and trd.date between ''%s'' and ''%s'' %s '...
                'group by date, trd.trading_destination_id'],get_flds, sec_text_in, td_str, dt_from, dt_to, order_str);
        else
            req_txt = sprintf(['select tt.name, convert(varchar(12),trd.date,102)as date, tt.security_id, trd.trading_destination_id, '...
                ' %s from (select distinct cac.security_id as security_id, cac.name as name from KGR..security_cac cac '...
                ' where security_id in (%s)) tt '...
                ' %s and trd.date between ''%s'' and ''%s'' %s '],get_flds, sec_text_in, td_str, dt_from, dt_to, order_str);
        end
        
    case {'security_histo'}
        sec_text_in = sprintf('%d,', code);
        sec_text_in(end) = [];
        if opt.get('group_by_date')
            req_txt = sprintf(['select convert(varchar(12),trd.date,102) as date, trd.trading_destination_id, '...
                ' %s from (select distinct CONVERT(int, SYMBOL6) as security_id from KGR..SECURITY '...
                ' where SYMBOL6 in (%s)) tt '...
                ' %s and trd.date between ''%s'' and ''%s'' %s '...
                'group by date, trd.trading_destination_id'],get_flds, sec_text_in, td_str, dt_from, dt_to, order_str);
        else
            req_txt = sprintf(['select NULL as NAME, convert(varchar(12),trd.date,102) as date, tt.security_id, trd.trading_destination_id, '...
                ' %s from (select distinct CONVERT(int, SYMBOL6) as security_id from KGR..SECURITY '...
                ' where SYMBOL6 in (%s)) tt '...
                ' %s and trd.date between ''%s'' and ''%s'' %s '],get_flds, sec_text_in, td_str, dt_from, dt_to, order_str);
        end
        
    otherwise
        error('read_trading_daily_legacy:source', 'source <%s> unavailable', source);
end

%% Request
base = 'MARKET_DATA';
if iscellstr(colnames) && numel(colnames) == 1 && strcmp(colnames{1},'*') %isequaln(colnames,{'*'})
    % Patched by nMayo
    req= generic_structure_request( base, req_txt );
    data= struct2st_data__(req);
else
    req = sql_read_func(base, req_txt);
    
    if ~isempty(req)
        if opt.get('group_by_date')
            data = st_data('init', 'title', 'Read trading daily', ...
                'value', [zeros(size(req,1),1) cell2mat( req(:,2:end))], ...
                'colnames', cat(2, { 'security_id', 'trading_destination_id'}, colnames), ...
                'date', datenum( req(:,1), 'yyyy.mm.dd') );
        else
            data = st_data('init', 'title', 'Read trading daily', ...
                'value', cell2mat( req(:,3:end)), ...
                'colnames', cat(2, { 'security_id', 'trading_destination_id'}, colnames), ...
                'rownames', req(:,1), ...
                'date', datenum( req(:,2), 'yyyy.mm.dd') );
        end
    else
        data = st_data('init', 'title', 'Read trading daily', ...
            'value', NaN(0, length(colnames) + 2), ...
            'colnames', cat(2, { 'security_id', 'trading_destination_id'}, colnames), ...
            'rownames', {{}}, ...
            'date', NaN(0, 1) );
    end
end
data.info.data_log.req = req_txt;
data.info.data_log.base = base;
data.info.td = get_repository( 'trading-destinations');
switch lower( source)
    case {'security','security_histo'}
        data.info.security_id = code;
end

data = st_data('sort', data, '.date');


%% Add rate to euro if asked - TO DO: CHECK
if opt.get('add_rate_to_euro')
    if opt.get('group_by_date')
        error('Impossible to add conversion rate because securities are grouped.')
    else
        data = st_data('add-col', data, NaN(size(data.date)), 'rate_to_euro');
        u_sec_ids = unique(st_data('cols', data, 'security_id'));
        u_dates = unique(data.date);
        if ~isempty(u_sec_ids)
            rates = get_repository('sec_id_rate2ref', 'security_id', u_sec_ids, 'from', FR_dt_from, 'to', FR_dt_to); %'dd/mm/yyyy'
            for s = 1:length(u_sec_ids)
                for d = 1:length(u_dates)
                    idx_data = data.date==u_dates(d) & st_data('cols', data, 'security_id')==u_sec_ids(s);
                    if ~isempty(rates)
                        idx_rates = rates.date==u_dates(d) & st_data('cols', rates, 'security_id')==u_sec_ids(s);
                        if ~isempty(find(idx_rates))
                            data.value(idx_data, end) = repmat(rates.value(idx_rates, end), size(data.value(idx_data, end),1),1);
                        else
                            data.value(idx_data, 3:end) = NaN(size(data.value(idx_data, 3:end)));
                        end
                    else
                        data.value(idx_data, 3:end) = NaN(size(data.value(idx_data, 3:end)));
                    end
                end
            end
        end
    end
end

%% Correct from corporate actions if asked
if opt.get('cac')
    data = st_data('trading_daily_cac', data);
end

%% Output format
format_mode = opt.get('format');
switch lower( format_mode)
    case 'plain'
        % nothing to do
        varargout = { data };
    case 'by-field'
        varargout = {};
        sec_id = st_data('col', data, 'security_id');
        [si_ref, tmp, si_idx] = unique( sec_id);
        new_cols = cellfun(@(n,s)sprintf('%s:%d', n,s), data.rownames(tmp), num2cell(si_ref),'uni', false)';
        [dt_ref, ~, dt_idx] = unique( floor(data.date));
        for f=1:length(colnames)
            this_field = st_data('col', data, colnames{f});
            those_vals = accumarray([dt_idx, si_idx], this_field);
            varargout{end+1} = st_data('init', 'title', colnames{f}, ...
                'value', those_vals, 'date', dt_ref, 'colnames', new_cols);
            varargout{end}.info.data_log.req = req_txt;
            varargout{end}.info.data_log.base = base;
        end
    otherwise
        error('read_trading_daily_legacy:format', 'output format <%s> unknown', source);
end

    function data= struct2st_data__(s)
        % Transforme en st_data le r�sultat de generic_structure_request !
        % Ignore les champs prenant des valeurs de type cell (sauf date, extraite � part)
        names= fieldnames(s);
        values= struct2cell(s);
        inum= cellfun(@isnumeric,values);
        data = st_data('init', 'title', 'Read trading daily', ...
            'value', cat(2,values{inum}), ...
            'colnames', names(inum)', ...
            'rownames', s.name, ...
            'date', datenum( s.date, 'yyyy.mm.dd') );
    end

    function r = sql_read_func(b, t)
        try
        r = exec_sql( b, t );    
        catch er
            if ~isempty(regexp(er.message, 'deadlock'))
                pause(60)
                r = exec_sql( b, t ); %r = sql_read_func(b, t);
            else
                error(er.message)
            end
        end
    end
end