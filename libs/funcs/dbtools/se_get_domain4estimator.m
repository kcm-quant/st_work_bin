function rslt = se_get_domain4estimator(estimator_name)
% GET_DOMAIN4ESTIMATOR - recup�re tous les domaines d'un estimateur
%
% se_get_domain4estimator('Volume curve')
%


global st_version
q_base = st_version.bases.quant;

estimator_id = se_get_estimator_id(estimator_name);

query = sprintf(['SELECT' ...
    ' ds.domain_id,' ...
    ' ds.security_id,' ...
    ' ds.trading_destination_id,' ...
    ' d.domain_name' ...
    ' FROM' ...
    ' %s..domain_security ds,' ...
    ' %s..job j,' ...
    ' %s..domain d' ...
    ' WHERE' ...
    ' j.domain_id=ds.domain_id' ...
    ' AND j.domain_id=d.domain_id' ...
    ' AND j.estimator_id=%d' ...
    ' ORDER BY' ...
    ' ds.domain_id'],q_base , q_base, q_base, estimator_id);

data = exec_sql('QUANT', query);
tmp_rslt = cell2mat(data(:, 1:end-1));

if ~isfinite(tmp_rslt(1, 1))
    rslt = {tmp_rslt(1, 2), {[]}, data{1, 4}};
else
    rslt = {tmp_rslt(1, 2), {tmp_rslt(1, 3)}, data{1, 4}};
end
domain_id = tmp_rslt(1, 1);
for i=2:size(tmp_rslt, 1)
    % on travaille sur un nouveau domaine
    if tmp_rslt(i, 1) ~= domain_id
        % on actualise les param du groupe
        domain_id = tmp_rslt(i, 1);
        %on commence � le remplir
        if ~isfinite(tmp_rslt(i, 3))
            rslt(end+1, :) = {tmp_rslt(i, 2), {[]}, data{i, 4}};
        else
            rslt(end+1, :) = {tmp_rslt(i, 2), {tmp_rslt(i, 3)}, data{i, 4}};
        end
    else
        rslt{end, 1} = [rslt{end, 1}; tmp_rslt(i, 2)];
        if ~isfinite(tmp_rslt(i, 3))
            rslt{end, 2} = {rslt{end, 2}{:}, []};
        else
            rslt{end, 2} = {rslt{end, 2}{:}, tmp_rslt(i, 3)};
        end
    end
end