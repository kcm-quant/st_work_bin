function out = se_get_job_id_from_association(estimator_name, sec_id, td_id)
% SE_GET_JOB_ID_FROM_ASSOCIATION - on r�cup�re les jobs_id dans la table
% d'association, ici on ne fait pas attention au fait que le run ai �t�
% valid� ou non!
%
% out = se_get_job_id_from_association('volume curve', 276, [])
% out = se_get_job_id_from_association('volume curve', 276, 4)
%
% out = se_get_job_id_from_association('market impact', 276, 4)
global st_version
estimator_id = se_get_estimator_id(estimator_name);
quant_base_name = st_version.bases.quant;
switch lower(estimator_name)
    case 'volume curve'
        out = vc_association(quant_base_name, estimator_id, sec_id, td_id);
    otherwise
        out = gen_association(quant_base_name, estimator_id, sec_id, td_id);
end
end

function out = vc_association(quant_base_name, estimator_id, sec_id, td_id)
    out = gen_association(quant_base_name, estimator_id, sec_id, td_id);
    if isempty(td_id) || ~isfinite(td_id) % jokers multidestinations
        tdi = get_repository('tdi', sec_id);
        j_and_c = exec_sql('QUANT', sprintf(['select job_id, context_id ' ...
                ' from %s..association ' ...
                ' where estimator_id = %d  ' ...
                ' and security_id is null ' ...
                ' and trading_destination_id = %s ' ...
                ' and varargin is null' ...
                ' order by rank'], quant_base_name, estimator_id, ...
                num2str_req(tdi(1))));
            if ~isempty(j_and_c)
                out = cat(1, out, struct('job_id', j_and_c{:, 1}, 'context_id', j_and_c{:, 2}));
            end
    elseif isnumeric(td_id) % jokers monodestination
        tdinfo = get_repository('tdinfo', sec_id);
        idx = [tdinfo.trading_destination_id] == td_id;
        if ~isempty(idx)
            j_and_c = exec_sql('QUANT', sprintf(['select job_id, context_id ' ...
                ' from %s..association ' ...
                ' where estimator_id = %d  ' ...
                ' and security_id is null ' ...
                ' and trading_destination_id = %s ' ...
                ' and varargin = ''%s''' ...
                ' order by rank'], quant_base_name, estimator_id, ...
                num2str_req(td_id), tdinfo(idx).quotation_group));
            if ~isempty(j_and_c)
                out = cat(1, out, struct('job_id', j_and_c{:, 1}, 'context_id', j_and_c{:, 2}));
            end
        end
    else
        error('se_get_job_id_from_association:vc_association:exec', 'Wrong type : <%s>', class(my_num));
    end
end

function out = gen_association(quant_base_name, estimator_id, sec_id, td_id)
j_and_c = exec_sql('QUANT', sprintf(['select job_id, context_id ' ...
    ' from %s..association ' ...
    ' where estimator_id = %d  ' ...
    ' and security_id = %d ' ...
    ' and trading_destination_id = %s ' ...
    ' order by rank'], quant_base_name, estimator_id, sec_id, num2str_req(td_id)));
if isempty( j_and_c)
    out = struct('job_id', {}, 'context_id', {});
else
    out = struct('job_id', j_and_c{:, 1}, 'context_id', j_and_c{:, 2});
end
end

function str_req = num2str_req(my_num)
if isempty(my_num) || ~isfinite(my_num)
    str_req = 'null';
elseif isnumeric(my_num)
    str_req = num2str(my_num);
else
    error('se_get_job_id_from_association:num2str_req:exec', 'Wrong type : <%s>', class(my_num));
end
end