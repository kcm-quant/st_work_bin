function out = se_get_nb_deal_frm_sec_id (sec_id, varargin )
%SE_GET_NB_DEAL_FRM_SEC_ID - return deal number from security_id
%
%exemple :
%se_get_nb_deal_frm_sec_id (110 , 'date_t','23/09/2009' )


global st_version
q_base = st_version.bases.quant;


opt = options( { ...
    'date_t', (today -1)});

if ~isempty(sec_id)
    sec_id_str = sprintf('%d',sec_id(1));
    for i= 2 : length(sec_id)
        sec_id_str = [ sec_id_str sprintf(',%d',sec_id(i))];
    end
end


quals = exec_sql('BSIRIUS', sprintf( [ ...
    'SELECT trading_destination_id , nb_deal '...
    'FROM tick_db ..trading_daily ' ...
    'WHERE ' ...
    'security_id in (%s)  AND ' ...
    'date = ''%s'' '...
    'ORDER BY nb_deal'],sec_id_str ,datestr(opt.get('date_t') , 'yyyymmdd')));

colnames = { 'trading_destination_id', 'nb_deal'};
nb_deal = struct('value', {quals}, 'colnames', {colnames});
fprintf('%24s | ', nb_deal.colnames{:});
fprintf('\n');
str = '';
for d=1:size(nb_deal.value,1)
    fprintf('%24d | %24d \n', nb_deal.value{d,1},nb_deal.value{d,2});
end
fprintf('\n');


end
