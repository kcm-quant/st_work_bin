function [se_data, assoc_info] = se_get_param(run_id, varargin)
% SE_GET_PARAM - r�cup�ration des param�tres d'un run
% 
% out = se_get_job_id_from_association('volume curve', 276, [])
% d=se_get_run_and_check4job(out(1).job_id)
% run_ids = st_data('cols', d, 'run_id');
% params = se_get_param(run_ids(end))
% vc = vcse2vcdata(params, datenum(0,0,0,0,5,0))
%
% out = se_get_job_id_from_association('volume curve', 1021508, [])
% d=se_get_run_frm_job('run' , out(1).job_id, 'is_valid', 1)
% run_ids = st_data('cols', d, 'run_id');
% params = se_get_param(run_ids)
% vc = vcse2vcdata(params, datenum(0,0,0,0,5,0))
% st_plot(st_data('keep', vc, 'Usual day'))
%
% params = se_get_param({2809944, 2, 19,2}, 'source', 'quant_data')
% vc = vcse2vcdata(params, datenum(0,0,0,0,5,0))


opt = options_light({'source', 'quant', ... % where to get it from ? quant or quant_data in this case we are wiating for a cell array with run_id context_id, domain_id, estimator_id instead of run_id
    }, varargin);

if length(run_id)>=1
    se_data = [];
    for i = 1 : size(run_id, 1)
        se_data = st_horzcat(se_data, se_get_param_unit(run_id(i, :), opt), 'colnames2', @(coln, title)(coln), ...
            'missing warning', 1e-5, 'missing error',  1e-4);
    end
else
    error('se_get_param:exec', 'non strictly positive integer number of run ids');
end
end

function [se_data, assoc_info] = se_get_param_unit(run_id, opt)
global st_version
switch opt.source
    case 'quant'
        params = exec_sql('QUANT', sprintf(['select pd.parameter_name, pv.value, pd.x_value ' ...
            ' from %s..param_value as pv, %s..param_desc as pd ' ...
            'where pv.run_id = %d and pd.parameter_id = pv.parameter_id order by pd.x_value'], ...
            st_version.bases.quant, st_version.bases.quant, run_id));
        
        context_name = exec_sql('QUANT', sprintf(['select context_name from %s..context c, %s..estimator_runs er ' ...
            ' where er.context_id = c.context_id and er.run_id = %d'], ...
            st_version.bases.quant, st_version.bases.quant, run_id));
    case 'quant_data'
        
        params = exec_sql('QUANT', sprintf(['select parameter_name, value, x_value ' ...
            ' from quant_data..quant_param  ' ...
            'where context_id = %d and domain_id = %d and estimator_id = %d'], ...
            run_id{:, 2:4}));
        
        context_name = exec_sql('QUANT', sprintf(['select context_name from %s..context c, %s..estimator_runs er ' ...
            ' where er.context_id = c.context_id and er.run_id = %d'], ...
            st_version.bases.quant, st_version.bases.quant, run_id{1}));
    otherwise
end

% On construit un st_data � partir des param�tres r�cup�r�s
se_data = struct('rownames', {params(:, 1)}, 'value', cat(1, params{:, 2}), ...
    'colnames', {context_name}, 'date', cell2mat(params(:, 3)), 'title', '');
end