function data = se_get_run_and_check4job(job_id)
% SE_GET_RUN_AND_CHECK4JOB - pour un job_id, r�cup�re les check_quality,
% run_quality  et autrezs informations
%
% out = se_get_job_id_from_association('volume curve', 276, [])
% d=se_get_run_and_check4job(out(1).job_id)
% slippage_bp = vc_quality_trans('inv-trans', st_data('cols', d, 'check_quality'));
% 

global st_version
info = exec_sql('QUANT', sprintf(['select cq.value, cq.stamp_date, ' ... 
                '       er.run_quality, er.run_id, er.stamp_date, er.context_id ' ...
                ' from %s..estimator_runs er, ' ...
                '      %s..check_quality cq ' ...
                ' where er.job_id = %d  ' ...
                ' and er.run_id = cq.run_id ' ...
                ' order by cq.stamp_date'], ...
                st_version.bases.quant, st_version.bases.quant, job_id));
data = st_data('init', 'value', [cell2mat(info(:, [1,3,4,6])) datenum(info(:,5), 'yyyy-mm-dd')], ...
    'date', datenum(info(:,2), 'yyyy-mm-dd'), 'title', num2str(job_id), ...
    'colnames', {'check_quality', 'run_quality', 'run_id', 'context_id', 'run_date'});