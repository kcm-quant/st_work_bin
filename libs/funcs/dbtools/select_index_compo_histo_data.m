function s_data = select_index_compo_histo_data(data, dt_col, compo_histo)

%%
us = cellfun(@str2num, compo_histo.member_histo.colnames);

mmb = reshape(compo_histo.member_histo.value,[],1);
dts = reshape(repmat(compo_histo.member_histo.date,1,size(compo_histo.member_histo.value,2)),[],1);
sec_ids = reshape(repmat(us,size(compo_histo.member_histo.value,1),1),[],1);

dts_keep = dts(mmb>0);
sec_ids_keep = sec_ids(mmb>0);

if istable(data)
    s_data = data(ismember([data.security_id, dt_col],[sec_ids_keep, dts_keep],'rows'),:);
else
    s_data = data;
    ix = ismember([st_data('cols',data,'security_id'), dt_col],[sec_ids_keep, dts_keep],'rows');
    s_data.value = s_data.value(ix,:);
    s_data.date = s_data.date(ix,:);
end

end

%%
% s_data = cell(length(us),1);
% for s = 1:length(us)
%     dts = compo_histo.member_histo.date(compo_histo.member_histo.value(:,s)>0);
%     s_data{s} = data(data.security_id==us(s) & ismember(dt_col, dts),:);
% end
% s_data = vertcat(s_data{:});

