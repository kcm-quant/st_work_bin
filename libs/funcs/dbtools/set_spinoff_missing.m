function res = set_spinoff_missing()
% res = set_spinoff_missing()
% 

change_connections('production')
filepath = 'W:\Global_Research\Quant_research\projets\data\dynamic universe\split\' ;

% split excel file
filename            = 'spinoff2process' ;
sheetname_add       = 'spinoff2add';

% Read excel file to " Table "
T_use     = readtable(sprintf('%s%s.xlsx', filepath, filename), 'sheet', sheetname_add, 'PreserveVariableNames', true);

EVENTTYPE = 53;

%% Add Spinoff
res = cell(height(T_use),5);
for i = 1:height(T_use)
    
    date  = T_use{i,1};
    secid_parent = T_use{i,2};
    price_ratio   = T_use{i,3};
    volume_ratio  = T_use{i,4};
    secid_child = T_use{i,5};
    
    % Step 1: Get eventID of split from calevent
    SQLQuery_EventID_CALEVENT = sprintf('select EVENTID from KGR..CALEVENT where EVENTTYPE = %d and SCOPETYPE=1 and EVENTDATE =''%s'' and SCOPEID = %d',...
        EVENTTYPE, datestr(date, "yyyy-mm-dd"), secid_parent);
    EVENTID_init = exec_sql('KGR', SQLQuery_EventID_CALEVENT);

    % Step 2: check if split exists in CALEVENT 
    if isempty(EVENTID_init)
        %     If not, insert split into calevent
        SQLQuery_insert_CALEVENT = sprintf('insert into KGR..CALEVENT (EVENTDATE, EVENTTYPE, SCOPETYPE, SCOPEID) values ( ''%s'', %d, 1, %d)',...
            datestr(date, "yyyy-mm-dd"), EVENTTYPE, secid_parent);
        exec_sql('KGR', SQLQuery_insert_CALEVENT);

        %     Get eventID of split newly inserted into calevent
        SQLQuery_EventID_CALEVENT = sprintf('select EVENTID from KGR..CALEVENT where EVENTTYPE = %d and SCOPETYPE=1 and EVENTDATE =''%s'' and SCOPEID = %d',...
            EVENTTYPE, datestr(date, "yyyy-mm-dd"), secid_parent);
        EVENTID_init = exec_sql('KGR', SQLQuery_EventID_CALEVENT);
        EVENTID = EVENTID_init{1}(1);
        
        res{i,2} = SQLQuery_insert_CALEVENT;
        res{i,3} = SQLQuery_EventID_CALEVENT;
    else
        EVENTID = EVENTID_init{1}(1);
        res{i,2} = sprintf('Split (EventID = %d) exists in CALEVENT\n', EVENTID);
    end

    % Step 3: Check if split Event exists in CALEVENTTRADINGADJUST
    SQLQuery_EventID_CTA = sprintf('select EVENTID from KGR..CALEVENTTRADINGADJUST where EVENTID = %d', EVENTID);
    EVENTID_init = exec_sql('KGR', SQLQuery_EventID_CTA);
    
    if isempty(EVENTID_init)
        % Step 4: Get ADJUSTEMENTID for split from calevent
        SQLQuery_ADJUSTEMENTID_CTA = 'select max(ADJUSTEMENTID)+1 from KGR..CALEVENTTRADINGADJUST';
        ADJUSTEMENTID_init = exec_sql('KGR', SQLQuery_ADJUSTEMENTID_CTA);
        ADJUSTEMENTID = ADJUSTEMENTID_init{1}(1); % adjID of split in number type
        
        % Step 5: Insert split_ratio into CALEVENTTRADINGADJUST
        SQLQuery_insert_CTA = sprintf('insert into KGR..CALEVENTTRADINGADJUST (EVENTID, ADJUSTTYPE, PRICERATIO, VOLUMERATIO, DIVIDEND,ADJUSTEMENTID) values (%d,''S'',%f,%f,NULL,%d)',...
            EVENTID, price_ratio, volume_ratio, ADJUSTEMENTID);
        exec_sql('KGR', SQLQuery_insert_CTA);
        
        res{i,4} = SQLQuery_insert_CTA;
    else
        EVENTID = EVENTID_init{1}(1);
        res{i,4} = sprintf('Split (EventID = %d) exists in CALEVENTTRADINGADJUST\n', EVENTID);
    end

    % Step 6: add spinoff details
    SQLQuery_insert_details = sprintf('insert into temp_works..spin_off_details (EVENTID, parent_security_id, child_security_id) values (%d,%d,%d)',EVENTID,secid_parent,secid_child);
    exec_sql('KGR', SQLQuery_insert_details);
    res{i,5} = SQLQuery_insert_details;
    
    res{i,1} = sprintf('Spinoff Add - S6 = %d / EVENTID = %d / date = %s / price_factor = %.2f \n', secid_parent, EVENTID, date, price_ratio);
end


end