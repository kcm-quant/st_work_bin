change_connections('production')

filepath = 'W:\Global_Research\Quant_research\projets\data\dynamic universe\split\' ;

% split excel file
filename            = 'split2process' ;
sheetname_add       = 'split2add';
sheetname_delete    = 'split2delete';

% Read excel file to " Table "
T_split2add     = readtable(sprintf('%s%s.xlsx', filepath, filename), 'sheet', sheetname_add, 'PreserveVariableNames', true);
T_split2delete  = readtable(sprintf('%s%s.xlsx', filepath, filename), 'sheet', sheetname_delete, 'PreserveVariableNames', true);


%% Delete Split

T_use = T_split2delete;
res_delete = cell(height(T_use),1);
for i = 1:height(T_use)
    
    date_split  = T_use{i,1};
    secid_split = T_use{i,2};
    
    % Step 1: Get info of split from calevent
    SQLQuery_select_CALEVENT = sprintf('select * from KGR..CALEVENT where EVENTTYPE = 51 and SCOPETYPE=1 and EVENTDATE =''%s'' and SCOPEID = %d', datestr(date_split, "yyyy-mm-dd"), secid_split);
    INFO_CALEVENT = exec_sql_table('KGR', SQLQuery_select_CALEVENT);
    
    % Step 2: Check if only one event in CALEVENT 
    if height(INFO_CALEVENT)~=1 
        fprintf('Error for Split/Right (%d, %s): nothing or more than one event in CALEVENT \n', secid_split, date_split)
    else
        EVENTID = INFO_CALEVENT.EVENTID;

        % Step 3: Get info of split from CALEVENTTRADINGADJUST
        SQLQuery_select_CTA = sprintf('select * from KGR..CALEVENTTRADINGADJUST where EVENTID = %d', EVENTID);
        INFO_CTA = exec_sql_table('KGR', SQLQuery_select_CTA);

        % Step 4: Check if only one event in CALEVENTTRADINGADJUST 
        if height(INFO_CTA)~=1
            fprintf('Error for Split/Right (%d, %s): nothing or more than one event in CALEVENTTRADINGADJUST \n', secid_split, date_split)
        else 
            %     delete from CALEVENT
            SQLQuery_delete_CALEVENT = sprintf('delete from KGR..CALEVENT where EVENTID = %d', EVENTID);
            exec_sql('KGR', SQLQuery_delete_CALEVENT);

            %     delete from CALEVENTTRADINGADJUST
            SQLQuery_delete_CTA = sprintf('delete from KGR..CALEVENTTRADINGADJUST where EVENTID = %d', EVENTID);
            exec_sql('KGR', SQLQuery_delete_CTA);
            res_delete{i} = {'Success of delte', INFO_CALEVENT, INFO_CTA, SQLQuery_delete_CALEVENT, SQLQuery_delete_CTA};
        end
    end
    
    fprintf('Success of delte for Split/Right (%d, %s) \n', secid_split, date_split)

end


filepath = 'W:\Global_Research\Quant_research\projets\data\dynamic universe\split\delete_record\' ;
filename = sprintf('%s%s_delete_record', filepath, datestr(today(),'yyyymmdd'));
save(filename, 'res_delete');

%% Add Split

% example: 
% date_split  = datenum(2016,07,27)
% secid_split = 8150;
% price_ratio_split = 10;
% volume_ratio_split = 0.1;

T_use = T_split2add;
for i = 1:height(T_use)
    
    date_split  = T_use{i,1};
    secid_split = T_use{i,2};
    price_ratio_split   = T_use{i,3};
    volume_ratio_split  = T_use{i,4};

    % Step 1: Get eventID of split from calevent
    SQLQuery_EventID_CALEVENT = sprintf('select EVENTID from KGR..CALEVENT where EVENTTYPE = 51 and SCOPETYPE=1 and EVENTDATE =''%s'' and SCOPEID = %d', datestr(date_split, "yyyy-mm-dd"), secid_split);
    EVENTID_init = exec_sql('KGR', SQLQuery_EventID_CALEVENT);

    % Step 2: check if split exists in CALEVENT 
    if isempty(EVENTID_init)
        %     If not, insert split into calevent
        SQLQuery_insert_CALEVENT = sprintf('insert into KGR..CALEVENT (EVENTDATE, EVENTTYPE, SCOPETYPE, SCOPEID) values ( ''%s'', 51, 1, %d)', datestr(date_split, "yyyy-mm-dd"), secid_split);
        exec_sql('KGR', SQLQuery_insert_CALEVENT);

        %     Get eventID of split newly inserted into calevent
        SQLQuery_EventID_CALEVENT = sprintf('select EVENTID from KGR..CALEVENT where EVENTTYPE = 51 and SCOPETYPE=1 and EVENTDATE =''%s'' and SCOPEID = %d', datestr(date_split, "yyyy-mm-dd"), secid_split);
        EVENTID_init = exec_sql('KGR', SQLQuery_EventID_CALEVENT);
        EVENTID = EVENTID_init{1}(1);

    else
        EVENTID = EVENTID_init{1}(1);
        fprintf('Split/Right already exist in calevent - S6 = %d / date = %s  \n', secid_split, date_split)
    end

    % Step 3: Check if split Event exists in CALEVENTTRADINGADJUST
    SQLQuery_EventID_CTA = sprintf('select EVENTID from KGR..CALEVENTTRADINGADJUST where EVENTID = %d', EVENTID);
    EVENTID_init = exec_sql('KGR', SQLQuery_EventID_CTA);
    
    if isempty(EVENTID_init)
        % Step 4: Get ADJUSTEMENTID for split from calevent
        SQLQuery_ADJUSTEMENTID_CTA = 'select max(ADJUSTEMENTID)+1 from KGR..CALEVENTTRADINGADJUST';
        ADJUSTEMENTID_init = exec_sql('KGR', SQLQuery_ADJUSTEMENTID_CTA);
        ADJUSTEMENTID = ADJUSTEMENTID_init{1}(1); % adjID of split in number type
        
        % Step 5: Insert split_ratio into CALEVENTTRADINGADJUST
        SQLQuery_insert_CTA = sprintf('insert into KGR..CALEVENTTRADINGADJUST (EVENTID, ADJUSTTYPE, PRICERATIO, VOLUMERATIO, DIVIDEND,ADJUSTEMENTID) values (%d,''S'',%f,%f,NULL,%d)', EVENTID, price_ratio_split, volume_ratio_split, ADJUSTEMENTID);
        exec_sql('KGR', SQLQuery_insert_CTA);
        
        fprintf('Split/Right Add - S6 = %d / date = %s / price_factor = %.2f \n', secid_split, date_split, price_ratio_split)
    else
        fprintf('Split/Right (EventID = %d) exists in CALEVENTTRADINGADJUST\n', EVENTID)
    end


end



%% Check split

T_use = T_split2add;
T_use.compare_ratio =  T_use{:,2};
T_use.compare_ratio(:) = nan;

close all;

for i = 1:height(T_use)
    
    date_split  = T_use{i,1};
    secid_split = T_use{i,2};
    price_ratio_split   = T_use{i,3};
    volume_ratio_split  = T_use{i,4};
    
    tdinfo      = get_repository('tdinfo', secid_split );
    base_name   = tdinfo(1).global_zone_suffix;
    
    dt_from     = datestr(date_split-25, 'yyyymmdd');
    dt_to       = datestr(date_split+25, 'yyyymmdd');
    
    q           = sprintf(['select r.date, r.close_prc as close_raw, c.close_prc as close_corr  '...
                    'from MARKET_DATA..trading_daily%s r, MARKET_DATA..trading_daily%s_splits_correction c '...
                    'where r.security_id = c.security_id '...
                    'and r.date = c.date '...
                    'and r.trading_destination_id = c.trading_destination_id '...
                    'and r.security_id = %d '...
                    'and r.close_volume >0 '...
                    'and r.date between ''%s'' and ''%s'' order by date'
                    ], base_name, base_name, secid_split, dt_from, dt_to);
                
    close_prc       = exec_sql_table('MARKET_DATA', q);
    close_prc.date  = datetime(close_prc.date, 'InputFormat', 'uuuu-MM-dd');

    close_prc{:,3}  = close_prc{:,3}/close_prc{end,3}*close_prc{end,2};
    
    idx_adj         = find(close_prc.date < date_split, 1, 'last');
    
    adj_before_ratio  = close_prc.close_raw(idx_adj) ./ close_prc.close_corr(idx_adj);
    adj_after_ratio   = close_prc.close_raw(idx_adj+1) ./ close_prc.close_corr(idx_adj+1);

    T_use.compare_ratio(i) = (adj_before_ratio)/(adj_after_ratio);
    
    figure;
    plot(close_prc.date, close_prc{:,2:3}, '-*')
    grid on;
    xtickformat('dd-MMM-yyyy')
    grid on;
    title(sprintf('sec id = %d, date = %s', secid_split, date_split));
    legend({'close raw', 'close adjusted'}, 'location', 'best')
end


