function data = st_data_get_trading_destination_mapping(data)

[td_id_ch,~,idx_td_id_ch] = unique(st_data('cols',data,'trading_destination_id'));
td_id_kgr = get_trading_destination_mapping(td_id_ch,data.info.security_id);
data.value(:,strcmp(data.colnames,'trading_destination_id')) = td_id_kgr(idx_td_id_ch);

new_tdinfo = data.info.td_info;
new_tdinfo_tdid = joint(td_id_ch,td_id_kgr,[new_tdinfo.trading_destination_id]');
new_tdinfo_tdid(isnan(new_tdinfo_tdid)) = -[new_tdinfo(isnan(new_tdinfo_tdid)).trading_destination_id];
new_tdinfo_tdid = num2cell(new_tdinfo_tdid);
[new_tdinfo.trading_destination_id] = deal(new_tdinfo_tdid{:});

if isfield(new_tdinfo, 'true_place_id')
    new_tdinfo = rmfield(new_tdinfo,'true_place_id');
end
data.info.td_info = new_tdinfo;
data.info.version = 'kepche_1';
