function c = userdatabase( mode, varargin)
% USERDATABASE - gestion de connections multiples ? des bases de donn?es
%
% c = userdatabase('sybase:j', 'BSIRIUS')
% userdatabase('close', 'BSIRIUS')
%
% userdatabase reset
%
% c = userdatabase('job',      'PAMLB001', 'toto')
%
% mrk = userdatabase('sle', 'test', 'gui', 'true', 'event_handler', 'sle_listener');
% userdatabase('close', 'sle', 'test', 'gui', 'true', 'event_handler', 'sle_listener')
%
% c = userdatabase('sqlite', 'repository' , 'test', 'test');
%
% jdbc_sqlite (multi platform) mode
% First time (create the connection)  
% c = userdatabase('jdbc_sqlite', 'logical_name_of_the_connection' , path_too_the_db_file);
% then one can retrieve if needed the connection in this way (should ne no use , as c is not used):
% c = userdatabase('jdbc_sqlite', 'logical_name_of_the_connection' );
% and use it with exec_sql : 
% exec_sql('logical_name_of_the_connection', SQL_Query);
%
%
% On peut aussi r?cup?rer l'ensemble des connections ouvertes :
% userdatabase('get_open_databases');
%
% See also userdata database exec_sql


global st_version
persistent memory_udb

if strcmpi(mode, 'reset')
    if ~isempty(memory_udb)
        % fermeture propre
        try
            to_close = memory_udb.get('open_databases');
        catch ME
            if strcmp(ME.identifier, 'options:key')
                to_close = [];
            else
                rethrow(ME);
            end
        end
        for tc=1:length(to_close)
            st_log('Closing connection <%s>...\n', to_close{tc});
            close(  memory_udb.get_no_err_no_ddot( to_close{tc}));
        end
        % plus rien:
        memory_udb = [];
    end
end
if isempty(memory_udb)
    memory_udb = options({});
    try
        st_log('userdatabase: reloading <st_work.xml>...\n');
        se_xml  = xmltools( 'st_work.xml');
        st_log('userdatabase: reloaded.\n');
    catch er
        st_log('userdatabase: unable to reload <st_work.xml>.\n');
        if strcmp(er.message, 'File st_work.xml not found')
            error('userdatabase:initFromXML', 'Verify that you get the <st_work.xml> file in the MATLAB <work> directory...\n');
        else
            rethrow(er);
        end
    end    
    xml  = xmltools('st_work.xml');
    target = xmltools(xml.get_tag('target_connections'));
    all_target = xmltools(target.get_tag('target'));
    for j=1:all_target.nb_children()
        target = xmltools(all_target.keep_children(j));
        if strcmpi(target.get_attrib_value('target','name'), st_version.my_env.target_name)
            
            drivers = xmltools( target.get_tag('connections'));
            for d=1:drivers.nb_children()
                this_driver = xmltools( drivers.keep_children(d));
                driver_name = this_driver.get_attrib_value('connections', 'driver');
                databases   = this_driver.get_attrib_value('server', 'name');
                if ~iscell(databases)
                    databases = {databases};
                end
                opt_lst = {};
                for b=1:length(databases)
                    this_xml_connection = this_driver.get_attrib('server', 'name', databases{b});
                    this_connection = this_xml_connection.children.value;
                    opt_lst = cat(2, opt_lst, { databases{b}, this_connection});
                    st_log('Setting server : <%s>, with connection values : <%s>\n', databases{b}, this_connection);
                end
                memory_udb.set( driver_name, options( opt_lst));
            end
            
        end
    end
end

switch lower(mode)
    case 'job'
        s_name = varargin{1};
        j_name = varargin{2};
        job_key = sprintf('%s:', varargin{:});
        c = memory_udb.get_no_err_no_ddot([ job_key 'job']);
        if isempty( c)
            c = findResource( 'scheduler', 'type', 'jobmanager', 'name', j_name, 'LookupURL', s_name);
            memory_udb.set( [ job_key 'job'], c);
        end
    case 'rtce'
        c = memory_udb.get_no_err_no_ddot('rtce');
        if isempty( c)
            c = { '10.136.39.141', 7070 };
            st_log('loading RTCE DLL..\n');
            loadlibrary ('C:\Apps\VhaYu\MATLAB\bin\TBMatLab.dll', 'C:\Apps\VhaYu\MATLAB\include\TBMatLab.h');
            memory_udb.set('rtce', c);
            ADD_KEY( 'sle');
        end
    case 'access'
        key = [varargin{:}];
        c   = memory_udb.get_no_err_no_ddot( key);
        if isempty( c)
            c = database(varargin{:});
            memory_udb.set( key, c);
        end
    case 'sqlite'
        if nargin==4
            key = [varargin{:}];
            c   = memory_udb.get_no_err_no_ddot( key);
            if isempty( c)
                c = database(varargin{:});
                memory_udb.set( key, c);
            end
        else
            conn_dico = memory_udb.get_no_err_no_ddot('sqlite');
            if ischar(varargin{1})
                key_ = conn_dico.get_no_err_no_ddot(varargin{1});
                if isempty(key_)
                    error('userdatabase:exec', 'Unknown server : <%s>', varargin{1});
                end
                c = memory_udb.get_no_err_no_ddot(key_);
                if isempty(c)
                    kelem = tokenize( key_, '|');
                    c = database( kelem{[1 3:4]});
                    memory_udb.set(key_, c);
                    ADD_KEY(key_);
                end
            else
                error('userdatabase:exec', 'Unexpected type for first optionnal argument (expected ones are char or database object)');
            end
        end
     
    case 'jdbc_sqlite'
        % Retrieve logiacl name of the connection , provided by user
        key = varargin{1};
        c   = memory_udb.get_no_err_no_ddot( key);
        if isempty(c)
            % create a new db connection
            if nargin == 3
                local_db_file = varargin{2};
                % check that the requested db exist
                if exist(local_db_file,'file') == 0
                    error('userdatabase:jdbc_sqlite', 'requested sqlite db file : <%s>doest not exist, please check', local_db_file);
                else
                    c = database('','','', ...
                        'org.sqlite.JDBC', ...
                        ['jdbc:sqlite:' local_db_file]);
                    if ~isempty( c.Message)
                        error('userdatabase:connection', 'Connection have failed. URL = [%s]\n Message = %s\n',...
                            c.URL, c.Message);
                    else
                        memory_udb.set( key, c); % already some open sqlite DB
                        ADD_KEY(key);
                        % we need to pssibly update the st_version with the
                        % new connection if no sqlite_jdbc connection was
                        % declared
                        % this is needed for a clean compatibility with
                        % exec_sql suing the logical name
                        jdbc_sqlite_mask = strcmp(st_version.connection_driver(:,1),'jdbc_sqlite');
                        if sum(jdbc_sqlite_mask ) == 0
                            % add 
                            st_version.connection_driver{end + 1 ,1} = 'jdbc_sqlite';
                            st_version.connection_driver{end,2} = {key};
                        else
                            % append a key in the sqlite_jdbc connections
                            st_version.connection_driver(jdbc_sqlite_mask,2)= {[st_version.connection_driver{jdbc_sqlite_mask,2},{key}]};
                        end
                    end
                end
            else
                error('userdatabase:jdbc_sqlite', '2 or 3 arguments expected');
            end
        else
            % just check that we rertieve the db file in the url !!
            % Otherwise it s a mess, means user is using the same name for
            % 2 differnt files , which is very bad
            if nargin == 3
                local_db_file = varargin{2};
                if exist(local_db_file,'file') == 0
                    error('userdatabase:jdbc_sqlite', 'requested sqlite db file : <%s>doest not exist, please check', local_db_file);
                else
                    if isempty(strfind(c.URL,  local_db_file))
                        error('userdatabase:jdbc_sqlite', 'useing an exsiting connection (URL : <%s>) with a different db file : <%s> : ',c.URL , local_db_file);
                    end
                end
            elseif nargin > 3
                error('userdatabase:jdbc_sqlite', '2 or 3 arguments expected');
            end
        end
    case 'sle'
        c = memory_udb.get_no_err_no_ddot('sle');
        if isempty( c)
            c = sleconnection( varargin{:});
            memory_udb.set('sle', c);
            ADD_KEY( 'sle');
        end
    case 'sybase:j'
        conn_dico = memory_udb.get_no_err_no_ddot('sybase:j');
        if ischar(varargin{1})
            key_ = conn_dico.get_no_err_no_ddot(varargin{1});
            if isempty(key_)
                error('userdatabase:exec', 'Unknown server : <%s>', varargin{1});
            end
            c = memory_udb.get_no_err_no_ddot(key_);
        elseif strcmp(class(varargin{1}), 'database')
            c = varargin{1};
            tmp = tokenize(c.URL, ':');
            partial_key = [tmp{end - 1} ':' tmp{end}(1:end - 1) '|' c.Instance '|' c.UserName];
            u = memory_udb.get_no_err_no_ddot('open_databases');
            key_ = u{strmatch(partial_key, u)};
            close(c);
            DEL_KEY( key_);
            c = [];
        else
            error('userdatabase:exec', 'Unexpected type for first optionnal argument (expected ones are char or database object)');
        end
        if isempty( c)
            conn_params = tokenize(key_, '|');
            if isfield(st_version.my_env, 'kepche_host') && st_version.my_env.kepche_host
                server_port = tokenize(conn_params{1}, ':');
                alias_table_str = ...
                    {'ASE-SIRIUS','10.157.70.134','tcsyb019.caic.com'; ...
                    'BILBO','10.157.11.3','thsyb016.caic.com'; ...
                    'BSIRIUS','10.157.7.134','thsyb019.caic.com'; ...
                    'PREPA15','10.157.7.150','thsyb092.caic.com'; ...
                    'TCKPL001','10.157.77.116','TCKPL001.caic.com'; ...
                    'THDAS002','10.157.15.115','THDAS002.caic.com'; ...
                    'bksyb003','10.157.70.135','tcsyb003.caic.com'; ...
                    'bksyb019','10.157.7.134','thsyb019.caic.com'; ...
                    'localhost','127.0.0.1','padev929.pa.mscac.caic.com'; ...
                    'nysql001','10.157.242.7','nysql001.caic.com'; ...
                    'pasyb002-bis','10.157.7.150','thsyb092.caic.com'; ...
                    'pasyb005','10.157.11.5','thsyb005.caic.com'; ...
                    'pasyb016','10.157.11.3','thsyb016.caic.com'; ...
                    'pasyb043','10.157.11.1','thsyb043.caic.com'; ...
                    'thmlb006','10.157.18.59','thmlb006.caic.com'; ...
                    'thsyb073','10.157.11.2','thsyb073.caic.com'; ...
                    'vega','10.157.7.135','thsyb003.caic.com'};
                idx = strcmp(server_port{1}, alias_table_str(:, 1));
                assert(sum(idx) == 1, 'userdatabase:alias_resolving', ...
                    'Problem with Alias : <%s>', server_port{1});
                conn_params{1} = [alias_table_str{idx, 2} ':' server_port{2}];
            end
            c = database(conn_params{2:end}, ...
                'com.sybase.jdbc3.jdbc.SybDriver', ...
                ['jdbc:sybase:Tds:' conn_params{1} '/']);
            if ~isempty( c.Message)
                error('userdatabase:connection', ['Probable cause is either the server is down, or your login/pssword is wrong' ...
                    ' TEST THIS HYPOTHESIS BY TRYING TO CONNECT WITH ASEISQL with user : <%s> and password <%s> ' ...
                    ' IF THIS TEST DOES NOT REPRODUCE the ERROR, then perhaps <%s> is no longer what it used to be ' ...
                    '\nConnection have failed. URL = [%s]\n Message = %s\n'],...
                    conn_params{3:4}, conn_params{2},c.URL, c.Message);
            end
            memory_udb.set(key_, c);
            ADD_KEY(key_);
        end
    case 'mysql'
        conn_dico = memory_udb.get_no_err_no_ddot('mysql');
        if ischar(varargin{1})
            key_ = conn_dico.get_no_err_no_ddot(varargin{1});
            if isempty(key_)
                error('userdatabase:exec', 'Unknown server : <%s>', varargin{1});
            end
            c = memory_udb.get_no_err_no_ddot(key_);
        elseif strcmp(class(varargin{1}), 'database')
            c = varargin{1};
            tmp = tokenize(c.URL, ':');
            partial_key = [tmp{end - 1} ':' tmp{end}(1:end - 1) '|' c.Instance '|' c.UserName];
            u = memory_udb.get_no_err_no_ddot('open_databases');
            key_ = u{strmatch(partial_key, u)};
            close(c);
            DEL_KEY( key_);
            c = [];
        else
            error('userdatabase:exec', 'Unexpected type for first optionnal argument (expected ones are char or database object)');
        end
        if isempty( c)
            conn_params = tokenize(key_, '|');
            c = database(conn_params{2:end}, ...
                'com.mysql.jdbc.Driver', ...
                ['jdbc:mysql://' conn_params{1} '/' conn_params{2}]);
            if ~isempty( c.Message)
                error('userdatabase:connection', 'Connection have failed. URL = [%s]\n Message = %s\n',...
                    c.URL, c.Message);
            end
            memory_udb.set(key_, c);
            ADD_KEY(key_);
        end
        % Modif ajout driver sqlserver
        
    case 'sqlserver'
        conn_dico = memory_udb.get_no_err_no_ddot('sqlserver');
        if ischar(varargin{1})
            key_ = conn_dico.get_no_err_no_ddot(varargin{1});
            if isempty(key_)
                error('userdatabase:exec', 'Unknown server : <%s>', varargin{1});
            end
            c = memory_udb.get_no_err_no_ddot(key_);
        elseif strcmp(class(varargin{1}), 'database')
            c = varargin{1};
            tmp = tokenize(c.URL, ':');
            partial_key = [tmp{end - 1} ':' tmp{end}(1:end - 1) '|' c.Instance '|' c.UserName];
            u = memory_udb.get_no_err_no_ddot('open_databases');
            key_ = u{strmatch(partial_key, u)};
            close(c);
            DEL_KEY( key_);
            c = [];
        else
            error('userdatabase:exec', 'Unexpected type for first optionnal argument (expected ones are char or database object)');
        end
        if isempty( c)
            conn_params = regexp(key_,'\|(?!$)','split');
            conn_params{end} = strrep(conn_params{end},'|',''); % Remove trailing separator
            if isfield(st_version.my_env, 'kepche_host') && st_version.my_env.kepche_host
                server_port = tokenize(conn_params{1}, ':');
                alias_table_str = ...
                    {'THDAS002', '10.157.15.115', 'THDAS002.caic.com'; ...
                     'TCKPL001', '10.157.77.116', 'tckpl001.caic.com'; ...
                     '10.157.77.116', '10.157.77.116', '10.157.77.116'; ... TCKPL001
                     '10.157.15.115', '10.157.15.115', '10.157.15.115'; ... THDAS002
                     'LUIDBQ01','LUIDBQ01','LUIDBQ01';...
                     'LUISRV16','LUISRV16','LUISRV16';...
                     'TELTDB02','TELTDB02','TELTDB02';...
                     'KECH-DB03','KECH-DB03','KECH-DB03';...
                     'KEHO-DB02','KEHO-DB02','KEHO-DB02';...
                     'KECH-DB04','KECH-DB04','KECH-DB04';...
                     'KECH-DB03-dev','KECH-DB03-dev','KECH-DB03-dev';...
                     'KECH-DB03-UAT','KECH-DB03-UAT','KECH-DB03-UAT';...
                     'KECH-DB02','KECH-DB02','KECH-DB02';...
                     'KECH-DB03-rec','KECH-DB03-rec','KECH-DB03-rec';...
                     'TELTDB04','TELTDB04','TELTDB04';...
                     'KECH-SQL03','KECH-SQL03','KECH-SQL03';...
                     'TELSQL03-DEV\BDDINST01','TELSQL03-DEV\BDDINST01','TELSQL03-DEV\BDDINST01';...
                     'TLHSQL03-UAT\BDDINST01', 'TLHSQL03-UAT\BDDINST01', 'TLHSQL03-UAT\BDDINST01'};
                idx = strcmp(server_port{1}, alias_table_str(:, 1));
                assert(sum(idx) == 1, 'userdatabase:alias_resolving', ...
                    'Problem with Alias : <%s>', server_port{1});
                % Dealing with connexion 
                temp = regexp(alias_table_str{idx, 2},'\\','split');
                conn_params{1} = [temp{1} ':' server_port{2}];
                if length(temp) > 1
                    conn_params{2} = [conn_params{2},';instance=',temp{2}];
                end
            else
                server_port = regexp(conn_params{1},':','split');
                temp = regexp(server_port{1},'\\','split');
                conn_params{1} = [temp{1} ':' server_port{2}];
                if length(temp) > 1
                    conn_params{2} = [conn_params{2},';instance=',temp{2}];
                end
            end
            c = database('',conn_params{3}, conn_params{4}, ...
                'net.sourceforge.jtds.jdbc.Driver', ...
                strcat('jdbc:jtds:sqlserver://',conn_params{1},'/',conn_params{2},sprintf(';%s',conn_params{5:end})));
            if ~isempty( c.Message)
                error('userdatabase:connection', 'Connection have failed. URL = [%s]\n Message = %s\n',...
                    c.URL, c.Message);
            end
            memory_udb.set(key_, c);
            ADD_KEY(key_);
        end
        
        %
    case 'close'
        if strcmpi( varargin{1}, 'sle')
            c = memory_udb.get_no_err_no_ddot('sle');
            c.dispose();
            memory_udb.set('sle', []);
            DEL_KEY( 'sle');
            return
        end
        k = [varargin{:}];
        c = memory_udb.get_no_err_no_ddot(k);
        if isempty( c)
            st_log('nothing to close at <%s>\n', [varargin{:}]);
        else
            close(c);
            memory_udb.set(k, []);
            DEL_KEY( k);
        end
    case 'get_open_databases'
        c = memory_udb.get();
    case 'reset'
        % nothing
    otherwise
        error('userdatabase:mode', 'mode <%s> unknown', mode);
end

    function ADD_KEY( k)
        u = memory_udb.get_no_err_no_ddot('open_databases');
        memory_udb.set('open_databases', cat(1, u, {k}));
    end

    function DEL_KEY( k)
        u = memory_udb.get_no_err_no_ddot('open_databases');
        memory_udb.set('open_databases', setdiff(u, { k}));
    end

end