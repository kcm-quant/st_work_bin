function gkvol = GKvolatility(open, high, low, close)
% GKVOLATILITY - Computes GK volatility
% 
% References:
% [1] On the Estimation of Security Price Volatilities from Historical Data
%     Mark B Garman and Michael J Klass
%     The Journal of Business, 1980, vol. 53, issue 1, 67-78


gkvol = sqrt((high - low).^2/2 - (2*log(2)-1).*(close - open).^2)./...
    ((open+high+low+close)/4);