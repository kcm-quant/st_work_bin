function bufferize_deal_book(from,to,universe)
% bufferize_deal_book('01/01/2013','31/12/2013',get_repository('index-comp','DJ STOXX 600'))
% bufferize_deal_book('28/10/2013','31/12/2013',110)

% HARD CODED VARIABLES
STATE_FILE_PATH = ['/quant/usr/',getenv('USER'),'/bufferize_deal_book'];
MAX_NB_JOB = 100;
if ~exist(STATE_FILE_PATH,'dir')
    mkdir(STATE_FILE_PATH)
end

% Clean up of STATE_FILE_PATH
delete(fullfile(STATE_FILE_PATH,'*'));
if exist(fullfile(STATE_FILE_PATH,'log'),'dir')
    delete(fullfile(STATE_FILE_PATH,'log','*'))
end

% Variables needed for the script
uni_size = length(universe);
from_num = datenum(from,'dd/mm/yyyy');
to_num = datenum(to,'dd/mm/yyyy');
date_list = (from_num:to_num)';
week_num = weeknum(date_list);
week_num = week_num-week_num(1)+1;
nb_week = max(week_num);
num_el = nb_week*uni_size;
beg_week  = accumarray(week_num,date_list,[],@min);
end_week = accumarray(week_num,date_list,[],@max);

jm = findResource('scheduler','type','jobmanager','name','quant_jm','LookupURL','tcmlb001'); %#ok<REMFF1>

job_size = ceil(num_el/MAX_NB_JOB);
nb_job = ceil(num_el/job_size); % Forcément nb_job <= MAX_NB_JOB !

% Job creation
job_list = cell(nb_job,1);
% initialisation:
idx = (1:job_size)';
for i = 1:nb_job
    beg_date = beg_week(mod(idx-1,nb_week)+1);
    end_date = end_week(mod(idx-1,nb_week)+1);
    sec_list = universe(floor((idx-1)/nb_week)+1);
    job_list{i} = createJob(jm);
    createTask(job_list{i},...
        @(f,t,s,n)vect_fun(f,t,s,n),...
        0,{beg_date,end_date,sec_list,fullfile(STATE_FILE_PATH,['job_',num2str(i)])});
    submit(job_list{i});
    
    % idx for next loop
    idx = idx(end)+(1:job_size)';
    idx(idx>num_el) = [];
end

% All jobs have been submitted: monitoring of the process
files = dir(fullfile(STATE_FILE_PATH,'job_*'));
files = {files.name};
temp = regexp(files,'^job_[0-9]+_([0-9]+$)','tokens','once');
nb_task_done = str2double(cellfun(@(x)x,temp));
tot_nb_task_done = sum(nb_task_done);
while tot_nb_task_done < num_el
    files = dir(fullfile(STATE_FILE_PATH,'job_*'));
    files = {files.name};
    temp = regexp(files,'^job_[0-9]+_([0-9]+$)','tokens','once');
    nb_task_done = str2double(cellfun(@(x)x,temp));
    tot_nb_task_done = sum(nb_task_done);
    pct_done = tot_nb_task_done/num_el*100;
    fid = fopen(fullfile(STATE_FILE_PATH,'global_status'),'w');
    fprintf(fid,'%0.1f\n',pct_done);
    fclose(fid);
    pause(10)
end
cellfun(@destroy,job_list);

    % Fonction de vectorisation
    function vect_fun(from,to,universe,job_name)
        fid_job = fopen([job_name,'_',num2str(0)],'w');
        fprintf(fid_job,'');
        fclose(fid_job);
        job_num = str2double(char(regexp(job_name,'^[\w\\/]+job_([0-9]+)$','tokens','once')));
        path_log = fullfile(fileparts(job_name),'log');
        if ~exist(path_log,'dir')
            mkdir(path_log);
        end
        for j = 1:length(universe)
            try
                flow_dec('daily','from',datestr(from(j),'dd/mm/yyyy'),...
                    'to',datestr(to(j),'dd/mm/yyyy'),'security_id',universe(j));
            catch e
                stack = [{e.stack.file};{e.stack.name};{e.stack.line}];
                stack = sprintf('file: %s\nname: %s\nline: %d\n',stack{:});
                error_message = sprintf('Loop - %d: %s\n%s',j,e.message(1:end-1),stack);
                fid_log = fopen(fullfile(path_log,['job_',num2str(job_num),'.log']),'a+');
                fprintf(fid_log,'%s',error_message);
                fclose(fid_log);
            end
            movefile([job_name,'_',num2str(j-1)],[job_name,'_',num2str(j)])
        end
    end

end