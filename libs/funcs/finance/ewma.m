function sdata = ewma(mode, data, varargin)
    opt = options({'fixed-coeff', 0.95,...
                    }, varargin);
    switch lower(mode)
        case 'id-fix'
            sdata = data;
            days = unique(floor(data.date));
            sdata.value = [];
            fc = opt.get('fixed-coeff');
            for i = 1 : length(days)
                idx = floor(data.date) == days(i);
                curr_data = data.value(idx, :);
                for j = 2 : size(curr_data, 1)
                    curr_data(j, :) = fc * curr_data(j-1, :) + (1 - fc) * curr_data(j, :);
                end
                sdata.value = [sdata.value; curr_data];
            end
        case 'id-fix-no0' % ne fonctionne que pour une colonne
            sdata = data;
            days = unique(floor(data.date));
            sdata.value = [];
            fc = opt.get('fixed-coeff');
            for i = 1 : length(days)
                idx = floor(data.date) == days(i);
                curr_data = data.value(idx, 1);
                for j = 2 : size(curr_data, 1)
                    if ~isnan(curr_data(j, 1)) && curr_data(j, 1) 
                        curr_data(j, 1) = fc * curr_data(j-1, 1) + (1 - fc) * curr_data(j, 1);
                    else
                        curr_data(j, 1) = curr_data(j-1, 1);
                    end
                end
                sdata.value = [sdata.value; curr_data];
            end
        case 'id-fix-no0-squared' % ne fonctionne que pour une colonne
            data.value = data.value.^2;
            sdata = ewma('id-fix-no0', data, varargin{:});
            sdata.value = sdata.value.^0.5;
        otherwise
            error('Error in ewma, mode <%s> unknown', mode);
    end
end