function varargout = flow_dec( mode, varargin)
% FLOW_DEC - flow decomposition
%
% h = flow_dec('plot', row_data, dec_data [, 'N', 40, 'debug', false, 'ylim', [y0 y1]])
%
% Example:
%   data = read_dataset('tick4bi', 'security_id', sec_id, 'from', dt,  'to', dt , 'trading-destinations', { 'main' } )
%   dec_data = flow_dec( 'estimate', data)
%   flow_dec('plot', data, dec_data);
%
%   data_u = flow_dec('daily', 'security_id', 'FTE.PA', 'from', '20/03/2012', 'to', '27/03/2012' )
%
%   flow_dec('pretty-plot', stats);
%   flow_dec('green-plot', data_u, 'roll-date', '');
%
%   data_rep2 = flow_dec('index-report', stats2);data_rep1 = flow_dec('index-report', stats1)
%   data_merged = flow_dec('merge-index', { data_rep1, data_rep2 })
%   st_data('2csv', data_merged, '../data/test_merged.csv')
%
% integration with from_buffer
%   data = from_buffer( 'flow_dec', 'dealbook', 'from', '26/03/2012', 'to', '26/03/2012', 'security_id', 110)
%   data_fd = read_dataset('flowdec', 'security_id', 'FTE.PA', 'from', '20/03/2012', 'to', '27/03/2012' )
%   n = flow_dec('clean', bad_ids);

global st_version

persistent dealbook_mode
if isempty(dealbook_mode)
    dealbook_mode = false;
end

varargout = {};

switch lower( mode)
    
    case 'masterkey' % gestion
        %<* Return the generic+specific dir
        fmode = varargin{1};
        fmode = regexprep( fmode, '[ |*|\\|/|:|?|"|<|>|\|]', '_');
        opt = options({'security_id', NaN}, varargin(2:end));
        sec_id = opt.get('security_id');
        if ~isnumeric( sec_id) && numel(sec_id)==1
            error('flow_dec:secid', 'I need an unique numerical security id');
        end
        varargout = { fullfile(fmode, get_repository( 'security-key', sec_id)) };
        %>*
        
    case 'clean' % gestion
        %<* Clean the buffers
        sec_ids = varargin{1};
        opts = options({'repository', fullfile( st_version.my_env.st_repository, 'flow_dec', 'dealbook')}, varargin(2:end));
        u=1;
        for s=1:length(sec_ids)
            st_log('flow_dec: removing repository for <%d>...\n', sec_ids(s));
            rcode = get_repository('sec_id2ric', sec_ids(s));
            if isempty( rcode)
                st_log('flow_dec: clean, no RIC found, try to see if it is an US stock...\n');
                rcode = get_repository('sec_id2ticker', sec_ids(s));
            end
            rcode=rcode{1};
            if isempty(rcode)
                fpritnf('No code found.\n');
            else
                fname = fullfile( opts.get('repository'), rcode);
                try
                    rmdir( fname, 's');
                    st_log('<%s> removed.\n', fname);
                    u=u+1;
                catch
                    fprintf('error on <%s>\n', fname);
                end
            end
        end
        varargout = { u};
        %>*
        
    case 'dealbook' % donn�es
        % Read into the tickdb
        dealbook_mode = true;
        opt = options({'security_id', NaN, ...
                       'day', '??/??/????',...
                       'date_format:char', 'dd/mm/yyyy',....
                       'decay','spread'}, ... %ou 'tick'
                       varargin);
        % Lecture des options
        format_date = opt.get('date_format:char');
        sec_id = opt.get('security_id');
        if ~isnumeric( sec_id) && numel(sec_id)==1
            error('dec_flow:secid', 'I need an unique numerical security id');
        end
        day_    = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        % get the data for the day
       
        % PATCH US
        % si c'est NY on prend toutes les
        % destinations de trading (cf. bande consolid�e)
        td_info = get_repository('tdinfo',sec_id);
        if ~isempty(td_info) && isfield(td_info,'timezone') && strcmp(td_info(1).timezone,'America/New_York')
            data = read_dataset('tick4bi', 'security_id', sec_id, 'from', day_,  ...
                'to', day_ , 'trading-destinations', {}, 'today_allowed',  day_ == today );
            data = st_data('where',data,'{trading_destination_id}~=96&{trading_destination_id}~=97');
        elseif ~isempty(td_info) && isfield(td_info,'timezone')
            data = read_dataset('tick4bi', 'security_id', sec_id, 'from', day_,  ...
                'to', day_ , 'trading-destinations', { 'main' }, 'today_allowed', day_ == today);
        else
            error('No td_info field');
        end
        % FIN DU PATCH US
        
        % decomposition
        sec_txt = num2str(sec_id);
        day_str = datestr( day_, 'dd/mm/yy');
        st_log('flow_dec:work decomposing the flow for <%s> at <%s>...\n', sec_txt, day_str);
        t0 = clock;
        [~,e] = st_data('isempty', data);
        if ~e && sum(1-st_data('cols',data,'auction')) > 1
            dec_data = flow_dec( 'estimate', data, opt.get('decay'));
        else
            dec_data = st_data('empty');
        end
        dec_data.title = sprintf('Flow decomposition on %s (%s)', sec_txt, day_str);
        st_log('done in %5.2f seconds.\n', etime(clock, t0));
        varargout = { dec_data };
        
    case 'estimate' % donn�es
        % HARD CODED VARIABLES
        MIN_PRICE_STEP = 1e-6;
        % data
        data = varargin{1};
        ac = st_data('col', data, 'auction');
        oc_dx = logical(st_data('col', data, 'opening_auction;closing_auction'));
        idx_close   = find( st_data('col', data,'closing_auction'), 1);
        if isempty( idx_close)
            % Patch : If no closing auction one takes the price of the last trade
            prc_close = st_data('cols',data,'price',length(data.date));
        else
            prc_close = st_data('col', data, 'price', idx_close);
        end
        volume_close = sum(st_data('col', data, 'volume', oc_dx(:,2)));
        idx_open    = find( st_data('col', data,'opening_auction'), 1, 'last');
        if isempty( idx_open)
            prc_open = NaN;
        else
            prc_open = st_data('col', data, 'price', idx_open);
        end
        volume_open = sum(st_data('col', data, 'volume', oc_dx(:,1)));
        
        bapvdp   = st_data('col', data, 'bid;ask;bid_size;ask_size;price;volume', ~ac);
        is_pr_ba = round(bapvdp(:,5)/MIN_PRICE_STEP)==round(bapvdp(:,1)/MIN_PRICE_STEP) | ...
            round(bapvdp(:,5)/MIN_PRICE_STEP)==round(bapvdp(:,2)/MIN_PRICE_STEP);
        
        % On d�termine si c'est un trade � l'achat ou � la vente.
        % Dans le cas o� prix = mid on choisi au hasard.
        is_sell = round((bapvdp(:,1)+ bapvdp(:,2))/MIN_PRICE_STEP) > ...
            round(2*bapvdp(:,5)/MIN_PRICE_STEP);
        is_mid  = round((bapvdp(:,1)+ bapvdp(:,2))/MIN_PRICE_STEP) == ...
            round(2*bapvdp(:,5)/MIN_PRICE_STEP);
        is_sell(is_mid) = logical(randi(2,[sum(is_mid),1])-1); % Tirage al�atoire
        bapvdp  = [bapvdp ,is_sell];
        % On traite des deux possibilit�s achat/vente en cas d'incertitude.
        rand_ba_size = bapvdp(is_mid,[3:4,3:4,6]);
        
        %% CORRECTIONS ON DEAL DATA
        qty_correction_option1 = true; % correction generale plus r�aliste: max(size, real volume)
        qty_correction_option1_bis = false; % correction de la taille du c�t� oppos� au trade
        qty_correction_option2 = false; % correction si prix<>ask / bid: opposite size redondante
        prc_correction = true; % Correction inutile car une fois d�termin� le sens de traitement, deal/book est insensible aux prix.
        
        if qty_correction_option1            
            idx_wrong_limit_size = (bapvdp(:,7)==1&(bapvdp(:,3)<bapvdp(:,6))) |...
                (bapvdp(:,7)==0&(bapvdp(:,4)<bapvdp(:,6)));
            fprintf( 'flow_dec: %d corrections on the bid size (total nb trades: %d)\n',...
                sum(bapvdp(:,7)==1&(bapvdp(:,3)<bapvdp(:,6))), size(bapvdp,1));
            fprintf( 'flow_dec: %d corrections on the ask size (total nb trades: %d)\n',...
                sum(bapvdp(:,7)==0&(bapvdp(:,4)<bapvdp(:,6))), size(bapvdp,1));
            
            % Si "vente" bid_size = max(bid_size,trade_size) :
            bapvdp(bapvdp(:,7)==1,3) = max(bapvdp(bapvdp(:,7)==1,3), bapvdp(bapvdp(:,7)==1,6));
            rand_ba_size(:,1) = max(bapvdp(is_mid,3), bapvdp(is_mid,6));
            % Si "achat" ask_size = max(ask_size,trade_size) :
            bapvdp(bapvdp(:,7)==0,4) = max(bapvdp(bapvdp(:,7)==0,4), bapvdp(bapvdp(:,7)==0,6));
            rand_ba_size(:,4) = max(bapvdp(is_mid,4), bapvdp(is_mid,6));
        end
        
        if qty_correction_option1_bis
            for i = 2:size(bapvdp,1)
                if idx_wrong_limit_size(i)==1 %� la vente
                    bapvdp(i,4) = bapvdp(i,3)*(bapvdp(i-1,4)/bapvdp(i-1,3));
                else %� l'achat
                    bapvdp(i,3) = bapvdp(i,4)*(bapvdp(i-1,3)/bapvdp(i-1,4));
                end
            end
        end
        
        if qty_correction_option2
            %< QUANTITIES / OPTION 2 (corrections if price differs from bid or ask)
            % BID_SIZE - if the price is closest to the ask: keeps the bid_size
            % BID_SIZE - if the price is closest to the bid: takes the ask_size
            bapvdp(:,3) = bapvdp(:,3).*is_pr_ba +...
                (bapvdp(:,4).*bapvdp(:,7)+ bapvdp(:,3).*(1-bapvdp(:,7))).*(1-is_pr_ba); %#ok<UNRCH>
            % ASK_SIZE - if the price is closest to the ask: takes the bid_size
            % ASK_SIZE - if the price is closest to the bid: keeps the ask_size
            bapvdp(:,4) = bapvdp(:,4).*is_pr_ba +...
                (bapvdp(:,4).*bapvdp(:,7)+ bapvdp(:,3).*(1-bapvdp(:,7))).*(1-is_pr_ba);
            %>
        end
        
        if prc_correction
            % Correction des prix bid-ask avec maximum de vraisemblance.
            bid = bapvdp(:,1);
            ask = bapvdp(:,2);
            price = bapvdp(:,5);
            bid_prim = nan(length(price),1);
            ask_prim = nan(length(price),1);
            bid_prim(1) = bid(1);
            ask_prim(1) = ask(1);
            count = 0;
            for i = 2:length(price)
                if is_pr_ba(i)
                    bid_prim(i) = bid(i);
                    ask_prim(i) = ask(i);
                elseif bapvdp(:,7) == 1 % Si "vente"
                    bid_prim(i) = price(i);
                    ask_prim(i) = price(i) + ask_prim(i-1) - bid_prim(i-1);
                    count = count+1;
                else % Sinon, donc si "achat"
                    bid_prim(i) = price(i) - (ask_prim(i-1) - bid_prim(i-1));
                    ask_prim(i) = price(i);
                    count = count+1;
                end
            end
            bapvdp(:,1) = bid_prim;
            bapvdp(:,2) = ask_prim;
            fprintf('Nb of corrections on prices: %d -> Pct: %f\n',count,count*100/length(price));
        end
        
        % WARNING! replaces lines where a bid price or a ask price is null by the
        % previous line
        idx_zero_ba_prices = find(bapvdp(:,1)==0 | bapvdp(:,2)==0);
        if ismember(1, idx_zero_ba_prices)
            bapvdp(1,:) = [prc_open, prc_open, 1, 1, prc_open, 1, 1]; % bid = ask= prc_open et les qttes sur bid et ask � 1
            idx_zero_ba_prices = find(bapvdp(:,1)==0 | bapvdp(:,2)==0);
        end
        if ~isempty(idx_zero_ba_prices)
            for l = 1:length(idx_zero_ba_prices)
                bapvdp(idx_zero_ba_prices(l), :) = bapvdp(idx_zero_ba_prices(l)-1, :);
            end
        end
        
        %% CONTINUE ESTIMATION
        % implicit tick
        % On effectue le calcul du tick via bid et ask uniquement.
        % On �vite les probl�mes de deals qui ont lieu sur une grille plus petit que la grille de tick.
        tick = pgcd([pgcd(unique(round(bid/MIN_PRICE_STEP))),pgcd(unique(round(ask /MIN_PRICE_STEP)))])*MIN_PRICE_STEP;
        %         dp          = abs(diff(bapvdp(:,5)));
        %         tick        = min(dp(dp>0));
        assert(~isempty(tick),'FLOW_DEC:no_tick_size','Unable to compute tick size');
        assert(tick~=0,'FLOW_DEC:tick_size_null','Tick size = 0');
        
        % d�calage
        if nargin ==1
            decay_type = 'tick';
        else
            decay_type = varargin{2};
        end
        if strcmpi(decay_type, 'tick')
            decay = tick;
        else
            spr = nansum(bapvdp(:,6).*(bapvdp(:,2)-bapvdp(:,1)))/nansum(bapvdp(:,6));
            decay = max(tick,spr/2);
        end
        
        % Explicit price
        Ep = [NaN;  EXP_MARKET_PRICE( bapvdp(:,1:4), decay); NaN];
        
        % impacted book
        is_sell     = bapvdp(:,7);
        q_ba_after  = max(0,[bapvdp(:,3)-is_sell.*bapvdp(:,6), bapvdp(:,4)-(1-is_sell).*bapvdp(:,6)]);
%         q_ba        = [bapvdp(:,3), bapvdp(:,4)];
        
        % modifications en cas de quantit�s nulles avec une qt� en 2 ieme
        % limite egale � la qt� en 1 ere limite avant le deal
        % Adaptation apres commentaires de Nicolas
        
%         bapvdp_fake=bapvdp(:,1:2);
%         bapvdp_fake(q_ba_after(:,1)==0,1)=bapvdp_fake(q_ba_after(:,1)==0,1)-tick;
%         q_ba_after(q_ba_after(:,1)==0,1)=q_ba(q_ba_after(:,1)==0,1);
%         bapvdp_fake(q_ba_after(:,2)==0,2)=bapvdp_fake(q_ba_after(:,2)==0,2)+tick;
%         q_ba_after(q_ba_after(:,2)==0,2)=q_ba(q_ba_after(:,2)==0,2);
%         bapvdp_fake = [bapvdp_fake, q_ba_after];
        
        % Ancienne ligne remise en action pour corriger les cas extremes du
        % screening
        
        bapvdp_fake = [bapvdp(:,1:2), q_ba_after];
        
        % Explicit price post deal
        Ep_plus     = [prc_open; EXP_MARKET_PRICE( bapvdp_fake, decay); prc_close];
        % just for the close:
        %Ep(end) = Ep_plus(end-1); pour mettre le jump on close sur les
        %book pour que les deal effects restent plus proches des imbalances qui n integrent pas le jump on close  
        Ep(end) = prc_close;
        
        dts         = data.date(~ac);
        deal_impact = [0 ; Ep_plus(2:end) - Ep(2:end)       ];
        resiliency  = [0 ; Ep(2:end)      - Ep_plus(1:end-1)]; % the 1st zero could be a NaN
        
        % decide if the close volume is buy or sell oriented:
        if prc_close > bapvdp(end,5)
            w_buy = 1;
        elseif prc_close < bapvdp(end,5)
            w_buy = 0;
        else
            w_buy = 0.5;
        end
        
        % Estimation de l'incertitude due au deals au prix mid :
        D_deal_sell = tick/2*((diff(rand_ba_size(:,[5,1]),[],2) - rand_ba_size(:,2))./...
            max(diff(rand_ba_size(:,[5,1]),[],2),rand_ba_size(:,2)) - ...
            (rand_ba_size(:,1)-rand_ba_size(:,2))./max(rand_ba_size(:,1),rand_ba_size(:,2)));
        D_deal_buy  = tick/2*((rand_ba_size(:,3) - diff(rand_ba_size(:,[5,4]),[],2))./...
            max(rand_ba_size(:,3),diff(rand_ba_size(:,[5,4]),[],2)) - ...
            (rand_ba_size(:,3)-rand_ba_size(:,4))./max(rand_ba_size(:,3),rand_ba_size(:,4)));
        daily_biais = sum((D_deal_buy+D_deal_sell)/2);
        daily_error  = sqrt(sum((D_deal_buy-D_deal_sell).^2/4));
        
        % build the outputs:
        varargout = { st_data('init', 'title', data.title, 'value', [deal_impact, resiliency, Ep, Ep_plus, ...
            [ [volume_open/2, volume_open/2];...
                cumsum( [ [ bapvdp(:,6).*(1-is_sell), bapvdp(:,6).*is_sell]; [w_buy, (1-w_buy)]*volume_close]) ] ], ...
            'date', [dts(1); dts; dts(end)], 'colnames', { 'Deal Impact', 'Resiliency', 'Ep', 'Ep plus', 'Buy volume', 'Sell volume'}) };
        varargout{1}.info = struct('deal_daily_biase',daily_biais,'deal_daily_error',daily_error,'localtime',data.info.localtime,...
            'td_info',data.info.td_info);
        
    case 'daily' % donn�es
        %<* Daily d�composition
        opt         = options({'security_id', '', 'from', '', 'to', '', 'bloomberg', false, 'date-format', 'dd/mm/yyyy'}, varargin);
        sec_id      = opt.get('security_id');
        if opt.get('bloomberg')
            rrr    = get_repository('bloomberg', sec_id);
            sec_id = rrr.security_id;
        end
        
        dt_from_txt = opt.get('from');
        dt_to_txt   = opt.get('to');
        
        stats = [];
        st_log('Working on <%d>:\n', sec_id);
        
        dt_from_num = datenum( dt_from_txt, opt.get('date-format'));
        dt_to_num   = datenum( dt_to_txt  , opt.get('date-format'));
        for dt=dt_from_num:dt_to_num
            dec_data = read_dataset('flowdec',  'from', dt, 'to', dt, 'security_id', sec_id);
            [~,e] = st_data('isempty', dec_data);
            if ~e
                von_balance = st_data('col', dec_data, 'Buy volume;Sell volume');
                dec_data    = st_data('apply-formula', dec_data, '[nansum({Deal Impact}), nansum({Resiliency})]');                                 
                dec_data = st_data('add-col', dec_data, [von_balance(end-1,:), von_balance(end,:)-von_balance(end-1,:)], 'Buy volume;Sell volume;Buy at close;Sell at close');
                if isempty( stats)
                    stats = dec_data;
                else
                    stats = st_data('stack', stats, dec_data);
                end
            else
                st_log('No data for %d at %s.\n', sec_id, datestr(dt, 'dd/mm/yyyy'));
            end
        end
        if ~isempty( stats)
            td_info = get_repository('tdinfo',sec_id);
            gzsuff = td_info(1).global_zone_suffix;
            data_day = exec_sql('MARKET_DATA',['select td.trading_destination_id,td.date,td.open_prc,td.close_prc,td.volume',...
                ' from ',st_version.bases.market_data,'..trading_daily',gzsuff,' td,',...
                st_version.bases.repository,'..EXCHANGEREFCOMPL ec',...
                ' where td.date between ''',datestr(dt_from_num, 'yyyy-mm-dd'),'''',...
                ' and ''',datestr(dt_to_num, 'yyyy-mm-dd'),'''',...
                ' and td.trading_destination_id = ec.EXCHANGE',...
                ' and ec.EXCHGID not in (''TRQSE'',''CHIX'',''BATE'',''SIGMX'',''EQDCT'')',...
                ' and td.security_id = ',num2str(sec_id)]);
            if length(unique(cell2mat(data_day(:,1)))) >1
                idx = cell2mat(data_day(:,1))==td_info(1).trading_destination_id;
                data_day = data_day(idx,:);
            end
            date_day = datenum(data_day(:,2),'yyyy-mm-dd');
            [~,I] = sort(date_day);
            data_prc = st_data('init','date',date_day(I),'colnames',{'open_prc','close_prc','volume'},...
                'value',cell2mat(data_day(I,3:end)));
            data_prc = st_data('keep',data_prc, 'open_prc;close_prc;volume');
            data_prc.value = [[data_prc.value(1,1); data_prc.value(2:end,1) - data_prc.value(1:end-1,2)], data_prc.value(:,3)];
            data_prc.colnames = { 'Overnight move', 'Daily volume'};
            
            stats.date = floor(stats.date);
            data_u = st_data('union', { stats, data_prc});
        else
            data_u = [];
            st_log('flow_dec: empty output...');
        end
        
        varargout = { data_u };
        %>*
        
    case {'plot', 'intraday-plot'} % graphique
        data = varargin{1};
        dec  = varargin{2};
        
        opts = options({'debug', false, 'ylim', [], 'double-axe', true, ...
            'slice_sec_duration', 300}, varargin(3:end));
        
        %% Data
        bapvdp  = st_data('col', data, 'bid;ask;bid_size;ask_size;price;volume');
        
        Ep = st_data('col', dec, 'Ep');
        Ep_plus = st_data('col', dec, 'Ep plus');
        deal_impact = st_data('col', dec, 'Deal Impact');
        resiliency  = st_data('col', dec, 'Resiliency');
        
        % agg
        data = st_data('add-col', data, sign(st_data('cols', data, 'dbid')-st_data('cols', data, 'dask')), 'repl_sign');
        slice_sec_duration = opts.get('slice_sec_duration');
        data_agg = st_data('agg', data, ...
            'colnames',{'volume','imbalance_rel','imbalance_numer','imbalance_denom',...
            'replenishment_rel','replenishment_numer','replenishment_denom', 'avg_sizes','nb_deal',...
            'nb_deal_imbalance_rel','nb_deal_replenishment_rel',...
            'nb_deal_imbalance_numer','nb_deal_imbalance_denom','nb_deal_replenishment_numer','nb_deal_replenishment_denom'},...
            'grid_mode', 'time', ...
            'grid_options', {'time_type', 'seconds','window', slice_sec_duration,'step', slice_sec_duration});
        slice_min_duration = slice_sec_duration/60; 
        
        idx_wrong_h = find((diff(data_agg.date)-datenum(0,0,0,0,slice_min_duration/2,0))<0);
        data_agg.date = floor(data_agg.date)+datenum(0,0,0,hour(data_agg.date),...
            ceil(minute(data_agg.date)/slice_min_duration)*slice_min_duration,0)-datenum(0,0,0,0,slice_min_duration/2,0);
        data_agg.date(idx_wrong_h) = data_agg.date(idx_wrong_h-1)+datenum(0,0,0,0,slice_min_duration/2,0);
        
        imbalance = st_data('cols', data_agg, 'imbalance_rel');
        imbalance(isnan(imbalance)) = 0;
        imbalance_sep = st_data('cols', data_agg, 'imbalance_numer;imbalance_denom');
        imbalance_sep(isnan(imbalance_sep))=0;
        cum_imb = cumsum(imbalance_sep(:,1))./cumsum(imbalance_sep(:,2));
        
        nbd_imbalance = st_data('cols', data_agg, 'nb_deal_imbalance_rel');
        nbd_imbalance(isnan(nbd_imbalance)) = 0;
        nbd_imbalance_sep = st_data('cols', data_agg, 'nb_deal_imbalance_numer;nb_deal_imbalance_denom');
        nbd_imbalance_sep(isnan(nbd_imbalance_sep))=0;
        nbd_cum_imb = cumsum(nbd_imbalance_sep(:,1))./cumsum(nbd_imbalance_sep(:,2));
        
        repl = st_data('cols', data_agg, 'replenishment_rel');
        repl(isnan(repl)) = 0;
        repl_sep = st_data('cols', data_agg, 'replenishment_numer;replenishment_denom');
        repl_sep(isnan(repl_sep))=0;
        cum_repl = cumsum(repl_sep(:,1))./cumsum(repl_sep(:,2));
        
        nbd_repl = st_data('cols', data_agg, 'nb_deal_replenishment_rel');
        nbd_repl(isnan(nbd_repl)) = 0;
        nbd_repl_sep = st_data('cols', data_agg, 'nb_deal_replenishment_numer;nb_deal_replenishment_denom');
        nbd_repl_sep(isnan(nbd_repl_sep))=0;
        nbd_cum_repl = cumsum(nbd_repl_sep(:,1))./cumsum(nbd_repl_sep(:,2));
        
        % by deal: imbalance
        %         idx = st_data('cols', data, 'over_ask')>0;
        %         [dts_ask,~,idx_dts] = unique(round(mod(data.date(idx),1)*24*60*60*100e6));
        %         volume_ask = accumarray(idx_dts,st_data('cols',data,'volume', idx),[],@nansum);
        
        volume = st_data('cols', data, 'volume');
        idx = st_data('cols', data, 'over_ask')>0|st_data('cols', data, 'over_bid')>0;
        volume(~idx) = NaN;
        cum_avg = arrayfun(@(v) nanmean(volume(1:v)), (1:length(data.date))');
        
        volume_ask = st_data('cols', data, 'volume');
        volume_ask(st_data('cols', data, 'over_ask')~=1) = NaN;
        cum_avg_ask = arrayfun(@(v) nanmean(volume_ask(1:v)), (1:length(data.date))');
        volume_bid = st_data('cols', data, 'volume');
        volume_bid(st_data('cols', data, 'over_bid')~=1) = NaN;
        cum_avg_bid = arrayfun(@(v) nanmean(volume_bid(1:v)), (1:length(data.date))');
        end_imb_ATS = (cum_avg_ask(end)-cum_avg_bid(end))/cum_avg(end);
        
        cum_sum = nancumsum(volume);
        cum_ask = nancumsum(volume_ask);
        cum_bid = nancumsum(volume_bid);
        end_imb_volume = (cum_ask(end)-cum_bid(end))/cum_sum(end);
        
        cum_nb = cumsum(st_data('cols', data, 'over_ask')>0 | st_data('cols', data, 'over_bid')>0);
        cum_nb_ask = cumsum(st_data('cols', data, 'over_ask')>0);
        cum_nb_bid = cumsum(st_data('cols', data, 'over_bid')>0);
        cum_imb_nb = (cum_nb_ask-cum_nb_bid)./cum_nb;
        end_imb_nb = (cum_nb_ask(end)-cum_nb_bid(end))/cum_nb(end);
        
        % by deal: replenishment
        %             idx = st_data('cols', data, 'over_ask')>0|st_data('cols', data, 'over_bid')>0;
        %             bidt = (st_data('cols', data, 'over_bid')>0).*(st_data('cols', data,'bid_size')-st_data('cols', data,'volume'))...
        %                 +(st_data('cols', data, 'over_bid')==0).*st_data('cols', data,'bid_size');
        %             askt = (st_data('cols', data, 'over_ask')>0).*(st_data('cols', data,'ask_size')-st_data('cols', data,'volume'))...
        %                 +(st_data('cols', data, 'over_ask')==0).*st_data('cols', data,'ask_size');
        %             dbid = idx(1:end-1).*(st_data('cols', data,'bid_size',2:length(data.date))-bidt(1:end-1));
        %             dask = idx(1:end-1).*(st_data('cols', data,'ask_size',2:length(data.date))-askt(1:end-1));
            
        dbid = st_data('cols', data, 'dbid');
        dask = st_data('cols', data, 'dask');
        cum_numer_bid = cumsum(dbid);
        cum_numer_ask = cumsum(dask);
        cum_denom = cumsum(abs(dbid)+abs(dask));
        end_repl_volume = (cum_numer_bid(end)-cum_numer_ask(end))/cum_denom(end);
        
        cum_nb_repl_pos = cumsum((st_data('cols', data, 'dbid')-st_data('cols', data, 'dask'))>0);
        cum_nb_repl_neg = cumsum((st_data('cols', data, 'dbid')-st_data('cols', data, 'dask'))<0);
        cum_nb_repl = cum_nb;
        end_repl_nb = (cum_nb_repl_pos(end)-cum_nb_repl_neg(end))/cum_nb_repl(end);
        
        %% Plot specs
        cm = [51, 158, 210;... %bleu
            235, 125, 16;... %orange (248, 202, 156)
            191, 191, 191]/256; %gris
        g = cm_cheuvreux(1);
        dts = data.date;
        mima = [min(dts)-datenum(0,0,0,0,slice_min_duration,0) max(dts)+datenum(0,0,0,0,slice_min_duration/2,0)];
        sname = exec_sql('KGR',['select SECNAME from ',st_version.bases.repository,'..SECURITY',...
            ' where SYMBOL6 = ''',num2str(data.info.security_id),'''',...
            ' and STATUS = ''A''',...
            ' and PRIORITY = 1']);
        sname = sname{1};
        
        %% Intraday plot
        get_focus([sname ' - Intraday deal-book']); clf
        NS = 7;
        if NS<=5
            set(gcf,'position',[271   231   732   696])
        else
            set(gcf,'position',[1514 71 730 855])
        end
        
        % dP
        a = subplot(NS,1,1:2);
        di = NANCUMSUM(deal_impact)/bapvdp(1,5);
        dr =  NANCUMSUM(resiliency)/bapvdp(1,5);
        dp = (bapvdp(:,5) - bapvdp(1,5))/bapvdp(1,5);
        plot(dec.date, di, '-k', 'linewidth',2, 'color', cm(1,:));
        hold on
        plot(dec.date, dr, '-k', 'linewidth',2, 'color', cm(2,:));
        plot(data.date, dp, '-k','linewidth',2);
        plot(data.date([1 end]), [0 0], '--k');
        hold off
        xlim(mima);
        if ~isempty(opts.get('ylim'))
            ylim( opts.get('ylim'));
        end
        ylabel('Relative price variation');
        ht = stitle( '%s - %s\ndP: %.2f%% - dDeal: %.2f%% - dBook: %.2f%%',...
            sname, datestr( data.date(1), 'dd/mm/yyyy'),...
            100*(bapvdp(end,5)-bapvdp(1,5))/bapvdp(1,5), 100*di(end), 100*dr(end));
        set(ht,'FontWeight', 'bold');
        ax1=gca;
        ax2 = axes('Position',get(ax1,'Position'),...
            'XAxisLocation','bottom','YAxisLocation','right',...
            'Color','none','XColor','k','YColor','k');
        set(ax2, 'xtick', []);
        yt = get(ax1,'ytick');
        lim1 = axis(ax1);
        set(ax2,'ytick', yt+bapvdp(1,5));
        ylim(lim1(3:4)+bapvdp(1,5));
        axes(ax1);
        legend({'Deal effect', 'Book effect', 'Price'}, 'location', 'SouthWest');
        set(gca,'xticklabel',[],'xtick', unique(data_agg.date+datenum(0,0,0,0,slice_min_duration/2,0))) %'xgrid', 'on',
        if ~opts.get('double-axe')
            set(ax1, 'ytick', []);
            ylabel('Price');
        end
        
        % Volume
        s = 3;
        a(end+1)=subplot(NS,1,s);
        bar(data_agg.date, st_data('cols', data_agg, 'volume'), 'facecolor',  [170 160 149]/256);
        xlim(mima);
        ylabel('Volume');
        set(gca,'xtick', data_agg.date+datenum(0,0,0,0,slice_min_duration/2,0)) %,'xgrid', 'on'
        attach_date('init', 'date', mima, 'format', 'HH:MM'); %:SS
        
        % Taille en 1ere limite
        s = s+1;
        a(end+1) = subplot(NS,1,s);
        bar(data_agg.date, st_data('cols', data_agg, 'avg_sizes'), 'facecolor',  [170 160 149]/256);
        xlim(mima);
        ylabel('1st lim. qty')
        set(gca,'xgrid', 'on','xticklabel',[])

        % Nb deals
        s = s+1;
        a(end+1)=subplot(NS,1,s);
        bar(data_agg.date, st_data('cols', data_agg, 'nb_deal'), 'facecolor',  [170 160 149]/256);
        xlim(mima);
        ylabel('Nb deal');
        set(gca,'xgrid', 'on','xticklabel',[])

        % Imbalance & Replenishment
        YM = max(max([abs(imbalance), abs(repl)]))*1.1;
        YMc = max(max([abs(nbd_imbalance), abs(nbd_repl)]))*1.1; %cum_imb %nbd_cum_repl
        plot_mode = 'agg';
        
        % Imbalance
        s = s+1;
        if strcmpi(plot_mode, 'agg')
            subplot(NS,1,s);
            bar( data_agg.date, imbalance, 'edgecolor',[0 0 0], 'facecolor', [.7 .7 .7]);
            title( sprintf('Imbalance: %+.1f%% (N/%+.1f%%)', cum_imb(end)*100, end_imb_nb*100));
            xlim(mima);
            set(gca,'xticklabel',[],'xtick', data_agg.date+datenum(0,0,0,0,slice_min_duration/2,0),'ylim',YM*[-1 1]) %'xgrid', 'on',
            set_percentage_labels(gca,'y','text_precision','%+.0f%%')
            pause(1)
            ax1_imb = gca;
            ax2_imb = axes('Position',get(ax1_imb,'Position'),...
                'XAxisLocation','bottom','YAxisLocation','right',...
                'Color','none','XColor','k','YColor','k');
            set(ax2_imb, 'xtick', []);
            hold on
            plot(data_agg.date, nbd_imbalance, '-','color',cm(2,:),'parent',ax2_imb,'linewidth',2) %cum_imb
            set(ax2_imb,'xlim',mima,'ylim', YMc*[-1 1])
            set_percentage_labels(gca,'y','text_precision','%+.0f%%')
            hold off
            a(end+1) = ax1_imb;
            a(end+1) = ax2_imb;
        elseif strcmpi(plot_mode, 'cum')
            a(end+1) = subplot(NS,1,s);
            plot(data.date, (cum_ask-cum_bid)/cum_sum(end),'-k', 'linewidth', 2)
            hold on
            % plot(data.date, (cum_nb_ask-cum_nb_bid)/cum_nb(end),'-r', 'linewidth', 2)
            % plot(data.date, (cum_avg_ask-cum_avg_bid)/cum_avg(end),'-', 'linewidth', 2)
            hold off
            % legend({sprintf('Volume: %.1f%%',end_imb_volume), sprintf('Nb deal: %.1f%%', end_imb_nb)}) %sprintf('ATS: %.1f%%', end_imb_ATS)}
        end
        hold on
        plot(mima, 0*[1 1], '--k')
        hold off
        xlim(mima);
        set(gca,'xticklabel',[]) %'xgrid', 'on',
        
        % Replenishment
        s = s+1;
        if strcmpi(plot_mode, 'agg')
            subplot(NS,1,s);
            bar( data_agg.date, repl, 'edgecolor',[0 0 0], 'facecolor', [.7 .7 .7]);
            xlim(mima);
            set(gca, 'xticklabel',[],'xtick', data_agg.date+datenum(0,0,0,0,slice_min_duration/2,0),'ylim',YM*[-1 1]) %'xgrid', 'on',
            attach_date('init', 'date', mima, 'format', 'HH:MM'); %:SS
            title(sprintf('Replenishment: %+.1f%% (N/%+.1f%%)', cum_repl(end)*100,end_repl_nb*100));
            set_percentage_labels(gca,'y','text_precision','%+.0f%%')
            pause(1)
            ax1_repl = gca;
            ax2_repl = axes('Position',get(ax1_repl,'Position'),...
                'XAxisLocation','bottom','YAxisLocation','right',...
                'Color','none','XColor','k','YColor','k');
            set(ax2_repl, 'xtick', []);
            hold on
            plot( data_agg.date, nbd_repl, '-','color',cm(2,:),'parent',ax2_repl,'linewidth',2)
            set(ax2_repl,'xlim',mima,'ylim',YMc*[-1 1])
            set_percentage_labels(gca,'y','text_precision','%+.0f%%')
            hold off
            a(end+1) = ax1_repl;
            a(end+1) = ax2_repl;
        elseif strcmpi(plot_mode, 'cum')
            a(end+1) = subplot(NS,1,s);
            plot(data.date, (cum_numer_bid-cum_numer_ask)/cum_denom(end),'-k', 'linewidth', 2)
            hold on
            % plot(data.date, (cum_nb_repl_pos-cum_nb_repl_neg)/cum_nb_repl(end),'-r', 'linewidth', 2)
            % legend({sprintf('Volume: %.1f%%',end_repl_volume) , sprintf('Nb deal: %.1f%%', end_repl_nb)})
            hold off
            xlim(mima);
            attach_date('init', 'date', mima, 'format', 'HH:MM'); %:SS
        end
        hold on
        plot(mima, 0*[1 1], '--k')
        hold off
        
        linkaxes(a,'x');
        
        varargout = {[end_imb_nb,end_repl_nb]};
        
        %% Intraday plot (debug)
        if opts.get('debug')
            get_focus('Prices');
            a1=subplot(2,1,1);
            h=stairs( data.date, bapvdp(:,1:2), '-k', 'color', cm(1,:));
            hold on
            h=[h(1),plot( data.date, (bapvdp(:,1)+bapvdp(:,2))/2, '+b', 'color', cm(2,:))];
            h=[h,plot( data.date, bapvdp(:,5), '*r')];
            h=[h,plot( dec.date, Ep, 'og')];
            h=[h,plot( dec.date, Ep_plus, 'd', 'color',[.5 .1 0])];
            hold off
            xlim(mima);
            set(gca,'xticklabel',[]);
            legend(h, {'bid-ask', 'mid', 'deal', 'Ep', 'Ep+'})
            title( dec.title);
            ylabel('Prices');
            
            a2=subplot(2,1,2);
            plot(dec.date, deal_impact, '+k', dec.date, resiliency, 'or');
            legend({'Deal impact', 'Resilience'})
            xlim(mima);
            ylabel('dP');
            attach_date('init', 'date', mima, 'format', 'HH:MM:SS');
            
            if false
                a4=subplot(2,1,2); %#ok<UNRCH>
                plot(data.date, cumsum(st_data('col', data, 'volume'))/sum(st_data('col', data, 'volume')), '-k', 'linewidth',2);
                hold on
                
                plot( dec.date,  cumsum(st_data('col', dec, 'Buy volume'))/sum(st_data('col', dec, 'Buy volume')) , '-r', 'linewidth',2, 'color', cm(1,:));
                plot( dec.date, -cumsum(st_data('col', dec, 'Sell volume'))/sum(st_data('col', dec, 'Sell volume')), '-k', 'linewidth',2, 'color', cm(3,:));
                hold off
                attach_date('init', 'date', mima, 'format', 'HH:MM:SS', 'tick-size', 16);
                legend({'Cumulated volumes', 'Cum. buy volumes', 'Cum. sell volumes'})
                
                % % %         if false % smoothing (desactivated)
                % % %             a4=subplot(2,1,2);
                % % %             plot(dec.date, filter( ones(1,N)/N,1,deal_impact), '-k', 'linewidth',2, 'color', cm(3,:));
                % % %             hold on
                % % %             plot( dec.date, filter( ones(1,N)/N,1,resiliency), '-r', 'linewidth',2, 'color', cm(1,:));
                % % %             plot( data.date(2:end), filter( ones(1,N)/N,1,diff(bapvdp(:,5))), '-k', 'linewidth',2);
                % % %             hold off
                % % %             xlim(mima);
                % % %             ylabel( sprintf('Local sum (on %d trades)', N));
                % % %             attach_date('init', 'date', mima, 'format', 'HH:MM:SS', 'C:\temp\matlab', 16);
                % % %             legend({'Deal effect', 'Book effect', 'Price'});
                % % %         end
                
                linkaxes([a1,a2,a4],'x');
            else
                linkaxes([a1,a2],'x');
            end
        end

    case {'gplot', 'green-plot', 'daily-plot'} % graphique
        stats = varargin{1};
        opt= options({'roll-date', '', 'date-format', 'dd/mm/yyyy'}, varargin(2:end));
        dt_roll = opt.get('roll-date');
        is_roll = ~isempty( dt_roll);
        c = cm_kepler(5);
        sec_id = regexp( stats.title, '.* ([0-9]+) \(.*', 'tokens', 'once');
        sec_id = sec_id{1};
        name = exec_sql('KGR',['select SECNAME from ',st_version.bases.repository,'..SECURITY',...
            ' where SYMBOL6 = ''',sec_id,'''',...
            ' and STATUS = ''A''',...
            ' and PRIORITY = 1']);
        name = name{1};
        
        dts = stats.date; 
        dl  = st_data('col', stats, 'nansum(Deal Impact);nansum(Resiliency);Overnight move');
        first_open = dl(1,3);
        prices     = sum(cumsum(dl),2);
        dl(1,3)    = 0;
        vbs  = st_data('col', stats, 'Buy volume;Sell volume;Buy at close;Sell at close');
        vdaily = (vbs(:,1)+vbs(:,2)+vbs(:,3)+vbs(:,4));
        
        hs = get_focus('EF'); clf % FABRICATION DU GRAPHE   
        pos = [296   207   688   653];
        set(gcf, 'position', pos)
        dts_idx = 1:length(dts);
        if is_roll
            dt_roll_num  = datenum(dt_roll, 'dd/mm/yyyy');
            idx_roll_num = find( dt_roll_num<=dts, 1);
        end
        
        %1er SS GRAPH
        a1= subplot(10,1,1:2);
        hold on
        plot( dts_idx, prices, 'k','linewidth',2);
        if is_roll
            plot([idx_roll_num idx_roll_num], get(gca, 'ylim'), 'color', 'black')
        end
        hold off
        ylabel('Prices');
        stitle( '%s (%s) - Open(0)=%5.2f', name, sec_id, first_open);
        set(gca, 'xlim',[ 1 length(dts) ],'xticklabel', [], 'ygrid', 'on');
        
        %3eme SS GRAPH
        a3 = subplot(10,1,3:5);
        all_cumulated = first_open+cumsum(dl);
        hold on
        plot( dts_idx, all_cumulated(:,1), '--', 'color', c(1,:), 'linewidth',2);
        plot( dts_idx, all_cumulated(:,2), 'color', c(1,:), 'linewidth',2);
        plot( dts_idx, all_cumulated(:,3), 'color', c(3,:), 'linewidth',1);
        if is_roll
            plot([idx_roll_num idx_roll_num], get(gca, 'ylim'), 'color', 'black'); % [dt_roll_num dt_roll_num]
        end
        hold off
        legend({'Cumulated Deal Impact', 'Cumulated Book Moves', 'Cumulated Overnight Moves'}, 'location', 'NorthWest');
        set(gca, 'xlim',[ 1 length(dts) ],'xticklabel', [], 'ygrid', 'on');
        ylabel('Prices');
        
        %2nd SS GRAP
        a2 = subplot(10,1,6:7);
        hold on
        fill([dts_idx(1); dts_idx'; dts_idx(end)], [0; vdaily/1e3; 0], 'k');
        if is_roll
            plot([idx_roll_num idx_roll_num], get(gca, 'ylim'), 'color', 'black')
        end
        hold off
        set(gca, 'xlim',[ 1 length(dts) ],'xticklabel', [], 'ygrid', 'on');
        ylabel(sprintf('Volumes\n(k shares)'));
        
        %4eme SS GRAPH
        a4 = subplot(10,1,8:10);
        xl = [min(dts) max(dts)];
        cid = cumsum(vbs(:,1)-vbs(:,2));
        y = cid/ (max(cid)-min(cid)); %std(vbs(:,1)-vbs(:,2))*length(vbs);
        h1=plot( dts_idx, y, '--', 'color', c(1,:), 'linewidth',2);
        text( dts_idx(end)+0.01*diff(xl), y(end), sprintf('%4.1f%%', cid(end)/sum(vbs(:,1)+vbs(:,2))*100));
        hold on
        plot([ 1 length(dts) ], [0 0], '-', 'color', 'k', 'linewidth',1);
        ccd = cumsum(vbs(:,3)-vbs(:,4));
        y = ccd/(max(ccd)-min(ccd)); %std(vbs(:,3)-vbs(:,4))*length(vbs);
        h2=plot( dts_idx, y, 'color', 'k', 'linewidth',1);
        text(dts_idx(end)+0.01*diff(xl), y(end), sprintf('%4.1f%%', ccd(end)/sum(vbs(:,3)+vbs(:,4))*100));
        if is_roll
            plot([idx_roll_num idx_roll_num], get(gca, 'ylim'), 'color', 'black')
        end
        hold off
        legend([h1,h2], { 'Net intraday volume', 'Net close volume'});
        ylabel('Normalized imbalance');
        set(gca, 'xlim',[ 1 length(dts) ],'xticklabel', [], 'ygrid', 'on');
        attach_date('init', 'date', [min(dts) max(dts)], 'format', 'dd/mm/yy');
        % attach_date('init', 'date', dts, 'format', 'dd/mm/yy', 'allticks', length(dts));
        
        %<** options graphique
        linkaxes([a1,a2,a3,a4],'x');
        %**>
        
        % normalized imb
        hs = [hs, get_focus('Flow imbalance')]; clf
        h = copyobj( a4,gcf);
        set(gca, 'position',[0.1 0.1 0.8 0.8])
        attach_date('init', 'date', [min(dts) max(dts)], 'format', 'dd/mm/yy');
        legend(h, { 'Net intraday volume', 'Net close volume'});
        
        % deal-book split
        hs = [hs, get_focus('Book/deal')]; clf
        copyobj( a3,gcf);
        set(gca, 'position',[0.1 0.1 0.8 0.8])
        legend({'Cumulated Deal Impact', 'Cumulated Book Moves', 'Cumulated Overnight Moves'}, 'location', 'NorthWest');
        attach_date('init', 'date', [min(dts) max(dts)], 'format', 'dd/mm/yy');
        %>*
        
        varargout = { hs };
              
    case 'corpo-intraday-plot'
        opts = options({'debug', false, 'ylim', [], 'rotation', -40, 'language', 'EN'}, varargin(4:end));
        
        data        = varargin{1};
        dec         = varargin{2};
        
        fdata = st_data('intersect_v2', data, dec); % ???
        
        bapvdp      = st_data('col', fdata, 'bid;ask;bid_size;ask_size;price;volume;sell');
        dts         = fdata.date;
        LW = 4;
        mima        = [min(dts) max(dts)+LW*1e-3];
        
        deal_impact = st_data('col', fdata, 'Deal Impact');
        resiliency  = st_data('col', fdata, 'Resiliency');
        
        bdata       = varargin{3}; % data jour pcdt
        bprice      = st_data('col', bdata, 'price');

        h_hours = (datenum(0, 0, 0, 9, 0, 0):datenum(0, 0, 0, 0, 30, 0):datenum(0, 0, 0, 17, 30, 0))';
        hdates = fdata.date-floor(fdata.date);
        hdata = st_data('init', 'title','data per half hour', 'value', zeros(length(h_hours), 3),...
            'date', h_hours+unique(floor(fdata.date)), 'colnames', {'Price', 'Deal', 'Book'});
        hdata.value(1,1) = bapvdp(1,5);
        for d = 2:length(h_hours)
            idx_h = hdates>= h_hours(d-1) & hdates<h_hours(d);
            pc = bapvdp(idx_h,5);
            cd = cumsum(deal_impact(idx_h));
            cb = cumsum(resiliency(idx_h));
            hdata.value(d,1) = pc(end);
            hdata.value(d,2) = cd(end);
            hdata.value(d,3) =  cb(end);
        end        
        
        q_hours = (datenum(0, 0, 0, 9, 0, 0):datenum(0, 0, 0, 0, 10, 0):datenum(0, 0, 0, 17, 30, 0))';
        qdates = fdata.date-floor(fdata.date);
        qdata = st_data('init', 'title','data per half hour', 'value', zeros(length(q_hours), 5),...
            'date', q_hours+unique(floor(fdata.date)), 'colnames', {'Price', 'Deal', 'Book', 'Bid', 'Ask'});
        qdata.value(1,1) = bapvdp(1,5);
        qdata.value(1,4) = bapvdp(1,1);
        qdata.value(1,5) = bapvdp(1,2);
        for d = 2:length(q_hours)
            idx_q = qdates>= q_hours(d-1) & qdates<q_hours(d);
            pc = bapvdp(idx_q,5);
            cd = cumsum(deal_impact(idx_q));
            cb = cumsum(resiliency(idx_q));
            qdata.value(d,1) = pc(end);
            qdata.value(d,2) = cd(end);
            qdata.value(d,3) =  cb(end);
            
            bid_q = bapvdp(idx_q,1);
            ask_q = bapvdp(idx_q,2);
            qdata.value(d,4) =  bid_q(end);
            qdata.value(d,5) =  ask_q(end);
        end
%         col2agg = {'Deal Impact'};
%         [data_ft_agg, ~,~,~] = st_data('agg', fdata, ...
%             'colnames', col2agg,...
%             'grid_mode', 'time',...
%             'grid_options', {'time_type', 'minutes','window', 30 'user_specified_step_grid', datenum(0,0,0,9:0.5:17.5,0,0)});
%         sdata_i = st_data('stack',data_ft_agg{:});
        
        get_focus('Flows'); clf
        set(gcf, 'Position', [1520 225 670 660])
        %< PRICE
        a1 = subplot(2,1,1);
%         plot(fdata.date, bapvdp(:,5), '-k','linewidth',2);
        plot(qdata.date, qdata.value(:,1), '-k','linewidth', 1);
        hold on
%         stairs(fdata.date, bapvdp(:,1), 'color', 'blue')
%         stairs(fdata.date, bapvdp(:,2), 'color', 'red')
        h = stairs(qdata.date, qdata.value(:,4), 'color', 'blue');
        h = [h, stairs(qdata.date, qdata.value(:,5), 'color', 'red')];
        plot(mima(1), bprice(end,1), '*', 'color', 'blue', 'MarkerSize', 10)
        plot(mima, [bapvdp(1,5) bapvdp(1,5)], '--k');
        hold off
        xlim(mima);
        if ~isempty(opts.get('ylim'))
            ylim( opts.get('ylim')+ bapvdp(1,5));
        else
            ylim([0.99*min(min( bapvdp(:,5)),bprice(end,1)) 1.01*max(max( bapvdp(:,5)), bprice(end,1))]);
        end
        if strcmpi(opts.get('language'), 'EN')
            ylabel('Price');
            rtitl = sprintf('Flow decomposition on %s - %s\nMaximum variation during day: %1.1f%%\nClose / previous close: %1.1f%%',...
                data.info.security_key,  datestr(data.info.data_datestamp, 'dd/mm/yyyy'), ...
                100*(max(bapvdp(:,5))-min(bapvdp(:,5)))/bapvdp(1,5)-1, 100*(bapvdp(end,5)/bprice(end,1)-1));
            title(rtitl);
%             legend({'Price', 'Ask', 'Previous close price', 'Open price'}, 'location', 'SW')
            legend(h, {'Bid', 'Ask'}, 'location', 'SW')
        elseif strcmpi(opts.get('language'), 'FR')
            ylabel('Prix');
            rtitl = sprintf('D�composition du prix deal/book de %s - %s\nVariation maximale dans la journ�e: %0.1f %%\nVariation cl�ture / cl�ture du jour ouvr� pr�c�dent: %0.1f %%',...
                data.info.security_key,  datestr(data.info.data_datestamp, 'dd/mm/yyyy'), ...
                100*(max(bapvdp(:,5))-min(bapvdp(:,5)))/bapvdp(1,5)-1, 100*(bapvdp(end,5)/bprice(end,1)-1));
            title(rtitl);
%             legend({'Prix', 'Demande', 'Offre','Cl�ture du jour ouvr� pr�c�dent', 'Ouverture du jour'}, 'location', 'SW')
            legend(h, {'Demande', 'Offre'}, 'location', 'SW')
        end
        attach_date('init', 'date', mima, 'format', 'HH:MM:SS', 'C:\temp\matlab', 16);
        attach_date('draw', 'rotation', opts.get('rotation'))
        %>
        %< DECOMPOSITION
        a2 = subplot(2,1,2);
        stem(hdata.date(2:end), (hdata.value(2:end,2)+hdata.value(2:end,3))./hdata.value(1:end-1,1), 'color', 'k', 'Marker','none', 'LineWidth', LW)
        hold on
        stem(hdata.date(2:end)-(LW-0.5)*1e-3, hdata.value(2:end,2)./hdata.value(1:end-1,1),  'Marker','none', 'LineWidth', LW) 
        stem(hdata.date(2:end)+(LW-0.5)*1e-3, hdata.value(2:end,3)./hdata.value(1:end-1,1), 'color', 'r', 'Marker','none', 'LineWidth', LW) 
        plot(mima, [0 0], '--k');
        hold off
        
        if ~isempty(opts.get('ylim'))
            ylim( opts.get('ylim'));
%         else
%             ylim([0.99*min(min( bapvdp(:,5)),bprice(end,1)) 1.01*max(max( bapvdp(:,5)), bprice(end,1))]-bapvdp(1,5));
        end
%         % second axe
%         ax1 = a2;
%         pause(0.01) %aide matlab a prendre la bonne position pour l'axe de droite
%         ax2 = axes('Position',get(ax1,'Position'),'XAxisLocation','bottom','YAxisLocation','right',...
%             'Color','none','XColor','k','YColor','k');
%         set(ax2, 'xtick', []);
%         yt = get(ax1,'ytick');
%         lim1 = axis(ax1);
%         set(ax2,'ytick', yt+bapvdp(1,5));
%         ylim(lim1(3:4)+bapvdp(1,5));
%         axes(ax1);

        xlim(mima);
        attach_date('init', 'date', mima, 'format', 'HH:MM:SS', 'C:\temp\matlab', 16);
        attach_date('draw', 'rotation', opts.get('rotation'))
        if strcmpi(opts.get('language'), 'EN')
            ylabel('Price variations');
            legend({'Price', 'Deal effect', 'Book effect'}, 'location', 'SW');
        elseif strcmpi(opts.get('language'), 'FR')
            ylabel('Variations de prix');
            legend({'Prix', 'Transactions', 'Carnets d''ordres'}, 'location', 'SW');
        end
        set_percentage_labels(gca, 'y', 'text_precision', '%.1f%%')
        %>
        varargout = {a1,a2};
        
    case 'corpo-daily-plot'
        stats = varargin{1};
        opt= options({'roll-date', '', 'date-format', 'dd/mm/yyyy'}, varargin(2:end));
        dt_roll = opt.get('roll-date');
        is_roll = ~isempty( dt_roll);
        
        sec_id = regexp( stats.title, '.* ([0-9]+) \(.*', 'tokens', 'once');
        sec_id = sec_id{1};
        name = exec_sql('KGR',['select SECNAME from ',st_version.bases.repository,'..SECURITY',...
            ' where SYMBOL6 = ''',sec_id,'''',...
            ' and STATUS = ''A''',...
            ' and PRIORITY = 1']);
        name = name{1};
        
        dts = stats.date; 
        dl  = st_data('col', stats, 'nansum(Deal Impact);nansum(Resiliency);Overnight move');
        first_open = dl(1,3);
        prices     = sum(cumsum(dl),2);
        dl(1,3)    = 0;
        
        hs = get_focus('EF'); clf
        dts_idx = 1:length(dts);
        if is_roll
            dt_roll_num  = datenum(dt_roll, 'dd/mm/yyyy');
            idx_roll_num = find( dt_roll_num<=dts, 1);
        end
        a1= subplot(2,1,1);
        hold on
        plot( dts_idx, prices, 'k','linewidth',2);
        if is_roll
            plot([idx_roll_num idx_roll_num], get(gca, 'ylim'), 'color', 'black') %dts
        end
        hold off
        ylabel('Prix');
        stitle( '%s (%s) - Ouverture(0)=%5.2f', name, sec_id, first_open);
        xlim([ 1 length(dts) ]);
        set(gca, 'xticklabel', []);
        grid
        
        a2 = subplot(2,1,2);
        K = 4/length(dts);
%         all_cumulated = first_open+cumsum(dl);
        hold on
%         plot( dts_idx, all_cumulated(:,1), 'color', c(1,:), 'linewidth',2);
%         plot( dts_idx, all_cumulated(:,2), 'color', 'r', 'linewidth',2);
%         plot( dts_idx, all_cumulated(:,3), '--', 'color', c(3,:), 'linewidth',2);
        stem((2:length(dts))-K, diff(prices)./prices(1:end-1), 'color', 'k', 'linewidth', K*length(dts), 'marker', 'none')
        stem(2:length(dts)+K, diff(dl(:,1))./prices(1:end-1), 'linewidth', K*length(dts), 'marker', 'none')
%         stem((2:length(dts))+K, diff(dl(:,3)./prices(1:end-1)), 'color', 'blue','linewidth', 3, 'marker', 'none')
        if is_roll
            plot(([idx_roll_num idx_roll_num]), get(gca, 'ylim'), 'color', 'black'); % [dt_roll_num dt_roll_num] %dts
        end
        hold off
%         legend({'Transactions (cumul�es)', 'Carnets d''ordre (cumul�s)', 'Overnight'}, 'location', 'NorthWest');
        legend({'Prix', 'Transactions'}, 'location', 'NorthWest');
        xlim([ 1 length(dts)+K ]);
        set(gca, 'xticklabel', []);
        ylabel('Variations de prix');
        set_percentage_labels(gca, 'y', 'text_precision', '%.0f%%')
        grid
        attach_date('init', 'date', dts, 'format', 'dd/mm/yy', 'allticks', length(dts), 'tick_on_dates', false); %
        
        varargout = {hs, a1, a2};
        
    case 'index-report' % sortie: statistiques
        stats = varargin{1};
        opts  = options(varargin(2:end), {'colnames', 'nansum(Deal Impact);nansum(Resiliency);Overnight move;Buy volume;Sell volume;Buy at close;Sell at close'});
        cnames = tokenize( opts.get('colnames'), ';');
        
        sec_id = regexp( stats.title, '.* ([0-9]+) \(.*', 'tokens', 'once');
        sec_id = sec_id{1};
        name = exec_sql('KGR',['select SECNAME from ',st_version.bases.repository,'..SECURITY',...
            ' where SYMBOL6 = ''',sec_id,'''',...
            ' and STATUS = ''A''',...
            ' and PRIORITY = 1']);
        name = name{1};
        
        %< Extract proper columns
        dl  = st_data('col', stats, cnames(1:3)); % 'sum(Deal Impact);sum(Resiliency);Overnight move'
        first_open     = dl(1,3); % Open price is hidden as the first point of the overnight moves
        dl(1,3)        = 0;
        previous_close = first_open + sum(dl(1,1:2));
        first_open     = previous_close + dl(2,3);
        vbs            = st_data('col', stats, cnames(4:7)); % 'Buy volume;Sell volume;Buy at close;Sell at close'
        % j'ai le close de la veille:
        % j'efface ma premi�re ligne et je recommence
        dl(1,:)  = [];
        vbs(1,:) = [];
        % Open price is hidden as the first point of the overnight moves
        prices     = sum(cumsum(dl),2);
        vdaily = (vbs(:,1)+vbs(:,2)+vbs(:,3)+vbs(:,4));
        %>
        dP   = sum(dl, 1);
        mean_volume = mean(vdaily);
        vals = [ mean_volume, ...                         % volume moyen
            mean(vbs(:,1)-vbs(:,2))/mean_volume, ... % volume intraday sign� / volume moyen
            mean(vbs(:,3)-vbs(:,4))/mean_volume, ... % volume close sign� / volume moyen
            sum(dP)/first_open , ...                 % dP/P0
            dP(1)/first_open, ...                    % dPdeal / P0
            dP(2)/first_open, ...                    % dPbook / P0
            dP(3)/first_open ];                      % dPnight/ P0
        
        stats.value = vals;
        stats.title = sprintf('%s (%s) -> (%s)', name, datestr( stats.date(1), 'dd/mm/yy'), datestr( stats.date(end), 'dd/mm/yy'));
        stats.date  = stats.date(end);
        stats.colnames = { 'Average volume', 'Relative Imbalance Intraday Volume', 'Relative Imbalance Fixing Volume', ...
            'Relative dP', 'Relative dP Deal',  'Relative dP Book',  'Relative dP Overnight' };
        stats.rownames = { name };
        
        varargout = { stats };
        
    case 'merge-index' % aggr�ge datas
        datas = varargin{1};
        varargout = { st_data('stack', datas{:}) };
           
end

    function emp = EXP_MARKET_PRICE(bbo, tk)
        % Expected market price -
        q_max   = max(bbo(:,3:4),[],2);
        prices  = [ (bbo(:,1) - tk)      , bbo(:,1)           , bbo(:,2)     , (bbo(:,2) + tk)];
        qty     = [  q_max-bbo(:,3), min(q_max, bbo(:,3)), min(q_max,bbo(:,4)), q_max-bbo(:,4)];
        emp     = sum( prices .* qty, 2) ./ (2*q_max);
    end

    function cs = NANCUMSUM( v)
        % cumsum with nans
        v(isnan(v))=0;
        cs = cumsum(v);
    end

end