function data = keep_intraday_volume( mode, data, varargin)
% KEEP_INTRADAY_VOLUME - 
% 
% uses columns volume and price of the input data
%
% data_id = keep_intraday_volume( 'intra-day', data, 'out', 'row|cell')
% 
% data_id{1} ->
%        title: 'TOTF.PA - 05/05/2009 -> TOTF.PA - 05/05/2009'
%         date: [3x1 double]
%     colnames: {'Turnover'  'Market share'  'ATS'  'Nb'  'Mean(TS**2)'  'Max TS'  'VWAP'  'trading_destination_id'  'day phase'}
%         info: [1x1 struct]
%        value: [3x9 double]
%
%  data_p  = keep_intraday_volume( 'period', data_id)
%
%  data_se = keep_intraday_volume( 'format4se', data_p)
%
% TODO: real day phases management + pals instead of vals

%%** Intra day slices for heatmap
% The goal here is to compute statistics for the heatmap

switch lower(mode)

    case 'intra-day'
        %<* Intra day slicing
        % And computations of basic indicators (for one day, to be put in a
        % post computation of a read dataset request).
        opt = options({ 'out', 'raw'}, varargin);
        %< Indexing for accumarray
        
        % r�cup�ration des trading destination
        [td, tmp, ddx] = unique(st_data('col', data, 'trading_destination_id'));
        % r�cup�ration des tranches horaires
        % (je d�cide de d�couper la premi�re heure et demi, puis les derni�res 2 heures)
        data.date   = mod(data.date,1);
        date_bounds = [min(data.date) max(data.date)];
        date_slices = [date_bounds(1); date_bounds(1)+datenum(0,0,0, 1,30,0); ...
            date_bounds(2)-datenum(0,0,0, 2,0,0); date_bounds(2)];
        [ts, tmp, tdx] = unique( 1+ ...
            (data.date>date_slices(2)) + ...
            (data.date>date_slices(3)) );
        %>
        
        %< Computing indicators
        % I need: turnover, ATS, Square of ATS, Nb of trades, Max ATS, VWAP
        
        % accumarray par trading destination
        % 1. du turnover
        turnover = st_data('apply-formula', data, '{volume}.*{price}', 'output', 'value');
        vals_turnover = accumarray([tdx,ddx], turnover);
        vals_ATS      = accumarray([tdx,ddx], turnover, [], @mean);
        vals_SQATS    = accumarray([tdx,ddx], turnover, [], @(x)mean(x.^2));
        vals_NB       = accumarray([tdx,ddx], 1);
        vals_MATS     = accumarray([tdx,ddx], turnover, [], @max);
        vals_price    = vals_turnover./accumarray([tdx,ddx], st_data('col', data, 'volume'));
        %>
        
        %< Storing the results
        
        % fabrication du pourcentage (market share)
        % et juxtaposition de la phase (1 pour tout le monde � ce stade)
        data_template = rmfield( data, 'value');
        data_template.colnames = { 'Turnover', 'Market share', ...
            'ATS', 'Nb', 'Mean(TS**2)', 'Max TS', 'VWAP', 'trading_destination_id', 'day phase'};
        data_template.date = date_slices(1:end-1);
        out_data = repmat({ data_template}, length(td),1);
        % one st_data for each trading destination
        for d=1:length(td)
            out_data{d}.value = cat(2, vals_turnover(:,d), vals_turnover(:,d)./sum(vals_turnover,2), ...
                vals_ATS(:,d), vals_NB(:,d), vals_SQATS(:,d), vals_MATS(:,d), vals_price(:,d), repmat(td(d), length(ts),1), ts(:) );
            out_data{d}.info.trading_destination_id = td(d);
        end
        data = out_data;
        switch lower( opt.get('out'))
            case 'raw'
                data = out_data{1};
                for d=2:length(out_data)
                    data.value = cat(1, data.value, out_data{d}.value);
                    data.date  = cat(1, data.date , out_data{d}.date );
                end
            case 'cell'
                % nothing special to do
            otherwise
                error('keep_intraday_volume:intraday', 'no out mode called <%s> available', opt.get('out'));
        end
        %>
        %>*
    case 'period'
        %<* Fusion of several days
        opt = options({'proba', .95, 'out', 'raw'}, varargin);
        alpha = norminv( opt.get('proba'));
        
        %< split back the daily data
        td = unique(st_data('col', data, 'trading_destination_id'));
        T  = length(td);
        td_data = repmat({[]},T,1);
        for t=1:T
            td_data{t} = st_data('where', data, sprintf('{trading_destination_id}==%d', td(t)));
            td_data{t}.info.trading_destination_id = td(t);
        end
        %>
        
        % sur chaque TD
        data_template = rmfield(td_data{1}, { 'value', 'colnames', 'date'});
        data_template.colnames = { 'Turnover (sum)', 'Turnover (std)', 'Nb trades (sum)', ...
                'Max daily ATS', 'Market share (prop)', 'ATS', 'vwap', 'ATS quantile', 'Fiability (chi2 test)', ...
                'trading_destination_id', 'day phase'  };
        out_data   = repmat( { data_template}, T,1);
        for t=1:T
            turns  = st_data('col', td_data{t}, 'Turnover');
            ATS    = st_data('col', td_data{t}, 'ATS');
            nb     = st_data('col', td_data{t}, 'Nb');
            ATS2   = st_data('col', td_data{t}, 'Mean(TS**2)');
            maxTS  = st_data('col', td_data{t}, 'Max TS');
            vwap   = st_data('col', td_data{t}, 'VWAP');
            mshare = st_data('col', td_data{t}, 'Market share');
            day_phases = st_data('col', td_data{t}, 'day phase');
            
            [ph, phases_idx, pdx] = unique( day_phases);
            % sum/std/proportions
            ssnp = [ ...
                accumarray(pdx,turns ,[],@sum),  ... % vals
                accumarray(pdx,turns ,[],@var),  ... % std
                accumarray(pdx,nb    ,[],@sum),  ... % nb
                accumarray(pdx,maxTS ,[],@mean), ... % max daily ATS
                accumarray(pdx,mshare,[],@mean) ];    % pars
            var_mshare = accumarray(pdx,mshare ,[],@var);
            % ATS issues
            atsi = [ ...
                accumarray(pdx, ATS   .* nb  ,[],@sum) ./ ssnp(:,3), ... % ATS
                accumarray(pdx, ATS2  .* nb  ,[],@sum) ./ ssnp(:,3), ... % ATS**2
                accumarray(pdx, turns .* vwap,[],@sum) ./ ssnp(:,1), ... % VWAP
                ];
            %< ATS quantile
            % hypothesis: log normality of TS
            mu   = log( atsi(:,1).^2./sqrt( atsi(:,2)+atsi(:,1).^2));
            sigm = sqrt( log( atsi(:,2)./atsi(:,1).^2+1));
            TS80 = exp(mu + alpha*sigm);
            %>
            %< Fiability
            normalized_shares = arrayfun( ...
                @(c)sum((mshare(day_phases==ph(c))-ssnp(c,5)).^2/var_mshare(c)), ...
                1:length(ph),'uni',true);
            fiab = repmat( chi2cdf( sum( normalized_shares), length(mshare)), ... 
                length(ph), 1);
            %>
            out_data{t}.value = cat(2, ...
                ssnp, ...
                atsi(:,[1 3]), ...
                TS80, ...
                fiab, ...
                repmat( td(t), length(ph), 1), ...
                ph );
            out_data{t}.info.trading_destination_id = td(t);
            out_data{t}.date = td_data{t}.date(phases_idx);
        end
        data = out_data;
        switch lower( opt.get('out'))
            case 'raw'
                data = out_data{1};
                for d=2:length(out_data)
                    data.value = cat(1, data.value, out_data{d}.value);
                    data.date  = cat(1, data.date , out_data{d}.date );
                end
            case 'cell'
                % nothing special to do
            otherwise
                error('keep_intraday_volume:intraday', 'no out mode called <%s> available', opt.get('out'));
        end
        %>*
    case 'format4se'
        %<* Format for the Statistical Engine
        mshares = st_data('col', data, 'Market share (prop)');
        ATS     = st_data('col', data, 'ATS');
        ATSqu   = st_data('col', data, 'ATS quantile');
        fiabs   = st_data('col', data, 'Fiability (chi2 test)');
        [td, td_idx    , tdx] = unique( st_data('col', data, 'trading_destination_id'));
        [dp, phases_idx, pdx] = unique( st_data('col', data, 'day phase'));
        
        data_template = rmfield( data, {'value', 'date'});
        data_template.date  = data.date(phases_idx);
        data_template.colnames = arrayfun(@(x)get_repository( 'td_id2td_name', x), td, 'uni', false)';
        data_template.info.quality = fiabs(td_idx)';
        
        out_data = repmat({ data_template}, 3, 1);
        
        out_data{1}.value = accumarray([pdx,tdx],mshares,[],@sum);
        out_data{1}.title = [ data.title ' - Prop'];
        out_data{2}.value = accumarray([pdx,tdx],ATS    ,[],@sum);
        out_data{2}.title = [ data.title ' - ATS'];
        out_data{3}.value = accumarray([pdx,tdx],ATSqu  ,[],@sum);
        out_data{3}.title = [ data.title ' - ATS quantile'];
        
        data = out_data;
        %>*
        
    otherwise
        error('keep_intraday_colume:mode', 'mode <%s> unknown', mode);
end
        