function varargout = market_impact( mode, data, varargin)
% MARKET_IMPACT -
%
%  [outp, q, params] = market_impact( 'simple', data_mi);
%  [outp, q, params] = market_impact( 'simple', data_mi, 'delta-r', .15, ...
%            'spread-weight', 0.8, 'colnames', 'Overvolume;dS;VWAS', ...
%            'overvolume', true)
%
% See also mi_characteristics

opt = options({'colnames', 'Overvolume;dS;VWAS', ...
    'overvolume', false, ...
    'delta-r', 0.1, ...
    'spread-weight', 0.7 }, ...
    varargin);

switch lower(mode)
    
    case 'simple'
        rsp = st_data('col', data, opt.get('colnames'));
        delta_r = opt.get('delta-r');
        
        dts = data.date;
        rho = rsp(:,1);
        if opt.get('overvolume')
            rho = rho-1;
        end
        dS  = rsp(:,2);
        psi = rsp(:,3);
        
        params = [];
        
        
        %<* Estimation of s0
        % Le principe est relativement simple:
        % \begin{enumerate}
        % \item je calcul le spread moyen |mean_spread| sur {\bf toutes} mes
        % donn?es (en esp?rant qu'il ressemble au spread moyen tout court, car
        % j'ai d?j? pas mal conditionn?)
        % \item je calcul le spread moyen sachant que $\rho>0$
        % \end{enumerate}
        idx_pos_rho = rho>0;
        s0_pop = abs(dS(idx_pos_rho));
        params = struct_dens( params, (s0_pop), 's0', .05);
        s0   = nanmedian( s0_pop);
        st_log('    s0_init = %3.2f\n', s0);
        mean_spread = nanmean( rsp(:,3));
        if s0<mean_spread
            s0 = mean_spread;
        else
            c = opt.get('spread-weight');
            s0 = c * mean_spread/2 + (1-c) * s0;
        end
        st_log('    s0 = %3.2f\n', s0);
        idx_MI = (dS>s0) & (rho>0);
        deltaS =  dS(idx_MI)-s0;
        rho_p  = rho(idx_MI);
        dts(~idx_MI) = [];
        
        params.s0 = s0;
        %>*
        if length( rho_p)<100
            st_log('market_impact:simple rho is not long enough (%d<100)\n', length( rho_p));
            varargout = { ...
                st_data('log', data, 'gieo', 'rho_p has not the size I asked'), ...
                [], ...
                []};    % revoir ....
            return
        end
        %<* Estimation of Kappa
        % Je cherche des gens avec un rho_p proche de 1
        idx_rho1 = 0;
        n=1;
        while sum(idx_rho1)<5
            idx_rho1 = (rho_p>1-n*delta_r) & (rho_p<1+n*delta_r);
            n=n+1;
            if n>10
                st_log('market_impact:simple rho is not close enough to 1 (%d<10)\n', sum(idx_rho1));
                varargout = { ...
                    st_data('log', data, 'gieo', 'idx_rho1 has not the sum I asked'), ...
                    [], ...
                    params};
                return
            end
        end
        st_log('    length(rho)=%d ; %d kept in %d steps for kappa computations...\n', length( rho_p), sum(idx_rho1), n);
        K_pop = deltaS(idx_rho1);
        K_star   = median( K_pop);
        params = struct_dens( params, K_pop, 'kappa', .05);
        st_log('    kappa = %3.2f\n', K_star);
        params.kappa = K_star;
        %>*
        %<* Estimation of gamma
        idx_keep = ((deltaS<K_star) & (rho_p<1)) | ...
            ((deltaS>K_star) & (rho_p>1));
        gammas = log( deltaS(idx_keep)/K_star) ./ log(rho_p(idx_keep));
        params = struct_dens( params, gammas, 'gamma', .10);
        log_gamma_star = median( log(gammas));
        gamma_star = exp( log_gamma_star);
        st_log('    gamma = %3.2f\n', gamma_star);
        
        params.gamma = gamma_star;
        dts(~idx_keep) = [];
        [days, tmp, idx_day] = unique( floor(dts));
        gamma_confs = accumarray( idx_day, log(gammas), [], @(g)std(g), NaN);
        gamma_confidence = nanmedian(1./gamma_confs);
        %>*
        
        %<* Build output
        
        outputs = { [], []};
        f_MI   = @(r)params.s0 + (r>0).*( params.kappa .* r.^params.gamma );
        dS_hat =  f_MI( rho);
        outputs{2} = st_data('add-col', data, dS_hat, 'dS hat');
        
        quality    = sqrt(mean( (dS - dS_hat).^2));
        
        outputs{1} = st_data('init', 'title', data.title, ...
            'value', [params.s0, params.kappa, params.gamma, gamma_confidence,length(gammas)], ...
            'date', floor(data.date(end)) , ...
            'colnames', { 's_0', '\kappa', '\gamma', '\gamma_{conf}', 'nb'});
        
        varargout = { outputs, quality, params };
        
    case 'simple_03'
        
        %%% GLOBAL PARAMS
        RHO_MIN=0.2;
        NB_MIN_KAPPA=20;
        NB_MIN_GAMMA=10;
        NB_MIN_RHO=100;
        NB_MIN_DAYS_TO_COMPUTE_PROCESS=20;
        %%% OTHER PARAMS
        delta_r = opt.get('delta-r');
        c = opt.get('spread-weight');
        
        rsp = st_data('col', data, opt.get('colnames'));
        dts = data.date;
        
        if length(unique(floor(dts)))<NB_MIN_DAYS_TO_COMPUTE_PROCESS
            st_log('market_impact:not enough different days from preprocess \n',length(unique(floor(dts))));
            varargout = { ...
                st_data('log', data, 'gieo', 'not enough different days from preprocess'), ...
                [], ...
                []};
            return
        end
        
        rho  = rsp(:,strcmpi(data.colnames,'rho_v'));
        dS  = rsp(:,strcmpi(data.colnames,'delta_dS_side'));
        psi = rsp(:,strcmpi(data.colnames,'spread_p'));
        
        params = [];
        
        %<* Estimation of s0
        idx_pos_rho = rho>0 & rho<RHO_MIN & dS~=0;
        mean_spread = nanmean(psi);
        if any(idx_pos_rho)
            s0_pop = abs(dS(idx_pos_rho));
            s0   = nanmedian( s0_pop);
            st_log('    s0_init = %3.2f\n', s0);
            if s0<0.5*mean_spread
                s0 = 0.5*mean_spread;
            else
                s0 = c * mean_spread/2 + (1-c) * s0;
            end
        else
            s0 = mean_spread/2;
        end
        
        idx_MI = (dS>s0) & (rho>RHO_MIN);
        deltaS =  dS(idx_MI)-s0;
        rho_p  = rho(idx_MI);
        dts(~idx_MI) = [];
        %>*
        if length( rho_p)<NB_MIN_RHO
            st_log('market_impact:simple rho is not long enough (%d<100)\n', length( rho_p));
            varargout = { ...
                st_data('log', data, 'gieo', 'rho_p has not the size I asked'), ...
                [], ...
                []};
            return
        end
        %<* Estimation of Kappa
        % Je cherche des gens avec un rho_p proche de 1
        idx_rho1 = 0;
        n=1;
        while sum(idx_rho1)<NB_MIN_KAPPA
            idx_rho1 = (rho_p>1-n*delta_r) & (rho_p<1+n*delta_r);
            n=n+1;
            if n>4
                st_log('market_impact:simple rho is not close enough to 1 (%d<10)\n', sum(idx_rho1));
                varargout = { ...
                    st_data('log', data, 'gieo', 'idx_rho1 has not the sum I asked'), ...
                    [], ...
                    params};
                return
            end
        end
        st_log('    length(rho)=%d ; %d kept in %d steps for kappa computations...\n', length( rho_p), sum(idx_rho1), n);
        K_pop = deltaS(idx_rho1);
        K_star   = median( K_pop);
        %>*
        
        
        %<* Estimation of gamma
        idx_keep = ((deltaS<K_star) & (rho_p<1)) | ...
            ((deltaS>K_star) & (rho_p>1));
        
        if sum(idx_keep)<NB_MIN_GAMMA
            st_log('market_impact:not enough data to compute gamma\n', sum(idx_keep));
            varargout = { ...
                st_data('log', data, 'gieo', 'idx_keep has not the sum I asked'), ...
                [], ...
                params};
            return
        else
            gammas = log( deltaS(idx_keep)/K_star) ./ log(rho_p(idx_keep));
            params = struct_dens( params, gammas, 'gamma', .10);
            log_gamma_star = median( log(gammas));
            gamma_star = exp( log_gamma_star);
        end
        
        % Correction of gamma, if gamma<1 !!
        value_test=[0.7 1 1.5];
        if gamma_star<1
            res_star=[];
            for i_test=1:length(value_test)
                idx_rho_gamma = 0;
                n=1;
                while sum(idx_rho_gamma)<NB_MIN_GAMMA
                    idx_rho_gamma = (rho_p>value_test(i_test)-n*delta_r & rho_p<value_test(i_test)+n*delta_r);
                    n=n+1;
                    if n>4
                        st_log('market_impact:not enough data to compute gamma correction\n', sum(idx_rho_gamma));
                        varargout = { ...
                            st_data('log', data, 'gieo', 'idx_rho_gamma has not the sum I asked'), ...
                            [], ...
                            params};
                        return
                    end
                end
                res_star = cat(2,res_star,nanmedian(deltaS(idx_rho_gamma)));
            end
            reg_res=simplereglin([0 res_star]',[0 value_test]') ;
            K_star_tmp=reg_res.a(2);
            s0_tmp=s0+reg_res.a(1);
            if s0_tmp>(1+0.25)*s0
                s0=(1+0.25)*s0;
            elseif s0_tmp>s0
                s0=s0_tmp;
            end
            if K_star_tmp<(1-0.25)*K_star
                K_star=(1-0.25)*K_star;
            elseif K_star_tmp>(1+0.25)*K_star
                K_star=(1+0.25)*K_star;
            else
                K_star=K_star_tmp;
            end
            gamma_star=1;
        end
        
        
        %%% construct params
        
        params = struct_dens( params, (s0_pop), 's0', .05);
        st_log('    s0 = %3.2f\n', s0);
        params.s0 = s0;
        params = struct_dens( params, K_pop, 'kappa', .05);
        st_log('    kappa = %3.2f\n', K_star);
        params.kappa = K_star;
        st_log('    gamma = %3.2f\n', gamma_star);
        params.gamma = gamma_star;
        dts(~idx_keep) = [];
        [days, tmp, idx_day] = unique( floor(dts));
        gamma_confs = accumarray( idx_day, log(gammas), [], @(g)std(g), NaN);
        gamma_confidence = nanmedian(1./gamma_confs);
        %>*
        
        %<* Build output
        idx_compute=rho>RHO_MIN;
        dS=dS(idx_compute);
        rho=rho(idx_compute);
        data=st_data('from-idx',data,find(idx_compute==1));
        
        outputs = { [], []};
        f_MI   = @(r)params.s0 + (r>0).*( params.kappa .* r.^params.gamma );
        dS_hat =  f_MI( rho);
        outputs{2} = st_data('add-col', data, dS_hat, 'dS hat');
        
        quality    = sqrt(mean( (dS - dS_hat).^2));
        
        outputs{1} = st_data('init', 'title', data.title, ...
            'value', [params.s0, params.kappa, params.gamma, gamma_confidence,length(gammas)], ...
            'date', floor(data.date(end)) , ...
            'colnames', { 's_0', '\kappa', '\gamma', '\gamma_{conf}', 'nb'});
        
        varargout = { outputs, quality, params };
        
    otherwise
        error('market_impact:mode', 'mode <%s> unknown', mode);
end

%%** Return structured density
function p = struct_dens( p, pop, name_, ql, qg, u)
p.([name_ '_x'])=[];
p.([name_ '_y'])=[];
qg = quantile( pop, [ql (1-ql)]);
u = pop( pop>qg(1) & pop<qg(2));
if ~isempty(u)
    [ p.([name_ '_y']), p.([name_ '_x']) ] = ksdensity( u, 'kernel', 'epanechnikov');
end

