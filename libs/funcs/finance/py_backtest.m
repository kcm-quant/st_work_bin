function res = py_backtest(mode, start_date, end_date, varargin)
% res = py_backtest('spec_standard', '2009-01-01', '2021-08-27')
% 
% w_dts = (datenum(2021,1,1):datenum(2021,7,31))';
% w_mat = rand(length(w_dts),3);
% w_cols = [2, 8, 110];
% res = py_backtest('ext', '2009-01-01', '2021-07-31','weight_mat',w_mat,'weight_dates',w_dts,'weight_cols',w_cols)
%
%

if count(py.sys.path, 'C:\st_sim\usr\methodo\backtests') == 0
    insert(py.sys.path, int32(0), ['C:\st_sim\usr\methodo\backtests']);
end

opt = options({'weight_mat',[],'weight_dates',[],'weight_cols',[]},varargin);

%%
switch mode
    case 'spec_standard'
        res = py.launch_backtest.compute_backtest_spec(start_date, end_date, pyargs('matlab_format',1));
        res = py_format(res);
    case 'ext'
        w_mat = opt.get('weight_mat');
        w_dts = opt.get('weight_dates');
        w_cols = opt.get('weight_cols');
        
        w_dts = arrayfun(@(v) datestr(v,'yyyy-mm-dd'),w_dts,'uni',0)';
        w_dts = py.numpy.array(w_dts, pyargs('dtype','datetime64'));
        w_dts = py.numpy.transpose(w_dts);
        w_dts = py.pandas.to_datetime(w_dts);
        % w_dts = py.list(w_dts);
        % w_dts = py.numpy.array2string(w_dts);
        
        w_mat = py.numpy.array(w_mat); %[arrayfun(@(v) datestr(v,'yyyy-mm-dd'),w_mat(:,1),'uni',0), arrayfun(@num2str, w_mat(:,2:end),'uni',0)];
        % w_mat = py.list(w_mat);

        w_cols = py.numpy.array(w_cols); %arrayfun(@num2str, w_cols, 'uni',0);
        df_weight = py.pandas.DataFrame(w_mat,pyargs('index',w_dts,'columns',w_cols));

        % res = py.launch_backtest.compute_backtest_ext(w_mat, w_dts, w_cols, start_date, end_date);
        res = py.launch_backtest.compute_backtest_ext(df_weight, start_date, end_date, pyargs('matlab_format',1));
        res = py_format(res);
end

end

function out = py_format(in)
if length(in)==0
    out = [];
else
    st = struct(in);
    fields = fieldnames(st);
    out = struct();
    for f = 1:numel(fields)
        fld = fields{f};
        out.(regexprep(fld,'matlab_','')) = struct(st.(fld));
        fields_in = fieldnames(out.(fld));
        for fi = 1:numel(fields_in)
            fldi = fields_in{fi};
            out.(fld).(regexprep(fldi,'matlab_','')) = py2m_format(out.(fld).(fldi));
        end
    end
end
end