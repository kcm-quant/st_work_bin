function outputs = transversal_TDrepartition(data, bool_indice_list , indicateur, title_ric)
%TRANSVERSAL_TDREPARTITION fonction qui calcule les parts de march� du titre
%sur les differentes trading destination 

% author   : 'dcroize@cheuvreux.com'
% reviewer : 
% version  : '1'
% date     : '26/01/2009'


%R�cuperation des noms des tradings destination sur lesquels les titres de
%l'indice ont �t� trait�s
trading_dest = unique(st_data( 'cols', data, 'trading_destination') );
namestr = sprintf(['%d'],trading_dest(1));
for u= 2:length(trading_dest)
    namestr = strcat(namestr,sprintf([',%d '],trading_dest(u)));
end
sql_req = sprintf ([' select name from repository..trading_destination '...
    'where trading_destination_id in (%s) '...
    ],namestr);
name_trade = exec_sql('BSIRIUS', sql_req);


all_day = unique (st_data('col',data,'.date'));
fday = all_day(1);
eday = all_day(end);
date_t = st_data('col',data,'.date') - (all_day(1)-1);
data.date = date_t;


%disons que pour un indice g bien l'indice_id ......
 if bool_indice_list
     
     if strcmp(indicateur,'nb_deal')
         func_eval = @(x)mean(x);
         arr_indic = accumarray({st_data( 'cols', data,'security_id'),st_data( 'cols', data,'trading_destination')},st_data( 'cols', data,'nb_deal'),[],@(x)func_eval(x));
         indic = 'Nombre de deal';
         
     elseif strcmp(indicateur,'VWAS')
         func_eval = @(x)sum(x);
         spread_num   = accumarray({st_data( 'cols', data,'security_id'),st_data( 'cols', data,'trading_destination')},st_data( 'cols', data,'spread_numerateur'),[],@(x)func_eval(x));
         spread_denum = accumarray({st_data( 'cols', data,'security_id'),st_data( 'cols', data,'trading_destination')},st_data( 'cols', data,'spread_denominateur'),[],@(x)func_eval(x));
         indic = 'VWAS en bp';
         
     elseif strcmp(indicateur,'MARKET SHARE')
         func_eval = @(x)sum(x);
         turnover_Td = accumarray({st_data( 'cols', data,'security_id') st_data( 'cols', data,'trading_destination') },st_data( 'cols', data,'turnover'),[],@(x)func_eval(x));
         turnover_oneTd = bsxfun(@times,turnover_Td,1./sum(turnover_Td,2));
         
         indic = 'Market Share' ;
     end
     
     nb_deal_arr=[];
     for j= 1 : size(trading_dest,1)
         if strcmp(indicateur,'nb_deal')
             arr_tp = [ arr_indic(idx_indice{1},trading_dest(j)) repmat(trading_dest(j), size(idx_indice{1},1),1)];
             nb_deal_arr =[ nb_deal_arr ; arr_tp]  ;
             
         elseif strcmp(indicateur,'VWAS')
             arr_tp = [ spread_num(idx_indice{1},trading_dest(j))./spread_denum(idx_indice{1},trading_dest(j)) repmat(trading_dest(j), size(idx_indice{1},1),1)];
             nb_deal_arr =[ nb_deal_arr ; arr_tp];
             
         elseif strcmp(indicateur,'MARKET SHARE')
             arr_tp = [ turnover_oneTd(idx_indice{1},trading_dest(j)) repmat(trading_dest(j), size(idx_indice{1},1),1)];
             nb_deal_arr =[ nb_deal_arr ; arr_tp];
             
         end
     end
 end
 
 
 index_turnover_Alltd = accumarray({st_data('col',data,'trading_destination'),st_data('col',data,'.date')},st_data('col',data,'turnover'));
 cumul_curve = bsxfun(@times,cumsum(index_turnover_Alltd,2),1./cumsum(sum(index_turnover_Alltd),2)); 
 date_or= unique (all_day(1):all_day(end));
 
 %Si le titre traite sur plusieurs TD faire un plot des parts de march� de
 %toutes les TD sauf marh� primaire 
 if length(name_trade)>1
     idx_p = 2 ;
 else
     idx_p = 1 ;     %plot march� primaire: le titre ne traite que sur le primaire
 end
 
 cumul_curve = cumul_curve(trading_dest(idx_p:end),:);
 datplot= st_data( 'init', 'title', sprintf('Market Share:%s',data.title), 'value',cumul_curve', ...
     'date', date_or', 'colnames', name_trade(idx_p:end)');

 
open_vlm   = accumarray(st_data( 'cols', data,'trading_destination'),st_data( 'cols', data,'Open volume'));
close_vlm  = accumarray(st_data( 'cols', data,'trading_destination'),st_data( 'cols', data,'Close volume'));
total_vlm  = accumarray(st_data( 'cols', data,'trading_destination'),st_data( 'cols', data,'Volume'));
total_nb_deal  = accumarray(st_data( 'cols', data,'trading_destination'),st_data( 'cols', data,'Nb deals'));
close_price  = accumarray(st_data( 'cols', data,'trading_destination'),st_data( 'cols', data,'Close'));
spread_moy = accumarray(st_data( 'cols', data,'trading_destination'),st_data( 'cols', data,'Mean spread') , [], @(x)mean(x));
turnover    = accumarray(st_data( 'cols', data,'trading_destination'),st_data( 'cols', data,'Turnover'));

vals = [open_vlm(trading_dest)./total_vlm(trading_dest)*100 close_vlm(trading_dest)./total_vlm(trading_dest)*100 (total_vlm(trading_dest)-(close_vlm(trading_dest)+open_vlm(trading_dest)))./total_vlm(trading_dest)*100 spread_moy(trading_dest) total_vlm(trading_dest) turnover(trading_dest) (total_vlm(trading_dest)./total_nb_deal(trading_dest)).*close_price(trading_dest) trading_dest];

data_TD = st_data( 'init', 'title', sprintf('%s',title_ric), 'value', vals, ...
                   'date', (1:length(trading_dest))', 'colnames', ...
                   { 'Volume Open', 'Volume Close' , 'Volume Intra day', 'spread moyen' , 'Volume' , 'Turnover','ATS en capital', 'Trading destination'});

outputs{1} = data_TD;


end