function check = vc_check(mode, vc, data, varargin)
% VC_CHECK - calcul de la qualit� (check, pas run quality) d'une courbe de
%       volume pour une journ�e pr�cise ou pour une p�riode (dans ce cas
%       c'est juste une moyenne de la valeur sur chaque jour)
% arguments :   - mode : (de calcul des qualit�s quotidiennes)
%               - vc   : une courbe de volume sous forma de st_data
%               - data : les donn�es sur lesquels on veut appr�cier la
%                       qualit� de la courbe de volume
% arguments optionnels :
%               - 'aggregate_func' un pointeur vers la fonction �
%                   utiliser pour calculer la qualit� sur une p�riode �
%                   partir de la matrice 'qualit� de la courbe sur chaque
%                   jour (lignes), pour chaque contexte (colonnes)'.
%                   Par d�faut : @nanmean
%

vc = st_data('drop', vc, 'opening_auction;intraday_auction;closing_auction');
days = unique(floor(data.date));
if length(days) > 1
    check = arrayfun(@(d)vc_check(mode, vc, st_data('extract-day', data, d)), days, 'uni', false);
    check = cat(1, check{:});
    opt = options({'aggregate_func', @nanmedian}, varargin);
    aggregate_func = opt.get('aggregate_func');
    check = aggregate_func(check);
    return
end
if st_data('isempty-nl', data) || st_data('isempty-nl', vc)
    check = NaN(1, length(vc.colnames));
    return
elseif ~strcmpi(mode,'cvdyn_indicator') && length(vc.date) ~= length(data.date)
    error('vc_check:check_args', 'REPOSITORY: Trading_hours changed');
elseif ~strcmpi(mode,'cvdyn_indicator') && any(round(mod(vc.date, 1)*24*3600/5) ~= round(mod(data.date, 1)*24*3600/5)) % five seconds accuracy
    error('vc_check:check_args', 'REPOSITORY: Trading_hours changed');
end
nb_contexts = length(vc.colnames);
market_volume = st_data('col', data, 'volume');
switch mode
    case {'vwap_slippage', 'slippage', 'full'}
        market_vwaps  = st_data('col', data, 'vwap');
        % S'il n'y a pas eu d'�x�cution sur l'un des intervalles alors le
        % vwap va �tre NaN. Or 0 * NaN = NaN donc il faut corriger �a avant
        % de chercher � calculer la performance de la courbe de volume sur
        % la journ�e, d'o� :
        
        market_vwaps = fill_nan_v(market_vwaps);
        
        vwap = repmat(nansum(market_vwaps.*market_volume)/sum(market_volume), 1, nb_contexts);
        my_vwap_for_each_col = nansum(repmat(market_vwaps, 1, nb_contexts).*vc.value)./sum(vc.value);
        check = 10000*(my_vwap_for_each_col-vwap)./vwap; % classical vwap slippage in basis points
        if mode(1) == 'v'
            check = vc_quality_trans('trans', abs(check)); % check = 2 ./ (1+exp(check/10)); % A transformation in order to be in [0,1]
        end
        if strcmp(mode, 'full')
            check = [check(1), sum((market_volume/sum(market_volume)-vc.value(:, 1)).^2),...
                max(abs(cumsum(market_volume)/sum(market_volume)-cumsum(vc.value(:, 1))))];
        end
 
     case {'cvdyn_indicator'}   
         
         start_s=vc.info.start_slice;
         end_s=vc.info.end_slice;
         idx_prop=find(strcmpi(vc.colnames,'vwap volume strategy'));  
         idx_histo_prop=find(strcmpi(vc.colnames,'histo proportion'));  

         market_prop=market_volume(start_s:end_s)/sum(market_volume(start_s:end_s));
         cvdyn_prop=vc.value(:,idx_prop)./sum(vc.value(:,idx_prop));
         cvdyn_histo_prop=vc.value(:,idx_histo_prop)./sum(vc.value(:,idx_histo_prop));

         criterion_cumsum=(cumsum(market_prop)-cumsum(cvdyn_prop)).^2;
         criterion_cumsum_histo_prop=(cumsum(market_prop)-cumsum(cvdyn_histo_prop)).^2;
 
         quality_fun=@(x,y,w)(0.5*(tanh(-100*((w*x-(1-w)*y)-0.01*(2*w-1)))+1));
         w=0.66; 
              
         check =quality_fun(nanmean(criterion_cumsum),nanmean(criterion_cumsum_histo_prop),w);
         
%          figure;
%          plot(cumsum(market_prop),'-k')
%          hold on;
%          plot(cumsum(cvdyn_prop),'-m')
%          hold on;
%          plot(cumsum(cvdyn_histo_prop),'-r')
%          hold off;

    otherwise
        error('vc_check:mode', 'BAD_USE: Unknown mode <%s>', mode);
end