function out = vc_forecast(vc, data, varargin)
switch vc.model.name
    case 'std'
        if ~isempty(varargin) && ischar(varargin{1}) && strcmp(varargin{1}, 'stdny')
            out = st_data('keep', vc, 'March special ny');
        else
            out = st_data('keep', vc, 'Usual day');
        end
        out.colnames = {'vwap volume strategy'};
        
    case 'stdny'
        out.colnames = {'vwap volume strategy'};
        
    case 'static_agg'
        out = st_data('keep', vc, 'Usual day');
        out.colnames = {'vwap volume strategy'};
        if length(varargin) > 1
            out.value = out.model.other_vc(:, varargin{2});
        end
    case 'gc'
        out = gc_forecast(vc, data, varargin{:});
    case 'gb'
        vals = st_data('col', data, 'volume')';
        prop = bsxfun(@times, vals, 1./sum(vals, 2));
        
        seas = st_data('col', vc, 'Usual day')';
        
        seasrel2beg = seas;
        prop_rel2beg = prop;
        for i = 1 : size(prop, 2)
            prop_rel2beg(:, i) = prop(:, i) ./ sum(prop(:, 1:i), 2);
            seasrel2beg(:, i)  = seas(:, i) ./ sum(seas(:, 1:i), 2);
        end
        out = vc;
        vc.value(:, 1) = prop_gloubiboulga(prop_rel2beg, seas, seasrel2beg, vc.model.my_params(1:2), vc.model.my_params(3));
        out = st_data('keep', out, 'Usual day');
        out.colnames = {'vwap volume strategy'};
    otherwise
        error('vc_forecast:exec', 'MODE: unknown mode <%s>', mode);
end
switch vc.model.name
    case {'std', 'gb','static_agg'}
        slices=find_idx_slice('cvdyn','forecast_params',varargin,'data_vc',st_data('keep', vc, 'Usual day'));
        start_slice=slices(1);
        end_slice=slices(end);
        [nv, eodv, real_eodv] = proportion2volumeforecast(out, st_data('col', data, 'volume'),start_slice,end_slice);
        out=st_data('from-idx',out,start_slice:end_slice);
        out.value(:,1)=out.value(:,1)/sum(out.value(:,1));
        out = st_data('add-col', out, nv, 'one step volume forecast');
        out = st_data('add-col', out, eodv, 'end of day volume forecast');
        out = st_data('add-col', out, real_eodv, 'real end of day volume forecast');
        out.info.start_slice=start_slice;
        out.info.end_slice=end_slice;
end
end

function [nv, eodv,real_eodv] = proportion2volumeforecast(prop, volume,start_slice,end_slice)

end_s=end_slice;
start_s=start_slice;
prop=prop.value(:, 1);
prop_tmp=prop(1:end_s)/sum(prop(1:end_s));
eodv_tmp = [ NaN; cumsum(volume(1:end_s-1)) ./ cumsum(prop_tmp(1:end_s-1))];
cum_volume_tmp=cumsum(volume(1:end_s-1));
real_eodv_tmp=eodv_tmp-[NaN;cum_volume_tmp];
nv_tmp = prop_tmp .* eodv_tmp;
nv=nv_tmp(start_s:end);

already_traded_volume_market=cat(1,0,cumsum(volume(start_s:end_s-1)));
real_eodv=real_eodv_tmp(start_s:end);
eodv=real_eodv+already_traded_volume_market;

end



function p = prop_gloubiboulga(rel_prop, seas, seas_rel, modif_bounds, modif_conf)
p = seas;
for i = 3 : length(seas)-1
    p(i) = min(1-sum(p(1:i-1)), max(modif_bounds(1)*seas(i), min(modif_bounds(2)*seas(i), p(i) + modif_conf * (seas_rel(i-1)-rel_prop(i-1)) * sum(seas(1:i-1)))));
end
p(end) = 1 - sum(p(1:end-1));% plot([p; seas]')
end



function prop2trade = gc_forecast(vc, data, mode, model2use, varargin)


%------------------------------------------------------------------------------------------
% --------- R�cup�ration des donn�es
%--------------------------------------------------------------------------


if nargin < 3
    mode = 'std';
    vc5m=st_data('keep', vc, 'Usual day');
end
if nargin > 3
    if model2use
        tmp = vc.model.prop.med_prop;
        vc5m=st_data('keep', vc, 'Usual day');
        vc = vc.model.other_models(model2use);
        vc.model.prop.my_med_prop = tmp/sum(tmp);
    end
else
    vc.model.prop.my_med_prop = vc.model.prop.med_prop / sum(vc.model.prop.med_prop);
end



%------------------------------------------------------------------------------------------
% --------- Calcul des pr�visions de volume en 30 minutes
%------------------------------------------------------------------------------------------
% Remarque : vc est en version x minutes correspondant au model 'model2use' du vc initial
% data est en version 5 minutes -> on conserve les data 5 minutes dans data_copy
% et on met dans data les donn�es 30 minutes

vc = st_data('keep', vc, 'Usual day');
data = st_data('keep', data, 'volume');
data_copy = data;

if length(data.date)~=length(vc5m.date)
    error('gc_forecast:exec', 'MODE: unknown mode <%s>', mode);
end

if size(vc.model.bins_idx, 1) ~= length(data.date)
    vals = NaN(size(vc.model.bins_idx, 1), 1);
    for i = 1 : length(vc.model.bins_idx)
        vals(i) = sum(data.value(vc.model.bins_idx(i, 1):vc.model.bins_idx(i, 2)));
    end
    data.value = vals;
end

log_data = log(data.value);
log_data(~isfinite(log_data))=0;

forecastAllNextVolume=repmat(nan,length(data.value),length(data.value));
%forecastNextSigma=repmat(nan,length(data.value),length(data.value));

quant0Index=find(strcmpi(varargin,'quant0'));
scalingMeanIndex=find(strcmpi(varargin,'mean_scaling'));

switch mode
    case {'std','ema 30m cap','composite 1','std cap'}
        %%% on choisit la methode de transformation des 0
        
        if ~isempty(quant0Index) && varargin{quant0Index+1}>0
            if ~isempty(scalingMeanIndex) && varargin{scalingMeanIndex+1}>0
                scaling_period=varargin{scalingMeanIndex+1};
                if scaling_period==30
                    meanHisto=vc.valueQuant0Scaling30(:,1);
                elseif scaling_period==60
                    meanHisto=vc.valueQuant0Scaling60(:,1);
                else
                    meanHisto=vc.valueQuant0ScalingAll(:,1);
                end
            else
                meanHisto=vc.valueQuant0(:,1);
            end
            matCov=vc.model.covQuant0;
            diagMat=diag(matCov);
            log_data(log_data==0)=norminv(vc.model.quant0.q,meanHisto(log_data==0),sqrt(diagMat(log_data==0)));
        else
            matCov=vc.model.cov;
            meanHisto=vc.value;
        end
        
        %%% on calcul les previsions de volumes
        for i = 1:(length(log_data)-1)
            dp=matCov(1:i,1:i);
            dc=matCov((i+1):end,1:i);
            meanNextGaussianCurve=meanHisto((i+1):end)+(dc*pinv(dp'*dp))*dp*(log_data(1:i)-meanHisto(1:i));
            nextVolumePrediction=exp(meanNextGaussianCurve);
            forecastAllNextVolume(i+1:end,i+1)=nextVolumePrediction;
            % - prediction des sigmas
            %df=matCov((i+1):end,(i+1):end);
            %covNextGaussianCurve=df-(dc*pinv(dp'*dp))*dp*dc';
            %forecastNextSigma(i+1:end,i+1)=sqrt(diag(covNextGaussianCurve));
            
        end
        
    otherwise
        error('gc_forecast:exec', 'MODE: unknown mode <%s>', mode);
end



% quantile_forecastAllNextVolume=normcdf(repmat(log_data,1,size(forecastAll
% NextVolume,2)),log(forecastAllNextVolume),forecastNextSigma);
% sum(repmat(nansum(forecastAllNextVolume),nbSimulation,1)<simulEodVolume)/
% nbSimulation
% sum(repmat(sum(exp(log_data'))-cat(2,0,cumsum(exp(log_data(1:end-1)'))),nbSimulation,1)<simulEodVolume)/nbSimulation






%------------------------------------------------------------------------------------------
%--- Securit� : cap/floor on volume estimation (just prevent from outliers)
%------------------------------------------------------------------------------------------
% Remarque : par d�faut, on ne fait pas de cap sur les volumes
% m�thode non utilis� actuellement

cap_volumeIndex=find(strcmpi(varargin,'cap_volume'));
if ~isempty(cap_volumeIndex) && varargin{cap_volumeIndex+1}>0
    
    const_mult=1.25;
    quant_max_30m=norminv(0.99,meanHisto,sqrt(diagMat));
    quant_min_30m=norminv(0.01,meanHisto,sqrt(diagMat));
    pct_previous_max=cumsum(data.value>exp(quant_max_30m));
    pct_previous_max=[0 ;pct_previous_max(1:end-1)./(1:length(pct_previous_max)-1)'];
    pct_previous_min=cumsum(data.value<exp(quant_min_30m));
    pct_previous_min=[0 ;pct_previous_min(1:end-1)./(1:length(pct_previous_min)-1)'];
    
    out_up=zeros(size(forecastAllNextVolume,2),1);
    out_down=zeros(size(forecastAllNextVolume,2),1);
    for i_test=1:size(forecastAllNextVolume,2)
        idx_up_tmp=forecastAllNextVolume(:,i_test)>exp(quant_max_30m);
        idx_up_2_tmp=forecastAllNextVolume(:,i_test)>const_mult*exp(quant_max_30m);
        idx_down_tmp=forecastAllNextVolume(:,i_test)<exp(quant_min_30m);
        if pct_previous_max(i_test)<0.05 && any(idx_up_tmp)
            out_up(i_test)=sum(idx_up_tmp);
            forecastAllNextVolume(idx_up_tmp,i_test)=exp(quant_max_30m(idx_up_tmp));
        elseif pct_previous_max(i_test)>=0.05 && any(idx_up_2_tmp)
            out_up(i_test)=sum(idx_up_2_tmp);
            forecastAllNextVolume(idx_up_2_tmp,i_test)=const_mult*exp(quant_max_30m(idx_up_2_tmp));
        end
        if pct_previous_min(i_test)<0.05 && any(idx_down_tmp)
            out_down(i_test)=sum(idx_down_tmp);
            forecastAllNextVolume(idx_down_tmp,i_test)=exp(quant_min_30m(idx_down_tmp));
        end
    end
    
    if nansum(out_up>0)+ nansum(out_down>0)>0
        sprintf('!!!!!!!!! TEST !!!!!!!! - %s - - %s - out_up : %d - out_down %d',datestr(data.date(1),'dd/mm/yyyy'),vc.info.security_key,nansum(out_up>0),nansum(out_down>0))
        
    end
    % ---- TEST ---
    
end




%------------------------------------------------------------------------------------------
%---- on repasse les donn�es 30 minutes en 5 minutes + on effectue des calcul de volume
%------------------------------------------------------------------------------------------

vc_end=vc;
vc_end.value=[];
vc_end.date=[];
vc_end.colnames=[];

%-- transform  forecastVolume -> forecastAllNextVolume_5m
nb_bins_vector=vc.model.bins_idx(:, 2)-vc.model.bins_idx(:, 1) +1;
forecastAllNextVolume_5m_tmp=[];
for i = 1 : length(vc.model.bins_idx)
    vals_all_tmp=[];
    for j = 1 : length(vc.model.bins_idx)
        vals_all_tmp=cat(2,vals_all_tmp,repmat(forecastAllNextVolume(i,j)/nb_bins_vector(i),nb_bins_vector(i), 1));
    end
    forecastAllNextVolume_5m_tmp=cat(1,forecastAllNextVolume_5m_tmp,vals_all_tmp);
end
forecastAllNextVolume_5m=[];
for i = 1 : length(vc.model.bins_idx)
    forecastAllNextVolume_5m=cat(2,forecastAllNextVolume_5m,repmat(forecastAllNextVolume_5m_tmp(:,i),1,nb_bins_vector(i)));
end
for i=2:size(forecastAllNextVolume_5m,2)
    forecastAllNextVolume_5m(1:i-1,i)=nan;
end

tmp_cum=cumsum(data_copy.value);
eod_value_5m=nansum(forecastAllNextVolume_5m)'+cat(1,0,tmp_cum(1:end-1));
real_eod_value_5m=nansum(forecastAllNextVolume_5m)';
forecastNextVolume_5m=diag(forecastAllNextVolume_5m);
forecastNextVolumeEma_5m=repmat(NaN,size(eod_value_5m));


%-- Transformation des valeurs de volume 0
% -- dans le cas quant0, on remplace par un quantile
% -- sinon on remplace par 0
% -- on cr�e : mean_value_5m et log_data_copy

if ~isempty(quant0Index) && varargin{quant0Index+1}>0
    quant0_30m=norminv(vc.model.quant0.q,meanHisto,sqrt(diagMat));
    %%% on prend comme reference les valeurs corrig�es des 0 en 30m pour vc (vc.valueQuant0)
    %%% on cr�e la cb de volume log mean 5m + les quantile 5m
    quant0_5m=[];
    mean_value_5m = [];
    for i = 1 : length(vc.model.bins_idx)
        mean_value_5m = cat(1, mean_value_5m, repmat(vc.valueQuant0(i)-log(nb_bins_vector(i)),nb_bins_vector(i),1));
        quant0_5m=cat(1,quant0_5m,repmat(quant0_30m(i)-log(nb_bins_vector(i)), nb_bins_vector(i), 1));
    end
    % log_data_copy contient les log-volume du jour 5m corrig� des 0
    % pour rester coh�rent ac le forecast GC30, on conserve com reference pr la calcul de l'ema,
    % la moyenne des log 30m renormalis�e en 5m
    % de m�me pour le quantile 30m qu'on renormalise en 5m
    log_data_copy=log(data_copy.value);
    log_data_copy(~isfinite(log_data_copy))=quant0_5m(~isfinite(log_data_copy));
else
    mean_value_5m = [];
    for i = 1 : length(vc.model.bins_idx)
        mean_value_5m = cat(1, mean_value_5m, repmat(vc.value(i, 1)-log(nb_bins_vector(i)),nb_bins_vector(i),1));
    end
    log_data_copy=log(data_copy.value);
    log_data_copy(~isfinite(log_data_copy))=0;
end





%---- Pour certaine m�thode de forecast
%-- On met un autre modele sur l'estimation du next volume, ici une moyenne mobile

if strcmpi(mode,'ema 30m cap') || strcmpi(mode,'composite 1') || strcmpi(mode,'std cap')
    
    % on recupere les variables de forecast
    emaLengthIndex=find(strcmpi(varargin,'emaLength'));
    if ~isempty(emaLengthIndex)
        emaLength=varargin{emaLengthIndex+1};
    else
        emaLength=4; %param par defaut
    end
    
    forecastNextDiffLogVolumeEma=tsmovavg(log_data_copy'-mean_value_5m','s', emaLength);
    forecastNextDiffLogVolumeEma=cat(2,nan,forecastNextDiffLogVolumeEma(1:end-1))';
    forecastNextVolumeEma=exp(mean_value_5m+forecastNextDiffLogVolumeEma);
    forecastNextVolumeEma(isnan(forecastNextVolumeEma))=forecastNextVolume_5m(isnan(forecastNextVolumeEma));
    %%% on calcule un forecast volume mix entre GC et EMA
    forecastNextDiffLogMix=0.5*(forecastNextDiffLogVolumeEma+log(forecastNextVolume_5m)-mean_value_5m);
    forecastNextVolumeMix=exp(mean_value_5m+forecastNextDiffLogMix);
    forecastNextVolumeMix(isnan(forecastNextVolumeMix))=forecastNextVolume_5m(isnan(forecastNextVolumeMix));
    %%% on calcule un forecast volume ema exponential
    forecastNextDiffLogVolumeEmaExp=tsmovavg(log_data_copy'-mean_value_5m','e', emaLength);
    forecastNextDiffLogVolumeEmaExp=cat(2,nan,forecastNextDiffLogVolumeEmaExp(1:end-1))';
    forecastNextVolumeEmaExp=exp(mean_value_5m+forecastNextDiffLogVolumeEmaExp);
    forecastNextVolumeEmaExp(isnan(forecastNextVolumeEmaExp))=forecastNextVolume_5m(isnan(forecastNextVolumeEmaExp));
    
    vc_end = st_data('add-col', vc_end, mean_value_5m,vc.colnames{1});
    vc_end = st_data('add-col', vc_end, forecastNextVolume_5m, 'one step volume forecast');
    vc_end = st_data('add-col', vc_end, eod_value_5m, 'end of day volume forecast');
    vc_end = st_data('add-col', vc_end, real_eod_value_5m, 'real end of day volume forecast');
    vc_end = st_data('add-col', vc_end, forecastAllNextVolume_5m(end,:)', 'close volume forecast');
    vc_end = st_data('add-col', vc_end, forecastNextVolumeEma, 'one step volume ema forecast');
    vc_end = st_data('add-col', vc_end, forecastNextVolumeMix, 'one step volume ema-gc forecast');
    vc_end = st_data('add-col', vc_end, forecastNextVolumeEmaExp, 'one step volume ema-exp forecast');
    
    % pour r�cup�rer l'info dans volume_forecast2vwap_strategy sur les critere 5min
    vc_end.model.critere5m=vc5m.model.critere;
    vc_end.model.histo_prop_5m=exp(vc5m.valueQuant0)/sum(exp(vc5m.valueQuant0));
    % on r�cup�re l'estimation EMA dans forecastNextVolumeEma_5m pour le
    % mettre en .info de la fin
    forecastNextVolumeEma_5m=forecastNextVolumeEma;
    
else
    vc_end = st_data('add-col', vc_end, mean_value_5m,vc.colnames{1});
    vc_end = st_data('add-col', vc_end, forecastNextVolume_5m, 'one step volume forecast');
    vc_end = st_data('add-col', vc_end, eod_value_5m, 'end of day volume forecast');
    vc_end = st_data('add-col', vc_end, real_eod_value_5m, 'real end of day volume forecast');
    vc_end = st_data('add-col', vc_end, forecastAllNextVolume_5m(end,:)', 'close volume forecast');
    
    % pour r�cup�rer l'info dans volume_forecast2vwap_strategy sur les critere 5min
    vc_end.model.critere5m=vc5m.model.critere;
    vc_end.model.histo_prop_5m=exp(vc5m.valueQuant0)/sum(exp(vc5m.valueQuant0));
end

vc_end.date=data_copy.date;




%------------------------------------------------------------------------------------------
%-- Renormalisation dans le cas ou ne teste les proportions sur un autre interval que la journ�e
%------------------------------------------------------------------------------------------
% recuperation de la fenetre de test
slices=find_idx_slice('cvdyn','forecast_params',varargin,'data_vc',vc5m);
start_slice=slices(1);
end_slice=slices(end);
if start_slice~=1 || end_slice~=length(vc5m.date)
    already_traded_volume_market=cat(1,0,cumsum(data_copy.value(start_slice:end_slice-1)));
    vc_end=st_data('from-idx',vc_end,start_slice:end_slice);
    real_eod_value_5m_modif=nansum(forecastAllNextVolume_5m(start_slice:end_slice,start_slice:end_slice))';
    eod_value_5m_modif=real_eod_value_5m_modif+already_traded_volume_market;
    vc_end.value(:,strcmpi(vc_end.colnames,'end of day volume forecast'))=eod_value_5m_modif;
    vc_end.value(:,strcmpi(vc_end.colnames,'real end of day volume forecast'))=real_eod_value_5m_modif;
    vc_end.model.histo_prop_5m=vc_end.model.histo_prop_5m(start_slice:end_slice)/sum(vc_end.model.histo_prop_5m(start_slice:end_slice));
end





%--------------------------------------------
%-- Pr�vision des proportions
%-----------------------------------------------

prop2trade = volume_forecast2vwap_strategy(mode, vc_end, data_copy, varargin{:});
prop2trade.info.start_slice=start_slice;
prop2trade.info.end_slice=end_slice;
prop2trade.info.forecastAllNextVolume_5m=forecastAllNextVolume_5m;
prop2trade.info.mean_value_5m=mean_value_5m;
prop2trade.info.forecastNextVolumeEma_5m=forecastNextVolumeEma_5m;
prop2trade.info.covariance_matrix=matCov;
prop2trade.info.nb_bins_vector=nb_bins_vector;
prop2trade.info.log_data_copy=log_data_copy;

end



