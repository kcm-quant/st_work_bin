function prop2trade = volume_forecast2vwap_strategy(mode, d_forecast, data, varargin)

prop2trade = d_forecast;
% - histo_prop vecteur 5m des proportions historiques (provenant des log volume)
histo_prop = st_data('cols', d_forecast, 'Usual day');
histo_prop = exp(histo_prop) / sum(exp(histo_prop));
% - prop2trade=vecteur des proprtions forecast
prop2trade.colnames = {'vwap volume strategy'};
prop2trade.value = NaN(length(histo_prop), 1);
% - Quelque soit la m�thode, la premi�re proportion trait� est toujours la proportions m�diane !!
prop2trade.value(1) = histo_prop(1);

one_step_volume_forecast = st_data('cols', d_forecast, 'one step volume forecast');
end_of_day_volume_forecast = st_data('cols', d_forecast, 'end of day volume forecast');
real_end_of_day_volume_forecast = st_data('cols', d_forecast, 'real end of day volume forecast');
close_volume_forecast = st_data('cols', d_forecast, 'close volume forecast');
forecast_method=NaN;

switch mode
    
    
    case 'std'
        % la proportion de volume � traiter pendant le prochain intervalle est le volume pr�dit pour le prochain interval
        % divis� par le volume pr�dit pour la fin de p�riode
        for i = 2 : size(d_forecast.value,1)-1
            prop2trade.value(i) = max(0, min((1-sum(prop2trade.value(1:i-1))), one_step_volume_forecast(i)/(real_end_of_day_volume_forecast(i))*(1-sum(prop2trade.value(1:i-1)))));
        end
        
        %%% on rajoute les donn�es pour les crit�res suivants
        prop2trade = st_data('add-col', prop2trade, end_of_day_volume_forecast, 'end of day volume forecast');
        prop2trade = st_data('add-col', prop2trade, one_step_volume_forecast, 'one step volume forecast');
        prop2trade = st_data('add-col', prop2trade, real_end_of_day_volume_forecast, 'real end of day volume forecast');
        prop2trade = st_data('add-col', prop2trade, histo_prop, 'histo proportion');
        
        
        
    case 'std cap'
        % similaire � 'std', sauf que l'on cap les proportions autour au proportiosn m�diane
        
        one_step_volume_forecast_new=one_step_volume_forecast;
        end_of_day_volume_forecast_new=end_of_day_volume_forecast;
        real_end_of_day_volume_forecast_new = real_end_of_day_volume_forecast;
        
        if ~isempty(varargin)
            pctU=varargin{1};
            pctD=varargin{2};
        else
            pctU=0.10;
            pctD=0.20;
        end
        
        %%% histo_prop actuel 30m...mais on pe passer en 5 min
        curveRefIndex=find(strcmpi(varargin,'curve_ref'));
        if ~isempty(curveRefIndex) && varargin{curveRefIndex+1}==5
            histo_prop=d_forecast.model.histo_prop_5m;
        end
        
        histo_prop_up_bounds=histo_prop*(1+pctU);
        histo_prop_down_bounds=histo_prop*(1-pctD);
        
        
        %%% on conserve les proportions historique jusqu'� ?
        startPropIndex=find(strcmpi(varargin,'start_prop'));
        if ~isempty(startPropIndex) && varargin{startPropIndex+1}>0
            startCompute=varargin{startPropIndex+1};
            for i = 2 : startCompute-1
                prop2trade.value(i) = histo_prop(i);
            end
        else
            startCompute=2;
        end
        
        for i = startCompute : size(d_forecast.value,1)-1
            propToEnd=one_step_volume_forecast_new(i)/(real_end_of_day_volume_forecast_new(i));
            alreadyTraded=sum(prop2trade.value(1:i-1));
            prop2trade.value(i) = max(0,min(max(histo_prop_down_bounds(i), min(histo_prop_up_bounds(i),propToEnd*(1-alreadyTraded))),1-alreadyTraded-0.001));
        end
        
        %%% on rajoute les donn�es pour les crit�res suivants
        prop2trade = st_data('add-col', prop2trade, end_of_day_volume_forecast_new, 'end of day volume forecast');
        prop2trade = st_data('add-col', prop2trade, real_end_of_day_volume_forecast_new, 'real end of day volume forecast');
        prop2trade = st_data('add-col', prop2trade, one_step_volume_forecast_new, 'one step volume forecast');
        prop2trade = st_data('add-col', prop2trade, histo_prop, 'histo proportion');
        
        
    case 'ema 30m cap'
        
        one_step_volume_forecast_new=one_step_volume_forecast;
        end_of_day_volume_forecast_new=end_of_day_volume_forecast;
        real_end_of_day_volume_forecast_new = real_end_of_day_volume_forecast;
        
        if ~isempty(varargin)
            pctU=varargin{1};
            pctD=varargin{2};
        else
            pctU=0.10;
            pctD=0.20;
        end
        
        %%% histo_prop actuel 30m...mais on pe passer en 5 min
        curveRefIndex=find(strcmpi(varargin,'curve_ref'));
        if ~isempty(curveRefIndex) && varargin{curveRefIndex+1}==5
            histo_prop=d_forecast.model.histo_prop_5m;
        end
        
        histo_prop_up_bounds=histo_prop*(1+pctU);
        histo_prop_down_bounds=histo_prop*(1-pctD);
        
        
        %%% on prend l'ema pour forecast ou mean ema-gc
        emaGCIndex=find(strcmpi(varargin,'ema_gc'));
        if ~isempty(emaGCIndex) && varargin{emaGCIndex+1}>0
            one_step_volume_forecast_ema = st_data('cols', d_forecast, 'one step volume ema-gc forecast');
            one_step_volume_forecast_new=one_step_volume_forecast_ema;
        else
            one_step_volume_forecast_ema = st_data('cols', d_forecast, 'one step volume ema forecast');
            one_step_volume_forecast_new=one_step_volume_forecast_ema;
        end
        if strcmp(mode, 'lr ema 30m cap')
            prop2trade.value(1) = d_forecast.model.prop.my_med_prop(1);
        end
        
        %%% on conserve les proportions historique jusqu'� ?
        startPropIndex=find(strcmpi(varargin,'start_prop'));
        if ~isempty(startPropIndex) && varargin{startPropIndex+1}>0
            startCompute=varargin{startPropIndex+1};
            for i = 2 : startCompute-1
                prop2trade.value(i) = histo_prop(i);
            end
        else
            startCompute=2;
        end
        
        for i = startCompute : size(d_forecast.value,1)-1
            end_of_day_volume_forecast_new(i)=end_of_day_volume_forecast(i)-one_step_volume_forecast(i)+one_step_volume_forecast_ema(i);
            real_end_of_day_volume_forecast_new(i)=real_end_of_day_volume_forecast(i)-one_step_volume_forecast(i)+one_step_volume_forecast_ema(i);
            propToEnd=one_step_volume_forecast_ema(i)/real_end_of_day_volume_forecast_new(i);
            alreadyTraded=sum(prop2trade.value(1:i-1));
            prop2trade.value(i) = max(0,min(max(histo_prop_down_bounds(i), min(histo_prop_up_bounds(i),propToEnd*(1-alreadyTraded))),1-alreadyTraded-0.001));
        end
        
        %%% on rajoute les donn�es pour les crit�res suivants
        prop2trade = st_data('add-col', prop2trade, end_of_day_volume_forecast_new, 'end of day volume forecast');
        prop2trade = st_data('add-col', prop2trade, real_end_of_day_volume_forecast_new, 'real end of day volume forecast');
        prop2trade = st_data('add-col', prop2trade, one_step_volume_forecast_new, 'one step volume forecast');
        prop2trade = st_data('add-col', prop2trade, histo_prop, 'histo proportion');
        
        
        
    case 'composite 1'
        
        %%% suivant la m�thode, on estime les volumes de facon diff�rente
        one_step_volume_forecast_new=one_step_volume_forecast;
        end_of_day_volume_forecast_new=end_of_day_volume_forecast;
        real_end_of_day_volume_forecast_new = real_end_of_day_volume_forecast;
        
        
        if ~isempty(varargin)
            pctU=varargin{1};
            pctD=varargin{2};
        else
            pctU=0.10;
            pctD=0.20;
        end
        %%% suivant les critere historique l'estimation se fait de facon
        %%% differente
        meanNb05mIndex=find(strcmpi(varargin,'meanNb05m'));
        pctMedian05mIndex=find(strcmpi(varargin,'pctMedian05m'));
        
        if ~isempty(meanNb05mIndex) && varargin{meanNb05mIndex+1}>0
            criterionMeanNb05m=varargin{meanNb05mIndex+1};
        else
            criterionMeanNb05m=10;
        end
        
        if ~isempty(pctMedian05mIndex) && varargin{pctMedian05mIndex+1}>0
            criterionPctMedian05m=varargin{pctMedian05mIndex+1};
        else
            criterionPctMedian05m=0;
        end
        
        % on change histo_prop
        %%% histo_prop actuel 30m...mais on pe passer en 5 min
        curveRefIndex=find(strcmpi(varargin,'curve_ref'));
        if ~isempty(curveRefIndex) && varargin{curveRefIndex+1}==5
            histo_prop=d_forecast.model.histo_prop_5m;
        end
        histo_prop_up_bounds=histo_prop*(1+pctU);
        histo_prop_down_bounds=histo_prop*(1-pctD);
        
        
        %%% on conserve les proportiosn historique jusqu'� ?
        startPropIndex=find(strcmpi(varargin,'start_prop'));
        if ~isempty(startPropIndex) && varargin{startPropIndex+1}>0
            startCompute=varargin{startPropIndex+1};
            for i = 2 : startCompute-1
                prop2trade.value(i) = histo_prop(i);
            end
        else
            startCompute=2;
        end
        
        
        %%% calcul des proportions
        %cas 'std cap' ou 'ema 30m cap'
        
        if d_forecast.model.critere5m.nb_period_median0>criterionPctMedian05m ||...
                ( d_forecast.model.critere5m.nb_period_median0<=criterionPctMedian05m && d_forecast.model.critere5m.mean_nb_0>criterionMeanNb05m)
            
            
            for i = startCompute : size(d_forecast.value,1)-1
                propToEnd=one_step_volume_forecast_new(i)/(real_end_of_day_volume_forecast_new(i));
                alreadyTraded=sum(prop2trade.value(1:i-1));
                prop2trade.value(i) = max(0,min(max(histo_prop_down_bounds(i), min(histo_prop_up_bounds(i),propToEnd*(1-alreadyTraded))),1-alreadyTraded-0.001));
            end
            
            forecast_method=0;
            
        else
            
            %%% on prend l'ema pour forecast ou mean ema-gc
            emaGCIndex=find(strcmpi(varargin,'ema_gc'));
            emaExpIndex=find(strcmpi(varargin,'ema_exp'));
            if ~isempty(emaGCIndex) && varargin{emaGCIndex+1}>0
                one_step_volume_forecast_ema = st_data('cols', d_forecast, 'one step volume ema-gc forecast');
            elseif ~isempty(emaExpIndex) && varargin{emaExpIndex+1}>0
                one_step_volume_forecast_ema = st_data('cols', d_forecast, 'one step volume ema-exp forecast');
            else
                one_step_volume_forecast_ema = st_data('cols', d_forecast, 'one step volume ema forecast');
            end
            
            for i = startCompute : size(d_forecast.value,1)-1
                end_of_day_volume_forecast_new(i)=end_of_day_volume_forecast(i)-one_step_volume_forecast(i)+one_step_volume_forecast_ema(i);
                real_end_of_day_volume_forecast_new(i)=real_end_of_day_volume_forecast(i)-one_step_volume_forecast(i)+one_step_volume_forecast_ema(i);
                propToEnd=one_step_volume_forecast_ema(i)/real_end_of_day_volume_forecast_new(i);
                alreadyTraded=sum(prop2trade.value(1:i-1));
                prop2trade.value(i) = max(0,min(max(histo_prop_down_bounds(i), min(histo_prop_up_bounds(i),propToEnd*(1-alreadyTraded))),1-alreadyTraded-0.001));
            end
            
            %%% cr�e one_step_volume_forecast_new pour la sortie
            one_step_volume_forecast_new=one_step_volume_forecast_ema;
            forecast_method=1;
        end
        
        
        
        %%% on rajoute les donn�es pour les crit�res suivants
        prop2trade = st_data('add-col', prop2trade, end_of_day_volume_forecast_new, 'end of day volume forecast');
        prop2trade = st_data('add-col', prop2trade, real_end_of_day_volume_forecast_new, 'real end of day volume forecast');
        prop2trade = st_data('add-col', prop2trade, one_step_volume_forecast_new, 'one step volume forecast');
        prop2trade = st_data('add-col', prop2trade, histo_prop, 'histo proportion');
        
        
        
        
        
        
    otherwise
        error('gc_forecast:exec', 'MODE: unkown mode <%s>', mode);
end

prop2trade.value(end,1) = 1 - sum(prop2trade.value(1:end-1,1));

%%% on rajoute les donn�es pour les crit�res suivants
prop2trade = st_data('add-col', prop2trade, close_volume_forecast, 'close volume forecast');
prop2trade.info.forecast_method=forecast_method;

if any(prop2trade.value(:,1) < 0)
    error('volume_forecast2vwap_strategy:exec', 'Buying and selling is forbidden (there are negative proportions)');
end

if abs(sum(prop2trade.value(:,1))-1) > eps(1)
    error('volume_forecast2vwap_strategy:exec', 'You have executed too much or too few volume');
end


