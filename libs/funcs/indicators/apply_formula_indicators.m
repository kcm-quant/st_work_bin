function data = apply_formula_indicators(data,cell_is_not_empty,available_cols,wanted_outcols,on_the_fly_indicators)
% APPLY_FORMULA_INDICATORS - auto find and apply formulas to get a set of indicators from a given set
% 
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   date     :  '24/11/2011'
%   
%   last_checkin_info : $Header: apply_formula_indicators.m: Revision: 1: Author: robur: Date: 11/24/2011 04:44:59 PM$

persistent mem_computation_plan

% TODO any mandatory cols?

ac_hk = convs('safe_str', available_cols);

%< On repere le premier cell non vide
ne_1st = find(cell_is_not_empty,1, 'first');
%>

cols2build = setdiff(wanted_outcols, available_cols);
if isempty(cols2build)
    return;
end

rc_hk = convs('safe_str', cols2build);

%< * Find out what aggregation path should be used :
[indicators, indicators_hk] = indicator_factory('add_replace_indicators', ...
    on_the_fly_indicators);

%< Making an hash key with indicators_computation_plan inputs in order to
% avoid having to recompute the optimal formula path 
my_full_hash_key = ['a' hash([rc_hk ac_hk indicators_hk], 'MD5')]; % TODO ???????? est ce bien injectif ?
%>
all_formulas = cat(1, indicators.formula);

if isempty(mem_computation_plan) || ~isfield(mem_computation_plan, my_full_hash_key)
    [ind_formulas, ~, ~, cost, used_cols, col_dico] = ...
        indicators_computation_plan(cols2build, ...
        available_cols, all_formulas);
    mem_computation_plan.(my_full_hash_key) = struct(...
        'ind_formulas', {ind_formulas}, 'cost', {cost}, ...
        'used_cols', {used_cols}, 'col_dico', {col_dico});
else
    ind_formulas = mem_computation_plan.(my_full_hash_key).ind_formulas;
    col_dico = mem_computation_plan.(my_full_hash_key).col_dico;
end
formulas_apriori = all_formulas([ind_formulas{:}]);

%> *


%<* Ajout aux st_data d'origine (non vides) de colonnes calcul�es par les formulas_apriori
data{ne_1st} = compute_formula(data{ne_1st}, formulas_apriori,...
    [ind_formulas{:}],col_dico,col_dico.ind_in_ac);
idx_col2keep = cellfun(@(c)find(strcmp(c, data{ne_1st}.colnames)), wanted_outcols, 'uni', true);
%< On retire les colonnes ayant servi d'intermediaire, pour ne garder que
%celles demand�es par l'utilisateur
data{ne_1st}.colnames = wanted_outcols;%col_dico.cols(ind_final_outcols);
data{ne_1st}.value = data{ne_1st}.value(:,idx_col2keep);
%>
if ~isempty(formulas_apriori)
    for c = (ne_1st+1):length(data)
        if cell_is_not_empty(c)
            data{c} = compute_formula(data{c}, formulas_apriori,...
                [ind_formulas{:}],col_dico,col_dico.ind_in_ac);
            data{c}.colnames = wanted_outcols;
            data{c}.value = data{c}.value(:,idx_col2keep);
        end
    end
end
%>*

end
