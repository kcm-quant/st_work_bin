function output = apply_plan_to_grid(data,cell_is_not_empty,grid_inputs,...
    cols, on_the_fly_indicators)
% APPLY_PLAN_TO_GRID - 
% 
% author   : 'rburgot@cheuvreux.com'
% reviewer : 'athabault@cheuvreux.com'
% version  : '1'
% date     : '04/01/2011'

persistent mem_computation_plan

% If this variable is no longer a MAGIC NUMBER then it will probably be
% needed to include it in the hash key as the optimal path for aggregation
% will depend on its value
POST_FORMULA_COST_SAVING_FACTOR = 0.5; %TODO more realistic estimations of 
% the relative cost according to the targeted nb_slices and distributions
% of number of slices/trades to aggregate

mandatory_time_cols = {'time_open', 'time_close'};
%< * prepare input
wanted_outcols = cols.wanted_outcols;
% rc_hk = hash(convs('safe_str', unique(wanted_outcols)), 'MD5');
available_cols = cols.available_cols;
% < le statut particulier du champs date force � des manipulations
% sp�cifiques

for i = 1 : length(mandatory_time_cols)
    if ~any(strcmp(mandatory_time_cols{i}, available_cols))
        available_cols{end+1} = mandatory_time_cols{i};
        for j = 1 : length(data)
            if cell_is_not_empty(j)
                if isfield(data{j}.info, 'apply_plan_to_grid')
                    error('apply_plan_to_grid:check_input',...
                        'You are trying to reaggregate data but have dropped %s. I won''t let you do this as this can cause funny/weird/surprising/WTF?! time grids to be generated',...
                        mandatory_time_cols{i});
                end
                data{j}.value(:, end+1) = data{j}.date;
                data{j}.colnames{end+1} = mandatory_time_cols{i};
            end
        end
        cols.available_cols = available_cols;
        st_log(['WARN: the indicator %s has been built from the field date\n\t' ...
            'You should worry if the whole process was not working on tick by tick data.\n'], ...
            mandatory_time_cols{i});
    end
    if ~any(strcmp(mandatory_time_cols{i}, wanted_outcols))
        wanted_outcols{end+1} = mandatory_time_cols{i};
    end
end
%>

ac_hk = convs('safe_str', available_cols);

mg_cols = make_grid(grid_inputs.grid_mode, 'return_cols', grid_inputs.o_grid{:});
% mg_hk = hash(convs('safe_str', mg_cols), 'MD5');


%< On repere le premier cell non vide
ne_1st = find(cell_is_not_empty,1, 'first');
%>
%> *

%< * Find out what aggregation path should be used :
[indicators, indicators_hk] = indicator_factory('add_replace_indicators', ...
    on_the_fly_indicators);

%< Making an hash key with indicators_computation_plan inputs in order to
% avoid having to recompute the optimal aggregation path 
if iscell(grid_inputs.grid_mode)
    gms = grid_inputs.grid_mode{1};
else
    gms = grid_inputs.grid_mode;
end
my_full_hash_key = [gms '_' hash([cols.rc_hk ac_hk indicators_hk cols.mg_hk], 'MD5')]; % TODO ???????? est ce bien injectif ?
%>
all_formulas = cat(1, indicators.formula);
all_slicers = cat(1, indicators.slicer);

if isempty(mem_computation_plan) || ~isfield(mem_computation_plan, my_full_hash_key)
    [ind_preformulas, ind_slicers, ind_postformulas, cost, used_cols, col_dico] = ...
        indicators_computation_plan(wanted_outcols, ...
        available_cols, all_formulas, all_slicers, mg_cols.cols_needed4grid,...
        mg_cols.cols_provided_by_make_grid, POST_FORMULA_COST_SAVING_FACTOR);
    mem_computation_plan.(my_full_hash_key) = struct(...
        'ind_preformulas', {ind_preformulas}, 'ind_slicers', {ind_slicers}, ...
        'ind_postformulas', {ind_postformulas}, 'cost', {cost}, ...
        'used_cols', {used_cols}, 'col_dico', {col_dico});
else
    ind_preformulas = mem_computation_plan.(my_full_hash_key).ind_preformulas;
    ind_slicers = mem_computation_plan.(my_full_hash_key).ind_slicers;
    ind_postformulas = mem_computation_plan.(my_full_hash_key).ind_postformulas;
%     cost = mem_computation_plan.(my_full_hash_key).cost;
%     used_cols = mem_computation_plan.(my_full_hash_key).used_cols;
    col_dico = mem_computation_plan.(my_full_hash_key).col_dico;
end
formulas_apriori = all_formulas([ind_preformulas{:}]);
slicers = all_slicers(ind_slicers);
formulas_aposteriori = all_formulas([ind_postformulas{:}]);

%> *


%< Ajout aux st_data d'origine (non vides) de colonnes calcul�es par les formulas_apriori
if ~isempty(formulas_apriori)
    for c = 1:length(data)
        if cell_is_not_empty(c)
            [data{c}, ind_in_current_cols] = compute_formula(data{c}, formulas_apriori,...
                [ind_preformulas{:}],col_dico,col_dico.ind_in_ac);
        end
    end
else
    ind_in_current_cols = col_dico.ind_in_ac;
end
%>


%< Rustine LSE : Le fixing de la colonne de INTRADAYSTOPFIXING
% doit avoir pour identifiant ID_FIXING1 et non pas ID_FIXING2
% comme c'est le cas (formula_compute_auction.m).
if ~isempty(formulas_apriori) && any(strcmp('phase',{formulas_apriori.out_col}))
    data = patch_intraday_auction_lse(data);
end
% Fin de la rustine LSE>


%< cr�ation de la grille d'agr�gation
data_out = cell(length(data),1);
grid = cell(length(data),1);
idx_dump = cell(length(data),1);
accum_idx = cell(length(data),1);
kingpin_out = cell(length(data),1);


for c = 1:length(data)
    if cell_is_not_empty(c)
        [data_out{c}, grid{c}, idx_dump{c}, accum_idx{c}, kingpin_out{c}]= ...
            make_grid(grid_inputs.grid_mode, data{c}, grid_inputs.o_grid{:});
    end
end
%>

%< On retire les bins vides si l'option 'no_empty_bins' dans st_data est
% est � true:
if grid_inputs.no_empty_bins
    for c = 1:length(data)
        if cell_is_not_empty(c)
            empty_bins_idx = cellfun(@isempty, grid{c});
            data_out{c} = st_data('from-idx', data_out{c}, ~empty_bins_idx);
            grid{c}(empty_bins_idx) = [];
            for i = 1 : numel(grid{c}) % numel because of 1*0 vectors
                accum_idx{c}(grid{c}{i}) = i;
            end
        end
    end
end
%>

slicers_outcols = {slicers.out_col};

ind_in_current_out_cols = cellfun(@(c)strmatch(c, col_dico.cols, 'exact'), ...
    data_out{ne_1st}.colnames, 'uni', true);

for c = 1:length(data)
    if cell_is_not_empty(c)
        data_out{c}.colnames = cat(2, data_out{c}.colnames,slicers_outcols);
    end
end

idxcols4sl = cell(length(slicers), 1);
for i = 1 : length(slicers)
    ind_in_slicers = col_dico.ind_in_s_inputs{ind_slicers(i)};
    idxcols4sl{i} = NaN(1, length(ind_in_slicers));
    for j = 1:length(ind_in_slicers)
        idxcols4sl{i}(j) = find(ind_in_slicers(j) == ind_in_current_cols);
    end
end

slicers_params = [slicers.params];

full_def_vals = cat(2, slicers_params.def_vals);
prefilled_cols_nb = size(data_out{ne_1st}.value, 2); % columns already filled with make_grid
% cumnbcolsout = prefilled_cols_nb +[1;1+cumsum(nb_cols_out)];
nb_cols_not_prefilled = size(full_def_vals, 2);

%< aggr�gation proprement dite

for c = 1:length(data)
    if cell_is_not_empty(c)
        dim4accum = size(grid{c});
        data_out{c}.value(:, prefilled_cols_nb+(1:nb_cols_not_prefilled)) = ...
            repmat(full_def_vals, [dim4accum, 1]);
        if all(isfinite(accum_idx{c})) % si les �l�ments de la grille sont disjoints
            for j = 1 : length(slicers)
                if length(idxcols4sl{j}) > 1
                    for i = 1 : size(grid{c}, 1)
                        if ~isempty(grid{c}{i})
                            data_out{c}.value(i, prefilled_cols_nb+j) = ...
                                slicers(j).params.func(data{c}.value(grid{c}{i}, idxcols4sl{j})); % TODO? mettre un try catch ou le stocker dans une variable temporaire et faire un test pour indiquer � l'utilisateur que ca ne fait pas la bonne dimension
                            
                        end
                    end
                else %We can use accumarray freely on slicers that do not have an order dependent func
                    data_out{c}.value(:, prefilled_cols_nb+j) = accumarray(accum_idx{c}, ...
                        data{c}.value(:, idxcols4sl{j}), dim4accum, slicers(j).params.func,full_def_vals(j));
                end
            end
        else 
            for i = 1 : size(grid{c}, 1)
                if ~isempty(grid{c}{i})
                    ex_vals = data{c}.value(grid{c}{i}, 1:end);
                    for j = 1 : length(slicers)
                        data_out{c}.value(i, prefilled_cols_nb+j) = ...
                            slicers(j).params.func(ex_vals(:, idxcols4sl{j})); % TODO? mettre un try catch ou le stocker dans une variable temporaire et faire un test pour indiquer � l'utilisateur que ca ne fait pas la bonne dimension
                    end
                end
            end
        end
    end
end
%>

%< recopie des champs infos et title
for c = 1:length(data)
    if cell_is_not_empty(c)
        
        data_out{c}.title = data{c}.title;
        data_out{c} = st_data('copy_info_field', data_out{c}, ...
            struct('apply_plan_to_grid', ...
            struct('grid_inputs', {grid_inputs},...
            'cols', {cols}, 'on_the_fly_indicators', {on_the_fly_indicators})), ...
            {'apply_plan_to_grid'});
        data_out{c} = st_data('copy_info_field', data_out{c}, ...
            data{c}.info, {'apply_plan_to_grid', 'data_log'}); 
        
        % codebook handling
        if isfield(data{c}, 'codebook')
           data_out{c}.codebook = data{c}.codebook;
        else
           data_out{c}.codebook = []; 
        end
        
    else
        data_out{c} = st_data('copy_info_field', data{c}, ...
            struct('apply_plan_to_grid', ...
            struct('grid_inputs', {grid_inputs},...
            'cols', {cols}, 'on_the_fly_indicators', {on_the_fly_indicators})), ...
            {'apply_plan_to_grid'});
        %         if isfield(data_out{c}, 'info')
        %             data_out{c}.info  = copy_fields(data{c}.info, data_out{c}.info);
        %         else
        %             data_out{c}.info = data{c}.info;
        %         end
        %         if isfield(data_out{c}.info, 'apply_plan_to_grid')
        %             data_out{c}.info.apply_plan_to_grid(end+1) = aptg_info;
        %         else
        %             data_out{c}.info.apply_plan_to_grid = aptg_info;
        %         end

    end
end
%>

%< Ajout aux st_data de sortie (non vides) de colonnes calcul�es par les formulas    
ind_in_current_out_cols = cat(2,ind_in_current_out_cols, col_dico.indices_so(ind_slicers));
if ~isempty(formulas_aposteriori)
    for c = 1:length(data)
        if cell_is_not_empty(c)
            
            [data_out{c}, ind_in_final_cols] = compute_formula(data_out{c}, formulas_aposteriori,...
                [ind_postformulas{:}],col_dico,ind_in_current_out_cols);
        end
    end
else
    ind_in_final_cols = ind_in_current_out_cols;
end
%>

%< On retire les colonnes ayant servi d'intermediaire, pour ne garder que
%celles demand�es par l'utilisateur
v = version();
if str2num(strtok(v, '.')) > 7 %v(1)
    [ind_final_outcols,~, iu2] = unique(cat(2, col_dico.ind_in_pg, col_dico.ind_in_rc), 'legacy');
else
    [ind_final_outcols,~, iu2] = unique(cat(2, col_dico.ind_in_pg, col_dico.ind_in_rc));
end
[~, iifc] = intersect(ind_in_final_cols, ind_final_outcols);
idx_col2keep = iifc(iu2);

for c = 1:length(data)
    if cell_is_not_empty(c)
        data_out{c}.colnames = data_out{c}.colnames(idx_col2keep);%col_dico.cols(ind_final_outcols);
        data_out{c}.value = data_out{c}.value(:,idx_col2keep);
        
    end
end

%>


output = struct('data_out',{data_out} ,...
    'grid',{grid},...
    'accum_idx', {accum_idx},...
    'idx_dump', {idx_dump},...
    'extract_idx',{cellfun(@(x) find(~x),idx_dump,'uni',false)} ,... %TODO Supprimer entierement cet output
    'kingpin_out',{kingpin_out});


end
