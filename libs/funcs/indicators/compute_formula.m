function [data, ind_in_ac] = compute_formula(data, formulas,ind_formula,col_dico,ind_in_ac)
% COMPUTE_FORMULA - apply formulas from indicator factory with computation plan
% 
% 
% 
%  See also : apply_plan_to_grid
% 
%  author   : 'rburgot@cheuvreux.com'
%  reviewer : 'athabault@cheuvreux.com'
%  version  : '1'
%  date     : '10/01/2011'
% 

 
nb_c = size(data.value, 2);
value = NaN(size(data.value,1),length(formulas));
value = cat(2,data.value, value);

ind_in_ac = cat(2,ind_in_ac,col_dico.indices_fo(ind_formula));

for i = 1:length(formulas)
    
    indf = ind_formula(i);
    incols_id = find(col_dico.idx_foi(indf, :));
    real_formula = formulas(i).params;
    
    for j=1:length(incols_id)
        idx_colnames = find(incols_id(j) == ind_in_ac);
        real_formula = strrep(real_formula,...
            ['{' col_dico.cols{incols_id(j)} '}'],...
            ['value(:,' num2str(idx_colnames) ')']);
        
    end
    
    value(:,nb_c+i) =  eval( real_formula );
    
end

data.value = value;
data.colnames = cat(2, data.colnames, {formulas.out_col});

end

