function full_path_png = create_png_from_dot_code(dot_code, filename, dirp)
% 
%  POST_FORMULA_COST_SAVING_FACTOR = 0.5;
%   indicators = indicator_factory();
%   formulas = cat(1, indicators.formula);
%   slicers = cat(1, indicators.slicer);
%   [ind_preformulas, ind_slicers, ind_postformulas, cost, used_cols,col_dico] = ...
%       indicators_computation_plan({'spread_bp'}, {'price','bid','ask','volume'}, formulas, ...
%       slicers, {}, {'end_time', 'begin_time'}, ...
%       POST_FORMULA_COST_SAVING_FACTOR)
%   
%   path_describer([], ...
%       formulas, slicers, ...
%       POST_FORMULA_COST_SAVING_FACTOR, ...
%       {ind_preformulas}, {ind_slicers}, {ind_postformulas}, cost)
%   
%   [c_pre, c_s, c_post] = path_formater([], ...
%       formulas, slicers, ...
%       POST_FORMULA_COST_SAVING_FACTOR, ...
%       {ind_preformulas}, {ind_slicers}, {ind_postformulas}, cost)
%   
%   dot_code = path_dotifier(c_pre{1}, c_s{1}, c_post{1});
%   
%   dirp = 'C:\st_repository\pdf\dot';
%   filename = 'test_dot_code';
%   
%   create_png_from_dot_code(dot_code, filename, dirp)
global st_version
if nargin < 3 
    dirp = fullfile(st_version.my_env.st_repository, 'dot');
end
if ~exist(dirp, 'dir')
    mkdir(dirp);
    st_log('Creating directory : <%s>', dirp);
end
if nargin < 2 || isempty(filename)
    id = 1;
    files_ = dir(dirp);
    if ~isempty(files_);
        files_names = cat(1, {files_.name});
        files_names(1:2) = [];
        for i = 1 : length(files_names)
            f_name = files_names{i};
            if f_name(1) == 't'
                if (strcmp(f_name(1:4), 'tmp_') && strcmp(f_name(end - 3: end), '.png'))
                    id = max(str2double(f_name(5:end-4)) + 1, id);
                end
            elseif f_name(1) > 't'
                break;
            end
        end
    end
    filename = sprintf('tmp_%d', id);
end

full_path_dot = [fullfile(dirp, filename) '.dot'];
full_path_png = [fullfile(dirp, filename) '.png'];

fid = fopen(full_path_dot, 'w');
fprintf(fid,dot_code);
fclose(fid);

[t, t2] = system(['dot -Tpng ' full_path_dot ' -o' full_path_png]);

if strcmp(t2, sprintf('''dot'' is not recognized as an internal or external command,\noperable program or batch file.\n'));
    error('create_png_from_dot_code:exec', 'Please download and install graphviz first :\n\t web(''http://www.graphviz.org/'', ''-browser'')');
end
end