function out = formula_aggressivity(mode, aggressivity, cdk)
% FORMULA_AGGRESSIVITY - Short_one_line_description
%
%
%
% Examples:
%
% 
% 
%
% See also:
%    
%
%   author   : 'athabault@cheuvreux.com'
%   reviewer : ''
%   date     :  '08/08/2011'
%   
%   last_checkin_info : $Header: formula_aggressivity.m: Revision: 1: Author: antha: Date: 08/08/2011 08:29:45 PM$


FLAG_REMOVE = codebook('extract','names2ids',cdk,'agressivity',{'R'}); % BEWARE THERE IS ONLY ONE G


switch lower(deblank(mode))
    case 'is_aggressive'
       out = aggressivity == FLAG_REMOVE;
        
    case 'is_aggressivity_unknown'
       out = isnan(aggressivity);
        
    otherwise
        error('formula_aggressivity:mode', 'MODE: <%s> unknown', mode);
        
end

end