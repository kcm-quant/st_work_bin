function v = formula_compute_auction(mode,varargin)

persistent phase_id
if isempty(phase_id)
    [~,phase_id,~] = trading_time_interpret([],[]);
end

switch lower(mode)
    case 'phase'
        opening_auction = varargin{1};
        intraday_auction = varargin{2};
        closing_auction = varargin{3};
        auction = varargin{4};
        trading_at_last = varargin{5};
        trading_after_hours = varargin{6};
        periodic_auction = varargin{7};
        
        assert(~any(sum([opening_auction intraday_auction closing_auction], 2)>1), ...
            'Phases booleans are supposed to be mutually exclusive');
        
        
        if any(sum([auction trading_at_last trading_after_hours], 2)>1)
            trading_at_last = trading_at_last & ~auction;
            trading_after_hours = trading_after_hours & ~(auction|trading_at_last);
            warning('There is at least one deal flagged both auction and trading_at_last or trading_after_hours. We will make an arbitrary choice of priority on these flag');
        end
        
        v = phase_id.ID_FIXING2 * intraday_auction + ...
            phase_id.OPEN_FIXING * opening_auction + ...
            phase_id.CLOSE_FIXING * closing_auction + ...
            phase_id.VOL_FIXING * (auction & ~(intraday_auction|opening_auction|closing_auction|periodic_auction) )+...
            phase_id.TRADING_AT_LAST * trading_at_last +...
            phase_id.TRADING_AFTER_HOURS * trading_after_hours +...
            phase_id.PERIODIC_AUCTION * periodic_auction;
        
        
    case 'auction'
        phase = varargin{1};
        v = ismember(phase,...
            [phase_id.OPEN_FIXING, phase_id.ID_FIXING1, phase_id.ID_FIXING2, ...
            phase_id.CLOSE_FIXING, phase_id.VOL_FIXING]);
    case 'opening_auction'
        phase = varargin{1};
        v = phase == phase_id.OPEN_FIXING;
    case 'closing_auction'
        phase = varargin{1};
        v = phase == phase_id.CLOSE_FIXING;
    case 'intraday_auction'
        phase = varargin{1};
        v = ismember(phase,...
            [phase_id.ID_FIXING1, phase_id.ID_FIXING2]);
    case 'stop_auction'
        phase = varargin{1};
        v = phase == phase_id.VOL_FIXING;
    case 'trading_at_last'
        phase = varargin{1};
        v = phase == phase_id.TRADING_AT_LAST;
    case 'trading_after_hours'
        phase = varargin{1};
        v = phase == phase_id.TRADING_AFTER_HOURS;
    case 'periodic_auction'
        phase = varargin{1};
        v = phase == phase_id.PERIODIC_AUCTION;
end
