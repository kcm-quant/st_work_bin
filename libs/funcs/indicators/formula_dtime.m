function out = formula_dtime(fake_var,time,varargin)
% FORMULA_DTIME - Short_one_line_description
%
%
%
% Examples:
%
% 
% 
%
% See also:
%    
%
%   author   : ''
%   reviewer : ''
%   date     :  '24/11/2011'
%   
%   last_checkin_info : $Header: formula_dtime.m: Revision: 3: Author: nijos: Date: 08/08/2012 08:38:52 AM$


group_by_data=[];

if ~isempty(varargin)
    group_by_data=cell2mat(varargin);
    
end


if isempty(group_by_data)
    
    if size(time,1)==1
        out=1;
    else
        out=cat(1,diff(time),0);
    end
else
    out=NaN(size(time,1),1);
    [~,~,idx_gby]=unique(group_by_data,'rows');
    uni_idx_gby=unique(idx_gby);
    
    for i=1:length(uni_idx_gby)
        id=idx_gby==uni_idx_gby(i);
        if sum(id)==1
            out(id)=1;
        else
            out(id)=cat(1,diff(time(id)),0);
        end
        
    end
end


end