function v = formula_ifthenelse(cond, iftrue, iffalse)
s_cond = size(cond, 1);
s_iftrue = size(iftrue, 1);
s_iffalse = size(iffalse, 1);
sizev = max([s_cond, s_iftrue, s_iffalse]);
if s_cond == 1
    cond = repmat(cond, sizev, 1);
end
if s_iftrue == 1
    iftrue = repmat(iftrue, sizev, 1);
end
if s_iffalse == 1
    iffalse = repmat(iffalse, sizev, 1);
end
v = iffalse;
v(logical(cond)) = iftrue(logical(cond));
end