function out = formula_market_order_id(mode,varargin)
% UNTITLED - Short_one_line_description
%
%
%
% Examples:
%
% 
% 
%
% See also:
%    
%
%   author   : 'njoseph@cheuvreux.com'
%   reviewer : ''
%   date     :  '30/07/2012'
%   
%   last_checkin_info : $Header: formula_market_order_id.m: Revision: 1: Author: nijos: Date: 08/08/2012 08:38:34 AM$

out=[];

opt = options_light({...
    'data',[],...
    'group_by_variables',{'trading_destination_id','dark','opening_auction','intraday_auction','closing_auction','auction'},...
    'price_eps', 1e-5, ... %Magic Number :  used to round prices given by Sybase
    'time_eps', 15e-3/(24*3600), ... % Maximal time between two trades with the same liquidity consuming order
    },varargin);

[data b_data]=st_data('isempty',opt.data);
if b_data
    return
end
price_eps=opt.price_eps;
time_eps=opt.time_eps;
group_by_variables=opt.group_by_variables;

needed_cols=cat(2,'price',group_by_variables);
if ~all(ismember(needed_cols,data.colnames))
   error('formula_market_order_id:bad "data" input'); 
end


out=NaN(length(data.date),1);

values_gbv = st_data('cols', data, group_by_variables);
v = version();
if strtok(v, '.') > '7' %v(1)
    [uv_gbv, ~,idx_in_uvgbv] = unique(values_gbv, 'rows', 'legacy');
else
    [uv_gbv, ~,idx_in_uvgbv] = unique(values_gbv, 'rows');
end

switch lower(deblank(mode))
    
    case 'time_price'
        last_market_order_id=0;
        for i=1:size(uv_gbv,1)
            idx_tm = find(idx_in_uvgbv==i);
            data_this_modality = st_data('from-idx', data, idx_tm);
            
            [seq_begins_tm, seq_ends_tm] = ...
                compute_begin_ends4liquidity_consumation(data_this_modality, price_eps, time_eps);
            
            idx_start=find(seq_begins_tm);
            idx_end=find(seq_ends_tm);
            
            for i_o=1:length(idx_start)
                out(idx_tm(idx_start(i_o):idx_end(i_o)))=last_market_order_id+i_o;
            end
            
            last_market_order_id=last_market_order_id+length(idx_start);
        end
        
        
     case 'time_only'   
        
           last_market_order_id=0;
           for i=1:size(uv_gbv,1)
               idx_tm = find(idx_in_uvgbv==i);
               data_this_modality = st_data('from-idx', data, idx_tm);
               
               if length(idx_tm)>1
                   diff_time=diff([0;data_this_modality.date]);
                   
                   switch_order=diff_time>time_eps;
                   
                   out_tmp=NaN(length(idx_tm),1);
                   out_tmp(abs(switch_order)>0)=last_market_order_id+transpose(1:sum(abs(switch_order)>0));
                   out_tmp=fill_nan_v2(out_tmp,'mode','before');
                   
                   
                   out(idx_tm)=out_tmp;

                   last_market_order_id=max(out_tmp);
               else
                   out(idx_tm)=last_market_order_id+1;
                   last_market_order_id=last_market_order_id+1;
               end
           end
           
         
        
    otherwise
        error('formula_side_volume:mode', 'MODE: <%s> unknown', mode);
        
end

end


function [seq_begins, seq_ends] = compute_begin_ends4liquidity_consumation(data, price_eps, time_eps)

price  = round(st_data('col', data, 'price') / price_eps) * price_eps;

% % sizes = {st_data('col', data, 'ask_size');st_data('col', data, 'bid_size')};
% % prices_ob = {round(st_data('col', data, 'ask') / price_eps) * price_eps;...
% %     round(st_data('col', data, 'bid') / price_eps) * price_eps};

secs    = data.date;

% seq_begins =  ... logical(diff(sells)) ... % ou bien on change de c�t� du carnet % TODO????
%     (diff(secs) > time_eps)... % Ou bien l'horodatage permet de conclure que ce n'est pas le m�me ordre agressif
%     | any(diff([sizes{1}, sizes{2}, prices_ob{1}, prices_ob{2}]), 2); % Ou bien le carnet d'ordre a �t� updat� entre les deux transactions, ce qui n'est pas le cas s'il s'agit du m�me ordre march�
seq_begins =  ... logical(diff(sells)) ... % ou bien on change de c�t� du carnet % TODO????
    (diff(secs) > time_eps);... % Ou bien l'horodatage permet de conclure que ce n'est pas le m�me ordre agressif
seq_begins = [true; seq_begins];

diff_p  = [0;diff(price)];

for i = 1 : length(seq_begins)
    % curr_moving_side sera 1 si le prix est en train de monter, -1
    % s'il descend, 0 si on ne sait pas encore
    if seq_begins(i)
        curr_moving_side = 0;
    else
        if curr_moving_side == 1 && diff_p(i) <= -price_eps  % le prix montait (ou stagnait mais a mont� dans la s�quence) et maintenant il baisse
            curr_moving_side = -1;
            seq_begins(i) = 1;
        elseif curr_moving_side == -1 && diff_p(i) >= price_eps % le prix baissait (ou stagnait mais a baiss� dans la s�quence) et maintenant il monte
            curr_moving_side = 1;
            seq_begins(i) = 1;
        elseif curr_moving_side == 0 % on ne connaissait pas encore le sens de la s�quence et on en est d�j� au deuxi�me �l�ment de la s�quence
            if diff_p(i) >= price_eps
                curr_moving_side = 1;
            elseif diff_p(i) <= -price_eps
                curr_moving_side = -1;
            end
        end
    end
end

%         acc_volume = accumarray(accum_idx, volume);
%         acc_price  = accumarray(accum_idx, volume.*price) ./ acc_volume;
seq_ends = false(size(seq_begins));
seq_ends([find(seq_begins(2:end));length(seq_begins)]) = true;
end
