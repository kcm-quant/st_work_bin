function out = formula_ob(mode, varargin)
% FORMULA_OB - Short_one_line_description
%
%
%
% Examples:
% %% params
% localtd_start_time=datenum(0,0,0,10,0,0);localtd_end_time=datenum(0,0,0,11,0,0);
% security_id=110;date='04/11/2011';td_id=4;
% %% data
% data_ob = get_orderbook_tbt2('time-filtered','security',security_id,'date',date,'trading-destinations',td_id,'begin-time',localtd_start_time,'end-time',localtd_end_time);
% %% ex1
% out = formula_ob('ofi','data',data_ob,'depth_max',2);
% out = formula_ob('ofic','data',out,'depth_max',2);
% out = formula_ob('discover','data',out,'depth_max',2);
% out_view=st_data('keep-cols',out,'ofi_1;ofi_ask_1;ofi_bid_1;ofi_2;ofi_ask_2;ofi_bid_2;odiscover_bid_1;odiscover_bid_2;odiscover_ask_1;odiscover_ask_2;bid_price_2;bid_size_2;bid_price_1;bid_size_1;ask_price_1;ask_size_1;ask_price_2;ask_size_2;');
% st_data_explorer(st_data('from-idx',out_view,1:min(1000,size(out_view.value,1))),'datestr_ms',true);
%
%
% See also:
%
%
%   author   : 'njoseph@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '02/06/2011'fixing



opt = options_light({...
    'data',[],...
    'depth_max',5,...
    'PRICE_PRECISION',10^5},varargin);

[data b_data]=st_data('isempty',opt.data);
depth_max=opt.depth_max
PRICE_PRECISION=opt.PRICE_PRECISION;

% TO DO : prendre en compte la variable phase et trading_destination_id !!!!

out=data;

%--------------------------------------------------------------------------
%-- NEEDED INPUT
%--------------------------------------------------------------------------

switch mode
    
    case {'ob_limit_update','ofi','ofi_and_price','discover'}
        
        if b_data
            return
        end
        
        depth_max=min(depth_max,...
            sum(cellfun(@(c)(strcmp(c(1:min(length(c),10)),'bid_price_')),data.colnames)));
        
        % --- decomposed values
        %/// bid price
        cols2keep='';
        for i_d=1:depth_max
            cols2keep=[cols2keep sprintf('bid_price_%d;',i_d)];
        end
        [id idx_in]= ismember(tokenize(cols2keep,';'),data.colnames);
        bid_price=round(data.value(:,idx_in(id))*PRICE_PRECISION)/PRICE_PRECISION;
        %/// bid_size
        cols2keep='';
        for i_d=1:depth_max
            cols2keep=[cols2keep sprintf('bid_size_%d;',i_d)];
        end
        [id idx_in]= ismember(tokenize(cols2keep,';'),data.colnames);
        bid_size=data.value(:,idx_in(id));
        %/// bid_nb
        cols2keep='';
        for i_d=1:depth_max
            cols2keep=[cols2keep sprintf('bid_nb_%d;',i_d)];
        end
        [id idx_in]= ismember(tokenize(cols2keep,';'),data.colnames);
        bid_nb=data.value(:,idx_in(id));
        %/// ask price
        cols2keep='';
        for i_d=1:depth_max
            cols2keep=[cols2keep sprintf('ask_price_%d;',i_d)];
        end
        [id idx_in]= ismember(tokenize(cols2keep,';'),data.colnames);
        ask_price=round(data.value(:,idx_in(id))*PRICE_PRECISION)/PRICE_PRECISION;
        %/// ask_size
        cols2keep='';
        for i_d=1:depth_max
            cols2keep=[cols2keep sprintf('ask_size_%d;',i_d)];
        end
        [id idx_in]= ismember(tokenize(cols2keep,';'),data.colnames);
        ask_size=data.value(:,idx_in(id));
        %/// ask_nb
        cols2keep='';
        for i_d=1:depth_max
            cols2keep=[cols2keep sprintf('ask_nb_%d;',i_d)];
        end
        [id idx_in]= ismember(tokenize(cols2keep,';'),data.colnames);
        ask_nb=data.value(:,idx_in(id));
        
    case {'ofic'}
        
        if b_data
            return
        end
        
        % ofi doit etre dans les colnames
        depth_max=min(depth_max,...
            sum(cellfun(@(c)(strcmp(c(1:min(length(c),10)),'bid_price_')),data.colnames)));
        
        % --- decomposed values
        %/// ofi_bid
        cols2keep='';
        for i_d=1:depth_max
            cols2keep=[cols2keep sprintf('ofi_bid_%d;',i_d)];
        end
        [id idx_in]= ismember(tokenize(cols2keep,';'),data.colnames);
        ofi_bid=data.value(:,idx_in(id));
        %/// ofi_ask
        cols2keep='';
        for i_d=1:depth_max
            cols2keep=[cols2keep sprintf('ofi_ask_%d;',i_d)];
        end
        [id idx_in]= ismember(tokenize(cols2keep,';'),data.colnames);
        ofi_ask=data.value(:,idx_in(id));
        
        
        
    otherwise
        error('formula_ob_update: this mode is not handle <%s>',mode);
end





%--------------------------------------------------------------------------
%-- COMPUTATION
%--------------------------------------------------------------------------

switch mode
    
    case 'ob_limit_update'
        
        %---------------
        % default values
        %---------------
        out_values=NaN(size(data.value,1),1);
        
        %---------------
        %-- COMPUTATION
        %---------------
        % out =
        % * 0 : if nothing change
        % * value>0 : for the number of the limit change
        
        for i_d=1:depth_max
            diff_tmp=cat(1,zeros(1,6),...
                diff(cat(2,bid_price(:,i_d),bid_size(:,i_d),bid_nb(:,i_d),...
                ask_price(:,i_d),ask_size(:,i_d),ask_nb(:,i_d))));
            out_values(~isfinite(out_values) & any(abs(diff_tmp)>0,2))=i_d;
        end
        out_values(~isfinite(out_values))=0;
        
        %---------------
        %-- create output
        %---------------
        out=st_data('add-col',out,out_values,'ob_limit_update');
        
        
    case 'ofi'
        
        %---------------
        % default output
        %---------------
        ofi_bid=NaN(size(bid_price));
        ofi_ask=NaN(size(bid_price));
        
        %---------------
        %-- COMPUTATION
        %---------------
        
        for i_ob=2:size(ofi_bid,1)
            % bid
            ob_union_tmp=union_2obcons('bid',bid_price(i_ob-1:i_ob,:),bid_size(i_ob-1:i_ob,:));
            if ~isempty(ob_union_tmp)
                % ofi_bid
                for i_p=1:min(size(ob_union_tmp,1),size(ofi_bid,2))
                    ofi_bid(i_ob,i_p)=ob_union_tmp(i_p,3)-ob_union_tmp(i_p,1);
                end
            end
            % ask
            ob_union_tmp=union_2obcons('ask',ask_price(i_ob-1:i_ob,:),ask_size(i_ob-1:i_ob,:));
            if ~isempty(ob_union_tmp)
                for i_p=1:min(size(ob_union_tmp,1),size(ofi_bid,2))
                    ofi_ask(i_ob,i_p)=ob_union_tmp(i_p,3)-ob_union_tmp(i_p,1);
                end
            end
        end
        
        %---------------
        %-- create output
        %---------------
        out_values=cat(2,ofi_bid-ofi_ask,ofi_bid,ofi_ask);
        out_cols='';
        for i_d=1:depth_max
            out_cols=[out_cols sprintf('ofi_%d;',i_d)];
        end
        for i_d=1:depth_max
            out_cols=[out_cols sprintf('ofi_bid_%d;',i_d)];
        end
        for i_d=1:depth_max
            out_cols=[out_cols sprintf('ofi_ask_%d;',i_d)];
        end
        out=st_data('add-col',out,out_values,out_cols);
        
        
    case 'discover'
        
        %---------------
        % default output
        %---------------
        discover_bid=NaN(size(bid_price));
        discover_ask=NaN(size(bid_price));
        
        %---------------
        %-- COMPUTATION
        %---------------
        
        for i_ob=2:size(discover_bid,1)
            % discover_bid
            discover_bid(i_ob,:)=compute_discover('bid',bid_price(i_ob-1:i_ob,:),bid_size(i_ob-1:i_ob,:));
            % discover_bid
            discover_ask(i_ob,:)=compute_discover('ask',ask_price(i_ob-1:i_ob,:),ask_size(i_ob-1:i_ob,:));
        end
        
        %---------------
        %-- create output
        %---------------
        out_values=cat(2,discover_bid,discover_ask);
        out_cols='';
        for i_d=1:depth_max
            out_cols=[out_cols sprintf('odiscover_bid_%d;',i_d)];
        end
        for i_d=1:depth_max
            out_cols=[out_cols sprintf('odiscover_ask_%d;',i_d)];
        end
        out=st_data('add-col',out,out_values,out_cols);
        
    case 'ofic'
        
        %---------------
        %-- COMPUTATION
        %---------------
        ofic_bid=cumsum(ofi_bid,2);
        ofic_ask=cumsum(ofi_ask,2);
        
        %---------------
        %-- create output
        %---------------
        out_values=cat(2,ofic_bid-ofic_ask,ofic_bid,ofic_ask);
        out_cols='';
        for i_d=1:depth_max
            out_cols=[out_cols sprintf('ofic_%d;',i_d)];
        end
        for i_d=1:depth_max
            out_cols=[out_cols sprintf('ofic_bid_%d;',i_d)];
        end
        for i_d=1:depth_max
            out_cols=[out_cols sprintf('ofic_ask_%d;',i_d)];
        end
        out=st_data('add-col',out,out_values,out_cols);
        
        
    case 'ofi_and_price'
        
        %---------------
        % default output
        %---------------
        ofi_bid=NaN(size(bid_price));
        diffp_bid=NaN(size(bid_price));
        ofi_ask=NaN(size(bid_price));
        diffp_ask=NaN(size(bid_price));
        
        %---------------
        %-- COMPUTATION
        %---------------
        
        for i_ob=2:size(ofi_bid,1)
            % bid
            ob_union_tmp=union_2obcons('bid',bid_price(i_ob-1:i_ob,:),bid_size(i_ob-1:i_ob,:));
            if ~isempty(ob_union_tmp)
                for i_p=1:min(size(ob_union_tmp,1),size(ofi_bid,2))
                    ofi_bid(i_ob,i_p)=ob_union_tmp(i_p,3)-ob_union_tmp(i_p,1);
                    diffp_bid(i_ob,i_p)=ob_union_tmp(i_p,2)-bid_price(i_ob-1,1);
                end
            end
            % ask
            ob_union_tmp=union_2obcons('ask',ask_price(i_ob-1:i_ob,:),ask_size(i_ob-1:i_ob,:));
            if ~isempty(ob_union_tmp)
                for i_p=1:min(size(ob_union_tmp,1),size(ofi_bid,2))
                    ofi_ask(i_ob,i_p)=ob_union_tmp(i_p,3)-ob_union_tmp(i_p,1);
                    diffp_ask(i_ob,i_p)=ob_union_tmp(i_p,2)-ask_price(i_ob-1,1);
                end
            end
            %cat(1,ask_price(i_ob,:),bid_price(i_ob,:))
            %cat(1,ask_size(i_ob,:),bid_size(i_ob,:))
            %cat(1,ofi_bid(i_ob,:),diffp_bid(i_ob,:),ofi_ask(i_ob,:),diffp_ask(i_ob,:))
        end
        
        %---------------
        %-- create output
        %---------------
        out_values=cat(2,ofi_bid-ofi_ask,ofi_bid,ofi_ask);
        out_cols='';
        for i_d=1:depth_max
            out_cols=[out_cols sprintf('ofi_%d;',i_d)];
        end
        for i_d=1:depth_max
            out_cols=[out_cols sprintf('ofi_bid_%d;',i_d)];
        end
        for i_d=1:depth_max
            out_cols=[out_cols sprintf('ofi_ask_%d;',i_d)];
        end
        out=st_data('add-col',out,out_values,out_cols);
        
        
    otherwise
        error('formula_ob_update: this mode is not handle <%s>',mode);
end











end



function out=union_2obcons(mode,data_price,data_volume)

out=[];

if size(data_price,1)~=2 || size(data_price,1)~=2 || ~all(size(data_price)==size(data_volume))
    error('union_2obcons:bad mode');
end

switch mode
    
    
    
    case 'bid'
        
        uni_price_tmp=sort(unique(data_price),'descend');
        % attention : si prix egal a zero, negatif ou nana, on prend pas en compte...
        uni_price_tmp(uni_price_tmp<=0)=NaN;
        uni_price_tmp=uni_price_tmp(isfinite(uni_price_tmp));
        
        if isempty(uni_price_tmp)
            return
        end
        
        before_volume=NaN(length(uni_price_tmp),1);
        volume=NaN(length(uni_price_tmp),1);
        
        
        [id1 idx_in1]= ismember(uni_price_tmp,data_price(1,:));
        before_volume(id1)=data_volume(1,idx_in1(id1));
        
        [id2 idx_in2]= ismember(uni_price_tmp,data_price(2,:));
        volume(id2)=data_volume(2,idx_in2(id2));
        
        before_volume(isnan(before_volume) & uni_price_tmp>=data_price(1,end))=0;
        volume(isnan(volume) & uni_price_tmp>=data_price(2,end))=0;
        
        out=cat(2,before_volume,uni_price_tmp,volume);
        
    case 'ask'
        
        uni_price_tmp=sort(unique(data_price),'ascend');
        % attention : si prix egal a zero, negatif ou nana, on prend pas en compte...
        uni_price_tmp(uni_price_tmp<=0)=NaN;
        uni_price_tmp=uni_price_tmp(isfinite(uni_price_tmp));
        
        if isempty(uni_price_tmp)
            return
        end
        
        before_volume=NaN(length(uni_price_tmp),1);
        volume=NaN(length(uni_price_tmp),1);
        
        [id1 idx_in1]= ismember(uni_price_tmp,data_price(1,:));
        before_volume(id1)=data_volume(1,idx_in1(id1));
        
        [id2 idx_in2]= ismember(uni_price_tmp,data_price(2,:));
        volume(id2)=data_volume(2,idx_in2(id2));
        
        before_volume(isnan(before_volume) & uni_price_tmp<=data_price(1,end))=0;
        volume(isnan(volume) & uni_price_tmp<=data_price(2,end))=0;
        
        out=cat(2,before_volume,uni_price_tmp,volume);
        
        
    otherwise
        error('union_2obcons:bad mode');
end

end



function out=compute_discover(mode,data_price,data_volume)


if size(data_price,1)~=2 || size(data_price,1)~=2 || ~all(size(data_price)==size(data_volume))
    error('compute_discover:bad mode');
end

diff_price=diff(data_price);
out=zeros(1,size(data_price,2));

switch mode
    case 'bid'
        idx_diffbidp=find(abs(diff_price)>0);
        if ~isempty(idx_diffbidp)
            for i_p=1:length(idx_diffbidp)
                if diff_price(idx_diffbidp(i_p))>0
                    idx_ok=find(data_price(1,1:idx_diffbidp(i_p))<...
                        data_price(2,idx_diffbidp(i_p)));
                    out(:,idx_diffbidp(i_p))=-sum(data_volume(1,idx_ok));
                else
                    idx_ok=find(data_price(2,1:idx_diffbidp(i_p))<...
                        data_price(1,idx_diffbidp(i_p)));
                    out(:,idx_diffbidp(i_p))=sum(data_volume(2,idx_ok));
                end
            end
        end
        
    case 'ask'
        idx_diffaskp=find(abs(diff_price)>0);
        if ~isempty(idx_diffaskp)
            for i_p=1:length(idx_diffaskp)
                if diff_price(idx_diffaskp(i_p))>0
                    idx_ok=find(data_price(2,1:idx_diffaskp(i_p))>...
                        data_price(1,idx_diffaskp(i_p)));
                    out(:,idx_diffaskp(i_p))=sum(data_volume(2,idx_ok));
                else
                    idx_ok=find(data_price(1,1:idx_diffaskp(i_p))>...
                        data_price(2,idx_diffaskp(i_p)));
                    out(:,idx_diffaskp(i_p))=-sum(data_volume(1,idx_ok));
                end
            end
        end
        
    otherwise
        error('compute_discover:bad mode');
end

end

