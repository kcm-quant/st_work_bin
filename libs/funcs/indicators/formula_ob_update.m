function out = formula_ob_update(mode,varargin)
% FORMULA_OB_UPDATE - Short_one_line_description
%
%
%
% Examples:
% gmt_start_time=datenum(0,0,0,7,1,0);
% gmt_end_time=datenum(0,0,0,7,5,0);
% security_id=110;
% date='30/08/2011';
% td_id=4;
% data_ob = get_orderbook_tbt2('time-filtered','security',security_id,'date',date,'trading-destinations',td_id,'begin-time',gmt_start_time,'end-time',gmt_end_time);
% data_deal = get_tickdb2( 'ft', 'security_id', security_id, 'from',date,'to',date, 'trading-destinations',4,'where_formula', '~({auction}|{trading_at_last}|{cross}|{trading_after_hours})', 'last_formula', '[{price},{volume},{trading_destination_id}]','day-into-date', false);
% data_deal=st_data('from-idx',data_deal,data_deal.date>=data_deal.info.GMT_offset+gmt_start_time & data_deal.date<data_deal.info.GMT_offset+gmt_end_time);
% st_data_explorer(st_data('from-idx',data,1:1000),'datestr_ms',true);
% %% 'ob_update_limit'
% out = formula_ob_update('ob_update_limit',st_data('col',data_ob,'bidPrice1'),1,1,1,1,1,data_ob.value,data_ob.colnames);
% data_ob=st_data('add-col',data_ob,out,'ob_update_limit');
% st_data_explorer(st_data('from-idx',data_ob,101:500),'datestr_ms',true);
% %% 'ofi_and_price'
% out_1 = formula_ob_update('ob_update_limit',st_data('col',data_ob,'bidPrice1'),1,1,1,1,1,data_ob.value,data_ob.colnames);
% out_2 = formula_ob_update('ofi_and_price',st_data('col',data_ob,'bidPrice1'),1,1,1,1,1,data_ob.value,data_ob.colnames);
% data_ob=st_data('add-col',data_ob,out_1,'ob_update_limit');
% data_ob=st_data('add-col',data_ob,out_2,'ofi_bid1;ofi_bid2;ofi_bid3;ofi_bid4;ofi_bid5;ofi_ask1;ofi_ask2;ofi_ask3;ofi_ask4;ofi_ask5');
% data_view=st_data('keep-cols',data_ob,'trading-destination-id;bidNb1;bidSize1;bidPrice1;askPrice1;askSize1;askNb1;ob_update_limit;ofi_bid5;ofi_bid4;ofi_bid3;ofi_bid2;ofi_bid1;ofi_ask1;ofi_ask2;ofi_ask3;ofi_ask4;ofi_ask5');
% data_view=st_data('extended_stack',{data_deal,data_view});
% st_data_explorer(st_data('from-idx',data_view,1:min(1000,size(data_view.value,1))),'datestr_ms',true);
% See also:
%
%
%   author   : 'njoseph@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '02/06/2011'fixing



PRICE_PRECISION=10^5;

% TO DO : prendre en compte la variable phase et trading_destination_id !!!!

%////////////
%// INPUT
%////////////

switch mode
    
    case 'ob_update_limit'
        
        if nargin ~=9
            error('formula_ob_update: bad inputs');
        end
        
        bid_price_1=varargin{1};
        %         bid_size_1=varargin{2};
        %         bid_nb_1=varargin{3};
        %         ask_price_1=varargin{4};
        %         ask_size_1=varargin{5};
        %         ask_nb_1=varargin{6};
        values=varargin{7};
        colnames=varargin{8};
        
        
        % default output
        out=NaN(size(bid_price_1,1),1);
        
        depth_max=sum(cellfun(@(c)(strcmp(c(1:min(length(c),8)),'bidPrice')),colnames));
        
        % extract needed values
        cols2keep='';
        for i_d=1:depth_max
            cols2keep=[cols2keep ...
                sprintf('bidPrice%d;bidSize%d;bidNb%d;askPrice%d;askSize%d;askNb%d;',i_d,i_d,i_d,i_d,i_d,i_d)];
        end
        [id idx_in]= ismember(tokenize(cols2keep,';'),colnames);
        size_values=depth_max*3*2;
        if length(idx_in(idx_in>0))~=size_values
            error('formula_ob_update: bad inputs');
        end
        values=values(:,idx_in(id));
        
        % decomposed values
        bid_price=round(values(:,1:6:size_values)*PRICE_PRECISION)/PRICE_PRECISION;
        bid_size=values(:,2:6:size_values);
        bid_nb=values(:,3:6:size_values);
        ask_price=round(values(:,4:6:size_values)*PRICE_PRECISION)/PRICE_PRECISION;
        ask_size=values(:,5:6:size_values);
        ask_nb=values(:,6:6:size_values);
        
        
    case 'ofi_and_price'
        
        if nargin ~=9
            error('formula_ob_update: bad inputs');
        end
        
        bid_price_1=varargin{1};
        values=varargin{7};
        colnames=varargin{8};
        
        depth_max=sum(cellfun(@(c)(strcmp(c(1:min(length(c),8)),'bidPrice')),colnames));
        
        % default output
        out=NaN(size(bid_price_1,1),1);
        
        % extract needed values
        cols2keep='';
        for i_d=1:depth_max
            cols2keep=[cols2keep ...
                sprintf('bidPrice%d;bidSize%d;bidNb%d;askPrice%d;askSize%d;askNb%d;',i_d,i_d,i_d,i_d,i_d,i_d)];
        end
        [id idx_in]= ismember(tokenize(cols2keep,';'),colnames);
        size_values=depth_max*3*2;
        if length(idx_in(idx_in>0))~=size_values
            error('formula_ob_update: bad inputs');
        end
        values=values(:,idx_in(id));
        
        % decomposed values
        bid_price=round(values(:,1:6:size_values)*PRICE_PRECISION)/PRICE_PRECISION;
        bid_size=values(:,2:6:size_values);
        bid_nb=values(:,3:6:size_values);
        ask_price=round(values(:,4:6:size_values)*PRICE_PRECISION)/PRICE_PRECISION;
        ask_size=values(:,5:6:size_values);
        ask_nb=values(:,6:6:size_values);
        
    otherwise
        error('formula_ob_update: this mode is not handle <%s>',mode);
end





%////////////
%// COMPUTE
%////////////

switch mode
    
    case 'ob_update_limit'
        
        % out =
        % * 0 : if nothing change
        % * value>0 : for the number of the limit change
        
        for i_d=1:depth_max
            diff_tmp=cat(1,zeros(1,6),...
                diff(cat(2,bid_price(:,i_d),bid_size(:,i_d),bid_nb(:,i_d),...
                ask_price(:,i_d),ask_size(:,i_d),ask_nb(:,i_d))));
            out(~isfinite(out) & any(abs(diff_tmp)>0,2))=i_d;
        end
        out(~isfinite(out))=0;
        
        
    case 'ofi_and_price'  
        
        %output definition
        ofi_bid=NaN(size(bid_price));
        diffp_bid=NaN(size(bid_price));
        ofi_ask=NaN(size(bid_price));
        diffp_ask=NaN(size(bid_price));        

        for i_ob=2:size(ofi_bid,1)
            % bid
            ob_union_tmp=union_2obcons('bid',bid_price(i_ob-1:i_ob,:),bid_size(i_ob-1:i_ob,:));
            if ~isempty(ob_union_tmp) 
                for i_p=1:min(size(ob_union_tmp,1),size(ofi_bid,2))
                    ofi_bid(i_ob,i_p)=ob_union_tmp(i_p,3)-ob_union_tmp(i_p,1);
                    diffp_bid(i_ob,i_p)=ob_union_tmp(i_p,2)-bid_price(i_ob-1,1);
                end
            end
            % ask
            ob_union_tmp=union_2obcons('ask',ask_price(i_ob-1:i_ob,:),ask_size(i_ob-1:i_ob,:));
            if ~isempty(ob_union_tmp) 
                for i_p=1:min(size(ob_union_tmp,1),size(ofi_bid,2))
                    ofi_ask(i_ob,i_p)=ob_union_tmp(i_p,3)-ob_union_tmp(i_p,1);
                    diffp_ask(i_ob,i_p)=ob_union_tmp(i_p,2)-ask_price(i_ob-1,1);
                end
            end
            %cat(1,ask_price(i_ob,:),bid_price(i_ob,:))
            %cat(1,ask_size(i_ob,:),bid_size(i_ob,:))
            %cat(1,ofi_bid(i_ob,:),diffp_bid(i_ob,:),ofi_ask(i_ob,:),diffp_ask(i_ob,:))
        end
        out=cat(2,ofi_bid,ofi_ask);
        

        
    otherwise
        error('formula_ob_update: this mode is not handle <%s>',mode);
end











end



function out=union_2obcons(mode,data_price,data_volume)

out=[];

if size(data_price,1)~=2 || size(data_price,1)~=2 || ~all(size(data_price)==size(data_volume)) 
    error('union_2obcons:bad mode');
end

switch mode
    
     
     
    case 'bid'
        
        uni_price_tmp=sort(unique(data_price),'descend');
        % attention : si prix egal a zero, negatif ou nana, on prend pas en compte...
        uni_price_tmp(uni_price_tmp<=0)=NaN;
        uni_price_tmp=uni_price_tmp(isfinite(uni_price_tmp));
        
        if isempty(uni_price_tmp)
            return
        end
        
        before_volume=NaN(length(uni_price_tmp),1);
        volume=NaN(length(uni_price_tmp),1);
        
        
        [id1 idx_in1]= ismember(uni_price_tmp,data_price(1,:));
        before_volume(id1)=data_volume(1,idx_in1(id1));
        
        [id2 idx_in2]= ismember(uni_price_tmp,data_price(2,:));
        volume(id2)=data_volume(2,idx_in2(id2));
        
        before_volume(isnan(before_volume) & uni_price_tmp>=data_price(1,end))=0;
        volume(isnan(volume) & uni_price_tmp>=data_price(2,end))=0;
        
        out=cat(2,before_volume,uni_price_tmp,volume);

    case 'ask'
        
        uni_price_tmp=sort(unique(data_price),'ascend');
        % attention : si prix egal a zero, negatif ou nana, on prend pas en compte...
        uni_price_tmp(uni_price_tmp<=0)=NaN;
        uni_price_tmp=uni_price_tmp(isfinite(uni_price_tmp));
        
        if isempty(uni_price_tmp)
            return
        end
        
        before_volume=NaN(length(uni_price_tmp),1);
        volume=NaN(length(uni_price_tmp),1);
        
        [id1 idx_in1]= ismember(uni_price_tmp,data_price(1,:));
        before_volume(id1)=data_volume(1,idx_in1(id1));
        
        [id2 idx_in2]= ismember(uni_price_tmp,data_price(2,:));
        volume(id2)=data_volume(2,idx_in2(id2));
        
        before_volume(isnan(before_volume) & uni_price_tmp<=data_price(1,end))=0;
        volume(isnan(volume) & uni_price_tmp<=data_price(2,end))=0;
        
        out=cat(2,before_volume,uni_price_tmp,volume);
        
        
    otherwise
        error('union_2obcons:bad mode');
end
 
end




% TRASH
%         diff_values=NaN(size(values));
%         for i_c=1:size(values,2)
%             tmp_change=cat(1,0,(1+floor(i_c/6.0000001))*(abs(diff(values(:,i_c)))>0));
%             diff_values(tmp_change>0,i_c)=tmp_change(tmp_change>0);
%         end
%         out=min(diff_values,[],2);
%         out(~isfinite(out))=0;