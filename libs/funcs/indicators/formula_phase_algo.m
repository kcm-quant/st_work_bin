function out = formula_phase_algo(mode,varargin)
% FORMULA_PHASE_ALGO - Short_one_line_description
%
%
%
% Examples:
%
% See also:
%    
%
%   author   : 'njoseph@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '27/05/2011'

global simap_version
persistent cdk_phase_algo
if isempty(cdk_phase_algo)
    if isempty(simap_version)
       simap_startup; 
    end
    cdk_phase_algo = simap_version.codebook;
    cdk_phase_algo=cdk_phase_algo(strcmp({cdk_phase_algo.colname},'phase_algo'));
end

s_on=varargin{1};

switch mode
    
    case 'DAY'
        phase_algo_id=codebook('extract','names2ids',cdk_phase_algo,'phase_algo',{mode});
        out=phase_algo_id*ones(size(s_on));
    case 'ON'
        s_act=varargin{2};
        s_on_t=varargin{3};
        s_act_t=varargin{4};
        phase_algo_id=codebook('extract','names2ids',cdk_phase_algo,'phase_algo',{mode});
        out=phase_algo_id*(s_on>0);
        % TO DO : REALLy BAD !!!
        out(out~=phase_algo_id)=codebook('extract','names2ids',cdk_phase_algo,'phase_algo',{'DAY'});
    case 'ACTIVE'
        s_act=varargin{2};
        s_on_t=varargin{3};
        s_act_t=varargin{4};
        phase_algo_id=codebook('extract','names2ids',cdk_phase_algo,'phase_algo',{mode});
        out=phase_algo_id*(s_act>0);
        % TO DO : REALLy BAD !!!
        out(out~=phase_algo_id)=codebook('extract','names2ids',cdk_phase_algo,'phase_algo',{'DAY'});
    case 'ON_TARGET'
        s_act=varargin{2};
        s_on_t=varargin{3};
        s_act_t=varargin{4};
        phase_algo_id=codebook('extract','names2ids',cdk_phase_algo,'phase_algo',{mode});
        out=phase_algo_id*(s_on_t>0);
        % TO DO : REALLy BAD !!!
        out(out~=phase_algo_id)=codebook('extract','names2ids',cdk_phase_algo,'phase_algo',{'DAY'});
    case 'ACTIVE_TARGET'
        s_act=varargin{2};
        s_on_t=varargin{3};
        s_act_t=varargin{4};
        phase_algo_id=codebook('extract','names2ids',cdk_phase_algo,'phase_algo',{mode});
        out=phase_algo_id*(s_act_t>0);
        % TO DO : REALLy BAD !!!
        out(out~=phase_algo_id)=codebook('extract','names2ids',cdk_phase_algo,'phase_algo',{'DAY'});
        
    otherwise
        error('formula_phase_algo:mode', 'MODE: <%s> unknown', mode);
        
end

end