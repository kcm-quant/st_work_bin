function out = formula_phase_on(mode, varargin)
% FORMULA_PHASE_ON - Short_one_line_description
%
%
%
% Examples:
% out = formula_phase_on('phase_th')
%
%
%
% See also:
%
%
%   author   : 'njoseph@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '02/06/2011'fixing




%////////////
%// INPUT
%////////////


% profile on

switch mode
    
    case 'phase_th'
        %// extract input
        if nargin==7
            begin_time=varargin{1};
            end_time=varargin{2};
            phase=varargin{3};
            td_id=varargin{4};
            cdk=varargin{5};
            info=varargin{6};
        else
            error('formula_phase_on')
        end
        
        %// test
        if isempty(cdk) || isempty(info) || ~isfield(info,'td_info')
            error('formula_phase_on: bad input')
        end
        
        %// extract
        place_timezone=info.timezone;
        td_phase_info=info.td_info;
        
    case 'phase'
        
        %// extract input
        if nargin==9
            begin_time=varargin{1};
            end_time=varargin{2};
            time_open=varargin{3};
            time_close=varargin{4};
            phase=varargin{5};
            td_id=varargin{6};
            cdk=varargin{7};
            info=varargin{8};
            
            begin_time(~isfinite(begin_time))=mod(time_open(~isfinite(begin_time)),1);
            end_time(~isfinite(end_time))=mod(time_close(~isfinite(end_time)),1);
            
        else
            error('formula_phase_on')
        end
        
        %// test
        if isempty(cdk) || isempty(info) || ~isfield(info,'td_phase_info')
            error('formula_phase_on: bad input')
        end
        
        %// extract
        place_timezone=info.timezone;
        td_phase_info=info.td_phase_info;
        
    otherwise
        error('formula_phase_on: mode is not handle')
end



%////////////
%// Computation
%////////////

if isempty(td_phase_info)
    out=NaN(size(phase));
    return
end

out=zeros(size(phase));

uni_td_id=unique(td_id(isfinite(td_id)));

% 1/ BOUCLE : ON TD
for i_td=1:length(uni_td_id)
    
    % // preliminary : id and test
    id_in_tdphase=ismember([td_phase_info.trading_destination_id],uni_td_id(i_td));
    if ~any(id_in_tdphase)
        continue
    end
    % // extract
    phase_info=td_phase_info(id_in_tdphase).phase;
    td_timezone=td_phase_info(id_in_tdphase).timezone;
    day=td_phase_info.day;
    
    
    if isempty(phase_info)
        continue
    end
    uni_phase=unique(phase(td_id==uni_td_id(i_td)));
    uni_phase(~isfinite(uni_phase))=[];
    if isempty(uni_phase)
        continue
    end
    
    % // computation
    offset_time=timezone('get_offset','initial_timezone',td_timezone,...
        'final_timezone',place_timezone,'dates',day);
    
    MIN_DURATION=10/(24*3600*1000);
    
    for i_p=1:length(uni_phase)
        
        % test and recup
        id_ph=ismember([phase_info.phase_id],uni_phase(i_p));
        if ~any(id_ph)
            continue
        end
        phase_info_tmp=phase_info(id_ph);
        is_fixing_phase=~isempty(strfind(phase_info_tmp.phase_name,'FIXING'));
        
        % recup begin and end time in data
        id_in_data=td_id==uni_td_id(i_td) & phase==uni_phase(i_p);
        
        uni_time=unique(cat(2,begin_time(id_in_data),end_time(id_in_data)),'rows');
        
        on_off_matrix_tmp=phase_info_tmp.on_off_matrix+offset_time;
        
        uni_time=uni_time(~(uni_time(:,2)<on_off_matrix_tmp(1,1)-MIN_DURATION | ...
            uni_time(:,1)>on_off_matrix_tmp(end,end)+MIN_DURATION),:);
        
        if ~isempty(uni_time)
            for i_u=1:size(uni_time,1)
                tmp_val=compute_on(on_off_matrix_tmp,uni_time(i_u,:),MIN_DURATION,is_fixing_phase);
                out(id_in_data & begin_time==uni_time(i_u,1) & end_time==uni_time(i_u,2))=tmp_val;
            end
        end
        % datestr_ms(begin_time(find(out>0 & id_in_data)))
        % datestr_ms(end_time(find(out>0 & id_in_data)))
    end
    
end














end



function out=compute_on(on_off_matrix,slice_time,MIN_DURATION,is_fixing_phase)

% Remark : if on_off_matrix_start==on_off_matrix_end et == borne de
% slice_time : transform_on_off_time_matrix, va tjs sortir du vide...
if is_fixing_phase 
    on_off_matrix=on_off_matrix+MIN_DURATION;% pour que ce soit bien dans le slice "d'apres"
    id_equal=on_off_matrix(:,2)==on_off_matrix(:,1);
    on_off_matrix(id_equal,2)=on_off_matrix(id_equal,2)+MIN_DURATION;
end

out_mat=transform_on_off_time_matrix('merge_slice_ref',on_off_matrix,slice_time);

if isempty(out_mat)
    out=0;
else
    dur_tmp=sum(out_mat(:,2)-out_mat(:,1));
    if dur_tmp==0
        % Il y a des horarires, donc c'est obligatourement different de 0 en temps
        dur_tmp=MIN_DURATION;
    end
    dur_slice=slice_time(2)-slice_time(1);
    if abs(dur_tmp-dur_slice)<MIN_DURATION
        out=dur_slice;
    else
        out=dur_tmp;
    end
end

end
