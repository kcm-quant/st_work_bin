function out = formula_reference_price(mode, varargin)
% FORMULA_REFERENCE_PRICE - Short_one_line_description
%
%
%
% Examples:
% vals=[3 1 2 2 2 2 1 2 1 NaN 3 NaN NaN 1 2 1 1 2 3 4 4 3 5]';
% cat(2,vals,formula_reference_price('next_mid_deal',vals),formula_reference_price('next_price',vals,3))
% data=st_data('sort',read_dataset('tick4simap','security_id',26,'from','05/09/2012','to','05/09/2012'),'.date');
% figure;hold on;plot(data.date,st_data('col',data,'price'),'o');stairs(data.date,formula_reference_price('last_mid_deal',st_data('col',data,'price')),'-*k');
%
% See also:
%
%
%   author   : 'njoseph@cheuvreux.com'
%   reviewer : ''
%   date     :  '14/09/2012'
%
%   last_checkin_info : $Header: formula_reference_price.m: Revision: 3: Author: nijos: Date: 09/25/2012 04:38:35 PM$

out=[];


switch lower(deblank(mode))
    
    
    case 'next_price'
        
        x=varargin{1};
        nb=1;
        same_in=false;
        if length(varargin)>=2
            nb=varargin{2};
        end
        if length(varargin)>=3
            same_in=varargin{3};
        end
        
        out=NaN(size(x,1),nb);
        
        idx_finite=find(isfinite(x));
        if length(idx_finite)<=1
            return
        end
        x(idx_finite(1):idx_finite(end))=fill_nan_v2(x(idx_finite(1):idx_finite(end)),'mode','before');
        
        idx_finite=find(isfinite(x));
        x=x(idx_finite);
        dx=diff([0;x]);
        idx_price_change=find(abs(dx)>0);
        if length(idx_price_change)<=2
            return
        end
        
        %/ add firsdt same price for the fill nan...
        %/ attention pour les deals au meme prix, on le prend en compte ou pas !
        if same_in
            dindex=find(diff([idx_price_change(1);idx_price_change])>1);
            if any(dindex)
                idx_price_change=unique(cat(1,idx_price_change,idx_price_change(dindex-1)+1));
            end
        end
        x=x(idx_price_change);
        out_price_change=NaN(length(idx_price_change),nb);
        for i_=1:nb
            if length(idx_price_change)>i_+1
                for i_v=1:length(idx_price_change)-i_
                    idx_tmp=find(~ismember(x(i_v+1:end),out_price_change(i_v,1:i_-1)),1,'first');
                    if ~isempty(idx_tmp)
                        out_price_change(i_v,i_)=x(i_v+idx_tmp);
                    end
                end
            end
        end
        out(idx_finite(idx_price_change),:)=out_price_change;
        for i_=nb:-1:1
            if i_==1
                out(1:idx_finite(idx_price_change(end-1)),i_)=fill_nan_v2(out(1:idx_finite(idx_price_change(end-1)),i_),'mode','before');
            else
                out(:,i_)=fill_nan_v2(out(:,i_),'mode','before');
            end
        end
        for i_=nb:-1:1
            if i_>1
                out(out(:,i_)==out(:,i_-1) | ~isfinite(out(:,i_-1)),i_)=NaN;
            end
        end
        
        
    case 'last_price'
        
        x=varargin{1};
        nb=1;
        same_in=false;
        if length(varargin)>=2
            nb=varargin{2};
        end
        if length(varargin)>=3
            same_in=varargin{3};
        end
        
        
        out=NaN(size(x,1),nb);
        
        idx_finite=find(isfinite(x));
        if length(idx_finite)<=1
            return
        end
        x(idx_finite(1):idx_finite(end))=fill_nan_v2(x(idx_finite(1):idx_finite(end)),'mode','before');
        
        idx_finite=find(isfinite(x));
        x=x(idx_finite);
        dx=diff([0;x]);
        idx_price_change=find(abs(dx)>0);
        if length(idx_price_change)<=2
            return
        end
        
        %/ add firsdt same price for the fill nan...
        %/ attention pour les deals au meme prix, on le prend en compte ou pas !
        if same_in
            dindex=find(diff([idx_price_change(1);idx_price_change])>1);
            if any(dindex)
                idx_price_change=unique(cat(1,idx_price_change,idx_price_change(dindex-1)+1));
            end
        end
        x=x(idx_price_change);
        out_price_change=NaN(length(idx_price_change),nb);
        for i_=1:nb
            if length(idx_price_change)>i_+1
                for i_v=(i_+1):length(idx_price_change)
                    idx_tmp=find(~ismember(x(1:i_v-1),out_price_change(i_v,1:i_-1)),1,'last');
                    if ~isempty(idx_tmp)
                        out_price_change(i_v,i_)=x(idx_tmp);
                    end
                end
            end
        end
        % cat(2,x,out_price_change)
        out(idx_finite(idx_price_change),:)=out_price_change;
        for i_=1:nb
            out(:,i_)=fill_nan_v2(out(:,i_),'mode','before');
        end
        
        
    case 'last_mid_deal'
        
        x=varargin{1};
        same_in=false;
        if length(varargin)>=2
            same_in=varargin{2};
        end
        
        last_vals=formula_reference_price('last_price',x,2,same_in);
        out=0.5*(last_vals(:,1)+last_vals(:,2));
        
    case 'next_mid_deal'
        
        x=varargin{1};
        same_in=false;
        if length(varargin)>=2
            same_in=varargin{2};
        end
        vals=formula_reference_price('next_price',x,2,same_in);
        out=0.5*(vals(:,1)+vals(:,2));
        
    otherwise
        error('formula_reference_price:mode', 'MODE: <%s> unknown', mode);
        
end

end