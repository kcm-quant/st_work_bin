function out = formula_score_price(mode,x,y,varargin)
% FORMULA_SCORE_PRICE - Short_one_line_description
%
%
% formula_score_price('base',[1 ; 2], [1 3 4 ; -2 2 7])
% examples:
%
%
%
%
% see also:
%
%
%   author   : 'njoseph@cheuvreux.com'
%   reviewer : ''
%   date     :  '24/09/2012'
%
%   last_checkin_info : $header: mode_func_template.m: revision: 1: author: robur: date: 01/12/2012 02:19:52 pm$



out=[];


switch lower(deblank(mode))
    
    
    case 'base'
        
        % x : represente la valeur a scorer, y : le vecteur des valeurs
        % comparative
        if size(x,1) ~= size(y,1)
            error('formula_score_price:bad input');
        end
        out=nan(size(x,1),1);
        
        idx_=find(all(isfinite(cat(2,x,y)),2));
        if ~isempty(idx_)
            for i_=1:length(idx_)
                out(idx_(i_))=score_compute(x(idx_(i_)),y(idx_(i_),:));
            end
        end
        
      case 'min_max'
        
        % ---- extract input
        % x : represente la valeur a scorer, y : le vecteur des valeurs
        % comparative
        if size(x,1) ~= size(y,1)
            error('formula_score_price:bad input');
        end
        
        if ~isempty(varargin) && any(strcmp(varargin,'side'))
            side=varargin{1+find(strcmp(varargin,'side'))};
            if size(side,1) ~= size(y,1)
                error('formula_score_price:bad input');
            end
        else
            side=ones(size(x,1) ,1);
        end
        
        out=nan(size(x,1),1);
        
        idx_=find(all(isfinite(cat(2,x,y)),2));
        if ~isempty(idx_) 
            id_buy=side(idx_)==1;
            id_sell=side(idx_)==-1;
            if any(id_sell)
                out(idx_(id_sell))=(x(idx_(id_sell))-min(y(idx_(id_sell),:),[],2))./(max(y(idx_(id_sell),:),[],2)-min(y(idx_(id_sell),:),[],2));
            end
            if any(id_buy)
                out(idx_(id_buy))=(max(y(idx_(id_buy),:),[],2)-x(idx_(id_buy)))./(max(y(idx_(id_buy),:),[],2)-min(y(idx_(id_buy),:),[],2));
            end
            out(idx_)=min(cat(2,max(cat(2,zeros(length(idx_),1),out(idx_)),[],2),ones(length(idx_),1)),[],2);
        end
        
    otherwise
        error('formula_score_price:mode', 'mode: <%s> unknown', mode);
        
end


end


function out = score_compute(val,vector)
vector=unique(vector);
out=1/length(vector)*sum(val>=vector);
end