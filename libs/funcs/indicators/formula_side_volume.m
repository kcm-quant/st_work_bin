function out = formula_side_volume(mode, varargin)
% FORMULA_SIDE_VOLUME - Short_one_line_description
%
%
%
% Examples:
%
% 
% 
%
% See also:
%    
%
%   author   : 'njoseph@cheuvreux.com'
%   reviewer : ''
%   date     :  '06/08/2012'
%   
%   last_checkin_info : $Header: formula_side_volume.m: Revision: 1: Author: nijos: Date: 08/08/2012 08:38:39 AM$




switch lower(deblank(mode))
    
    case 'mid_sell'
        
        price=varargin{1};
        volume=varargin{2};
        bid=varargin{3};
        ask=varargin{4};
        
        id_sell=price<0.5*(bid+ask);
        id_mid=price==0.5*(bid+ask);
        
        out=zeros(size(price));
        
        out(id_sell)=volume(id_sell);
        out(id_mid)=0.5*volume(id_mid);
       
    
    case 'mid_deal_sell'        
        
        
        price=varargin{1};
        volume=varargin{2};
        
        out=zeros(size(price));
        
        if length(price)>1
            
            delta_price=diff([0;price]);
            idx_price_change=find(abs(delta_price)>0);
            
            
            last1_price=NaN(size(price));
            last2_price=NaN(size(price));
            
            
            if length(idx_price_change)>2
                last1_price(idx_price_change(2:end))=price(idx_price_change(1:end-1));
                last2_price(idx_price_change(3:end))=price(idx_price_change(1:end-2));
            end
            
            last1_price=fill_nan_v2(last1_price,'mode','before');
            last2_price=fill_nan_v2(last2_price,'mode','before');
            
            ref_price=0.5*(last1_price+last2_price);
            
            id_sell=price<ref_price;
            id_undetermined=~isfinite(ref_price) | price==ref_price;
            
            out(id_sell)=volume(id_sell);
            out(id_undetermined)=0.5*volume(id_undetermined);
            
        end
        
        
    otherwise
        error('formula_side_volume:mode', 'MODE: <%s> unknown', mode);
        
end

end