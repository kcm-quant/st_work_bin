function sw_limit4max_qty = formula_size_weighted_limit(limit_sizes,limit_prices,max_qty)
% FORMULA_SIZE_WEIGHTED_LIMIT - Short_one_line_description
%
%
%
% Examples:
%
% 
% 
%
% See also:
%    
%
%   author   : 'athabault@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  03/03/2011

sz = size(limit_sizes);
nb_updates = sz(1);
nb_limits = sz(2);

% sw_limit4max_qty = NaN(nb_updates, 1);

cum_qty = cumsum(limit_sizes,2);

limit_sizes = limit_sizes .* (cum_qty <= max_qty);

for i = 1:nb_updates
    max_limit = find(limit_sizes(i,:), 1, 'last')+1;
    if isempty(max_limit)
        limit_sizes(i,1) = max_qty;
    elseif max_limit < nb_limits+1
        limit_sizes(i,max_limit) = max_qty - cum_qty(i,max_limit-1);
    elseif max_limit == nb_limits+1
        % nothing to do as the cumulated volume over limits is just max_qty
    else 
        limit_sizes(i,:) = NaN; % impossible to gather max_qty from this side of the orderbook
    end
end

sw_limit4max_qty = sum(limit_sizes .* limit_prices,2)/max_qty;

end