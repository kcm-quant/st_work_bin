function out = formula_spread_id(bid_price,ask_price,trading_destination_id)
% FORMULA_SPREAD_ID - Short_one_line_description
%
%
%
% Examples:
%
%
%
%
% See also:
%
%
%   author   : ''
%   reviewer : ''
%   date     :  '24/11/2011'
%
%   last_checkin_info : $Header: formula_spread_id.m: Revision: 1: Author: nijos: Date: 02/02/2012 01:55:28 PM$

replace_=-999;

out=NaN(size(bid_price,1),1);

if nargin<3
    trading_destination_id=ones(size(bid_price,1),1);
end


bid_price(~isfinite(bid_price))=replace_;
ask_price(~isfinite(ask_price))=replace_;

uni_td_id=unique(trading_destination_id(isfinite(trading_destination_id)));

for i_td=1:length(uni_td_id)
    id_tmp=trading_destination_id==uni_td_id(i_td);
    out(id_tmp)=cumsum(any(cat(2,cat(1,0,abs(diff(bid_price(id_tmp)))),...
        cat(1,0,abs(diff(ask_price(id_tmp)))))>0,2));
end



end