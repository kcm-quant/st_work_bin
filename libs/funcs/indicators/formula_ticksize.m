function out = formula_ticksize(data_info,price,trading_destination_id)
% FORMULA_TICKSIZE - Short_one_line_description
%
%
%
% Examples:
%
%
%
%
% See also:
%
%
%   author   : ''
%   reviewer : ''
%   date     :  '24/11/2011'
%
%   last_checkin_info : $Header: formula_ticksize.m: Revision: 1: Author: nijos: Date: 11/24/2011 04:10:32 PM$



% !!!!!!!!!!!!!! ATTENTION !!!!!!!!!!!!!!!!!!!!!!!!!
% ne marche pas pour tous les march?s, exemple Turquie...il faut le prix de
% r?f?rence + la regle... :(


out=NaN(size(price,1),1);

% // if there is the tick_size_matrix in the data_info
if isfield(data_info,'td_info') && ...
        (~isempty(data_info.td_info) && ...
        isfield(data_info.td_info(1),'price_scale'))
    
    all_td_in_tdinfo=[data_info.td_info.trading_destination_id];
    
    if nargin<=2
        %// If no trading_destination_id in inputs
        % // we apply the tick_size of the main_trading_destination
        trading_destination_id=all_td_in_tdinfo(1)*ones(size(price,1),1);
    end
    
    
    uni_td_id=unique(trading_destination_id);
    
    
    for i_t=1:length(uni_td_id)
        
        id_in_=ismember(all_td_in_tdinfo,uni_td_id(i_t));
        if ~any(id_in_)
            continue
        end
        
        tick_size_rule_matrix=data_info.td_info(all_td_in_tdinfo==uni_td_id(i_t)).price_scale;
        idx_in_val=find(trading_destination_id==uni_td_id(i_t));
        
        if ~isempty(tick_size_rule_matrix)
            nb_class_tick_size=size(tick_size_rule_matrix,1);
            if nb_class_tick_size==1
                out(idx_in_val)=tick_size_rule_matrix(2);
            else
                for i_class=1:nb_class_tick_size-1
                    id_tmp=tick_size_rule_matrix(i_class,1)<=price(idx_in_val) &...
                        price(idx_in_val)<tick_size_rule_matrix(i_class+1,1);
                    if any(id_tmp)
                        out(idx_in_val(id_tmp))=tick_size_rule_matrix(i_class,2);
                    end
                end
                id_tmp_end=price(idx_in_val)>=tick_size_rule_matrix(end,1);
                if any(id_tmp_end)
                    out(idx_in_val(id_tmp_end))=tick_size_rule_matrix(end,2);
                end
            end
        end
    end
end



end