function v = formula_vol_gk(open, high, low, close, mean_price, nb_trades, begin_slice, end_slice)
% FORMULA_VOL_GK - Computes 10 minutes GK volatility
v = sqrt(	( (high-low).^2/2 - (2*log(2)-1).*(close-open).^2 ) ...
    ./((mean_price).^2 .*...
    (end_slice-begin_slice) /  0.006944444444)  ) * 1e4; 
% MAGIC NUMBER 0.006944444444 = 10 minutes


% Quand il y a plusieurs transactions dans un intervalle de temps mais
% toutes au m�me prix : 
% - s'il s'agit d'un fixing, alors cela n'a pas de sens de dire que la
%       volatilit� est de 0, normalement end_slice = begin_slice pour un
%       fixing, d'o� le test
% - s'il s'agit d'un intervalle d'une phase de trading en continu le
%       conditions devrait surement porter sur le nombre de points dans cet
%       intervalle

idx2replace = ((v==0) & ( (nb_trades < 20) | (end_slice-begin_slice < 0.000694444444) )) |...
            (nb_trades == 0) |...
            (end_slice-begin_slice < 0.000694444444/2); % En dessous de 30s on force la valeur � NaN (la renormalisation en racine du temps n'est probablement plus valable)
% MAGIC NUMBER 0.006944444444 = 1 minute

v(idx2replace) = NaN;

end