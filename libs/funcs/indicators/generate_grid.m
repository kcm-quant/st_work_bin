
function [grid,accum_idx,begin_slice, end_slice, kingpin] = generate_grid(mode,data,window,step,bt_grid,et_grid,kingpin,...
    exceed_begin,exceed_end,progressive,auction_slicing_mode, user_specified_step_grid)
% GENERATE_GRID - create time grid according to time parameters
%
%
% 'trading_times', struct('opening_auction', datenum(0,0,0,7,15,0), ...
%                             'opening_fixing', datenum(0,0,0,9,0,0), ...
%                             'opening', datenum(0,0,0,9,0,0), ...
%                             'intraday_stop_auction', NaN, ...
%                             'intraday_stop_fixing', NaN, ...
%                             'intraday_stop', NaN, ...
%                             'intraday_resumption_auction', NaN, ...
%                             'intraday_resumption_fixing', NaN, ...
%                             'intraday_resumption', NaN, ...
%                             'closing_auction', datenum(0,0,0,17,30,0), ...
%                             'closing_fixing', NaN, ...
%                             'closing', datenum(0,0,0,17,35,0), ...
%                             'post_opening', datenum(0,0,0,17,35,0), ...
%                             'post_closing', datenum(0,0,0,17,40,0) ...
%                             ), ...
%
%  VARARGIN :
% options({'window',datenum(0,0,0,0,15,0) ,...
%     'step',datenum(0,0,0,0,15,0) ,...
%     'is_gmt', true, ...
%     'bt_grid', mod([trading_times.opening], 1) ,...
%     'et_grid', isnull(mod([trading_times.closing_auction], 1),...
%                         mod([trading_times.closing], 1)),...
%     'kingpin',  mod([trading_times.opening], 1) ,...
%     'exceed_begin', false,...
%     'exceed_end', false,...
%     'auction_slicing_mode', 'single_slice',...
%     'respect_trading_times', true,...
%     'progressive', false,...
%     }, varargin)
%
% 'window'
% 'step'
% 'is_gmt'
% 'bt_grid'   Begin time of the grid
% 'et_grid'   End time of the grid
% 'kingpin'   date that should belong to the grid
% 'exceed_begin'  boolean to indicate if the grid starting point can be before bt_grid
% 'exceed_end'    boolean to indicate if the grid ending point can be after et_grid
% 'auction_slicing_mode'
%          'multi_slice' auction phase is cut in several slices if
%          they are 1/5*window appart
%          'single_slice': 1 auction = 1 slice
%  'respect_trading_times', true,...    :  true=respect trading hours,
%                                           false = respect data.date
%  'progressive', false,... : true if we want cumulated size bins
%
%
%
% Examples:
%
%
%
%
% See also: make_grid
%
%
%   author   : 'athabault@cheuvreux.com'
%   reviewer : 'rburgot@cheuvreux.com'
%   version  : '1.1'
%   date     :  '15/03/2011'

FLOAT_EPS = 0.25/(24*3600);
SEC_DT = 1/(24*3600);
SEP_FACTOR = 1/5;
MICRO_SEC = 24*3600*1e6;



% opt = options({'window',datenum(0,0,0,0,15,0) ,...
%     'step',datenum(0,0,0,0,15,0) ,...
%     'bt_grid', NaN,...
%     'et_grid', NaN,...
%     'kingpin',  NaN ,...
%     'exceed_begin', false,...
%     'exceed_end', false,...
%     'progressive', false,...
%     'auction_slicing_mode', 'single_slice',...
%     }, varargin);
%
%

is_disjoint = window == step;
is_col = @(name)any(strcmp(data.colnames,name));

dts = mod(data.date,1);
if is_col('microseconds') % Si on a la micro-seconde, on peut obtenir une meilleure pr�cision
    dts = (floor(mod(data.date,1)*24*3600)+st_data('cols',data,'microseconds')*1e-6)/24/3600;
    idx_excessive_time_diff = abs(dts-mod(data.date,1))*24*3600*1e6 > st_data('cols',data,'microseconds')+100;
    if any(idx_excessive_time_diff)
        dts(idx_excessive_time_diff) = (round(mod(data.date(idx_excessive_time_diff),1)*24*3600)+...
            st_data('cols',data,'microseconds',idx_excessive_time_diff)*1e-6)/24/3600;
    end
end

assert(issorted(dts), 'generate_grid:check_args', 'Your st_data is not time sorted, I refuse to work with it');

switch lower(mode)
    case 'time_grid' % continuous phase
        if isempty(user_specified_step_grid)
            if step < (4 * FLOAT_EPS)*0.999 || step < SEC_DT*0.999
                error('generate_grid:check_args', ...
                    'Too small step for the accuracy in this function');
            end
            %< On cale la grille sur des heures rondes
            if ~isfinite(kingpin)
                kingpin = floor(et_grid/step)*step;
            end
            %>
            
            %< Tous les traitements de cas particuliers qui suivent se
            % comportent mal dans ce cas particulier...
            if et_grid - bt_grid < 2 * step
                if (progressive && exceed_begin==exceed_end && ...
                    ~exceed_begin && step==window)
                    step_grid = [kingpin et_grid];
                elseif exceed_begin==exceed_end && ...
                    exceed_begin && step==window
                    if bt_grid > kingpin
                        step_grid = (kingpin:step:(floor(et_grid/step)*step))';
                        step_grid = [step_grid (step_grid+step)];
                        step_grid(step_grid(:, 2)<bt_grid-FLOAT_EPS, :) = [];
                        step_grid(step_grid(:, 1)>et_grid+FLOAT_EPS, :) = [];
                    elseif et_grid < kingpin
                        step_grid = (kingpin:-step:(floor(bt_grid/step)*step))';
                        step_grid = step_grid(end:-1:1);
                        step_grid = [step_grid (step_grid+step)];
                        step_grid(step_grid(:, 2)<bt_grid-FLOAT_EPS, :) = [];
                        step_grid(step_grid(:, 1)>et_grid+FLOAT_EPS, :) = [];
                    else
                        step_grid = [(kingpin-2*step:step:kingpin+step)', ...
                                    (kingpin-step:step:kingpin+2*step)']; % as et_grid - bt_grid < 2 * step
                    end
                    step_grid = step_grid(:, 2)';
                else
                    error('generate_grid:check_inputs', 'Not yet implemented grid');
                end
            else
                %>
                
                %<
                if et_grid > bt_grid + step % This is the most frequent case
                    %<New version
                    % /!\ step_grid defini la borne sup�rieure de la grille
                    step_grid = mod(round(kingpin*MICRO_SEC), round(step*MICRO_SEC)) : round(step*MICRO_SEC) : 1*MICRO_SEC;
                    step_grid = step_grid/MICRO_SEC;
                    step_grid(step_grid-window+FLOAT_EPS<bt_grid) = [];
                    step_grid(step_grid-FLOAT_EPS>et_grid) = [];
                    %>
                    
                    %< Old version
%                     tmp_step_grid = mod(kingpin, step) : step : 1;
%                     step_grid = tmp_step_grid;
%                     step_grid((find(step_grid > et_grid + FLOAT_EPS, 1, 'first')):end) = [];
%                     step_grid(1:find(step_grid < bt_grid+window - FLOAT_EPS, 1, 'last')) = [];
                    %>
                else
                    step_grid = et_grid;
                end
                if isempty(step_grid)
                    if exceed_end
                        step_grid = tmp_step_grid(find(tmp_step_grid > et_grid + FLOAT_EPS, 1, 'first'));
                    else
                        step_grid = et_grid+ FLOAT_EPS;
                    end
                elseif (et_grid - step_grid(end) )>FLOAT_EPS  % compare_my_dates(g_tail(end), '<', et_grid)%On rajoute les bouts d'intervalles au bout
                    if exceed_end
                        step_grid(end+1) = step_grid(end) + step;
                    elseif (et_grid - step_grid(end) )< 4*FLOAT_EPS
                        step_grid(end) = et_grid;
                    else
                        step_grid(end+1) = et_grid;
                    end
                end
                if  (step_grid(1) - (bt_grid+window))>FLOAT_EPS % compare_my_dates(g_head(end), '>', bt_grid+window)
                    step_grid = cat(2, step_grid(1) - step, step_grid);
                end
                %>
                
                %         %< USED FOR DEBUGGING
                %         datestr(step)
                %         [datestr(bt_grid)  datestr(bt_grid) datestr(td_open)]
                %         [datestr(g_head(1:floor(end/20))); '........' ;datestr(g_head(end-floor(end/20):end))]
                %         [datestr(g_tail(1:floor(end/20))) ; '........' ;datestr(g_tail(end-floor(end/20):end))]
                %         [datestr(et_grid)  datestr(et_grid) datestr(td_close)]
                %         datestr(kingpin)
                %         %>
                %
            end
            
            if isempty(step_grid)
                error('generate_grid:EmptyGrid', 'Step_grid is empty: change params')
            end
            
            
            
            %< fill the bins and return timestamps
            step_grid = step_grid';
            
            
            if progressive
                bound_inf = repmat(bt_grid,length(step_grid),1);
                is_disjoint = false;
            else
                bound_inf = step_grid - window;
                if length(step_grid) > 4
                    bound_inf(2:end-1) = round((step_grid(2:end-1) - window)/SEC_DT)*SEC_DT;
                end
            end
            
            bound_sup = step_grid;
            if length(step_grid) > 4
                bound_sup(2:end-1) = round(step_grid(2:end-1)/SEC_DT)*SEC_DT;
            end
            
            if ~exceed_begin
                bound_inf(1) =  floor(bt_grid/SEC_DT)*SEC_DT;
            end
            
            %attention a ne pas reprendre toutes les valeurs
            if ~exceed_end && is_disjoint
                if length(bound_sup) > 1
                    bound_inf(end) =  bound_sup(end-1);
                else
                    bound_inf(end) =  bound_sup(end)- step;
                end
            end
            

        else
            if progressive
                bound_sup = user_specified_step_grid(2:end);
                bound_inf = user_specified_step_grid(1)*ones(size(bound_sup));
                is_disjoint = false;
            else
                bound_sup = user_specified_step_grid(2:end);
                bound_inf = user_specified_step_grid(1:end-1);
                is_disjoint = true;
            end
        end
        
        bound_sup(end) = bound_sup(end) + FLOAT_EPS; % numeric comparisons are just painful
        bound_inf(1)   = bound_inf(1) - FLOAT_EPS;% numeric comparisons are just painful
        
        nb_bins = length(bound_sup);
        
        nb = length(dts) + 1;
        
        idx_first = nb*ones(nb_bins, 1);
        idx_last  = zeros(nb_bins, 1);
        
        %         if isdisjoint
        %             [~, accum_idx] = histc(dts,[bound_inf(1);bound_sup]);
        %         else
        for i = 1 : nb_bins
            idx = (dts >= bound_inf(i) & dts < bound_sup(i));
            tmp_idx_first = find(idx, 1, 'first');
            if ~isempty(tmp_idx_first)
                idx_first(i) = tmp_idx_first;
                idx_last(i)  = find(idx, 1, 'last');
            end
        end
        %         end

        bound_sup(end) = bound_sup(end) - FLOAT_EPS; % numeric comparisons are just painful
        bound_inf(1)   = bound_inf(1) + FLOAT_EPS;% numeric comparisons are just painful
        
        %>
        
        
    case 'fuzzy_grid' % it's an auction : special treatment
        

        switch lower(auction_slicing_mode)
            case 'single_slice' % auction phase is aggregated in one slice only
                
                idx_first = 1;
                idx_last = length(dts);
                bound_sup = et_grid;
                bound_inf = isnull(bt_grid,min(dts));
                
            case 'multi_slice' %FOR VOLATILITY FIXINGs auction phase is cut in several slices if they are 1/5*window appart
                
                wind = SEP_FACTOR*window; %% Si deux fixing sont s?par?s de cette dur?e on ne les aggr?ge pas ensemble
                if length(dts) > 1
                    idx_first = [1;1+find(diff(dts)>wind)];
                    idx_last = [idx_first(2:end)-1; length(dts)];
                else
                    idx_first = 1;
                    idx_last = 1;
                end
                
                bound_sup = dts(idx_last); %mod(dts(end_subphase),1)+FLOAT_EPS;
                bound_inf = dts(idx_first);%isnull(bt_grid,min(dts));
                
            otherwise
                error('Error in generate_grid, mode : <%s> unknown', mode)
        end
        
        
    otherwise
        error('generate_grid:mode', 'Mode <%s> is unknown', mode);
        
end

% grid_old = [idx_first, idx_last, bound_sup, bound_inf];


grid = cell(size(bound_inf, 1), 1);
begin_slice = bound_inf;
end_slice = bound_sup;
accum_idx = NaN(length(data.date), 1);

for j = 1 : size(grid,1)
    grid{j} = (idx_first(j):idx_last(j))';
    if isempty(grid{j})  % pour contr?ler la dimension des intervalles vides pour eviter de planter les cat dans les autres codes
        grid{j} = [];
    end
end

if is_disjoint
    for i = 1 : size(grid, 1)
        accum_idx(grid{i}) = i;
    end
end

end

