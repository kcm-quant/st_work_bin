function [grid,accum_idx,begin_slice, end_slice ,kingpin,mid_slice, idx_first,idx_last] = ...
    generate_specific_grid(mode,data,window,step,colname_vals_window, colname_vals_step,...
    progressive)
% GENERATE_SPECIFIC_GRID - Short_one_line_description
%
%
%
% Examples:
% mode='basic_forward';data=st_data('init','title','','value',[20 30 15 12 70 17 50 84 101 30]','date',0.1*(1:10)','colnames',{'volume'});
% window=45;
% step=100;
% colname_vals_window='volume';
% colname_vals_step='volume';
% progressive = false;
% [grid,accum_idx,begin_slice, end_slice ,kingpin] = ...
% generate_specific_grid(mode,data,window,step,...
%     colname_vals_window, colname_vals_step,progressive)
%
%
% mode='basic_forward';data=st_data('init','title','','value',[[20 30 15 12 70 17 50 84 101 30]', ones(10,1)],'date',0.1*(1:10)','colnames',{'volume', 'nb_trades'});
% window=45;
% step=1;
% colname_vals_window='volume';
% colname_vals_step='nb_trades';
% progressive = false;
% [grid,accum_idx,begin_slice, end_slice ,kingpin] = ...
% generate_specific_grid(mode,data,window,step,...
%     colname_vals_window, colname_vals_step,progressive)
%
%
% mode='basic_backward';data=st_data('init','title','','value',[[20 30 15 12 70 17 50 84 101 30]', ones(10,1)],'date',0.1*(1:10)','colnames',{'volume', 'nb_trades'});
% window=45;
% step=1;
% colname_vals_window='volume';
% colname_vals_step='nb_trades';
% progressive = false;
% [grid,accum_idx,begin_slice, end_slice ,kingpin] = ...
% generate_specific_grid(mode,data,window,step,...
%     colname_vals_window, colname_vals_step,progressive)
%
% See also:
%
%
%   author   : 'njoseph@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '11/07/2011'


kingpin=NaN;

SEC_DT = 1/(24*3600);
is_disjoint = ~strcmp(mode, 'basic_both') && ...
    strcmp(colname_vals_window, colname_vals_step) && ...
    window == step ;

if progressive && (~is_disjoint || ~strcmp(mode, 'basic_forward'))
    error('generate_specific_grid: progressive grid can only be constructed when (window == step) and mode=basic_forward');
    % TO DO : this si true if and only if the grid is not in time mode !!
    % time_mode + backward + progressive, is possible !!
end

switch lower(deblank(mode))
    
    case 'basic_forward'
        
        cum_vals_window = cumsum(st_data('col',data,colname_vals_window));
        cum_vals_step = cumsum(st_data('col',data,colname_vals_step));
        date=round(mod(data.date,1)/SEC_DT)*SEC_DT;
        
        % -- compute idx_last and idx_first
        tmp_cum_vals=0;
        idx_last=[];
        idx_first=[];
        idx_mid = [];
        
        if ~is_disjoint % in this case, idx_first must be de
            %on calcule idx_last � la frequence window pour en deduire les idx_first finaux
            while tmp_cum_vals<cum_vals_step(end)
                
                tmp_idx=find(cum_vals_step>=tmp_cum_vals+step,1,'first');
                if isempty(tmp_idx)
                    tmp_idx = length(cum_vals_step);
                end
                idx_last = cat(1,idx_last,tmp_idx);
                tmp_cum_vals = cum_vals_step(idx_last(end));
                
            end
            if length(idx_last)==1
                idx_first = 1;
            else
                idx_first = cat(1,1,idx_last(1:end-1)+1);
            end
            
            %on efface les idx_last et on les recalcule � la fr�quence step
            %� partir de chaque idx_first
            idx_last=[];
            for i =1:length(idx_first)
                if idx_first(i)==1
                    tmp_idx = find(cum_vals_window>=window,1,'first');
                else
                    tmp_idx = find(cum_vals_window>=cum_vals_window(idx_first(i)-1)+window,1,'first');
                end
                if isempty(tmp_idx)
                    tmp_idx = length(cum_vals_window);
                end
                idx_last=cat(1,idx_last,tmp_idx);
            end
            
        elseif is_disjoint
            
            while tmp_cum_vals<cum_vals_step(end)
                
                tmp_idx = find(cum_vals_step>=tmp_cum_vals+step,1,'first');
                if isempty(tmp_idx)
                    tmp_idx = length(cum_vals_step);
                end
                idx_last = cat(1,idx_last,tmp_idx);
                tmp_cum_vals = cum_vals_step(idx_last(end));
                
            end
            
            if progressive
                idx_first = ones(length(idx_last),1);
                is_disjoint = false;
            else
                if length(idx_last)==1
                    idx_first = 1;
                else
                    idx_first = cat(1,1,idx_last(1:end-1)+1);
                end
            end
        end
        
    case 'basic_backward'
        
        cum_vals_window = cumsum(st_data('col',data,colname_vals_window));
        cum_vals_step = cumsum(st_data('col',data,colname_vals_step));
        date=round(mod(data.date,1)/SEC_DT)*SEC_DT;
        
        % -- compute idx_last and idx_first
        tmp_cum_vals=0;
        idx_last=[];
        idx_first=[];
        idx_mid = [];
        
        %in basic backward there is no special case for disjoint
        
        %         if ~is_disjoint
        %on calcule idx_last � la frequence window
        while tmp_cum_vals<cum_vals_step(end)
            
            tmp_idx=find(cum_vals_step>=tmp_cum_vals+step,1,'first');
            if isempty(tmp_idx)
                tmp_idx = length(cum_vals_step);
            end
            idx_last = cat(1,idx_last,tmp_idx);
            tmp_cum_vals = cum_vals_step(idx_last(end));
            
        end
        
        
        %         idx_first=NaN(size(idx_last));
        %on calcule les idx_first a partir des idx_last
        
        for i =1:length(idx_last)
            
            tmp_idx = find(cum_vals_window>cum_vals_window(idx_last(i))-window,1,'first');
            
            if isempty(tmp_idx)
                %                     tmp_idx = 1;
                error('generate_specific_grid:bin_start_error','This should happen in basic backward mode');
            end
            idx_first=cat(1,idx_first,tmp_idx);
        end
        
        %         elseif is_disjoint
        %
        %             while tmp_cum_vals<cum_vals_step(end)
        %
        %                 tmp_idx = find(cum_vals_step>=tmp_cum_vals+step,1,'first');
        %                 if isempty(tmp_idx)
        %                     tmp_idx = length(cum_vals_step);
        %                 end
        %                 idx_last = cat(1,idx_last,tmp_idx);
        %                 tmp_cum_vals = cum_vals_step(idx_last(end));
        %
        %             end
        %
        %             if progressive
        %                 idx_first = ones(length(idx_last),1);
        %                 is_disjoint = false;
        %             else
        %                 if length(idx_last)==1
        %                     idx_first = 1;
        %                 else
        %                     idx_first = cat(1,1,idx_last(1:end-1)+1);
        %                 end
        %             end
        %         end
        if progressive %% && is_disjoint
            idx_first = ones(length(idx_last),1);
            is_disjoint = false;
            % error( 'generate_specific_grid:option_compatibility', 'I am not sure basic backward and progressive are compatible,\n if you do, feel free to implement it :o)');
        end
        
        
    case 'basic_both'
        
        
        date=round(mod(data.date,1)/SEC_DT)*SEC_DT;
        
        [~,~,~, ~,~, ~,idx_first_f,idx_last_f] =...idx_first_f
            generate_specific_grid('basic_forward',data,window(end),step,colname_vals_window, colname_vals_step,...
            progressive);
        
        [~,~,~, ~,~, ~,idx_first_b,~] =...idx_last_b
            generate_specific_grid('basic_backward',data,window(1),step,colname_vals_window, colname_vals_step,...
            progressive);
        
        idx_first = idx_first_b(1:end-1);
        idx_last = idx_last_f(2:end);
        idx_mid =  idx_first_f(2:end);
        
        %                 idx_first = [idx_first_f(1); idx_first_b(1:end-1)];
        %         idx_last = idx_last_f;
    otherwise
        error('generate_specific_grid:mode', 'MODE: <%s> unknown', mode);
        
end


%-- output
grid = cell(size(idx_first, 1), 1);
begin_slice = date(idx_first);
end_slice = date(idx_last);
if ~isempty(idx_mid)
    mid_slice = date(idx_mid);
else
    mid_slice = NaN(size(end_slice));
end

accum_idx = NaN(length(data.date), 1);

for j = 1 : size(grid,1)
    grid{j} = (idx_first(j):idx_last(j))';
    if isempty(grid{j})  % pour contr�ler la dimension des intervalles vides pour eviter de planter les cat dans les autres codes
        grid{j} = [];
    end
end

if is_disjoint
    for i = 1 : size(grid, 1)
        accum_idx(grid{i}) = i;
    end
end


end