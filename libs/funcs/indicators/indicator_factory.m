function [indicators, hash_key] = indicator_factory(mode, varargin)
% INDICATOR_FACTORY - builds indicators from st_methodology.xml and
% maintains a persistent variable with indicators
%
%  if an indicator is defined in 'bin' environment in the xml file and in the 
%      local hostname, the bin indicator is overridden by the local definition
%  Local hostname indicators not defined in 'bin' environment are added to the indicators
%
%   About on the fly indicators: 
%
% struct('name', 'dark_mkt_share', ...
%                 'default_value', NaN, ...
%                 'slicer', ...
%                     struct('func', @(x)(nansum(x(:, 1).*x(:, 2)) ./nansum(x(:, 1))), ...
%                     'in_cols', {{'turnover', 'dark'}}), ... % argh do not forget to add {}
%                 'formula', {{'{hum je ne sais}.*{pas quoi mettre}', '{ceci est la deuxi�me formule}'}} ... % idem
%                 )
% 
% 
% 
% 
% 
% ex :
%   indicator_factory() %no mode does a 'get'
%   [indicators, hash_key] = indicator_factory('build_from_xml')
%   indicator_factory('reset')
% 
%   indicator_factory('make-doc') % makes some documentation for indicators
%
% See also indicators_computation_plan st_methodology.xml
%
% author   : 'rburgot@cheuvreux.com'
% reviewer : 'athabault@cheuvreux.com'
% version  : '1'
% date     : '04/01/2011'
%

global st_version
persistent mem_indicators
INDICATORS_FILE = 'st_methodology.xml';
v____ = version();

if nargin < 1
    mode = 'get';
end

switch lower(mode)
    case 'make-doc'
        % < MAGIC LISTs
        ft_cols = {'price', 'volume', 'sell', 'bid', 'ask', 'trading_destination_id', ...
            'bid_size', 'ask_size', 'buy', 'cross', 'auction', 'opening_auction', ...
            'closing_auction', 'trading_at_last', 'trading_after_hours', ...
            'intraday_auction', 'dark', 'time_open', 'time_close'};
        gbft2biss = {'time_open', 'time_close', 'phase','dark' , 'cross', 'trading_destination_id'};
        bi1cols = {'begin_slice','end_slice','phase','dark',...
            'cross','trading_destination_id','volume','turnover','nb_trades',...
            'volume_sell','turnover_sell','nb_sell','vwas','open','high','low',...
            'close','bid_open','bid_high','bid_low','bid_close','ask_open','ask_high',...
            'ask_low','ask_close','sum_price','time_close','time_open'};
        gbbiss2bi = {'time_open', 'time_close', 'phase'};
        bi2cols = {'volume', 'vwap', 'nb_trades', ...
            'volume_sell', 'vwap_sell', 'nb_sell', 'vwas', 'vol_GK', ...
            'open','high','low','close', ...
            'bid_open', 'bid_high', 'bid_low','bid_close', ...
            'ask_open', 'ask_high', 'ask_low','ask_close', ...
            'auction', 'opening_auction', 'closing_auction', ...
            'intraday_auction', 'stop_auction','time_open','time_close'};
        gbbi2bi = gbbiss2bi;
        fichier2 = 'tmp_intermediary_file.tex';
        fpfichier2 = fullfile(st_version.my_env.st_repository, 'pdf', 'tex', 'tmp_intermediary_file.tex');
        if exist(fpfichier2)
            delete(fpfichier2);
            assert(~exist(fpfichier2));
        end
        % >
        [indicators, hash_key] = indicator_factory('build_from_xml');
        fichier = 'indicators_doc';
        latex('header', fichier, 'title', 'Indicators in Matlab');
        
        latex('plain', fichier, ['It might be interesting to first get an introduction to ' ...
            '\href{https://parapache.kepler-equities.net/svn/QuantitativeResearch/branches/malas\_st\_work\_kepler/doc/mmqr\_toolbox/demos/indicators/demo\_agg\_mode\_of\_st\_data.html}{st\_data mode ''agg''} ' ...
            ' as well as '...
            '\href{https://parapache.kepler-equities.net/svn/QuantitativeResearch/branches/malas\_st\_work\_kepler/doc/mmqr\_toolbox/demos/html/demo\_st\_methodology.html}{st\_methodology} ' ...
            '\\\\ \\\\ ' ...
            'The aim of this automatic documentation is to provide a clear ' ...
            'understanding about how indicators are computed as well as their depdendencies.' ...
            '\\\\' ...
            'In the document you will find some computation plans : how' ...
            ' an indicator can be computed from a set of indicators. In such' ...
            ' plans, blue arrows stand for formula, whereas red arrows stands for slicers.' ...
            '\\\\' ...
            ' Formulas produce an indicator from a set of indicators, without any temporal or group by aggregation, ' ...
            ' whereas slicers are the function that will be applied on a set of records to be aggregated.' ...
            ]);
        %         ' as well as \url[st\_data mode ''agg'']{https://parapache.kepler-equities.net/svn/QuantitativeResearch/branches/malas_st_work_kepler/' ...
        %             'framework before reading this document'
        %< Summary
        latex('section', fichier, 'List of Indicators');
        latex('plain', fichier, sprintf(['\\begin{enumerate}[a)]\n\\item default value : the value that should ' ....
            'be taken by the indicator in the case there are no data in a bin.\n\n' ...
            '\n\\item ft : boolean indicating whether this indicator can be found in tick-by-tick data / ' ...
            ' boolean indicated if an aggregation of tick-by-tick data may provide this indicator. \n\n' ...
            '\n\\item BISS : boolean indicating whether this indicator can be found in basic indicators smallest step / ' ...
            ' boolean indicated if an aggregation of basic indicators smallest step data may provide this indicator. \n\n' ...
            '\n\\item BI : boolean indicating whether this indicator can be found in basic indicators / ' ...
            ' boolean indicated if an aggregation of basic indicators may provide this indicator. \n\n' ...
            '\n\\item CA Price : boolean indicating whether this indicator is sensitive '...
            'to a change in price and a correction for splits or dividends is known\n\n'...
            '\n\\item CA volume : boolean indicating that this indicator is sensitive to a change '...
            'in volume and a correction for corporate actions changing the level of volume is known\n'...
            '\n\\item Nb formula and Nb Slicer are mere counters of the number of slicers and formulas that this indicator has.\n'...
            ' \\end{enumerate} ' ...
            ]));
%         ' Please note that the ability to provide an indicator from a set of indicators to which it belongs is ' ...
%             ' computed without considering a group-by this indicator, otherwise it would allways be possible to do so.'
%         
        % we now have to go through full documentation of each indicators
        % in order to get more informations about them before prrinting
        % this summary
        maybeaggfromft   = NaN(length(indicators), 1);
        maybeaggfrombiss = NaN(length(indicators), 1);
        maybeaggfrombi   = NaN(length(indicators), 1);
        %>
        
        
        
        % < Going through the full documentation of indicators
        % this information will first be printed in a different file
        % then we will print the summary table in the main file using the
        % info gathered here, finally we will copy-paste the secondary file
        % into the main one
        latex('section', fichier2, 'Full documentation of each indicator');
        latex('plain', fichier2, sprintf(['In the following sections you''ll find first a comment written in the xml ' ...
            ' to describe the indicator. Then you''ll find a description of the '...
            'formulas and slicers that this indicator has.\n\n For a formula you''ll '...
            'find in a first subsection an array with the following informations : ' ...
            '\\begin{enumerate}[a)]\n\n\\item cost : which is supposed to be representative for the cost '...
            'of applying the formula or the slicer and will be used in the search of the less costly ' ...
            'computation plan whan an aggregation is asked\n\\item H allergy : which gives the'...
            ' list of variable which when existing in a st\\_data and heterogenous should forbid '...
            'the use of the formula or slicer \n\\item Used indicators : the list'...
            'of indicators needed to apply the formula or the slicer \n\\item formula or func :'...
            ' which is either the string for the formula of the function by the slicer'...
            ' \\end{enumerate} For a slicer, only the last element is different : this is a function rather than a string.' ...
            ' \\\\ The next subsections shows computation plan from tick-by-tick data, ' ...
            ' then basic indicators smallest step, and finally from basic indicators, as if this indicator was ' ...
            'asked alone. These sections may not be found if the computation is impossible ' ...
            ' given the available indicators at one of these two steps.\\\\ ' ...
            'Please note that in the case of tick-by-tick data : .date=time\\_open=time\\_close' ...
            ' \\\\ ']));
        dep_functions = {};
        all_in_cols = {};
        for i = 1 : length(indicators)
            latex('subsection', fichier2, indicators(i).name);
            latex('plain', fichier2, sprintf('\\label{%s}', indicators(i).name));
            latex('plain', fichier2, indicators(i).comments);
            [pot_dep, ~] = regexp(indicators(i).comments, '\\nameref{(\w+)}', 'tokens', 'match');
            if strtok(v____, '.') > '7' %v____(1)
                dep_functions = cat(2, dep_functions, setdiff(cellflat(pot_dep), {indicators(i).name}, 'legacy')); % legacy for R2013a and more
            else
                dep_functions = cat(2, dep_functions, setdiff(cellflat(pot_dep), {indicators(i).name}));
            end
            if ~isempty(indicators(i).formula)
                latex('subsubsection', fichier2, 'Formulas');
                table2include = cell(length(indicators(i).formula), 4);
                depfuncinformulas = {};
                for j = 1 : length(indicators(i).formula)
                    table2include{j, 1} = indicators(i).formula(j).cost;
                    table2include{j, 2} = text_to_tex_text(strrep(strrep(indicators(i).formula(j).heterogeneous_allergy, ',', ' '), ';', ' '));
                    all_in_cols = cat(2, all_in_cols, indicators(i).formula(j).in_cols{:});
                    table2include{j, 3} = sprintf('\\nameref{%s} ', indicators(i).formula(j).in_cols{:});
                    table2include{j, 4} = text_to_tex_text(addblank(indicators(i).formula(j).params));
                    
                    [~, pot_dep, start_, end_] = regexp(strrep(strrep(indicators(i).formula(j).params, ...
                        '(', ' '), '(', ' '), '(\w+)', 'tokens', 'match', 'start', 'end');
                    idx2rem = ismember(pot_dep, {indicators.name});
                    start_(idx2rem) = [];
                    end_(idx2rem) = [];
                    pot_dep(idx2rem) = [];
                    for k = 1 : length(pot_dep)
                        if ~(k>1 && strcmp(pot_dep{k-1}, 'data') && strcmp(indicators(i).formula(j).params((end_(k-1)+1):(start_(k)-1)), '.'))  ...
                                && ~strcmp(pot_dep{k}, 'data') ...
                                && ~isempty(which(pot_dep{k}))
                            dep_functions{end+1} = pot_dep{k};
                            depfuncinformulas{end+1} = pot_dep{k};
                        end
                    end
                end
                latex('array', fichier2, table2include, 'col_header', {'Cost', ...
                    'H Allergy', 'Used indicators', 'formula'}, ...
                    'format', '%d', 'plain', 'true', 'full_col_spec', 'c|p{3cm}|p{4cm}|p{7cm}|');%, 'col_format', 'p{3cm}'
                if ~isempty(depfuncinformulas)
                    depfuncinformulas = unique(depfuncinformulas);
                    tmp = '';
                    for kkkk = 1: length(depfuncinformulas)
                        tmp = cat(2, tmp, func4sec_ref(depfuncinformulas{kkkk}));
                    end
                    latex('text', fichier2, 'List of functions used in these formulas :');
                    latex('plain', fichier2,  tmp);
                end
            end
            if ~isempty(indicators(i).slicer)
                latex('subsubsection', fichier2, 'Slicers');
                table2include = cell(length(indicators(i).formula), 4);
                for j = 1 : length(indicators(i).slicer)
                    table2include{j, 1} = indicators(i).slicer(j).cost;
                    table2include{j, 2} = text_to_tex_text(strrep(strrep(indicators(i).slicer(j).heterogeneous_allergy, ',', ' '), ';', ' '));
                    all_in_cols = cat(2, all_in_cols, indicators(i).slicer(j).in_cols{:});
                    table2include{j, 3} = sprintf('\\nameref{%s} ', indicators(i).slicer(j).in_cols{:});
                    dep_functions{end+1} = addblank(func2str(indicators(i).slicer(j).params.func));
                    table2include{j, 4} = func4sec_ref(dep_functions{end});
                end
                latex('array', fichier2, table2include, 'col_header', {'Cost', ...
                    'H Allergy', 'Used indicators', 'function'}, ...
                    'format', '%d', 'plain', 'true', 'full_col_spec', 'c|p{3cm}|p{4cm}|p{7cm}|');%, 'col_format', 'p{3cm}'
            end
            
            %             try
            %                 [ind_preformulas, ind_slicers, ind_postformulas, cost] = ...
            %                     indicators_computation_plan({indicators(i).name}, available_cols4exemples, formulas, ...
            %                     slicers, {}, {'end_slice', 'begin_slice', 'time_open', 'time_close'}, ...
            %                     POST_FORMULA_COST_SAVING_FACTOR);
            %                 [c_pre, c_s, c_post] = path_formater([], ...
            %                     formulas, slicers, ...
            %                     POST_FORMULA_COST_SAVING_FACTOR, ...
            %                     {ind_preformulas}, {ind_slicers}, {ind_postformulas}, cost);
            %                 png_path = create_png_from_dot_code(path_dotifier(c_pre{1}, c_s{1}, c_post{1}));
            %                 png_path = strrep(png_path, 'C:\st_repository\dot', '..\..\dot');
            %             catch ME
            %                 if strcmp(ME.identifier, 'indicators_computation_plan:check_args') ...
            %                         && length(ME.message) > 52 ...
            %                         && ( strcmp(ME.message(1:43), 'No path through slicers and formulas found,') || ...
            %                             strcmp(ME.message(1:53), 'There is no path at all, even if we had all the cols,'))
            %                     continue
            %                 else
            %                     rethrow(ME);
            %                 end
            %             end
            
            if ~ismember(indicators(i).name, gbft2biss)
                gpngpath = make_graphivz(indicators, ft_cols, {indicators(i).name}, gbft2biss);
                if length(gpngpath)>1
                    latex('plain', fichier2, sprintf('\\subsubsection{Computation plan exemple from tick-by-tick data}\n'));
                    %                 latex('text', fichier2, sprintf('Asking for the compuation plan for computing indicators : <%s>\n with available indicators : <%s>\n\n\n', ...
                    %                     sprintf('%s/', indicators(i).name), sprintf('%s/', ft_cols{:})));
                    latex('include_graphic_auto_scale', fichier2, gpngpath, 'width', true);
                    maybeaggfromft(i) = true;
                else
                    maybeaggfromft(i) = str2double(gpngpath);
                end
            else 
                latex('plain', fichier2, sprintf('\\subsubsection{Computation plan exemple from tick-by-tick data}\n'));
                latex('text', fichier2, sprintf('This indicator is part of the group by clause.\n'));
                maybeaggfromft(i) = true;
            end
            if ~ismember(indicators(i).name, gbbiss2bi)
                gpngpath = make_graphivz(indicators, bi1cols, {indicators(i).name}, gbbiss2bi);
                if length(gpngpath)>1
                    latex('plain', fichier2, sprintf('\\subsubsection{Computation plan exemple from BI smallest step}\n'));
                    %                 latex('text', fichier2, sprintf('Asking for the compuation plan for computing indicators : <%s>\n with available indicators : <%s>\n\n\n', ...
                    %                     sprintf('%s/', indicators(i).name), sprintf('%s/', bi1cols{:})));
                    latex('include_graphic_auto_scale', fichier2, gpngpath, 'width', true);
                    maybeaggfrombiss(i) = true;
                else
                    maybeaggfrombiss(i) = str2double(gpngpath);
                end
            else 
                latex('plain', fichier2, sprintf('\\subsubsection{Computation plan exemple from BI smallest step}\n'));
                latex('text', fichier2, sprintf('This indicator is part of the group by clause.\n'));
                maybeaggfrombiss(i) = true;
            end   
            if ~ismember(indicators(i).name, gbbi2bi)
                gpngpath = make_graphivz(indicators, bi2cols, {indicators(i).name}, gbbi2bi);
                if length(gpngpath)>1
                    latex('plain', fichier2, sprintf('\\subsubsection{Computation plan exemple from basic indicators}\n'));
                    %                 latex('text', fichier2, sprintf('Asking for the compuation plan for computing indicators : <%s>\n with available indicators : <%s>\n\n\n', ...
                    %                     sprintf('%s/', indicators(i).name), sprintf('%s/', bi1cols{:})));
                    latex('include_graphic_auto_scale', fichier2, gpngpath, 'width', true);
                    maybeaggfrombi(i) = true;
                else
                    maybeaggfrombi(i) = str2double(gpngpath);
                end
            else
                latex('plain', fichier2, sprintf('\\subsubsection{Computation plan exemple from basic indicators}\n'));
                latex('text', fichier2, sprintf('This indicator is part of the group by clause.\n'));
                maybeaggfrombi(i) = true;
            end
            %             st_log('$out$', 'C:\temp\comp_plan_ex.txt');
            %             path_describer([], ...
            %                 formulas, slicers, ...
            %                 POST_FORMULA_COST_SAVING_FACTOR, ...
            %                 {ind_preformulas}, {ind_slicers}, {ind_postformulas}, cost);
            %             st_log('$close$');
            %             latex('plain', fichier2, '\begin{verbatim}');
            %             latex('copy-paste', fichier2, 'C:\temp\comp_plan_ex.txt');
            %             latex('plain', fichier2, '\end{verbatim}');
        end
        % >
        
        % < Now we can really print the summary table
        table2include = cell(length(indicators), 9);
        for i = 1 : length(indicators)
            table2include{i, 1} = sprintf('\\nameref{%s}', indicators(i).name);
            table2include{i, 2} = indicators(i).default_value;
            table2include{i, 3} = sprintf('%d/%d', ismember(indicators(i).name, ft_cols), ...
                maybeaggfromft(i));
            table2include{i, 4} = sprintf('%d/%d', ismember(indicators(i).name, bi1cols), ...
                maybeaggfrombiss(i));
            table2include{i, 5} = sprintf('%d/%d', ismember(indicators(i).name, bi2cols), ...
                maybeaggfrombi(i));
            if indicators(i).is_s2ca
                table2include{i, 6} = indicators(i).simplified_cac.mult_by_cum_price_ratio;
                table2include{i, 7} = indicators(i).simplified_cac.mult_by_cum_volume_ratio;
            else
                table2include{i, 6} = false;
                table2include{i, 7} = false;
            end
            table2include{i, 8} = length(indicators(i).formula);
            table2include{i, 9} = length(indicators(i).slicer);
        end
        latex('array', fichier, table2include, 'col_header', {'Name', ...
            'Default value', 'ft', 'BISS', 'BI', 'CA price', ...
            'CA volume', 'Nb formula', 'Nb slicer'}, ...
            'format', '%d', 'plain', 'true', 'full_col_spec', 'c|p{0.8cm}|p{0.8cm}|p{0.8cm}|p{0.8cm}|p{0.8cm}|p{0.8cm}|p{0.8cm}|p{0.8cm}|p{0.8cm}', ..., 'col_format', 'p{2.5cm}'
            'maxlines', 45);
        % >
        
        % < Now we can really print the fulle documentation of each
        % indicator
        latex('copy-paste', fichier, fichier2);
        % >
        
        
        if strtok(v____, '.') > '7' %v____(1)
            missing_def_ind = setdiff(unique(all_in_cols), {indicators.name}, 'legacy');  % legacy for R2013a and more
        else
            missing_def_ind = setdiff(unique(all_in_cols), {indicators.name});
        end
        
        if ~isempty(missing_def_ind)
            error('indicator_factory:check_ref', ...
                'The following indicators are used by some other but have no definition : <%s>', ...
                sprintf('%s/', missing_def_ind{:}));
        end
        if ~isempty(dep_functions)
            dep_functions = unique(dep_functions);
            latex('section', fichier, 'Functions used');
            for i = 1 : length(dep_functions)
                if can_be_sec_title(dep_functions{i})
                    %                     any(ismember(, '@'))
                    %                     latex('subsection', fichier, strrep(dep_functions{i}, '-', '--'));
                    % %                     latex('plain', fichier, ['\subsection[$' dep_functions{i} '$]{' text_to_tex_text(dep_functions{i}) '}']);
                    %                 else
                    latex('subsection', fichier, dep_functions{i});
                    %                 end
                    latex('plain', fichier, func4sec_labels(dep_functions{i}));
                    latex('plain', fichier, '\begin{lstlisting}[language=st_Matlab,breaklines=true]');
                    try
                        doc_my_func(fichier, dep_functions{i});
                    catch ME
                        latex('plain', fichier, se_stack_error_message(ME));
                    end
                    latex('plain', fichier, '\end{lstlisting}');
                end
            end
        end
        
        latex('section', fichier, 'Most common computation plans');
        
%         latex('subsection', fichier, 'get_basic_indicator_v2 from tick by tick to aggregated data');
        gpngpath = make_graphivz(indicators, ft_cols, bi1cols, gbft2biss);
        if length(gpngpath) > 1
            latex('plain', fichier, ...
                sprintf(['\\begin{landscape}\n', ...
                '\\subsection{get\\_basic\\_indicator\\_v2 from tick by tick to aggregated data}\n' ...
                '\\begin{center}\n' ...
                '{\\includegraphics[width=24cm,height=20cm,keepaspectratio=true]{%s}}\n' ...
                '\\end{center}\n' ...
                '\\end{landscape}\n'], gpngpath));
            %             latex('include_graphic', fichier, gpngpath, 'width', true, 'rotate', 90, 'scalebox', 1);
        else
            error();
        end
        
%         latex('subsection', fichier, 'get_basic_indicator_v2 from finest aggregated data to coarser aggegrated date');
        gpngpath = make_graphivz(indicators, bi1cols, bi2cols, gbbiss2bi);
        if length(gpngpath) > 1
            latex('plain', fichier, ...
                sprintf(['\\begin{landscape}\n', ...
                '\\subsection{get\\_basic\\_indicator\\_v2 from finest aggregated data to coarser aggegrated date}\n' ...
                '\\begin{center}\n' ...
                '{\\includegraphics[width=24cm,height=20cm,keepaspectratio=true]{%s}}\n' ...
                '\\end{center}\n' ...
                '\\end{landscape}\n'], gpngpath));
            %             latex('include_graphic', fichier, gpngpath, 'width', true, 'rotate', 90, 'scalebox', 1);
        else
            error();
        end
        
        latex('section', fichier, 'Plain text xml file used for this document');

        latex('plain', fichier, '\begin{lstlisting}[language=st_XML,breaklines=true]');
        latex('copy-paste', fichier, INDICATORS_FILE);
        latex('plain', fichier, '\end{lstlisting}');
        
        latex('end', fichier);
        latex('compile', fichier);
        
    case 'get'
        if isempty(mem_indicators)
            [indicators, hash_key] = indicator_factory('build_from_xml');
        else
            indicators = mem_indicators.indicators;
            hash_key = mem_indicators.hash_key;
        end
        
    case {'build_from_xml','reset'}
        xmldoc  = xmltools( INDICATORS_FILE);
        xml_doc = xmltools( xmldoc.get_tag('ENV') );

        indicators = hostname2indic('bin',xml_doc);
        indicators_user = hostname2indic(st_version.my_env.hostname, xml_doc);
        
        if ~isempty(indicators_user)
            if strtok(v____, '.') > '7' %v____(1)
                [~, ib, iu] = intersect({indicators.name}, {indicators_user.name}, 'legacy');  % legacy for R2013a and more
            else
                [~, ib, iu] = intersect({indicators.name}, {indicators_user.name});
            end
            indicators(ib) = indicators_user(iu);
            indicators_user(iu) = [];
            indicators = cat(1, indicators, indicators_user);
        end
        
        hash_key = hash(convs('safe_str', indicators), 'MD5');
        
        mem_indicators = struct('indicators', indicators, 'hash_key', hash_key);
        
        clear apply_plan_to_grid;
        
    case 'build_indicators_from_already_existing' %TODO
        % on construit des familles d'indicateurs par exemple proportions
        % � partir d'indicateurs existants par ex volume, etc
        error('indicator_factory:mode', 'MODE: unknown mode <%s>', mode); %not yet implemented
        
    case 'add_replace_indicators' %TODO
        % modifier le d�finition d'un indicateur ou en rajouter un
        % c'est juste la m�me chose que indicators_bin et indicators_user
        [indicators, hash_key] = indicator_factory('get');
        otf_ind = varargin{1};
        
        if isempty(otf_ind)
           return;
        end
        
        % These indices wont be used before the end of this mode, do not
        % modify and when adding indicators, do it at the end of the struct
        % array
        if strtok(v____, '.') > '7' %v____(1)
            [ind2be_overridden, i_xml, iotf] = intersect({indicators.name}, {otf_ind.name}, 'legacy');  % legacy for R2013a and more
        else
            [ind2be_overridden, i_xml, iotf] = intersect({indicators.name}, {otf_ind.name});
        end
        
        ind2add = otf_ind;
        ind2add(iotf) = [];
        for i = 1 : length(otf_ind)
            indicators(end+1) = convert_otf_to_my_struct(ind2add(i));
        end
        
        if ~isempty(ind2be_overridden)
            [indicators, hash_key] = indicator_factory('add_slicers_formulas', ...
                indicators, otf_ind(iotf), i_xml);
        else
            hash_key = hash(convs('safe_str', indicators), 'MD5');
        end
    case 'add_slicers_formulas' %TODO
        % on rajoute � des indicateurs existant des slicers et/ou des
        % formules
        error('indicator_factory:mode', 'MODE: unknown mode <%s>', mode); %not yet implemented
    otherwise
        error('indicator_factory:mode', 'MODE: unknown mode <%s>', mode);
end

end

function doc_my_func(fichier, s)
outs = which(s);
if length(outs) > 10 && strcmp(outs(1:10), 'built-in (')
    t = help(s);
    t = ['%%' strrep(t, sprintf('\n'), sprintf('\n%%%%'))];
    latex('plain', fichier, t);
else
    latex('copy-paste', fichier, outs);
end
end

function my_struct = convert_otf_to_my_struct(otf) % TODO corporate_action_sensitivity and heterogeneous_allergy
otf_cas = ~strcmp('none',{otf.corporate_action_sensitivity});
if length(otf_cas) >1
    otf_cas = mat2cell(otf_cas);
else 
    otf_cas = {otf_cas};
end;
my_struct = struct('name', otf.name, ...
    'corporate_action_sensitivity', otf.corporate_action_sensitivity, ...
    'default_value', otf.default_value, 'slicer', [],'formula', [], ...
    'is_s2ca',otf_cas{:}, 'simplified_cac', [], 'comments', '');
if isfield(otf, 'slicer') && ~isempty(otf.slicer)
    s_params = struct( 'def_vals', otf.default_value, ...
                'func', {otf.slicer.func});
    slicers = struct('out_col', otf.name, ...
            'type', 'slicer', ...
            'cost', NaN, ...
            'params', [],...
            'in_cols', {otf.slicer.in_cols}, ...
            'heterogeneous_allergy', {otf.slicer.heterogeneous_allergy});
    for i = 1 : length(otf.slicer)
        slicers(i).params = s_params(i);
        slicers(i).cost = length(slicers(i).in_cols);
    end
    my_struct.slicer = slicers';
end
if isfield(otf, 'formula') && ~isempty(otf.formula)
    formulas = struct('out_col', otf.name, ...
            'type', 'formula', ...
            'cost', NaN, ...
            'params', otf.formula,...
            'in_cols', [], ...
            'heterogeneous_allergy', otf.formula_heterogeneous_allergy);
    for i = 1 : length(otf.formula)
        formulas(i).in_cols = unique(cellflat(regexp(otf.formula{i}, '{([^}]+)}','tokens') ));
        formulas(i).cost = length(formulas(i).in_cols);
    end
    my_struct.formula = formulas';
end

for i = 1:length(my_struct)
   my_struct(i) = fill_simplified_cac_field(my_struct(i)); 
end

end

function my_struct =  convert_xmlchildren2my_struct(child, name, def_val)

tmp = cat(1, {child.attribs.key}, {child.attribs.value});
my_fields = struct(tmp{:});
switch lower(child.tag)
    case 'slicer'
        if ~isfield(my_fields, 'in_cols')
            inc = {name};
        else
            inc = cellflat(tokenize(my_fields.in_cols, ','));
        end
        my_struct = struct('out_col', name, ...
            'type', child.tag, ...
            'cost', str2double(my_fields.cost), ...
            'params', struct('def_vals', def_val, ...
                'func', eval(my_fields.func)),...
            'in_cols', {inc}, ...
            'heterogeneous_allergy', my_fields.heterogeneous_allergy);
        
    case 'formula'
        my_struct = struct('out_col', name, ...
            'type', child.tag, ...
            'cost', str2double(my_fields.cost), ...
            'params', my_fields.string,...
            'in_cols', {unique(cellflat(regexp(my_fields.string, '{([^}]+)}','tokens') ))}, ...
            'heterogeneous_allergy', my_fields.heterogeneous_allergy);
        
    otherwise
        error('convert_xmlchildren2my_struct:mode', ...
            'Unhandled struct type <%s>', child.tag);
end

end

function gpngpath = make_graphivz(indicators, av_cols, req_cols, groupbyvariables)
POST_FORMULA_COST_SAVING_FACTOR = 0.5;
formulas = cat(1, indicators.formula);
slicers = cat(1, indicators.slicer);
gpngpath = '0';
try
    try
        [ind_preformulas, ind_slicers, ind_postformulas, cost] = ...
            indicators_computation_plan(req_cols, av_cols, formulas, ...
            slicers, {}, {}, ...
            POST_FORMULA_COST_SAVING_FACTOR);   
    catch
        [ind_preformulas, ind_slicers, ind_postformulas, cost] = ...
            indicators_computation_plan(req_cols, av_cols, formulas, ...
            slicers, groupbyvariables, cat(2, groupbyvariables, {'end_slice', 'begin_slice'}), ...
            POST_FORMULA_COST_SAVING_FACTOR);
    end
catch ME
    if strcmp(ME.identifier, 'indicators_computation_plan:check_args') ...
            && length(ME.message) > 52
        if strcmp(ME.message(1:43), 'No path through slicers and formulas found,')
%             st_log([sprintf('%s,', req_cols{:}) '\n']);
            return
        elseif strcmp(ME.message(1:53), 'There is no path at all, even if we had all the cols,')
            return
        else
            rethrow(ME);
        end
    else
        rethrow(ME);
    end
end
if ~isempty(ind_slicers)
    [c_pre, c_s, c_post] = path_formater([], ...
        formulas, slicers, ...
        POST_FORMULA_COST_SAVING_FACTOR, ...
        {ind_preformulas}, {ind_slicers}, {ind_postformulas}, cost);
    gpngpath = create_png_from_dot_code(path_dotifier(c_pre{1}, c_s{1}, c_post{1}));
    gpngpath = strrep(strrep(gpngpath, 'C:\st_repository\dot', '../../dot'), '\', '/');
else
    gpngpath = '1';
end
end

function indicators = hostname2indic(hostname,xml_doc)
        xml_bin = xml_doc.keep_where_attrib('hostname', hostname);
        xml_indicators = xml_bin.children.children;
        idx2del = ~strcmp('indicator', {xml_indicators.tag});
        xml_indicators(idx2del) = [];
        
        indicators = struct('name', cell(length(xml_indicators), 1), 'corporate_action_sensitivity', [], ...
            'default_value', [], 'slicer', [],'formula', [], 'is_s2ca', true, 'simplified_cac', [], ...
            'comments', '');
        for i = 1 : length(indicators)
            tmp = cat(1, {xml_indicators(i).attribs.key}, {xml_indicators(i).attribs.value});
            opt = options({'name',[], 'corporate_action_sensitivity',[],'default_value',[]},tmp(:));
            this_indic_child = xml_indicators(i).children;
            idx2del_ = ~ismember({this_indic_child.tag}, {'slicer', 'formula'});
            this_indic_child(idx2del_) = [];
            indicators(i).name = opt.get('name');
            indicators(i).corporate_action_sensitivity = opt.get('corporate_action_sensitivity');
            indicators(i).default_value = opt.get('default_value');
            indicators(i).comments = xml_indicators(i).value;
            for j = 1: length(this_indic_child)
                tmp_struct = convert_xmlchildren2my_struct(this_indic_child(j), indicators(i).name , ...
                    str2double(indicators(i).default_value));
                indicators(i).(tmp_struct.type) = cat(1, indicators(i).(tmp_struct.type), tmp_struct);
            end
            if isempty(indicators(i).corporate_action_sensitivity)
                error('indicators_factory:parsing_xml', ...
                    'Indicator <%s> has no specification for its sensitivity to corporate actions', ...
                    indicators(i).name);
            elseif strcmp(indicators(i).corporate_action_sensitivity, 'none')
                indicators(i).is_s2ca = false;
            else
                indicators(i) = fill_simplified_cac_field(indicators(i));
            end
        end
end

function my_indic = fill_simplified_cac_field(my_indic)
if ~my_indic.is_s2ca
my_indic.simplified_cac = [];
return;
end

ca_s = my_indic.corporate_action_sensitivity;
in_cac_cols = cellflat(regexp(ca_s, '{([^}]+)}','tokens'));
if length(in_cac_cols) > 1 || ~strcmp(in_cac_cols{1}, my_indic.name)
    error('indicator_factory:parsing_xml', ...
        'This type of correction is currently unhandled, see st_data(''cac''');
end
in_cac_cols = unique(in_cac_cols);
for j = 1 : length(in_cac_cols)
    ca_s = strrep(ca_s, ['{' in_cac_cols{j} '}'], '');
end
in_cac_params = cellflat(regexp(ca_s, '\$([^\$]+)\$','tokens'));
if length(in_cac_params) ~= length(unique(in_cac_params))
    error('indicator_factory:parsing_xml', ...
        'This type of correction is currently unhandled, see st_data(''cac''');
end

%< building the structure simplified_cac
s_cac = struct('mult_by_cum_price_ratio', false, 'mult_by_cum_volume_ratio', false);
for j = 1 : length(in_cac_params)
    ca_s = strrep(ca_s, ['$' in_cac_params{j} '$'], '');
    if strcmp(in_cac_params{j}, 'price_ratio')
        s_cac.mult_by_cum_price_ratio  = true;
    elseif strcmp(in_cac_params{j}, 'volume_ratio')
        s_cac.mult_by_cum_volume_ratio = true;
    end
end
my_indic.simplified_cac = s_cac;
%>

if mod(length(ca_s), 2) ~= 0
    error('indicator_factory:parsing_xml', ...
        'This type of correction is currently unhandled, see st_data(''cac''');
end
% TODO should it be .* or ./?
if ~strcmp(ca_s, repmat('.*', 1, length(ca_s)/2))
    error('indicator_factory:parsing_xml', ...
        'This type of correction is currently unhandled, see st_data(''cac''');
end

end


function s = func4sec_labels(s)
if can_be_sec_title(s)
    s = sprintf('\\label{%s} ',hash(s, 'MD5'));
else
    s = text_to_tex_text(s);
end
end

function s = func4sec_ref(s)
if can_be_sec_title(s)
    s = sprintf('\\nameref{%s} ',hash(s, 'MD5'));
else
     s = text_to_tex_text(s);
end
end

function b = can_be_sec_title(s)
b = ~any(ismember(s, '@'));
end

function s=addblank(s)
patterns = {'(', ',', '+', '-', './', '.*', '&', '|'};
for i = 1 : length(patterns)
    s = strrep(strrep(s, patterns{i}, [patterns{i} ' ']), ...
        [patterns{i} '  '], [patterns{i} ' ']);
end
end