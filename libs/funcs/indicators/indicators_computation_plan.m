function [ind_preformulas, ind_slicers, ind_postformulas, cost, used_cols,col_dico] = ...
    indicators_computation_plan(required_cols, available_cols, formulas, ...
    slicers, cols_needed4grid, cols_provided_by_grid, ...
    POST_FORMULA_COST_SAVING_FACTOR) % TODO better knowledge of relative cost of dependencies according to the scale decrease at the slicer step?
%
% test_computation_plan
%
% See also indicator_factory
%
% version: 1.0
% date: 03/01/2010
% author: rburgot@cheuvreux.com
% reviewer: athabault@cheuvreux.com
global st_version

v = version();

CAI_PATH_LIMIT = 2^15; % in order to stop too long explorations this should be less than one minute
TIME_LIMIT_SEC = 60;
VERBOSITY_LVL_HERE = 10;

if st_version.my_env.verbosity_level >= VERBOSITY_LVL_HERE
    st_log('indicators_computation_plan:exec: Starting intilization\n');
end
t0 = clock();

    function is_time_limit_passed
        if etime(clock(), t0) > TIME_LIMIT_SEC
            error('indicators_computation_plan:exec', ...
                ['TIME_SPENT: indicators_computation_plan took too much time \n\t', ...
                'for computation. Consider increasing TIME_LIMIT_SEC if this is not relevant']);
        end
    end

agg_wanted = (nargin >= 4);
if ~agg_wanted
    ind_slicers = 'YOU SHOULDNT NEED THIS!!! INDICATORS_COMPUTATION_PLAN considered you just wanted to use formulas to convert some indicators into others';
    POST_FORMULA_COST_SAVING_FACTOR = 1;
    cols_needed4grid = {};
    cols_provided_by_grid = available_cols;
    required_cols = setdiff(required_cols, available_cols);
    if str2num(strtok(v, '.')) > 7 %v(1)
        [cols, nb_cols, indices] = unique(cat(2, required_cols, available_cols, ...
        cols_needed4grid, cols_provided_by_grid, {formulas.out_col}, formulas.in_cols), 'legacy');
    else
        [cols, nb_cols, indices] = unique(cat(2, required_cols, available_cols, ...
        cols_needed4grid, cols_provided_by_grid, {formulas.out_col}, formulas.in_cols));
    end
else
    if str2num(strtok(v, '.')) > 7 %v(1)
        [cols, nb_cols, indices] = unique(cat(2, required_cols, available_cols, ...
        cols_needed4grid, cols_provided_by_grid, {formulas.out_col}, formulas.in_cols, ...
        {slicers.out_col}, slicers.in_cols), 'legacy');
    else
        [cols, nb_cols, indices] = unique(cat(2, required_cols, available_cols, ...
        cols_needed4grid, cols_provided_by_grid, {formulas.out_col}, formulas.in_cols, ...
        {slicers.out_col}, slicers.in_cols));
    end
    
end

% patch MATLAB R2023B
% indices = indices';

%< Creating the col dictionnary which will help us identifying dependencies

% The order in this concatenation is important for next steps

%> preprocesing colnames
nb_cols = length(nb_cols);

% required_col
[indices, is_rc,ind_in_rc] = ...
    process_col_type(indices, length(required_cols), nb_cols, 'required_cols');
% available_cols
[indices, is_ac, ind_in_ac] = ...
    process_col_type(indices, length(available_cols), nb_cols, 'available_cols');
% cols_needed4grid
[indices, is_ng] = ...
    process_col_type(indices, length(cols_needed4grid), nb_cols, 'cols_needed4grid');
% cols_provided_by_grid
[indices, is_pg, ind_in_pg] = ...
    process_col_type(indices, length(cols_provided_by_grid), nb_cols, 'cols_provided_by_grid');
% formulas dependencies
[indices, indices_fo, idx_foi, osf_parents, f_parents] = ...
    process_dependency(indices, formulas, nb_cols);
% slicers dependencies
if agg_wanted
    [indices, indices_so, idx_soi, oss_parents, s_parents, ind_in_s_inputs] = ...
        process_dependency(indices, slicers, nb_cols);
else
    indices_so = [];
    idx_soi = [];
    oss_parents = [];
    ind_in_s_inputs = [];
end
if ~isempty(indices)
    error('indicators_computation_plan:exec', 'Unexpected error 1');
end
% >

col_dico = struct('cols', {cols}, 'idx_foi', idx_foi, 'idx_soi', idx_soi, ...
    'ind_in_ac', ind_in_ac, 'indices_fo', indices_fo, 'indices_so', indices_so, ...
    'ind_in_s_inputs', {ind_in_s_inputs},...
    'ind_in_rc',ind_in_rc, 'ind_in_pg', ind_in_pg);

if is_included_in(is_rc, is_pg) && is_included_in(is_ng, is_ac)
    ind_preformulas = {};
    ind_slicers = [];
    ind_postformulas = {}; 
    cost = 0;
    used_cols = is_ng;
    return;
end

% < potentially useful cols at any step from post_formulas to preformulas
% may help to produce the required cols
    function ptfpu = postf_puc_upd(is_rc_, is_ac_, one_step_f_parents_)
        ptfpu = set_diff_(is_rc_,is_ac_);
        next_ptfpu = set_diff_( ptfpu* one_step_f_parents_, is_ac_ ) | ptfpu;
        while any(any(ptfpu ~= next_ptfpu))
            ptfpu = next_ptfpu;
            next_ptfpu = set_diff_( ptfpu* one_step_f_parents_, is_ac_ ) | ptfpu;
            next_ptfpu(logical(next_ptfpu)) = 1;
        end
%         
%         
%         ptfpu = set_diff_( ...
%             set_diff_(is_rc_,is_ac_) ...
%             * f_parents_, ...
%             is_ac_ );
    end

% 
%     function ptfpu = postf_puc_upd(is_rc_, is_ac_, f_parents_)
%         ptfpu = set_diff_( ...
% 				set_diff_(is_rc_,is_ac_) ...
% 							* f_parents_, ...
% 			is_ac_ );
%     end

postf_puc = postf_puc_upd(is_rc, is_pg, osf_parents);

if agg_wanted
    slicer_puc = (postf_puc * oss_parents) ... %  potential usefull cols before applying the slicers
			| is_ng; % or needed to make the grid

    pref_puc = pref_puc_upd(slicer_puc, is_ac, osf_parents); %  potential usefull cols in the pre_formula step
end
    function ppu = pref_puc_upd(is_rc_, is_ac_, one_step_f_parents_)
        ppu = set_diff_(is_rc_,is_ac_);
        next_ppu = set_diff_( ppu* one_step_f_parents_, is_ac_ ) | ppu;
        while any(any(ppu ~= next_ppu))
            ppu = next_ppu;
            next_ppu = set_diff_( ppu* one_step_f_parents_, is_ac_ ) | ppu;
            next_ppu(logical(next_ppu)) = 1;
        end
    end

%     function ppu = pref_puc_upd(slicer_puc_, is_ac_, f_parents_)
% 		ppu = set_diff_( set_diff_(slicer_puc_, is_ac_ ) * f_parents_, ... 
% 				is_ac_ ); % that is not already available
%     end

% >

% < Does a path exist from the step I am currently in? an swer taking into
% account the formulas I no longer want to use in the current step

    function [b, mc, grid_blocked, is_ac_so_far] = pref_path_exist(is_ac_so_far, idx_foi_restr, indices_fo_restr)
        next_generation = is_ac_so_far;
        next_generation(indices_fo_restr(~any(bsxfun(@gt, idx_foi_restr, is_ac_so_far), 2))) = true; % INLINE : contains(is_ac_so_far, idx_foi_restr)
        while any(next_generation ~= is_ac_so_far)
            is_ac_so_far = next_generation;
            next_generation(indices_fo_restr(~any(bsxfun(@gt, idx_foi_restr, is_ac_so_far), 2))) = true; % INLINE : contains(is_ac_so_far, idx_foi_restr)
        end
        
        [b, mc, grid_blocked] = slicer_path_exist(is_ac_so_far);
    end

    function [b, mc, grid_blocked] = slicer_path_exist(is_ac_so_far, varargin)
        % < now we'll compute the set of all the cols that could be existing after
        % the slicer step
        mc = is_ng & ~is_ac_so_far;
        if any(mc)
            b = false;
            grid_blocked = true;
            return;
        else
            grid_blocked = false;
        end
        appliable_slicers = ~any(bsxfun(@gt, idx_soi, is_ac_so_far), 2); % INLINE : is_included_in(idx_soi, is_ac_so_far)
        slicers_children  = is_pg;
        slicers_children(indices_so(appliable_slicers)) = true;
        % >
        [b, mc] = post_f_path_exist(slicers_children, idx_foi, indices_fo);
    end
    
    function [b, mc, reachable_cols] = post_f_path_exist(reachable_cols, idx_foi_restr, indices_fo_restr)
    % < Keeping on moving forward but quickly to see all the reachable cols
        next_generation = reachable_cols;
        next_generation(indices_fo_restr(~any(bsxfun(@gt, idx_foi_restr, reachable_cols), 2))) = true; % INLINE : contains(reachable_cols, idx_foi_restr)
        while any(next_generation ~= reachable_cols)
            reachable_cols = next_generation;
            next_generation(indices_fo_restr(~any(bsxfun(@gt, idx_foi_restr, reachable_cols), 2))) = true; % INLINE : contains(reachable_cols, idx_foi_restr)
        end
        % >
        mc = is_rc & ~reachable_cols; % INLINE : is...
        b = ~any(mc); % ... INLINE : is_included_in(is_rc, reachable_cols);
    end
% >

% < How will I update the arrival set after using a slicer or formula?
% for a formula just adding the new cols
    function pot_arrival_set = get_potential_arrival_set_formulas(is_ac_this_path, formulas_comb)
        pot_arrival_set = is_ac_this_path;
        this_formulas_indices = formulas_comb;
        pot_arrival_set(indices_fo(this_formulas_indices)) = true;
    end

% for a slicer : i'll have some alreaday produced columns (for instance is_pg, but maybe some 
% needed slicers also) to which i will add the columns produced by the
% slicer i will apply
    function pot_arrival_set = get_potential_arrival_set_slicers(apc, is_ac_this_path, slicers_comb)
        pot_arrival_set = apc;
        pot_arrival_set(indices_so(slicers_comb)) = true;
    end
% >

% < Testing wether a path exist, if not trying to tell usr what is missing
% to compute it
if agg_wanted
    [b, is_mc, grid_blocked, preformula_reachable_cols] = pref_path_exist(is_ac, idx_foi, indices_fo);
    if ~b
        %     % If there is no path to required cols, we'd better stop now and explain why
        err_id = 'indicators_computation_plan:check_args';
        error_mess = {'No path through slicers and formulas found, ',...
            '\tyou have to reconsider either the required cols or the cols you can provide.\n'}; %, ...
        %sprintf('The cols we are unable to produce are : <%s>', sprintf('%s/', tmp{:}))};
        
        [b, probably_mispelled_cols] = pref_path_exist(true(1, nb_cols), idx_foi, indices_fo);
        if ~b
            tmp = cols(probably_mispelled_cols);
            error(err_id, ['There is no path at all, even if we had all the cols,\n'...
                sprintf('the cols that are impossible to build are : <%s>\n', sprintf('%s/', tmp{:}))...
                '\t Please check the spelling of these colnames']);
        end
        
        
        if grid_blocked
            useless_formulas = preformula_reachable_cols(indices_fo);
            %  il est inutle d'appliquer en pref des formulas qui produisent des colonnes que l'on poss?de d?j?
            error_mess{end+1} = '';
            tmp = cols(is_mc);
            error_mess{end+1} = sprintf('There is even no path in which we can build the required for grid making cols : <%s>', sprintf('%s/', tmp{:}));
            error_mess{end+1} = sprintf('Please use one of the following sets :');
            ind_mc = find(is_mc);
            for i = 1 : length(ind_mc)
                solution_sets = false(size(is_mc));
                solution_sets(ind_mc(i)) = true;
                solution_sets = unique(set_diff_(explore_pathes_backwardformulas(solution_sets, ...
                    ~useless_formulas,indices_fo, idx_foi), preformula_reachable_cols), 'rows');
                for j = 1 : size(solution_sets, 1)
                    tmp = cols(solution_sets(j, :));
                    error_mess{end+1} = sprintf('\t<%s>', sprintf('%s/', tmp{:}));
                end
            end
            
            is_ac = is_ac | is_ng;
            [b, is_mc, grid_blocked] = pref_path_exist(is_ac, idx_foi, indices_fo);
            if grid_blocked
                error('indicators_computation_plan:check_args', 'Unexpected error');
            end
            
            if ~b
                postf_puc_ = postf_puc_upd(is_mc, is_pg, f_parents);
                slicer_puc_ = (postf_puc_ * oss_parents);
                ppu = set_diff_( set_diff_(slicer_puc_, preformula_reachable_cols ) ...
                    * f_parents, preformula_reachable_cols );
                
                tmp = cols(is_mc);
                error_mess{end+1} = '';
                error_mess{end+1} = sprintf('We didn''t succeeded in building cols : <%s>', sprintf('%s/',tmp{:}));
                tmp = cols(ppu);
                error_mess{end+1} = sprintf('\tPossible candidates to solve this problem are : %s', sprintf('%s, ',tmp{:}));
            end
        else
            postf_puc_ = postf_puc_upd(is_mc, is_pg, f_parents);
            slicer_puc_ = (postf_puc_ * oss_parents);
            ppu = set_diff_( set_diff_(slicer_puc_, preformula_reachable_cols ) ...
                * f_parents, preformula_reachable_cols );
            
            tmp = cols(is_mc);
            error_mess{end+1} = sprintf('We didn''t succeeded in building cols : <%s>', sprintf('%s/',tmp{:}));
            tmp = cols(ppu);
            error_mess{end+1} = sprintf('\tPossible candidate to solve this problem are : %s', sprintf('%s, ',tmp{:}));
        end
        error(err_id, sprintf('%s\n', error_mess{:}));
    end
else
    [b, is_mc, reachable_cols] = post_f_path_exist(is_ac, idx_foi, indices_fo);
    if ~b
        err_id = 'indicators_computation_plan:check_args';
        
        [b, probably_mispelled_cols] = post_f_path_exist(true(1, nb_cols) & (~is_rc |is_ac), idx_foi, indices_fo);
        
        if any(probably_mispelled_cols)
            pmc = cols(probably_mispelled_cols);
            error(err_id, 'INDICATORS : <%s> HAVE NO FORMULA TO BE BUILT AND WERE NOT PROVIDED!', sprintf('%s\', pmc{:}));
        end
        
        if ~b
            tmp = cols(probably_mispelled_cols);
            error(err_id, ['There is no path at all, even if we had all the cols,\n'...
                sprintf('the cols that are impossible to build are : <%s>\n', sprintf('%s/', tmp{:}))...
                '\t Please check the spelling of these colnames. \n' ...
                'If there is no mispelling, consider adding some formulas in the xml.']);
        end
        error_mess = {'No formula combination can reach the indicators you requested from the ones you provided,',...
            '\tyou have to reconsider the required cols or the cols you can provide, or add some formulas to the xml.\n'};
        
        missing_cols = cols(is_mc);
        
        
        
        error_mess{end+1} = sprintf('We didn''t succeeded in building cols : <%s>', sprintf('%s/',missing_cols{:}));
        
        ppu = set_diff_(postf_puc_upd(is_mc, is_pg, f_parents), is_ac | reachable_cols | is_rc);
        if ~any(ppu)
            ppu = set_diff_(postf_puc_upd(is_mc, is_pg, f_parents), is_ac | reachable_cols);
        end
        tmp = cols(ppu);
        error_mess{end+1} = sprintf('\tPossible candidate to solve this problem are : %s', sprintf('%s, ',tmp{:}));
        
        error_mess{end+1} = sprintf('\tAs a reminder, here are the formulas that can build the indicators we didnt succeeded to build :\n');
        for i = 1 : length(missing_cols)
            error_mess{end+1} = sprintf('\t\t<%s>:\n', missing_cols{i});
            error_mess{end+1} = sprintf('\t\t\t- %s\n', formulas(strmatch(missing_cols{i}, {formulas.out_col})).params);
            error_mess{end+1} = sprintf('\n\n');
        end
        error(err_id, sprintf('%s\n', error_mess{:}));
    end
end
% >


    function varargout = dummy_iwc(varargin)
        varargout = {true,inf};
    end

if agg_wanted
% < defining the way preformula exploration should be done : 

        pathes_after_pref = struct('dep_indices', {{{}}}, 'cost', 0, 'ac_so_far', is_ac, ...
            'is_active', true, 'potential_dep', pref_puc(indices_fo));
        SS_pref = struct('dependencies_cost', [formulas.cost], ...
            'get_potential_arrival_set', @get_potential_arrival_set_formulas, ...
            'add_path_elmnt', @add_formula_path_forward, ...
            'indices_dep_o', indices_fo, ...
            'idx_dep_oi', idx_foi, ...
            'potentially_useful_cols', @(ia)pref_puc_upd(slicer_puc, ia, osf_parents),  ...
            'is_a_valid_path_for_next_step', @slicer_path_exist, ...
            'is_a_valid_path_to_explore', @pref_path_exist, ...
            'is_a_comparable_path', @is_a_comparable_path_formulas, ...
            'is_worse_continuing', @dummy_iwc, ...
            'min_cost', inf, ...
            'CAI_PATH_LIMIT', CAI_PATH_LIMIT);

    % >

    %< Computing preformula pathes
    if st_version.my_env.verbosity_level >= VERBOSITY_LVL_HERE
        st_log('indicators_computation_plan:exec: exploring pre formulas... ');
        t = clock;
    end
    pathes_after_pref = explore_pathes_forward(pathes_after_pref, SS_pref);
    is_time_limit_passed
    %>

    % < removing pathes that have generated useless intermediaries, but why did it?
    [pathes_after_pref.cost, idx] = sort(pathes_after_pref.cost);
    pathes_after_pref.ac_so_far = pathes_after_pref.ac_so_far(idx, :);
    pathes_after_pref.dep_indices = pathes_after_pref.dep_indices(idx, :);
    % pathes_after_pref.is_active = pathes_after_pref.ac_so_far(idx, :); % useless, will be overridden below
    % pathes_after_pref.potential_dep = pathes_after_pref.ac_so_far(idx, :); % useless, will be overridden below
    if str2num(strtok(v, '.')) > 7 %v(1)
        [~, idx] = unique(bsxfun(@and, pathes_after_pref.ac_so_far, slicer_puc), 'rows', 'first', 'legacy');
    else
        [~, idx] = unique(bsxfun(@and, pathes_after_pref.ac_so_far, slicer_puc), 'rows', 'first');
    end
    
    pathes_after_pref.cost = pathes_after_pref.cost(idx, :);
    pathes_after_pref.ac_so_far = pathes_after_pref.ac_so_far(idx, :);
    pathes_after_pref.dep_indices = pathes_after_pref.dep_indices(idx, :);
    % >

    % < Preparing pathes for next step
    pathes_after_slicers = pathes_after_pref;
    pathes_after_slicers.is_active = true(size(pathes_after_slicers.cost));
    pathes_after_slicers.preformula = pathes_after_slicers.dep_indices ;
    pathes_after_slicers.dep_indices = cell(size(pathes_after_slicers.cost));
    pathes_after_slicers.potential_dep = repmat(postf_puc(indices_so), ...
        size(pathes_after_slicers.cost, 1), 1);
    % >

    % < Specifying slicers step
    SS_slicer = SS_pref;
    SS_slicer.dependencies_cost = [slicers.cost];
    SS_slicer.get_potential_arrival_set = @(ia, sc)get_potential_arrival_set_slicers(is_pg, ia, sc); ... % wee also have the cols provided by grid making
    SS_slicer.add_path_elmnt = @add_slicer_path_forward;
    SS_slicer.indices_dep_o = indices_so;
    SS_slicer.idx_dep_oi = idx_soi;
    SS_slicer.potentially_useful_cols = @(ia)(postf_puc); % static update : only one step
    SS_slicer.is_a_valid_path_for_next_step = @(potia, isoi_restr, indo_restr)post_f_path_exist(potia, idx_foi, indices_fo); 
    SS_slicer.is_a_valid_path_to_explore = ... if a postfformula path exists
        @(potia, isoi_restr, indo_restr)post_f_path_exist(potia, idx_foi, indices_fo);
    SS_slicer.is_a_comparable_path = @is_a_comparable_path_slicers;
    % SS_slicer.is_worse_continuing = @dummy_iwc; % equal to pref
    % SS_slicer.min_cost = inf; % equal to pref
    % SS_slicer.CAI_PATH_LIMIT = CAI_PATH_LIMIT
    % >

    % < Exploring the Slicers step
    if st_version.my_env.verbosity_level >= VERBOSITY_LVL_HERE
        st_log('Done in %gs\n', etime(clock, t));
        st_log('indicators_computation_plan:exec: exploring slicer step... ');
        t = clock;
    end
    % path_describer(cols, is_ac, pathes_after_slicers, formulas, slicers, POST_FORMULA_COST_SAVING_FACTOR);
    pathes_after_slicers = explore_pathes_forward(pathes_after_slicers, SS_slicer);
    is_time_limit_passed
    % >

    if st_version.my_env.verbosity_level >= VERBOSITY_LVL_HERE
        st_log('Done in %gs\n', etime(clock, t));
        st_log('indicators_computation_plan:exec: processing slicer step info... ');
        t = clock;
    end

    % < ordering pathes with increasing costs in the hope of quickly finding a
    % complete path in order to limit other ones cost
    [pathes_after_slicers.cost, idx] = sort(pathes_after_slicers.cost);
    pathes_after_slicers.ac_so_far = pathes_after_slicers.ac_so_far(idx, :);
    pathes_after_slicers.dep_indices = pathes_after_slicers.dep_indices(idx, :);
    % >

    % < preparing pathes for next step
    c_pathes = pathes_after_slicers;
    c_pathes.s_indices = c_pathes.dep_indices ;
    c_pathes.dep_indices = repmat({{}}, size(c_pathes.cost));
    c_pathes.is_active = true(size(c_pathes.cost));
    c_pathes.potential_dep = true(size(c_pathes.cost, 1), length(formulas));
    for i = 1 : length(c_pathes.is_active)
        tmp = postf_puc_upd(is_rc, c_pathes.ac_so_far(i, :), f_parents);
        c_pathes.potential_dep(i, :) = tmp(indices_fo);
    end
    % >

    if st_version.my_env.verbosity_level >= VERBOSITY_LVL_HERE
        st_log('Done in %gs\n', etime(clock, t));
        st_log('indicators_computation_plan:exec: exploring post formula step... ');
        t = clock;
    end
    
    % < Has already a path found a way to reuired cols?
    iii = is_included_in(is_rc, c_pathes.ac_so_far);
    if any(iii)
        min_cost = min(c_pathes.cost(iii));
        c_pathes.is_active(iii) = false;
    else
        min_cost = inf;
    end
    % >
    
    % < Specifying postformulas step
    SS_postf = SS_pref;
    SS_postf.dependencies_cost = SS_postf.dependencies_cost*POST_FORMULA_COST_SAVING_FACTOR;
    % SS_postf.get_potential_arrival_set = @get_potential_arrival_set_formulas;
    % SS_postf.add_path_elmnt = @add_formula_path_forward;
    % SS_postf.indices_dep_o = indices_fo;
    % SS_postf.idx_dep_oi = idx_foi;
    SS_postf.potentially_useful_cols = @(ia)postf_puc_upd(is_rc, ia, osf_parents);
    SS_postf.is_a_valid_path_for_next_step = @(ia)is_included_in(is_rc, ia); % finished
    SS_postf.is_a_valid_path_to_explore = @post_f_path_exist;
    % SS_postf.is_a_comparable_path = @is_a_comparable_path_formulas;
    SS_postf.is_worse_continuing = @iwc_;
    SS_postf.min_cost = min_cost;
    % SS_postf.CAI_PATH_LIMIT = CAI_PATH_LIMIT
    % >
else
    if st_version.my_env.verbosity_level >= VERBOSITY_LVL_HERE
        st_log('indicators_computation_plan:exec: exploring formulas... ');
        t = clock;
    end
    c_pathes = struct('dep_indices', {{{}}}, 'cost', 0, 'ac_so_far', is_ac, ...
            'is_active', true, 'potential_dep', postf_puc(indices_fo));
    SS_postf = struct('dependencies_cost', [formulas.cost], ...
            'get_potential_arrival_set', @get_potential_arrival_set_formulas, ...
            'add_path_elmnt', @add_formula_path_forward, ...
            'indices_dep_o', indices_fo, ...
            'idx_dep_oi', idx_foi, ...
            'potentially_useful_cols', @(ia)postf_puc_upd(is_rc, ia, osf_parents),  ...
            'is_a_valid_path_for_next_step', @(ia)is_included_in(is_rc, ia), ... % finished
            'is_a_valid_path_to_explore', @post_f_path_exist, ...
            'is_a_comparable_path', @is_a_comparable_path_formulas, ...
            'is_worse_continuing', @iwc_, ...
            'min_cost', inf, ...
            'CAI_PATH_LIMIT', CAI_PATH_LIMIT);
end

% < When should we stop exploring post formula step?
function [iw, min_cost] = iwc_(ac_so_far_, c, min_cost)
    iw = c < min_cost;
    if iw && is_included_in(is_rc, is_pg | ac_so_far_)
        iw = false;
        min_cost = c;
    end
end
% >

% < exploring the post_formula step
% path_describer(cols, is_ac, c_pathes, formulas, slicers, POST_FORMULA_COST_SAVING_FACTOR);
c_pathes = explore_pathes_forward(c_pathes, SS_postf);
c_pathes.postformula = c_pathes.dep_indices;
% <
if st_version.my_env.verbosity_level >= VERBOSITY_LVL_HERE
    st_log('Done in %gs\n', etime(clock, t));
    st_log('Total Elapsed time = %g secondes\n', etime(clock, t0));
end

[cost, chosen_path] = min(c_pathes.cost);
if agg_wanted
    if isempty(c_pathes.preformula)
        ind_preformulas = {};
    else
        ind_preformulas = c_pathes.preformula{chosen_path, :};
    end
    if isempty(c_pathes.s_indices)
        ind_slicers = [];
    else
        ind_slicers = c_pathes.s_indices{chosen_path, :};
    end
    if isempty(c_pathes.postformula)
        ind_postformulas = {};
    else
        ind_postformulas = c_pathes.postformula{chosen_path, :};
    end
    used_cols = ( any(idx_foi(cat(2, ind_preformulas{:}), :), 1) | ...
        any(idx_soi(ind_slicers, :), 1) ) ...
        & is_ac;
else
    ind_preformulas = c_pathes.postformula{chosen_path, :};
    ind_postformulas = 'YOU SHOULDNT NEED THIS!!! INDICATORS_COMPUTATION_PLAN considered you just wanted to use formulas to convert some indicators into others';
    used_cols = ( any(idx_foi(cat(2, ind_preformulas{:}), :), 1) ) ...
        & is_ac;
    slicers = [];
end

if st_version.my_env.verbosity_level >= VERBOSITY_LVL_HERE
    path_describer([], ...
        formulas, slicers, ...
        POST_FORMULA_COST_SAVING_FACTOR, ...
        {ind_preformulas}, {ind_slicers}, {ind_postformulas}, cost);
end

end

% < ** inputs processing
function [idx, is_this_col_type, ind_in_inputs] = process_col_type(idx, nb_typed_cols, nb_cols, type_)
% process the link between a col, its id and a type
idxrange = 1:nb_typed_cols;
ind_in_inputs  = idx(idxrange);
is_this_col_type = false(1, nb_cols);
is_this_col_type(ind_in_inputs) = true;
idx(idxrange) = [];
if sum(is_this_col_type) ~= nb_typed_cols
    error('indicator_compuation_plan:check_args', ...
        'The %s should be unique.', type_);
end
end

function [idx, indices_o_cols, idx_oi_cols, one_step_parents, parents, ind_in_inputs] = ...
    process_dependency(idx, dependencies, nb_cols)
% process the dependencies between cols that exist beacause of formulas or
% slicers

nbd = length(dependencies);
% < indices_o_cols, is_out_col
idxrange = 1:nbd;
indices_o_cols = idx(idxrange); % indices_o_cols
idx(idxrange) = [];
ind_in_inputs = cell(nbd, 1);
% >

% < idx_oi_cols for eaxh dependency, whose out_col id is given by indices_o_cols
% what are the idx of required cols
idx_oi_cols = false(length(dependencies), nb_cols);
one_step_parents = zeros(nb_cols);% also intialising parents
for i = 1 : nbd
    idxrange = 1:length(dependencies(i).in_cols);
    idx_oi_cols(i, idx(idxrange)) = true;
    ind_in_inputs{i} = idx(idxrange);
    idx(idxrange) = [];
    one_step_parents(indices_o_cols(i), idx_oi_cols(i, :)) = 1; % intialising parents
end
parents = double(eye(nb_cols) | one_step_parents);
% parents(~parents) = 2.0;
% >

%< parents
% out_cols are the columns of parent
% a column is a boolean of wether the col appears as an in_col of a
% formula which could provide (with potentially other columns) the lines of
% this matrix are the whole cols
% therefore parents * parents are second orders parents
next_generation = parents * parents;
while any(any(next_generation ~= parents))
    parents = next_generation;
    next_generation = parents * parents;
    next_generation(logical(next_generation)) = 1;
end
%>

end
% > **

% < * used only for exploration of the reasons why a col cannot be produced.
% This function provides the set of all minimal sets (in terms of 
% inclusion of cols that an be used to provide a given set of cols
function isneeded = explore_pathes_backwardformulas(isneeded, ...
    allowed_formula, ...
    indices_fo, ...
    idx_foi)
% ind_col2produce, ...
%     indices_fo, ...
%     idx_foi)
% isneeded = false(1, size(idx_foi, 2));
% isneeded(ind_col2produce) = true;
% already_produced_cols = false(1, size(idx_foi, 2));
if isempty(allowed_formula)
    allowed_formula = true(size(isneeded, 1), size(indices_fo, 2));
end
is_active = true;
while any(is_active)
    curr_path = find(is_active, 1);
    curr_is_needed = isneeded(curr_path, :);
    curr_allowed_formula = allowed_formula(curr_path, :);
    ind_pot_formula = curr_is_needed(:, indices_fo) & ...
        curr_allowed_formula;
    ind_pot_formula = find(ind_pot_formula);
    for i = 1 : length(ind_pot_formula)
        curr_formula = ind_pot_formula(i);
        isneeded(end+1, :) = curr_is_needed;
        isneeded(end, indices_fo(curr_formula)) = false;
        isneeded(end, idx_foi(curr_formula, :)) = true;
        % the formulas which need the produced col are now forbidden
        allowed_formula(end+1, :) = curr_allowed_formula;
        allowed_formula(end, idx_foi(:, indices_fo(curr_formula))) = false; 
        allowed_formula(end, curr_formula) = false;
        is_active(end+1) = true;
    end
    is_active(curr_path) = false;
end
isneeded = unique(isneeded, 'rows');
%global st_cols
% for i = 1:size(isneeded, 1)
% st_cols(isneeded(i, :))
%end
end

function isneeded = explore_pathes_backwardslicers(ind_col2produce, ...
    indices_fo, ...
    idx_foi)
isneeded = false(1, size(idx_foi, 2));
already_produced_cols = isneeded;
isneeded(ind_col2produce) = true;
is_active = true;
while any(is_active)
    curr_path = find(is_active);
    is_active = false;
    curr_is_needed = isneeded(curr_path, :);
    curr_already_produced_cols = already_produced_cols(curr_path, :);
    pot_formula = curr_is_needed(indices_fo) & ...
        ~curr_already_produced_cols(indices_fo);
    pot_formula = find(pot_formula);
    for i = 1 : length(pot_formula)
        curr_formula = pot_formula(i);
        isneeded(end+1, :) = curr_is_needed;
        isneeded(end, indices_fo(curr_formula)) = false;
        isneeded(end, idx_foi(curr_formula, :)) = true;
        already_produced_cols(end+1, :) = curr_already_produced_cols;
        already_produced_cols(end, indices_fo(curr_formula)) = true;
    end
    is_active(curr_path) = false;
end
end
% % < *

% > *** The function to explore forward all the step in a path
function pathes = explore_pathes_forward(pathes, SS)
% EXPLORE_PATHES_FORWARD - forward exploration of the chilren of 
% the available cols thanks to dependencies

% information about the condition and dynamic of the step under exploration
% SS = struct('dependencies_cost', dependencies_cost, ...
%     'get_potential_arrival_set', get_potential_arrival_set, ...
%     'add_path_elmnt', add_path_elmnt, ...
%     'indices_dep_o', indices_dep_o, ...
%     'idx_dep_oi', idx_dep_oi, ...
%     'potentially_useful_cols', potentially_useful_cols,  ...
%     'is_a_valid_path_for_next_step', is_a_valid_path_for_next_step, ...
%     'is_a_valid_path_to_explore', is_a_valid_path_to_explore, ...
%     'is_a_comparable_path', is_a_comparable_path, ...
%     'is_worse_continuing', ...
%     'min_cost');
%     CAI_PATH_LIMIT

%informations about the current step
CS = struct('is_ac_this_path', [], ...
    'potential_f_this_path', [], ...
    'curr_path_cost', [], ...
    'pot_pathes_potential_f', [], ...
    'min_cost', SS.min_cost);
nb_cai = 0;
active_path_ind = NaN;
while any(pathes.is_active)
    if nb_cai > SS.CAI_PATH_LIMIT
        error('indicators_computation_plan:exec', ...
            ['Unfinished exploration because of too many pathes to explore : %d\n' ...
            'Consider increasing the CAI_PATH_LIMIT which is actually set to %d pathes\n'], ...
            nb_cai, SS.CAI_PATH_LIMIT);
    end
    if isfinite(active_path_ind)
        pot_arrival_set = SS.get_potential_arrival_set(CS.is_ac_this_path, []);
        is_valid = SS.is_a_valid_path_for_next_step(pot_arrival_set);
        if ~is_valid
            [pathes, active_path_ind] = ...
                struct_delete_elements(pathes, active_path_ind, active_path_ind);
        end
    end
    active_path_ind = find(pathes.is_active, 1);
    
    %< unfortunately not a good idea % could have been usefull for post
    % formula in order to find a complete path more quickly but lead to
    % decreased performance on the exmple tried
%     active_path_ind = find(pathes.is_active);
%     [~, ind_in_actives] = min(pathes.cost(active_path_ind));
%     active_path_ind = active_path_ind(ind_in_actives);
    %>
    CS.is_ac_this_path = pathes.ac_so_far(active_path_ind, :); % available columns in this path
    CS.potential_f_this_path = pathes.potential_dep(active_path_ind, :); % iscell(pathes.dep_indices{active_path_ind}) && length(pathes.dep_indices{active_path_ind}{1}) == 7 && all(pathes.dep_indices{active_path_ind}{1} == 1:7)
    CS.curr_path_cost = pathes.cost(active_path_ind);
    
    pathes.is_active(active_path_ind) = false; % will be processed soon
    [iwc, CS.min_cost] = SS.is_worse_continuing(CS.is_ac_this_path, CS.curr_path_cost, CS.min_cost);
    if ~iwc
        continue;
    end
    
    indices_dependencies = SS.potentially_useful_cols(CS.is_ac_this_path)';% the formula might be worse appyling : producing a new col, potential parent of the next step
    
    if any(indices_dependencies)
        idx_dependencies = CS.potential_f_this_path & indices_dependencies(SS.indices_dep_o)';
        appliable_dependencies = contains(CS.is_ac_this_path, SS.idx_dep_oi(idx_dependencies, :)); % it is possible to apply formula;
        if any(appliable_dependencies)
            indices_dependencies = find(idx_dependencies);
            indices_dependencies = indices_dependencies(appliable_dependencies);
            % < we will have a look at all the combinations of
            % dependencies = (formulas or slicers) that could be applied at
            % the current stage so if a path does not have applied one of
            % these functions he shouldn't be able to do later
            CS.pot_pathes_potential_f = CS.potential_f_this_path;
            CS.pot_pathes_potential_f(indices_dependencies) = false;
            % >
            
            % < checking if any of the dependencies are producing the same
            % column. Then it would be an easy choice : let's pick the
            % formula that is the cheapest if they are equivalent then
            % we can also take anyone
            n = length(indices_dependencies);
            ind_needed_dep = [];
            if n > 1
                col_set2be_produced = SS.indices_dep_o(indices_dependencies);
                v = version();
                if str2num(strtok(v, '.')) > 7 %'7' %v(1)>'7'
                    [u_ind_p, no_use, idx] = unique(col_set2be_produced, 'legacy');
                else
                    [u_ind_p, no_use, idx] = unique(col_set2be_produced);
                end
                
                
                if length(u_ind_p) ~= n % then indices_dependencies can cill change, that's why the line just the above
                    % idx = idx'; col_set2be_produced=col_set2be_produced'; 
                    tmp = accumarray(idx', ones(size(col_set2be_produced))');
                    chosen_dep = [];
                    groups_of_dep = cell(1, length(tmp));
                    for i = 1 : length(tmp)
                        if tmp(i) > 1.1
                            groups_of_dep{i} = indices_dependencies(idx == i);
                            [no_use, ind] = min(SS.dependencies_cost(groups_of_dep{i}));
                            chosen_dep = cat(2, chosen_dep, groups_of_dep{i}(ind));
                        end
                    end
                    indices_dependencies = [chosen_dep, setdiff(indices_dependencies, cat(2, groups_of_dep{:}))];
                end
                n = length(indices_dependencies);
                % >
                % < Now we will have a look at the cols that has to be produced
                % by answering the question : if i forbid this formula to be
                % applied is there still a path to the required cols?
                if n > 2
                    needed_dep = false(size(indices_dependencies));
                    for i = 1 : n
                        % < if the path is valid, then it will furthermore be
                        % added to the pathes so that we wont need to
                        % process it any furher for now
                        [pathes, active_path_ind, nb_cai, needed_dep(i)] = ...
                            check_and_insert_path(indices_dependencies([1:i-1, i+1:n]), ...
                            pathes, SS, CS, active_path_ind, nb_cai);
                        % >
                    end
                    ind_needed_dep = indices_dependencies(needed_dep);
                    % <
                    [pathes, active_path_ind, nb_cai] = ...
                        check_and_insert_path(ind_needed_dep, pathes, ...
                        SS, CS, active_path_ind, nb_cai);
                    % >
                    if all(needed_dep)
                        continue;
                    end
                    n = length(indices_dependencies)-length(ind_needed_dep);
                    % < We have already processed n-1 among n combinations 
                    % (among not needed dependencies) as well as the zero one
                    % but not all combinations, that's what we do now :
                    [pathes, active_path_ind, nb_cai] = ...
                        check_and_insert_path(indices_dependencies, pathes,...
                        SS, CS, active_path_ind, nb_cai);
                    % >
                    n = n - 2;
                    indices_dependencies = setdiff(indices_dependencies, ind_needed_dep);
                end
            end
            % >
            % > Looking at the remaining combinations of formulas or
            % slicers
            if nb_cai + 2^n > SS.CAI_PATH_LIMIT % this is an approximative limit, 
                % some of the 2^n have already been processed and thus counted in nb_cai
                error('indicators_computation_plan:exec', ...
                    ['Unfinished exploration because of too many pathes to explore : %d\n' ...
                    'Consider increasing the CAI_PATH_LIMIT which is actually set to %d pathes\n'], ...
                    nb_cai + 2^n, SS.CAI_PATH_LIMIT);
            end
            for i = 1 : n
                all_combinations = nchoosek(indices_dependencies,i); % an important way to prune the tree : by making all the combination of these formula,
                % as we don't care about the order of applying the selected dependencies,
                % then we will generate all combination and forbid the use of any of these dependencies in the futur of the path
                for j = 1:size(all_combinations, 1)
                    path_s_dep_indices = [ind_needed_dep all_combinations(j, :)];
                    [pathes, active_path_ind, nb_cai, not_is_valid] = ...
                        check_and_insert_path(path_s_dep_indices, pathes, ...
                        SS, CS, active_path_ind, nb_cai);
                end
            end
            % >
        end
    end
end
if isfinite(active_path_ind)
    pot_arrival_set = SS.get_potential_arrival_set(CS.is_ac_this_path, []);
    is_valid = SS.is_a_valid_path_for_next_step(pot_arrival_set);
    if ~is_valid
        [pathes, active_path_ind] = ...
            struct_delete_elements(pathes, active_path_ind, active_path_ind);
    end
end
end

% < *
function [pathes, active_path_ind, nb_cai, not_is_valid] = check_and_insert_path(...
    path_s_dep_indices, pathes, SS, CS, active_path_ind, nb_cai)
EPS = 1e-4;
if nargout < 3
    error('check_and_insert_path:check_output', ...
        'Please care about active_path_ind and nb_cai');
end
nb_cai = nb_cai + 1;

pot_arrival_set = SS.get_potential_arrival_set(CS.is_ac_this_path, path_s_dep_indices);
not_is_valid = SS.is_a_valid_path_to_explore(pot_arrival_set, ...
    SS.idx_dep_oi(CS.pot_pathes_potential_f, :), ...
    SS.indices_dep_o(CS.pot_pathes_potential_f));
not_is_valid= ~not_is_valid;
if not_is_valid
    return;
end

potential_cost = CS.curr_path_cost ...
                + sum(SS.dependencies_cost(path_s_dep_indices));

if potential_cost >= SS.min_cost
    not_is_valid = true;
    return;
end
            
iacp = SS.is_a_comparable_path(pathes, CS.pot_pathes_potential_f);



% [idx_e, idx_is, idx_cs] = set_relation(pot_arrival_set, pathes.ac_so_far(iacp, :));
more_expensive_pathes = (potential_cost < pathes.cost-EPS);

ind_iacp = find(iacp);

% tmp = false(size(iacp));
% tmp(ind_iacp(idx_e)) = true;
% idx_e = tmp;
% 
% tmp = false(size(iacp));
% tmp(ind_iacp(idx_is)) = true;
% idx_is = tmp;
% 
% tmp = false(size(iacp));
% tmp(ind_iacp(idx_cs)) = true;
% idx_cs = tmp;

idx_cs_oe = false(size(iacp));
idx_cs_oe(ind_iacp(~any(bsxfun(@gt, pathes.ac_so_far(iacp, :), pot_arrival_set), 2))) = true; 

idx_is_oe = false(size(iacp));
idx_is_oe(ind_iacp(~any(bsxfun(@gt, pot_arrival_set, pathes.ac_so_far(iacp, :)), 2))) = true; 


%< deleting more expensive pathes to a smaller or equal set
idx_wop = (idx_cs_oe) & more_expensive_pathes & iacp;
if any(idx_wop)
    indices_wop = find(idx_wop);
    for k = length(indices_wop) : -1 : 1
        [pathes, active_path_ind] = ...
            struct_delete_elements(pathes, indices_wop(k), active_path_ind);
    end
end
%>
%< There is not already a path less expensive to to bigger set of cols than this potential path
% so let's try this way
if ~any((idx_is_oe) & ~more_expensive_pathes & iacp)
    [pathes, active_path_ind] = SS.add_path_elmnt(pathes, ...
        path_s_dep_indices, ...
        potential_cost, ...
        pot_arrival_set,...
        CS.pot_pathes_potential_f, ...
        active_path_ind);
%      = active_path_ind+1;
end
%>
end
% < Which pathes under exploration can i compare to given path?
% for formulas, any that has at least the same allowed formulas :
    function iacp = is_a_comparable_path_formulas(pathes, pot_formula_this_path)
        iacp = is_included_in(pot_formula_this_path, pathes.potential_dep);
    end
% for slicers, only the ones which already have made their slicer step
    function iacp = is_a_comparable_path_slicers(pathes, pot_formula_this_path)
        iacp = ~cellfun(@isempty, pathes.dep_indices, 'uni', true);
    end
% >
% > *

% <** add_path_element functions and delete path
function [pathes, active_path_ind] = add_formula_path_forward(pathes, ...
    f_indices, cost, ...
    ac_so_far, ...
    potential_f, ...
    indice_previous_step_element)
fn = fieldnames(pathes);



% for i = 1 : length(fn)
%     pathes.(fn{i})(end+1, :) = pathes.(fn{i})(indice_previous_step_element, :);
% end
% if isempty(pathes.dep_indices{end}) || isempty(pathes.dep_indices{end}{end})
%     pathes.dep_indices{end, :} = {f_indices};
% else
%     pathes.dep_indices{end, :} =cellflat( cat(1, pathes.dep_indices{end}, f_indices) );
% end
% pathes.cost(end, :) = cost; % ~isempty(pathes.dep_indices{end}) && length(pathes.dep_indices{end}{1}) == 7 && all(pathes.dep_indices{end}{1} == 1:7)
% pathes.ac_so_far(end, :) = ac_so_far;
% pathes.is_active(end, :) = true;
% pathes.potential_dep(end, :) = potential_f;


% Putting it in first element so that we will first be looking for a
% complete path in order to stop too expensive pathes
for i = 1 : length(fn)
    pathes.(fn{i}) = cat(1, pathes.(fn{i})(indice_previous_step_element, :), pathes.(fn{i}));
end
if isempty(pathes.dep_indices{1}) || isempty(pathes.dep_indices{1}{end})
    pathes.dep_indices{1, :} = {f_indices};
else
    pathes.dep_indices{1, :} =cellflat( cat(1,pathes.dep_indices{1}, f_indices) );
end
pathes.cost(1, :) = cost; % ~isempty(pathes.dep_indices{end}) && length(pathes.dep_indices{end}{1}) == 7 && all(pathes.dep_indices{end}{1} == 1:7)
pathes.ac_so_far(1, :) = ac_so_far;
pathes.is_active(1, :) = true;
pathes.potential_dep(1, :) = potential_f;

active_path_ind = indice_previous_step_element + 1;
end

function [slicer_step, indice_previous_step_element] = add_slicer_path_forward(slicer_step, ...
    s_indices, ...
    cost, ...
    ac_so_far, ...
    potential_s, ...
    indice_previous_step_element)

fn = fieldnames(slicer_step);
for i = 1 : length(fn)
    slicer_step.(fn{i})(end+1, :) = slicer_step.(fn{i})(indice_previous_step_element, :);
end
slicer_step.dep_indices{end, :} = cat(2, slicer_step.dep_indices{end, :}, s_indices);
slicer_step.cost(end, :) = cost;
slicer_step.ac_so_far(end, :) = ac_so_far;
slicer_step.is_active(end, :) = false; % ! only one slicer step
slicer_step.potential_dep(end, :) = potential_s;
end

function [s,active_path_ind] = struct_delete_elements(...
    s, j, active_path_ind)
% remove the j-th row of each fields in the structure
if nargout < 2
    error('struct_delete_elements:exec', 'Please do care about the moving indices');
end
if j < active_path_ind
    active_path_ind = active_path_ind - 1;
elseif j == active_path_ind
    active_path_ind = NaN;
end
fn = fieldnames(s); % ~isempty(s.dep_indices{j}) && length(s.dep_indices{j}{1}) == 7 && all(s.dep_indices{j}{1} == 1:7)
for i = 1 : length(fn)
    s.(fn{i})(j, :) = [];
end
end

% >**

% > ***

% < *** col set (represented by a logical vector of presence) functions
function idx = contains(a, b)
idx = ~any(bsxfun(@gt, b, a), 2);
end

function idx = is_equal_(a, b)
    idx = ~any(bsxfun(@ne, a, b), 2);
end

function idx = is_included_in(a, b)
    idx = ~any(bsxfun(@gt, a, b), 2);
end

function c = set_diff_(a, b)
    c = bsxfun(@and, a, ~b);
end

function [idx_e, idx_is, idx_cs] = set_relation(a, b)
% [idx_e, idx_is, idx_cs, idx_nr] = set_relation(a, b)
	EPS = eps(1);
%     a = double(a);
%     b = double(b);
    idx = bsxfun(@minus, a, b);
    idx_a_minus_b = any(idx >  EPS, 2);
    idx_b_minus_a = any(idx < -EPS, 2);
    % equal = 1
    idx_e = ~(idx_a_minus_b |  idx_b_minus_a);
    % included strict = -1 
    idx_is =  idx_b_minus_a & ~idx_a_minus_b;
    % contains strict = 2
    idx_cs =  idx_a_minus_b & ~idx_b_minus_a;
%     % both setdiff non enmpty = 0
%     idx_nr =  idx_a_minus_b &  idx_b_minus_a;
end

function c = set_union_(a, b)
    c = bsxfun(@or, a, b);
end
% > ***
 