function [data_out, grid, idx_dump,accum_idx, kingpin_out] =...
    make_grid(mode, data, varargin)
% MAKE_GRID - create bins according to a way of grouping observations
%
% In this function we assume :
%   - data is timestamped on Local Time therefore if you need to be able to
%       align data from different timezone and have an exotic step
%       (not a divisor of one hour) then you have to compute a good
%       kingpin by yourself : Therefore kingpin is expressed in localtime
%   - that trading time in local time are on only one day!
%
% Be careful when developping new modes:
% * not to give a name for a mode that
%       could also be the name of a variable, otherwise, the group_by mode
%       will be annoyed
%
% * add at the very begining a kind of return_cols submode
%
% Examples:
%
%
%
%
% See also: generate_grid  apply_plan_to_grid
%
%
%   author   : 'athabault@cheuvreux.com'
%   reviewer : 'rburgot@cheuvreux.com'
%   version  : '1.1'
%   date     :  '15/03/2011'
%
%   last_checkin_info : $Header: make_grid.m: Revision: 36: Author: robur: Date: 04/13/2012 06:05:03 PM$
% 

TIME_EPS = 2e-5/(24*3600);%0.5/(24*3600);
v = version();

if iscell(mode)
    if length(mode) == 1 % in case the user entered a mode like 'grid_mode', {'time_and_phase'}, ... instead of 'grid_mode', 'time_and_phase', ...
        mode = mode{1};
    else
        group_by_sub_mode = mode{1};
        group_by_variables = mode(2:end);
        mode = 'group_by';
    end
end

switch lower(mode)
    case 'group_by'
        
        wc = [];
        us_gbm = [];
        
        idx_wc = find(strcmp('wildcard', varargin(1:2:end))); % impossible to use normal options or options_light parsing
        if ~isempty(idx_wc)
            wc = varargin{2*idx_wc};
            if isnan(wc)
                error('make_grid:group_by', 'The wildcard value cannot be NaN.'); % otherwise unique wont work
            end
            varargin([(2*idx_wc-1) 2*idx_wc]) = [];% removing it as this option is specific to the group-by mode
        end
        
        %< Getting user_specified_group_by_modalities
        idx_us_gbm = find(strcmp('user_specified_group_by_modalities', varargin(1:2:end))); % impossible to use normal options or options_light parsing
        if ~isempty(idx_us_gbm)
            us_gbm = varargin{2*idx_us_gbm};
            if any(any(isnan(us_gbm)))
                error('make_grid:group_by', 'The user_specified_group_by_modalities values cannot be NaN.'); % otherwise unique wont work
            end
            varargin([(2*idx_us_gbm-1) 2*idx_us_gbm]) = [];% removing it as this option is specific to the group-by mode
        end
        %>
        
        if ischar(data) && strcmp(data, 'return_cols')
            if strcmp(group_by_sub_mode, 'variable_based_time')&&any(strcmp('phase', group_by_variables))
                error('make_grid:group_by', 'You sould probably not use phase as a group_by_variable with variable_base_time, as it may create funny bins. \nI suggest you filter out auction data or implement a variable_based_time_and_phase mode for make_grid.\n');
            end
            
            
            cols_nap_by_sub_mode = make_grid(group_by_sub_mode, 'return_cols',varargin{:});
            %<
            if ~isempty(intersect(cols_nap_by_sub_mode.cols_needed4grid, ...
                    group_by_variables))
                %                 if ~(strcmp('liquidity_consuming', group_by_sub_mode) && ...
                %                         any(strcmp('trading_destination_id', group_by_variables)) )
                %                     error('make_grid:group_by', ['You are asking for unicity of ' ...
                %                         'a variable that is already used in the grid mode <%s>'], ...
                %                         group_by_sub_mode);
                %                 else
                cols_nap_by_sub_mode.cols_needed4grid = ...
                    setdiff(cols_nap_by_sub_mode.cols_needed4grid, 'trading_destination_id'); % trading_destination_id should never be computed from something else
                data_out.cols_needed4grid = cellflat(cat(2, ...
                    cols_nap_by_sub_mode.cols_needed4grid, group_by_variables));
                data_out.cols_provided_by_make_grid = cellflat(cat(2, ...
                    cols_nap_by_sub_mode.cols_provided_by_make_grid, ...
                    group_by_variables));
                return;
                %                 end
            else
                data_out.cols_needed4grid = cellflat(cat(2, ...
                    cols_nap_by_sub_mode.cols_needed4grid, group_by_variables));
                data_out.cols_provided_by_make_grid = cellflat(cat(2, ...
                    cols_nap_by_sub_mode.cols_provided_by_make_grid, ...
                    group_by_variables));
                return;
            end
            %>
        end
        
        
        
        [data_out_subm, grid_subm, idx_dump, ...
            accum_idx_subm, kingpin_out] =...
            make_grid(group_by_sub_mode, data, varargin{:});
        
        values_gbv = st_data('cols', data, group_by_variables);
        nb_gbv = length(group_by_variables);
        nb_lines_original_data = size(data.value, 1);
        
        if any(any(isnan(values_gbv)))
            error('make_grid:group_by', ...
                'Unable to group by variables which take NaN values as NaN ~= NaN.');
        end
        
        if ~isempty(us_gbm)
            uv_gbv_tmp = unique(values_gbv, 'rows');
            
            assert(size(us_gbm,2) == nb_gbv , 'make_grid:group_by', 'Group by modalities do not have correct size');
            
            unexpected_modalities = setdiff(uv_gbv_tmp, us_gbm, 'rows');
            if ~isempty(unexpected_modalities)
                error('make_grid:group_by', ...
                    'Data contains group_by modalities not expected in user_specified_group_by_modalities:\n\t\t<%s>.',mat2str(unexpected_modalities));
            end
        end
        
        if ~isempty(wc)
            if any(any(wc == values_gbv))
                error('make_grid:group_by', ...
                    'The wildcard value cannot be a value already taken by one of your grouping variables.');
            end
            if ~isempty(us_gbm) && any(any(wc == us_gbm))
                error('make_grid:group_by', ...
                    'The wildcard value cannot be a value found in user_specified_group_by_modalities.');
            end
            %< Creating fake wild card values and fake same rows with these values
            
            fictitious_subgrids = cell(size(grid_subm,1), nb_gbv+1);
            tmp_values_gbv = [values_gbv; wc * ones(size(values_gbv))];
            fictitious_subgrids(:,1) = cellfun(@(c) c+length(values_gbv),grid_subm,'uni', false);
            
            if nb_gbv >1
                wc_one_col = wc * ones(size(values_gbv, 1), 1);
                for i = 1 : nb_gbv
                    tmp = values_gbv;
                    tmp(:, i) = wc_one_col;
                    tmp_values_gbv = [tmp_values_gbv; tmp];
                    fictitious_subgrids(:,i+1) = cellfun(@(c) c+(i+1)*nb_lines_original_data,grid_subm,'uni', false);
                end
            end
            
            values_gbv = tmp_values_gbv;
            tmp_grid_subm = cat(2, grid_subm, fictitious_subgrids);
            for i = 1:size(grid_subm,1)
                grid_subm{i} = cat(1,tmp_grid_subm{i,:});
            end
            %>
            
        end
        
        
        if strtok(v, '.') > '7' %v(1)
            [uv_gbv, ~, idx_in_uvgbv] = unique(values_gbv, 'rows', 'legacy');
        else
            [uv_gbv, ~, idx_in_uvgbv] = unique(values_gbv, 'rows');
        end
        
        
        %< Adding NEW user_specified_group_by_modalities to those found in data
        % (and the fake wildcard) to the END of the vector (be careful that
        % idx_in_uv_gbv is not changed).
        if ~isempty(us_gbm)
            uv_gbv = cat(1, uv_gbv, setdiff(us_gbm,uv_gbv, 'rows' ));
        end
        %>
        
        nb_uv_gbv = size(uv_gbv, 1);
        
        %< demultiplying grid_subm, accum_idx_subm, data_out_subm
        nb_lines_subm = size(grid_subm, 1);
        nb_cols_subm = size(data_out_subm.value, 2);
        grid = cell(nb_uv_gbv*nb_lines_subm, 1);
        accum_idx = NaN(size(accum_idx_subm));
        data_out = data_out_subm;
        data_out.date = NaN(nb_lines_subm*nb_uv_gbv, 1);
        data_out.value = NaN(nb_lines_subm*nb_uv_gbv, ...
            length(group_by_variables) + nb_cols_subm);
        k = 1;
        for i = 1 : nb_lines_subm
            i_bin = grid_subm{i};
            for j = 1 : nb_uv_gbv
                grid{k} = mod(i_bin(idx_in_uvgbv(i_bin) == j), nb_lines_original_data); % modulo is useful only when wildcard option is used, i.e. when fictitious subgrids are added to the original grid_subm
                grid{k}(grid{k}==0) = nb_lines_original_data;
                accum_idx(grid{k}) = k;
                data_out.value(k, :) = [data_out_subm.value(i, :) uv_gbv(j, :)];
                data_out.date(k) = data_out_subm.date(i);
                k = k + 1;
            end
        end
        if ~isempty(wc)||~all(isfinite(accum_idx_subm)) % alors la grille n'est pas disjointe et on ne peut utiliser accumarray
            accum_idx = NaN(size(accum_idx_subm));
        end
        %>
        
        %<
        data_out.colnames = cat(2,data_out.colnames, group_by_variables);
        
        %>
    case 'liquidity_consuming'
        % This mode requires a pre group by trading_destination_id and dark
        group_by_variables = {'trading_destination_id', 'dark'};
        if ischar(data) && strcmp(data, 'return_cols')
            data_out.cols_needed4grid = cat(2,{'price', 'bid', 'ask', 'ask_size', 'bid_size'},group_by_variables);
            data_out.cols_provided_by_make_grid = cat(2,{ 'begin_slice', 'end_slice'},group_by_variables);
            return;
        end
        
        opt = options_light({ ...
            'price_eps', 1e-5, ... %Magic Number :  used to round prices given by Sybase
            'time_eps', 15e-3/(24*3600), ... % Maximal time between two trades with the same liquidity consuming order
            }, varargin);
        
        price_eps = opt.price_eps;
        time_eps = opt.time_eps;
        
        seq_begins = false(length(data.date),1);
        seq_ends = false(length(data.date),1);
        
        values_gbv = st_data('cols', data, group_by_variables);
        if strtok(v, '.') > '7' %v(1)
            [uv_gbv, ~, idx_in_uvgbv] = unique(values_gbv, 'rows', 'legacy');
        else
            [uv_gbv, ~, idx_in_uvgbv] = unique(values_gbv, 'rows');
        end
        
        
        for i=1:length(uv_gbv)
            idx_tm = find(idx_in_uvgbv==i);
            data_this_modality = st_data('from-idx', data, idx_tm);
            
            [seq_begins_tm, seq_ends_tm] = ...
                compute_begin_ends4liquidity_consumation(data_this_modality, price_eps, time_eps);
            
            seq_begins(idx_tm) = seq_begins_tm;
            seq_ends(idx_tm) = seq_ends_tm;
        end
        
        accum_idx  = cumsum(seq_begins);
        
        data_out = st_data('init', 'colnames', cat(2,{ 'begin_slice', 'end_slice'},group_by_variables), ... , 'volume', 'vwap', 'bid', 'ask'}, ...
            'value', [data.date(seq_begins), data.date(seq_ends), uv_gbv(idx_in_uvgbv(seq_ends),:)],  ...
            ... acc_volume, acc_price, prices_ob{2}(seq_begins), prices_ob{1}(seq_begins)], ...
            'date', data.date(seq_ends), 'title', 'Aggregated liquidity consuming orders');
        
        grid = cell(length(data_out.date), 1);
        for i = 1 : length(grid)
            grid{i} = find(accum_idx == i);
        end
        
        % TODO check that assumed nothing thrown
        idx_dump = false(length(data.date), 1);
        
        
        kingpin_out = NaN;
    case 'time'
        if ischar(data) && strcmp(data, 'return_cols')
            data_out.cols_needed4grid = {'time_open', 'time_close'};
            data_out.cols_provided_by_make_grid = { 'begin_slice', 'end_slice'};
            return;
        end
        
        
        opt = options_light({'time_type', 'min', ... ['min']/'minutes'/'seconds'/'sec'/'hours'/'matlab' the unit used for step and width
            'user_specified_step_grid', [],...   %if the user wants to specify himself the grid and bypass its creation in mode 'time_grid' of the generate_grid function; if it is a 2 column matrix, first column is bound inf, and 2nd is bound sup
            'window', 15, ... % eponym unit = time_type
            'step', 15, ... % eponym unit = time_type
            'kingpin',  NaN ,... % this is the end of an slice which then set the end of all other slices according to step
            'bt_grid', NaN,... % aim at specifying the begining of the  time grid
            'et_grid', NaN,... % aim at specifying the end of the grid
            'exceed_begin', false,...
            'exceed_end', false,...
            'progressive', false,... % [false]/true: nijos mode : if true then the slices have growing sizes (with always the same start)
            'try2correct_auction_flag', 0, ... % [0]/numeric_matlab_time when non zero, for each unidentified auction deal (flagged auction but neither opening nor intraday, closing) we'll have a look at trading times and try to identify a potential specific auction to which it belongs. When non-zero, the value will be used as a tolerance offseting time to theretical fixing auction trading time. the two previous options should be set to false when this one is desirted to be active
            }, varargin);
        
        bt_grid = opt.bt_grid;
        et_grid = opt.et_grid;
        kingpin =  opt.kingpin;
        progressive = opt.progressive;
        exceed_begin = opt.exceed_begin;
        exceed_end = opt.exceed_end;
        ussg = opt.user_specified_step_grid;
        %< Just making sure ussg is a colum vector or a 2 column matrix
        if size(ussg,2) > size(ussg,1)
            ussg = ussg';
        end
        %>
        
        %< l'unit� de temps
        switch lower(opt.time_type)
            case 'hours'
                window = datenum(0,0,0,opt.window,0,0);
                step = datenum(0,0,0,opt.step,0,0);
            case {'minutes', 'min'}
                window = datenum(0,0,0,0,opt.window,0);
                step = datenum(0,0,0,0,opt.step,0);
            case {'seconds', 'sec'}
                window = datenum(0,0,0,0,0,opt.window);
                step = datenum(0,0,0,0,0,opt.step);
            case 'matlab'
                window = opt.window;
                step = opt.step;
            case {'day', 'week', 'month', 'year'}
                error('make_grid:check_args', 'Not yet implemented'); % TODO
            otherwise
                error('make_grid:check_args', 'Unknown time_type : <%s>', ...
                    opt.time_type);
        end
        %>
        
        idx_dump= true(length(data.date),1);
        
        time_open = st_data('col', data, 'time_open');
        time_close = st_data('col', data, 'time_close');
        
        %< Offsetting dates is necessary for reaggregation, TIME_EPS has
        %been chosen as small as possible
        data.date = data.date - TIME_EPS;
        %>
        
        day_ = floor(data.date(1));
        
        if size(ussg,2)==2
            accum_idx = NaN(length(data.date), 1);
            kingpin_out = NaN; %value of the kingpin used for continuous phase
            begin_slice = mod(ussg(:,1),1);
            end_slice =  mod(ussg(:,2),1);
            dts = mod(data.date,1);
            grid = generate_grid_from_bounds(dts,begin_slice, end_slice);
            
        else
            %< Normal grid construction
            %On extrait les donn�es qui sont comprises dans les horaires
            %fournis par l'utilisateur
            bt_grid_switch = isnull(bt_grid, min(mod(time_open,1)) ) - TIME_EPS;   % numerics comparisons are so painful!
            et_grid_switch = isnull(et_grid, max( max(mod(time_close,1)), max(mod(data.date,1)) ) ) + 2*TIME_EPS; % numerics comparisons are so painful!
            
            % TODO is this a good idea to have a variable kingpin rather than
            % the same by default
            [grid,accum_idx,begin_slice, end_slice, kingpin_out] = ...
                generate_grid('time_grid',data,window,step,...
                bt_grid_switch,et_grid_switch,kingpin,...
                exceed_begin,exceed_end,progressive,'not_used_in_mode_time_grid', ussg);
            %>
        end
        
        data_out = st_data('init', 'date', day_ + end_slice, ...
            'value', [begin_slice, end_slice], ...
            'colnames', {'begin_slice', 'end_slice'});
        
        
        idx_dump(cat(1,grid{:})) = false;
        
    case 'day'
        % TODO clean it, but first who wants what?
        % TODO Are you sure you don't want to use day_and_phase instead ?
        
        % robur 4 malas too quick too dirty
        if ischar(data) && strcmp(data, 'return_cols')
            data_out.cols_needed4grid = {};
            data_out.cols_provided_by_make_grid = {};
            return;
        end
        
        kingpin_out = NaN;
        idx_dump = [];
        if strtok(v, '.') > '7' %v(1)
            [d, ~, accum_idx] = unique(floor(data.date), 'legacy');
        else
            [d, ~, accum_idx] = unique(floor(data.date));
        end
        
        
        grid = cell(length(d), 1);
        for i = 1 : length(d)
            grid{i} = find(accum_idx == i);
        end
        
        data_out = st_data('init', 'value', NaN(size(d,1),0), 'colnames', {}, 'title', 'USE AT YOUR OWN RISK', 'date', d);
        
    case 'day_and_phase'
        %< This mode is supposed to create a daily grid, with
        %begin_slice and end_slice given by theoretical trading times
        %>
        
        % TODO clean it, but first who wants what?
        
        %< return cols submode
        if ischar(data) && strcmp(data, 'return_cols')
            data_out.cols_needed4grid = {'phase'};
            data_out.cols_provided_by_make_grid = {'begin_slice', 'end_slice','phase'};
            return;
        end
        %>
        
        kingpin_out = NaN;
        idx_dump = [];
        
        phase = st_data('col', data, 'phase');
        if strtok(v, '.') > '7' %v(1)
            [d_p, ~, accum_idx] = unique([floor(data.date), phase], 'rows', 'legacy');
        else
            [d_p, ~, accum_idx] = unique([floor(data.date), phase], 'rows');
        end
        
        
        grid = cell(size(d_p,1), 1);
        for i = 1 : length(d_p)
            grid{i} = find(accum_idx == i);
        end
        
        bs_es = NaN(size(d_p,1),2);
        current_d = d_p(1,1);
        phases_missing_in_data = NaN(0,4);
        [phase_th, phase_id, ~, from_phase_id_to_phase_name, extract_phase] = ...
            trading_time_interpret(data.info.td_info, d_p(i,1));
        
        for i = 1 : length(d_p)
            if current_d ~= d_p(i,1)
                [phase_th, phase_id, ~, from_phase_id_to_phase_name, ...
                    extract_phase, ~, phase_type, ~, th_event] = ...
                    trading_time_interpret(data.info.td_info, d_p(i,1));
                
                current_d = d_p(i,1);
                phases_this_day = dp(dp(:,1) == current_d, 2);
                
                missing_phase_ids_this_day = setdiff([phase_th.phase_id], phases_this_day);
                if ~isempty(missing_phase_ids_this_day) % if some day there are expected phases that do not exist in the data, we identify them in order to add them at the end
                    missing_phases_names = arrayfun(from_phase_id_to_phase_name, missing_phase_ids_this_day);
                    missing_phases_this_day = extract_phase(phase_th, missing_phases_names);
                    phases_missing_in_data = cat(1,phases_missing_in_data,...
                        [current_d * ones(length(missing_phase_ids_this_day),1),...
                        missing_phase_ids_this_day',...
                        [missing_phases_this_day.begin]',...
                        [missing_phases_this_day.end]'...
                        ]);
                end
                
            end
            this_phase_id = d_p(i,2);
            if this_phase_id == phase_id.VOL_FIXING     % VOL_FIXING HAVE NO THEORETICAL TIME SO bs_es is NaN
                continue;
            end
            this_phase = extract_phase(phase_th, from_phase_id_to_phase_name(this_phase_id));
            bs_es(i,:) = [this_phase.begin, this_phase.end];
        end
        
        %< we handle missing phases in the data to create empty lines in
        %the grid
        if ~isempty(phases_missing_in_data)
            grid = cat(1, grid, cell(size(phases_missing_in_data,1)));
            d_p = cat(1, d_p,  phases_missing_in_data(:,1:2));
            bs_es = cat(1, bs_es, phases_missing_in_data(:,3:4));
            
            [~, idx_sort] = sort(d_p(:,1));
            d_p = d_p(idx_sort,:);
            bs_es = bs_es(idx_sort,:);
            grid = grid(idx_sort);
        end
        %>
        
        data_out = st_data('init',...
            'value', [bs_es, d_p(:,2)],...
            'colnames', {'begin_slice', 'end_slice','phase'},...
            'title', 'USE AT YOUR OWN RISK',...
            'date', d_p(:,1));
        
    case 'time_and_phase'
        %< Sous mode permettant de renvoyer les colonnes n�cessaires �
        %l'application du mode 'time_and_phase' et les colonnes qui seront
        %pr�sentes dans data_out apr�s application
        
        % TODO % parse options in apply_plan to grid, the submode return
        % cols could be then used as a prefetch for parsing options in
        % order to avoid parsing options many times
        % these prefetched options could be then sent directly to the mode
        % 'time_and_phase'
        if ischar(data) && strcmp(data, 'return_cols')
            data_out.cols_needed4grid = {'phase'};
            data_out.cols_provided_by_make_grid = { 'begin_slice', 'end_slice','phase'};
            return;
        end
        
        
        accum_idx = NaN(length(data.date), 1);
        kingpin_out = NaN; %value of the kingpin used for continuous phase
        idx_dump= true(length(data.date),1);
        
        
        opt = options_light({'time_type', 'min', ... ['min']/'minutes'/'seconds'/'sec'/'hours'/'matlab' the unit used for step and width
            'user_specified_step_grid', [],...   %if the user wants to specify himself the grid and bypass its creation in mode 'time_grid' of the generate_grid function
            'user_specified_trading_times', {},...% used if the user wants to specify himself the trading hours to use, for example when working with multiple stocks, struct with fields : phase_th, phase_id, trading_hours_base_dump,from_phase_id_to_phase_name, extract_phase
            'window', 15, ... % eponym unit = time_type
            'step', 15, ... % eponym unit = time_type
            'kingpin',  NaN ,... % this is the end of an slice which then set the end of all other slices according to step
            ... % <
            'bt_grid', NaN,... % aim at specifying the begining of the  time grid
            'et_grid', NaN,... % aim at specifying the end of the grid
            ... % >
            ... %< For some window and special trading_times it is possible to have the first and last slice which should lie outside trading_times (or begin_time and end_time)
            ... % this options aims at throwing or not the data lying outside trading_times but theoretically inside the slice according to its width.
            ... % if false : the second effect will be that begin_slice (resp. end_slice) will be raised (resp. lowered) according to the time bounds
            'exceed_begin', false,...
            'exceed_end', false,...   % TODO better comment : this is supposed to be
            ... %>
            ... %< is really meaningful when set to false, then no data should be thrown away for timestamps reason.
            ... % TODO better comment and verify interaction with exceed_begin/exceed_end
            ... %  not even considered if  bt_grid or et_grid is not NaN
            'respect_trading_times', true,... % [true]/false: if true : then every data lying outside the trading times (repository..trading_hours_master) will be thrown
            ... %>
            'progressive', false,... % [false]/true: nijos mode : if true then the slices have growing sizes (with always the same start)
            'auction_slicing_mode', 'single_slice',... % ['single_slice']/'multi_slice': if single_slice then there will be only one slice for a kind of auction (except continuous and volatility_stop_auction). If multi_slice then if the timestamps of data flagged in this auction are not as close as 0.2*window (this MAGIC 0.2 being the variable SEP_FACTOR in generate_grid) then it will be separated into different slices (see generate_grid)
            'vol_stop_slice', false, ... [false]/true: if true then stop auctions (fixing that takes place to halt trading when the price has moved too much) will generate an extra slice (thus making the number of slices random) whereas if false these data will be aggregated to the continuous ones.
            'periodic_auction',false,... [false]/true: if true then periodic auctions will have their own slices (thus making the number of slices random). If false they will be aggregated to the continuous slices
            'unxpctd_as_cont', false,... [false]/true if true every unexpected phase (means an auction which is flagged as such in the data but which shouldnt exist according to the trading times in KGR..trading_hours_master) is considered as continuous
            ... % just a word about intraday auctions : there are intraday stops, and intraday_resumption. The former should generally be the end of the continuous auction in order to make a trading pause for lunch. The Latter should be the one which take place when the lunch pause is finished and aims at gathering liquidity for re-starting the continuous auction. The most usual (XETRA) is a resumption one, this is a case when there is no trading pause.
            'force_unique_intraday_auction', true, ... % [true]/false: For most of the uses, e.g. volume curves, only one intraday auction is handled in the system (when it is even handled) so this option enables to have a unique intraday auction which will be the aggregation of the two intraday auctions. Its begin and end will be the begining of the first one and the end of the second one.
            'convert_ifix2_to_ifix1', false, ... % [false]/true by default, all intraday auctions are in the ID_FIX_PHASE2 if this options is set to true, then we will try to identify any data coming from a ID_FIX_PHASE1 according to the timsestamps of the data. This options will
            'try2correct_auction_flag', 0, ... % [0]/numeric_matlab_time when non zero, for each unidentified auction deal (flagged auction but neither opening nor intraday, closing) we'll have a look at trading times and try to identify a potential specific auction to which it belongs. When non-zero, the value will be used as a tolerance offseting time to theretical fixing auction trading time. the two previous options should be set to false when this one is desirted to be active
            'context_selection', 'null', ... % ['null']/ 'contextualised' / 'span_all' : used for context_selection option of trading_time_interpret
            }, varargin);
        
        bt_grid = opt.bt_grid;
        et_grid = opt.et_grid ;
        kingpin =  opt.kingpin ;
        exceed_begin = opt.exceed_begin;
        exceed_end = opt.exceed_end;
        auction_slicing_mode = opt.auction_slicing_mode;
        respect_tt = opt.respect_trading_times;
        progressive = opt.progressive;
        fuia = opt.force_unique_intraday_auction;
        ci2ti1 = opt.convert_ifix2_to_ifix1;
        ustt = opt.user_specified_trading_times;
        ussg = opt.user_specified_step_grid;
        t2caf = opt.try2correct_auction_flag;
        
        if isempty(ustt)
           [phase_th, phase_id, ~, ~, extract_phase, ~, phase_type, ~, th_event] = ...
                trading_time_interpret(data.info.td_info, ...
                    floor(data.date(1)), 'force_unique_intraday_auction', fuia, ...
                    'context_selection', opt.context_selection);
        else
            %             tti_fieldnames = {'phase_th', 'phase_id', 'trading_hours_base_dump', ...
            %             'from_phase_id_to_phase_name', 'extract_phase'};
            phase_th = ustt.phase_th;
            phase_id = ustt.phase_id;
            %             trading_hours_base_dump = ustt.trading_hours_base_dump;
            %             from_phase_id_to_phase_name = ustt.from_phase_id_to_phase_name;
            extract_phase = ustt.extract_phase;
            %< TODO : 
            if t2caf > 0
                error('make_grid:check_args', 'Please implement something for phase_type');
            end
            %> phase_type ....
        end
        
        CONT_PHASE      = phase_id.CONTINUOUS;
        OPEN_FIX_PHASE  = phase_id.OPEN_FIXING;
        ID_FIX_PHASE1   = phase_id.ID_FIXING1;
        ID_FIX_PHASE2   = phase_id.ID_FIXING2;
        CLOSE_FIX_PHASE = phase_id.CLOSE_FIXING;
        VOL_FIX_PHASE   = phase_id.VOL_FIXING;
        PER_AUC_PHASE   = phase_id.PERIODIC_AUCTION;
        
        if fuia
            ci2ti1 = false; % forced not to differentiate intraday phases
        end
        
        %< Grid Is Based On Trading Times: gibott
        gibott = isfinite(bt_grid) || isfinite(et_grid) || respect_tt;
        %>
        
        %< l'unit� de temps
        switch lower(opt.time_type)
            case 'hours'
                window = datenum(0,0,0,opt.window,0,0);
                step = datenum(0,0,0,opt.step,0,0);
            case {'minutes', 'min'}
                window = datenum(0,0,0,0,opt.window,0);
                step = datenum(0,0,0,0,opt.step,0);
            case {'seconds', 'sec'}
                window = datenum(0,0,0,0,0,opt.window);
                step = datenum(0,0,0,0,0,opt.step);
            case 'matlab'
                window = opt.window;
                step = opt.step;
            otherwise
                error('make_grid:check_args', 'Unknown time_type : <%s>', ...
                    opt.time_type);
        end
        %>
        
        %< on regarde quelles phases sont pr�sentes et on corrige les flags
        %auctions si les options demandent de le faire
        phase_col = strmatch('phase', data.colnames, 'exact');
        uphase = unique(data.value(:, phase_col));
        if t2caf > 0
            if fuia || ci2ti1
                error('make_grid:chec_args', 'OPTIONS: Incompatible options value, when try2correct_auction_flag is used, then force_unique_intraday_auction and convert_ifix2_to_ifix1 sgould be set to false');
            end
            data = patch_all_fix_phase(data,phase_th, extract_phase, phase_id, t2caf, phase_type);
            uphase = unique(data.value(:, phase_col));
        elseif ci2ti1 && any(uphase == ID_FIX_PHASE2) %phase par d�faut attribu�e aux donn�es ayant intraday_auction==1 dans formula_factory
            % pour les intraday stop si l'on peut
            data = patch_id_fix_phase(data,phase_th, extract_phase, phase_id, TIME_EPS);
            uphase = unique(data.value(:, phase_col));
        end
        %>
        
        %< on regarde quelles phases sont effectivement requises compte tenu
        % des horaires sp�cifi�s
        if isfinite(bt_grid) || isfinite(et_grid)
            idx2del = ([phase_th.phase_id]~=CONT_PHASE) & ...
                ([phase_th.end]<= bt_grid + TIME_EPS | [phase_th.end]>et_grid - TIME_EPS);
            phase_th(idx2del) = [];
        end
        %>
        
        %< si les phases sont requises et qu'on est en gibott on rajoute les
        % phases vides qui devraient exister
        if gibott
            unxpctd_phases = setdiff(uphase, [phase_th.phase_id,VOL_FIX_PHASE]);
            if ~isempty(unxpctd_phases) && ~opt.unxpctd_as_cont
                st_log('make_grid:check_data WARN: There is an unexpected phase <%s> in your data\n', ...
                    sprintf('%d,', unxpctd_phases));
            end
            uphase = unique(cat(1, uphase, phase_th.phase_id));
        else
            exceed_end = true; % Cas non gibott: on force les valeurs d'exceed (see line 94 for gibott case)
            exceed_begin = true;
        end
        %>
        
        %< Si l'on souhaite agr�ger les fixing stop de volatilit� avec le
        %continu
        if ~opt.vol_stop_slice
            data.value(data.value(:, phase_col) == VOL_FIX_PHASE, phase_col) = CONT_PHASE;
            uphase(uphase == VOL_FIX_PHASE) = []; % on retire la phase de uphase
        end
        %>
        
        %< Periodic auction aggregation with continuous
        if ~opt.periodic_auction
            data.value(data.value(:, phase_col) == PER_AUC_PHASE, phase_col) = CONT_PHASE;
            uphase(uphase == PER_AUC_PHASE) = []; % on retire la phase de uphase
        end
        %>
        
        %< Si l'on souhaite agr�ger les phases inattendues avec le
        %continu
        if opt.unxpctd_as_cont
            for i= 1:length(unxpctd_phases)
                data.value(data.value(:, phase_col) == unxpctd_phases(i), phase_col) = CONT_PHASE;
                uphase(uphase == unxpctd_phases(i)) = []; % on retire la phase inattendue de uphase
            end
        end
        %>
        
        %< Offsetting dates is necessary for reaggregation, TIME_EPS has
        %been chosen as small as possible
        data.date = data.date - TIME_EPS;
        %>
        
        data_phases = cell(length(uphase),1);
        grid_phases = cell(length(uphase),1);
        accum_idx_phase = cell(length(uphase),1);
        
        day_ = floor(data.date(1));
        
        
        ph = data.value(:, phase_col);
        for i = 1 : length(uphase)
            idx = ph == uphase(i);
            data_phases{i} = data;
            data_phases{i}.value = data_phases{i}.value(idx, :);
            data_phases{i}.date = data_phases{i}.date(idx, :);
            
            
            switch uphase(i)
                case  CONT_PHASE
                    
                    if gibott
                        this_pth = phase_th([phase_th.phase_id] == uphase(i));
                        if isempty(this_pth) % so this is an unexpected_phase
                            bt_grid_switch = isnull(bt_grid, min(data_phases{i}.date));
                            et_grid_switch = isnull(et_grid, max(data_phases{i}.date));
                        else
                            bt_grid_switch = isnull(bt_grid,this_pth.begin);
                            et_grid_switch = isnull(et_grid, this_pth.end);
                        end
                    else
                        bt_grid_switch = min(mod(data_phases{i}.date,1));
                        et_grid_switch = max(mod(data_phases{i}.date,1));
                    end
                    
                    bt_grid_switch = bt_grid_switch - TIME_EPS; % numerics comparisons are so painful!
                    et_grid_switch = et_grid_switch + 2 * TIME_EPS;% numerics comparisons are so painful!
                    
                    [grid,accum_idx_phase{i},begin_slice, end_slice, kingpin_out] = ...
                        generate_grid('time_grid',data_phases{i},window,step,...
                        bt_grid_switch,et_grid_switch,kingpin,...
                        exceed_begin,exceed_end,progressive,'not_used_in_mode_time_grid', ussg);
                    
                    
                case {VOL_FIX_PHASE, PER_AUC_PHASE}
                    [grid,accum_idx_phase{i},begin_slice, end_slice] = ...
                        generate_grid('fuzzy_grid',data_phases{i},...
                        window,step,NaN,NaN,NaN,...
                        exceed_begin,exceed_end,progressive,'multi_slice');
                    
                case {OPEN_FIX_PHASE,ID_FIX_PHASE1,ID_FIX_PHASE2,CLOSE_FIX_PHASE}
                    
                    if gibott
                        this_pth = phase_th([phase_th.phase_id] == uphase(i));
                        if isempty(this_pth) % so this is an unexpected_phase
                            bt_grid_switch = isnull(bt_grid, min(data_phases{i}.date));
                            et_grid_switch = isnull(et_grid, max(data_phases{i}.date));
                        else
                            bt_grid_switch = max(bt_grid,this_pth.begin);%isnull(bt_grid,this_pth.begin);
                            et_grid_switch = min(et_grid, this_pth.end);%isnull(et_grid, this_pth.end);
                        end
                    else
                        bt_grid_switch = min(mod(data_phases{i}.date,1));
                        et_grid_switch = max(mod(data_phases{i}.date,1));
                    end
                    
                    [grid,accum_idx_phase{i},begin_slice, end_slice] = ...
                        generate_grid('fuzzy_grid',data_phases{i},...
                        window,step,bt_grid_switch,et_grid_switch,NaN,...
                        exceed_begin,exceed_end,progressive,auction_slicing_mode);
                    
                otherwise
                    error('make_grid:exec', 'This phase is not handled : <%d>', uphase(i));
            end
            
            
            data_phases{i}.date = day_ + end_slice;
            data_phases{i}.value = [begin_slice, end_slice, repmat(uphase(i), size(grid, 1), 1)];
            data_phases{i}.colnames = {'begin_slice', 'end_slice', 'phase'};
            
            grid_phases{i} = cell(size(grid, 1), 1);
            
            origin_idx = find(idx);
            
            for j = 1 : size(grid,1)
                grid_phases{i}{j} = origin_idx(grid{j});
                if isempty(grid_phases{i}{j})
                    grid_phases{i}{j} = [];
                end
            end
            
            if ~isempty(origin_idx)
                accum_idx(origin_idx) = numel(cat(1,grid_phases{1:i-1}))+accum_idx_phase{i};
            end
            
        end
        grid = cat(1, grid_phases{:});
        data_out = st_data('fast-stack', data_phases);
        [~, idx] = sort(data_out.date);
        data_out.value = data_out.value(idx, :);
        data_out.date = data_out.date(idx, :);
        data_out.info.phase_th = phase_th;
        data_out.info.th_event = th_event;
        grid = grid(idx);
        
        %< TODO : use accum_idx_phases ?
        if all(isfinite(accum_idx))
            for i = 1 : size(grid, 1)
                accum_idx(grid{i}) = i;
            end
        end
        %>
        
        idx_dump(cat(1,grid{:})) = false;
        
        
        
        
    case 'volumic_time_and_phase'
        
        %< Sous mode permettant de renvoyer les colonnes n�cessaires �
        %l'application du mode 'volumic_time_and_phase' et les colonnes qui seront
        %pr�sentes dans data_out apr�s application
        
        % TODO % parse options in apply_plan to grid, the submode return
        % cols could be then used as a prefetch for parsing options in
        % order to avoid parsing options many times
        % these prefetched options could be then sent directly to the mode
        % 'time_and_phase'
        if ischar(data) && strcmp(data, 'return_cols')
            data_out.cols_needed4grid = {'phase','volume'};
            data_out.cols_provided_by_make_grid = { 'begin_slice', 'end_slice','phase'};
            return;
        end
        
        
        accum_idx = NaN(length(data.date), 1);
        kingpin_out = NaN; %value of the kingpin used for continuous phase
        idx_dump= true(length(data.date),1);
        
        
        opt = options_light({'time_type', 'min', ... ['min']/'minutes'/'seconds'/'sec'/'hours'/'matlab' the unit used for step and width
            'user_specified_step_grid', [],...   %if the user wants to specify himself the grid and bypass its creation in mode 'time_grid' of the generate_grid function
            'user_specified_trading_times', {},...% used if the user wants to specify himself the trading hours to use, for example when working with multiple stocks, struct with fields : phase_th, phase_id, trading_hours_base_dump,from_phase_id_to_phase_name, extract_phase
            'window', 15, ... % eponym unit = time_type
            'step', 15, ... % eponym unit = time_type
            'kingpin',  NaN ,... % this is the end of an slice which then set the end of all other slices according to step
            ... % <
            'bt_grid', NaN,... % aim at specifying the begining of the  time grid
            'et_grid', NaN,... % aim at specifying the end of the grid
            ... % >
            ... %< For some window and special trading_times it is possible to have the first and last slice which should lie outside trading_times (or begin_time and end_time)
            ... % this options aims at throwing or not the data lying outside trading_times but theoretically inside the slice according to its width.
            ... % if false : the second effect will be that begin_slice (resp. end_slice) will be raised (resp. lowered) according to the time bounds
            'exceed_begin', false,...
            'exceed_end', false,...   % TODO better comment : this is supposed to be
            ... %>
            ... %< is really meaningful when set to false, then no data should be thrown away for timestamps reason.
            ... % TODO better comment and verify interaction with exceed_begin/exceed_end
            ... %  not even considered if  bt_grid or et_grid is not NaN
            'respect_trading_times', true,... % [true]/false: if true : then every data lying outside the trading times (repository..trading_hours_master) will be thrown
            ... %>
            'progressive', false,... % [false]/true: nijos mode : if true then the slices have growing sizes (with always the same start)
            'auction_slicing_mode', 'single_slice',... % ['single_slice']/'multi_slice': if single_slice then there will be only one slice for a kind of auction (except continuous and volatility_stop_auction). If multi_slice then if the timestamps of data flagged in this auction are not as close as 0.2*window (this MAGIC 0.2 being the variable SEP_FACTOR in generate_grid) then it will be separated into different slices (see generate_grid)
            'vol_stop_slice', false, ... % [false]/true: if true then stop auctions (fixing that takes place to halt trading when the price has moved too much) will generate an extra slice (thus making the number of slices random) whereas if false these data will be aggregated to the continuous ones. One important thing is that any auction who is not flagged as opening intraday or closing will be considered as a vol stop auction.
            'unxpctd_as_cont', false,...    % [false]/true if true every unexpected phase (means an auction which is flagged as such in the data but which shouldnt exist according to the trading times in repository..trading_hours_master) is considered as continuous
            ... % just a word about intraday auctions : there are intraday stops, and intraday_resumption. The former should generally be the end of the continuous auction in order to make a trading pause for lunch. The Latter should be the one which take place when the lunch pause is finished and aims at gathering liquidity for re-starting the continuous auction. The most usual (XETRA) is a resumption one, this is a case when there is no trading pause.
            'force_unique_intraday_auction', true, ... % [true]/false: For most of the uses, e.g. volume curves, only one intraday auction is handled in the system (when it is even handled) so this option enables to have a unique intraday auction which will be the aggregation of the two intraday auctions. Its begin and end will be the begining of the first one and the end of the second one.
            'convert_ifix2_to_ifix1', false, ... % [false]/true by default, all intraday auctions are in the ID_FIX_PHASE2 if this options is set to true, then we will try to identify any data coming from a ID_FIX_PHASE1 according to the timsestamps of the data. This options will
            }, varargin);
        
        bt_grid = opt.bt_grid;
        et_grid = opt.et_grid ;
        kingpin =  opt.kingpin ;
        exceed_begin = opt.exceed_begin;
        exceed_end = opt.exceed_end;
        auction_slicing_mode = opt.auction_slicing_mode;
        respect_tt = opt.respect_trading_times;
        progressive = opt.progressive;
        fuia = opt.force_unique_intraday_auction;
        ci2ti1 = opt.convert_ifix2_to_ifix1;
        ustt = opt.user_specified_trading_times;
        ussg = opt.user_specified_step_grid;
        
        if isempty(ustt)
            [phase_th, phase_id, ~, ~, extract_phase, ~, phase_type, ~, th_event] = ...
                trading_time_interpret(data.info.td_info, ...
                floor(data.date(1)), 'force_unique_intraday_auction', fuia);
        else
            %             tti_fieldnames = {'phase_th', 'phase_id', 'trading_hours_base_dump', ...
            %             'from_phase_id_to_phase_name', 'extract_phase'};
            phase_th = ustt.phase_th;
            phase_id = ustt.phase_id;
            %             trading_hours_base_dump = ustt.trading_hours_base_dump;
            %             from_phase_id_to_phase_name = ustt.from_phase_id_to_phase_name;
            extract_phase = ustt.extract_phase;
        end
        
        CONT_PHASE      = phase_id.CONTINUOUS;
        OPEN_FIX_PHASE  = phase_id.OPEN_FIXING;
        ID_FIX_PHASE1   = phase_id.ID_FIXING1;
        ID_FIX_PHASE2   = phase_id.ID_FIXING2;
        CLOSE_FIX_PHASE = phase_id.CLOSE_FIXING;
        VOL_FIX_PHASE   = phase_id.VOL_FIXING;
        
        if fuia
            ci2ti1 = false; % forced not to differentiate intraday phases
        end
        
        
        %< Grid Is Based On Trading Times: gibott
        gibott = isfinite(bt_grid) || isfinite(et_grid) || respect_tt;
        %>
        
        %< l'unit� de temps
        switch lower(opt.time_type)
            case 'volume'
                window = opt.window;
                step = opt.step;
            otherwise
                error('make_grid:check_args', 'Unknown time_type : <%s>', ...
                    opt.time_type);
        end
        %>
        
        %< on regarde quelles phases sont pr�sentes et on corrige le flag
        % pour les intraday stop si l'on peut
        phase_col = strmatch('phase', data.colnames, 'exact');
        uphase = unique(data.value(:, phase_col));
        if any(uphase == ID_FIX_PHASE2) %phase par d�faut attribu�e aux donn�es ayant intraday_auction==1 dans formula_factory
            if ci2ti1
                data = patch_id_fix_phase(data,phase_th, extract_phase, phase_id, TIME_EPS);
            end
            uphase = unique(data.value(:, phase_col));
        end
        %>
        
        %< on regarde quelles phases sont effectivement requises compte tenu
        % des horaires sp�cifi�s
        if isfinite(bt_grid) || isfinite(et_grid)
            idx2del = ([phase_th.phase_id]~=CONT_PHASE) & ...
                ([phase_th.end]<= bt_grid + TIME_EPS | [phase_th.end]>et_grid - TIME_EPS);
            phase_th(idx2del) = [];
        end
        %>
        
        %< si les phases sont requises et qu'on est en gibott on rajoute les
        % phases vides qui devraient exister
        if gibott
            unxpctd_phases = setdiff(uphase, [phase_th.phase_id,VOL_FIX_PHASE]);
            if ~isempty(unxpctd_phases) && ~opt.unxpctd_as_cont
                st_log('make_grid:check_data WARN: There is an unexpected phase <%s> in your data\n', ...
                    sprintf('%d,', unxpctd_phases));
            end
            uphase = unique(cat(1, uphase, phase_th.phase_id));
        else
            exceed_end = true; % Cas non gibott: on force les valeurs d'exceed (see line 94 for gibott case)
            exceed_begin = true;
        end
        %>
        
        %< Si l'on souhaite agr�ger les fixing stop de volatilit� avec le
        %continu
        if ~opt.vol_stop_slice
            data.value(data.value(:, phase_col) == VOL_FIX_PHASE, phase_col) = CONT_PHASE;
            uphase(uphase == VOL_FIX_PHASE) = []; % on retire la phase de uphase
        end
        %>
        
        
        %< Si l'on souhaite agr�ger les phases inattendues avec le
        %continu
        if opt.unxpctd_as_cont
            for i= 1:length(unxpctd_phases)
                data.value(data.value(:, phase_col) == unxpctd_phases(i), phase_col) = CONT_PHASE;
                uphase(uphase == unxpctd_phases(i)) = []; % on retire la phase inattendue de uphase
            end
        end
        %>
        
        %< Offsetting dates is necessary for reaggregation, TIME_EPS has
        %been chosen as small as possible
        data.date = data.date - TIME_EPS;
        %>
        
        data_phases = cell(length(uphase),1);
        grid_phases = cell(length(uphase),1);
        accum_idx_phase = cell(length(uphase),1);
        
        day_ = floor(data.date(1));
        
        
        ph = data.value(:, phase_col);
        for i = 1 : length(uphase)
            idx = ph == uphase(i);
            data_phases{i} = data;
            data_phases{i}.value = data_phases{i}.value(idx, :);
            data_phases{i}.date = data_phases{i}.date(idx, :);
            
            
            switch uphase(i)
                case  CONT_PHASE
                    
                    % %                     if gibott
                    % %                         this_pth = phase_th([phase_th.phase_id] == uphase(i));
                    % %                         if isempty(this_pth) % so this is an unexpected_phase
                    % %                             bt_grid_switch = isnull(bt_grid, min(data_phases{i}.date));
                    % %                             et_grid_switch = isnull(et_grid, max(data_phases{i}.date));
                    % %                         else
                    % %                             bt_grid_switch = isnull(bt_grid,this_pth.begin);
                    % %                             et_grid_switch = isnull(et_grid, this_pth.end);
                    % %                         end
                    % %                     else
                    % %                         bt_grid_switch = min(mod(data_phases{i}.date,1));
                    % %                         et_grid_switch = max(mod(data_phases{i}.date,1));
                    % %                     end
                    % %
                    % %                     bt_grid_switch = bt_grid_switch - TIME_EPS; % numerics comparisons are so painful!
                    % %                     et_grid_switch = et_grid_switch + 2 * TIME_EPS;% numerics comparisons are so painful!
                    
                    [grid,accum_idx_phase{i},begin_slice, end_slice, kingpin_out] = ...
                        generate_specific_grid('basic_forward',data_phases{i},window,step,'volume','volume',progressive);
                    
                case VOL_FIX_PHASE
                    [grid,accum_idx_phase{i},begin_slice, end_slice] = ...
                        generate_grid('fuzzy_grid',data_phases{i},...
                        window,step,NaN,NaN,NaN,...
                        exceed_begin,exceed_end,progressive,'multi_slice');
                    
                case {OPEN_FIX_PHASE,ID_FIX_PHASE1,ID_FIX_PHASE2,CLOSE_FIX_PHASE}
                    
                    if gibott
                        this_pth = phase_th([phase_th.phase_id] == uphase(i));
                        if isempty(this_pth) % so this is an unexpected_phase
                            bt_grid_switch = isnull(bt_grid, min(data_phases{i}.date));
                            et_grid_switch = isnull(et_grid, max(data_phases{i}.date));
                        else
                            bt_grid_switch = max(bt_grid,this_pth.begin);%isnull(bt_grid,this_pth.begin);
                            et_grid_switch = min(et_grid, this_pth.end);%isnull(et_grid, this_pth.end);
                        end
                    else
                        bt_grid_switch = min(mod(data_phases{i}.date,1));
                        et_grid_switch = max(mod(data_phases{i}.date,1));
                    end
                    
                    [grid,accum_idx_phase{i},begin_slice, end_slice] = ...
                        generate_grid('fuzzy_grid',data_phases{i},...
                        window,step,bt_grid_switch,et_grid_switch,NaN,...
                        exceed_begin,exceed_end,progressive,auction_slicing_mode);
                    
                otherwise
                    error('make_grid:exec', 'This phase is not handled : <%s>', uphase(i));
            end
            
            
            data_phases{i}.date = day_ + end_slice;
            data_phases{i}.value = [begin_slice, end_slice, repmat(uphase(i), size(grid, 1), 1)];
            data_phases{i}.colnames = {'begin_slice', 'end_slice', 'phase'};
            
            grid_phases{i} = cell(size(grid, 1), 1);
            
            origin_idx = find(idx);
            
            for j = 1 : size(grid,1)
                grid_phases{i}{j} = origin_idx(grid{j});
                if isempty(grid_phases{i}{j})
                    grid_phases{i}{j} = [];
                end
            end
            
            if ~isempty(origin_idx)
                accum_idx(origin_idx) = numel(cat(1,grid_phases{1:i-1}))+accum_idx_phase{i};
            end
            
        end
        grid = cat(1, grid_phases{:});
        data_out = st_data('fast-stack', data_phases);
        [~, idx] = sort(data_out.date);
        data_out.value = data_out.value(idx, :);
        data_out.date = data_out.date(idx, :);
        data_out.info.phase_th = phase_th;
        data_out.info.th_event = th_event;
        grid = grid(idx);
        
        %< TODO : use accum_idx_phases ?
        if all(isfinite(accum_idx))
            for i = 1 : size(grid, 1)
                accum_idx(grid{i}) = i;
            end
        end
        %>
        
        idx_dump(cat(1,grid{:})) = false;
        
        
        
        
        
        
    case 'variable_based_time'
        
        opt = options_light({'step_time_type', 'volume', ... ['volume']/'min'/'nb_trades' the unit used for step
            'window_time_type', 'volume', ...['volume']/'min'/'nb_trades' the unit used for width
            'bin_construction_mode', 'basic_backward' ,...  %['basic_forward']/ 'basic_backward'/'basic_both' %if the user wants to specify himself the grid and bypass its creation in mode 'time_grid' of the generate_grid function
            'window', 15, ... % eponym unit = time_type, in basic_both this can be a 2x1 array, respectively backward_window and forward_window
            'step', 15, ... % eponym unit = time_type,
            'kingpin',  NaN ,... % this is the end of an slice which then set the end of all other slices according to step
            ...%            'bt_grid', NaN,... % aim at specifying the begining of the  time grid (matlab time)
            ...%            'et_grid', NaN,... % aim at specifying the end of the grid (matlab time)
            ...%             'exceed_begin', false,...
            ...%             'exceed_end', false,...
            'progressive', false,... % [false]/true: nijos mode : if true then the slices have growing sizes (with always the same start)
            }, varargin);
        
        %         bt_grid = opt.bt_grid;
        %         et_grid = opt.et_grid;
        kingpin =  opt.kingpin;
        progressive = opt.progressive;
        %         exceed_begin = opt.exceed_begin;
        %         exceed_end = opt.exceed_end;
        bcm = opt.bin_construction_mode;
        
        colname_vals_window = opt.window_time_type;
        colname_vals_step = opt.step_time_type;
        
        if ischar(data) && strcmp(data, 'return_cols')
            data_out.cols_needed4grid = unique({ colname_vals_window, colname_vals_step}); %'time_open', 'time_close',
            data_out.cols_provided_by_make_grid = { 'begin_slice', 'end_slice'};
            return;
        end
        
        
        %< l'unit� de grille
        switch lower(colname_vals_step)
            case {'volume','nb_trades'}
                step = opt.step;
                
            otherwise
                error('make_grid:check_args', 'Unknown time_type for step : <%s>', ...
                    opt.time_type);
        end
        switch lower(colname_vals_window)
            case {'volume','nb_trades'}
                window = opt.window;
                
            otherwise
                error('make_grid:check_args', 'Unknown time_type for window : <%s>', ...
                    opt.time_type);
        end
        %>
        
        idx_dump= true(length(data.date),1);
        
        %         time_open = st_data('col', data, 'time_open');
        %         time_close = st_data('col', data, 'time_close');
        
        %< Offsetting dates is necessary for reaggregation, TIME_EPS has
        %been chosen as small as possible
        data.date = data.date - TIME_EPS;
        %>
        
        day_ = floor(data.date(1));
        
        %         %On extrait les donn�es qui sont comprises dans les horaires
        %         %fournis par l'utilisateur
        %         bt_grid_switch = isnull(bt_grid, min(mod(time_open,1)) ) - TIME_EPS;   % numerics comparisons are so painful!
        %         et_grid_switch = isnull(et_grid, max(mod(time_close,1)) ) + 2*TIME_EPS; % numerics comparisons are so painful!
        
        
        [grid,accum_idx,begin_slice, end_slice ,kingpin_out,mid_slice] = ...
            generate_specific_grid(bcm,data,window,step,...
            colname_vals_window, colname_vals_step,progressive);
        
        
        
        switch lower(bcm)
            case 'basic_forward'
                bin_timestamp = begin_slice;
            case 'basic_backward'
                bin_timestamp = end_slice;
            case 'basic_both'
                bin_timestamp = mid_slice;
        end
        
        data_out = st_data('init', 'date', day_ + bin_timestamp, ...
            'value', [begin_slice, end_slice], ...
            'colnames', {'begin_slice', 'end_slice'});
        
        
        idx_dump(cat(1,grid{:})) = false;
        
        
        
        case 'trend_by_trend'
        
        opt = options_light({'all_trend_info', [], ... 
            }, varargin);
        
        all_trend_info = opt.all_trend_info;
        
        if isempty(all_trend_info)||...
                ~all(ismember({'trend_time_start', 'trend_time_end'}, all_trend_info.colnames))
            error('make_grid:trend_by_trend', 'incorrect trend info - I need trend_time_start and trend_time_end');
        end
        
        if ischar(data) && strcmp(data, 'return_cols')
            data_out.cols_needed4grid = {};
            data_out.cols_provided_by_make_grid = cat(2,{ 'begin_slice', 'end_slice' },all_trend_info.colnames);
            return;
        end
        

        
        accum_idx = NaN(length(data.date), 1);
        kingpin_out = NaN; %value of the kingpin used for continuous phase
        idx_dump= true(length(data.date),1);
        
        day_ = floor(data.date(1));
        dts = mod(data.date,1);
        bound_inf = mod(st_data('col', all_trend_info, 'trend_time_start'),1);
        bound_sup = mod(st_data('col', all_trend_info, 'trend_time_end'),1);
        
        grid = generate_grid_from_bounds(dts,bound_inf, bound_sup);
        
        data_out = st_data('init', 'date', day_ + bound_sup, ...
            'value', [bound_inf, bound_sup, all_trend_info.value], ...
            'colnames', cat(2,{'begin_slice', 'end_slice'}, all_trend_info.colnames));
        
        
        idx_dump(cat(1,grid{:})) = false;
        
    otherwise
        error('make_grid:mode', 'MODE: unknown mode <%s>', mode);
end

if isfield(data_out, 'rownames')
    data_out = rmfield(data_out, 'rownames');
    st_log('WARNING:make_grid, \n input data had rownames, I''ve just dropped them :o)\n');
end

if any(idx_dump)
    st_log(sprintf('WARNING:make_grid, \n \t%d idx have been dumped on %s !\n', sum(idx_dump), datestr(floor(data.date(1)), 'dd/mm/yyyy')));
end

end

%< Fonction sordide qui corrige la phase d'intraday fixing
% en s'aidant des horaires de trading : la valeur par d�faut ID_FIX_PHASE2
% est chang�e dans le st_data data en ID_FIX_PHASE1 si les donn�es flagg�es
% ID_FIX_PHASE2 ont des dates qui correspondent aux horaires de
% ID_FIX_PHASE1
% here id means intraday
%
function data = patch_id_fix_phase(data,phase_th,extract_phase,phase_id,TIME_EPS)
phases_id1and2 = extract_phase(phase_th, {'ID_FIXING1'});
phase_col = strmatch('phase', data.colnames, 'exact');

idx = data.value(:, phase_col) == phase_id.ID_FIXING2;

idx = idx & ( mod(data.date,1) >= phases_id1and2(1).begin -TIME_EPS )...
    & ( mod(data.date,1) <= phases_id1and2(1).end +TIME_EPS );
data.value(idx,phase_col) = phases_id1and2(1).phase_id;
end
%>

%< Fonction encore plus sordide qui essaie d'attribuer les deals flagu�s
% auction (mais ni opening, ni intraday, ni closing) � l'une des phases de fixing 
function data = patch_all_fix_phase(data,phase_th,extract_phase,phase_id,TIME_EPS, phase_type)
phase_col = strmatch('phase', data.colnames, 'exact');
ind = find(data.value(:, phase_col) == phase_id.VOL_FIXING);
fixing_phases = phase_th([phase_th.phase_type_id] == phase_type.FIXING);
for i = 1 : length(ind)
    [dist2phase pot_fixing_phases_idx] = min(abs([fixing_phases.end]-mod(data.date(ind(i)), 1)));
    if dist2phase < TIME_EPS
        data.value(ind(i),phase_col) = fixing_phases(pot_fixing_phases_idx).phase_id;
    end
end
end
%>

%<
function [seq_begins, seq_ends] = compute_begin_ends4liquidity_consumation(data, price_eps, time_eps)

price  = round(st_data('col', data, 'price') / price_eps) * price_eps;
%         volume = st_data('col', data, 'volume');
sizes = {st_data('col', data, 'ask_size');st_data('col', data, 'bid_size')};
prices_ob = {round(st_data('col', data, 'ask') / price_eps) * price_eps;...
    round(st_data('col', data, 'bid') / price_eps) * price_eps};
secs    = data.date;

seq_begins =  ... logical(diff(sells)) ... % ou bien on change de c�t� du carnet % TODO????
    (diff(secs) > time_eps)... % Ou bien l'horodatage permet de conclure que ce n'est pas le m�me ordre agressif
    | any(diff([sizes{1}, sizes{2}, prices_ob{1}, prices_ob{2}]), 2); % Ou bien le carnet d'ordre a �t� updat� entre les deux transactions, ce qui n'est pas le cas s'il s'agit du m�me ordre march�
seq_begins = [true; seq_begins];

diff_p  = [0;diff(price)];

for i = 1 : length(seq_begins)
    % curr_moving_side sera 1 si le prix est en train de monter, -1
    % s'il descend, 0 si on ne sait pas encore
    if seq_begins(i)
        curr_moving_side = 0;
    else
        if curr_moving_side == 1 && diff_p(i) <= -price_eps  % le prix montait (ou stagnait mais a mont� dans la s�quence) et maintenant il baisse
            curr_moving_side = -1;
            seq_begins(i) = 1;
        elseif curr_moving_side == -1 && diff_p(i) >= price_eps % le prix baissait (ou stagnait mais a baiss� dans la s�quence) et maintenant il monte
            curr_moving_side = 1;
            seq_begins(i) = 1;
        elseif curr_moving_side == 0 % on ne connaissait pas encore le sens de la s�quence et on en est d�j� au deuxi�me �l�ment de la s�quence
            if diff_p(i) >= price_eps
                curr_moving_side = 1;
            elseif diff_p(i) <= -price_eps
                curr_moving_side = -1;
            end
        end
    end
end

%         acc_volume = accumarray(accum_idx, volume);
%         acc_price  = accumarray(accum_idx, volume.*price) ./ acc_volume;
seq_ends = false(size(seq_begins));
seq_ends([find(seq_begins(2:end));length(seq_begins)]) = true;
end

%>


%< function to convert trend detection output into a grid
function grid = generate_grid_from_bounds(dts,bound_inf, bound_sup)

SEC_DT = 1/(24*3600);
FLOAT_EPS = 0.25/(24*3600);

bound_sup = round(bound_sup/SEC_DT)*SEC_DT;
bound_inf = round(bound_inf/SEC_DT)*SEC_DT;

bound_sup(end) = bound_sup(end) + FLOAT_EPS; % numeric comparisons are just painful
bound_inf(1)   = bound_inf(1) - FLOAT_EPS;% numeric comparisons are just painful

nb_bins = length(bound_sup);

nb = length(dts) + 1;

idx_first = nb*ones(nb_bins, 1);
idx_last  = zeros(nb_bins, 1);


for i = 1 : nb_bins
    idx = (dts >= bound_inf(i) & dts < bound_sup(i));
    tmp_idx_first = find(idx, 1, 'first');
    if ~isempty(tmp_idx_first)
        idx_first(i) = tmp_idx_first;
        idx_last(i)  = find(idx, 1, 'last');
    end
end


% bound_sup(end) = bound_sup(end) - FLOAT_EPS; % numeric comparisons are just painful
% bound_inf(1)   = bound_inf(1) + FLOAT_EPS;% numeric comparisons are just painful

grid = cell(nb_bins,1);

for j = 1 : size(grid,1)
    grid{j} = (idx_first(j):idx_last(j))';
    if isempty(grid{j})  % pour contr�ler la dimension des intervalles vides pour eviter de planter les cat dans les autres codes
        grid{j} = [];
    end
end

end
%>
