function path_describer(complete_path, formulas, slicers, ...
    POST_FORMULA_COST_SAVING_FACTOR, ind_formulas_apriori,  ind_slicers,...
    ind_formulas_aposteriori, cost)
if isempty(complete_path)
    complete_path = struct('cost', cost, ...
        'preformula', {ind_formulas_apriori}, ...
        's_indices', {{ind_slicers}}, ...
        'postformula', {ind_formulas_aposteriori});
end
    for k = 1 : length(complete_path.cost)
        st_log('************************************************\n');
        st_log('********* Path number : %d | cost = %g ********\n', k, complete_path.cost(k));
        st_log('************************************************\n');
        st_log('----------------------------------------------\n');
        st_log('------        PRE-FORMULAS    ----------------\n');
        st_log('----------------------------------------------\n');
        if ~isempty(complete_path.preformula)
            c = dep_describe(complete_path.preformula{k}, formulas, 1);
        end
        if ~isempty(slicers)
            st_log('PRE-FORMULAS cost = <%g>\n', c);
            st_log('----------------------------------------------\n');
            st_log('------        SLICERS         ----------------\n');
            st_log('----------------------------------------------\n');
            c = dep_describe(complete_path.s_indices{k}, slicers, 1);
            st_log('SLICERS cost = <%g>\n', c);
            st_log('----------------------------------------------\n');
            st_log('------        POST-FORMULAS   ----------------\n');
            st_log('----------------------------------------------\n');
            if ~isempty(complete_path.postformula)
                c = dep_describe(complete_path.postformula{k}, formulas, ...
                    POST_FORMULA_COST_SAVING_FACTOR);
            end
            st_log('POST-FORMULAS cost = <%g>\n', c);
        end
        st_log('************************************************\n\n\n');
    end
end

function cost = dep_describe(dep_list, dependencies, COST_SAVING_FACTOR)
cost = 0;
if iscell(dep_list)
    for i = 1 : length(dep_list)
        st_log('----------------------------------------------\n');
        st_log('NEW STEP with a group of independant formulas \n');
        for j = 1 : length(dep_list{i})
            cost = cost + my_sprintf(dependencies, dep_list{i}(j), COST_SAVING_FACTOR);
        end
    end
else
    for j = 1 : length(dep_list)
        cost = cost + my_sprintf(dependencies, dep_list(j), COST_SAVING_FACTOR);
    end
end
end

function cost = my_sprintf(dep, idx, COST_SAVING_FACTOR)
cost = dep(idx).cost * COST_SAVING_FACTOR;
tmp = dep(idx).in_cols;
st_log('Producing <%s> for a cost of <%d> thanks to <%s>\n', ...
    dep(idx).out_col, cost, ...
    sprintf('%s/',tmp{:}));
end