function dot_text = path_dotifier(c_pre, c_s, c_post)

c_input = input_formater(c_pre, c_s);

subgraph_input = create_subgraph('INPUT_COLS', c_input, 1);

subgraph_pre = create_subgraph('PRE_FORMULAS', c_pre, 1);

subgraph_s = create_subgraph('SLICERS', c_s, 2);

subgraph_post = create_subgraph('POST_FORMULAS', c_post, 3);

dot_text = [subgraph_input subgraph_pre subgraph_s subgraph_post];

edges_pre = create_edges(c_pre, 1, 1, 'blue');
edges_s = create_edges(c_s, 1, 2, 'red');
edges_post = create_edges(c_post, 2, 3, 'blue');


dot_text = [dot_text edges_pre edges_s edges_post];

dot_text = ['digraph "computation_plan" {\n' dot_text '}\n'];
end

function c_input = input_formater(c_pre, c_s)
    all_in = {};
    all_out = {};
    for s=1:length(c_pre)
       step = cat(1,c_pre{s}{:});
       all_in = cat(2,all_in, step{:,1});
       all_out = cat(2,all_out, step{:,2});
    end
    for s=1:length(c_s)
       step = cat(1,c_s{s}{:});
       all_in = cat(2,all_in, step{:,1});
    end
    all_in = unique(all_in);
    all_out = unique(all_out);
    inputs = setdiff(all_in, all_out);
    c_input = cell(1,1);
    for i=1:length(inputs)
        c_input{1}{i} =  {{}, inputs{i}, 0};
    end
end

function subgraph_txt = create_subgraph(name, meta_step, meta_step_id)
    nb_steps = length(meta_step);
    subgraph_txt = sprintf('subgraph "%s"{\\nnode [shape=box];\\n',name);
    for s = 1:nb_steps
        step = cat(1,meta_step{s}{:});
        node_labels = {step{:,2}};
        step_str = '{rank = same; ';
        for i = 1:length(node_labels)
            step_str = [step_str sprintf('%s%d [label="%s"]; ', node_labels{i}, meta_step_id, node_labels{i})];
        end
        step_str = [step_str '}\n'];
        subgraph_txt = [subgraph_txt step_str];
    end
    subgraph_txt = [subgraph_txt '}\n\n'];
end


function edges_txt = create_edges(meta_step, in_id, out_id, edge_color)
    nb_steps = length(meta_step);
    edges_txt = '';
    for s = 1:nb_steps
        step = cat(1,meta_step{s}{:});
        for i = 1:size(step,1)
            for j = 1:length(step{i,1})
            edges_txt = [edges_txt sprintf('%s%d -> %s%d [color=%s];\\n', step{i,1}{j}, in_id,step{i,2},out_id, edge_color)];
            end
        end
        edges_txt = [edges_txt '\n'];
    end
    edges_txt = [edges_txt '\n\n'];
end