function b = slicer_boolean_all_or_nan(bool_flag)
b = all(bool_flag) && all(~isnan(bool_flag));
if ~b 
    b = NaN;
else
    b = 1;  % Not really a logical to avoid the following error : ??? Error using ==> accumarray
            % FILLVALUE and output from the function
            % 'slicer_boolean_all_or_nan' did not have the same
            % class.
end

end