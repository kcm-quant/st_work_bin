function val = slicer_first_finite_sort(x)
x = x(isfinite(x(:, 1)), :);
if isempty(x)
    val = NaN;
    return;
end
[~, idx] = sort(x(:, 2));
val = x(idx(1),1);
end