function val = slicer_last_finite_sort(x)
if size(x,2)==3
    x = x(isfinite(x(:, 1)) & x(:,3)==1, :);
else
    x = x(isfinite(x(:, 1)), :);
end
if isempty(x)
    val = NaN;
    return;
end
[~, idx] = sort(x(:, 2));
val = x(idx(end),1);
end