function val = slicer_nansum_with_nan_def_val(x)
val = x(isfinite(x));
if isempty(val)
    val = NaN;
else
    val = nansum(val);
end
end