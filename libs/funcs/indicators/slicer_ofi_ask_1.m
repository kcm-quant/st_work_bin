function ofi_ask_1=slicer_ofi_ask_1(x)
% OFI_ASK_1 : Calcul de l'OFI � la premi�re limite
% input: ask_size_1,ask_price_1


[n,a]=size(x);
% v�rifier que x dispose de 2 colonnes
if a~=2
    error('input must contain 2 columns');
end
ofi_ask_1=zeros(n,1);
% boucler sur le nombre d'updates de l'ob
for i=1:n-1
    x_tmp=x(i:i+1,:);
    if x_tmp(1,2)==x_tmp(2,2) % m�me premi�re limite
        ofi_ask_1(i+1)=x_tmp(2,1)-x_tmp(1,1);
    elseif x_tmp(1,2)>x_tmp(2,2)
        ofi_ask_1(i+1)=x_tmp(2,1);
    else
        ofi_ask_1(i+1)=-1*x_tmp(1,1);
    end
end
ofi_ask_1=sum(ofi_ask_1);
end