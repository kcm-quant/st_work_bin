function ofi_bid_1=slicer_ofi_bid_1(x)
% OFI_BID_1 : Calcul de l'OFI � la premi�re limite
% input: bid_size_1,bid_price_1


[n,a]=size(x);
% v�rifier que x dispose de 2 colonnes
if a~=2
    error('input must contain 2 columns');
end
ofi_bid_1=zeros(n,1);
% boucler sur le nombre d'updates de l'ob
for i=1:n-1
    x_tmp=x(i:i+1,:);
    if x_tmp(1,2)==x_tmp(2,2) % m�me premi�re limite
        ofi_bid_1(i+1)=x_tmp(2,1)-x_tmp(1,1);
    elseif x_tmp(1,2)>x_tmp(2,2)
        ofi_bid_1(i+1)=-1*x_tmp(1,1);
    else
        ofi_bid_1(i+1)=x_tmp(2,1);
    end
end
ofi_bid_1=sum(ofi_bid_1);
end