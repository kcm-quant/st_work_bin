function ofi_bid_2=slicer_ofi_bid_2(x)
% OFI_BID_2 : Calcul de l'OFI � la premi�re limite
% input: bid_size_1,bid_price_1,bid_size_2,bid_price_2


[n,a]=size(x);
% v�rifier que x dispose de 4 colonnes
if a~=4
    error('input must contain 4 columns');
end
ofi_bid_2=zeros(n,1);
% boucler sur le nombre d'updates de l'ob
for i=1:n-1
    x_tmp=x(i:i+1,:);
    if x_tmp(1,2)==x_tmp(2,2) % m�me premi�re limite
        if x_tmp(1,4)==x_tmp(2,4) % m�me deuxi�me limite
            ofi_bid_2(i+1)=x_tmp(2,3)-x_tmp(1,3);
        elseif x_tmp(1,4)>x_tmp(2,4)
            ofi_bid_2(i+1)=-1*x_tmp(1,3);
        else
            ofi_bid_2(i+1)=x_tmp(2,3);
        end
    elseif x_tmp(1,2)>x_tmp(2,2)
        if x_tmp(1,4)==x_tmp(2,2) 
            ofi_bid_2(i+1)=x_tmp(2,1)-x_tmp(1,3);
        elseif x_tmp(1,4)>x_tmp(2,2) % cas impossible ??
            ofi_bid_2(i+1)=-1*x_tmp(1,3);
        else
            ofi_bid_2(i+1)=x_tmp(2,1);
        end        
    else
        if x_tmp(1,2)==x_tmp(2,4) 
            ofi_bid_2(i+1)=x_tmp(2,3)-x_tmp(1,1);
        elseif x_tmp(1,2)>x_tmp(2,4)
            ofi_bid_2(i+1)=-1*x_tmp(1,1);
        else % cas impossible ??
            ofi_bid_2(i+1)=x_tmp(2,3);
        end   
    end
end
ofi_bid_2=sum(ofi_bid_2);
end