function ofi_bid_3=slicer_ofi_bid_3(x)
% OFI_BID_3 : Calcul de l'OFI � la troisi�me limite
% input:
% bid_size_1,bid_price_1,bid_size_2,bid_price_2,bid_size_3,bid_price_3


[n,a]=size(x);
% v�rifier que x dispose de 6 colonnes
if a~=6
    error('input must contain 6 columns');
end
ofi_bid_3=zeros(n,1);
% boucler sur le nombre d'updates de l'ob
for i=1:n-1
    x_tmp=x(i:i+1,:);
    if x_tmp(1,2)==x_tmp(2,2) % m�me premi�re limite
        if x_tmp(1,4)==x_tmp(2,4) % m�me deuxi�me limite
            if x_tmp(1,6)==x_tmp(2,6) % m�me troisi�me limite
                ofi_bid_3(i+1)=x_tmp(2,5)-x_tmp(1,5);
            elseif x_tmp(1,6)>x_tmp(2,6)
                ofi_bid_3(i+1)=-1*x_tmp(1,5);
            else
                ofi_bid_3(i+1)=x_tmp(2,5);
            end
        elseif x_tmp(1,4)>x_tmp(2,4)
            if x_tmp(1,6)==x_tmp(2,4)
                ofi_bid_3(i+1)=x_tmp(2,3)-x_tmp(1,5);
            elseif x_tmp(1,6)>x_tmp(2,4) % cas impossible ??
                ofi_bid_3(i+1)=-1*x_tmp(1,5);
            else
                ofi_bid_3(i+1)=x_tmp(2,3);
            end
        else
            if x_tmp(1,4)==x_tmp(2,6)
                ofi_bid_3(i+1)=x_tmp(2,5)-x_tmp(1,3);
            elseif x_tmp(1,4)>x_tmp(2,6)
                ofi_bid_3(i+1)=-1*x_tmp(1,3);
            else % cas impossible ??
                ofi_bid_3(i+1)=x_tmp(2,5);
            end
        end
        
    elseif x_tmp(1,2)>x_tmp(2,2)
        
        if x_tmp(1,4)==x_tmp(2,2)
            if x_tmp(1,6)==x_tmp(2,4)
                ofi_bid_3(i+1)=x_tmp(2,3)-x_tmp(1,5);
            elseif x_tmp(1,6)>x_tmp(2,4) % cas impossible ??
                ofi_bid_3(i+1)=-1*x_tmp(1,5);
            else
                ofi_bid_3(i+1)=x_tmp(2,3);
            end
        elseif x_tmp(1,4)>x_tmp(2,2)
            if x_tmp(1,6)==x_tmp(2,2)
                ofi_bid_3(i+1)=x_tmp(2,1)-x_tmp(1,5);
            elseif x_tmp(1,6)>x_tmp(2,2) % cas impossible ??
                ofi_bid_3(i+1)=-1*x_tmp(1,5);
            else
                ofi_bid_3(i+1)=x_tmp(2,1);
            end
        else
            if x_tmp(1,4)==x_tmp(2,4)
                ofi_bid_3(i+1)=x_tmp(2,3)-x_tmp(1,3);
            elseif x_tmp(1,4)>x_tmp(2,4)
                ofi_bid_3(i+1)=-1*x_tmp(1,3);
            else
                ofi_bid_3(i+1)=x_tmp(2,3);
            end
        end
        
    else
        
        if x_tmp(1,2)==x_tmp(2,4)
            if x_tmp(1,4)==x_tmp(2,6)
                ofi_bid_3(i+1)=x_tmp(2,5)-x_tmp(1,3);
            elseif x_tmp(1,4)>x_tmp(2,6)
                ofi_bid_3(i+1)=-1*x_tmp(1,3);
            else % cas impossible ??
                ofi_bid_3(i+1)=x_tmp(2,5);
            end
        elseif x_tmp(1,2)>x_tmp(2,4)
            if x_tmp(1,4)==x_tmp(2,4)
                ofi_bid_3(i+1)=x_tmp(2,3)-x_tmp(1,3);
            elseif x_tmp(1,4)>x_tmp(2,4)
                ofi_bid_3(i+1)=-1*x_tmp(1,3);
            else
                ofi_bid_3(i+1)=x_tmp(2,3);
            end
        else
            if x_tmp(1,2)==x_tmp(2,6)
                ofi_bid_3(i+1)=x_tmp(2,5)-x_tmp(1,1);
            elseif x_tmp(1,2)>x_tmp(2,6) % cas impossible ??
                ofi_bid_3(i+1)=-1*x_tmp(1,1);
            else
                ofi_bid_3(i+1)=x_tmp(2,5);
            end
        end
    end
end
ofi_bid_3=sum(ofi_bid_3);
end