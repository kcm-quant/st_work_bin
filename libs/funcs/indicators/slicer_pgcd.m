function y = slicer_pgcd(x)
MY_EPS = 1e-8;
x = x(1:end);
x = x(isfinite(x) & (x > MY_EPS));
y = MY_EPS*pgcd(round(x(1:end)/MY_EPS));
end