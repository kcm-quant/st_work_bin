function val = slicer_safe_weighted_mean(x)
% SLICER_SAFE_WEIGHTED_MEAN - This is a NaN free weighted mean
%   first col is the weight
%   a row with a NaN will just be filtered
%
if size(x,2)==3
    idx = all(isfinite(x(:,1:2)), 2) & x(:,3)==1;
else
    
    idx = all(isfinite(x), 2);
end
val = nansum(x(idx, 1).*x(idx, 2)) / nansum(x(idx, 1));
if isempty(val)
    val = NaN;
end

end