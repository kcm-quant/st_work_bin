function val = slicer_twap_from_agg_data(x) % twap,close,time_open,time_close,trading_destination_id, phase
persistent cont_phases_ids
if isempty(cont_phases_ids)
    [~,phase_id] = trading_time_interpret([],[]);
    cont_phases_ids = [phase_id.CONTINUOUS;phase_id.SECOND_CONTINUOUS];
end
if length(unique(x(:, 5))) > 1 % isn't allergy implemented?
    error();
end
if length(unique(x(:, 6)))>1 % s'il y a une autre phase que continu, alors il y a peut �tre une intersection entre les bins
    not_cont_bin_all = find(~ismember(x(:, 6), cont_phases_ids));
    EPW = 1/(24*3600*2);
    while ~isempty(not_cont_bin_all)
        not_cont_bin = not_cont_bin_all(1);
        int_bin_ind = find( (x(not_cont_bin, 4)<x(:, 4)-eps & x(not_cont_bin, 4)>x(:, 3)+eps ) | ...
            (x(not_cont_bin, 3)>x(:, 3)+eps & x(not_cont_bin, 3)<x(:, 4)-eps ) );
        if length(int_bin_ind) > 1
            tmp = x(int_bin_ind, :);
            tmp(1, 3) = x(not_cont_bin, 4);
            x(not_cont_bin, 6) = cont_phases_ids(1);
            x(int_bin_ind, 4) = x(not_cont_bin, 3);
            x = [x(setdiff(1:int_bin_ind, not_cont_bin)); x(not_cont_bin, :); ...
                tmp; x(setdiff(int_bin_ind+1:size(x, 1), not_cont_bin))];
        elseif isempty(int_bin_ind) % then the continuous bin is very short (less than eps) ...
            % just deleting the non-continuous bin
            x(not_cont_bin, :) = [];
        else
            error();
        end
        not_cont_bin_all = find(~ismember(x(:, 6), cont_phases_ids));
    end
end
val = ( sum( x(:, 1).*(x(:, 4)-x(:, 3)) ) ...
        + sum( x(1:end-1, 2).*(x(2:end, 3)-x(1:end-1, 4)) ) )...
    / ( x(end, 4)-x(1, 3) );
if abs(val/mean(x(:, 1))-1) > 0.05
    error();
end
end