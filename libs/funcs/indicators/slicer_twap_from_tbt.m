function val = slicer_twap_from_tbt(x) % price,time_open
if size(x, 1) == 1
    val = x(1, 1);
    return;
end
val = sum(x(1:end-1, 1).*diff(x(:, 2)))/(x(end, 2)-x(1, 2));
% if abs(val/mean(x(:, 1))-1) > 0.01
%     error();
% end
end