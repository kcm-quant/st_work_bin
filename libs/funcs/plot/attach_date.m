function attach_date( mode, varargin)
% ATTACH_DATE - attache un vecteur de dates ? un axe horizontal
%   repose sur l'inclusion de donn?es dans le champ 'userdata' de l'axe
%   avec la clef 'attached-date' 
%
% options (persistantes):
% - format: 'dd/mm/yy'
% - x-margin: [] (non utilis? pour l'instant)
% - y-margin: []
% - z-margin: []
% - rotation: -30
% - tick-size: 32 (pts)
% - fontsize: 8
% - xlabels : bottom|top
% - axe     : x
% - dim     : 2
%
% En r?alit?, j'utilise  les min et max de l'axe plut?t que la date fournie
% par l'utilisateur...
%
% attach_date('draw')
% attach_date('init', 'date', data{d}.date)
%
% See also userdata options 

switch lower( mode)
    case 'init'
        %<* Initialisation des dates
        opt = options({'date', [], 'format', 'dd/mm/yy', ...
               'x-margin', [], 'y-margin', [], 'z-margin', [], 'axe', [], ...
               'pct-margin', .01,...
               'rotation', -30, 'tick-size', 32, 'handle', [], ...               
               'fontsize', 10, 'xlabels', 'bottom',...
               'textaxe', 'x', 'dim', 2, ...
               'tick_on_dates', false,...
               'allticks', 0, ...
               'diff_on_dates',[],...
               'fontname','lato'},...
               varargin);
        h = opt.get('axe');
        if isempty(h)
            h = gca;
            opt.set('axe', h);
        end
        
        userdata(h, 'set', 'attached-date', opt);
        attach_date( 'show','axe',h);
        
        % Listener creation.
%         hhAxes = handle(h);
%         hProp = findprop(hhAxes,'YTick');
%         switch opt.get('textaxe')
%             case 'x'
%                 hListener = handle.listener(hhAxes, hProp, 'PropertyPostSet',...
%                     @(x,y)attach_date( 'show','axe',h,'date',get(h,'xlim')));
%             case 'y'
%                 hListener = handle.listener(hhAxes, hProp, 'PropertyPostSet',...
%                     @(x,y)attach_date( 'show','axe',h,'date',get(h,'ylim')));
%             case 'z'
%                 hListener = handle.listener(hhAxes, hProp, 'PropertyPostSet',...
%                     @(x,y)attach_date( 'show','axe',h,'date',get(h,'zlim')));
%         end
%         setappdata(h, 'YTickListener', hListener);
        
        %>*
    case { 'draw', 'show'}
        %<* Draw ticks
%         st_log('attach_date: WARNING, charles changed something on this code, may have effect on ticks...\n');
        opt_tmp = options({'axe',gca},varargin);
        opt = userdata(opt_tmp.get('axe'), 'get', 'attached-date');
        opt.amend( varargin);
        %< Calcul du nombre de ticks ? afficher
        allticks = opt.get('allticks');
        tick_s   = opt.get('tick-size');
        ua       = get(opt.get('axe'),'units');
        set(opt.get('axe'),'units','normalized');
        ap       = get(opt.get('axe'),'position');
        set(opt.get('axe'),'units',ua);
        uf       = get(gcf,'units');
        set(gcf,'units','pixels');
        fp       = get(gcf,'position');
        set(gcf,'units',uf);        
        xaxe_s   = fp(3)*ap(3);
        nb_ticks = floor(xaxe_s / tick_s); % multiple
        if allticks>0
            nb_ticks = allticks;
        end
        %>
        %< Determiner l'axe
        switch lower(opt.get('textaxe'))
            case 'x'
                idx = 1;
                axe_tick = 'xtick';
                tick_label = 'xticklabel';
            case 'y'
                idx = 3;
                axe_tick = 'ytick';
                tick_label = 'yticklabel';
            case 'z'
                idx = 5;
                axe_tick = 'ztick';
                tick_label = 'zticklabel';
        end
        %>
        
        %< Affichages des ticks
        % C'est l? que je n'utilise que le min et le max...
        % En fait je me moque de la date...
        % |dts = opt.get('date');|
        ax = axis(opt.get('axe'));
        tod = opt.get('tick_on_dates');
        if tod
            dts_ticks = opt.get('date');
            if tod > 1
                dts_ticks = dts_ticks(tod:tod:end);
            end
        else
            dts_ticks = linspace(ax(idx), ax(idx+1), nb_ticks);
        end
        set(opt.get('axe'), axe_tick, dts_ticks);
        dts_labels = opt.get('date');
        if tod > 1
            dts_labels = dts_labels(tod:tod:end);
        elseif (tod==0) && (allticks==0)
            dts_labels = linspace(dts_labels(idx), dts_labels(end), nb_ticks);
        end
        
        %>
        %< Affichage des labels
        % Je stocke le handle pour les effacer la fois suivante
        ht = opt.get('handle');
        if ~isempty( ht)
            delete(ht);
        end
        set(opt.get('axe'), tick_label, []);
        ax = axis(opt.get('axe'));
        ax(7:8) = ax(1:2);
        x_margin = opt.get('x-margin');
        y_margin = opt.get('y-margin');
        z_margin = opt.get('z-margin');
        if isempty(x_margin) && isempty(y_margin) && isempty(z_margin)
            axe_margin = diff(ax(idx+2:idx+3))*opt.get('pct-margin');
        elseif ~isempty(x_margin)
            axe_margin = x_margin;
        elseif ~isempty(y_margin)
            axe_margin = y_margin;
        elseif ~isempty(z_margin)
            axe_margin = z_margin;
        end

        xlabels  = opt.get('xlabels');
        rotation = opt.get('rotation');
        if strcmpi(xlabels, 'top')
            axe_pos = ax(idx+3) + axe_margin;
            rotation = -rotation;
        else % bottom
            axe_pos = ax(idx+2)- axe_margin;
        end
        if opt.get('dim')==2
            dod =opt.get('diff_on_dates');
            if tod && length(dod)==length(dts_ticks)
                ht = text(dts_ticks, repmat(axe_pos, size(dts_ticks)),...
                    datestr( dts_labels, opt.get('format')), 'rotation', rotation, ...
                    'fontsize', opt.get('fontsize'),'Parent',opt.get('axe'),'fontname',opt.get('fontname')); % dts_ticks+dod
            else
                ht = text(dts_ticks, repmat(axe_pos, size(dts_ticks)),...
                    datestr( dts_labels, opt.get('format')), 'rotation', rotation, ...
                    'fontsize', opt.get('fontsize'),'Parent',opt.get('axe'),'fontname',opt.get('fontname'));
            end
        else
            axe_pos_1 = ax(idx)- diff(ax(idx:idx+1))*opt.get('pct-margin');
            ht = text(repmat(axe_pos_1, size(dts_ticks)), dts_ticks, repmat(axe_pos, size(dts_ticks)),...
                datestr( dts_ticks, opt.get('format')), 'rotation', rotation, ...
                'fontsize', opt.get('fontsize'),'Parent',opt.get('axe'),'fontname',opt.get('fontname'));
        end
        opt.set('handle', ht);
        %>
        userdata(opt.get('axe'), 'set', 'attached-date', opt);
        %>*
    otherwise
        error('attach_date:mode', 'mode <%s> unknwon', mode);
end