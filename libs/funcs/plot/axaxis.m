function axaxis( varargin)
% AXAXIS - red�finition des axes
%
%  axaxis( mima)
%    revient �
%  ax = axis;
%  axis([ mima ax(3:4)]);
%
% See also get_focus
if nargin==1
    mima = varargin{1};
elseif nargin==2
    mima = [varargin{1} varargin{2}];
end
ax = axis;
axis([ mima ax(3:4)]);