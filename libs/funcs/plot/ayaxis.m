function ayaxis( varargin)
% AYAXIS - red�finition des axes
%
%  ayaxis( mima)
%    revient �
%  ax = axis;
%  axis([ ax(1:2) mima]);
%
% See also get_focus
if nargin==1
    mima = varargin{1};
elseif nargin==2
    mima = [varargin{1} varargin{2}];
end
ax = axis;
axis([ ax(1:2) mima]);