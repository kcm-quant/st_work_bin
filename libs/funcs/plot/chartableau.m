function Results= chartableau(varargin)

szs= cellfun(@(x) size(x,1), varargin);
N= length(varargin);
Results= [];
dim=max(szs);
for n=1:N    
    if iscell(varargin{n})   
        if all(cellfun(@isnumeric,varargin{n}))
            varargin{n}= num2str(cell2mat(varargin{n}));
        else
            varargin{n}= char(varargin{n});
        end
    elseif isnumeric(varargin{n})
        [a,b]= size(varargin{n});
        assert(a==1 | b==1);            
        varargin{n}= num2str(varargin{n}(:));  
    end
    if size(varargin{n},1) == 1
        varargin{n} = repmat(varargin{n},dim,1);
    end
    
    Results= [Results, makeSeparator(dim), varargin{n}];
    
end


function s=makeSeparator(dim,sep)
if nargin<= 1;sep= ' ';end
s= repmat(' ',dim,1 );