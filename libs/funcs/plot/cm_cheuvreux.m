function cm = cm_cheuvreux( n, varargin)
% cm_cheuvreux - Cheuvreux' colormap
%
% [170 160 149]/256 ; [0 150 97] / 256; [153 213 192]/ 256
if nargin<1
    n=64;
end
opt = options({'type', 'std'}, varargin);
c0 = [0 0 0]; 
c1 = [170 160 149]/256;
c2 = [0 150 97] / 256;
switch opt.get('type')
    case 'std'
        cs = arrayfun(@(a,b)linspace(a,b, n-2)',c0,c1, 'uni', false);
        cm = [c2; [153 213 192]/ 256; [cs{:}]];
    case 'shaded'
        cs1 = arrayfun(@(a,b)linspace(a,b, n/2)',c0,c1, 'uni', false);
        cs2 = arrayfun(@(a,b)linspace(a,b, n-size(cs1{1}, 1))',c1,c2, 'uni', false);
        cm = [[cs1{:}]; [cs2{:}]];
    otherwise
        error('cm_cheuvreux:check_args', 'Unknown type <%s>', opt.get('type'));
end
