function cm = cm_kepler_legacy(n,mfilter)
% cm_kepler_legacy - Kepler's legay colormap
% cm = cm_kepler_legacy(8);cm = cm([1, 2, 5, 7, 8],:);
% revision 2021/05/28

if nargin<1
    n=64;
end
c = nan(8,3);

c(1,:) = [51 158 210]/256;
c(2,:) = [0 51 102]/256;
c(3,:) = [128 195 227]/256;
c(4,:) = [178 219 238]/256;
c(5,:) = [128 153 179]/256;
c(6,:) = [178 194 209]/256;
c(7,:) = [248 202 158]/256;
c(8,:) = [240 149 61]/256;
    cm = c([1:n-1, 8],:);

if n<=8
    % cm = c(1:n,:);
    if nargin>1
        cm= cm(mfilter,:);     
    end
    return
end

r = mod(n-8,7);
q = floor((n-8)/7);

c_interp = cell(7,1);
for i = 1:r
    temp = arrayfun(@(a,b)linspace(a,b,(q+1)+2)',c(i,:),c(i+1,:),'uni',false);
    temp = [temp{:}];
    c_interp{i} = temp(2:end-1,:);
end
for i = r+1:7
    temp = arrayfun(@(a,b)linspace(a,b,q+2)',c(i,:),c(i+1,:),'uni',false);
    temp = [temp{:}];
    c_interp{i} = temp(2:end-1,:);
end
cm = c(1,:);
for i = 1:7
   cm = [cm;c_interp{i};c(i+1,:)];
end

if nargin>1
   cm= cm(mfilter,:);     
end
