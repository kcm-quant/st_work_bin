function cm = cm_quant(varargin)
% 2 bleus, 2 oranges, 1 gris

if 1
    cm = cm_kepler(8);cm = cm([1, 2, 5, 7, 8],:);
    
else
    opt = options({'n',5}, varargin);
    n = opt.get('n');
    
    if n<=5
        bcolors = [0    51   102;... %fonc?
            51   158   210;...
            128   195   227;...
            128   153   179]/256; %gris
        
        ocolors = [240  149   61 %fonc?
            248  202  158
            209  171.5  135]/256; %marron
        
        
        cm = [bcolors(1:2,:); ocolors(1:2,:); bcolors(end,:)];
    else
        error('Please use less than 5 values');
    end
end

end