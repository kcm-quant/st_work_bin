function repr= degradeHandle(h,K,lgd,max_color,cmp_type)
% colorie les handles h en un d?grad? de K couleur
%
% degradeHandle(h,10) : s?pare les handles h en 10 groupes
%                       suivant l'ordre de leur indices dans h
%
% degradeHandle(h,v ) : s?pare les handles h suivant les valeurs distinctes de v
%                       length(v)= length(h)
%
% degradeHandle(...,lgd ): affiche la l?gende, associ?e ? un repr?sentant
%                          de chaque clase
%    
% Outputs repr: les repr?sentant sont renvoy?s, pour une l?gende ext?rieure
%               indice: h(repr) repr?sente la classe K(repr) 
% 
%
% * Exemple 1
%    t= (1:200)/200;
%    h= plotpoint(sin(2*pi*t).*t,cos(2*pi*t).*t,'Marker','*');
%    idx= degradeHandle(h,10);
%    legend(h(idx),num2str(idx'))
%
% * Exemple 2
%    t= (1:200)/200;
%    h= plotpoint(sin(2*pi*t).*t,cos(2*pi*t).*t,'Marker','*');
%    cluster= floor(10*rand(1,200)) + 1;
%    repr= degradeHandle(h,cluster);
%    legend(h(repr)',num2str((1:10)'));
%
%    marche aussi avec cell array
%    ->  cluster= arrayfun(@num2str,cluster,'uniformoutput',0)
%        repr= degradeHandle(h,cluster)
%
if nargin< 4; max_color= 0;end
if nargin<5;cmp_type= @jet;end
mks={};
repr= []; % un representant de chaque classe de couleur
N= length(h);
if length(K)== 1
	% d?coupage lin?aire
	repr= 1:floor(N/K):N;
	i= 1+ floor(((1:N)-1)/N*K);
	cmp = jet(K);
else
	% d?coupage suivant les classes (valeur de K)
	[labels,repr,classes] = unique(K);
	i= classes;
	cmp = cmp_type(length(labels));    
	if max_color> 0
        cmp_base = cmp_type(max_color);
        Ncl=length(labels);
        mks_base = {'+','o','*','.','x','s','d'};
        
        QQ= ceil(Ncl / max_color);
        cmp= kron((1:max_color)',ones(QQ,1));
        mks= kron(ones(max_color,1),(1:QQ)');
        
        cmp= cmp_base(cmp,:);
        mks= mks_base(mks);
    end
end

%% coloriage
if isempty(mks)
    for nn= 1:N
        set(h(nn),'color',cmp(i(nn),:));
    end
else
    for nn= 1:N
        set(h(nn),'color',cmp(i(nn),:),'Marker',mks{i(nn)});
    end    
end
if nargin>= 3
    if isempty(lgd);lgd= labels;end
    if isa(lgd,'mdict')
        if iscell(K)
            lgd= cellfun(@(x) lgd.get(x),K(repr) ,'uni',0);
        else
            lgd= arrayfun(@(x) lgd.get(x),K(repr) ,'uni',0);
        end
    end
	legend(h(repr),lgd);
end


function DEMO

gg= plotpoint((1:10)');
degradeHandle(gg,2)

degradeHandle(gg,1:10)

m= mdict();
for k=0:10;m.set(k,num2str(k));end
for k=0:10;m.set(num2str(k),num2str(k));end

gg= plotpoint((1:10)');
degradeHandle(gg,mod(1:10,3),m)

degradeHandle(gg,cellstr(num2str(mod(1:10,3)')),m)

gg= plotpoint((1:10)');
degradeHandle(gg,mod(1:10,4),m,2)



