function h = get_focus( fname, varargin)
% GET_FOCUS - get focus on a figure given its name or create it
%
%  h = get_focus( 'MI params');
%
% See also axaxis
h = findobj( 'name', fname);
if isempty( h)
    h = figure( 'name', fname, varargin{:});
else
    if nargin > 1
        set(0,'CurrentFigure',h);
        set(h, varargin{:});
    else
        figure(h);
    end
end