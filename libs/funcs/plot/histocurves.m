function h = histocurves( mode, varargin)
% HISTOCURVES - histogrammes de courbes
%
% data = st_data('from-matrix', exp(bsxfun(@times, randn(36,40), rand(1,40))) );
% h = histocurves( 'init', data, 'axe', [])
% h = histocurves( 'init', data, 'axe', [], 'auctions', [1 36])
%
% options:
% - colorize : text|background
% - color    : base color
% - quantiles: nb quantiles|quantile values
% - colormap : eponym
% - xlabels  : bottom|top
% - first-quantile: 1
% - menu: true
% - fill: true
%
switch lower(mode)
    case 'init'
        data = varargin{1};
        opt  = options( { 'axes', [], 'quantiles', 3, 'color', [.67 0 .12], ...
            'colorize', 'text', 'colormap', 'jet', 'xlabels', 'bottom', 'auctions', [], ...
            'external', [], 'first-quantile',1 , 'menu', true, 'fill', true}, ...
            varargin(2:end));
        h = opt.get('axes');
        if isempty(h) || ~ishandle( h)
            h = axes;
        else
            axes(h);
        end
        cla
        qs   = opt.get('quantiles');
        cm   = opt.get('color');
        if length(qs)==1 && qs > 1
            q  = (qs:-1:1)/qs;
            qs = [0 q(2:end)/2 .5 q(end:-1:2)/2+.5 1];
        end
        qs = sort(qs);
        quants = quantile(data.value, qs, 2);
        Lq = length(qs)/2;
        auctions = opt.get('auctions');
        if isempty( auctions)
            dt0   = data.date;
            y     = quants;
            mimax = [min(data.date) max(data.date)];
        else
            % en cas d'auctions, je les retire
            continuous = setdiff((1:length(data.date))', auctions);
            dt0 = data.date(continuous);
            y   = quants(continuous,:);
        end
        dt  = [dt0; flipud(dt0) ];
        for c=opt.get('first-quantile'):floor(Lq)
%             fill( dt, [y(:,c); flipud(y(:,end-c+1))], 'r', ...
%                 'facecolor', cm/(c+1), 'edgecolor', [0 0 0], 'linewidth',2 );
            if opt.get('fill')
                ec = cm/(c+1);
            else
                ec = 'none';
            end
            fill( dt, [y(:,c); flipud(y(:,end-c+1))], 'r', ...
                'facecolor', ec, 'edgecolor', [0 0 0], 'linewidth',2 );
            hold on
        end
        if floor(Lq)~=ceil(Lq)
            plot( dt0, y(:,ceil(Lq)), 'linewidth',2, 'color', cm);
            hold on
        end
        if isempty( auctions)
            delta_a = 0;
        else
            % en cas d'auctions, je les rajoute ici
            delta_t  = min(abs(diff(data.date)));
            x_a      = auctions;
            idx_plus = find(x_a~=1&x_a~=length(data.date));
            dt_a     = data.date(x_a);
            y_a      = quants(x_a,:);
            if ~isempty( idx_plus)
                % si j'ai des auctions en plus du d�but et de la fin
                dt_a(idx_plus) = [];
                y_a(idx_plus)  = [];
                md       = max(data.date);
                dt_a     = [dt_a(:); md:delta_t:md+length(idx_plus)+1];
                y_a      = [y_a(:); quants(x_a(idx_plus))];
            end
            delta_a = delta_t*2/3;
            for d=1:length(dt_a)
                for c=1:floor(Lq)
                    [x,y] = DRAW_SQUARE( dt_a(d), delta_a, y_a(c), y_a(end-c+1));
                    fill( x, y, 'r', ...
                        'facecolor', cm/(c+1), 'edgecolor', 'none' );
                end
                % plus la m�diane
                if floor(Lq)~=ceil(Lq)
                    plot( [dt_a(d)-delta_a/2, dt_a(d)+delta_a/2], repmat(y_a(d,ceil(Lq)),2,1), 'linewidth',2, 'color', cm);
                end
            end
            mimax = quantile([dt0(:); dt_a(:)],[0 1]);
            mimax(1) = mimax(1)-delta_a/2;
            mimax(2) = mimax(2)+delta_a/2;
        end
        ax = axis;
        axis([mimax ax(3:4)]);
        hold off
        if isfield( data, 'attach_format')
            attach_date('init', 'date', data.date, 'format', data.attach_format, 'xlabels', opt.get('xlabels') );
        end
        %< Affichage des labels
        labels = opt.get('external');
        if isempty( labels)
            labels = data;
        end
        cm = feval( opt.get('colormap'), length(labels.colnames));
        if strcmpi( opt.get('colorize'), 'text')
            colorize = 'color';
        else
            colorize = 'backgroundcolor';
        end
        if opt.get('menu')
            for c=1:length(labels.colnames)
                text(mimax(2), ax(3)+diff(ax(3:4))*c/(1+length(labels.colnames)), labels.colnames{c}, ...
                    'fontsize', 8, 'ButtonDownFcn', sprintf( 'histocurves(''callbacks'', ''toggle-curve'', ''%s'');', labels.colnames{c}), ...
                    colorize, cm(c,:));
            end
        end
        %>
        if ~isempty(auctions)
            %< Modif data pour auctions
            % travail sur les donn�es pour les affichages de courbes
            % compl�mentaires.
            % je sais que |dt0| est la partie continue de ma courbe, |dt_a|
            % contenant la totalit�.
            % Je vais donc d�doubler le surplus en y intercalant des NaN et
            % des doublons...
            dt_cont = setdiff((1:length(data.date))', auctions);
            curves_dt   = [data.date(dt_cont); DEDOUBLE(dt_a, delta_a)];
            curves_vals = [data.value(dt_cont,:); DEDOUBLE(data.value(auctions,:), 0)];
            data.date   = curves_dt;
            data.value  = curves_vals;
            %>
        end
        userdata(gca, 'set', 'histocurves_data', labels);
        % c'est une table de correspondance: id -> handle
        userdata(gca, 'set', 'c_handles', repmat(NaN, length(labels.colnames), 1)); % TODO voir avec Charles  : � la place de labels il y avait data
        userdata(gca, 'set', 'colormap', cm);
    case 'callbacks'
        c_mode = varargin{1};
        switch lower(c_mode)
            case 'toggle-curve'
                cname  = varargin{2};
                
                data   = userdata(gca, 'get', 'histocurves_data');
                hs     = userdata(gca, 'get', 'c_handles');
                cm     = userdata(gca, 'get', 'colormap');
                
                [vals, id] = st_data('col', data, cname);
                if isnan( hs(id))
                    % il faut le cr�er
                    hold on
                    h = plot(data.date, vals, 'linewidth', 2, 'color', cm(id,:));
                    hold off
                    hs(id) = h;
                else
                    % il faut le r�truire
                    delete( hs(id));
                    hs(id)=NaN;
                end
                userdata(gca, 'set', 'c_handles', hs);
            otherwise
                error('histocurves:callbacks:mode', 'callback mode <%s> unknown', c_mode);
        end                
    otherwise
        error('histocurves:mode', 'mode <%s> unknown', mode);
end

%%** Internals

%<* Draw a square
function [a,b] = DRAW_SQUARE( x, delta_x, y1, y2)
a = [x-delta_x/2, x-delta_x/2, x+delta_x/2, x+delta_x/2];
b = [y1, y2, y2, y1];
%>*

%<* D�doublage
% Pour affichage plot
function v1 = DEDOUBLE( v0, delta_a)
v1 = [];
for c=1:size(v0,2)
    v0_ = v0(:,c);
    v0_ = v0_(:)';
    v1_ = [repmat(NaN,size(v0_)); v0_+delta_a/2; v0_-delta_a/2];
    v1_ = v1_(:);
    v1 = [v1, v1_];
end
%>*