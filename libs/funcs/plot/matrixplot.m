function z_=matrixplot(m, varargin)
% MATRIXPLOT - plots matrix values with colorcode
%
%  matrixplot(m [,ylabels] [,n_color])
%
% Examples:
%  l= randn(10,10);
%  labels= cellstr( num2str((1:10)'));
%  matrixplot(l ,labels' ,10);
% 
%
% See also:
%    
%
%   author   : 'nmayo@cheuvreux.com'
%   reviewer : ''
%   date     :  '18/11/2011'
%   
%   last_checkin_info : $Header: matrixplot.m: Revision: 6: Author: namay: Date: 01/02/2013 07:43:15 PM$
z_=[];
z_.axis= @axis;
z_.shading=  @shading;
z_.handle= nan;

no_bone= 0;
n_color= 10;

ylabel={};
break_value= nan;
break_length= range(m)/5;

for k=1:length(varargin)
    current= varargin{k};
    if iscell(current)
        if ischar(current{1}) & strcmpi(current{1},'break_value')
           break_value= current{2};
           if length(current)>2
                break_length= current{3};
           end
        else
            ylabel= current;
        end
    elseif length(current)==1 && mod(current,1)==0
        n_color= current;
    elseif ischar(current) && strcmpi(current,'nobone')
        no_bone= 1;
    else; error('');
    end
end

% if~isnan(break_value)
%     m(m>) = m(m ) + break_length;

[P,Q]= size(m);
mm= nan(P+1,Q+1);
mm(1:P,1:Q)= m;
z_.handle= gca;
pcolor(mm);
axis ij;
%axis square;
shading flat % faceted %interp

cmp= jet(n_color+1); 
if ~no_bone
    cmp_ = bone(10);
    cmp([1 end],:)= cmp_([end 1],:);
end
colormap(cmp);
%colormap(jet(6));
colorbar;

% c= get(gcf,'children');
% clb_= c(find(strcmp(get(c,'tag'),'Colorbar')));
% get(clb_,'ytick')
% get(clb_,'yticklabel')

if ~isempty(ylabel)
    ylabel={ylabel{:}};
   [N,R]= size(ylabel);
   if iscell(ylabel); assert(N==1); N= R;
   end
   assert(P==N,'in <matrixplot>: pas assez d''elements dans ylabel');   
   set(gca,'ytick',(1:N)+0.5);
   set(gca,'yticklabel', ylabel);            
end

%%
function TEST()
 l= randn(10,10);
labels= cellstr( num2str((1:10)'));
matrixplot(l ,labels ,10);
matrixplot(l ,labels ,100,'nobone');
% 