function h= ns_figure(varargin)
% Non focus-stealing figure
% figure(); allways steals focus, but set(0,'CurrentFigure',h) does not.
% So the idea is to create a bunch of figures a beginning (focus-stealing), and then
% to use ns_figure() for non stealing behaviour.
%
% Modes:
%   ns_figure('init',100); %creates 100 figure
%   ns_figure('init',[]);  %deletes buffer
%   h= ns_figure() % returns a bufferized empty figure, without losing focus
%                  % adds DEFAULT_EXPAND_BY=10 figures if empty buffer
%   ns_figure(h)  % sets h as active figure,without losing focus
%                 % The case of invalid handle integer h is not supported yet
%
% Exemple:
%   for k=1:30; ns_figure('name','demo ns_figure');end
%

% TODO: laisser varargin paramatrer les figures ?
persistent fig_buffer;
h=[];
% if nargin == 2 % assure la distinction
%     assert(strcmp(varargin{1},'init'));
% if nargin == 2 % assure la distinction
%     assert(strcmp(varargin{1},'init'));
if nargin == 2 & strcmp(varargin{1},'init') % assure la distinction    
    N= varargin{2};
    if isempty(N) % DELETES buffer
        % ne delete pas les figures renvoy�es et d�pil�es par ns_figure();
        for k=fig_buffer
            try
                close(fig_buffer);
            catch ME; end
        end
        fig_buffer=[];
    else
        % bufferizes new figures, does not destroy previous
        for n=1:N
            fig_buffer(end+1)= figure;
        end
    end
    return
end

if length(varargin)==1
    % TODO: si h n'existe pas encore, c'est non compatible !
    h= varargin{1};
    set(0,'CurrentFigure',h);    
else    
    if isempty(fig_buffer)
        DEFAULT_EXPAND_BY= 10;
        ns_figure('init',DEFAULT_EXPAND_BY); % Stealing !
    end
    assert(~isempty(fig_buffer))
    h= fig_buffer(end);
    fig_buffer(end)= [];    
    set(0,'CurrentFigure',h);
    set(h,varargin{:});
end

