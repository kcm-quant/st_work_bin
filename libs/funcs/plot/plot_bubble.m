function [h,cr] = plot_bubble( data, varargin)
% PLOT_BUBBLE - bubble plot
% 
% options
% - panel       : []
% - center-names: ''
% - radius-names: ''
% - coef-radius : [] / [cx,cy]
% - nb-points   : 12
% - alpha       : true
% - color       : [.67 .12 .12]
% - bubble-names: ''
%
% example:
%  data = read_dataset( 'btickdb', 'security', 'TOTF.PA', 'from', '26/09/2007', 'to', '26/09/2007')
%  f = build_st_function( 'init', @(x)[mean(x(:,2).*x(:,1)), std(x(:,2).*x(:,1)), mean(diff(log(x(1:50:end,1)))), std(diff(log(x(1:50:end,1))))], ...
%           'colnames-fun', 'm-v;s-v;m-r;s-r', 'date-fun', @(t)t(end))
%  data_ech = f.sliding( st_data('keep', data, 'volume;price'), 600, 600)
%  plot_bubble( data_ech, 'center-names', 'm-v;m-r', 'radius-names', 's-v;s-r')
%
% See also st_plot
h=[];
opt = options({ 'panel', [], 'center-names', '', 'radius-names', '', 'bubble-names', '',...
    'coef-radius', [], 'nb-points', 12, ...
    'alpha', true, 'color', [.67 .12 .12], 'hold-off', true,'Visible','on'}, varargin);

f = opt.get('panel');
if isempty(f)
    h=figure('Visible',opt.get('Visible')); 
    f = uipanel('backgroundcolor', [238 238 208]/256, 'borderwidth', 0);
end
axes('parent', f);
vals    = st_data('col', data, opt.get('center-names'));
r_names = tokenize(opt.get('radius-names'),';');
if length(r_names) == 1
    vals = [vals st_data('col', data, r_names{1}) st_data('col', data, r_names{1})];
else
    vals = [vals st_data('col', data, opt.get('radius-names'))];
end
% nan: out
idx_out = any(isnan(vals),2);
st_log('plot_bubble: remove %d lines over %d (%3.2f%% because of the nans)\n', ...
    sum(idx_out), length(idx_out), 100*sum(idx_out)/length(idx_out));
vals(idx_out,:)=[];
cr = opt.get('coef-radius');
if isempty( cr)
    %< Renormalisation automatique des rayon
    % heuristique
    dv = mean(diff( sort( vals(:,1:2))));
    dr = mean(( sort( vals(:,3:4))));
    cr = dv./dr;
    %>
end
vals(:,3:4) = bsxfun( @times, cr, vals(:,3:4));
nbp       = opt.get('nb-points');
alpha     = opt.get('alpha');
colr      = opt.get('color');

bubble_names = tokenize(opt.get('bubble-names'),';');
% cla
hold on
if isempty(bubble_names)
    arrayfun( @(k)ELLIPSE2D( vals(k,1), vals(k,3), vals(k,2), vals(k,4), nbp, alpha, colr), ...
        (1:size(vals,1))');
else
    arrayfun( @(k)ELLIPSE2D( vals(k,1), vals(k,3), vals(k,2), vals(k,4), nbp, alpha, colr, bubble_names{k}), ...
        (1:size(vals,1))');    
end
if opt.get('hold-off')
    hold off
end
names = tokenize( opt.get('center-names'), ';');
xlabel(names{1}); ylabel(names{2});
title( data.title);
set(gca, 'parent', f);



function hc = ELLIPSE2D( c1, r1, c2, r2, nb, a, c, t)

k=0:nb;
x = c1 + r1 * cos(2*pi/nb*k);
y = c2 + r2 * sin(2*pi/nb*k);
if a
    fill( x,y, 'r', 'facecolor', c, ...
        'edgecolor', c, 'facealpha', .2);
else
    plot(x,y, 'linewidth', 2, 'color', c);
end
if nargin==8
    text(c1, c2, t);
end
    



