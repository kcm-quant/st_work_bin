function ax = plot_diag( data, varargin)
% PLOT_DIAG - plus diagnosis control map
%   it takes as input a st_data with a field 'diag_threhold'
%   containing a matrix with one column for each variable of the 
%   st_data, and one row for each threhold to apply
%
% use:
%  h = plot_diag( data, options)
%  - data: is a st_data
%  - h   :is an handle on the plot axes
%  - options are:
%    * axes: an handle on target axes
%    * colormap: a colormap
%
% example:
%   data = st_data('from-matrix', randn(100,3))
%   data.diag_threshold = repmat([1.96 1.6 -1.6 -1.96]', 1, 3)
%   plot_diag( data)

% author:  'Charles-Albert Lehalle'
% version: '1.0'
% date:    '27/09/2010'
% project: 'plot'

%% Plot Diag
% plot diagnosis bars

%% Parameters
if ~isfield( data, 'diag_threshold')
    error('plot_diag:diag', 'I need a diag_threshold field to work');
end
diag_th = data.diag_threshold;
nb_D = size(diag_th,1)+1;

opt = options({'axes', [], 'colormap', '','lines',false,'deal_with_nan', false}, varargin);
%< Graph handles
ax = opt.get('axes');
if isempty( ax)
    figure;
    ax = axes;
else
    %axes(ax);
end
%>
cm = opt.get('colormap');
if isempty( cm)
    zo = [102 255  51; 255 204  51; 255 102  51; 255 51 51; 184  0  0]/255;
    this_cm = [];
    for c=1:size(zo,2)
        this_cm(:,c) = interp1((0:4)/4, zo(:,c)',(0:nb_D-1)/(nb_D-1))';
    end
else
    this_cm=cm;
    if size(this_cm,1)~=nb_D
        error('plot_diag: colormaps in input, has not the good dimension')
    end
   %this_cm = feval( cm, nb_D); 
end
% In order to handle NaN values
% the color for this will be white
deal_with_nan=opt.get('deal_with_nan');
if deal_with_nan
    this_cm=cat(1,this_cm,[1 1 1]);
end


%% Compute the control map
vals = repmat(nan, size(data.value));
nb_C = size(vals,2);
if deal_with_nan
    for c=1:nb_C
        idx_nans=isnan(data.value(:,c));
        vals(idx_nans,c)=nb_D+1;
        vals(~idx_nans,c) = 1+sum( bsxfun(@ge, data.value(~idx_nans,c), diag_th(:,c)'), 2);
    end
else
    for c=1:nb_C
        vals(:,c) = 1+sum( bsxfun(@ge, data.value(:,c), diag_th(:,c)'), 2);
    end
    
end
vals = vals';
%% Plot the control map
imagesc(data.date, 1:nb_C, vals);
if opt.get('lines');
    xlim=get(gca,'xlim');
    xitckrange=(xlim(2)-xlim(1))/size(vals,2);
    ylim=get(gca,'ylim');
    yitckrange=(ylim(2)-ylim(1))/size(vals,1);
    for i_x=1:size(vals,2)-1
        val_tmp=xlim(1)+i_x*xitckrange;
        hold on;
        plot([val_tmp val_tmp],ylim,'-k')
    end
    for i_y=1:size(vals,1)-1
        val_tmp=ylim(1)+i_y*yitckrange;
        hold on;
        plot(xlim,[val_tmp val_tmp],'-k')
    end
end

set(gca, 'ytick', 1:nb_C)
set(gca, 'yticklabel', data.colnames);
this_cm=this_cm(unique(vals(:)),:);
colormap(gca, this_cm);




