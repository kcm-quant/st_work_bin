function h = plot_distrib( data, varargin)
% PLOT_DISTRIB - �tude graphique d'une distribution
%
% plot_distrib( 'mode', data, options)
%
% les modes:
% - boxplot 
% - density 
% - classic : les donn�es sont en colonnes
% - group   : une variable de groupement est utilis�e
%
% 
% plot_distrib( 'group', bid_data, 'colnames', 'Volume', 'nbpoints', 1000, ...
%      'kernel', 'normal', 'groupname', 'Price', 'colormap', [.67 0 .12; 0 0 0], ...
%      'labels', { '0.01'});
% 
%
% Il y a plein d'options,
% cf le script analyse-perf-01 pour un exemple en attendant une meilleure
% documentation
%
% See also weightened_statistics 
if ischar(data)
    mode     = data;
    data     = varargin{1};
    varargin = varargin(2:end);
else
    mode     = 'classic';
end
opt = options({'colnames', [], 'figure', [],  ...
    'kernel', 'epanechnikov', 'boxplot', true, ...
    'colormap', [], 'nbpoints', 100, ...
    'weights', '', ...
    'rownames', {}, ...
    'groupname', '', ...
    'labels', {}, ...
    'compare', ''}, varargin);
switch lower(mode)
    case 'boxplot'
        colnames = opt.get('colnames');
        kern     = opt.get('kernel');
        comp     = opt.get('compare');
        cm       = opt.get('colormap');
        rownames = opt.get('rownames');
        weights  = opt.get('weights');
        if ~isempty( weights)
            weights = st_data('col', data, weights);
        else
            weights = [];
        end
        if ~isempty( rownames)
            [lbls, idx, jdx] = unique(rownames);
        else
            lbls = {};
        end
        if ~isempty( colnames)
            data = st_data( 'keep', data, colnames);
        end
        if size(data.value,2)>1
            error('plot_distrib:dim', 'the <boxplot> mode is only available for a dimentional datasets');
        end
        
        a1=subplot( 2,1,2);
        ax = [Inf, -Inf];
        h = [];
        lgd = {};
        if size(cm,1)<2+length(lbls)
            cm = [cm; jet( 2+length(lbls)-size(cm,1))];
        end

        perf = data.value;
        [y,x] = ksdensity( perf, 'npoints', opt.get('nbpoints'), 'kernel', kern);
        h = [h; fill( [x(:);x(1)], [y(:); y(1)], 'r', 'edgecolor', 'none', 'facecolor', cm(1,:))];
        lgd{end+1} = data.colnames{1};
        ax(1) = min( ax(1), quantile( perf, .01));
        ax(2) = max( ax(2), quantile( perf, .99));
        switch lower(comp)
            case 'normal'
                if isempty(weights)
                    mx = median( perf);
                    sx = std(perf);
                else
                    mx = sum( perf.*weights)/sum(weights);
                    sx = sqrt( sum(perf.^2.*weights)/sum(weights) - mx^2);
                end
                g_density = @(x)(exp(-(x-mx).^2/(2*sx^2))/(2*pi*sx));
                hold on
                h = [h; plot(x, g_density(x), 'linewidth',2, 'color', cm(2,:))];
                lgd{end+1} = sprintf('%s, Gaussian model (m=%5.2f,std=%5.2f)', data.colnames{1}, mx, sx);
                hold off
            case { 'robust-normal', 'rnormal' }
                [mx, sx] = estimationRobuste('gaussien', perf, 'quantile-level', .25, 'troncate-level', 1.96);
                g_density = @(x)(exp(-(x-mx).^2/(2*sx^2))/(2*pi*sx));
                hold on
                h = [h; plot(x, g_density(x), 'linewidth',2, 'color', cm(2,:))];
                lgd{end+1} = sprintf('%s, Gaussian model (m=%5.2f,std=%5.2f)', data.colnames{1}, mx, sx);
                hold off
        end
        ay = axis;
        axis([ax ay(3:4)]);
        legend( h, lgd);
        
        a2=subplot(2,1,1);
        if ~isempty( rownames)
            my_labs = cat(1, lbls, { 'ALL' });
            boxplot([perf; perf], [repmat(length(lbls)+1,size(perf)); jdx], 'orientation', 'horizontal', 'labels', my_labs);
        else
            boxplot(perf, 'orientation', 'horizontal');
        end
        xlabel('');
        linkaxes([a1,a2],'x');
    case 'density'
        colnames = opt.get('colnames');
        kern     = opt.get('kernel');
        comp     = opt.get('compare');
        cm       = opt.get('colormap');
        rownames = opt.get('rownames');
        weights  = opt.get('weights');
        if ~isempty( weights)
            weights = st_data('col', data, weights);
        else
            weights = [];
        end
        if ~isempty( rownames)
            [lbls, idx, jdx] = unique(rownames);
        else
            lbls = {};
        end
        if ~isempty( colnames)
            data = st_data( 'keep', data, colnames);
        end
        Lc = length(data.colnames);
        for c=1:Lc
            subplot( Lc,1,c);
            ax = [Inf, -Inf];
            h = [];
            lgd = {};
            if size(cm,1)<2+length(lbls)
                cm = [cm; jet( 2+length(lbls)-size(cm,1))];
            end
            perf = data.value(:,c);
            if isempty( weights)
                [y,x] = ksdensity( perf, 'npoints', opt.get('nbpoints'), 'kernel', kern);
            else
                [y,x] = ksdensity( perf, 'npoints', opt.get('nbpoints'), 'kernel', kern, 'weights', weights);
            end
            h = [h; fill( [x(:);x(1)], [y(:); y(1)], 'r', 'edgecolor', 'none', 'facecolor', cm(1,:))];
            lgd{end+1} = data.colnames{c};
            ax(1) = min( ax(1), quantile( perf, .01));
            ax(2) = max( ax(2), quantile( perf, .99));
            switch lower(comp)
                case 'normal'
                    if isempty( weights)
                        mx = median( perf);
                        sx = std(perf);
                    else
                        mx = sum( perf.*weights)/sum(weights);
                        sx = sqrt( sum(perf.^2.*weights)/sum(weights) - mx^2);
                    end
                    g_density = @(x)(exp(-(x-mx).^2/(2*sx^2))/(2*pi*sx));
                    hold on
                    h = [h; plot(x, g_density(x), 'linewidth',2, 'color', cm(2,:))];
                    lgd{end+1} = sprintf('%s, Gaussian model (m=%5.2f,std=%5.2f)', data.colnames{c}, mx, sx);
                    hold off
                case { 'robust-normal', 'rnormal' }
                    [mx, sx] = estimationRobuste('gaussien', perf, 'quantile-level', .25, 'troncate-level', 1.96);
                    g_density = @(x)(exp(-(x-mx).^2/(2*sx^2))/(2*pi*sx));
                    hold on
                    h = [h; plot(x, g_density(x), 'linewidth',2, 'color', cm(2,:))];
                    lgd{end+1} = sprintf('%s, Gaussian model (m=%5.2f,std=%5.2f)', data.colnames{1}, mx, sx);
                    hold off
            end
            if ~isempty( rownames)
                for r=1:length(lbls)
                    perf = data.value(jdx==r,c);
                    [y,x] = ksdensity( perf, 'npoints', opt.get('nbpoints'), 'kernel', kern);
                    hold on
                    h = [h; plot( x(:), y(:), 'color', cm(2+r,:), 'linewidth', 2)];
                    hold off
                    lgd{end+1} = [ data.colnames{c} ' x ' lbls{r}];
                end
            end
        end 
        ay = axis;
        axis([ax ay(3:4)]);
        legend( h, lgd);
    case 'classic'
        colnames = opt.get('colnames');
        kern     = opt.get('kernel');
        if ~isempty( colnames)
            data = st_data( 'keep', data, colnames);
        end
        
        if opt.get('boxplot')
            h1 = subplot(1,2,1);
            boxplot( data.value, 'labels', data.colnames);

            h2 = subplot(1,2,2);
        end
        for c=1:size(data.value,2)
            val   = data.value(:,c);
            [y,x] = ksdensity( val,'kernel', kern, 'npoints', opt.get('nbpoints'));
            [tmp,mode_idx] = max(y);
            mode_v = x(mode_idx);
            nr    = .4/max(y(:));
            Y = [x(:);flipud(x(:))];
            X = [c-y(:)*nr;flipud(c+y(:)*nr)];
            fill(X,Y,'r', ...
                'edgecolor','none', 'facecolor', [.8 .4 .4]);
            hold on
            g = [];
            g = [g, plot([c-.4, c+.4], [mean(val), mean(val)],'k', 'linewidth',2)];
            g = [g, plot([c-.4, c+.4], [mode_v, mode_v],'--r', 'linewidth',2)];
            g = [g, plot([c-.4, c+.4], [median(val), median(val)],'r', 'linewidth',2)];
            q = quantile(val,[.75 .75 .25 .25 .75]);
            g = [g, plot([c-.4, c+.4, c+.4, c-.4, c-.4], q,'--k', 'linewidth',2)];
        end
        hold off
        legend(g,{'mean', 'mode', 'median', 'quantile +/-25%'});
        linkaxes([h2,h1], 'y');
    case 'group'
        colnames  = opt.get('colnames');
        groupname = opt.get('groupname');
        kern     = opt.get('kernel');
        if isempty( colnames) || isempty(groupname)
            error('plot_distrib:group', 'for <group> mode, I need a <colnames> and a <groupname>');
        end
        vals = st_data('col', data, colnames);
        grp  = st_data('col', data, groupname);
        ugrp = unique( grp);
        for c=1:length(ugrp)
            val = vals(grp==ugrp(c));
            [y,x] = ksdensity( val,'kernel', kern, 'npoints', opt.get('nbpoints'));
            [tmp,mode_idx] = max(y);
            mode_v = x(mode_idx);
            nr    = .4/max(y(:));
            Y = [x(:);flipud(x(:))];
            X = [c-y(:)*nr;flipud(c+y(:)*nr)];
            fill(X,Y,'r', ...
                'edgecolor','none', 'facecolor', [.8 .4 .4]);
            hold on
            g = [];
            g = [g, plot([c-.4, c+.4], [mean(val), mean(val)],'k', 'linewidth',2)];
            g = [g, plot([c-.4, c+.4], [mode_v, mode_v],'--r', 'linewidth',2)];
            g = [g, plot([c-.4, c+.4], [median(val), median(val)],'r', 'linewidth',2)];
            q = quantile(val,[.75 .75 .25 .25 .75]);
            g = [g, plot([c-.4, c+.4, c+.4, c-.4, c-.4], q,'--k', 'linewidth',2)];
        end
    otherwise
        error('plot_distrib:mode', 'mode <%s> unknwn', mode);
end