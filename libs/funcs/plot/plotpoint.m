function h= plotpoint(x,varargin)
% plot la courbe (x,y) mais chaque point s?par?ment
% renvoie le vecteur des handles de ts les points
%
%

if nargin> 1 & isnumeric(varargin{1}) % (x,y,opts)
    y= varargin{1};  varargin= varargin(2:end);       
else
    %y=x;x= 1:size(y,1);
    if size(x,2)> 1
        y= x(:,2);
        x= x(:,1);
    else
        y=x;x= 1:numel(y);
    end
end

ish= ishold;
h=[];
N= length(x);
assert(length(y)==N);
hold on;
for n=1:N
    h(n)= plot(x(n),y(n),varargin{:},'marker','.');%,'markersize',5,);
end
if ~ish;hold off;end