function set_cm_cheuvreux(varargin)
% SET_CM_CHEURVEUX - set the cheuvreux cm to the current axe
%
% options: 'n', 'cga'
%
% See also cm_cheurveux

opt = options({'n', 64, 'gca', gca}, varargin);
set(opt.get('gca'),'NextPlot','add', 'ColorOrder',  cm_cheuvreux(opt.get('n')));