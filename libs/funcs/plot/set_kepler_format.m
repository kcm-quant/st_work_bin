function h = set_kepler_format(h, varargin)
% WARNING: appliquer set_kepler_format � la fin de la construction du graphe
% le format KC n'est pas id�al pour des dates au format dd/mm/yyyy
% plutot utiliser: dd/mm (courtes p�riodes) ou mm/yyyy (longues p�riodes)
% exemples:
% figure;subplot(1,2,1);hold on;plot(rand(10,1),'color','b');bar(rand(10,1),'facecolor','r');hold off;legend({'A','B'});subplot(1,2,2);area(rand(10,1),'facecolor','g');legend({'C'})
% set_kepler_format(gcf, 'size','S') %small: "2 perp age"
% set_kepler_format(gcf, 'size','M') %mid: "page width" (default value)
% set_kepler_format(gcf, 'size','S', 'ytl','%+.0f%%') %pour des y en pct
% set_kepler_format(gcf, 'size','S', 'change_colors',1) %pour changer les couleurs
% set_kepler_format(gcf, 'size','S', 'ytl','%+.0f%%', 'change_colors',1)
% set_kepler_format(gcf, 'size','S', 'xtl','dd/mm/yyyy')
%

opt = options({'size','M',... S (small), M (mid), L (large)
    'xtl',[],... '%.0f%%' 'dd/mm/yyyy'
    'ytl',[],... '%.0f%%'
    'change_all_axes',1,...
    'change_colors',1,...
    'heatmap',0,...
    'bar',0,...
    'powerpoint',0},varargin);

% get axes
a = get(h,'CurrentAxes');
a_set = findall(h, 'type','axes');
% c = get(h,'Children'); %strcmpi(c(t).Type,'axes')

for i = 1:length(a_set)
    init_xtick{i} = get(a_set(i),'xtick');
    init_xticklabel{i} = get(a_set(i),'xticklabel');
    init_ytick{i} = get(a_set(i),'ytick');
    init_yticklabel{i} = get(a_set(i),'yticklabel');
end

if opt.get('heatmap')
    cm = cm_kepler(8);
    cm = cm([1 2 5 7 3],:);
elseif opt.get('bar')
    cm = cm_kepler(8);
    cm([3 4], :) = cm([4 3], :);
    cm = [cm([3 1 2],:); cm(4:end,:)];
elseif opt.get('powerpoint')
    cm = cm_kepler(8);
    cm([3 4], :) = cm([4 3], :);
    cm = [cm(2:end,:); cm(end,:)];
else
    cm = cm_kepler(8);
    cm([3 4], :) = cm([4 3], :);
end

set(h,'Colormap',cm)

%% fond transparent
if opt.get('powerpoint')
    set(gca,'color','white')
elseif ~opt.get('heatmap')
    fond = [245 250 253]/255;
    % set(a,'color','none')
    set(a,'color',fond)
    if opt.get('change_all_axes')
        for t =1:length(a_set) %manage several axes
            set(a_set(t),'color',fond)
        end
    end
    lgd = findall(h, 'type', 'legend');
    set(lgd,'color',fond)
end

%% taille
pos = get(h,'position');
if strcmpi(opt.get('size'),'S')
    % 2 per page
    dim = [5.57 8.36];
elseif strcmpi(opt.get('size'),'M1')
    % powerpoint 4.5 x 7.49
    dim = [5.2 7.6];
elseif strcmpi(opt.get('size'),'M')
    % page width
    dim = [5.57 17.89];
elseif strcmpi(opt.get('size'),'L')
    % landscape
    dim = [12.56 22.77];
end
set(h,'position',[pos(1) pos(2) 37*dim(2) 37*dim(1)]);

%% font
Font = 'Source Sans Pro';
if strcmpi(opt.get('size'),'L')
    FontSize = 9.5;
elseif opt.get('heatmap') || strcmpi(opt.get('size'),'M1')
    FontSize = 6.5;
else
    FontSize = 7.5;
end
if opt.get('heatmap')
    set(a,'FontName',Font,'FontSize',FontSize)
else
    set(a,'FontName',Font,'FontSize',FontSize,'XColor',cm(8,:),'YColor',cm(8,:))
    for t =1:length(a_set) %manage several axes
        set(a_set(t),'FontName',Font,'FontSize',FontSize,'XColor',cm(8,:),'YColor',cm(8,:))
    end
end

txt = findall(h, 'type', 'text');
for t =1:length(txt)
    set(txt(t),'FontName',Font,'FontSize',FontSize,'Color',cm(8,:),'FontWeight', 'Normal')
end

%% powerpoint special features
if opt.get('powerpoint')
    legend Box  off
    legend Orientation horizontal
    legend Location southoutside

    box off
end

%% replace ytick labels after new size
for i = 1:length(a_set)
    if ~isempty(init_xticklabel{i})
        new_xtick = get(a_set(i),'xtick');
        ix_joint = joint(init_xtick{i}',(1:length(init_xtick{i}))',new_xtick');
        if length(init_xticklabel{i})>=length(ix_joint)
            if ~any(isnan(ix_joint))%&&(length(init_xticklabel{i})>=length(ix_joint))
                set(a_set(i),'xticklabel',init_xticklabel{i}(ix_joint))
            else
                set(a_set(i),'xtick',init_xtick{i},'xticklabel',init_xticklabel{i})
            end
        end
    end
    if ~isempty(init_yticklabel{i})
        new_ytick = get(a_set(i),'ytick');
        ix_joint = joint(init_ytick{i}',(1:length(init_ytick{i}))',new_ytick');
        if ~any(isnan(ix_joint))
            set(a_set(i),'yticklabel',init_yticklabel{i}(ix_joint))
        else
            set(a_set(i),'ytick',init_ytick{i},'yticklabel',init_yticklabel{i})
        end
    end
end

%% specified format for ticklabels
xtl = opt.get('xtl');
if ~iscell(xtl)
    xtl = {xtl};
end
for i = 1:length(xtl)
    if isstr(xtl{i})
        xt = get(a_set(end-i+1),'xtick');
        if ~isempty(regexp(xtl{i},'mm'))
            xli = get(a_set(end-i+1),'xlim');
            if strcmpi(opt.get('size'),'S') || strcmpi(opt.get('size'),'M1')
                NBD = 10;
            else
                NBD = 20;
            end
            xt = xli(1):floor((xli(2)-xli(1))/NBD):xli(2);
            set(a_set(end-i+1),'xtick',xt)
            datetick(a_set(end-i+1), 'x', xtl{i}, 'keeplimits', 'keepticks')
            xtickangle(30)
        elseif ~isempty(regexp(xtl{i},'%%'))
            set(a_set(end-i+1),'xticklabel',arrayfun(@(v) sprintf(xtl{i},v), 100*xt, 'uni',0))
        else
            set(a_set(end-i+1),'xticklabel',arrayfun(@(v) sprintf(xtl{i},v), xt, 'uni',0))
        end
    end
end
%if opt.get('powerpoint')
%    set(gca,'XTickLabelRotation',00)
%end

ytl = opt.get('ytl');
if ~iscell(ytl)
    ytl = {ytl};
end
for i = 1:length(ytl)
    if isstr(ytl{i})
        yt = get(a_set(end-i+1),'ytick');
        if ~isempty(regexp(ytl{i},'%%'))
            set(a_set(end-i+1),'yticklabel',arrayfun(@(v) sprintf(ytl{i},v), 100*yt, 'uni',0))
        else
            set(a_set(end-i+1),'yticklabel',arrayfun(@(v) sprintf(ytl{i},v), yt, 'uni',0))
        end
    end
end

%% colormap
if opt.get('change_colors')
    if length(a_set)==2 & all(get(a_set(1),'position')==get(a_set(2),'position')) %plotyy
        h2change = [];
        for i = 1:length(a_set)
            axis_use = a_set(i);
            lin = findall(axis_use, 'type', 'line');
            ba = findall(axis_use, 'type', 'bar');
            ar = findall(axis_use, 'type', 'area');
            pa = findall(axis_use, 'type', 'patch');
            cl = findall(axis_use, 'type', 'ConstantLine');
            h2change = [h2change; flipud(lin); flipud(ba); flipud(ar); flipud(pa); flipud(cl)];
        end
        tot_nb = length(h2change);
        if tot_nb>8
            fprintf('Warning: more than 8 colors to choose, some will have same.\n')
            cm = repmat(cm, ceil(tot_nb/8), 1);
        end
        for h = 1:tot_nb
            if ismember(h2change(h).Type,{'bar','area','patch'})
                set(h2change(h),'EdgeColor',cm(h,:),'FaceColor',cm(h,:))
            else
                set(h2change(h),'Color',cm(h,:))
            end
        end

    else %subplots
        for i = 1:length(a_set)
            axis_use = a_set(i);
            lin = findall(axis_use, 'type', 'line');
            ba = findall(axis_use, 'type', 'bar');
            ar = findall(axis_use, 'type', 'area');
            pa = findall(axis_use, 'type', 'patch');
            cl = findall(axis_use, 'type', 'ConstantLine');
            h2change = [flipud(lin); flipud(ba); flipud(ar); flipud(pa); flipud(cl)];
            tot_nb = length(h2change);
            if tot_nb>8
                fprintf('Warning: more than 8 colors to choose, some will have same.\n')
                cm = repmat(cm, ceil(tot_nb/8), 1);
            end
            for h = 1:tot_nb
                if ismember(h2change(h).Type,{'bar','area','patch'})
                    set(h2change(h),'EdgeColor',cm(h,:),'FaceColor',cm(h,:))
                else
                    set(h2change(h),'Color',cm(h,:))
                end
            end
        end
    end
end

%% grid ?

end