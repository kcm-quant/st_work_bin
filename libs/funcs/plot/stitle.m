function h = stitle( varargin)
% STITLE - title o sprintf
h = title( sprintf(varargin{:}));