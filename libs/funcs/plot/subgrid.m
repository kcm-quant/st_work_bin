function [n,p] = subgrid( mode, varargin)
% SUBGRID - codage des subplots
%  [n,p] = subgrid('grid-size', 12)
%  
% layout = [1 1 1 1; 2 2 2 3; 4 4 4 3]
% [h, pos] = subgrid( 'axes-layout', layout, 'figure', get_focus('test'))
% get_focus('test');f = uipanel('backgroundcolor', [238 238 208]/256, 'borderwidth', 1)
% [h, pos] = subgrid( 'panels-layout', layout, 'parent', f)
n = [];
p = [];
if isnumeric( mode)
    [n,p] = subgrid('grid-size', mode, varargin{:});
    return
end

switch lower(mode)
    
    case 'grid-size'
        %<* Returns only the size of a regular grid
        sz = varargin{1};
        if sz == 3
            n = 3;
            p = 1;
        else
            k = log(sz)/log(2);
            if round(k)==k
                n = 2^ceil(k/2);
                p = ceil(sz/n);
            else
                n = ceil(sqrt(sz));
                p = ceil(sz/n);
            end
        end
        %>*
    case 'axes-layout'
        [n,p] = subgrid('layout', varargin{:});
    case 'panels-layout'
        hf = figure('visible', 'off', 'tag', 'only_for_subgrid_purpose');
        [n,pos] = subgrid('layout', varargin{:}, 'figure', hf);
        close(hf);
        inset = [.2 .18 .04 .1];
        opt = options({'parent', []}, varargin(2:end));
        par = opt.get('parent');
        bgcolor = get(par, 'backgroundcolor');
        bdwidth = get(par, 'borderwidth');
        n = repmat(nan, size(n));
        all_x = [pos(:,1); pos(:,1)+pos(:,3); 0; 1];
        all_y = [pos(:,2); pos(:,2)+pos(:,4); 0; 1];
        for r=1:size(pos,1)
            this_pos = pos(r,:);
            new_pos = [nan, nan, nan, nan];
            
            this_x   = all_x(all_x~=this_pos(1));
            [tmp, i] = min( abs(this_pos(1) - this_x));
            new_pos(1) = (this_pos(1)+this_x(i))/2;
            
            this_x   = all_x(all_x~=this_pos(1)+this_pos(3));
            [tmp, i] = min( abs(this_pos(1)+this_pos(3) - this_x));
            new_pos(3) = (this_pos(1)+this_pos(3)+this_x(i))/2 - new_pos(1);
            
            this_y   = all_y(all_y~=this_pos(2));
            [tmp, i] = min( abs(this_pos(2) - this_y));
            new_pos(2) = (this_pos(2)+this_y(i))/2;
            
            this_y   = all_y(all_y~=this_pos(2)+this_pos(4));
            [tmp, i] = min( abs(this_pos(2)+this_pos(4) - this_y));
            new_pos(4) = (this_pos(2)+this_pos(4)+this_y(i))/2 - new_pos(2);
            
            n(r) = uipanel('backgroundcolor', bgcolor, 'borderwidth', bdwidth, ...
                    'position', new_pos, 'parent', par);
        end
    case 'layout'
        %<* Generic layout
        layout = varargin{1}';
        opt = options({'figure', []}, varargin(2:end));
        
        size_layout = size(layout);
        layout = layout(:);
        n = max(layout);
        hf = opt.get('figure');
        if isempty(hf) || ~ishandle(hf)
            hf = figure;
        else
            figure(hf);
        end
        clf
        h = repmat(nan, n, 1);
        p = repmat(nan, n, 4);
        for i=1:n
            mask = find(layout == i);
            h(i) = subplot(size_layout(2), size_layout(1), mask);
            p(i,:) = get(h(i), 'position');
        end
        
        n = h;
        %>*
        
        
end