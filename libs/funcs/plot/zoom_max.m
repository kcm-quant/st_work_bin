function zoom_max(mode, varargin)
% ZOOM_MAX - zooms on axes, using range of data
% Accepts both axis handle and figure handle (applies to all sub axis)
%
% DONE: extend to 3D graphes (should be easy)
% DONE: use dx, dy, dz for extra offset
%
% TODO : contrairement ? zoom, set
%
% Examples:
%    figure; 
%    subplot(2,2,1:2);plot(1:10,cos((1:10)/10*10*pi));
%    xlim([-1 20]);ylim([-2 2])
%    subplot(2,2,3);plot(1:10,sin((1:10)/10*10*pi));
%    xlim([-1 20]);ylim([-2 2])
%    subplot(2,2,4);plot(1:10,tan((1:10)/10*10*pi));
%    xlim([-1 20]);ylim([-2 2])
%    zoom_max('xy', 'handle' ,gcf);
% 
%    % keeps 50% of data range around data on each side on x-axis
%    zoom_max('xy', 'handle' ,gcf, 'dx',0.5);  
%
%    zoom_max('x', 'handle' ,gcf);
%    zoom_max('y', 'handle' ,gcf);
%
% See also:
%    
%
%   author   : 'nmayo@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '16/11/2011'

mode= lower(mode);
mode= unique(mode); % keeps one occurence of X,Y and Z
opt = options({'handle',gcf,'dx',0,'dy',0,'dz',0,'keeptk',0},...   !!!!!! PUT YOUR DEFAULT VALUES HERE !!!!!!
                varargin);

h= opt.get('handle');

switch lower(get(h,'type'))
    case 'figure'
        l= findobj(h,'type','axes');       
        for sub_axis = (l(:)')     
            %if ~strcmpi(get(sub_axis,'tag'), 'legend')
            if strcmpi(get(sub_axis,'tag'), '')
                zoom_axis(mode,sub_axis,opt );
            end
        end        
    case 'axes'                
        zoom_axis(mode, h,opt);
    otherwise
        error('handle must be axe or figure');
end
    
% NB: mode doit etre constitu? des caract?res {xyz} uniquement
if ~all(arrayfun(@(x)ismember(x,{'x','y','z'}),mode))
    error('zoom_max:mode', 'MODE: <%s> unknown. Mode must be formed on [xyz] only', mode );
end


end

function zoom_axis(mode,a,opt)
% treats all axes of one axis
for name= (mode(:)')
    zoom_on_one_axe(name,a,opt.get(['d' name]),opt.get('keeptk')) ;
end
end

function zoom_on_one_axe(axe_name,a,offset,keeptk)    
    % treats one axe of one axis
    l= findobj(a,'type','line');
    p= get(gca,[ axe_name 'lim']);
    x= arrayfun(@(x) get(x,[ axe_name 'data']),l,'uni',0);
    if isempty(x)
        return
    end
    i= cellfun(@(x) ~isempty(x),x,'uni',1);
    x= x(i);
%     m= cellfun(@(t) min(t(isfinite(t))),x,'uni',1); %cellfun(@min,x,'uni',1); % uni fails if only nans !
%     M= cellfun(@(t) max(t(isfinite(t))),x,'uni',1); %cellfun(@max,x,'uni',1);
    
    m= cellfun(@(t) ftool.remap(min(t(isfinite(t))), nan(1,0),nan),x,'uni',1); %cellfun(@min,x,'uni',1); % uni fails if only nans !
    M= cellfun(@(t) ftool.remap(max(t(isfinite(t))), nan(1,0),nan),x,'uni',1); %cellfun(@max,x,'uni',1);
    
    
    m= min(m);M=max(M);
    
    m= m- offset*(M-m);
    M= M+ offset*(M-m);
    
    if m==M
        if isempty(get(a,[axe_name 'ticklabel'])) % pas de tick INITIALEMENT
            return 
        end
        %m= 0.9*m-0.00000001; M= M*1.1+0.00000001;        
        set(a,[axe_name 'tick'],[m]); 
        tl= num2str(m,2); tl= tl(1:min(4,length(tl)));
        set(a,[axe_name 'ticklabel'],tl);
        return
    end
    try
        set(a,[ axe_name 'lim'],[m,M]);
    catch ME
        if strcmpi(axe_name,'z')
            error('zoom_max(): Z-axis mode requires a 3D figure') ;
        else
            rethrow(ME);
        end
    end     
    %yt= get(a,[axe_name 'lim']);
    yt0= get(a,[axe_name 'tick']);
    if isempty(get(a,[axe_name 'ticklabel'])) % pas de tick INITIALEMENT
       return 
    end
    yt= yt0(yt0>=m & yt0<=M);
    if keeptk; return ;end
    if length(yt)<length(yt0) %isempty(yt)
        % FIXME: solution temporaire pour pas perdre les xticks
        set(a,[axe_name 'ticklabel'],[]); 
        set(a,[axe_name 'tick'],[m M]); 
        tl= {num2str(m),num2str(M)};
        tl={tl{1}(1:min(end,3)),tl{2}(1:min(3,end))};
        set(a,[axe_name 'ticklabel'],tl)
    if length(get(a,[axe_name 'tick'])) < 2  & axe_name=='y'     
       set(gca,[axe_name 'tick'],[m M]);   
       tl= {num2str(m),num2str(M)};
        tl={tl{1}(1:min(end,3)),tl{2}(1:min(3,end))};
        set(a,[axe_name 'ticklabel'],tl)
    end
    end
end

%%
function TEST()
figure; 
subplot(2,2,1:2);plot(1:10,cos((1:10)/10*10*pi));
hold on; plot(-5:15,sin((-5:15)/10*10*pi));

xlim([-1 20]);ylim([-2 2])
subplot(2,2,3);plot(1:10,sin((1:10)/10*10*pi));
xlim([-1 20]);ylim([-2 2])
subplot(2,2,4);plot(1:10,tan((1:10)/10*10*pi));
xlim([-1 20]);ylim([-2 2])
%zoom_max('xy', 'handle' ,gcf);

zoom_max('xy', 'handle' ,gcf,'dx',0.5);

zoom_max('x', 'handle' ,gcf);
zoom_max('y', 'handle' ,gcf);
end
