function score_max = ScoreMaxLog( gaussian_sample, m, s)
% ScoreMaxLog - renvoie la loi empirique des max
%   si gaussian_sample est un scalaire, c'est sa longueure
%
%  scores_valeurs = ScoreMaxLog( gaussian_sample [, m, s])

persistent max_sims

sims_u = 10000;

if isempty(max_sims)
    max_sims = repmat(nan,10,200);
    for i=1:10
        max_sims(i,:) = max(randn(sims_u,200));
    end
end
%%* Loi du max sur le log
% Etude de la loi des max sur les log des volumes
if nargin<2
    [m,s] = estimationRobuste( 'gaussien', gaussian_sample );
end
if numel( gaussian_sample)==1
    sz_lv  = gaussian_sample;
else
    sz_lv  = length(gaussian_sample);
end

idx_sims = ceil( sz_lv / sims_u);

L = size(max_sims,1);
if L < idx_sims
    missing_rows = idx_sims - L;
    max_sims = cat(1, max_sims, ...
        repmat(nan,missing_rows,200) );
    for i=L+1:L+missing_rows
        max_sims(i,:) = max(randn(sims_u,200));
    end
end
% contient les 200 max
E_max     = sort(max(max_sims(1:idx_sims,:),[],1)'*s+m);
E_score   = linspace(0, 100, length(E_max))';
score_max = [E_score E_max];

