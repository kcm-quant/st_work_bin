function D= allDistance(X)
% Calcule les distances entre les lignes de X


[N,P] = size(X);
D=nan(P,P);
for n=1:N
    for m=n:N
        D(n,m)= sqrt(sum( (X(n,:) -X(m,:)).^2 ) );
        D(m,n) = D(n,m);
    end
end
