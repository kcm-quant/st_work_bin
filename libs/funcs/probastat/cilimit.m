function ci_limit = cilimit( varargin)
% CILIMIT calcule des seuils de l'intervalle de confiance de niveau 'alpha' 
% pour un quantile d'ordre 'perct'.
%
% See also proc_univariate
opt = options({'data', [],... % donn�e
    'alpha', 0.05,... % niveau de confiance
    'perct', 10,... % percentile
    'normal', 1,... % normal data?
    }, varargin);

data  = sort(opt.get('data'));
alpha = opt.get('alpha');
perct = opt.get('perct');
norm  = opt.get('normal');
N     = length(data);
x_bar = mean(data);
s_bar = std(data)/sqrt(N);
ci_limit = zeros(length(perct),4);
for p=1:length(perct)
    if ~isempty(data)
        if norm
            if (perct(p) < 50)
                delta = sqrt(N)*norminv(1-perct(p)/100);
            else
                delta = sqrt(N)*norminv(perct(p)/100);
            end
            grt50 = double(perct(p) >= 50);
            % two-sided
            ci_limit(p,1) = x_bar + (2*grt50 - 1)*nctinv(alpha/2,N-1,delta)*s_bar;
            ci_limit(p,2) = x_bar + (2*grt50 - 1)*nctinv(1-alpha/2,N-1,delta)*s_bar;
            % one-sided
            ci_limit(p,3) = x_bar + (2*grt50 - 1)*nctinv(alpha,N-1,delta)*s_bar;
            ci_limit(p,4) = x_bar + (2*grt50 - 1)*nctinv(1-alpha,N-1,delta)*s_bar;
        else
            bino_cdf = binocdf(1:length(data),N,perct(p)/100);
            % two-sided
            milieu = floor(N*perct(p)/100) + 1;
            if milieu == 1
                l = 1;
                u = 2;
                ci_limit(p,1) = data(l);
                ci_limit(p,2) = data(u);

                % one-sided
                ci_limit(p,3) = data(l);
                ci_limit(p,4) = data(1+find(bino_cdf >= 1-alpha, 1, 'first'));
            else
                delta  = 0;
                cont   = true;
                while cont
                    delta = delta + 1;
                    if ((bino_cdf(milieu + delta) - bino_cdf(milieu - delta)) >= 1-alpha)
                        u = 1 + milieu + delta;
                        l = 1 + milieu - delta;
                        cont = false;
                    elseif ((bino_cdf(milieu + delta + 1) - bino_cdf(milieu - delta)) >= 1-alpha)
                        u = 2 + milieu + delta;
                        l = 1 + milieu - delta;
                        cont = false;
                    elseif ((bino_cdf(milieu + delta) - bino_cdf(milieu - delta - 1)) >= 1-alpha)
                        u = 1 + milieu + delta;
                        l = milieu - delta;
                        cont = false;
                    end
                end
                 ci_limit(p,1) = data(l);
                 ci_limit(p,2) = data(u);

                 % one-sided
                 ci_limit(p,3) = data(1+find(bino_cdf <= alpha, 1, 'last'));
                 ci_limit(p,4) = data(1+find(bino_cdf >= 1-alpha, 1, 'first'));
            end           
        end
    end
end