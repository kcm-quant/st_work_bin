function cv = continuous_renorm(cv,idx_auction)
% CONTINUOUS_RENORM - Normalisation of CV not in idx_auction
% Normalisation of the volume curve only over the continuous phase.
% Fixing phases are kept unchanged.

norm = (1-sum(cv(any(idx_auction,2))))/sum(cv(~any(idx_auction,2)));
cv(~any(idx_auction,2)) = norm*cv(~any(idx_auction,2));