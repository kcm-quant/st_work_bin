function rslt = dirichlet(mode, varargin)
switch mode
    case 'fit'
        try
            rslt = dirichlet_fit(varargin{1});
            if any(~isfinite(rslt))
                error('dirichlet:fit:go_to_catch', 'all updates failed');
            end
        catch ME
            if strcmp(ME.message, 'all updates failed')
                st_log('dirichlet_fit failed, so we''ll use dirichlet_fit_simple\n');
                rslt = dirichlet_fit_simple(varargin{1});
                if any(~isfinite(rslt))
                    st_log('dirichlet_fit_simple failed, so we''ll use dirichlet_moment_match\n');
                    rslt = dirichlet_moment_match(varargin{1});
                    if any(~isfinite(rslt))
                        error('dirichlet:fit', 'We tried each method we know to estimate the dirichlet parameters, but we didnt succed\n');
                    end
                end
            else
                rethrow(ME);
            end
        end
    case 'sample'
        rslt = dirichlet_sample(varargin{:});
    case 'll'
        rslt = dirichlet_logProb(varargin{:});
    otherwise
        error('dirichlet:exec', 'Unknown mode : <%s>', mode)
end