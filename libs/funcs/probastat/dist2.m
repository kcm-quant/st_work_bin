function D= dist2(x,y,d)

if nargin<3; d= @(x,y) norm(x-y);end

[P,K]= size(x);
[Q,K_]= size(y);
assert(K==K_);

D= nan(P,Q);
for p=1:P; for q=1:Q
		D(p,q)= d(x(p,:),y(q,:)); % sqrt(sum((x(p,:)-y(q,:)).^2));
	end
end

assert(~any(any(isnan(D))));