function sdata = est_volatility( mode, data, varargin)
% EST_VOLATILITY - estimation de la volatilit�
%       Volatilit� 10 miutes exprim�e en points de base!!!!!!!!!!!!!!!!
%
%
% data_gk = est_volatility('gk:1', st_ans, 'window:date', datenum(0,0,0,0,10,0), ...
%                'step:date',  datenum(0,0,0,0,1,0), 'input-names:char', 'price');
% vol = est_volatility('gk:1', data, 'cotation_group:int', 1, 'step:date', ...
%                datenum(0,0,0,0,10,0));
%
% vol = est_volatility('gk:1', data4vol, 'window:date', ...
% datenum(0,0,0,0,10,0),'step:date', [],'cotation_group:int', 1, 'is_gmt:bool', false);
%
% vol_gk  = est_volatility('gk', value) / sqrt( date(end)-date(1))


switch lower(mode)
    case 'one'
        opt = options({'window:date', datenum(0,0,0,0,10,0), ... % Dur�e de la p�riode sur laquelle on d�termine OHLC
                'step:date',  datenum(0,0,0,0,1,0), ...  % Fr�quence � laquelle on souhaite avoir la volatilit� 
                'input-names:char', 'price', ...         % Nom du champs dans lequel on va r�cup�rer le prix
                'cotation_group:int', [], ...            % Est-ce que l'on souhaite avoir la volatilit� sur une grille temporelle fixe, ce qui comprend : imposer l'heure de la premi�re et de la derni�re volatilit� dans une journ�e, et imposer une transformation des heures GMT en heures locales? Si cette option est utilis�e, alors il faut n�cessairement fournir un cotation_group qui corresponde � ce que l'on peut trouver dans trading_phase_definition
                'sampling-freq:date', datenum(0,0,0,0,5,0), ...
                'is_gmt:bool', false, ...
                }, varargin);
        cg = opt.get('cotation_group:int');
        if isempty(cg)
            cg = 1;
        end
        sdata = get_time_grid('get_returns:last', data, ...
                                'window:date', opt.get('window:date'), ...
                                'step:date', opt.get('step:date'),...
                                'cotation_group:int', cg, ...
                                'is_gmt:bool', opt.get('is_gmt:bool'));
        sdata.value = ones(size(sdata.value, 1), size(sdata.value, 2));
    case 'returns'
        opt = options({'window:date', datenum(0,0,0,0,10,0), ... % Dur�e de la p�riode sur laquelle on d�termine OHLC
                'step:date',  datenum(0,0,0,0,1,0), ...  % Fr�quence � laquelle on souhaite avoir la volatilit� 
                'input-names:char', 'price', ...         % Nom du champs dans lequel on va r�cup�rer le prix
                'cotation_group:int', [], ...            % Est-ce que l'on souhaite avoir la volatilit� sur une grille temporelle fixe, ce qui comprend : imposer l'heure de la premi�re et de la derni�re volatilit� dans une journ�e, et imposer une transformation des heures GMT en heures locales? Si cette option est utilis�e, alors il faut n�cessairement fournir un cotation_group qui corresponde � ce que l'on peut trouver dans trading_phase_definition
                'sampling-freq:date', datenum(0,0,0,0,5,0), ...
                'is_gmt:bool', false, ...
                }, varargin);
        cg = opt.get('cotation_group:int');
        if isempty(cg)
            cg = 1;
        end
        sdata = get_time_grid('get_returns:last', data, ...
                                'window:date', opt.get('window:date'), ...
                                'step:date', opt.get('step:date'),...
                                'cotation_group:int', cg, ...
                                'is_gmt:bool', opt.get('is_gmt:bool'));
        sdata.value = abs(sdata.value);
    case 'gk:1'
        opt = options({'window:date', datenum(0,0,0,0,10,0), ... % Dur�e de la p�riode sur laquelle on d�termine OHLC
                'step:date',  datenum(0,0,0,0,1,0), ...  % Fr�quence � laquelle on souhaite avoir la volatilit� 
                'input-names:char', 'price', ...         % Nom du champs dans lequel on va r�cup�rer le prix
                'cotation_group:int', [], ...            % Est-ce que l'on souhaite avoir la volatilit� sur une grille temporelle fixe, ce qui comprend : imposer l'heure de la premi�re et de la derni�re volatilit� dans une journ�e, et imposer une transformation des heures GMT en heures locales? Si cette option est utilis�e, alors il faut n�cessairement fournir un cotation_group qui corresponde � ce que l'on peut trouver dans trading_phase_definition
                'sampling-freq:date', datenum(0,0,0,0,5,0), ...
                'is_gmt:bool', false, ...
                'grid_mode', 'fix', ...
                }, varargin);
        %<* Garman-Klass estimator
        vals = st_data('col', data, opt.get('input-names:char'));
        wind = opt.get('window:date');
        step = opt.get('step:date');
        if ~isempty(opt.get('cotation_group:int'))
            time_grid = st_grid(opt.get('grid_mode'), 'data', data, 'window:date', wind, ... %'date', data.date,
                                        'step:date', step, ...
                                        'cotation_group:int', opt.get('cotation_group:int'), ...
                                        'is_gmt:bool', opt.get('is_gmt:bool'));
            sigm = repmat(NaN, size(time_grid, 1), 1);
            for i = 1 : size(time_grid, 1)
                if isnan(time_grid(i, 1))
                    if (i == 1)
                        error('�a me gonfle de g�rer cette erreur : pas de donn�es pas de volatilit�!!!');
                    else
                        sigm(i) = sigm(i - 1);
                    end
                else
                    idx = time_grid(i, 1):time_grid(i, 2);
                    if isempty( idx)
                        sigm(i) = nan;
                    else
                        sigm(i) = est_volatility('gk', vals(idx));% * sqrt(datenum(0,0,0,0,10,0)) / sqrt(data.date(time_grid(i, 2)) - data.date(time_grid(i, 1)));
                    end
                end
            end
            dtu = time_grid(:, 3);
        else
            dts  = data.date;
            mit  = min(dts);
            mat  = max(dts);
            sigm = repmat(nan,ceil((mat-mit)/step),1);
            dtu  = repmat(nan,ceil((mat-mit)/step),1);
            n    = 1;
            for t=mit:step:mat-wind
                idx = (dts>=t) & (dts<t+wind);
                if sum( idx)>0
                    t_vals = vals(idx);
                    sigm(n)= est_volatility('gk', t_vals);
                    dtu(n) = t+wind;
                    n=n+1;
                end
            end
            sigm(n:end)=[];
            dtu(n:end) =[];
        end
        % mise en forme des sorties
        sdata = data;
        sdata.value = sigm / sqrt(wind / datenum(0,0,0,0,10,0)) * 10000; %volatilite 10 minutes en bp
        sdata.date  = dtu;
        sdata.colnames = { 'volatility GK (10 minutes expressed in bp)'};
        %>*
    case 'gk'
        %<* Volatilit� de GK
        t_vals = data;
        sdata = sqrt(((max(t_vals)-min(t_vals)).^2/2-(2*log(2)-1)*(t_vals(1)-t_vals(end)).^2) / mean(t_vals).^2);
        %>*
    case 'gk:ohlc'
        %<* Volatilit� de GK sur OHLC
        vals  = data;
        sdata =  sqrt((vals(:,2)-vals(:,3)).^2/2-(2*log(2)-1)*(vals(:,1)-vals(:,4)).^2)./mean(vals,2);
        %>*
    case 'vq:maz:5' % Quadratic Variation : Mykland A�t-Sahalia Zhang : Fifth best approach
        opt = options({'window:date', datenum(0,0,0,0,10,0), ... % Dur�e de la p�riode sur laquelle on d�termine OHLC
                'step:date',  datenum(0,0,0,0,1,0), ...  % Fr�quence � laquelle on souhaite avoir la volatilit� 
                'input-names:char', 'price', ...         % Nom du champs dans lequel on va r�cup�rer le prix
                'cotation_group:int', [], ...            % Est-ce que l'on souhaite avoir la volatilit� sur une grille temporelle fixe, ce qui comprend : imposer l'heure de la premi�re et de la derni�re volatilit� dans une journ�e, et imposer une transformation des heures GMT en heures locales? Si cette option est utilis�e, alors il faut n�cessairement fournir un cotation_group qui corresponde � ce que l'on peut trouver dans trading_phase_definition
                'sampling-freq:date', datenum(0,0,0,0,5,0), ...
                'is_gmt:bool', false, ...
                }, varargin);
        sdata = data;
        data = get_time_grid(   'get_returns:last', data, ...
                                'window:date', datenum(0,0,0,0,0,1), ...
                                'step:date', datenum(0,0,0,0,0,1), ...
                                'cotation_group:int', opt.get('cotation_group:int'), ...
                                'is_gmt:bool', false); %gets returns every one second
        grid = get_time_grid(   'last', data, ...
                                'window:date', opt.get('window:date'), ...
                                'step:date', opt.get('step:date'), ...
                                'cotation_group:int', opt.get('cotation_group:int'), ...
                                'is_gmt:bool', false);
        sdata.date = grid(:, 3);
        sdata.value = zeros(length(sdata.date), 1);
        for i = 1 : size(grid, 1)
            sdata.value(i) = sum(data.value(grid(i, 1) : grid(i, 2)).^2);
        end
        sdata.info.n = opt.get('window:date') / datenum(0,0,0,0,0,1);
    case 'vq:maz:2'
        opt = options({'window:date', datenum(0,0,0,0,10,0), ... % Dur�e de la p�riode sur laquelle on d�termine OHLC
                'step:date',  datenum(0,0,0,0,1,0), ...  % Fr�quence � laquelle on souhaite avoir la volatilit� 
                'input-names:char', 'price', ...         % Nom du champs dans lequel on va r�cup�rer le prix
                'cotation_group:int', [], ...            % Est-ce que l'on souhaite avoir la volatilit� sur une grille temporelle fixe, ce qui comprend : imposer l'heure de la premi�re et de la derni�re volatilit� dans une journ�e, et imposer une transformation des heures GMT en heures locales? Si cette option est utilis�e, alors il faut n�cessairement fournir un cotation_group qui corresponde � ce que l'on peut trouver dans trading_phase_definition
                'sampling-freq:date', datenum(0,0,0,0,5,0), ...
                'is_gmt:bool', false, ...
                }, varargin);
        sec_dt = datenum(0,0,0,0,0,1);
        nb_sec = (data.date(end) - data.date(1)) / sec_dt;
        delta_rec_in_sec = floor(opt.get('sampling-freq:date') / sec_dt);
        nb_rec = floor(nb_sec / delta_rec_in_sec);
        curr_idx = 1;
        ini_dt = data.date(1);
        vals   = zeros(nb_rec, delta_rec_in_sec);
        for i = 1 : nb_rec
            for j = 1 : delta_rec_in_sec
                vals(i, j) = data.value(curr_idx);
                if (ini_dt + ((i - 1) * delta_rec_in_sec + j) * sec_dt >= data.date(curr_idx))
                    curr_idx = curr_idx + 1;
                end
            end
        end
        sdata = mean(sum(diff(log(vals)).^2));
    case 'maz'   %Mykland A�t-Sahalia Zhang : First best approach. Arguments : 'window:date', 'step:date', 'cotation_group:int', 'is_gmt:bool'
        % Fifth best approach part
        opt = options({'window:date', datenum(0,0,0,0,10,0), ... % Dur�e de la p�riode sur laquelle on d�termine OHLC
                'step:date',  datenum(0,0,0,0,1,0), ...  % Fr�quence � laquelle on souhaite avoir la volatilit� 
                'input-names:char', 'price', ...         % Nom du champs dans lequel on va r�cup�rer le prix
                'cotation_group:int', [], ...            % Est-ce que l'on souhaite avoir la volatilit� sur une grille temporelle fixe, ce qui comprend : imposer l'heure de la premi�re et de la derni�re volatilit� dans une journ�e, et imposer une transformation des heures GMT en heures locales? Si cette option est utilis�e, alors il faut n�cessairement fournir un cotation_group qui corresponde � ce que l'on peut trouver dans trading_phase_definition
                'sampling-freq:date', datenum(0,0,0,0,5,0), ...
                'is_gmt:bool', false, ...
                }, varargin);
        wind = opt.get('window:date');
        step = opt.get('step:date');
        cg   = opt.get('cotation_group:int');
        ig   = opt.get('is_gmt:bool');
        if ig
            data.date = gmt('dec-fr', data.date);
        end
        sec_dt = datenum(0,0,0,0,0,1);
        % noise
        vq_all = est_volatility('vq:maz:5', data, ...
                                'window:date', wind, ...
                                'step:date', step, ...
                                'cotation_group:int', cg, ...
                                'is_gmt:bool', false);
        noise = vq_all.value / ( 2 * wind / sec_dt);
        % Second best approach part
        grid = get_time_grid(   'last', data, ...
                                'window:date', wind, ...
                                'step:date', step, ...
                                'cotation_group:int', cg, ...
                                'is_gmt:bool', false);
        vq_avg.date  = grid(:, 3);
        vq_avg.value = zeros(length(vq_avg.date), 1);
        vq_avg.info.n_star = zeros(length(vq_avg.date), 1);
        for i = 1 : size(grid, 1)
            tmp_data.value = data.value(grid(i, 1) : grid(i, 2));
            tmp_data.date  = data.date(grid(i, 1) : grid(i, 2));
            vol   = est_volatility('gk', tmp_data.value) * sqrt(sec_dt / wind);
            n_star_sparse = ( (wind /sec_dt)^2 * vol^4 / (12 * noise(i)^2) )^(1/3); %TODO whats the good formulae?
            if (n_star_sparse > wind / sec_dt)
                warning('Error in est_volatility:maz, optimal frequency too high (delta_time < 1 second)');
                n_star_sparse = floor(wind / sec_dt) - 1;
            elseif n_star_sparse <= 2
                warning('Error in est_volatility:maz, optimal frequency too low (delta_time >= wind)');
                n_star_sparse = 3;
            elseif ~isfinite(n_star_sparse)
                n_star_sparse = vq_avg.info.n_star(i - 1); % Mauvaise fa�on de faire : si i = 1
            end
            vq_avg.value(i) = est_volatility(  'vq:maz:2', tmp_data, ...
                                                'sampling-freq:date', wind / n_star_sparse, ...
                                                'cotation_group:int', cg, ...
                                                'is_gmt:bool', false, ...
                                                'step:date', []);
            vq_avg.info.n_star(i) = n_star_sparse;
        end
        % Bias correction : First best approach
        coeff_all = vq_avg.info.n_star / vq_all.info.n;
        sdata = data;
        sdata.value = sqrt(( vq_avg.value  - coeff_all .* vq_all.value ) ./ (1 - coeff_all)); % sqrt pour avoir un truc plus homog�ne � une volatilit�, mais est-ce de la volatilit�? et pour quel horizon temporel?
        sdata.date = vq_avg.date;
        sdata.info.noise = noise;
        sdata.colnames = {'Volatility:maz'};
    case 'jl' % Estimating Quadratic Variation When Quoted Prices Change by a Constant Increment - Jeremy Large 31/01/2007
        opt = options({'window:date', datenum(0,0,0,0,10,0), ... % Dur�e de la p�riode sur laquelle on d�termine OHLC
                'step:date',  datenum(0,0,0,0,1,0), ...  % Fr�quence � laquelle on souhaite avoir la volatilit� 
                'input-names:char', 'price', ...         % Nom du champs dans lequel on va r�cup�rer le prix
                'cotation_group:int', [], ...            % Est-ce que l'on souhaite avoir la volatilit� sur une grille temporelle fixe, ce qui comprend : imposer l'heure de la premi�re et de la derni�re volatilit� dans une journ�e, et imposer une transformation des heures GMT en heures locales? Si cette option est utilis�e, alors il faut n�cessairement fournir un cotation_group qui corresponde � ce que l'on peut trouver dans trading_phase_definition
                'sampling-freq:date', datenum(0,0,0,0,5,0), ...
                'is_gmt:bool', false, ...
                }, varargin);
        sdata = data;
        grid = get_time_grid('days', data);
        sdata.date  = grid(:, 3);
        sdata.value = zeros(length(sdata.date), size(data.value, 2));
        for j = 1 : size(grid, 1)
            for i = 1 : size(data.value, 2)
                vals = data.value(grid(j, 1):grid(j, 2), i);
                %dts = data.date;
                % Estimating tick size : the Constant Increment 'k'
                id_diff = round(diff(vals) * 10^6) * 10^(-6); % Necessary rounding due to database...
                %d_dts = diff(dts); 
                idxes = id_diff ~= 0 ;%& d_dts < 0.5; % Filtering interday difference : close->open gap has nothing to do with our object of interest here
                id_diff = id_diff(idxes);
                k = min(abs(id_diff)); % this is minimal increment

                % TODO : A few minimal tests like uncorrelated alternation, etc...
                % Lets have a look at the proportion of jumps which are > k :
                proportion_big_jumps = sum(abs(id_diff) > k) / length(id_diff);
                
                c = 0;
                q = ones(length(id_diff) - 1, 1);
                for l = 2 : length(id_diff)
                    if (id_diff(l) * id_diff(l - 1) > 0 )
                        c = c + 1;
                        q(l - 1) = -1;
                    end
                end
                
%                 if abs(sum(q(1 : end - 1) .* q(2:end))) > 2 * sqrt(length(q) - 1) % Approximative test uncorrelated Alternation
%                     sdata.value(j, i) = NaN;
%                     warning('Error in est_volatility, Jeremy Large Method suppose that the price has uncorrelated alternation, but in data <title : %s & colnames : %s> this is not true', data.title, data.colnames{i});
%                 else
                    if (proportion_big_jumps > 0.5)
                        sdata.value(j, i) = NaN;
                        warning('Error in est_volatility, Jeremy Large Method suppose that the price only jumps by a constant increment but the proportion of jumps bigger than tick size is %f %% in data (title : %s & colnames : %s)', 100 * proportion_big_jumps, data.title, data.colnames{i});
                    else
                        if (proportion_big_jumps > 0.05)
                            warning('Warning in est_volatility, Jeremy Large Method suppose that the price only jumps by a constant increment but the proportion of jumps bigger than tick size is %f %% in data (title : %s & colnames : %s)', 100 * proportion_big_jumps, data.title, data.colnames{i});
                        end
                        sdata.value(j, i) = k^2 * (length(id_diff) - 1) * c / (length(id_diff) - 1 - c);
                    end
                %end
            end
        end
    case 'id-jl'
        opt = options({'window:date', datenum(0,0,0,0,10,0), ... % Dur�e de la p�riode sur laquelle on d�termine OHLC
                'step:date',  datenum(0,0,0,0,1,0), ...  % Fr�quence � laquelle on souhaite avoir la volatilit� 
                'input-names:char', 'price', ...         % Nom du champs dans lequel on va r�cup�rer le prix
                'cotation_group:int', [], ...            % Est-ce que l'on souhaite avoir la volatilit� sur une grille temporelle fixe, ce qui comprend : imposer l'heure de la premi�re et de la derni�re volatilit� dans une journ�e, et imposer une transformation des heures GMT en heures locales? Si cette option est utilis�e, alors il faut n�cessairement fournir un cotation_group qui corresponde � ce que l'on peut trouver dans trading_phase_definition
                'sampling-freq:date', datenum(0,0,0,0,5,0), ...
                'is_gmt:bool', false, ...
                }, varargin);
        sdata = data;
        grid = get_time_grid('last', data, varargin{:});
        sdata.date  = grid(:, 3);
        sdata.value = zeros(length(sdata.date), size(data.value, 2));
        for i = 1 : size(data.value, 2)
            % Estimating mininmal increment of price
            id_diff = round(diff(data.value(:, i)) * 10^8) * 10^(-8); % Necessary rounding due to database...
            idxes = id_diff ~= 0;
            id_diff = id_diff(idxes);
            k = min(abs(id_diff)); % this is minimal increment
            for j = 1 : size(grid, 1)
                id_diff = diff(data.value(grid(j, 1):grid(j, 2), i));
                id_diff(id_diff==0) = [];
                c = 0;
                for l = 2 : length(id_diff)
                    if (id_diff(l) * id_diff(l - 1) > 0 )
                        c = c + 1;
                    end
                end
                sdata.value(j, i) = k * sqrt((length(id_diff) - 1) * c / (length(id_diff) - 1 - c));
                if ~isfinite(sdata.value(j, i))
                    sdata.value(j, i) = 0;
                end
            end
            sdata.value = sdata.value / sqrt(opt.get('window:date') / datenum(0,0,0,0,10,0)) * 10000;
            sdata.colnames{i} = ['JL volatility of ' sdata.colnames{i}];
        end
    case 'id-jlcl'
        opt = options({'window:date', datenum(0,0,0,0,10,0), ... % Dur�e de la p�riode sur laquelle on d�termine OHLC
                'step:date',  datenum(0,0,0,0,1,0), ...  % Fr�quence � laquelle on souhaite avoir la volatilit� 
                'input-names:char', 'price', ...         % Nom du champs dans lequel on va r�cup�rer le prix
                'cotation_group:int', [], ...            % Est-ce que l'on souhaite avoir la volatilit� sur une grille temporelle fixe, ce qui comprend : imposer l'heure de la premi�re et de la derni�re volatilit� dans une journ�e, et imposer une transformation des heures GMT en heures locales? Si cette option est utilis�e, alors il faut n�cessairement fournir un cotation_group qui corresponde � ce que l'on peut trouver dans trading_phase_definition
                'sampling-freq:date', datenum(0,0,0,0,5,0), ...
                'is_gmt:bool', false, ...
                }, varargin);
        sdata = data;
        grid = get_time_grid('last', data, varargin{:});
        sdata.date  = grid(:, 3);
        sdata.value = zeros(length(sdata.date), size(data.value, 2));
        for i = 1 : size(data.value, 2)
            % Estimating mininmal increment of price
            id_diff = round(diff(data.value(:, i)) * 10^6) * 10^(-6); % Necessary rounding due to database...
            d_dts = diff(data.date); 
            idxes = id_diff ~= 0 & d_dts < 0.5; % Filtering interday difference : close->open gap has nothing to do with our object of interest here
            id_diff = id_diff(idxes);
            k = min(abs(id_diff)); % this is minimal increment
            for j = 1 : size(grid, 1)
                id_diff = diff(data.value(grid(j, 1):grid(j, 2), i));
                c = 0;
                a = 0;
                for l = 2 : length(id_diff)
                    if (id_diff(l) * id_diff(l - 1) > 0 )
                        c = c + abs(id_diff(l)) / k;
                    else
                        a = a + abs(id_diff(l)) / k;
                    end
                end
                sdata.value(j, i) = k * sqrt((a+c) * c / a);
            end
            sdata.colnames{i} = ['JLCL volatility of ' sdata.colnames{i}];
            sdata.value = sdata.value / sqrt(opt.get('window:date') / datenum(0,0,0,0,10,0)) * 10000;
        end
    otherwise
        error('est_volatility:mode', 'mode <%s> unknown', mode);
end