function varargout = estimationRobuste( mode, varargin)
% ESTIMATIONROBUSTE - estimation robuste des param�tres d'un loi
%
% exemple:
%  [m, sigma] = estimationRobuste('gaussien', randn(800,10), 'quantile-level', .25, 'troncate-level', 1.96)
%  [m, sigma] = estimationRobuste('gaussien', randn(800,10), 'quantile-level', .25, 'troncate-level', .95)
%
% - quantile level is for 1st step variance estimation,
% - troncate level is for truncature

% Author:   'Julien Raza'
% Reviewer: 'Charles-Albert Lehalle'
% Version   '3.0'
% date      '22/08/2008'
switch(lower(mode))
    case {'gaussien','normal'} 
        %<* Hypoth�se gaussienne
        vals = varargin{1};
        opt  = options({'quantile-level', .25, 'troncate-level', 1.96}, varargin(2:end));
        ql   = opt.get('quantile-level');
        %< Estimation robuste de l'esp�rance et de l'�cart type
        m1   = median( vals);
        s1   = diff(quantile(vals, [ql, 1-ql]))/(norminv(1-ql)-norminv(ql));
        %>
        %< Troncature
        tl   = opt.get('troncate-level');
        if tl<1
            tl = norminv( 1-(1-tl)/2);
        end
        idx  = bsxfun(@le, vals, m1-(tl*s1)) | bsxfun( @ge, vals, m1+(tl*s1));
        vtrk = vals;
        vtrk(idx) = nan;
        %>
        %< Estimations post troncature
        m    = nanmedian( vtrk);
        C = 1 - (sqrt(2/pi)*tl./(exp(0.5*(tl^2))*(normcdf(tl)-normcdf(-tl))));
        sigmahat_tronque = nanvar(vtrk);
        s2   = sqrt(sigmahat_tronque/C);
        %>
        varargout = { m, s2};
        %>*
    otherwise
        error('estimationRobuste:mode', 'mode <%s> in unknown', mode)
end
end