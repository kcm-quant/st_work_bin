function [meanMLE, stdMLE] = lognMLE(x)
    lgx     = log(x(x~=0 & isfinite(x)));
    mu      = mean(lgx);
    sigma2  = mean((lgx - mu).^2);
    meanMLE = exp(mu + sigma2/2);
    stdMLE  = (exp(sigma2) - 1) * exp(2 * mu + sigma2);
end