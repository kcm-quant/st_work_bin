function pca_engine = pca(varargin)
% PCA - Scoring Multivariate serie
%     - Denoising serie
%     - Forecasting univariate time serie
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%...           Methods
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% General Methods :
% - get
% - set
% - report
%
% Scoring Multivariate serie:
%
% - estimate(X)
% - compute_score(X)
% - is_regular(q) % q est un quantile toutes les observations avec un score
%                 % inf�rieur � ce quantile seront consid�r�es comme outliers
% - compute_ci(q); %calcul d'un "intervalle de confiance" multivari�, q est
%                   %le niveau de confaince
%
% Denoising Multivariate serie
%
% - filter(X)
%
% Forecasting univariate time series
%
% - forecast(x, nb_step)
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%...           Exemples
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%...Reporting exemple 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% pca_engine = pca('mode_nb_factors', 'bootstrap');
% data = indicateurs_mi('get-mi0', 'security','EDF.PA', 'from', '01/04/2007', 'to', '01/07/2007', 'step', datenum(0,0,0,0,15,0));
% data.title    = regexprep(data.colnames{1}, '.*::([^:]+)::.*', '$1');
% data.colnames = regexprep( data.colnames, '.*:([^:]+)$', '$1');
% z = st_data('keep', data, 'volume');
% z = bin_separator(z);
% pca_engine.estimate(z);
% pca_engine.report();
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%...Scoring exemples 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% pca_engine = pca();
% data = indicateurs_mi('get-mi0', 'security','EDF.PA', 'from', '01/04/2007', 'to', '01/07/2007', 'step', datenum(0,0,0,0,15,0));
% data.title    = regexprep(data.colnames{1}, '.*::([^:]+)::.*', '$1');
% data.colnames = regexprep( data.colnames, '.*:([^:]+)$', '$1');
% z = volume_to_proportion(st_data('keep', data, 'volume'));
% z_bs = bin_separator(z); x= z_bs.value;
% pca_engine.estimate(x);
% s = pca_engine.compute_score(x(end-40:end, :));
% tmp = s >= 0.05;
% tmp2 = pca_engine.is_regular(0.05);
% all(tmp == tmp2)
% datestr(z_bs.date(~tmp))
%
% pca_engine.estimate(x(end-40:end, :));
% s = pca_engine.compute_score(x(end-40:end, :));
% tmp2 = pca_engine.is_regular(0.05);
% all(tmp == tmp2)
% datestr(z_bs.date(~tmp2))
%
% pca_engine = pca('mode_nb_factors', 'fixed_1');
% rho = 0.5;
% nb_data = 1000;
% x = randn(nb_data, 1);
% y = rho * x + sqrt(1 - rho^2) * randn(nb_data, 1);
% plot(x, y, '+');
% pca_engine.estimate([x y]);
% z  = pca_engine.compute_score([x y]);
% ci = pca_engine.compute_ci(0.05);
% hold on
% plot([ci(1, :), ci(1, 1)], [ci(2, :), ci(2, 1)], 'r');
% hold off
% figure;hist(z);
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%...Denoising exemples 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% pca_engine = pca();
% data = indicateurs_mi('get-mi0', 'security','EDF.PA', 'from', '01/04/2007', 'to', '15/07/2007', 'step', datenum(0,0,0,0,15,0));
% data.title    = regexprep(data.colnames{1}, '.*::([^:]+)::.*', '$1');
% data.colnames = regexprep( data.colnames, '.*:([^:]+)$', '$1');
% z = st_data('keep', data, 'volume');
% x = bin_separator(z); x= x.value;
% y = pca_engine.filter(x);
% y = y';
% y = y(1:end);
% plot(1: length(z.value), z.value, 1 : length(z.value), y);title('No denoising : keeping all factors (just factorizing then rebuilding)')
%
% pca_engine.set('mode_nb_factors', 'inertia%_98');
% y = pca_engine.filter(x);
% y = y';
% y = y(1:end);
% figure;plot(1: length(z.value), z.value, 1 : length(z.value), y);title('Denoising as a multivariate daily serie : keeping 98% of inertia')
%
% y = pca_engine.filter(z.value);
% y = y';
% y = y(1:end);
% figure;plot(1: length(z.value), z.value, 1 : length(z.value), y);title('Denoising as a univariate 15min serie : keeping 98% of inertia')
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%...Forecating exemples 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% pca_engine = pca();
%
% nb_data = 20;
% p_data = 1/6;
% nb_data_in = max(6, floor(p_data * nb_data));
% x = 0.5 * (1 + (-1).^(1:nb_data))';
% x_ = x(1:nb_data_in);
% x_= [x_; pca_engine.forecast(x_, nb_data - nb_data_in), ];
% figure;plot(1:nb_data, x_, '-+', 1:nb_data, x, '-o');legend({'forecast', 'original data'});
% y_lim = get(gca, 'ylim'); x_lim = get(gca, 'xlim');x_lim = x_lim(2) - x_lim(1);
% hold on 
% plot([nb_data_in nb_data_in], y_lim, '-r')
% y_lim = y_lim(1) + 0.1 * (y_lim(2) - y_lim(1));
% text(nb_data_in - 0.2 * x_lim, y_lim, 'data in sample', 'color', 'r')
% text(nb_data_in + 0.02 * x_lim, y_lim, 'data out of sample', 'color', 'r')
% hold off
%
%
% nb_data = 20;nb_data_out = 10;
% nb_data_in = nb_data - nb_data_out;
% x = [0; 1];
% for i = 3 : nb_data x(end + 1) = x(end) + x(end - 1); end
% x_ = x(1:nb_data_in);
% x_= [x_; pca_engine.forecast(x_, nb_data - nb_data_in), ];
% figure;plot(1:nb_data, x_, '-+', 1:nb_data, x, '-o');legend({'forecast', 'original data'});
% y_lim = get(gca, 'ylim'); x_lim = get(gca, 'xlim');x_lim = x_lim(2) - x_lim(1);
% hold on 
% plot([nb_data_in nb_data_in], y_lim, '-r')
% y_lim = y_lim(1) + 0.1 * (y_lim(2) - y_lim(1));
% text(nb_data_in - 0.2 * x_lim, y_lim, 'data in sample', 'color', 'r')
% text(nb_data_in + 0.02 * x_lim, y_lim, 'data out of sample', 'color', 'r')
% hold off
%
%
% p_data = 0.5;
% nb_data_in = max(7, floor(p_data * nb_data));
% x = arrayfun(@(i)sin(i)-i/2, 1:nb_data)';
% x_ = x(1:nb_data_in);
% x_= [x_; pca_engine.forecast(x_, nb_data - nb_data_in), ];
% figure;plot(1:nb_data, x_, '-+', 1:nb_data, x, '-o');legend({'forecast', 'original data'});
% y_lim = get(gca, 'ylim'); x_lim = get(gca, 'xlim');x_lim = x_lim(2) - x_lim(1);
% hold on 
% plot([nb_data_in nb_data_in], y_lim, '-r')
% y_lim = y_lim(1) + 0.1 * (y_lim(2) - y_lim(1));
% text(nb_data_in - 0.2 * x_lim, y_lim, 'data in sample', 'color', 'r')
% text(nb_data_in + 0.02 * x_lim, y_lim, 'data out of sample', 'color', 'r')
% hold off
%
%
% nb_data = 42;nb_data_out = 15;
% nb_data_in = nb_data - nb_data_out;
% x = arrayfun(@(i)-(i-4)*(i-2)*(i+1)*(i+5), (0:nb_data - 1)/3-7)';
% x_ = x(1:nb_data_in);
% x_= [x_; pca_engine.forecast(x_, nb_data - nb_data_in), ];
% figure;plot(1:nb_data, x_, '-+', 1:nb_data, x, '-o');legend({'forecast', 'original data'});
% y_lim = get(gca, 'ylim'); x_lim = get(gca, 'xlim');x_lim = x_lim(2) - x_lim(1);
% hold on 
% plot([nb_data_in nb_data_in], y_lim, '-r')
% y_lim = y_lim(1) + 0.1 * (y_lim(2) - y_lim(1));
% text(nb_data_in - 0.2 * x_lim, y_lim, 'data in sample', 'color', 'r')
% text(nb_data_in + 0.02 * x_lim, y_lim, 'data out of sample', 'color', 'r')
% hold off
%
%
% nb_data = 42;nb_data_out = 15;
% nb_data_in = nb_data - nb_data_out;
% x = arrayfun(@(i)i^2*sin(i), (1:nb_data) - nb_data/2)';
% x_ = x(1:nb_data_in);
% x_= [x_; pca_engine.forecast(x_, nb_data - nb_data_in), ];
% figure;plot(1:nb_data, x_, '-+', 1:nb_data, x, '-o');legend({'forecast', 'original data'});
% y_lim = get(gca, 'ylim'); x_lim = get(gca, 'xlim');x_lim = x_lim(2) - x_lim(1);
% hold on 
% plot([nb_data_in nb_data_in], y_lim, '-r')
% y_lim = y_lim(1) + 0.1 * (y_lim(2) - y_lim(1));
% text(nb_data_in - 0.2 * x_lim, y_lim, 'data in sample', 'color', 'r')
% text(nb_data_in + 0.02 * x_lim, y_lim, 'data out of sample', 'color', 'r')
% hold off
%
% nb_data = 180;nb_data_out = 10;
% nb_data_in = nb_data - nb_data_out;
% x_seas = [-0.5; 0; 0.5; 0; 0.6; 1.2; 1.8; 1.2; 0.6; 0];
% x = repmat(x_seas, floor(nb_data/length(x_seas)), 1);
% x = [x; x(1:nb_data - length(x))];
% x = x  + (1:nb_data)' / 20 + randn(nb_data, 1) / 4;%
% x_ = x(1:nb_data_in);
% x_= [x_; pca_engine.forecast(x_, nb_data - nb_data_in)];
% figure;plot(1:nb_data, x_, '-+', 1:nb_data, x, '-o');legend({'forecast', 'original data'}, 'location', 'northwest');
% y_lim = get(gca, 'ylim'); x_lim = get(gca, 'xlim');x_lim = x_lim(2) - x_lim(1);
% hold on 
% plot([nb_data_in nb_data_in], y_lim, '-r')
% y_lim = y_lim(1) + 0.1 * (y_lim(2) - y_lim(1));
% text(nb_data_in - 0.2 * x_lim, y_lim, 'data in sample', 'color', 'r')
% text(nb_data_in + 0.02 * x_lim, y_lim, 'data out of sample', 'color', 'r')
% hold off
%




% data = indicateurs_mi('get-mi0', 'security','EDF.PA', 'from', '01/04/2007', 'to', '15/07/2007', 'step', datenum(0,0,0,0,15,0));
% data.title    = regexprep(data.colnames{1}, '.*::([^:]+)::.*', '$1');
% data.colnames = regexprep( data.colnames, '.*:([^:]+)$', '$1');
% z = st_data('keep', data, 'volume');
% pca_engine = pca();
% x = bin_separator(z); x= x.value;
% [y, tmp] = pca_engine.filter(x);
% y = y'
% y = y(1:end);
% plot(1: length(z.value), z.value, 1 : length(z.value), y);


% figure;autocorr(x, 50);
% trend = mean(diff(x));
% y = x - (1:nb_data)' * trend;
% figure;autocorr(y, 50);
% for i = 1 : length(x_seas)
%   seas(i, :) = mean(y(i : length(x_seas) : end));
% end
% figure; plot(seas);
% seas = repmat(seas, floor(nb_data/length(x_seas)), 1); seas = [seas; seas(1:nb_data - length(seas))];
% y = y - seas;
% figure;autocorr(y);
% 
%
% pca_engine = pca(); 
% nb_data = 150;
% nb_data_out = 1;
% eps = randn(nb_data, 1);
% x = zeros(length(eps) + 1, 1);
% for i = 1 : length(eps) x(i + 1) = 0.8 * x(i) + eps(i); end
% nb_data_in =  nb_data-nb_data_out; x(1) = [];
% for nb_lags = 1 : 10
%     x_ = x(1:nb_data_in);
%     x_= [x_; pca_engine.forecast(x_, nb_data - nb_data_in, nb_lags), ];
%     figure;plot(1:nb_data, x_, '-+', 1:nb_data, x, '-o');legend({'forecast', 'original data'});
%     y_lim = get(gca, 'ylim'); x_lim = get(gca, 'xlim');x_lim = x_lim(2) - x_lim(1);
%     hold on 
%     plot([nb_data_in nb_data_in], y_lim, '-r')
%     y_lim = y_lim(1) + 0.1 * (y_lim(2) - y_lim(1));
%     text(nb_data_in - 0.2 * x_lim, y_lim, 'data in sample', 'color', 'r')
%     text(nb_data_in + 0.02 * x_lim, y_lim, 'data out of sample', 'color', 'r')
%     hold off
% end
%
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Options, settable at building through varargin, %
    % or whenever you want through public method set  %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    opt = options({  'nb_simul', 200 ... if you chose to determine the number of relevant factor by the bootstrap method, then this is the number of resampling which will be used
                    'transformation', @(x)x,... initial transformation before computing pca
                    'mode_nb_factors', 'keep_all', ... how to determine the number of relevant eigen vectors : bootstrap/inflexion/all/keep_all/fixed_%d/inertia%_%f
                    'normalized', true, ... Would you like to make a normalized pca (svd on correlation matrix) or not (svd on var-covar matrix) wont be used for univariate series forecasting
                    }, varargin);
    fun = opt.get('transformation');
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % The object that will be returned %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    pca_engine = struct('estimate', @estimate, ... estimate pca and scores points
                        'compute_score', @compute_score, ... only scores, do not estimate
                        'is_regular', @is_regular, ... only checks if score greater than input, do not estimates nor scores
                        'compute_ci', @compute_ci, ...
                        'filter', @filter, ...
                        'forecast', @forecast_danilov, ... SSA_Rforecasting, etc...
                        'report', @report, ...
                        'get', @get_, ...
                        'set',@set_);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Private fields which may be accessed through get and set %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    this = struct(  'transformation', @(x)arrayfun(fun, x), ...
                    'nb_simul', opt.get('nb_simul'), ...
                    'mode_nb_factors', opt.get('mode_nb_factors'), ...
                    'normalized', opt.get('normalized'), ...
                    ...
                    ... Set in get_mat_to_be_factorised :
                    'data', [], ... original data, except that the inital transformation has been done
                    'N', [], ... number of record
                    'P', [], ... number of variables
                    'mat', [], ... centered matrix, and standardized as well if normalized PCA chosen
                    'mean', [], ... variables mean
                    'std', [], ... variables std, remains empty if non-normalized PCA
                    'mat_to_factorize', [], ... % var-covar matrix, or correlation matrix if normalized PCA
                    ...
                    ... Set in factorize :
                    'factors', [], ... eigen vectors of the matrix to be factorized (var-covar or correlation matrix, depending on wether we want normalized acp or not) 
                    'eigen_values', [], ... of the matrix to be factorized
                    'principal_components', [], ... projection of this.mat on factors = this.mat * this.factors
                    'explained', [], ... percentage of inertia explained if we chose a number of factors => first element is the percentage explained if we choose to use only one factor 
                    ...
                    ... Set in compute_nb_factors
                    'nb_factors', [], ...
                    'bootstrap_ev', [], ... bootstrapped eigen_values
                    ...
                    ... Set in estimate
                    'score', [], ...
                    'normal_pc', [], ...
                    'regular_data', [], ...
                    ...
                    ... Set in st_data_to_values
                    'colnames', [], ...
                    'date', [], ...
                    'title', [], ...
                    'attach_format', [] ...
                    );
                
    %%%%%%%%%%%%%%%%%%%
    % Private Methods %
    %%%%%%%%%%%%%%%%%%%
    
    % function to center, eventually standardize data, and compute
    % var-covar matrix (or correlation matrix if not standardized)
    function get_mat_to_be_factorized(x)
        y = this.transformation(x);
        this.data = y;
        y = y(~any(isnan(y), 2),:);
        this.mean = mean(y);
        [this.N, this.P] = size(y);
        y = y - repmat(this.mean, this.N, 1);
        if this.normalized
            this.std = sqrt(sum(y.^2) / (this.N - 1));
            y = y ./ repmat(this.std, this.N, 1);%ACP normee
        else
            this.std = [];
        end
        this.mat = y;
        this.mat_to_factorize = (y' * y) / (this.N - 1);
    end

    function factorize(x)
        get_mat_to_be_factorized(x);
        [this.factors,D] = svd(this.mat_to_factorize);
        D = diag(D);
        this.eigen_values = D;
        this.explained = cumsum(D /sum(D));
        this.principal_components = this.mat * this.factors;
    end

    function nb_f = compute_nb_factors()
        switch(lower(this.mode_nb_factors))
            case 'bootstrap'
                X_2 = this.mat;
                exp_boots = zeros(this.nb_simul,this.P);
                for i = 1 : this.nb_simul
                    for j = 1 : this.P - 1
                        X_2(:,j) = this.mat(randperm(this.N),j);
                    end
                    S_2 = svd(X_2'*X_2/(this.N - 1));
                    exp_boots(i, :) = S_2';
                end
                this.bootstrap_ev = mean(exp_boots)';
                S_2 = cumsum(this.bootstrap_ev /sum(this.bootstrap_ev));
                [mx nb_f] = max(cumsum(this.eigen_values/sum(this.eigen_values)) - S_2);
                
             case 'bootstrap2'
                 
                pthres=0.1; 
                exp_boots = zeros(this.nb_simul,this.P);
                for i = 1 : this.nb_simul
                    X_tmp=this.mat(randsample(this.N,this.N,true),:);
                    S_2 = svd(X_tmp'*X_tmp/(this.N - 1));
                    exp_boots(i, :) = S_2';
                end
                this.bootstrap_ev = mean(exp_boots)';
                qup = quantile(exp_boots,1-pthres)';
                qdown = quantile(exp_boots,pthres)';
                nb_f=1;
                while nb_f<this.P && qdown(nb_f)>=qup(nb_f+1)
                    nb_f=nb_f+1;
                end
                
            case 'inflexion'
                nb_f =  find(diff(this.eigen_values, 2) < 0, 1, 'first');
            case 'all'
                nb_f = min([compute_nb_factors('bootstrap'); compute_nb_factors('inflexion')]);
            case 'keep_all'
                nb_f = this.P;
            otherwise
                tmp = tokenize(this.mode_nb_factors, '_');
                switch lower(tmp{1})
                    case 'fixed'
                        nb_f = round(str2double(tmp{2}));
                    case 'inertia%'
                        tmp = str2double(tmp{2});
                        if tmp > 1
                            tmp = tmp / 100;
                        end
                        if ~(tmp > 0 && tmp < 1 )
                            error('pca:compute_nb_factors:exec',['Do you really want to keep ' num2str(100 * tmp) ' % of inertia?'])
                        end
                        nb_f = find(this.explained >= tmp, 1);
                    otherwise
                        error('pca:compute_nb_factors:exec', 'mode <%s> unknown', lower(this.mode_nb_factors));
                end
        end
        this.nb_factors = nb_f;
        if isempty(nb_f)
            error('pca:compute_nb_factors:exec', 'There is obviously a problem, no factor has benn kept');
        end
    end

    function filtered_data = filter_univariate(X, varargin)
        N = length(X);
        if nargin > 1
            tau = varargin{1};
            if length(varargin) > 1
                tm_tmp = this.mode_nb_factors;
                this.mode_nb_factors = sprintf('fixed_%d', varargin{2});
            end
        else
            tau = floor(N/2);
        end
        n = N - tau + 1;
        Y = make_lag_matrix(X, tau);
        x = filter(Y);
        filtered_data = zeros(N, 1);
        for s = 1 : tau
            filtered_data(s) = mean(arrayfun(@(i)x(s - i + 1, i), 1:s));
        end
        for s = tau + 1 : n
            filtered_data(s) = mean(arrayfun(@(i)x(s - i + 1, i), 1:tau));
        end
        for s = n + 1 : N
            filtered_data(s) = mean(arrayfun(@(i)x(n - i + 1, i + s - n), 1:N-s+1));
        end
        if length(varargin) > 1
            this.mode_nb_factors = tm_tmp;
        end
    end
    
    function f = hankel_extraction(x)
        [n, tau] = size(x);
        filtered_data = zeros(N, 1);
        for s = 1 : tau
            filtered_data(s) = mean(arrayfun(@(i)x(s - i + 1, i), 1:s));
        end
        for s = tau + 1 : n
            filtered_data(s) = mean(arrayfun(@(i)x(s - i + 1, i), 1:tau));
        end
        for s = n + 1 : N
            filtered_data(s) = mean(arrayfun(@(i)x(n - i + 1, i + s - n), 1:N-s+1));
        end
    end

    function Y = make_lag_matrix(X, tau)
        Y = lagmatrix(X, tau - 1:-1:0);
        Y(any(~isfinite(Y), 2), :) = [];
    end

    function X = st_data_to_values(X, record)
        if isstruct(X)
            if st_data('check', X) && (nargin == 1 || record)
                this.colnames = X.colnames;
                this.date = X.date;
                this.title = X.title;
                try
                    this.attach_format = X.attach_format;
                catch
                    this.attach_format = 'dd/mm/yy';
                end
                X = X.value;
            elseif ~st_data('check', X)
                error('pca:st_data_to_values:exec', 'Incorrect st_data');
            else
                X = X.value;
            end
        else
            this.colnames = [];
            this.date = [];
            this.title = [];
        end
    end

    %%%%%%%%%%%%%%%%%%
    % Public Methods %
    %%%%%%%%%%%%%%%%%%

    function estimate(X)
        X = st_data_to_values(X);
        factorize(X);
        compute_nb_factors();
        idx_nongauss = [];
        for i = 1 : this.P
            try
                if (kstest(this.principal_components(:, i)/sqrt(this.eigen_values(i)), [], 0.05))
                    st_log(['Warning : ' num2str(i) 'th Principal Component isn''t gaussian (kstest, level :' num2str(0.05) ')\n']);
                    idx_nongauss(end + 1) = i;
                end
            catch ME
                st_log('error.identifier : <%s>\n\t', ME.identifier);
                st_log('error.message : <%s>\n', ME.message);
            end
        end
        this.normal_pc = setdiff(1:this.nb_factors, idx_nongauss);
        this.regular_data   = is_regular(0.05);
    end
    
    function filtered_data = filter(X, varargin)
        X = st_data_to_values(X);
        if size(X, 2) == 1
            filtered_data = filter_univariate(X, varargin{:});
            return;
        end
        factorize(X);
        compute_nb_factors();
        filtered_data =  this.principal_components(:, 1:this.nb_factors) * this.factors(:, 1:this.nb_factors)';
        if ~isempty(this.std)
            filtered_data = filtered_data .* repmat(this.std, this.N, 1);
        end
        filtered_data = filtered_data + repmat(this.mean, this.N, 1);
    end
    
    % Must be used after estimate
    function rslt = compute_ci(q) %% TODO evrything, this is just nothing and this is wrong...
        p = 1 - (q ^(1/this.nb_factors)) / 2;
        abs_max = norm_inv(p);
        vals_max = abs_max  * this.factors(1 : this.nb_factors, :)' .* repmat(sqrt(this.eigen_values(1 : this.nb_factors))', this.P, 1);
        vals_min = -vals_max;
        tmp = min(vals_min, vals_max);
        vals_max = max(vals_min, vals_max);
        rslt = [sum(tmp, 2), sum(vals_max, 2)] .* repmat(this.std, 2, 1);
    end
    
    % Must be used after estimate
    function rslt = compute_score(x)
        x = st_data_to_values(x, false);
        if ~isempty(this.factors)
            n = size(x, 1);
            x = this.transformation(x);
            y = x - repmat(this.mean, n, 1);
            if ~isempty(this.std) && this.normalized
                y = y ./ repmat(this.std, n, 1);
            end
            proj_on_factors = y * (this.factors(:, 1:this.nb_factors) ./ repmat(sqrt(this.eigen_values(1:this.nb_factors))', this.P, 1));
            pof_pow2 = proj_on_factors.^2;
            rslt = 1 - chi2cdf(sum(pof_pow2(:, 1 : this.nb_factors), 2), this.nb_factors);
            if this.nb_factors < this.P
                rslt = (rslt .* (1 - chi2cdf(sum(pof_pow2(:,this.nb_factors + 1 : end), 2), this.P - this.nb_factors)) ).^0.5;
            end
            % rslt = 1 - ( (chi2cdf(sum(pof_pow2(:, 1 : this.nb_factors), 2), this.nb_factors) )...
            %   .* (chi2cdf(sum(pof_pow2(:,this.nb_factors + 1 : end), 2), P - this.nb_factors)) ).^0.5;
            rslt(isnan(rslt)) = 0;
            this.score = rslt;
        else
            error('pca:compute_score:exec', 'You should make a pca before asking for scoring, use estimate before compute_score');
        end
    end

    % Must be used after compute_score
    function bool = is_regular(q)
        bool  = this.score >= q;
    end

    % Must be used after estimate/forecast/filter
    function report(varargin)
        opt = options({'pdf_filename', 'pca_report', 'test_levels', 0.05, 'maxcols', 8}, varargin);
        test_levels = opt.remove('test_levels');
        if ~isempty(this.factors)
            filename = opt.remove('pdf_filename');
            lst = opt.get();
            str = '';
            if ~isempty(this.title)
                str = ['de ' this.title ' du ' datestr(this.date(1), this.attach_format) ' au ' datestr(this.date(1), this.attach_format)];
            end
            latex('header', filename, 'title', ['Aide � l''interpr�tation de l''ACP ' str]);
            
            % Matrice de corr�lation (ou de variance-covariance)
            str = 'variances-covariances';
            bm = [];
            if ~isempty(this.std)
                str = 'corr�lations';
                [rho,bm] = corr(this.data);
                bm = bm < test_levels;
            end
            latex('section', filename, ['La matrice de ' str]);
            if ~isempty(this.std)
                latex('text', filename, ['En gras les corr�lations significatives (test de Pearson au niveau ' num2str(100 * test_levels, '%1.1f') '%)']);
            end
            if ~isempty(this.title)
                latex('array', filename, this.mat_to_factorize, 'col_header', this.colnames, 'linesHeader', this.colnames, 'bold_matrix', bm, lst{:});
            else
                tmp = tokenize(['var_1;' sprintf(';var_%d', 2:this.P)], ';');
                latex('array', filename, this.mat_to_factorize, 'col_header', tmp, 'linesHeader', tmp, 'bold_matrix', bm, lst{:});
            end
            
            % Valeurs propres
            latex('section', filename, 'Valeurs propres et inertie expliqu�e');
            figure;
            subplot(2,2,1);plot(this.eigen_values);ylabel('Eigen value');xlabel('factor #');
            subplot(2,2,2);plot(1:length(this.eigen_values), cumsum(this.eigen_values)/sum(this.eigen_values), '-+', 1:length(this.eigen_values), this.eigen_values/sum(this.eigen_values), ':+');ylabel('Inertia proportion');xlabel('factor #');legend({'cumulative proportion', 'Proportion'}, 'location', 'NorthOutside');
            idx = 1:this.nb_factors;
            subplot(2,2,3);plot(this.eigen_values(idx));ylabel('Eigen value');xlabel('factor #');
            subplot(2,2,4);plot(idx, cumsum(this.eigen_values(idx))/sum(this.eigen_values), '-+', idx, this.eigen_values(idx)/sum(this.eigen_values), ':+');ylabel('Inertia proportion');xlabel('factor #');legend({'cumulative proportion', 'Proportion'}, 'location', 'NorthOutside');
            latex('graphic', filename);
            
            %facteurs
            latex('section', filename, 'Facteurs');
            figure;
            plot(this.factors(:, 1:this.nb_factors), '-+');legend(tokenize(['factor_1' sprintf(';factor_{%d}', 2:this.nb_factors), ';']), 'location', 'EastOutside');
            if ~isempty(this.title)
                ylim = get(gca, 'ylim');
                set(gca, 'xtick', 1:this.P, 'xticklabel', []);
                for i = 1 : this.P
                    text(i, ylim(1), this.colnames{i}, 'VerticalAlignment', 'middle', 'rotation', -90);
                end
            end
            latex('graphic', filename);
            for i = 1 : this.nb_factors
                latex('subsection', filename, sprintf('Facteur %d', i));
                figure;
                plot(this.factors(:, i), '-+');title(sprintf('factor_{%d}', i));
                if ~isempty(this.title)
                    set(gca, 'xtick', 1:this.P, 'xticklabel', []);
                    ylim = get(gca, 'ylim');
                    for j = 1 : this.P
                        text(j, ylim(1), this.colnames{j}, 'VerticalAlignment', 'middle', 'rotation', -90);
                    end
                end
                latex('graphic', filename);
            end
            
            % Composantes principales
            latex('section', filename, 'Composantes principales');
            figure;
            if ~isempty(this.title)
                plot(this.date, this.principal_components(:,1:this.nb_factors));legend(tokenize(['pc_1' sprintf(';pc_%d', 2:this.nb_factors), ';']), 'location', 'EastOutside');
                set(gca, 'Xlim', [this.date(1) this.date(end)]); % �trange d'avoir besoin de faire ce set, c'est � cause de la l�gende mais je ne comprends pas pourquoi...
                attach_date('init', 'Format', this.attach_format);
            else
                plot(this.principal_components(:,1:this.nb_factors));legend(tokenize(['pc_1' sprintf(';pc_%d', 2:this.nb_factors), ';']), 'location', 'EastOutside');
            end
            latex('graphic', filename);
            for i = 1 : this.nb_factors
                latex('subsection', filename, sprintf('PC %d', i));
                figure;
                if ~isempty(this.title)
                    plot(this.date, this.principal_components(:, i));title(sprintf('pc_{%d}', i));
                    attach_date('init', 'Format', this.attach_format);
                else
                    plot(this.principal_components(:, i));title(sprintf('pc_{%d}', i));
                end
                latex('graphic', filename);
            end
            nb2plot = min(this.nb_factors, 10);
            figure;
            for i = 1 : nb2plot
                for j = 1 : nb2plot
                    subplot(nb2plot, nb2plot, i + (j - 1) * nb2plot);
                    if i == j
                        hist(this.principal_components(:, i));
                    else
                        plot(this.principal_components(:, i), this.principal_components(:, j), 'o');
                    end
                    xlim = get(gca, 'XLim');
                    ylim = get(gca, 'YLim');
                    if i~=j
                        hold on
                        plot([0 0], ylim, 'k', xlim, [0 0], 'k');
                    end
                    if j == 1
                        title(num2str(i));
                    end
                    if i == 1
                        text(xlim(1), mean(ylim), num2str(j), 'HorizontalAlignment', 'right');
                    end
                    set(gca, 'XTickLabel', []);
                    set(gca, 'YTickLabel', []);
                end
            end
            latex('graphic', filename);
%             latex('subsection', filename, 'Etude de la distribution des composantes principales'); % TODO
            
            % Scatter plots 
            latex('section', filename, 'Projections sur les trois premiers axes factoriels');
            latex('subsection', filename, 'Projection 1er axe, 2eme axe');
            plot_proj(1, 2);
            latex('graphic', filename);
            latex('subsection', filename, 'Projection 1er axe, 3eme axe');
            plot_proj(1, 3);
            latex('graphic', filename);
            latex('subsection', filename, 'Projection 2eme axe, 3eme axe (la valeur sur le premier axe est repr�sent� gr�ce aux couleurs)');
            plot_proj(2, 3, 1);
            latex('graphic', filename);
            
            latex('end', filename);
            latex('compile', filename);
        else
            error('pca:report:exec', 'You should make a pca before asking for report, use estimate/filter/forecast before report');
        end
        
        function plot_proj(nb_axe1, nbaxe2, varargin)
            figure;
            set(gca, 'Xlim', [min(this.principal_components(:, nb_axe1)) max(this.principal_components(:, nb_axe1))]);
            set(gca, 'Ylim', [min(this.principal_components(:, nbaxe2)) max(this.principal_components(:, nbaxe2))]);
            tmp = num2str((1 : this.N)');
            if ~isempty(this.date)
                tmp = datestr(this.date, this.attach_format);
            end
            if nargin == 2
                for count = 1 : this.N
                    text(this.principal_components(count, nb_axe1), this.principal_components(count, nbaxe2), tmp(count, :), 'HorizontalAlignment', 'center');
                end
            else
                cm = hot(256);z_max = max(this.principal_components(:, varargin{1}));z_min = min(this.principal_components(:, varargin{1}));
                for count = 1 : this.N
                    text(this.principal_components(count, nb_axe1), this.principal_components(count, nbaxe2), tmp(count, :), 'HorizontalAlignment', 'center', 'color', cm(1 + floor(255 *(this.principal_components(count, varargin{1}) - z_min )/(z_max - z_min)), :));
                end
            end
        end
    end

    % setters and getters
    function rslt = get_(varargin)
        if nargin == 0
            rslt = this;
        else
            rslt = this.(varargin{1});
        end
    end

    function set_(name, value)
        this.(name) = value;
    end

    %%%%%%% Brouillons de fonctions de pr�diction SSA
    
    function f = basic_SSA_Vforecasting() % page 108-109 de Analysis of Time Series Structure, SSA and Related Techniques
        
    end
    
    function f = SSA_Rforecasting(x, nb_step, L, r) % page 95 de Analysis of Time Series Structure, SSA and Related Techniques
        x = st_data_to_values(x);
        N = length(x);
        if nargin < 3
            L = floor(N / 2);
            r = [];
        end
        X = make_lag_matrix(x, L);
        [P, lambda, tmp] = svd(X'*X);
        lambda = diag(lambda);
        if isempty(r)
            r = sum(lambda > eps * 1e5);
            if r == L
                r = find(cumsum(lambda) > (1 - 1/ L) * sum(lambda));
            end
        end
        P  = P(:, 1:r);
        pi = P(end, :);
        P  = P(1:end-1, 1:r);
        R  = 1 / (1 - sum(pi.^2)) * P * pi';
        if nargin >= 2
            if nb_step > 1
                for i = 1 : nb_step
                    x(end + 1) = R' * x(end - L + 2 : end);
                end
                f = x(end - nb_step + 1:end);
                return;
            elseif nb_step <= 0
                error('nb_step <= 0');
            end
        end
        f  = R' * x(end - L + 2 : end);
    end

    function f = forecast_danilov(x, nb_step, nb_lags, r_) % Principal Components in Time Series Forecast, D. L. Danilov, Journal of Computational and Graphical Statistics, Vol6, No 1 Mars 1997 pp. 112-121
        x = st_data_to_values(x);
        if nargin < 3
            nb_lags = [];
            r_ = [];
        end
        if nargin >= 2
            if nb_step > 1
                for i = 1 : nb_step
                    x(end + 1) = forecast_danilov(x, 1, nb_lags, r_);
                end
                f = x(end - nb_step + 1:end);
                return;
            elseif nb_step <= 0
                error('nb_step <= 0');
            end
        end
        if size(x, 2) ~=1
            if size(x, 1) == 1
                x = x';
            else
                error('pca:estimate_forecast:exec', 'this function is currently implemented only for univariate series');
            end
        end
        N = length(x);
        tn_tmp = this.normalized;
        this.normalized = false;
        if isempty(nb_lags)
            tau = floor(N / 2);
        else
            tau = nb_lags + 1;
        end
        X = make_lag_matrix(x, tau);
        factorize(X);
        this.normalized = tn_tmp;
        if isempty(r_)
            r = sum(this.eigen_values > eps * 1e5);
            if r >= tau / 2
                tmnf_tmp = this.mode_nb_factors;
                this.mode_nb_factors = 'inertia%_99';
                r = compute_nb_factors();
                this.mode_nb_factors = tmnf_tmp;
            end
        else
            r = r_;
        end
        this.nb_factors = r;
        V = this.factors(:, 1:r);
        tmp = arrayfun(@(i)x(i+N-tau+1) - mean(x(i:i+N-tau)), (1:tau-1)');
        v =V(1:tau-1, :);
        H = inv(v' * v) * v' * tmp;
        f = mean(x(tau:N)) +  V(end, :) * H;
    end
end


function xLag = lagmatrix(x , lags)
%LAGMATRIX Create a lagged time series matrix.
%   Copyright 1999-2003 The MathWorks, Inc.   
%   $Revision: 4$   $Date: 05/01/2013 09:21:45 AM$
if nargin ~= 2
    error('pca:lagmatrix:UnspecifiedInput' , ' Inputs ''X'' and ''Lags'' are both required.');
end
if numel(x) == length(x)
   x  =  x(:);
end
if numel(lags) ~= length(lags)
   error('pca:lagmatrix:NonVectorLags' , ' ''Lags'' must be a vector.');
end
lags  =  lags(:);
if any(round(lags) - lags)
   error('pca:lagmatrix:NonIntegerLags' , ' All elements of ''Lags'' must be integers.')
end
missingValue  =  NaN;
nLags =  length(lags);
[nSamples , nTimeSeries] = size(x);
xLag  =  zeros(nSamples , nTimeSeries * nLags);
for c = 1:nLags
    columns  =  (nTimeSeries*(c - 1) + 1):c*nTimeSeries;
    if lags(c) > 0
       xLag(:,columns) = filter([zeros(1,lags(c)) 1] , 1 , x , missingValue(ones(1,lags(c))));
    else
       xLag(:,columns) = x;
    end
end
end