function rslt = polya(mode, varargin)
switch mode
    case 'fit'
        rslt = polya_fit(varargin{1});
        if any(~isfinite(rslt))
            st_log('polya_fit failed, so we''ll use polya_fit_simple\n');
            rslt = polya_fit_simple(varargin{1});
            if any(~isfinite(rslt))
                st_log('polya_fit_simple failed, so we''ll use polya_moment_match\n');
                rslt = polya_moment_match(varargin{1});
                if any(~isfinite(rslt))
                    error('polya:fit', 'We tried each method we know to estimate the polya parameters, but we didnt succed\n');
                end
            end
        end
    case 'sample'
        rslt = polya_sample(varargin{:});
    case 'll'
        rslt = polya_logProb(varargin{:});
    otherwise
        error('polya:exec', 'Unknown mode : <%s>', mode)
end