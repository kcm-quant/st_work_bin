function [qq linefun] = create_data_probplot( data, invcdf, logpp)
% CREATE_DATA_PROBPLOT g�n�re des donn�es pour probplot
%
% Fonction priv�e utilisable seulement par create_probplot
%
% See also create_probplot

% empirical curve
pp   = plotpos(data);
qq   = feval( invcdf, pp);

% reference line
% empirical quantile
% Get upper quartile, or as high up as we can go
P3 = min(.75, pp(end));
DataQ3 = interp1(pp,data,P3);

% Get lower quartile or a substitute for it
P1 = max(P3/3,pp(1));
DataQ1 = interp1(pp,data,P1);

% theoretical quantile
DistrQ = feval( invcdf, [P1 P3]);
if logpp
    DataQ1 = log(DataQ1);
    DataQ3 = log(DataQ3);
end
% the reference line pass two points
% (DataQ1, DistrQ(1)) & (DataQ3, DistrQ(2))
%      A = y1 - x1*(y2-y1)/(x2-x1)
%      B = (y2-y1)/(x2-x1)
B = (DistrQ(2)-DistrQ(1)) ./ (DataQ3-DataQ1);
A = DistrQ(1) - DataQ1 .* B;

if logpp
    % The reference x1 and x2 values are already on the log scale
    linefun = @(x)(A+B*log(x));
else
    linefun = @(x)(A+B*x);
end
end