function create_histogram( hAxes, nb_bins, hData)
% CREATE_HISTOGRAM creates the histogram to the graphic handlehAxes, 
% superimposes the parametric and non-parametric density estimations.
%
% Fonction priv�e utilisable seulement par proc_univariate
% 
% See also proc_univariate
if nargin == 2
    hData = hAxes;
end
t_distribution = getappdata(hData, 'TheoriticalDistribution');
params_hat     = getappdata(hData, 'DistributionParameters');
data           = getappdata(hData, 'Data');

[freq, x_out]  = hist(data, nb_bins);
[p_emp, x_emp] = ksdensity(data);
p_theo         = pdf(t_distribution, x_emp, params_hat{:});
freq           = freq * max(p_emp)/max(freq);

fmt = repmat('%g,',1,length(params_hat));
fmt(end) = [];
paramtext = sprintf(['(' fmt ')'],params_hat{:});
bar( hAxes, x_out, freq);
hold on;
plot(hAxes, x_emp, p_emp, 'b', x_emp, p_theo, 'r', 'linewidth', 2);
hold off;
title(hAxes, sprintf('Histogram - Empirical density - Theoretical density for the %s%s distribution',...
    t_distribution, paramtext));
legend(hAxes, 'Histogram', 'Empirical density', 'Theoretical density');
end
