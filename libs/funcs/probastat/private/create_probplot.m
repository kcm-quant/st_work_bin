function create_probplot( hAxes, hData)
% CREATE_PROBPLOT create the probability plot to the graphic handle
% hAxes
%
% Fonction priv�e utilisable seulement par proc_univariate
% 
% See also proc_univariate
if nargin == 1
    hData = hAxes;
end
t_distribution = getappdata(hData, 'TheoriticalDistribution');
t_quantile     = getappdata(hData, 'TQuantile');
params_hat     = getappdata(hData, 'DistributionParameters');
invcdf         = getappdata(hData,'InverseCdfFunction'); 
logpp          = getappdata(hData, 'LogScale');
alpha          = getappdata(hData, 'Alpha');
data           = getappdata(hData, 'Data');
perct          = getappdata(hData, 'Percentile'); 

data = sort(data);
[qq linefun] = create_data_probplot( data, invcdf, logpp);

fmt = repmat('%g,',1,length(params_hat));
fmt(end) = [];
paramtext = sprintf(['(' fmt ')'],params_hat{:});

title(hAxes, sprintf('Probability plot for %s%s distribution',...
    t_distribution, paramtext));

% Set X axis to log scale if this distribution uses that scale
if logpp
    set(hAxes,'XScale','log');
end

% plot the empirical curve
hdata = line(data,qq,'Parent',hAxes,'linewidth',2);
% Use log X scale if distribution requires it
xmin = min(data(1,:));
xmax = max(data(end,:));
if logpp
    xmin = log(xmin);
    xmax = log(xmax);
    dx = 10^-4 * (xmax - xmin);
    if (dx==0)
        dx = 1;
    end
    xmin = exp(xmin - dx);
    xmax = exp(xmax + dx);
    newxlim = [xmin xmax];
else
    dx = 10^-4 * (xmax - xmin);
    if (dx==0)
        dx = 1;
    end
    newxlim = [xmin-dx, xmax+dx];
end
oldxlim = get(hAxes,'XLim');
set(hAxes,'XLim',[min(newxlim(1),oldxlim(1)), max(newxlim(2),oldxlim(2))]);

% Make sure they have different markers and no connecting line
markerlist = {'x','o','+','*','s','d','v','^','<','>','p','h'}';
set(hdata,'LineStyle','none',...
    {'Marker'},markerlist(1+mod((1:length(hdata))-1,length(markerlist))));

% plot the reference line
href = graph2d.functionline(linefun,'Parent',hAxes,'linewidth',2);
% Start off with reasonable default properties
set(href,'XLimInclude','off','YLimInclude','off','Color','k','LineStyle','--');
% Now that y limits are established, define good y tick positions and labels
hold on
% Display the special values quantile .1 .8 .95 and their confidence limits
ax  = axis;
d   = (ax(4) - ax(3))/10;
lr  = {'right', 'left'};

v_q = prctile(data, perct);
if strcmp(t_quantile,'norm')
    ci_limit = cilimit( 'data', data, 'alpha', alpha, 'perct', perct, 'normal', 1);
elseif strcmp(t_quantile,'logn')
    ci_limit = exp(cilimit( 'data', log(data), 'alpha', alpha, 'perct', perct, 'normal', 1));
else
    ci_limit = cilimit( 'data', data, 'alpha', alpha, 'perct', perct, 'normal', 0);
end

for i=1:3   
    plot(hAxes, repmat(v_q(i),2,1), [ax(3) v_q(i)+d], 'r', 'linewidth', 2);
    text(v_q(i),v_q(i)+d,sprintf('q(%0.f) = %3.5f', perct(i), v_q(i)), 'horizontalalignment', 'center', 'verticalalignment', 'bottom');
    for j=1:4
        v = ci_limit(i,j);
        text(v_q(i),ax(3) + ceil(j/2)*(v_q(i)-ax(3))/3,sprintf(' %3.5f ',v), 'horizontalalignment', lr{2-rem(j,2)}, 'verticalalignment', 'bottom');
    end
end
hold off
set_prob_ticks(hAxes, hData);
xlabel(hAxes,'Data');
ylabel(hAxes,'Probability');
end
