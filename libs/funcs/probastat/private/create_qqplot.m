function create_qqplot( hAxes, hData)
% CREATE_QQPLOT create the Q-Q plot to the graphic handle hAxes
%
% Fonction priv�e utilisable seulement par proc_univariate
% 
% See also proc_univariate
if nargin == 1
    hData = hAxes;
end
t_distribution = getappdata(hData, 'TheoriticalDistribution');
params_hat     = getappdata(hData, 'DistributionParameters');
data           = getappdata(hData, 'Data');

empi = sort(data);
pp   = plotpos(empi);
theo = icdf(t_distribution, pp, params_hat{:});

q1th = prctile(theo,25);
q3th = prctile(theo,75);
q1em = prctile(empi,25);
q3em = prctile(empi,75);
q_th = [q1th; q3th];
q_em = [q1em; q3em];

dx = q3th - q1th;
dy = q3em - q1em;
slope = dy./dx;
centerx = (q1th + q3th)/2;
centery = (q1em + q3em)/2;
maxx = max(theo);
minx = min(theo);
maxy = centery + slope.*(maxx - centerx);
miny = centery - slope.*(centerx - minx);

mx = [minx; maxx];
my = [miny; maxy];

plot( hAxes,theo,empi,'+',q_th,q_em,'-',mx,my,'-.','linewidth',2);
fmt = repmat('%g,',1,length(params_hat));
fmt(end) = [];
paramtext = sprintf(['(' fmt ')'],params_hat{:});

title(hAxes, sprintf('Q-Q plot for %s%s distribution',...
    t_distribution, paramtext));
xlabel(hAxes, 'Theoretical Quantiles');
ylabel(hAxes, 'Empirical Quantiles');
end