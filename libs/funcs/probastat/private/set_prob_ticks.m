function set_prob_ticks( hAxes, hData)
%SET_PROB_TICKS Set the y axis tick marks to use a probability scale
%
% Fonction priv�e utilisable seulement par create_probplot
%
% See also create_probplot    
invcdffunc = getappdata(hData,'InverseCdfFunction');
% Define tick locations
ticklevels = [.0001 .0005 .001 .005 .01 .05 .1 .25 .5 ...
    .75 .9 .95 .99 .995 .999 .9995 .9999];
tickloc = feval(invcdffunc,ticklevels);

% Remove ticks outside axes range
set(hAxes,'YLimMode','auto');
ylim = get(hAxes,'YLim');
t = tickloc>=ylim(1) | tickloc<=ylim(2);
tickloc = tickloc(t);
ticklevels = ticklevels(t);

% Remove ticks that are too close together
j0 = ceil(length(tickloc)/2);
delta = .025*diff(ylim);
j = j0-1;
keep = true(size(tickloc));
while(j>0)
    if tickloc(j) > tickloc(j0)-delta
        keep(j) = false;
    else
        j0 = j;
    end
    j = j-1;
end
j = j0+1;
while(j<length(tickloc))
    if tickloc(j) < tickloc(j0)+delta
        keep(j) = false;
    else
        j0 = j;
    end
    j = j+1;
end

if ~all(keep)
    tickloc = tickloc(keep);
    ticklevels = ticklevels(keep);
end

set(hAxes,'YTick',tickloc,'YTickLabel',ticklevels);
end