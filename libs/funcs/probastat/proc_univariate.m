function o = proc_univariate( varargin)
% PROC_UNIVARIATE analyse une entr�e de type st_data et fournit toutes les
%                 statistiques sur cette entr�e et �galement des tests
%                 statistiques.
%
% Entr�es optionnelles:
% - alpha : niveau de confiance pour des intervalles de confiance
%
% Distributions support�es:
% - 'beta' : Beta distribution
% - 'bino' : Binomial distribution
% - 'exp'  : Exponential distribution
% - 'ev'   : Extreme value distribution
% - 'gam'  : Gamma distribution
% - 'gev'  : Generalized extreme value distribution
% - 'gp'   : Generalized Pareto distribution
% - 'logn' : Lognormal distribution
% - 'nbin' : Negative binomial distribution
% - 'norm' : Normal distribution
% - 'poiss': Poisson distribution
% - 'rayl' : Rayleigh distribution
% - 'unif' : Uniform distribution
% - 'wbl'  : Weibull distribution
%
% Tests support�s:
% - 'chi2': Chi-square goodness-of-fit test
% - 'jb'  : Jarque-Bera test
% - 'ks'  : Kolmogorov-Smirnov test
% - 'lf'  : Lilliefors test
% - 'rdn' : Runs test for randomness
% - 'sr'  : Wilcoxon signed rank test
% - 'sign': Sign test
% - 't'   : One-sample t-test
% - 'var' : Chi-square variance test
%
% data = st_data( 'init', 'title', 'Mes donn�es', 'value', normrnd(0,1,1000,1), ...
% 'date', (1:1000)', 'colnames', { 'V-1'});
% p = proc_univariate( 'data', data, 'alpha', 0.05);
% p.probplot('t_distribution', 'norm');
% p.qqplot('t_distribution', 'norm');
% p.histogram('t_distribution', 'norm', 'nb_bins', 50 );
%
% data = st_data( 'init', 'title', 'Mes donn�es', 'value', wblrnd(3,1,1000,1), ...
% 'date', (1:1000)', 'colnames', { 'V-1'});
% p = proc_univariate( 'data', data, 'alpha', 0.01);
% p.probplot('t_distribution', 'wbl');
% p.qqplot('t_distribution', 'wbl');
% p.histogram('t_distribution', 'wbl', 'nb_bins', 80 );
% p.show('t_distribution', 'wbl', 'nb_bins', 80); 
%
% See also create_probplot create_qqplot create_histogram

opt = options({'data', [], ...
    'alpha', .05, ...    
    }, varargin);

o = struct('histogram', @histogram,...
    'probplot', @probplot,...
    'qqplot', @qqplot,...
    'show', @show,...
    'fit', @fit,...
    'test', @test,...
    'get', @get,...
    'set', @set,...
    'try_get', @try_get,...
    'amend', @amend,...
    'get_idx', @get_idx);

this.assoc = opt.get();
if ~isempty(opt.get('data'))
    init( varargin{:});
end

%%** Embedded functions
%<* INIT - called each time data changed
    function init (varargin)
        opt_it = options({'data', [],...
            }, varargin);
        data = opt_it.get('data');
        if isstruct(data)
            vals = data.value;
        else
            vals = data;
        end
        amend({'N', length(vals),...
            'mean', mean(vals),...
            'median', median(vals),...
            'std', std(vals),...
            'var', var(vals),...
            'kurt', kurtosis(vals),...
            'skew', skewness(vals),...
            'min', min(vals),...
            'max', max(vals)});        
    end

%<* FIT - fit the data to a theoretical distribution
    function [params_hat, params_ci, t_distribution, logpp] = fit( varargin)
        opt_ft = options({'t_distribution', 'norm',...
            'save', 0,...
            }, varargin);
        data   = get('data');
        if isstruct(data)
            data = data.value;
        end
        alpha = get('alpha');
        logpp = 0;
        switch lower(opt_ft.get('t_distribution'))
            case {'normal', 'norm'}
                [muhat,sigmahat,muci,sigmaci] = normfit(data, alpha);
                params_hat = [muhat sigmahat];
                params_ci  = horzcat(muci,sigmaci);
                t_distribution = 'norm';

            case 'beta'
                [phat,pci] = betafit(data,alpha);
                params_hat = phat;
                params_ci  = pci;
                t_distribution = 'beta';

            case {'binomial', 'bino'}
                [phat,pci] = binofit(data,length(data),alpha);
                params_hat = phat;
                params_ci  = pci;
                t_distribution = 'bino';

            case {'exponential', 'exp'}
                [muhat,muci] = expfit(data,alpha);
                params_hat = muhat;
                params_ci  = muci;
                t_distribution = 'exp';

            case {'extreme-value', 'ev'}
                [parmhat,parmci] = evfit(data,alpha);
                params_hat = parmhat;
                params_ci  = parmci;
                t_distribution = 'ev';

            case {'gamma', 'gam'}
                [phat,pci] = gamfit(data,alpha);
                params_hat = phat;
                params_ci  = pci;
                t_distribution = 'gam';

            case {'generalized-extreme-value', 'gev'}
                [parmhat,parmci] = gevfit(data,alpha);
                params_hat = parmhat;
                params_ci  = parmci;
                t_distribution = 'gev';

            case {'generalized-pareto', 'gp'}
                [parmhat,parmci] = gpfit(data,alpha);
                params_hat = parmhat;
                params_ci  = parmci;
                t_distribution = 'gp';

            case {'lognormal', 'logn'}
                [parmhat,parmci] = lognfit(data,alpha);
                params_hat = parmhat;
                params_ci  = parmci;
                t_distribution = 'logn';
                logpp = 1;

            case {'negative-binomial', 'nbin'}
                [parmhat,parmci] = nbinfit(data,alpha);
                params_hat = parmhat;
                params_ci  = parmci;
                t_distribution = 'nbin';

            case {'poisson', 'poiss'}
                [lambdahat,lambdaci] = poissfit(data,alpha);
                params_hat = lambdahat;
                params_ci  = lambdaci;
                t_distribution = 'poiss';

            case {'rayleigh', 'rayl'}
                [phat,pci] = raylfit(data,alpha);
                params_hat = phat;
                params_ci  = pci;
                t_distribution = 'rayl';

            case {'uniform', 'unif'}
                [ahat,bhat,aci,bci] = unifit(data,alpha);
                params_hat = [ahat bhat];
                params_ci  = horzcat(aci,bci);
                t_distribution = 'unif';

            case {'weibull', 'wbl'}
                [parmhat,parmci] = wblfit(data,alpha);
                params_hat = parmhat;
                params_ci  = parmci;
                t_distribution = 'wbl';
                logpp = 1;

            otherwise
                error('proc_univariate:fit:mode <%s> unknown', opt_ft.get('distribution:char'));
        end
        params_hat = num2cell(params_hat,1);
        params_ci  = num2cell(params_ci,1);
        if opt_ft.get('save')
            set('t_distribution', t_distribution);
            set('params_hat', params_hat);
            set('params_ci', params_ci);
            set('logpp', logpp);
        end
    end

%<* PROBPLOT - creates a probability plot, which compare ordered
%variable values with the percentiles of a specified theoretical
%distribution
    function probplot( varargin)
        opt_pp = options({'t_distribution', 'norm',...
            't_quantile', 'norm',...
            'perct', [10 80 95], ...
            'axe', [],...
            }, varargin);
        switch lower(opt_pp.get('t_distribution'))
            case {'beta','bino','exp','ev','gam','gev','gp','logn','nbin','norm','poiss','rayl','unif','wbl'}
                [params_hat params_ci t_distribution logpp] = fit('t_distribution', opt_pp.get('t_distribution'));
                
                hAxes = opt_pp.get('axe');
                if isempty( hAxes)
                    hAxes = cla('reset');
                end
                data = get('data');
                if isstruct(data)
                    data = data.value;
                end
                if logpp
                    switch t_distribution
                        case 'wbl'
                            setappdata( hAxes,'InverseCdfFunction',@(x)icdf( 'ev', x, params_hat{:}));
                        case 'logn'
                            setappdata( hAxes,'InverseCdfFunction',@(x)icdf( 'norm', x, params_hat{:}));
                    end
                else
                    setappdata( hAxes,'InverseCdfFunction',@(x)icdf( t_distribution, x, params_hat{:}));
                end                    
                setappdata( hAxes,'TheoriticalDistribution',t_distribution);
                setappdata( hAxes,'DistributionParameters',params_hat);
                setappdata( hAxes,'LogScale',logpp);
                setappdata( hAxes,'Alpha',get('alpha'));               
                setappdata( hAxes,'Percentile',opt_pp.get('perct'));
                setappdata( hAxes,'Data',data);
                setappdata( hAxes,'TQuantile',opt_pp.get('t_quantile'));
               
                
                create_probplot( hAxes);
            otherwise
                error('proc_univariate:probplot:mode <%s> unknown', opt_pp.get('t_distribution'));
        end
    end
%>*

%<* QQPLOT - creates a quantile-quantile plot compares ordered variable
%values with quantiles of a specifed theoretical distribution
    function qqplot( varargin)
        opt_qq = options({'t_distribution', 'norm',...
            'axe', [],...
            }, varargin);
        switch lower( opt_qq.get('t_distribution'))
            case {'beta','bino','exp','ev','gam','gev','gp','logn','nbin','norm','poiss','rayl','unif','wbl'}
                [params_hat params_ci t_distribution logpp] = fit('t_distribution', opt_qq.get('t_distribution'));

                hAxes = opt_qq.get('axe');
                if isempty( hAxes)
                    hAxes = cla( 'reset');
                end
                data = get('data');
                if isstruct(data)
                    data = data.value;
                end
                setappdata( hAxes,'TheoriticalDistribution',t_distribution);
                setappdata( hAxes,'DistributionParameters',params_hat);
                setappdata( hAxes,'LogScale',logpp);
                setappdata( hAxes,'Data',data);

                create_qqplot( hAxes);
            otherwise
                error('proc_univariate:qqplot:mode <%s> unknown', opt_qq.get('t_distribution'));
        end
    end
%>*

%<* HISTOGRAM - creates histograms, optionally superimposes parametric and
%nonparametric density curve estimates
    function histogram( varargin)
        opt_hm = options({'t_distribution', 'norm',...
            'nb_bins', 10,...
            'axe', [],...
            }, varargin);
        switch lower(opt_hm.get('t_distribution'))
            case {'beta','bino','exp','ev','gam','gev','gp','logn','nbin','norm','poiss','rayl','unif','wbl'}
                [params_hat params_ci t_distribution logpp] = fit('t_distribution', opt_hm.get('t_distribution'));

                hAxes = opt_hm.get('axe');
                if isempty( hAxes)
                    hAxes = cla( 'reset');
                end
                data = get('data');
                if isstruct(data)
                    data = data.value;
                end
                setappdata( hAxes,'TheoriticalDistribution',t_distribution);
                setappdata( hAxes,'DistributionParameters',params_hat);
                setappdata( hAxes,'LogScale',logpp);
                setappdata( hAxes,'Data',data);
                
                nb_bins = opt_hm.get('nb_bins');
                if isnumeric( nb_bins)
                    create_histogram( hAxes, nb_bins);
                else
                    error('proc_univariate:histogram:nb_bins must be numerical');
                end
            otherwise
                error('proc_univariate:histogram:mode <%s> unknown', opt_qq.get('t_distribution'));
        end
    end
%>*

%<* TEST - return the statistic test results
    function test( varargin)
        opt_tt = options({'t_distribution', 'norm',...
            'test_name', 'chi2',...
            }, varargin);
        switch lower( opt_tt.get('t_distribution'))
            case {'norm'}
                
            otherwise
                error('proc_univariate:test:mode <%s> unknown', opt_tt.get('t_distribution'));
        end
    end

    function show( varargin)
        opt_sw = options({'t_distribution', 'norm',...
            't_quantile', 'norm',...
            'perct', [10 80 95], ...
            'nb_bins', 10,...
            }, varargin);
         switch lower(opt_sw.get('t_distribution'))
            case {'beta','bino','exp','ev','gam','gev','gp','logn','nbin','norm','poiss','rayl','unif','wbl'}
                [params_hat params_ci t_distribution logpp] = fit('t_distribution', opt_sw.get('t_distribution'));                
                data = get('data');
                if isstruct(data)                    
                    name = data.title;
                    vals = data.value;
                else                    
                    vals = data;
                    name = 'unknown';
                end
                hAxes = figure('numbertitle', 'off', 'name', sprintf('R�sultats du proc-univariate pour ''%s''', name), 'color', [238 238 208]/256);
                h(1) = subplot(2,2,1);
                plot(h(1),vals);
                title(h(1), sprintf('Data (mean : %2.2f, std : %2.2f, skew : %2.2f, kurt : %2.2f)', get('mean'), get('std'), get('skew'), get('kurt')));
                if logpp
                    switch t_distribution
                        case 'wbl'
                            setappdata( hAxes,'InverseCdfFunction',@(x)icdf( 'ev', x, params_hat{:}));
                        case 'logn'
                            setappdata( hAxes,'InverseCdfFunction',@(x)icdf( 'norm', x, params_hat{:}));
                    end
                else
                    setappdata( hAxes,'InverseCdfFunction',@(x)icdf( t_distribution, x, params_hat{:}));
                end
                setappdata( hAxes,'TheoriticalDistribution',t_distribution);
                setappdata( hAxes,'DistributionParameters',params_hat);
                setappdata( hAxes,'LogScale',logpp);
                setappdata( hAxes,'Alpha',get('alpha'));
                setappdata( hAxes,'Percentile',opt_sw.get('perct'));
                setappdata( hAxes,'Data',vals);
                setappdata( hAxes,'TQuantile',opt_sw.get('t_quantile'));
                
                h(2) = subplot(2,2,2);
                nb_bins = opt_sw.get('nb_bins');
                if isnumeric( nb_bins)
                    create_histogram( h(2), nb_bins, hAxes);
                else
                    error('proc_univariate:show:nb_bins must be numerical');
                end
                
                h(3) = subplot(2,2,3);
                create_probplot( h(3), hAxes);
                
                h(4) = subplot(2,2,4);
                create_qqplot( h(4), hAxes);                
            otherwise
                error('proc_univariate:show:mode <%s> unknown', opt_sw.get('t_distribution'));
        end
    end

%<* Get the index
    function idx = get_idx( k)
        idx = strmatch( lower(k), lower(this.assoc(1:2:length(this.assoc)-1)), 'exact');
    end
%>*
%<* Get the index
    function idx = get_idx_ddot( k)
        idx = strmatch( lower(k), lower(ctokenize(this.assoc(1:2:length(this.assoc)-1), 1, ':')), 'exact');
    end
%>*
%<* Try get
    function [v, idx] = try_get(k, v)
        idx = get_idx( k);
        if ~isempty(idx)
            [v, idx] = get(k, v);
        end
    end
%>*
%<* Get value from key
    function [v, idx] = get( k, varargin)
        if nargin==0
            v   = this.assoc;
            idx = 1:2:length(this.assoc)-1;
            return
        end
        idx = get_idx(k);
        if isempty(idx)
            idx = get_idx_ddot( k);
            if isempty( idx)
                error('options:key', 'key <%s> not available', k);
            end
        end
        if length(idx)>1
            warning('options:key', 'key <%s> has more that one occurence, I take the first one', k);
            idx = idx(1);
        end
        if nargin<2
            v   = this.assoc{idx*2};
        else
            v   = this.assoc{idx*2}(varargin{:});
        end
    end
%>*
%<* Set value from key
    function [v, idx] = set(k, v)
        idx = get_idx(k);
        if isempty(idx)
            this.assoc{end+1} = k;
            this.assoc{end+1} = v;
        elseif length(idx)>1
            error('options:key', 'key <%s> has more that one occurence, impossible to set its value', k);
        else
            this.assoc{idx*2} = v;
            if strcmp( 'data', k)
                init(k, v);
            end
        end
    end
%>*
%<* Modify by a cellarray
    function amend( lst)
        cellfun(@(k,v)set(k,v), lst(1:2:length(lst)-1), lst(2:2:length(lst)) , 'uniformoutput', false);
    end
%>*
end