function ind_bin = quantile_bin(x, nb_bin)
% QUANTILE_BIN - given a nb and a variable which 1/nb-ile am I in?
%
% for a varaible, and a number of bin (eg 10), in which bin to put an
% observation (eg this is the decile)
%
% Examples:
%   x = randn(100,1);
%   ind_bin1 = quantile_bin(x, 10) 
%
% % is equivalent to the following brutal
% % lines:
%
%   quant_ = quantile( x, 0.1:.1:1);
%   ind_bin2 = arrayfun(@(i) (sum(quant_<x(i))), [1:length(x)]')+1;
%
% % and it is also equivalent to this other barbaric method :
% 
%   quant_ = quantile( x, 0.1:.1:1);
%   ind_bin3 = sum( bsxfun(@gt, x, quant_),2)+1;
%
%   mean(ind_bin1 == ind_bin2)
%   mean(ind_bin1 == ind_bin3)
%   mean(ind_bin2 == ind_bin3)
%
% See also quantile histc tiedrank cond2d 
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : 'athabault@cheuvreux.com'
%   version  : '1'
%   date     :  '13/01/2011'

tr = tiedrank(x);
ind_bin =  1+floor(tr/(max(tr)+1)*nb_bin); 
end