function rez = simplereglin( y,x, varargin)
% SIMPLEREGLIN - regression simple
%
% result = simplereglin(y, x, options)
%             a: [3x1 double]
%           var: 8.2395e-004
%            R2: 0.9998
%             F: 2.2679e+005
%       p_value: 0
%         y_hat: []
%     residuals: []
%         const: 1
%        y_mean: ..
%         y_std: ..
%
% result = simplereglin(y, x, 'bootstrap', 100)
%     overall: [1x1 struct]
%     samples: [100x1 struct]
%        plot: @simplereglin/plot_ (a/stats)
%
% then:
%  y_hat = simplereglin( result, x)
% 
% options:
% - bootstrap: []*/nb
% - const    : 0/1* une constante (en premier)
% - residuals: 0*/1
% - hat      : 0*/1 estimateur
% - data     : st_data - dans ce cas x et y sont des noms de colonnes
%
% See also regress test_regress

%%** Apply the model
if isstruct(y)
    model = y;
    if isfield( model, 'overall')
        model = model.overall;
    end
    const = model.const;
    if const
        x = [ones(size(x,1),1),x];
    end
    rez = x * model.a;
    return
end

%%** Build the model
opt   = options({'bootstrap', [], 'const', true, 'residuals', false, 'hat', false, 'data', [] }, varargin);
data  = opt.get('data');
if ~isempty( data)
    x = st_data('col', data, x);
    y = st_data('col', data, y);
end
[boot, bdx] = opt.get('bootstrap');
const       = opt.get('const');
if const
    x = [ones(size(x,1),1),x];
end
if isempty( boot) || boot==0
    %<* On one sample
    [a,a_,r,r_,stats] = regress( y,x);
    rez = struct('a', a, 'std', nanstd( r), ...
        'R2', stats(1), 'F', stats(2), 'p_value', stats(3), ...
        'y_hat', [], 'residuals', [], ...
        'const', const, 'y_mean', mean(y), 'y_std', std(y), 'nonans', sum( ~isnan(r)), 'N', length(r) );
    if opt.get('residuals')
        rez.residuals = r;
    end
    if opt.get('hat')
        rez.y_hat     = y-r;
    end
    %>*
else
    %<* Bootstrap
    opt.set('const', false); % car si besoin est c'est d�j� fait
    opt.remove('bootstrap');
    lst = opt.get();
    bstats = bootstrp(boot, @(a,b)simplereglin(a,b,lst{:}),y,x);
    regr   = simplereglin(y,x,lst{:});
    rez    = struct('overall', regr, 'samples', bstats, 'plot', @plot_);
    if const
        rez.overall.const=const;
        [rez.samples.const]=deal(const);
    end
    %>*
end

%%** Embedded

%<* Plot
function ah = plot_( stat_name, varargin)
    opt_ = options({'figure', []}, varargin);
    h = opt_.get('figure');
    if isempty( h)
        h = figure;
        f = uipanel('backgroundcolor', [238 238 208]/256, 'borderwidth', 0);
    else
        figure(h);
        f = findobj( h,'type', 'uipanel');
        if isempty( f)
            f = uipanel('backgroundcolor', [238 238 208]/256, 'borderwidth', 0);
        end
    end
    ah = [];
    switch lower(stat_name)
        case 'a'
            %< Coef
            main_a = rez.overall.a;
            boot_a = [rez.samples.a];
            D = numel( main_a);
            [n,p] = subgrid( D);
            for d=1:D
                ah = [ah, subplot(n,p, d)];
                set(gca,'parent',f);
                boot_mean = mean(boot_a(d,:));
                [y,x] = ksdensity(boot_a(d,:), 'kernel', 'epanechnikov');
                fill(x,y,'r','facecolor',[.67 0 .12],'edgecolor', 'none');
                ax = axis;
                hold on
                plot(repmat(main_a(d),1,2), ax(3:4), 'k', 'linewidth',2);
                plot(repmat(boot_mean,1,2), ax(3:4), '--k', 'linewidth',2, 'color', [.67 .67 .67]);
                hold off
                stitle('Coef %d : %3.2f mean~%3.2f std~%3.2f', d, main_a(d), boot_mean, std(boot_a(d,:)));
            end
            %>
        case 'stats'
            %< Statistiques
            main_a = [rez.overall.std rez.overall.R2 rez.overall.F rez.overall.p_value];
            boot_a = [rez.samples.std ;rez.samples.R2 ;rez.samples.F ;rez.samples.p_value];
            s_name = { 'Std(residuals)', 'R^2', 'Fisher', 'p_{value}' };
            D = numel( main_a);
            [n,p] = subgrid( D);
            for d=1:D
                ah = [ah, subplot(n,p, d)];
                set(gca,'parent',f);
                boot_mean = mean(boot_a(d,:));
                [y,x] = ksdensity(boot_a(d,:), 'kernel', 'epanechnikov');
                fill(x,y,'r','facecolor',[.67 0 .12],'edgecolor', 'none');
                ax = axis;
                hold on
                plot(repmat(main_a(d),1,2), ax(3:4), 'k', 'linewidth',2);
                plot(repmat(boot_mean,1,2), ax(3:4), '--k', 'linewidth',2, 'color', [.67 .67 .67]);
                hold off
                stitle('%s : %3.2f mean~%3.2f std~%3.2f', s_name{d}, main_a(d), boot_mean, std(boot_a(d,:)));
            end
            %>
        case 'residuals'
            %< Residus
            
            %>
            
        otherwise
            error('simplereglin:plot', 'plot mode <%s> unavailable', stat_name);
    end
end
%>*
end

