function varargout = test_regress( mode, varargin)
% TEST_REGRESS - test de regressions
%
% example:
%  x = randn(200,2);
%  y = x * [1;2] - 3 + randn(200,1)*.01;
%  figure; subplot(2,2,1); plot3(x(:,1),x(:,2),y, '.');
%  subplot(2,2,2); plot(x(:,1), y, '.');
%  subplot(2,2,3); plot(x(:,2), y, '.');
%  r = test_regress( '2vs1', y, x, 100), r.stats
%
%  x1 = randn(200,2); x2 = randn(200,2);
%  y  = [x1 * [1;2]; x2 * [1;2.1]] -3 + randn(400,1)*.26;
%  x  = [x1; x2];
%  figure; subplot(2,2,1); 
%  plot3(x(:,1),x(:,2),y, '.');
%  subplot(2,2,2); plot(x(:,1), y, '.');
%  subplot(2,2,3); plot(x(:,2), y, '.');
%  r1 = test_regress( '2vs1', y, x, 200), r1.stats
%  r2 = test_regress( '2vs1', y, x, 100), r2.stats
%
%  ra = test_regress( '2vs1', y, x, 100:300), ra.stats
%  p=[ra.stats.p];pt=[ra.stats.pt];
%  figure; plot(pt,p,'linewidth',2); ylabel('Proba of cut presence'); xlabel('Cutting point');
%
% web references :
% - <a href="http://www.stata.com/support/faqs/stat/chow.html">Stata FAQ</a>
% - <a href="http://www.citeulike.org/user/lehalle/article/1438525">Article de Chow </a>
%
% See also simplereglin 
switch lower(mode)
    case '2vs1'
        %<* Test one regress vs two regress
        y   = varargin{1};
        x   = varargin{2};
        if size(x,1)~=size(y,1)
            error('test_regress:2vs1', 'X and Y must have the same nb of rows');
        end
        if size(y,2)~=1
            error('test_regress:2vs1', 'Y must have only one column');
        end
        pt  = varargin{3};
        opt = options({ 'cut-specif', 'point', 'date', [], 'constant', true, 'verbose', 1}, varargin(4:end));
        %< Instant de coupe
        % cut-specif can be 'time', but user has to provide a vector of
        % dates
        c_spec = opt.get('cut-specif');
        switch lower(c_spec)
            case 'point'
                cdx = repmat(false, size(y,1), length(pt));
                for p=1:length(pt)
                    cdx(1:pt(p),p) = true;
                end
            case 'date'
                % il y a un arrayfun � appeler
                cdx = bsxfun(@lt, opt.get('date'), pt(:)');
            otherwise
                error('test_regress:2vs1:cut_spec', '<%s> is an unknown cut mode', c_spec);
        end
        %>
        const = opt.get('constant');
        if const
            if opt.get('verbose')>0
                st_log('   adding a column of 1 o X before applying regression\n');
            end
            x = cat(2,ones(size(x,1),1),x);
        end
        %< R�gressions
        [b , b_int , r ] = regress( y, x);
        build_stats = @( ESS, ESS1, ESS2, F, df1, df2, pF, k, N1, N2, t)struct('ESS', ESS, 'ESS1', ESS1, 'ESS2', ESS2, 'F', F, 'df1', df1, 'df2', df2, 'p', pF, 'k', k, 'N1', N1, 'N2', N2, 'pt', t);
        stats       = build_stats({},{},{},{},{},{},{},{},{},{},{});
        for p=1:size(cdx,2)
            [b1, b1_int, r1] = regress( y(cdx(:,p) ,:), x(cdx(:,p) ,:));
            [b2, b2_int, r2] = regress( y(~cdx(:,p),:), x(~cdx(:,p),:));
            %>
            %< Indicateur
            % Somme des carr�s des r�sidus
            ESS  = sum( r.^2);
            ESS1 = sum(r1.^2);
            ESS2 = sum(r2.^2);
            k    = size(x,2);
            N1   = sum( cdx(:,p));
            N2   = sum(~cdx(:,p));
            F    = (ESS - (ESS1 + ESS2))/k * (N1+N2-2*k)/(ESS1 + ESS2);
            df1  = k;
            df2  = N1+N2-2*k;
            pF   = fcdf( F, df1, df2);
            %>
            stats(end+1) = build_stats( ESS, ESS1, ESS2, F, df1, df2, pF, k, N1, N2, pt(p));
        end
        rez   = struct('stats', stats, 'data', struct( 'YX', [y,x], 'idx1', cdx));
        varargout = { rez };
        %>*
    otherwise
        error('test_regress:mode', 'mode <%s> not available', mode);
end