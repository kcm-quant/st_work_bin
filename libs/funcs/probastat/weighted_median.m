function wm = weighted_median(X,W)
% WEIGHTED_MEDIAN - median with weights. Not NaN robust. Operates on rows only
%
% X = randn(1000, 1);
% W = (1 : 1000)';
%
% weighted_median(X,W)
% median(X)
% weighted_median(X,ones(size(X)))
%
% See also median nanmedian

[n, p] = size(X);
[n2, p2] = size(W);
if n ~= n2
    error('weighted_median:check_args', 'Observation and Weights should have the same number of lines');
end
if p > 1
    if p2 == 1
        wm = arrayfun(@(i)weighted_median(X(:, i), W), 1:p, 'uni', true);
        return;
    elseif p == p2
        wm = arrayfun(@(i)weighted_median(X(:, i), W(:, i)), 1:p, 'uni', true);
        return;
    else
        error('weighted_median:check_args', 'Observation and Weights should either have the same number of rows, or weights must be a vector');
    end
end

if n == 1
    wm = X;
    return;
end

% Simple weighted median
[X, idx] = sort(X);
W = W(idx);
pente_post = cumsum(W)-(sum(W)-cumsum(W));
ix_pos_pente = find(pente_post>=0,1);
if pente_post(ix_pos_pente)==0
    wm = mean(X(ix_pos_pente)/2+X(ix_pos_pente+1)/2);
else
    wm = X(ix_pos_pente);
end

% cum_W = cumsum(W);
% sum_W = cum_W(end);
% wm_idx = find(cum_W >= sum_W / 2, 1, 'first');
% if cum_W(wm_idx) == sum_W / 2
%     wm = X(wm_idx);
% elseif wm_idx == 1
%     wm = X(1);
% else
%     wm = (sum(W(1:wm_idx-1)) * X(wm_idx) + sum(W(wm_idx:end)) * X(wm_idx-1)) / sum_W;
% end

% Optimisation
% warning off
% options = optimset('fminunc');
% options = optimset(options, 'Display', 'off');
% wm = fminunc(@(x)sum(W.*abs(X-x)),wm, options);
% warning on