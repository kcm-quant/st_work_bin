function out = weighted_quantile(x,w,dim,varargin)
% WEIGHTED_QUANTILE - Short_one_line_description
%
%
%
% Examples:
% data=randn(10000,1);
% qu = quantile(data,0:0.1:1)
% w_qu = weighted_quantile(data,cumsum(ones(size(data))),0:0.1:1)
%
%
%
% See also:
%
%
%   author   : 'njoseph@cheuvreux.com'
%   reviewer : ''
%   date     :  '04/05/2012'
%
%   last_checkin_info : $Header: weighted_quantile.m: Revision: 3: Author: nijos: Date: 06/01/2012 03:30:58 PM$


out=NaN;

opt = options_light({'handle_nonfinite_weight',true},varargin);

%------------------
%- Check inputs
%------------------
handle_nonfinite_weight=opt.handle_nonfinite_weight;

[n p] = size(x);
[n2 p2] = size(w);


if any(dim>1) || ...
        any(dim<0) || ...
        (n>1 && p>1) || ...
        (~handle_nonfinite_weight && any(~isfinite(w))) || ...
        n~=n2 || p~=p2
    error('weighted_quantile: bad oinputs');
end

is_transpose=false;
if p>1
    x=transpose(x);
    w=transpose(w);
    is_transpose=true;
end

is_finitite=isfinite(x) & isfinite(w);
x=x(is_finitite);
w=w(is_finitite);

if isempty(x)
    return
end


%------------------
%- Compute
%------------------

if length(x)==1
    out=x*ones(length(dim),1);
    if is_transpose
        out=out';
    end
    return
end

[x, idx] = sort(x);
w = w(idx);
cum_W = cumsum(w);
sum_W = cum_W(end);

out=NaN(length(dim),1);

for i_=1:length(dim)
    wm_idx = find(cum_W >= dim(i_)* sum_W, 1, 'first');
    if cum_W(wm_idx) == sum_W / 2
        out(i_) = x(wm_idx);
    elseif wm_idx == 1
        out(i_) = x(1);
    else
        out(i_) = (sum(w(1:wm_idx-1)) * x(wm_idx) + sum(w(wm_idx:end)) * x(wm_idx-1)) / sum_W;
    end
end


if is_transpose
    out=out';
end



end