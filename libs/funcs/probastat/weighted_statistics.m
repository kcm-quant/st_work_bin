function res = weighted_statistics(stats,X,W,varargin)
%weighted_statistics Calculation of basic weighted statistics
%   NaN value are removed, work if X and W have more than one columns.

%   Examples :
% x = randn(10000,2);
% w = [1+2*(x(:,1)>=1) ones(10000,1)];
%  
% weighted_statistics('std',x,w) %x(:,1) weighted by w(:,1) and x(:,2) by w(:,2)
% weighted_statistics('mean',x,w(:,1)) %both weighted by w(:,1) 
% weighted_statistics('median',x,w)
% weighted_statistics('skew',x,w)
% weighted_statistics('kurt',x,w)
% weighted_statistics('std',x,[w(:,1) 1+1*(abs(x(:,2)) < 1)])
% weighted_statistics('quantile',x,w,[0.25 0.5 0.75])
% weighted_statistics('quantile',x,w,[0.25 0.5 0.75]')
% weighted_statistics('quantile',x,w,0.75)


%  author   : 'lmassoulard-ext@cheuvreux.com'
%  reviewer : ''
%  version  : '1'
%  date     :  28/11/2011
%  see also : stat_summary.m


if nargin < 3
    W = ones(size(X));
end

[n p] = size(X);
[n2 p2] = size(W);

if n ~= n2
    error('weighted_statistics:check_args', 'Observation and Weights should have the same number of lines');
end
if p > 1
    if ~isempty(varargin)
        varargin{1} = vertcat(varargin{1}(:)); %force quantile to be vertical if more that one variable
    end
    if p2 == 1 %same weight for each variable
        res = cell2mat(arrayfun(@(i) weighted_statistics(stats,X(:, i),W,varargin{:}), 1:p, 'uni', false));
        return;
    elseif p == p2 %different weight for each variable
        res = cell2mat(arrayfun(@(i) weighted_statistics(stats,X(:, i), W(:, i),varargin{:}), 1:p,'uni', false));
        return;
    else
        error('weighted_statistics:check_args', 'Observation and Weights should either have the same number of rows, or weights must be a vector');
    end
end

if n == 0
   res = NaN;
   return;
end


%HERE X and W have only one column

ii = isfinite(X) & isfinite(W);
if ~any(ii)
   res = NaN;
   return;
end
X = X(ii);
W = W(ii);


switch stats
    
    case 'mean'
        res = sum( X .* W) / sum(W);
        
    case 'median'
        res = weighted_statistics('quantile',X,W,0.5);
        return;
        
    case 'std'
        sW = sum(W);
        res = sqrt(  max(sum(  X.*X.*W ) / sW - (sum(X.*W)/sW) ^ 2 , 0 ));  %max(. , 0) to avoid small negative value due to calcul errors
        
    case 'var'
        sW = sum(W);
        res = max(sum(X.*X.*W)/sW - (sum(X.*W)/sW) ^ 2 ,0);
        
    case 'skew'
        sW = sum(W);
        mx = weighted_statistics('mean',X,W);
        sd = weighted_statistics('std',X,W);
        %Rmk : could be done more efficiently.
        X = (X - mx) / sd;
        res = sum( W .* X .* X .* X ) / sW;
        
    case 'kurt'
        %Rmk : could be done more efficiently.
        sW = sum(W);
        mx = weighted_statistics('mean',X,W);
        sd = weighted_statistics('std',X,W);
        
        X = (X - mx) / sd;
        res = sum( W .* X .* X .* X .* X) / sW;
        
    case 'quantile'
        q = varargin{1};
        
        res = nan(size(q));

        [X, idx] = sort(X);
        W = W(idx);
        
        cum_W = cumsum(W);
        sum_W = cum_W(end);
        
        for i = 1:size(q,1)
            for j = 1:size(q,2)
                wm_idx = find(cum_W >= sum_W * q(i,j), 1, 'first'); 
                if cum_W(wm_idx) == q(i,j) * sum_W
                    res(i,j) = X(wm_idx);
                elseif wm_idx == 1
                    res(i,j) = X(1);
                else
                    res(i,j) = (sum(W(1:wm_idx-1)) * X(wm_idx) + sum(W(wm_idx:end)) * X(wm_idx-1)) / sum_W;
                end
            end 
        end
        
    case 'error' 
        sW = sum(W);
        res = 1.96 * sqrt(  max(sum(  X.*X.*W ) / sW - (sum(X.*W)/sW) ^ 2,0) ) / sqrt(length(X));
        
    otherwise
        error('weighted_statistics:mode', 'statistic <%s> unknown', stats);
        
end


end

