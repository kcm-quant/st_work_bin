function create_sec_id_list_for_scenario(output_filename)


Securities = get_index_comp('CAC40 UNION DAX UNION IBEX35 UNION FTSE 60 intersect fobu', 'recent_date_constraint' , false);
All_TD = [];
All_RIC = [];
for i = 1 : length(Securities)
    [td_id,rics] =  get_rics_and_tdid(Securities(i));
    All_TD = [All_TD;td_id];
    All_RIC = [All_RIC;rics];
end


fid = fopen(output_filename,'w');
for i = 1 : length(All_TD);
    fprintf(fid,'%s \t %d \n',All_RIC{i},All_TD(i));
end
fclose(fid);
