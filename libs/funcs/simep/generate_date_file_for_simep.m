function generate_date_file_for_simep(from, to,  output_filename,varargin)
%%
%generate_date_file_for_simep('01/01/2009', today - 1, ...
% 'simep_weekly_input.txt', 'frequency','weekly','day_in_the_week',5);
the_days =  generate_time_frame_for_simep_backtest(from, to,  varargin{:});
txt_days = cellstr(datestr(the_days','yyyymmdd'));

fid = fopen(output_filename,'w');
for i = 1 : length(txt_days);
    fprintf(fid,'%s\n',txt_days{i});
end
fclose(fid);