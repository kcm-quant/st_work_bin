function generate_stock_params_for_observer(input_file,output_file,txt_date,indicators_list)

all_cac_data = dataset('xlsfile',input_file);
fid = fopen(output_file,'w');

fprintf(fid,'\nfrom simep.scenarii.metascenario import MetaScenario');
fprintf(fid,'\nimport datetime');

fprintf(fid,sprintf('\nsimulation_lowlevel_file = ''C:/st_repository/simep_scenarii/gui/SimepRunner_CAC40_%s_slave.py''',txt_date));
fprintf(fid,'\nstsim_xml_file = ''C:/st_sim/simep/st_sim.xml''');
fprintf(fid,'\n\n');

fprintf(fid,'\nengine_params = {');
fprintf(fid,'\n''number_of_bt''           : 10,');
fprintf(fid,'\n''full''                   : True');
fprintf(fid,'\n}');


fprintf(fid,'\n');
fprintf(fid,'\n');

for i = 1 : length(all_cac_data)
       fprintf(fid,'\nstock_params_0%d = {',i);
       fprintf(fid,'\n''date''                    : ''%s'',',txt_date);
       fprintf(fid,'\n''trading_destination_id''  : %d,',all_cac_data.TD(i));
       fprintf(fid,'\n''input_file_name''         : ''Y:/tick_ged'',');
       fprintf(fid,'\n''data_type''               : ''TBT2'',');
       fprintf(fid,'\n''trading_destination_name'': ''ENPA'',');
       fprintf(fid,'\n''security_id''             : %d,',get_sec_id_from_ric(deblank(all_cac_data.RIC{i})));
       fprintf(fid,'\n''ric''                     : ''%s'',',deblank(all_cac_data.RIC{i}));
       fprintf(fid,'\n}');
       fprintf(fid,'\n');
       fprintf(fid,'\n');
       fprintf(fid,'\n');
end

fprintf(fid,'\n\n\n# Stock parameters' ); 

for i = 1 : length(all_cac_data)
    fprintf(fid,'\nStockObserver0%d_params = {',i);
    fprintf(fid,sprintf('\n''setup''      : {''indicators''             : ''%s'',', indicators_list)); 
    fprintf(fid,'\n                  ''name''                   : ''StockObserver%d''},',i);
    fprintf(fid,'\n''context''    : {''date''                   : ''%s'',', txt_date);
    fprintf(fid,'\n                  ''security_id''            : %d,', get_sec_id_from_ric(deblank(all_cac_data.RIC{i})));
    fprintf(fid,'\n                  ''trading_destination_id'' : %d,',all_cac_data.TD(i));
    fprintf(fid,'\n                  ''ric''                    : ''%s''},',deblank(all_cac_data.RIC{i}));
    fprintf(fid,'\n''parameters'' :  {''plot_mode''             : 0,');
    fprintf(fid,'\n                   ''print_orderbook''       : False,');
    fprintf(fid,'\n                   ''save_into_file''        : False}');
    fprintf(fid,'\n}');
    fprintf(fid,'\n');
end

 fprintf(fid,'\n\n\n# Scenario definition');
 fprintf(fid,'\nmy_scenario = MetaScenario(stsim_xml_file, simulation_lowlevel_file)');
 fprintf(fid,'\nmy_scenario.SetEngine(''ROBModel'', engine_params)');

str_set_universe = sprintf('\n\nmy_scenario.SetUniverse({''%s'' :[ ',txt_date);
for i = 1 : length(all_cac_data)
    str_set_universe  = [ str_set_universe  , sprintf('stock_params_0%d,',i)];
end
str_set_universe(end) = ']';
str_set_universe = [str_set_universe,'})\n\n'];


 fprintf(fid,'\n\n');
 fprintf(fid,str_set_universe);



for i = 1 : length(all_cac_data)
   fprintf(fid,'\nmy_scenario.AddTrader(''StockObserver'', StockObserver0%d_params)',i);
end

  fprintf(fid,'\nmy_scenario.GenerateAndRunSimulations(''C:/st_repository/simep_scenarii/gui'')');


fclose(fid);

