function days_list = generate_time_frame_for_simep_backtest(from, to,  varargin ) 
%% GENERATE_TIME_FRAME_FOR_SIMEP_BACKTEST
%   generate a list of dates corerspondig to the asked criterion
%   week_pos_in_the_month is the nth occurrence of the desired weekday, (an integer from 1 to 5),
%   day_in_the_week is the weekday, (1 through 7 equal Sunday through Saturday)
% example :
% weekly
% generate_time_frame_for_simep_backtest(datenum('01/01/2009'), today - 1, 'frequency','weekly','day_in_the_week',5)
% monthly :
% generate_time_frame_for_simep_backtest(datenum('01/01/2009'), today - 1,
%  'frequency','monthly','week_pos_in_the_month', 2,'day_in_the_week',5)

optargin = size(varargin(:),1);
switch optargin 
    case 4 
        opt = options( {'frequency', 'monthly','day_in_the_week', 4}, varargin);
    case 6 
        opt = options( {'frequency', 'monthly','week_pos_in_the_month', 2, 'day_in_the_week', 4}, varargin);
    otherwise
        st_log('\nWrong parameters. Pelase checked the provide examples');
        days_list = [];
        return;
end

mode = opt.get('frequency');

switch mode
    case 'monthly'
        week_pos_in_the_month = opt.get('week_pos_in_the_month');
        day_in_the_week = opt.get('day_in_the_week');
        days_list = generate_monthly_timeframe(from,to,week_pos_in_the_month,day_in_the_week);
    case 'weekly'
        day_in_the_week = opt.get('day_in_the_week');
        days_list = generate_weekly_timeframe(from,to,day_in_the_week);
    otherwise 
        st_log('this mode is not yet supported')
end


end

function days_list = generate_monthly_timeframe(from,to,week_pos_in_the_month,day_in_the_week)
n = week_pos_in_the_month;      % 4rd date
theDay = day_in_the_week; % Wednesday (1-7 = Sun-Sat)


from_year = year(from);
to_year   = year(to);
years = from_year : to_year;
from_month = month(from);
to_month   = month(to);

nb_years =length(years);
my_months = []; %#ok

my_months = NaN(nb_years,12);

if nb_years == 1
    my_months(1,from_month : to_month) = from_month : to_month;
else
    my_months(1,from_month : 12) = from_month : 12 ; 
    for i = 2 : nb_years - 1
        my_months(i,:) = [1 : 12]; %#ok
    end
    my_months(nb_years,1 : to_month) = [1 : to_month]; 
end

days_list = [];
for i = 1 : length(years)
    year_days = nweekdate(n, theDay, years(i), my_months(i,~isnan(my_months(i,:))));
    days_list = [days_list, year_days]; %#ok
end

% post processing : check if all the dates are business dates

for z = 1 : length(days_list)
    if ~isbusday(days_list(z))
        st_log('one of the date is not a business daate : ', datestr(days_list(z)));
    end
end

    
    

end
        
        

function days_list = generate_weekly_timeframe(from,to,day_in_the_week) 

theDay = day_in_the_week; % Wednesday (1-7 = Sun-Sat)
n = 1; % First week

from_year = year(from);
from_month = month(from);

Start_Date = nweekdate(n, theDay,from_year, from_month);
days_list = Start_Date : 7 : to;

end
    
    