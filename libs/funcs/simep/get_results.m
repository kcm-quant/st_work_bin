function R = get_results()

directory = 'C:/st_repository/simep_scenarii/gui/';
n = length(directory);

fid = fopen('C:/st_repository/simep_scenarii/gui/last_simulation_mat_files.txt');
tmp = textscan(fid,'%s');
files = tmp{1};
fclose(fid);
R = struct;

for i=1:length(files);
    file = files{i}(n+1:end);
    fields = textscan(file,'%s %s %s %s %s', 'delimiter','\\');    
    model = fields{1}{1};
    class = fields{2}{1};
    stock = fields{3}{1};
    date  = fields{4}{1};
    R.(model).(class).(stock).(date) = load(files{i});
end


end