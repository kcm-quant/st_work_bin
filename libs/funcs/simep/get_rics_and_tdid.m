function [td_id,rics] =  get_rics_and_tdid(sec_id)

sql_source = 'select trading_destination_id, reference  from repository..security_source where security_id = %d and  source_id = 2';
res = exec_sql('BSIRIUS',sprintf(sql_source, sec_id));

td_id = cell2mat(res(:,1));
rics = res(:,2);




