function f = slicer_lib( mode, varargin)
% SLICER_LIB - slicer library
%
% available slicers:
% - mavg
% - last
% - vol_gk
% - flow
%
% mavg = slicer_lib('mavg')

switch lower(mode)
    case 'mavg'
        % moving average
        f = build_slicer( 'moving-avg', @(x)mean(x) );
    case 'last'
        f = build_slicer( 'last', @(x)x(end) ); 
    case 'vol_gk'
        f = build_slicer( 'vol_gk', ...
            @(x,t)10000*sqrt( ((max(x)-min(x)).^2/2-(2*log(2)-1)*(x(1)-x(end)).^2) )/mean(x) * sqrt(1/(8.5*6*(t(end)-t(1)))), ...
            'c-func', { 'vol_gk'} );
    case 'flow'
        f = build_slicer_flow;
    otherwise
        error('slicer_lib:unknown', 'unknown slicer <%s>, please update file <%s>', mode, which(slicer_lib));
end
