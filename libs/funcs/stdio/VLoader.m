function self= VLoader(varargin)
% Load Volumes for Contextual Volume Forecast
% PROJECT: context sensitive volume forecast
% Goals:
% - get learning volume (15 min usually)
% - get full intraday data (vwap, GK, spread and volume) for operational criteria
%
% load one stock
% principe: */ cr?e le full buffer (plutot sur U pour commencer)
%           */ selectionne une aggreg
%
% TODO: buffer index (getbin restera stock ? stock)

if nargin== 1 & strcmpi(varargin{1},'tbx');
else
    tbx= newsProjectTbx(varargin{:});
end


%% Object
% Identity
self= [];
self.classe = 'VLoader.m';
% Estimation parameters
self.opt= options({},varargin);
self.bufferize= @bufferize;
self.bufferizeAll= @bufferizeAll;
self.main= @main;

%%
%function main(IDstock,dts,onlyfreq,tbxOpt)
    function out_data=main(IDstock,dts,TD,Lfreq,folder,NminObs)
        if nargin< 2
            disp('using magic Dates')
            path= 'U:\Matlab\data\';
            news=load([path 'news.mat']);
            news=news.news;
            dts= news.date([1 end]);
            assert(dts(1)<= dts(2));
        end
        %         if nargin< 3; onlyfreq= [];end
        %         if nargin< 1; IDstock= 276;end
        %         if nargin < 4
        %             vl= VLoader();
        %         else
        %             vl= VLoader(tbxOpt);
        %         end
        %tbx= newsProjectTbx();
        vl= VLoader();
        out_data=vl.bufferizeAll(IDstock, dts, TD,Lfreq,folder,NminObs); %tbx.mdate(dts))
    end

    function test()
        disp('using magic Dates')
        path= 'U:\Matlab\data\';
        news=load([path 'news.mat']);
        news=news.news;
        dts= news.date([1 end]);
        assert(dts(1)<= dts(2));
        vl= VLoader();
        %tbx= newsProjectTbx();
        vl.bufferizeAll(8, dts);
        
        learnData= vl.getbin(276,dts,15,{'volume'})
        fullData= vl.getbin(276,dts,1,{'vwap','volume','vol_GK','vwas'})
        fullData5= vl.getbin(276,dts,5,{'vwap','volume','vol_GK','vwas'})
        V= load([path 'cacVolume.mat'])
        V= V.Vols(28);
        try; V=V{1};end
        %V.value=V.value1; % DAILY
        %V.value
        V.value= bsxfun(@times,V.value,1./sum(V.value,2));
        assert(all(abs(sum(V.value,2)-1)< 10^(-6)))
    end
%%
    function b= checkExist(security_id,dates,type)
        IO= tbx.FILE(tbx.FILEconfig.folder(security_id,'Buffer'));
        from_= dates(1); to_= dates(2);
        files= IO.parse('type','buffer','from',from_,'to',to_,'stockID',security_id,'freq',type);
        assert(length(files)<2);
        b= (length(files)== 1);
        
    end
%%
    function out_data= bufferizeAll(security_id,dates,TD,Lfreq,folder,NminObs)
        %FORMAT= 'dd/mm/yyyy';
        %if ~checkExist(security_id,dates,'full')
        out_data= {};
        if iscell(Lfreq); Lfreq=[Lfreq{:}];end
        for f= (Lfreq(:)') % returns  [] if do_save=1 !!
            out_data{end+1}=bufferize(security_id,dates,1,TD,f,folder,NminObs);
        end
        %end
        %         from_= dates(1); to_= dates(2);
        %         IO= tbx.FILE(tbx.FILEconfig.folder(security_id,'Buffer'));
        %
        %         if isempty(onlyfreq)
        %             LD= getbin(security_id,dates,15,{'volume'});
        %             IO.save(LD,'type','buffer','from',num2str(from_),'to',num2str(to_),'stockID',security_id,'freq','learning');
        %             clear LD
        %             % HF1 va produire Out of memory -> il faut le conserver sous forme de cell (un seul exemplaire passe en m?moire)
        %             % et traiter sp?cifiquement cette fr?quence dans le calcul des
        %             % crit?res op?rationnels
        %             % Par contre la LearningFrequence est ? conserver en tant que
        %             % matrice !!!
        %             HF1= getbin(security_id,dates,1,{'vwap','volume','vol_GK','vwas'},1); % keep cell-array !
        %             IO.save(HF1,'type','buffer','from',num2str(from_),'to',num2str(to_),'stockID',security_id,'freq','mn1');
        %             clear HF1
        %             % HF5 bug pour une raison inconnue !
        %             HF5= getbin(security_id,dates,5,{'vwap','volume','vol_GK','vwas'},1);
        %             IO.save(HF5,'type','buffer','from',num2str(from_),'to',num2str(to_),'stockID',security_id,'freq','mn5');
        %             clear HF5
        %         else
        %             LD= getbin(security_id,dates,onlyfreq,{'volume'});
        %             IO.save(LD,'type','buffer','from',num2str(from_),'to',num2str(to_),'stockID',security_id,'freq','learning');
        %             clear LD
        %         end
    end
%%
    function out_data= bufferize(security_id,dates,dosave,TD,learning_freq,folder,NminObs)
        out_data=[];
        if nargin<3;dosave=1;end
        if nargin<4;TD={};end
        if nargin<5;learning_freq=15;end
        [do_reload,file]= createNewBuffer(security_id,dates,TD,learning_freq,folder);
        
        %         if do_reload ==1
        %             IO= tbx.FILE(tbx.FILEconfig.folder({security_id,TD,dates,folder},'Buffer'));
        %             out_data= IO.loadSingle('type','buffer','from',num2str(dates(1)),'to',num2str(dates(2)),'stockID',security_id,'freq','learning','f',learning_freq);
        %             out_data= out_data{1}.savedVariable;
        %             return
        %         end
        if ischar(do_reload) & strcmp(do_reload,'direct_load')
            buf= file; % file= cell array of data (mode no_buffer)
        elseif ~isempty(file)
            rep=tbx.FILEconfig.folder({security_id,TD,dates,folder},'Buffer');
            assert(strcmp(file(1:length(rep)),rep),file);
            buf= load(file);
            buf= buf.savedVariable;
        end
        %% BufferFull fait dans createNewBuffer si inexistant ou demand?
        % Donc pas besoin de refaire ici ???
        % TODO: CHECK pourquoi pas de boucle splits dans createNewBuffer() ???
        %
        % ANSWER: On ne peut pas reconstituer Learning buffer depuis 1min bufefr
        % car on enregistre pas les times (on a besoin des 27 colonnes obtenues avec readdataset)
        % TODO: supprimer le 1er chargement
        %         DFORMAT= 'dd/mm/yyyy';
        %         BIN_SIZE= 1;
        %         from_ = datestr(dates(1),DFORMAT);
        %         to_   = datestr(dates(2),DFORMAT);
        %
        %         %< gestion options des basic_indicator
        %         opt4bi = options({'window:time', datenum(0,0,0,0,BIN_SIZE,0), ...
        %             'step:time', datenum(0,0,0,0,BIN_SIZE,0), ...
        %             'bins4stop:b', false, ...
        %             'context_selection:char', 'span_all'});
        %         bi_str = ['gi:basic_indicator/' opt2str(opt4bi)];
        %         STOP_BUFFER= 0;
        %         d0= datenum(from_,DFORMAT);
        %         d1= datenum(to_,DFORMAT);
        %         assert(d1>d0);
        %         splits= floor(d1):-50:floor(d0);
        %         %f= @(x,y) read_dataset(bi_str,'security_id',security_id,'from',x ,'to',y ,'trading-destinations', {},'output-mode', 'day' );
        %         f= @(x,y) myreaddataset(bi_str,security_id,x,y,TD); % TODO: TD
        %         buf={};
        %         for k=2:length(splits)
        %            % bb est par date croissante, mais la boucle recule
        %            % si les donn?es n'existent pas, on arrete la. On espere donc
        %            % qu'elles arretent d'exister dans le pass?, mais pas qu'il y a
        %            % un trou
        %            try
        %                bb= f(datestr(splits(k)+1,DFORMAT),datestr(splits(k-1),DFORMAT));
        %            catch ME
        %                if strcmpi(ME.identifier,'exec:sql');
        %                    STOP_BUFFER= 1;
        %                    break
        %                else
        %                    rethrow(ME)
        %                end
        %            end
        %            bb(cellfun(@(x)isempty(x) | prod(size(x.value))==0, bb)) = [];
        %            buf={bb{:},buf{:}};
        %            clear bb;
        %         end
        %         %if isempty(buf) | ((splits(end)> d0) && STOP_BUFFER)
        %         % CHANGED 13/01/2012 -> explique le 29/01/2007 on a oubli? la premi?re tranche
        %         if isempty(buf) | ((splits(end)> d0)) % && STOP_BUFFER)
        %             bb= f(datestr(d0,DFORMAT),datestr(splits(end),DFORMAT));
        %             bb(cellfun(@(x)isempty(x) | prod(size(x.value))==0, bb)) = [];
        %             buf={bb{:},buf{:}};
        %             clear bb
        %         end
        
        if isempty(buf)
            if isempty(TD); TD=[];end
            msg= sprintf('Vloader.bufferize() : No available data for Stock=<%d>, TD=<%s>, Dates=<%s to %s>, read_datesetmode=<%s>',security_id, TD,from_,to_,bi_str);
            error('Vloader:NoData', msg);
        end
        
        dts = cellfun(@(x) unique(floor(x.date)) , buf);
        assert(length(dts)== length(unique(dts)));
        
        assert(issorted(dts)); %unique(cellfun(@(x) floor(x.date(1)),buf))
        
        % NEW: removes days without continuous phase
        idx_with_conti= cellfun(@(x) check_continuous_exists(x),buf);
        
        
        if length(buf(idx_with_conti)) < NminObs
            if isempty(TD); TD=[];end
            msg= sprintf('Vloader : not enough (%d<%d) observations for <%d; %d> [%d %d]',length(buf(idx_with_conti)),NminObs,security_id,TD,dates(1),dates(2));
            error(msg);
        end
        
        buf= buf(idx_with_conti);
        
        %buf= load(file);
        %buf= buf.savedVariable
        
        %% DONE: conserver open/close du dernier jour
        %begin_= st_data('keep', buf{end}, 'begin_slice');
        %end_=st_data('keep', buf{end}, 'end_slice');
        %phase_data= cellfun(@(d)st_data('keep', d, {'opening_auction','auction', 'closing_auction','intraday_auction','stop_auction'}) ,vol,'uni',1);
        
        
        
        %% save learning Buffer
        freq= learning_freq;
        %         [data_agg, idx_extract,idx_dump, grid_] = st_data(...
        %                     'agg', buf, 'colnames',{'vwap','volume','vol_GK','vwas'},...
        %                     'grid_options', {'time_type', 'min','window', freq,'step',...
        %                     freq, 'context_selection', 'span_all','force_unique_intraday_auction',false}  );
        
        [data_agg, idx_extract,idx_dump, grid_] = st_data(...
            'agg', buf, 'colnames',{'vwap','volume','vol_GK','vwas'},...
            'grid_options', {'time_type', 'min','window', freq,'step',...
            freq, 'context_selection', 'contextualised','force_unique_intraday_auction',false}  );
        %data_agg2 = st_data('fast-stack', data_agg);
        
        %% PHASES, next step:
        % les phases d?pendent du jour, mais avec une convention, on a
        % qu'une phase sur learning (celle ou on a choisit de mettre les IA)
        % il faudrait qu'elle soit tjs ? la mm colonne, mais son horaire change !
        
        [data_agg,changing_continuous_bin,false_agg_changes]= aggregate_mktTime_extension(data_agg);
        assert(all( cellfun(@(x) issorted(x.date+st_data('col',x,'phase')),data_agg   )   ))
        %         assert(length(unique(cellfun(@(x) size(x.value,1), data_agg)))==1)
        
        [data_agg,phases,volume] = unif(data_agg,{'phase','volume'});
        phases = nanmean(phases,1);
        colNames2come = cellstr(datestr(st_data('cols',data_agg{1},'end_slice'),'HH:MM:SS'))';
        dts = cellfun(@(x)floor(x.date(find(~isnan(x.date),1))),data_agg);
        binned_volume = st_data( 'init', 'title', 'volume', 'value', volume, 'date', dts, 'colnames', colNames2come);
        binned_volume.info2 =  binned_volume.info;
        binned_volume.info = buf{1}.info;
        binned_volume.phases = phases;
        binned_volume.frequency = freq;
        binned_volume.destinationID = TD;
        binned_volume.original_date = dates;
        binned_volume.changing_continuous_bin = changing_continuous_bin;
        binned_volume.false_agg_changes = false_agg_changes;
        binned_volume.idx_with_conti = idx_with_conti;
        
        
        if dosave && ~(ischar(do_reload) && strcmp(do_reload,'direct_load') )
            IO= tbx.FILE(tbx.FILEconfig.folder({security_id,binned_volume.destinationID,dates,folder},'Buffer'));
            IO.save(binned_volume,'type','buffer','from',num2str(dates(1)),'to',num2str(dates(2)),'stockID',security_id,'freq','learning','f',learning_freq);
            clear binned_volume
        else
            out_data=binned_volume;
        end
        
    end

    function dists=check_align(datas)
        D= datas{1};
        %dists= [];
        for k=2:length(datas)
            DD= datas{k};
            distdates= (mod(D.date,1)-mod(DD.date,1));
            i= find(distdates~=0);
            assert(all(D.phase(i)==DD.phase(i)));
            distdates(i)=0;
            dists= norm(distdates) +norm(D.phase-DD.phase);
            assert(dists==0);
        end
    end

    function [colNames2come,dts]= getTransposeInfo(binned_volume)
        colNames2come= mod(binned_volume{1}.date,1);
        colNames2come= cellstr(datestr(colNames2come,'HH:MM:SS'));
        dts= cellfun(@(x) unique(floor(x.date)), binned_volume, 'uni',1);
    end

    function flag= check_continuous_exists(daydata)
        % -> true pour jour conserv?
        flag = true;
        ID_CONTINUE = daydata.info.phase_th(strcmpi({daydata.info.phase_th.phase_name},'CONTINUOUS')).phase_id;
        if length(ID_CONTINUE)~=1
            flag= false;
        else
            % les changements de groupe vont rapatrier des 0 en continue,
            % alors qu'elle n'existait pas ? l'?poque !
            v = st_data('cols',st_data('where',daydata,'{auction}==0'),'volume');
            windowSize = floor(0.1*length(v)); % 10% consécutifs nul ?
            vma = filter(ones(1,windowSize)/windowSize,1,v==0);
            if max(vma)> 0.9999
                flag= false;
                %             else
                %                 flag= mean(v==0) < 0.5 ; % moins de 20 % de bins vides
            end
            
        end
    end

    function buf= myreaddataset(bi_str,security_id,from_,to_,TD)
        try
            buf= read_dataset(bi_str,'security_id',security_id,'from',from_ ,'to',to_ ,'trading-destinations', TD,'output-mode', 'day' );
        catch ME
            if isempty(regexp(ME.message, ...
                    'SQL: tick_db_([a-zA-Z0-9]*)_20\d\d\.\.deal_archive([a-zA-Z_0-9]*) not found\.(.*)', 'match'));
                rethrow(ME);
            else
                buf = {st_data('empty-init')};
            end
        end
    end

    function [data,varargout] = unif(data,colnames)
        varargout = cell(1,length(colnames));
        ph = cellfun(@(x)st_data('cols',x,'phase;begin_slice;end_slice'),data,'uni',false);
        un_ph = unique(cell2mat(ph),'rows');
        data = cellfun(@(x)st_data('reindex',x,un_ph,'phase;begin_slice;end_slice'),data,'uni',false);
        for k = 1:length(colnames)
            varargout{k} = cellfun(@(x)st_data('cols',x,colnames{k})',data,'uni',false);
            varargout{k} = cell2mat(varargout{k});
        end
    end

end

