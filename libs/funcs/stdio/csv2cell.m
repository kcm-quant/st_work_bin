function out = csv2cell(filename,delimiter)
% CSV2CELL 
%
%
%
% Examples:
% filename='C:/test.csv';
% M={'a','b';1,'e';2,'d';3,'10:23'};
% cell2csv(filename,M);
% data=csv2cell(filename);
%
% See also:
%    
%
%   author   : 'njoseph@cheuvreux.com'
%   reviewer : ''
%   date     :  '13/03/2013'
%   
%   last_checkin_info : $Header: csv2cell.m: Revision: 1: Author: nijos: Date: 03/13/2013 12:19:32 PM$

if nargin<2
    delimiter=',';
end

fid = fopen(filename,'r');
header = textscan(fid,'%s',1,'EndOfLine','\n');
header = regexp(header{1}{1},delimiter,'split');
header = regexprep(header,'\W','_');
format = repmat('%s ',1,length(header));
res = textscan(fid,format,'Delimiter',delimiter,'EndOfLine','\n');
fclose(fid);
out={};
for i_=1:length(res)
    out=cat(2,out,res{i_});
end
out=cat(1,header,out);

end