function tbx= fileToolbox(rep)
% Gestion de fichiers .mat avec paires (parametre,valeur)
% 
% load recupere tous les fichiers avec parametres== valeur
% 
% Exemple:
% tbx= fileToolbox()
% for k=1:5
% 	x=[];x.data= randn(10,10);x.name= ['name',num2str(k)];
% 	path= tbx.save(x,'mode',1,'name',x.name)
% end
% for k=1:5
% 	x=[];x.data= randn(10,10);x.name= ['name',num2str(k)]; x.champSupl = 'test';
% 	path= tbx.save(x,'mode',2,'name',x.name)
% end
% 
% tbx= fileToolbox();
% [z,path]= tbx.load('name','name1');
% path
% z{1}.savedVariable
% end
%
%

global st_version

if nargin== 0; 
    if isempty(st_version)
        rep= 'C:\MSave';
    else
        rep = fullfile(st_version.my_env.st_repository, 'MSave');
    end
end

tbx= [];
tbx.rep_= rep;
tbx.basicName_= 'fileSave';
tbx.save= @save_;
tbx.load= @load_;
tbx.loadSingle= @load1_;
tbx.parse= @parse_;
%tbx.loader= @(name) load(fullfile(tbx.rep_, name));
tbx.loader= @(name) treat_buggy_decorate(load(fullfile(tbx.rep_, name)));

	function fname= save_(savedVariable , varargin )
        if ~exist(tbx.rep_, 'dir')
            mkdir(tbx.rep_);
        end
		fname= fullfile(tbx.rep_ , tbx.basicName_);
		N= length(varargin);
		for n=1:2:N-1
			fname= [fname '~' varargin{n} '-' num2str(varargin{n+1}) ];
		end
		fname= [fname, '~.mat'];
		%disp(fname)
		info= ['fileToolbox, ' datestr(now) ];
		params= varargin;
		%save(fname,'savedVariable','info','params');    
        %savedVariable.filetrace= wrap_options_for_io(savedVariable.filetrace);
        save('-v7.3',fname,'savedVariable','info','params');

        
	end

	function ffound= parse_(varargin)
		N= length(varargin);
		stest= {}; % liste des strings recherches dans les noms de fichiers
		for n=1:2:N; stest{end+1}= ['~' varargin{n} '-' num2str(varargin{n+1}) ]; end
		N= length(stest);
		sfiles = dir(tbx.rep_); % liste files parcourues
		ffound= {}; % liste fichiers qui matchent les tests de stest
		K= length(sfiles);
		for k=1:K
			f= sfiles(k);
			try
                b= strcmpi(f.name(end-3:end),'.mat');
                b= b& strcmp(f.name(1:8),'fileSave');
            catch
                b= 0;
            end            
			for n=1:N				
				if isempty(strfind(f.name, [stest{n},'~'] )); b= 0; end
            end            
			if b;
                ffound{end+1}= f.name;
                if strcmpi(f.name(1:2), 'C:')
                    www=1;
                end
            end
		end		
	end

	function [contents,filenames]=load_(varargin)
		loader= @(name) load(fullfile(tbx.rep_, name));
		filenames= parse_(varargin{:});
		N= length(filenames);
		contents= cellfun(loader, filenames,'uni',0);
        contents= cellfun(@treat_buggy_decorate, contents,'uni',0);
        
		% donn�es originales r�cup�r�es dans le champ .savedVariable;
		% autorise non uniformit�
    end

    function [contents,filenames]=load1_(varargin)
        % assert only one file exist for given atributes
    	loader= @(name) load(fullfile(tbx.rep_, name));
		filenames= parse_(varargin{:});
		N= length(filenames);
        if N>1
            f_= strfind(filenames,'J0-1');
            f_= uo_(cellfun(@isempty,f_,'uni',0));
            filenames= filenames(find(f_));
            N= length(filenames);
        end
        if N~=1
            p_= length(varargin);
            args_= cellfun(@num2str,varargin,'uni',0);
            msg= sprintf(['fileToolbox.loadSingle() searched <%s> for '  repmat('%s ',1,p_)],tbx.rep_,args_{:});            
            msg1= sprintf(['but found <%d> matching files:'],N);
            msg= [msg ' ' msg1];
            disp(msg);
            disp(char(filenames));
            error('BAD_USE:fileToolbox.loadSingle()',msg );
            %assert(N==1);
        end
		contents= cellfun(loader, filenames,'uni',0);        
        filenames= filenames{1};
		% donn�es originales r�cup�r�es dans le champ .savedVariable;
		% autorise non uniformit�
	end
				

end

function x= treat_buggy_decorate(x)
if isstruct(x) & isfield(x,'savedVariable') & isfield(x.savedVariable,'filetrace')
    x.savedVariable.filetrace= options(x.savedVariable.filetrace.get());
end
end
% Matlab7: cellfun ne prend pas une fonction quelconque
% function y= cellfun(f,varargin)
% y={};
% N= length(varargin{1});
% P= length(varargin);
% for n=1:N
%     args={};
%     for p=1:P
%         args{end+1}= varargin{p}{n};
%     end
%     y{end+1}= f(args{:});
% end
% end

%%
function EXAMPLE()
tbx= fileToolbox()
for k=1:5
	x=[];x.data= randn(10,10);x.name= ['name',num2str(k)];
	path= tbx.save(x,'mode',1,'name',x.name)
end
for k=1:5
	x=[];x.data= randn(10,10);x.name= ['name',num2str(k)]; x.champSupl = 'test';
	path= tbx.save(x,'mode',2,'name',x.name)
end

tbx= fileToolbox();
[z,path]= tbx.load('name','name1');
path
z{1}.savedVariable
end





