function [deals,obs] = get_orderbook(mode, varargin)
% GET_ORDERBOOK - 

% [deals,obs]=get_orderbook('read', 'security_id', 'ALSO.PA', 'day',...
% '19/05/2010','tradingDestinationId',4,'dir','Y:\tick_ged\', 'tradesOnly', 0)
% plot(st_data('cols', obs, [sprintf('bidPrice%d;',1:5) sprintf('askPrice%d;',1:5)]))
switch lower(mode)
    case 'masterkey'
        
    case 'read'
        opt = options({'security', 'FTE.PA', 'day', '01/04/2008',...
            'date_format:char', 'dd/mm/yyyy', 'dir', fullfile(getenv('st_repository'),'FOBU_bin') ...
            'trading-destinations',{},'auctionType',0,'tradesOnly',1}, varargin);
        ric = opt.get('security');
        
        day_ = opt.get('day');
        if ~isnumeric( day_)
            day_ = datenum( day_, opt.get('date_format:char'));
        end
        tbtDirectory = opt.get('dir');
        tradesOnly = opt.get('tradesOnly');
        try
%            ric_str = get_repository( 'security-key', ric);
            tradingDestinationId = opt.get('tradingDestinationId');
            auctionType = opt.get('auctionType');
            if(tradesOnly == 0)                
                [o d] = loadOrderBook(tbtDirectory, ric , datestr(day_, 'yyyymmdd'),tradingDestinationId,auctionType,tradesOnly);
                o = o';
                d = d';
                title = sprintf('deals_%s_%s',ric,datestr(day_, 'yyyymmdd'));
%                 deals = st_data('init','title',title,'value',d(:,2:19),'date',d(:,1),...
%                     'colnames',{'OrderBookId','price','size','sell','bid','bidSize','ask','askSize','cross','auction',...
%                     'openingAuction','intradayAuction','closingAuction','tradingAtLast','tradingAfterHour','dark',...
%                     'tradingDestinationId','securityId'});
%                 
                
                  deals = st_data('init','title',title,'value',d(:,2:19),'date',d(:,1),...
                    'colnames', {'price',    'volume',    'sell'   , 'bid'  ,  'ask'  ,  'trading_destination_id'   , 'bid_size' ,'ask_size'  ,  'buy',...
                    'cross',    'auction' ,   'opening_auction' ,   'closing_auction'   , 'trading_at_last',...
                    'trading_after_hours' ,  'intraday_auction'  ,  'dark' ,'orderBook_Id'});
                
   
                
                obstitle = sprintf('obs_%s_%s',ric,datestr(day_, 'yyyymmdd'));
                obs = st_data('init','title',obstitle,'value',o(:,2:24),'date',o(:,1),...
                    'colnames',{'bidPrice5','bidSize5','bidPrice4','bidSize4','bidPrice3','bidSize3','bidPrice2',...
                    'bidSize2','bidPrice1','bidSize1','askPrice1','askSize1','askPrice2','askSize2','askPrice3',...
                    'askSize3','askPrice4','askSize4','askPrice5','askSize5','orderBookId','tradingDestinationId',...
                    'securityId'});
            else
                d = loadOrderBook(tbtDirectory, ric, datestr(day_, 'yyyymmdd'),tradingDestinationId,auctionType,tradesOnly);
                d = d';
                title = sprintf('deals_%d_%s',ric,date);
                deals = st_data('init','title',title,'value',d(:,2:18),'date',d(:,1),...
                    'colnames',{'OrderBookId','price','size','sell','bid','bidSize','ask','askSize','cross','auction',...
                    'openingAuction','intradayAuction','closingAuction','tradingAtLast','tradingAfterHour','dark',...
                    'tradingDestinationId'});
                obs = st_data('empty');
            end
        catch er
            deals = st_data('empty');
            obs = st_data('empty');
            st_log('error: %s\n', er.message);
        end

    otherwise
        
end