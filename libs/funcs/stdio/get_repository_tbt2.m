function ref = get_repository_tbt2(mode, date_, varargin)
% GET_REPOSITORY_TBT2 - Retrieves historised referential info from tbt2 repository
%
%
%
% Examples:
%
% tic; tdi = get_repository_tbt2('tdinfo', '12/04/2011', 110);toc;
% tic; tdi = get_repository('tdinfo', 110);toc;
%
% See also: get_repository
%    
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     :  '15/04/2011'

dateyyyymmdd = datestr(datenum(date_, 'dd/mm/yyyy'), 'yyyymmdd');

switch lower(deblank(mode))
    case 'tdinfo'
        [ref, ~, ts] = get_ref(dateyyyymmdd);
        idx2keep = (ref.security_id == varargin{1});
        fldn = fieldnames(ref);
        fields_content = cell(1, length(fldn));
        for i = 1 : length(fldn)
            fields_content{i} = ref.(fldn{i})(idx2keep);
            if ~iscell(fields_content{i})
                fields_content{i} = num2cell(fields_content{i});
            end
        end
        ref = cat(1, fldn', fields_content);
        ref = struct(ref{1:end}); % as there is a cell it will become a struct array
        [ref.tick_sizes] = deal([]);
        for i = 1 : length(ref)
            idx_this_ts_id = (ts.tick_size_id == ref(i).price_scale_id);
            ref(i).tick_sizes = struct('threshold', ts.threshold(idx_this_ts_id), ...
                'tick_size', ts.tick_size(idx_this_ts_id));
        end
        
        ids = select_id_dico(cat(1, ref.tbt2_id));
    otherwise
        error('get_repository_tbt2:mode', 'MODE: <%s> unknown', mode);
        
end

end

function ids = select_id_dico(tbt2_id)
    ids = get_id_dico();
    [~, ind_in_original_order] = ismember(ids.tbt2_id, tbt2_id);
    liioo = logical(ind_in_original_order);
    ind_in_original_order = ind_in_original_order(liioo);
    if length(tbt2_id) ~= length(ind_in_original_order)
        error('select_id_dico:exec', 'Inconsistency in referential, Unable to find some of the required tbt2id');
    end
    fldn = fieldnames(ids);
    for i = 1 : length(fldn)
        ids.(fldn{i}) = ids.(fldn{i})(liioo);
        ids.(fldn{i})(ind_in_original_order) = ids.(fldn{i});
    end
end

function id_dico = get_id_dico()
global st_version
persistent mem_id_dico
if isempty(mem_id_dico)
    filen = fullfile(st_version.my_env.tbt2_repository, 'config', ...
            'Universe.txt');
%     expected_headers = {'TBTID', 'Ric', 'CheuvreuxId', 'Type', 'Ticker', ...
%         'TradingDestination', 'QuoteCalculationRule'};
    % TODO check headers?
    fid = fopen(filen);
    if fid == -1
        error('get_repository_tbt2:get_id_dico', 'Unable to read file <%s>', filen);
    end
    ts =textscan(fid,'%d%s%d%d%s%d%s','Delimiter','|', 'HeaderLines',1);
    mem_id_dico = struct('tbt2_id', ts(:, 1), 'ric', ts(:, 2), ...
        'security_id', ts(:, 3), 'security_type', ts(:, 4), ...
        'ticker', ts(:, 5), 'trading_destination_id', ts(:, 6));
end
id_dico = mem_id_dico;
end

function [ref, th, ts] = get_ref(dateyyyymmdd)
global st_version
persistent mem
EPS4TS = 1e-8;
fieldn2look = ['d' dateyyyymmdd];

if isfield(mem, fieldn2look)
    [ref, th, ts] = deal(mem.(fieldn2look){:});
else
    if ~isempty(mem)
        fldn = fieldnames(mem);
        if fldn > 19
            mem = rmfield(mem, fldn{1});
        end
    end
    
   
    
    % < Referential
    filen = fullfile(st_version.my_env.tbt2_repository, dateyyyymmdd, ...
        sprintf('deal_referential.%s.txt', dateyyyymmdd));
    expected_headers = tokenize(['tbt2_id|security_id|trading_destination_id|ranking|quotation_group|'...
                'quotation_group_id|price_scale_id|currency_id|quotation_unit|'...
                'execution_market_short_name|execution_market_id|gmt_offset_seconds|'...
                'use_localtime|context_id|nb_limits|place_id|global_zone'], '|');
    fid = fopen(filen);
    if fid == -1
        error('get_repository_tbt2:exec', 'Unable to read file <%s>', filen);
    end
    % TODO check headers?
    ref =textscan(fid,'%d%d%d%d%s%d%d%d%f%s%d%d%d%d%d%d%s',...
        'Delimiter','|', 'HeaderLines',1);
    for i = 1 : length(ref)
        ref{i} = ref(i);
    end
    ref = cat(1, expected_headers, ref);
    ref = struct(ref{1:end});
    
    [~, idx_order] = sort([ref.ranking]);
    fldn = fieldnames(ref);
    for i = 1 : length(fldn)
        ref.(fldn{i}) = ref.(fldn{i})(idx_order);
    end
    
    ref.gmt_offset = ref.gmt_offset_seconds/(24*3600);
    
    fclose(fid);
    % >
    
    % < Trading_hours
    % This information is correctly historised in repository and the format
    % of the file is wrong so for now we'll keep on using databases for
    % this
    % Moreover, using files means we would have to correct them when
    % something's wrong in the files
%     %< As there is an error in the file trading_hour we prototype the
%     % function with an edited file  whose dirtectory will be  :
%     % TODO : when error will be corrected replace
%     % "st_version_my_env_tbt2_repository" by
%     % "st_version.my_env.tbt2_repository"
%     st_version_my_env_tbt2_repository = 'G:\DEVELOPMENT\PARIS\TEAM_QUANT_RESEARCH\data\tick_ged';
%     %>
%     filen = fullfile(st_version_my_env_tbt2_repository, dateyyyymmdd, ...
%         sprintf('trading_hours.%s.txt', dateyyyymmdd));
%     expected_headers = tokenize(['trading_destination_id|quotation_group|'...
%         'quotation_group_id|opening_auction|opening_fixing|opening|'...
%         'intraday_stop_auction|intraday_stop_fixing|intraday_stop|'...
%         'intraday_resumption_auction|intraday_resumption_fixing|'...
%         'intraday_resumption|closing_auction|closing_fixing|closing|'...
%         'post_opening|post_closing|trading_at_last_opening|'...
%         'trading_at_last_closing|trading_after_hours_opening|trading_after_hours_closing'], '|');
%     fid = fopen(filen);
%     if fid == -1
%         error('get_repository_tbt2:exec', 'Unable to read file <%s>', filen);
%     end
%      % TODO check headers?
%     ref =textscan(fid,'%d%s%d%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s',...
%         'Delimiter','|', 'HeaderLines',1);
%     something to do here ...
%     fclose(fid);
      th = [];
%     % >
    
    % < Tick size
    filen = fullfile(st_version.my_env.tbt2_repository, dateyyyymmdd, ...
        sprintf('tick_size.%s.txt', dateyyyymmdd));
    fid = fopen(filen);
    if fid == -1
        error('get_repository_tbt2:exec', 'Unable to read file <%s>', filen);
    end
    ts =textscan(fid,'%d%f%f','Delimiter',',', 'HeaderLines',1);
    ts = struct('tick_size_id', ts(:, 1),'threshold', ts(:, 2),'tick_size', ts(:, 3));
    ts.threshold = round(ts.threshold/EPS4TS)*EPS4TS;
    ts.tick_size = round(ts.tick_size/EPS4TS)*EPS4TS;
    fclose(fid);
    % >
    
    mem.(fieldn2look) = {ref, th, ts};
end

end