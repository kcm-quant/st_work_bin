function rslt = latex(mode, filename,  varargin)
% LATEX - quelques fonctions pour ?crire un fichier tex, puis le compiler
%           et ouvrir le pdf
%
%   Pour chaque mode, on peut si l'on se moque des options, utiliser la
%   syntaxe minimale latex(mode, filename). Les exceptions a cette regle
%   sont les mode 'array', pour lequel on doit n?cessairement fournir
%   une matrice ou un st_data a ecrire dans le fichier
%   latex('array', filename, data); le mode 'text' latex('text',
%   filename, 'texte'); les modes 'section', 'subsection', 'subsubsection'
%   latex('section', filename, 'titre de la section')
%
%   Si l'on ne donne pas de chemin absolu pour le fichier, le dossier par
%   defaut pour le pdf sera 'st_repository(la variable d'environement)\pdf'
%   Dans ce meme r?pertoire vous pouvez trouver le tex dans le repertoire
%   tex, et les graphes dans tmp_graphes
%
%   latex('header', filename, 'title', 'nouveau document', 'author', 'Moi') :
%       ecrit le debut du fichier tex, avec les include et jusqu'a
%       \begin{document} \maketitle'. dans cet exemple le
%       titre du document sera 'nouveau document' et l'auteur sera Moi
%
%   latex('graphic', filename, 'graph_handle', graph_handle, 'scalebox', ...
%       0.75, 'explicit_name', 'mon_graphique_001', 'close', true, 'rotate', 90, ...
%       'fullscreen', false) :
%       inclue le graphique dont on a donne le graph_handle (generalement
%       grace a gcf), filename doit contenir le chemin
%       complet. L'option scalebox permet de r?duire ou d'agrnadir cette
%       figure. l'option 'explicit_name' permet de donner un nom explicite
%       au graphique afin que vous soyez capable de le retrouver dans le
%       r?pertoire o? se trouve le fichier tex. Dans le cas contraire le
%       graphique s'appellera tmp_1 puis tmp_2, etc...
%
%       si vous voulez sauver la figure courante, et que vous vous moquez
%       de retrouver le graphe, et que vous vous moquez de la taille du
%       graphe dans le pdf la syntaxe suivante sera suffisante :
%       latex(filename, 'graphic')
%
%       cette utilisation ferme la figure apres l'avoir sauvee et incluse.
%       si vous ne desirez pas que ce soit le cas, utilisez l'option
%       'close' avec la valeur false.
%
%       il y a une option ('width', true) pour ceux qui veulent l'utiliser
%
%   latex('compile', filename) : compile le tex filename et ouvre le pdf
%
%   latex('array', filename, array, 'col_header', ch,  'linesHeader', lh, 'format', { by col})
%       permet d'ecrire une matrice dans un tableau. ou meme un cell array
%       contenant a la fois du numerique et des chaines de caractere
%       voir mat2latex pour plus de detail sur les options
%       avec l'option ('plain', true) lorsqu'on sait ce qu'on fait
%   latex('table', ...) identique ? 'array'
%
%   latex('text', filename, string)
%       permet d'ecrire du texte (!pas des formules mathematiques!) dans le fichier tex
%
%   latex('plain', filename, string)
%       permet d'?crire directement dans le fichier tex
%
%   latex('section', filename, 'titre de la section')
%       permet d'ouvrir une nouvelle section nommee 'titre de la section'
%
%   latex('formula', fichier, 'formule du type : $\phi(y) = \frac{1}{\sqrt{2 \pi}}\int_{-\infty}^{y}e^{-\frac{x^2}{2}}dx$')
%       permet d'?crire une formule dans le tex
%
%   latex('copy-paste', fichier, fichier_a_copier)
%       permet de copier coller le fichier_a_copier dans le fichier
%
%   latex('end', filename)
%       permet de finir le tex grace a '\end{document}'
%
%   latex('st_plot+graphic', filename, data)
%       vous n'avez pas ? faire le st_plot, la fonction latex s'en charge,
%       de plus elle ne le rend pas visible ce qui vous evite d'etre
%       d?rang? par l'apparition des graphes...
%
% exemple d'utilisation :
% data = st_data( 'init', 'title', 'Mes donn?es', 'value', cumsum(randn(100,10)), ...
%                     'date', (1:100)', 'colnames',...
%                     arrayfun(@(i)sprintf('V-%d', i), 1:10,...
%                     'UniformOutput', false));
% fichier = 'exemple';
% fichier2 = 'fichierQuiSertJusteAmontrerLeCopierColl';
% latex('header', fichier, 'title', 'Exemple d''utilisation de la fonction Latex en Matlab');
% latex('header','no_title_test.tex','no_title',true,'margin','hmargin=1cm,vmargin=2cm','language','english');
% latex('section', fichier2, 'Le mode array de la fonction latex');
% latex('subsection', fichier2, 'Ecrire un st_data dans un tex');
% latex('array', fichier2, data);
% latex('subsection', fichier2, 'Ecrire une matrice dans un tex');
% latex('array', fichier2, data.value, 'col_header', data.colnames);
% latex('subsection', fichier2, 'Ecrire un cellarray');
% latex('array', fichier2, {'bla bla', 1}, 'col_header', {'du texte', 'des chiffres'});
% latex('subsection', fichier2, 'Changer le format des chiffres ?crits dans un tableau');
% latex('array', fichier2, {'bla bla', 1}, 'col_header', {'du texte', 'des chiffres'}, 'format', '%d');
% latex('section', fichier2, 'Inclure le graphique courant dans un tex');
% st_plot(data);
% latex('graphic', fichier2);
% latex('section', fichier2, 'Inclure un graphique trop gros');
% st_plot(data);
% set(gcf, 'Position', [1281 1 1280 957])
% latex('graphic', fichier2);
% latex('section', fichier2, 'Inclure un graphique trop gros initialement mais ? la bonne taille dans le pdf');
% st_plot(data);
% set(gcf, 'Position', [1281 1 1280 957])
% latex('graphic', fichier2, 'scale', 'automatic');
% latex('section', fichier2, 'Ecrire du texte dans un tex');
% latex('subsection', fichier2, 'Ecrire du texte dans un tex (!pas des formules!)');
% latex('text', fichier2, 'Je teste les caract?res qui doivent ?tre traduits avant d''?tre ?crits dans le tex : & ~ % \ _ $ { } " < >');
% latex('subsection', fichier2, 'Ecrire des formules');
% latex('formula', fichier2, '$\phi(y) = \frac{1}{\sqrt{2 \pi}}\int_{-\infty}^{y}e^{-\frac{x^2}{2}}dx$');
% latex('copy-paste', fichier, fichier2);
% latex('end', fichier);
% latex('compile', fichier);
%
%
% See also mat2latex wiki2latex
rslt = {};

% detection du mode de sortie
[p,f,e]= fileparts( filename);
output_mode = '';
switch lower(e)
    case '.html'
        output_mode = 'html';
    case '.tex'
        output_mode = 'tex';
    otherwise
        filename = [filename '.tex'];
        output_mode = 'tex';
end

% r?pertoire d'?criture
switch lower(output_mode)
    case 'tex'
        if (ispc && ~strcmp(filename(2:3), [':' filesep])) || (~ispc && ~strcmp(filename(1), filesep))
            pdf_dir = getenv('st_repository');
            if isempty(pdf_dir)
                error('Only users with a <st_repository> environnement variable may give a filename without a path.');
            end
            pdf_dir = fullfile(pdf_dir, 'pdf');
            dir_test_create(pdf_dir);
            fullfilename = fullfile(pdf_dir, filename);
        else
            fullfilename = filename;
            ofilename    = filename;
        end
        
        [pdf_dir,filename,ext] = fileparts(fullfilename);
        filename = [filename, '.tex'];
        tex_dir = fullfile(pdf_dir, 'tex');
        dir_test_create(tex_dir);
        fullfilename = fullfile(tex_dir, filename);
        g_dir    = fullfile('graphes', hash(filename, 'MD5'));
        
    case 'html'
        
        
        idx_html_root_path=find(strcmpi(varargin,'html_root_path'));
        idx_html_path_from_root=find(strcmpi(varargin,'html_path_from_root'));
        idx_html_path_max_files=find(strcmpi(varargin,'html_path_max_files'));
        
        %//////
        % filename
        if any(filename==filesep)
            error('latex: with html use, filename has to be THE filename and not the fullfilename');
        end
        
        %//////
        % pdf_dir
        if ~isempty(idx_html_root_path) && ~isempty(varargin{idx_html_root_path+1}) && ...
                ~isempty(idx_html_path_max_files) && ~isempty(varargin{idx_html_path_max_files+1})
            pdf_dir = fullfile(varargin{idx_html_root_path+1},...
                dir_mod_key(filename,varargin{idx_html_path_max_files+1}));
        elseif ~isempty(idx_html_root_path) && ~isempty(varargin{idx_html_root_path+1})
            pdf_dir = fullfile(varargin{idx_html_root_path+1});
        else
            pdf_dir = fullfile(getenv('st_repository'),'html');
        end
        if isempty(pdf_dir)
            error('Only users with a <st_repository> environnement variable may give a filename without a path.');
        end
        if pdf_dir(end)==filesep
            pdf_dir=pdf_dir(1:end-1);
        end 
        dir_test_create(pdf_dir);
        
        %//////
        % tex_dir
        if ~isempty(idx_html_path_from_root) && ~isempty(varargin{idx_html_root_path+1})
            html_path_from_root = varargin{idx_html_path_from_root+1};
            
            if isempty(html_path_from_root) || length(html_path_from_root)<2 || ...
                    ~all(html_path_from_root(1:2)=='./')
                error('latex : html_path_from_root is not valid relative path !!!');
            end
            
            html_path_from_root=html_path_from_root(2:end);
            
            html_path_from_root=strrep(html_path_from_root,'/',filesep);
            html_path_from_root=strrep(html_path_from_root,'\',filesep);
            
            %tex_dir = [pdf_dir,html_path_from_root];
            tex_dir = fullfile(pdf_dir,html_path_from_root);
            if html_path_from_root(end)==filesep
                tex_dir=tex_dir(1:end-1);
            end
        else
            tex_dir = pdf_dir;
        end
        dir_test_create(tex_dir);
        
        %//////
        % fullfilename
        fullfilename = fullfile(tex_dir, filename);
        
        %//////
        % g_dir
        g_dir=fullfile('graphes',...
            sprintf('keycode=%d',dir_mod_key(filename,30000)), hash(fullfilename, 'MD5'));
        %g_dir=fullfile('graphes',hash(fullfilename, 'MD5'));
        
        %//////
        % g_rel_dir
        nb_up=length(find(fullfilename==filesep))-length(find(pdf_dir==filesep))-1;
        
        if nb_up==0
            g_rel_dir='./';
        else
            g_rel_dir='';
            for i=1:nb_up
                g_rel_dir=[g_rel_dir '../'];
            end
        end
        g_rel_dir=[g_rel_dir g_dir];
        
end



% switch on mode

switch lower(mode)
    case 'copy-paste'
        cd(tex_dir);
        try
            print_in_file(text_to_printable_text(fileread( varargin{1})), fullfilename, 'a+');
        catch
            file_lst = dir(tex_dir);
            file_lst = {file_lst.name};
            for i = 3 : length(file_lst)
                tmp = tokenize(file_lst{i}, '.');
                if strcmp(tmp{1}, varargin{1})
                    print_in_file(text_to_printable_text(fileread(file_lst{i})), fullfilename, 'a+');
                    return;
                end
            end
        end
    case 'end'
        switch lower(output_mode)
            case 'tex'
                print_in_file('\\end{document}', fullfilename, 'a+');
            case 'html'
                print_in_file('\n</body>\n</html>', fullfilename, 'a+');
        end
        rslt=fullfilename;
    case 'text'
        print_in_file([text_to_pable_tex(varargin{1}, output_mode) ' \n'], fullfilename, 'a+');
    case 'plain'
        print_in_file([strrep(varargin{1},'\','\\') ' \n'], fullfilename, 'a+');
    case 'formula'
        % sans passer par tex 2 tex
        switch lower(output_mode)
            case 'tex'
                print_in_file([text_to_printable_text(varargin{1}) ' \n'], fullfilename, 'a+');
            case 'html'
                print_in_file(['</br><center><tt>' text_to_printable_text(varargin{1}) ' </tt></center></br>\n'], fullfilename, 'a+');
        end
    case {'section', 'subsection', 'subsubsection', 'part', 'paragraph' }
        switch lower(output_mode)
            case 'tex'
                print_in_file(['\\' mode '{' text_to_pable_tex([varargin{1}], output_mode) '} \n'], fullfilename, 'a+');
            case 'html'
                switch lower(mode)
                    case 'part'
                        mode = 'h1';
                    case 'section'
                        mode = 'h1';
                    case 'subsection'
                        mode = 'h2';
                    case 'subsubsection'
                        mode = 'h3';
                    otherwise
                        mode = 'b';
                end
                print_in_file(['<a name="' text_to_pable_tex([varargin{1}], output_mode) '"></a><' mode '>'  text_to_pable_tex([varargin{1}], output_mode) '</' mode '>\n'], fullfilename, 'a+');
        end
    case { 'array', 'table'}
        opt = options({'col_header', [], ...
            'plain', false, ...
            'link', false, ...
            'link_matrix',[],...
            'link-postfix', '.html', ...
            'link-prefix', './' ...
            'link_col_header',false,...
            'link_linesHeader',false,...
            'linesHeader_rplacelink',{},...
            'col_header_rplacelink',{}}, varargin(2:end));
        data = varargin{1};
        if isempty(data) ...
                || ((~isstruct(data) || ~st_data('check', data)) && ~isnumeric(data) && ~iscell(data))
            error('Error in Latex, third argument must be either numeric array or st_data');
        end
        if isstruct(data)
            if ~st_data('check', data)
                error('Bad st_data');
            end
            col_header   = {'date', data.colnames{:}};
            try
                format_ = data.attach_format;
            catch
                format_ = 'dd/mm/yyyy';
            end
            lines_header = arrayfun(@(i)datestr(i, format_), data.date, 'UniformOutput', false);
            opt.set('linesHeader', text_to_pable_tex(lines_header, output_mode));
            data = data.value;
        else
            col_header = opt.get('col_header');
            if isempty(col_header)
                warning('You didnt give any header for column, so they wil be named var_1, var_2, etc...');
                col_header = arrayfun(@(i)sprintf('$var_%d$', i), 1:size(data, 2), 'UniformOutput', false);
            end
            try
                opt.set('linesHeader', text_to_pable_tex(opt.get('linesHeader'), output_mode));
            catch end
        end
        lst = opt.get();
        if ~opt.get('plain')
            col_header = text_to_pable_tex(col_header, output_mode);
        else
            col_header = text_to_printable_text(col_header);
        end
        if strcmp(output_mode, 'html') && ~opt.get('plain')
            if iscellstr(data)
                for i = 1 : size(data, 1)
                    for j = 1 : size(data, 2)
                        data{i, j} = text_to_pable_tex(data{i, j}, output_mode);
                    end
                end
            end
        elseif strcmp(output_mode, 'html')
            if iscellstr(data)
                for i = 1 : size(data, 1)
                    for j = 1 : size(data, 2)
                        data{i, j} = text_to_printable_text(data{i, j});
                    end
                end
            end
        end
        lst{end+1} = 'output-mode';
        lst{end+1} = output_mode;
        rslt = mat2latex(data, col_header, lst{:});
        print_in_file(rslt, fullfilename, 'a+');
    case 'header'
        opt = options({ 'title', 'Statistiques Descriptives', 'tipa', true, ...
            'author', 'Burgot Romain', ...
            'css_from_root','css/styles.css',...
            'sorttable_from_root','sorttable.js',...
            'src_script', {}, ...
            'language','french',...
            'margin','',... %in latex format : example : 'hmargin=1cm,vmargin=2cm'
            'no_title',false},varargin);    %if true... we don't make a title page
        switch lower(output_mode)
            case 'tex'
                if ~isempty(opt.get('author'))
                    author = ['\\author{' opt.get('author') '}\n'];
                else
                    author = '';
                end
                % Pour compiler dvi/pdf sans douleur:
                % \usepackage{ifpdf}
                % \ifpdf
                % \usepackage[pdftex]{graphicx} %%graphics in pdfLaTeX
                % \DeclareGraphicsExtensions{.pdf,.jpg,.png}
                % \else
                % \usepackage[draft,dvips]{graphicx} %%graphics and normal LaTeX
                % \DeclareGraphicsExtensions{.eps}
                % \fi
                kw = iskeyword;
                kw = sprintf('%s,', kw{:});
                kw(end) = [];
                if opt.get('tipa')
                    with_tipa = '\\usepackage{tipa}\n';
                else
                    with_tipa = '%%\\usepackage{tipa}\n';
                end
          		language = opt.get('language');
                language2 = language;
                if strcmp(language,'french') || strcmp(language,'francais')
                    language = 'french';
                    language2 = 'francais';
                end
           		if opt.get('no_title')
                    title = '';
                else
                    title = '\\maketitle\n';
                end

                rslt = ['\\documentclass[a4paper,french]{article}\n'...
                    '\\usepackage[pdftex]{graphicx}\n'...
                    '\\usepackage{latexsym}\n'...
                    '\\usepackage{pdflscape}\n'...
                    '\\usepackage{longtable}\n'...
                    '\\usepackage{amsmath}\n'...
                    '\\usepackage{bbm}\n'...
                    '\\usepackage{amsfonts}\n'...
                    '\\usepackage{amssymb}\n'...
                    '\\usepackage[' opt.get('margin') ']{geometry}\n'...
                    ...'\\usepackage{vmargin}\n'...
                    '\\usepackage{listings}\n' ...
                    '\\usepackage{enumerate}\n'...
                    '\\usepackage{endnotes}\n'...
                    '\\usepackage{color}\n'...
                    '\\usepackage{showidx}\n'...
                    '\\usepackage{array}\n'...
                    with_tipa ...
                    '\\usepackage{makeidx}\n'...
                    '\\usepackage[latin1]{inputenc}\n'...
                    '\\usepackage[' language ']{babel}\n'...
                    '\\usepackage[pdftex,colorlinks=true,linkcolor=blue,urlcolor=blue,pdfstartview=FitH]{hyperref}\n'...
                    '\\usepackage{courier}\n' ...
                    '\\usepackage{color}\n' ...
                    '\\usepackage{calc}\n' ...
                    '\\definecolor{dkgreen}{rgb}{0,0.6,0}\n' ...
                    '\\definecolor{gray}{rgb}{0.5,0.5,0.5}\n' ...
                    '\\definecolor{purple}{rgb}{0.8,0.2,0.8}\n' ...
                    '\\definecolor{vert}{rgb}{0.2,0.6,0.4}\n' ...
                    '\\makeindex\n'...
                    '\\title{' opt.get('title') '}\n'...
                    author ...
                    '\\lstdefinelanguage{st_Matlab}[]{Matlab}\n' ...
                    sprintf('{keywords={%s}', kw) ',\n' ...
                    'basicstyle=\\ttfamily,\n' ...
                    'keywordstyle=\\color{blue},\n' ...
                    'commentstyle=\\color{dkgreen},\n' ...
                    'stringstyle=\\color{purple},\n' ...
                    'numbers=left,\n' ...
                    'numberstyle=\\tiny\\color{gray},\n' ...
                    'stepnumber=1,\n' ...
                    'numbersep=10pt,\n' ...
                    'backgroundcolor=\\color{white},\n' ...
                    'tabsize=4,\n' ...
                    'showspaces=false,\n' ...
                    'showstringspaces=false}\n' ...
                    '\\lstdefinelanguage{st_XML}[]{XML}\n' ...
                    '{markfirstintag=true,\n' ...
                    'frame=single,\n' ...
                    'breaklines=true,\n' ...
                    'basicstyle=\\ttfamily,\n' ...
                    'backgroundcolor=\\color{white},\n' ...
                    'basicstyle=\\scriptsize,\n' ...
                    'keywordstyle=\\color{blue},\n' ...
                    'commentstyle=\\color{vert},\n' ...
                    'stringstyle=\\color{red},\n' ...
                    'identifierstyle=\\ttfamily,\n' ...
                    'deletestring=[d]''}\n' ...
                    ...'\\setpapersize{A4}\n'...
                    '\\newlength{\\imgwidth}\n' ...
... %                    '\\newcommand\\scalegraphics[1]{\\includegraphics[width=1.0\\textwidth]{#1}}\n' ...
                     '\\newcommand\\scalegraphics[1]{%%   \n' ...    No Longer seem to work
                     '    \\settowidth{\\imgwidth}{\\includegraphics{#1}}%%\n' ...
                     '    \\setlength{\\imgwidth}{\\minof{\\imgwidth}{\\textwidth}}%%\n' ...
                     '    \\includegraphics[width=\\imgwidth]{#1}%%\n' ...
                     '}\n' ...
                    '\\begin{document}\n'...
                    ... '\\lstloadlanguages{[Visual]C++,[ISO]C++,XML,Matlab,Python}\n' ...
                    title ...
					];
                print_in_file(rslt, fullfilename, 'w');
                delete(fullfile(fullfile(pdf_dir, g_dir), '*'));
                dbs = dbstack();
                if length(dbs) > 1
                    tmp = which(dbs(2).name);
                    if isempty(tmp)
                        tmp = dbs(2).name;
                    end
                    latex('text', filename, sprintf('PDF generated automatically with the mfile :\n\n<%s>\n', tmp));
                    if length(dbs) > 2
                        latex('text', filename, sprintf('which was himself called with the following stack : <%s>', sprintf('%s/', dbs(3:end).name)));
                    end
                end
            case 'html'
                
                css_from_root = opt.get('css_from_root');
                sorttable_from_root = opt.get('sorttable_from_root');
                css_style_sheet='';
                if ~isempty(css_from_root)
                    % edit : css_style_sheet
                    if css_from_root(1)=='/' || css_from_root(1)==filesep
                        css_from_root = css_from_root(2:end);
                    end
                    
                    nb_up=length(find(fullfilename==filesep))-length(find(pdf_dir==filesep))-1;
                    
                    if nb_up==0
                        css_rel='./';
                    else
                        css_rel='';
                        for i=1:nb_up
                            css_rel=[css_rel '../'];
                        end
                    end
                    css_style_sheet=[css_rel css_from_root];
                end
                sorttable_path='';
                if ~isempty(sorttable_from_root)
                    % edit : css_style_sheet
                    if sorttable_from_root(1)=='/' || sorttable_from_root(1)==filesep
                        sorttable_from_root = sorttable_from_root(2:end);
                    end
                    
                    nb_up=length(find(fullfilename==filesep))-length(find(pdf_dir==filesep))-1;
                    
                    if nb_up==0
                        sorttable_rel='./';
                    else
                        sorttable_rel='';
                        for i=1:nb_up
                            sorttable_rel=[sorttable_rel '../'];
                        end
                    end
                    sorttable_path=[sorttable_rel sorttable_from_root];
                end
                
                % init header
                print_in_file(['<html>\n<header>\n<script src="' sorttable_path '"></script>\n'], fullfilename, 'w');
                src_script = opt.get('src_script');
                if ~isempty(src_script)
                    for i = 1 : length(src_script)
                        print_in_file(['<script src="' src_script{i} '"></script>\n'], fullfilename, 'w');
                    end
                end
                % COPY /bin/external/javascript/sorttable.js in the proper directory
                % add title
                print_in_file([ '<title> ' opt.get('title') '</title>\n'], fullfilename, 'a+');
                % add css reference
                print_in_file(['<link href="' css_style_sheet  '" rel="stylesheet" type="text/css" />'], fullfilename, 'a+');
                print_in_file([ '<title> ' opt.get('title') '</title>\n'], fullfilename, 'a+');
                % close header
                print_in_file('</header>\n<body>\n', fullfilename, 'a+');
                
                print_in_file(['<center><font size="+4"><b>' opt.get('title') '</b></font></br>\n'], fullfilename, 'a+');
                if ~isempty(opt.get('author'))
                    print_in_file(['<font size="+1"><i>Author: ' opt.get('author') '</i></font>'], fullfilename, 'a+');
                end
                print_in_file('</center>\n', fullfilename, 'a+');
        end
    case 'st_plot+graphic'
        st_plot(varargin{1}, 'visible', 'off');
        latex('graphic', filename, varargin{2:end});
    case 'graphic'
        opt = options({'graph_handle', gcf, ...
            'explicit_name', [], ...
            'close', true, ...
            'fullscreen', false, ...
            'scale', '', ...
            'saveonly', false}, varargin);
        g_name   = opt.get('explicit_name');
        g_handle = opt.get('graph_handle');
        if opt.get('fullscreen')
            set(g_handle, 'Position', get(0,'ScreenSize'));% On passe la figure en plein ?cran
        end
        pathstr  = fullfile(pdf_dir, g_dir);
        
        switch lower(output_mode)
            case 'tex'
                if isempty(g_name)
                    id = 1;
                    files_ = dir(fullfile(pdf_dir, g_dir));
                    if ~isempty(files_);
                        files_names = cat(1, {files_.name});
                        files_names(1:2) = [];
                        for i = 1 : length(files_names)
                            f_name = files_names{i};
                            if f_name(1) == 't'
                                if (strcmp(f_name(1:4), 'tmp_') && strcmp(f_name(end - 3: end), '.png'))
                                    id = max(str2double(f_name(5:end-4)) + 1, id);
                                end
                            elseif f_name(1) > 't'
                                break;
                            end
                        end
                    end
                    g_name = sprintf('tmp_%d.png', id);
                    fpath = ['../' fullfile(g_dir, g_name)];
                else
                    g_dir  = [filename(1:end-4) '_graphes' ];
                    pathstr = fullfile(pdf_dir, g_dir);
                    if ~strcmp(g_name(max(1,end-3):end), '.png')
                        g_name = [g_name '.png'];
                    end
                    fpath = ['../' fullfile(g_dir, g_name)];
                end
                dir_test_create(pathstr);
                set(g_handle, 'PaperPositionMode', 'auto');
                saveas(g_handle, fullfile(pathstr, g_name), 'png');
                
                if ~opt.get('saveonly')
                    if ~strcmp(opt.get('scale'), 'automatic')
                        latex('include_graphic', fullfile(pdf_dir, filename), fpath, varargin{:});
                    else
                        latex('include_graphic_auto_scale', fullfile(pdf_dir, filename), fpath, varargin{:});
                    end
                else
                    rslt = fpath;
                end
                
            case 'html'
                if isempty(g_name)
                    id = 1;
                    files_ = dir(fullfile(pdf_dir, g_dir));
                    if ~isempty(files_);
                        files_names = cat(1, {files_.name});
                        files_names(1:2) = [];
                        for i = 1 : length(files_names)
                            f_name = files_names{i};
                            if f_name(1) == 't'
                                if (strcmp(f_name(1:4), 'tmp_') && strcmp(f_name(end - 3: end), '.png'))
                                    id = max(str2double(f_name(5:end-4)) + 1, id);
                                end
                            elseif f_name(1) > 't'
                                break;
                            end
                        end
                    end
                    g_name = sprintf('tmp_%d.png', id);
                end
                dir_test_create(pathstr);
                set(g_handle, 'PaperPositionMode', 'auto');
                saveas(g_handle, fullfile(pathstr, g_name), 'png');
                if ~opt.get('saveonly')
                    if ~strcmp(opt.get('scale'), 'automatic')
                        latex('include_graphic',filename, fullfile(g_rel_dir,g_name), varargin{:});
                    else
                        latex('include_graphic_auto_scale',filename, fullfile(g_rel_dir,g_name), varargin{:});
                    end
                else
                    rslt = fpath;
                end
        end
        
        if opt.get('close')
            close(g_handle);
        end
        
        
    case 'include_graphic_auto_scale'
        rslt = ['\\begin{center}\n'...
            '{\\scalegraphics{' strrep(varargin{1}, '\', '/') '}}\n'...
            '\\end{center}\n'];
        print_in_file(rslt, fullfilename, 'a+');
    case 'include_graphic'
        graph_fpath = varargin{1};
        opt = options({ 'width', false, ...
            'rotate', [], ...
            'scalebox', 0.75}, varargin(2:end));
        switch lower(output_mode)
            case 'tex'
                if opt.get('width')
                    str1 = '';
                    if ~isempty(opt.get('rotate')) && opt.get('rotate') == 90
                        str2 = sprintf('[width=%3.2f\\\\textheight,height=%3.2f\\\\textwidth,keepaspectratio=true]', ...
                            opt.get('scalebox'), opt.get('scalebox'));
                    else
                        str2 = sprintf('[width=%3.2f\\\\textwidth]', opt.get('scalebox'));
                    end
                else
                    str1 = ['\\scalebox{' num2str(opt.get('scalebox')) '}'];
                    str2 = '';
                end
                rot = opt.get('rotate');
                if ~isempty(rot)
                    str2 = [str2(1:end-1) sprintf(',angle=%3.0f]', rot)];
                end
                rslt = ['\\begin{center}\n'...
                    str1 '{\\includegraphics' str2 '{' strrep(graph_fpath, '\', '/') '}}\n'...
                    '\\end{center}\n'];
            case 'html'
                if opt.get('width')
                    str1 = sprintf('width="%1.0f"', opt.get('scalebox')*1024);
                else
                    str1 = '';
                end
                rslt = ['</br><center><img ' str1 ' src="./' strrep(graph_fpath, '\', '/') '"/></center></br>'];
        end
        print_in_file(rslt, fullfilename, 'a+');
        
    case 'compile'
        switch lower(output_mode)
            case 'tex'
				current_dir = pwd;
                opt = options({'compile-twice', true, 'open_file', true, 'fullpath', false}, varargin);
                if opt.get('fullpath')
                    [d,f,x] = fileparts( ofilename);
                    cd(d);
                else
                    cd(tex_dir);
                end
                system(['pdflatex "' filename '"']);
                if (opt.get('compile-twice'))
                    system(['pdflatex "' filename '"']);
                end
                pdf_file = strrep( filename, '.tex', '.pdf');
                if ~opt.get('fullpath')
                    movefile(pdf_file, '..');
                    cd('..');
                end
                if opt.get('open_file') && ispc()
                    winopen( pdf_file);
                end
				cd(current_dir)
            case 'html'
                st_log('latex: nothing to compile in html mode...\n');
        end
    otherwise
        error('ERROR in latex, mode : <%s> unknown', mode);
end

% Ouvre le mode dans le mode d'?criture qui convient, ?crit et referme le
% fichier
function print_in_file(str, file_, openning_mode)
f = fopen(file_, openning_mode);
fprintf(f, str);
fclose(f);

% convertit un texte en printable tex (texte que l'on peut printer dans
% un fichier tex en dehors des formules)
function str = text_to_pable_tex(str, output_mode)
str = text_to_printable_text(text_to_tex_text(str, output_mode));


% teste si un r?pertoire existe, sinon il le cr?? en avertissant
% l'utilisateur
function dir_test_create(directory_)
if exist(directory_, 'dir')~=7
    st_log('Creating directory : <%s> \n', directory_);
    mkdir(directory_);
end