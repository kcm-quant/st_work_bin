function tbx= loadEvent(set_test_mode)
% load events from database
% Utilise la nouvelle version de BDD avec table d'influence

% reinit magique en cas d'interruption de requete
%  -> setdbprefs('DataReturnFormat', 'cellarray')
%

if nargin==0; set_test_mode=[];end

if ~isempty(set_test_mode)
    test_mode= set_test_mode;
    return;    
end

tbx= [];
tbx.getEvent= @getAllEvent_; % main function
%tbx.getEvent= @(varargin)getAllEvent_(varargin{:},test_mode); % main function
tbx.TDC= @evList2TDC_;
tbx.makedate= @makedate_;
tbx.name_= 'load Event toolbox : loadEvent.m';
tbx.getAllData= @TEST_allevents;
tbx.pipoEvent= @pipoEvent;
tbx.filter= @filter;

function [tdc,e]= getAllEvent_(sec_id, filtercallback)    
    persistent test_mode    
    if nargin== 2 & ischar(sec_id) & strcmpi(sec_id,'test_mode')
        test_mode= filtercallback;
        return
    end
    
    if nargin< 1; sec_id=[];end
    if nargin< 2; filtercallback= @(x)x;end    
    
    global st_version
    br = st_version.bases.calendar; %'market';% br = st_version.bases.repository;
    types= generic_structure_request('KGR', sprintf('select * from %s..CALEVENTTYPE', br));
    if isempty(sec_id)
        r= sprintf('select * from %s..[CALEVENTBYSECURITY]', br);
        %r= ['select * from market..cal_event_by_security']; 
    else
        r= sprintf('select * from %s..[CALEVENTBYSECURITY] where SECURITYID= %s', br,num2str(sec_id));
    end
    e= basicRequest_('loadEvent.getAllEvent_()',r);
    % Attention les events dans types concernent ts les titres
    % les news non associ�es au titre dans basicRequest_() disparaissent
    ee=e;
    e= filtercallback(e);
    
    if ~isempty(test_mode)
       e= filter(e,[100 10 15]);        
    end
    
    tdc= TDC(e);            
    tdc= associerInfo(tdc,types);

    % TODO: sort par event_type

    % on retire les events �l�mentaire dont on traite les sous-events
    %  par exemple: pas de march� ferm� car on traite march� par march�.
    REMOVED_EVENT_TYPE= [4,5,6,7,8];
    tdc= removeTypes(tdc,REMOVED_EVENT_TYPE);
    check_dim(tdc)
    tdc.sec_id= sec_id;
    %display_stats(tdc)

function check_dim(tdc)
    T= length(tdc.date);
    assert(size(tdc.value,1)==T);
    P= size(tdc.value,2);
    assert(length(tdc.colnames)==P);
    assert(length(tdc.col_id)==P);
    
function e= basicRequest_(name,r,dates)
    e= generic_structure_request('KGR', r);
    %e= generic_structure_request('GANDALF', r);
    e.name = name;
    if ~ isfield(e,'x'); e.x= [];end
    if nargin>= 3
        [d_,num1]= makedate_(dates(1)); e.x= e.x(e.x>= num1 );
        [d_,num2]= makedate_(dates(2)); e.x= e.x(e.x<= num2 );
    end
    
   assert(length(unique([e.SECURITYID]))==1, 'Il y plusieurs sec_id dans la requete !');
    T= length(e.EVENTID);
    assert(length(e.EVENTDATE)==T);
    assert(length(e.EVENTTYPE)==T);
    assert(length(e.SECURITYID)==T);
 
    
function tdc= TDC(data, dates) 
    if nargin== 1
        dates= [min(datenum(data.EVENTDATE,'yyyy-mm-dd')),max(datenum(data.EVENTDATE,'yyyy-mm-dd'))];
    end
    % Attention:
    %  si historique ant�rieur � dates(1) -> indexError
    %  si historique > dates(2) => pas le mm nombre de date et de value
    tdc=[];
    [a,b,c]= unique(data.EVENTTYPE);
    nC= max(c);
    tdc.date= (dates(1):dates(2))';
    edate= datenum(data.EVENTDATE,'yyyy-mm-dd');
    tdc.value= zeros(length(tdc.date),nC);

    linearInd = sub2ind(size(tdc.value), 1+edate-dates(1), c);        
    tdc.value(linearInd) = 1;        
    %tdc.value(1+edate-dates(1),c) = 1;        
    tdc.col_id= a;

function tdc= associerInfo(tdc,types) 
    tdc.colnames={};
    for id =tdc.col_id(:)'
        if any(types.EVENTTYPE==id)
            tdc.colnames{end+1}= types.SHORTNAME{types.EVENTTYPE==id};
        else
            tdc.colnames{end+1}= nan;
        end
    end
        
        
function x= display_stats(data)
    x= [num2str(data.col_id) repmat('.',size(data.value,2),1) char(data.colnames) repmat(' ',size(data.value,2),1) num2str(sum(data.value)')];                                    
        
function tdc= removeTypes(tdc,toRemove)        
    i= ismember(tdc.col_id,toRemove);
    tdc.removed_in_loadEvent= tdc.col_id(i);
    tdc.col_id(i)= [];
    tdc.value(:,i)= [];
    tdc.colnames(i)= [];


function tdc= pipoEvent(sec_id,dates)
    dates= dates(1):dates(2);
    dates= dates(:);
    
    J= weekday(dates);    
    idx= J>1 & J<7;
    J= J(idx);
    dates= dates(idx);    
    tdc=[];
    tdc.value= [];tdc.colnames={};
    tdc.date= dates;    
    days_= {'Sun','Mon','Tue','Wed','Thu','Fri','Sat'};
    u= unique(J);u= u(:)';       
    for modal= u
        tdc.value(J== modal,end+1) = 1;        
        tdc.colnames{end+1}= days_{modal};
    end
    
    [Y, M, D] = datevec(dates);
    D= floor((D-1)/7)+ 1;    
    % croisement D * J
    D= 100*J+D;
    
    u= unique(D);u= u(:)';           
    for modal= u
        tdc.value(D== modal,end+1) = 1;  
        tdc.colnames{end+1}= num2str(modal);
    end
        
    tdc.col_id= 1:length(tdc.colnames);
    tdc.removed_in_loadEvent= [];
    tdc.sec_id= sec_id;
    tdc.removed= {};
    

function e=filter(e,keep_,remove_,select_fun)
if nargin<3; remove_=[];end
if nargin<2; keep_=[];end
if nargin<4; select_fun= [];end
if ~isempty(keep_)
    idx= ismember([e.EVENTTYPE],keep_);
    e.EVENTID=e.EVENTID(idx);e.EVENTDATE=e.EVENTDATE(idx);
    e.EVENTTYPE=e.EVENTTYPE(idx);e.SECURITYID=e.SECURITYID(idx);
end
if ~isempty(remove_)
    idx=  ~ismember([e.EVENTTYPE],remove_);
    e.EVENTID=e.EVENTID(idx);e.EVENTDATE=e.EVENTDATE(idx);
    e.EVENTTYPE=e.EVENTTYPE(idx);e.SECURITYID=e.SECURITYID(idx);
end
if ~isempty(select_fun)
    idx= select_fun([e.EVENTTYPE]);
    e.EVENTID=e.EVENTID(idx);e.EVENTDATE=e.EVENTDATE(idx);
    e.EVENTTYPE=e.EVENTTYPE(idx);e.SECURITYID=e.SECURITYID(idx);
end




 
%%
function test()

tbx= loadEvent();
tbx.getEvent('test_mode',1);
[t,e]=tbx.getEvent(2)

unique(e.event_type);
L= [3   4   5   6   7   8   9  10  11  12  13  14  15  20  21  100 101  102  103  104  105  106  107  108  109  110  111  112  113  114  115  116 117  118  119]
[t,e]=tbx.getEvent(2,@(x) tbx.filter(x,L))
  