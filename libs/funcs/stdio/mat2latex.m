function s = mat2latex(M, header, varargin)
% MAT2LATEX - transforme un tableau matlab en tableau latex
%
% 'linesHeader', [],...
% 'bold_linesHeader',[],...
% 'landscape', false, ...
% 'longtable', false, ...
% 'format', '%2.2f', ...
% 'maxlines', 50, ...
% 'maxcols', 12, ...
% 'plain', false, ...
% 'bold_matrix', [], ...
% 'underline_matrix', [],
% 'output-mode', 'tex'|'html'
%
% See also latex
opt = options({ 'linesHeader', [],...
    'landscape', false, ...
    'longtable', false, ...
    'format', '%2.2f', ...
    'maxlines', 50, ...
    'maxcols', 12, ...
    'plain', false, ...
    'bold_matrix', [], ...
    'output-mode', 'tex', ...
    'underline_matrix', [], ...
    'col_format', 'c', ...
    'full_col_spec', [], ...
	'bold_linesHeader',[],...
    }, varargin);

[m n] = size(M);
linesHeader = opt.get('linesHeader');
bold_linesHeader = opt.get('bold_linesHeader');
boldMatrix = opt.get('bold_matrix');
cf = opt.get('col_format');
om = lower(opt.get('output-mode'));
fcs = opt.get('full_col_spec');

switch om
    case 'tex'
        if ischar(boldMatrix)
            switch boldMatrix
                case 'min'
                    [trash,idx_min] = min(M, [], 2);
                    boldMatrix = false(m, n);
                    for i = 1 : m
                        boldMatrix(i, idx_min(i)) = true;
                    end
                otherwise
                    error('mat2latex:exec', 'unknown bold_matrix mode : <%s>', boldMatrix);
            end
        end
        underlineMatrix = opt.get('underline_matrix');
        if ischar(underlineMatrix)
            switch underlineMatrix
                case 'min'
                    [trash,idx_min] = min(M, [], 2);
                    underlineMatrix = false(m, n);
                    for i = 1 : m
                        underlineMatrix(i, idx_min(i)) = true;
                    end
                otherwise
                    error('mat2latex:exec', 'unknown underline_matrix mode : <%s>', underlineMatrix);
            end
        end
        % A few test to warn the user about big mistakes
        if ~isempty(linesHeader) && length(linesHeader) ~= m
            error('mat2latex:exec', 'Wrong number of lines header');
        end
        if length(header) ~= n && length(header) ~= n+1
            error('mat2latex:exec', 'Wrong number of columns header');
        end
        if ~isempty(boldMatrix)
            [m_tmp n_tmp] = size(boldMatrix);
            if any([m_tmp n_tmp] ~= [m n])
                error('mat2latex:exec', 'Your bold_matrix is not the same size as the matrix you''d like to print');
            end
        end
        if ~isempty(underlineMatrix)
            [m_tmp n_tmp] = size(underlineMatrix);
            if any([m_tmp n_tmp] ~= [m n])
                error('mat2latex:exec', 'Your undeline_matrix is not the same size as the matrix you''d like to print');
            end
        end
        
        % Recursive calls to divide a very big matrix into small ones which
        % may be printed on a pdf page
        if m > opt.get('maxlines') || n > opt.get('maxcols')
            ml = opt.get('maxlines');
            mc = opt.get('maxcols');
            nb_tab_4lines = floor(m / ml);
            if nb_tab_4lines * ml ~= m
                nb_tab_4lines = nb_tab_4lines + 1;
            end
            nb_tab_4cols  = floor(n / mc);
            if nb_tab_4cols * mc ~= n
                nb_tab_4cols = nb_tab_4cols + 1;
            end
            nblines_pertab = ceil(m / nb_tab_4lines);
            nbcols_pertab  = ceil(n / nb_tab_4cols);
            s = '';
            firstHeader = '';
            if length(header) == n + 1
                firstHeader = header{1};
                header(1) = [];
            end
            for i = 1 : nb_tab_4lines
                idx_l = 1 + (i - 1) * nblines_pertab : min(nblines_pertab + (i - 1) * nblines_pertab, m);
                cols_partial_mat2str(idx_l);
            end
            if idx_l(end) ~= m % il y a un petit r�sidu pour lequel on doit faire un autre tableau
                cols_partial_mat2str(idx_l(end)+1 : m);
            end
            return;
        end
        
        format_ = opt.get('format');
        if ~iscell(format_)
            format_ = repmat( { format_ }, 1, size(M,2));
        elseif length(format_) ~= size(M,2)
            st_log('Warning: you should give me one format for each column');
        end
        
        %Write begin tabular
        s = ['\\begin{table}[htp!]\n\\caption{' text_to_printable_text(opt.get('caption')) '}\n\\begin{center}\n\\begin{'];
        if opt.get('longtable')
            s = [s 'longtable'];
        else
            s = [s 'tabular'];
        end
        s = [s '}{|'];
        if opt.get('landscape')
            s = ['\\begin{landscape}\n' s];
        end
        if isempty(fcs)
            s = [s repmat([cf '|'], 1, length(header) + ~isempty(linesHeader))];
        else
            s = [s fcs];
        end
        s = [s '}\n \\hline \n '];
        
        %Divide headers into a few lines, so that they are not too long
        if length(header) == size(M, 2) && ~isempty(linesHeader)
            header = {'', header{:}};
        end
        if opt.get('plain')
            tmp = sprintf('%s& ', header{:});
            s = [s tmp(1:end-2) '\\\\ \\hline \n '];
        else
            s = [s headerFormater(header) '\\hline \n '];
        end
        %Writes data
        for i = 1 : m
            if ~isempty(linesHeader)
                bold_linesHeader = opt.get('bold_linesHeader');
                if ~isempty(bold_linesHeader) && bold_linesHeader(i)
                    s = [s '\\textbf{' cell2mat(linesHeader(i)) '} & '];  
                else
                    s = [s cell2mat(linesHeader(i)) ' & '];    
                end
            end
            for j = 1 : n - 1
                s = [s write_cell_and_data() ' & '];
            end
            j = n;
            s = [s write_cell_and_data() '\\\\ \n '];
        end
        
        %Writes end tabular
        s = [s '\\hline \n \\end{'];
        if opt.get('longtable')
            s = [s 'longtable'];
        else
            s = [s 'tabular'];
        end
        s= [s '} \n \\end{center} \n \\end{table}\n'];
        if opt.get('landscape')
            s = [s '\\end{landscape} \n'];
        end
        
    case 'html'
        
        [nbR, nbC] = size(M); % values
        
        
        %///// link mode for values
        link_mode    = opt.get('link');
        link_matrix  = opt.get('link_matrix');
        link_prefix  = opt.get('link-prefix');
        link_postfix = opt.get('link-postfix');
        link_linesHeader=opt.get('link_linesHeader');
        link_col_header=opt.get('link_col_header');
        linesHeader_rplacelink=opt.get('linesHeader_rplacelink');
        col_header_rplacelink=opt.get('col_header_rplacelink');
        % linesHeader_view : if =true then we show on html this value linesHeader
        my_format = opt.get('format');
        %///// test on link mode for values
        use_link_matrix=false;
        if link_mode && ~isempty(link_matrix)
            [nbRlink, nbClink] = size(link_matrix);
            if any([nbR, nbC] ~= [nbRlink, nbClink]) || ~iscell(link_matrix)
                error('mat2latex:html', 'Your link_matrix is not the same size as the matrix you''d like to print');
            end
            use_link_matrix=true;
        end
        %///// test on rplace link
        if (link_linesHeader && ~isempty(linesHeader_rplacelink) && ...
                length(linesHeader_rplacelink)~=length(linesHeader)) || ...
                (link_col_header && ~isempty(col_header_rplacelink) && ...
                length(col_header_rplacelink)~=length(header))
            error('mat2latex:html', 'Input "rplacelink" is bad');
        end
        % // test on link prefix
        if (iscell(link_prefix) && link_linesHeader && ...
                length(linesHeader)~=length(link_prefix)) || ...
                (iscell(link_prefix) && link_col_header && ...
                length(header)~=length(link_prefix))
            error('mat2latex:html', 'Input "link-prefix" is bad');
        end
        
        %///// colheader
        
        s = '</br><center><table class="sortable" border="1">\n';
        s = [s, '<tr><th></th>'];
        for i_c=1:nbC
            if link_col_header
                if ~isempty(col_header_rplacelink)
                    tmp_header_link= col_header_rplacelink{i_c};
                else
                    tmp_header_link=header{i_c};
                end
                if iscell(link_prefix)
                    link_prefix_tmp=link_prefix{i_c};
                else
                    link_prefix_tmp=link_prefix;
                end
                if ~isempty(tmp_header_link)
                    linescolheader_tmp = sprintf('<a href="%s%s%s">%s</a>', ...
                        link_prefix_tmp,tmp_header_link, ...
                        link_postfix, header{i_c});
                    s = [s, '<th align="right">' linescolheader_tmp '</th>'];
                else
                    s = [s, '<th align="right">' header{i_c} '</th>'];
                end 
            else
                s = [s, '<th align="right">' header{i_c} '</th>'];
            end
        end
        s = [s, '</tr>'];
        
        
        %///// values and lines header
        
        for r=1:nbR
            if link_linesHeader
                if ~isempty(linesHeader_rplacelink)
                    tmp_header_link= linesHeader_rplacelink{r};
                else
                    tmp_header_link=linesHeader{r};
                end
                if iscell(link_prefix)
                    link_prefix_tmp=link_prefix{r};
                else
                    link_prefix_tmp=link_prefix;
                end
                if ~isempty(tmp_header_link)
                    linesHeader_tmp = sprintf('<a href="%s%s%s">%s</a>', ...
                        link_prefix_tmp, tmp_header_link, ...
                        link_postfix, linesHeader{r});
                    s = [s, '<tr><th align="right">' linesHeader_tmp '</th>'];
                else
                    % no link is possible
                    s = [s, '<tr><th align="right">' linesHeader{r} '</th>'];
                end
            else
                if ~isempty(linesHeader)
                    s = [s, '<tr><th align="right">' linesHeader{r} '</th>'];
                else
                    s = [s, '<tr><th align="right">' '' '</th>'];
                end
            end
            
            for c=1:nbC
                if iscell(M)
                    Mrc = M{r,c};
                else
                    Mrc = M(r,c);
                end
                if ischar( Mrc)
                    str = Mrc;
                    str_sign = { '', ''};
                else
                    if Mrc<0
                        str_sign = { '<font color="#AA0000">', '</font>'};
                    else
                        str_sign = { '', ''};
                    end
                    
                    if isempty( my_format)
                        str = num2str(Mrc);
                    elseif iscell(my_format)
                        str = sprintf(my_format{c}, Mrc);
                    else
                        str = sprintf(my_format, Mrc);
                    end
                end
                if link_mode
                    if use_link_matrix
                        if ~isempty(link_matrix{r,c})
                            str = sprintf('<a href="%s%s%s">%s</a>', ...
                                link_prefix, link_matrix{r,c},link_postfix, str);
                        end
                    else
                        str = sprintf('<a href="%s%s%s%s">%s</a>', ...
                            link_prefix, header{c}, str, link_postfix, str);
                    end
                end
                s = [s, '<td align="right">' str_sign{1} str str_sign{2} '</td>'];
            end
            s = [s, '</tr>'];
        end
        
        s = [s, '</table></center></br>\n'];
        
    otherwise
        error('mat2latex:output_mode', 'output mode <%s> unknown', opt.get('output-mode'));
end

    function str = write_cell_and_data()
        c = M(i, j);
        if iscell(c)
            c= c{:};
            if iscell(c)
                error('mat2latex:check_args', 'cells containing cells are not handled');
            end
            if ischar(c)
                if ~opt.get('plain')
                    str = text_to_printable_text(text_to_tex_text(c, om));
                else
                    str = strrep(c,'\','\\');
                end
            else
                str = num2str(c, format_{j});
            end
        else
            str = num2str(c, format_{j});
        end
        switch om
            case 'tex'
                if ~isempty(boldMatrix) && boldMatrix(i, j)
                    str = ['\\textbf{' str '}'];
                end
                if ~isempty(underlineMatrix) && underlineMatrix(i, j)
                    str = ['\\underline{' str '}'];
                end
        end
    end

    function partial_mat2str(idx_l, idx_c)
        M_trunc = M(idx_l, idx_c);
        if ~isempty(boldMatrix)
            opt.set('bold_matrix', boldMatrix(idx_l, idx_c));
        end
        if ~isempty(underlineMatrix)
            opt.set('underline_matrix', underlineMatrix(idx_l, idx_c));
        end
        if ~isempty(linesHeader)
            header_trunc = {firstHeader, header{idx_c}};
        else
            header_trunc = header(idx_c);
        end
        lst = opt.get();
        s = [s mat2latex(M_trunc, header_trunc, lst{:})];
    end

    function cols_partial_mat2str(idx_l)
        if ~isempty(linesHeader)
            opt.set('linesHeader', linesHeader(idx_l));
        end
        if ~isempty(bold_linesHeader)
            opt.set('bold_linesHeader',bold_linesHeader(idx_l));
        end
        for j = 1 : nb_tab_4cols
            idx_c = 1 + (j - 1) * nbcols_pertab : min((j - 1) * nbcols_pertab + nbcols_pertab, n);
            partial_mat2str(idx_l, idx_c);
        end
        if idx_c(end) ~= n % il y a un petit r�sidu pour lequel on doit faire un autre tableau
            idx_c = idx_c(end) + 1 : n;
            partial_mat2str(idx_l, idx_c);
        end
    end
end

function s = headerFormater(headers)
n = length(headers);
headersWords = {};
s = [];
maxLines = 0;
for i = 1 : n
    maxLenWord = 0;
    headerWords = strread(cell2mat(headers(i)), '%s ');
    for word = headerWords'
        maxLenWord = max(maxLenWord, length(cell2mat(word)));
    end
    headerDivided = {};
    if ~isempty(headerWords)
        for i = 1 : length(headerWords)
            word = cell2mat(headerWords(i));
            if isempty(headerDivided)
                headerDivided = {word};
            elseif length(headerDivided{end}) + length(word) < maxLenWord
                headerDivided(end) = {[headerDivided{end} ' ' word]};
            elseif length(headerDivided{end}) + length(word) <= maxLenWord + 1 ...
                    && length(headerWords) > 2 ...
                    && (    i == length(headerWords) ...
                    ||  length(headerWords{i + 1}) >= length(headerDivided{end}))
                headerDivided(end) = {[headerDivided{end} ' ' word]};
            else
                headerDivided = vertcat(headerDivided, word);
            end
        end
        maxLines = max(maxLines, length(headerDivided));
        headersWords = horzCatCellVector(headersWords, headerDivided);
    else
        headersWords =  horzCatCellVector(headersWords, {''});
    end
end
for i = 1 : maxLines
    for j = 1 : n - 1
        headerDivided = headersWords{j};
        if i > length(headerDivided)
            s = [s ' & '];
        else
            s = [s cell2mat(headerDivided(i)) ' & '];
        end
    end
    headerDivided = headersWords{n};
    if i > length(headerDivided)
        s = [s ' \\\\ \n'];
    else
        s = [s cell2mat(headerDivided(i)) ' \\\\ \n'];
    end
end
end



function outCell = horzCatCellVector(cell1, cell2)
[n1 m1] = size(cell1);
[n2 m2] = size(cell2);
outCell = cell(1, m1 + m2);
for i = 1 : m1
    outCell(i) = cell1(:, i);
end
for i = 1 : m2
    outCell(i + m1) = {cell2(:, i)};
end
end