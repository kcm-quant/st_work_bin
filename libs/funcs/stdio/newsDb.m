function self=newsDb(with_nonews)

global st_version
br = st_version.bases.calendar; %repository;

if nargin<1;with_nonews=0;end
self= [];
%self.types= generic_structure_request('BSIRIUS', sprintf('select * from %s..cal_event_type', br));
self.types= generic_structure_request('CAL_EVENT', sprintf('select * from %s..cal_event_type', br));
if with_nonews
    self.types.event_type(end+1)= 0;
    self.types.short_name{end+1}= 'No news';
end
self.types.size= length(self.types.event_type);
self.id2name= @(x) arrayfun(@id2name,x,'uni',0);
self.name2id= @(x) cellfun(@name2id,x,'uni',1);

    function n=id2name(id)
        x= find(self.types.event_type==id);
        if length(x)~=1
            error('id2name');
        end
        n= self.types.short_name{x};
    end

    function id= name2id(name)
        x= strcmpi(name,self.types.short_name);
        x= find(x);
        if length(x)~=1
            error('name2id');
        end
        id= self.types.event_type(x);
        
    end


end


function TEST()
     n= newsDb()
    n.id2name([1 2])
    
    n.name2id({'monthly_derivative_expiry','expiry_auction'})
     
   
l={'expiry_auction', 'new_york_shift' , 'unclosed_holiday','half_day_trading'            ,'derivative_expiry','new_york_closed','london_closed','paris_closed','frankfurt_closed','earnings','earnings_conf_call','us-employment_situation','us-gdp','us-employment_cost_index','us-retail_sales','us-ism_manufacturing','us-ppi','us-new_housing_sales','us-durable_goods_orders','us-existing_home_sales','us-consumer_confidence','us-construction_spending','us-philly_fed_survey','us-initial_jobless_claims','de-zew_business_sentiment','us-real_earnings','us-import_price_index','us-ism_non-manufacturing','us-factory_orders','us-housing_starts','us-eia_petroleum_status_report','monthly_derivative_expiry'};
    
    
end