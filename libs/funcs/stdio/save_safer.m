function save_safer(filen, varargin)
% SAVE_SAFER - used to save variable in a file, but in a safer way than
% buitlin save matlab does. May be used in parallel saves, to prevent file 
% corruptions, but nothing really guaranteed.
%
%
%
% Examples:
%
% save('/home/team13/test.mat', myvarname);
% 
%
% See also:
%    save
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   date     :  '25/10/2013'
%   
%   last_checkin_info : $Header: mode_func_template.m: Revision: 1: Author: robur: Date: 01/12/2012 02:19:52 PM$

assert(strcmp(filen(end-3:end), '.mat'),'save_safer:check_args', ...
    'Only implemented for .mat files');
assert(iscellstr(varargin),'save_safer:check_args', ...
    'Only implemented for .mat files');
if isempty(varargin)
    varargin = '';
else
    varargin = sprintf(',''%s''', varargin{:});
end

filentxt = [filen(1:end-4) '.txt'];
nbiter = 1;
while exist(filentxt, 'file')
    pause(2^(nbiter-4));
    nbiter = nbiter + 1;
    if nbiter > 14 % 2^10 seconds is 17 minutes
        error('save_safer:check_time', 'Too long a time to wait');
    end
end

fid = fopen(filentxt, 'w');
fclose(fid);
assert(fid ~= -1, 'save_safer:check_wrights', 'Unable to create .txt file');

evalin('caller', ['save(''' filen '''' varargin ')']);

delete(filentxt);
assert(~exist(filentxt, 'file'), 'save_safer:check_wrights', 'Unable to create .txt file');


end