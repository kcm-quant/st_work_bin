function str = text_to_printable_text(str)
% convertit un texte en printable texte (texte que l'on peut utiliser dans 
% un sprintf, fprintf, etc...)

translate_exp = {'\', '\\';...  % TODO better
                 '%', '%%'};
for i = 1 : size(translate_exp, 1)
    str = strrep(str, translate_exp{i, 1}, translate_exp{i, 2});
end