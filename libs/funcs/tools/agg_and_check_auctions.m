function [a_vals, a_dts] = agg_and_check_auctions(data_a, trdt, time_accuracy, curr_day, main_td)
a_vals = [];
a_dts = [];

if nargin > 4
    data_a = st_data('where', data_a, ['{trading_destination_id} == ' num2str(main_td)]);
end

nb_auction_type_handled = 4; % auction d'ouveture/auction de fermeture/auctions milieu de journ�e/auctions stop de volatilit� cette derni�re n'a le droit d'�tre pr�sente qu'une fois par jour, cette fonction n'est pas pr�vue pour plus. TODO : better
auc_vals  = cell(nb_auction_type_handled, 1);
auc_date  = NaN(nb_auction_type_handled, 1);
auc_flags = {[1,1,0,0,0];[1,0,1,0,0];[1,0,0,1,0];[1,0,0,0,1]};
auc_form = {'{opening_auction}', '{closing_auction}', '{intraday_auction}', '{auction} & ~({opening_auction}|{closing_auction}|{intraday_auction})'};

%< traitement de l'auction d'ouveture
[auc_vals{1}, auc_date(1)] = process_auction(data_a, auc_form{1}, ...
    trdt.opening_auction, trdt.opening_fixing, trdt.opening, time_accuracy, false, curr_day);
%>
%< traitement de l'auction de fermeture
[auc_vals{2}, auc_date(2)] = process_auction(data_a, auc_form{2}, ...
    trdt.closing_auction, trdt.closing_fixing, trdt.closing, time_accuracy, false, curr_day);
%>
%< traitement des auctions milieu de journ�e
if nargin > 4 && main_td == 37 % HACK : cas special de KUALA LUMPUR le point de vue adopt� pour l'instant est que l'on fait la somme des deux auction intraday en esp�rant qu'elles sont correctement flaggu�es
    [auc_vals{3}, auc_date(3)] = process_auction(data_a, auc_form{3}, ...
    trdt.intraday_stop_auction, trdt.intraday_resumption, trdt.intraday_resumption, time_accuracy, false, curr_day);
else
    [auc_vals{3}, auc_date(3)] = process_auction(data_a, auc_form{3}, ...
    trdt.intraday_resumption_auction, trdt.intraday_resumption_fixing, trdt.intraday_resumption, time_accuracy, false, curr_day);
end
%>
%< traitement des auctions qui ne sont pas flaggu�s ni opening ni intraday
% ni closing
[auc_vals{4}, auc_date(4), more_auc_date] = process_auction(data_a, auc_form{4}, ...
    NaN, NaN, NaN, time_accuracy, true, curr_day);
%>

%< On filtre les donn�es vides, on rassemble les autres, et on
% rajoute les NaN pour les colonnes n'ayant pas de sens pour les
% auctions
%>


% % ALL DATA COLNAMES
% {'volume', 'vwap', 'nb_trades', ... % available data in auctions
% 'volume_sell', 'vwap_sell', 'nb_sell', 'vwas', 'vol_GK', ...
%     'open','high','low','close', ...
%     'bid_open', 'bid_high', 'bid_low','bid_close', ...
%     'ask_open', 'ask_high', 'ask_low','ask_close', ...
%     'mean_price', ...
%     'auction', 'opening_auction', 'closing_auction', 'intraday_auction', 'stop_auction'};% available data in auctions
%


% { 'vwap_sell', 'nb_sell', 'vwas', 'vol_GK', ...
%     'open','high','low','close', ...
%     'bid_open', 'bid_high', 'bid_low','bid_close', ...
%     'ask_open', 'ask_high', 'ask_low','ask_close', ...
%     'mean_price'}
% definition des valeurs par defaut pour les colonnes nomm�es ci-dessus
% 
output_def_vals = [NaN, 0, NaN, NaN, ...
    NaN(1,4), ...
    NaN(1,4), ...
    NaN(1,4), ...
    NaN];
for i = 1 : 3
    if ~isempty(auc_vals{i})
        a_vals(end+1, :) = [auc_vals{i},auc_vals{i}(1)/2,output_def_vals,auc_flags{i}];
        a_dts(end+1) = auc_date(i);
    end
end
if ~isempty(auc_vals{4})
    a_vals(end+1, :) = [auc_vals{4}(1, :),auc_vals{4}(1)/2,output_def_vals,auc_flags{4}];
    a_dts(end+1) = auc_date(4);
    for i = 2 : size(auc_vals{4}, 1)
        a_vals(end+1, :) = [auc_vals{4}(i, :),auc_vals{4}(i,1)/2,output_def_vals,auc_flags{4}];
        a_dts(end+1) = more_auc_date(i-1);
    end
end
end

function [auc_vals, auc_time, more_auc_date] = process_auction(data_a, where_clause, begin_t, fixing_t, end_t, time_accuracy, isstop, curr_day)
more_auc_date = [];
EPS_TIME = datenum(0,0,0,0,1,0);


% TODO : simplifier ce bazard, et faire en sorte que les intraday auction
% passe aussi pour KUALA lumpur

[tmp_data, idx] = st_data('where', data_a, where_clause);
auction_expected = (isfinite(begin_t) || isfinite(fixing_t) )&& isfinite(end_t) ;
auction_is_in_data = numel(idx) && any(idx);
idx_price = strmatch('price', tmp_data.colnames, 'exact');
if isempty(idx_price) && auction_is_in_data
    % grosse verrue lorsque l'on travaille sur les donn�es hors europe
    idx_price = strmatch('sum_price', tmp_data.colnames, 'exact');
    tmp_idx = strmatch('nb_deal', tmp_data.colnames, 'exact');
    tmp_data.value(:, idx_price) = tmp_data.value(:, idx_price)./tmp_data.value(:, tmp_idx);
    if isempty(idx_price)
        error('process_auction:exec', 'unable to work without any prices');
    end
    already_agg_data = true;
    idx_nb_deal = tmp_idx;
else
    already_agg_data = false;
end
idx_volume = strmatch('volume', tmp_data.colnames, 'exact');
if isfinite(fixing_t)
    ref_time = fixing_t;
elseif isfinite(end_t)
    ref_time = end_t;
else
    if ~isempty(tmp_data.date)
        ref_time = mod(tmp_data.date(end), 1);
    else
        ref_time = NaN;
    end
end
if ~auction_is_in_data
    if auction_expected
        auc_time = curr_day + ref_time;
        auc_vals = [0, NaN, 0];
    else
        auc_time = NaN;
        auc_vals = [];
    end
    return;
    % Then the auction is in the data
elseif auction_expected || isstop
    % nothing to do : auction is in the data and this auction is either
    % expected or is a stop auction, this will be treated below
elseif ~isstop && ~auction_expected% auction is in the data, is not expected and is not a stop_auction : problem.... Perhaps a witching day on LSE
    auc_vals = [-1 -1 -1];
    auc_time = NaN;
    return;
else
    error('st_basic_indicator:process_auction', 'Process auction reached an assumed unreachable code');
end
if isstop
    [s_auctions_dates, idx_last] = unique(floor(tmp_data.date/EPS_TIME), 'last');
    s_auctions_dates = s_auctions_dates * EPS_TIME;
    auc_vals = NaN(length(s_auctions_dates), 3);
    auc_time = s_auctions_dates;
    for i = 1 : length(s_auctions_dates)
        if i == 1
            idx_first = 1;
        else
            idx_first = idx_last(i-1) + 1;
        end
        auc_vals(i, :) = [sum(tmp_data.value(idx_first:idx_last(i), idx_volume)) tmp_data.value(idx_last(i), idx_price) idx_last(i)-idx_first+1];
    end
    more_auc_date = auc_time(2:end);
    auc_time = auc_time(1);
else
    if ((mod(tmp_data.date(1), 1) - begin_t < -time_accuracy) || (mod(tmp_data.date(end), 1) - end_t > time_accuracy))
        auc_vals = [-1 -1 -1];
        auc_time = NaN;
        return;
    elseif tmp_data.date(end) - tmp_data.date(1) > time_accuracy 
        if isfield(data_a, 'info') && ~isempty(data_a.info.td_info) && data_a.info.td_info(1).trading_destination_id ~= 37 % HACK KUALAL LUMPUR comme expliqu� ci dessus
            auc_vals = [-1 -1 -1];
            auc_time = NaN;
            return;
        end
    end
    % as the price is supposed to be unique  comuting the real vwap should
    % be the same that taking only the first price.
    % but as there ares ome cases in chich this is not true, but the
    % variraible is vwap, we'll really compute the VWAP 
    % exemple : security_id = 45672, date = 05/02/2009 (dd/mm/yyyy)
    volume = sum(tmp_data.value(:, idx_volume));
    price = sum(tmp_data.value(:, idx_volume).*tmp_data.value(:, idx_price))/...
        sum(tmp_data.value(:, idx_volume));
    if already_agg_data
        auc_vals = [volume,price,sum(tmp_data.value(:, idx_nb_deal))];% volume vwap (prix unique) nb_trades
    else
        auc_vals = [volume,price,length(tmp_data.date)];% volume vwap (prix unique) nb_trades
    end
    auc_time = curr_day + ref_time;
    data_a = st_data('from-idx', data_a, ~idx);
end
end