function data = agg_candles(data, td_id, wind, step, t_acc, bins4stop)

if ~isempty(td_id)
    if isnumeric(td_id)
        data = st_data('where', data, sprintf('{trading_destination_id} == %d', td_id));
    else
        error('agg_candles:exec', 'Don''t know how to handle that typeof trading_destination');
    end
end

[data, b] = st_data('isempty', data);
if b
    return;
end

curr_day = floor(data.date(1));
sec_datenum = 1/(24*3600);

data.date = data.date - sec_datenum;
data = st_data('where', data, '~({trading_after_hours}|{trading_at_last}|{cross})');

% nouveau test
[data, b] = st_data('isempty', data);
if b
    return;
end

if bins4stop
    [data_a, idx_auction] = st_data('where', data, '{auction}');
else
    [data_a, idx_auction] = st_data('where', data, '{opening_auction}|{closing_auction}|{intraday_auction}');
end
data_na = st_data('from-idx', data, ~idx_auction);
[grid, trdt, main_td] = get_time_grid('disjoint', data_na, 'is_gmt:bool', false, ...
    'window:time', wind, 'step:time', step, ...
    'default_date', floor(data.date(1)));

temp = st_data('init','title','temp','date',(1:length(grid(:,1)))','value',...
    [accum_fuction(grid(:,1:2),st_data('cols', data_na, 'volume;turnover;turnover_overbid;turnover_overask;nb_deal;volume_overbid;volume_overask;average_spread_numer;average_spread_denom;sum_price'),@nansum),...
    accum_fuction(grid(:,1:2),st_data('cols',data_na,'open;open_bid;open_ask'),@first),...
    accum_fuction(grid(:,1:2),st_data('cols',data_na,'high'),@max),...
    accum_fuction(grid(:,1:2),st_data('cols',data_na,'low;'),@min),...
    accum_fuction(grid(:,1:2),st_data('cols',data_na,'close'),@last)],...
    'colnames',...
    {'volume','turnover','turnover_overbid','turnover_overask','nb_deal','volume_overbid','volume_overask','average_spread_numer','average_spread_denom','sum_price','open','open_bid','open_ask','high','low','close'});

mean_price = st_data('cols',temp,'sum_price')./st_data('cols',temp,'nb_deal');
vol_GK = sqrt(((st_data('cols',temp,'high')-st_data('cols',temp,'low')).^2/2 - (2*log(2)-1)*(st_data('cols',temp,'close')-st_data('cols',temp,'open')).^2)./( mean_price.^2 * wind/datenum(0,0,0,0,10,0) ))*1e4;



vals = [st_data('cols',temp,'volume'),...    %'volume'
    st_data('cols',temp,'turnover')./st_data('cols',temp,'volume'),...    % 'vwap',
    st_data('cols',temp,'nb_deal'),...    % 'nb_trades',
    st_data('cols',temp,'volume_overbid')+0.5*(st_data('cols',temp,'volume')-sum(st_data('cols',temp,'volume_overbid;volume_overask'),2)),...   % 'volume_sell'
    (st_data('cols',temp,'turnover_overbid')+0.5*(st_data('cols',temp,'turnover')-sum(st_data('cols',temp,'turnover_overbid;turnover_overask'),2)))./...
    (st_data('cols',temp,'volume_overbid')+0.5*(st_data('cols',temp,'volume')-sum(st_data('cols',temp,'volume_overbid;volume_overask'),2))),... % 'vwap_sell'
    nan(length(temp.date),1),...% 'nb_sell',
    st_data('cols',temp,'average_spread_numer')./st_data('cols',temp,'average_spread_denom'),...    % 'vwas',
    vol_GK,...  % 'vol_GK',
    st_data('cols',temp,'open;high;low;close'),... % 'open','high','low','close',
    st_data('cols',temp,'open_bid'),nan(length(temp.date),3),... % 'open_bid', 'bid_high', 'bid_low','bid_close'
    st_data('cols',temp,'open_ask'),nan(length(temp.date),3),... % 'open_ask', 'ask_high', 'ask_low','ask_close'
    mean_price,...    %mean_price
    zeros(length(temp.date),5)];  % 'auction', 'opening_auction', 'closing_auction','intraday_auction', 'stop_auction'


% vals = st_data('keep-cols', data_na, 'volume;turnover;turnover_overbid;turnover_overask;nb_deal;volume_overbid;volume_overask;open;high;low;close;open_ask;open_bid;average_spread_numer;average_spread_denom;sum_price');
% vals = arrayfun(@(inf,sup)sliding_func(st_data('from-idx',vals,inf:sup), wind), grid(:,1), grid(:,2), 'uni', false);
% vals = cat(1, vals{:});

tmp = abs(sum(vals(:, 1)) / sum(st_data('cols', data_na, 'volume')) - 1);


if sum(vals(:, 1))==0
    st_log(['Please have a closer look to day :<%s> for <%s>. \n\t' ...
        'Have a special look to continuous phase trading hours %f %% of the volume is out of what we thought to be the hours of the continuous phase.\n\t' ...
        'As a consequence this day will be deleted from basic_indicator.\n'], datestr(floor(data.date(1))), data.title, 100 * tmp);
    data = st_data('log', data, 'gieo', '''There is no data in trading phase time !!!.');
    return;
end

cont_phase_end = ifnan2ndarg(trdt.closing_auction, trdt.closing); % the end of the continuous phase
cont_phase_begin = trdt.opening; % the begining time of the continuous phase
if abs(wind - step) < datenum(0,0,0,0,0,1) % si step = wind, alors on regarde la proportion de volume perdues en ignorant les donn�es en dehors de ce qui est pour nous les horaires de trading en continu. Si cette proportion est trop grande, on rejette les donn�es
    if tmp > (t_acc/(cont_phase_end-cont_phase_begin)) || ~isfinite(tmp)
        st_log(['Please have a closer look to day :<%s> for <%s>. \n\t' ...
            'Have a special look to continuous phase trading hours %f %% of the volume is out of what we thought to be the hours of the continuous phase.\n\t' ...
            'As a consequence this day will be deleted from basic_indicator.\n'], datestr(floor(data.date(1))), data.title, 100 * tmp);
        data = st_data('log', data, 'gieo', 'There are too much data flagged ''continuous phase'' which are not in what we thought to be the hours of the continuous phase.');
        return;
    end
end

% si step~=wind, alors on regarde au moins si les horaires ne d�bordent pas trop
if cont_phase_end + t_acc < mod(data_na.date(end), 1)
    st_log(['Please have a closer look to day :<%s> for <%s>. \n\t' ...
        'Have a special look to the end of the continuous phase trading. It is supposed to be <%s>, but this particular day, it was <%s>.\n\t' ...
        'As a consequence this day will be deleted from basic_indicator.\n'], datestr(floor(data.date(1))), data.title, datestr(cont_phase_end, 'HH:MM:SS'), datestr(data_na.date(end), 'HH:MM:SS'));
    data = st_data('log', data, 'gieo', sprintf('The end of the continuous phase is supposed to be : <%s> but this set of data ends at : <%s>', datestr(cont_phase_end, 'HH:MM:SS'), datestr(data_na.date(end), 'HH:MM:SS')));
    return;
elseif cont_phase_begin - t_acc > mod(data_na.date(1), 1)
    st_log(['Please have a closer look to day :<%s> for <%s>. \n\t' ...
        'Have a special look to the begining of the continuous phase trading. It is supposed to be <%s>, but this particular day, it was <%s>.\n\t' ...
        'As a consequence this day will be deleted from basic_indicator.\n'], datestr(floor(data.date(1))), data.title, datestr(cont_phase_begin, 'HH:MM:SS'), datestr(data_na.date(1), 'HH:MM:SS'));
    data = st_data('log', data, 'gieo', sprintf('The beginning of the continuous phase is supposed to be : <%s> but this set of data begins at : <%s>', datestr(cont_phase_begin, 'HH:MM:SS'), datestr(data_na.date(1), 'HH:MM:SS')));
    return;
end

% [a_vals, a_dts] = agg_and_check_auctions(data_a, trdt, t_acc, curr_day, main_td);
[a_vals, a_dts] = agg_and_check_auctions(data_a, trdt, t_acc, curr_day); % On agrege les volumes de fixing sur toute les destinations

if any(any(a_vals == -1))
    mess = sprintf('There is a problem with an auction \n\tAs a consequence this day will be deleted from basic_indicator.\n');
    st_log(mess);
    data = st_data('log', data, 'gieo', mess);
    return;
end

vals = [vals; a_vals];
dts  = [grid(:,3); a_dts' + sec_datenum]; % + sec_datenum permet d'assurer l'unicit� des horaires

[data.date, idx_sorted] = sort(dts);
data.value = vals(idx_sorted, :);
%<* on met en forme
data.colnames = {'volume', 'vwap', 'nb_trades', ... % available data in auctions
    'volume_sell', 'vwap_sell', 'nb_sell', 'vwas', 'vol_GK', ...
    'open','high','low','close', ...
    'bid_open', 'bid_high', 'bid_low','bid_close', ...
    'ask_open', 'ask_high', 'ask_low','ask_close', ...
    'mean_price', ...
    'auction', 'opening_auction', 'closing_auction', 'intraday_auction', 'stop_auction'};% available data in auctions
data.date = data.date + sec_datenum;
end

function v = ifnan2ndarg(arg1, arg2)
    if ~isfinite(arg1)
        v = arg2;
    else
        v = arg1;
    end
end

function rslt = accum_fuction(indices,vals,fun)
rslt = arrayfun(@(i)fun_(vals(indices(i,1):indices(i,2),:)),(1:size(indices,1))','uni',false);
rslt = vertcat(rslt{:});
    function out = fun_(x)
        out = fun(x);
        if size(x,1)==1
            out=x;
        elseif isempty(out)
            out = nan(1,size(x,2));
        end
    end
end

function output = last(vals)
if isempty(vals)
    output = nan(1,size(vals,2));
else
    output = vals(end,:);
end
end

function output = first(vals)
if isempty(vals)
    output = nan(1,size(vals,2));
else
    output = vals(1,:);
end
end

function rslt = sliding_func(vals, window)
% expected data
% data.colnames = {'volume', 'vwap', 'nb_trades', ... % available data in auctions
%             'volume_sell', 'vwap_sell', 'nb_sell', 'vwas', 'vol_GK', ...
%             'open','high','low','close', ...
%             'bid_open', 'bid_high', 'bid_low','bid_close', ...
%             'ask_open', 'ask_high', 'ask_low','ask_close', ...
%             'auction', 'opening_auction', 'closing_auction', 'intraday_auction', 'stop_auction'};% available data in auctions
if isempty(vals)
    rslt = [0, NaN, 0, ...
        0, NaN, 0, NaN, NaN, ...
        NaN, NaN, NaN, NaN, ...
        NaN, NaN, NaN, NaN, ...
        NaN, NaN, NaN, NaN, ...
        NaN,...
        0,0,0,0,0];
    return;
end
% input data :
% 'volume;turnover;nb_deal;volume_overbid;volume_overask;
%open;high;low;close;open_ask;
%open_bid;average_spread_numer;average_spread_denom;sum_price'

open        = vals(1, 6);
close       = vals(end, 9);
high        = max(vals(:, 7));
low         = min(vals(:, 8));

nb_trades = sum(vals(:, 3));
mean_price = sum(vals(:, 14)) / nb_trades;

if nb_trades > 1
    vol_GK = sqrt(	( (high-low).^2/2 - (2*log(2)-1)*(close-open).^2 ) / ( mean_price.^2 * window/datenum(0,0,0,0,10,0) )  ) * 10000;
else
    vol_GK = NaN;
end

% Filtrage des nans dans les colonnes average_spread_numer et
% average_spread_denom :
% on filtre sur les indices : ~isnan(vals(:,12))&~isnan(vals(:,13))
rslt = [sum(vals(:, 1)), sum(vals(:, 2)) / sum(vals(:, 1)), nb_trades, ...
    sum(vals(:, 4)) + 0.5 * (sum(vals(:, 1))-sum(vals(:, 4))-sum(vals(:, 5))), NaN, NaN,...
    sum(vals(~isnan(vals(:,12))&~isnan(vals(:,13)), 12))/sum(vals(~isnan(vals(:,12))&~isnan(vals(:,13)), 13)),...
    vol_GK, ...
    open, high, low, close, ...
    vals(1, 11), NaN, NaN, NaN, ...
    vals(1, 10), NaN, NaN, NaN,...
    mean_price, ...
    0,0,0,0,0];

end