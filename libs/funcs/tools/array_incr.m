function out = array_incr(data, data_min, data_max, step, idx)
% ARRAY_INCR - fonction récursive permettant d'incrémenter un tableau de
% compteurs
% 
% array_incr([0, 0, 0], [0, 0, 0], [1, 1, 1], 1, 1)
% 
% Author: edarchimbaud@cheuvreux.com
% Date: 26/01/2009
% Version: 1.0

out = data;

if sum(data == data_max) == length(data)
    return
end

if data(idx) < data_max(idx)
    out(idx) = out(idx) + step;
    return
else
    if idx == length(data)
        return
    else
        out(idx) = data_min(idx);
        out = array_incr(out, data_min, data_max, step, idx + 1);
    end
end

end