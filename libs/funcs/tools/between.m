function idx = between(A,d,f)

% Selection de donn�es entre d et f (inclus)
if ~exist('f', 'var')
    if numel(d)==2
        f = d(2);
        d = d(1);
    else
        f = d(2, :);
        d = d(1, :);
    end
end
idx = d<=A&A<=f;