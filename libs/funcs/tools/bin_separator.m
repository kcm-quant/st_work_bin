function [data_, varargout] = bin_separator(data, varargin)
% BIN_SEPARATOR - D�bitage en tranches
%
% See also cv_select_prod slice_stats
if isempty(data) || isempty(data.value)
    data_ = data;
    return
end

opt = options({'mode', 'intraday', ...
    'colnames_format', 'HH:MM:SS:FFF', ...
    'idx', [], ...
    'cols', {}, ...
    'explicit_colnames', true, ...
    'eps_dt', datenum(0,0,0,0,0,1), ...
    'intersect_hours', false, ...
    }, varargin);
mode = opt.get('mode');
colnames_format = opt.get('colnames_format');
eps_dt      = opt.get('eps_dt');
idx = opt.get('idx');
cols = opt.get('cols');
explicit_colnames = opt.get('explicit_colnames');

switch lower(mode)
    case 'intraday'
        if 1 == length(data.colnames)
            [bin_time,~,bin_num]  = unique(round(mod(data.date, 1)/eps_dt)*eps_dt);
            [day,~,idx_day] = unique(floor(data.date));
            nb_hours    = length(bin_time);
            nb_days     = length(day);
            data_       = data;
            data_.value = accumarray([idx_day,bin_num],data.value);
            
            if isempty(idx)
                if nb_hours*nb_days~=numel(data.value)
                    if ~opt.get('intersect_hours')
                        error('bin_separator:exec', 'Trading hours changed');
                    end
                    nb_elts = accumarray([idx_day,bin_num],1);
                    idx2del = any(nb_elts~=1,1);
                    bin_time    = bin_time(~idx2del);
                    data_.value = data_.value(:,~idx2del);
                    nb_hours = length(bin_time);
                end
            else
                data_.value = nan(nb_days,nb_hours);
                for i = 1:length(idx)
                    data_.value(:,i) = data.value(idx{i});
                end
            end
            
            if ~isempty(cols)
                data_.colnames = cols;
            elseif explicit_colnames
                data_.colnames = reshape(cellstr(datestr(bin_time, colnames_format)),1,[]);
            else
                data_.colnames = tokenize(sprintf('%g;', bin_time));
            end
            data_.date = day;
            if nargout
                if isempty(idx)
                    idx = accumarray([idx_day,bin_num],(1:length(data.date))');
                    idx = mat2cell(idx,size(idx,1),ones(size(idx,2),1));
                end
                varargout = {bin_time, idx};
            end
        else
            nb_data = length(data.colnames);
            data_ = cell(nb_data, 1);
            data_{1} = data;
            data_{1}.colnames = data.colnames(1);
            data_{1}.title = data.colnames{1};
            data_{1}.value = data_{1}.value(:, 1);
            [data_{1}, h, idx] = bin_separator(data_{1}, varargin{:});
            nb_hours = length(h);
            for i = 2 : nb_data
                data_{i} = data;
                data_{i}.colnames = data_{1}.colnames;
                data_{i}.title = data.colnames{i};
                data_{i}.value = NaN(length(idx{1}), nb_hours);
                data_{i}.date  = data_{1}.date;
                for j = 1 : nb_hours
                    data_{i}.value(:, j) = data.value(idx{j}, i);
                end
            end
        end
    case 'weekly'
        if length(data.colnames) ~= 1
            error('Not yet implemented for several cols');
        end
        data = bin_separator(data);
        data_ = cell(5, 1);
        for i = 1 : 5
            data_{i} = data;
            idxes = weekday(data.date) - 1 == i;
            data_{i}.date = data.date(idxes);
            data_{i}.value = data.value(idxes, :);
        end
    otherwise
        error('Error in bin_separator, mode <%s> unknown', lower(mode));
end
end