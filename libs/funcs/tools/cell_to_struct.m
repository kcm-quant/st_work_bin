function [rslt] = cell_to_struct(request_fields, rslt)

% CELL2STRUCT - prend en argument une liste de nom de colonnes et un cell array de donn�es
% Renvoie un tableau de structures
% 
% Author: edarchimbaud@cheuvreux.com
% Date: 31/10/2008

tmp = arrayfun(@(i){request_fields{i},rslt(:,i)},1:length(request_fields), 'uni', false);
tmp = [tmp{:}];
rslt = struct(tmp{:});

end