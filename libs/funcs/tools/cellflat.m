function c = cellflat(varargin)
% CELLFLAT - flatten any cellarray
%     cellflat({{{{1,2,3},{{{4,4}}},{{5,struct('r', 5)}}}},7,{8,'alpha'}}) 
% DONE: treats varargin as a single cell
% 
% Exemples:
%  cellflat({1,2},{3,4})
%  cellflat({{1,2},{3,4}})
%  cellflat({1,2},{3,{4}})
% 
% DONE: doesn't flat numeric !
% DONE: a single cell-column argument returns a column

% -- Version 3.0
% - (:)' que les cells [ne (:) pas les num�riques]
% - retrouver l'orientation initiale
% Trouve l'orientation depuis la racine seulement

if isequalwithequalnans(varargin{end},@cellflat.OV_)
    c= varargin(1:end-1);
    while any( cellfun(@(x)iscell(x), c))
       c=[c{:}];
    end
    return
end

c= varargin;
isrow= true;
ic= cellfun(@iscell, c);
if all(size(varargin)== 1)
   if any(ic)
       ici= find(ic,1);
       isrow = size(c{ici},1)== 1;
   end
end
while any(ic)
      c(ic)= cellfun(@(x) x(:)',c(ic),'uni',0);         
      c= cat(2,c{:});
      ic= cellfun(@iscell, c);
end
if ~isrow
   c= c'; 
end


% -- version 0.
%    Concat�ne sur le m�me ligne
%    N�cessite meme orientation
% c= varargin;
% while any( cellfun(@(x)iscell(x), c))
%     c=[c{:}];
% end


% -- version 1.
%    Concat�ne sur le m�me ligne
%    N�cessite meme orientation
%    MAIS il n'est pas normal de (:) les num�riques !
% c= varargin;
% while any( cellfun(@(x)iscell(x), c))
% %    c=[c{:}];
%       c= cellfun(@(x) x(:)',c,'uni',0);    
%       c= cat(2,c{:});%
% end
% back compatibility, on s'attends a une ligne !
%c= c';

% % -- Version 2.0
% % - (:)' que les cells [ne (:) pas les num�riques]
% % - retrouver l'orientation initiale
% % Tentative de trouver l'orientation r�cursive
% c= varargin;
% isrow= true;
% % if size(c,1) > 1; isrow= false;end
% 
% if size(c{ic(1)},1) > 1
% 
% ic= find(cellfun(@iscell, c));
% if any(ic); 
%     if size(c{ic(1)},1) > 1; isrow= false;end
% end
%     
% while any(ic)
% %    c=[c{:}];
%       c(ic)= cellfun(@(x) x(:)',c(ic),'uni',0);         
%       c= cat(2,c{:});%
%       % trop tard pour check size ici !
%       %if ~iscol & size(c,1) > 1; iscol= true;end
%       ic= cellfun(@iscell, c);
%       ic= find(cellfun(@iscell, c));
%         if isrow & any(ic); 
%             if size(c{ic(1)},1) > 1; isrow= false;end
%         end
% end
% if ~isrow
%    c= c'; 
% end

function DEMO()
cellflat({1,2},{3,4})
cellflat({{1,2},{3,4};{5,6},{7,8}})

cellflat({[1.1 1.2],{2.1;2.2},[3.1; 3.2]})

cellflat({[1 1],[2;2]},{3,4})

% renvoie des lignes par d�faut
cellflat({1,2},{3,4})
cellflat({{1,2},{3,4}})
cellflat({1;2},{3;4})

% sauf si nargin==1 et que le 1er sous-cell est une colonne
cellflat({{1;2};{3;4}})
cellflat({{1;2};{5,6}})
cellflat({{1,2};{5,6}})

cellflat({{{1;2};{3;4}}},@cellflat.OV_)
cellflat({{{1;2};{3;4}}})

cellflat({{1;2},{3;4}})

cellflat({1;2},{3,4})
cellflat({1,2},{3;4})

cellflat({{1;2};{3;4}})

cellflat({1,2}',{3,4}')

cellflat({{1,2},{3,4}})
cellflat({1;2},{3;{4,5}})



