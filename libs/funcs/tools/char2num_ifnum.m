function n = char2num_ifnum( n)
% CHAR2NUM_IFNUM
%
% char2num_ifnum( 3)
% char2num_ifnum( '3')

if ischar(n)
    n = str2double(n);
end

