function cond_boxplot(Y, X, h, use_x_value)
% cond_boxplot(Y, X)
% 
% X = randn(100, 1);
% Y = X + randn(100, 1);
%
% plot(X, Y, '+', 'Color', [0 150 97]/256)
% cond_boxplot(Y, X)
%  
% cond_boxplot(Y, X, h, use_x_value)
% third argument h is an axis 
% fourth argument allows you to choose wether to use x values in the xaxis
%
% see also cond2d 
cmk = cm_kepler(3);

idx2del = any(~isfinite([Y X]), 2);
Y(idx2del) = [];
X(idx2del) = [];

idx_q = quantile_bin(X, 10);

if nargin < 3 || isempty(h)
    figure;
    h = gca;
end

if nargin < 4 || use_x_value
    xt = accumarray(idx_q, X, [], @mean, NaN);
    xt = xt(~isnan(xt));
    boxplot_cheuvreux(h, Y, idx_q, 'cheuvreux_cg', true, ...
        'positions', xt, 'widths', min(diff(xt))/2, ...
        'labels', tokenize(sprintf('%2.2f;', xt)),'rotate_labels', false);
else
    xt = 1:10;
    boxplot(h, Y, idx_q, 'cheuvreux_cg', true);
end

hold on
avg = accumarray(idx_q, Y, [], @mean,NaN);
avg = avg(~isnan(avg));
plot( xt, avg, 'Marker', 'o', 'MarkerFaceColor', ...
    cmk(1,:), 'MarkerEdgeColor', cmk(1,:), 'LineWidth', 2, 'Color', 'k');
hold off

end