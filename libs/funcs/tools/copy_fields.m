function data = copy_fields(data1, data2)
% COPY_FILEDS - prend comme arguments deux structures :
% renvoie le premier argument, et recopie l'ensemble des 
% champs du deuxieme qui n'entrent pas en collision avec ceux du premier 
% argument, et cela � n'importe quel niveau de sous-structure
% Lorsque 'lun des deux champs est un struct array on renvoie le premier
% argument

    if (isempty(data1))
        data = data2;
        return
    elseif (isempty(data2)) || length(data1) > 1 || length(data1) > 1
        data = data1;
        return;
    end
    if (~isstruct(data2) || ~isstruct(data1))
        if (~isempty(data1))
            data = data1;
        else
            data = data2;
        end
    else
        fs1 = fieldnames(data1);
        fs2 = fieldnames(data2);
        fs_common = intersect(fs1, fs2);
        fs1_not_fs2 = setdiff(fs1, fs_common);
        for i = 1 : length(fs1_not_fs2)
            if ~isempty(fs1_not_fs2(i))
                data.(cell2mat(fs1_not_fs2(i))) = data1.(cell2mat(fs1_not_fs2(i)));
            end
        end
        fs2_not_fs1 = setdiff(fs2, fs_common);
        for i  = 1 : length(fs2_not_fs1)
            if ~isempty(fs2_not_fs1(i))
                data.(cell2mat(fs2_not_fs1(i))) = data2.(cell2mat(fs2_not_fs1(i)));
            end
        end
        for i = 1 : length(fs_common)
            data.(cell2mat(fs_common(i))) = copy_fields(data1.(cell2mat(fs_common(i))), data2.(cell2mat(fs_common(i))));
        end
    end
end