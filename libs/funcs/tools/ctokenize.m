function varargout = ctokenize( lst, n, sep)
% CTOKENIZE - tokenize strings inside a cell
%
% c = {'alpha:1', 'beta:2', 'garzol:3', 'u:4'}
% ctokenize( c, 1, ':')
% [a,b]=ctokenize({'alpha:1', 'beta:2', 'garzol:3', 'u:4'},[],':')
%
% See also tokenize 

if nargin < 3
    sep = ';';
end
if nargin < 2 
    n = [];
end

lst = regexp(lst, ['([^' sep ']+)'], 'tokens');
if isempty( n)
    varargout = {};
    mx = max( cellfun(@(c)length(c), lst));
    for i=1:mx
        varargout{end+1} = cellfun(@(c)c{i}{:}, lst, 'uni', false);
    end
else
    varargout = {cellfun(@(c)c{n}{:}, lst, 'uni', false)};
end