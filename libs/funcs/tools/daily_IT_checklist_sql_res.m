function [d,val,avg_val]=daily_IT_checklist_sql_res(base,table,dt_field,dt_all,perim_str,test_type)

%% daily count
if isempty(perim_str)
    res = exec_sql('KGR',...
        sprintf('select %s,count(*) from %s..%s where %s %s group by %s order by %s ',...
        dt_field,base,table,dt_field,dt_all.ref,dt_field,dt_field));
else
end

d.date = cellfun(@(v) datenum(v,'yyyy-mm-dd'), res(:,1));
d.value = cell2mat(res(:,2));

%% check date
ix = d.date==dt_all.date_num;
ref_ix = d.date<dt_all.date_num;

if isempty(ix)
    fprintf('WARNING // %s / No lines for date %s', test_type, dt_all.date)
else
    ttl = sprintf('%s - Nb lines in %s',test_type,regexprep(table,'_',' '));
    get_focus(ttl)
    mima = [min(d.date) max(d.date)];
    plot(d.date, d.value)
    hold on
    plot(get(gca,'xlim'),val*[1 1],'--r')
    hold off
    axaxis(mima)
    attach_date('init','date',mima,'format','dd/mm/yyyy')
    set(gca,'ygrid','on')
    title(ttl)
    
    val = d.value(ix,1);
    avg_val = nanmean(d.value(ref_ix,1));
    fprintf('%s / %d lines on date %s versus %.0f on average.\n', test_type, val, dt_all.date, avg_val)
end

end
