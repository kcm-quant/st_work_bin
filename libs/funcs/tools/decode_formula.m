function decoded_frml = decode_formula(formula)
% formula = '2*{close_prc}({is_prim})*sum({open_prc})';

s = regexp(formula, '\}\(', 'end', 'once');
if ~isempty(s)
    e = find(cumsum((formula(s:end)=='(')-(formula(s:end)==')'))==0, 1)+s-1;
    i = find(formula(1:s-1)=='{', 1, 'last');
    decoded_frml = [decode_formula(formula(1:i-1)),...
        'subsref(', decode_formula(formula(i:s-1)),...
        ',struct(''type'', ''()'', ''subs'', {{logical(', decode_formula(formula(s+1:e-1)), ')}}))',...
        decode_formula(formula(e+1:end))];
else
    decoded_frml = strrep(strrep(formula, '*', '.*'), '/', './');
    decoded_frml = regexprep(decoded_frml, '\{([^\{\}]+)\}', 'val(i, #$1@)');
end

