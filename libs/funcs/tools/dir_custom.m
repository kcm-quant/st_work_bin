function res = dir_custom(path)
% dir_custom('X:\kc_repository\get_basic_indicator_v2\smallest_step\toto\')

if ispc
    [status,res] = system(['dir "',path, '"']);
else
    [status,res] = system(['ls "',path, '"']);
end
assert(~status,'dir_custom:system', '%s',res);


res = regexp(res,'[0-9\_]*\.mat','match');