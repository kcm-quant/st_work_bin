function id_dir = dir_mod_key(str,max_dir)
% id_dir = dir_mod_key('2jg000M',50000)
% id_dir = dir_mod_key('2jg000M',50000)
% id_dir = dir_mod_key('2jg000m',50000)
% id_dir = dir_mod_key('2Jg000m',50000)
REL_BIAS_TOL = 1e-3;
GROUPED_NBHEX = 12;
if 16^GROUPED_NBHEX > 2^48
    error('dir_mod_key:exec', 'Please decrease GROUPED_NBHEX it is too high to work with matlab''s floating accuracy');
elseif 16^GROUPED_NBHEX*REL_BIAS_TOL < max_dir^2
    error('dir_mod_key:exec', 'Nothing guaranted with such a number of max key' );
end
str = hash(str, 'SHA-256');
id_dir = 0;
for i = 1 : GROUPED_NBHEX : (length(str)-11)
 	id_dir = mod(id_dir+mod(hex2dec(str(i:i+11)), max_dir), max_dir);
end
if 16^(length(str)-(i+GROUPED_NBHEX)+1)*REL_BIAS_TOL > max_dir^2
    id_dir = mod(id_dir+mod(hex2dec(str(i:i+11)), max_dir), max_dir);
end
end