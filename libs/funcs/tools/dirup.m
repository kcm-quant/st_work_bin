function d = dirup(d, n)
% DIRUP - climbing up the directory tree
%
%
%
% Examples:
%
% cd
% dirup(cd, 1)
% dirup([cd filesep], 2)
% assert(all(dirup(dirup(cd)) == dirup(cd, 2)))
% % dirup(cd, 40) % intended error
%
% See also: fullfile, filesep, fileparts
%    
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   date     :  '17/04/2012'
%   
%   last_checkin_info : $Header: dirup.m: Revision: 2: Author: robur: Date: 03/04/2013 11:37:53 AM$

if nargin < 2
    n = 1;
end

fs = filesep();
if length(d) > 2
    double_sep = strfind(d(2:end), [fs fs]);
    while ~isempty(double_sep)
        d(2+double_sep) = [];
        double_sep = strfind(d(2:end), [fs fs]);
    end
end
idxes = strfind(d, fs);
if n > (length(idxes)-(idxes(1)==1)) % (idxes(1)==1) for unix path begining by the separator
    error('dirup:check_args', 'Not enough levels to climb up');
end
d = d(1:idxes(end-n+1-(idxes(end) == length(d))));
end