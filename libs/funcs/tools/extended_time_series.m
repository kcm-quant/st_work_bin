function ets = extended_time_series(series, time, extended_time)
% Inputs:
% series, time - time series. We suppose time already sorted out.
% extended_time - a broader time scale. INCLUDES time.
% Output
% ets: intercalation of NaN between series to match the size of extended_time
% Premises:
% length(series) == length(time)
% time and extended_time: sorted, increasing time values.
% extended_time contains all elements from time



% - preliminary testing
if(~ (size(series,1) == size(time, 1)))
    error('extended_time_series:Input lengths do not match: series, time');
end
if ~all(ismember(time,extended_time))
    error('extended_time_series: extended_time do not contend time');
end
if ~issorted(time) || ~issorted(extended_time)
    error('extended_time_series: extended_time or time is not sorted');
end
if length(time)~=length(unique(time)) || length(extended_time)~=length(unique(extended_time))
    error('extended_time_series: extended_time or time hes a similar date');
end

% - ets computing
ets = zeros(size(extended_time, 1), size(series, 2));
time_index = 1;
for i = 1:length(extended_time)
    if(time_index > length(time) || extended_time(i) < time(time_index))
        ets(i, :) = nan(1, size(series, 2));
    else
        ets(i, :) = series(time_index, :);
        time_index = time_index + 1;
    end    
end




end