function vals = fill_nan_v(vals, varargin)
% FILL_NAN - fonction qui remplace les nans et les infs d'un vecteur par 
% interpolation lin�aire et une extrapolation constante
%
% example :
%   fill_nan_v([1 2 3 NaN 5 6])
%   fill_nan_v([1 2 NaN NaN 5 6])
%   fill_nan_v([1 2 NaN NaN NaN 6])
%   fill_nan_v([NaN 2 NaN NaN NaN 6])
%   fill_nan_v([NaN NaN NaN NaN NaN 6])
%   fill_nan_v([1 NaN NaN 4 NaN 6])

idx_nans = ~isfinite(vals);
if ~any(idx_nans)
    return
elseif all(idx_nans)
    error('fill_nan_v:check_args', 'Unable to replace NaN values as there is no finite values');
elseif ~(size(vals,1)==1 || size(vals,2)==1)
    error('fill_nan_v:check_args', 'Unable to work on a matrix');
end

nb_vals = numel(vals);

%< extrapolations avec des constantes
% au d�but du vecteur
idx_first_finite = find(~idx_nans, 1);
if idx_first_finite ~= 1
    vals(1:idx_first_finite-1) = vals(idx_first_finite);
end
% � la fin du vecteur
idx_last_finite = find(~idx_nans, 1, 'last');
if idx_last_finite ~= nb_vals
    vals(idx_last_finite+1:nb_vals) = vals(idx_last_finite);
end
%>

for i = idx_first_finite+1 : idx_last_finite-1
    if idx_nans(i)
        offset_next_finite = find(~idx_nans(i+1:nb_vals), 1, 'first');
        vals(i) = (vals(i+offset_next_finite) + vals(i-1)*offset_next_finite) / (offset_next_finite+1);
    end
end