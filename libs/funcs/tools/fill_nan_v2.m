function [vals,nanidx] = fill_nan_v2(vals, varargin)
% FILL_NAN_v2 - fonction qui remplace les nans et les infs d'un vecteur par
% interpolation lin�aire et une extrapolation constante
%
% example :
%   fill_nan_v2([1 2 3 NaN 5 6])
%   fill_nan_v2([1 2 3 NaN 5 6],'mode','after')
%   fill_nan_v2([1 2 3 NaN 5 6],'mode','before')
%   fill_nan_v2([1 2 NaN NaN 5 6])
%   fill_nan_v2([1 2 NaN NaN 5 6],'mode_time',[0.1 0.2 0.25 0.4 0.5 0.6])
%   fill_nan_v2([1 2 NaN NaN NaN 5 6],'mode_time',[0.1 0.2 0.2 0.4 0.45 0.5 0.6])
%   fill_nan_v2([NaN 1 2 NaN NaN NaN 5 6],'mode_time',[0.05 0.1 0.2 0.2 0.4 0.45 0.5 0.6],'mode_start',0)
%   fill_nan_v2([1 2 NaN NaN 5 6],'mode','after')
%   fill_nan_v2([1 2 NaN NaN 5 6],'mode','before')
%   fill_nan_v2([NaN NaN NaN  2 NaN 6 NaN])
%   fill_nan_v2([NaN NaN NaN  2 NaN 6 NaN],'mode','before')
%   fill_nan_v2([NaN NaN NaN  2 NaN 6 NaN],'mode','after')
%   fill_nan_v2([NaN NaN NaN NaN NaN])

nanidx= isnan(vals);

if min(size(vals))> 1
    vals = matrixFun(vals,@(x) fill_nan_v2(x,varargin{:}),'c');
    return
end


% - mode : before - after - interp
% - mode_time : time vector or not
% - mode_start : what to print before firt NaN (a value)
opt  = options( {'mode','interp','mode_time',[],'mode_start',[]}, varargin);

% - transform input mode_...
mode = opt.get('mode');
mode_time = opt.get('mode_time');
mode_start = opt.get('mode_start');

if isempty(mode_time)
    mode_time_bool=false;
else
    mode_time_bool=true;
    
    if size(mode_time,1)~=size(vals,1) || size(mode_time,2)~=size(vals,2)
        error('fill_nan_v2:check_args', 'Size of vals and time vals does not match')
    elseif ~issorted(mode_time)
        error('fill_nan_v2:check_args', 'time vals is not sorted')
    else
        time_vals=mode_time;
    end
end

if isempty(mode_start)
    mode_start_bool=false;
else
    mode_start_bool=true;
    if length(mode_start)>1
        error('fill_nan_v2:check_args', 'mode_start has to be a number !!')
    else
        start_val=mode_start;
    end
end



idx_nans = ~isfinite(vals);
if ~any(idx_nans) || (~mode_start_bool && all(idx_nans)) 
    return
elseif (mode_start_bool && all(idx_nans)) 
    vals=start_val*ones(size(vals));
elseif ~(size(vals,1)==1 || size(vals,2)==1)
    error('fill_nan_v2:check_args', 'Unable to work on a matrix');
end

nb_vals = numel(vals);


switch mode
    
    case 'interp'
        
        %< extrapolations avec des constantes
        % au d�but du vecteur
        idx_first_finite = find(~idx_nans, 1);
        if idx_first_finite ~= 1
            if mode_start_bool
                vals(1:idx_first_finite-1) = start_val;
            else
                vals(1:idx_first_finite-1) = vals(idx_first_finite);
            end
        end
        % � la fin du vecteur
        idx_last_finite = find(~idx_nans, 1, 'last');
        if idx_last_finite ~= nb_vals
            vals(idx_last_finite+1:nb_vals) = vals(idx_last_finite);
        end
        %>
        
        if mode_time_bool
            for i = idx_first_finite+1 : idx_last_finite-1
                if idx_nans(i)
                    offset_next_finite = find(~idx_nans(i+1:nb_vals), 1, 'first');
                    offset_time_before=time_vals(i)-time_vals(i-1);
                    offset_time_after=time_vals(i+offset_next_finite)-time_vals(i);
                    if offset_time_before+offset_time_after==0
                        vals(i)=vals(i-1);
                    else
                        vals(i) = (vals(i+offset_next_finite)*offset_time_before + vals(i-1)*offset_time_after) / (offset_time_before+offset_time_after);
                    end
                end
            end
        else
            
            for i = idx_first_finite+1 : idx_last_finite-1
                if idx_nans(i)
                    offset_next_finite = find(~idx_nans(i+1:nb_vals), 1, 'first');
                    vals(i) = (vals(i+offset_next_finite) + vals(i-1)*offset_next_finite) / (offset_next_finite+1);
                end
            end
            
        end
        
        
        
    case 'before'
        
        idx_first_finite = find(~idx_nans, 1);
        if idx_first_finite ~= 1
            if mode_start_bool
                vals(1:idx_first_finite-1) = start_val;
            end
        end
        % � la fin du vecteur
        idx_last_finite = find(~idx_nans, 1, 'last');
        if idx_last_finite ~= nb_vals
            vals(idx_last_finite+1:nb_vals) = vals(idx_last_finite);
        end
        %>
        for i = idx_first_finite+1 : idx_last_finite-1
            if idx_nans(i)
                vals(i) = vals(i-1);
            end
        end
        
        
        
    case 'after'
        
        idx_first_finite = find(~idx_nans, 1);
        if idx_first_finite ~= 1
            if mode_start_bool
                vals(1:idx_first_finite-1) = start_val;
            else
                vals(1:idx_first_finite-1) = vals(idx_first_finite);
            end
        end
        % � la fin du vecteur
        idx_last_finite = find(~idx_nans, 1, 'last');
        if idx_last_finite ~= nb_vals
            vals(idx_last_finite+1:nb_vals) = vals(idx_last_finite);
        end
        %>
        for i = idx_first_finite+1 : idx_last_finite-1
            if idx_nans(i)
                offset_next_finite = find(~idx_nans(i+1:nb_vals), 1, 'first');
                vals(i) = vals(i+offset_next_finite);
            end
        end
        
        
    otherwise
        error('fill_nan_v2:check_args', 'mode not handle');
end





end












