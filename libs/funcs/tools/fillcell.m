function c= fillcell(c,idx,v)
% filled_cell = fillcell(cell,idx,single_value)

cc= repmat({v},size(c(idx)));
c(idx)= cc;

function TEST

c={1,2;3,4};
fillcell(c,[1,3],[])

c={1,2;3,4};
fillcell(c,cellfun(@(x)x>2,c),[])