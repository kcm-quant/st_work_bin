function rslt = get_security_name(sec_id)
%
% @deprecated
% 
% gets the firm name from the security_id
%
% get_repository('sec_id2name', [110; 276]);
%
% See also get_repository
    rslt = get_repository('sec_id2name', sec_id);
    if length(rslt) == 1
        rslt = cell2mat(rslt);
    end
end