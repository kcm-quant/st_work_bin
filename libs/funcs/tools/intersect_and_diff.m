function [set_inter, set1minset2, set2minset1, idx1, idx2] = intersect_and_diff(set1, set2)
% INTERSECT_AND_DIFF - intersection and both differences of two set (cell or numeric)
% first output  = intersection
% second output = setdiff(set1, set2)
% third output  = setdiff(set2, set1)
%
% Examples:
% set1 = [-1 1:10];
% set2 = 1:0.5:10;
% [set_inter, set1minset2, set2minset1, idx_set1notset2, idx_set2notset1] = intersect_and_diff(set1, set2)
% 
% assert(all(set_inter == intersect(set1, set2)));
% assert(all(setdiff(set1, set2) == set1minset2));
% assert(all(setdiff(set1, set2) == set1(idx_set1notset2)));
% assert(all(setdiff(set2, set1) == set2minset1));
% assert(all(setdiff(set2, set1) == set2(idx_set2notset1)));
% 
% See also : intersect setdiff
% 
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : 'athabault@cheuvreux.com'
%   version  : '1'
%   date     :  '24/01/2011'

[set_inter, i1, i2] = intersect(set1, set2);
idx1 = true(size(set1));
idx1(i1) = false;
set1minset2 = set1(idx1);
idx2 = true(size(set2));
idx2(i2) = false;
set2minset1 = set2(idx2);

end