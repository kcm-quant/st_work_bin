function x = isnull(a,b)
% ISNULL - isnull is sql function isnull: isnull(a,b) outputs a if it is finite, else b.
    x = a;
    idx = ~isfinite(x);
    if any(idx)
        x(idx) = b;
    end
end