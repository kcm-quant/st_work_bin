function Y = joint(data1,X,data2)
% JOINT - Maps each row of "data1" to each row of "X"
% "data2" is a matrix with the same column number as data1.
% "Y" is the result of the map application to rows of "data2".
% For rows of "data2" which are not in "data1", "Y" contains NaNs (in case
% "X" is numeric) or empty cells (in case "X" is a cellarray).
%
% Example:
% joint({'un';'deux';'trois';'quatre'},{1,'one';2,'two';3,'three';4,'four'},...
%     {'quatre';'un';'trois';'trois';'deux';'deux';'quatre';'quatre';'deux';'un';'cinq'})

if ~iscell(data1)
    [data2,~,idx] = unique(data2,'rows');
    
    [~,a,b] = intersect(data1,data2,'rows');
    if isdatetime(X)
        Y = NaT(length(data2),size(X,2));
    elseif ~iscell(X)
        Y = nan(length(data2),size(X,2));
    else
        Y = cell(length(data2),size(X,2));
    end
    Y(b,:) = X(a,:);
    
    Y = Y(idx,:);
else
    cell_idx = cell_to_num([data1;data2]);
    Y = joint(cell_idx(1:size(data1,1),:),X,cell_idx(1+size(data1,1):end,:));
end

end

function varargout = cell_to_num(data)
idx_char = cellfun(@ischar,data(1,:));
not_idx_char = find(~idx_char);
data_char = mat2cell(data(:,idx_char),size(data(:,idx_char),1),ones(size(data(:,idx_char),2),1));
data_to_int = cell2mat(cellfun(@uniq,data_char,'uni',false));
data_to_cell = nan(size(data));
if ~isempty(not_idx_char)
    data_to_cell(:,not_idx_char) = cell2mat(data(:,not_idx_char));
end
if any(idx_char)
    data_to_cell(:,idx_char)  = data_to_int;
end
varargout{1} = data_to_cell;

    function varargout = uniq(data)
        [~,~,idx]      = unique(data);
        varargout{1}   = idx;
    end
end
