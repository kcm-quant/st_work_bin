function data = mysql_data2st_data(a)
global st_version
jtool = handle(java_tools4matlab.Mysql_data_reader);
if st_version.my_env.endian=='L'
    a = [0;1;73;77;0;0;0;0;jtool.uncompress(a)];
else
    a = [0;1;77;73;0;0;0;0;jtool.uncompress(a)];
end
a = typecast(a,'uint8');
data = getArrayFromByteStream(a);