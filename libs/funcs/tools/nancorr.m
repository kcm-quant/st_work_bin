function [V,S,nbo]= nancorr(x,varargin)
% [CorrMatrix,stds]= NANCORR(X) - or NANCORR(X,Y)
% Computes correlation matrix, ignoring nans
% TODO: compute nanfrequency, and confidence
% FIXME: this is not a correlation matrix (non nan indices differ on <x,y>
% and <x,x> ! use closest_cov to project on positive definite matrices
% See also closest_cov


%TODO: plusieurs modes de robustesses
rob= false; 
is= find(cellfun(@(x) isequalwithequalnans(x,@robust),varargin));
if ~isempty(is)
    varargin(is)=[];
    rob= true;
end

is= cellfun(@ischar,varargin);
mode= 'pairwise';
if any(is)
    is= find1(is); 
    mode= varargin{is};
    varargin(is)=[];    
end

one_row= 0;
if length(varargin)== 1
    one_row=size(x,2);
    x= [x,varargin{1}];
elseif length(varargin)> 1
    x=[x,varargin{:}];
end

if rob
    io= moutlier1(x);
    x(io,:)= [];
end

% S= nanstd(x);
V= nancov(x,mode);
S= sqrt(diag(V)); %norm(S-nanstd(x)') c'est pareil !
% V= diag(1./S)*V*diag(1./S); % ceci démultiplie les nans dans V !!!
V= bsxfun(@times,V,1./S);
V= bsxfun(@times,V,1./S');

nbo= 0+~isnan(x);
nbo= nbo'*nbo;

if one_row> 0
    V= V(1:one_row,one_row+1:end);
    nbo= nbo(1:one_row,one_row+1:end);
end



function UnitTest()
x= randn(100,3);
[c,s,nbo]= nancorr(x);
cc= corr(x);
assert(norm(c-cc)< 10^(-10));

x= 1:100;y= x;
y(2:10)=nan; x(50:60)= nan;
[c,s,nbo]= nancorr(x',y'); %98% seulement, les std sont différentes !
assert(norm(c-1)< 10^(-10))

x= randn(1000,5);
x(1+floor(4999*rand(1000,1)))=nan;
[c,s,nbo]=nancorr(x);
cp= nancorr(x(:,1:2),x(:,3:end));
assert( norm(c(1:2,3:end)-cp) < 10^(-10) )



