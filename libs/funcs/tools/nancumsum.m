function y = nancumsum(x,dim)
%NANCUMSUM cumSum, ignoring NaNs.

% Find NaNs and set them to zero.  Then cumsum up non-NaNs.  Cols of all NaNs
% will return zero.
x(isnan(x)) = 0;
if nargin == 1 % let sum figure out which dimension to work along
    y = cumsum(x);
else           % work along the explicitly given dimension
    y = cumsum(x,dim);
end