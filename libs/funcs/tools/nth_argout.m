function g = nth_argout( f, n)
% NTH_ARGOUT - returns a function handler of a function returning the nth ouput
% nth_argout( f, n) : 
%   - f : a function handle or string naming the function
%   - n : an integer telling which output you want the function to return             
% 
% Examples:
%
% mindx = nth_argout( @min, 2)
% mindx( [.7 .8 .9 .1 .2 .5 .6])
%
% mindx = nth_argout( @sort, 2)
% mindx( [.7 .8 .9 .1 .2 .5 .6], 'descend')
% mindx( [.7 .8 .9 .1 .2 .5 .6], 'ascend')
% 
% mindx = nth_argout( 'sort', 2)
% mindx( [.7 .8 .9 .1 .2 .5 .6], 'descend')
% mindx( [.7 .8 .9 .1 .2 .5 .6], 'ascend')
% 
% 
%
% See also : nth_out, varargout, varargin
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : 'mayo.nath@gmail.com'
%   date     :  '24/10/2011'
%   
%   last_checkin_info : $Header: nth_argout.m: Revision: 3: Author: robur: Date: 10/24/2011 02:07:53 PM$

if ischar(f)
    f = str2func(f);
end
g = @(varargin)nth_output(f, n, varargin{:});

end