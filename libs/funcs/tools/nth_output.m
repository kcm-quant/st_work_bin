function out = nth_output(f, n, varargin)
% NTH_OUTH - returns the n th output of the function f
%
% Before using this function please think twice. Cases when this function 
% are very uncommon : prefer the syntax [~, ~, third_arg] = f(my_inputs)
% as this will be faster and clearer
%
% nth_argout(f, n, ...) : 
%   - f : a function handle, NOT a string naming the function
%   - n : an integer telling which output you want the function to return             
%   - ... : the inputs with which f should be called
%
% Examples:
%
% mindx = nth_output( @min, 2, [.7 .8 .9 .1 .2 .5 .6])
%
% sortdx = nth_output( @sort, 2, [.7 .8 .9 .1 .2 .5 .6], 'descend')
% 
% nan_value = nth_output( @nan, 1) % yes this is a dumb example
% 
% See also : nth_argout, varargout, varargin
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : 'mayo.nath@gmail.com'
%   date     :  '24/10/2011'
%   
%   last_checkin_info : $Header: nth_output.m: Revision: 2: Author: robur: Date: 10/24/2011 02:07:10 PM$

outputs = cell(1, n);
[outputs{:}] = f(varargin{:});
out = outputs{n};

end

