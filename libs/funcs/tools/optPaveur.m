function self= optPaveur(varargin)
% optPaveur : pavage fini pour des paramètres d'options
% Use
%   p= optPaveur('oui',{1,2,'oui'},'non',{[],3});
%   % Each value is a cellarray for all possible values
%   o= p.get(1);
%   % All possible options combinations is linearly indexed
%   o.get()
%
% Use
%   p= optPaveur(optPaveur1,optPaveur2,...);
%   % linear concatenation of each paveur
%

x= varargin{1};
if isstruct(x) & isfield(x,'class__') & strcmpi(x.class__,'optPaveur.m')
    % surchargé par optPaveurListe
    self= optPaveurListe(varargin{:});
    return;
end
    
self.Z= varargin;
self.listes= options(varargin);
self.dims=[];
for n=1:2:length(varargin)
    self.dims(end+1)= length(varargin{n+1});
end
self.dimdim= length(self.dims);
self.lindim= prod(self.dims);
self.get= @get;
self.getdim= @getdim;
self.class__ = 'optPaveur.m';

function o= get(n)
    assert(floor(n)==n);
    if n> self.lindim |  n<=0;
        str= ['optPaveur() accessed at ' num2str(n) ' but (lin)dim=' num2str(self.lindim) ' <- [' num2str(self.dims) ']' ];
        error(str);
    end
    v={};for k=1:self.dimdim; v{k}= nan;end
    [v{:}]= ind2sub(self.dims,n); 
    v= [v{:}];
    o= options();
    for i=1:2:length(self.Z)        
        L= self.Z{i+1};            
        o.set(self.Z{i}, L{v(floor(i/2)+1)} );
    end
end

    function d= getdim()
        d= self.lindim;
    end

end % END optPaveur

%% Paveur Liste
function self= optPaveurListe(varargin)
    
self.Z= varargin;
self.dimdim= length(varargin);
self.dims= []; for k=1:self.dimdim; self.dims(k)= varargin{k}.lindim;end
self.lindim = sum(self.dims);

self.get= @getListe;
self.class__ = 'optPaveur.m/optPaveurListe';    
self.getdim= @getdim2;

function d= getdim2()
    d= self.lindim;
end

function o= getListe(n)
    assert(floor(n)==n);
    if n> self.lindim |  n<=0;
        str= ['optPaveurListe() accessed at ' num2str(n) ' but (lin)dim=' num2str(self.lindim) ' <- [' num2str(self.dims) ']' ];
        error(str);
    end
    cs= [0,cumsum(self.dims)];
    idx= find(n> cs);idx = idx(end);
    o= self.Z{idx}.get(n- cs(idx));
end    
    
    
end



%% 
function test()
% paveur simple
p= optPaveur('oui',{1,2,'oui'},'non',{[],3});
o= p.get(1);o.get()

for k= 1:p.getdim()
    o= p.get(k);o.get()
end
% concaténation de paveur
p0= optPaveur('oui',{1,2,'oui'},'non',{[],3});
p1= optPaveur('a',{1,2,'oui'},'b',{[],3});
p= optPaveur(p0,p1)
o= p.get(1);o.get()
o= p.get(6);o.get()
o= p.get(7);o.get()
o= p.get(12);o.get()
o= p.get(13);o.get()

% concaténation de paveur 2
p0= optPaveur('mode',{'oui','o'},'param',{1,2,3});
p1= optPaveur('mode',{'non'},'param',{5,6,7},'additmode',{[],1});
p= optPaveur(p0,p1,p0)
o= p.get(1);o.get()
o= p.get(6);o.get()
o= p.get(9);o.get()


% for k= 1:p.getdim()
%     o= p.get(k);o.get()'
% end
for k= 1:p.lindim
    o= p.get(k);o.get()'
end

end


