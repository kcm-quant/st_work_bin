function self= optPaveur2(varargin)
% optPaveur : v2
% on liste toutes les possibilitÚs, pour pouvoir les filtrer
%
% TODO: mettre methode getdim dans OptPaveur initial pour compatibilitÚ


self=[];
self.paveur= OptPaveur(varargin{:});
self.lindim= self.paveur.lindim;
x= 1:self.lindim;
self.content= arrayfun(@(i) self.paveur.get(i),x);

self.get= @get_;
self.destroy= @destroy;
self.getdim= @getDim;


    function d= getDim()
       d= self.lindim;
    end            
    function o= get_(i)                
        o= self.content(i);                
    end
    function destroy(i)
        self.content(i)=[];    
        self.lindim= self.lindim-1;
    end
end
%% 
function test()
% paveur simple
p= optPaveur2('oui',{1,2,'oui'},'non',{[],3});
o= p.get(1);o.get()

% destruction
p.getdim()
o= p.get(1);o.get()
o= p.get(6);o.get()
p.destroy(1)
o= p.get(1);o.get()
o= p.get(5);o.get()
p.getdim()

% concatÚnation de paveur
p0= optPaveur('oui',{1,2,'oui'},'non',{[],3});
p1= optPaveur('a',{1,2,'oui'},'b',{[],3});
p= optPaveur(p0,p1)
o= p.get(1);o.get()
o= p.get(6);o.get()
o= p.get(7);o.get()
o= p.get(12);o.get()
o= p.get(13);o.get()
end


