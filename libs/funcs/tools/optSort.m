function classes= optSort(opts, varargin)
% sort various options structure, according to the values of entries
%
%
% Takes cell array of options and keywords: -> index of sort (cell array of groups index)
% Recursive : resplit chaque cluster par le keyword suivant
% 

P= length(varargin);
N= length(opts);
if iscell(opts)
    if iscell(opts{1}) % { {key,val,...}, {key,val,...}, ...}
        opts2= {};
        for n= 1:N
            opts2{end+1}= options(opts{n});
        end
        opts= opts2;
    end
    if isstruct(opts{1}) % {opt, opt, ...}
        opts= [opts{:}]; % on repasse ici apres le premier if
    end
end

classes= {1:N};
% � chaque etape, on subdivise classes
for p=1:P
    key= varargin{p};
    newclass= {};
    Q= length(classes);
    for q=1:Q
        tclass= sort1(opts, classes{q}, key );        
        newclass= {newclass{:}, tclass{:}};
    end
    classes= newclass;
end

end

function classes= sort1(optlst, idx, key)
% renvoie la liste idx, ordonn�e par valeur uniques de opt.get(key)
% ca suffit pas, il faut renvoyer les classes
o= optlst(idx);
vals= arrayfun(@(x) getcatch(x,key), o ,'UniformOutput',0);
v = unique_(vals); N= length(vals);
classes= {};
for k= 1:length(v) 
    classes{k} =[];
    for n=1:N
        if equals_(vals{n},v{k})
            classes{k}(end+1)= idx(n);
        end
    end
end
assert(length([classes{:}])== N)                 
end


function v= getcatch(o,key)
try; v= o.get(key);
catch;v = '__None__';
end
end

function u= unique_(cf)
o= options();
for n= 1:length(cf)
    o.set(cf{n},[]);
end
c= o.get();
u= c(1:2:end);
end

function b= equals_(x,y)
if ischar(x)
    if ischar(y)
        b= strcmpi(x,y);
    else
        b= 0;
    end
else
    b= (x==y);
end
end

%%
function main()
o= options({'oui',1,'non',1});
o(2)= options({'oui',1,'non',2});
o(3)= options({'oui',1,'non',2});
o(4)= options({'oui',2,'non',1});
o(5)= options({'oui',2,'non',1});
o(6)= options({'oui',2,'non',2});
o(7)= options({'oui',2});

i= optSort(o,'oui','non')
[i{:}]
oo= o([i{:}]);cm= compareModels();
char(arrayfun(@(x) cm.misc.opt2name(x.get()),oo ,'uniformoutput',0))

%
i= optSort({o(1),o(2),o(3)},'oui','non')
[i{:}]
oo= o([i{:}]);cm= compareModels();
char(arrayfun(@(x) cm.misc.opt2name(x.get()),oo ,'uniformoutput',0))

%
i= optSort({{'oui',1,'non',1},{'oui',2,'non',1},{'oui',2,'non',1},{'oui',2,'non',2}},'oui','non')
[i{:}]
oo= o([i{:}]);cm= compareModels();
char(arrayfun(@(x) cm.misc.opt2name(x.get()),oo ,'uniformoutput',0))
end


