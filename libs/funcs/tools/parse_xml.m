function pxml = parse_xml( xml_txt, varargin)
% PARSE_XML - xml parsing
%
% cd(fullfile(getenv('st_work'), 'dev\lehalle\funcs\tools'))
% z = parse_xml( 'file', 'FIXMLNewOrder.XML')
% z = parse_xml( 'file', 'PositionReport_BatchExample.xml')
%
% by Charles-Albert Lehalle, charles@lehalle.net
if strcmpi(xml_txt, 'file')
    xml_txt = fileread( varargin{1});
end
oxml = xml_txt;
cnt = ''; % content
same = @(x)x;
% search for <TAG PROPERTIES>ANYTHING</TAG>
% il me reste le probl�me de
% <A/><A/><A>a</A>
[pxml, b, e] = regexp(xml_txt, '<(?<tag>[^> ]+)(?<properties>[^>]*[^/])>(?<child>.*?)</(??@same($1))>', 'names', 'start', 'end');
% remaining
if ~isempty( b)
    rem = '';
    b = [b, length(xml_txt)];
    e = [0, e];
    for r=1:length(b)
        rem = [rem, xml_txt(e(r)+1:b(r)-1)];
    end
    xml_txt = strtrim(rem);
end
if ~isempty( xml_txt)
    % test for <TAG PROPERTIES/>
    [pxml2, b, e] = regexp(xml_txt, '<(?<tag>[^> ]+)(?<properties>[^>]*)/>', 'names', 'start', 'end');
    if ~isempty( pxml2)
        [pxml2.child] = deal([]);        
        pxml = cat(2, pxml, pxml2);
    end
    if isempty( b)
        rem = xml_txt;
    else
        rem = '';
        b = [b, length(xml_txt)];
        e = [0, e];
        for r=1:length(b)
            rem = [rem, xml_txt(e(r)+1:b(r)-1)];
        end
    end
    xml_txt = rem;
end
if ~isempty( xml_txt)
    cnt = xml_txt;
end
if isempty(pxml)
    % if none: then its contents
    pxml = oxml;
else
    [pxml.content] = deal( xml_txt);
    for c=1:length(pxml)
        pxml(c).properties = regexp(pxml(c).properties, '(?<name>[^( |=)]+)="(?<value>[^"]*)"', 'names');
        if ~isempty( pxml(c).child)
            z = parse_xml( pxml(c).child);
            if ischar(z)
                pxml(c).child = [];
                pxml(c).content = z;
            else
                pxml(c).child = z;
                pxml(c).content = '';
            end
        else
            
        end
    end
end
    