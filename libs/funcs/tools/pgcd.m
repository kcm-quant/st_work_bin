function p = pgcd(X)
% PGCD - Plus grand diviseur commun d'un vecteur
%
% ex = pgcd([24 12 21])
% pgcd([10 17 14])
if length(X) == 1
    p = X(1);
    return;
end
X = sort(X);
p = gcd(X(1), X(2));
for i = 3 : length(X)
    tmp = X(i) / p;
    if floor(tmp) ~= tmp
        p = gcd(p, X(i));
    end
end