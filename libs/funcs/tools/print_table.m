function str = print_table( tbl, colnames, rownames, varargin)
% PRINT_TABLE - pretty print a table of results
%
%  str = print_table( est_tbl, est_names, uer, 'output', 'html')
%  str = print_table( est_tbl, est_names, uer, 'output', 'txt')
%  str = print_table( est_tbl, est_names, uer, 'output', 'wiki')
%  str = print_table( est_tbl, est_names, uer, 'output', 'tex')
%  sprintf('%s\n', str{:})
opt         = options({'output', 'wiki', 'format', '%3.2f%%', 'overall', [true, true] }, varargin);
output_mode = lower(opt.get('output'));
frm         = opt.get('format');

str = {};

if ~iscell(rownames)
    rownames = {rownames};
end
if ~iscell(colnames)
    colnames = {colnames};
end

overall_rc = opt.get('overall');

if overall_rc(1)
    if size(tbl, 1) > 1
        tbl = cat(1, tbl , sum(tbl));
    end
    rownames{end+1} = 'Overall';
end
if overall_rc(2)
    if size(tbl, 2) > 1
        tbl = cat(2, tbl, sum(tbl,2));
    end
    colnames{end+1} = 'Overall';
end

switch output_mode
    case 'html'
        rownames = strrep( strrep(strtrim( rownames), '<', '&lt;'), '>', '&gt;');
        colnames = strrep( strrep(strtrim( colnames), '<', '&lt;'), '>', '&gt;');
        
end

switch output_mode
    case 'html'
        str{end+1} = '<table border=1>';
    case 'wiki'
        str{end+1} = '{| border="1"';
        str{end+1} = '|-';
    case 'mail'
        str{end+1} = '';
    case 'tex'
        tbl_format = repmat('c|', 1, size(tbl,2));
        str{end+1} = [ '\begin{tabular}{|c|' tbl_format '}'];
    otherwise
        str{end+1} = '';
end

% header
switch output_mode
    case 'html'
        a = sprintf('<td><b>%s</b></td>', '', colnames{:});
        str{end+1} = [ '<tr>' a(1:end-5) '</tr>'];
    case 'wiki'
        a = sprintf('%s||', '', colnames{:});
        str{end+1} = [ '!' a(1:end-2)];
        str{end+1} = '|-';
    case 'mail'
        a = [sprintf('%100s', ''), sprintf('%20s', colnames{:})];
        str{end+1} = a(:);
    case 'tex'
        a = sprintf('{\\bf %s}&', '', colnames{:});
        str{end+1} = [ a(1:end-1) '\\\hline'];
    otherwise
        a = sprintf('%12s , ', '', colnames{:});
        str{end+1} = a(1:end-3);
end

for r=1:size(tbl,1)
    % rows
    switch output_mode
        case 'html'
            a = sprintf(['<td>' frm '</td>'], tbl(r,:));
            str{end+1} = [ '<tr><td><b>', rownames{r}, '</b></td>' , a '</tr>'];
        case 'wiki'
            a = sprintf([ frm '||'], tbl(r,:));
            str{end+1} = [ '|', rownames{r}, ' || ' , a(1:end-2)];
            str{end+1} = '|-';
        case 'mail'
            a = sprintf('%20.2f%%', tbl(r,:));
            str{end+1} = [sprintf('%100s', rownames{r})  a(1:end)];
        case 'tex'
            a = sprintf([ frm '&'], tbl(r,:));
            str{end+1} = [ '{\bf ', rownames{r}, '}&' , a(1:end-3) '\\\hline'];
        otherwise
            a = sprintf([ frm ' , '], tbl(r,:));
            str{end+1} = [rownames{r} ' , ' a(1:end-3)];
    end
end    

% footer
switch output_mode
    case 'html'
        str{end+1} = '</table>';
    case 'wiki'
        str{end+1} = '|}';
    case 'mail'
        str{end+1} = '';
    case 'tex'
        str{end+1} = '\end{tabular}';
    otherwise
        str{end+1} = '';
end
