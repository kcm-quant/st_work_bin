function t = py2m_format(d)

%% dataframe
try
st = struct(d.to_dict());
catch er
st = struct(d);
end
inStruct = structfun( @(x)py.list(x.values), st, 'UniformOutput', false);

fields = fieldnames(inStruct);
out = struct();
for f = 1:numel(fields)
    fld = fields{f};
    data_value = cellstr(string(cell(inStruct.(fld))))';
    if strcmp(fld, 'date')
        out.(regexprep(fld,'matlab_','')) = datetime(data_value);
    else
%         out.(regexprep(fld,'matlab_','')) = str2double(data_value);
        out.(fld) = str2double(data_value);
    end
end
t = struct2table(out);

end