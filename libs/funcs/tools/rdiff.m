function r= rdiff(x,nlag,mode)
% ReturnsSerie= RDIFF( PriceSerie, nb Lags, mode= ['geo','log'] )
% Works columnwise
% TODO: les inverses


if nargin<2; nlag=1;end
if nargin<3; mode='geo';end
if ischar(nlag)
   assert(nargin<3)
   mode= nlag; nlag=1;
end

mode_stdata=0;
if isstruct(x)
    mode_stdata=1;
    STDA= x;
    x=x.value;
end

step= 1;
if length(nlag)==2
    step= nlag(2);
    nlag= nlag(1);
end

if strcmpi(mode,'geo')
    r= (x(nlag+1:end,:) - x(1:end-nlag,:)) ./ x(1:end-nlag,:);
else
    assert(strcmpi(mode,'log')) 
    r= log(x(nlag+1:end,:) ./ x(1:end-nlag,:));
end
if step> 1
    r= r(end:-1:1,:); r= r(1:step:end,:); r= r(end:-1:1,:);
end
if mode_stdata
    STDA.value=r;
    STDA.date(1:nlag)=[];
    r= STDA;
end

function CHECK

t= 2;
r= rdiff(df.value(1:2*t,1),[t t]) % 1 seul point !
% il n'y a qu'un point non overlap

function DEMO

r0= 0.01*randn(1000,1);
P= cumprod(1+r0);plot(P)
r= rdiff(P)
plot([0;r],r0,'k.')

function TEST

r0= 0.01*randn(1000,1);
P= cumprod(1+r0);
P=[1;P(:)];
figure;plot(P)
r= rdiff(P,1,'geo');

norm(r-r0)
2 * norm(r-r0) / (sum(abs(r)+abs(r0)))

function TRENDS_geo_vs_log

T=10000;
mu= (5/100)  /255; % par jour => exprimé T en jours
s = (25/100) /sqrt(255);
s = (1/100) /sqrt(255); % s+petit => +facile


r0= mu+ s*randn(T,1);
P= cumprod(1+r0); P=[1;P(:)];
figure;plot(P)

rg= rdiff(P,1,'geo');rl= rdiff(P,1,'log');
[std(rg) std(rl)]*sqrt(255)*100
[ mean(rg), mean(rl), mean(rl) + std(rl)^2/2] *255*100
% => E(dP/P) = E(log(rl)) + V(log(rl))/2

% ** 2 paramétrisation du MBG
%   P= exp(W(m,s))            => E P(T) = exp( (m-s^2/2 )*T ) % log-paramétrisation
%   P= exp(W(m',s')- s^2/2)   => E P(T) = exp( (m')*T ) % moment-paramétrisation
%   with m'= m-s^2/2

(P(end)-P(1)) / P(1)
[P(end),exp(mean(rl)*T), exp(mean(rg)*T), exp(mean(rg)*T - T*std(rg)^2/2),]
[P(end),exp(mu*T), exp( (mu + s^2/2)*T),exp( (mu - s^2/2)*T)]


[P(end),exp(mu*T)]
[P(end),exp(mu*T+ s^2/2*T)]
[P(end),exp(mu*T - s^2/2*T)]

%%
% r ~ N(m,s) =>
% E(prod(1+r))      = exp( mu T)
% E(exp(cumsum(r))) = exp((mu+s^2/2) T)

% le mu des rdts <geo> est bien le mu du MBG
T= 500; MC= 5000; r0= mu+ s*randn(T,MC); P= cumprod(1+r0); P=[ones(1,MC);P];
figure;plot(mean(P,2)),hold on; plot(exp((mu  )*(1:T)),'k')
% plot(exp((mu -s^2/2 )*(1:T)),'k--'); plot(exp((mu +s^2/2 )*(1:T)),'k.-')

P= exp([zeros(1,MC);cumsum(r0)]);
figure;plot(mean(P,2)),hold on; plot(exp((mu +s^2/2 )*(1:T)),'k.-')

k=randi(size(df.value,2)) ;plot([df.value(:,k) / df.value(1,k),rsim_mu(:,k)])

x= 0.1+rand(100,1);
r= rdiff(x,1,'log')
xx= exp(cumsum([0;r]))
norm(x-xx)



