function rp = relpath(from, to)
% relpath - compute relative path froml one dir to another
%
%
%
% Examples:
%
% relpath(cd, fullfile(cd, 'blabla'))
% relpath(cd, fullfile(dirup(cd), 'blabla'))
% relpath('C:\test\truc', 'C:\test\trucbidule')
% relpath('C:\test\truc\machin', 'C:\test\truc\bidule\truc')
% relpath('C:\test\truc\machin', 'C:\test\truc\trucbidule\machin')
% relpath('C:\test\truc\machin', 'C:\test\trucbidule\machin')
%
% See also: dirup, fullfile, filesep, fileparts
%    
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   date     :  '17/04/2012'
%   
%   last_checkin_info : $Header: relpath.m: Revision: 1: Author: robur: Date: 05/08/2012 04:38:34 PM$

fs = filesep;

if from(end) == fs
    from(end) = [];
end
if to(end) == fs
    to(end) = [];
end

nbcf = length(from);
nbct = length(to);

firstne = find(from(1:min(nbct, nbcf))~=to(1:min(nbct, nbcf)), 1, 'first');

if isempty(firstne) 
    if nbcf == nbct
        rp = './';
        return
    elseif nbct>nbcf && to(nbcf+1)==fs
        rp = ['./' strrep(to(nbcf+2:end), '\', '/')];
        return;
    else
        idxfs = strfind(from, fs);
        firstne = idxfs(end)+1;
    end
elseif ismember(firstne, [1 2]) || (ispc && firstne == 3)
    error('relpath:check_args', 'There does not seem to be a relative path between these path');
else
    idxfs = strfind(from, fs);
    if from(firstne-1) ~= fs
        firstne = idxfs(find(idxfs<firstne, 1, 'last'))+1;
    end
end

rp = ['./' repmat('../', 1, 1+sum(idxfs>firstne)) strrep(to(firstne:end), '\', '/')];

end