function [binned_d, d, d2clean, has_changed] = safer_bin_separator(d, d2clean, req_field, varargin)
% "Si les horaires de trading changent, alors, le nombre d'intervalles par
% jour change, il faut donc empiler les jours en ayant pris soin de retirer
% les jours du d�but de l'intervalle qui ont un nombre diff�rent d'intervalles par jour. (Romain B.)"
%
% SAFER_BIN_SEPARATOR - Fonction heuristique permettant l'appel � BIN_SEPARATOR
% Liste des heuristiques :
% * Pas le m�me nombre de bins tous les jours :
%   - Seules derni�res dates qui ont un m�me nombre sont conserv�es
% * Fixings � des heures diff�rentes :
%   - Alignements des heures sur celles de la derni�re date
% * Horaires de trading diff�rents :
%   - Seules les derni�res dates qui ont les m�mes heures sont conserv�es
% * Fixing intraday ou de cl�ture qui ont la m�me heure qu'un intervalle du continu :
%   - On ajoute 3/2 seconde � ces fixings

has_changed = false;
if st_data('isempty-nl', d)
    binned_d = d;
    return;
end
try
    binned_d = bin_separator(st_data('keep', d, req_field), varargin{:});
catch ME
    if strcmp(ME.message, 'Trading hours changed')
        initial_length = length(d.date);
        [udays,~,idxdays] = unique(floor( d.date));
        
        % On efface les journees avec "nb. interval ~= nb. interval(end)" (on efface aussi les precedentes)
        nb_per_days = accumarray(idxdays,1);
        N = nb_per_days(end);
        i_hourchange = find(nb_per_days~=N,1,'last');
        if ~isempty(i_hourchange)
            idx_before_hourchange = idxdays<=i_hourchange;
            d.value(idx_before_hourchange, :) = [];
            d.date(idx_before_hourchange, :) = [];
            idx_before_hourchange = d2clean.date<=udays(i_hourchange)+1;
            d2clean.value(idx_before_hourchange, :) = [];
            d2clean.date(idx_before_hourchange, :) = [];
            has_changed = true;
        end
        
        % Realignement des horaires de fixings
        idx_open = st_data('cols', d, 'closing_auction')==1;
        last_open_hour = mod(d.date(find(idx_open,1,'last')),1);
        if any(mod(d.date(idx_open),1) ~= last_open_hour)
            d.date(idx_open) = floor(d.date(idx_open))+last_open_hour;
            d2clean.date(idx_open) = floor(d.date(idx_open))+last_open_hour;
            has_changed = true;
        end
        
        idx_close = st_data('cols', d, 'closing_auction')==1;
        last_close_hour = mod(d.date(find(idx_close,1,'last')),1);
        if any(mod(d.date(idx_close),1) ~= last_close_hour)
            d.date(idx_close) = floor(d.date(idx_close))+last_close_hour;
            d2clean.date(idx_close) = floor(d.date(idx_close))+last_close_hour;
            has_changed = true;
        end
        
        % Comparaison des horaires avec ceux de la derniere journee
        % Si differences, on ne conserve que les derni�res dates qui utilisent les horaires les plus r�cents
        if initial_length <= length(d.date) % On ne travaille pas sur des donnees retravaillees
            hours = reshape(mod(d.date,1),N,[])';
            i_hourchange = find(~all(bsxfun(@eq,hours,hours(end,:)),2),1,'last');
            if ~isempty(i_hourchange)
                d.date(1:i_hourchange*N,:) = [];
                d.value(1:i_hourchange*N,:) = [];
                idx_before_hourchange = d2clean.date<floor(d.date(1));
                d2clean.value(idx_before_hourchange, :) = [];
                d2clean.date(idx_before_hourchange, :) = [];
                has_changed = true;
            end
        end
        
        if ~has_changed
            % si l'intraday auction est estampill�e � la m�me heure que
            % l'un des intervalles du continu, alors on peut se retrouver
            % ici (exemple d'Istanbul apr�s le 14 septembre 2009).
            idx = find(st_data('cols', d, 'intraday_auction')==1);
            idx_before = idx(idx>1)-1;
            idx_after = idx(idx<length(d.date))+1;
            if any(mod(d.date(idx),1)==mod(d.date(idx_before),1)) || any(mod(d.date(idx),1)==mod(d.date(idx_after),1))
                d.date(idx) = d.date(idx) + 1.5/(24*3600); % on rajoute une 1.5 sec �  l'intraday_auction
                d2clean.date(idx) = d2clean.date(idx) + 1.5/(24*3600); % on rajoute une 1.5 sec �  l'intraday_auction
                has_changed = true;
                [binned_d, d, d2clean] = safer_bin_separator(d, d2clean, req_field);
                return
            end
            
            % Cela peut aussi arriver sur le close, exemple de Tokyo
            [~, idx] = st_data('where', d, '{closing_auction}');
            idx_last = find(idx,1,'last');
            if ~isempty(idx_last) && d.date(idx_last) == d.date(idx_last-1)
                d.date(idx) = d.date(idx) + 1.5/(24*3600); % on rajoute une seconde et demi � l'auction
                d2clean.date(idx) = d2clean.date(idx) + 1.5/(24*3600); % on rajoute une seconde et demi � l'auction
                has_changed = true;
                [binned_d, d, d2clean] = safer_bin_separator(d, d2clean, req_field);
                return;
            end
            
            error('safer_bin_separator:checking_steps', 'It is probable that Trading hours changed, but no step was made by safer_bin_separator');
        else
            [binned_d, d, d2clean] = safer_bin_separator(d, d2clean, req_field);
        end
    else
        % si l'intraday auction est estampill�e � la m�me heure que
        % l'un des intervalles du continu, alors on peut se retrouver
        % ici (exemple d'Istanbul apr�s le 14 septembre 2009).
        [~, idx] = st_data('where', d, '{intraday_auction}');
        idx_last = find(idx, 1, 'last');
        if d.date(idx_last) == d.date(idx_last+1) || d.date(idx_last) == d.date(idx_last-1)
            d.date(idx) = d.date(idx) + 1.5/(24*3600); % on rajoute une demi seconde �  l'intraday_auction
            d2clean.date(idx) = d2clean.date(idx) + 1.5/(24*3600); % on rajoute une demi seconde �  l'intraday_auction
            [binned_d, d, d2clean] = safer_bin_separator(d, d2clean, req_field);
            return;
        end
        error('safer_bin_separator:checking_steps', 'It is probable that Trading hours changed, but no step was made by safer_bin_separator');
    end
end
end
