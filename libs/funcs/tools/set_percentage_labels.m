function set_percentage_labels(axes_handle, axe_dir, varargin)
% SET_PERCENTAGE_LABELS - Transform the TickLabel in percentage
% Can be used for XTickLabel (axe_dir='x') or YTickLabel (axe_dir='y')
% Default: 2 decimals in labels
%
% Examples:
% plot( 0 : 0.1 : 1, 10 : 1 : 20)
% set_percentage_labels(gca, 'x')
% set_percentage_labels(gca, 'x', 'text_precision', '%.1f%%')
% set_percentage_labels(gca, 'x', 'text_precision', '%.0f%%')
%
% See also: 
%    
%
%   author   : 'vilec'
%   reviewer : ''
%   date     :  '12/04/2012'
%   
%   last_checkin_info : $Header: set_percentage_labels.m: Revision: 5: Author: hahar: Date: 09/18/2012 11:06:59 AM$

opt = options({'text_precision', '%.2f%%'}, varargin); 

tick_values = get(axes_handle, [axe_dir 'Tick']);
nb_ticks = length(tick_values);
New_Labels = cell(nb_ticks, 1);

for i = 1 : nb_ticks
    try 
        New_Labels{i} = sprintf(opt.get('text_precision'), 100 * tick_values(i));
    catch e
        st_log('\nNot able to convert one of the tick value to percentage');
    end
end

set(axes_handle,[axe_dir 'TickLabel'],New_Labels);
    