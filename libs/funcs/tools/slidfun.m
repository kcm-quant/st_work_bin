function varargout= slidfun(fun,window,varargin)
%
%  [y1,...] = slidfun(@fun,window,x1,...)
% 
% Applies the function handle fun using sliding windows
% 
% Arguments
%     * fun      = function handle
%     * window   = [w.width, w.step]
%                  windows are adjusted to the last point of the sample
%     * x1,x2,...= arguments of fun
%                  matrices or cell-array, with lines <=> observations
%
% Outputs
%     * y1,y2,...= outputs of fun
%           -  When using p outputs and q inputs, the syntax
%              [y1,...,yp]= fun(x1,...,xq) must be legal
%            - slidfun returns cell-array (non uniform output):
%              yp{k} : p-th output on the k-th sliding window
%
% Exemples: sliding mean
%     sm= slidfun(@mean,[100, 1],randn(1000,3));
%     plot([sm{:}])
%
% See also uo_
%
% Get indices of each window-  special syntax (2 arguments only):
%     [idxLast,idxFirst]= slidfun(T,window)
%     with T= size(x1,1) and window as before
%
% @ Author: Nathanael Mayo
%
% todo: 3D array
%
% DONE: structoutput
%
% TODO: flags
% TODO: renvoyer mm dimension que input (nan/smaller widths, aligner windows ou realisation ?)
% 
%


%1/ mode indice: 2 arguments
if nargin == 2	
    T= fun;
    ww= window(1); ws= window(2);
    idx= T:(-ws):(ww); % la derni�re fen�tre (aujourdhui) est compl�te
    idx= idx(end:-1:1);
    idxStart= idx - ww+1;
	varargout={idx,idxStart};
	return
end

opt= dico({'struct',false,'uo',[]});
if isa(varargin{end},'dico')
    opt.update(varargin{end});
    varargin(end)=[];
end


%% initialisation
%2/ mode normal: > 2 arguments
if nargin< 2; error('slidfun: wrong number of arguments');end
nArgs= length(varargin);
nOuts= nargout;% length(varargout);
varargout= cell(1,nOuts);
x1= varargin{1}; [T,P]= size(x1);

%% sliding window index
ww= window(1); ws= window(2);
idx= T:(-ws):(ww); % la derni�re fen�tre (aujourdhui) est compl�te
idx= idx(end:-1:1);

if length(window)== 3 && window(3)== -1
	% mode rapide
	for wk = 1:length(idx)
		%1/ subsampling
		xks=[];
		varinSubs= cell(1,nArgs );
		for k=1:nArgs
			varinSubs{k} = varargin{k}(idx(wk)-ww+1:idx(wk) ,: );
			% ceci marche que arg.k soit matrice ou cell
		end
		%2/ function eval
		value = cell(1,nOuts);
		[value{:}] = fun(varinSubs{:} );
		%3/ concatenates outputs
		for k=1:nOuts
			varargout{k}{end+1} = value{k};
		end
	end
	return;
end
%% sliding window
for wk = 1:length(idx)
	%1/ subsampling
    xks=[];
	varinSubs= cell(1,nArgs );
	for k=1:nArgs
		varinSubs{k} = varargin{k}(idx(wk)-ww+1:idx(wk) ,: );
		% ceci marche que arg.k soit matrice ou cell 
	end
	%2/ function eval
	value = cell(1,nOuts);
	[value{:}] = fun(varinSubs{:} );
	%3/ concatenates outputs
	for k=1:nOuts
		varargout{k}{end+1} = value{k};
	end		
end

iuo= opt.get('uo');
if ~isempty(iuo)    
    varargout(iuo)= cellfun(@uo_,varargout(iuo),'uni',0);
end

if opt.get('struct')
    OUT= [];
    OUT.type= 'slidfun';
    OUT.fun= fun;
    OUT.W= window;
    T= size(varargin{1},1);
    ww= window(1); ws= window(2);
    OUT.iend= T:(-ws):(ww); % la derni�re fen�tre (aujourdhui) est compl�te
    OUT.iend= OUT.iend(end:-1:1);
    OUT.istart= OUT.iend - ww+1;
    OUT.values= varargout;
    varargout= {OUT};
end

%% test
function test_()
[sm,sm2]= slidfun(@size,[10, 1],randn(100,1))
[sm,sm2]= slidfun(@(x)eig(cov(x)),[100, 1],randn(1000,50))

V= slidfun(@size,[10, 1],randn(100,1),kwargs.dico.uo(1))
V= slidfun(@size,[10, 1],randn(100,1),kwargs.dico.uo(1).struct(true))

T=1333;
x= rand(T,1)
W= [300,1]
[fi,di]= slidfun(length(x),W)
sm= uo_(slidfun(@mean,W,x))
assert(length(fi)== length(sm))

sm= uo_(slidfun(@(x,y)mean(x,max(y)),W,x,ones(T,1)))
assert(length(fi)== length(sm))

[fi,di]= slidfun(length(x),W)
sm= uo_(slidfun(@OLSforecast,W,x,[x,x]))
assert(length(fi)== length(sm))



