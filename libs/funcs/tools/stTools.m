function varargout= stTools(varargin)
% Tools for st_data manipulation
% stTools() returns a toolbox with function handles
%
% stTools(funname,varargin) calls the method funname(varargin{:}) of the
% toolbox
 
tbx= [];
tbx.name_ = 'st_data tools: stTools.m ';
tbx.index= @index_;
tbx.remove= @remove_;
tbx.aggregateIndicatrix= @aggregateIndicatrix_;
tbx.intersect= @intersect_;
tbx.union= @union_;
tbx.alignerColonne= @alignerColonne_;
tbx.rename= @rename_;
tbx.eventFilter= @eventFilter_;

tbx.struct2st_data= @struct2stdata;
tbx.aggregate=@aggregate;

tbx.extend_date = @extend_date;
tbx.splitDays= @splitDays;
tbx.cat= @cat_;
tbx.agg= @agg;

tbx.slidfun = @slidfun_;
tbx.fast = @fast_;

if nargin> 0
%     nbo= nargout(tbx.(varargin{1}));
%     if nbo==-1; nbo= nargout;end
    nbo= ftool.selectNbOut(tbx.(varargin{1}));
    varargout= cell(nbo,1);
    [varargout{:}]= tbx.(varargin{1})(varargin{2:end});
else
    varargout={tbx};
end

% Extends non existing NEWDATES by NaNs. Keeps ONLY newdates
function data= extend_date(data, newdates, only)
    if nargin<2; only= 0; end
    if ~only; newdates= sort(union(data.date,newdates)); end
    assert(min(diff(newdates))>= 1 ,'multiple occurences in dates !')
    oldvalues = data.value;
    [idx,loc]= ismember(data.date,newdates);
    data.value= nan(length(newdates),size(oldvalues,2));
    data.value(loc(loc>0),:)= oldvalues(idx,:);  
    data.date= newdates;
    assert(length(newdates)== size(data.value,1));


function agg= aggregate(varargin)
    agg= varargin{1};
    agg.value= [];
    agg.date= [];  
    agg.phases= {};
    for k=1:length(varargin)
        x= varargin{k};
        assert(all(cellfun(@isequal,agg.colnames,x.colnames)))
        assert(size(x.value,1)==1);
        assert(length(x.date)==1);
        agg.value(end+1,:)= x.value;
        agg.date(end+1)= x.date;
        agg.phases{end+1}= x.phases;
    end    

function x= cat_(dim,varargin)    
% Assumes correct dimensions !
% TODO: check colnames order, et dates intersect vide ?
% TODO: cat generique gerant a la fois dates et colonnes (les rajoutant si besoin)
x= varargin{1};
if iscell(x); varargin= x;x= varargin{1};end
values= cellfun(@(x) x.value,varargin,'uni',0); x.value= cat(dim,values{:});
dates= cellfun(@(x) x.date,varargin,'uni',0);
colns= cellfun(@(x) x.colnames,varargin,'uni',0);
if dim== 1
    check_cols= cellfun(@(x) isequal(x,colns{1}),colns);
    assert(all(check_cols))
    x.date= cat(dim,dates{:});
elseif dim==2    
    x.colnames= cat(dim,colns{:});
else;assert(false)
end

function Z= agg(data,windowWS,cols)
% Aggregates indicators in time. Splits days and cats them
% data.intraday_only or all(auction==0) => <time> mode

if nargin< 3; cols= data.colnames;end % gets all existing colnames

intrad_only_flag= isfield(data,'intraday_only');
if ~intrad_only_flag
    intrad_only_flag= all(CNwrap(data).auction==0);
end
if intrad_only_flag
   Gmode= {'grid_mode', {'time',... % il faut group by et passer en mode <time>
       'auction','closing_auction','intraday_auction','opening_auction','stop_auction'}};
else
   Gmode= {'grid_mode', {'time_and_phase'}};
end

cols= setdiff(cols,{'begin_slice','end_slice'}); % sinon en doublon !!!
daydatas= splitDays(data);
data_agg = cellfun(@(x) st_data('agg', x, 'colnames', cols,...
    Gmode{:},...
    'grid_options', {'time_type', 'seconds', 'window', windowWS(1), 'step', windowWS(2)}),...
    daydatas,'uni',0);
clear data; clear daydatas;
Z= cat_(1,data_agg); % OOM error
% Z= data_agg{1};
% for k=2:length(data_agg)
%     Z= cat_(1,Z,data_agg{k});
% end
Z.frequency= windowWS;
Z.intraday_only= intrad_only_flag;



function x= struct2stdata(s)
l= struct2list(s);
N= length(l);
x_=[];
for n=2:2:N
   x_=[x_, l{n}{:}];    
end
x= st_data('from-matrix', x_);
x.colnames= l(1:2:end);

function names= strname_(st,names)
    if ~ischar(names) && ~iscell(names)
        names= st.colnames(names);
    end

% - si names est un string ou un cell-array de string, on renvoie le tableau
%   des indicatrices : [idx(i,j) == 1] <=> [names{j} se retrouve dans st.colnames{i} ]
% - si names est un tableau d'entiers, on ne le modifie pas
function idx= index_(st, names)
 if ischar(names)
     names= {names};
 end
 if iscell(names)
     idx= cellfun(@(x)strcmpi(x,st.colnames'),names,'UniformOutput' ,0);
     idx= [idx{:}];
     %idxMiss= (sum(idx)==1)== 0;
     assert(all(sum(idx)==1));
 else
     idx= names;
 end
 
 %en ligne st.colnames , en cols names
 % i.e une colonne par variable demand?e dans la requete
 
function st= remove_(st,names)
    if iscell(names) || ischar(names)
        tb= stTools();
        idx= tb.index(st,names);
        idx= any(idx,2);
    else
        idx= names;
    end
    st.value(:,idx)= [];
    st.colnames(idx)= [];


function st= aggregateIndicatrix_(st, names, modeUnion)
% aggregation de variables binaires
if nargin < 3;modeUnion= 0;end
idx= index_(st,names);
if modeUnion== 0 % Premiere dans la liste names
    newvar= st.value(:,idx(:,1));
    newname= strname_(st,names{idx(:,1)});       
elseif modeUnion== 1 % Intersection des Events
    newvar= st.value(:, any(idx,2));
    newvar= prod(newvar,2);
    newname= [strname_(st,names{idx(:,1)}) ' intersect'];      
elseif modeUnion== 2 % Union des Events
    newvar= st.value(:, any(idx,2));
    newvar= min(1,sum(newvar,2) );
    newname= [strname_(st,names{idx(:,1)}) ' union'];  
else
    error(['unknown mode in stTools.aggregateIndicatrix(): ' num2str(modeUnion) ] );        
end
st= remove_(st, any(idx,2) ); 
st.value(:,end+1)= newvar;
st.colnames{end+1}= newname;


function varargout= union_(varargin)
% Synchronise un cell-array de st_data sur l'union de ses dates
if isnumeric(varargin{1});
   precision= varargin{1};
   varargin(1)=[];
else
   precision= 1;
end
if iscell(varargin{1})
    OneCellArg= 1;
    datas= varargin{1};
else
    datas= varargin;
    OneCellArg= 0;
end
interdates= datas{1}.date * precision;
K= length(datas);
for k=2:K
    interdates= union(interdates,datas{k}.date*precision);
end
cellout= {};
for k=1:K
    d= datas{k};    
    nv= nan(length(interdates),size(d.value,2));
%     idx= ismember(d.date*precision,interdates); % elles le sont toutes !!!
%     nv(find(idx),:)= d.value;  
    [idx,loc]= ismember(d.date*precision,interdates);
    nv(loc,:)= d.value;
    d.date= interdates / precision;
    d.value= nv;
    cellout{k}= d;
end
if OneCellArg
    varargout{1}= cellout;
else
    varargout= cellout;
end

function varargout= intersect_(varargin)
% Synchronise un cell-array de st_data sur l'intersection de ses dates
% procede par ?galit? des dates
% date daily uniquement: (buggera si partie fractionnaire)
if isnumeric(varargin{1});
   precision= varargin{1};
   varargin(1)=[];
else
   precision= 1;
end

if iscell(varargin{1})
    OneCellArg= 1;
    datas= varargin{1};
else
    datas= varargin;
    OneCellArg= 0;
end
interdates= datas{1}.date * precision;
K= length(datas);
for k=2:K
    interdates= intersect(interdates,datas{k}.date * precision);
end
cellout= {};
for k=1:K
    d= datas{k};    
    idx= ismember(d.date * precision,interdates);
    assert(sum(idx) == length(interdates));
    d.value= d.value(find(idx),:);    
    d.date= interdates / precision;
    cellout{k}= d;
end
if OneCellArg
    varargout{1}= cellout;
else
    varargout= cellout;
end


function [newdata, nonFound]= alignerColonne_(data,ordre)
% les colonnes de data sont remises dans l'ordre. ordre= colnames dans
% l'ordre voulu. Les variables non pr?sentes dans ordres sont remises ? la
% fin.
nonFound= {}; % elements de ordre non trouv? dans data
usedData= []; % elements de data utilis?s
N0= length(data.colnames);
N= length(ordre);
newdata= data;
newdata.value= [];
newdata.colnames={};
for n=1:N
    found_= 0;
    for m=1:N0
        % ordre(n) est retrouve en colonne(m)
        if strcmp(ordre{n},data.colnames{m})
            newdata.value(:,end+1)= data.value(:,m);
            newdata.colnames{end+1}= data.colnames{m};
            usedData(end+1)= m;
            found_= 1;
            break
        end        
    end
    if ~found_
        nonFound{end+1}= ordre{n};
    end
    
end
nonUsed= setdiff(1:N0,usedData);
for k= (nonUsed(:))'
    newdata.value(:,end+1)= data.value(:,k);
    newdata.colnames{end+1}= data.colnames{k};
end
    
function data= rename_(data,old,new)
idx= strcmpi(data.colnames, old);
idx= find(idx);
% 2 colonnes ont le meme nom !!!
assert(length(idx)== 1);
data.colnames{idx}= new;
        
            
function data= eventFilter_(news, removedNames)
% hand- removes of ill columns

data= news;
data.Removed_eventFilter= {};
%removedNames= {'Expiry.SET1','Expiry.SET0','Future EURO STOXX 50','Future DAX','Close.NY','Close.Londres','Future CAC40'};
%removedNames= {'Expiry.SET1','Expiry.SET0','Future EURO STOXX 50','Future DAX'};
for k=1:length(removedNames)
    data = st_data( 'remove', data, removedNames{k});
    data.Removed_eventFilter{end+1}= removedNames{k};
end

function c= splitDays(d)
    % returns cell array of st_data in each day
    fd= floor(d.date);
    days= unique(fd);
    %idx= arrayfun(@(x)fd==x,days,'uni',0);
    c= arrayfun(@(x) st_data('extract-day',d,x),days,'uni',0);

    
function data= slidfun_(data,fun,W,names,align)
% Allows CNwrap access in fun using compound functions
% Aligns on the end of windows (fefault, allows 'begin', 'end' and 'after')

%-- Slidfun, fun(CNwrap)
CN= data.colnames;
wfun= @(x) fun(CNwrap(x,CN)); % rewraps sliding windows values w constant colnames
T= size(data.value,1);
z= uo_(slidfun(wfun,W,data.value));
[fi,di]= slidfun(T,W);
%-- Interps and aligns in data
if nargin<5; align= 'end';end
if       strcmpi(align,'end'); yi= interp1(data.date(fi),z,data.date);
elseif strcmpi(align,'begin'); yi= interp1(data.date(di),z,data.date);
elseif strcmpi(align,'after'); yi= interp1(data.date(fi)+1,z,data.date);
end
%-- Assigns in data
h= CNwrap(data);
if ~iscell(names); names={names};end
subsi= substruct('{}',names); % multicolumn % h.(name)= yi;
subsasgn(h,subsi,yi);
data= h.wrapped;

function data= fast_(values,varargin)
% st_data from values. Detects optionnal dates/title/colnames by type in varargin
[P,Q]= size(values);
% COLNAMES
i= find(cellfun(@iscell,varargin));
if isempty(i); cnames= arrayfun(@(x) ['V' num2str(x)],1:Q,'uni',0);
else; cnames= varargin{i};
end
% TITLE
i= find(cellfun(@ischar,varargin));
if isempty(i); ttl= 'untitled';
else; ttl= varargin{i};
end
% DATES
i= find(cellfun(@isnumeric,varargin));
if isempty(i); dts= (1:P)';
else; dts= varargin{i};
end
data = st_data( 'init', 'title',ttl, 'value', values, ...
                    'date', dts, 'colnames', cnames);


    
 
 %%
function TEST_()
    % Creation
    x= tb.fast([(1:100)',(101:200)'])
    x= tb.fast([(1:100)',(101:200)'],'title')
    x= tb.fast([(1:100)',(101:200)'],{'x','y'})
    x= tb.fast([(1:100)',(101:200)'],-(1:100)')
    x= tb.fast([(1:100)',(101:200)'],-(1:100)',{'x','y'},'title')
    x= tb.fast([(1:100)',(101:200)'],{'x','y'},-(1:100)','title')

    % Index, remove cols 
    data = tb.fast(cumsum(randn(100,5)));
    tb= stTools();
    idx= tb.index(data,{'V1','V2'})
    
    d= tb.remove(data,{'V1','V4'})
    d= tb.remove(data,'V3')
    d= tb.remove(data,[1 2])
    
    idx= index_(data,{'V1','V4'});
    
    
    % Indicatrix
     data = tb.fast( [ [ones(3,1);zeros(6,1)], [ones(6,1);zeros(3,1)],randn(9,1) ]);
                    
    
    tb= stTools();
    d= tb.aggregateIndicatrix(data,{ 'V1', 'V2'}); d.value
    d= tb.aggregateIndicatrix(data,{ 'V2', 'V1'}); d.value
    d= tb.aggregateIndicatrix(data,{ 'V1', 'V2'},1); d.value
    d= tb.aggregateIndicatrix(data,{ 'V1', 'V2'},2); d.value
    
    % Intersect
    data1 = tb.fast((1:100)');
    data2 = tb.fast((1:100)',(51:150)',{'V3'});
    tb= stTools();
    c= tb.intersect({data1,data2});
    d1= c{1}
    d2= c{2}
    
    [dd1,dd2]= tb.intersect(data1,data2);
    
    % Aligner
    data = tb.fast( [[1:5];1:5],(1:2)',{ 'V1', 'V2','V3', 'V4','V5' });
    tb= stTools();
    [d,nonF]= tb.alignerColonne(data,{'V2','V1'})
    
    
    tb= stTools();
    x= tb.fast([(1:100)',(101:200)'])
    
    z= tb.slidfun(x,@(x) mean(x.V1),[10,1],'slidmean')
    plot(z.value(:,1),z.value(:,3),'k.')
    
    z= tb.slidfun(x,@(x) mean(x.V1.V2),[10,1],{'slidmean1','slidmean2'})
    plot(z.value(:,1),z.value(:,3),'k.')
    
    