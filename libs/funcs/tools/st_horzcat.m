function data = st_horzcat(data1, data2, varargin)
% ST_HORZCAT - Fonction utilis�e pour agr�ger les colonnes de deux
% donn�es structur�es
% 
% Arguments : data1, data2, deux donn�es structur�es dont on souhaite
%   agr�ger les colonnes, de fa�on � ce que le jeu de donn�es en sortie
%   aie autant de dates que la r�union des dates des jeux de donn�es.
%   Lorsque l'un des jeux de donn�es structur�es ne contient pas de donn�es
%   pour un jour pour lequel l'autre jeu de donn�es en contient, la valeur 
%   pour ce jour sera un NaN (ou la valeur de 'default_value si vous 
%   utilisez cet argument optionnel')
%
% Attention le titre qui sera conserv� est le titre du premier jeu de
% donn�es
%
% Options : 'default_value' -> la valeur � utiliser pour remplir les
%                               donn�es manquantes
%           'colnames2'     -> doit �tre un pointeur de fonction � deux
%                               arguments, le premier argument est le nom
%                               de la colonne, le deuxi�me le titre.
%                               cette fonction est appliqu�e aux noms de la
%                               deuxi�me structure de donn�es afin de
%                               composer un nouveau nom de colonne � partir
%                               du nom de colonne et du titre de ce
%                               jeu de donn�es strucutr�es
%           'missing warning'-> Si la proportion de valeurs manquantes est
%                               sup�rieure � cette valeur, un warning
%                               pr�viendra l'utilisateur � l'aide de st_log
%           'missing error' ->  Si la proportion de valeurs manquante est
%                               sup�rieure � cette valeur alors
%                               st-aggregate ne fera pas l'aggr�gation, il
%                               renverra la structure contenant le moins de
%                               donn�es manquantes
%
% ex d'utilisation :
% data = [];
% for i = 1 : N
%     data = st_aggregate(data, fonction_qui_extrait_des_donnees_et_les_renvoie_sous_forme_de_donn�es_structur�es(i));
% end    
%
%   See also st_vertcat
    
    opt = options({'default_value', NaN, 'colnames2', @(x,y)x, 'missing warning', 0.1, 'missing error', 0.5}, varargin);

    % Permet d'utiliser la fonction dans une boucle sans faire
    % d'initialisation particuli�re
    if (isempty(data1) && isempty(data2))
        data = [];
        return;
    end
    if (isempty(data1) || isempty(data1.value))
        [N2 P2]    = size(data2.value);
        data = copy_fields(data2, data1);
        data.colnames = cellfun(opt.get('colnames2'), data2.colnames, repmat({data2.title}, 1, P2), 'uni', false);
        return;
    elseif (isempty(data2) || isempty(data2.value))
        data = copy_fields(data1, data2);
        %data.colnames = cellfun(opt.get('colnames2'), data1.colnames, repmat({data1.title}, 1, P1), 'uni', false);
        return;
    end
    
    % Un petit test minimal (absolument n�cessaire ici) sur l'int�grit� des
    % donn�es
    [N1 P1]    = size(data1.value);
    [N2 P2]    = size(data2.value);
    if (length(data1.date) ~= N1)
        error(['Bad data1 : ' data1.title ' has not the same number of lines in value and date']);
    elseif ~all(diff(data1.date))
        error(['Bad data1 : ' data1.title ' has several times the same date']);
    elseif (length(data2.date) ~= N2)
        error(['Bad data2 : ' data2.title ' has not the same number of lines in value and date']);
    elseif ~all(diff(data2.date))
        error(['Bad data2 : ' data2.title ' has several times the same date']);
    end

    %On recopie dans data le contenu de data1.info et de data2.info
    data = copy_fields(data1, data2);
    
    % Les champs de la structure de donn�es qui sera renvoy�e
    data.date = union(data1.date, data2.date);
    if length(data.date) == length(data1.date) ...
        && length(data.date) == length(data2.date) ...
        && (~all(data.date == data1.date) || ~all(data.date == data2.date))
        error('Error in st_horzcat, your data should be ordered with increasing dates.');
    end
    default_value = opt.get('default_value');
    if isnumeric(default_value)
        miss_prop1 = 1 - length(data1.date) / length(data.date);
        miss_prop2 = 1 - length(data2.date) / length(data.date);
        if (miss_prop1 >= opt.get('missing warning'))
            missing_proportion_disp(data1, miss_prop1)
            if (miss_prop1 >= opt.get('missing error'))
                if (miss_prop2 >= opt.get('missing error'))
                    error('st_horzcat : les deux jeux de donn�es se recoupent en trop peu de point')
                else
                    st_log('ERRROR in st_horzcat : there was so much missing data that the first time serie has been ignored');
                    data = data2;
                    return;
                end
            end
        elseif (miss_prop2 >= opt.get('missing warning'))
            missing_proportion_disp(data2, miss_prop2)
            if (miss_prop2 >= opt.get('missing error'))
                st_log('ERRROR in st_horzcat : there was so much missing data that the second time serie has been ignored');
                data = data1;
                return;
            end
        end
    end
    N = length(data.date);
    data.value = zeros(N, P1 + P2);
    data.colnames = horzcat(data1.colnames,...
                        cellfun(opt.get('colnames2'), data2.colnames, repmat({data2.title}, 1, P2), 'uni', false));
    data.title = data1.title;
    
    % Aggr�gation proprement dite
    default_value = opt.get('default_value');
    i1 = 1;
    i2 = 1;
    for i = 1 : N
        if (i1 <= N1 && data.date(i) == data1.date(i1))
            data.value(i, 1:P1) = data1.value(i1, :);
            i1 = i1 + 1;
        else  %data.date(i) < data1.date(i1) || i1 > N1 
            % => cette date n'apparait pas dans data1
            if isnumeric(default_value)
                data.value(i, 1:P1) = repmat(default_value, 1, P1);
            elseif strcmp(default_value, 'last')
                if i ~= 1
                    data.value(i, 1:P1) = data.value(i - 1, 1:P1);
                else
                    data.value(i, 1:P1) = NaN(1, P1);
                end
            end
        end
        if (i2 <= N2 && data.date(i) == data2.date(i2))
            data.value(i, P1 + 1:P1 + P2) = data2.value(i2, :);
            i2 = i2 + 1;
        else  %data.date(i) < data2.date(i2) || i2 > N2
            % => cette date n'apparait pas dans data2
             if isnumeric(default_value)
                data.value(i, P1 + 1:P1 + P2) = repmat(default_value, 1, P2);
             elseif strcmp(default_value, 'last')
                if i ~= 1
                    data.value(i, P1 + 1:P1 + P2) = data.value(i - 1, P1 + 1:P1 + P2);
                else
                    data.value(i, P1 + 1:P1 + P2) = NaN(1, P2);
                end
            end
        end
    end
end

function missing_proportion_disp(data_, missing_prop)
    st_log('WARNING in st_horzcat : ');
    for k = 1 : length(data_.colnames)
        st_log([' ' cell2mat(data_.colnames(k))]);
    end
    st_log([' ' data_.title]);
    st_log([' have ' num2str(100 * missing_prop, 2) '%% missing values']);
end