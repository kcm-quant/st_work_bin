function data = st_vertcat(data1, data2, varargin)
% ST_VERTCAT - permet d'agr�ger deux donn�es structur�es portant sur les
%               m�me donn�es, mais sur des p�riodes de temps diff�rentes
%
%               Je crois que c'est redondant avec st_data('add-col',...)
%               Si quelqu'un a un avis l�-dessus, qu'il n'h�site pas � le
%               dire � Romain
%
%   See also st_horzcat

    if (data1.date(1) > data2.date(1))
        tmp = data1;
        data2 = data1;
        data1 = tmp;
    end
    data = copy_fields(data1, data2);
    data.date = [data1.date; data2.date];
    data.colnames = intersect(data1.colnames, data2.colnames);
    P = length(data.colnames);
    if (P == length(data1.colnames) && P == length(data2.colnames) ...
            && P == size(data1.value, 2) && P == size(data2.value, 2))
        data.value = [data1.value; data2.value];
    else
        data.value = zeros(size(data1.value, 1) + size(data2.value, 1), P);
        for i = 1 : P
            data.value(:, i) = [st_data('col', data1, data.colnames(i)); st_data('col', data2, data.colnames(i))];
        end
    end
end