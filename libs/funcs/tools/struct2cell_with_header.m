function c = struct2cell_with_header(s)
% STRUCT2CELL_WITH_HEADER - converting a structure into printable (latex) cell
%
%
%
% Examples:
%   struct2cell_with_header(struct('field1', 'val1', 'field2', 'val2'))
%   struct2cell_with_header(struct('field1', {{'val11', 'val12'}}, 'field2', {{'val21', 'val22'}}))
%   struct2cell_with_header(struct('field1', 1:2, 'field2', 3:4))
% 
% 
%
% See also: latex
%    
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   date     :  '18/04/2012'
%   
%   last_checkin_info : $Header: struct2cell_with_header.m: Revision: 2: Author: robur: Date: 04/18/2012 03:43:02 PM$

fns = fieldnames(s)';
c = struct2cell(s)';
for i = 1 : length(fns)
    switch class(c{i})
        case 'cell'
            c{i} = c{i}(:);
        case 'char'
            c{i} = cellstr(c{i});
        case {'double', 'logical'}
            c{i} = num2cell(c{i}(:));
        otherwise
            error('struct2cell_with_header:check_args', 'Unexpected type');
    end
end
c = cat(1, fns,cellflat(c));

end