function out = struct2varargin(s)
% STRUCT2VARARGIN - Short_one_line_description
%
%
%
% Examples:
% struct2varargin(struct('a',1,'b',[1 2 3]))
% 
% 
%
% See also:
%    
%
%   author   : 'njoseph@cheuvreux.com'
%   reviewer : ''
%   date     :  '06/03/2013'
%   
%   last_checkin_info : $Header: struct2varargin.m: Revision: 1: Author: nijos: Date: 03/06/2013 03:05:50 PM$


out={};

if ~isempty(s)
    if ~isstruct(s)
        error('struct2varargin:input','input is not a struct!');
    end
    fns = fieldnames(s);
    for i = 1 : length(fns)
        out=cat(2,out,{fns{i},getfield(s,fns{i})});
    end
end
end