function s = struct_orderby(s, fields4order, mode)
% STRUCT_ORDERBY - reorder the values of a struct according to values of a field
%
%
%
% Examples:
%
% struct_orderby(struct('a', 2:-1:1, 'b', 1:2), 'a')
% struct_orderby(struct('a', 2:-1:1, 'b', 1:2), 'b', 'desc')
% 
%
% See also:
%    
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   date     :  '18/04/2012'
%   
%   last_checkin_info : $Header: struct_orderby.m: Revision: 1: Author: robur: Date: 04/18/2012 12:02:05 PM$

% TODO allow for several columns to be used for sorting

fns = fieldnames(s);
if nargin < 3
    mode = 'asc';
end

[~, idx] = sort(s.(fields4order));
switch mode
    case 'asc'
    case 'desc'
        idx = idx(end:-1:1);
    otherwise
        error('struct_orderby:mode', 'MODE: <%s> unknown', mode);
end
for i = 1 : length(fns)
    s.(fns{i}) = s.(fns{i})(idx);
end


end