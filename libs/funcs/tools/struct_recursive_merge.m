function s = struct_recursive_merge(s1, s2, field2concat)
% STRUCT_RECURSIVE_COPY - recursive merging of a 2 structures
% - if a fieldname appears only in one field, it will appear
%       in the result with the same content.
% - if a fieldname appears in both fields but not in the input
%       list of fieldnames (field2concat below), then the merged info
%       field will be the recursive merged structures
% - if a fieldname appears in both fields and also in the input
%       list of fieldnames (field2concat below), then the merged info
%       field will concatenate the contents of both info fields
%
% - if there is a field that is a struct array then we wont try to merge
%   we'll just take take the first struct array if y'oud rather concat it
%   then use field2concat
%
% test :
% a = struct('a', {repmat({'a'}, 4, 1)}, 'b', struct('c', repmat({'a'}, 4, 1)), 'c', struct('d', struct('e', 3, 'f', 4, 'g', 7)))
% b = struct('a', {repmat({'b'}, 7, 1)}, 'b', struct('c', repmat({'b'}, 7, 1)), 'c', struct('d', struct('e', 1, 'f', 2, 'h', 12)))
% 
% c = struct_recursive_merge(a, b, {'f'})
% c.a,c.b,c.c,c.c.d,c.c.d.f
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : 'athabault@cheuvreux.com'
%   version  : '1'
%   date     :  24/01/2011

if isempty(s1)
    s = s2;
    return
elseif isempty(s2)
    s = s1;
    return;
end
if ~isstruct(s2) || ~isstruct(s1)
    if (~isempty(s1))
        s = s1;
    else
        s = s2;
    end
else
    if length(s2)>1 || length(s1)>1
        if ~isempty(s1)
            s = s1;
        else
            s = s2;
        end
        return;
    end
    s = s1;
    fs1 = fieldnames(s1)';
    fs2 = fieldnames(s2)';
    [fs_common, fs2_not_fs1] = intersect_and_diff(fs2, fs1);
    for i = 1 : length(fs2_not_fs1)
        s.(fs2_not_fs1{i}) = s2.(fs2_not_fs1{i});
    end
    [fs2concat, fs_not2concat] = intersect_and_diff(fs_common, field2concat);
    for i = 1 : length(fs2concat)
        [n k] = size(s1.(fs2concat{i}));
        if k > n
            s1.(fs2concat{i}) = s1.(fs2concat{i})';
        end
        [n k] = size(s2.(fs2concat{i}));
        if k > n
            s2.(fs2concat{i}) = s2.(fs2concat{i})';
        end
        s.(fs2concat{i}) = cat(1, s1.(fs2concat{i}), s2.(fs2concat{i}));
    end
    for i = 1 : length(fs_not2concat)
        s.(fs_not2concat{i}) = struct_recursive_merge(s1.(fs_not2concat{i}), ...
            s2.(fs_not2concat{i}), field2concat);
    end
end

end