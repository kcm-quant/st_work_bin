function [X,keys,missing]= uni_options(c,keys)
% aligns uniformly a cellarray of options, filling missing keys with nan values
% X: Lignes= options, colonnes= keys
% c= cell array de options, valeurs numeriques

missing=[];
if nargin ==2 % transfo inverse matrice->options
   [P,Q]= size(c);
   assert(length(keys)== Q);
   X={};   
   for p=1:P    
       items ={};
       for q=1:Q
           if isnan(c(p,q))
           else
               items{end+1}= keys{q};
               items{end+1}= c(p,q);
           end
       end
       X{end+1}= options(items);
   end 
   return 
end

N= length(c);
keys={};
for n=1:N
    it= c{n}.get();
    keys= {keys{:},it{1:2:end}};
end
types_= cellfun(@ischar,keys);
numkeys= unique([keys{~types_}]);
numkeys= unique(numkeys);
numkeys= num2cell(numkeys);
strkeys= unique(keys(types_));
strkeys= unique(strkeys);
keys= {numkeys{:},strkeys{:}};
Q= length(keys);
missing={};
X= nan(N,Q);
for n=1:N
    for q=1:Q
        if c{n}.contains_key(keys{q})
            v= c{n}.get(keys{q});        
            X(n,q)= v;
        else
            missing{end+1}= sprintf('key<%s> %s',num2str(keys{q}),num2str(n));
        end        
    end
end

function TEST()
o1= options({'a',1,'b',3})
o2= options({'a',1,'c',3})
%o3= options({'a',1,'b',2,'c',5})

[X,keys,missing]= uni_options({o1,o2})
% transfo inverse
oo= uni_options(X,keys)

o1= options({1,1,2,3})
o2= options({1,1,3,3,'a',0})
%o3= options({'a',1,'b',2,'c',5})

[X,keys]= uni_options({o1,o2})


