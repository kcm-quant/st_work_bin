function y= uo_(x)
% y= uo_(x): uniform output
% transfoms a cell array x into a matrix y
% y of size [T,P], with
% - T: length cell array x.
% - P: length of the content of x

try
	% il faut que le contenu des x soit une colonne -> devient une ligne ci-dessous
	x= cellfun(@(t) t(:) , x ,'uniformoutput', false);
	% concatÚnation des contenus
	y= [x{:}]';
catch
	error('All cells do not have the same number of element in uo_()');
end