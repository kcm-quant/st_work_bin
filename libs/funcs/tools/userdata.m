function z = userdata( h, mode, varargin)
% USERDATA - manipulation du champ userdata
%
% See also options 
opt = get(h, 'userdata');
if isempty(opt)
    opt = options({});
end
switch lower(mode)
    case 'set'
        z = opt.set( varargin{:});
    case 'get'
        try
            z = opt.get( varargin{1});
        catch 
            if nargin>3
                z = varargin{2};
                lasterr('');
            else
                lasterr('');
            end
        end
    otherwise
        error('userdata:mode', 'mode <%s> not implemented yet', mode);
end
set(h,'userdata', opt);
        