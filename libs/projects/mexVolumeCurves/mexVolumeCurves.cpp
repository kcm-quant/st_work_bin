#include "VwapPortofolioTrCurves.h"
#include "VwapTrendTrCurves.h"
#include "TargetCloseTrCurves.h"
#include "ISPortofolioTrCurves.h"
#include "MathModule.h"
#include "DynamicVolumeCurve.h"
#include "mex.h"

using namespace VolumeCurves;


void
mexFunction(int nlhs,mxArray *plhs[],int nrhs,const mxArray *prhs[])
{
	char mode[256];	 
	mxGetString(prhs[0],mode,(mxGetM(prhs[0]) * mxGetN(prhs[0]) * sizeof(mxChar)) + 1);
	if(strcmp(mode, "TargetClose")==0){

		int N							= (int)mxGetScalar(prhs[1]);
		int orderVolume					= (int)mxGetScalar(prhs[2]);
		double* closingAuctionVolume	= mxGetPr(prhs[3]);

		double arbitrage				= mxGetScalar(prhs[4]);
		double dLambda					= mxGetScalar(prhs[5]);
		double dKappa					= mxGetScalar(prhs[6]);
		double dGamma					= mxGetScalar(prhs[7]);

		double *market_volume			= mxGetPr(prhs[8]);
		double *volatility_curve		= mxGetPr(prhs[9]);
		double* volCurve				= mxGetPr(prhs[10]);
		
		double auction_participation	= mxGetScalar(prhs[11]);
		double auction_participation_min= mxGetScalar(prhs[12]);
		double auction_participation_max= mxGetScalar(prhs[13]);

		const double** curves = VolumeCurves::TargetCloseTrCurves::computeTradingCurves(N, orderVolume, closingAuctionVolume, 
				arbitrage, dLambda, dKappa, dGamma, 
				market_volume, volatility_curve, volCurve, 
				auction_participation, auction_participation_min, auction_participation_max);
		

		plhs[0] = mxCreateDoubleMatrix(N+1, 3, mxREAL);
		double* outArray = mxGetPr(plhs[0]);
		for(int i = 0; i < 3;i++)
		{
			for(int j = 0; j < N+1; j++)
			{
				outArray[(i*(N+1))+j] = curves[i][j];
			}
		}
	}

	if(strcmp(mode, "VwapTrend")==0){
		
		int N							= (int)mxGetScalar(prhs[1]);		
		int orderVolume					= (int)mxGetScalar(prhs[2]);
		
		double arbitrage_up				= mxGetScalar(prhs[3]);
		double arbitrage_down			= mxGetScalar(prhs[4]);
		double dLambda					= mxGetScalar(prhs[5]);
		double dKappa					= mxGetScalar(prhs[6]);
		double dGamma					= mxGetScalar(prhs[7]);
		
		double* volCurve				= mxGetPr(prhs[8]);
		double* volatility_curve		= mxGetPr(prhs[9]);		
		double dailyVolume				= mxGetScalar(prhs[10]);

		const double** curves = VwapTrendTrCurves::computeTradingCurves(N, orderVolume, arbitrage_up, arbitrage_down, dLambda, dKappa, dGamma, 
			volCurve, volatility_curve, dailyVolume);
		
		plhs[0] = mxCreateDoubleMatrix(N+1, 3, mxREAL);
		double* outArray = mxGetPr(plhs[0]);
		for(int i = 0; i < 3;i++)
		{
			for(int j = 0; j < N+1; j++)
			{
				outArray[(i*(N+1))+j] = curves[i][j];
			}
		}
	}

	if(strcmp(mode, "ImplemShortfall")==0){
		
		int N							= (int)mxGetScalar(prhs[1]);		
		int orderVolume					= (int)mxGetScalar(prhs[2]);
		
		double arbitrage_up				= mxGetScalar(prhs[3]);
		double arbitrage_down			= mxGetScalar(prhs[4]);
		double dLambda					= mxGetScalar(prhs[5]);
		double dKappa					= mxGetScalar(prhs[6]);
		double dGamma					= mxGetScalar(prhs[7]);
		
		double* volCurve				= mxGetPr(prhs[8]);
		double* volatility_curve		= mxGetPr(prhs[9]);		
		double dailyVolume				= mxGetScalar(prhs[10]);

		const double** curves = VolumeCurves::ISPortofolioTrCurves::computeTradingCurves(
			N, 
			orderVolume, 
			arbitrage_up, 
			arbitrage_down, 
			dLambda, 
			dKappa, 
			dGamma, 
			volCurve, 
			volatility_curve, 
			dailyVolume);
		
		plhs[0] = mxCreateDoubleMatrix(N+1, 3, mxREAL);
		double* outArray = mxGetPr(plhs[0]);
		for(int i = 0; i < 3;i++)
		{
			for(int j = 0; j < N+1; j++)
			{
				outArray[(i*(N+1))+j] = curves[i][j];
			}
		}
	}

	if(strcmp(mode, "VwapPortofolio")==0){
		
		
		int N							= (int)mxGetScalar(prhs[1]);		
		int K							= (int)mxGetScalar(prhs[2]);		
		int orderVolume					= (int)mxGetScalar(prhs[3]);		
		

		double arbitrage				= mxGetScalar(prhs[4]);
		
		double * pi						= mxGetPr(prhs[5]);
		double dLambda					= mxGetScalar(prhs[6]);
		double * dKappa					= mxGetPr(prhs[7]);
		double * dGamma					= mxGetPr(prhs[8]);		
		double * sigma_pr				= mxGetPr(prhs[9]);
		double * v_curve				= mxGetPr(prhs[10]);		
		double * volatility_curve		= mxGetPr(prhs[11]);		
		double * dailyVolume			= mxGetPr(prhs[12]);
		
		double ** sigma					= (double**)malloc(sizeof(double*) * K);
		double ** volCurve				= (double**)malloc(sizeof(double*) * K);
		double ** volatility			= (double**)malloc(sizeof(double*) * K);
		for(int i = 0; i < K; i++)
		{
			sigma[i] = sigma_pr + i * K;
			volCurve[i] = v_curve + i * N;
			volatility[i] = volatility_curve + i * N;
		}		

		const double** curves = VolumeCurves::VwapPortofolioTrCurves::computeTradingCurves(N, K, orderVolume, arbitrage, pi, dLambda, dKappa, dGamma, 
			sigma, volCurve, volatility, dailyVolume);
		
		plhs[0] = mxCreateDoubleMatrix(N+1, 3, mxREAL);
		double* outArray = mxGetPr(plhs[0]);
		for(int i = 0; i < 3;i++)
		{
			for(int j = 0; j < N+1; j++)
			{
				outArray[(i*(N+1))+j] = curves[i][j];
			}
		}


	}
	if(strcmp(mode, "ImplemShortfallPortofolio")==0){
		
		
		int N							= (int)mxGetScalar(prhs[1]);		
		int K							= (int)mxGetScalar(prhs[2]);		
		int orderVolume					= (int)mxGetScalar(prhs[3]);		
		

		double arbitrage_up				= mxGetScalar(prhs[4]);
		double arbitrage_down			= mxGetScalar(prhs[5]);
		
		double * pi						= mxGetPr(prhs[6]);
		double dLambda					= mxGetScalar(prhs[7]);
		double * dKappa					= mxGetPr(prhs[8]);
		double * dGamma					= mxGetPr(prhs[9]);		
		double * sigma_pr				= mxGetPr(prhs[10]);
		double * v_curve				= mxGetPr(prhs[11]);		
		double * volatility_curve		= mxGetPr(prhs[12]);		
		double * dailyVolume			= mxGetPr(prhs[13]);
		
		double ** sigma					= (double**)malloc(sizeof(double*) * K);
		double ** volCurve				= (double**)malloc(sizeof(double*) * K);
		double ** volatility			= (double**)malloc(sizeof(double*) * K);
		for(int i = 0; i < K; i++)
		{
			sigma[i] = sigma_pr + i * K;
			volCurve[i] = v_curve + i * N;
			volatility[i] = volatility_curve + i * N;
		}		

		const double** curves = VolumeCurves::ISPortofolioTrCurves::computeTradingCurves(
			N, 
			K, 
			orderVolume, 
			arbitrage_up,
			arbitrage_down, 
			pi, 
			dLambda, 
			dKappa, 
			dGamma, 
			sigma, 
			volCurve, 
			volatility, 
			dailyVolume);
		
		plhs[0] = mxCreateDoubleMatrix(N+1, 3, mxREAL);
		double* outArray = mxGetPr(plhs[0]);
		for(int i = 0; i < 3;i++)
		{
			for(int j = 0; j < N+1; j++)
			{
				outArray[(i*(N+1))+j] = curves[i][j];
			}
		}


	}
	if(strcmp(mode, "pwrapping")==0){
		
		double * v						= mxGetPr(prhs[1]);
		int size						= (int)mxGetScalar(prhs[2]);
		double pi						= (double)mxGetScalar(prhs[3]);
		double q						= (double)mxGetScalar(prhs[4]);

				
		int N = size - 1;
		double** wrap = VolumeCurves::MathModule::pwrapping(v, size, pi, q);
		
		plhs[0] = mxCreateDoubleMatrix(N+1, 3, mxREAL);
		double* outArray = mxGetPr(plhs[0]);
		for(int i = 0; i < 3;i++)
		{
			for(int j = 0; j < N+1; j++)
			{
				if (i == 0){
					outArray[(i*(N+1))+j] = v[j];
				}
				else{
					outArray[(i*(N+1))+j] = wrap[i-1][j];
				}
			}
		}


	}

	if(strcmp(mode, "CorrelMat")==0){
		double * v_pr					= mxGetPr(prhs[1]);
		int N							= (int)mxGetScalar(prhs[2]);		
		int K							= (int)mxGetScalar(prhs[3]);	
		double ** v						= (double**)malloc(sizeof(double*) * K);
	
		for(int i = 0; i < K; i++){			
			v[i] = v_pr + i * N;
		}

		double ** m = VolumeCurves::MathModule::CorrelationMatrix(v, N, K);
		
		plhs[0] = mxCreateDoubleMatrix(K, K, mxREAL);
		double* outArray = mxGetPr(plhs[0]);
		for(int i = 0; i < K;i++)
		{
			for(int j = 0; j < K; j++)
			{		
					outArray[i*K+j] = m[i][j];				
			}
		}

	}

	if(strcmp(mode, "VolumeCurveDynamic")==0){
		int N							= (int)mxGetScalar(prhs[1]);
		double * slice					= mxGetPr(prhs[2]);
		double * correl					= mxGetPr(prhs[3]);
		bool has_auction_start			= (bool)mxGetScalar(prhs[4]);
		bool has_auction_end			= (bool)mxGetScalar(prhs[5]);
		bool has_auction_mobile			= (bool)mxGetScalar(prhs[6]);
		int nb_bins_first				= (int)mxGetScalar(prhs[7]);
		int nb_bins_last				= (int)mxGetScalar(prhs[8]);
		double * histo					= mxGetPr(prhs[9]);
		int used_size					= (int)mxGetScalar(prhs[10]);
		int capacity					= (int)mxGetScalar(prhs[11]);

		VolumeCurves::DynamicVolumeCurve dyn(slice, N, correl, N, has_auction_start, has_auction_end, has_auction_mobile, nb_bins_first, nb_bins_last);
		
		int rez = dyn.get(histo, used_size, capacity);
		if (rez > 0){		
		plhs[0] = mxCreateDoubleMatrix(capacity, 1, mxREAL);
		double* outArray = mxGetPr(plhs[0]);
		
			for(int i = 0; i < capacity;i++)
			{
				outArray[i] = histo[i];
			}
		}
		else{
			plhs[0] = mxCreateDoubleMatrix(0, 1, mxREAL);
		}

	}

	if(strcmp(mode, "NormalizeVolumes") == 0){
		int NStart							= (int)mxGetScalar(prhs[1]);
		int NCurrent						= (int)mxGetScalar(prhs[2]);
		int NEnd							= (int)mxGetScalar(prhs[3]);

		int orderVolume						= (int)mxGetScalar(prhs[4]);
		double currentVolume				= (double)mxGetScalar(prhs[5]);
		
		double * dynamicVolumeCurve			= mxGetPr(prhs[6]);
		double * staticVolumeCurve			= mxGetPr(prhs[7]);

		const double * rez = VolumeCurves::VwapTrendTrCurves::computeVolumeCurveNormalization(NStart, NCurrent, NEnd, orderVolume, currentVolume, dynamicVolumeCurve, staticVolumeCurve);
		
		plhs[0] = mxCreateDoubleMatrix(-NCurrent + NEnd, 1, mxREAL);
		double* outArray = mxGetPr(plhs[0]);
		
		for(int i = 0; i < -NCurrent + NEnd;i++)
		{
				outArray[i] = rez[i];
		}
	}

	if(strcmp(mode, "ExpStaticSlices") == 0){
		
		double * slices					= mxGetPr(prhs[1]);
		int slice_size					= (int)mxGetScalar(prhs[2]);

		bool has_auction_open			= (bool)mxGetScalar(prhs[3]);
		bool has_auction_close			= (bool)mxGetScalar(prhs[4]);
		int nb_bins_first				= (int)mxGetScalar(prhs[5]);
		int nb_bins_last				= (int)mxGetScalar(prhs[6]);
		
		double * correl = new double[int((slice_size * (slice_size - 1)) / 2)];
		VolumeCurves::DynamicVolumeCurve dyn(slices, slice_size, correl, slice_size, has_auction_open, has_auction_close, 0, nb_bins_first, nb_bins_last);
		
		int size = has_auction_open + has_auction_close + (slice_size - has_auction_open - has_auction_close - 2) * 6 + nb_bins_first + nb_bins_last;
		double * rez = new double[size];
		dyn.get(rez, 0, size);		
		
		plhs[0] = mxCreateDoubleMatrix(size, 1, mxREAL);
		double* outArray = mxGetPr(plhs[0]);
		

		for(int i = 0; i < size; i++)
		{
			outArray[i] = rez[i];
		}
	}

	if (strcmp(mode, "Adjustment") == 0){
		double * slices1				= mxGetPr(prhs[1]);
		int slice_size1					= (int)mxGetScalar(prhs[2]);
		double * slices2				= mxGetPr(prhs[3]);
		int slice_size2					= (int)mxGetScalar(prhs[4]);
		
		double ** a = new double * [3];
		double ** b = new double * [3];
		for (int i = 0; i < 3; i++){
			a[i] = new double [slice_size1];
			for (int j=0; j< slice_size1; j++){
				a[i][j] = slices1[slice_size1*i+j];
			}
			b[i] = new double [slice_size2];
			for (int j=0; j < slice_size2; j++){
				b[i][j] = slices2[slice_size2*i+j];
			}
		}
		
		double ** rez = VolumeCurves::VwapTrendTrCurves::dynamicCurveAdjustment((const double ** const)a, slice_size1, (const double ** const)b, slice_size2);
		
		delete[] a;
		delete[] b; 

		plhs[0] = mxCreateDoubleMatrix(slice_size1	, 3, mxREAL);
		double* outArray = mxGetPr(plhs[0]);
		for(int i = 0; i < 3;i++)
		{
			for(int j = 0; j < slice_size1	; j++)
			{
				outArray[(i*(slice_size1))+j] = rez[i][j];
			}
		}
	}
}