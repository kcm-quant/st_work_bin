function [tr_c, w_up, w_down] = calibration_arbitrage_parameter(stock_id, trading_destination_id, date, m, varargin)
% un seul stock pour l'instant.

if isempty(varargin)
    v = 20000;
else
    v = varargin{1};
end
opt = options({'trend', 0});
opt.set('lambda', 1e-006);
opt.set('security_id', stock_id);
opt.set('from', date);
opt.set('to', date);
opt.set('trading_destination_id', trading_destination_id);
%tr_c = zeros (105, length(m) * length(v));
%w_up = zeros (105, length(m) * length(v));
%w_down = zeros (105, length(m) * length(v));

for i=1:length(m)       
    for j = 1:length(v)
        order_volume = v(j);
        arbitrage = m(i) * order_volume ;
        opt.set('arbitrage', arbitrage);
        opt.set('order-volume', order_volume);
        data = read_dataset(['gi:vwap_trend/' opt2str(opt)], 'security_id',stock_id, 'from', date, 'to', date, 'trading-destinations', trading_destination_id);
        tr_c  (:, (length(v)-1) * j + i) = data.value(:, 1);
        w_up  (:, (length(v)-1) * j + i) = data.value(:, 2);
        w_down(:, (length(v)-1) * j + i) = data.value(:, 3);
        
%         gr = gr_import_xml('build-file', 'vwap_trend_edited.xml');
%         set_params_vwap_trend(gr, opt);
%         gr.next();
%         outputs = gr.get_outputs('main');
%         a = outputs{1}.value(:, 1);
%         b = outputs{1}.value(:, 2);
%         c = outputs{1}.value(:, 3);
%         
%         diff = [tr_c - a, w_up - b, w_down - c];
        %plot(diff);
        %result(:, (i-1)*3+1:(i-1)*3+3 ) = [tr_c w_up w_down];
    end
end

% colors = {'blue', 'green', 'yellow', 'magenta', 'black'};
% figure;
% hold on
% for i=1:length(m_ag_2pc)       
%     plot(result(:, (i-1)*3+1), 'color', 'red', 'linewidth', 2);
%     plot(result(:, (i-1)*3+2), 'color', colors{i}, 'linewidth', 2);
%     plot(result(:, (i-1)*3+3), 'color', colors{i}, 'linewidth', 2);
% end
% hold off
% 
% result
% m
end