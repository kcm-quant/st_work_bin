function score = check_run_aggressivity_levels(aggr_params)

score = zeros(1, length(aggr_params.colnames));
security_id = aggr_params.info.security_id;
trading_destination_id = aggr_params.info.trading_destination_id;
date = aggr_params.info.day;

for i = 1:length(score)
   m = st_data('col', aggr_params, aggr_params.colnames{i});
   market_volume = m(strcmpi(aggr_params.rownames, 'market_volume'));
   m = m(m ~= market_volume);
   
   [tr_c w_up w_down] = calibration_arbitrage_parameter(security_id, trading_destination_id, date, m);  
   
   mean_up = repmat(mean(abs(tr_c - w_up)), size(abs(tr_c - w_up), 1), 1);
   mean_down = repmat(mean(abs(tr_c - w_down)), size(abs(tr_c - w_down), 1), 1);
   
   variance_up = repmat(var(abs(tr_c - w_up)), size(tr_c, 1), 1 );
   variance_down = repmat(var(abs(tr_c - w_down)), size(tr_c, 1), 1 );
   
   score_up   = 1 - chi2cdf(nansum( (abs(tr_c - w_up) - mean_up ) ./ variance_up, 1 ), size(tr_c, 1) );
   score_down = 1 - chi2cdf(nansum( (abs(tr_c - w_down) - mean_down ) ./ variance_down, 1 ), size(tr_c, 1) );
   score(i) = mean(score_up .* score_down);
   
end


end