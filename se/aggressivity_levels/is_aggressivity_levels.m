function out = is_aggressivity_levels(mode, varargin)
% data = is_aggressivity_levels('levels', 'security_id', 110,...
%     'trading-destinations', 4, 'window_type', 'day', 'window_width', 1,...
%     'day', '20/07/2009', 'remove', '','volume-coefficient', 20, ...
%     'criterion','final_margin', 'aggressivity_levels', 0.1, 'lambda', 1e-6);

switch mode
    
    
    case 'masterkey'
        %ne marche pas
        params = options({'security_id', 110,...
            'trading-destinations', 4,...
            'window_type', 'day',...
            'window_width', 1,...
            'day', '20/03/2009',...
            'remove', '',...
            'volume-coefficient', 20 ... typical size of an order (in ATS)
            }, varargin);
        
        security_id = params.get('security_id');
        trading_destination_id = params.get('trading-destinations');
        trading_destination_id_str = regexprep(convs('safe_str', trading_destination_id), '["~]', '');
        
        fmode = varargin{1};
        fmode = regexprep( fmode, '[ |*|\\|/|:|?|"|<|>|\|]', '_');
        
        security = get_repository('security-key', security_id);
        security = sprintf('%s_%s', security, trading_destination_id_str);
    
        out = fullfile( ...
            fmode, ...            
            security);
    
    case 'multi-criterion'
        
        criteria = {'symmetric', 'assymetric_up', 'assymetric_down', 'global_distance'};
        out = st_data('empty-init');
        
        params = options({'security_id', 110,...
            'trading-destinations', 4,...
            'window_type', 'day',...
            'window_width', 1,...
            'day', '20/03/2009',...
            'remove', '',...
            'volume-coefficient', 20 ... typical size of an order (in ATS)
            'criterion', 'global_distance',...
            'aggressivity_levels', [0.010, 0.025, 0.050, 0.075, 0.100];
            }, varargin);      
                
        levels = params.get('aggressivity_levels');
        sec_id = params.get('security_id');
        td_id  = params.get('trading-destinations');
        w_type = params.get('window_type');
        w_width= params.get('window_width');
        day    = params.get('day');
        remove = params.get('remove');
        coeff  = params.get('volume-coefficient');
        
        for i = 1:length(criteria)
            if(strcmp(criteria{i}, 'global_distance'))
                levels_specific = 2 * levels;
            else
                levels_specific = levels;
            end
            data = aggressivity_levels('levels',...
                'security_id', sec_id,...
                'trading-destinations', td_id,...
                'window_type', w_type, ...
                'window_width', w_width, ...
                'day', day, ...
                'remove', remove,...
                'volume-coefficient', coeff, ...
                'criterion', criteria{i}, ...
                'aggressivity_levels', levels_specific);
            
            out = st_data('stack', out, data);
            info_levels{i} = levels_specific;
        end                     
        out.rownames = {'symmetric' 'assymetric_up' 'assymetric_down' 'global_distance'};
        out.info.levels = info_levels;
    case 'default'
        out = st_data('init', 'title', 'IS Lambda', 'value', 1e-8, 'colnames', 'IS_LAMBDA', 'date', NaN); 
    
    case 'levels'
        
        params = options({'security_id', 110,...
            'trading-destinations', 4,...
            'window_type', 'day',...
            'window_width', 1,...
            'day', '20/03/2009',...
            'remove', '',...
            'max-volume', 20000 ... typical size of an order (in ATS)
            'criterion', 'final_margin',...
            'lambda', 1e-6,...
            'aggressivity_levels', 0.1 ...
            }, varargin);
        
        % Arbitrage parameter model considered:
        % A = m * V / ats
        % @return: calibrated constant m for the 5 correspondant levels of
        % aggresivity(volume curve width), for the stock_id considered
        %%%%% Params
        
        stock_id = params.get('security_id');
        trading_destinations = params.get('trading-destinations');        
        day = params.get('day');        
        volume_coefficient = params.get('max-volume');
        lambda = params.get('lambda');
        switch params.get('criterion')
            case 'global_distance'
                criterion = @global_distance;
            case 'assymetric_up'
                criterion = @assymetric_up;
            case 'assymetric_down'
                criterion = @assymetric_down;
            case 'symmetric'
                criterion = @symmetric;
            case 'final_margin'
                criterion = @final_margin;
            otherwise
                error('Invalid Criterion');                
        end
        
        %%%%% Init values
        %%%%%%%%%%%%%%%%%%%%%
        m_start = 0.001;
        eps_aggr = 0.005;
        
        % memory grid to compute all 5 parameters in a single sweep
        memory_m = [];
        memory_aggr = [];
        
        % Aggresivity levels
        % %%%%%%%%%%%%%%%%%%
        
        aggr_perc = params.get('aggressivity_levels');
        real_aggr_perc = aggr_perc;
        m = zeros(1, length(aggr_perc));
        
%         if(~isempty(trading_destinations))
%             ats = exec_sql('BILBO', sprintf('select avg_deal_size from market..trading_security_market_view4 where security_id=%d AND trading_destination_id=%d', stock_id,trading_destinations));
%             ats = ats{1};            
%         end
        
        volume = volume_coefficient; % TODO: ask Philippe Guillaut about regular traded volumes
        
        %%% Init vwap_trend grapher
        opt = options({'trend', 0});
        opt.set('order-volume', volume);
        opt.set('lambda', lambda);
        opt.set('security_id', stock_id);
        opt.set('from', day);
        opt.set('to', day);
        opt.set('trading_destination_id', trading_destinations);
        
        %%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%% Fill Grid with Samples%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        %% Compute value for m_start
        m_sweep = m_start;
        
        arbitrage = m_sweep * volume;
        
        opt.set('arbitrage', arbitrage);
        data = read_dataset(['gi:implementation_shortfall/' opt2str(opt)], 'security_id',stock_id, 'from', day, 'to', day, 'trading-destinations', trading_destinations);
        
        tr_c = data.value(:, 1);
        w_up = data.value(:, 2);
        w_down = data.value(:, 3);
        
        d = criterion(tr_c, w_up, w_down);      
        
        memory_m(end+1) = m_sweep;
        memory_aggr(end+1) = d;
        market_volume = data.info.market_volume;
        
        %% Sample until the superior limit is reached
        
        m_sweep = 2 * m_start;
        while (max(memory_aggr) < max(aggr_perc))
            
            arbitrage = m_sweep * volume;
            
            opt.set('arbitrage', arbitrage);
            data = read_dataset(['gi:implementation_shortfall/' opt2str(opt)], 'security_id',stock_id, 'from', day, 'to', day, 'trading-destinations', trading_destinations);
            
            tr_c = data.value(:, 1);
            w_up = data.value(:, 2);
            w_down = data.value(:, 3);
            d = criterion(tr_c, w_up, w_down);
            
            memory_m(end+1) = m_sweep;
            memory_aggr(end+1) = d;
            memory_m = sort(memory_m);
            memory_aggr = sort(memory_aggr);
            
            m_sweep = 2 * m_sweep;
        end
        
        %% Sample until the inferior limit is reached
        m_sweep = m_start / 2;
        while (min(memory_aggr) > min(aggr_perc))
            
            arbitrage = m_sweep * volume;
            
            opt.set('arbitrage', arbitrage);
            data = read_dataset(['gi:implementation_shortfall/' opt2str(opt)], 'security_id',stock_id, 'from', day, 'to', day, 'trading-destinations', trading_destinations);
            
            tr_c = data.value(:, 1);
            w_up = data.value(:, 2);
            w_down = data.value(:, 3);
            d = criterion(tr_c, w_up, w_down);
            
            memory_m(end+1) = m_sweep;
            memory_aggr(end+1) = d;
            memory_m = sort(memory_m);
            memory_aggr = sort(memory_aggr);
            
            m_sweep = 1/2 * m_sweep;
        end
        
        %%  Search for the 5 aggresivity levels
        for i = 1:length(aggr_perc)
            mmin = memory_m(find(memory_aggr < aggr_perc(i), 1, 'last'));
            mmax = memory_m(find(memory_aggr > aggr_perc(i), 1, 'first'));
            reccursive_binary_search(mmin, mmax, i, criterion);
        end
        
        %% build output
        stock_name = get_repository('security-key', stock_id);        
        title = sprintf('Aggressivity Level Parameter for %s, %s-%s', stock_name, day, day); 
        colnames = {};
        for i = 1:length(aggr_perc)
            colnames{i} = num2str(100 * aggr_perc(i));
        end
        out = st_data('init', 'title', title, 'value', m, 'colnames', colnames, 'date', NaN);
        out = st_data('add-col', out, market_volume, 'MARKET_VOLUME');
end
%% Bynary search recursive function
    function reccursive_binary_search(mmin, mmax, i, criterion)
        m_sweep = (mmin + mmax) / 2;
        arbitrage = m_sweep * volume;
    
        opt.set('arbitrage', arbitrage);        
        data = read_dataset(['gi:implementation_shortfall/' opt2str(opt)], 'security_id',stock_id, 'from', day, 'to', day, 'trading-destinations', trading_destinations);
        tr_c = data.value(:, 1);
        w_up = data.value(:, 2);
        w_down = data.value(:, 3);
        d = criterion(tr_c, w_up, w_down);
        
        memory_m(end+1) = m_sweep;
        memory_aggr(end+1) = d;
        memory_m = sort(memory_m);
        memory_aggr = sort(memory_aggr);
        
        st_log(sprintf('Distance to target: %f\n', d - aggr_perc(i)));
        if(abs(d - aggr_perc(i)) < eps_aggr)
            m(i) = m_sweep;
            real_aggr_perc(i) = d;
        else
            if(d - aggr_perc(i) > 0)
                st_log(sprintf('New Search Interval:[%f %f]\n', mmin, m_sweep));
                reccursive_binary_search(mmin, m_sweep, i, criterion);
            else      
                st_log(sprintf('New Search Interval:[%f %f]\n', m_sweep, mmax));
                reccursive_binary_search(m_sweep, mmax, i, criterion);
            end
        end
    end

%% Criterion functions
    function dist = global_distance(middle, up, down)
        dist = mean ( abs (up - down) );
    end

    function dist = assymetric_up (middle, up, down)
        dist = mean(abs(middle - up));
    end

    function dist = assymetric_down(middle, up, down)
        dist = mean(abs(middle - down));
    end
    function dist = symmetric(middle, up, down)
        dist = mean( [ mean(abs(middle - up)), mean(abs(middle - down)) ] );
    end
    function dist = final_margin (middle, up, down)
        dist = up(find(middle < 0.001, 1));
    end
end