function score = se_check_aggressivity_levels(security_id, trading_destination_id, window_type, window_width, as_of_day, data) 

idx_market_volume = strcmp(data.rownames, 'market_volume');
m = data.value(~idx_market_volume, :);
score = zeros(1, length(data.colnames));
[tr_c, w_up, w_down] = calibration_arbitrage_parameter(security_id, trading_destination_id, as_of_day, m);

for i = 1:length(score)
    mean_up = repmat(mean(abs(tr_c - w_up)), size(abs(tr_c - w_up), 1), 1);
    mean_down = repmat(mean(abs(tr_c - w_down)), size(abs(tr_c - w_down), 1), 1);
    
    variance_up = repmat(var(abs(tr_c - w_up)), size(tr_c, 1), 1 );
    variance_down = repmat(var(abs(tr_c - w_down)), size(tr_c, 1), 1 );
    
    score_up   = 1 - chi2cdf(nansum( (abs(tr_c - w_up) - mean_up ) ./ variance_up, 1 ), size(tr_c, 1) );
    score_down = 1 - chi2cdf(nansum( (abs(tr_c - w_down) - mean_down ) ./ variance_down, 1 ), size(tr_c, 1) );
    score(i) = mean(score_up .* score_down);
end


end