function out = cmse2mat_vec(data_se)
%CMSE2AT_VEC : fonction qui permmet de reconstruire � partir des donn�es extraites du SE
%la matrice variance/covariance et le vecteur moyenne "BRUTS"

% see also :get_param_mode

if (isfield( data_se, 'rownames'))
    params_name =  data_se.rownames;
    params_value =  data_se.value;
else
   error('func: <cmse2mat_vec>::Format of Inputs data is not correct, it must be a data structure ')
end
idx_m = cellfun(@(str)~isempty(strmatch('mean',str)), params_name,'uni',false);
idx_v = cellfun(@(str)~isempty(strmatch('vcov',str)), params_name,'uni',false);

vect = params_value([idx_m{:}]');
mat = params_value([idx_v{:}]');

mat_o = [];
nb_prm = size(vect,1);

for i = 1 : nb_prm
    mat_o = [mat_o mat((i-1)*nb_prm+1:i*nb_prm) ];    
end

out = [mat_o  vect];

end