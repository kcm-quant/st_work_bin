function out = se_check_capture_micros(security_id, trading_destination_id, window_type, window_width, as_of_day, data_c, varargin_)

% SE_CHECK_CAPTURE_MICRO
% example
%  data = se_run_capture_micros(110, { 'MAIN' }, 'day', 14,'09/06/2008', [], [])
%  res = se_check_capture_micros(110, { 'MAIN' }, 'day', 14, datestr( today-12, 'dd/mm/yyyy'),data, [])

% author   : 'dcroize@cheuvreux.com'
% reviewer :
% version  : '1'
% date     : '29/01/2009'
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bin_duration = datenum(0,0,0,0,5,0); % constante universelle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
opt = options({'step:time', bin_duration, 'window:time', bin_duration});

data = read_dataset(['gi:basic_indicator/' opt2str(opt)], ...
    'last_formula',...
    '[ {volume}, {vwap}, {nb_trades}, {volume_sell}, {vwap_sell}, {nb_sell}, {vwas}, {vol_GK}]',...
    'where_formula', '~{auction}',...
    'security_id', security_id, 'from', as_of_day, 'to', as_of_day,...
    'trading-destinations', trading_destination_id);

%<Récuperation du vecteur et de la matrice variance / covariance
mat_vec = cmse2mat_vec(data_c);

%<Construction de l'objet
density = capture_microstructure(mat_vec, 'raw:data',0);

%>
%fonction se_check_quality
check_base = [];
out = test_gaussian_log_data(data ,check_base, 'check:quality',density,'raw:data',0);

end