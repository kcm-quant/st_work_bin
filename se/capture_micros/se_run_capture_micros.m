
function out = se_run_capture_micros(security_id, trading_destination_id, window_type, window_width, as_of_date, ...
    dt_remove, varargin_)
% SE_RUN_CAPTURE_MICRO Run de l'estimateur courbe de spread
% se_run_capture_micros(110, { 'MAIN' }, 'day', 30, '09/06/2008', [], [])

% author   : 'dcroize@cheuvreux.com'
% reviewer : 
% version  : '1'
% date     : '16/02/2009'
%
% 
out = read_dataset(sprintf('gi:capture_micros/window:char%s"%s|%d', char(167),window_type, window_width ), ...
    'security_id', security_id, ...
    'trading-destinations', trading_destination_id, ...
    'from', as_of_date,...
    'to', as_of_date);

out.value(isnan(out.value)) = 0;

if st_data('isempty-nl', out)
    error('No data for this job!')
end

out.colnames = {'USUAL DAY'};

end

