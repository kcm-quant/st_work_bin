function multi_scv_cfv(index_name, td_id, width)
%
% multi_scv_cfv([])
% multi_scv_cfv([], 4)
% multi_scv_cfv([], [], 180)
% multi_scv_cfv([], 4, 180)
% multi_scv_cfv('SBF hors CAC')
if isempty(index_name)
    index_name = 'CAC40';
end
if nargin < 2
    td_id = [];
end
if nargin < 3 || isempty(width)
    width = 360;
end

if strcmp(index_name, 'SBF hors CAC')
    sec_id = setdiff(get_repository('index-comp', 'SBF120', 'recent_date_constraint', false),...
        get_repository('index-comp', 'CAC40', 'recent_date_constraint', false));
else
    sec_id = get_repository('index-comp', index_name, 'recent_date_constraint', false);
end

% sec_id = sec_id(1:2);
nb_days4out_of_sample = 10;
nb_test = 18;
dts_num = datenum('23/01/2009', 'dd/mm/yyyy');
dts = {};
for i = 1 : nb_test
    dts{i} = datestr(dts_num-nb_days4out_of_sample*(nb_test-i), 'dd/mm/yyyy');
end
% dts = {'23/01/2008', '23/02/2008', '23/03/2008', '23/04/2008', '23/05/2008',...
%         '23/06/2008', '23/07/2008', '23/08/2008', '23/09/2008', '23/10/2008', '23/11/2008', '23/12/2008', '23/01/2009'};
% dts = {'23/08/2008', '23/09/2008', '23/10/2008', '23/11/2008', '23/12/2008', '23/01/2009'};
% dts = {'23/10/2008', '23/11/2008', '23/12/2008', '23/01/2009'};

fichier = ['report_forecast_cav_' index_name '_' num2str(td_id) '_' num2str(width)];
latex('header', fichier, 'title', 'Rapport sur les algorithmes de pr�vision du volume de l''ench�re de cloture');
ric = get_ric_from_sec_id(sec_id);
m_qr = NaN(length(sec_id), 4);
m_qe = NaN(length(sec_id), 4);
latex('section', fichier, 'Rapport detaille titre par titre');
col_header = {'log linear', 'trunc log linear', 'med prop', 'med'};
lines_header1 = cat(1,dts(2:end)', 'mean');
for i = 1 : length(sec_id)
    latex('subsection', fichier, ric{i});
    q_o = scv_cfv(sec_id(i), td_id, width, dts);
    qr = q_o(:, 1:4);
    m_qr(i, :) = mean(qr(all(isfinite(qr), 2), :));
    qe = q_o(:, 5:8); 
%     rel_qe = bsxfun(@times, qe, 1./qe(:, end));
    m_qe(i, :) = mean(qe(all(isfinite(qe), 2), :));
    latex('array', fichier, [qr; m_qr(i, :)], ...
        'col_header', col_header, 'bold_matrix', 'min',...
        'linesHeader', lines_header1, 'maxcols', 6);
    latex('array', fichier, [qe; m_qe(i, :)], ...
        'col_header', col_header, 'bold_matrix', 'min',...
        'linesHeader', lines_header1, 'maxcols', 6);
end

lines_header2 = cat(1,ric, 'mean');
% rel_m_qe = bsxfun(@times, m_qe, 1./m_qe(:, end));
latex('section', fichier, 'Rapport agr�g�');
latex('array', fichier, [m_qr;mean(m_qr)], ...
        'col_header', col_header, 'bold_matrix', 'min',...
        'linesHeader', lines_header2, 'maxcols', 6);
    latex('array', fichier, [m_qe;mean(m_qe)], ...
        'col_header', col_header, 'bold_matrix', 'min',...
        'linesHeader', lines_header2, 'maxcols', 6);
latex('end', fichier);
latex('compile', fichier);

