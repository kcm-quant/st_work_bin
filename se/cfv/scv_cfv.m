function q_o = scv_cfv(sec_id, td_id, width, dts)
% q_o = scv_cfv(110, 4)
% q_o = scv_cfv(23, 4)
% q_o = scv_cfv(128533, 4)
if nargin < 3
    width = 360;
end
if nargin < 4
    dts = {'01/01/2008', '01/02/2008', '01/03/2008', '01/04/2008', '01/05/2008',...
        '01/06/2008', '01/07/2008', '01/08/2008', '01/09/2008', '01/10/2008', '01/11/2008', '01/12/2008'};
end
q_o = NaN(length(dts)-1, 8);
st_log('$no_log$');
var4debug = [];
for i = 1 : (length(dts)-1)
    try
        tmp = se_run_cfv(sec_id, td_id, 'day', width, dts{i},[],[]);
        [tmp, q_o(i, :)] = se_check_cfv(sec_id, td_id, 'day', datenum(dts{i+1}, 'dd/mm/yyyy')-datenum(dts{i}, 'dd/mm/yyyy'), dts{i+1},tmp);
    catch ME
        if ~strcmp(ME.identifier, 'se_cfv_get_data:empty_data') ...
                && ~strcmp(ME.identifier, 'se_run_cfv:checking_data_length') ...
                && ~strcmp(ME.identifier, 'se_efq_cfv:no_data')
            rethrow(ME);
        end
    end
end
st_log('$out$', '');