function [vca, vbca, l_X, l_Y, u_days] = se_cfv_get_data(security_id, trading_destination_id, window_type, window_width, as_of_date)
% [vca, vbca, l_X, l_Y] = se_cfv_get_data(110, 4, 'day', 360, '01/11/2008')
% [vca, vbca, l_X, l_Y] = se_cfv_get_data(110, 4, 'day', 1, '17/11/2008')
% 
% [vca, vbca, l_X, l_Y] = se_cfv_get_data(6146, [], 'day', 360, '16/01/2009')
% [vca, vbca, l_X, l_Y] = se_cfv_get_data(6146, 8, 'day', 360, '16/01/2009')

[from, to] = se_window_spec2from_to(window_type, window_width, as_of_date);

optchar= opt2str(options({'step:time', 1, 'window:time', 1, ...
    'source:char', 'tick4cfv', 'bins4stop:b', false, ...
    't_acc:time', 0.05}));

volume = read_dataset(['gi:basic_indicator/' optchar],...
    'security_id', security_id, 'from', from, 'to', to, 'trading-destinations', trading_destination_id, ...
    'last_formula', '[{volume},{closing_auction}]');

if st_data('isempty-nl', volume)
    error('se_cfv_get_data:empty_data', 'NO_DATA_FILTER:No data');
end

% < Excluding closing auction : the variable to be forecasted
[vca, idx] = st_data('where', volume, '{closing_auction}');
u_days = unique(floor(vca.date));
vca = vca.value(:, 1);
volume = st_data('keep', st_data('from-idx', volume, ~idx), 'volume');
% >

% > Summing volume before closing auction : the variabler to use for forecasting
days = floor(volume.date);
u_days = intersect(unique(floor(volume.date)), u_days);
vbca = NaN(length(u_days), 1);
for i = 1 : length(u_days)
    vbca(i) = sum(volume.value(u_days(i) == days));
end
% <

% < Excluding zeros for log
if any(vca==0) || any(~isfinite(vca)) || any(any(vbca==0)) || any(any(~isfinite(vbca)))
    idx2del = (vca==0) | ~isfinite(vca) | any(vbca==0, 2) | any(~isfinite(vbca), 2);
    vca(idx2del) = [];
    vbca(idx2del, :) = [];
end
% >

% < log of variables
l_Y = log(vca);
l_X = log(vbca);
l_X = [l_X ones(size(vca, 1), 1)];
% >


% [from, to] = se_window_spec2from_to(window_type, window_width, as_of_date);
% volume = get_tick('tick4cfv',...
%     'security_id', security_id, 'from', from, 'to', to, 'trading-destinations', trading_destination_id, ...
%     'last_formula', '[{volume},{closing_auction}]');
% 
% % < Excluding closing auction : the variable to be forecasted
% [volume_auc, idx] = st_data('where', volume, '{closing_auction}');
% volume_auc = st_data('keep', volume_auc, 'volume');
% days = floor(volume_auc.date);
% u_days = unique(floor(volume_auc.date));
% vca = NaN(length(u_days), 1);
% for i = 1 : length(u_days)
%     vca(i) = sum(volume_auc.value(u_days(i) == days));
% end
% volume = st_data('from-idx', volume, ~idx);
% % >
% 
% % > Summing volume before closing auction : the variabler to use for forecasting
% volume = st_data('keep', volume, 'volume');
% days = floor(volume.date);
% u_days = intersect(unique(floor(volume.date)), u_days);
% vbca = NaN(length(u_days), 1);
% for i = 1 : length(u_days)
%     vbca(i) = sum(volume.value(u_days(i) == days));
% end
% % <
% 
% % < Excluding zeros for log
% if any(vca==0) || any(~isfinite(vca)) || any(any(vbca==0)) || any(any(~isfinite(vbca)))
%     idx2del = (vca==0) | ~isfinite(vca) | any(vbca==0, 2) | any(~isfinite(vbca), 2);
%     vca(idx2del) = [];
%     vbca(idx2del, :) = [];
% end
% % >
% 
% % < log of variables
% l_Y = log(vca);
% l_X = log(vbca);
% l_X = [l_X ones(size(vca, 1), 1)];