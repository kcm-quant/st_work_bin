function [q, other_q] = se_check_cfv(security_id, trading_destination_id, window_type, window_width, as_of_date, data)
% data = se_run_cfv(110, 4, 'day', 180, '01/10/2008',[],[])
% [q, other_q] = se_check_cfv(110, 4, 'day', 30, '01/11/2008',data)
% q
% other_q(1:4)
% other_q(5:8)


% [vca, vbca] = temporary_bufferize('se_cfv_get_data', {security_id, trading_destination_id, ...
%     window_type, window_width, as_of_date}, 'buff_errors');

[vca, vbca] = se_cfv_get_data(security_id, trading_destination_id, ...
    window_type, window_width, as_of_date);

[q, other_q] = se_efq_cfv(data, vca, vbca);

q = 2/(1+exp(q));