function [q, other_q] = se_efq_cfv(data, vca, vbca)
f = (vbca.^(data.value(1)))*data.value(2);

f = repmat(f, 1, 4);
idx_gt = f(:, 1)./vbca > data.value(5);
f(idx_gt, 2) = data.value(5).*vbca(idx_gt);
idx_lt = f(:, 1)./vbca < data.value(3);
f(idx_lt, 2) = data.value(3).*vbca(idx_lt);

f(:, 3) = data.value(4).*vbca;

f(:, 4) = data.value(6);

other_q = [mean(abs(bsxfun(@minus, f, vca)))/mean(abs(vca-median(vca))) sqrt(mean(bsxfun(@minus, f, vca).^2))/std(vca)];

if size(other_q, 2) ~= 8
    error('se_efq_cfv:no_data', 'NO_DATA_FILTER:Not enough data');
end

q = other_q(2);
