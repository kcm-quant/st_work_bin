function data = se_run_cfv(security_id, trading_destination_id, window_type, window_width, as_of_date,dt_remove,varargin_)
% data = se_run_cfv(110, 4, 'day', 180, '01/10/2008',[],[])
% data.value(1:5)
% data.info.run_quality
% data = se_run_cfv(110, [], 'day', 180, '01/10/2008',[],[])
% data.value(1:5)
% data.info.run_quality

% data.info.run_quality
% data = se_run_cfv(26, [], 'day', 180, '01/10/2008',[],[])
% data.value
% data.info.run_quality
% data = se_run_cfv(257, [], 'day', 180, '01/10/2008',[],[])
% data.value
% data.info.run_quality
%
% data = se_run_cfv(6855, [], 'day', 360, '16/01/2008',[],[])
% data.value
% data.info.run_quality
%
%
% 

% if isempty(trading_destination_id)
%     error('se_run_cfv:checking_args', 'I refuse to work with an unspecified trading_destination');
% end

% [vca, vbca, l_X, l_Y] = temporary_bufferize('se_cfv_get_data', {security_id, trading_destination_id,...
%     window_type, window_width, as_of_date}, 'buff_errors');
[vca, vbca, l_X, l_Y] = se_cfv_get_data(security_id, trading_destination_id,...
    window_type, window_width, as_of_date);

if length(vca) < 90
    error('se_run_cfv:checking_data_length', 'NOT_ENOUGH_DATA: Not enough data for this security to work on it');
end

s_vca = vca(end-59:end);
s_vbca = vbca(end-59:end);
q_prop = [0.2;0.5;0.8];

params = [((l_X'*l_X)^-1)*l_X'*l_Y; quantile(s_vca./s_vbca, q_prop); nanmedian(s_vca)];
params(2) = exp(params(2));

data = st_data('init', 'value', params, ...
    'date', repmat(datenum(as_of_date, 'dd/mm/yyyy'), length(params), 1),...
    'rownames', cat(2, {'exponent', 'multiplier'}, tokenize(sprintf('q_%g_prop;', 100*q_prop)), {'med_cfv'}), 'title', 'cfv', ...
    'colnames', {'context1'}, 'info', []);

data.info.run_quality = se_efq_cfv(data, vca, vbca);

data.info.run_quality = 2/(1+exp(data.info.run_quality));