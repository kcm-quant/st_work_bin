function self= AGranker(lenGenCode)
% eModel2 habille cet objet avec getData,Jacknife et decoration
%
% STOPPED AT 110

self= [];
self.classe = 'AGranker.m';
self.getData_test= @getData_test;
self.rankContext= @rankContext;
 
    function self= rankContext(self)
        % INIT
         ga = gaNews( 'dna_length', lenGenCode, 'pop_size', 100, 'r_xover', 0.15, 'r_mutation', @(t)[0.03*(t<301)+0.6*(t>300)/sqrt(t-300)*sqrt(2)] );
         ga.verbosity = -1;
         %ga.dataset( 'pattern', g.self.news.value, 'signal', g.self.volume.value, 'colnames', {g.self.news.colnames} )
         ga.dataset( 'pattern', self.news.value, 'signal', self.volume.value, 'colnames', {self.news.colnames} );
         ga.set_ref('mean');
         ga.set_fit('L2');
         ga.set_aggregation('mean');
         ga.init();

         ga.get('population');
         fit = ga.fit(ga.population{1});
         [pop, fit] = ga.pressure( fit);
         pop = ga.xover( pop, fit);
         pop = ga.mutate( pop);
         [pop, fit] = ga.evol_one_step();
         ga.verbosity = -1;

         self.ga= ga;
        
         % LEARN
         ga.evol('max_iter',1200) 
         % mode test
         %self.ga.evol('max_iter',300)         
         codegen =  mv_mode(self.ga.population{end});
         self.ranks=[];self.note=[];
         
         % COMPATIBILITE
         % TODO cr�er un buffer au m�me format que type= ranks
         % rendre les options (estim, seuil etc) non buggant
         % Pr�voir un mode quelque part pour lancer cette �tape depuis indexProcess

         % pourquoi a-t'on des 0 dans le code g�n�tique ????
         [~,s]=sort(codegen,'descend');
         nz= sum(codegen>0);
         self.ranks= s(1:nz);
         
%          for k=1:length(codegen)
%              if codegen(k)> 0
%                 self.ranks(codegen(k)) = k;        
%              end
%          end
         Nctxt= length(self.ranks);
         for k=1:Nctxt
             % on divise par 2 pour etre sur de ts les prendre une fois
             % commencer � 0, des NaN apparaissent dans jacknife si ts les
             % ctxt< seuils !
             self.note(k) = (k-1)/Nctxt/2;
         end
         self.indiceEffectif=self.news.value(:,self.ranks);
         self.indiceEffectif= cumsum(self.indiceEffectif,2);
         self.indiceEffectif(self.indiceEffectif> 1)= 0;
         
    end
 
 %ga.plot();
 %ga.plot('DNA', -(1:1200));
 %ga.plot('snapshot', 125);

%[ga, g] = gaNews.main( 'dna_length', 8, 'population_size', 100, 'max_iter', 12000)

% mode ne marche pas elle va prendre le mode sur chaque colonne !
 %codegen = mode(ga.population{end})
 

 
 
end

function self= getData_test(self,ID,modeFreq)    
    % eModelPool est responsable des data
    if nargin< 3;modeFreq= 'intraday'; end    
    p= eModelPool();
    p.mode= modeFreq;
    p= p.inidata(p,ID,modeFreq);
    self.news= p.news;
    self.volume= p.volume;
    clear p;
    self.IDstock= ID;
end

%% Mode multivari�

function mode= mv_mode(x)
% multivariate mode, options a des clefs string !
x= cellstr(num2str(x));
count= options();
cellfun( @(t) updateCount(t,count) ,x);

vs= count.get();
occurs= vs{2:2:end};
[~,i]= max(occurs);
mode= vs{1+2*(i-1)};

% il arrive � reconstituer un vecteur grace aux espaces � partir du string!!!
mode=str2num(mode) ;

end

function updateCount(x,o)
try;    v= o.get(x);
catch;    v=0;
end
o.set(x,v+1);
end

%%
function main()
ca= {'Npoint',10,'seuil',1,'noter','ag','estimateur','median','sbreak',0 ,'dimG',5};
ag= eModel2(ca{:});

ag= ag.getData_test(ag,2);
  
ag= ag.rankContext(ag)
sum(ag.indiceEffectif)
 any(cumprod(ag.indiceEffectif)) % seule la 1ere colonne a le droit a non 0
end




 
