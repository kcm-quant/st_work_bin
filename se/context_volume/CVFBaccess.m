function self= CVFBaccess()
% Contextual Volume Forecast Base Access
%
% Updating Ranks needs to destroy existing info, and enters each context !
%
% TODO :lock !!!
%

assert(0,'deprecated');
global st_version
bq = st_version.bases.quant;

TR= sprintf('%s..context_ranking',bq);
TM= sprintf('%s..learningmodel',bq);

tempTR= sprintf('%s..temp_context_ranking',bq);
tempTM= sprintf('%s..temp_learningmodel',bq);

minfo= 'modeleinfo';
sid= 'security_id';
cid= 'context_id';
r= 'rankings';
m= 'model';
RT= 'tablename';
Dt= 'Date';
integer= 'integer';
model= 'model';
V= ',';
TD= 'trading_destination';
EST= 'estimator_id';
% Rankings with fields= security_id, context_id, rankings

function R= del_rankings(sec_id)
R= {{'DELETE FROM',TR ,'WHERE', sid, '=', sec_id},...
    {'DELETE FROM',TM ,'WHERE', sid, '=', sec_id}};        
end

% function R=set_rankings(sec_id,data)
% R= {'UPDATE',RT ,'SET', r, '=', data,...
%     'WHERE' sid '=' 'Los Angeles',...
%     'AND', Dt, '=',  '08-Jan-1999' };
% end

function R=set_rankings(sec_id,data,ligne)
if nargin== 2
    K= size(data.num,1);
    R={};
    for k=1:K
       R{end+1}= set_rankings(sec_id,data,k);        
    end    
    R{end+1}= {'insert into' TM '(' model ')' 'values(' stringer(data.model) ')' };    
else
    d= data.num(ligne,:);
    R={'insert into' TR '(' sid V cid V r V TD V EST ')' 'values(' d(1) V d(2) V d(3) V 'NULL' V 1 ')' };    
end
end

self= [];
self.set_rankings= joiner(@set_rankings);
self.create= joiner(@create);
self.get_rankings= joiner(@get_rankings);
self.set_predictor= joiner(@set_predictor);
self.del_rankings= joiner(@del_rankings);
self.exec= @exec;
%self.get_predictor= get_predictorlearn;

end

function z= exec(r)
    z= {};
    G= 'QUANT';
    N= length(r)
    for n=1:N
        z{end+1}= generic_structure_request(G,r{n});
    end    
end

function s= join(c,sep)
if nargin<2;sep= '';end
s=[];
for n=1:length(c)
   s= [s num2str(c{n}) sep]; 
end
s=s(1:end-1);
end

function f= joiner(fun)
    f= @(varargin) cellfun(@(x) join(x,' '), fun(varargin{:}),'uni',0 );
end

function s=stringer(s)
s=['''' s ''''];
end


function get_rankings(sec_id)
end



function set_predictor(sec_id,data)
end
function get_predictor(sec_id)
end
%%

function real_script()
security_id=6069;
[D,base,b]= contexte_learning(security_id);
a= CVFBaccess();
r= a.create();r{:}
a.exec(r(4));
a.exec(r);

data=[];data.num= [6069,15,110;6069,3,120;6069,100,130];
data.model= 'model';
r= a.del_rankings(security_id);r{:}
r= a.set_rankings(security_id,data); r{:}

end

function TEST()    
data= [6069,15,110,0;6069,3,120,0;6069,100,130,0];
a= CVFBaccess()
r= a.create();r{:}
r= a.del_rankings(2);r{:}
r= a.set_rankings(2,b); r{:}

ip= indexProcess('CAC40');
ip0= indexProcess('DAX');

[D,base,b]= contexte_learning([ip.comp]);
b= load('U:\Matlab\pipo_base.mat');
b= b.b;
base.filter= @(id) b.num(base.num(:,1) == sec_id,:);
base.filter(6069)
%base.filter= @(id) b.num(find(cellfun(@(x) x{1},base0.model) == sec_id),:)
end


