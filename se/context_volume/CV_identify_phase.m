function info= CV_identify_phase(identifiant)

if ischar(identifiant{2}) && strcmp(identifiant{2},'MAIN')
    identifiant{2}=[];
end

info=[];
info.step='base';
info.substep= 100;

%% Buffers
tbx = newsProjectTbx();
%tbx.FILEconfig.folder(identifiant,'buffer')

IO= fileToolbox(tbx.FILEconfig.folder(identifiant,'buffer'));
l= IO.parse('stockID',identifiant{1},'from',identifiant{3}(1),'to',identifiant{3}(2),'type','buffer','freq','full');
if isempty(l)
    info.step= 'volume';
    info.substep=0 ;
    return
end

IO= fileToolbox(tbx.FILEconfig.folder(identifiant,'buffer'));
l= IO.parse('stockID',identifiant{1},'from',identifiant{3}(1),'to',identifiant{3}(2),'type','buffer','freq','learning');
if isempty(l)
    info.step= 'volume';
    info.substep= 0.5;
    return
end

IO= fileToolbox(tbx.FILEconfig.folder(identifiant,'news'));
l= IO.parse('stockID',identifiant{1},'type','news');
if isempty(l)
    info.step= 'news';
    info.substep=0 ;
    return
end

%% Learning
IO= fileToolbox(tbx.FILEconfig.folder(identifiant,'nplearn'));

%done= cellfun(@(x) str2opt(x,'~','-') ,l );

t= eModelPool();
p= t.definePaveur('intraday');
pp = options(p{:});
n_estim = length(pp.get( 'estimateur')); % estimators names
rankers= cellfun(@(x) options(x), p);
rankers= arrayfun(@(x) x.get('noter'), rankers,'uni',0);
rankers=[rankers{:}];
n_model= sum(cellfun( @(x) prod(cellfun(@(y) length(y) , x(2:2:end))),p));

for k= 1:length(rankers)
    l= IO.parse('type','ranks','noter',rankers{k});
    if isempty(l)
        info.step= 'ranks';
        info.substep= k/length(rankers);
        return
    end
end


l= IO.parse('type','jack','noter','None');
if( length(l)< n_estim)
    info.step= 'jack0';
    info.substep= length(l)/ n_estim;
    return
end

l= IO.parse('type','jack');
if( length(l)< (n_model +n_estim))
    info.step= 'jack';
    info.substep= length(l)/ (n_model +n_estim);
    return
end

l= IO.parse('type','error','inhibition',0);
if( length(l)< (n_model +n_estim))
    info.step= 'error';
    info.substep= length(l)/ (n_model +n_estim);
    return
end

l= IO.parse('type','MRANK','inhibition',0);
if isempty(l)
    info.step= 'MRANK';
    info.substep=0 ;
    return
end

l= IO.parse('type','error','inhibition',1);
if( length(l)< (n_model +n_estim))
    info.step= 'inhibition';
    info.substep= length(l)/ (n_model +n_estim);
    return
end

l= IO.parse('type','MRANK','inhibition',1);
if isempty(l)
    info.step= 'IMRANK';info.substep=0 ;
    return
end

l= IO.parse('type','FINALmodel','inhibition',1);
if isempty(l)
    info.step= 'nonews';info.substep=0 ;
    return
end


function o= str2opt(str,sep1,sep2)
l= tokenize(str,sep1);
x= cellfun(@(x) tokenize(x,sep2) ,l, 'uni',0);
dims= cellfun(@(x) length(x), x);
x(dims < 2) = []; % retire filesave et .mat
o= options([x{:}]);




