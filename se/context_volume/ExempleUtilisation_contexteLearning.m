global st_version
quant = st_version.bases.quant;

%% EXEMPLE UTILISATION contexte learning
% TODO: plus besoin de renseigner buffer-dates (<- := dates)
%
%
%

%% -1 / Apprentissage local test
eModelPool(1);
tbx= loadEvent(); tbx.getEvent('test_mode',1);

dates= [datenum('01/01/2008','dd/mm/yyyy'), datenum('01/04/2012','dd/mm/yyyy')]
a= {'security_id',6249,'trading_destination',[],...
    'dates',dates,... % 'buffer_dates', dates,...
    'tresh_inhibit',1};%,'steps','ranks'};
L= context_volume_learning(a{:});

[failed,base]= L.learn();


%% 0/ Lancer l'apprentissage en local
eModelPool(0);
tbx= loadEvent(); tbx.getEvent('test_mode',[]);

dates= [datenum('01/01/2008','dd/mm/yyyy'), datenum('01/02/2008','dd/mm/yyyy')]
a= {'security_id',2,'trading_destination',[],'old-data',0,...
    'dates',dates, 'buffer_dates', dates,...
    'folder','test','continue',1,'tresh_inhibit',0.9};
L= context_volume_learning(a{:});

[failed,base]= L.learn();

%% 1/ Lancer l'apprentissage en distribu?, sans mise en base

sec_ids = get_index_comp('CAC40', 'recent_date_constraint', false);

jm = findResource('scheduler','type','local');
job = jm.createJob();
for i = 1 : length(sec_ids)
     job.createTask('learning_phase_fct_wrap', 2, {'dts', 'security_id',sec_ids(i),'trading_destination', {}, ...
      'dates', [datenum(2005,1,1,0,0,0), datenum(2012, 3,8,0,0,0)], ...
      'tresh_inhibit',0.95,'thres_nobs',300,...
'buffer_dates', [datenum(2005,1,1,0,0,0), datenum(2012, 3,8,0,0,0)],'old-data', 0,...
});
%'steps',{'volume','news','ranks','jack0','jack','error','MRANK','inhibition','IMRANK','nonews'}

end
% l'etape base n'ecrit pas en base !

job.submit();

waitForState(job.Tasks(1), 'finished');

job.Tasks(1).error

%% 1 bis/ avec decoration
tbx= distribute_fun('tbx');
f0= tbx.compose(@(L)L.learn(),@context_volume_learning); %[failed,base]= f0(a)
fd= distribute_fun(f0);
failed={};
base={};
for i = 1 : length(sec_ids)
    a= {'security_id',sec_ids(i),'trading_destination', {}, ...
        'dates', [datenum(2005,1,1,0,0,0), datenum(2012, 3,8,0,0,0)], ...
     	'tresh_inhibit',0.95,'thres_nobs',300,...
        'buffer_dates', [datenum(2005,1,1,0,0,0), datenum(2012, 3,8,0,0,0)],'old-data', 0,...
        };
    [failed{i},base{i}]= fd(a{:}) ;
end



%% 2/ G?n?rer le Rapport d'apprentissage

% par d?faut, r?p?rtoire Y:\\www
learning_report_wrapper('SP500_report','index', 'S&P500 STOCK INDEX','TD',[],'dates',[733774,734888],'scf',[1 1 1])

% si on n'a pas les droits sur www
learning_report_wrapper('CAC_report','index', 'CAC40','TD',[],'dates',[733043,734319],'scf',[1 1 1],'report_folder','C:\learning_reports')

%% 3/ Mise en base
% si le rapport est satisfaisant

% a/ R?cup?rer base, le 2eme output des fonctions distribu?es 
% b/ Appeler base.write() 
for n=1:length(jm.jobs) % attention ? ?liminer les jobs qui n'ont rien ? voir
    FAILED= jm.jobs(1).tasks.outputArguments{1}{1}
    BASE= jm.jobs(1).tasks.outputArguments{2}{1};
    BASE.write(); % ENVOIE EN BASE
end


%% plus cr?dible : on n'a plus les jobs, car on a fait l'apprentissage, 
% puis le rapport, plein de d?buggage, r?-apprentissage, etc et maintenant
% on voudrait le mettre en base ...

% il faudrait avoir conserver la liste de security sur lesquels on l'a fait
% tant pis, on va consid?rer que le composition de l'indice n'a pas chang? ...

% ne pas oublier de faire un change_connections adapte

indexes = {'S&P500 STOCK INDEX','CAC40','AEX', 'SWISS LEADER PR INDEX', 'BEL20'};

jobs = cell(size(indexes));
for i = 1 : length(indexes)
    [~, ~, jobs{i}] = learning_phase_fct_wrap('dts', ...
            'security_id',indexes{i},'trading_destination', {}, ...
            'dates', [datenum(2009,1,1,0,0,0), datenum(2012,3,23,0,0,0)],...
            'buffer_dates', [datenum(2009,1,1,0,0,0), datenum(2012,3,23,0,0,0)], ...
            'old-data', 0, 'force', 1, 'tresh_inhibit',0.95, 'steps', {'base'});
end
for i = 1 : length(jobs)
    waitForState(jobs{i}, 'finished');
end
for i = 1 : length(jobs)
    truc = jobs{i}.getAllOutputArguments();
    for j = 1 : size(truc, 1)
        truc{j, 2}.write();
    end
end





%% plus cr?dible : on n'a plus les jobs, car on a fait l'apprentissage, 
% puis le rapport, plein de d?buggage, r?-apprentissage, etc et maintenant
% on voudrait le mettre en base ...

% il faudrait avoir conserver la liste de security sur lesquels on l'a fait
% tant pis, on va consid?rer que le composition de l'indice n'a pas chang? ...

% ne pas oublier de faire un change_connections adapte
'S&P500 STOCK INDEX',
indexes = {'CAC40','AEX', 'SWISS LEADER PR INDEX', 'BEL20'};

jobs = cell(size(indexes));
for i = 1 : length(indexes)
    [~, ~, jobs{i}] = learning_phase_fct_wrap('dts', ...
            'security_id',indexes{i},'trading_destination', {}, ...
            'dates', [datenum(2009,1,1,0,0,0), datenum(2012,3,23,0,0,0)],...
            'buffer_dates', [datenum(2009,1,1,0,0,0), datenum(2012,3,23,0,0,0)], ...
            'old-data', 0, 'force', 1, 'tresh_inhibit',0.95, 'steps', {'base'});
end
for i = 1 : length(jobs)
    waitForState(jobs{i}, 'finished');
end
for i = 1 : length(jobs)
    truc = jobs{i}.getAllOutputArguments();
    for j = 1 : size(truc, 1)
        e = truc{j, 2}.write();
        if ~isempty(e) && ~isempty(e{1}.error)
            st_log('%s\n', se_stack_error_message(e{1}.error))
        end
    end
end

%%

change_connections('production')
indexes = {'SWISS LEADER PR INDEX'}; % 'AEX', , 'BEL20'
for i = 1 : length(indexes)
    sec_ids = get_index_comp(indexes{i}, 'recent_date_constraint', false);
    for j = 1 : length(sec_ids)
        st_log('%d-%d\n', i, j);
        try
            L= context_volume_learning('security_id',sec_ids(j),'trading_destination', {}, ...
                'dates', [datenum(2009,1,1,0,0,0), datenum(2012,4,20,0,0,0)],...
                'buffer_dates', [datenum(2009,1,1,0,0,0), datenum(2012,4,20,0,0,0)], ...
                'old-data', 0, 'force', 1, 'tresh_inhibit',0.95, 'steps', {'base'});
            [failed,base]= L.learn();
            base.write();
        catch ME
            st_log('%s\n', se_stack_error_message(ME));
        end
    end
end


indexes = {'CAC40'}; % 'AEX', 'SWISS LEADER PR INDEX',
for i = 1 : length(indexes)
    sec_ids = get_index_comp(indexes{i}, 'recent_date_constraint', false);
    for j = 1 : length(sec_ids)
        st_log('%d-%d\n', i, j);
        try
            L= context_volume_learning('security_id',sec_ids(j),'trading_destination', {}, ...
                'dates', [datenum(2009,1,1,0,0,0), datenum(2012,4,20,0,0,0)],...
                'buffer_dates', [datenum(2009,1,1,0,0,0), datenum(2012,4,20,0,0,0)], ...
                'old-data', 0, 'force', 1, 'tresh_inhibit',0.95, 'steps', {'base'});
            [failed,base]= L.learn();
            exec_sql('QUANT', ['delete from ',quant,'..context_ranking where estimator_id = 15 and security_id = ' num2str(sec_ids(j))])
            base.write();
        catch ME
            st_log('%s\n', se_stack_error_message(ME));
        end
    end
end




st_version.my_env.switch_basic_indicator_to_v2 = 1;
old_target_env = st_version.my_env.target_name;
if ~strcmp(st_version.my_env.target_name, target_env)
    changed_connections = true;
    st_log(['!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n'  ...
        'Changing connections to production ones\n' ...
        '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n']);
    change_connections(target_env);
end
% this is not enough as workers will still connect to the other env
% we will create a jobstartup file that we will then have to delete
js_fpath = fullfile(env_root_dir, 'jobStartup.m');
fid = fopen(js_fpath, 'w'); % Creating a jobStartup that has to be in used in filedependency of jobs
fprintf(fid, ['function jobStartup(job)\n global st_version; \nchange_connections(''' target_env ''');\n st_version.my_env.switch_basic_indicator_to_v2 = 1;']);
fclose(fid);

indexes = {'AEX', 'SWISS LEADER PR INDEX','CAC40', 'BEL20', 'OBX'}; % 'S&P500 STOCK INDEX',  
for i = 1 : length(indexes)
    sec_ids = get_index_comp(indexes{i}, 'recent_date_constraint', false);
    for j = 1 : length(sec_ids)
        st_log('%d-%d\n', i, j);
        try
            L= context_volume_learning('security_id',sec_ids(j),'trading_destination', {}, ...
                'dates', [datenum(2009,1,1,0,0,0), datenum(2012,4,20,0,0,0)],...
                'buffer_dates', [datenum(2009,1,1,0,0,0), datenum(2012,4,20,0,0,0)], ...
                'old-data', 0, 'force', 1, 'tresh_inhibit',0.95, 'steps', {'base'});
            [failed,base]= L.learn();
            exec_sql('QUANT', ['delete from ',quant,'..context_ranking where estimator_id = 15 and security_id = ' num2str(sec_ids(j))])
            exec_sql('QUANT', ['delete from ',quant,'..learningmodel where estimator_id = 15 and security_id = ' num2str(sec_ids(j))])
            base.write();
            job_id = cell2mat(exec_sql('QUANT', ['select job_id from ',quant,'..association where estimator_id = 15 and trading_destination_id is null and security_id = ' num2str(sec_ids(j))]));
            
        catch ME
            st_log('%s\n', se_stack_error_message(ME));
        end
    end
end