function ident= GLD(varargin)
%function ident= GLD(varargin)
% get learning dates

modecell=0;
if isnan(varargin{1})
    modecell= 1;    
    varargin= varargin(2:end);    
end

if length(varargin)==2
    sec_id= varargin{1};
    td= varargin{2};
end

tbx=newsProjectTbx();
ident= {sec_id,td,nan,''};

r= tbx.FILEconfig.folder({1,2,3,''},'main');
l= dir(r);

%sec_id=1;td=4;
if isempty(td);td= '';
else; td= num2str(td);end
patern= sprintf('stock_id_%d_%s_',sec_id,td);

idx=arrayfun(@(x) ~isempty(strfind(x.name,patern)) & x.isdir , l );
ll= l(idx);

if isempty(ll)
    m= sprintf('Unable to find <%s> in <%s>', patern,r);
    error(m);
end

ou_ca= [];
if length(ll) > 1
    warning(sprintf('Multiple learning dates for <%s>',patern));
    for k=1:length(ll)
       IO= filetoolbox(fullfile(r,ll(k).name));
       res= IO.parse('type','FINALmodel','inhibition',1);
       if length(res)>0
           ou_ca(end+1)= k;           
       end
    end    
end

assert(length(ou_ca)==1);
ll= ll(ou_ca);
assert(length(ll)==1);
ident{3}= name2dts(ll(1).name);

if modecell
    ident= {'security_id',ident{1},'trading_destination',ident{2},'dates',ident{3}};
end
%varargout= ident;

function dts= name2dts(pth)    
    l= tokenize(pth,'_');
    dts= [str2num(l{end-1}),str2num(l{end})];



