function [data,changing_continuous_bin,false_changes] = aggregate_mktTime_extension(data)
% Uniformise les bins d'un cell array.
%
% Data= cell-array de jours (deja aggrege a frequence learning), mais non
% uniformes sur les phases continues. Le volume n'est pas encore normalis�, mais
% l'extrapolation se fait bien en repartition. Utilis� pour uniformiser les buffers
% � fr�quence de learning (10 ou 15min)
%
% Alignement sur le dernier jour:
% * Les phases inexistantes dans le pass� sont cr�es, avec la moyenne sur ces bins
%   lorsqu'ils existent vraiment (rajout de bins). (seuil sur Nobs new horaire: Nmin_day_new_horaire_add)
% * Les phases qui disparaissent sont supprim�es
%
% L'alignement ne concerne que les phases continues (les autres phases sont identifi�es
% et align�es) 

% V2: On aligne sur le dernier jour, avec la moyenne des bins lorsqu'ils
% existent vraiment. (V1: on aggr?geait sur grille la plus basse)
% 
% % data= data(end:-1:1); % test allongement trading
% test� sur oslow, sans bug

Nmin_day_new_horaire_add= 1; %%%%%%20;

ID_CONTINUE = data{1}.info.phase_th(strcmpi({data{1}.info.phase_th.phase_name},'CONTINUOUS')).phase_id;
% Liste toutes les phases sur tout le sample, identifiees par [begin, end,phase]
N = length(data);
extract_ph = @(data)st_data('cols',data,'begin_slice;end_slice;phase');
idphase = cellfun(extract_ph,data,'uni',false);
false_changes=[];

% Unicit?-> cr?e identifiant de toutes ces phases
% [begin end phase jour.idx bin.idx]
X = cell2mat(cellfun(@(x,n)...
    [round(x(:,1:2)*24*600)/24/600,x(:,3),n*ones(length(x),1),(1:length(x))'],... On arrondit � 10 secondes
    idphase,num2cell(1:length(idphase))','uni',false));


X_oneNc_only = X;
X_oneNc_only(X(:,3) ~= ID_CONTINUE,1:2)= 0;
[all_phases,~,APn] = unique(X_oneNc_only(:,1:3),'rows');

% Liste les phases pr�sentes sur tout l'�chantillon
is_phase_tjs_present = accumarray(APn,1)==N;
is_phase_tjs_present = is_phase_tjs_present|~(all_phases(:,3)==ID_CONTINUE); % on se limite � la phase continue
idx_CV = find(~is_phase_tjs_present);

changing_continuous_bin = all_phases(idx_CV,:);


[end_t_sorted,ord]= sort(changing_continuous_bin(:,3)+changing_continuous_bin(:,2)); % sort by end_time
dt_= diff(end_t_sorted);
i_= find(dt_< 1/24/60/60); % changing phase cons?cutives dont end
if ~isempty(i_)
     i_= ord(i_);
     if changing_continuous_bin(i_,3) == changing_continuous_bin(i_+1,3)
         false_changes(end+1:end+2,:)= changing_continuous_bin([i_ i_+1],:);
         changing_continuous_bin([i_ i_+1],:)=[];
         idx_CV([i_ i_+1],:)= [];
     end
end


for k = reshape(idx_CV,1,[])
    days = X(APn==k,end-1); % Indice du jours o� la phase est pr�sente
    bins = X(APn==k,end); % Indice du bin concern�
    phase_bin_courant = all_phases(k,3); % "phase" de la phase concern�e
    Ndp = length(days);
    [z_,bin_horaire] = deal(nan(1,Ndp));
    remove_ = false;
    %-- Alignement sur le dernier jour
    if ~ismember(N,days) % On �carte la phase si elle n'est pas pr�sente le dernier jour
        remove_ = true;
    else
        % v�rifie au moins 9 jours de donn�es avec les nouveaux horaire
        % FIXME: a faire dans le 1er cas du if ?
       assert( all(ismember(N-Nmin_day_new_horaire_add:N,days)),...
           'aggregate_mktTime_extension(): non conti. phase change, less than 10 days ago !');
    end
    
    if ~remove_
        for u = 1:Ndp
            volume = get_col(data{days(u)},'volume');
            z_(u) = volume(bins(u))/sum(volume);
            bin_horaire(u)= mod(data{days(u)}.date(bins(u)),1);
        end
        assert(~any(isnan(z_)));
        proxy = mean(z_);
        bin_horaire = unique(bin_horaire);
        days2extend = setdiff(1:length(data),days);
    else
        days2extend = days; % jours avec, n�cessite suppression
    end
    
    for u = 1:length(days2extend)
        DD = data{days2extend(u)};
        [volume,id_vol] = get_col(DD,'volume');
        [~,id_phase] = get_col(DD,'phase');
        if ~remove_            
            % Ajout de la phase absente pour les jours de "days2extend"
            new_line = nan(1,length(DD.colnames));
            new_line(id_vol) = proxy * sum(volume);
            new_line(id_phase) = phase_bin_courant;
            DD.value(end+1,:) = new_line;
            DD.date(end+1) = bin_horaire + floor(DD.date(1)); % Tri effectu� plus loin
        else
            % Suppression de la phase pr�sente pour les jours de "days2extend"
            if ~isfield(DD,'to_del')
                DD.to_del = [];
            end
            DD.to_del(end+1) = bins(u);
        end
        if ~isfield(DD,'aggregate_info')
            DD.aggregate_info = {};
        end
        DD.aggregate_info{end+1} = [remove_, u,days2extend(u), bin_horaire ];
        data{days2extend(u)} = DD;
    end        
end

for k=1:length(data)
    D= data{k};
    if isfield(D,'to_del')
        idx= D.to_del;
        D.value(idx,:)=[];
        D.date(idx)=[];
        data{k}= D;
    end
    sortby= st_data('col',D,'phase')+D.date;
    if ~issorted(sortby)
        [~,i]= sort(sortby,'ascend');
        D.date= D.date(i);
        D.value= D.value(i,:);
        data{k}= D;
    end
end

% assert(length(unique(  cellfun(@(x) size(x.value,1),data)  ))==1);
% assert(length(unique(  cellfun(@(x) size(x.date,1),data)  ))==1);

function varargout = get_col(d,cn)
i = find(strcmpi(d.colnames,cn));
assert(length(i)==1);
v = d.value(:,i);
varargout{1} = v;
varargout{2} = i;