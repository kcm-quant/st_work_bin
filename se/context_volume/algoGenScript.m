%% script algo g�n�tique (@Lehalle)

sec_ID= 276; % TOTAL
p= eModelPool();
p.mode= 'intraday'; % ou 'daily'
p= p.inidata(p,sec_ID,p.mode);

news= p.news.value;
names= p.news.colnames;
tnames= cellfun(@(x,y) [x '(' y ')'], p.news.colnames, cellstr(datestr(p.news.time))' , 'UniformOutput' ,0);

doublons= {'Expiry.SET1','Expiry.SET0','Future DAX','Future EURO STOXX 50'};
i= ismember( p.news.colnames,doublons); i=  find(i);
news(:,i)= []; % expiry
names(i)=[]; tnames(i)=[];


%%

 %g = load('news_test_set')
 clear ga
 ga = gaNews( 'dna_length', 5, 'pop_size', 100, 'r_xover', 0.15, 'r_mutation', @(t)[0.03*(t<301)+0.6*(t>300)/sqrt(t-300)*sqrt(2)] )
 %ga.dataset( 'pattern', g.self.news.value, 'signal', g.self.volume.value, 'colnames', {g.self.news.colnames} )
 ga.dataset( 'pattern', news, 'signal', p.volume.value, 'colnames', {names} )
 ga.set_ref('mean')
 ga.set_fit('L2')
 ga.set_aggregation('mean')
 ga.init()

 ga.get('population')
 fit = ga.fit(ga.population{1})
 [pop, fit] = ga.pressure( fit)
 pop = ga.xover( pop, fit);
 pop = ga.mutate( pop)
 [pop, fit] = ga.evol_one_step()
 ga.verbosity = 2

 %ga.evol()
 ga.evol('max_iter',1200)
 ga.plot()
 ga.plot('DNA', -(1:1200))
 ga.plot('snapshot', 125)

[ga, g] = gaNews.main( 'dna_length', 8, 'population_size', 100, 'max_iter', 12000)

mode(ga.population{end})

