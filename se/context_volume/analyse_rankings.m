function analyse_rankings()
% Analyses 'Y:\dev_repository\news_project\synthetic_result' buffers


function loadall()
f= 'Y:\dev_repository\news_project\synthetic_result';
l= dir(f);
% sparse matrix or couple listing ?
sec2row= Dwrap(mdict()); % sec_id -> idx
news2col= Dwrap(mdict()); % news -> idx

items= {}; %struct('stock',{},'news',{},'rank',{},'roc',{});
for k=1:length(l)
   if ~(l(k).isdir) & strcmpi(l(k).name(end-3:end),'.mat')
       x= load([f '\\' l(k).name]);
       savedVariable= x.savedVariable;
       z=[];
       z.stock= savedVariable.security_id;
       z.news= savedVariable.news_id;
       z.roc = savedVariable.relative_operational_criteria;
       z.coverage= savedVariable.selected / savedVariable.occurences ;
       z.rank= savedVariable.rank;
       z.kur= kurtosis(savedVariable.value(4:end));
       items{end+1}= z;              
   end    
end
its= cellfun(@(x)x,items);
[an,bn,cn]= unique([its.news])
[as,bs,cs]= unique([its.stock]);
i_= sub2ind([length(as),length(an)], cs,cn );

n=newsDb(1); newsnames= n.id2name(an);


V=nan([length(as),length(an)]) ;V(i_)= [its.rank]
r= odata(V,as,newsnames,'ranks') % vdct

V=nan([length(as),length(an)]) ;V(i_)= [its.roc]
roc= odata(V,as,newsnames,'roc') % vdct

V=nan([length(as),length(an)]) ;V(i_)= [its.kur]
kur= odata(V,as,newsnames,'kur') % vdct

V=nan([length(as),length(an)]) ;V(i_)= [its.coverage]
coverage= odata(V,as,newsnames,'coverage') % vdct

r.matrixplot()
roc.matrixplot()

rc.matrixplot()

kur.matrixplot()
coverage.matrixplot()

%%
rr= r; rr.value(rr.value==0) = max(rr.value(:)); rr.value(isnan(rr.value)) = max(rr.value(:));
[W,H]= nnmf(rr.value,5,'replicates',20)

rc= roc;rc.value(rc.value>1)= 1; rc.value(isnan(rc.value))= 1;
[W,H]= nnmf(rc.value,3,'replicates',10)
%[W,H]= rc.nnmf(3,'replicates',10)

h= matrixplot(H',rr.colnames);
figure;
h= matrixplot(W);




