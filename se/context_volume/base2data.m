function [B,R,news]= base2data(base0,sec_id,news,version)
% Changed 8/3/2012 : some news learned are missing in base -> create pipo
% event in news -> return news
if nargin< 4; version=1;end

if version==1
    funs= base0{1,4};
    M= model2str(funs);
    [P,Q]= size(base0);
    base= cell2mat(base0(:,1:3));
    base= base(base(:,1)==sec_id,:);
    [~,i]= sort(base(:,3),'ascend');
    base= base(i,:);
    ranks_id= base(:,2);
    ranks= nan(1,P);
    %db= newsDb()
    for p=1:P
        r= news.col_id== ranks_id(p);
        r= find(r);
        assert(length(r)==1);
        ranks(p)= r; % num de la colonne            
    end
elseif version==2
    % base.num et base.model
    idx_ =cellfun(@(x) x{1},base0.model);
    idx_ = find(idx_ == sec_id);
    funs= base0.model{idx_}{2};
    M= model2str(funs);
    [P,Q]= size(base0.num);
    %base= cell2mat(base0(:,1:3));
    base= base0.num(base0.num(:,1)==sec_id,:);
    %base= base(base(:,1)==sec_id,:);
    [~,i]= sort(base(:,3),'ascend');
    base= base(i,:);
    ranks_id= base(:,2);
    ranks= nan(1,P);
    %db= newsDb()
    for p=1:P
        r= news.col_id== ranks_id(p);
        r= find(r);
        assert(length(r)==1);
        ranks(p)= r; % num de la colonne            
    end
elseif version==3
    funs= base0.model;
    M= model2str(funs);
    [P,Q]= size(base0.num);
    %base= cell2mat(base0(:,1:3));
    base= base0.num(base0.num(:,1)==sec_id,:);
    %base= base(base(:,1)==sec_id,:);
    [~,i]= sort(base(:,3),'ascend');
    base= base(i,:);
    ranks_id= base(:,2);
    ranks= nan(1,P);
    %db= newsDb()
    for p=1:P
        r= news.col_id== ranks_id(p);
        r= find(r);
        if isempty(r)
            news.colnames{end+1}= 'MISSING EVENT';
            news.value(:,end+1)= 0==1;
            news.col_id(end+1)= ranks_id(p);
        end
        r= news.col_id== ranks_id(p);
        r= find(r);
        assert(length(r)==1);
        ranks(p)= r; % num de la colonne                    
    end
else
    assert(0);
    
end
B=[];
B.opt= M;
B.ranks= ranks;
B.IDstock= sec_id;
B.news= news;
B.model_str= funs;
B.frequency= base0.frequency;
B.trading_destination_id= base0.trading_destination_id;
B.inhibited= base0.inhibited;

R= eModel2('noter',nan);
R.opt= M;
R.ranks= ranks;
R.IDstock= sec_id;
R.news= news;
R.frequency= base0.frequency;
R.trading_destination_id= base0.trading_destination_id;
R.inhibited= base0.inhibited;

