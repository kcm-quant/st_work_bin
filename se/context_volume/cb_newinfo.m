function full_info= cb_newinfo(ip, sec_id)
    if isfield(ip,'path_modifier')
       path_modifier= ip.path_modifier;
    else
       path_modifier= []; 
    end
    assert(ismember(sec_id,ip.comp));
    TD= ip.TD;
    if iscell(TD)
        if isempty(TD); TD= [];
        else; TD= TD{1};
        end
    end
    if ischar(TD) && strcmpi(TD,'main')
        x_=get_repository('tdinfo',sec_id);        
        TD= x_(1).trading_destination_id;
    end
    dates= ip.dates;
    full_info={sec_id,TD,dates,path_modifier};