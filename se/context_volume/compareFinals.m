function compareFinals()
ID=276;
INH= 1;

tbx= newsProjectTbx();
IO= tbx.FILE(tbx.FILEconfig.folder(ID,'stock'));
n1= IO.loadSingle('type','FINALmodel','inhibition',INH,'nonews',1);
n1= n1{1}.savedVariable;
n0= IO.loadSingle('type','BESTmodel','inhibition',INH);
n0= n0{1}.savedVariable;
%% GET 1


[a,b]= n0.best.opt.compare(n1.J.opt)
[a,b]= n1.J.opt.compare(n0.best.opt)
assert(isempty(a));assert(isempty(b))

[a,b]= n1.J.filetrace.compare(n0.best.filetrace)
[a,b]= n0.best.filetrace.compare(n1.J.filetrace )

[a,b]= n1.ER.filetrace.compare(n0.best.filetrace)
[a,b]= n0.best.filetrace.compare(n1.ER.filetrace )



%% CMP
corr(n0.best.BN,n1.J.BN)
corr(n0.best.BN,n1.ER.BN)
BN= n0.best.BN;

c= corr(n1.ER.value(BN>0,:), n0.best.value(BN>0,:) );
c= diag(c); unique(c)

n0.AS.no



% cmp2
x= tbx.getFromFileTrace(n0.best.filetrace,'jackSave',ID);
x= x.savedVariable;

corr(x.BN,n1.J.BN)
corr(x.BN,n0.best.BN)
c= corr(x.pred(BN>0,:),n1.J.pred(BN>0,:));
c= diag(c); unique(c)

%%







