    function self= compareModels(varargin)
% Selects the best model fo each stock
% PROJECT: context sensitive volume forecast
% Input= fichier jacknife avec error, bufferis� par eModelPool
%
% Loads the jacknife done by eModelPool, and plot the results
%
% TODO: automatically select best model
%
% attention, l'ad�quation params / opt / filetrace
% - a l'air fausse sur note:None
% - automatiser le test ?

if nargin== 1 & strcmpi(varargin{1},'tbx');
else; tbx= newsProjectTbx();end

self.stockID= nan;
self.loadBuffer= @loadBuffer;
self.data= nan;
self.vdata= nan;

self.allplots= @allplots;
self.dispnames= @dispnames;

self.getNotes= @getNotes;
self.bestM0=@bestM0;

self.compute_gain=@compute_gain;

self.misc=[];
self.misc.opt2name= @opt2name;
self.misc.dispnames= @dispnames;
self.main= @main;
self.getM0=@getM0;

self.checkError= @checkError;
self.bnPlot=@bnPlot;
self.findBestModel= @findBestModel;

self.profilePlotSimple= @profilePlotSimple;
self.mainProcess=@mainProcess;
%%
    function TEST()
        inhibition= 0;
        c= compareModels('tbx');
        %[rm,c,u]=c.main(276,36);
        [rm,c]=c.main(6155,37,inhibition);
        
        cc=c.getNotes(c)
        [r,full]= rm.filter()
        best= cc.findBestModel(cc,rm,'cash 1mn',inhibition)
        
        cc.allplots(cc)
        
        %c.bnPlot(c,'cash 1mn')
    end
    function mainProcess(EMP,freq,inhibited)
        ID= EMP.stockID;
        % TODO: check <1 !!!
        if nargin< 3; inhibited=0;end
            
        if ID== 71;
            www=2;
        end        
        c= compareModels('tbx');
        c.path_modifier= EMP.path_modifier;
        %[rm,c,u]=c.main(ID,freq);        
        %[rm,c]=
        [rm,c]= c.main(EMP,freq,inhibited);
        
         c=c.getNotes(c);
%         [r,full]= rm.filter();
         best= c.findBestModel(c,rm,'cash 1mn',inhibited);
        %c.bnPlot(c,'cash 1mn')
    end
    %function [rm,c,u]= main(ID,freq)
    function [rm,c]= main(EMP,freq,inhibited,varargin)
        %function main(ID,freq,inhibited,varargin)
        %assert(ismember(freq,[1,36]));
        if nargin==0;
            ID=2;
        end
        ID= EMP.stockID;
        c= compareModels();    
        c= decorate(c);
        c.TD= EMP.TD;
        V_= EMP.volume.get();V_=V_{2}; % ca sert juste � choper original_date
        c.original_date= V_.original_date;
        c.path_modifier= EMP.path_modifier;
        %c= c.loadBuffer(c,ID,'dim',freq,'inhibition',inhibited,varargin{:});                                
        c= c.loadBuffer(c,ID,'inhibition',inhibited,varargin{:});                                
        %[u,rm]= c.compute_gain(c);     
        
        % MEMORY !
        %c.vdata= [];
        
        %c.data= arrayfun(@(x) x.savedVariable,c.data,'uni',0);
        %c.data= cellfun(@homogeneise_,c.data);
        
        %cb= c.compute_gain;
        %clear c;        
        tbx= newsProjectTbx();
        [rm]= c.compute_gain(c);%,inhibited);
        IO= tbx.FILE(tbx.FILEconfig.folder(c,'stock'));
        %clear c;        
        ids= {'type','MRANK','inhibition',inhibited};
        fname= IO.save(rm,ids{:});
    end
    function x= homogeneise_(x)
       if ~isfield(x,'inhibitedCtxt')
           x.inhibitedCtxt= nan;
       end
           
        
    end

    function main1()
        c= compareModels();
        ID=2; %'X'
        c= c.loadBuffer(c,ID,'dim',36);
        %c.data(1).savedVariable.colnames(c.bestm0)
        
        c=c.getNotes(c)
        
        rm= c.compute_gain(c)
        
        c.bestM0(c)
        c.dispnames(c);
        c.allplots(c)
        
        o= arrayfun(@(x)options(x.params),c.data)
        char(arrayfun(@(x)[x.get('estimateur') ,' ',x.get('noter') ],o,'UniformOutput',0))
        
        c.checkError(c);
        
    end
    function main2()
       c=compareModels();
       ep= eModelPool();
       ep.stockID= 2;
       d= ep.getAllPred(ep);
       opts= arrayfun(@(x) x.params,d,'UniformOutput',0) ;  
       classes= optSort(opts, 'noter','estimateur','Npoint');
       d= d([classes{:}])

       d1= uo_(arrayfun(@(x) x.savedVariable.pred(:,1),d,'UniformOutput',0) )';  
       
       idx= any(d(1).savedVariable.news.value,2);
       
       figure,matrixplot(abs(corr(d1(idx,:))))
       P= size(d1,2);
       names= arrayfun(@(x) c.misc.opt2name(x.params,{'dim','type'}),d ,'uniformoutput',0);  
       set(gca,'ytick',(1:P)+0.5);set(gca,'yticklabel',names);
       title('corr(pred)')

       bn= uo_(arrayfun(@(x) x.savedVariable.BN,d,'UniformOutput',0) )';  
       figure,matrixplot(abs(corr(bn(idx,:))))
       P= size(d1,2);
       %names= arrayfun(@(x) c.misc.opt2name(x.params,{'dim','type'}),d ,'uniformoutput',0);  
       names= arrayfun(@(x) c.misc.opt2name(x.params),d ,'uniformoutput',0);  
       set(gca,'ytick',(1:P)+0.5);set(gca,'yticklabel',names);
       title('corr(BN)')
    
       c.misc.opt2name(d(1).params)
    end



%%
end

%% SELECT BEST
% automatic selection
%function getBest()

function m0= getM0(self)
    % OV: on renvoyait indices non contextuel moyenne/mediane
    % NV il suffit de renvoyer les meilleurs
    % Attention cela maintient plus bestM0 pour les autres crit�res (meilleur frequence en C.Op)
    m0=[nan,nan]; % moyenne/mediane (indices)
    N=length(self.data);
    ma=[];md=[];
    for n=1:N
        o=options(self.data(n).params);
        if strcmpi(o.get('noter'),'None')
            if strcmpi(o.get('estimateur'),'mean')
                ma(end+1)= n;
            elseif strcmpi(o.get('estimateur'),'median')
                md(end+1)= n;
            else;assert(0);
            end
        end
    end
    quality=[]; % MOYENNE
    for i= (ma(:)')
        avg_qual= mean(self.data(i).savedVariable.value);
        col= strcmpi('cash 1mn', self.data(i).savedVariable.colnames);col=find(col);
        assert(length(col)==1);
        quality(end+1)= avg_qual(col);
    end
    if ~isempty(quality); [~,i]= min(quality); m0(1)= ma(i); end
    
    quality=[]; % MEDIANE
    for i= (md(:)')
        avg_qual= mean(self.data(i).savedVariable.value);
        col= strcmpi('cash 1mn', self.data(i).savedVariable.colnames); col=find(col);
        assert(length(col)==1);
        quality(end+1)= avg_qual(col);
    end
    if ~isempty(quality); [~,i]= min(quality); m0(2)= md(i); end
    
end

function self= bestM0(self)
% selection pour chaque crit�re du meilleur M0
% -> plus pessimiste
m0= self.getM0(self);
i=nan;
if(isnan(m0(1)))
    i= 1;
    m2= mean(self.data(m0(2)).savedVariable.value); 
    o=options(self.data(m0(2) ).params);
    assert(strcmp(o.get('noter'),'None'));
    m1=m2;
    i= ones(size(m2,2),1);
elseif(isnan(m0(2)))    
    m1= mean(self.data(m0(1)).savedVariable.value);
    o=options(self.data(m0(1) ).params);
    assert(strcmp(o.get('noter'),'None'));
    m2=m1;
    i= zeros(size(m2,2),1);
else
    o=options(self.data(m0(1) ).params);
    assert(strcmp(o.get('noter'),'None'));
    o=options(self.data(m0(2) ).params);
    assert(strcmp(o.get('noter'),'None'));
    m1= mean(self.data(m0(1)).savedVariable.value);
    m2= mean(self.data(m0(2)).savedVariable.value); 
    i= m1>m2; % 2 est preferable
end
self.bestm0= nan(1,size(m1,2));
self.bestm0(i==1)= m0(2);
self.bestm0(i==0)= m0(1);
%self.data(1).savedVariable.value(:,i) = self.data(2).savedVariable.value(:,i);
%self.data(2)=[];

end

function rankModels(self)


end

function [rankM]= compute_gain(self)
% Est-ce qu'on sort plusieurs ranking ? (fs,ns,op,op^2,...)
self= bestM0(self);
N= length(self.data);
fs= []; % full sample
ns= []; % news selected
nsr=[]; % news selected relative to NoNews
nbn= [];
%tbx= newsProjectTbx();
ordre = self.data(1).savedVariable.colnames;

[T,K]= size(self.data(1).savedVariable.value);
BMdata= nan(T,K);
for k=1:K
    BMdata(:,k)= self.data(self.bestm0(k)).savedVariable.value(:,k);
end

for n=1:N    
    newdata= st_data('keep',self.data(n).savedVariable,ordre);
    %[newdata, nonFound]= tbx.st.alignerColonne(self.data(n).savedVariable,ordre); 
    % check alignement
    fs(n,:)= mean(newdata.value,1);
    if any(isnan(fs(n,:)));
        www=1;
    end
    % ne pas forc�ment comparer les nonews sans BN aux news sur les BN ->
    % pas le m�me jour !!!
    try;ns(n,:)= mean(newdata.value(newdata.BN>0,:),1);       
    catch; ns(n,:)= nan(1,size(newdata.value,1)); %= fs(n,:);%
    end
    
    if all(newdata.BN==0)
        nsr(n,:)= mean(newdata.value,1) ./ mean(BMdata,1);
    else
        nsr(n,:)= mean(newdata.value(newdata.BN>0,:),1) ./ mean(BMdata(newdata.BN>0,:),1);
    end
    nbn(n)= sum(newdata.BN> 0) / T;
end
rownames= arrayfun(@(x) opt2name(x.params),self.data ,'uniformoutput',0);
i= strcmpi('cash 1mn',ordre); i = find(i); assert(length(i)==1);
[s,ranks]= sort(fs(:,i),'ascend');
rankM= [];
rankM.colnames= ordre;
rankM.value= ranks; % ranks(1)= indice du meilleur mod�le
rankM.IDstock= self.data(1).savedVariable.IDstock;
rankM.fs=fs;
rankM.ns=ns;
rankM.nsr=nsr;
rankM.nbn=nbn;
rankM.rownames= rownames;
rankM.bestm0= self.bestm0;

idx= self.getM0(self);
rankM.note= s;
if isnan(idx(1));rankM.noteNNmoy =nan;
else;rankM.noteNNmoy= fs(idx(1),i);end
if isnan(idx(2));rankM.noteNNmoy =nan;
else;rankM.noteNNmed= fs(idx(2),i);end


    function [r,full]= filter(critere,remove)
        % probl�me: M indice dans self, pas dans full ou c !!!
       if nargin<1;critere='cash 1mn';end
       if nargin<2;remove=1;end       
       critere= find(strcmpi(rankM.colnames,critere));
       assert(length(critere)==1);
       
       r= rankM;
       r.fs= r.fs(:,critere);
       r.ns= r.ns(:,critere);
       r.nsr= r.nsr(:,critere);  
       r.colnames= r.colnames{critere};
       r.bestm0 =r.bestm0(critere);
       full= self;
       if remove
          idx1= r.fs < r.fs(r.bestm0);
          idx2= r.nsr < 1;
          idx= intersect(find(idx1),find(idx2));
           r.fs= r.fs(idx);
           r.ns= r.ns(idx);
           r.nsr= r.nsr(idx);  
           r.nbn= r.nbn(idx);  
           r.rownames= r.rownames(idx);           
           full.data= full.data(idx);
           %full.vdata= full.vdata(idx,:,:);
           
       end
       
    end
rankM.filter= @filter;
end


function best= findBestModel(self,rankM,critere)

if nargin<3;critere= 'cash 1mn';end
critere= find(strcmpi(critere,rankM.colnames));
assert(length(critere)== 1);

best=[];

if isempty(rankM.fs)
     best.best= self.data(rankM.bestm0(critere)).savedVariable; %cash 1mn
     best.best.ranks= self.ranks{rankM.bestm0(critere)};
     best.best.note= self.note{rankM.bestm0(critere)};
else
    [t_,s]= min(rankM.fs(:,critere));
    bestIdx=s(1);

    best.best= self.data(bestIdx).savedVariable;
    best.best.ranks= self.ranks{bestIdx};
    best.best.note= self.note{bestIdx};
end
best.M0 = self.data(rankM.bestm0(critere)).savedVariable; %cash 1mn
best.M0.ranks= self.ranks{rankM.bestm0(critere)};

best.AS= [];
best.AS.fs= mean(best.best.value,1);
best.AS.no= mean(best.best.value(best.best.BN>0,:),1);
best.AS.prob= mean(best.best.BN>0);
best.AS.nctxt= length(unique(best.best.BN));

best.RS= [];
best.RS.fs= mean(best.best.value,1) ./ mean(best.M0.value,1);
best.RS.no= mean(best.best.value(best.best.BN>0,:),1) ./ mean(best.M0.value(best.best.BN>0,:),1);
best.RS.prob= mean(best.best.BN>0);
best.RS.nctxt= length(unique(best.best.BN));


end



%% LOAD / decorate
    function self= loadBuffer(self, stockID,varargin)
        %inhibited= varargin{4};
        inhibited= varargin{end};
        % il faut r�cup�rer les data NoNews qui sont sans inhibition
        % ou cr�er les fichiers inhib�s !
    
       self.stockID= stockID;
       ep= eModelPool();
       ep.path_modifier= self.path_modifier;
        ep= ep.inidata(ep,stockID,self.TD,'intraday',self.original_date);        
       self.data= ep.getAllErrors(ep,varargin{:});
       
       if isempty(self.data)
           varargin= removeOptInCell(varargin,'inhibition');
           self.data= ep.getAllErrors(ep,varargin{:});
       end
       
       %self= bestM0(self);
       
       self.vdata= [];
       for k=1:length(self.data)
            self.vdata(k,:,:)= self.data(k).savedVariable.value;
        end
    end

    function self= getNotes(self)
    tbx= newsProjectTbx();
    F= tbx.FILEconfig.folder(self,'stock');
    N= length(self.data);
    self.ranks= {};
    for n=1:N
        % TODO 3<=>n ???
        try
            f= [F, self.data(n).savedVariable.filetrace.get('rankLoad')];
            u= load(f);
            self.ranks{n}= u.savedVariable.ranks;
            self.note{n}= u.savedVariable.note;
        catch
            self.ranks{n}= [];
            self.note{n}= [];
        end
    end

    end

function self=decorate(self)
    tbx= newsProjectTbx();
    super= self;
    %function [u,X]= decorated_compute_gain(u)            
%     function [X]= decorated_compute_gain(u,inhibited)            
%         X= super.compute_gain(u);        
%         IO= tbx.FILE(tbx.FILEconfig.folder(u.stockID,'stock'));
%         clear u;
%         ids= {'type','MRANK','inhibition',inhibited};
%         %fname= IO.save(u,ids{:});
%         %u.filetrace.set('rankSave',fname);
%         fname= IO.save(X,ids{:});
%         % ne pas return X en mode prod
%     end
%    self.compute_gain= @decorated_compute_gain;
    function [X,u]= decorated_findBestM(varargin)    
        u= varargin{1};
        X= super.findBestModel(varargin{1:end-1});
        IO= tbx.FILE(tbx.FILEconfig.folder(u,'stock'));
        ids= {'type','BESTmodel','inhibition',varargin{4}};
        %fname= IO.save(u,ids{:});
        %u.filetrace.set('rankSave',fname);
        fname= IO.save(X,ids{:});
        % ne pas return X en mode prod
    end
    self.findBestModel= @decorated_findBestM;
end

%% CHECK
function c=checkError_1(modl,bench)
    % v�rifie que le mod�le a les m�mes erreurs lors d'un no-news ?
    i = modl.savedVariable.BN== 0;
    e1= modl.savedVariable.value(i,:);
    e2= bench.savedVariable.value(i,:);
    c= diag(corr(e1,e2))';
end

function [a1,a2]= checkError(self)
    f= @(x) strcmpi(x.savedVariable.opt.get('estimateur'),'median');
    iMed=  arrayfun(f,self.data);
    iM0= self.getM0(self);
    a1= arrayfun(@(t) checkError_1(t,self.data(iM0(1))),self.data(iMed==0), 'UniformOutput' ,0 );
    a1= uo_(a1);
    a2= arrayfun(@(t) checkError_1(t,self.data(iM0(2))),self.data(iMed==1), 'UniformOutput' ,0 );
    a2= uo_(a2);
    % on a du 1, du -1 et du NaN !!!
    % pourquoi -1 ?
end

%% PLOTS
 function allplots(self)
     %   bnPlot(self,'cash 1mn')
    %bnPlot(self,'L^2')

 %   profilePlotSimple(self, 'cash 1mn')
	% profilePlot(self)
    %    errorNonContext(self)
    %    plotError(self);
        
        %idx=any(self.data(1).savedVariable.news.value,2);
        %plotError(self,idx);
       % idx=self.data(1).savedVariable.news.value(:,5)==1;
       %plotError(self,idx);
       plotError(self)
    %  plotNContext(self);
        %plotProbSelect(self);
        
   % plotErrorSelect(self);
    end

function setYlabel(N,self,x)
    %T= length(self.data(1).savedVariable.BN);    
%     probs= arrayfun(@(x) num2str(mean(x.savedVariable.BN>0)) ,self.data ,'uniformoutput',0);    
%     probs= cellfun(@(x) 100*str2num(x(1:min(length(x),3))) ,probs ,'uniformoutput',1);    
%     names= arrayfun(@(x,probs) [opt2name(x.params) ' (' num2str(probs) ')'],self.data ,probs,'uniformoutput',0);   
    
    names= arrayfun(@(x) [opt2name(x.params) ' (' num2str(floor(100*mean(x.savedVariable.BN>0))) ')'],self.data ,'uniformoutput',0);
    names= arrayfun(@(x) [opt2name(x.params) ' (' num2str(floor(sum(x.savedVariable.BN>0))) ')'],self.data ,'uniformoutput',0);
    set(gca,'Ytick',(1:N)+0.5)
    set(gca,'Yticklabel',names)    
end

function plotError(self,idx)    
    ttl= ['Learning errors (jacknife) ' self.data(1).savedVariable.NAMEstock ]; %' ' self.data(1).savedVariable.colnames{col}];    
    T= size(self.vdata,2);
    if nargin==1; idx=1:T;
    else;ttl=[ttl ,' (news occurence only)'];
    end
    x= squeeze( mean(self.vdata(:,idx,:),2) );
    N= size(x,1);
    figure; hold on;
    h= []; styles= {'k+','r.','b+','g*','gd','y*','yd'};
    COL= [(1:5),[8,9]] ;  
    COL= COL(COL< size(x,2));
    for c= 1:length(COL)
        h(c)= plot(x(:,COL(c)),(1:N)',styles{c});    
    end
    setYlabel(N,self,x);
    lgd= self.data(1).savedVariable.colnames(COL);
    legend(h',lgd);
    title(ttl)
end

function plotErrorSelect(self)        
    ttl= ['Learning errors when selected context (jacknife) ' self.data(1).savedVariable.NAMEstock ]; %' ' self.data(1).savedVariable.colnames{col}];
    N= length(self.data);
    x=[];     
    for n=1:N
       idx= self.data(n).savedVariable.BN>0;      
       if all(~idx)
          idx= 1:size(self.vdata,2) ;
       end
       
       x(n,:) = squeeze(mean(self.vdata(n,idx,:),2)) ./ x0;     
    end
    m0= getM0(self);
    x0  = squeeze(mean(self.vdata(1,idx,:),2));
    x0  = squeeze(mean(self.vdata(1,idx,:),2));
    
    figure; hold on;
    h= []; styles= {'k+','r.','b+','g*','gd','y*','yd'};
    COL= [(1:5),[8,9]] ;  COL= COL(COL< size(x,2)); 
    for c= 1:length(COL)
        h(c)= plot(x(:,COL(c)),(1:N)',styles{c});    
    end
    setYlabel(N,self,x);    
    lgd= self.data(1).savedVariable.colnames(COL);
    legend(h',lgd);
    title(ttl)
end

 function plotNContext(self)        
    ttl= ['Number of selected contexts (jacknife) ' self.data(1).savedVariable.NAMEstock ]; %' ' self.data(1).savedVariable.colnames{col}];
    x= arrayfun(@(x) length(unique(x.savedVariable.BN)) -1 ,self.data );
    N= size(x,2);
    figure;
    h= plot(x,(1:N)','b*');    
    setYlabel(N,self,x);
    lgd= {'Number of selected contexts'},
    legend(h(1),lgd);
    title(ttl)
 end
 
 function plotProbSelect(self)        
    ttl= ['Days with selected contexts (jacknife) ' self.data(1).savedVariable.NAMEstock ]; %' ' self.data(1).savedVariable.colnames{col}];
    x= arrayfun(@(x) mean(x.savedVariable.BN>0) ,self.data );  
    N= size(x,2);
    figure;
    h= plot(x,(1:N)','b*');    
    names= arrayfun(@(x) opt2name(x.params),self.data ,'uniformoutput',0);        
    set(gca,'Ytick',1:N)
    set(gca,'Yticklabel',names)    
    lgd= {'P(context selected)'};
    legend(h',lgd);
    title(ttl)
 end

 function profilePlot(self)
    ttl= ['Profile ' self.data(1).savedVariable.NAMEstock ]; %' ' self.data(1).savedVariable.colnames{col}];
    N= length(self.data);
    x=[];   
    prob=[];
    for n=1:N
       idx= self.data(n).savedVariable.BN>0;      
       if all(~idx)
          idx= 1:size(self.vdata,2) ;
       end
       x0  = squeeze(mean(self.vdata(2,idx,:),2));
       x(n,:) = squeeze(mean(self.vdata(n,idx,:),2)) ;%./ x0;  
       prob(n)= mean(self.data(n).savedVariable.BN>0);
    end        
    figure; hold on;
    h= []; styles= {'k+','r.','b+','g*','gd','y*','yd'};
    COL= [(1:5),[8,9]] ;  COL= COL(COL< size(x,2)); 
    for c= 1:length(COL)
        h(c)= plot(prob,x(:,COL(c)),styles{c});    
    end
    %setYlabel(N,self,x);    
    lgd= self.data(1).savedVariable.colnames(COL);
    legend(h',lgd);
    title(ttl)
 end
 
  function [prob,x]= profilePlotSimple(self,name,modeG)
  % ERREUR il faut faire dependre X0 de idx relatif au mod�le....
  if nargin< 3; modeG=1;end
      I=strcmpi(self.data(1).savedVariable.colnames,name);
     I=find(I);assert(length(I)==1);
    ttl= ['Profile ' self.data(1).savedVariable.NAMEstock ]; %' ' self.data(1).savedVariable.colnames{col}];
    N= length(self.data);
    x=[];   
    prob=[];
    
    m0= getM0(self);
    
    for n=1:N
       idx= self.data(n).savedVariable.BN>0;      
       if all(~idx)
          idx= 1:size(self.vdata,2) ;
       end
       %x0  = squeeze(mean(self.vdata(2,idx,:),2));
       x(n) = squeeze(mean(self.vdata(n,idx,I),2)) ;%./ x0;  
       prob(n)= mean(self.data(n).savedVariable.BN>0);
    end     
    x0= min(x(m0));
    x=x/x0;
    if modeG
        figure; hold on;
        h= [];
        h= plot(prob,x(:,I),'k.');    

        %setYlabel(N,self,x);    
        %lgd= self.data(1).savedVariable.colnames(COL);
        %legend(h',lgd);
        title(ttl);
    end
 end
 
 function errorNonContext(self)
 % test seulement pr v�rifier les jours sans ctxt selectionn�
    ttl= ['Profile ' self.data(1).savedVariable.NAMEstock ]; %' ' self.data(1).savedVariable.colnames{col}];
    N= length(self.data);
    x=[];   
    prob=[];
    for n=1:N
       idx= self.data(n).savedVariable.BN==0;      
       if all(~idx)
          idx= 1:size(self.vdata,2) ;
       end
       x0  = squeeze(mean(self.vdata(2,idx,:),2));
       x(n,:) = squeeze(mean(self.vdata(n,idx,:),2))./ x0;  
       %prob(n)= mean(self.data(n).savedVariable.BN>0);
    end        
    figure; hold on;
    h= []; styles= {'k+','r.','b+','g*','gd','y*','yd'};
    COL= [(1:5),[8,9]] ;  COL= COL(COL< size(x,2)); 
    for c= 1:length(COL)
        h(c)= plot(x(:,COL(c)),(1:N)',styles{c});    
    end
    %setYlabel(N,self,x);    
    lgd= self.data(1).savedVariable.colnames(COL);
    legend(h',lgd);
    title(ttl)
 
 end
 
 function bnPlot(self,name,fullSelf)
 self= self.getNotes(self);
 I=strcmpi(self.data(1).savedVariable.colnames,name);
 I=find(I);assert(length(I)==1);
 N=length(self.data);
 idxBestM0= self.bestm0(I);
 x=[];
 for n=1:N
     nts= self.ranks{n};
     for bni= 1:length(nts)
        bn= nts(bni);
        idx= self.data(n).savedVariable.BN==bn;
         x0= mean(fullSelf.data(idxBestM0).savedVariable.value(idx,I),1);
         x(n,bni)= mean(self.data(n).savedVariable.value(idx,I),1) /x0;
     end
 end
 x(isnan(x)) = 0.5;
 figure; hold on;
 matrixplot(x)
 setYlabel(N,self,[])
 
% h= []; styles= {'k+','r.','b+','g*','gd','y*','yd'};
% plot(x);

%COL= [(1:5),[8,9]] ;  COL= COL(COL< size(x,2)); 
% for c= 1:length(COL)
%     h(c)= plot(prob,x(:,COL(c)),styles{c});    
% end
%setYlabel(N,self,x);    
% lgd= self.data(1).savedVariable.colnames(COL);
% legend(h',lgd);
% title(ttl)
 
 
 end
 
 
%% misc
    function s= opt2name(c,remove)
       if nargin== 1; remove={};end
       s=[];
       idx= setdiff(1:2:length(c),2*find(ismember(c(1:2:length(c)), remove))-1 );
       for k=idx
           s= [s ' ' c{k} '=' num2str(c{k+1})];
       end                
    end
    
    function dispnames(self)
        c= arrayfun(@(x) opt2name(x.params),self.data ,'uniformoutput',0);    
        char(c)
    end

%% test
function test0()
f=@(x) strcmpi(x.savedVariable.opt.get('noter'),'ag')
i= arrayfun(f, c.data)


end


function TEST()
ID= 71;
c= compareModels('tbx');
[rm,c]=c.main(ID,36);
load([ 'C:\NPlearn\stock_id_' num2str(ID) '\fileSave~type-MRANK~.mat']);
[r,full]= savedVariable.filter()
r=rm

full.bnPlot(full,'cash 1mn',c)

char(r.rownames)
ns= tiedrank(r.ns);fs= tiedrank(r.fs);nsr= tiedrank(r.nsr)
K= length(r.rownames);



%
figure,
subplot(1,4,2)
hold on;plot(ns,1:K,'k.');plot(fs,1:K,'r+');plot(nsr,1:K,'g*');legend('News only','Full sample','News relative')
subplot(1,4,3)
hold on;plot(r.ns,1:K,'k.');plot(r.fs,1:K,'r+');plot(r.nsr,1:K,'g*');legend('News only','Full sample','News relative')
subplot(1,4,4)
plot(r.nbn,1:K,'k.')

hold on;plot(r.nbn,r.ns,'k.');plot(r.nbn,r.fs,'r+')


subplot(1,4,2)
set(gca,'Ytick',1:K)
set(gca,'Yticklabel',r.rownames)

gname(r.rownames)

%
[~,c]=cov2corr(nancov(r.fs,r.nsr))
[~,c]=cov2corr(nancov(r.ns,r.nsr))

[~,c]=cov2corr(nancov(fs,nsr))
[~,c]=cov2corr(nancov(ns,nsr))




end


function c= removeOptInCell(c,name)
    i= find(strcmpi(c,name));
    assert(length(i)==1);
    c(i+1)=[];
    c(i)=[]; 
end
    
        