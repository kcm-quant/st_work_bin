% compare rankings
as=[2 8 11 18 26 31 41 45 71];

figure;
for n__=1:length(as)
    stockID= as(n__);
    pth= 'C:\st_repository\news_project\NPlearn_REAL_LEARNING_uniscale\';
    pth= [pth,'stock_id_',num2str(stockID)];
    IO= fileToolbox(pth);
    f1= IO.load('type','ranks');

    pth= 'C:\st_repository\news_project\NP_AG\';
    pth= [pth,'stock_id_',num2str(stockID)];
    IO= fileToolbox(pth);
    f2= IO.load('type','ranks');
    f={f1{:},f2{:}};
    r= cellfun(@(x) x.savedVariable.ranks',f,'uni',0);
    R= rank_discord(r);
    %nms
    nms={};
    for n=1:length(f);
        nms{n}= f{n}.savedVariable.opt.get('noter');
        if f{n}.savedVariable.opt.contains_key('dimG')
            nms{n}=[nms{n}, ' ', num2str(f{n}.savedVariable.opt.get('dimG')) ];
        end    
    end
    subplot(3,3,n__);
    matrixplot(R,nms);
    ttl_= ['Rankings discordance. stock=' num2str(stockID)];
    title(ttl_);
end


