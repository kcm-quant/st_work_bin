function x=compare_with_SE_estimator(sec_id,mode_sumary)
x=[];
if nargin< 2;mode_sumary=0;end

if mode_sumary== 1
    x=[];
    x.sec_id= sec_id;
    [x.fs,x.no,x.se_fs,x.se_no,x.nno,x.se_nno]= compare_values(sec_id); 
    %IO.save(mn1,'security',sec_id,'critereOp','sumary');
   return 
end

global st_version;
sdata4nath(sec_id);

fname= ['nath_bt_' num2str(sec_id) '.mat'];
load(fullfile(st_version.my_env.st_repository,'news_project','bt',fname));

x= create_btest(this_sid_s,sec_id);
mn1= critereOpComputer(x,sec_id);

IO= FileToolbox(fullfile(st_version.my_env.st_repository,'news_project','bt_results'));
IO.save(mn1,'security',sec_id,'critereOp','SEmediane');



function x= create_btest(x,sec_id)

x.volume.value= x.pred;
x.volume.phases= x.volume.phase;
rmfield(x.volume,'phase');
x.news= x.volume;
x.IDstock= sec_id;
x.TD={};

x.original_date= [733043,734319];
x.volume.original_date= [733043,734319];
x.from_compare_with_SE_estimator= 1;


function [fs,no,se_fs,se_no,nno,se_nno]= compare_values(sec_id)
global st_version;
identifiant= {sec_id,[],[733043,734319]};

%st_version.my_env.st_repository = 'C:\st_repository';
t=test_learning_output();
[pred,B,R]= t.plot_predictors(identifiant,{'type','FINALmodel','inhibition',1},0);
%file_pars= B.opt.get();

IO= FileToolbox(fullfile(st_version.my_env.st_repository,'news_project','bt_results'));
data= IO.load('security',sec_id,'critereOp','SEmediane');
data=data{1}.savedVariable;

st= stTools();
data.value= data.pognon';
[B_,data]=st.intersect(B,data);
assert(all(B.date==B_.date));

idx= find(strcmpi(B.colnames,'cash 1mn'));assert(length(idx)==1)
fs= mean(abs(B.value(:,idx)));
no= mean(abs(B.value(B.BN>0,idx)));
nno= mean(abs(B.value(B.BN==0,idx)));

se_fs= mean(abs(data.value));
se_no= mean(abs(data.value(B.BN>0)));
se_nno= mean(abs(B.value(B.BN==0,idx)));

function TEST()
ip= indexProcess('CAC40');
stats={};
for x= (ip.comp(:)')
    try
        stats{end+1}= compare_with_SE_estimator(x,1);
    end    
end
save('/quant/context_l_repository/nath_bt.mat')


 %hist(cellfun(@(x)x.fs/x.se_fs,stats),30)
 hist(cellfun(@(x)x.no/x.se_no,stats),30)
mean(cellfun(@(x)x.no/x.se_no,stats))
mean(cellfun(@(x)x.fs/x.se_fs,stats))
mean(cellfun(@(x)x.fs/x.se_fs,stats))

