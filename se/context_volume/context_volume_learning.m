function self= context_volume_learning(varargin)
%  context_volume_learning() : statistical learning for volume repartition
%  contextualized by market and economics events.
%
% main entry point: use 
%     L= context_volume_learning('security_id',2,'trading_destination',4);
%     [failed,base]= L.learn();
%
% Outputs of the learning procedure come in two parts:
%  - A file buffer system located on '\\st_repository\news_project\buffers\'
%    and '\\st_repository\news_project\NPlearn\'
%  - Temporary tables <quant..cc_context_ranking_temporary> and <quant..cc_learningmodel_temporary>
%    These tables gather the only data from buffers that will be send to database
% 
% File buffersystem does not currently support keeping different values of
% trading_destination and/or learning_frequency. Hence before changing these
% parameters, the user needs
%   * save temporary table (will be destroyed)
%   * call L.clear_files() (destroys files with former trading_destination and/or learning_frequency)
%   * use .learn() with the new parameters.
%
%
%

% pas besoin de mnemonic, ce n'est pas du SE !!!
%

self= [];

%% Default arguments
%df= [today-1000,today-10];
%df= [today-200,today-190];
%df= [datenum('20/01/2009','dd/mm/yyyy')-5,datenum('20/01/2009','dd/mm/yyyy')-1];


df= [datenum('30/06/2006','dd/mm/yyyy'),datenum('30/06/2010','dd/mm/yyyy')];
%df= [datenum('30/06/2006','dd/mm/yyyy'),datenum('30/06/2007','dd/mm/yyyy')];

dts= df;


steps_= {'volume','news','ranks','jack0','jack','error','MRANK',...
    'inhibition','IMRANK','nonews','base'};
defaults= {...
    'dates',df, 'buffer_dates',dts,...              % DATES
    'force',0,'old-data',0,'redo-buffer',0,'continue',0,...         % modes forts
    'security_id',[],'trading_destination',{},...   % identifiant titre
    'learning_frequency',15,...                      % param?tres apprentissage
    'thres_nobs_global',20,'thres_nobs',400,'tresh_inhibit',0.95,... % seuils sur les ratio et le nb global d'obs
    'steps',steps_,...
    'folder','',...
    'delete',0}; 

self.options= options(defaults,varargin);
self.options.set('buffer_dates',self.options.get('dates'));

if self.options.get('continue')
    self.options.set('redo-buffer',0);
end
if self.options.get('redo-buffer')
    createNewBuffer();    
else
    createNewBuffer('no_reload');  
end

% */ security= string (index-name or 'all')
ids= self.options.get('security_id');
if ischar(ids)
    if strcmpi(ids,'all')
        ip= indexProcess('CAC40');
        ip2=indexProcess('DAX');
        ids= [ip.comp(:);ip2.comp(:)];
        self.options.set('security_id',ids );
    else
        self.ip= indexProcess(ids);
        self.options.set('security_id',self.ip.comp );
        ids= self.options.get('security_id');
    end
end

CA_format= self.options.get('trading_destination');
if isempty(CA_format)
    non_CA_format= CA_format;
else
    if ~iscell(CA_format); CA_format= {CA_format};end
    assert(length(CA_format)==1);
    non_CA_format= CA_format{1};
end
% MAIN -> TDid. Attention, suppose 1 seul titre!
% Evite de se pourrir au main plus tard
if ischar(non_CA_format)
    assert(strcmpi(non_CA_format,'main'));
    a=get_repository('maintd', self.options.get('security_id')); %returns vector in same order
    if iscell(a); a= cell2mat(a);end % FIXME: remove line
    % non_CA_format = a(1).trading_destination_id;                        
    steps__= self.options.get('steps');
    if (length(steps__)== 1 && strcmp(steps__{1},'base')) || ...
            ischar(steps__) && strcmp(steps__,'base')
        self.options.set('all_td_for_base', a);
    else
        assert(length(self.options.get('security_id'))==1,'multiStock only allowed in ''base'' step')
        assert(length(unique(cell2mat(a)))==1, 'multiTD only allowed in ''base'' step');
    end
    % l'astuce est que les td_id de l'etape base.write() ne sont retrouv?s dans les buffers 
    % l'etape base elle m?me (sans le write) utilise par contre les td_id ???
    non_CA_format= a(1);
end
self.options.set('trading_destination',non_CA_format);

% */ Mode old data (<- labels sont faux dans EventDb)
if self.options.get('old-data')
    format= 'dd-mmm-yyyy';   
    dts(1)= datenum('03-Jan-2006',format);
    dts(2)= datenum('15-May-2011',format);
    df= [] ;
    df(1)= datenum('03-Jan-2007',format);
    df(2)= datenum('29-Jun-2010',format);
    self.options.set('dates',df, 'buffer_dates',dts);
end

% */ force=1 to force learning stock outside indices
if ~self.options.get('force')
    ip= indexProcess('CAC40');
    ip2=indexProcess('DAX');
    champ= [ip.comp(:);ip2.comp(:)];
    clear('ip','ip2');    
    assert(all(ismember(ids,champ)));    
end


if self.options.get('continue')    
    id__= {self.options.get('security_id') ,self.options.get('trading_destination'),self.options.get('dates'),self.options.get('folder') };
    info= CV_identify_phase(id__);   
    self.options.set('steps',info.step);
end
% */ Determines steps
if ischar(self.options.get('steps')) % non cell-array <=> starts from
    assert(ismember(self.options.get('steps'),steps_),char(self.options.get('steps')));
    S= self.options.get('steps');
    n= find(strcmpi(S,steps_));
    self.options.set('steps',steps_(n:end));
end
assert(all(ismember(self.options.get('steps'),steps_)),[char(self.options.get('steps'))]);

% */ Create ip_data
% if isfield(self.ip,'all_td_for_base')
%     
% end
self.ip= indexProcess('CAC40',self.options.get('trading_destination'));
self.ip.comp=  self.options.get('security_id');
self.ip.dates= self.options.get('dates');
self.ip.TD= self.options.get('trading_destination');
self.ip.NminObs= self.options.get('thres_nobs_global');
if ismember('all_td_for_base',self.options.get_keys())
    self.ip.all_td_for_base= self.options.get('all_td_for_base');
end
%% Object
self.learn= @learn;
self.show_learning= @show_learning;
self.mettre_en_base= @mettre_en_base;
self.clear_files= @clear_files;

if self.options.get('delete')
    self.clear_files()
end

%% methods
function [failed,base]= learn()
    failed={};
%     L= list_files(); % DONE files pour multi sec_id
%     if ~isempty(L)
%         if self.options.get('redo-buffer')
%             msg= 'BAD_USE: file system allready exists for (%d,%d). Please use context_volume_learning.clear_files()';
%             msg= sprintf(msg,self.options.get('security_id'),self.options.get('trading_destination'));
%             error(msg,msg); FIX
%         end
%     end
    % ---- Statistical Learning main routine
%     st_log('$off$');    
    % */ create file buffer data
    
    if ismember('volume',self.options.get('steps'))
 %   try
         emp_= eModelPool();x=emp_.definePaveur('intraday');x=options(x{:});
         self.ip.bufferAll(self.ip,self.options.get('buffer_dates'),self.options.get('trading_destination'),x.get('learning_frequency'),self.options.get('folder'),self.options.get('thres_nobs_global'));
 
    
       % self.ip.bufferAll(self.ip,self.options.get('buffer_dates'),self.options.get('trading_destination'),self.options.get('learning_frequency'));
%     catch ME
%        add_error_mnemonic(ME, 'NODATA_FILTER') ;
%     end
    end
%    try
    if ismember('news',self.options.get('steps'))
        self.ip.bufferNews(self.ip,self.options.get('dates'),self.options.get('folder'));    
    end
        %return % FIXME remove
%     catch ME
%         add_error_mnemonic(ME, 'SQL ') ;
%     end


    % ---- Statistical Learning (ranks contexts, jacknife predictors, picks best model)
    %      Results of the learning are buffered in files
    
    % started 41->run inhibit
    %self.ip.comp= self.ip.comp(self.ip.comp> 41);
    %self.ip.comp= self.ip.comp(self.ip.comp> 45)
    %self.ip.comp= self.ip.comp(self.ip.comp~=14334);

    %self.ip.dates= self.options.get('buffer_dates');
    self.ip.path_modifier= self.options.get('folder');
    self.ip.learn_options= self.options;
    indexProcess1by1(self.ip,self.options.get('steps'));
    %indexProcess1by1(self.ip,1); % FIXME: tester cette inhibition
    
    base = 'not created';
    % ---- Creates Database format, does not fill database
    if ismember('base',self.options.get('steps'))
        self.ip.TD= self.options.get('trading_destination'); % useless if ip.all_td_for_base;
        self.ip.date= self.options.get('dates');
        
        allids_= self.options.get('security_id');
        rmv_not_finished= [];
        for k= 1:length(allids_)
            if isfield(self.ip,'all_td_for_base')
                bufferstate_info= CV_identify_phase({allids_(k),self.ip.all_td_for_base(k),self.ip.date,self.options.get('folder')});            
            else
                bufferstate_info= CV_identify_phase({allids_(k),self.ip.TD,self.ip.date,self.options.get('folder')});            
            end
            if (~ strcmp(bufferstate_info.step,'base') || bufferstate_info.substep~=100 )
                rmv_not_finished(end+1)= allids_(k);          
            end
        end
        for x= (rmv_not_finished(:)')
            failed{end+1}= x;            
        end                
        self.ip.comp= setdiff(self.ip.comp,rmv_not_finished);
        if isempty(self.ip.comp)
            base=[];
        else
            [D,b,base,failed]= contexte_learning(self.ip,0,self.options.get('tresh_inhibit'),self.options.get('thres_nobs')); % inhib si >95% de gain et 400 obs mini
        end
    end
        
    % FIXME: if failed ?
    % ---- Fills Database temporary tables
    %base.write(); %add_error_mnemonic(ME, 'SQL') ;    
    
    % sert a quoi ca ??? save le Z dans file ?
%     Z=[];
%     Z.value=base.num;
%     Z.colnames=  {'security_id'  'context'  'rankings'};
%     Z.no_news_string= base.model{1}{2};
%     Z.security_id= sec_id;
%     Z.trading_destination_id= self.options.get('trading_destination');
end


function all_files= list_files()
security_ids= self.options.get('security_id');
TD= self.options.get('trading_destination');
dates_= self.options.get('buffer_dates');
freq= self.options.get('learning_frequency');
folder= self.options.get('folder');


all_files={};
for sec_id= (security_ids(:)')
    IDENT= {sec_id,TD,dates_,folder};  % FIXME : delete Event buffer too
    tbx= newsProjectTbx();
    IO= tbx.FILE(tbx.FILEconfig.folder(IDENT,'stock'));
    current= IO.parse();
    current= cellfun(@(x) fullfile(tbx.FILEconfig.folder(IDENT,'stock'),x),current,'uni',0);
    all_files={all_files{:},current{:}};
    IO= tbx.FILE(tbx.FILEconfig.folder(IDENT,'Buffer'));
    all_buf= IO.parse('stockID',sec_id);
    all_buf= cellfun(@(x) fullfile(tbx.FILEconfig.folder(IDENT,'Buffer'),x),all_buf,'uni',0);
    all_files= {all_files{:},all_buf{:}};
end
end

function clear_files()
all_files= list_files();
if ~isempty(all_files)
    delete(all_files{:});
end
end

end

%%
function TEST()

L= context_volume_learning('security_id',1512703,'trading_destination',[],'redo-buffer',0);%,'old-data',0);
failed= L.learn();
L.clear_files();

%ip= indexProcess('CAC40')
ip= indexProcess('CAC40');
% test la bufferisation
L= context_volume_learning('security_id',ip.comp,'trading_destination',{},'old-data',0);
datestr(L.options.get('buffer_dates'))
failed= L.learn();
%L.clear_files();


L= context_volume_learning('security_id',2,'trading_destination',{'all'},'old-data',0,'dates',[734632 734642], 'buffer_dates',[734632 734642],'folder','test');
datestr(L.options.get('buffer_dates'))
failed= L.learn();

L= context_volume_learning('security_id',6589,'trading_destination',{},'old-data',0,'dates',[733043,734895], 'buffer_dates',[733043,734895],'steps','volume','continue',1);
datestr(L.options.get('buffer_dates'))
failed= L.learn();

end
