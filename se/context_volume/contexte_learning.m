function [D,base,data,failed]= contexte_learning(ip,make_graph,inhib_thresh,nobs_thresh)
% Cree les info qui vont etre enregistr? en base apres le learning
% Le learning a d?ja ete lanc? avec des param?tres pr?sp?cifi?s, on ne
% fait que r?cup?rer les donn?es du learning


D=[];
data=[];
data.num= [];
data.model= {};
data.all_sec_id= [];
failed= [];
base=[];

all_td= options();
all_frequencies= options();
if nargin< 2;make_graph =0;end
if nargin< 3;inhib_thresh =1;end
if nargin< 4;nobs_thresh =-1;end

security_id= ip.comp(:);
tbx= generateFullModel();
%for sec_id= (security_id(:)')
for p_= 1:length(security_id(:)')
    %tbx= newsProjectTbx();
    %filedes= {'type','FINALmodel','inhibition',0,'nonews',0};    
    
    sec_id= security_id(p_);
    if isfield(ip,'all_td_for_base')    
        ip.TD= ip.all_td_for_base(p_);
    end
    
    %% TODO: acceder ? la version string en base de ce modele
    %try
    %% REINHIBER !!!
    temp_= cb_newinfo(ip,sec_id);
    if temp_{2} ~= 4
       DBG=1 ;
    end
        [B,R,D]= tbx.getBasicModel(cb_newinfo(ip,sec_id),'type','FINALmodel','inhibition',1); % learning data
%      catch ME         
%          % FIXME go sur la non inhibante
%          try
%              [B,R,D]= tbx.getBasicModel(sec_id,'type','FINALmodel','inhibition',0);
%          catch ME2
%              disp(ME.message);
%              disp(ME2.message);
%              disp(['failed ' num2str(sec_id) ]);
%              failed(end+1)= sec_id;
%          end
%          
%     end
    try
        B.inhibition_ratio= D.J.inhibition_ratio;
    catch
        B.inhibition_ratio= 'unavailable';
    end
    % B.inhibition_ratio peut etre vide, malgres la pr?sence de inh_tresh
    % dans le cas ou 0 contextes selectionn?s
    if isempty(B.inhibition_ratio)
       B.inhibition_ratio= inhib_thresh; 
    end
    if ismember(sec_id,failed)
        continue;
    end
    
    data.all_sec_id(end+1)= sec_id;
    
    ipp= ip;ipp.comp= sec_id;
    s= model2str(D);
    [x,preds,allR]= createFullRankTable(ipp,make_graph);

    [P,Q]= size(x);
    if isfield(B,'IDdestination')
        all_td.set(sec_id, B.IDdestination);
    else
        if isfield(R,'IDdestination')
            all_td.set(sec_id, R.IDdestination);
        else
            all_td.set(sec_id, R.volume.destinationID);
        end
    end
    
    inh_= [];
    if isfield(B,'inhibitedCtxt')
        inh_= zeros(P,1);
        keeped_ranks= B.ranks(1:P);
        inh_(ismember(keeped_ranks,B.inhibitedCtxt))= 1;
    end
    if isfield(B,'inhibition_ratio')
        if ischar(B.inhibition_ratio)
            assert(strcmpi(B.inhibition_ratio,'unavailable'));
        else
            inh__= zeros(P,1);
            keeped_ratio= B.inhibition_ratio(1:P);
            inh__(keeped_ratio>= inhib_thresh) = 1;
            if ~isempty(inh_) & ~isnan(inhib_thresh)
                assert(all(ismember(find(inh_),find(inh__))));
            end
            inh_=inh__;
        end
    end
    if isempty(inh_);inh_= zeros(P,1);end
    
    if nobs_thresh> 0 & length(B.date)< nobs_thresh
        inh_= ones(P,1);
    end    
    x(:,end+1)= inh_;
    
    base= {};
    for p=1:P
        for q=1:Q        
           base{p,q}= x(p,q); 
        end
        base{p,Q+1}= s;
    end    
    data.num=[data.num;x];
    data.model{end+1}= {sec_id,s};
    % attention pour un bestmodel= NoNews, on risque de perdre ces infos
    all_frequencies.set(sec_id, R.volume.frequency);    
end
data.inhibited= data.num(:,end);
data.num(:,end)=[];
security_id= setdiff(security_id,failed);
%data.inhibited= ones(1,size(data.num,1)); % DONE: le faire vraiment
data.write= @(varargin) write_to_base(data,security_id,all_td,all_frequencies);
data.all_td= all_td;
data.all_frequencies= all_frequencies;

function log_= write_to_base(data,security_id,tds,freqs)
% security_id contient en variable locale les stocks sans erreurs
% donc si aucun idx n'est pr?sent, il faut quand m?me write NoNews (absent de base.num par d?faut)
assert(all(data.all_sec_id(:)==security_id(:)));
log_={};
DB=contexte_volume_dbIO();
ip= indexProcess('CAC40');
for sec_id= (security_id(:)')    
    %[D,b,base,failed]= contexte_learning(sec_id);
    Z=[];
    Z.colnames=  {'security_id'  'context'  'rankings'};
    Z.security_id= sec_id;
    Z.trading_destination_id= tds.get(sec_id);
    Z.frequency= freqs.get(sec_id);
    if isempty(data.num)
        idx=[];
    else
        idx= data.num(:,1)== sec_id;
        % si Z.num est vide, c'est qu'aucun context n'est selectionn?
        % mais UsualDays sera rajout? dans DBwrite(), 
        % ce n'est PAS une erreur
        
        %assert(length(find(idx))>=1);
%         if length(find(idx))==0
%             err_.error= sprintf('stock <%d> had no data !',sec_id);;
%             err_.sec_id= sec_id;
%             log_{end+1}= err_;            
%             continue
%         end
    end
    if 1%any(idx)
        Z.inhibited= data.inhibited(idx);        
        Z.value=data.num(idx,:);    
        i_= [data.model{:,:}];
        idx_= [i_{1:2:end}]== sec_id; 
        assert(length(find(idx_))==1);
        Z.no_news_string= data.model{idx_}{2};        
       % try
            DB.set(Z);
%         catch ME
%             err_=[];
%             err_.error= ME;
%             err_.sec_id= sec_id;
%             log_{end+1}= err_;
%         end
    else
        log_{end+1}= sprintf('stock <%d> was not learned',sec_id);
    end
    
end

