function tbx= contexte_volume_DBaccess(auto_eid)
% Low level access to DB
% FIXME: atomic update, table transitoire ?
% table temporaire pour chaque titre/TD uniquement
%
%
%
tbx=[];
tbx.delete= @delete;
tbx.get_estimator_id=@get_estimator_id;
tbx.update= @update;
tbx.exec= @exec;
tbx.get= @get_;
tbx.eid= @eid;
tbx.suffixTable=@suffixTable;
tbx.get_varargin=@get_varargin;
tbx.check_association= @check_association;
tbx.contexte_num2id= @contexte_num2id;
tbx.map_id_num= @map_id_num;
tbx.delete_all_data=@delete_all_data;

%% Decoration: automatically get estimator_id
% function g= add_varargin_where(f)
%     % cela marche pour WHERE condition, mais pas pour les inserts !
%     function out= decorated_(varargin)
%         v= tbx.get_varargin(varargin{1:2});
%         varg= tbx.exec(v);
%         varg= varg{1}.varargin{1}; 
%         if strcmpi(varg,'null'); varg= 'NULL';end
%         out= f(varargin{:});
%         out= cellfun(@(x) [x ' and varargin = ' varg],out,'uni',0);        
%     end
%     g= @decorated_;
% end
function g= add_varargin_update(f)
    function out= decorated_(varargin)
        v= tbx.get_varargin(varargin{1:2});
        varg= tbx.exec(v);
        if isempty(varg{1})
           if isempty(varargin{2}); varargin{2}=[];end
           msg= 'TITLE_NOT_SUITABLE /BAD_USE: stock <%d,%d,%d> is not in association table';
           msg= sprintf(msg,varargin{1:3});
           error(msg,msg); 
        end
        varg= varg{1}.varargin{1};  
        if strcmpi(varg,'null'); varg= 'NULL';end
        out= f(varargin{:},varg );
    end
    g= @decorated_;
end
tbx.delete= add_varargin_update(tbx.delete);
tbx.get= add_varargin_update(tbx.get); % pour ne pas varargin sur cc_model
tbx.update= add_varargin_update(tbx.update);
%tbx.delete_all_data= add_varargin_update(tbx.delete_all_data);
function id_=  eid()
    r= tbx.get_estimator_id();
    x= tbx.exec(r);
    assert(length(x)==1);
    id_= x{1}.estimator_id;
end
if nargin==0; auto_eid= 1;end
function f= complete_eid(decorated)    
    f= @(varargin) decorated(varargin{:},tbx.eid() );
end
if auto_eid
    tbx.delete= complete_eid(tbx.delete);
    tbx.update= complete_eid(tbx.update);
    tbx.get= complete_eid(tbx.get);
    tbx.get_varargin= complete_eid(tbx.get_varargin);
end


%% Tables and variables
global st_version;
bq = st_version.bases.quant;

clear('TR_','TM_','TE','TA');
TR_= sprintf('%s..context_ranking',bq);
TM_ = sprintf('%s..learningmodel',bq);
TE= sprintf('%s..estimator',bq);
TA= sprintf('%s..association',bq);
%TC= sprintf('%s..context',bq);
EA= sprintf('%s..event_action',bq); % lie un context_num (ce que j'utilise) � un ctxt_id pour un estimateur

TR= TR_;
TM= TM_;
function suffixTable(suf)
    if nargin==0; suf='';end
    TR= [TR_,suf];
    TM= [TM_,suf];
end


EST= 'estimator_id';
SID= 'security_id';
CTX= 'context_id';
EVENT= 'event_type';
TD= 'trading_destination_id';
RK= 'ranking'; % switched from rank
V= ',';
MLI= 'model';
VARG= 'varargin';
FREQ= 'sampling_frequency';

%% Contexte_id to context_num
function R= contexte_num2id()
id_estimator= eid();
r_= ['select * from ' EA ,' where ', EST '= %d and action = ''C'''];  
R={sprintf(r_,id_estimator)};
end
CN_map_request=tbx.contexte_num2id();
CN_map= tbx.exec(CN_map_request);
CN_map=CN_map{1};
function y= map_id_num(x,mode)
    y=[];
    if ischar(x);
        if strcmpi(x,'NULL'); x= nan;
        else;x= str2num(x);    
        end
    end
    if strcmpi(mode,'e2c') % event to context
        for xx= (x(:)')
            if isnan(xx);y(end+1)= CN_map.context_id(find(isnan(CN_map.event_type)));continue;end
            y(end+1)= CN_map.context_id(find(CN_map.event_type==xx));
        end
    elseif strcmpi(mode,'c2e')
        for xx= (x(:)')
            if isnan(xx);y(end+1)= CN_map.event_type(find(isnan(CN_map.context_id)));continue;end
            y(end+1)= CN_map.event_type(find(CN_map.context_id==xx));
        end
    else; assert(0);
    end
    assert(length(y)==length(x));
end


%% couche DB get/update
function R= get_(sec_id, td_id,estimator_id,varg)
td= allow_null(td_id);
varg= allow_null(varg);
R={};
r= ['select * from ' TR ' where ' SID ,' = %d and ' TD ' %s and ' EST '=%d and ' VARG ' %s'];        
R{1}=sprintf(r,sec_id,td,estimator_id,varg);
r= ['select * from ' TM ' where ' SID ,' = %d and ' TD ' %s and ' EST '=%d and ' VARG ' %s'];        
R{2}=sprintf(r,sec_id,td,estimator_id,varg);
end

function R= get_estimator_id()
   R= {['select * from ' TE ,' where estimator_name = ''Context volume'' ']};       
end
function R= get_varargin(sec_id,td_id,est_id)
td= allow_null(td_id);
r_= ['select * from ' TA ,' where ',SID '= %d and ', EST '= %d and ' TD , ' %s'];  
R={sprintf(r_,sec_id,est_id,td)};
end

%--------------------------------------------------------------
function R= delete(sec_id,td_id,estimator_id,varg)
td= allow_null(td_id);
varg= allow_null(varg);
r1= ['DELETE FROM %s WHERE security_id = %s and trading_destination_id %s and ' EST '=%d and ' VARG ' %s'];
r1= sprintf(r1,TR,num2str(sec_id),td,estimator_id,varg);
r2= ['DELETE FROM %s WHERE security_id = %s and trading_destination_id %s and ' EST '=%d'];
r2= sprintf(r2,TM,num2str(sec_id),td,estimator_id);
R={r1,r2};
end

function R= delete_all_data(estimator_id)
r1= ['DELETE FROM %s WHERE ' EST '=%d '];
r1= sprintf(r1,TR,eid());
r2= ['DELETE FROM %s WHERE ' EST '=%d'];
r2= sprintf(r2,TM,eid());
R={r1,r2};
end
%--------------------------------------------------------------
function R= update(sec_id,td_id,learning,estimator_id,varg)
assert(sec_id==learning.security_id);
if isempty(td_id)
    assert(isempty(learning.trading_destination_id));
    td = 'NULL';
else
    assert(td_id==learning.trading_destination_id);
    td = num2str(td_id);
end
if isempty(varg)
    varg = 'NULL';
else
    varg = num2str(varg);
end

[P,~] = size(learning.value);
R={};
r_=['insert into ', TR, '(', SID, V, CTX ,V,EVENT,V, RK, V, TD, V, EST,V,VARG ') values(%d,%s,%s,%d,%s,%d,%s)' ];    
if iscell(r_);r_= cat(2,r_{:});end
for p=1:P
    assert(learning.value(p,1)== sec_id);
    if isempty(learning.value(p,2))
        event__ = 'NULL';
    else
        event__ = num2str(learning.value(p,2));
    end
    if learning.inhibited(p)       
       ctxt__= map_id_num('NULL','e2c');
    else
        ctxt__= map_id_num(str2double(event__),'e2c');       
    end
     ctxt__= num2str(ctxt__);
    %DONE: le 2eme learning.value(p,3) peut etre inhibe
    R{end+1}= sprintf(r_,sec_id,ctxt__,event__,learning.value(p,3),td,estimator_id,varg);    
end
% rajoute nonews dans le table des rankings
event__= 'NULL';
ctxt__= num2str(map_id_num('NULL','e2c'));
if isempty(learning.value)
    rank__ = 10;
else
    rank__= max(learning.value(p,3))+10;
end
R{end+1}= sprintf(r_,sec_id,ctxt__,event__,rank__,td,estimator_id,varg);    

r__= ['insert into ', TM, '(', SID, V, TD, V, EST, V,FREQ , V , MLI,V,VARG ') values(%d,%s,%d,%d,''%s'',%s)' ];    
R{end+1}= sprintf(r__,sec_id,td,estimator_id,learning.frequency,learning.no_news_string,varg);        
end

function R = check_association(sec_id,td_id,est_id,varg)
td= allow_null(td_id);
varg= allow_null(varg);
r_= ['select * from ' TA ,' where ',SID '= %d and ', EST '=%d and ' VARG ' %s and' TD , ' %s'];  
R={sprintf(r_,sec_id,est_id,varg,td)};
end


end
%% main
function z= exec(r)
z= {};
G= 'QUANT';
N= length(r);
for n=1:N
    z{end+1}= generic_structure_request(G,r{n});
end
end

function x= allow_null(x,ref)
if isempty(x) || strcmpi(x,'null')
    x = 'is NULL';
else
    x = strcat('=',num2str(x));
end
if nargin> 1
    ref = allow_null(ref);
    assert(strcmp(x,ref));
end
end


%%
function test()
% Avec auto_id
db=contexte_volume_DBaccess();
db.delete_all_data()

db.suffixTable('_temporary');


db.delete(1512703,[])
r= db.get(276,[]);r{:}
r= db.get_varargin(276,[]);r{:}

r= db.get_estimator_id();r{:}
r= db.delete(Z.security_id,Z.trading_destination_id); r{:}
r= db.update(Z.security_id,Z.trading_destination_id,Z); r{:}
x= db.exec(r)
% Sans auto_id
db=contexte_volume_DBaccess(0);
r= db.get(276,[],15);r{:}
r= db.get_estimator_id();r{:}
r= db.delete(Z.security_id,Z.trading_destination_id,15); r{:}
r= db.update(Z.security_id,Z.trading_destination_id,Z,15); r{:}
x= db.exec(r)

r=db.contexte_num2id()
x= db.exec(r)

i_=db.map_id_num([3,10],'e2c')
db.map_id_num(i_,'c2e')

i_=db.map_id_num([nan],'e2c')
i_=db.map_id_num([nan],'c2e')

end
