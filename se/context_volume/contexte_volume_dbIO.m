function tbx=contexte_volume_dbIO()
% Higher level acess
% See also contexte_volume_DBaccess
% FIXME: get() ne doit pas aller chercher dans les tables temporaires
DB= contexte_volume_DBaccess();

%
% a=get_repository('tdinfo', 110)
% a(1).trading_destination_id

%DB.suffixTable('_temporary');

tbx=[];
tbx.eid= @eid;
tbx.delete= @delete;
tbx.get_estimator_id=@DB.get_estimator_id;
tbx.update= @update;
tbx.exec= @exec;
tbx.get= @get;
tbx.set= @set;

function Z= get(sec_id,td_id)
 r= DB.get(sec_id,td_id);
 x= DB.exec(r);
if isempty(x{1})
    msg= sprintf('TITLE_NOT_SUITABLE: stock <%d,%d> not in table context_learning',sec_id,td_id) ;
    error(msg,msg);
elseif isempty(x{2})
    msg= sprintf('TITLE_NOT_SUITABLE: stock <%d,%d> not in table learningmodel',sec_id,td_id) ;
    error(msg,msg);
end
  Z= [];
  id_= x{1}.security_id(:);
  assert(length(unique(id_))==1);
  id_= unique(id_);
  td_= x{1}.trading_destination_id(:);
  if all(isnan(x{1}.trading_destination_id(:)))
      td_= nan;
  else
    td_= unique(td_);  
    assert(length(unique(td_))==1);
  end
    
  Z.value= [x{1}.security_id(:),x{1}.event_type(:),x{1}.ranking(:)];
  Z.colnames={'security_id'  'context'  'rankings'};
  
  
  
  ctxt__= DB.map_id_num(x{1}.context_id(:),'c2e');
  Z.inhibited= ctxt__(:) ~= x{1}.event_type(:);
  
  % On efface nonews !
  % NB: il n'est plus possible que event_type soit nan, donc le test
  % d'egalit� pour d�finir inhibited ne sera pas feint� par un nan==nan
  no_news= find(isnan(Z.value(:,2)));
  assert(length(no_news)==1);
  Z.value(no_news,:)=[];
  Z.inhibited(no_news,:)=[];
  
  assert(x{2}.security_id== unique(id_));
  if isnan(td_); assert(isnan(x{2}.trading_destination_id))
  else; assert(x{2}.trading_destination_id== td_);
  end
  assert(length(x{2}.security_id)==1 & length(x{2}.model)==1 );
  Z.security_id= unique(id_);
  if isnan(td_); td_= [];end
  Z.trading_destination_id= td_;
  
  Z.no_news_string= x{2}.model{1};
  Z.frequency= x{2}.sampling_frequency;
end

function set(Z)
    % DONE: num2id
    DB.map_id_num(3,'e2c');
    
    r= DB.delete(Z.security_id,Z.trading_destination_id); %r{:};
    x= DB.exec(r);
    r= DB.update(Z.security_id,Z.trading_destination_id,Z);% r{:}
    x= DB.exec(r);
end


end
%%
function TEST
 io=contexte_volume_dbIO;
 
 ZZ= io.get(286,[])
 
 DB= contexte_volume_DBaccess()
 r= DB.exec(DB.get_estimator_id())
 r{1}.estimator_id
 
 plot(ZZ.value')
 
 
end 

