function [x,preds,allR]= createFullRankTable(ip,plot)
% * dessine les courbes de volume des contextes selectionn�s pour un titre si
%   plot=1
% * le c renvoy� est la table de rankings. colonnes= [sec_id,ctxt_id,note]
% * les preds sont calcul�s sans jack sur l'echantillon d'apprentissage.
%

if nargin==0
    x= @TEST;
    return
end

allB={};
plotfun={};
preds={};
allR= {};
assert(length(ip.comp)==1)
for ID= (ip.comp(:))'
    td_= ip.TD;
    if iscell(td_)
        if isempty(td_); td_=[];
        else; td_= td_{1};
        end
    end
    if ischar(td_) & strcmpi(td_,'main')
        x_=get_repository('tdinfo',ID);        
        td_= x_(1).trading_destination_id;
    end
    [preds{end+1},allB{end+1},allR{end+1},plotfun{end+1}]= generateFullModel({ID,td_,ip.dates,ip.path_modifier},'type','FINALmodel','inhibition',1);
end
assert(length(allB)==1);assert(length(allR)==1);
% FIXME: pourquoi y'a-t'il plusieurs B dans allB ???
x= cellfun(@createRanks,allB,'uni',0);
x= [x{:}]';

if plot
    cellfun(@(x) x(), plotfun,'uni',0);
end

end


function x= createRanks(B)
    k=0;x=[];stockID= B.IDstock;
    for ctxt_col = (B.ranks(:)')
        k=k+1;
        IDctxt= B.news.col_id(ctxt_col);
        x(:,end+1)= [stockID;IDctxt;100+k*10];
    end
end



function v= try_get(o,k,v)
% returns default value
    if o.contains_key(k)
        v= o.get(k);
    end
end

function [count,countstock]= TEST(x)
	if nargin< 1
        ip= indexProcess('DAX');
        ip.comp= setdiff(ip.comp,1261636); % DAX
        x= arrayfun(@(x) createFullRankTable(x,0), ip.comp,'uni',0);
    end
    %ip.comp= 2;
%     %ip= indexProcess('DAX');
%     %ip= indexProcess('FTSE 100');
%     dts= [];
%     format= 'dd-mmm-yyyy';
%     %dts(1)= datenum('03-Jan-2008',format);
%     dts(1)= datenum('03-Jan-2006',format);
%      dts(2)= datenum('15-May-2011',format);
    

    count= options();
    count.try_get= @(k,v) try_get(count,k,v);  
    countstock= options();
    countstock.try_get= @(k,v) try_get(countstock,k,v);  

    n= newsDb();
    Nctxt= n.types.size;
    for n=1:length(x)
       xx=x{n};
       P= size(xx,1);
       for p=1:P
            % p est le ranks de la news xx(p,2)
            old= count.try_get(xx(p,2),0);
            new= old+ (Nctxt-p+1)/Nctxt;
            count.set(xx(p,2),new); 
            old= countstock.try_get(xx(p,2),0);
            new= old+ 1;
            countstock.set(xx(p,2),new); 
       end
    end

end

function dispSumRankings()

T=createFullRankTable;
[count,scount]=T();


n= newsDb();

[z,zz]=count.get();
zx= [z{1:2:end}];
zy= [z{2:2:end}];
[sz,szz]=scount.get();
assert(all([z{1:2:end}]==[sz{1:2:end}]))
sy= [sz{2:2:end}];

D= [zx(:),zy(:),sy(:)];

[~,i]= sort(D(:,2),'ascend');
D= D(i,:);

ctxt= n.id2name(D(:,1));
plot(D(:,2)/40,1:length(D(:,1)),'k+'); hold on
plot(D(:,3)/40,1:length(D(:,1)),'r.')
set(gca,'ytick',1:length(D(:,1)))
set(gca,'yticklabel',ctxt)
legend({'Prevalence','Number of selections'});
title('Prevalence of contexts (average over the DAX)')

end



