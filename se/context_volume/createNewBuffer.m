function [state,file]= createNewBuffer(security_id,dates,td,learning_freq,folder,use_no_buffer)
% createNewBuffer(security_id,dates,td) : bufferizes News for one stock
% * dates= [from,to] numeric
%
%

% do_reload = [] ou 'do_not_reload'
% 
% 

file=[];
state= []; % Empty <=>  or 1 <=>
persistent do_reload;% vide ou pas vide
persistent no_buffer;% empty <=> False <=> use buffer [default]
if nargin==0
    do_reload= []; % true
    no_buffer= []; % uses buffers
    return
elseif nargin==1
    switch(security_id)
        case 'no_reload'; do_reload= 'do not reload';  
        case 'no_buffer'; no_buffer= 'no buffer';
        otherwise; assert(false,'wrong mode createNewBuffer()');
    end   
   return
end

    tbx= newsProjectTbx();
    d0=dates(1);
    d1=dates(2);
    
    if ~isempty(do_reload)       
        IO= tbx.FILE(tbx.FILEconfig.folder({security_id,td,dates,folder},'Buffer'));
        l= IO.parse('type','buffer','from',d0,'to',d1,'stockID',security_id,'freq','full');
        if ~isempty(l)
            assert(length(l)==1);
            file= fullfile(tbx.FILEconfig.folder({security_id,td,dates,folder},'Buffer'),l{1});
            l= IO.parse('type','buffer','from',d0,'to',d1,'stockID',security_id,'freq','learning','f',learning_freq);
            if ~isempty(l);state=1;end % state=1 si learning_buffer, [] sinon
            return            
        end        
    end

    if isempty(no_buffer)
    
        IO= tbx.FILE(tbx.FILEconfig.folder({security_id,td,dates,folder},'Buffer'));
        f= IO.load('type','buffer','from',d0,'to',d1,'stockID',security_id,'freq','full');

        if length(f)> 0;
            assert(length(f)==1);
            if ~isempty(do_reload)   
                return % FIXME
            end
        end
    end
    
    TD= td;
    if ischar(TD)
        assert(strcmpi(TD,'MAIN'),num2str(TD))
        a= get_repository('tdinfo',security_id);
        TD= a(1).trading_destination_id;
    end %
    
    DFORMAT= 'dd/mm/yyyy';      
    BIN_SIZE= 1;
    from_ = datestr(dates(1),DFORMAT);
    to_   = datestr(dates(2),DFORMAT);        

    %< gestion options des basic_indicator
    opt4bi = options({'window:time', datenum(0,0,0,0,BIN_SIZE,0), ...
        'step:time', datenum(0,0,0,0,BIN_SIZE,0), ...
        'bins4stop:b', false, ...
        'context_selection:char', 'contextualised',...
        'force_unique_intraday_auction:b',false,...
        'ind_set:char','std+phase'});
    bi_str = ['gi:basic_indicator/' opt2str(opt4bi)];
    STOP_BUFFER= 0;
    d0= datenum(from_,DFORMAT);
    d1= datenum(to_,DFORMAT);
    assert(d1>=d0); % d1>d0
    splits= floor(d1):-50:floor(d0);
    %f= @(x,y) read_dataset(bi_str,'security_id',security_id,'from',x ,'to',y ,'trading-destinations', {},'output-mode', 'day' );
    f= @(x,y) myreaddataset(bi_str,security_id,x,y,TD); % TODO: TD
    buf={};
    for k=2:length(splits)
       % bb est par date croissante, mais la boucle recule           
       % si les donn?es n'existent pas, on arrete la. On espere donc
       % qu'elles arretent d'exister dans le pass?, mais pas qu'il y a
       % un trou
       try
           bb= f(datestr(splits(k)+1,DFORMAT),datestr(splits(k-1),DFORMAT));
       catch ME
           if strcmpi(ME.identifier,'exec:sql');
               STOP_BUFFER= 1;
               break
           else
               rethrow(ME)
           end
       end      
       bb(cellfun(@(x)isempty(x) | prod(size(x.value))==0, bb)) = [];  
       buf={bb{:},buf{:}};
       clear bb;
    end
    %if isempty(buf) | ((splits(end)> d0) && STOP_BUFFER)
    % CHANGED 13/01/2012 -> explique le 29/01/2007 on a oubli? la premi?re tranche
    if isempty(buf) | ((splits(end)> d0)) % && STOP_BUFFER)
        bb= f(datestr(d0,DFORMAT),datestr(splits(end),DFORMAT));
        bb(cellfun(@(x)isempty(x) | prod(size(x.value))==0, bb)) = []; 
        buf={bb{:},buf{:}};
        clear bb
    end

    idx_with_conti= cellfun(@(x) check_continuous_exists(x),buf);        
    
    if isempty(buf(idx_with_conti))
        if isempty(TD); TD=[];end
        msg= sprintf('Vloader.bufferize() : No available data for Stock=<%d>, TD=<%s>, Dates=<%s to %s>, read_datesetmode=<%s>',security_id, TD,from_,to_,bi_str);
        error('Vloader:NoData', msg);            
    end   
    buf= buf(idx_with_conti);
    buf= cellfun(@(x) setfield(x,'frequency',BIN_SIZE),buf,'uni',0);
   
    if isempty(no_buffer)
        IO= tbx.FILE(tbx.FILEconfig.folder({security_id,td,dates,folder},'Buffer'));   
        file= IO.save(buf,'type','buffer','from',d0,'to',d1,'stockID',security_id,'freq','full');
        clear buf;
        state= 0;
    else
        file= buf;
        state= 'direct_load';
    end
    
    
end

function flag= check_continuous_exists(daydata)
    ID_CONTINUE= find(strcmpi({daydata.info.phase_th.phase_name},'CONTINUOUS'));
    if isempty(ID_CONTINUE)
        flag= false;
    else
        if isempty(daydata.date)
            flag = false;
        else
            flag = any(st_data('cols',daydata,'phase') == daydata.info.phase_th(ID_CONTINUE).phase_id);
        end
    end
end

function buf= myreaddataset(bi_str,security_id,from_,to_,TD)
        try
            buf = read_dataset(bi_str,'security_id',security_id,'from',from_ ,'to',to_ ,'trading-destinations', TD,'output-mode', 'day' );
            is_hd = check_half_day(buf);
            buf = buf(~is_hd);
        catch ME
            if isempty(regexp(ME.message, ...
                    'SQL: tick_db_([a-zA-Z0-9]*)_20\d\d\.\.deal_archive([a-zA-Z_0-9]*) not found\.(.*)', 'match'));
                rethrow(ME);
            else
                buf = {st_data('empty-init')};
            end
        end
%         return 
%         try
%             buf= read_dataset(bi_str,'security_id',security_id,'from',from_ ,'to',to_ ,'trading-destinations', TD,'output-mode', 'day' );
%         catch ME
%             done=0;
%             while done== 0
%                 [m_ s_ e_] = regexp(ME.message, 'q._200', 'match', 'start', 'end');
%                 if isempty(s_);
%                     rethrow(ME);
%                 end
%                 q= str2num(ME.message(s_+1));        
%                 y= str2num(ME.message(e_+1));
%                 q= q+1; if q> 4; q=1;y=y+1;end
%                 l= {'jan','apr','jul','oct'};
%                 md_= ['01-' l{q} '-200' num2str(y)];
%                 md= datenum(md_,'dd-mmm-yyyy');
%                 try
%                     buf= read_dataset(bi_str,'security_id',security_id,'from',md ,'to',to_ ,'trading-destinations', TD,'output-mode', 'day' );
%                     done=1;
%                 catch ME
%                     if isempty(regexp(ME.message, ...
%                             'SQL: tick_db_([a-zA-Z0-9]*)_20\d\d\.\.deal_archive_([a-zA-Z]*) not found\.(.*)', 'match'));
%                         rethrow(ME);
%                     else
%                         continue;
%                     end
%                 end
%             end
%         end
end

function is_hd = check_half_day(data)
% is_hd -> true if there is an half trading day
if isstruct(data)
    prim_id = data.info.td_info.trading_destination_id(1);
    beg_date = floor(data.date(1));
    end_date = floor(data.date(1));
else
    i = find(cellfun(@(x)~isempty(x.date),data),1,'first');
    j = find(cellfun(@(x)~isempty(x.date),data),1,'last');
    if isempty(i)
        is_hd = false(length(data),1);
        return
    end
    prim_id = data{i}.info.td_info(1).trading_destination_id;
    beg_date = floor(data{i}.date(1));
    end_date = floor(data{j}.date(1));
end
half_day_dt = exec_sql('KGR',['select EVENTDATE from KGR..CALEVENT',...
    sprintf(' where SCOPEID = %d and SCOPETYPE = 2',prim_id),...
    sprintf(' and EVENTDATE between ''%s'' and ''%s''',datestr(beg_date,'yyyymmdd'),datestr(end_date,'yyyymmdd')),...
    ' and EVENTTYPE = 6']);
if isstruct(data)
    is_hd = ~isempty(half_day_dt);
else
    if isempty(half_day_dt)
        is_hd = false(length(data),1);
    else
        date_list = nan(length(data),1);
        date_list(cellfun(@(x)~isempty(x.date),data)) = cellfun(@(x)floor(x.date(1)),data(cellfun(@(x)~isempty(x.date),data)));
        is_hd = ismember(date_list,datenum(half_day_dt,'yyyy-mm-dd'));
    end
end
end

function X= load1day(security_id,d,td)
assert(0,'Deprectaed');
BIN_SIZE= 1; 
    opt = options({'slice_durations', datenum(0,0,0,0,BIN_SIZE,0), ...
        'indice_volume', false});
%< gestion options des basic_indicator
opt4bi = options({'window:time', opt.get('slice_durations'), ...
    'step:time', opt.get('slice_durations'), ...
    'bins4stop:b', false, ...
    'context_selection:char', 'span_all'});
bi_str = ['gi:basic_indicator/' opt2str(opt4bi)];
try
    X= read_dataset(bi_str,'security_id',security_id,'from',d ,'to',d ,'trading-destinations', td);
    % A= read_dataset(bi_str,'security_id',security_id,'from',d ,'to',d ,'trading-destinations', td,'output-mode', 'day','last_formula','[{opening_auction},{closing_auction},{intraday_auction}]');
    X = st_data('fi', X,{'volume', 'vwap', 'vwas', 'vol_GK', 'phase'});
%     X=X{1};  
    if ~isempty(X.value)
        phases = st_data('col', X, 'phase');
        X = st_data('drop', X,'phase');
%         phases= sum(bsxfun(@times,A{1}.value,[2,5,4]),2);
        phases= false_phase(phases,X.info.phase_th,mod(X.date,1));
        assert(all(phases>=0), 'phase negative !!!' )
    	X.phases=phases;
        X.frequency= BIN_SIZE;
    end
catch ME
    if strcmpi(ME.identifier,'exec:sql');
        STOP_BUFFER= 1;
        X={};
    else
        rethrow(ME)
    end
end

end