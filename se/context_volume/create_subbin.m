function [idx,t1,p1,ta,pa]= create_subbin(freq,t0,p0,subfreq)
% Sous-grille r�guli�re. create_subbin(freq,t0,p0,subfreq) sous sample la grille (t0,p0) selon le
% quotient subfreq/freq
%
% (t0,p0):= times et phases initiales, � fr�quence freq
% (t1,p1):= la sous grille de (t0,p0), � fr�quence subfreq
% (ta,pa):= Auctions. Les phases non continues sont trait�es � part (pas de sous samplage)
%
% Desaggrege les dates d'un signal de la fr�quence freq � la frequence subfreq< freq
% desaggregation exacte, r�partit uniform�ment les bins continus en splittant le temps
%
%
% See also dispatchSubbin_v2 , qui s'occupe de la desaggregation des donn�es 
% � proprement parler
% idx(k)== 1 <=> p0(k)==0
% (t1,p1) : dates et phases extrapol�es pour la partie continue
% (ta,pa) : dates et phases extrapol�es pour la partie auction

 [no_use,phase_id,no_use,from_phase_id_to_phase_name] = trading_time_interpret([],[]);
 CID= phase_id.CONTINUOUS;

n= subfreq/freq;
assert(n==mod(n,1))

N = length(t0);
assert(length(p0)==N);

t1=[];
p1=[];
idx= p0==CID;
tc= t0(p0==CID);
M= length(tc);
for m=1:M
    add= (tc(m)-(1/n-1)*subfreq):subfreq:tc(m);
    t1= [t1,add ];
    p1= [p1, CID *ones(1,length(add))];
end

[a,b,c]= unique(round(t1*24*60));
id_suppr= setdiff(1:length(t1),b);
t1(id_suppr)=[];
p1(id_suppr)=[];

ta= t0(p0 ~= CID);
pa= p0(p0 ~= CID);
% M= length(ta)
% for m=1:M
%    t1= [t1, ta(m)];
%    p1= [p1, pa(m)];        
% end


