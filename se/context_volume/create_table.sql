
use quant
go

/*******************************************************************************************************/
/********************************************Version 1.0************************************************/
/*******************************************************************************************************/


/******************************TABLES********************************/

print "Creation de la table cc_context_ranking"
go

create table cc_context_ranking
(
  security_id   int not null,
  context_id    int not null,
  rankings      int not null,  
)
lock DataRows
go

create unique nonclustered index id on cc_context_ranking(security_id,context_id)
go

grant select,insert,update,delete  on cc_context_ranking  to cdv_users
go
grant select   on cc_context_ranking  to sel_users
go

print "Creation de la table cc_learningmodele"
go

create table cc_learningmodele
(
  security_id int identity not null,
  model varchar(255),  
)
lock DataRows
go

create unique nonclustered index id on cc_learningmodele(security_id)
go

grant select,insert,update,delete  on cc_learningmodele  to cdv_users
go
grant select   on cc_learningmodele  to sel_users
go



