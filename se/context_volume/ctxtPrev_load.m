function tbx= ctxtPrev_load()
% va chercher le bon fichier ?
% use ctxtPrev_load(ID,'inhibition',0)

tbx=[];
tbx.compareModels= @compareModels;
tbx.load= @load_;
end

function x= load_(varargin)
    tbx= newsProjectTbx() ;     
    x= nan;    
    full_info_ID= varargin{1};
    ID= full_info_ID.stockID;
    varargin=varargin(2:end);    
    if isstruct(ID)
        % on doit utiliser filetrace directement sur l'objet ?
        % TODO: l'encapsuler    
        x=[];
        x.best= ID;
        try;ID= x.best.IDstock;
        catch; x.best.IDstock= x.best.stockID;
        end
        x.M0= varargin{1};
        varargin={};
        assert(isnumeric(ID));
        assert(length(ID)==1);
    else
        assert(isnumeric(ID));
        assert(length(ID)==1);
        IO= tbx.FILE(tbx.FILEconfig.folder(full_info_ID,'stock'));
        x= IO.loadSingle('type','BESTmodel',varargin{:}); % inhibiton-1 
        x= x{1}.savedVariable;
    end
    opt= options(varargin);
    j0= tbx.getFromFileTrace(x.M0.filetrace,'jack0Save',full_info_ID);
    x.j0=j0.savedVariable; 
    if x.best.filetrace.contains_key('jackSave')
        j = tbx.getFromFileTrace(x.best.filetrace,'jackSave',full_info_ID);      
    %elseif x.best.filetrace.contains_key('jackSave')
    else
        assert(isnan(x.best.opt.get('seuil')));
        % pourquoi note=mpv ???
        j=j0;
    end
   x.j=j.savedVariable;

    % BN comprends des num�ros de colonnes, et non pas les indices dans
    % rank !!!
    u= unique(x.best.BN);
    u= setdiff(u,[0]);
    for i= (u(:))';
       assert(all( x.best.news.value(x.best.BN==i,i ))); 
    end

    assert(all(x.j.news.value(:)== x.j0.news.value(:) ));
    assert(all(x.best.news.value(:)== x.j0.news.value(:) ));
    assert(all(x.M0.news.value(:)== x.j0.news.value(:) ));
    assert(all(all( char(x.j.news.colnames)== char(x.j0.news.colnames) )));
    assert(all(all( char(x.best.news.colnames)== char(x.j0.news.colnames) )));
    assert(all(all( char(x.M0.news.colnames)== char(x.j0.news.colnames) )));

    %assert(all(x.j.volume.value(:)== x.j0.volume.value(:) ));    
    assert( optEquality(x.j.opt,x.best.opt, {'type','inhibition'} ) );
    assert( optEquality(x.j0.opt,x.M0.opt, {'type','inhibition','noter','seuil'} ) );
    if  ~opt.contains_key('inhibition') ||  opt.get('inhibition')== 0
        assert(all(x.best.BN==  x.j.BN ));  
    end
    
    if isfield(x.best,'ranks')
        assert(all( x.best.ranks== x.j.ranks ));    
    else
        R = tbx.getFromFileTrace(x.best.filetrace,'rankLoad',full_info_ID);  
        R= R.savedVariable;
        assert(all( R.ranks== x.j.ranks ));
        x.best.ranks= R.ranks;
    end
end

%function compareModels(x,P)

function compareModels(best,M0,j,j0,P)
x=[];
x.best= best; x.M0=M0; x.j=j; x.j0= j0;

%getIdx= @(p) x.j.indiceEffectif(:,p)==1;
getIdx= @(p) x.best.BN==x.best.ranks(p);

% chartableau(x.best.news.colnames(x.best.ranks),sum(x.best.news.value(:,x.best.ranks)), sum(x.j.indiceEffectif) ) 
critere= 'cash 1mn';
C= strcmpi(x.best.colnames,critere);C= find(C); assert(length(C)==1);
f1=figure;
if nargin==4;
    P= length(x.best.ranks);   
    Pparcouru= 1:P;
    f2=figure;
    f3= figure;
else; Pparcouru= P;
end
for p= Pparcouru
    col= x.best.ranks(p);
    name= x.best.news.colnames{col};
    ttl_= ['(' num2str(p) ')' name];
    if  isfield(x.best,'inhibitedCtxt')
        if ismember(col, x.best.inhibitedCtxt)
            ttl_=[ttl_ ,' Inhib'];
        end
    end
    % volumes moyens
    if nargin ==1
        figure(f1);
        subplot(5,5,p);
    else
        subplot(3,1,1);
    end    
    idx= getIdx(p);%x.j.indiceEffectif(:,p)== 1;    
    
    hold on
    plot( mean(x.j.volume.value(idx,:),1) ,'k.-');
    plot( mean(x.j0.pred(idx,:),1) ,'b.-');
    plot( mean(x.j.pred(idx,:),1) ,'r.-');
    title(ttl_);

    % tous les volumes
    if nargin ==1
        figure(f2);
        subplot(5,5,p);
    else
        subplot(3,1,2);
    end
    idx= getIdx(p);%x.j.indiceEffectif(:,p)== 1;
    col= x.best.ranks(p);
    name= x.best.news.colnames{col};
    hold on
    plot( x.j.volume.value(idx,:)' ,'k.-');
    plot( x.j0.pred(idx,:)' ,'b.-');
    plot( x.j.pred(idx,:)' ,'r.-');
    title(ttl_);

    % erreurs
    if nargin ==1
        figure(f3);
        subplot(5,5,p);
    else
        subplot(3,1,3);
    end
    idx= getIdx(p);%x.j.indiceEffectif(:,p)== 1;
    col= x.best.ranks(p);
    name= x.best.news.colnames{col};
    hold on
    plot( mean(x.best.value(idx,C),1) ,'r.-');
    plot( mean(x.M0.value(idx,C),1) ,'b.-');
    title(ttl_);
end

end

function b= optEquality(o1,o2,except)
if nargin== 2;except= {};end
oo1= o1.clone(); oo2= o2.clone();
for k=1:length(except)
    if oo1.contains_key(except{k}) ;oo1.remove(except{k});end
    if oo2.contains_key(except{k}) ;oo2.remove(except{k});end
end
b= char(cellfun(@num2str,oo1.get(),'uni',0)) == char(cellfun(@num2str,oo2.get(),'uni',0));
b= all(b(:));
end


function TEST_()
tbx= ctxtPrev_load();
x= tbx.load(92,'inhibition',0);
tbx.compareModels(x.best,x.M0,x.j,x.j0,1);

%%
%load('C:\st_repository\news_project\NPlearn\stock_id_92\fileSave~inhibition-0~Npoint-10~sbreak-0~noter-mpv~type-error~dim-36~seuil-0.05~estimateur-mean~.mat')
load('C:\st_repository\news_project\NPlearn\stock_id_92\fileSave~inhibition-0~Npoint-10~sbreak-0~noter-mpv~type-error~dim-36~seuil-0.01~estimateur-median~.mat')
m1= savedVariable;
load('C:\st_repository\news_project\NPlearn\stock_id_92\fileSave~inhibition-0~Npoint-10~noter-None~type-error~dim-36~estimateur-median~.mat')
m0=savedVariable;

tbx= ctxtPrev_load();
x= tbx.load(m1,m0);
tbx.compareModels(x.best,x.M0,x.j,x.j0,1);

%%
load('C:\st_repository\news_project\NPlearn\stock_id_92\fileSave~type-jack~Npoint-10~noter-mpv~dim-36~estimateur-median~seuil-0.01~sbreak-0~.mat')
m1= savedVariable;
load('C:\st_repository\news_project\NPlearn\stock_id_92\fileSave~type-jack~Npoint-10~noter-None~dim-36~estimateur-median~.mat')
m0=savedVariable;

erc=errorComputer();
[er0,mn0]= erc.applyWithoutFileSave(m0);
[er1,mn1]= erc.applyWithoutFileSave(m1);

idx= m0.news.value(:,2)==1;
mean(er0.value(idx,4))
mean(er1.value(idx,4))

%%
pT= size(mn1.err,2);
pT=400:pT;
pT= 1:400

mean(abs(sum(mn1.err(idx,pT).*mn1.W.value(idx,pT),2)) ./ mn1.Wdaily(idx))
mean(abs(sum(mn0.err(idx,pT).*mn0.W.value(idx,pT),2)) ./ mn0.Wdaily(idx))

mean(sum(abs(mn1.err(idx,pT).*mn1.W.value(idx,pT)),2) ./ mn1.Wdaily(idx))
mean(sum(abs(mn0.err(idx,pT).*mn0.W.value(idx,pT)),2) ./ mn0.Wdaily(idx))

mean(sum( (mn1.err(idx,pT).*mn1.gk.value(idx,pT)).^2  ,2))
mean(sum( (mn0.err(idx,pT).*mn0.gk.value(idx,pT)).^2  ,2))


idx=er0.news.value(:,2)==1;
it= mod(datenum(mn0.vol.colnames),1);
figure,hold on;plot(it,mn0.vol.value(idx,:),'k');plot(it,mn0.pred(idx,:),'b');plot(it,mn1.pred(idx,:),'r')
figure,plot(it,bsxfun(@times, mn1.W.value(idx,:),1./ mn1.W.value(idx,1) ) )
%%
abs(sum(mn1.err(:,:).*mn1.W.value(:,:),2)) ./ mn1.Wdaily(:,:)< abs(sum(mn0.err(:,:).*mn0.W.value(:,:),2)) ./ mn0.Wdaily(:,:)

%% C.I avec sous-plage al�atoire
MC=1000;
pT= size(mn1.err,2);
u_= floor(pT*rand(1,MC))+1;
v_= floor(pT*rand(1,MC))+1;
u= min(u_,v_);v= max(u_,v_);
ci0=[];ci1=[];
for mc=1:MC
   times_= u(mc):v(mc);
   ci1(mc)=  mean(abs(sum(mn1.err(idx,times_).*mn1.W.value(idx,times_),2)) ./ mn1.Wdaily(idx));
   ci0(mc)=  mean(abs(sum(mn0.err(idx,times_).*mn0.W.value(idx,times_),2)) ./ mn0.Wdaily(idx));
end
plot(ci0,ci1,'k.')
xl=get(gca,'xlim')
line([xl(1),xl(2)],[xl(1),xl(2)])

h= plotpoint(ci0,ci1)
degradeHandle(h,v-u);

%% C.I avec sous-plage multi�chelle
idx= 1:size(mn1.err,1); %mettre que les news ?

pT= size(mn1.err,2);
allf= [15,30,60,120,240,360,480,];
c1={};
c0={};
Nf= length(allf);
% ne pas le faire pour close et open, mais il faut les inclure !
for nf=1:Nf
    c1{nf}= [];c0{nf}= [];
    f= allf(nf);
    allt0= 1:pT-f;
    Nt= length(allt0);
    for nt=1:Nt
       u= allt0(nt); v= u+f; times_= u:v;
       c1{nf}(nt)= mean(abs(sum(mn1.err(idx,times_).*mn1.W.value(idx,times_),2)) ./ mn1.Wdaily(idx));
       c0{nf}(nt)= mean(abs(sum(mn0.err(idx,times_).*mn0.W.value(idx,times_),2)) ./ mn0.Wdaily(idx));                
    end
end

cmp= cool(Nf);
figure, hold on;
for nf=1:Nf
   plot(c0{nf},c1{nf},'marker','.','linestyle','none','color',cmp(nf,:)) ;
end
xl=get(gca,'xlim');
line([xl(1),xl(2)],[xl(1),xl(2)]);
legend(num2str(allf'))

title(['multiscale O.C. stock=' num2str(er1.IDstock) ' obs= all'])

mean(cellfun(@mean,c0))
mean(cellfun(@mean,c1))




end