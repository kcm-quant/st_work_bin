function data = cvc_week_perf(mode, varargin)
% CVC_WEEK_PERF - CVC weekly perf analysis
%
%
%
% Examples:
%
% cvc_week_perf('std', 'security_id', 110, 'trading-destinations', {}, 'day', datenum(2012,4,20,0,0,0))
%
%
% See also:
%
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   date     :  '27/04/2012'
%
%   last_checkin_info : $Header: cvc_week_perf.m: Revision: 7: Author: robur: Date: 05/13/2013 05:12:48 PM$





switch lower(deblank(mode))
    case 'masterkey'
        %<* Return the generic+specific dir
        fmode = varargin{1};
        fmode = regexprep( fmode, '[ |*|\\|/|:|?|"|<|>|\|]', '_');
        opt = options({'security_id', NaN}, varargin(2:end));
        sec_id = opt.get('security_id');
        td_id = opt.get('trading-destinations');
        assert(~isnumeric( sec_id)&numel(sec_id)==1,'cvc_week_perf:secid','I need an unique numerical security id');
        if (iscell(td_id) && strcmpi(td_id{1},'MAIN')) || (ischar(td_id) && strcmpi(td_id,'MAIN'))
            td_id = get_repository('maintd',sec_id);
        end
        key = get_repository( 'security-td-key', sec_id, td_id);
        data = fullfile(fmode,key);
        %>*
        
    case 'std'
        opt = options({'day', NaN,...
            },varargin);
        sec_id = opt.get('security_id');
        td_id = opt.get('trading-destinations');
        d = opt.get('day');
        if ischar(d)
            d = datenum(d, 'dd/mm/yyyy');
        end
        curr_fow = friday_of_week(today);
        if weekday(d)~=6
            error('cvc_week_perf:check_args', 'Only Fridays can be asked');
        elseif d > curr_fow
            error('cvc_week_perf:check_args', 'Please wait for the end of week');
        elseif d == curr_fow
            if now < friday_of_week(d) + datenum(0,0,0,32,0,0);
                error('cvc_week_perf:check_args', 'Please wait saturday 8PM');
            end
        end
        prev_sunday_str = datestr(d-5, 'dd/mm/yyyy');
        
        optbi = options({ 'window:time', datenum(0,0,0,0,5,0),...
            'step:time', datenum(0,0,0,0,5,0), ...
            'bins4stop:b', false, ...
            'output-mode', 'day', ...
            'context_selection:char', 'span_all', ...
            });
        
        curr_market_data = read_dataset(['gi:basic_indicator/' opt2str(optbi)], 'security_id', sec_id, ...
            'trading-destinations', td_id, 'from', d-5, 'to', d, 'output-mode', 'day');
        curr_market_data = cellfilter(@(c)~st_data('isempty-nl', c), curr_market_data);
        
        if isempty(curr_market_data)
            data = st_data('empty-init');
            return;
        end
        
        past_market_data = read_dataset(['gi:basic_indicator/' opt2str(optbi)], 'security_id', sec_id, ...
            'trading-destinations', td_id, 'from', d-40, 'to', d-5, 'output-mode', 'day');
        past_market_data = cellfilter(@(c)~st_data('isempty-nl', c), past_market_data);
        if length(past_market_data) < 20
            data = st_data('empty-init');
            return;
        end
        past_market_data = past_market_data(end-19:end); % keeping only the last 20 days
        
        if isempty(td_id)
            td_id = []; % fucking get_se
        end
        % It is less dangerous to use temporary_bufferize than the buffer
        % of get_se
        key_file = get_repository( 'kech-security-td-key', sec_id, td_id);
        
        se_vc = vcse2vcdata(temporary_bufferize('get_se', {'security','estimator', 'volume curve' , ...
            'security_id' , sec_id,...
            'trading_destination_id',td_id,...
            'is_valid', 1 , 'backup_process', 'default', ...
            'as_of_date', prev_sunday_str}, 'not_too_much_files', key_file), ...
            datenum(0,0,0,0,5,0));
        
        % < Spanning SE_VC curves over the span_all contexts grid. 
        % Here it is not generic but dedicated to UK's Derivatives Expiry
        idx_iaf = find(st_data('col', curr_market_data{end}, 'intraday_auction'));
        if ~isempty(idx_iaf) && ~any(st_data('col', se_vc, 'intraday_auction'))
            idx_col_ia = strcmp(se_vc.colnames, 'intraday_auction');
            tmp = zeros(1, length(se_vc.colnames));
            tmp(idx_col_ia) = 1;
            se_vc.value = [se_vc.value(1:idx_iaf-1, :);tmp;se_vc.value(idx_iaf:end, :)];
            se_vc.date = [se_vc.date(1:idx_iaf-1, :);...
                mod(curr_market_data{end}.date(idx_iaf), 1);se_vc.date(idx_iaf:end, :)];
        end
        % >
        
        cvc  = vcse2vcdata(temporary_bufferize('get_se', {'security','estimator', 'context_volume' , ...
            'security_id' , sec_id,...
            'trading_destination_id',td_id,...
            'is_valid', 1 , 'backup_process','none', ...
            'as_of_date', prev_sunday_str, ...
            'retrieve_all_context', true}, 'not_too_much_files', key_file), ...
            datenum(0,0,0,0,5,0));
        
        % TODO cv indice du jour?
        % refindex = get_repository('refindex', 'security_id',110,'ref_type','place','output_mode','name');
        
        c2a = get_contexts2be_applied(sec_id, 'from', d-5, 'to', d);
        
        past_volumes =[];
        for i = 1 : length(past_market_data)
            past_volumes = cat(1, past_volumes, st_data('col', past_market_data{i}, 'volume')');
        end
        curr_volumes = [];
        curr_vwap = [];
        curr_dates = [];
        for i = 1 : length(curr_market_data)
            curr_volumes = cat(1, curr_volumes, st_data('col', curr_market_data{i}, 'volume')');
            curr_vwap = cat(1, curr_vwap, fill_nan_v2(st_data('col', curr_market_data{i}, 'vwap')', 'mode', 'before'));
            curr_dates = cat(1, curr_dates, curr_market_data{i}.info.data_datestamp );
        end
        
        volumes4spvo = cat(1, past_volumes, curr_volumes);
        volumes4spvo = volumes4spvo(end-4:end, :);
        
        dvwap = sum(curr_volumes.*curr_vwap, 2)./sum(curr_volumes, 2);
        svpo_vc = mean(bsxfun(@times, volumes4spvo, 1./sum(volumes4spvo, 2)));
        p20vp_vc = mean(bsxfun(@times, past_volumes, 1./sum(past_volumes, 2)));
        se_vc = getfield(st_data('drop', se_vc, {'opening_auction','intraday_auction','closing_auction'}), 'value')';
        cused = c2a.context2use(ismember(c2a.date, curr_dates));
        cvc_vc   = getfield(st_data('keep', cvc, cused), 'value')';
        
        sum_vc = sum(cat(1, svpo_vc, p20vp_vc, se_vc, cvc_vc), 2);
        assert(all(sum_vc>0.999 & sum_vc<1.0001))
        
        svpo_vc = svpo_vc/sum(svpo_vc, 2);
        p20vp_vc = p20vp_vc/sum(p20vp_vc, 2);
        se_vc = se_vc/sum(se_vc, 2);
        cvc_vc = bsxfun(@times, cvc_vc, 1./sum(cvc_vc, 2));
        
        svpo_slippages  = 1e4*(sum(bsxfun(@times, curr_vwap, svpo_vc ), 2)./dvwap - 1);
        p20vp_slippages = 1e4*(sum(bsxfun(@times, curr_vwap, p20vp_vc), 2)./dvwap - 1);
        sevc_slippages  = 1e4*(sum(bsxfun(@times, curr_vwap, se_vc   ), 2)./dvwap - 1);
        
        cvc_slippages = 1e4*(sum(curr_vwap.*cvc_vc, 2)./dvwap - 1);
        
        data = st_data('init', 'title', sprintf('slippages for week %s to %s', ...
            datestr(d-4, 'mm/dd/yyyy'), datestr(d, 'mm/dd/yyyy')), ...
            'colnames', {'svpo_slippages', 'p20vp_slippages', 'sevc_slippages', 'cvc_slippages', 'volume', 'turnover'}, ...
            'value', [svpo_slippages, p20vp_slippages, sevc_slippages, cvc_slippages, sum(curr_volumes, 2), sum(curr_volumes, 2).*dvwap], ...
            'date', curr_dates, 'info', curr_market_data{end}.info);
        
        data.info.full_data = {svpo_vc, p20vp_vc, se_vc, cvc_vc, past_volumes, curr_market_data, cused'};
    otherwise
        
        idx = strfind(mode, ':');
        if strcmp(mode(1:idx-1), 'buffer')
            opt = options(varargin);
            opt.set('from', friday_of_week(opt.get('from')), ...
                'to', friday_of_week(opt.get('to')));
            varargin_ = opt.get();
            data = from_buffer('cvc_week_perf', mode(idx+1:end), varargin_{:}, 'frequency', 7, 'output-mode', 'day');
            data(cellfun(@(c)st_data('isempty-nl', c), data, 'uni', true)) = [];
            rownames = cellflat(cellfun(@(c)c.info.full_data{1, end}, data, 'uni', false))';
            data = st_data('fast-stack', data);
            data.rownames = rownames;
            return;
        end
        error('Untitled2:mode', 'MODE: <%s> unknown', mode);
        
end

end

function d = friday_of_week(d)
wd = weekday(d);
if wd == 1
    d  = d - 2;
else
    d = d-wd+6;
end
end