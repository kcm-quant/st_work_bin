function [dispatched,disp_from]= dispatchSubbin_v2(data,t1,t0,p1,p0)
% Extrapole les data, depuis la grille (t1,p1) sur (t0,p0).
% t0 plus fin que t1 (t0<t1)
% 
% (t,p)= (times,phases)
%
% See also create_subbin (permet de cr�er une sous grille r�guli�re (t1,p1) )
%
% si range(t1)> range(t0), on a des nan => erreurs
% si range(t1)< range(t0), on extrapole que les bins intersectant les
% ranges, et on renormalise 
%
% Exemple: [dispatched,disp_from]= dispatchSubbin_v2(ones(10,1),0:9,(0:99)/10,zeros(10,1),zeros(100,1))
% attention si t1(1)== t0(1), comme dans l'exemple ci dessus
%
% avec des nans: [dispatched,disp_from]= dispatchSubbin_v2(ones(10,1),0:9,(0:99)/5,zeros(10,1),zeros(100,1))


 [no_use,phase_id,no_use,from_phase_id_to_phase_name] = trading_time_interpret([],[]);
 CID= phase_id.CONTINUOUS;


% data= data(p1==CID);
% t0= t0(p0==CID); t1= t1(p1==CID);

assert(issorted(t1))
assert(issorted(t0))

p0=p0(:);p1=p1(:);
t0=t0(:);t1=t1(:);
% t0 =t0 + max(diff(t1))*0.001;
% i= bsxfun(@ge,t0,t1');
% i= [zeros(1,length(t1));i];
% ii= sum(i,2);

% t1=t1(1:3)
% t0=t0(1:50)


% il faudrait retirer de t0 les non continuous
% -> pas la peine, tant qu'on laisse Nan, ca va sauter les indices faux continus 1:01, 1:02

epsilon= 1*1/24/60/60;
d_t0= round(t0*24*60);
d_t1= round(t1*24*60);
epsilon= 0.2;

i1= bsxfun(@gt,d_t0,d_t1(1:end)');%-epsilon);
i2= bsxfun(@lt,d_t0,d_t1(1:end)'+epsilon);

% i1= bsxfun(@gt,t1(1:end-1),t0');%-epsilon);
% i2= bsxfun(@lt,t1(2:end),t0')%+epsilon);

i=[i2(:,1),i1(:,1:end-1)&i2(:,2:end),i1(:,end)];
% i(p,q) = 1 si le bin original t1 se dispatch sur t0(p)
% une colonne= les endoits ou le bin original sera dispatch�

assert(all(sum(i,2)<=1));

dispatched= nan(size(t0))';
disp_from=  nan(size(t0))';

done=[];
for k=1:length(t1)
    col= i(:,k);
    empl= find(col);
    %if length(empl)> 1; empl= empl(2:end);end
    empl= setdiff(empl,done);
    %assert(ismember(length(empl),[1 15]) | all(p0(empl)~=0))    
    if p1(k)~=CID && p1(k)~=-1 
        %empl= empl(p0(empl)== p1(k)); % necessite que les times correspondent sur IA
        empl= find(p0== p1(k) ); % autorise inversion des dates sur la I.A.
    else
        empl= empl(p0(empl)==CID);        
    end
    dispatched(empl)= data(k) / length(empl);
    disp_from(empl)= k;
    done= [done;empl];
end


% done=[];
% for k=1:length(t1)
%     col= i(:,k);
%     empl= find(col);
%     %if length(empl)> 1; empl= empl(2:end);end
%     empl= setdiff(empl,done);
%     %assert(ismember(length(empl),[1 15]) | all(p0(empl)~=0))    
%     if p1(k)~=CID && p1(k)~=-1 
%         %empl= empl(p0(empl)== p1(k)); % necessite que les times correspondent sur IA
%         empl= find(p0== p1(k) ); % autorise inversion des dates sur la I.A.
%     else
%         empl= empl(p0(empl)==CID);        
%     end
%     dispatched(empl)= data(k) / length(empl);
%     disp_from(empl)= k;
%     done= [done;empl];
% end

%d(isnan(d))=0
%stairs(mod(q.v0.date,1),cumsum(d),'color','r'); hold on;stairs(mod(datenum(q.v1.colnames),1),cumsum(q.pred))


function TEST()
% en debug dans load_one_day
find(X{end}.phases==4)
find(X{1}.phases==4)
chartableau([1:10,240:250],datestr(X{1}.date([1:10,240:250])),X{1}.phases([1:10,240:250]),datestr(X{end}.date([1:10,240:250])),X{end}.phases([1:10,240:250]))

uo_(cellfun(@(x) x.value(243,:),X,'uni',0))
uo_(cellfun(@(x) x.value(248,:),X,'uni',0))
X{end-1}.value([244 247],:)


