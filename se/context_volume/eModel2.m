function self= eModel2(varargin)
% eventModel2 : forecasts volume using contexts (events and news)
% PROJECT: context sensitive volume forecast
% v.2 : takes context destruction into account with recursive ranks.
%
% A/ Rankings of contexts
%    - Recursive rankings: selects the best context, based on remaining
%    data in the sample (i.e not previsouly used by another selected context)
%    - Can use different ranking functions (in self.rankers )
%    - Exclude contexts with too few remaining observations (self.opt.get('Npoint') )
%    - Rankings only defines an order on contexts, it does not define an estimator
%    - Keeps Effective indices (partially destroyed context) for the
%      jacknife step
%
% B/ Jacknife
%    - Outliers are removed from the full sample before the jacknife
%     (removed from learning data only, but still forecast)
%    - The jacknife can be performed for various estimator/predictor (in self.estimators)
%    - The benchmark model (non contextual forecast) is jacknifed first.
%    - The contextual model is jacknifed, applying the predictor
%      corresponding to the best ranked conetxt occuring each day.
%    - Contexts whose note is below a threeshold (self.opt.get('seuil'))
%      are not applied, and the benchmark forecast is used instead.
%    - The context selection stops as soon as any context is below the threeshold,
%      except if self.opt.get('sbreak')==1
%   
% C/ DECORATION
%   - Used only for file loading/saving
%
% Recursive ranking
%
% Doit d?finir un ordre, mais ne stocke pas les predicteurs.
% Ils seront recalcul?s sur ech. jacknif?
% Ainsi, NoNews ne sait pas ici s'il est filtr? etc, c'est un pr
% d'estimation /pr?vision seulement.
%
%
% ** Parameters
%  Ranks                     -> Npoint, noter, dim
%  jack0 (M0: noter= None) -> estimateur
%  jack  (adaptive algo)     -> seuil
%
% The predictor is the "mean" of volumes over days of occurence of the
% event. The forecast pretends the news occurs each day. The selection of
% proper news each day is made in See also EventSelector (comparison of
% different eModels).
%
% tant que on jackn un point sans, news, cela ne change pas la valeur
% theta1 estim?e, mais pr l'instant on fait tous les calculs
%
% [robustesse] On ne peut pas supposer observations identiques partout.
%              Il faudra resynchroniser.
%
%
MODE_TBX= 0;
MODE_AG= 0;
if nargin== 1 & strcmpi(varargin{1},'tbx')
    MODE_TBX= 1;
else
    tbx= newsProjectTbx();
end


%% Object
% Identity
self= [];
self.classe = 'eModel.m';
self.IDstock= nan;
% Estimation parameters
self.opt= options({'sbreak',0,'Npoint',6,'seuil',nan},varargin);

if ~MODE_TBX
    noter= self.opt.get('noter');
    if strcmpi(noter,'ag')
       lengthGenCode= self.opt.get('dimG');
       self= AGranker(lengthGenCode); 
       MODE_AG= 1;
       self.IDstock= nan;
       self.opt= options({'sbreak',0,'Npoint',6,'seuil',nan},varargin);
    end
end


% Estimated parameters

%Globals
self.estimators= options({'mean',@mean,'median',@medianRenorm...
    '3m',@(x) mean(quantile(x,[.25,.5,.75]),2)','trim',@(x) trimmean(x,.05)});
% estimator= @median; % renorm

self.rankers= options({'pv',@noterContextepv,'mpv',@noterContexteMultipv,'std',@noterContexteSTD,'ag',});
%self.seuils= options({'pv',.1});
%self.exclude= @(self_,i) self_.note(i)> .7;
        %self.exclude= @(self_,i) self_.note(i)> -0.25;
        %self.exclude= @(self_,i) self_.note(i)> .2;
        
self.funs=options();
% Methods
self.isvalid= @isvalid;
self.show_jack=@show_jack;

self.setRankings = @setRankings_;
self.setEstimator = @setEstimator_;
self.initData = @ initData_;
self.jacknife= @jacknife;
self.jacknife0= @jacknife0;

self.contextProfiles=@contextProfiles;

self.print_stats= @print_stats;

self.run= @ BTdata;
% TODO: brancher fonction algo genetique
if ~MODE_AG
    self.rankContext= @ rankContext;
else
    www=1;
end
self.decorate= @decorate;

%self.exclude= @(self_,i) self_.note(i)> .9;

self.news=[];
self.volume=[];
self.ranks= [];

self.filetrace=options();
%self.filetrace= decorate_options_filter(self.filetrace);

    function opt= decorate_options_filter(opt)
        % ce decorateur ne passe pas dans save file !!!
        super= opt;
        % super_set= opt.set;
        function x= set_decorated(k,v)
            if strcmpi(v(1:2), 'C:' );
                % ils sont tous globaux !
                www=1;
            else
                www=1;
            end
            x= super.set(k,v);
        end
        opt.set= @set_decorated;
    end
        
        


%% MAIN
    function [ee,e,V]= main_()
        %clear %path= 'C:\N4TH\MATLAB\newsProjectHOME\data\';
        path= 'U:\Matlab\data\';        
        V= load([path 'cacVolume.mat']);
        V= V.Vols(28);
        try; V=V{1};end
        %V.value=V.value1; % DAILY
        %V.value
        V.value= bsxfun(@times,V.value,1./sum(V.value,2));
        assert(all(abs(sum(V.value,2)-1)< 10^(-6)))
        news=load([path 'news.mat']);
        news=news.news;

        %erc= errorComputer();        erc.stockID= V.IDstock;        erc.getFullData(erc.stockID)
        
        e= eModel2('Npoint',10,'seuil',.1);
        e=e.initData(e,V,news);        
        %e.setRankings(e,'mpv');        
        e=e.setRankings(e,'AG');        
        e.setEstimator(e,'mean')
        e=e.decorate(e);

        ee= e.jacknife0(e);
        e=e.rankContext(e);
        ee= e.jacknife(e);
        %
        %e=e.decorate(e);
        e=e.rankContext(e);
        char(e.news.colnames(e.ranks))
        e.note
        sum(e.news.value(:,e.ranks))
        assert(~any(prod(e.indiceEffectif,2)));% disjoint
        %
        ee= e.jacknife(e);
        %
        
        unique(ee.BN)
        h=plotpoint(ee.volume.value,ee.pred);
        lgi= degradeHandle(h,ee.BN);
        set(gca,'ylim',get(gca,'xlim'))
        line(get(gca,'xlim'),get(gca,'xlim'))
        legend(h(lgi(ee.BN(lgi)>0)),ee.news.colnames(ee.BN(lgi(ee.BN(lgi)>0))))

        meanPC(ee.pred-ee.volume.value,ee.BN+1,'num',@std)
    end

%% init(), set()
    function self= initData_(self, volume, news)
        % volume= soit daily, soit intraday
        c= tbx.st.intersect({news,volume});
        self.news=c{1};self.volume= c{2};
        clear c;
        assert(all(self.news.date== self.volume.date));
        self.IDstock= self.volume.info.security_id;
        self.IDdestination= [];
        if isfield(volume,'destinationID')
            self.IDdestination= volume.destinationID;
        end
        
        assert(news.sec_id== self.IDstock,...
            sprintf('sec_id inconsistent entre news<%s> et volume<%s>',news.sec_id,self.IDstock));
        % tentative de patch pour g?ere le pb de changement de sec_id de
        % Bouygues
%         if news.sec_id~= self.IDstock
%             n0= get_security_name(news.sec_id);
%             n1= get_security_name(self.IDstock);
%             assert(strcmp(n0,n1))
%             assert(strcmp(n0,'BOUYGUES')); % FIXME: remove this line
%             self.IDstock= news.sec_id;
%         end
        assert(news.sec_id== self.IDstock);
        self.NAMEstock= self.volume.info.security_key;
        self.opt.set('dim', size(volume.value,2) );
    end

    function self= setRankings_(self,noteur)
        if ischar(noteur) & strcmp(noteur, 'None')
            return;
        end
        
        if strcmpi(noteur,'AG')
            % !!! Il faut d?corer APRES !!!
            self.rankContext= @AGranker;
            
        else        
            fnote= self.rankers.get(noteur);
            self.funs.set('noter', fnote); % value, f-handle
            self.opt.set('noter',noteur); %key, string
        end
    end
    function self= setEstimator_(self,estimator)
        festi= self.estimators.get(estimator);
        self.funs.set('estimateur', festi); % value, f-handle
        self.opt.set('estimateur',estimator); %key, string
    end


%noterContexte(volume,idx));

%self.estimator= estimator;

%% Rankings, Jacknife
    function self= rankContext(self)
        % ranks(k)= i <=> la k-eme meilleure news est la i-?me colonne de news
        %
        %
        noterCtx= self.funs.get('noter');
        P= length(self.news.colnames);
        news= self.news.value;
        self.note=[];
        self.ranks=[];
        self.indiceEffectif= [];
        while true
            %1/ Note tous les contextes
            res = nan(2,P);
            for p = 1:P                
                ctxt= news(:,p);
                if sum(ctxt) < self.opt.get('Npoint') || ismember(p,self.ranks)
                    % les ctxts deja not? ne sont pas supprim?s de news,
                    % donc on les refiltre ici via inf
                    [res(1,p),res(2,p)] = deal(false,inf);
                else
                    [res(1,p),res(2,p)] = noterCtx(self.volume.value,ctxt);
                end
                assert(~isnan(res(p)));
            end
            %2/ Selects Best context
            
            if all(res(2,:)==inf) % No News;
                break
            else
                % Ajout du contexte aux ranks et effaçage journées d'occurence
                res(1,:) = -res(1,:);
                [m,i] = sortrows(res');
                i = i(1);
                m = m(1,2);
                self.ranks(end+1) = i;
                self.note(end+1) = m;
                % stocke les indices des jours ayant produit la note
                self.indiceEffectif(:,end+1) = news(:,i);
                % Efface ces indices (lignes) pour noter la suite
                news( news(:,i)==1,:) = 0;
            end
        end
        assert(length(self.ranks) == length(unique(self.ranks)))
    end
%
    function self= jacknife0(self)%,volume,news)
        estimator= self.funs.get('estimateur');
        volume= self.volume;
        news= self.news;
        [T,Q]= size(volume.value);[T_ ,P]= size(news.value);assert(T==T_);
        pred= nan(T,Q);
        BN= nan(T,1);
        [trading_hours_mask,~,mask_num] = unique(~isnan(volume.value),'rows');
        % a faire passer dans un estim0.
        i0=ones(T,1);i= moutlier1(volume.value,.2);i0(i)=0;
        for t=1:T
            BN(t)= 0;i= i0;
            i(t)= 0; % Jacknife-remove current obs
            i(mask_num~=mask_num(t)) = 0;
            pred(t,trading_hours_mask(mask_num(t),:))= estimator(volume.value(i==1,trading_hours_mask(mask_num(t),:)) );
        end
        self.pred= pred;
        self.BN=BN;
    end

    function self= jacknife(self, basicFC)%,volume,news)
        % TODO: si les ctxt sont sous seuil, cela cr?e des Nan
        estimator= self.funs.get('estimateur');
        volume= self.volume;
        news= self.news;        
        seuil= self.opt.get('seuil');
        sbreak = self.opt.get('sbreak');
        if isnan(seuil); self.exclude= @(self_,i) false;
        else; self.exclude= @(self_,i) self_.note(i)> seuil;        
        end
        volume= self.volume;
        news= self.news;
        % vol, news ne sont pas les memes qu'en learning
        % On va supposer que si, c'est un JN et pas un prev simple
        % donc on utilise indiceEffectectif
        [T,Q]= size(volume.value);[T_ ,P]= size(news.value);assert(T==T_);
        [T_,Q_]= size(basicFC); assert(T==T_);assert(Q==Q_);
        pred= nan(T,Q);
        BN= nan(T,1);
        for t=1:T
            BN(t)= 0;i=ones(T,1);
            for iterRanks=1:length(self.ranks)
                top= self.ranks(iterRanks); % num. de colonne
                if self.exclude(self,iterRanks)
                    if sbreak==0; break;
                    elseif sbreak==1; continue;
                    else;assert(false);end                    
                    % si une est pas assez bien, elle a filtr? les suivantes c'est pas forc?ment intelligent
                    % mais c'est par ordre croisant, ?????
                else
                    if news.value(t,top)== 1
                        BN(t)= top; %indice de colonne ds arg news
                        i= self.indiceEffectif(:,iterRanks);
                        break;
                    end
                end
            end
            if BN(t)== 0 % no context-> use file from jacknife0
                pred(t,:)= basicFC(t,:);
            else % context -> apply
                i(t)= 0; % Jacknife-remove current obs
                td_hour_mask = any(~isnan(volume.value(i==1,:)));
                pred(t,td_hour_mask)= estimator(volume.value(i==1,td_hour_mask) );
                assert(all(~isnan(pred(t,td_hour_mask))),'NaN predicted value in eModel2.jacknife()');
            end
        end
        self.pred= pred;
        self.BN=BN;
    end

%% Buffer predictors in contexts
function profile= contextProfiles(self)
% cr?e les pr?dicteurs de chaque contexte: profils de pr?dictions marginaux
%sachant les pr?c?dents non arriv?s. Utilise indice effectif
[T,Q]= size(self.volume.value);[T_ ,P]= size(self.news.value);assert(T==T_);
[T_,R]= size(self.indiceEffectif); assert(T==T_);

estimator= self.funs.get('estimateur');


% seuil= self.opt.get('seuil');
% sbreak = self.opt.get('sbreak');
% if isnan(seuil); self.exclude= @(self_,i) false;
% else; self.exclude= @(self_,i) self_.note(i)> seuil;        
% end

idxE= self.exclude(self,1:R);
if self.opt.get('sbreak')==0
    if all(~idxE)
    else
        j=min(find(idxE));
        idxE= (1:R)>= j;
    end
end

profile= [];
profile.matchCtxt= options();
profile.rownames= self.news.colnames(self.ranks);
profile.value= nan(R,Q);
profile.BN=[]; % renum?rotation
for iterRanks=1:length(self.ranks)
    i= self.indiceEffectif(:,iterRanks);
    profile.value(iterRanks,:)= estimator(self.volume.value(i==1,:) )';
end

profile.value= profile.value(idxE==0,:);
profile.rownames= profile.rownames(idxE==0);

i0=ones(T,1);i= moutlier1(self.volume.value,.2);i0(i)=0;
i= i0;    
profile.value(end+1,:)= estimator(self.volume.value(i==1,:) );
profile.rownames{end+1}= 'no context';

profile.colnames= self.volume.colnames;
profile.IDstock= self.IDstock;
profile.idxE= [idxE, 0];

r= self.ranks(idxE==0);
assert(all(ismember(self.BN,[0,r])),'BN prends ses valeurs sur des contextes sous le seuil !!!');
% checks
for ctxt = (r(:)')        
end



% Elles sont d?ja dans l'ordre ...
%profile.ranks= self.ranks; % attention ? virer les indices et ? reindexer !!!

end

        


%% decoration
%utiliser self.method() et non pas method() (sinon methode non d?riv?e)
    function self=decorate(self)
        super= self;
        function u= decorated_(u)
            %super= u.rankContext;
            u= super.rankContext(u);
            IO= tbx.FILE(tbx.FILEconfig.folder(u,'stock'));
            op_= self.opt.clone();
            op_.remove('seuil');
            op_.remove('estimateur');
            op_.remove('sbreak');
            op_= op_.get();
            ids= {'type','ranks',op_{:}};
            fname= IO.save(u,ids{:});
            %u.filetrace.set('rankSave',fname);
            FiletraceSet(u,'rankSave',fname);
            fname= IO.save(u,ids{:});
        end
        self.rankContext= @decorated_;
        function u= decorated_J0(u)
            %super= u.jacknife0;
            u= super.jacknife0(u);
            IO= tbx.FILE(tbx.FILEconfig.folder(u,'stock'));
            ids= {'type','jack','Npoint',self.opt.get('Npoint'),'noter','None','dim',self.opt.get('dim'),...
                  'estimateur',self.opt.get('estimateur')};
            fname= IO.save(u,ids{:});
            %u.filetrace.set('jack0Save',fname);
            FiletraceSet(u,'jack0Save',fname);
            fname= IO.save(u,ids{:});
        end
        self.jacknife0= @decorated_J0;
        function u= decorated_J(u)
            IO= tbx.FILE(tbx.FILEconfig.folder(u,'stock'));
            [data,fname]= IO.loadSingle('type','jack','Npoint',u.opt.get('Npoint'),'noter','None','dim',u.opt.get('dim'),'estimateur',u.opt.get('estimateur'));                 
            % LAST_MODIF
            % [data,fname]= IO.loadSingle('type','ranks','Npoint',u.opt.get('Npoint'),'noter','None','dim',u.opt.get('dim'),'estimateur',u.opt.get('estimateur'));                 
            
            FiletraceSet(u,'jack0Load',fname); %u.filetrace.set('jack0Load',fname);
            if u.opt.contains_key('dimG')
                [ranks,fname] = IO.loadSingle('type','ranks','Npoint',u.opt.get('Npoint'),'noter',u.opt.get('noter'),'dim',u.opt.get('dim'),'dimG',u.opt.get('dimG'));     
            elseif u.opt.contains_key('learning_frequency')
                [ranks,fname] = IO.loadSingle('type','ranks','Npoint',u.opt.get('Npoint'),'noter',u.opt.get('noter'),'dim',u.opt.get('dim'),'learning_frequency',u.opt.get('learning_frequency'));
            else
                [ranks,fname] = IO.loadSingle('type','ranks','Npoint',u.opt.get('Npoint'),'noter',u.opt.get('noter'),'dim',u.opt.get('dim'));
            end
            u.filetrace.set('rankLoad',fname);
            u.ranks= ranks{1}.savedVariable.ranks;
            u.note= ranks{1}.savedVariable.note;
            u.indiceEffectif= ranks{1}.savedVariable.indiceEffectif;
            u= super.jacknife(u,data{1}.savedVariable.pred);
            if u.opt.contains_key('dimG')
                ids= {'type','jack','Npoint',u.opt.get('Npoint'),'noter',u.opt.get('noter'),'dim',u.opt.get('dim'),...
                    'estimateur',u.opt.get('estimateur'),'seuil',u.opt.get('seuil'),'sbreak',u.opt.get('sbreak'),'dimG',u.opt.get('dimG')};
            else
                ids= {'type','jack','Npoint',u.opt.get('Npoint'),'noter',u.opt.get('noter'),'dim',u.opt.get('dim'),...
                    'estimateur',u.opt.get('estimateur'),'seuil',u.opt.get('seuil'),'sbreak',u.opt.get('sbreak') };
            end
            fname= IO.save(u,ids{:});
            FiletraceSet(u,'jackSave',fname); %jack0Loadu.filetrace.set('jackSave',fname);
            fname= IO.save(u,ids{:});
        end
        self.jacknife= @decorated_J;
        
    end


% function self=decorNoContext(self)
% 	super= self;
% 	function u= rank_NC(u)
% 		u= super.rankContext(u);
% 		IO= tbx.FILE(tbx.FILEconfig.folder(u.IDstock,'stock'));
% 		op_= self.opt.get();
% 		ids= {'type','ranks',op_{:}};
% 		IO.save(u,ids{:})
% 	end
%     self.rankContext= @decorated_;
%end

%%

%%
end


%% MISC
%function v= noterContextepv(volume,idx)
%v= std(volume(idx==1)) / std(volume);

%end
function x=remove0(x,supermin)
% on est colonne ? colonne
m=min(x(x>0));
if isempty(m)
    m= supermin;
else;
    x(x==0,:)= m;
end
end


function [cont,v] = noterContextepv(volume,idx)
% - v : Quantile 10% des p-values
% - cont : indicateur de « contextualisation » d'un contexte
% * Un contexte est « contextualisant » s'il modifie les horaires de
%   trading de manière univoque.

% Test d'univocité
[~,~,th_class] = unique(~isnan(volume),'rows'); % Classes d'équivalence
cont = length(unique(th_class(idx==1)))==1;
if cont==1
    idx_equiv = th_class==unique(th_class(idx==1));
else
    idx_equiv = true(size(volume,1),1);
end

v = nan(size(volume,2),1);
for k = 1:size(volume,2)
    [~,p] = ttest2(log(remove0(volume(idx==1,k),[])),log(remove0(volume(idx_equiv,k),[])));
    v(k)=p;
end
v = quantile(v,.1);
assert(~isnan(v));
end

function [cont,v] = noterContexteMultipv(volume,idx)
% - v : p-value de l'hypothèse nulle, les espérances contextualisaées ne
%       sont pas colinéaires aux espérance non contextualisées
% - cont : Indicateur de contextualisation de heures de trading.

[th,~,th_equiv] = unique(~isnan(volume),'rows');
cont = length(unique(th_equiv(idx==1)))==1;
if cont
    idx_equiv = th_equiv==unique(th_equiv(idx==1));
    volume = volume(:,th(unique(th_equiv(idx==1)),:));
else
    idx_equiv = true(size(volume,1),1);
end

idx = idx(idx_equiv);
dcheck = volume(idx_equiv,1:end-1);
dcheck(isnan(dcheck)) = 0;
s = nanstd(dcheck);
if ~all(s)
    warning('rank issues in Manova, Removing <%d> degenerated bins',sum(s==0));
    dcheck = dcheck(:,s>0);
end

if all(idx==1)
    v = inf;
    return
end
W0 = nancov(dcheck(idx==0,:));
W1 = nancov(dcheck(idx==1,:));
if rank(W0)< size(W0,1)
    warning('rank issues in Manova, using QR filtering'); % extracts subcolums
    DC0 = dcheck(idx==0,:);
    [~,R,E]= qr(DC0);
    tol= 1e-10;diagr= abs(diag(R)); rk= find(diagr >= tol*diagr(1),1,'last');
    assert(rank(DC0)==rk);
    dcheck = dcheck*E(:,1:rk); % hope it only extracts columns
    dcheck = dcheck(:,1:end-1); % reperdre une dimension POURQUOI ?(deg libert? en plus in intragroup sum)
    W0 = nancov(dcheck(idx==0,:));
    assert(rank(W0) == min(size(W0)));
end
if rank(W1)< size(W1,1)
    warning('Manova will fail in news==1, not corrected');
end

try
    [~,v] = manova1(dcheck,idx==1);
catch ME
    error('rank issues in Manova, using randomize');
end
end


function [cont,v] = noterContexteSTD(volume,idx)
% - v : Ratio des écarts-types (contextualisé vs. pas contextutalisé)
% - cont : Indicateur de contextualisation

[th,~,th_equiv] = unique(~isnan(volume),'rows');
cont = length(unique(th_equiv(idx==1)))==1;
if cont
    idx_equiv = th_equiv==unique(th_equiv(idx==1));
    volume = volume(:,th(unique(th_equiv(idx==1)),:));
else
    idx_equiv = true(size(volume,1),1);
end
volume = volume(idx_equiv,:);
idx = idx(idx_equiv);

v = nanstd(volume(idx==1,:));
v= nansum(v,2);
v0= nanstd(volume);
v0= nansum(v0,2);
v=v/v0;
% TODO; prendre un quantile ,comme pour les pv !
end


function v= noterContexte0(volume,idx)
%v= std(volume(idx==1)) / std(volume);
v= std(volume(idx==1)) / std(volume);
end

function getter= getEstimator()
end

%function self=

function m=medianRenorm(x)
% j'aurais pr?f?r? renormaliser toutes les prev plutot que ici

m= median(x,1);
if length(m) >1
    m= bsxfun(@times,m,1./sum(m,2));
end

end

%mapContexts= options();
% for iterRanks=1:length(self.ranks)
% 	top= self.ranks(iterRanks);
% 	nameInLearn= self.news.colnames{top};
% 	f= strcmpi(nameInLearn,news.colnames);f= find(f);
% 	if length(f)> 0
% 		assert(length(f)==1);
% 		if ~self.exclude(self,iterRanks)
% 			mapContexts.set(nameInLearn,f);
% 		end
% 	end
% end
%self.mapContexts= mapContexts;

function print_stats(self)

st_log('The call to chartableau has been removed to avoid the strange parser error : unable to determine wether "chartableau" is a function name');
% chartableau(self.news.colnames(self.ranks), sum(self.news.value(:,self.ranks)), sum(self.indiceEffectif),self.note)

end



function isvalid(self)
assert(all(self.news.date== self.volume.date));
assert(length(self.pred)== size(self.volume.value,1));

rl= tbx.getFromFileTrace(savedVariable.filetrace,'rankLoad',savedVariable.IDstock);
rl= rl.savedVariable;

assert(all(self.news.date== rl.news.date));
assert(all(self.news.value(:)== rl.news.value(:)));
assert(all(self.volume.date== rl.volume.date));
assert(all(self.volume.value(:)== rl.volume.value(:)));
assert(self.IDstock== rl.IDstock);

j0= tbx.getFromFileTrace(savedVariable.filetrace,'jack0Load',savedVariable.IDstock);
j0= j0.savedVariable;
assert(all(self.news.date== j0.news.date));
assert(all(self.news.value(:)== j0.news.value(:)));
assert(all(self.volume.date== j0.volume.date));
assert(all(self.volume.value(:)== j0.volume.value(:)));
assert(self.IDstock== j0.IDstock);
end

function show_jack(self,dates)
i= ismember(self.volume.date,dates);
figure,
plot(self.volume.value(i,:),'k')
plot(self.pred(i,:),'r')

end








%
% function self= estimate_(self,data,idx)
% 	tbx= newsProjectTbx();
%     % data contient une variable par colonne
%     % les cols peuvent etre, diff?rents titre X diff?rentes plages (aggregation avant)
%     [T,N]= size(data.value);
%     % il faut le faire separement pr pas filtrer V1 avec eModelCA: thetas= self.fmean([data.value1(idx==1,:),data.value(idx==1,:)] );
%     self.thetaV=  self.fmean(data.value1(idx==1,:) );
% 	self.thetaR=  self.fmean( data.value(idx==1,:) );
%     if(any(any(isnan(self.thetaR))))
%         warning(['eModel has NaN thetaR parameters. ' ,self.news ]);
% 	end
% 	if(any(any(isnan(self.thetaV))))
%         warning(['eModel has NaN thetaV parameters. ' ,self.news ]);
%     end
%     self.stdR= std(data.value(idx==1,:) );
% 	self.stdV= std(data.value1(idx==1,:) );
%
% % le backtest est peu adapt? au nombre de points
% % DONE: ne bootstraper que les jours de news, mais on veut aussi l'erreur
% % lorsqu'il n'y a pas de news
% %
% function [predicted, results]= jacknife_(self,data)
%     [T,N]= size(data.value);
%     predicted= data;
%     predicted.value= nan([T,N]);
% 	predicted.value1= nan([T,1]);
%     B= T;
%     if self.modeOutlyer==2;
%         [o,io]=moutlier1(zscore(data.value),.1);
%         %o= getOutlyer_(data.value);
%         o= find(o); % pour meme format que b
%         predicted.NOutlyer=[];
%     end
%     for b=1:T
%         idx= self.idx1;
%         idx(b)= [];
%         Ytrain= data;
%         Ytrain.value(b,:)= [];
%         Ytrain.value1(b,:)= [];
%         self= estimate_(self,Ytrain,idx);
%         predicted.value(b,:)= self.thetaR;
% 		predicted.value1(b,:)= self.thetaV;
%         if self.modeOutlyer==2;
%             % on refait la prev relative
%             predicted.NOutlyer(end+1)= length(o);
%             idx= self.idx1;
%             idx([b;o(:)])= [];
%             self= estimate_(self,Ytrain,idx);
%             predicted.value(b,:)= self.thetaR;
% 		end
% 	end
%     predicted.info= 'eModel.Jacknife()';
%     results= [];
%     results.pred= predicted;
%     results.real= data;
%     %results.univers= 'CAC';
%     results.eModel= self;
%     results= computeErrors_(results);
% 	results.IDstock= data.IDstock;
%
%     %tbx= fileToolbox('C:\matlabBacktest\');
%     tbx= newsProjectTbx();
%     IO= tbx.FILE(tbx.FILEconfig.folder(results.IDstock,'eModel'));
%     IO.save(results,'newsID',self.IDnews,'ID',results.IDstock,'meanfun',self.meanfun,'type','eModel');
%     % il faut detruire theta1 etc -> NON
%     % !!! Theta1 est utilis? comme predicteur par EvenSelector
%
% function results= computeErrors_(results)
%     % compute predictive errors, returns struct results with fields
%     % - erreurs: (predicted - realized)^2 at each date
%     % - dX2, dL2, dL1, dL0 : errors for repartition, at each date
%     % (chisquare and usual norms)
%     [T,N]= size(results.real.value);
%     % chi2
% 	% erreur sans carr? !
% 	results.erreurs= results.pred.value-results.real.value;
% 	results.erreurs1= results.pred.value1-results.real.value1;
% 	results.dL2= sum(results.erreurs.^2,2);
% 	results.dKS= max( abs(cumsum(results.erreurs)),[],2);
% 	results.dL0= max(results.erreurs.^2,[],2);
%
%
% %% INTERNALS
% function predicted= jacknife_NewsOnly(self,data)
% [T,N]= size(data.value);
% Ndays= sum(self.idx1);
% ndays= find(self.idx1);
% predicted= data;
% predicted.value= nan([Ndays,N]);
% predicted.dates= data.dates(self.idx1);
% B= Ndays;
% for b=1:Ndays
% 	idx= self.idx1;
% 	idx(ndays(b) )= [];
% 	Ytrain= data.value;
% 	Ytrain(ndays(b),:)= [];
% 	self= estimate_(self,Ytrain,idx);
% 	predicted.value(b,:)= self.theta1;
% end
%
% function m= mean3(x)
% m= quantile(x ,[.25 .5 .75]); % chaque quantile est sur une ligne
% m= mean(m);
% end
% function m= medianRenorm(x)
% [T,N]= size(x);
% m= median(x); % chaque quantile est sur une ligne
% if N> 1
% 	m= m/sum(m,2);
% end
% end
% function estimator= useOutlyer_(meanfun)
% % Je passe eModel.meanfun= eModelCA(meanfun) au eModel de reference
% % C'est joli, mais ca tourne trop lentement -> on va filtrer une fois pour
% % toutes le learning sample !
% estimator= @(x) useOutlyer_estimator_(x, meanfun);
% end
% function vals= useOutlyer_estimator_(x, meanfun)
% % On lui passe les memes donn?es qu'aux fmean: une fois V1, une fois rep
% %
% % x est une matrice de volume jours en lignes intervalles de temps en
% % colonnes
%
% [T,K]= size(x);
% assert(T>1);
% if K==1;
%     % daily vol
%     vals = meanfun(x);
% else
%     PROP2FILTER = 0.1; % MAGIC NUMBER
%     MINIMAL_NB_OBS_4_STOCKS = 60;% MAGIC NUMBER
%
%     % daily_v = sum(x, 2);
%     % x = bsxfun(@times, x, 1./daily_v);
%
%     % On extrait les noisy days
%     pca_engine = pca('nb_simul', 200 ,...
%         'transformation', @(x)log(1 + x),...TODO something better than + 1?
%         'mode_nb_factors', 'bootstrap', ...
%         'normalized', false);
%     pca_engine.estimate(x(:, 1:end-1));
%     scores = pca_engine.compute_score(x(:, 1:end-1));
%     nd = scores <= PROP2FILTER;
%     if T - sum(nd) < MINIMAL_NB_OBS_4_STOCKS
%         s_scores = sort(scores);
%         nd = scores <= s_scores(end-MINIMAL_NB_OBS_4_STOCKS);
%     end
%     %< d?sormais les noisy day sont ceux o? nd est vrai
%     vals = meanfun(x(~nd, :))';
%     vals = vals/sum(vals); % renormalisation (a priori assuree par meanfun)
% end
%
% function vals= getOutlyer_(x )
% % On lui passe les memes donn?es qu'aux fmean: une fois V1, une fois rep
% %
% % x est une matrice de volume jours en lignes intervalles de temps en
% % colonnes
%
% [T,K]= size(x);
% assert(T>1);
% if K==1;
%     % daily vol
%     vals = boolean(zeros(T,1));
% else
%     PROP2FILTER = 0.1; % MAGIC NUMBER
%     MINIMAL_NB_OBS_4_STOCKS = 60;% MAGIC NUMBER
%
%     % daily_v = sum(x, 2);
%     % x = bsxfun(@times, x, 1./daily_v);
%
%     % On extrait les noisy days
%     pca_engine = pca('nb_simul', 200 ,...
%         'transformation', @(x)log(1 + x),...TODO something better than + 1?
%         'mode_nb_factors', 'bootstrap', ...
%         'normalized', false);
%     pca_engine.estimate(x(:, 1:end-1));
%     scores = pca_engine.compute_score(x(:, 1:end-1));
%     nd = scores <= PROP2FILTER;
%     if T - sum(nd) < MINIMAL_NB_OBS_4_STOCKS
%         s_scores = sort(scores);
%         nd = scores <= s_scores(end-MINIMAL_NB_OBS_4_STOCKS);
%     end
%     %< d?sormais les noisy day sont ceux o? nd est vrai
%     vals= nd;
% end
%

%% TEST
% function testdata()
% T= 100;
% x= [];
% x.colnames= {'A1','A1.matin','A2'};
% x.value= randn(T,3);
% x.dates= 1:T;
%
% news= [];
% news.colnames= {'N1','N2','N3'};
% news.value= rand(T,3)> .5;
% news.dates= 1:T;
%
% function TEST()
% m= eModel();
% m= m.init(m,news,0); pred= m.jacknife(m, Y);
% pred= m.jackNO(m, x);
%
%
% % function iniData_()
% % load('U:\Matlab\data\cacVolume.mat')
% % load('U:\Matlab\data\news.mat')
% % tbx= newsProjectTbx();
% % %[data,news]= tbx.LV.getData(2, tbx.mdates({'01/01/2007','30/06/2010'}));
% % c= tbx.st.intersect({news,Vols{:}});
% % news= c{1};
% % Vols= c(2:end);
% % Vols= cellfun(tbx.LV.volume2repart,Vols);
% % Y= tbx.LV.concatVols(Vols);
%
% %function BTdata()
%
% function BTdata(news,Y,meanfun)
% % Runs jacknife for each event contained in news, on the volume data Y
% %if ~iscell(Y); Y= {Y};end
% Nstock= length(Y);
% Nnews= length(news.colnames);
% for ns= 1:Nstock
% 	stockVolume= Y(ns);
%     % NoNews ajout? a news, fait tout seul
% % 	m= eModel();
% % 	m= m.init(m,news,0,'mean');
% % 	stockVolume= m.iniData(m,stockVolume);
% % 	m.jacknife(m, stockVolume);
% % 	m= m.init(m,news,0,'median');
% % 	m.jacknife(m, stockVolume);
% % 	m= m.init(m,news,0,'3mean');
% % 	m.jacknife(m, stockVolume);
% 	for nn= [1:Nnews]
% 		% TODO: remove 0 et modele ref specifique
% 		m=[];m= eModel();
% 		m= m.init(m,news,nn,meanfun);
%         if strcmpi(news.colnames{nn},'NoNews')
%             m.modeOutlyer= 2;
%         end
%         %m.idx1;
% 		m.jacknife(m, stockVolume);
% 	end
% end
% % 160 secondes pour le CAC et 27 news ie 6 sec par news
%
% function rerunWithOutlyer_(news,Y,meanfun)
% % Runs jacknife for each event contained in news, on the volume data Y
% %if ~iscell(Y); Y= {Y};end
% st_log('off');
% Nstock= length(Y);
% Nnews= length(news.colnames);
% nn = strcmpi(news.colnames,'NoNews');
% nn= find(nn);
% assert(length(nn)==1);
% for ns= 1:Nstock
% 	stockVolume= Y(ns);
%     m=[];m= eModel();
%     m= m.init(m,news,nn,meanfun);
%     %m.fmean= useOutlyer_(m.fmean);
%     m.modeOutlyer= 2;
%     m.jacknife(m, stockVolume);
%     disp(ns)
% end
% st_log('on');
%
% function errs()
% er= tbx.BT.erreur(results);
% er.value= mean(er.value.^2);
%
% % redim
% nP= length(er.plagenames);
% nS= length(er.value) / nP;
% ers= [];
% ers.plageidx= reshape(er.plageidx,[nP,nS]);% ligne constante
% ers.stockidx= reshape(er.stockidx,[nP,nS]) ;% colonne constante
% ers.value= reshape(er.value,[nP,nS]);
%
%
% % erreurs intradays du titre 1
% plot(ers.value(1:end-1,1))
%
% function TEST_()




