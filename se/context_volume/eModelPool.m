function self= eModelPool(ID,TD,mode,substep,inhibited,dates,path_modifier,learning_opt)
% Applies various eModel on one stock
% PROJECT: context sensitive volume forecast
%
% All steps (RANK/JACK/ERROR) are applied on the stock
%
%

persistent paveur_type;
if nargin==1
    paveur_type= ID;
    return
else
    if isempty(paveur_type)
        paveur_type=0;
    end
end
    

if nargin< 7; path_modifier=[];end
if nargin< 8; learning_opt=options();end

MODE_EXPLORATOIRE_ONLY= 'normal'; % 'ag';

self=[];
self.news= nan;
self.volume= nan;
self.stockID= nan;
self.mode=nan;

self.stockID= [];
%self.runRank= @runRank;
self.run= @run;
self.inidata= @inidata2;
self.compare= @compare;
self.definePaveur= @definePaveur;
self.getAllErrors= @getAllErrors;
self.getAllPred= @getAllPred;
%self.main= @main;

self.path_modifier= path_modifier;
self.learning_options = learning_opt;
if nargin==0;
    tbx= newsProjectTbx();    
elseif nargin== 1 & strcmpi(varargin{1},'tbx');
else
    
    if ischar(TD) && strcmpi(TD,'main')
         x_=get_repository('tdinfo',ID);        
         TD= x_(1).trading_destination_id;
    end
    self.TD= TD;
    %assert(nargin>=1)
    tbx= newsProjectTbx();    
%     if nargin< 1;ID= 2;end
%     if nargin< 2;mode= 'intraday'; end    
%     if nargin< 3;substep= 'all'; end  
%     if nargin< 4;inhibited= 0; end  
    %self= eModelPool();
    self.mode= mode;
    self= self.inidata(self,ID,TD,mode,dates);
    
    all_data= self.volume.get();
    for k=1:2:length(all_data)
        if size(all_data{k+1}.value,1) < self.learning_options.get('thres_nobs_global')           
            error(['Not enough observations (' num2str(size(all_data{k+1},1)) ') in volume sample ', opt2str(self.learning_options)])
        end
    end
    
    self.sousmode= MODE_EXPLORATOIRE_ONLY;
    
    fullopt= self.definePaveur(mode,self.sousmode);
    dt=[];
    if strcmpi(substep,'all')
        tic;self.run(self,'ranks',fullopt,inhibited); dt(1)= toc;
        tic;self.run(self,'jack0',fullopt,inhibited); dt(2)= toc;
        tic;self.run(self,'jack',fullopt,inhibited) ; dt(3)= toc;        
        tic;self.run(self,'error',fullopt,inhibited) ;dt(4)= toc;   
        tic;self.run(self,'MRANK',fullopt,inhibited) ;dt(5)= toc;  
        tic;self.run(self,'nonews',fullopt,inhibited) ;dt(5)= toc;  
        %mainProcess(ID,freq)
    else
        tic;self.run(self,substep,fullopt,inhibited) ;dt(1)= toc;  
    end
end



%%
%     function main(ID,mode,substep,inhibited)
%         if nargin< 1;ID= 2;end
%         if nargin< 2;mode= 'intraday'; end    
%         if nargin< 3;substep= 'all'; end  
%         if nargin< 4;inhibited= 0; end  
%         p= eModelPool();
%         p.mode= mode;
%         p= p.inidata(p,ID,mode);
%         fullopt= p.definePaveur(mode);
%         dt=[];
%         if strcmpi(substep,'all')
%             tic;p.run(p,'ranks',fullopt,inhibited); dt(1)= toc;
%             tic;p.run(p,'jack0',fullopt,inhibited); dt(2)= toc;
%             tic;p.run(p,'jack',fullopt,inhibited) ; dt(3)= toc;        
%             tic;p.run(p,'error',fullopt,inhibited) ;dt(4)= toc;   
%             tic;p.run(p,'MRANK',fullopt,inhibited) ;dt(5)= toc;              
%             %mainProcess(ID,freq)
%         else
%             tic;p.run(p,substep,fullopt,inhibited) ;dt(1)= toc;  
%         end
%         %err= p.getAllErrors(p);
%     end

%%
% function self= inidata(self, stockID, type)
%     if nargin< 3;type= 'intraday';end
%     self.stockID= stockID;
%     path= 'U:\Matlab\data\';        
%     V= load([path 'cacVolume.mat']);
%     z= [V.Vols{:}]; z= [z.info]; z= [z.security_id];
%     idx= find(z == stockID);
%     assert(length(idx)==1); idx=idx(1);
%     V= V.Vols(idx);
%     try; V=V{1};end
%     if strcmpi(type,'daily')
%         V.value= sum(V.value,2); % DAILY
%     elseif strcmpi(type,'intraday')    
%         V.value= bsxfun(@times,V.value,1./sum(V.value,2));
%         assert(all(abs(sum(V.value,2)-1)< 10^(-6)))
%     end    
%     news=load([path 'news.mat']);
%     self.news=news.news; 
%     self.volume= V;
% end
function self= inidata2(self, security_id,destinationID, type,dates)   
    if nargin< 4;type= 'intraday';end
    if nargin< 3;destinationID= -1 ;end %-1: pas de test
    self.stockID= security_id;
    tbx_emp= eModelPool();x=tbx_emp.definePaveur('intraday');x=options(x{:});
    all_freq= x.get('learning_frequency'); all_freq= [all_freq{:}];    
    
    IO= tbx.FILE(tbx.FILEconfig.folder({security_id,destinationID,dates,self.path_modifier},'News'));  
    news= IO.loadSingle('type','news','stockID',num2str(security_id) );
    news=news{1};
    try;news=news.savedVariable;
    catch;news= news.news;
    end
    tbx= newsProjectTbx();
    IO= tbx.FILE(tbx.FILEconfig.folder({security_id,destinationID,dates,self.path_modifier},'Buffer'));
    self.volume=options();
    for f= (all_freq(:)')    
        
        buf= IO.loadSingle('type','buffer','stockID',security_id,'freq','learning','f',f);
        V= buf{1}.savedVariable;
        try; V=V{1};end
        if strcmpi(type,'daily')
            V.value= sum(V.value,2); % DAILY
        elseif strcmpi(type,'intraday')    
            V.value= bsxfun(@rdivide,V.value,nansum(V.value,2));
        end    
        if isfield(V,'destinationID')
           if isempty(destinationID)
               assert(isempty(V.destinationID))
           elseif ischar(destinationID)
               assert(strcmpi(destinationID,'main'));
           elseif destinationID~=-1
                assert(V.destinationID == destinationID);
           end
        end
        %path= 'U:\Matlab\data\';        
        %synchro= tbx.st.intersect({V,news.news});    
        synchro= tbx.st.intersect({V,news});    
        self.news=synchro{2}; 
        self.volume.set(f, synchro{1});  
        self.original_date= synchro{1}.original_date;
    end
    self.volume.original_date= synchro{1}.original_date;
    self.volume.destinationID= synchro{1}.destinationID;
end


% function run(self, mode, fullopt,inhibited)
%     if strcmpi(mode,'ranks'); runRank(self,fullopt);
%     elseif strcmpi(mode,'jack0'); runJack0(self,fullopt);
%     elseif strcmpi(mode,'jack'); runJack(self,fullopt);
%     elseif strcmpi(mode,'error'); runError(self,fullopt);
%     elseif strcmpi(mode,'MRANK'); runBestModel(self,fullopt,inhibited);
%     elseif strcmpi(mode,'inhibition'); runInhibition(self,fullopt)
%         
%     elseif strcmpi(mode,'all');
%         runRank(self,fullopt);
%         runJack0(self,fullopt);
%         runJack(self,fullopt);
%         runError(self,fullopt);
%         runBestModel(self,fullopt,0)
%         runInhibition(self,fullopt);
%         runBestModel(self,fullopt,1)        
%     else;error('');
%     end
% end

function run(self, mode, fullopt,inhibited)
    if strcmpi(mode,'ranks'); runRank(fullopt);
    elseif strcmpi(mode,'jack0'); runJack0(fullopt);    
    elseif strcmpi(mode,'jack'); runJack(fullopt);
    elseif strcmpi(mode,'error'); runError(fullopt);
    elseif strcmpi(mode,'MRANK'); runBestModel(fullopt,inhibited);
    elseif strcmpi(mode,'inhibition'); runInhibition(fullopt)
    elseif strcmpi(mode,'nonews'); runNonews(fullopt,inhibited)
        
    elseif strcmpi(mode,'all');
        % TODO inhibition ?
        runRank(fullopt);
        runJack0(fullopt);
        runJack(fullopt);
        runError(fullopt);
        runBestModel(fullopt,0)
        runInhibition(fullopt);
        runBestModel(fullopt,1) ;
        runNonews(fullopt,1);
    else;error('');
    end
end
function data= getAllErrors(self,varargin) % from buffer
    IO= tbx.FILE(tbx.FILEconfig.folder(self,'stock'));
    data= IO.load('type','error',varargin{:});     
    data= [data{:}];
end
function data= getAllPred(self) % from buffer
    IO= tbx.FILE(tbx.FILEconfig.folder(self,'stock'));
    data= IO.load('type','jack');     
    data= [data{:}];
end

%% Run internals
function runRank(fullopt)
    paveur= liste2paveur(fullopt,{'seuil','estimateur'});
%     optRank= fullopt.clone();
%     optRank.remove('seuil');    optRank.remove('estimateur');
%     co= optRank.get();
%     paveur= optPaveur(co{:});
    for k=1:paveur.lindim
    %for k=1:paveur.getdim()
        o= paveur.get(k);
        %erc= errorComputer(); erc.stockID= V.IDstock; erc.getFullData(erc.stockID);
        ca= o.get();
        e= eModel2(ca{:});
        e.path_modifier=self.path_modifier;
        V= self.volume.get(o.get('learning_frequency'));
        e=e.initData(e,V,self.news); 
        if isfield(V,'destinationID')
           e.destinationID= V.destinationID;
        end
        e.setRankings(e,o.get('noter') );               
        e=e.decorate(e);       
        e2= e.rankContext(e);       
    end
end
function runJack0(fullopt)
    paveur= liste2paveur(fullopt,{'seuil'});
%     optRank= fullopt.clone();
%     optRank.remove('seuil');
%     co= optRank.get();
%     paveur= optPaveur(co{:});
    for k=1:paveur.lindim
    %for k=1:paveur.getdim()
        o= paveur.get(k);        
        ca= o.get();
        e= eModel2(ca{:});
        e.path_modifier=self.path_modifier;
        V= self.volume.get(o.get('learning_frequency'));
        e=e.initData(e,V,self.news);
        if isfield(V,'destinationID')
           e.destinationID= V.destinationID;
        end
        e.setRankings(e,o.get('noter') );        
        e.setEstimator(e,o.get('estimateur') );
        e=e.decorate(e);
        e1= e.jacknife0(e);               
    end
end

function runJack(fullopt)
    paveur= liste2paveur(fullopt);
%     optRank= fullopt.clone();    
%     co= optRank.get();
%     paveur= optPaveur(co{:});
    for k=1:paveur.lindim
    %for k=1:paveur.getdim()
        o= paveur.get(k);        
        ca= o.get();
        e= eModel2(ca{:});
        e.path_modifier=self.path_modifier;
        V= self.volume.get(o.get('learning_frequency'));
        e=e.initData(e,V,self.news);  
        if isfield(V,'destinationID')
           e.destinationID= V.destinationID;
        end
        e.setRankings(e,o.get('noter') );        
        e.setEstimator(e,o.get('estimateur') );
        e=e.decorate(e);        
        e3= e.jacknife(e);
    end
end
function runError(fullopt)
    IO= tbx.FILE(tbx.FILEconfig.folder(self,'stock'));
    files= IO.parse('type','jack');                 
    for k=1:length(files)    
        data= IO.loader(files{k});
        erc= errorComputer();
        erc.path_modifier=self.path_modifier;
        erc.stockID= self.stockID;
        V= self.volume.get(data.savedVariable.opt.get('learning_frequency'));
        erc.trading_destination_id= V.destinationID;
        erc.volume=[];erc.volume.original_date= V.original_date;
        if data.savedVariable.opt.contains_key('dimG')
            if ~isnan(data.savedVariable.opt.get('seuil'))
               data.params{end+1}= 'dimG';
               data.params{end+1}= data.savedVariable.opt.get('dimG');
            end
        end
        er=erc.apply(erc,data.params{:});        
    end
    if length(files)>0 % recup�re frequence de la derni�re ?
        er= oracle_for_critereOp(self.learning_options);
    end
end

function runBestModel(fullopt,inhibited)
    if strcmpi(self.mode,'daily')
        freq= 1;
    elseif strcmpi(self.mode,'intraday')   
        IO= tbx.FILE(tbx.FILEconfig.folder(self,'Buffer'));        
        %buf= IO.loadSingle('type','buffer','stockID',self.stockID,'freq','learning');
        %freq=36;
        %freq= size(buf{1}.savedVariable.value,2);
        freq= nan;
    else;assert(0);
    end    
    c= compareModels('tbx');    
    %c.main(self.stockID,freq);    
    % inhibited= 1;
    c.mainProcess(self,freq,inhibited)
end

function runInhibition(fullopt)
    IO= tbx.FILE(tbx.FILEconfig.folder(self,'stock'));
    files= IO.parse('type','error','inhibition',0);   
    
    c= compareModels();  
    c.path_modifier= self.path_modifier;
    c.TD= self.TD;
    V= self.volume.get(); V= V{2}; % -> original_date => on se moque de r?cup?rer celui ? la bonne fr?quence,
    c.original_date= V.original_date;
    % ATTENTION 36/37
    c= c.loadBuffer(c,self.stockID,'inhibition',0);
%     c= c.loadBuffer(c,self.stockID,'dim',37,'inhibition',0);
%     rand_patch = 40;
%     while isempty(c.data) && rand_patch >= 1
%         c= c.loadBuffer(c,self.stockID,'dim',rand_patch,'inhibition',0);
% %         if isempty(c.data)
% %             c = c.loadBuffer(c,self.stockID,'dim',36+rand_patch,'inhibition',0);
% %         end
%         rand_patch = rand_patch-1;
%     end
    c= c.bestM0(c);
    critere= 'cash 1mn';
    i0= find(strcmpi(c.data(1).savedVariable.colnames,critere)); assert(length(i0)==1);
    bm0= c.data(c.bestm0(i0)).savedVariable;  
    
    tbx____= ctxtPrev_load();
    M0_check= tbx____.load(c,'inhibition',0);
    n_ = norm( M0_check.M0.value - bm0.value);
    assert(n_==0);
    %assert(size( unique(bm0.value,'rows') ,1)==1);
    assert(unique(bm0.BN)==0);
    
    for k=1:length(files)         
        data= IO.loader(files{k});
        data= data.savedVariable;
        data.path_modifier= self.path_modifier;
        data.learning_options= self.learning_options;
        
        erc= errorComputer();         
        
        erc.stockID= self.stockID;  
        if isempty(strfind(files{k}, 'noter-None'))
            erc.inhiberCtxt(data,bm0,i0,0);
        else
            erc.inhiberCtxt(data,bm0,i0,1);
        end
    end
    
end

function runNonews(fullopt,inhibition)
    tbx__= prevNoNews();
    tbx__.main(self,inhibition);
end

function fullopt= definePaveur(mode, soustype)
    if nargin< 2; soustype='normal';end
    if strcmpi(mode,'intraday')
    % */ Intraday
    %% REGULAR HERE
        %fullopt= {'Npoint',{10},'seuil',{.01 .02 .05 .1 .8 .9},'noter',{'pv','mpv','std'},'estimateur',{'mean','median'},'sbreak',{0} };
        % on renvoit une liste de liste maintenant !
        %fullopt= options({'Npoint',{10},'seuil',{.01 .02 .05 .1 .8 .9},'noter',{'pv','mpv','std'},'estimateur',{'mean','median'},'sbreak',{0} });    
        %fullopt={fullopt};
        if strcmpi(soustype,'normal')
            if paveur_type==0
            %% NORMAL
              lpv= {'Npoint',{10},'seuil',{.01 .02 .05 .1},'noter',{'pv','mpv'},'estimateur',{'mean','median'},'sbreak',{0},'learning_frequency',{10} };
              lstd= {'Npoint',{10},'seuil',{.6 .8 .9},'noter',{'std'},'estimateur',{'mean','median'},'sbreak',{0},'learning_frequency',{10} };
              fullopt={lpv,lstd};
            elseif paveur_type==1
             %% TEST
             %lpv= {'Npoint',{10},'seuil',{.05},'noter',{'mpv'},'estimateur',{'median'},'sbreak',{0},'learning_frequency',{10,15} };
             lpv= {'Npoint',{5},'seuil',{0.01 .05 0.1},'noter',{'mpv'},'estimateur',{'median'},'sbreak',{0},'learning_frequency',{5,10,15} };
             fullopt={lpv};%,lstd};
            elseif paveur_type==2
             %% TEST
             %lpv= {'Npoint',{10},'seuil',{.05},'noter',{'mpv'},'estimateur',{'median'},'sbreak',{0},'learning_frequency',{10,15} };
             lpv= {'Npoint',{10},'seuil',{0.01 .05 0.1},'noter',{'mpv'},'estimateur',{'median'},'sbreak',{0},'learning_frequency',{15} };
             fullopt={lpv};%,lstd};
            else; assert(0);
            end
            %% TEST
            %lpv= {'Npoint',{10},'seuil',{.05},'noter',{'mpv'},'estimateur',{'median'},'sbreak',{0},'learning_frequency',{10,15} };
            %fullopt={lpv};
             %% fast-test
             %lpv= {'Npoint',{10},'seuil',{.05 .1},'noter',{'mpv'},'estimateur',{'mean','median'},'sbreak',{0} };
             %lstd= {'Npoint',{10},'seuil',{.8 .9},'noter',{'std'},'estimateur',{'mean','median'},'sbreak',{0} };
             %fullopt={lpv,lstd};
            
            %fullopt= {{'Npoint',{10},'seuil',{.01},'noter',{'mpv'},'estimateur',{'mean','median'},'sbreak',{0}}};
        elseif strcmpi(soustype,'ag')
            fullopt= {{'Npoint',{10},'seuil',{1},'noter',{'ag'},'dimG',{5,8,10,15},'estimateur',{'mean','median'},'sbreak',{0}}};
        end
        
    else
        assert(strcmpi(mode,'daily'))
    %fullopt= options({'Npoint',{10},'seuil',{.1, .5},'noter',{'pv'},'estimateur',{'mean','median'},'sbreak',{0} });    
        fullopt= options({'Npoint',{10},'seuil',{0.05, .1, .5, .8, .9, .95},'noter',{'pv','std'},'estimateur',{'mean','median'},'sbreak',{0,1} });    
    %fullopt= options({'Npoint',{10},'seuil',{.8 .9 .95},'noter',{'std'},'estimateur',{'mean','median'},'sbreak',{1,0} });    
    
    %fullopt= options({'Npoint',{6,10},'seuil',{.05,.1 .2 .5},'noter',{'None'},'estimateur',{'mean','median'} });    
    %fullopt= options({'Npoint',{10},'seuil',{.1},'noter',{'mpv'},'estimateur',{'mean'} });  
    end
end

%%
end


function p=liste2paveur(l,remove)
% on recoit une liste d'options, chacun ? transformer en paveur, puis ?
% concat?ner. On se permet de les filtrer avant !
if nargin==1;remove= {};end
P= length(remove);
N= length(l);
lo={};
for n= 1:N    
    %for p=1:P
    i= find(ismember(l{n}(1:2:end),remove));
    i=i(:);
    l{n}([2*i-1,2*i])= [];      
    %end    
    lo{n}= optPaveur(l{n}{:});
end
p= optPaveur(lo{:});
end


    
% 
% function fullopt= definePaveur_OV(mode)
%     if strcmpi(mode,'intraday')
%     % */ Intraday
%     %% REGULAR HERE
%         fullopt= options({'Npoint',{10},'seuil',{.01 .02 .05 .1 .8 .9},'noter',{'pv','mpv','std'},'estimateur',{'mean','median'},'sbreak',{0} });    
%     
%    %% debug
%         %fullopt= options({'Npoint',{10},'seuil',{.15 .8},'noter',{'pv'},'estimateur',{'median'},'sbreak',{0} });    
%    
%     %% test
%         %fullopt= options({'Npoint',{10},'seuil',{.05,.15 .5 .8 .9},'noter',{'ag'},'estimateur',{'mean','median'},'sbreak',{0} });    
%         
%     %fullopt= options({'Npoint',{10},'seuil',{.05,.1 .5},'noter',{'pv'},'estimateur',{'mean','median'},'sbreak',{0,1} });    
%     
%     % */ Daily
%     else
%         assert(strcmpi(mode,'daily'))
%     %fullopt= options({'Npoint',{10},'seuil',{.1, .5},'noter',{'pv'},'estimateur',{'mean','median'},'sbreak',{0} });    
%         fullopt= options({'Npoint',{10},'seuil',{0.05, .1, .5, .8, .9, .95},'noter',{'pv','std'},'estimateur',{'mean','median'},'sbreak',{0,1} });    
%     %fullopt= options({'Npoint',{10},'seuil',{.8 .9 .95},'noter',{'std'},'estimateur',{'mean','median'},'sbreak',{1,0} });    
%     
%     %fullopt= options({'Npoint',{6,10},'seuil',{.05,.1 .2 .5},'noter',{'None'},'estimateur',{'mean','median'} });    
%     %fullopt= options({'Npoint',{10},'seuil',{.1},'noter',{'mpv'},'estimateur',{'mean'} });  
%     end
% end
