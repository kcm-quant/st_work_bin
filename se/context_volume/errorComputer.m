function self= errorComputer(varargin)
% Computes forecast errors using Jacknife made by eModel
% PROJECT: context sensitive volume forecast
%
% NB: on recharge les donn�es (on n'utilise pas les buffers ?)
%
% Computes both
% - Theoretical errors (L2,L0,KS)
% - Operationnal criteria (1mn and 5mn)
%
% Decoration is used to load and save files

% Input= fichier jacknife avec pred et volume
% TODO: factoriser mn1, mn5 sur la boucle de mod�le !!!

if nargin== 1 & strcmpi(varargin{1},'tbx');
else
    tbx= newsProjectTbx();
end

self.stockID= [];
self.apply= @apply;
self.getFullData= @getFullData;
self.inhiberCtxt= @inhiberCtxt;
self.applyWithoutFileSave= @applyWithoutFileSave;
%self.bsxfun= @bsxfun;
    function main_test()
        erc= errorComputer();
        erc.stockID= 276;
        %erc.getFullData(erc.stockID,'monofile')
        er=erc.apply(erc,'type','jack','Npoint',10,'ranking','None','dim',36,'estimateur','mean');
        er2=erc.apply(erc,'type','jack','Npoint',10,'ranking','mpv','dim',36,'estimateur','mean','seuil',0.05);
        mean(er2.value) ./ mean(er.value);
        %mean(er2.value.^2) ./ mean(er.value.^2)
        mean(er2.value(er2.BN>0,:)) ./ mean(er.value(er2.BN>0,:));
        %mean(er2.value(er2.BN>0,:).^2) ./ mean(er.value(er2.BN>0,:).^2)
        mean(er2.BN>0);
        length(unique(er2.BN))-1;
    end


    function getFullData(stockID, mode)
        assert(0,'deprecated');
        if nargin< 2; mode= 'fullfile';end
        if strcmpi(mode,'fullfile')
            % rapatrie le fichier buffer
            x= load('E:\matab\MATLAB\newsProjectHOME\data\AllOtherData.mat');
            id= [x.Vols.IDstock];
            i= find(id== stockID); assert(length(i)==1);
            x= x.Vols(i);
            IO= tbx.FILE(tbx.FILEconfig.folder(stockID,'stock'));
            IO.save(x,'type','fullData');
        elseif strcmpi(mode,'monofile')
            x= load(['E:\newsProjectAllov\saveAll' num2str(stockID) '.mat']);    
            IO= tbx.FILE(tbx.FILEconfig.folder(stockID,'stock'));
            IO.save(x,'type','fullData');    
        end
    end

    function er=apply(self,varargin)
        IO= tbx.FILE(tbx.FILEconfig.folder(self,'stock'));
        [data,fname]= IO.loadSingle(varargin{:});        
        data= data{1}.savedVariable;
        FiletraceSet(data,'errorLoad',fname); %data.filetrace.set('errorLoad',fname);
        er= compute(data.volume.value,data.pred);
        opt= options(varargin);
        if opt.get('dim')== 1
            mn1=[]; mn1.filename = 'daily -> not used';
            mn5=[]; mn5.filename = 'daily -> not used';
        else            
            mn1= critereOpComputer(data,self.stockID);         
            er.cash1= mn1.pognon;                       
            er.value(:,end+1)= abs(mn1.pognon);
            er.value(:,end+1)= abs(mn1.pognon).^2;
            er.value(:,end+1)= mn1.ri;
            er.value(:,end+1)= mn1.illiquid;
            er.colnames{end+1}= 'cash 1mn';
            er.colnames{end+1}= 'cash^2 1mn';
            er.colnames{end+1}= 'ri 1mn';
            er.colnames{end+1}= 'illiquid 1mn';    
        end
        if isfield(data.volume,'destinationID')
           er.destinationID= data.volume.destinationID;
        end
        er.filetrace= data.filetrace;
        er.date= data.volume.date;
        er.NAMEstock= data.NAMEstock;
        er.BN= data.BN;
        er.IDstock= data.IDstock;
        er.opt= data.opt;
        er.news= data.news;
        er.volume_rereparti = mn1.volume_rereparti;
        pars= options(varargin);
        pars.set('type','error');
        pars.set('inhibition',0);
        pars= pars.get();
        er.TD= self.trading_destination_id;
        er.original_date= self.volume.original_date;
        FiletraceSet(er,'fullDataLoadMn1',mn1.filename); %er.filetrace.set('fullDataLoadMn1',mn1.filename);        
        fname= IO.save(er,pars{:});   
        FiletraceSet(er,'errorSave',fname); %er.filetrace.set('errorSave',fname);
        IO.save(er,pars{:});   
    end

    function er= compute(ref,pred)
        N = size(ref,2);
        ecart = arrayfun(@(i)ref(i,~isnan(pred(i,:)))-pred(i,~isnan(pred(i,:))),1:size(ref,1),'uni',false);
        L2 = cellfun(@(x)sqrt(N*mean(x.^2)),ecart)';
        L0 = cellfun(@(x)max(abs(x)),ecart)';
        KS = cellfun(@(x)max(abs(cumsum(x))),ecart)';
        er = struct('value',{[L2,L0,KS]},'colnames',{{'L^2','L^0','KS'}});
    end

function [er,mn1]=applyWithoutFileSave(data)
    er= compute(data.volume.value,data.pred);
    opt= data.opt;
    if opt.get('dim')== 1
        mn1=[]; mn1.filename = 'daily -> not used';
        mn5=[]; mn5.filename = 'daily -> not used';
    else            
        % TODO: check OK
        falsself=[];
        falsself.stockID= data.IDstock;
        falsself.btest= data;
        %[mn1,mn5]= computeOp_newV(falsself,data);  
        mn1= critereOpComputer(data,falsself.stockID);     
        er.cash1= mn1.pognon;            
        %er.cash5= mn5.pognon;            
        er.value(:,end+1)= abs(mn1.pognon);
        er.value(:,end+1)= abs(mn1.pognon).^2;
        er.value(:,end+1)= mn1.ri;
        er.value(:,end+1)= mn1.illiquid;
        er.colnames{end+1}= 'cash 1mn';
        er.colnames{end+1}= 'cash^2 1mn';
        er.colnames{end+1}= 'ri 1mn';
        er.colnames{end+1}= 'illiquid 1mn';    
    end
    er.filetrace= data.filetrace;
    er.date= data.volume.date;
    er.NAMEstock= data.NAMEstock;
    er.BN= data.BN;
    er.IDstock= data.IDstock;
    er.opt= data.opt;
    er.news= data.news;
    pars= options(varargin);
    pars.set('type','error');
    pars.set('inhibition',0);
    pars= pars.get();
    FiletraceSet(er,'fullDataLoadMn1',mn1.filename); %er.filetrace.set('fullDataLoadMn1',mn1.filename);
    % utilis� par NoNewsPred
    if isfield(data,'BACKTEST_startIdx')
        er.BACKTEST_startIdx= data.BACKTEST_startIdx;
    end
end

function inhiberCtxt(self, bm0,i0, isM0)
    % Inhibe les contextes dont erreur > erreur M0
    % calcul un ErrorComputer, et non pas un jack
    % TODO: enlever les ctxt dont note<seuil ?
    assert(all(all(self.news.value==bm0.news.value)))    
    RATIO_INHIBITION= self.learning_options.get('tresh_inhibit');
    if isM0== 1
        self.opt.set('type','error','inhibition','1','noter','None');
        IO= tbx.FILE(tbx.FILEconfig.folder(self,'stock'));    
        pars= self.opt.get();    
        IO.save(self,pars{:});      
        %save('-v7.3','C:\\test.mat','self');
        return
    end
        
    critere= 'cash 1mn';
    i0= find(strcmpi(bm0.colnames,critere)); assert(length(i0)==1);
    i= find(strcmpi(self.colnames,critere)); assert(length(i)==1);

    R= tbx.getFromFileTrace(self.filetrace,'rankLoad',self);  R= R.savedVariable;  
    assert(all(all(R.news.value==bm0.news.value)))
    
    idx_= self.BN>0;
    bn=self.BN(idx_); 
    %bn= R.ranks(bn); % BN c'est bien la colonne et pas l'indice dans self.ranks
    w_=[];
    u= self.news.value(idx_==1,:); 
    for k=1:length(bn); assert( u(k,bn(k)) == 1 ); end
        
%     J0= tbx.getFromFileTrace(self.filetrace,'jack0Load',self.IDstock); J0= J0.savedVariable;    
%     J= tbx.getFromFileTrace(self.filetrace,'jackSave',self.IDstock);  J= J.savedVariable;
    self.inhibitedCtxt= []; % num�ros de colonnes correspondants � self.ranks ,i.e self.news
    self.inhibition_ratio= [];
    for k=1:length(R.ranks)
        idx= find(self.BN== R.ranks(k)); % la k-�me meilleure colonne !
        cri= mean(self.value(idx,i));
        cri0= mean(bm0.value(idx,i0));
        if mean(self.value(:,i),1) > mean(bm0.value(:,i0),1)
            www=1;
        end
        self.inhibition_ratio(end+1)= cri/cri0;
        if cri> cri0 * RATIO_INHIBITION
            if R.ranks(k)==2
                www=1;
            end
           % inhibition : pas besoin de recr�er le type-jack, il suffit de
           % recr�er le type-error
           self.inhibitedCtxt(end+1)= R.ranks(k); % numero de colonne dans self.news: comme R.ranks!
           self.BN(idx)= 0;
           self.value(idx,:)= bm0.value(idx,:);            
        end        
    end
    
    self.opt.set('type','error','inhibition','1');
    IO= tbx.FILE(tbx.FILEconfig.folder(self,'stock'));    
    pars= self.opt.get();    
    IO.save(self,pars{:});   
end



%%
end

