function phase=false_phase(phase,phase_info,times)
% mets des -1 sur les bins flagg�s continues qui tombent dans une phase non
% continue
phase=phase(:);
return; % ATTENTION, DESACTIVEE

vpi= uo_(arrayfun(@(x) [x.phase_id,x.begin,x.end]',phase_info,'uni',0));
vpi(vpi(:,1)==0,:)= [];
vpi(:,1)=[];

i1= bsxfun(@le,times,vpi(:,2)');
i2= bsxfun(@gt,times,vpi(:,1)');
i= any(i1&i2,2); % indices des bins dans une phase non continue
i= i& phase==0; % phase mal not�e
if any(i)
   DBG=1; 
end
phase(i)= -1;

if any(phase==-1)
   WWW=1;
end