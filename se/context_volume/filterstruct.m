function s= filterstruct(s,i,j,except)

if nargin<4 ; except= {};end
names = fieldnames(s);
names= setdiff(names,except);
N= length(names);

ei= isempty(i);
ej= isempty(j);
if ei & ~ej
    for n=1:N
        name= names{n};
        old= getfield(s,name);
        s= setfield(s,name,old(:,j));
    end
elseif ej & ~ei
    for n=1:N
        name= names{n};
        old= getfield(s,name);
        s= setfield(s,name,old(i,:));
    end
else
    assert(~ei & ~ej)
    for n=1:N
        name= names{n};
        old= getfield(s,name);
        s= setfield(s,name,old(i,j));
    end
end