 function self= gatherResults(idx, useInhibition, isFinal,cb_newinfo)
% Rassemble les meilleurs mod?les de diff?rents titres
%
%

if nargin< 2;useInhibition=0;end
self=[];
self.useInhibition= useInhibition;
self.final_= isFinal;
tbx= newsProjectTbx();
self.failed= {};
self.alldata= gather(self,idx,cb_newinfo);
self= vectorialize(self);


self.display= @display_;

self.get= @get_;
self.allopt= @allopt;
self.gatherAll= @gatherAll;
self.tableauModel= @tableauModel;


function main()
    ip= indexProcess('CAC40');
    dts= [];
    format= 'dd-mmm-yyyy';
    dts(1)= datenum('03-Jan-2007',format);
    dts(2)= datenum('29-Jun-2010',format);                

    ip.contextualLearningAll(ip,'MRANK')

    R= ip.gatherCompare(ip);

    res= gatherResults(ip.comp(:)');
    res.display(res)
       
    
    p=res.get(res,26);
   
    plot(p.value');legend(p.rownames);    
    dts= cellfun(@(x) x(1:5),p.colnames,'uniformOUtput',0);
    z= 1:4:size(p.value,2);
    set(gca,'Xtick',z);
    set(gca,'XtickLabel',dts(z));
    
end

function [alldata,failed]= gather(self, idx, cb_newinfo)
    
    alldata={};
    for id=(idx(:))'
       IO= tbx.FILE(tbx.FILEconfig.folder(cb_newinfo(id),'stock'));
       if self.final_== 1
            ids= {'type','BESTmodel','inhibition',self.useInhibition};            
            try
                data= IO.loadSingle(ids{:});
            catch ME
                info=[]; info.sec_id= id;info.err= ME;
                self.failed{end+1}= info;
                continue
            end
            data= data{1}.savedVariable;                       
           ids= {'type','FINALmodel','inhibition',self.useInhibition,'nonews',1};
           try
               withNN= IO.loadSingle(ids{:});
           catch
               continue 
           end
           withNN= withNN{1}.savedVariable;
           if self.useInhibition;
               if ~isfield(data.best,'inhibitedCtxt')
                   data.best.inhibitedCtxt= []; % ranks est vide aussi ?
               end
               inh_=data.best.inhibitedCtxt;                        
           end
           data.best= withNN.ER;   
           if self.useInhibition;data.best.inhibitedCtxt=inh_ ;end
           perf= recomputePerf(data);
           data.AS= perf.AS;
           data.RS= perf.RS;
           data.noNewsModel= withNN.M;
           alldata{end+1}= data; 
       else           
            ids= {'type','BESTmodel','inhibition',self.useInhibition};
            try
                data= IO.loadSingle(ids{:});
            catch ME
                info=[]; info.sec_id= id;info.err= ME;
                self.failed{end+1}= info;
                continue
            end    
            data= data{1};
            alldata{end+1}= data.savedVariable;        
       end       
       info_=cb_newinfo(id);
       alldata{end}.best.destinationID= info_{2};
       alldata{end}.best.original_date= info_{3};
       
       if length(info_)<4; alldata{end}.best.path_modifier='';
       else; alldata{end}.best.path_modifier= info_{4}; end
       alldata{end}.best.ranks= get_ranks(alldata{end}.best);
       
    end
    % TODO: finir de rassembler les news selon leurs ID et non leur num?ro
    % de colonne !
    %r= gatherRanks(alldata);
end

     function R= get_ranks(model)
         if model.filetrace.contains_key('rankLoad')
             R= tbx.getFromFileTrace(model.filetrace,'rankLoad',model);
             R=R.savedVariable;
             S= model.opt.get('seuil');
             assert(~isnan(S))
    %          if isnan(S) % M0
    %              R.ranks= [];             
    %          else
             sb= model.opt.get('sbreak');
             f_= find(R.note> S); % ctxt ?limin?s via notes
             if isempty(f_)
             else
                 if sb==0;f_= f_(1):length(R.ranks);end
                 if length(f_) == length(R.ranks)
                     wwww=1;
                 end
             end
             u= unique(model.BN); u=setdiff(u,0);
             assert(all(ismember(u,R.ranks)));
             R.ranks(f_)= [];
             R.indiceEffectif(:,f_)=[];             
             u= unique(model.BN); u=setdiff(u,0);
             assert(all(ismember(u,R.ranks)));
            %end
            R= R.ranks;
         else
            R= []; 
         end
     end


function x=homogeneiser_(x)
    if ~isfield(x.best,'inhibitedCtxt'); x.best.inhibitedCtxt= [];end
end
function self= vectorialize(self)    
    alldata= cellfun(@(x)x,self.alldata);   
    self.v=[];
    alldata= arrayfun(@homogeneiser_,alldata);
    
    N_=length(alldata);
    for n_= 1:N_;
        if isfield(alldata(n_).best,'note')
            % juste pour total ???
            alldata(n_).best= rmfield(alldata(n_).best,'note');
        end
    end
    for n_= 1:N_;if ~isfield(alldata(n_).best,'inhibition_ratio');alldata(n_).best.inhibition_ratio=1;end;end
    x=[alldata.best];
    self.v.IDstock= [x.IDstock];
    self.v.names= arrayfun(@(x) x.best.NAMEstock,alldata,'UniformOutput',0);
    
    self.v.AS= [];  
    x= [alldata.AS];
    N= length(alldata);
    K= length([x.fs])/N;
    self.v.AS.fs= reshape([x.fs],K,N)';
    self.v.AS.no= reshape([x.no],K,N)';
    self.v.AS.prob= [x.prob];
    self.v.AS.nctxt= [x.nctxt];
    
    self.v.RS= [];  
    x= [alldata.RS];
    N= length(alldata);
    K= length([x.fs])/N;
    self.v.RS.fs= reshape([x.fs],K,N)';
    self.v.RS.no= reshape([x.no],K,N)';
    self.v.RS.prob= [x.prob];
    self.v.RS.nctxt= [x.nctxt];
    self.v.RS.inhib= zeros(size(self.v.RS.nctxt));
    try
        self.v.RS.inhib= arrayfun(@(x)length(x.best.inhibitedCtxt),alldata);
    end
    try
        self.v.RS.colnames= alldata(1).RS.colnames;
    end
    self.v.RS.nctxt= arrayfun(@(x)length(unique(x.best.BN)),alldata);
    
    self.opt= arrayfun(@(x) x.best.opt,alldata,'UniformOutput',0);
end

     function c= allopt(self,key)
         %N= length(self.opt);
         f= @(x) x.get(key);
         c= cellfun(f,self.opt,'uni',0);
     end


 end

 
 
 function s= concatopt_(x)
     N= length(x);
     s=[];
     for n=1:N
         s=[s ' ' num2str(x{n})];
     end
 end
 
 function s=names_(self)
     s1= char(self.v.names);
     T= size(s1,1);
     s2= repmat(' (',T,1);
     s3= num2str( self.v.IDstock(:));
     s4= repmat(') ',T,1);
     s5= num2str(floor(100*self.v.RS.prob(:)));
     s5= [s5, repmat('%',size(s1,1),1)];
     s=[s1,s2,s3,s4,s5];
 end
 
 function [res,nn]= tableauModel(self)
 N= length(self.opt);
 res= options({'pv',0,'mpv',0,'std',0,'mean',0,'median',0});
 for n=1:N
     noter= self.opt{n}.get('noter');
     res.set(noter,res.get(noter)+1);
     est= self.opt{n}.get('estimateur');
     res.set(est,res.get(est)+1);
 end
 nn= nan;
 if self.final_
     nn= options({'filter',0,'20',0,'30',0,'50',0,'100',0,'200',0});
     N= length(self.alldata);
      for n=1:N
          if length(self.alldata{n}.noNewsModel)<4
              filter=0;
          else
            filter= self.alldata{n}.noNewsModel{4};
          end
         if filter==1; nn.set('filter',nn.get('filter')+1);end
         l= self.alldata{n}.noNewsModel{2};
         ll= num2str(l);         
         nn.set(ll,nn.get(ll)+1);
         mdl= func2str(self.alldata{n}.noNewsModel{1});
         if ~nn.contains_key(mdl); nn.set(mdl,0);end
         nn.set(mdl,nn.get(mdl)+1);
      end
 end
 end
 
 
 %%
 function count= display_(self)
 if self.useInhibition; displayInhibition(self);end
    maintitle= [];
    if self.useInhibition
        maintitle= ['<Inhibition> used.'];
    end
    if self.final_
        maintitle= [maintitle ' <No context optimized> used'];
    end
    criterename= 'cash 1mn';
    try
        CN_= self.v.RS.colnames;
    catch
        CN_= self.alldata{1}.best.colnames;
    end
    critere= find(strcmpi(criterename,CN_));
    assert(length(critere)==1);
    % catch critere= 4;
    
    [P,Q] = size(self.v.AS.fs);
    count= options();
    count.data= self.v;
    count.set('pv',0);count.set('mpv',0);count.set('std',0);
    count.set('None',0);
    count.set('median',0);count.set('mean',0);
    count.set('ag',0);
    for p=1:P
        pars= concatopt_(self.opt{p}.get());
        if self.v.AS.prob(p)== 0;
            pars= 'Non model found';
        end
        disp([self.v.names{p} ' -> ' pars]);
        
        noter= self.opt{p}.get('noter');
        count.set(noter,count.get(noter)+1);
        est= self.opt{p}.get('estimateur');
        count.set(est,count.get(est)+1);
    end
 
    nTotalCtxt= cellfun(@(x) length(x.best.news.colnames), self.alldata);    
    nTotalCtxt=max(nTotalCtxt);
    
    
    figure;    
    my_buffer('GR_display',gcf);
    subplot(1,5,2:3);hold on
    
    N_stock_= length(self.v.RS.prob);
    plot(self.v.RS.prob,1:P,'b*');        
    plot(self.v.RS.nctxt./nTotalCtxt,1:P,'r+');
    plot(self.v.RS.inhib./nTotalCtxt,1:P,'k.');
    legend({'P(context switch)',['Nb selected context / ' num2str(nTotalCtxt)],'Nb inhibited context'});  
    set(gca,'Ytick',1:P)
    set(gca,'Yticklabel',names_(self) )
    title(['Context switch \newline' maintitle ]);
    set(gca,'ylim',[0 N_stock_+5])
%     N_stock_= length(self.v.RS.prob);
%     matrixplot(self.v.RS.prob',cellstr(names_(self)),'nobone',30);    
%     subplot(1,10,5:6)
%     matrixplot([self.v.RS.nctxt;self.v.RS.inhib]','nobone',30);
    
    %plot(self.v.RS.prob,1:P,'b*');        
    %plot(self.v.RS.nctxt./nTotalCtxt,1:P,'r+');
    %plot(self.v.RS.inhib./nTotalCtxt,1:P,'k.');
    %legend({'P(context switch)','Nb selected context','Nb inhibited context'});  
    %set(gca,'Ytick',1:P)
    %set(gca,'Yticklabel',names_(self) )
    title(['Context switch \newline' maintitle ]);
    set(gca,'ylim',[0 N_stock_+5])
    
    subplot(1,5,4:5)    
    hold on
    plot(self.v.RS.fs(:,critere),1:P,'k+');
    plot(self.v.RS.no(:,critere),1:P,'r.');
    x_= self.v.RS.fs(:,critere);
    x_(isnan(x_))= 1;
    mfs= mean(x_);
    i= isnan(self.v.RS.no(:,critere));
    self.v.RS.no(i,critere)= 1;
    mno= mean(self.v.RS.no(:,critere));
    legend({'Full sample error','Context switch error'});%,'No context error'});      
    set(gca,'ylim',[0 N_stock_+5])
    %title(['Performances (multiscale criteria) \newline full sample ->' num2str(mfs,2) ' context only->' num2str(mno,2)  ])

    title(['Performances (multiscale criteria) \newline full sample ->' num2str(mfs,2) ' context only->' num2str(mno,2)  ])
    
    
    figure
    my_buffer('GR_display_volume',gcf);
        subplot(1,5,2:3);hold on
    plot(self.v.RS.fs(:,1),1:P,'b*');        
    plot(self.v.RS.fs(:,2),1:P,'r+');
    plot(self.v.RS.fs(:,3),1:P,'k.');
    legend({'l^2','l^1','KS'});  
    set(gca,'Ytick',1:P)
    set(gca,'Yticklabel',names_(self) )
    title(['volume forecast quality (15min,full sample)' ]);
    set(gca,'ylim',[0 N_stock_+5]);
        subplot(1,5,4:5);hold on
    plot(self.v.RS.no(:,1),1:P,'b*');        
    plot(self.v.RS.no(:,2),1:P,'r+');
    plot(self.v.RS.no(:,3),1:P,'k.');
    legend({'l^2','l^1','KS'});  
    set(gca,'Ytick',1:P)
    title(['volume forecast quality (when context selected)' ]);
    set(gca,'ylim',[0 N_stock_+5]);
    
    
%    subplot(1,4,4)
        
%     subplot(1,4,1:2)
%     
%     plot(self.v.RS.prob,1:P,'b*');
%     set(gca,'Ytick',1:P)
%     set(gca,'Yticklabel',names_(self) )
%     title('Probability of context switch');
%     subplot(1,4,3)
%     title('Performances')
%     hold on
%     plot(self.v.RS.fs(:,4),1:P,'k+');
%     plot(self.v.RS.no(:,4),1:P,'r.');
%     legend({'Full sample error','Context switch error'});      
%     subplot(1,4,4)
%     hold on
%     plot(self.v.RS.inhib,1:P,'k+');
%     plot(self.v.RS.nctxt,1:P,'r.');
%     legend({'Nb selected context','Nb inhibited context'});  
    
    %if self.useInhibition; displayInhibition(self);end
 end
 
 
 function displayInhibition(self)
 CTXT= rassemblerNews( cellfun( @(x) x.best.news,self.alldata,'uni',0 ) ) ;
 d= cellfun(@(x) length(x.best.news.colnames ),self.alldata  );
 [d,dim_max]=max(d);
 Mnn= self.alldata{dim_max}.best.news.colnames;
 ndb= newsDb();
 nn= ndb.id2name(CTXT);
 N=length(self.alldata);
 m=zeros(length(CTXT),N);
 for n=1:N       
     best_= self.alldata{n}.best;
     try;IC= best_.inhibitedCtxt;
     catch; IC=[];
     end          
      
     RK_id= best_.news.col_id(best_.ranks) ;
     m(ismember(CTXT,RK_id),n)= 1; 
     
     IC_id= best_.news.col_id(IC);
     m(ismember(CTXT,IC_id),n)= 2; 
     
%      non_selected= setdiff(CTXT, unique(best_.BN) );
%      non_selected_id= best_.news.col_id(non_selected);
%      non_selected_id_non_inhibited= setdiff(non_selected_id,IC_id);
%      m(non_selected_id_non_inhibited,n)= 1;
 end
 [~,idx_]= sort(nn);
 m= m(idx_,:);nn= nn(idx_);
 figure, matrixplot(m,nn); title('Context Selection (green) and inhibition (black)');
 my_buffer('GR_display_inhib',gcf);
 end
 
 function l= rassemblerNews(c)
 % rassemble les news pr?sentes dans un cellarray de structure news.
 % Identifie les news via col_id !
 o= options();
 N= length(c);
 for n=1:N
     P= length(c{n}.colnames);
     for p=1:P
         o.set(c{n}.col_id(p),0);         
     end     
 end
 l= o.get();
 l=l(1:2:end);
 l=[l{:}];
 end

 
 function display_ov(self)
    [P,Q] = size(self.v.AS.fs);
    for p=1:P
        pars= concatopt_(self.opt{p}.get());
        if self.v.AS.prob(p)== 0;
            pars= 'Non model found';
        end
        disp([self.v.names{p} ' -> ' pars]);
    end
 
    figure;
    hold on
    plot(self.v.RS.prob,1:P,'b*');
    plot(self.v.RS.fs(:,4),1:P,'k+');
    plot(self.v.RS.no(:,4),1:P,'r.');
    title('Performances')
    set(gca,'Ytick',1:P)
    set(gca,'Yticklabel',names_(self) )
    legend({'Probability of context switch','Full sample error','Context switch error'});      
 end
 
 
 function [p,data]= get_(self, IDstock)
 i= cellfun(@(x) x.best.IDstock, self.alldata);
 i= find(i==IDstock); assert(length(i)==1);
 
 file= self.alldata{i}.best.filetrace.get('jackSave'); % attention full path ici !
 %tbx= newsProjectTbx();
 %rep= tbx.FILEconfig.folder(IDstock,'stock');
 %file= [rep, file];
 data= load(file);
 
 em2= eModel2('tbx');
 p= em2.contextProfiles(data.savedVariable);
 end
 
 
 function R= gatherRanks(alldata )
    tbx= newsProjectTbx();
    N=length(alldata);
    allR= options();
    for n=1:N
        m= alldata{n}.best;
        try;
            R= tbx.getFromFileTrace(m.filetrace,'rankLoad',m.IDstock);
            R= R.savedVariable;
        catch ME
            assert(strcmpi(ME.message,'key <rankLoad> not available'));         
            continue
        end
        R= R.news.col_id(R.ranks);
    end      
 
 end
 
 function [r,r0,r1]= gatherAll(self,mode)
 if nargin== 1;mode= 'ctxt';end
 if strcmpi(mode,'ctxt');
     [r,r0,r1]= gatherAll_groupCtxt(self);
 elseif strcmpi(mode,'best');
    [r,r0,r1]= gatherAll_groupBest(self);
elseif strcmpi(mode,'cum');
    [r,r0,r1]= gatherAll_groupCumul(self);
 else; assert(0);
 end
 end
 
 function [ratio,errWhenNewsOccurs,M0WhenNewsOccurs]= gatherAll_groupCtxt(self)
 N= length(self.alldata);
 coln= 'cash 1mn';
 
 errWhenNewsOccurs= [];
 M0WhenNewsOccurs= [];
 
 for n=1:N
     dn= self.alldata{n};
     i= find(strcmpi(dn.best.colnames,coln)); assert(length(i)==1);
     iM0= find(strcmpi(dn.M0.colnames,coln)); assert(length(iM0)==1);
     err= dn.best.value(:,i);   
     errM0= dn.M0.value(:,iM0);   
     for p=1:length(dn.best.news.colnames)
         % class? par id de news !
         idx= dn.best.news.value(:,p);
         errWhenNewsOccurs(dn.best.news.col_id(p),n)= mean(err(idx==1));
     end
     for p=1:length(dn.M0.news.colnames)
         idxM0= dn.M0.news.value(:,p);
         M0WhenNewsOccurs(dn.M0.news.col_id(p),n)= mean(errM0(idx==1));         
     end 
 end
 
 ratio= errWhenNewsOccurs./M0WhenNewsOccurs;
 ratio(any(isnan(ratio),2),:)=[];
 % todo: faire correspondre lignes aux labels des news ?
 end
 
 function [ratio,errWhenNewsOccurs,M0WhenNewsOccurs]= gatherAll_groupBest(self)
 N= length(self.alldata);
 coln= 'cash 1mn';
 
 errWhenNewsOccurs= [];
 M0WhenNewsOccurs= [];
 
 for n=1:N
     dn= self.alldata{n};
     i= find(strcmpi(dn.best.colnames,coln)); assert(length(i)==1);
     iM0= find(strcmpi(dn.M0.colnames,coln)); assert(length(iM0)==1);
     err= dn.best.value(:,i);   
     errM0= dn.M0.value(:,iM0);   
     for p=1:length(dn.best.ranks)
         % class? par id de news !
         idx= dn.best.BN== dn.best.ranks(p);
         errWhenNewsOccurs(p,n)= mean(err(idx==1));              
         M0WhenNewsOccurs(p,n)= mean(errM0(idx==1));         
     end 
 end
 
 ratio= errWhenNewsOccurs./M0WhenNewsOccurs;
 ratio(isnan(ratio))= 1;
 %ratio(any(isnan(ratio),2),:)=[];
 % todo: faire correspondre lignes aux labels des news ?

 figure,
 matrixplot(ratio);
ylabel('Best news number')
 
 end
 
function [ratio,errWhenNewsOccurs,M0WhenNewsOccurs]= gatherAll_groupCumul(self)
 N= length(self.alldata);
 coln= 'cash 1mn';
 
 errWhenNewsOccurs= [];
 M0WhenNewsOccurs= [];
 
 for n=1:N
     dn= self.alldata{n};
     i= find(strcmpi(dn.best.colnames,coln)); assert(length(i)==1);
     iM0= find(strcmpi(dn.M0.colnames,coln)); assert(length(iM0)==1);
     err= dn.best.value(:,i);   
     errM0= dn.M0.value(:,iM0);   
     for p=1:length(dn.best.ranks)
         % class? par id de news !
         %idx= dn.best.BN== dn.best.ranks(p);
         idx= ismember(dn.best.BN, dn.best.ranks(1:p));
         errWhenNewsOccurs(p,n)= mean(err(idx==1));              
         M0WhenNewsOccurs(p,n)= mean(errM0(idx==1));         
     end 
 end
 
 ratio= errWhenNewsOccurs./M0WhenNewsOccurs;
 ratio(isnan(ratio))= 1;
 %ratio(any(isnan(ratio),2),:)=[];
 % todo: faire correspondre lignes aux labels des news ?

 figure,
 matrixplot(ratio);
ylabel('Best news number')
title('Cumulative perf')
 
 end
 
 
 %%
 function display_OV(self)
    [P,Q] = size(self.v.AS.fs);
    for p=1:P
        pars= concatopt_(self.opt{p}.get());
        if self.v.AS.prob(p)== 0;
            pars= 'Non model found';
        end
        disp([self.v.names{p} ' -> ' pars]);
    end
 
    figure;
    plot(self.v.RS.prob,1:P,'k.');
    title('P(switch context)')
    set(gca,'Ytick',1:P)
    set(gca,'Yticklabel',names_(self) )
    
    figure;
    plot(self.v.RS.fs(:,4),1:P,'k.');
    title('Error | Full sample')
    set(gca,'Ytick',1:P)
    set(gca,'Yticklabel',names_(self) )
    
    figure;
    plot(self.v.RS.no(:,4),1:P,'k.');
    title('Error | context switch')
    set(gca,'Ytick',1:P)
    set(gca,'Yticklabel',names_(self) )
 end