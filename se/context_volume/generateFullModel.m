function [preds,B,R,plotfun,preds_non_inhibe]= generateFullModel(ip,varargin)
% Reconstruire le r?sultat du learning, sans jacknife en tant que
% pr?dicteur
%
% responsable de passer sur les vrais newsID
% doit rapatrier tous les fichiers

if nargin==0;
    F= [];
    F.get_ranks= @get_ranks;
    F.getBasicModel= @getBasicModel;
    F.computePred= @computePred;
    F.plot= @plot_;   
    preds= F;
    B=[];R=[];
    return ;
end
[preds,B,R,plotfun,preds_non_inhibe]= fullProcess(ip,varargin{:});

end

function [preds,B,R,plotfun,preds_non_inhibe]= fullProcess(ip,varargin)    
    tbx= newsProjectTbx();
    %filedes= {'type','FINALmodel','inhibition',0,'nonews',0};        
    [B,R]= getBasicModel(ip,varargin{:});
    [preds,stats,preds_non_inhibe]= computePred(B,R);
    %disp(stats);
    plotfun= @(varargin) plot_(preds,stats,R); %[figure(),plot_(preds,stats,R)];
end

function [preds,stats,preds_non_inhibe]= computePred(B,R)
    inhib= [];
    stats= []; %(nb obs in news, nb obs in IE, nb obs in BN, isInhibited )
    tbx= newsProjectTbx();
    % pred(:,[1 2 3]) = [rank,col_DB_id,colnum dans news]
    % pred(:,4:end) = volume profile
    IE= R.indiceEffectif;
    est=  R.estimators.get(B.opt.get('estimateur'));
    preds= [];
    preds_non_inhibe= [];
    for k= 1:length(R.ranks)
        %current= B.ranks(k); % indice de colonne
        current= R.ranks(k); % indice de colonne
        db_ID= R.news.col_id(current); % id dans base    
        idx= IE(:,k); % dates de selection BN
        if isfield(B,'inhibitedCtxt') & ismember(current,B.inhibitedCtxt)
            assert(~ismember(current,B.BN));
            profile= -1*ones(1,size(R.volume.value,2));
            inhib(end+1)= k;
            stats(end+1,:)=[sum(B.news.value(:,current)),sum(idx),sum(B.BN== current) ,1];
        else        
            assert(all(B.BN(idx~=0) == current));
            not_nan = any(~isnan(R.volume.value(IE(:,k)==1,:)));
            profile = nan(1,length(not_nan));
            profile(not_nan) = est(R.volume.value(IE(:,k)==1,not_nan));
            stats(end+1,:)=[sum(B.news.value(:,current)),sum(idx),sum(B.BN== current) ,0];
            assert(abs(nansum(profile)-1) <0.0001);
        end        
        preds_non_inhibe(k,:) = [k,db_ID,B.ranks(k),est(R.volume.value(IE(:,k)==1,:))];   
        preds(k,:)= [k,db_ID,B.ranks(k),profile];    
    end    
    % no news forecast
    profile_NN = nan(1,size(R.volume.value,2));
    idx = all(isnan(R.volume.value(:,~all(~isnan(R.volume.value)))),2);
    profile_NN(all(~isnan(R.volume.value))) = est(R.volume.value(idx,all(~isnan(R.volume.value))));
    assert(all(all(preds(inhib,4:end)==-1)));
    preds(inhib,4:end)= repmat(profile_NN,length(inhib),1);                
    preds(length(R.ranks)+1,:)= [length(R.ranks)+1,0,0,profile_NN]; 
    preds_non_inhibe(length(R.ranks)+1,:)= [length(R.ranks)+1,0,0,profile_NN];     
    stats(end+1,:)= [size(B.news.value,1),sum(B.BN== 0),sum(B.BN== 0) ,0];
    assert(all( abs(nansum(preds(:,4:end),2)-1)< 0.000001 ));
    
    i_= stats(:,4)==0;
    assert(all(stats(i_,2)==stats(i_,3)));
    assert(all(stats(:,1)>= stats(:,2)));
    i_= stats(:,4)==1;
    %assert(all(stats(i_,2)==0));
    assert(all(stats(i_,3)==0));
end

function [B,R,data]= getBasicModel(stockID,varargin)
    tbx= newsProjectTbx();
    %get_ranks= generateFullModel;
    %filedes= {'type','BESTmodel','inhibition',0};        
    filedes= varargin;
    IO= tbx.FILE(tbx.FILEconfig.folder(stockID,'stock'));
    data= IO.loadSingle(filedes{:});
    data= data{1}.savedVariable;
    if isfield(data,'M')&isfield(data,'ER')&isfield(data,'J')&isfield(data,'res')
        % FINAL model
        B= data.ER;   
        if isfield(data.J,'inhibitedCtxt')
           B.inhibitedCtxt=  data.J.inhibitedCtxt;
        end
        B.TD= data.J.IDdestination;
        B.original_date= data.J.volume.original_date;
        if iscell(stockID{2})
            %assert(iscell(B.TD) && isempty(B.TD) && isempty(stockID{2}));
            assert(isempty(B.TD) && isempty(stockID{2}));
        else
            if isempty(stockID{2}); assert(isempty(B.TD))
            else
                assert(stockID{2} == B.TD);
            end
        end
        assert(all(B.original_date == stockID{3}));
    else
        B= data.best;
        if isfield(B,'inhibitedCtxt')
            assert(~any(ismember(B.BN,B.inhibitedCtxt)));
        end
        B.TD= stockID{2};
        B.original_date= stockID{3};

    end    
    if ~isfield(B,'path_modifier')
        B.path_modifier = stockID{4} ;
    end
    
    %assert(length(B.note)==length(B.ranks));
    seuil= B.opt.get('seuil');    
    R= get_ranks(B,data.J);   
    %assert(all(ismember(B.BN,[0;R.ranks(:)])))
    assert(all(ismember(B.BN,[0;R.ranks(:)])))
    u= unique(B.BN);u= setdiff(u,0);
    for uu =(u(:)')
        assert(all( B.news.value(B.BN==uu,uu)==1) )
    end
    if ~isempty(R.ranks)
        assert(length(R.indiceEffectif)==length(B.news.date));
    end
    if ~isfield(B,'ranks')
       B.ranks= R.ranks;       
    end
    B.no_news_string= model2str(data);
    R.no_news_string= B.no_news_string;
    
    [~,i]= sort(datenum(data.J.volume.colnames));
    data.J.volume.colnames= data.J.volume.colnames(i);
    data.J.volume.value= data.J.volume.value(:,i);
    data.J.volume.phases= data.J.volume.phases(i);
    [~,i]= sort(datenum(R.volume.colnames));
    R.volume.colnames= R.volume.colnames(i);
    R.volume.value= R.volume.value(:,i);
    R.volume.phases= R.volume.phases(i);
    
    assert(issorted(datenum(data.J.volume.colnames)))
    assert(issorted(datenum(R.volume.colnames)))
    % DONE: assurer la monotonie des times
    % FIXME: rendre monotone les buffers de volume f-learning
end

function R= get_ranks(model, Jdata)
    tbx= newsProjectTbx();
     if model.filetrace.contains_key('rankLoad')
         R= tbx.getFromFileTrace(model.filetrace,'rankLoad',model); %.IDstock);        
         R=R.savedVariable;
         assert(length(R.note)== length(R.ranks))
         assert(size(R.indiceEffectif,2)== length(R.ranks))
         S= model.opt.get('seuil');
         assert(~isnan(S))
         sb= model.opt.get('sbreak');
         f_= find(R.note> S); % ctxt ?limin?s via notes
         if ~isempty(f_)
             if sb==0;f_= f_(1):length(R.ranks);end
             if length(f_) == length(R.ranks)
                 wwww=1;
             end
             R.ranks(f_)= [];
             R.indiceEffectif(:,f_)=[];  
         end
         u= unique(model.BN); u= setdiff(u,0);
         assert(all(ismember(u,R.ranks)));
        %end
        %R= R.ranks;
     else
        R= []; 
        R.volume= Jdata.volume;
        R.estimators= Jdata.estimators;
        R.ranks= [];
        R.indiceEffectif= ones(size(model.news.value,1),1);
     end
 end

 function plot_(preds,stats,R)
    figure, hold on;
    T= size(preds,1);
    cmp= hsv(T);
    for t=1:T
        ls= '-';
        col= cmp(t,:);
        if stats(t,4)== 1; ls= '-.';end
        if t== T; col= [0,0,0];end 
        plot(preds(t,4:end),'color',col,'linestyle',ls);
    end        
    lgd_= R.news.colnames(preds(1:end-1,3)); lgd_{end+1}= 'full sample';
    lgd_= cellfun(@(x) strrep(x,'_','.') ,lgd_,'uni',0);
    N=length(lgd_);
    for n=1:N        
        lgd_{n}= ['(',num2str(n),') ', lgd_{n} , ' ',num2str(stats(n,3)), '/',num2str(stats(n,1)) ];
        if stats(n,4)== 1
            lgd_{n}= [lgd_{n}, ' inh'];
        end
    end
    legend(lgd_,'location','NorthWest');
    title(R.NAMEstock);
 end
 
 %%
 function example()
 
 ID= 2
 
[preds,B,R]= generateFullModel(18,'type','BESTmodel','inhibition',0)
[preds,B,R]= generateFullModel(2,'type','BESTmodel','inhibition',0)
[preds,B,R]= generateFullModel(11,'type','BESTmodel','inhibition',0)

[preds,B,R]= generateFullModel(ID,'type','BESTmodel','inhibition',0)
[preds,B,R]= generateFullModel(ID,'type','BESTmodel','inhibition',1)
[preds,B,R]= generateFullModel(ID,'type','FINALmodel','inhibition',0)
[preds,B,R]= generateFullModel(ID,'type','FINALmodel','inhibition',1)
 
 end

 
 function pourRomain()
  ID= 276
[preds,B,R]= generateFullModel(ID,'type','BESTmodel','inhibition',0)
 
 
lgd_= R.news.colnames(preds(1:end-1,3))
 lgd_{end+1}= 'full sample';
 lgd_= cellfun(@(x) strrep(x,'_','.') ,lgd_,'uni',0);
 x=[];
 x.lignes_contexte= lgd_;
  x.preds= preds(:,4:end);
 
  figure
 plot(x.preds');legend(x.lignes_contexte)
 plot_= @(varargin) { plot(x.preds'),legend(x.lignes_contexte)}
 x.plot= plot_;
 
 
 end
 