function [FM,BM,perf_ctxt]=getCVbuffer(identifiant)
% [FM,BM,perf_ctxt]=getCVbuffer(identifiant) Get Buffers from Context Volume 
%  identifiant= {sec_id, TD, dates, path_modifier}
%
% exemples: 
%    identifiant={2,[],[733774,735112],''};    
%    d=GLD(8,[]);[FM,BM]=getCVbuffer(d)
%

tbx=newsProjectTbx();

% pour les buffers de data, autre répertoire !
%IO= FileToolbox(tbx.FILEconfig.folder(identifiant,'buffer'));

IO= FileToolbox(tbx.FILEconfig.folder(identifiant,'stock'));
FM= IO.loadSingle('type','FINALmodel');
FM= FM{1}.savedVariable;

% FM.ER = critereOp (ErrorComputer) (.value, chaque jour)
% FM.J  = Jacknife (.note, .rank)
IO= FileToolbox(tbx.FILEconfig.folder(identifiant,'stock'));
BM= IO.loadSingle('type','BESTmodel','inhibition',1);
BM= BM{1}.savedVariable;
% BM.AS et BM.RS: Absolute and Relative perf

TLO= test_learning_output();
ip=[];ip.comp=identifiant{1};ip.TD=identifiant{2};ip.dates= identifiant{3};
res01= gatherResults(ip.comp(:)',1,0,@(sec_id) cb_newinfo(ip,sec_id));
perf_ctxt= [];
[perf_ctxt.X,perf_ctxt.keys,perf_ctxt.X0,perf_ctxt.key0]= TLO.show_co(res01.alldata,0);

% o= FM.J.opt.get();
% 
% l= IO.parse('type','error',o{:}); % pas sur que le o{:} marche pour tout type de buffer
% ER= IO.loadSingle('type','error',o{:});
% ER= ER{1}.savedVariable;
% 
% IO= filetoolbox