function [s, fe, cvc] = get_contexts2be_applied(sec_id, varargin)
% GET_CONTEXTS2BE_APPLIED - retrieve the contex to be used for a period of time
%
% % se mettre dans le bon environnement pour les connections aux bases :
% % change_connections( 'production')
%
% % croiser les doigts ( quand on fait appel ? get_se, il faut avoir la foi et le coeur pur ) puis lancer :  
% cvc = vcse2vcdata(get_se('security','estimator', 'context_volume' , ...
%                       'security_id' , 110,...
%                       'trading_destination_id',[],...
%                       'is_valid', 1 , 'backup_process','none', ...
%                       'retrieve_all_context', true), datenum(0,0,0,0,5,0))
% 
% s = get_contexts2be_applied(110, 'from', today, 'to', today + 15);
% st_plot(st_data('keep', cvc, s.context2use(~ismember(weekday(today:today+15), [1 7]))))
%
% Examples:
%
% 
% 
%
% See also: get_cal_event
%    
%
%   author   : 'rburgot@cheuvreux.com'
%   reviewer : ''
%   date     :  '19/04/2012'
%   
%   last_checkin_info : $Header: get_contexts2be_applied.m: Revision: 12: Author: malas: Date: 03/26/2013 03:31:58 PM$

global st_version
quant = st_version.bases.quant;

opt = options({'from', today, 'to', today,...
    'trading_destination_id', [], ...
    },varargin);

td_id = opt.get('trading_destination_id');
from = opt.get('from');
to   = opt.get('to');
if ~isnumeric(from)
    from_str = from;
    from = datenum(from, 'dd/m/yyyy');
else
    from_str = datestr(from, 'dd/mm/yyyy');
end
if ~isnumeric(to)
    to = datenum(to, 'dd/m/yyyy');
end


s = struct('date', (from:to)', 'context2use', [], 'event_type', []);
s.context2use = cell(size(s.date));
s.event_type = nan(size(s.date));
[s.context2use{:}] = deal('Usual Day');

if isempty(td_id) || ~isfinite(td_id)
    td_str = ' is null';
else
    td_str = ['= ' num2str(td_id)];
end

fe = generic_structure_request('QUANT', sprintf([...
    ' select  ' ...
    ' 	734504+datediff(day,''20110101'',cebd.event_date) as "date", ' ...
    ' 	cebd.event_type, ' ...
    ' 	c.context_name as "context_name", ' ...
    ' 	cr.ranking as "ranking" ' ...
    ' from  ' ...
    ' 	%s..cal_event_by_security cebd, ' ...
    ' 	',quant,'..context_ranking cr, ' ...
    ' 	',quant,'..context c, ' ...
    ' 	',quant,'..estimator e ' ...
    ' where ' ...
    ' 	cebd.security_id = %d ' ...
    ' 	and cebd.security_id = cr.security_id ' ...
    '   and cr.trading_destination_id ' td_str ...
    ' 	and cebd.event_date >= ''%s''  ' ...
    ' 	and cebd.event_date <= ''%s'' ' ...
    ' 	and cr.event_type = cebd.event_type  ' ...
    ' 	and c.context_id = cr.context_id ' ...
    '   and e.estimator_id = cr.estimator_id ' ...
    '   and lower(e.estimator_name) = ''context volume'' '...
    ' order by cebd.event_date, cr.ranking '], ...
    st_version.bases.QUANT_calendar, sec_id, ...
    datestr(from, 'yyyymmdd'), datestr(to, 'yyyymmdd')));

if ~isempty(fe)
    [dates, idx] = unique(fe.date, 'first');
    [~, idx2] = intersect(s.date, dates);
    
    s.context2use(idx2) = fe.context_name(idx);
    s.event_type(idx2) = fe.event_type(idx);
end
s.event_type(strcmp(s.context2use, 'Usual Day')) = NaN;

if nargout > 2
    if ~isfinite(td_id)
        td_id = [];
    end
    cvc = vcse2vcdata(get_se('security','estimator', 'context_volume' , ...
                      'security_id' , sec_id,...
                      'trading_destination_id',td_id,...
                      'is_valid', 1 , 'backup_process','none', ...
                      'retrieve_all_context', true, 'as_of_date', opt.try_get('cvc_as_of', from_str)), ...
                      datenum(0,0,0,0,5,0));
end

end