function idx= get_phase(dts,phases)

assert(0,'deprecated');
% il y a une priorit� des phases de fixing sur celle continues 
% (on peut appartenir � la plage de temps continue malgre un fixing au milieu)
% donc on fixe la 1ere (continue), puis on prends le max !!!
ph_id= arrayfun(@(x) x.phase_id,phases);
[~,i]= sort(ph_id,'ascend');
phases= phases(i);
assert(phases(1).phase_id==0);
ph_id= arrayfun(@(x) x.phase_id,phases);
ph_id=ph_id(:);


idx= arrayfun(@(x) dts > x.begin & dts <= x.end,phases,'uni',0);
idx= uo_(idx); % ligne= dts, colonne= chaque phase -> 0/
assert(all(ismember(unique(idx(:)),[0 1])));
assert(all(sum(idx,1)>= 1));
i= sum(idx,1)> 1;
assert(all(idx(1,i)==1));
assert(all(sum(idx(2:end,i)== 1)));
idx(1,i)= 0; % si autre phase, on enleve phase continue 

idx= bsxfun(@times,idx,ph_id);
idx= sum(idx);

%uo_(arrayfun(@(x)  mod(datenum(btest.volume.colnames),1) > x.begin & mod(datenum(btest.volume.colnames),1) <= x.end, btest.volume.info.phase_th,'uni',0))

