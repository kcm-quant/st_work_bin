function c = identifyLV(data)
% Try find information in data. Returns  {sec_id,TD,dates,folder}

assert(isstruct(data));

si= {'security_id'}%,'comp'};
sd={'dates'}%,'date'};
st= {'trading_destination', 'TD'};
sf= {'folder'};
subsearch = {'learn_options','volume','ip','options','opt'};

c={getone(data,si,subsearch),getone(data,st,subsearch),...
    getone(data,sd,subsearch),getone(data,sf,subsearch)};

function x= getone(data,keylist,subsearch)
x= nan;
for k=1:length(keylist)
    key= keylist{k};
    try
        x= data.get(key);
        break
    catch ME
        if isfield(data,key)
            x= getfield(data,key)
            break
        end
        x= nan;
        for i=1:length(subsearch)
           field=subsearch{i};
           if isfield(data,field)
               subdata= getfield(data,field);
               x= getone(subdata,keylist,subsearch);
               if valide(x)
                  break 
               end
           end            
        end     
        if valide(x)
          break 
       end
    end
end
if ~valide(x)
    DBG=1; 
end


function b=valide(x)
b= iscell(x) || ~any(isnan(x))


