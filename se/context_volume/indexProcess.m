function self= indexProcess(index, destination_id)
% Applies eModelPool on all component of an index
% PROJECT: context sensitive volume forecast
%

if nargin<2;destination_id={};end

% MRANK sur donn?es PIPO => <220

self= [];
self.class__= 'indexProcess.m';
self.destinationID= destination_id;
if nargin>=1
    self.useInhibition= 0;
    self.final_= 0;
    self.indexname= index;
    self.comp= nan;    
    % en cas d'interruptions de requetes qui fait planter  [get_repository('index-comp','DAX')] refaire ceci:
    % setdbprefs('DataReturnFormat', 'cellarray');
    self= getList(self,index);
    % FIXME: changement de sec_id sur bouygues
    self.comp= setdiff(self.comp,1512703);
end
self.bufferAll= @bufferAll;
self.contextualLearningAll= @contextualLearningAll;
self.contextualLearning1= @contextualLearning1;
self.gatherCompare=@gatherCompare;
self.test_= @profilePlotAll;
self.bufferNews= @bufferNews;

    function main()
        % INIT
        ip= indexProcess('CAC40');
        %ip= indexProcess('DAX');
        %ip= indexProcess('FTSE 100');
        dts= [];
        format= 'dd-mmm-yyyy';
        %dts(1)= datenum('03-Jan-2008',format);
        dts(1)= datenum('03-Jan-2006',format);
         dts(2)= datenum('15-May-2011',format);
        %dts(2)= datenum('20-Jan-2007',format);
        
        % SPECIAL COMPOSITION
        %ip.comp=ip.comp(ip.comp<=45);
        %ip.comp= [2,8,11];
        %ip.comp= ip.comp(ip.comp>=45)
        %ip.comp= ip.comp(ip.comp<=209)
        %ip.comp= ip.comp(ip.comp>=192 & ip.comp<=246)
        %ip.comp= ip.comp(ip.comp>11)
        %ip.comp= ip.comp(ip.comp<210)
        %ip.comp= 276
        %ip.comp= setdiff(ip.comp,2);
        %ip.comp= ip.comp(ip.comp>=157)        
        %ip.comp=[18 71 232];
%         ip.comp= setdiff(ip.comp,[18 71 232]);
%         ip.comp= ip.comp(ip.comp>=26)        
%         ip.comp=[2,8,11,18]
%         ip.comp=11;        
%         ip.comp=276;        
%         ip.comp= ip.comp(ip.comp>=61553);        
        % 6346        
        % le 245 n'est pas bufferis?
        ip.comp= 6069;
        ip.comp= 192;
        ip.comp=2
        
        ip.comp= ip.comp(ip.comp<220) 
        ip.comp= 192;
        ip.comp=ip.comp(ip.comp<=92);
        ip.comp(ip.comp==1261636)=[];
      
        ip.comp=ip.comp(ip.comp<6155);
        
        ip.comp= [46116,10481,6855,6759,6724];
        
        ip.comp= ip.comp(ip.comp<107509)
        ip.comp=ip.comp(ip.comp~=14334)
        % BUFFERS VOLUME
        st_log('$off$')
        ip.bufferAll(ip,dts);
        % BUFFER NEWS
        df= [] ;
        df(1)= datenum('03-Jan-2007',format);
        df(2)= datenum('29-Jun-2010',format);
        ip.bufferNews(ip,df)
        
        ip.useInhibition= 0; 
        ip.contextualLearningAll(ip,'all')

        % inhibition: cr?e des errorComputer, qu'il faut ensuite Mranker
        ip.contextualLearningAll(ip,'inhibition')        
        ip.useInhibition= 1;
        ip.contextualLearningAll(ip,'MRANK')
        
        %R= ip.gatherCompare(ip);
        
        % FINAL STEP
        ip.final_=0 ;
        res= gatherResults(ip.comp(:)',ip.useInhibition,ip.final_);
        res.display(res)
        
        ip.test_()
        
        saveFile= @(s) ['C:\perf' s '_' ip.indexname '.fig'];
        
        [r,nn]= res01.tableauModel(res01);r.get();nn.get()
        
        ip.useInhibition= 0; 
        ip.final_=0;        
        res00= gatherResults(ip.comp(:)',ip.useInhibition,ip.final_);
        res00.display(res00); % res00.
        saveas(gca,saveFile('00'));
        ip.final_=1;        
        res01= gatherResults(ip.comp(:)',ip.useInhibition,ip.final_);
        res01.display(res01)
        saveas(gca,saveFile('01'));
        ip.useInhibition= 1; 
        ip.final_=0;        
        res10= gatherResults(ip.comp(:)',ip.useInhibition,ip.final_);
        res10.display(res10)
        saveas(gca,saveFile('10'));
        ip.final_=1;        
        res11= gatherResults(ip.comp(:)',ip.useInhibition,ip.final_);
        res11.display(res11)
        saveas(gca,saveFile('11'));
        
        res.allopt(res,'noter')
        res.allopt(res,'estimateur')
    end
    function main_steps()
        % basic       
        ip.useInhibition=0 ;
        ip.contextualLearningAll(ip,'ranks')
        ip.contextualLearningAll(ip,'jack0')
        ip.contextualLearningAll(ip,'jack')
        ip.contextualLearningAll(ip,'error')
        ip.contextualLearningAll(ip,'MRANK')
        ip.contextualLearningAll(ip,'nonews') % -> get final
        % inhiber
        ip.useInhibition= 1;
        ip.contextualLearningAll(ip,'inhibition')                
        %ip.contextualLearningAll(ip,'inhiber')        
        ip.contextualLearningAll(ip,'MRANK')        
        ip.contextualLearningAll(ip,'nonews') % -> get final
    end
%%
    function self= getList(self,index)
    %self.comp= get_repository('index-comp',index);   
    self.comp= get_index_comp(index, 'recent_date_constraint', false);   
    end

    function buffer1(ID,dates)
        vl= VLoader();
        vl.main(ID,dates);        
    end

    function contextualLearning1(ID,substep)
        % applies eModelPool to one stock
        vl= eModelPool();
        vl.main(ID,substep);        
    end

    function bufferAll(self,dates,TD,Lfreq,folder,NminObs)
        for idcourant= (self.comp(:)')
            %if idcourant>8 % reprise
                if ischar(TD)
                    assert(strcmpi(TD,'main'));
                    a=get_repository('tdinfo', idcourant);
                    TD_ = a(1).trading_destination_id;                    
                else
                    TD_= TD;
                end            
                vl= VLoader();
                vl.main(idcourant,dates,TD_,Lfreq,folder,NminObs);   
          % end
        end
    end

    function out_data= bufferNews(self,dateTronque,save_folder,does_hard_filter, does_hard_rdays)
        out_data=[];
        if nargin< 4, does_hard_filter= 1;end
        if nargin<5;does_hard_rdays= 1;end
        if nargin< 3; save_folder=1;end
        if nargin==1;dateTronque= [];callback=[];
        else;callback= @(n) tronquerNews(n,dateTronque);
        end
        
        % market close ?
        for id= (self.comp(:)')
            le=loadEvent();
            %news= le.getEvent(id);            
%             L= [3 10  11 12 13 14 15 20 21 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119];
% trying news parsed from econodays
%             L= [3 10  11 12 13 14 15 20 21 89034 88896 89078 89022 88982 89071 88946 89011 88913 88981 88952 89014 88944 89043 89077 88968 88980 88967 88888 88935 89067 89068 88997 89016 89032 88928 89004 88891 88922 88991 89062 88911 88919 88964 89039 88897 89066 89030 89027 89057 89020 89003 89028 88937 88892 89017 88894 88940 88970 89060 88958 89056 88912 88893 89047 89053 88993 88948 88999 88903 88973 89070 89026 89064 88929 89084 89010 89080 88915 88923 88957 88927 88925 88985 89050 89013 89042 88987 88977 89023 88984 88900 89008 88901 88995 88910 88924 89046 89081 89005 88971 89036 89079 89002 88972 88960 88932 89063 88949 89051 88953 88921 88941 89074 88947 89001 88979 89044 89000 89061 89012 88906 89085 88976 88988 88959 88939 88918 88914 88990 88890 89049 88998 89072 88955 88943 88938 89021 88926 89065 88934 89018 88994];
% second attempt
            if does_hard_filter
%                 L = setdiff([3 10  11 12 13 14 15 20 21 89034,88896,89078,89022,88982,89071,89011,88981,88952,89014,88944,89043,89077,88980,88888,88935,89067,89068,88997,89016,88928,89004,88891,88922,88991,89062,88911,88919,88964,89039,88897,89066,89057,89020,89003,89028,88894,88940,88970,89060,88958,89056,88912,88893,89047,89053,88993,88948,88999,88903,88973,89070,89026,89064,88915,88985,89050,89013,88977,89023,88984,88900,89008,88910,88924,89046,89005,88971,89036,89002,88972,88960,88953,88921,89074,88947,89001,89000,88906,89085,88988,88959,88939,88918,88890,88998,89072,88955,88943,88938,89021,88926,89065,88934,89018], ...
%                     [88915 89014 89028 89057 88918]);

%%%% List of event used originally by Nath to make the first learning run. 
%                 news_macro_orig_ev_type = [88890,88893,88894,88903,88906,88912,88918,88926,88934,88935,88938,88939,88940,88943,88948,88955,88958,88959,88970,88973,88988,88993,88998,88999,89018,89021,89026,89047,89053,89056,89060,89064,89065,89070,89072,89085];
%                 L = [3,5,6,10:15,20:27, news_macro_orig_ev_type];
%%%%%
%%%%% Limitation to the four events : new_york_shift, derivative_expiry, monthly_derivative_expiry, US-O-Employment Situation
                L = [3,10,15,89060]; 
%%%%
                [news,e]=le.getEvent(id,@(x) le.filter(x,L));
            else
                [news,e]=le.getEvent(id,@(x) le.filter(x,[],[],@(x) x>0));
            end
            news= eliminerCorrelParfaite(news);
            assert(issorted(news.date)); % ordre croissant seulement
            
            
            %% PATCH 1: rajoute contextes-jour semaine
%             week_day= weekday(news.date);
%             for k=2:6
%                 news.value(:,end+1)= (week_day==k);
%                 news.col_id(end+1) = 10000 +k;
%                 news.colnames{end+1} = ['day' num2str(k)];
%             end            
            %%
            %% PATCH 2: rajoute ls 1ers du mois: - de dates (-ddl)
%             [Y, M, D, H, MN, S] = datevec(news.date);
%             week_day= D;
%             for k= 1:length(unique(week_day))
%                 news.value(:,end+1)= (week_day==k);
%                 news.col_id(end+1) = 10000 +k;
%                 news.colnames{end+1} = ['day' num2str(k)];
%             end            
            %% patch 3 : pipoEvent
            %%news= le.pipoEvent(id,dateTronque);
            
            %%
            
            
            
            if ~isempty(callback)
                news= callback(news);
            end
            if does_hard_rdays
               % TODO: liste en dur, puis enlever de news les jours, de sorte que ca synchronise les volumes plus tard 
               L_remove_dates=[]; % half day trading...
               x_= news.value(:,ismember(news.col_id,L_remove_dates));
               x_= find(any(x_,2));
               news.value(x_,:)=[];
               news.date(x_)=[];               
            end
            
            
            if ischar(save_folder) %save_folder
                if ischar(save_folder);else;save_folder=[];end
                tbx= newsProjectTbx();
                IO= tbx.FILE(tbx.FILEconfig.folder({'X','X',dateTronque,save_folder},'News'));                           
                IO.save(news,'type','news','stockID',num2str(id));
            else
                out_data=news;
            end
        end
        
    end

    function contextualLearningAll(self,substep,folder)
        if nargin< 3; folder= [];end
        % applies eModelPool to all stocks
        warning off
        for idcourant= (self.comp(:)')
            %if idcourant>= 71 % reprise
                disp(idcourant)
                %emp= eModelPool();
                %emp.main(idcourant,'intraday',substep,self.useInhibition);
                emp= eModelPool(idcourant,self.destinationID,'intraday',substep,self.useInhibition,self.dates,self.path_modifier,self.learn_options);
            %end
        end
        warning on
    end
    % Il faudra apeler compare model a la main, ou cr?er une fun pr?vue pour
    % multicomparaison.

%%
    function R= gatherCompare(self)
        R={};       
        tbx= newsProjectTbx();
        for idcourant= (self.comp(:)')
            IO= tbx.FILE(tbx.FILEconfig.folder(idcourant,'stock'));
            data= IO.loadSingle('type','MRANK');
            R{end+1}= data{1};
        end
        
    end
end

function news= tronquerNews(news,d)
    % assert is sorted !
    i1= news.date>= d(1); i1= find(i1);

    i2= news.date<= d(2); i2= find(i2);
    idx= intersect(i1,i2);
    news.date= news.date(idx);
    news.value= news.value(idx,:);
end

function data= eliminerCorrelParfaite(data)
assert(issorted(data.col_id));
[T,P]= size(data.value);
toremove=[];
c= corr(data.value);
for p=1:P; for q=p+1:P;
    if c(p,q)==1 % seuil de robustesse ?
        toremove(end+1)= p;
        toremove(end+1)= q;        
    end
end;end

toremove= unique(toremove);
% normalement ca laisse la 1ere occurence
toremove= toremove(2:end);
data.removed= data.colnames(toremove);
data.value(:,toremove)= [];
data.colnames(toremove)= [];
data.col_id(toremove)= [];

end



function post_analysis_script()
R= cellfun(@(x)x,R)
sv= [R.savedVariable]
K= length(sv);
for k=1:K;sv(k).value= sv(k).value';end
ordre = sv(1).colnames;
f= @(x) st_data('keep',x,ordre);
sv= arrayfun(f,sv);


r= arrayfun(@(x)x.value,sv,'UniformOutput',0) % rank !
ar=uo_(r) % Stock * rank
bm=ar(:,1)
[n,xout]=hist(bm,50)

figure,
plot(n,xout,'k.')
set(gca,'Ytick',1:K)
set(gca,'Yticklabel',ordre)

NNy= arrayfun(@(x)x.noteNNmoy,sv)
NNd= arrayfun(@(x)x.noteNNmed,sv)
proc= arrayfun(@(x)x.note(1),sv)
figure,hold on
plot(NNy,'k.')
plot(NNd,'b.')
plot(proc,'r+')


end

function profilePlotAll()
ip= indexProcess('CAC40');
dts= [];
format= 'dd-mmm-yyyy';
dts(1)= datenum('03-Jan-2007',format);
dts(2)= datenum('29-Jun-2010',format);

N= length(ip.comp);
cmp= hsv(N);
figure,hold on;
n=0;
for ID = ip.comp(:)'
    n=n+1;
   c= compareModels();    
   c= c.loadBuffer(c,ID,'dim',36);    
   %c=c.getNotes(c);
   [p,x]=c.profilePlotSimple(c,'cash 1mn',0);
   %plot(p,x.*p,'marker','.','linestyle','none','color',cmp(n,:) );
   %plot(p,x,'marker','.','linestyle','none','color',cmp(n,:) );
   plot(p,x.*p +(1-p),'marker','.','linestyle','none','color',cmp(n,:) );
   disp(n);
end

end




