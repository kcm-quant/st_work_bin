function indexProcess1by1(ip0,steps)
% Learning procedure for an index


STEPS_ORDER    = {'ranks', 'jack0', 'jack', 'error', 'MRANK','inhibition','IMRANK','nonews'};
USE_INHIBITION = [      0,       0,      0,       0,       0,           1,       1,       1];
COMP_STEP2LAUNCH={'ranks', 'jack0', 'jack', 'error', 'MRANK','inhibition', 'MRANK','nonews'};

N=length(ip0.comp);
if N > 1 && ~isempty(intersect(STEPS_ORDER, steps))
    error('indexProcess1by1:check_args', ...
        ['BAD_USE: Donot launch a learning phase with several security ' ...
        '(nothing sure about td management + better to ditribute among security)']);
end
for n= 1 : N
    ip = ip0;
    ip.comp = ip0.comp(n);
    for s_ind = 1 : length(STEPS_ORDER)
        if ismember(STEPS_ORDER{s_ind},steps)
            st_log('indexProcess1by1:begin:security_id:%d:step:%s\n', ...
                ip0.comp(n), STEPS_ORDER{s_ind});
            t0 = tic;
            ip.useInhibition = USE_INHIBITION(s_ind);
            ip.contextualLearningAll(ip, COMP_STEP2LAUNCH{s_ind});
            st_log('indexProcess1by1:end:security_id:%d:step:%s:exectime:%d Execution took %d seconds\n', ...
                ip0.comp(n), STEPS_ORDER{s_ind}, round(toc(t0)), round(toc(t0)));
        end
    end
end

end


function example()

V= vwap_order_weights();
V.clear()

ip= indexProcess('CAC40');
%ip= indexProcess('DAX');
%ip= indexProcess('DJ');
ip= indexProcess('FTSE');
dts= [];
format= 'dd-mmm-yyyy';
%dts(1)= datenum('03-Jan-2008',format);
dts(1)= datenum('03-Jan-2006',format);
dts(2)= datenum('15-May-2011',format);
ip.comp=setdiff(ip.comp,[276,2]);
ip.comp=ip.comp(ip.comp<30);
ip.comp= ip.comp(ip.comp>10000);
%dts(2)= datenum('20-Jan-2007',format);

% le 245 n'est pas bufferis?

%ip.comp=ip.comp(ip.comp>=14334);
ip.comp=ip.comp(ip.comp>=6155);
ip.comp= ip.comp(ip.comp<107509)
ip.comp= 6427;
ip.comp=2
ip.comp= ip.comp(ip.comp ~=107509)
ip.destinationID= {};
% BUFFERS
st_log('$off$')
ip.bufferAll(ip,dts,4,15); % plante sur 16827

df= [] ;
df(1)= datenum('03-Jan-2007',format);
df(2)= datenum('29-Jun-2010',format);
ip.bufferNews(ip,df)

ip.comp= ip.comp(ip.comp>=26)
ip.comp= ip.comp(ip.comp<232)
indexProcess1by1(ip);
indexProcess1by1(ip,1);
end