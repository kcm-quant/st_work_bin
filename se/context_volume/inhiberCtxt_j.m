function J= inhiberCtxt_j(J, J0,isM0, ctxt)    
    assert(all(all(J.news.value==J0.news.value)))        
    if isM0== 1
        J.opt.set('type','error','inhibition','1','noter','None');       
        return
    end                  
    idx_= ismember(J.BN, ctxt);
    assert(~isfield(J,'inhibitedCtxt'));
    J.inhibitedCtxt= ctxt;
    J.BN(idx_)= 0;
    J.pred(idx_,:)= J0.pred(idx_,:);            
end