function self = latex_report(varargin)
% UNTITLED4 - object wrapper for function latex()
%
%
%
% Examples:
%
% 
% 
%
% See also:
%    
%
%   author   : 'nmayo@cheuvreux.com'
%   reviewer : ''
%   date     :  '23/11/2011'
%   
%   last_checkin_info : $Header: latex_report.m: Revision: 7: Author: namay: Date: 05/02/2012 06:27:07 PM$


self=[];

if ~isempty(varargin)
    self.pth= varargin{1};
    varargin= varargin(2:end);
    ini_varargin= varargin;
end
self.begin=@begin;
self.close= @close;
self.section= @section;
self.subsection=@subsection;
self.graph= @graph;
self.array=@array;
self.text= @text;
self.plain= @plain;
self.hlink= @hlink;
self.quick= @quick;
self.anchor= @anchor;
self.list_figs=@list_figs;


    function REPORT=quick(path)
        [pathstr, name, ext]= fileparts(path);
        fn= [name ext];
        pars= {'html_root_path','Y:\www\Statistical Engine\dev\Estimators\Contextualized Volume Profile\Learning phase report for learning validation',...
               'html_path_from_root', './','css_from_root', './../../../../../css/styles.css' };
        REPORT= latex_report(fn,'html_root_path',pathstr,...
            'html_path_from_root', './','css_from_root', './../../../../../css/styles.css'    );        
    end

    function section(name)        
        latex('section',self.pth, name,varargin{:});
    end

    function subsection(name)
        latex('subsection', self.pth, name,varargin{:});
    end
    function text(string,args)
        if nargin< 2; args={};end
        string= char(string);
        [P,Q]= size(string);
        if P>1
            for p=1:P; text([string(p,:) '<BR>'],args);end
        else            
            latex('text',args{:}, self.pth, string,varargin{:});
        end
    end
    function plain(string,args)
        if nargin< 2; args={};end
        string= char(string);
        [P,Q]= size(string);
        if P>1
            for p=1:P; plain([string(p,:) '<BR>'],args);end
        else      
            latex('plain',args{:}, self.pth, string,varargin{:});
        end
    end

    function html_= hlink(url,text,dont_write)
       if nargin< 3; dont_write=0;end
       %html_= '<a href="http://www.google.com">hyperlink to google</a>';
       html_= ['<a href="' url '"> ' text ' </a>' ];
       if dont_write==0
          latex('plain', self.pth,html_,varargin{:}); 
          latex('plain', self.pth,'<br />',varargin{:}); 
       end
    end

    function anchor(name)
        plain(sprintf('<a name="%s"></a>',name));        
    end

    function graph(fig_handle,scale_,close_,fullscreen_)
        if nargin< 2; scale_= 0.6;end
        if nargin< 3; close_= 0;end
        if nargin< 4; fullscreen_= 0;end
        if ischar(fig_handle)
            uiopen(fig_handle,true);                     
            fig_handle= gcf;
        end
        latex('graphic', self.pth, 'graph_handle', fig_handle, 'scalebox', ...         
        scale_, 'close', close_, ...
         'fullscreen', fullscreen_,'width', true,varargin{:});
     % close(fig_handle); close est surcharg�e ici !!!!
    end

    function begin(title_,author_)
        latex('header',self.pth, 'title', title_, 'author', author_,varargin{:});
    end

    function x= close()
        x= latex('end',self.pth,varargin{:});
    end

    
    function list_figs(foldr,scale_,close_,fullscreen_)
        if nargin< 2; scale_= 0.6;end
        if nargin< 3; close_= 1;end
        if nargin< 4; fullscreen_= 0;end
        ll= dir(foldr);
        isfig= cellfun(@(x)Strings.endswith(x,'.fig'), {ll.name});
        ll= ll(isfig);
        for k=1:length(ll)
            %subsection(ll.name);
            graph(fullfile(foldr,ll(k).name),scale_,close_,fullscreen_);
        end
    end
    
    function array(data,varargin)
        if isfield(data,'format')
            if data.format(1) == '*'
                % reconvertit les strings en double pour appliquer le format num�rique !
                ix= cellfun(@Strings.isfloat,data.value);
                %data.format(ix)= str2num(data.value(ix));
                data.value(ix)= cellfun(@str2num,data.value(ix),'uni',0);
                data.format(1)=[];                
            end            
        end
        args= {'array',self.pth,data.value};
        if isfield(data,'col_header')
            args{end+1}= 'col_header'; args{end+1}= data.col_header;
        end
        if isfield(data,'linesHeader')
            args{end+1}= 'linesHeader'; args{end+1}= data.linesHeader;
        end
        if isfield(data,'format')
            args{end+1}= 'format'; args{end+1}= data.format;
        end
        latex(args{:},ini_varargin{:},varargin{:});
        %latex('array', self.pth, mdl(2,:),'col_header',mdl(1,:), 'format', '%2.5f','linesHeader',);
    end


end


%%
function test()
 figure;plot(randn(100,1));


fn= ['report_test.html'];

pars= {'html_root_path','Y:\www\Statistical Engine\dev\Estimators\Contextualized Volume Profile\Learning phase report for learning validation',...
       'html_path_from_root', './','css_from_root', './../../../../../css/styles.css' };

REPORT= latex_report(fn,'html_root_path','Y:\www\Statistical Engine\dev\Estimators\Contextualized Volume Profile\Learning phase report for learning validation',...
       'html_path_from_root', './','css_from_root', './../../../../../css/styles.css'    );

REPORT.begin('demo','Nathana�l Mayo');
%REPORT.section('graphe');
%REPORT.text('some text');
REPORT.graph(gcf)
% data=[]; data.col_header={'col'}; data.value= 1;
% REPORT.array(data);
% REPORT.graph(gcf)
% REPORT.array(data);
REPORT.close()
end

function unwrapped_test()

latex('header', 'report_test.html','title', 'test', 'author', 'me',pars{:});
latex('section', 'report_test.html','section',pars{:});
latex('end', 'report_test.html',pars{:})

latex('header', 'report_test.html','title', 'test', 'author', 'me');
latex('text', 'report_test.html','<a href="http://www.google.com">hyperlink to google</a>');
latex('end', 'report_test.html')

end


function test_links()


fn= ['report_test.html'];

pars= {'html_root_path','Y:\www\Statistical Engine\dev\Estimators\Contextualized Volume Profile\Learning phase report for learning validation',...
       'html_path_from_root', './','css_from_root', './../../../../../css/styles.css' };

REPORT= latex_report(fn,pars{:});%'html_root_path','Y:\www\Statistical Engine\dev\Estimators\Contextualized Volume Profile\Learning phase report for learning validation',...
       %'html_path_from_root', './','css_from_root', './../../../../../css/styles.css'    );
REPORT.begin('demo','Nathana�l Mayo');
REPORT.hlink('http://www.google.com','google link')
REPORT.hlink(['file:///' pars{2} '//top.html'],'back');
x=REPORT.close();


pars= {'html_root_path','Y:\www\Statistical Engine\dev\Estimators\Contextualized Volume Profile\Learning phase report for learning validation',...
       'html_path_from_root', './','css_from_root', './../../../../../css/styles.css' };

SUB= latex_report('top.html',pars{:});%'html_root_path','Y:\www\Statistical Engine\dev\Estimators\Contextualized Volume Profile\Learning phase report for learning validation',...
       %'html_path_from_root', './','css_from_root', './../../../../../css/styles.css'    );
SUB.begin('hyperlink_demo','Author');
SUB.hlink('http://www.google.com','google link')
xx=['file:///' x];
SUB.hlink(xx,'sub'); %strrep(x,'\','\\'),'sub')
%SUB.hlink('http://www.google.com','sub')
SUB.close()


end

function test_link_in_table()

fn= ['report_test.html'];
pars= {'html_root_path','C:\test',...
       'html_path_from_root', './','css_from_root', './../../../../../css/styles.css' };

data=[];
data.col_header ={ 'col1','col2'};
data.value= {1,2;3,4};
   
REPORT= latex_report(fn,pars{:});%'html_root_path','Y:\www\Statistical Engine\dev\Estimators\Contextualized Volume Profile\Learning phase report for learning validation',...
       %'html_path_from_root', './','css_from_root', './../../../../../css/styles.css'    );
REPORT.begin('demo','Nathana�l Mayo');
REPORT.array(data,'plain',true,'link', 1,'link_matrix',{'oui','non';[],[]})
x=REPORT.close();


end

function DEMO_array
t=latex_report()
REPORT= t.quick('H:\demo_report\demo_array.html');
REPORT.begin('demo','N.Mayo');
ad=[];
ad.col_header= {'1','2','3'};
ad.value= 10000*rand(10,3);
%ad.value= num2cell(ad.value);
ad.format= '%0.4g';
REPORT.array(ad)
REPORT.close()

t=latex_report()
REPORT= t.quick('H:\demo_report\demo_array.html');
REPORT.begin('demo','N.Mayo');
ad=[];
%ad.col_header= {'1','2','3'};
ad.value= all_btd; % le format est try et n'affecte pas les strings !
%ad.value= num2cell(ad.value);
ad.format= '%0.0f';
REPORT.array(ad)
REPORT.close()


t=latex_report()
REPORT= t.quick('H:\demo_report\demo_array.html');
REPORT.begin('demo','N.Mayo');
ad=[];
%ad.col_header= {'1','2','3'};
ad.value= {'oui',123456,123456;'oui',123456,123456}; % le format est try et n'affecte pas les strings !
ad.value(:,1:2)= cellfun(@num2str,ad.value(:,1:2),'uni',0);
%ad.value= num2cell(ad.value);
ad.format= '%0.1e';
REPORT.array(ad)
REPORT.close()
% string+* -> convertit quand m�me
% int +* -> convertit quand m�me
% string sans �toile non convertis !
end

function DEMO_link

t=latex_report()
REPORT= t.quick('H:\demo_report\demo_array.html');
REPORT.begin('demo','N.Mayo');
REPORT.anchor('sec1A')
REPORT.section('sec1')
%REPORT.plain('<a name="sec1A"></a>')

for k=1:100;REPORT.plain('<br>');end
REPORT.plain('<hr>')
REPORT.section('sec2')
REPORT.hlink('#sec1A','here')
%REPORT.plain('<a href="#sec1A">My Link</a>')
for k=1:100;REPORT.plain('<br>');end
REPORT.plain('<hr>')
REPORT.section('sec3')
REPORT.hlink('#sec1A','here')
%REPORT.plain('<a href="#sec1A">My Link</a>')
for k=1:100;REPORT.plain('<br>');end
REPORT.plain('<hr>')
REPORT.close()

%<a href="#myanchor">My Link</a> (replace My Link with your link text) for your link, then <a name="myanchor"></a> 


end


function DEMO_graphe_file

% f= figure;plot(randn(100,1));
% saveas(f,'H:\demo_report\test.fig')

t=latex_report();
%REPORT= t.quick('C:\test\latexDemo\test.html');
REPORT= t.quick('W:/Global_Research/Quant_research/team/nmayo/ClientOrders/Turnovers.html');

REPORT.begin('Client Orders','Nathana�l Mayo');
%REPORT.graph('C:\test\latexDemo\Client_total_turnover+log_{10}(Turnover)_(�)+hist._over_stocks.fig')
% REPORT.list_figs('C:\dev_repository\ClientOrderGraphe\globals_nanok\fig2png')
REPORT.list_figs('C:\dev_repository\ClientOrderGraphe\globals_newg\fig2png')
%REPORT.graph(f)
REPORT.close()

end

