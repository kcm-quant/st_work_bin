function [failed,base] = launch_context_volume_learning(exchgid,multi_dest,beg_date,end_date,varargin)

% EXCHGID = 'SEPA';
% MULTI_DEST = true;
% BEG_DATE = datenum(2010,1,1);
% END_DATE = datenum(2015,5,31);
% [failed,base] = launch_context_volume_learning(EXCHGID,MULTI_DEST,BEG_DATE,END_DATE);

%%% <PATCH 1>
SECURITY_ID_TO_RM = [3,8,11,25,26,31,41,45,46,56,71,73,84,92,95,160,257,258,...
    273,110,121,12168,123394,1274,13280,137,138,141,14334,150,156,157,162,...
    17,174,18,180,187,192,2,208,209,210,214715,216,218135,219,220,226,228,...
    232,232517,238,239753,24,246,247];
%%% </PATCH 1>

td_id = cell2mat(exec_sql('KGR',sprintf('select EXCHANGE from KGR..EXCHANGEREFCOMPL where EXCHGID = ''%s''',exchgid)));
assert(length(td_id)==1,'Number of EXCHANGE found in KGR..EXCHANGEREFCOMPL is different from 1');

if ~isempty(varargin)
    fid = fopen(varargin{1},'w');
else
    fid = 1;
end
assert(fid>0,'Log output configuration error');

if multi_dest
    security_id = cell2mat(exec_sql('QUANT',sprintf(['select distinct a.security_id',...
        ' from KGR..security_market sm, QUANT..last_run lr,QUANT..association a',...
        ' where a.estimator_id = 2',...
        ' and sm.trading_destination_id = %d',...
        ' and sm.ranking = 1',...
        ' and sm.security_id = a.security_id',...
        ' and a.trading_destination_id is null',...
        ' and a.job_id = lr.job_id',...
        ' and lr.last_valid_run_id is not null'],td_id)));
    
    str = sprintf('%d,',security_id);
    str(end) = [];
    security_id = cell2mat(exec_sql('QUANT',sprintf(['select security_id',...
        ' from QUANT..association',...
        ' where security_id in (%s)',...
        ' and estimator_id = 18',...
        ' and trading_destination_id is null'],str)));
    %%% <PATCH 1>
    security_id = setdiff(security_id,SECURITY_ID_TO_RM);
    %%% </PATCH 1>
    for s = 1:length(security_id)
        L = context_volume_learning('security_id',security_id(s),'trading_destination',[],'dates',[beg_date,end_date],'force',true);
        try
            [failed,base] = L.learn();
        catch e
            fprintf(fid,'[%s]FAILED for security_id = %d, td_id = NULL: %s\n',datestr_ms(now),security_id(s),e.message);
            continue
        end
        fprintf(fid,'[%s]SUCCESS for security_id = %d, td_id = NULL\n',datestr_ms(now),security_id(s));
    end
else
    security_id = cell2mat(exec_sql('QUANT',sprintf(['select distinct a.security_id',...
        ' from KGR..security_market sm, QUANT..last_run lr,QUANT..association a',...
        ' where a.estimator_id = 2',...
        ' and sm.trading_destination_id = %d',...
        ' and sm.ranking = 1',...
        ' and sm.security_id = a.security_id',...
        ' and a.trading_destination_id = sm.trading_destination_id',...
        ' and a.job_id = lr.job_id',...
        ' and lr.last_valid_run_id is not null'],td_id)));
    
    str = sprintf('%d,',security_id);
    str(end) = [];
    security_id = cell2mat(exec_sql('QUANT',sprintf(['select security_id',...
        ' from QUANT..association',...
        ' where security_id in (%s)',...
        ' and estimator_id = 18',...
        ' and trading_destination_id = %d'],str,td_id)));
    %%% <PATCH 1>
    security_id = setdiff(security_id,SECURITY_ID_TO_RM);
    %%% </PATCH 1>
    for s = 1:length(security_id)
        L = context_volume_learning('security_id',security_id(s),'trading_destination',td_id,'dates',[beg_date,end_date],'force',true);
        try
            [failed,base] = L.learn();
        catch e
            fprintf(fid,'[%s]FAILED for security_id = %d, td_id = %d: %s\n',datestr_ms(now),security_id(s),td_id,e.message);
            continue
        end
        fprintf(fid,'[%s]SUCCESS for security_id = %d, td_id = %d\n',datestr_ms(now),security_id(s),td_id);
    end
end

if fid > 1
    try
        fclose(fid);
    catch
    end
end


