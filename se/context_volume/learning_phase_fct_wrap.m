function [failed, base, job] = learning_phase_fct_wrap(mode, varargin)
% LEARNING_PHASE_FCT_WRAP - wrapper of learning context to distribute it
%
%
% [failed, base] = learning_phase_fct_wrap('dts', ...
%         'security_id',110,'trading_destination', {}, ...
%         'dates', [datenum(2009,1,1,0,0,0), datenum(2012,8,31,0,0,0)],...
%         'force', 1);
%
% % This is an exemple with lighter arguments
% indexes = {'OBX'}
% for i = 1 : length(indexes)
%     [failed, base] = learning_phase_fct_wrap('dts', ...
%         'security_id',indexes{i},'trading_destination', {}, ...
%         'dates', [datenum(2009,1,1,0,0,0), datenum(2012,8,31,0,0,0)],...%         '
%         'force', 1);
% end
%
%
% indexes = {'OBX', 'CAC40',  'BEL20', 'IBEX35','AEX', 'SWISS LEADER PR INDEX', 'DAX', 'FTSE 100', 'FTSE MIB INDEX','OMX COPENHAGEN 20', 'OMX HELSINKI 25','OMX STOCKHOLM 30', 'S&P500 STOCK INDEX'}
% for i = 1 : length(indexes)
%     [failed, base] = learning_phase_fct_wrap('dts', ...
%         'security_id',indexes{i},'trading_destination', {}, ...
%         'dates', [datenum(2009,1,1,0,0,0), datenum(2012,8,31,0,0,0)],...
%         'force', 1);
% end
%
% indexes = {'DAX', 'FTSE 100', 'FTSE MIB INDEX','OMX COPENHAGEN 20', 'OMX HELSINKI 25','OMX STOCKHOLM 30', 'MDAX'}
% for i = 1 : length(indexes)
%     [failed, base] = learning_phase_fct_wrap('dts', ...
%         'security_id',indexes{i},'trading_destination', {}, ...
%         'dates', [datenum(2009,1,1,0,0,0), datenum(2012,5,4,0,0,0)],...
%         'buffer_dates', [datenum(2009,1,1,0,0,0), datenum(2012,5,4,0,0,0)], ...
%         'old-data', 0, 'force', 1);
% end
% for i = 1 : length(indexes)
%     [failed, base] = learning_phase_fct_wrap('dts', ...
%         'security_id',indexes{i},'trading_destination', {'MAIN'}, ...
%         'dates', [datenum(2009,1,1,0,0,0), datenum(2012,5,4,0,0,0)],...
%         'buffer_dates', [datenum(2009,1,1,0,0,0), datenum(2012,5,4,0,0,0)], ...
%         'old-data', 0, 'force', 1);
% end
%
% % Mettre en base
% % effacer avant ce qui n'a pas ?t? appris lors du dernier apprentissage
% % allr={};
% % dts= [datenum(2009,1,1,0,0,0), datenum(2012,8,31,0,0,0)];
% % sec_and_tds = cell2mat(exec_sql('QUANT', 'select security_id, isnull(trading_destination_id, -1) from quant..context_ranking UNION select security_id, isnull(trading_destination_id, -1) from quant..learningmodel'));
% % sec_and_tds = unique(sec_and_tds, 'rows');
% % DB= contexte_volume_DBaccess();
% % for i = 1 : size(sec_and_tds , 1)
% %     sid = sec_and_tds(i, 1);
% %     if sec_and_tds(i, 2) == -1
% %         tdid = {};
% %     else
% %         tdid = sec_and_tds(i, 2);
% %     end
% %     z= CV_identify_phase({sid,tdid,dts,''});
% %     if (~strcmp(z.step,'base')) | z.substep ~= 100
% %         r= DB.delete(sid,tdid); %r{:};
% %         allr{end+1}= r;
% %         %x= DB.exec(r);
% %     end
% % end % essayer de le faire proprement ne paye pas
%  
%  % Tout effacer :
%  % exec_sql('QUANT', 'delete from quant..context_ranking')
%  % exec_sql('QUANT', 'delete from quant..learningmodel')
%
% indexes = {'OBX', 'CAC40',  'BEL20', 'IBEX35','AEX', 'SWISS LEADER PR INDEX', 'DAX', 'FTSE 100', 'FTSE MIB INDEX','OMX COPENHAGEN 20', 'OMX HELSINKI 25','OMX STOCKHOLM 30', 'S&P500 STOCK INDEX'}
% failed = {};
% for i = 1 : length(indexes)
%       sid = get_index_comp(indexes{i})
%       st_log('index : %d/%d\n', i, length(indexes));
%     for j = 1 : length(sid)
%           try
%               [failed{end+1}, base] = learning_phase_fct_wrap('std', ...
%               'security_id',sid(j),'trading_destination', {}, ...
%                 'dates', [datenum(2009,1,1,0,0,0), datenum(2012,8,31,0,0,0)],...
%                'force', 1, 'steps', 'base');
%           catch
%               failed{end+1} = sid(j);
%           end
%           if isempty(failed{end})
%               base.write();
%           end
%           st_log('\t stock : %d/%d\n', j, length(sid));
%     end
% end
%



%
% f = {};
% e = {};
% g= @(indice,td,dates) learning_phase_fct_wrap('std', ...
%         'security_id',indice,'trading_destination', td, ...
%         'dates', dates,...         '
%         'old-data', 0, 'force', 1, 'steps', 'base');
% e={};f={};for i = 1 : length(indexes)
%     [f{end+1}, base] = g(indexes{i},{},[datenum(2009,1,1,0,0,0),datenum(2012,5,4,0,0,0)]);
%     e{end+1}= base.write();
% end
%
%learning_phase_fct_wrap('std', ...
%         'security_id',indexes{i},'trading_destination', {}, ...
%         'dates', [datenum(2009,1,1,0,0,0), datenum(2012,5,4,0,0,0)],...
%         'buffer_dates', [datenum(2009,1,1,0,0,0), datenum(2012,5,4,0,0,0)], ...
%         'old-data', 0, 'force', 1, 'steps', 'base');
%     e{end+1} = base.write();
% end
% for i = 1 : length(indexes)
%     [f{end+1}, base] = learning_phase_fct_wrap('std', ...
%         'security_id',indexes{i},'trading_destination', {'MAIN'}, ...
%         'dates', [datenum(2009,1,1,0,0,0), datenum(2012,5,4,0,0,0)],...
%         'buffer_dates', [datenum(2009,1,1,0,0,0), datenum(2012,5,4,0,0,0)], ...
%         'old-data', 0, 'force', 1, 'steps', 'base');
%     e{end+1} = base.write();
% end
%
%
% indexes = {'DAX', 'FTSE 100', 'FTSE MIB INDEX','OMX COPENHAGEN 20', 'OMX HELSINKI 25','OMX STOCKHOLM 30', 'MDAX'}
% for i = 1 : length(indexes)
%       sid = get_index_comp(indexes{i})
%         for j = 1 : length(sid)
%             [failed, base] = learning_phase_fct_wrap('std', ...
%                 'security_id',sid(j),'trading_destination', {}, ...
%                 'dates', [datenum(2009,1,1,0,0,0), datenum(2012,5,4,0,0,0)],...
%                 'buffer_dates', [datenum(2009,1,1,0,0,0), datenum(2012,5,4,0,0,0)], ...
%                 'old-data', 0, 'force', 1, 'steps', 'base');
%             base.write();
%         end
% end


% indexes = {'CAC40',  'BEL20', 'IBEX35','AEX', 'SWISS LEADER PR INDEX', 'OBX'}
% for i = 1 : length(indexes)
%     [failed, base] = learning_phase_fct_wrap('dts', ...
%         'security_id',indexes{i},'trading_destination', {}, ...
%         'dates', [datenum(2009,1,1,0,0,0), datenum(2012,4,20,0,0,0)],...
%         'buffer_dates', [datenum(2009,1,1,0,0,0), datenum(2012,4,20,0,0,0)], ...
%         'old-data', 0, 'force', 1);
% end

% indexes = {'EURO STOXX 50', 'DAX', 'MDAX', 'CAC40', 'FTSE MIB INDEX','DJ STOXX LARGE 200'}; 
% indexes = {'ATX', 'BEL20', 'IBEX35','OMX COPENHAGEN 20','AEX','FTSE / JSE'};
% indexes = {'FTSE/ATHEX20', 'SWISS LEADER PR INDEX','OMX HELSINKI 25','OMX STOCKHOLM 30'};
%
% for i = 1 : length(indexes)
%     [failed, base] = learning_phase_fct_wrap('dts', ...
%         'security_id',indexes{i},'trading_destination', {}, ...
%         'dates', [datenum(2009,1,1,0,0,0), datenum(2012,3,23,0,0,0)],...
%         'buffer_dates', [datenum(2009,1,1,0,0,0), datenum(2012,3,23,0,0,0)], ...
%         'old-data', 0, 'force', 1); % , 'folder', '20120326'
% end



% [failed, base] = learning_phase_fct_wrap('std', 'security_id',2,'trading_destination', {}, ...
%     'dates', [datenum(2007,1,1,0,0,0), datenum(2010,06,30,0,0,0)],...
%     'buffer_dates', [datenum(2007,1,1,0,0,0), datenum(2010,06,30,0,0,0)], 'old-data', 0);
% [failed, base] = learning_phase_fct_wrap('std', 'security_id',2,'trading_destination', {}, ...
%     'dates', [datenum(2007,1,1,0,0,0), datenum(2010,06,30,0,0,0)],...
%     'buffer_dates', [datenum(2007,1,1,0,0,0), datenum(2010,06,30,0,0,0)], 'old-data', 0);
% [failed, base] = learning_phase_fct_wrap('std', 'security_id',238,'trading_destination', {'main'}, ...
%     'dates', [datenum(2007,1,1,0,0,0), datenum(2010,06,30,0,0,0)],...
%     'buffer_dates', [datenum(2007,1,1,0,0,0), datenum(2010,06,30,0,0,0)],...
%     'old-data', 0, 'steps', {'news','ranks','jack0','jack','error','MRANK',...
%     'inhibition','IMRANK','nonews','base'})
% sec_ids = get_index_comp('CAC40');
% 
%
%
% jm = findResource('scheduler','type','local');
% job = jm.createJob();
% 
% for i = 1 : length(sec_ids)
%    job.createTask('learning_phase_fct_wrap', 2, {'dts', 'security_id',sec_ids(i),'trading_destination', {}, ...
%     'dates', [datenum(2007,1,1,0,0,0), datenum(2010,6,30,0,0,0)],...
%     'buffer_dates', [datenum(2007,1,1,0,0,0), datenum(2010,06,30,0,0,0)], 'old-data', 0});
%    job.createTask('learning_phase_fct_wrap', 2, {'dts','security_id',sec_ids(i),'trading_destination', {'main'}, ...
%     'dates', [datenum(2007,1,1,0,0,0), datenum(2010,6,30,0,0,0)],...
%     'buffer_dates', [datenum(2007,1,1,0,0,0), datenum(2010,06,30,0,0,0)], 'old-data', 0});
% end
% job.submit();
%
% 
% dir2monitor = '/quant/context_l_repository';
% d = dir(dir2monitor);
% d([d.isdir]) = [];
% for i = 1 : length(d)
%     [a, b] = system(sprintf('tail -n 1 %s', fullfile(dir2monitor, d(i).name)));
%      if isempty(strfind(b, '/home/team13/robur/work/st_work/bin/se/context_volume/context_volume_learning.m:context_volume_learning/learn:165'));
%       st_log('%s : %s\n', d(i).name, b);
%     end
% end
%
% L = context_volume_learning('security_id',sec_ids,'trading_destination', {'main'}, ...
%     'dates', [datenum(2007,1,1,0,0,0), datenum(2010,6,30,0,0,0)],...
%      'buffer_dates', [datenum(2007,1,1,0,0,0), datenum(2010,06,30,0,0,0)], 'old-data', 0,'steps',{'base'});
% [failed, base] = L.learn();
% % base.write()
%
% sec_ids = get_index_comp('SBF120\CAC40');
% jm = findResource('scheduler','type','local');
% job = jm.createJob();
% 
% for i = 1 : 45
%    job.createTask('learning_phase_fct_wrap', 2, {'dts', 'security_id',sec_ids(i),'trading_destination', {}, ...
%     'dates', [datenum(2007,1,1,0,0,0), datenum(2010,6,30,0,0,0)],...
%     'buffer_dates', [datenum(2007,1,1,0,0,0), datenum(2010,06,30,0,0,0)], 'old-data', 0, 'force', 1});
%    job.createTask('learning_phase_fct_wrap', 2, {'dts','security_id',sec_ids(i),'trading_destination', {'main'}, ...
%     'dates', [datenum(2007,1,1,0,0,0), datenum(2010,6,30,0,0,0)],...
%     'buffer_dates', [datenum(2007,1,1,0,0,0), datenum(2010,06,30,0,0,0)], 'old-data', 0, 'force', 1});
% end
% job.submit();
%
%
% sec_ids = get_index_comp('SBF120\CAC40');
% jm = findResource('scheduler','type','local');
% job = jm.createJob();
% 
% for i = 46 : length(sec_ids)
%    job.createTask('learning_phase_fct_wrap', 2, {'dts', 'security_id',sec_ids(i),'trading_destination', {}, ...
%     'dates', [datenum(2007,1,1,0,0,0), datenum(2010,6,30,0,0,0)],...
%     'buffer_dates', [datenum(2007,1,1,0,0,0), datenum(2010,06,30,0,0,0)], 'old-data', 0, 'force', 1});
%    job.createTask('learning_phase_fct_wrap', 2, {'dts','security_id',sec_ids(i),'trading_destination', {'main'}, ...
%     'dates', [datenum(2007,1,1,0,0,0), datenum(2010,6,30,0,0,0)],...
%     'buffer_dates', [datenum(2007,1,1,0,0,0), datenum(2010,06,30,0,0,0)], 'old-data', 0, 'force', 1});
% end
% job.submit();
%
%
% ip= indexProcess('CAC40');
% ip.comp=2;
% ip.TD= {}; {'main'};
% ip.dates= [733043,734319];
% 
% % en une etape
% LR = learning_report('test_scale')
% LR.full_report(ip)

global st_version

switch mode
    case 'dts'
        opt = options({'old-data', 0, 'force', 1}, varargin);
        sec_id = opt.get('security_id');
        
        if isnumeric(sec_id) && length(sec_id) == 1
            st_log('$out$', [fullfile(st_version.my_env.st_repository, 'news_project', hash(convs('safe_str',varargin), 'MD5')) '.txt']);

%             try
                L= context_volume_learning(varargin{:});
                [failed,base]= L.learn();
%             catch ME
%                 st_log(text_to_printable_text([ 'Error caught from learning_phase_fct_wrap : stack=<' se_stack_error_message(ME) '>']));
%                 rethrow(ME);
%             end

            st_log('$out$', '');
        else
            if ~isnumeric(sec_id)
                sec_id = get_index_comp(sec_id, 'recent_date_constraint', false);
            end
            jm = findResource('scheduler','type','local')
            job = jm.createJob();
            for i = 1 : length(sec_id)
                opt.set('security_id', sec_id(i));
                lst = opt.get();
                job.createTask('learning_phase_fct_wrap', 2, {'dts', lst{:}});
            end
            job.submit();
            failed = [];
            base = [];
        end
    case 'std'
        L= context_volume_learning(varargin{:});
        [failed,base]= L.learn();
    otherwise
        error();
end
end