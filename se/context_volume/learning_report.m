function self = learning_report(name,scf__) %(ip, glob, spec)
% UNTITLED3 - Automatic reporting on statistical learning
%
%
%
% Examples:
%
% 
% 
%
% See also:
%    
%
%   author   : 'nmayo@cheuvreux.com'
%   reviewer : ''
%   date     :  '23/11/2011'
%   
%   last_checkin_info : $Header: learning_report.m: Revision: 29: Author: namay: Date: 09/20/2012 04:07:02 PM$

if nargin==0; name= '';end
if nargin<2; scf__= [1 1 1];end
global st_version

% 
% latex_= @(mode,name,varargin)...
%         latex(mode,name,varargin{:},...
%         'html_root_path','Y:\www\Statistical Engine\dev\Estimators\Contextualized Volume Profile\Learning phase report for learning validation',...
%        'html_path_from_root', './','css_from_root', './../../../../../css/styles.css'       );
%         
% latex_('header', fn,...    
%         
%         'title', 'contextual volume forecast : learning reporting', 'author', 'Nathana?l Mayo'...
%     )
% 
% fn= fullfile(st_version.my_env.root_html_path,'Statistical Engine',st_version.my_env.target_name,'Estimators','Contextualized Volume Profile',...
%         'Learning phase report for learning validation',[name 'report.html']);        
%     

fn= 'index.html';
[pathstr, name, ext]=fileparts(name);
if isempty(pathstr) % we used a simple name-> path + index.html
    pathstr=name;
else % specific path
    fn= [name ext];
    name= pathstr;
end
pth__= name;

% if isempty(strfind(pathstr,st_version.my_env.root_html_path)) %==0
%     pth__= pathstr;
% else
%     pth__=st_version.my_env.root_html_path;
% end
% REPORT= latex_report(fn,'html_root_path',...
%     fullfile(st_version.my_env.root_html_path, 'Statistical_Engine', 'dev', 'Estimators', 'Contextualized_Volume_Profile', 'Learning_phase_report_for_validation'),...
%        'html_path_from_root', './','css_from_root', './../../../../../css/styles.css'    );
REPORT= latex_report(fn,'html_root_path',pth__,...
    'html_path_from_root', './','css_from_root', './../../css/styles.css'    );
% fullfile(st_version.my_env.root_html_path, 'learning_reports', pathstr),... %name),...
%        'html_path_from_root', './','css_from_root', './../../css/styles.css'    );
%    
   
b=my_buffer();
method_= REPORT.graph;
    function graph__(name)
        b=my_buffer();
        set(0, 'CurrentFigure', b.get(name));
        figure(b.get(name));
        set(b.get(name),'renderer','zbuffer');
        method_(b.get(name),scf__(1),scf__(2),scf__(3));
    end
REPORT.graph_v0= REPORT.graph;
REPORT.graph= @graph__; %@(name) method_(b.get(name),0.75,0,1);

    

%REPORT.legend= @(varargin) REPORT.array(make_tab(varargin{:}));
REPORT.legend= @(varargin) REPORT.array(make_tab3(varargin{:}));
legend2_= @(varargin) REPORT.array(make_tab2(varargin{:}));
function leg_labelized(varargin)
   %opt= options(varargin);
   %opt= labelize(opt);
   %args=opt.get();
   args= labelize(varargin);
   legend2_(args{:});
end
REPORT.legend2= @leg_labelized;

self=[];
self.REPORT= REPORT;
self.make_glob_graph= @make_glob_graph;
self.send_activ_graph=@send_activ_graph;
%self.add_name= @add_name;
%self.init_report= @init_report;
self.comment_specific=@comment_specific;
self.full_report=@full_report;
self.stockbystock=@stockbystock;
self.prelim_info=@prelim_info;

l_={'noter','Note Function','Npoint','Minimal obs. number',...
    'dim','Dim','seuil','Note Threeshold','sbreak','break > threesh',...    
};
self.labels= options(l_);
self.warning_info= @warning_info;

TLO= test_learning_output();    

    function full_report(ip)
        if nargin==0
            ip= indexProcess('CAC40'); %  ip.comp=2;
        end
        LR = learning_report();            
                
        warnings= LR.make_glob_graph(ip);
        
        b=my_buffer();
        b= b.get();
        n_= length(b)/2;
        for n=1:n_;set(b{2*n},'name', b{2*n-1});end
        
        self.REPORT.begin('contextual volume forecast : learning reporting','Nathana?l Mayo');    
        
        LR.prelim_info(self.REPORT,ip);
        
        LR.warning_info(self.REPORT,warnings);
        
        LR.send_activ_graph(self.REPORT);
                
        LR.comment_specific(self.REPORT); %self.REPORT);
        
        latex('section', fn, 'Stock by stock volume profiles')
        for sec_id= (ip.comp(:)')
            td_= ip.TD;
            if ischar(ip.TD) &strcmpi(ip.TD,'main')
                a=get_repository('tdinfo', sec_id);
                td_ = a(1).trading_destination_id;       
            end
            ident_= {sec_id,td_,ip.dates};
            stockbystock(ident_);
        end
        
        
        self.REPORT.close();
    end

    function f= add_name(fun)
        if nargin<2
            %global st_version
            fn= fullfile(st_version.my_env.root_html_path,'Statistical Engine',st_version.my_env.target_name,'Estimators','Contextualized Volume Profile',...
                    'Learning phase report for learning validation','report.html');        
            fn= 'report.html';
            REPORT= latex_report(fn);

            b=my_buffer();
            method_= REPORT.graph;
            REPORT.graph=@(name) method_(b.get(name),0.75,0,1);
            REPORT.legend= @(varargin) REPORT.array(make_tab(varargin{:}));
            REPORT.legend2= @(varargin) REPORT.array(make_tab2(varargin{:}));
        end
        f= @(varargin) fun(REPORT);        
    end


    function prelim_info(REPORT,ip)
        if nargin==1; ip= REPORT;REPORT= self.REPORT;end
        %latex('section', fn, 'Global informations')
        REPORT.section('Global informations')
        REPORT.text('This is an automatic report generated by the learning phase of the estimator context_volume');
        data=[];
        data.format= '%2.5f';
        td_= ip.TD;
        if iscell(td_) & length(td_) ==1
           td_=td_{1}; 
        end
        if isempty(td_); td_='all';end
        data.value= {datestr(today),length(ip.comp),td_,datestr(ip.dates(1)),datestr(ip.dates(2))};
        %data.value= {datestr(today),length(ip.comp),td_,ip.dates(1),ip.dates(2)};
        data.col_header= {'Date of report creation', 'Nb. of stocks','Trading destination','Date from','Date to'}; 
        REPORT.array(data); 
    end

    function warning_info(REPORT,w,text_callback)
        if nargin==1; ip= REPORT;REPORT= self.REPORT;end
        if nargin<3; text_callback= @list2str_;end
        REPORT.section('Stock warnings');       
        
        data=[];
        data.format= '%2.5f';       
        data.value= {'Missing stocks',text_callback(w,w.missings);'Low nb of obs. (> 30% missing obs)',text_callback(w,w.nbobs);'Weird last date (the two last days are missing)',text_callback(w,w.lastobs)};
        %data.value= {datestr(today),length(ip.comp),td_,ip.dates(1),ip.dates(2)};
        data.col_header= {'Warning', 'Stock list'}; 
        REPORT.array(data,'plain',true);         
%         REPORT.text('Missing stocks');
%         REPORT.text(list2str_(w.missings));
%         REPORT.text('\n');
%         REPORT.text('Low nb of obs.');
%         REPORT.text(list2str_(w.nbobs));
%         REPORT.text('\n');
%         REPORT.text('Weird last date');
%         REPORT.text(list2str_(w.lastobs));     
%         REPORT.text('\n');
    end
    
    function s=list2str_(l)
        if isempty(l); s= 'None';
        else
            s= num2str( l(:)' );
        end
    end
    
    l_horribl_align= {'learning_frequency','dim','estimateur','seuil','sbreak','Npoint'};
    function x=horrible_align(mdl)
        % mdl(1,:) are colnames
        % mdl(2,:) are values
        x=cell(2,length(l_horribl_align));
        for k=1:length(l_horribl_align)
            i= find(strcmp(l_horribl_align{k},mdl(1,:)));
            assert(length(i)==1);
            x(:,k)= mdl(:,i);
        end
    end

    function errors= stockbystock(REPORT,ident_)
        errors=[];
        if nargin==1;ident_= REPORT;REPORT= self.REPORT;end 
        aux_info=nan;
        link_callback=@(x)x;
        if length(ident_)>=6
            link_callback= ident_{6};
        end
        if length(ident_)>=5
            aux_info= ident_{5};
            ident_=ident_(1:4);
        end
        
        sec_id= ident_{1};
%         ric = cell2mat(get_repository('sec_id2ric', sec_id));
%         self.REPORT.pth = [ric '.html'];
%         REPORT= self.REPORT;
        
%         REPORT.begin('title', ric);
        
        
        my_buffer('clear');
          
        if iscell(ident_{2})
            if isempty(ident_{2});ident_{2}=[];
            else
                assert(length( ident_{2})==1);
                ident_{2}=ident_{2}{1};
            end
        end
        if ischar(ident_{2})
            assert(strcmpi(ident_{2},'main'));
            x_=get_repository('tdinfo',ident_{1});        
            ident_{2}= x_(1).trading_destination_id;
        end
        try
            [pred,B,R,info]= TLO.plot_predictors({ident_{:},'dont_show_inhib'}); % OK, diviser en plusieur plots ?
         catch ME            
             self.REPORT.subsection(['security= ' num2str(sec_id) ' failed']) ; % j'ai r?jout? self.
             errors= ME;
             return;
         end
        
        %[pred,B,R,info]= TLO.plot_predictors(ident_);
        
        self.REPORT.subsection(['security= ' B.NAMEstock ' (' num2str(sec_id) '), td= ' num2str(ident_{2})]) ; 
        
        self.REPORT.subsection('Parameters')
        
        mdl= reshape(info.data,2,length(info.data)/2);
        data= [];
        if iscell(mdl{2,2})
           td_= mdl{2,2};
           if isempty(mdl{2,2}) ; mdl{2,2}= 'main';
           else
               mdl{2,2}=td_{1};
           end            
        end
        data.value= labelize(mdl(2,:)) ;        
        data.col_header= labelize(mdl(1,:));
        data.format= '%2.3f';
        REPORT.array(data,'plain',true);
        
        persistent_buffer('stock_info_1',data);
                
        self.REPORT.text('The best predictive model has the following parameters (see section Stock by stock indicators for definition)');
        
        mdl= reshape(info.model,2,length(info.model)/2);
        data= [];
        mdl= horrible_align(mdl);
        if iscell(mdl{2,2})
           td_= mdl{2,2};
           if isempty(mdl{2,2}) ; mdl{2,2}= 'main';
           else
               mdl{2,2}=td_{1};
           end            
        end       
        data.value= labelize(mdl(2,:)) ;
        data.col_header= labelize(mdl(1,:));
        data.format= '%2.3f';
        REPORT.array(data);
        persistent_buffer('stock_info_2',data);
        
        self.REPORT.subsection('Selected contexts and rankings')
        REPORT.text('The following contexts have been selected');
        data= [];
        info.colnames{4}= [info.colnames{4} ' (bp)'];
        info.colnames{5}= [info.colnames{5} ' (bp)'];
        info.stats(:,4)= cellfun(@(x) x*10000,info.stats(:,4),'uni',0 );
        info.stats(:,5)= cellfun(@(x) x*10000,info.stats(:,5),'uni',0 );        
        info.colnames{8}= [info.colnames{8} ' (%)'];
        info.colnames{9}= [info.colnames{9} ' (%)'];
        info.colnames{7}= [info.colnames{7} ' (%)'];
        info.stats(:,8)= cellfun(@(x) str2num(x)*100,info.stats(:,8),'uni',0 );
        info.stats(:,9)= cellfun(@(x) str2num(x)*100,info.stats(:,9),'uni',0 );   
        info.stats(:,7)= cellfun(@(x) x*100,info.stats(:,7),'uni',0 );   
        if isstruct(aux_info)
            %idx_= find(aux_info.sec==sec_id);
%             idx_= find(aux_info.sec_ids_0==sec_id);
%             assert(length(idx_)==1);
%             aux_info.X= aux_info.X(idx_,:);
%             x= cellfun(@str2num,info.stats(:,3));                
%             z_=[];
%             for k= (x(:)')
%                 %i_= find([aux_info.key{:}]==k); assert(length(i_)==1);
%                 i_= find([aux_info.keys{:}]==k); assert(length(i_)==1);
%                 z_(end+1)= aux_info.X(i_);
%             end
            idx_= find(aux_info.sec_ids_0==sec_id);
            assert(length(idx_)==1);
            aux_info.X0= aux_info.X0(idx_,:);
            x= cellfun(@str2num,info.stats(:,3));                
            z_=[];
            for k= (x(:)')
                %i_= find([aux_info.key{:}]==k); assert(length(i_)==1);
                i_= find([aux_info.key0{:}]==k); assert(length(i_)==1);
                z_(end+1)= aux_info.X0(i_);
            end
            info.colnames{10}= 'Cash improvement (%)';
            info.stats(:,10)= num2cell(100*z_'); 
        end        
        %data.value= info.stats; data.col_header=  info.colnames; data.format= '%2.5f';
        data.value= info.stats; data.col_header=  info.colnames; data.format= '%2.3f';
        data.value(:,3)= cellfun(link_callback,data.value(:,3),'uni',0); % insert links
        
        [P_,Q_]=size(data.value);
        LM=cell(P_,Q_);
        for p_=1:P_
           LM{p_,2} = sprintf('news_%s',data.value{p_,3}); %['news_' num2str(data.value{p_,3},0)];
        end                   
        
        REPORT.array(data,'plain',true,'link',true,'link_matrix',LM);   
        
        self.REPORT.subsection('Volume curves')
        REPORT.text('Volume profiles in contexts');
        b=my_buffer();
        b= b.get();
        REPORT.graph(b{1});      
        
%         self.REPORT.pth = 'index.html';
    end

    function comment_specific(REPORT)     
        if nargin==0;REPORT= self.REPORT;end            
        
        REPORT.section('Stock by stock indicators')        
        REPORT.text('This section defines indicators shown for each stock in the next section.');
        
        REPORT.subsection('Legends and help')                     
        
        REPORT.text('Model parameters');
        
        REPORT.legend2(...
            'Model parameter','Description',...
            'noter','Note function used to rank contexts. See table below',...
            'Npoint','Minimum nb of observations required to keep an event',...
            'dim','nb of bins of learning data (36 <=> 15min bins)',...
            'seuil','Keeps events with note value < threeshold',...
            'sbreak','DEPRECATED. 1 if the process continues ranking events after finding one event with note> seuil',...
            'estimator','Volume predictor used in each (orthogonalized) context'...
        );
        
        REPORT.text('Note functions');
        REPORT.legend2(...
            'Parameter [Note Function]','Description',...
            'pv','pvalue for [H0: context is different]. Based on 10 percent quantile of pvalues in each bin (ANOVA)',...
            'mpv','pvalue for [H0: context is different]. Based on multivariate test (MANOVA)',...
            'std','ratio of dispersion'...           
        );
        
        REPORT.text('Details on graphics');
        REPORT.legend2(...
            'Information about context','Description',...
            'OC (operational criteria))','Execution Slippage (1 min bins), multiscale',...
            '...when selected','When the procedure selects the context as main context',...
            '...when occurs','When the event occurs',...
            'learning note','p-values (when noter =[pv] or [mpv]), or std ratio (noter=[std])',...
            'Graphic info','Volume forecast in each context (each subplot depicts the curves for 5 contexts)',...
            'dotted line','Inhibited contexts'...
        );
    end
    
    

    function x= send_activ_graph(REPORT)        
        if nargin==0; REPORT= self.REPORT;end
        x=nan;       
        %b= b.get();
        %n_= length(b)/2;
        %for n=1:n_;set(b{2*n},'name', b{2*n-1});end

        %+ date _run
        
        %REPORT.begin('contextual volume forecast : learning reporting','Nathana?l Mayo');
        %latex('header', fn, 'title', 'contextual volume forecast : learning reporting', 'author', 'Nathana?l Mayo')
        %latex('section', fn, 'Global performances');
        REPORT.section('Global indicators')
        REPORT.text('This graphic depicts the global quality of the forecast');
        REPORT.graph('main_summary')                        
        REPORT.legend(...
            'OC','Multiscale VWAP slippage, computed on 1 min bins. Relative to naive predictor.',...
            'When selection','When a context is selected',...
            '+Usual Day','Using a non naive predictor in Usual Day context'...            
        );
    
        REPORT.subsection('Contexts prevalence')  
        REPORT.text('This graphic depicts the most important contexts');
        REPORT.graph('avg_ranks')
        REPORT.legend(...
            'Average','mean over stocks of ranking / total number of context',...
            'Best','Best ranking across stocks',...
            'Worst','Worst ranking across stocks',...
            'top 25%','fraction of stock ranking the event in top 25%',...
            'top 50%','fraction of stock ranking the event in top 50%',...
            'Non-selected','Proportion of stocks that do not rank the event',...
            'Stock normalized','Divided by number of ranked events, rather than total number of events'...
        );
        REPORT.text('This graphic depicts how contexts are ranked for each stock');
        REPORT.graph('ranks')
        
        
        REPORT.subsection('Average performance')     
        REPORT.text('These graphics depict the average performance of our estimator for each stock.');
        REPORT.text('Performance is measured by vwap slippage on 1 min bins.');
        REPORT.legend(...
            'P(context switch)','Proportion of dates where a context is selected',...
            'Nb selected context','normalized by total number of events',...
            'Nb inhibited context','normalized by total number of events',...
            'Full sample Error','Multiscale slippage, computed on 1 min bins',...
            'Context switch Error','same, but computed on days where some context is selected',...
            'NB', 'slippage values are relative to the slippage of the (anticipative) median'...
        );
        REPORT.graph('GR_display');
        REPORT.text('Performance on volume forecast only (Volume forecast errors)');
        REPORT.graph('GR_display_volume');

        REPORT.subsection('Performance in contexts')
        REPORT.text('These graphics depict the performance (absolute values) for each stock and each context. ');
        REPORT.text('Before inhibition');
        REPORT.graph('CO4before');
        REPORT.text('After inhibition.');
        REPORT.graph('CO4after');
        REPORT.legend(...
            'Operationnal Criteria' ,'Multiscale slippage, computed on 1 min bins'...
        );
        
        REPORT.subsection('Relative performance in contexts')
        REPORT.text('These graphics depict the performance (relative values) for each stock and each context. ');
        REPORT.text('Values are relative to a naive estimator (full sample median). ');
        REPORT.text('Before inhibition');
        REPORT.graph('RCO4before');
        REPORT.text('After inhibition');
        REPORT.graph('RCO4after');
        REPORT.legend(...
            'Operationnal Criteria' ,'slippage values are relative to the slippage of the (anticipative) median'...
        );
        
        %comment_specific(REPORT) 
    
        %x= REPORT.close()
    end

function warnings=make_glob_graph(ip)
    warnings=[];
    t=test_learning_output();   
    t.show_performance();
    my_buffer('clear')
    
        res_final= gatherResults(ip.comp(:)',1,1,@(sec_id) cb_newinfo(ip,sec_id) ); 
    
        stocks= cellfun(@(x) x.best.IDstock,res_final.alldata);
        warnings.missings= setdiff(ip.comp,stocks);
        ip.comp= intersect(stocks,ip.comp);
        
        [warnings.nbobs,warnings.lastobs]= t.warning_nb_obs(res_final);
        
        [failed,res]= t.show_performance(ip,1,0); % FIXME (ip,1,0)
        set(gcf,'visible','off')
        
        t.show_main_indicators(res,res_final)
        set(gcf,'visible','off')
        
        [D,b,base,failed]= contexte_learning(ip);
        base.value= base.num;
        
        %latex('section', fn, 'Aggregated rankings of contexts')
        
        [R,N]= t.show_average_ranks(base,1) ;
        set(gcf,'visible','off')
        Y= t.show_ranks(base,1)  ;
        set(gcf,'visible','off')
        res01= gatherResults(ip.comp(:)',1,0,@(sec_id) cb_newinfo(ip,sec_id));
        [X,keys,X0,key0]= t.show_co(res01.alldata,1);
        set(gcf,'visible','off')
        [X,keys,X0,key0]= t.show_co(res01.alldata,1,1);
        set(gcf,'visible','off')

    end

    function x= make_tab(varargin)
        x= [];
        %x.linesHeader =varargin(1:2:end);
        x.col_header =varargin(1:2:end);
        x.col_header=x.col_header(:)';
        x.value= varargin(2:2:end);
        x.value=x.value(:)';
    end
    function x= make_tab3(varargin)
        x= [];
        x.linesHeader =varargin(1:2:end);
        x.linesHeader= x.linesHeader(:);
        %x.col_header =varargin(1:2:end);
        x.col_header= {'Legend'}; %x.col_header(:)';
        x.value= varargin(2:2:end);
        x.value=x.value(:);
    end
    function x= make_tab2(varargin)
        x= [];
        x.col_header = varargin(1:2);
        %x.linesHeader =varargin(1:2:end);
        %x.linesHeader=x.linesHeader(:);
        x.value= {};
        x.value(:,1)= varargin(3:2:end);
        x.value(:,2)= varargin(4:2:end);
        %x.value= reshape(x.value,length(varargin)/2,2);
        %x.value= varargin(2:2:end);
        %x.value=x.value(:)';
    end

    function v= labelize(name)
        L= self.labels;
        if ischar(name)  || isnumeric(name)      
            try; v= L.get(name);
            catch; v= name;
            end
        elseif iscell(name)
            v=name;
            %for n=1:2:length(name)
            for n=1:length(name)
               v{n}= labelize(name{n});
            end   
        else
           v=options();
           l= name.get();
           for n=1:2:length(l)
              v.set(labelize(l{n}),l{n+1}) 
           end            
        end
    end

    function old_()


      if nargin<2; glob=1;end
      if nargin<3; spec=1;end
      % TODO: fullscreen
      % -select le bon graphe (les?) avec show_co
      % section erased ?
    t=test_learning_output();        
    %ip= indexProcess('CAC40');

    %ip.comp=8

    %global st_version
    fn= fullfile(st_version.my_env.root_html_path,'Statistical Engine',st_version.my_env.target_name,'Estimators','Contextualized Volume Profile',...
        'Learning phase report for learning validation','report.html');
    % + date _run

    latex('header', fn, 'title', 'contextual volume forecast : learning reporting', 'author', 'Nathana?l Mayo')

    my_buffer('clear')

    if glob
        %latex('section', fn, 'Global performances');

        %latex('subsection', fn, 'Operationnal Criteria');

        failed= t.show_performance(ip,1,0); % OK
        
       

        [D,b,base,failed]= contexte_learning(ip.comp(:));
        base.value= base.num;

        %latex('section', fn, 'Aggregated rankings of contexts')
        
        [R,N]= t.show_average_ranks(base,1) ;
        Y= t.show_ranks(base,1)  ;

        %res01= gatherResults(ip.comp(:)',ip.useInhibition,ip.final_);
        res01= gatherResults(ip.comp(:)',1,0);
        [X,keys,X0,key0]= t.show_co(res01.alldata,1);
        
        [X,keys,X0,key0]= t.show_co(res01.alldata,1,1);
       

        latex('graphic', fn, 'graph_handle', gcf, 'scalebox', ...         
        0.75, 'close', true, ...
         'fullscreen', true) 

    end

    if spec        
        latex('section', fn, 'Stock by stock volume profiles')
        for sec_id= (ip.comp(:)')
            try
                [pred,B,R,info]= t.plot_predictors(sec_id); % OK, diviser en plusieur plots ?

                mdl= reshape(info.model,2,length(info.model)/2);
                latex('array', fn, mdl(2,:),'col_header',mdl(1,:), 'format', '%2.5f');

                latex('array', fn, info.stats, 'col_header', info.colnames,'format', '%2.5f');                   

                latex('graphic', fn, 'graph_handle', gcf, 'scalebox', ...         
                0.75, 'close', true, ...
                 'fullscreen', true) ;                

            end
        end
    end        
    return
    end
%%
end
%%

function run_all_report()
ip= indexProcess('CAC40');
ip.TD= {}; 
ip.comp=2;
ip.dates= [733043,734319];
LR = learning_report('test_new_v');
LR.full_report(ip)

close('all')

ip= indexProcess('CAC40');
ip.TD= [];
ip.dates= [733043,734319];
LR = learning_report('nnn_CAC_all_');
LR.full_report(ip)

%-----------------------------
ip= indexProcess('SBF120');
ip0= indexProcess('CAC40');
ip.comp= setdiff(ip.comp,ip0.comp);

ip.TD= {};
ip.dates= [733043,734319];
LR = learning_report('new_SBF_all_');
LR.full_report(ip)

close('all')

ip.TD= {'main'};
ip.dates= [733043,734319];
LR = learning_report('new_SBF_main_');
LR.full_report(ip)

close('all')
end

function TEST_propre()

    st_version.my_env.root_html_path= 'C:\learning_reports';

    % selection de la composition. Une seul TD ? la fois !
    ip= indexProcess('CAC40') %('S&P500 STOCK INDEX');
    ip.comp=[2,5];
    ip.TD= {}; {'main'};
    ip.dates= [733043,734319];
    
    % en une etape
    LR = learning_report('test')
    LR.full_report(ip)
    
    % etape s?par?e
    LR = learning_report('test')
    LR.REPORT.begin('test','moi');
    LR.prelim_info(ip)
    LR.make_glob_graph(ip)
    %LR.comment_specific()
    LR.stockbystock({11,{},ip.dates});
    LR.REPORT.close();
end

    function TEST()
        
        
        LR = learning_report('test')
        LR.REPORT.begin('test','moi');
        LR.comment_specific()
        
        LR.stockbystock({2,4,[732858,734319]});
        
        LR.REPORT.close();
        %%
        LR = learning_report('full_main')
        LR.full_report(ip)
        %%
        ip= indexProcess('CAC40'); %  ip.comp=2;
        LR = learning_report()
        LR.make_glob_graph(ip)
        
        b=my_buffer();
        b= b.get();
        n_= length(b)/2;
        for n=1:n_;set(b{2*n},'name', b{2*n-1});end
        
        
        LR.send_activ_graph();
        
        LR = learning_report();LR.REPORT.begin('test','moi');LR.stockbystock(2);LR.REPORT.close();
        
%%
    
    
    if glob
        latex('section', fn, 'Global performances');

        latex('subsection', fn, 'Operationnal Criteria');

        failed= t.show_performance(ip,1,0); % OK
        latex('graphic', fn, 'graph_handle', gcf, 'scalebox', ...         
        0.75, 'close', true, ...
         'fullscreen', true) 

        [D,b,base,failed]= contexte_learning(ip.comp(:));
        base.value= base.num;

        latex('section', fn, 'Aggregated rankings of contexts')
        [R,N]= t.show_average_ranks(base,1) ;
        latex('graphic', fn, 'graph_handle', gcf, 'scalebox', ...         
        0.75, 'close', true, ...
         'fullscreen', true);

        Y= t.show_ranks(base,1)  ;
        latex('graphic', fn, 'graph_handle', gcf, 'scalebox', ...         
        0.75, 'close', true, ...
         'fullscreen', true) ;

        %res01= gatherResults(ip.comp(:)',ip.useInhibition,ip.final_);
        res01= gatherResults(ip.comp(:)',1,0);
        [X,keys,X0,key0]= t.show_co(res01.alldata,1);
        latex('graphic', fn, 'graph_handle', gcf, 'scalebox', ...         
        0.75, 'close', true, ...
         'fullscreen', true) 

        latex('graphic', fn, 'graph_handle', gcf, 'scalebox', ...         
        0.75, 'close', true, ...
         'fullscreen', true) 

        [X,keys,X0,key0]= t.show_co(res01.alldata,1,1);
        latex('graphic', fn, 'graph_handle', gcf, 'scalebox', ...         
        0.75, 'close', true, ...
         'fullscreen', true) 

        latex('graphic', fn, 'graph_handle', gcf, 'scalebox', ...         
        0.75, 'close', true, ...
         'fullscreen', true) 

    end

    if spec        
        latex('section', fn, 'Stock by stock volume profiles')
        for sec_id= (ip.comp(:)')
            try
                [pred,B,R,info]= t.plot_predictors(sec_id); % OK, diviser en plusieur plots ?

                mdl= reshape(info.model,2,length(info.model)/2);
                latex('array', fn, mdl(2,:),'col_header',mdl(1,:), 'format', '%2.5f');

                latex('array', fn, info.stats, 'col_header', info.colnames,'format', '%2.5f');                   

                latex('graphic', fn, 'graph_handle', gcf, 'scalebox', ...         
                0.75, 'close', true, ...
                 'fullscreen', true) ;                

            end
        end
    end


    latex('end', fn)
    end
        %latex('compile', fn)