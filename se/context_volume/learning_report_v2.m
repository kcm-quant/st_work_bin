function self = learning_report_v2(name,scf__,SPLIT_SIZE)
% learning_report_v2(name,scf,split_size) - Contexte volume learning auto?atic report
%
% Arguments 
%   * scf= [scale,close, fullscreen] how latex deals with figs
%   * split_size= number of stocks on each (global) graphs
%
% Argument of self.create_full_report(comp,TD,dates,report_name,report_folder)
%   * comp : stock composition (either sec_id vector or index name string)
% Exemple:
%    R= learning_report_v2('',[1 1 1],40);
%    R.create_full_report('CAC40',{},[733043,734319],'rapport_de_demo','C:\\demo_folder');
%
%   author   : 'nmayo@cheuvreux.com'
%   reviewer : ''
%   date     :  '14/02/2012'
%   
%   last_checkin_info : $Header: learning_report_v2.m: Revision: 34: Author: robur: Date: 05/13/2013 05:12:48 PM$
if nargin==0; name= '';end
if nargin<2; scf__= [1 1 1];end

if nargin<3; SPLIT_SIZE=40; end
%% FIXME 40

global st_version
fn= 'index.html';
% REPORT= latex_report(fn,'html_root_path',...
%     fullfile(st_version.my_env.root_html_path, 'Statistical_Engine', 'dev', 'Estimators', 'Contextualized_Volume_Profile', 'Learning_phase_report_for_validation'),...
%        'html_path_from_root', './','css_from_root', './../../../../../css/styles.css'    );

%FOLDER= fullfile('learning_reports', name);

fn= 'index.html';
[pathstr, name, ext]=fileparts(name);
if isempty(pathstr) % we used a simple name-> path + index.html
    pathstr=name;
else % specific path
    name= [name ext];
    %name= pathstr;
end
if isempty(strfind(pathstr,st_version.my_env.root_html_path)) % == 0
    pth__= pathstr;
else
    pth__= fullfile(st_version.my_env.root_html_path, 'Contextualized Volume Curves', 'learning_reports'); %st_version.my_env.root_html_path;
end
%FOLDER= fullfile(pth__, 'Contextualized Volume Curves', 'learning_reports', name);

self=[];
self.name= fullfile(pth__,name);

self.full_report=@full_report;
self.root_doc= @root_doc;
self.go_all_stocks= @go_all_stocks;

self.make_glob_graph=@make_glob_graph;

self.glob_perf=sub_report('global_perf','Global Performance','N.Mayo',@glob_perf);
self.rankings=sub_report('rankings','Rankings','N.Mayo',@rankings);
self.full_perf=sub_report('full_perf','Full Performance','N.Mayo',@full_perf);
self.volume_perf=sub_report('volume_perf','Volume forecast Performance','N.Mayo',@volume_perf);
self.warnings=sub_report('warnings','Stock Warnings','N.Mayo',@warnings);
self.create_full_report= @create_full_report;

self.stockbystock=@stockbystock;

self.create_news_curves=@create_news_curves;

self.all_news_curves=@all_news_curves;
self.news_curves= @news_curves;
self.save_data= @save_data;


function pth= getlink(sec, td)
   if nargin==1; td= 'aucune';end
   FOLDER_='';
   if ischar(sec)% & strcmpi(sec,'index')
       if sec(1)== '-'
           pth= fullfile(FOLDER_,['news_' num2str(sec(2:end)) ]);
       else
           pth= fullfile(FOLDER_,sec);
       end       
   else
      if isempty(td); td='NaN';
      else; td= num2str(td);
      end      
      assert(~strcmpi(td,'aucune'));
      sec= num2str(sec);
      %name = [num2str(ident{1}) '_' td_ '_' num2str(ident{3}(1)) '_' num2str(ident{3}(2))];    
      pth= fullfile(FOLDER_,['stock_' sec '_' td]);
   end    
   %pth = ['file:///' pth '.html'];
   pth = [pth '.html'];
   if findstr(pth,'\report\')
      WWW=1; 
   end
end

function pth= getlink_v0(sec)
   FOLDER_='';
   if ischar(sec)% & strcmpi(sec,'index')
       if sec(1)== '-'
           pth= fullfile(FOLDER_,['news_' num2str(sec(2:end)) ]);
       else
           pth= fullfile(FOLDER_,sec);
       end       
   else
      sec= num2str(sec);
      %name = [num2str(ident{1}) '_' td_ '_' num2str(ident{3}(1)) '_' num2str(ident{3}(2))];    
      pth= fullfile(FOLDER_,['stock_' sec ]);
   end    
   %pth = ['file:///' pth '.html'];
   pth = [pth '.html'];
   if findstr(pth,'\report\')
      WWW=1; 
   end
end

%%
function rpath= root_doc(rep_folder,ip)
    % TODO : use ip.glob_stats pour graphes et tableaux globaux
    % TODO : hyperlinks dans les tableaux
%     REPORT= latex_report(fn,'html_root_path',FOLDER,...
%         'html_path_from_root', './','css_from_root', './../../css/styles.css'    );
%   DONE: rajout? ../ pour la feuille de style, pour Romain
    % TODO : acc?der ? cette feuille ind?pendemment de la ou on ?crit
    REPORT= latex_report(fn,'html_root_path',rep_folder,...
            'html_path_from_root', './','css_from_root', './../../../css/styles.css'    );
            %'html_path_from_root', './','css_from_root', './../../css/styles.css'    );
    
    LR = learning_report(self.name);     %fullfile(self.name,['stock_' sec '.html'])
    LR.REPORT.graph= get_graph_decorator_fun(LR.REPORT.graph_v0);
    
    REPORT.begin('contextual volume forecast : learning reporting','Nathanael Mayo');    
    LR.prelim_info(REPORT,ip);        
    REPORT.hlink(getlink('warnings'),'Stock warnings'); 
    REPORT.section('Global indicators'); 
    
    % graphique de perf globales
    nms_={};gfs=[];gno=[]; ps= [];
    for n= 1:length(ip.glob_perf.v.IDstock)
        idcol_= find(strcmpi(ip.glob_perf.v.RS.colnames,'cash^2 1mn')); assert(length(idcol_)==1);
        gfs(n)= ip.glob_perf.v.RS.no(n,idcol_);
        gno(n)= ip.glob_perf.v.RS.fs(n,idcol_);
        nms_{n}= ip.glob_perf.v.names{n};
        ps(n)= ip.glob_perf.v.RS.prob(n);
    end
    [~,idx]= sort(gno,'descend');gfs=gfs(idx);gno= gno(idx); nms_= nms_(idx);ps= ps(idx);
    ns_figure;    
    subplot(2,1,1)
    h= plotpoint(gfs,gno,'marker','*','markersize',10); degradeHandle(h,1:length(h));
    set(gca,'xlim',[0 1]);set(gca,'ylim',[0 1]);
    set(gca,'xtick',0:0.1:1),set(gca,'xticklabel',chartableau((0:10:100)','%'));
    set(gca,'ytick',0:0.1:1),set(gca,'yticklabel',chartableau((0:10:100)','%'));
    
    KK_= min(length(ip.glob_perf.v.IDstock),5);
    legend(h(1:KK_),nms_(1:KK_),'location','southwest');    
    ylabel('Performance (relative VWAP slippage). Full sample');    
    subplot(2,1,2)
    h= plotpoint(gfs,ps,'marker','*','markersize',10); degradeHandle(h,1:length(h));
    %ip.glob_perf.v.RS.prob
    set(gca,'xlim',[0 1]);set(gca,'ylim',[0 1]);
    set(gca,'xtick',0:0.1:1),set(gca,'xticklabel',chartableau((0:10:100)','%'));
    set(gca,'ytick',0:0.1:1),set(gca,'yticklabel',chartableau((0:10:100)','%'));
    ylabel('Prob(context selection)');
    xlabel('Performance (relative VWAP slippage) when ranked news occur');    
    my_buffer('globperfall',gcf);
    LR.REPORT.graph('globperfall');
    %
    REPORT.hlink(getlink('global_perf'),'Global performance');    
    REPORT.hlink(getlink('rankings'),'Rankings');
    REPORT.hlink(getlink('full_perf'),'Performance in every context');
    REPORT.hlink(getlink('volume_perf'),'Volume only performance');
       
    %REPORT.hlink(getlink('volume_perf'),'Volume only performance');
    %LR.warning_info(REPORT,warnings);
   
    REPORT.section('News by news volume curves'); 
    % TODO match les lignes et Nonews !!!
    new_info= ip.news_data.get();
    avg_rank= mean(ip.SR.Y,2);
        
    Z=[]; Z.format= '%2.3f';
    Z.linesHeader= {'Ranked','Inhibited','Avg.rank','Avg.rank,non inhibited	'}';
    Z.col_header= {'Legend'}; %{' ','Legend'}; %{'Legend'}'; % TIPS dimension 2 fait bugger !
    Z.value={'Number of stocks that rank the news (without inhibiting)',...
             'Number of stocks inhibiting the news',...
             'Average rank of news over stocks, including inhibition'...
             'Average rank of news over stocks, excluding inhibition'}';
    REPORT.array(Z);  
    
    x=persistent_buffer('tableau_news');
    if ~isempty(x)
        Z= x{1};
        for n=2:length(x)
           assert( all(strcmp(Z.col_header,x{n}.col_header)) )
           Z.value(end+1,:)=  x{n}.value;
        end        
        %       
        [P_,Q_]=size(Z.value);
        LM=cell(P_,Q_);
        for p_=1:P_
           LM{p_,1} = sprintf('news_%s',Z.value{p_,2}); %['news_' num2str(data.value{p_,3},0)];
        end                                
        col_= find(strcmpi(Z.col_header,'Avg.rank,non inhibited')); assert(length(col_)==1);
        col__= find(strcmpi(Z.col_header,'Avg.rank')); assert(length(col_)==1);
        [~,idx_s]= sort(([Z.value{:,col_}]+[Z.value{:,col__}]/1000));
        Z.value = Z.value(idx_s,:); LM= LM(idx_s,:);
        REPORT.array(Z,'plain',true,'link',true,'link_matrix',LM);   
    end
    
    
%     if ~isempty(ip.SR.ctxt)
%         [~,idx]= sort(avg_rank,'ascend');
%         n= newsDb();news_id_ag= n.name2id(ip.SR.ctxt(idx));
%         %for n=1:floor(length(new_info)/2)
%         for n=(news_id_ag(:)')
%             %REPORT.hlink(['file:///',fullfile(FOLDER,['stock_' num2str(sec) '.html'])],num2str(sec));               
%             id_n= ['-' num2str(n)];
%             info_= ip.news_data.get(n);
%             REPORT.hlink(getlink(id_n),info_.news_name);       
%         end   
%     end
%     id_n= ['-' num2str(0)]; %% Usual Days
%     info_= ip.news_data.get(0);
%     REPORT.hlink(getlink(id_n),info_.news_name);  
    
    REPORT.section('Stock by stock volume curves');  
    
    xxx= persistent_buffer('stock_info_1');Z=[];
    if ~isempty(xxx)
       Z= xxx{1} ;
       for n=2:length(xxx)
           all(strcmp(Z.col_header,xxx{n}.col_header))
           Z.value(end+1,:)= xxx{n}.value;
       end
    end
    xxx= persistent_buffer('stock_info_2');ZZ=[];
    if ~isempty(xxx)
       ZZ= xxx{1} ;
       for n=2:length(xxx)
           all(strcmp(ZZ.col_header,xxx{n}.col_header))
           ZZ.value(end+1,:)= xxx{n}.value; 
       end
    end    
    Z.value = [Z.value,ZZ.value];
    Z.col_header = [Z.col_header,ZZ.col_header];
    
    Z.col_header{3}= 'nb.obs';
    
    rem= ismember(Z.col_header,{'Note Function','Minimal obs. number', 'break > threesh','inhibition','type'});
    Z.value(:,rem)=[]; Z.col_header(rem)=[];    
    
    f=@(x) x(strfind(x,' ')+1:end);
    secs= cellfun(@(x) str2num(f(x)),Z.value(:,1));%,'uni',0 )
    
    COL= strcmpi(ip.glob_perf.v.RS.colnames, 'cash 1mn' );
    COL= find(COL); assert(length(COL)==1);
    new_info={};
    for n=1:length(secs)
        ligne= find(ip.glob_perf.v.IDstock== secs(n));
        assert(length(ligne)==1);  
        new_info{n,1}= ip.glob_perf.v.RS.no(ligne,COL);
        new_info{n,2}= ip.glob_perf.v.RS.fs(ligne,COL);
        new_info{n,3}= ip.glob_perf.v.RS.prob(ligne);
        new_info{n,4}= num2str(ip.glob_perf.v.RS.nctxt(ligne));
        new_info{n,5}= num2str(ip.glob_perf.v.RS.inhib(ligne));
    end
    
    Z.value= [Z.value,new_info];
    Z.col_header=[Z.col_header {'perf|news','perf|sample','P(context)','selected','inhibited' }];
    
    [P_,Q_]=size(Z.value);
    td__= ip.TD;
    if isempty(td__); td__= 'NaN';
    else; td__= num2str(td__);end
    LM=cell(P_,Q_);
    for p_=1:P_
       LM{p_,1} = sprintf('stock_%d_%s',secs(p_),td__); %['news_' num2str(data.value{p_,3},0)];
    end          
    
    % NEW
    Z.value(:,[6 7])= cellfun(@num2str,Z.value(:,[6 7]),'uni',0);
    
    col_= find(strcmpi(Z.col_header, 'perf|news')); assert(length(col_)==1);
    [~,idx_s]= sort(([Z.value{:,col_}]));
    Z.value = Z.value(idx_s,:); LM= LM(idx_s,:);
    
    REPORT.array(Z,'plain',true,'link',true,'link_matrix',LM);         
     
     
%     idx= strcmpi( ip.glob_perf.v.RS , 'cash 1mn');
%     assert(length(idx)==1);
%     infos={};
%     infos
%     data.value= labelize(mdl(2,:)) ;
%     data.col_header= labelize(mdl(1,:));
%     data.format= '%2.3f';
%     REPORT.array(data);
    %%
    
%     names_= get_security_name(ip.comp);
%     if ~iscell(names_);names_ ={names_};end
%     for n=1:length(ip.comp)
%         %REPORT.hlink(['file:///',fullfile(FOLDER,['stock_' num2str(sec) '.html'])],num2str(sec));       
%         REPORT.hlink(getlink(ip.comp(n)),[num2str(ip.comp(n)) ' - ' names_{n}]);       
%     end
    rpath= REPORT.close();
end

%%
function errors= go_all_stocks(ip,aux_info,filter)
    if nargin==2; filter= @(x) x;end
    ip.comp= filter(ip.comp);
    errors= {};
    
    for sec_id= (ip.comp(:)')
        td_= ip.TD;
        if ischar(ip.TD) &strcmpi(ip.TD,'main')
            a=get_repository('tdinfo', sec_id);
            td_ = a(1).trading_destination_id;       
        end
        ident_= {sec_id,td_,ip.dates,ip.path_modifier};
        errors{end+1}= self.stockbystock(ident_,aux_info);
    end    
end

function x= link_callback_(news_id)
%     l= []; news_id= ['-' num2str(news_id,'%d')]
%     x= list2str_(news_id);
    tbx= latex_report('blabla');
    news_id= ['-' num2str(news_id,'%d')];
    x= tbx.hlink(getlink(news_id),news_id(2:end),1);
end        

function errors= stockbystock(ident,aux_info) %(REPORT,ident_)
    % non decor? a cause de l'argument -> DONE
    if isempty(ident{2});td_= 'all';
    else;td_= num2str(ident{2});end
    
    td_postfix_path= td_;
    if strcmpi(td_,'all'); td_postfix_path= 'NaN'; end
    
    sec= num2str(ident{1});
    %LR = learning_report(['test//stock_' sec '.html']);
    LR = learning_report(fullfile(self.name,['stock_' sec '_' td_postfix_path '.html']));
    
    %nm_= fullfile(self.name,[subname,'.html']);

    LR.REPORT.graph= get_graph_decorator_fun(LR.REPORT.graph_v0);
    
    LR.REPORT.begin(['Security_id= ' sec ],'N.Mayo');
    LR.REPORT.hlink(getlink('index'),'Back to index');   
    %LR.stockbystock(LR.REPORT,{ident{:},aux_info,@link_callback_});
    errors= LR.stockbystock(LR.REPORT,{ident{:},aux_info}); % ca a l'air dur de metre un lien la dedans
    LR.REPORT.close();    
end
%%

%function [warnings,CO,SR,res_final_]=make_glob_graph(ip)
% Il faut renvoyer celle qui contient tt le monde res_final et non
% res_final_ (is temporaire for SPLIT_SIZE )
function [warnings,CO,SR,res_final]=make_glob_graph(ip)
    warnings=[];
    globstats=[];
    t=test_learning_output();   
    
    my_buffer('clear');my_buffer('stack');my_buffer('uni');
    
        res_final= gatherResults(ip.comp(:)',1,1,@(sec_id) cb_newinfo(ip,sec_id) );     
        stocks= cellfun(@(x) x.best.IDstock,res_final.alldata);
        warnings.missings= setdiff(ip.comp,stocks);
        ip.comp= intersect(stocks,ip.comp);               
        
        [warnings.nbobs,warnings.lastobs]= t.warning_nb_obs(res_final);
        CO=[];CO.X= {};CO.X0={};CO.keys={};CO.key0={};CO.sec_ids_0={};
        
        %% NEW       
        [D,b,base,failed]= contexte_learning(ip,0,nan);
        base.value= base.num;
        
        [R,N]= t.show_average_ranks(base,1) ;
        b= my_buffer(); b=b.get(); b=b(2:2:end);b= [b{:}];set([b{:}],'visible','off');
        %%
        
        Nstocks_=length(ip.comp);
        for k=1:ceil(Nstocks_/SPLIT_SIZE)
            
            t.show_performance();
            
            d_= (k-1)*SPLIT_SIZE +1;
            f_= (k)*SPLIT_SIZE;
            if f_>Nstocks_;f_=Nstocks_;end
            %[d_,f_];
            
            ip_=ip; ip_.comp= ip.comp(d_:f_);
            %res_= res(d_:f_);
            res_final_= res_final;
            res_final_.alldata= res_final.alldata(d_:f_);
            res_final_.v.IDstock= res_final_.v.IDstock(d_:f_);
            res_final_.v.names= res_final_.v.names(d_:f_);
            res_final_.v.AS.fs= res_final_.v.AS.fs(d_:f_,:);
            res_final_.v.AS.no= res_final_.v.AS.no(d_:f_,:);
            res_final_.v.AS.prob= res_final_.v.AS.prob(d_:f_);
            res_final_.v.AS.nctxt= res_final_.v.AS.nctxt(d_:f_);
                        res_final_.v.RS.fs= res_final_.v.RS.fs(d_:f_,:);
            res_final_.v.RS.no= res_final_.v.RS.no(d_:f_,:);
            res_final_.v.RS.prob= res_final_.v.RS.prob(d_:f_);
            res_final_.v.RS.nctxt= res_final_.v.RS.nctxt(d_:f_);
            res_final_.opt= res_final_.opt(d_:f_);
            
            [failed,res]= t.show_performance(ip_,1,0); % FIXME (ip,1,0)
            b= my_buffer(); b=b.get(); b=b(2:2:end);b= [b{:}];set([b{:}],'visible','off');

            t.show_main_indicators(res,res_final_);
             b= my_buffer(); b=b.get(); b=b(2:2:end);b= [b{:}];set([b{:}],'visible','off');

            % ce graphe la ne doit pas couper les titres (au mieux les news)
             %if k==1 % FULL ip et une seule fois
             % base_split est utilis? par all_rankings
                 [D,b,base_split,failed]= contexte_learning(ip_,0,nan);
                 base_split.value= base_split.num;
             %end

            %latex('section', fn, 'Aggregated rankings of contexts')
            
%             [R,N]= t.show_average_ranks(base,1) ;
%             b= my_buffer(); b=b.get(); b=b(2:2:end);b= [b{:}];set([b{:}],'visible','off');
            SR= [];
            [SR.Y,SR.ctxt,SR.stock,SR.RK_,SR.inh]= t.show_ranks(base_split,1)  ;
            % [SR.Y,SR.ctxt,SR.stock,SR.RK_,SR.inh]= t.show_ranks(base,1)  ;
            
             b= my_buffer(); b=b.get(); b=b(2:2:end);b= [b{:}];set([b{:}],'visible','off');
            res01= gatherResults(ip_.comp(:)',1,0,@(sec_id) cb_newinfo(ip_,sec_id));
            [X,keys,X0,key0]= t.show_co(res01.alldata,1);
             b= my_buffer(); b=b.get(); b=b(2:2:end);b= [b{:}];set([b{:}],'visible','off');
            [CO.X{end+1},CO.keys{end+1},CO.X0{end+1},CO.key0{end+1}]= t.show_co(res01.alldata,1,1);
            b= my_buffer(); b=b.get(); b=b(2:2:end);b= [b{:}];set([b{:}],'visible','off');
            CO.sec_ids_0{end+1}= [res01.v.IDstock];
        end
    end

%%
    function go=sub_report(subname,title,author,content_fun)
        % Decore une fonction qui remplit le contenu d'un html
        % Rajoute cr?ation du fichier (title,author), lien vers l'index et close
        function go_(varargin)
            nm_= fullfile(self.name,[subname,'.html']);
            LR = learning_report(nm_,[1 0 1]);
            LR.REPORT.graph= get_graph_decorator_fun(LR.REPORT.graph_v0);
            LR.REPORT.begin(title,author);
            LR.REPORT.hlink(getlink('index'),'Back to index');        
            content_fun(LR,varargin{:});
            LR.REPORT.close(); 
        end
        go=@go_;
    end

%%
function glob_perf(LR)
    LR.REPORT.section('Global indicators')
    LR.REPORT.text('This graphic depicts the global quality of the forecast');                           
    LR.REPORT.legend(...
        'Contexts selection: ranked','Number of ranked (and non inhibited) contexts',...
        'Model Quality: CO','Multiscale VWAP slippage (computed on 1 min bins). Relative to naive predictor.',...
        'CO, When selection','When a context is applied (ranked and non inhibited)',...
        'CO, full sample','Anticipative naive median is used in Usual Days',...
        'CO +Usual Day','Uses an optimized (non naive) predictor in Usual Days'...          
    );
    LR.REPORT.graph('main_summary') 
end

function rankings(LR)
    LR.REPORT.section('Contexts prevalence')  
    LR.REPORT.text('This graphic depicts the most important contexts');
    LR.REPORT.graph('avg_ranks')
    LR.REPORT.legend(...
            'Average','mean over stocks of ranking / total number of context',...
            'Best','Best ranking across stocks',...
            'Worst','Worst ranking across stocks',...
            'top 25%','fraction of stock ranking the event in top 25%',...
            'top 50%','fraction of stock ranking the event in top 50%',...
            'Non-selected','Proportion of stocks that do not rank the event',...
            'Stock normalized','Divided by number of ranked events for each stock, rather than total number of events'...
        );
   LR.REPORT.section('Rankings') 
   LR.REPORT.text('This graphic depicts how contexts are ranked for each stock');
   LR.REPORT.graph('ranks')    
end

function full_perf(LR)
    LR.REPORT.section('Performance in contexts')
    LR.REPORT.text('These graphics depict the performance (absolute values) for each stock and each context. ');
    LR.REPORT.subsection('Before inhibition');
    LR.REPORT.graph('CO4before');
    LR.REPORT.subsection('After inhibition.');
    LR.REPORT.graph('CO4after');
    LR.REPORT.legend(...
        'Operationnal Criteria' ,'Multiscale slippage, computed on 1 min bins'...
    );

    LR.REPORT.section('Relative performance in contexts')
    LR.REPORT.text('These graphics depict the performance (relative values) for each stock and each context. ');
    LR.REPORT.text('Values are relative to a naive estimator (full sample median). ');
    LR.REPORT.subsection('Before inhibition');
    LR.REPORT.graph('RCO4before');
    LR.REPORT.subsection('After inhibition');
    LR.REPORT.graph('RCO4after');
    LR.REPORT.legend(...
        'Operationnal Criteria' ,'slippage values are relative to the slippage of the (anticipative) median'...
    );     
end

function volume_perf(LR)
    LR.REPORT.section('Average performance')     
    LR.REPORT.text('These graphics depict the average performance of our estimator for each stock.');
    LR.REPORT.text('Performance is measured by vwap slippage on 1 min bins.');
    LR.REPORT.legend(...
        'P(context switch)','Proportion of dates where a context is selected',...
        'Nb selected context','normalized by total number of events',...
        'Nb inhibited context','normalized by total number of events',...
        'Full sample Error','Multiscale slippage, computed on 1 min bins',...
        'Context switch Error','same, but computed on days where some context is selected',...
        'NB', 'slippage values are relative to the slippage of the (anticipative) median'...
    );
    LR.REPORT.graph('GR_display');

    LR.REPORT.section('Volume forecast only performance')
    LR.REPORT.text('Performance on volume forecast only (Volume forecast errors)');
    LR.REPORT.graph('GR_display_volume');  
end

function warnings(LR,w)
    LR.warning_info(LR.REPORT,w,@list2str_);
end

function z=mydatenum(x)
    if isempty(regexp(x, '^\d\d:\d\d:\d\d|^\d:\d\d:\d\d'))
        z= -inf;
    else
       z= datenum(x) ;
    end
end

%% News perspective
function data= create_news_curves(ip)
    % DONE: sorter par date/colnames sinon xtick plante
    % un pr?dicteur ? Inf. fait planter zoom max dans plot_predictor_news
    vargs= {'type','FINALmodel', 'inhibition', 1};
    data= options(); % event_id -> datas
    data.failed={};
    for i= 1:length(ip.comp)
        id= {ip.comp(i),ip.TD,ip.dates,ip.path_modifier};        
         try                                                  
             [preds,B,R,plotfun,preds_non_inhibe]= generateFullModel(id,vargs{:} );
             assert(all(isfinite(preds(:))));
             assert(all(isfinite(preds_non_inhibe(:))));
         catch me                                                                                                           
             data.failed{end+1}= {ip.comp(i),me} ;                                                                          
             continue                                                                                                       
         end      
        
        [preds,B,R,plotfun,preds_non_inhibe]= generateFullModel(id,vargs{:} );
        
        [P,Q]= size(preds_non_inhibe);
        no_news_idx= find(preds_non_inhibe(:,2)==0);
        assert(length(no_news_idx)==1);
        P0= preds_non_inhibe(no_news_idx,:);
        for p=1:P            
            try;x= data.get(preds_non_inhibe(p,2));
            catch me
                x=[]; x.curves= []; x.rcurves=[]; x.occur=[]; x.news_name= 'init';
                x.sec_id= [];x.inh=[];  x.ranks=[]; x.selected=[];            
            end            
            x.curves(:,end+1)= preds_non_inhibe(p,:);
            rc= preds_non_inhibe(p,:); rc(4:end)= rc(4:end)./P0(4:end);
            x.rcurves(:,end+1)= rc;
            assert(all(isfinite(x.curves(:))));
            %assert(all(isfinite(x.rcurves)));
            % rcurves peut contenir >0/0 => Inf
        
            x.sec_id(end+1)= B.IDstock;   
            x.colnames= R.volume.colnames;
            x.colnames= {'rank','id_news','column',x.colnames{:}};
            x.inh(end+1)=0;
            x.ranks(end+1)= preds_non_inhibe(p,1);
            x.news_id= preds_non_inhibe(p,2);
            if isfield(B,'inhibitedCtxt') & ismember(preds_non_inhibe(p,3),B.inhibitedCtxt);x.inh(end)=1;end            
            if preds_non_inhibe(p,3)==0
                x.occur(end+1)= sum(B.BN==0);
                x.selected(end+1)= sum(B.BN==0);
            else
                x.occur(end+1)= sum(R.news.value(:,preds_non_inhibe(p,3)));
                x.selected(end+1)= sum(B.BN==preds_non_inhibe(p,3));
            end
            n= newsDb(1); x.news_name= n.id2name(x.news_id); x.news_name=x.news_name{1};
            
            dtn= cellfun(@mydatenum,x.colnames,'uni',1);
            [~,idx]= sort(dtn,'ascend');
            x.colnames= x.colnames(idx);
            %x.value= x.value(idx,:);
            x.curves= x.curves(idx,:);
            x.rcurves= x.rcurves(idx,:);
            
            data.set(preds_non_inhibe(p,2),x);            
        end        
    end
end

function fnames= all_news_curves(ip)
    fnames={};
    new_info= ip.news_data.get();
    for n=1:floor(length(new_info)/2)
        %REPORT.hlink(['file:///',fullfile(FOLDER,['stock_' num2str(sec) '.html'])],num2str(sec));       
        fnames{n}= news_curves(new_info{2*(n-1)+2},ip);              
    end           
end
    

function pth= news_curves(data,ip)
    my_buffer('clear');%my_buffer('stack');my_buffer('uni');
    
    TLO= test_learning_output();
    
    LR = learning_report(fullfile(self.name,['news_' num2str(data.news_id) '.html']));        
    LR.REPORT.graph= get_graph_decorator_fun(LR.REPORT.graph_v0);
    
    LR.REPORT.begin(['News_id= ' num2str(data.news_name) ],'N.Mayo');
    LR.REPORT.hlink(getlink('index'),'Back to index');       

    LR.REPORT.section(['News ' data.news_name , ' (' ,num2str(data.news_id) , ')'])     
    info=[];
    info.colnames={'News','id','Ranked','Inhibited','Avg.rank','Avg.rank,non inhibited','max nb of observations'};
    rk= mean(data.ranks);rknh= mean(data.ranks(data.inh==0));
    %info.stats= {data.news_name,num2str(data.news_id),num2str(length(data.sec_id)),sum(data.inh==1),rk,rknh,max(data.occur)};
    info.stats= {data.news_name,num2str(data.news_id),num2str(length(data.sec_id)),num2str(sum(data.inh==1)),rk,rknh,num2str(max(data.occur))};
    
    Z_=[]; Z_.value= info.stats; Z_.col_header=  info.colnames; Z_.format= '%2.3f';
    LR.REPORT.array(Z_);          
    
    persistent_buffer('tableau_news',Z_);
    
    h=ns_figure;
    subplot(2,2,1);
    plot(data.occur',data.selected','b+');xlabel('Occurences');ylabel('Selection');
    zoom_max('xy','handle',gca);
    title('Occurences of the news in learning data');
    subplot(2,2,2)    
    nhr= data.ranks(data.inh==0);
    hr= data.ranks(data.inh==1);
    [n1,xout1]= hist(data.ranks',30);
    [n2,xout2]= hist(nhr',xout1);
    [n3,xout3]= hist(hr',xout1);
    %plot(xout1,n1,'k.',xout2,n2,'b.',xout3,n3,'g.');
    bar(xout1',[n1;n2;n3]');
    title('All rankings');
    xl_= get(gca,'xlim');xl_(1)=0;
    set(gca,'xlim',xl_);
    legend('Ranks','Ranks (non inhibited)','Ranks (inhibited)');
    subplot(2,2,3) 
    idx_news= find([ip.RPC.key0{:}]== data.news_id); assert(length(idx_news)==1);
    [n1,xout1]= hist(ip.RPC.X0(:,idx_news),30);
    [n2,xout2]= hist(ip.RPC.X0(data.inh==0,idx_news),xout1);        
    bar(xout1',[n1;n2]');
    zoom_max('xy','handle',gca);
    %set(gca,'xlim',[0 1]);
    title('Relative cash slippage');
    legend('Non inhibited','After Inhibition');
    subplot(2,2,4) 
    idx_news= find([ip.RPC.keys{:}]== data.news_id); assert(length(idx_news)==1);
    [n1,xout1]= hist(ip.RPC.X(:,idx_news),30);
    [n2,xout2]= hist(ip.RPC.X(data.inh==0,idx_news),xout1);
    bar(xout1',[n1;n2]');
    legend('Non inhibited','After Inhibition');
    title('Cash slippage');
    zoom_max('xy','handle',gca);
    
    my_buffer('occurences',gcf);
    LR.REPORT.text('Number of occurences and Rankings');
    LR.REPORT.graph('occurences');
    
    LR.REPORT.subsection('Typical volume curves');
    LR.REPORT.text('These graphics depicts best ranked stocks (Top 5) for the event');
    LR.REPORT.legend(...
        'Top plot','Volume curve in context',...
        'Bottom plot','Curve, normalized by Usual day curve each bin',...
        'Legend','Stock (rank,performance)');%...     
    
    nm_plot= TLO.plot_predictors_news(ip,data,1);
    LR.REPORT.graph(nm_plot);
    LR.REPORT.text('Best inhibited stocks');
    nm_plot= TLO.plot_predictors_news(ip,data,0);
    LR.REPORT.graph(nm_plot);           
    
    LR.REPORT.subsection('All curves');
    LR.REPORT.plain('Non inhibited<BR>'); %,{'plain',1}); %,true});
     
    names_= get_security_name(data.sec_id);
    if ~iscell(names_);names_={names_};end
    for n=1:length(data.sec_id)
        if data.inh(n)==0
            LR.REPORT.hlink(getlink(data.sec_id(n),ip.TD),[num2str(data.sec_id(n)) ' - ' names_{n}]);       
        end
    end
    
    LR.REPORT.plain('<BR>')
    LR.REPORT.plain('Inhibited<BR>');%,{'plain',1}) %,true);
    for n=1:length(data.sec_id)
        if data.inh(n)==1
            LR.REPORT.hlink(getlink(data.sec_id(n),ip.TD),[num2str(data.sec_id(n)) ' - ' names_{n}]);       
        end
    end
    pth= LR.REPORT.close();
end

%%
function report= get_graph_decorator_nonuni(report)   
    method_= report.graph;
    function graph__(name)
        b=my_buffer();
        set(0, 'CurrentFigure', b.get(name));
        ns_figure(b.get(name));
        set(b.get(name),'renderer','zbuffer');
        method_(b.get(name));%,scf__(1),scf__(2),scf__(3));
    end
    report.graph= @graph__; %@(name) method_(b.get(name),0.75,0,1);
end
function report= get_graph_decorator(report)   
    method_= report.graph;
    function graph__(name)
        b=my_buffer();
        hdl= b.get(name);
        hdl=[hdl{:}];
        for h=hdl
            set(0, 'CurrentFigure', h );
            ns_figure(h);
            set(h,'renderer','zbuffer');
            method_(h);%,scf__(1),scf__(2),scf__(3));
        end
    end
    report.graph= @graph__; %@(name) method_(b.get(name),0.75,0,1);
end
function f= get_graph_decorator_fun(ff)   
    method_= ff;
    function varargout= graph__(name)
        varargout= cell(1,nargout);
        b=my_buffer();
        hdl= b.get(name);
        hdl=[hdl{:}];
        for h=hdl
            set(0, 'CurrentFigure', h );
            ns_figure(h);
            set(h,'renderer', 'painters' );%'zbuffer');
            [varargout{:}]= method_(h,scf__(1),scf__(2),scf__(3));
        end
    end
    f= @graph__; %@(name) method_(b.get(name),0.75,0,1);
end

function s=list2str_(wstruct,l)
    if isempty(l); s= 'None';        
    else
        TD= wstruct.TD;
        tbx= latex_report('blabla');
        s= arrayfun(@(x) tbx.hlink(getlink(x,TD),num2str(x),1) ,l,'uni',0);                
        s= strcat(s);
        s= [s{:}]; %char(s);
    end
end
%%
end
%%
function failed= save_data(ip)
failed= options();failed.info= 'keys are stock.news';
curves= ip.news_data;
co= ip.RPC;
news= curves.get();
news= news(1:2:end);
news= [news{:}];
for s= (ip.comp(:)')
    for n= (news(:)')
        try
            id_n= find([ip.RPC.keys{:}]==n); assert(length(id_n)==1);
            id_s= find(ip.RPC.sec_ids_0==s); assert(length(id_s)==1);
            co_ns= ip.RPC.X(id_s,id_n);
            id_n= find([ip.RPC.key0{:}]==n); assert(length(id_n)==1);
            id_s= find(ip.RPC.sec_ids_0==s); assert(length(id_s)==1);
            rco_ns= ip.RPC.X0(id_s,id_n);
            save_data_1(s,curves.get(n),co_ns,rco_ns,ip.path_modifier);
        catch ME
            kk= [num2str(s),num2str(n)];
            failed.set(kk, ME);
        end
    end    
end

end

function save_data_1(sec_id,news_data,co,rco ,pm)
%if nargin< 5; pm='';end
x=[];
x.colnames= news_data.colnames;
x.news_name= news_data.news_name;
x.news_id= news_data.news_id;
id_s= find(news_data.sec_id==sec_id);
if isempty(id_s); return ;end
assert(length(id_s)==1);
x.value= news_data.curves(:,id_s);
x.relative_profile= news_data.rcurves(:,id_s);
x.occurences= news_data.occur(id_s);
x.selected= news_data.selected(id_s);
x.rank= news_data.ranks(id_s);
x.security_id= news_data.sec_id(id_s);
x.operational_criteria= co;
x.relative_operational_criteria= rco;

tbx= newsProjectTbx();
IO= fileToolbox(tbx.FILEconfig.folder({0 0 0 pm},'report'));
IO.save(x, 'type','syntheticResult', 'news',x.news_id, 'stock',x.security_id);

end

function [info,ip]= create_full_report(comp,TD,dates,rep_name,rep_folder,path_modif)
DEBUG_= 0
ns_figure('init',[]);
ns_figure('init',30);

persistent_buffer('clear');
persistent_buffer('stack');    
info=[];
global st_version;
if ischar(comp)
    ip= indexProcess(comp);
else
    ip= indexProcess('CAC40');
    ip.comp= comp;
    ip.indexname= 'Composition saisie manuelle';
end
%if nargin<5; rep_folder= st_version.my_env.root_html_path;end
if nargin<5 | isempty(rep_folder);
    rep_folder= fullfile(st_version.my_env.root_html_path,'Contextualized Volume Curves','learning_reports',rep_name);
end

if nargin<6; path_modif= '';end
ip.path_modifier = path_modif;
ip.TD= TD;
ip.dates= dates ;%[733043,734319];

% TIPS: changement temporaire (attention si erreur) de var.globale?
% TODO: pourquoi faire ???

% keep_= st_version.my_env.root_html_path;
% st_version.my_env.root_html_path= rep_folder;

LR = learning_report_v2(rep_folder); %rep_name);
ip.RPC =[]; %relative perf in contexts
[w,CO,SR,globstats]= LR.make_glob_graph(ip); % TODO, ?purer globstats.alldata et .v
w.TD= ip.TD;
LR.glob_perf();
LR.full_perf();
LR.rankings()
LR.volume_perf()
LR.warnings(w);
% alignement des infos CO
oo=cellflat(cellfun(@(x,y) uni_options(x,y) , CO.X0,CO.key0,'uni',0 ));
[CO.X0,CO.key0]= uni_options(oo);
oo=cellflat(cellfun(@(x,y) uni_options(x,y) , CO.X,CO.keys,'uni',0 ));
[CO.X,CO.keys]= uni_options(oo);
CO.sec_ids_0= [CO.sec_ids_0{:}];
% 2/ stock by stock
ip.RPC= CO;
ip.SR=SR;
ip.glob_perf= globstats;
info.stock_errors= LR.go_all_stocks(ip,ip.RPC); %,@(x) 2);
% 3/ news by news -> D 
ip.news_data= LR.create_news_curves(ip);
if DEBUG_==1
    ndatasave= ip.news_data; % ip.news_data=ndatasave;
    keys= ndatasave.get();keys=keys(1:2:end);keys= sort([keys{:}],'ascend');
    smaller= options();
    for k=1:min(2,length(keys)); smaller.set(keys(k),ndatasave.get(keys(k)));end
    for k=1:length(keys);
        if keys(k) == 88973;
            smaller.set(keys(k),ndatasave.get(keys(k)));end
    end
    ip.news_data= smaller;
end

%  keys= ip.news_data.get();keys=keys(1:2:end);
%  for n_=1:26%length(keys)
%      ip.news_data.remove(keys{n_});
%  end
LR.all_news_curves(ip);
% data=D.get(10);
% TLO= test_learning_output();
% TLO.plot_predictors_news(ip,data)

info.report_path= LR.root_doc(rep_folder,ip);

info.save_data_failed= LR.save_data(ip);

%st_version.my_env.root_html_path= keep_;

end


%%
function EXEMPLE()
ip= indexProcess('CAC40'); %('S&P500 STOCK INDEX');
ip.comp= ip.comp(1:10);
%ip.comp=[2 8 11];
ip.TD= {}; {'main'};
ip.dates= [733043,734319];
ip.path_modifier='';

st_version.my_env.root_html_path= 'C:\report';
% en une etape
LR = learning_report_v2('test');
LR.create_full_report(ip)

%% plusieurs
% 1/ global infos -> warnings, perfs
RPC =[]; %relative perf in contexts
[w,CO,SR]= LR.make_glob_graph(ip);
LR.glob_perf();
LR.full_perf();
LR.rankings()
LR.volume_perf()
LR.warnings(w);
% alignement des infos CO
oo=cellflat(cellfun(@(x,y) uni_options(x,y) , CO.X0,CO.key0,'uni',0 ));
[CO.X0,CO.key0]= uni_options(oo);
oo=cellflat(cellfun(@(x,y) uni_options(x,y) , CO.X,CO.keys,'uni',0 ));
[CO.X,CO.keys]= uni_options(oo);
CO.sec_ids_0= [CO.sec_ids_0{:}];
% 2/ stock by stock
ip.RPC= CO;
ip.SR=SR;
LR.go_all_stocks(ip,RPC); %,@(x) 2);
% 3/ news by news -> D 
ip.news_data= LR.create_news_curves(ip);
ndatasave= ip.news_data; % ip.news_data=ndatasave;
 keys= ip.news_data.get();keys=keys(1:2:end);
 for n_=1:17%length(keys)
     ip.news_data.remove(keys{n_});
 end
LR.all_news_curves(ip);
% data=D.get(10);
% TLO= test_learning_output();
% TLO.plot_predictors_news(ip,data)

LR.root_doc(ip);

LR.save_data(ip);
end

function TEST_full()
    dates= [datenum('05/03/2009','dd/mm/yyyy'), datenum('12/03/2009','dd/mm/yyyy')]

    R= learning_report_v2('',[1 1 1],40);
    R.create_full_report('CAC40',{},[733043,734319],'rapport_de_demo','C:\\demo_folder');
    
    R.create_full_report([8 11],{},[733043,734319],'rapport_de_demo','C:\\demo_folder');
    
    R.create_full_report(8318,{},[733774,734888],'test',st_version.my_env.root_html_path)
    R= learning_report_v2('',[1 1 1],40);R.create_full_report('S&P500 STOCK INDEX',{},[733774,734888],'SP_report',st_version.my_env.root_html_path)
    %,'rapport_de_demo','C:\\demo_folder');
    
    R= learning_report_v2('',[1 1 1],40);[info,ip]= R.create_full_report([2],{},[datenum('01/01/2008','dd/mm/yyyy'), datenum('01/01/2009','dd/mm/yyyy')],'rapport_de_demo_test','C:\\demo_folder','ttest');
    
    % nouvelle syntaxe pour fichier local ???.
    R= learning_report_v2('C:\\',[1 1 1],40);[info,ip]= R.create_full_report([6069],{},[734153,734168],'rapport_de_demo_teste','C:\\demo_folder');
    R= learning_report_v2('C:\\',[1 1 1],40);[info,ip]= R.create_full_report([6069],{},[734153,734168],'rapport_de_demo_teste','C:\\demo_folder');
end

function find_line_number()
profile on;
YourFunct(a,b,c);
profile off;
 p=profile('info'); 
%missingSC= findNonSuppressedOutput( p, 'C:\st_work\bin\se\context_volume') %\test_learning_output.m' )
missingSC= findNonSuppressedOutput( p, 'C:\st_work\bin\libs\funcs') %\test_learning_output.m' )
msc= missingSC.get();
k=12
%for k=2:2:length(msc)
    v_ =msc{k}.get();
    k_= v_(1:2:end);v_= v_(2:2:end);
    [~,idx]= sort([k_{:}]);
    for kk=1:length(idx)
        disp(v_{idx(kk)})
    end    
%end


end
