function [info,ip]=learning_report_wrapper(name, varargin)
% learning_report_wrapper - Wraps learning_report_v2
%
% 
%
% Examples:
%
%
% indexes = {'OBX', 'CAC40',  'BEL20', 'IBEX35','AEX', 'SWISS LEADER PR INDEX', 'DAX', 'FTSE 100', 'FTSE MIB INDEX','OMX COPENHAGEN 20', 'OMX HELSINKI 25','OMX STOCKHOLM 30'}
% % , 'S&P500 STOCK INDEX'
% from_to = [datenum(2009,1,1,0,0,0), datenum(2012,8,31,0,0,0)];
% for i = 1 : length(indexes)
%     [failed, base] = learning_report_wrapper([indexes{i} 'all_TD' '_' datestr(from_to(1), 'yyyymmdd') '_' datestr(from_to(2), 'yyyymmdd')], ...
%         'index',indexes{i},'TD', {}, ...
%         'dates', from_to,...
%         'scf',[1 0 1]);
% end
%
%   learning_report_wrapper('DAX_allTD','index', 'DAX','TD',{},'dates',[datenum(2009,1,1,0,0,0),datenum(2012,1,20,0,0,0)],'scf',[1 0 1])
%   learning_report_wrapper('CAC40','index', 'DAX','TD','MAIN','dates',[datenum(2007,1,1,0,0,0),datenum(2012,1,20,0,0,0)],'scf',[1 0 1])
%   learning_report_wrapper('CAC40','index', 'DAX','TD','MAIN','dates',[datenum(2007,1,1,0,0,0),datenum(2012,1,20,0,0,0)],'scf',[1 0 1])
%   
%   learning_report_wrapper('SandP500','index','S&P500 STOCK INDEX','TD',{},'dates',[733774,734888],'scf',[1 0 1])
%   learning_report_wrapper('filename','index',[2 8 11],'TD',4,'dates',[733774 734888])
%   
%   NB :scf parameter <=> [scale, close_figure, fullscreen] for graphics
% See also:
%    
%
%   author   : 'nmayo@cheuvreux.com'
%   reviewer : ''
%   date     :  '25/01/2012'
%   
%   last_checkin_info : $Header: learning_report_wrapper.m: Revision: 8: Author: robur: Date: 09/10/2012 04:26:27 PM$

global st_version;
%opt = options({'scf',[1 1 1],'split_graph',40,'report_folder',st_version.my_env.root_html_path,'version','v2'},varargin);
opt = options({'scf',[1 1 1],'split_graph',40,'version','v2','report_folder',''},varargin);
% il ne faut pas pr?ciser l'argument root_html_path
% create_full_report() va utiliser soit
% [D] root_html_path\\Contextualized Volume Curves\\learning_reports\\report_name\\index.html]
% soit l'argument transmis


info=nan;ip=nan;
    assert(strcmpi(opt.get('version'),'v2'),'learning_report v1 is deprecated, use v2 instead');
% selection de la composition. Une seul TD ? la fois !
    idx= opt.get('index');
    if ischar(idx)
        ip= indexProcess(idx); 
    else
        ip= indexProcess('CAC40'); % on recupere un indexProcess pipo dont on change la comp
        ip.comp= idx;
    end    
    ip.TD= opt.get('TD');
    %ip.destinationID= opt.get('TD');
    ip.dates= opt.get('dates');
    
    % en une etape
    if strcmpi(opt.get('version'),'v2')
        R= learning_report_v2('',[1 1 1],40);
        [info,ip]= R.create_full_report(opt.get('index'),opt.get('TD'),opt.get('dates'),name,opt.get('report_folder'));

    else
        assert(0,'deprecated');
        LR = learning_report(name,opt.get('scf'));
        LR.full_report(ip);
    end

end