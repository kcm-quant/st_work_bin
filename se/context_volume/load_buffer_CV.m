function x= load_buffer_CV(mode,sec_id,TD,date,varargin)
% data= load_buffer_CV(mode,sec_id,TD,date, varargin )
% loads buffer from contexte_volume_estimator
% 
% varargin <=> what buffer to select
% Wil fail if 2 or more files match the varargin
% 
% Exemple:
%     x= load_buffer_CV('nplearn',6589,[],[733043,734895],'type','FINALmodel')
%     x= load_buffer_CV('news',6589,[],[733043,734895],'type','news')

tbx= newsProjectTbx();
%f= tbx.FILEconfig.folder({sec_id,TD,date},'nplearn')
IO= tbx.FILE(tbx.FILEconfig.folder({sec_id,TD,date},mode));
x= IO.loadSingle(varargin{:});
x=x{1}.savedVariable;