function s= model2str(M)
% M est un FINALmodel avec .M (no news) et .J (jacknife results)
%
% Correspond � un dictionnaire, stock� sous la forme python string ?
% les delim ([;]et[:]) n'ont pas le droit d'apparaitre dans les key/value !
%
% attention les nonewsmodel contiennent par exemple @(x,i)tbx.m3(x,i,[0.5])

delimKV= ':';
delimItem= ';';

if isstruct(M)
    % JackPred parameters
    l1= M.J.opt.get();
    l2={'nnfh',func2str(M.M{1}),'nnp1',M.M{2},'nnp2',M.M{3},'nnp3',M.M{4}};
    l={l1{:},l2{:}};
    
    l= cellfun(@(x) num2str(x),l,'uni',0);
    s= [];
    N= length(l);
    n=1;
    while n<= N
       s= [s delimItem l{n} delimKV l{n+1} ] ;
       n=n+2;        
    end
    s= s(2:end);
    % No news parameters
    
    
    
    
elseif ischar(M)
    % JackPred parameters
    key2num= {'Npoint','sbreak','dim','seuil','nnp1','nnp2','nnp3'};    
    % M est un string
    l=  tokenize( M , delimItem) ;
    N= length(l);
    o= options();
    for n=1:N
        KV= l{n};
        kvl= tokenize(KV,delimKV);
        assert(length(kvl)==2);  
        if ismember(kvl(1),key2num)
            kvl{2}= str2num(kvl{2});
        end
        o.set(kvl{1},kvl{2})                
    end
    
    fh= o.get('nnfh');
    fh= str2func(fh);
    o.set('nnfh',fh);
    
    s=o;        
end



%% 
function TEST()

load('C:\st_repository\news_project\NPlearn\stock_id_18\fileSave~type-FINALmodel~inhibition-0~nonews-1~.mat')
s= model2str(savedVariable);
s= model2str(s);s.get()



