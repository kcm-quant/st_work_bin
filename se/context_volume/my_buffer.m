function data = my_buffer(mode, value)
% UNTITLED2 - temporary keeps persistent data
% Examples:
%   my_buffer('clear');
%     my_buffer('champ',2)
%     my_buffer('champ2',3)
%     assert(my_buffer('champ')==2)
%     my_buffer('champ2',4) % FAILS !!!
% 
%     my_buffer('clear');
%     my_buffer('stack');
%     my_buffer('champ2',3)
%     my_buffer('champ2',4)
%     x= my_buffer('champ2')
%     my_buffer('uni')
%     x= my_buffer('champ2')
%
% 
%
% See also:
%    
%
%   author   : 'nmayo@cheuvreux.com'
%   reviewer : ''
%   date     :  '23/11/2011'
%   
%   last_checkin_info : $Header: my_buffer.m: Revision: 3: Author: namay: Date: 08/01/2012 06:42:43 PM$



            
persistent opt;
if isempty(opt)
    opt= options();
end
persistent stack;
if isempty(stack)
    stack= 0;
end
persistent uni;
if isempty(uni)
    uni= 0;
end

if nargin==0
    data= opt;
    return
end
%%
if ischar(mode) && strcmpi(mode,'clear')
    assert(nargin==1);    
    opt= options();
    return
end
if ischar(mode) && strcmpi(mode,'stack')
    assert(nargin==1);    
    stack= 1;
    return
end
if ischar(mode) && strcmpi(mode,'unstack')
    assert(nargin==1);    
    stack= 0;
    return
end
if ischar(mode) && strcmpi(mode,'uni')
    assert(nargin==1);    
    uni= 1;
    return
end
%%

if nargin==1
    data= opt.get(mode);    
    if uni==1
        data= [data{:}];
        data= data(:)';
    end
    return
end

inside= 1;
v={};
try
    v= opt.get(mode);
catch
    inside=0;
end


if stack==0
    assert(inside==0);
    opt.set(mode,value);
elseif stack==1
    if ~iscell(v);  v={v};end
    v{end+1}= value;        
    opt.set(mode,v);
else; assert(0);
end


end

function TEST()

my_buffer('clear');
my_buffer('champ',2)
my_buffer('champ2',3)
assert(my_buffer('champ')==2)
my_buffer('champ2',4) % FAILS !!!

my_buffer('clear');
my_buffer('stack');
my_buffer('champ2',3)
my_buffer('champ2',4)
x= my_buffer('champ2')
my_buffer('uni')
x= my_buffer('champ2')

end
