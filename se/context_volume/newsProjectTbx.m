function tbx= newsProjectTbx(mainFolder)
% Toolbox for context_volume_learning
%
% Global variables for eModel
% On ne maintient plus la version avec un argument (incompatible avec mode path_modifier)
global st_version

% mode= 'pipo';
mode= '';

specFile= 0;
if nargin==1; specFile=1;end

%Y:\context_l_repository\news_project_CAC40_20111213

news_project___= 'news_project'; %'news_project_CAC40_20111213'; % 'news_project_CAC40_20111213';% 'news_project'; % FIXME 'news_project'
NPlearn___= 'NPlearn'; % FIXME 'NPlearn'
if nargin < 1;     
    mainFolder = fullfile(st_version.my_env.st_repository, news_project___, mode, NPlearn___);    
else
    WWW=1;
end



s= which('newsProjectTbx');
%[pathstr, name, ext, versn] = fileparts(s);
%[pathstr, name, ext] = fileparts(s); % Warning: The fourth output, VERSN, of FILEPARTS will be removed in a future release. 
%path([pathstr '\utils'],path)
%path([pathstr '\utils\moutlier1'],path)

tbx= [];

tbx.mainFolder= mainFolder;

tbx.acm= [];
if ~isempty(which('ACMultiple'))    
    tbx.acm= ACMultiple();
end
tbx.st = stTools();
tbx.mdate= @(x) datenum(x,'dd/mm/yyyy');

tbx.dict=[];
if ~isempty(which('dico'))    
    tbx.dict= dico();
end

tbx.LE= loadEvent();

tbx.LDE=[];
if ~isempty(which('loadDirtyEvent'))    
    tbx.LDE= loadDirtyEvent();
end
tbx.LV= VLoader('tbx');

%tbx.ES= eventSelector2();
%tbx.ERC= errorComputer('noargs_toolbox');

tbx.eModel= eModel2('tbx');
%tbx.BT= backtestTbx();
%tbx.explo= exploTbx();

%tbx.ER= eventRanking();

tbx.ERC= errorComputer('tbx');
% ? handle car l'argument construisant la tb est important

tbx.FILE= @fileToolbox; % handle car l'argument construisant la tb est important
tbx.getFromFileTrace= @getFromFileTrace;


%*TRAIN
% - learningNews.mat
% * stock_id_ID
%    - learningData.mat
%    * eModel
%      - news / meanfun /.mat
%    * 
%
%

    function x= treat_buggy_decorate(x)
        if isstruct(x) & isfield(x,'savedVariable') & isfield(x.savedVariable,'filetrace')
            x.savedVariable.filetrace= options(x.savedVariable.filetrace.get());
        end
    end

    function x= getFromFileTrace(ft,key,sID)
        pth= ft.get(key);
        %assert(~strcmpi(pth(1:2),'C:'));
        [pathstr, name, ext] = fileparts(pth);
        pth= [name ext];
        f= folder_(sID, 'stock');
        x= load(fullfile(f,pth));
        x= treat_buggy_decorate(x);
        
        
%         if strcmpi(pth(1:2),'C:') || strcmpi(pth(1:2),'Y:')
%             % correction plutot que raise
%             [pathstr, name, ext] = fileparts(pth);
%             pth= [name ext];
%             f= folder_(sID, 'stock');
%             x= load(fullfile(f,pth));
%             x= treat_buggy_decorate(x);
%             
%             %assert(0);
%             %f= pth;
%             %x= load(f);
%         else        
%             f= folder_(sID, 'stock');
%             x= load(fullfile(f,pth));
%             x= treat_buggy_decorate(x);
%         end
%         
        
        
    end

    

    function f= folder_(IDstock, type)          
        if isstruct(IDstock)
            [IDstock,TD,dates_,path_modifier]= struct2info(IDstock); 
            if isempty(path_modifier)
               WWW=1; 
            end
        elseif iscell(IDstock)
            if length(IDstock)>3
                path_modifier=IDstock{4};
            else
                WWW = 1;
            end
            dates_= IDstock{3}; TD= IDstock{2}; IDstock= IDstock{1};            
        else; assert(false);
            
        end
            
		mainfold= mainFolder;%['C:\' mode 'NPlearn\'];
        fs = filesep();
        if mainfold(end) ~= fs; mainfold = [mainfold fs];end
        f= mainfold;
        f=fullfile(f,path_modifier); % NEW
        
        f = fullfile(st_version.my_env.st_repository, news_project___,path_modifier, mode, NPlearn___);  
        
        if f(end) ~= fs; f = [f fs];end
		if strcmpi(type,'main'); 1;	
        elseif strcmpi(type,'report')
            f= fullfile(st_version.my_env.st_repository, news_project___,path_modifier, mode, 'synthetic_result' );
		else
			f= [f 'stock_id_' num2str(IDstock) '_' num2str(TD) '_' num2str(dates_(1)) '_' num2str(dates_(2)) '_' fs];
			if strcmpi(type,'stock'); 0;
            elseif strcmpi(type,'Buffer')
                if specFile
                    f= [f];
                else
                    f= fullfile(st_version.my_env.st_repository, news_project___,path_modifier, mode, ['Buffers' '_' num2str(TD) '_' num2str(dates_(1)) '_' num2str(dates_(2))] );
                end
            elseif strcmpi(type,'News')
                if specFile
                    f= [f];
                else
                    f= fullfile(st_version.my_env.st_repository, news_project___,path_modifier, mode, ['Event_' num2str(dates_(1)) '_' num2str(dates_(2))] );
                end
			end
				
			
		end
	end



	function f= folder_OV(IDstock, type)        
		mainfold= mainFolder;%['C:\' mode 'NPlearn\'];
        fs = filesep();
        if mainfold(end) ~= fs
            mainfold = [mainfold fs];
        end
        f= mainfold;
		if strcmpi(type,'main'); 1;			
		else
			f= [f 'stock_id_' num2str(IDstock) fs];
			if strcmpi(type,'stock'); 0;
			elseif strcmpi(type,'rank')
				f= [f 'eModels' fs];
			elseif strcmpi(type,'jacknife')
				f= [f 'errComputer' fs];
            elseif strcmpi(type,'adaptative')
				f= [f 'adaptative' fs];
% 			elseif strcmpi(type,'allData')
% 				f= ['C:\' mode 'newsProjectAllData' fs 'saveAll' num2str(IDstock) '.mat']; % 
			elseif strcmpi(type,'opRatio')
                f= [f 'opratio.mat'];
            elseif strcmpi(type,'Buffer')
                if specFile
                    f= [f];
                else
                    f= fullfile(st_version.my_env.st_repository, 'news_project', mode, 'Buffers');
                end
			end
				
			
		end
	end
FILEconfig= [];
FILEconfig.folder= @folder_;
tbx.FILEconfig= FILEconfig;



% 	function f= folder_OV(IDstock, type)
% 		mainfold= mainFolder;%['C:\' mode 'NPlearn\'];
% 		f= mainfold;
% 		if strcmpi(type,'main'); 1;			
% 		else
% 			f= [f 'stock_id_' num2str(IDstock) '\'];
% 			if strcmpi(type,'stock'); 0;
% 			elseif strcmpi(type,'rank')
% 				f= [f 'eModels\'];
% 			elseif strcmpi(type,'jacknife')
% 				f= [f 'errComputer\'];
%             elseif strcmpi(type,'adaptative')
% 				f= [f 'adaptative\'];
% 			elseif strcmpi(type,'allData')
% 				f= ['C:\' mode 'newsProjectAllData\saveAll' num2str(IDstock) '.mat'];
% 			elseif strcmpi(type,'opRatio')
% 				f= [f 'opratio.mat'];
%             elseif strcmpi(type,'Buffer')
%                 if specFile
%                     f= [f];
%                 else
%                     f= ['C:\Buffers\'];
%                 end
% 			end
% 				
% 			
% 		end
% 	end

end


function [sid,std_,dts_,path_modifier]= struct2info(x)
    path_modifier=[];
    if isfield(x,'path_modifier')
        path_modifier= x.path_modifier;
    end

    l= {'secID','IDstock','stockID'};
    sid= nan;
    for n=1:length(l)
        if isfield(x,l{n})
            new_= getfield(x,l{n});
            if ~isnan(sid)
               assert(new_==sid ) ;
            end
            sid= new_;
        end
    end
    if isnan(sid); assert(0);end
    
    l= {'TD','trading_destination_id','destinationID'};
    std_= nan;
    for n=1:length(l)
        if isfield(x,l{n})
            new_= getfield(x,l{n});
            if iscell(std_)
                assert(isempty(std_));
                std_=[];
            end
            if ~isnan(std_)
               assert(new_==std_ ) ;               
            end
            std_= new_;
        end
    end
    if iscell(std_) & isempty(std_)
        std_=[];
    end    
    if isnan(std_)
        std_= x.volume.destinationID;        
    end
    if isempty(std_); std_= [];end
    if isnan(std_); assert(0);end
    
    l= {'original_date'};
    dts_= nan;
    for n=1:length(l)
        if isfield(x,l{n})
            new_= getfield(x,l{n});
            if ~isnan(dts_)
               assert(dts_==new_ ) ;               
            end
            dts_= new_;
        end
    end
    if isnan(dts_)
        dts_= x.volume.original_date;
    end
    
    if isnan(dts_); assert(0);end
    
    
end
        
        
    


