function data= oracle_analysis(ID)
% ID= {16060,10,[733774,734993],''}; %{sec_ids,TD,dates}
% data= oracle_analysis({get_index_comp('CAC40'),[],[733774,734993],''});

tbx= newsProjectTbx();
data=[];
data.value=[];
data.date= [];
data.failed= options();

N= length(ID{1});
for n=1:N
    idtemp= ID; idtemp{1}= ID{1}(n);   
    try
        IO= tbx.FILE(tbx.FILEconfig.folder(idtemp,'stock'));
        n1= IO.loadSingle('type','FINALmodel','inhibition',1,'nonews',1);
        mopt=n1{1}.savedVariable;
    catch ME
        data.failed.set(idtemp{1},ME);
        continue
    end
    try
        ora= IO.loadSingle('type','oracle','inhibition',1);
        ora= ora{1}.savedVariable;
    catch ME
        data.failed.set(idtemp{1},ME);
        continue
    end        
    %try
    assert(all(strcmp(mopt.ER.colnames,ora.colnames)));
%     catch ME
%         data.failed.set(idtemp{1},ME);
%         continue
%     end
    
    i= find(strcmp( 'cash 1mn',mopt.ER.colnames));
    j= find(strcmp( 'cash 1mn',ora.colnames));
    data.value(n) = mean(ora.value(:,i)) / mean(mopt.ER.value(:,j));
    data.date(n) = idtemp{1};
end