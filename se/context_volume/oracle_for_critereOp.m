function er= oracle_for_critereOp(security_id,destinationID,dates,path_modifier,freq)

%% creates btest struct from real volumes (at learning freq)
if nargin==1
    % 1 argin <=> self.learning_options
    freq= security_id.get('learning_frequency');
    path_modifier= security_id.get('folder');
    dates= security_id.get('dates');
    destinationID=  security_id.get('trading_destination');
    security_id= security_id.get('security_id');    
end

% FIXME: learning_frequency dans learning_options ne sert a rien, c'est
% eModelPool qui d?termine ca !
% PATCH
tbx_emp= eModelPool();x=tbx_emp.definePaveur('intraday');x=options(x{:});
all_freq= x.get('learning_frequency'); all_freq= [all_freq{:}];    
freq=all_freq(1); % ne fait qu'une des fr?quences !

tbx= newsProjectTbx();
IO= tbx.FILE(tbx.FILEconfig.folder({security_id,destinationID,dates,path_modifier},'Buffer'));

buf= IO.loadSingle('type','buffer','stockID',security_id,'freq','learning','f',freq);
data= buf{1}.savedVariable;
try; data=data{1};end
data.value= bsxfun(@rdivide,data.value,nansum(data.value,2));


if isfield(data,'destinationID')
   if isempty(destinationID)
       assert(isempty(data.destinationID))
   elseif ischar(destinationID)
       assert(strcmpi(destinationID,'main'));
   elseif destinationID~=-1
        assert(data.destinationID == destinationID);
   end
end
data.pred= data.value;
data.volume= data;

IO= tbx.FILE(tbx.FILEconfig.folder({security_id,destinationID,dates,path_modifier},'News'));  
news= IO.loadSingle('type','news','stockID',num2str(security_id) );
news=news{1};
try;news=news.savedVariable;
catch;news= news.news;
end
tbx= stTools();
[dd,news]= tbx.intersect(data,news);
assert(all(news.date== data.date));
data.news= news;

data= rmfield(data,'value');
assert(data.info.security_id== security_id)
data.stockID= security_id;
data.path_modifier= path_modifier;
data.BN= zeros(size(data.pred,1),1);

opt_= {'noter','None','learning_frequency',freq,'sbreak', 0,'Npoint',10,'seuil',NaN,...
    'estimateur','median','oracle',1}; %'dim',36,
data.opt= options(opt_);
%% Calls critereOpComputer
% proxy for errorComputer.apply()
% IO= tbx.FILE(tbx.FILEconfig.folder(self,'stock'));
% [data,fname]= IO.loadSingle(varargin{:});        
% data= data{1}.savedVariable;
% FiletraceSet(data,'errorLoad',fname); %data.filetrace.set('errorLoad',fname);
er= compute(data.pred-data.volume.value);   % Oracle=>0 sur du 15 minute, inutile !     
% opt= options(varargin);
% if opt.get('dim')== 1
%     mn1=[]; mn1.filename = 'daily -> not used';
%     mn5=[]; mn5.filename = 'daily -> not used';
% else            
mn1= critereOpComputer(data,data.info.security_id);         
er.cash1= mn1.pognon;                       
er.value(:,end+1)= abs(mn1.pognon);
er.value(:,end+1)= abs(mn1.pognon).^2;
er.value(:,end+1)= mn1.ri;
er.value(:,end+1)= mn1.illiquid;
er.colnames{end+1}= 'cash 1mn';
er.colnames{end+1}= 'cash^2 1mn';
er.colnames{end+1}= 'ri 1mn';
er.colnames{end+1}= 'illiquid 1mn';    
%end
if isfield(data.volume,'destinationID')
   er.destinationID= data.volume.destinationID;
end
%er.filetrace= data.filetrace;
er.date= data.volume.date;
er.NAMEstock= data.stockID;
er.BN= data.BN;
er.IDstock= data.stockID;
er.opt= data.opt;
er.news= data.news;
er.volume_rereparti = mn1.volume_rereparti;
er.TD= data.destinationID;
er.original_date= data.volume.original_date;
%FiletraceSet(er,'fullDataLoadMn1',mn1.filename); %er.filetrace.set('fullDataLoadMn1',mn1.filename);        

%% SAVE
tbx= newsProjectTbx();
IO= tbx.FILE(tbx.FILEconfig.folder(data,'stock'));
pars= options(data.opt.get());
pars.set('type','oracle');
pars.set('inhibition',0);
pars= pars.get();
fname= IO.save(er,pars{:});  

pars= options(data.opt.get())
pars.set('type','oracle');
pars.set('inhibition',1); % SAVE inhib et non inhib
pars= pars.get();
fname= IO.save(er,pars{:}); 
%FiletraceSet(er,'errorSave',fname); %er.filetrace.set('errorSave',fname);
%IO.save(er,pars{:});   


function er= compute(ecart)
    % doublon from critereOpComputer
    er= [];
    er.value= [];
    er.value(:,1)= sqrt(sum(ecart.^2,2));
    er.value(:,2)= max(abs(ecart),[],2);
    er.value(:,3)= max(abs(cumsum(ecart,2)),[],2);        
    er.colnames= {'L^2','L^0','KS'};

function DEMO()
    comp= get_index_comp('DAX', 'recent_date_constraint', false);
    dts=[733774,734993];
    for c=(comp(:)')        
        oracle_for_critereOp(c,[],dts,'',15);
    end
    
    failed={};
    comp= get_index_comp('OBX', 'recent_date_constraint', false);
    dts=[733774,735106]; % comp= comp(comp>18360)
    for c=(comp(:)')        
        try
            oracle_for_critereOp(c,{},dts,'',10);
        catch ME
            failed{end+1}= c;
            failed{end+1}= ME;
        end
    end
    
    
    

