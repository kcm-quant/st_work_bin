function data = persistent_buffer(mode, value)
% UNTITLED2 - temporary keeps persistent data
%
%
%
% Examples:
%
% 
% 
%
% See also:
%    
%
%   author   : 'nmayo@cheuvreux.com'
%   reviewer : ''
%   date     :  '23/11/2011'
%   
%   last_checkin_info : $Header: persistent_buffer.m: Revision: 1: Author: namay: Date: 04/24/2012 12:54:55 PM$

           
persistent opt;
if isempty(opt)
    opt= options();
end
persistent stack;
if isempty(stack)
    stack= 0;
end
persistent uni;
if isempty(uni)
    uni= 0;
end

if nargin==0
    data= opt;
    return
end
%%
if ischar(mode) && strcmpi(mode,'clear')
    assert(nargin==1);    
    opt= options();
    return
end
if ischar(mode) && strcmpi(mode,'stack')
    assert(nargin==1);    
    stack= 1;
    return
end
if ischar(mode) && strcmpi(mode,'unstack')
    assert(nargin==1);    
    stack= 0;
    return
end
if ischar(mode) && strcmpi(mode,'uni')
    assert(nargin==1);    
    uni= 1;
    return
end
%%

if nargin==1
    data= opt.get(mode);    
    if uni==1
        data= [data{:}];
        data= data(:)';
    end
    return
end

inside= 1;
v={};
try
    v= opt.get(mode);
catch
    inside=0;
end


if stack==0
    assert(inside==0);
    opt.set(mode,value);
elseif stack==1
    if ~iscell(v);  v={v};end
    v{end+1}= value;        
    opt.set(mode,v);
else; assert(0);
end


end

function TEST()
% On aimerait un premier niveau d'indexation (et faire varier les modes selon
% ce niveau), afin d'utiliser la m�me fonction diff�rement en m�me temps

% mode sans stack: chaque clef a un seul �l�ment, non r�assignable
persistent_buffer('clear');
persistent_buffer('champ',2)
persistent_buffer('champ2',3)
assert(persistent_buffer('champ')==2)
persistent_buffer('champ2',4) % FAILS !!!

% mode stack: chaque clef a un cell array, augm�nt� lors de la r�assignation
persistent_buffer('clear');
persistent_buffer('stack');
persistent_buffer('champ2',3)
persistent_buffer('champ2',4)
x= persistent_buffer('champ2')

% mode uni: le cell array est uniforme
persistent_buffer('uni')
x= persistent_buffer('champ2')

end
