function tbx= prevNoNews(data,varargin)
% Pr�vision lorsqu'aucun contexte n'est s�lectionn�
% On peut pr�dire n'importe quelle journ�e, m�me avec BN>0
% L'historique utilise BN pour filtrer les donn�es
%
%

% - R�cup�rer le Jack du bestModel
% - for pred in NNmodels: pred -> jack when bn=0
% - ErrorComputer : TODO, factoriser loadfile ???
% - pick best
%
%
%

if nargin==0
    tbx=[];
    tbx.m3= @threemean;
    tbx.ewma= @ewma;
    tbx.D_removeBN= @D_removeBN;
    tbx.backtest=@backtest;
    tbx.computeErr= @computeErr;
    tbx.makeJstruct= @makeJstruct;
    tbx.fsError= @fsError;
    tbx.allModels=@allModels;
    tbx.runAll=@runAll;
    tbx.getBestNNpred= @getBestNNpred;
    tbx.main= @main;
    tbx.main_from_file=@main_from_file;
    tbx.predict= @predict;
    return
end

model= options({'algo','EWMA','Nupdate',10,'past',60,'NminObs',40});


    function [M,ER,J,res]= main_from_file(BM)
        % BM: type-bestMODEL
        tbx= newsProjectTbx() ;
        if BM.best.filetrace.contains_key('jackSave')
            j= tbx.getFromFileTrace(BM.best.filetrace,'jackSave',BM.best.IDstock);
        else
            % mod�le NoNews
            assert(isnan(BM.best.opt.get('seuil')));
            j= tbx.getFromFileTrace(BM.best.filetrace,'jack0Save',BM.best.IDstock);
        end
        j= j.savedVariable;
        tbx__= prevNoNews();
        [M,ER,J,res]= tbx__.getBestNNpred(j);
    end


    function x= main(EMP,inhibition)
        stockID= EMP.stockID(:)';
        % stockID= list is accepted
        if nargin==1; inhibition=0;end
        filedes= {'type','BESTmodel','inhibition',inhibition};
        tbx= newsProjectTbx() ;
        for sID= stockID
            IO= tbx.FILE(tbx.FILEconfig.folder(EMP,'stock'));
            BM= IO.loadSingle(filedes{:});
            BM= BM{1}.savedVariable;
            if ~isfield(BM.best,'inhibitedCtxt')
                BM.best.inhibitedCtxt= []; % cas best model non contextuel !
                BM.best.inhibition_ratio = [];
            end
            if inhibition;assert(~ any(ismember(BM.best.BN,BM.best.inhibitedCtxt)));end
            x=[];
            % BM: type-bestMODEL
            tbx= newsProjectTbx() ;
            isM0= 0;
            if BM.best.filetrace.contains_key('jackSave')
                j= tbx.getFromFileTrace(BM.best.filetrace,'jackSave',BM.best);
                j0= tbx.getFromFileTrace(BM.best.filetrace,'jack0Load',BM.best);
            else
                % mod�le NoNews
                assert(isnan(BM.best.opt.get('seuil')));
                j= tbx.getFromFileTrace(BM.best.filetrace,'jack0Save',BM.best);
                j0= j;
                isM0=1;
            end
            j= j.savedVariable;
            j0= j0.savedVariable;
            j.inhibition_ratio =BM.best.inhibition_ratio;
            % Ici, j et BM n'ont pas le mm BN !!!
            % normal car BM a �t� inhib�, pas j
            % => il faut inhiber le J
            if inhibition
                assert(isfield(BM.best,'inhibitedCtxt'));
                j= inhiberCtxt_j(j, j0,isM0, BM.best.inhibitedCtxt) ;
            end
            tbx__= prevNoNews();
            [x.M,x.ER,x.J,x.res]= tbx__.getBestNNpred(j);
            filedes= {'type','FINALmodel','inhibition',inhibition,'nonews',1};
            IO.save(x,filedes{:});
        end
    end



% function prev= pred_fun(datas,bn,fun)
%     Nobs= sum(bn==0);
%     prev= fun(data(bn==0,:));
% end

%%
end
%%

%% Predictors
% BASIC
function m= threemean(x,idx,q)
if nargin==2; q=[.25 .5 .75];end
m= quantile(x(idx==0,:),q,1);
m= mean(m,1);
m= m/sum(m);
end

function m= ewma(x,idx,lambda)
w= ewma_w(size(x,1),lambda);
w(idx~=0,:) =0;
w= w/sum(w);
wx= bsxfun(@times,w,x);
m= sum(wx,1);
end

function w= ewma_w(N,lambda)
assert(lambda<=1);
w= lambda.^(0:N-1);
w= w(end:-1:1);
w= w/sum(w);
w=w(:);
end

% USED  PREDICTORS
function c= plusieursNdays(f)
% rajoute 4eme position 0/1 <=> use BN or not ? (only for idx learn, not validation !)
%Ndays=[30,20;50,30;100,50;500,250];
Ndays=[30,20;50,30;100,50;200,100];
c={};
[I,J]= size(Ndays);
for i=1:I;
    c{end+1}= {f,Ndays(i,1),Ndays(i,2),1};
    c{end+1}= {f,Ndays(i,1),Ndays(i,2),0};
end
end

function c= allModels()
tbx= prevNoNews();
c1= plusieursNdays(@(x,i) mean(x(i==0,:),1));
% c={c1{:}};
% return;
c2= plusieursNdays(@(x,i)tbx.m3(x,i,[0.5]));
c={c2{:}};
return;
c3= plusieursNdays(@(x,i)tbx.m3(x,i,[.25 .5 .75]));
c4= plusieursNdays(@(x,i) tbx.ewma(x,i,0.9));
c6= plusieursNdays(@(x,i) tbx.ewma(x,i,0.95));
%c={c1{:},c2{:},c3{:},c4{:},c5{:},c6{:}};
c={c2{:},c3{:},c4{:},c6{:}};
end

%% decoration
% function g=D_removeBN(f,bn,nHisto,fractionMin)
%     %TODO: retirer les news dans l'ordre
%     function value= deco_(x,bn)
%         os= size(x,1);
%         x(bn(idx)==0,:)=[];
%         assert(size(x,1)/os> fractionMin);
%         value= f(x);
%     end
%     g= @deco_;
% end
%% BACKTEST
function predNN= predict(data,bn,fun,minJ,minNN,useBN)
% Used only for real life forecast
if useBN == 0; bn= zeros(size(bn));end
startIdx = find(cumsum(bn==0)>=minNN, 1);
if isempty(startIdx)
    startIdx = 0;
end
[T,N]= size(data);
histo = data;
predNN = fun(histo,bn);
end


function [pred,startIdx]= backtest(data,bn,fun,minJ,minNN,useBN)
if useBN== 0; bn= zeros(size(bn));end
% passer l'historique buffer qui est plus long pour perdre moins d'obs -> non car on a pas BN dessus
startIdx= find(cumsum(bn==0)>=minNN);
if isempty(startIdx);startIdx= 0;end
startIdx= startIdx(1)+1;
startIdx= max(startIdx,minJ+1);

[T,N]= size(data);
predNN=[];
for t=startIdx:T
    past= 1:(t-1);
    histo= data(past,:);
    predNN(end+1,:)= fun(histo,bn(past));
end
pred= nan(T,N);
if ~isempty(predNN)
    pred(startIdx:end,:)= predNN;
end
end

function [res,ers,allJ]= runAll(data)
tbx= prevNoNews();
goPred= @(f) tbx.backtest(data.volume.value,data.BN,f{:});
c= tbx.allModels();
N= length(c);
res=nan(N,4);
ers={};
allJ={};
for n=1:N
    cc=c{n};
    [pred,startIdx]= goPred(cc);
    J= tbx.makeJstruct(data,pred,startIdx);
    allJ{end+1}=J;
    er= tbx.computeErr(J);
    c___= abs(corr(er.BN,data.BN)-1);
    if isnan(c___); assert(all(all([er.BN,data.BN]))==0);
    else;assert(c___< 00000.1);end
    c___= abs(corr(J.BN,data.BN)-1);
    if isnan(c___); assert(all(all([J.BN,data.BN]))==0);
    else;assert(c___< 00000.1);end
    
    [res(n,1),res(n,2),res(n,3),res(n,4),res(n,5)]= tbx.fsError(er);
    ers{end+1}= er;
end
end

function [M,ER,J,res]= getBestNNpred(data)
tbx= prevNoNews();
[res,ers,allJ]= tbx.runAll(data);
[m,i]= sort(res(:,1),'ascend');
[m__,i__]= sort(res(:,2),'ascend');

%assert(all(i(1)==i__(1)));
% pkoi le max marche pas ?

c= tbx.allModels();
M= c{i(1)};
ER= ers{i(1)};
J= allJ{i(1)};
end


%% Compute errors, structure misc.
function jackIni= makeJstruct(jackIni,pred,startIdx)
[T,N]= size(jackIni.pred);
idxApply= find((jackIni.BN== 0) & (~any(isnan(pred),2)));
jackIni.pred(idxApply,:)= pred(idxApply,:);
[T_,N_]= size(jackIni.pred);
assert(T==T_);
assert(N==N_);
jackIni.BACKTEST_startIdx= startIdx;
end

function er= computeErr(J)
% attention aux premi�res valeurs -> supprimer ?
[T,N]= size(J.pred);
erc= errorComputer();
erc.stockID= J.IDstock;
er=erc.applyWithoutFileSave(J);
end

function [fsE,nnE,nApply,nobs,totalE]= fsError(ER)
critere= 'cash 1mn';
critere= find(strcmpi(critere,ER.colnames));
assert(length(critere)== 1);
[T,Q]= size(ER.value);
idx= ER.BACKTEST_startIdx:T; idx= idx';
fsE= mean(ER.value(idx,critere));
nobs= length(idx);

idx1= intersect(idx,find(ER.BN== 0));
nnE= mean(ER.value(idx1,critere));
nApply= length(idx1);

totalE= mean(ER.value(idx,critere));

end



%% TESt
function TEST_()
x= rand(1000,10);
tbx= prevNoNews();

m= tbx.m3(x)
m= tbx.ewma(x,0.9)
end

function Example()
[18,71,232]
tbx= prevNoNews();
%load('C:\st_repository\news_project\NPlearn\stock_id_2\fileSave~type-BESTmodel~inhibition-0~.mat')
load('C:\st_repository\news_project\NPlearn\stock_id_232\fileSave~type-BESTmodel~inhibition-0~.mat')
BM= savedVariable;
j= BM.best.filetrace.get('jackSave');
j= load(j);
j= j.savedVariable;
j.BACKTEST_startIdx= 1;
% local path !
% load('C:\st_repository\news_project\saveExample\stock_id_18\fileSave~type-jack~Npoint-10~noter-pv~dim-36~estimateur-median~seuil-0.05~sbreak-0~.mat')
% j= savedVariable;

assert(all(j.BN==BM.best.BN))
cmpOpt= cellfun(@(x,y) strcmpi(num2str(x),num2str(y)) , j.opt.get(),BM.best.opt.get());
assert(all(cmpOpt));

goPred= @(f,n,n0) tbx.backtest(j.volume.value,j.BN,f,n,n0);

[pred,startIdx]= goPred(@(x,i) tbx.ewma(x,i,0.9),60,30)
[pred,startIdx]= goPred(@tbx.m3,60,30)
[pred,startIdx]= goPred(@(x,i) mean(x(i==0,:),1),60,60)
[pred,startIdx]= goPred(@(x,i) tbx.m3(x,i,[0.5]),20,10) % m�diane

jj= tbx.makeJstruct(j,pred,startIdx);


er0= tbx.computeErr(j);
[fsE0,nn0,nApply0,nobs0]= tbx.fsError(er0)
% Le NoNews initial est mieux mais n'est pas connu sous la filtration
% naturelle ! Il faut de tt facon trouver un pr�dicteur adapt� !

er= tbx.computeErr(jj);
[fsE,nnE,nApply,nobs]= tbx.fsError(er)


[res,ers]= tbx.runAll(j)
res(:,1)./ fsE0
res(:,2)./ nn0
%
N1=[];N0=[];
for n=1:16
    N1(n)= norm(ers{n}.value(ers{n}.BN>0) - er0.value(er0.BN>0));
    N0(n)= norm(ers{n}.value(ers{n}.BN==0) - er0.value(er0.BN==0));
end
N0


[M,ER]= tbx.getBestNNpred(j)

%
unique(sum(pred,2))
plot(jj.volume.date,jj.pred-j.pred)
plot(er.date,er.value(:,4))

end

%%
function test_main()
tbx= prevNoNews();
x=tbx.main(2);
end




