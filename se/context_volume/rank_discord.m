function r= rank_discord(x,y)

if nargin==1    
    y=x;
    if iscell(x)        
        P=length(x);
        r=nan(P,P);
        for p=1:P
            for q= 1:P
                r(p,q)= rank_discord_2variables(x{p},y{q});
            end
        end
        return 
    end    
end

[Nx,Px]= size(x);
[Ny,Py]= size(y);
r=nan(Px,Py);
for p=1:1Px
    for q= 1:Py
        r(p,q)= rank_discord_2variables(x(:,p),y(:,q));
    end
end

function r= rank_discord_2variables(x,y)
Nx= length(x);
Ny= length(y);
u= setdiff(x,y);
v= setdiff(y,x);
w= unique([x(:);y(:)]);

r= [];
for ww=(w(:))'
    f=find(x==ww);
    if isempty(f); f= Nx;end
    r(ww,1)= f;
    f=find(y==ww);
    if isempty(f); f= Ny;end
    r(ww,2)= f;    
end

assert(all(r(r(:,1)==0,2)==0));
r(r(:,1)==0,:)=[];
r= sum(abs(diff(r,1,2)));

