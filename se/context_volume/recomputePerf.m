function perf= recomputePerf(x)
% x.best-> errorComputer
% x.M0 -> errorComputer, Mod�le de r�f�rence NoNews anticipatif

assert(all(strcmpi(x.best.colnames,x.M0.colnames)));

% fs: full sample
% no: news selected only
% nn : nonews selected

perf=[];
perf.prob= mean(x.best.BN>0);
perf.NtotalCtxt= length(x.best.news.colnames);
perf.nctxt= length(unique(x.best.BN))-1;
perf.fractCtxt= perf.nctxt/perf.NtotalCtxt;
% AS :absolute
perf.AS= [];
perf.AS.prob= mean(x.best.BN>0);
perf.AS.fs= mean(x.best.value(:,:),1);
perf.AS.no= mean(x.best.value(x.best.BN>0,:),1);
perf.AS.nn= mean(x.best.value(x.best.BN==0,:),1);
perf.AS.NtotalCtxt= length(x.best.news.colnames);
perf.AS.nctxt= length(unique(x.best.BN))-1;
perf.AS.fractCtxt= perf.nctxt/perf.NtotalCtxt;
perf.AS.colnames= x.best.colnames;
% RS :relative
perf.RS= [];
perf.RS.fs= mean(x.best.value(:,:),1) ./ mean(x.M0.value(:,:),1);
perf.RS.no= mean(x.best.value(x.best.BN>0,:),1) ./ mean(x.M0.value(x.best.BN>0,:),1);
perf.RS.nn= mean(x.best.value(x.best.BN==0,:),1) ./mean(x.M0.value(x.best.BN==0,:),1);
perf.RS.NtotalCtxt= length(x.best.news.colnames);
perf.RS.nctxt= length(unique(x.best.BN))-1;
perf.RS.fractCtxt= perf.nctxt/perf.NtotalCtxt;
perf.RS.prob= mean(x.best.BN>0);
perf.RS.colnames= x.best.colnames;

if isfield(x.best,'inhibitedCtxt')
    perf.RS.inhib= length(x.best.inhibitedCtxt);
end