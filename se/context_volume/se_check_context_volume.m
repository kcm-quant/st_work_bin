function [pv,p_]= se_check_context_volume(security_id, trading_destination_id, window_type, window_width, as_of_date,pred)

[from, to] = se_window_spec2from_to(window_type, window_width, as_of_date);

%% Get Current Data
% Current event
tb= indexProcess();
falseself=[];falseself.comp= security_id;
news= tb.bufferNews(falseself,[from,to],0);
% db access
dbIO= contexte_volume_dbIO;
ZZ= dbIO.get(security_id,trading_destination_id);
freq_learning= ZZ.frequency;
ZZ.num= ZZ.value;
ZZ.model= ZZ.no_news_string;
[B,R]= base2data(ZZ,security_id,news,3);

% current Volume
new_freq= 5; 
CV_tbx= se_run_context_volume('toolbox');
vol= CV_tbx.getCurrentData(security_id,from,to,new_freq,trading_destination_id,freq_learning);
% synchronisation
stt= stTools();
c= stt.intersect({vol,news}); vol= c{1}; news= c{2};
assert(all(abs(sum(pred.value,2)-1)<0.0001));

%%
used= zeros(size(news.value,1),1);
vol_residu= vol.value;
for col_id = 1:length(R.ranks)
    % A/ ---- Contexte matching ------------------------------
    id_col= R.ranks(col_id);    
    id_news= find(R.news.col_id(id_col) == news.col_id); % num de la colonne dans new data e
    idn_pred= find(strcmpi(num2str(R.news.col_id(id_col)),pred.colnames)); %num.col dans pred
    assert(length(id_news)==1);  
    % B/ ---- Observations in contexts ------------------------
    idx= news.value(:,id_news)== 1;
    idx= idx & used==0;
    % C/ ---- Compute predictor at learning frequency ---------
    p= pred.value(:,idn_pred);
    pp= align_pred(p,pred.rownames,vol.phases,vol.frequency/pred.frequency);
    
    vol_residu= bsxfun(@plus,vol_residu(idx,:),-pp');
             
end

[h,p_]= ttest(vol_residu);
pv= min(p_);
sum(cumsum(sort(p_))<0.05)
end

function pp= align_pred(p,rn,real_phases,r)
pp= nan(size(real_phases));
%r= real_freq/pred.frequency;
assert(mod(r,1)==0);

j= cellfun(@(x) strcmpi(x(1:5),'slice')&~isempty(regexp(x(6), '[1-9]'))&~strcmpi(x(6), '_'),rn);

continuous= p(j);
T= length(continuous);
T= T/r; assert(mod(T,1)==0);
K_= kron(eye(T,T),ones(1,r));
conti= K_*continuous;
i_conti= real_phases==0;

pp(i_conti)= conti;

%pp= nan(size(p));
i= find(real_phases== 2); assert(length(i)==1);
j= find(strcmpi('slice_open',rn));assert(length(j)==1);
pp(i)= p(j);

i= find(real_phases== 5); assert(length(i)==1);
j= find(strcmpi('slice_close',rn));assert(length(j)==1);
pp(i)= p(j);

i= find(real_phases== 4);
j= find(strcmpi('slice_mid',rn));assert(length(j)==1);
if ~isempty(i)
    assert(length(i)==1);
    pp(i)= p(j);
end
assert(abs(sum(pp)-1)< 0.0001);
assert(all(~isnan(pp)));

end

function dim_check()
% A faire lors de l'update
% d1= st_data('check', data) REQUIRES AND CHECK DATES ���
d0= ~all(isfield(pred, {'title', 'value', 'rownames', 'colnames','info'}));
d1= length(pred.rownames)~= size(pred.value,1);
d2= length(pred.colnames)~= size(pred.value,2);
d3=length(pred.colnames)<=0;
d4=length(pred.rownames)<=0;

ipred= find(cellfun(@(x) strcmpi('slice',x(1:5)),pred.rownames));
auctions= find(cellfun(@(x) strcmpi('slice_',x(1:5)),pred.rownames));
ID= setdiff(ipred,auctions);

b1= ~any(any(isnan(pred.value(ID,:))));
b2= all(abs(nansum(pred.value(ipred,:),1)-1)<0.000001);
b3= min(min(pred.value(ipred,:)))<=0;


if any([b1,b2,b3,d0,d1,d2,d3,d4])
    disp('check fails');
    disp([b1,b2,b3,d0,d1,d2,d3,d4]);
end

end