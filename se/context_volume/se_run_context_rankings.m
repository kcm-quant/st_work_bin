function Z= se_run_context_rankings(security_id, trading_destination_id, window_type, window_width, as_of_date,dt_remove,varargin_)

mode_test= 1;
% DONE: trading_destination_id
% DONE: learning frequency doit etre dans model_table pour etre recupere en update()
if mode_test==0
    %% Init
    [from, to] = se_window_spec2from_to(window_type, window_width, as_of_date);
    V= vwap_order_weights();
    V.clear()
    %% Create Buffers
    ip= indexProcess('CAC40',trading_destination_id);
    ip.comp= [security_id];
    dts= [];
    format= 'dd-mmm-yyyy';
    dts(1)= from; %datenum('03-Jan-2009',format);
    dts(2)= to; %datenum('01-May-2011',format);
    % Volumes
    st_log('$off$')
    ip.bufferAll(ip,dts,trading_destination_id);
    % News/ events
    df= [] ;
    df(1)= from; %;datenum('03-Jan-2009',format);
    df(2)= to;%datenum('01-May-2011',format);
    ip.bufferNews(ip,df)

    %% LEARNING
    %ip=[];ip.comp= sec_id;
    indexProcess1by1(ip,0);
    %indexProcess1by1(ip,1);

    % Check
    %res= gatherResults(ip.comp(:)',ip.useInhibition,ip.final_);
    %res.display(res)
end    
    %% Rank table
    [x,preds,allR]= createFullRankTable(security_id,0);
    P= size(x,1); % nb contextes
    
    Z= [];
    Z.value= x;
    Z.colnames={'security_id','context','rankings'};
    
    R= allR{1};
    n= R.opt.get('noter')
    if strcmpi(n,'std')
        run_quality= 1-R.note(1:P);
    else
        run_quality= R.note(1:P);
    end
    Z.info= [];
    Z.info.run_quality=run_quality;
    Z.no_news_string= R.no_news_string;
    Z.security_id= security_id;
    Z.trading_destination_id= trading_destination_id;
% ecrire rankings dans [GANDALF market..cal_event_type, COLONNE rank]
%%
function TEST()


Z= se_run_context_rankings(2,4,'day',1000, '01/01/2010',[])
