function aligned= se_run_context_volume(security_id, trading_destination_id, window_type, window_width, as_of_date,dt_remove,varargin)
% Updates contextual volume forecast. Uses rankings + recent market data to compute static volume predictor
% in each context
%
% Parameters:
%  * slice_duration : length of continuous slices in minutes
%  * slice1,...sliceN : volume forecast in continuous trading slices
%  * auction_open, auction_mid, auction_close : volume forecast for auctions
%
% Output: predictor st_data structure, with
%  value : relative volume predictor, 5min extrapolation, contexts= columns
%
% Exemples :
%
% se_run_context_volume(45595, [], 'day', 1155, '22/03/2012')
%
% FIXME:utiliser base.learning_frequency

% < MAGIC NUMBERS
new_freq= 5;
% >

if nargin==1 && strcmpi(security_id,'toolbox')
    aligned=[];
    aligned.getCurrentData= @getCurrentData;
    return
end

[no_use,phase_id,no_use,from_phase_id_to_phase_name] = trading_time_interpret([],[]);
j=nan;
if iscell(trading_destination_id)
    if isempty(trading_destination_id); trading_destination_id=[];
    else
        assert(length(trading_destination_id)==1);
        trading_destination_id= trading_destination_id{1};
    end
end
if ischar(trading_destination_id)
    if strcmpi(trading_destination_id,'all')
        trading_destination_id=[];
    elseif strcmpi(trading_destination_id,'main')
        x_=get_repository('tdinfo',security_id);
        trading_destination_id= x_(1).trading_destination_id;
    else assert(0);
    end
end


% side_effect_file('start_',security_id, trading_destination_id);



% global base
% DONE par dbIO.get(): checks sec_if is in learning !
% TODO: check qu'on est dans le CAC (que l'id a ?t? learn?)

%assert(all([base{:,1}]== security_id));

log_={};

% post learning procedure: uses learning rankings with new data (volume/event)
% to recompute predictors
%
% plan g?n?ral
% * learning, lanc? ? la main
% * update predicteurs (this function), automatis? depuis SE, stocke les pr?dicteurs pour
%   chaque news
% * predictor. Utilise les pred stock?s en base, les news arrivant au jour
%   j, et les horaires de cotation de ce jour (cas du DAX!!! ). Du coup
%   est-ce vraiment ici que l'on dispatch 1 min ?

[from, to] = se_window_spec2from_to(window_type, window_width, as_of_date);



sec_id= security_id;
% DFORMAT= 'dd/mm/yyyy';
% from= datenum('01/01/2011',DFORMAT)
% to= datenum('08/01/2011',DFORMAT)
% [x,preds]= createFullRankTable(sec_id,0);

%% learning model and data
% TOD0 : recuperer depuis les ranks, contextes et predicteur depuis la base
% on a perdu les run quality li?es au pv de learning
%
%tbx= newsProjectTbx();
%filedes= {'type','FINALmodel','inhibition',0,'nonews',0};
%tbx= generateFullModel();

%% TODO: acceder ? la version string en base de ce modele
%[B,R]= tbx.getBasicModel(sec_id,'type','FINALmodel','inhibition',0); % learning data


%% Get Current Data
% db access
dbIO= contexte_volume_dbIO;
ZZ= dbIO.get(security_id,trading_destination_id);
ZZ.num= ZZ.value;
ZZ.model= ZZ.no_news_string;
% Current event
tb= indexProcess();
falseself=[];falseself.comp= sec_id;
news= tb.bufferNews(falseself,[from,to],0,0);
% HOOK: s?parer Usual Day ici surement !
[B,R,news]= base2data(ZZ,sec_id,news,3); % TODO :  pourquoi 3?
% current Volume
%% DONE:
% getCurrentData(5min) sert ? juste ? reaggr?g?er pour ?viter mm frequences
% renvoie du 15 min en dur !!!
freq_learning= ZZ.frequency; % TODO : puisque l'on peut optimiser suivant la frequence lors de l'apprentissage, il faudra mettre la frequence optimale en base et l'tuliser ici


DO_DEBUG_= false;
if DO_DEBUG_;
    try
        vol= my_buffer('se_run_cv_data');
        warning('Using last persistent data ');
    catch
        vol= getCurrentData(sec_id,from,to,new_freq,trading_destination_id,freq_learning);
        my_buffer('se_run_cv_data',vol);
    end
else
    vol= getCurrentData(sec_id,from,to,new_freq,trading_destination_id,freq_learning);
end
st_data('check',vol);


idx_fixing1 = strcmpi({vol.info.phase_th.phase_name},'ID_FIXING1');
if any(idx_fixing1)
    assert(vol.value(:,vol.phases==vol.info.phase_th(idx_fixing1).phase_id)<0.00001,...
        'There traded volume on a fixing of type1')
end

%-- checks number of obs
if size(vol.value,1) < 40
    if abs(diff(vol.original_date)) > 100
        error('Weird Volume data ! Low number of available observations');
    end
    if mean(vol.idx_with_conti) < 0.2
        error('Weird Volume data ! Too many weird days');
    end
    error('Weird Volume data !');
end



% TODO: remove
% test si horaire market out
% vol.begin_.value(2)= (9+5/60) /24;
% vol.end_.value(end-1)= (17.5-10/60) /24;

% synchronisation
stt= stTools();
c= stt.intersect({vol,news}); vol= c{1}; news= c{2};

% Grille 5minute du dernier jour

last_jour = floor(vol.date(end));
last_day = getCurrentData(sec_id,last_jour,last_jour,new_freq,trading_destination_id,new_freq);
last_day.date =  datenum(last_day.colnames,'HH:MM:SS'); %t1; % est ce vraiment les bins clock ?
last_day.colnames = {'volume'};
last_day.value = last_day.value(:);
%% Model parameters
%P= length(R.news.colnames); CHANGED
P= size(ZZ.value,1);

T= size(vol.value,2);
[est,seuil,Npoint,fpv]= get_parameters(B.opt,R);
est= modify_estimator(est);
%ranks= r.ranks(r.note<= seuil);
%* assert(all(R.note(1:length(R.ranks))<= seuil));


% side_effect_file('running_',security_id, trading_destination_id);
%% BN
% on a juste besoin de 0/1, mais il faut parcourir les rank?s seulement
BN = double(any(news.value(:,R.ranks),2));
if isempty(R.ranks)
    real_BN= inf(size(news.value,1),1); %(T,1);
else
    real_BN = news.value(:,R.ranks);
end

%[~,real_BN]= min(real_BN,[],2);
real_BN= bsxfun(@times,real_BN,1:size(real_BN,2));
real_BN(real_BN==0)= inf;
real_BN= min(real_BN,[],2);
real_BN(isinf(real_BN))= 0;
real_BN(ismember(real_BN,find(R.inhibited)))= 0;

% real_BN = news.value(:,R.ranks);
% [~,real_BN]= min(real_BN,[],2);
%% no news model
%p0= est(vol.value);
%nn= model2str(base{1,4});
nn= model2str(B.model_str);
tbx= prevNoNews();
f= nn.get('nnfh');
f= func2str(f);
eval(['g=' f]);
pred= @(x,i) g(x, i); %num2str(nn.get('nnp1'))
pars= {(nn.get('nnp1')),(nn.get('nnp2')),(nn.get('nnp3'))};
p = @(data,bn) tbx.predict(data,bn,pred,pars{:});
[prop,idx] = prop4context(vol.value(BN==0,:));
p0 = nan(1,size(vol.value,2));
p0(idx) = p(prop(:,idx),BN(BN==0)); % Usual Day, learning frequency !
assert(abs(nansum(p0)-1)< 0.00001);


%% init
old_freq= vol.frequency;
nph= sum(vol.phases>0);
new_bin_number= (size(vol.value,2) - nph) *old_freq/new_freq +4; % pas +nph, sinon perd bin

assert( mod(new_bin_number,1)==0 );

info=[];
info.sec_id= sec_id;
info.trading_dest_id= trading_destination_id;
info.run_quality=[];

pred= nan(new_bin_number,P);
pred_freq_l= [];
used= zeros(size(news.value,1),1); % indices utilis?s pour un contexte pr?c?dent

quality_update= [];
quality_ref= [];
the_med = nanmedian(vol.value,1);
the_med = bsxfun(@times,the_med,1./sum(the_med));
quality_done= [];
%% Context matching and predictors computation
% col_id: indice de parcours de ranks: i.e ranks 1<2<...
% id_col: num?ro de l'event dans les colonnes de R.news.value
% id_news: num?ro de l'event dans les colonnes de news.value

noteur= R.opt.get('noter');
if strcmpi(noteur,'None')
    assert(isempty(R.ranks));
else
    noter_fun_= R.rankers.get(noteur);
end
%suppr_idx_ref=nan;
%occurences_= [];
non_aligned_preds = cell(length(R.ranks)+1,1);
for col_id = 1:length(R.ranks)
    [non_aligned_preds{col_id},used]= compute_1pred(col_id,R,news,vol,p0,used,Npoint,old_freq,new_freq,last_day,the_med,est,noter_fun_,fpv,phase_id.ID_FIXING1);
end
% Traitement de no news predictor
%occurences_(end+1,:)= [sum(used==0),1]; % Pour NoNews
% ce n'est pas NoNews que l'on doit mettre, c'est Usual Days!
[predNN,usedNN]= compute_1pred(0,R,news,vol,p0,used,Npoint,old_freq,new_freq,last_day,the_med,est,nan,fpv,phase_id.ID_FIXING1);
non_aligned_preds{end}= predNN;
aligned= align_predictors(non_aligned_preds);
aligned.NAP_=non_aligned_preds;

assert(all(abs(nansum(aligned.value)-1)< 0.000001 ));

myphaseInfo = options({phase_id.OPEN_FIXING,{'auction_open','auction_open_closing'},...
    phase_id.ID_FIXING1,{'auction_mid1','auction_mid1_closing'},...
    phase_id.ID_FIXING2,{'auction_mid','auction_mid_closing'},...
    phase_id.CLOSE_FIXING,{'auction_close','auction_close_closing'}});

%% ------------- out_struct ---------------------------------
[T,C] = size(aligned.value);
% ============== Infos
aligned.title= 'predictors of se_run_context_volume';
aligned.frequency= new_freq;
aligned.learning_frequency= old_freq;
% ============= Rownames (continuing)
Tconti= aligned.phase==phase_id.CONTINUOUS;
assert(issorted(aligned.date(aligned.phase==0)))
% les conti sont pas forc?ment en premiers ? si car sorted avec mais bon, les phase_id peuvent changer!
rown= cell(T,1);
rown(Tconti)= arrayfun(@(x) sprintf('slice%03d',x),1:sum(Tconti),'uni',0);
Tnconti = find(~Tconti);

% ============== phases times
timer_info = struct('colnames',{cell(1,length(Tnconti))},'value',{nan(1,length(Tnconti))});
for i = 1:length(Tnconti)
    labels = myphaseInfo.get(aligned.phase(Tnconti(i)));
    rown{Tnconti(i)}= labels{1};
    timer_info.colnames{i}= labels{2};
    timer_info.value(i)= aligned.date(Tnconti(i));
end
aligned.rownames= rown;
assert(length(aligned.rownames)== size(aligned.value,1));

aligned.rownames= [aligned.rownames; timer_info.colnames'];
aligned.value = [aligned.value ; repmat(timer_info.value',1,C)];
assert(length(aligned.rownames)== size(aligned.value,1));
aligned.value(end+1,:)= min(aligned.date(Tconti)); %new_freq;
aligned.rownames = [aligned.rownames;{'end_of_first_slice'}];

% =============== enveloppe
[alpha_,beta_]=add_envelope(aligned.value_LF,vol,real_BN);
aligned.value(end+1,:)= alpha_;
aligned.value(end+1,:)= beta_;
aligned.rownames{end+1}= 'alpha';
aligned.rownames{end+1}= 'beta';
assert(length(aligned.rownames)== size(aligned.value,1));
% ============== colnames
% attention, colnames = context_name et non pas event_type
CN=R.news.col_id(R.ranks); CN(end+1)= 0;
aligned.colnames = reshape(arrayfun(@event_id_2_context_name, CN,'uni',false),1,[]);
assert(length(aligned.colnames)== length(unique(aligned.colnames)));
% =============== quality and other info
aligned.value(end+1,:)= aligned.occurences_(1,:);
aligned.value(end+1,:)= aligned.occurences_(2,:);
aligned.rownames{end+1}= 'nb_occurence';
aligned.rownames{end+1}= 'had_enough_occurence';
assert(length(aligned.rownames)== size(aligned.value,1));
update_info=[];
update_info.vol_= vol;
update_info.news_= news;
update_info.R= R;
update_info.B= B;
update_info.log= log_;
update_info.pred_freq_l= aligned.value_LF;
update_info.quality_update= quality_update;
update_info.quality_ref= quality_ref;
aligned.update_info= update_info;

% ========= Global Quality Indicator
quality_indicator= 0;
N= length(aligned.log_);
for n=1:N
    col= find(aligned.log_{n}.ctxt_id == ZZ.value(:,2));
    if aligned.log_{n}.ctxt_id==0
        error('se_run_context_volume:not enough data', 'Maybe this stock shouldnt be in learning tables !');
    end
    assert(length(col)==1);
    if ZZ.inhibited(col)==0
        r= aligned.log_{n}.rank;
        quality_indicator= quality_indicator + 1/r^2;
    end
end
quality_indicator= quality_indicator /pi^2 *6;
assert(quality_indicator<=1);
aligned.quality_indicator= quality_indicator;
name= where_to_put_global_quality();
i= find(strcmpi(name,aligned.colnames));
assert(length(i)==1);
aligned.info.run_quality(i)= aligned.quality_indicator;

aligned.info.run_quality= 1- aligned.info.run_quality; % 0 = mauvais, 1 = perfect

aligned.call_args= {security_id, trading_destination_id, window_type, window_width, as_of_date};
assert(aligned.update_info.vol_.info.security_id == security_id);
aligned.log_= cellfun(@(x)x ,aligned.log_); % uniformises log_,

td_= trading_destination_id;
if isempty(td_); td_='''ALL'''; else;td_=num2str(td_);end
aligned.title = sprintf('se_run_context_volume(%d,%s,''%s'',%d,''%s'') ',...
    security_id, td_, window_type, window_width, as_of_date);
aligned.select_bins = ...
    @(cn) cellfun(@(x) (length(x)>=5 &&strcmp(x(1:5),'slice')) || ((length(x)>=8 && strcmp(x(1:8),'auction_')) && ~strcmp(x(end-7:end),'_closing')) ,cn);

colnames = aligned.colnames;
aligned = repmat({aligned},1,length(colnames));
aligned = cellfun(@(dt,cn)st_data('keep',dt,cn),aligned,colnames,'uni',false);
for d = 1:length(aligned)
    aligned{d}.info.run_quality = aligned{d}.info.run_quality(d);
end

% On retire les param�tres qui sont estim�s �gaux � NaN
aligned = remove_nan_param(aligned);


function [pred1,used]= compute_1pred(col_id,R,news,vol,p0,used,Npoint,old_freq,new_freq,last_day,the_med,est,noter_fun_,fpv,phase_id_ID_FIXING1)
% use news not R.news (real news, not learning)
% careful for usualDay: il faut extrapoler p0, et non pas NoNews !
%
% FIXME: les contextes inhib?s en run doivent orthogonaliser (ceux en
% learning aussi!)
%
% A/ ---- Contexte matching ------------------------------
pred1= [];
pred1.info={};
pred1.log_={};
pred1.col_id= col_id; % numero de la colonne dans Ranks
pred1.occurences_= nan(2,1);
assert(~isnan(col_id)) % NoNews doit utiliser 0!
if col_id==0;
    pred1.id_col= 0;
    pred1.id_news= 0;
    idx_= ones(size(vol.value,1),1)==1; % en dernier, c'est pas grave de tout use!
else
    id_col= R.ranks(col_id); % R.ranks(?)-> indice de colonne dans news
    pred1.id_col=id_col;
    
    %id_news= find(R.news.col_id(id_col) == news.col_id); % num de la colonne dans new data e
    % CHECK:peur d'une erreur ici
    %id_news= find(news.col_id(id_col) == news.col_id); % num de la colonne dans new data e
    id_news= news.col_id(id_col);
    pred1.id_news= id_news;
    assert(length(id_news)==1);
    % B/ ---- Observations in contexts ------------------------
    idx_= news.value(:,id_col)== 1; % indices effectifs de la news (orthog.)
    idx_= idx_ & used==0;
end
% C/ ---- Compute predictor at learning frequency ---------
pred1.occurences_(1)= sum(idx_); % nb effective occurences
pred1.occurences_(2)= sum(idx_)>= Npoint; % 1 <=> enough occurences

if sum(idx_)< Npoint
    p= p0;
    if col_id==0; id_news_= 0;
    else;id_news_= R.news.col_id(id_col); end
    pred1.log_{end+1}= struct('type','Npoint too low','ctxt_id',id_news_,'obs',sum(idx_),'rank',col_id);
    pred1.info.run_quality= 1;
else % Predictor @learning freq
    if col_id==0 % calcul usualDay et non pas NoNews!
        p=p0;
    else
        % Premi�re fois o� l'on se retrouve en pr�sence de volumes contextualis�s !
        % On applique la fonction "prop4context" qui :
        % 1. retire les phases qui ont cess� d'exister
        % 2. propage la m�diane pour les phases qui sont apparues
        [prop,idx] = prop4context(vol.value(idx_,:));
        p = nan(1,size(vol.value,2));
        p(idx) = est(prop(:,idx),[]);
    end
    pred1.value_LF = p;
    if col_id > 0
        prop4noter_fun = vol.value;
        prop4noter_fun(idx_,:) = prop;
        pred1.info.run_quality = fpv(noter_fun_(prop4noter_fun,idx_));
    else
        pred1.info.run_quality = nan;
    end
end

assert(all(abs(nansum(p)-1) < 0.0001));
used(idx_) = 1;

assert(min(p(:))>=0);
pred1.value_LF= p(:);

%%% Version Malas (19/09/2016)
% Subbin: from 10 minutes to 5 minutes. last_day used as the reference
[~,phase_id] = trading_time_interpret([]);
subbinned_continuous_curve = kron(pred1.value_LF(vol.phases==phase_id.CONTINUOUS)/2,[1;1]);
last_day_bin_minute = round(mod(last_day.date(last_day.phases==phase_id.CONTINUOUS),1)*24*60);
if mod(last_day_bin_minute(1),10)==0 % Si premier bin de last_day multiple de 10 minutes, on vire le premier bin de subbinned_continuous_curve
    subbinned_continuous_curve(2) = subbinned_continuous_curve(2)*2;
    subbinned_continuous_curve(1) = [];
end
if mod(last_day_bin_minute(end)-1,10)<5 % Si dernier bin de last_day est � moins de 5 minute du multiple de 5 minutes pr�c�dent, on vire le dernier bin de subbinned_continuous_curve
    subbinned_continuous_curve(end-1) = subbinned_continuous_curve(end-1)*2;
    subbinned_continuous_curve(end) = [];
end

pred1.value = subbinned_continuous_curve;
pred1.date  = mod(last_day.date(last_day.phases==phase_id.CONTINUOUS),1);
pred1.phase = phase_id.CONTINUOUS*ones(length(pred1.date),1);

% Ajout des fixings
pred1.value = [pred1.value;pred1.value_LF(vol.phases~=phase_id.CONTINUOUS)];
pred1.date  = [pred1.date;reshape(mod(datenum(vol.colnames(vol.phases~=phase_id.CONTINUOUS)),1),[],1)];
pred1.phase = [pred1.phase;reshape(vol.phases(vol.phases~=phase_id.CONTINUOUS),[],1)];
assert(abs(nansum(pred1.value)-1)<0.00001,'Courbe de volume predite non normalis�e � 0.1 bps pr�s')
%%% Fin version Malas


%     %%% Version Namay
%     % D/ ---- Subbin ----------------------------------------
%     [idx,t1,p1,ta,pa]= create_subbin(old_freq/24/60,mod(datenum(vol.colnames),1),vol.phases,new_freq/24/60);
%     [dayExtrap_v0,disp_from]= dispatchSubbin_v2(p(idx),mod(datenum(vol.colnames(idx)),1),t1,vol.phases(idx),p1);    % dispatch phase continue
%     assert(abs(sum(dayExtrap_v0)-sum(dayExtrap_v0)) < 0.000001);
%
%     prev=[];
%     prev.value= dayExtrap_v0;
%     prev.date= t1';
%     prev.NC= p(~idx);
%     prev.NCp= pa;
%     [dayExtrap,volume_manquant]= treat_market_time_v2(prev,last_day); % remets phase NC
%     % TMT_v2 a rajout? les auctions !
%     assert(min(size(dayExtrap))==1)
%     assert(abs(sum(dayExtrap)-1)<0.00001) %assert(abs(nansum(dayExtrap)-1)< 0.001);
%     pred1.value= dayExtrap(:);
%     pred1.date= [t1(:);ta(:)];
%     pred1.phase= [p1(:);pa(:)];
%     %pred1.value_NH= pred1.value;
%     %%% Fin version Namay

% E/ ---- Qualities  ----------------------------
pred1.info.quality_update= mean(nansum(bsxfun(@plus,vol.value(idx_,:),-p).^2,2));
pred1.info.quality_ref= mean(nansum(bsxfun(@plus,vol.value(idx_,:),-the_med).^2,2));
pred1.info.quality_done= length(find(idx_)); %[quality_done;find(idx_)];

function data= align_predictors(liste)
% Detects presence of FIX1 (intraday auction)
[no_use,phase_id,no_use,from_phase_id_to_phase_name] = trading_time_interpret([],[]);
withFIX1= cellfun(@(x) x.date(find(phase_id.ID_FIXING1==x.phase)),liste,'uni',0);
withFIX1= withFIX1( cellfun(@(x)~isempty(x), withFIX1 ) );
withFIX1= [withFIX1{:}];
assert(length(unique(withFIX1))<=1);
for k=1:length(liste)
    fix1= find(liste{k}.phase== phase_id.ID_FIXING1);
    if ~isempty(withFIX1)
        if  ~isempty(fix1); assert(length(fix1)==1);
        else % systematically adds bin
            liste{k}.value(end+1)= 0;
            liste{k}.date(end+1)= withFIX1;
            liste{k}.phase(end+1)= phase_id.ID_FIXING1;
        end
    end
    [~,order]= sort(liste{k}.date+liste{k}.phase);
    liste{k}.date= liste{k}.date(order);
    liste{k}.phase= liste{k}.phase(order);
    liste{k}.value= liste{k}.value(order);
end

data= struct('value',[],'date',[],'phase',[],'info',[],'log_',{{}},...
    'col_id',[],'id_col',[],'id_news',[],'occurences_',[],'value_LF',[]);
data.info= struct('quality_update',[],'quality_ref',[],'quality_done',[],'run_quality',[]);
for k=1:length(liste)
    current= liste{k};
    check= norm(liste{1}.date-current.date)+norm(liste{1}.phase-current.phase);
    assert(check ==0 );
    if k==1
        data.phase(:,k)= current.phase;
        data.date(:,k)= current.date;
    end
    data.value(:,k)= current.value;
    data.value_LF(:,k)= current.value_LF;
    
    data.col_id(:,k)= current.col_id;
    data.id_col(:,k)= current.id_col;
    data.id_news(:,k)= current.id_news;
    
    data.occurences_(:,end+1)= current.occurences_;
    %data.log_(end:end+length(current.log_)) = current.log_;
    data.log_= {data.log_{:}, current.log_{:}};
    data.info.quality_update(:,end+1)= current.info.quality_update;
    data.info.quality_ref(:,end+1)= current.info.quality_ref;
    data.info.quality_done(:,end+1)= current.info.quality_done;
    data.info.run_quality(:,end+1)= current.info.run_quality;
end



%% INTERNALS -----------------------------------------------------------
function [dimension]= align_start_market(dimension,idx,vol)

f= find(idx);
f=f(1);
open_continous= vol.begin_.value(f);

dimension= dimension+1-f;


function name= event_id_2_context_name(id)
% les colnames de l'estimateurs doivent etre le nom des contextes
% pas le nom des news !!!
% dans ma table de rankings, l'event des inhib? n'est aps le bon
% ( on renvoit les courbes inhib?es non inhib?es !)
global st_version
quant = st_version.bases.quant;

if ischar(id); id= str2num(id);end
persistent x;
if isempty(x)
    r={['select event_type, context_name ',...
        'from ',quant,'..estimator e, ',quant,'..context c, ',quant,'..event_action ea ',...
        'where e.estimator_name = ''Context volume''  ',...
        'and c.context_id = ea.context_id ',...
        'and c.estimator_id = e.estimator_id ',...
        'and ea.action = ''C'' ']};
    DB= contexte_volume_DBaccess();
    x= DB.exec(r);
    x=x{1};
end
if id==0
    i= find(isnan(x.event_type));
else
    i= find(x.event_type==id);
end
assert(length(i)==1);
name= x.context_name{i};

function name= where_to_put_global_quality()
% r= {['select context_name ',...
% 'from ',quant,'..estimator e, ',quant,'..context c ',...
% 'where e.estimator_name = ''Context volume'' ',...
% 'and c.context_num = e.validation_context_num ',...
% 'and c.estimator_id = e.estimator_id ']};
% DB= contexte_volume_DBaccess();
% ctxt_nonews_id= DB.map_id_num(nan,'e2c');
% x= DB.exec(r);
% name= x{1}.context_name;
name= event_id_2_context_name(0);



function varargout= regexp_(varargin)
varargout= cell(1,nargout);
try
    [varargout{:}] = regexp(varargin {:});
catch
    varargout{1}= [];
end



% Il faut vraiment calculer pred ? la learning_freq, et le dispatcher sur freq= 5min
function vol= getCurrentData(sec_id,from,to,freq,trading_destination_id,freq_learning)
createNewBuffer();
createNewBuffer('no_buffer');
vl= VLoader();
n_min_obs= 1; % FIXME !!!!
%vl.main(idcourant,dates,TD_,Lfreq,folder,NminObs);
vol= vl.main(sec_id,[from,to],trading_destination_id,freq_learning,'',n_min_obs);
vol= vol{1};
% normalizes repartition (FIXME: pas ici ?)
vol.value= bsxfun(@rdivide,vol.value,nansum(vol.value,2));
if ~st_data('check',vol)
    warning('se_run_context_volume did not load proper data');
end



function [est,seuil,Npoint,fpv]= get_parameters(Bopt,R)
est= Bopt.get('estimateur');
est= R.estimators.get(est);
seuil=  Bopt.get('seuil');
Npoint= Bopt.get('Npoint');
fpv= @(x)x;
if strcmpi(R.opt.get('noter'),'std')
    fpv= @(x) 1-x;
end


function dayExtrap= treat_auction_data(auctions_data,pa,dayExtrap)
[no_use,phase_id,no_use,from_phase_id_to_phase_name] = trading_time_interpret([],[]);
for k=1:length(auctions_data)
    if pa(k)== phase_id.OPEN_FIXING % open
        assert(isnan(dayExtrap(end-3)))
        dayExtrap(end-3)= auctions_data(k);
    elseif pa(k)== phase_id.CLOSE_FIXING %close
        assert(isnan(dayExtrap(end)))
        dayExtrap(end)= auctions_data(k);
    elseif pa(k)== phase_id.ID_FIXING1 % intraday auction
        assert(isnan(dayExtrap(end-2)))
        dayExtrap(end-2)= auctions_data(k);
    elseif pa(k)== phase_id.ID_FIXING2 % intraday auction
        assert(isnan(dayExtrap(end-1)))
        dayExtrap(end-1)= auctions_data(k);
    else; assert(0);
    end
end

function f= modify_estimator(est)
f= @(s,b) modified_(est,s,b);

function v= modified_(est,sample,IA_bin)
% "Sample" : Contient les proportions des volumes (1 jour = 1 ligne)
% "IA_bin" : Indice du bin d'intraday auction (FIXING1)
v= est(sample);
if ~isempty(IA_bin)
    assert(length(IA_bin)==1);
    i= sample(:,IA_bin) > 0;
    v(IA_bin)= est(sample(i,IA_bin));
    if isnan(v(IA_bin)); v(IA_bin)=0;end
    v= v/sum(v,2);
end

function [alpha,beta]= add_envelope(pred,mkt_data,BN)
% BN prends ses valeurs dans [0,Ncontexte] et N+1 colonnes dans pred
% remplace 0 par N (colonen correcte de UsualDay dans pred)
[T__,N]= size(pred);
assert(all(BN<N));
BN(BN==0)= N;


pred= pred(2:end-1,:);
pred= bsxfun(@times,pred,1./nansum(pred,1));
mkt_data= mkt_data.value(:,2:end-1);
mkt_data= bsxfun(@times,mkt_data,1./nansum(mkt_data,2));
M_= pred(:,end);



predicteur_= pred(:,BN); % jour= colonnes
residuals_days= mkt_data - predicteur_';
residuals_days= residuals_days(:,1:end-1); % enleve le bin==1 depuis le cumul
M_= nancumsum(M_);M_=M_(1:end-1);
residuals_days= nancumsum(residuals_days,2);
v= mean(residuals_days.^2,1);

%     plot(v)
%     hold on
%     residuals_days= bsxfun(@minus, mkt_data, mean(mkt_data));
%     residuals_days= residuals_days(:,1:end-1);
%     residuals_days= cumsum(residuals_days, 2);
%     v= mean(residuals_days.^2,1);
%     plot(v, 'r')
%     hold off

[alpha,beta]= estimate_envelope(v,M_');

function [alpha,beta]= estimate_envelope(v,m)
MIN_BETA= 1;MIN_ALPHA= 3;
%alpha=MIN_ALPHA;beta= MIN_BETA;
%return %% WARNING WARNING WARNING
tmp = [ones(length(v), 1) log(m.*(1-m))'];
tmp = (tmp'*tmp)^-1*tmp'*log(v)';

beta = tmp(2);

alpha = 0.25^(1-beta)/exp(tmp(1))-1;

if beta <= MIN_BETA || ~isfinite(beta)
    tmp = (v./(m.*(1-m)));
    alpha = median(1./tmp) - 1;
    beta = MIN_BETA;
end
if alpha <= MIN_ALPHA
    alpha = MIN_ALPHA;
end


function data= make_full_profile(data,pred_lfreq)
% TODO: rendre coh?rent treat_mktTime_v2
%   * g?rer les auctions non pr?sentes que l'estimateur s'attends ? avoir dans
%   tous les cas !
data.value= pred_basique_subbin_v0;
data.date= t1';
data.NC= []; %p0(~idx);%[];% Evite de traiter les auctions  % p0(~idx);
data.NCp= []; %pa;[]; %pa;

function [prop,idx] = prop4context(prop)
% Fonction de correction des phases pour proportions contextualis�es
% idx : phases correctes
dis_phase = isnan(prop(end,:));
prop(:,dis_phase) = nan;
[r,c] = find(bsxfun(@and,~isnan(prop(end,:)),isnan(prop)));
filling_val = nanmedian(prop);
for i = unique(r)'
    idx_nan = c(r==i);
    not_idx_nan = setdiff(1:size(prop,2),idx_nan);
    prop(i,idx_nan) = filling_val(idx_nan);
    % Normalisation
    prop(i,not_idx_nan) = prop(i,not_idx_nan)*(1-sum(prop(i,idx_nan)))./...
        nansum(prop(i,not_idx_nan));
end
idx = ~dis_phase;
% p= est(prop4context,[]);


function data = remove_nan_param(data)
for d = 1:length(data)
    idx_slice = ~cellfun('isempty',regexp(data{d}.rownames,'^slice\d{3}','match','once'));
    idx_auction = ~cellfun('isempty',regexp(data{d}.rownames,'^auction_.*(?<!closing)$','match','once'));
    idx_auction_closing = ~cellfun('isempty',regexp(data{d}.rownames,'^auction_.*_closing$','match','once'));
    idx_rm = false(length(data{d}.rownames),1);
    idx_rm(idx_slice) = isnan(data{d}.value(idx_slice));
    idx_rm(idx_auction) = isnan(data{d}.value(idx_auction));
    idx_rm(idx_auction_closing) = idx_rm(idx_auction);
    data{d}.rownames(idx_rm) = [];
    data{d}.value(idx_rm) = [];
end

%% ------------------------------------------------------
function TEST()

ndb=newsDb(true);
my_buffer('clear')
% sec_id / TD_id /.../ varargin
args_={8,[], 'day', 1500, '01/06/2012',[]}; pred=se_run_context_volume(args_{:});
args_={10735,[], 'day', 1500, '01/11/2011',[]}; pred=se_run_context_volume(args_{:});


chartableau(pred.update_info.news_.colnames,sum(pred.update_info.news_.value))
pred.update_info.quality_update./pred.update_info.quality_ref
pred.update_info.log{:}
pred.update_info.R
%

% learning freq plot
lgd_= pred.id_news;
inh= ismember(pred.id_news,[pred.log_.ctxt_id]);
Linh= ismember(pred.col_id,find(pred.update_info.R.inhibited));
lgd_= cellstr(chartableau(lgd_',0+Linh,0+inh,pred.colnames));
%matrixplot(pred.value_LF',cellstr(lgd_),30)
figure;matrixplot(pred.value_LF',lgd_,100)
set(gca,'xticklabel',[]);
hrs= pred.update_info.vol_.colnames;
hrs= cellstr(chartableau(pred.update_info.vol_.phases,'- ',hrs));
for k=1:length(hrs)
    %text(k+0.5,1,hrs{k},'rotation',90);
    text(k+0.5,length(lgd_)+2.5,hrs{k},'rotation',90);
end
% checks estimation inhibition
inh= find(ismember(pred.id_news,[pred.log_.ctxt_id]) | ismember(pred.col_id,find(pred.update_info.R.inhibited)));
corr([pred.value_LF(:,[inh,end])])
corr([pred.value(:,[inh,end])])

select_bins= @(cn) cellfun(@(x)...
    (length(x)>=5 &&strcmp(x(1:5),'slice')) ||((length(x)>=8 && strcmp(x(1:8),'auction_')) && ~strcmp(x(end-7:end),'_closing')) ,...
    cn) ; %select_bins(pred.rownames)
chartableau(pred.rownames',0+select_bins(pred.rownames)')
sum(pred.value(select_bins(pred.rownames),:),1)

select_Cbins= @(cn) cellfun(@(x) length(x)>=5 &&strcmp(x(1:5),'slice') ,cn) ; select_Cbins(pred.rownames)
curves= pred.value(select_Cbins(pred.rownames),:);
curves= cumsum(curves,1);
grp= pred.learning_frequency/ pred.frequency;
curves= curves(grp:grp:end,:);
curves= diff(curves);
cov(curves,pred.value_LF(1:end-2,:))
%%
figure,
subplot(2,1,1);
plot(pred.value(1:end-1,:));
legend(lgd_);
subplot(2,1,2);
x_= pred.value; x_(isnan(x_))=0;
plot(cumsum(x_(1:end-1,:),1));

plot(pred.update_info.pred_freq_l);legend(lgd_);

plot(pred.update_info.vol_.value')



[pv,p_]= se_check_context_volume(pred, 'day', 500, '01/01/2010',[])


function TESTidx()
comp= get_index_comp('CAC40', 'recent_date_constraint', false);
for sid= (comp(:)')
    args_={sid, {} , 'day', 1000, '01/01/2011',[]};
    try
        pred=se_run_context_volume(args_{:});
    catch ME
        pred=  ME;
    end
    save(['C:\test_se_run_CV\cv_run' num2str(sid) '.mat'],'pred');
end
comp= get_index_comp('DAX', 'recent_date_constraint', false);
for sid= (comp(:)')
    args_={sid, {} , 'day', 1000, '01/01/2011',[]};
    try
        pred=se_run_context_volume(args_{:});
    catch ME
        pred=  ME;
    end
    save(['C:\test_se_run_CV\cv_run' num2str(sid) '.mat'],'pred');
end
%% READ
l=dir('C:\test_se_run_CV');
l= {l.name};
N=length(l);
failed={};
for n=1:N
    if l{n}(1)== '.';failed{end+1}= n;continue ;end
    data= load(['C:\test_se_run_CV' '\' l{n}]);
    if ismember('message',properties(data.pred));
        failed{end+1}= n;
    end
end
disp(failed)

datas={};
x= setdiff(1:N,[failed{:}])
for n= (x(:)')
    data= load(['C:\test_se_run_CV' '\' l{n}]);
    datas{end+1}= data.pred;
end

select_bins= @(cn) cellfun(@(x)...
    (length(x)>=5 &&strcmp(x(1:5),'slice')) ||(length(x)>=8 && strcmp(x(1:8),'auction_')) ,...
    cn) ;
for k=1:length(datas)
    pred= datas{k};
    figure,
    matrixplot(pred.value_LF(1:end-3,:)',pred.colnames,30);
    title(num2str(pred.update_info.B.IDstock));
end
subplot(2,1,1);
title()
plot(pred.value_LF);%(select_bins(pred.rownames),:));
legend(lgd_);
subplot(2,1,2);
x_= pred.value_LF; x_(isnan(x_))=0;
plot(cumsum(x_(select_bins(pred.rownames),:),1));


