function side_effect_file(rep,varargin)

global st_version


x= datestr(now);
x=strrep(x,':','_');
x=strrep(x,'-','_');
x=strrep(x,' ','_');

args= cellfun(@num2str,cellflat(varargin),'uni',0);
args= char(args);
args= args';
args= args(:);
args=args';

args= deblank(args);

mkdir(fullfile(st_version.my_env.st_repository, 'side_effect_file',  rep));
mainFolder = fullfile(st_version.my_env.st_repository, 'side_effect_file', rep,['done_' args  '_' x '.mat']);

xxx=1;
save(mainFolder,'xxx');