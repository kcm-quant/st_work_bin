function self= test_learning_output(mode)

self=[];
self.show_performance= @show_performance;
self.show_average_ranks= @show_average_ranks;
self.show_ranks=@show_ranks;
self.show_co= @show_co;

self.plot_predictors=@plot_predictors;
self.make_report=@make_report;
self.show_main_indicators=@show_main_indicators;
self.atypie=@atypie;
self.warning_nb_obs=@warning_nb_obs;
self.plot_predictors_news=@plot_predictors_news;

%self.[failed,base]= f0(a)
%% AVERAGE INDICATORS
function [x,y]= warning_nb_obs(datas)
    f= @(x) [length(x.best.date),max(x.best.date)];
    d= uo_(cellfun(f,datas.alldata,'uni',0));
    ids= uo_(cellfun(@(x)x.best.IDstock,datas.alldata,'uni',0));
    
    %x= find(d(:,1) < quantile(d(:,1),.75)*3/4 );
    x= find(d(:,1) < max(d(:,1))*0.7 ) ;%quantile(d(:,1),.75)*0.99 );
    %y= find(d(:,2) < max(d(:,2))- 50 );
    y= find(d(:,2) <= max(d(:,2))- 2 );
    x= ids(x);
    y= ids(y);

function [failed,res]= show_performance(ip,inhibition,final)
    res=[];failed=[];
    persistent res01;    
    if nargin == 0
        res01= [];
        return
    end

    % Draws operationnal criteria gain and context switch probabilities
    % uses data from learning buffers
    if isempty(res01)
        ip.useInhibition= inhibition; 
        ip.final_=final;        
        res01= gatherResults(ip.comp(:)',ip.useInhibition,ip.final_,@(sec_id) cb_newinfo(ip,sec_id) );        
    end
    res01.display(res01);
    failed= res01.failed;
    res= res01;

function s=names_(self)
     s1= char(self.v.names);
     T= size(s1,1);
     s2= repmat(' (',T,1);
     s3= num2str( self.v.IDstock(:));
     s4= repmat(') ',T,1);
     s5= num2str(floor(100*self.v.RS.prob(:)));
     s5= [s5, repmat('%',size(s1,1),1)];
     s=[s1,s2,s3,s4,s5];
function show_main_indicators(res,resF)    
    select= cellfun(@(x) sum(x.best.BN>0) ,res.alldata);
    ndates= diff(res.alldata{1}.best.original_date) *5/7;
    dates= cellfun(@(x) mean(size(x.best.value,1)) ,res.alldata);%/ndates;
    
    N_stock_= length(res.alldata);
    ns_figure
        subplot(1,7,2:3);hold on
    plot(dates,1:N_stock_,'b+');    
    plot(select,1:N_stock_,'g*');   
    legend({'in learning sample','when context is selected'});  
    set(gca,'Ytick',1:N_stock_)
    set(gca,'Yticklabel',names_(res) )
    title(['Number of observations' ]);
    set(gca,'ylim',[0 N_stock_+5]);
    
        subplot(1,7,4:5);hold on
    plot(res.v.RS.nctxt,1:N_stock_,'r.');        
    plot(res.v.RS.inhib,1:N_stock_,'k+');
    legend({'Ranked (non inhibited)','Ranked+ inhibited'});  
    set(gca,'Ytick',1:N_stock_);set(gca,'Yticklabel',[])
    title(['Contexts selection' ]);
    set(gca,'ylim',[0 N_stock_+5]);
    
        subplot(1,7,6:7);hold on
    plot(res.v.RS.no(:,1),1:N_stock_,'b+');        
    plot(res.v.RS.no(:,4),1:N_stock_,'r*');
    plot(res.v.RS.fs(:,4),1:N_stock_,'k+');
    plot(resF.v.RS.fs(:,4),1:N_stock_,'k.');
    legend({'L^2 error on volumes, when selection','CO, when selection','CO, full sample','CO, full sample+ Usual Days'});  
    set(gca,'Ytick',1:N_stock_);set(gca,'Yticklabel',[])    
    title(['Model quality' ]);
    set(gca,'ylim',[0 N_stock_+5]);
    my_buffer('main_summary',gcf);
   
    
    
function [Y,ctxt,stock,RK_,inh]= show_ranks(Z,doplot)
    if isempty(Z.value)
        Y=nan; ctxt={};%nan;
        stock=nan;RK_=nan;inh=nan;
        if doplot  
            ns_figure;
            title('No context is ranked : non contextual model is selected')
            my_buffer('ranks',gcf);
        end
        return
    end
    %my_buffer('clear');
    if nargin< 2; doplot=0;end
    [ctxt, m, n] = unique(Z.value(:,2));
    [stock, m, n] = unique(Z.value(:,1));
    %Y= 1.2*ones(length(ctxt),length(stock));
    Y= nan(length(ctxt),length(stock));
    RK_= nan(length(ctxt),length(stock));
    inh= nan(length(ctxt),length(stock));
    nr_= length(ctxt);  
    sec_ids= stock;
    for q = 1:length(stock)
        %nr_= sum(Z.value(:,1)==stock(q));           
        for p = 1:length(ctxt)                
            idx= Z.value(:,1)==stock(q) & Z.value(:,2)==ctxt(p);
            idx= find(idx); assert(length(idx)<=1);
            if ~isempty(idx)
                Y(p,q)= (Z.value(idx,3)-100)/10/nr_;
                RK_(p,q)= (Z.value(idx,3)-100)/10;
                inh(p,q)= Z.inhibited(idx);
            end
        end
    end
    m= mean(Y,2);
    [~,i]= sort(m,'ascend');
    Y= Y(i,:);
    ctxt= ctxt(i);
    RK_=RK_(i,:);
    inh=inh(i,:);
    n= newsDb();
    ctxt= n.id2name(ctxt);
    if doplot      
        ns_figure;
        matrixplot(Y,ctxt',30,'nobone');
        
        [n_ctxt,n_stock]= size(Y);
        set(gca,'xtick',[] );
        for n__=1:n_stock                
            text(n__+.5,n_ctxt+1.5,num2str(sec_ids(n__)),'rotation',-90);
        end
        
        title('Normalized ranks (lowest/blue= ranked 1st, white= not ranked)');
        my_buffer('ranks',gcf);
    end
    Y(isnan(Y))= 1.2;
    
    
function [X,stocks]= show_average_ranks(Z,doplot)
    if isempty(Z.value)
        X= nan;stocks=nan;
        if doplot;
            ns_figure;
            title('No context is ranked : non contextual model is selected')
            my_buffer('avg_ranks',gcf);
        end
        return
    end
    if nargin< 2; doplot=0;end
    [Y_,ctxt,stocks,RK_,inh]= show_ranks(Z,0);
    Y_(Y_>1.19)= nan;
    n_ctxt= length(ctxt);
    Y= RK_/n_ctxt;
    Y(isnan(Y))=1.01;
    %Y(Y==1.1)=nan;
    RK_SR= bsxfun(@times,RK_,1./max(RK_));
    X= [nanmean(Y,2),min(Y,[],2),max(Y,[],2),...
        quantile(RK_SR,0.25,2),quantile(RK_SR,0.5,2),1-mean(~isnan(RK_),2),nanmean(inh,2),max(RK_/n_ctxt,[],2),max(RK_SR,[],2)];    %max(RK_SR,[],2)
        %1-nanmean(Y_<0.25,2),1-nanmean(Y_<=0.5,2),1-mean(~isnan(RK_),2)];    
    [~,i]= sort(X(:,1),'ascend');
    X= X(i,:);
    ctxt=ctxt(i);
    
    RK_SR_inh=RK_SR; RK_SR_inh(inh==1)= nan;
    Yinh=Y; Yinh(inh==1)= nan;
    RK_(inh==1)= nan;
    Xinh= [nanmean(Yinh,2),min(Yinh,[],2),max(Yinh,[],2),...
        quantile(RK_SR_inh,0.25,2),quantile(RK_SR_inh,0.5,2),1-mean(~isnan(RK_),2),nanmean(inh,2),max(RK_/n_ctxt,[],2),max(RK_SR_inh,[],2)]; 
    Xinh= Xinh(i,:); 
   
    %X(:,1:3)= 1-X(:,1:3);%/length(ctxt);
    if doplot
        ns_figure;
        N1= 1:size(X,1);
        
        subplot(1,9,2:3)
        hold on;
        plot(X(:,1),N1,'r-.');
        plot(Xinh(:,1),N1,'r');
        plot(X(:,6),N1,'b');
        plot(X(:,7),N1,'k*');
        legend('Average rank','Average rank, non inhibited','Non selected','Inhibited');
        set(gca,'ytick',N1);
        set(gca,'yticklabel',ctxt);
        set(gca,'ylim',[0,size(X,1)+7]); set(gca,'xlim',[-0.05 1.05]);
        title(['Average rankings']);
        
        subplot(1,9,4:6)
        
        hold on;        
        plot(X(:,2),N1,'k-.');
        plot(X(:,3),N1,'k-.');
        plot(X(:,9),N1,'b');
        plot(X(:,8),N1,'b-.');
        plot(X(:,4),N1,'r.');
        plot(X(:,5),N1,'g*');
        
        set(gca,'ylim',[0,size(X,1)+7]); set(gca,'xlim',[-0.05 1.05]);
        legend('Best Rank','Worst Rank','Worst Rank when selected','Worst Rank when selected, stock normalized','Belonging to top 25%','Belonging to top 50%');
        set(gca,'ytick',[]);
        set(gca,'yticklabel',[]);        
        title(['Normalized rankings (average on ' num2str(length(unique(Z.value(:,1)))) ' stocks) \newline '...            
            'Quantiles, inhibited contexts are kept'])                
        xlabel('Rankings (normalized, the lower the better)')
        
        subplot(1,9,7:9);
        title(['Removing inhibited contexts']);
        hold on;        
        plot(Xinh(:,2),N1,'k-.');
        plot(Xinh(:,3),N1,'k-.');
        plot(Xinh(:,9),N1,'b');
        plot(Xinh(:,8),N1,'b-.');
        plot(Xinh(:,4),N1,'r.');
        plot(Xinh(:,5),N1,'g*');
        
        %legend('Best Rank','Worst Rank','Belonging to top 25%','Belonging to top 50%','Worst Rank when selected');
        set(gca,'ytick',[]);
        set(gca,'yticklabel',[]);
        set(gca,'ylim',[0,size(X,1)+7]); set(gca,'xlim',[-0.05 1.05]);
        title(['Quantiles, inhibited contexts are removed']);
        %xlabel('Rankings (normalized, the lower the better)')
        
        my_buffer('avg_ranks',gcf);
    end
    
    function [X,keys,X0,keys0]= show_co(res,doplot,show_ni,critere) 
        if nargin< 2; doplot=0;end
        if nargin< 3; show_ni=0;end
        if nargin< 4; critere=4;end
        % res= cell-array de type gatherResults()
        data= cellfun(@(x)x.best,res,'uni',0);
        sec_ids= cellfun(@(x) x.IDstock, data,'uni',1);
        N= length(data); % stocks
        allR= {};
        allR0= {};
        allR00= {};
        for n=1:N
            R= options();
            R0= options();
            R00= options();
            
            d= data{n};
            if show_ni
                dni= get_non_inhibited_model(d.opt,data{n});%.IDstock);  
            else
                dni= d;
            end
            
            d0= res{n}.M0;
            for r=([d.ranks(:)'])
               idx= dni.BN== r;
               co= mean(abs(dni.value(idx,critere)));
               co0= mean(abs(d0.value(idx,critere)));
               R.set(dni.news.col_id(r), co);    
               R0.set(dni.news.col_id(r), co/co0); 
               R00.set(dni.news.col_id(r), co/co0); 
%                if ismember(r,d.inhibitedCtxt)
%                     R00.set(d.news.col_id(r), 2);
%                end
            end          
            idx= d.BN== 0;
            co= mean(abs(d.value(idx,critere)));
            co0= mean(abs(d0.value(idx,critere)));
            R.set(0, co); 
            R0.set(0, co/co0);    
            R00.set(0, co/co0);    
            allR{end+1}= R;     
            allR0{end+1}= R0;   
            allR00{end+1}= R00;   
        end
        [X,keys,missing]= uni_options(allR);
        [X0,keys0,missing]= uni_options(allR0);   
        [X00,keys00,missing]= uni_options(allR00);   
        char(missing)
        [P,Q]= size(X);
        %X(isnan(X))= -max(X(:)); 
        %X0(isnan(X0))= -1; %1.2;         ;%X0(isnan(X0))= -max(X0(:));;
        %X00(isnan(X00))= -1; %1.2;         
        
        if doplot           
            n= newsDb();
            ctxt= n.id2name([keys{2:end}]);
            ctxt={'no news',ctxt{:}};
            [~,idx_]= sort(ctxt);
            ctxt=ctxt(idx_);X=X(:,idx_);
            ns_figure;
            if critere==4
                matrixplot(10000* X',ctxt,30,'nobone');
            else
                matrixplot(X',ctxt,30,'nobone');
            end
            all_ttl={'Predictive perf (L^2)','Predictive perf (L^1)','Predictive perf (K.S)','Operationnal criteria (bp)'};
            ttl= all_ttl{critere};
            ttl= {[ttl ' when contexts are applied']};
            ttl{end+1}= 'white= non selected contexts';
            if show_ni; ttl{end+1}= 'before inhibition';
            else; ttl{end+1}= 'after inhibition';
            end
            title(ttl);
            
            set(gca,'xtick',[]);
             
            [n_ctxt,n_stock]= size(X');
            %set(gca,'xtick',0.5+(1:n_stock) );
            for n__=1:n_stock                
                text(n__+.5,n_ctxt+1.5,num2str(sec_ids(n__)),'rotation',-90);
            end
            
            name__= 'after';
            if show_ni; name__='before';end
            my_buffer(['CO' num2str(critere) name__],gcf);
            
%             ctxt= n.id2name([keys0{2:end}]);
%             ctxt={'no news',ctxt{:}};
%             [~,idx_]= sort(ctxt);
%             ctxt=ctxt(idx_);X0=X0(:,idx_);
%             X0(X0>1)=2;
%             X0(:,end+1)= -1;ctxt{end+1} ='';
%             X0(:,end+1)= 2;ctxt{end+1} ='';
%             figure;matrixplot(X0',ctxt);
%             title({'Relative performance when contexts are applied, before inhibition','-perf greater than 1 are set to 2 (black) and should be inhibited','-non selected contexts are set to negative values (white)'})
%             
            ctxt= n.id2name([keys00{2:end}]);
            ctxt={'no news',ctxt{:}};
            [~,idx_]= sort(ctxt);
            ctxt=ctxt(idx_);X00=X00(:,idx_);
            %X00(X00>1)=2; % avec et sans
            %X00(:,end+1)= -1;ctxt{end+1} ='';
            %X00(:,end+1)= 2;ctxt{end+1} ='';
            
            ns_figure;matrixplot(X00',ctxt,30,'nobone');
            %set(gca,'ylim',[1 size(X00,2)-2]);
            set(gca,'xtick',[])
            %title({'Relative performance when contexts are applied, before inhibition','-perf greater than 1 are set to 2 and should be inhibited','-non selected contexts are set white'})
            
            ttl= all_ttl{critere};
            ttl= {[ttl ' when contexts are applied. Relative to anticipative median predictor']};
            ttl{end+1}= 'white= non selected contexts';
            if show_ni; ttl{end+1}= 'before inhibition (perf greater than 1 will be inhibited)';
            else; ttl{end+1}= 'after inhibition';
            end
            title(ttl);
            
            [n_ctxt,n_stock]= size(X00');
            %set(gca,'xtick',0.5+(1:n_stock) );
            for n__=1:n_stock                
                text(n__+.5,n_ctxt+1.5,num2str(sec_ids(n__)),'rotation',-90);
            end
            name__= 'after';
            if show_ni; name__='before';end
            my_buffer(['RCO' num2str(critere) name__],gcf);
        end

function [preds,failed]= atypie(ip,vargs)
    if nargin<2; vargs= {'type','FINALmodel','inhibition',1};end
    preds= [];
    failed=[];
    for sec_id= (ip.comp(:)')
        try
            [pred,B,R,~,pred_ni]= generateFullModel(sec_id,vargs{:});
        catch
            failed(end+1)= sec_id;
            continue
        end
        inh= ismember(pred(:,3),B.inhibitedCtxt);
        pred(inh,:)= [];
        current= [repmat(sec_id,size(pred,1),1),pred];
        preds= [preds; current];
    end
    
    
        
        
%% STOCK by STOCK indicators
function [pred,B,R,info]= plot_predictors(ident_,vargs,mode_graphe)
    show_inhib= 0;
    if length(ident_)==5;
        show_inhib=1;
        ident_= ident_(1:4);
    end
    % Uses learning buffers
    sec_id= ident_{1};
    if nargin<2; vargs= {'type','FINALmodel','inhibition',1};end
    if nargin<3;mode_graphe=1;end
    [pred,B,R,~,pred_ni]= generateFullModel(ident_,vargs{:});
    pred= pred_ni;
    assert( all( abs(sum(pred(:,4:end)')-1) < 0.00001));
    if ~isfield(B,'inhibitedCtxt')
        B.inhibitedCtxt= []; % CORRECTED 02/04/2012
    end
    inh= ismember(pred(:,3),B.inhibitedCtxt);
    inh= find(inh);
    
    %pred(:,4:end)= (pred(:,4:end)); %log !
    
    
    info=info_rank_1stock(pred,B,R);%,inh) ;
    
    if mode_graphe==0;return;end
    plot_predictors_graphprocess(pred,B,R,pred_ni,info,inh,show_inhib,vargs,sec_id);


function plot_predictors_graphprocess(pred,B,R,pred_ni,info,inh,show_inhib,vargs,sec_id)
    [n_ctxt,n_time]= size(pred);
    n_parplot=5;    
    cmp= hsv(n_parplot);        
    dts_=datenum(R.volume.colnames);    
    ns_figure;    
    n_line= ceil(n_ctxt/n_parplot);
    h=[];    
    hh=[];
    M_= 0;
    for nl_=1:n_line
        % intra journalier
        subplot(n_line,4,(nl_-1)*4+(1:3));
        hold on;
        for next_= 1:n_parplot            
            n_curve= (nl_-1)*n_parplot + next_;
            if n_curve> n_ctxt; break;end
            ls= '-'; lw= 2; mark= '.';vs= 'on';
            if ismember(n_curve,inh);ls= '-.';lw=1;mark='None';vs='off';end
            if show_inhib==1;vs= 'on';end
            %h(end+1)= plot(dts_,pred(n_curve,4:end)','linewidth',2,'LineStyle',ls,'color',cmp(mod(n_curve-1,n_parplot)+1,:));
            h(end+1)= plot(dts_(1:end-1),pred(n_curve,4:end-1)','linewidth',lw,'LineStyle',ls,'color',cmp(mod(n_curve-1,n_parplot)+1,:),'Marker',mark,'visible',vs);
            assert(all(ishandle(h)));            
        end            
        set(gca,'xtick',dts_(1:4:end));
        set(gca,'xticklabel',[]);
        zoom_max('xy','handle',gca);
        % CLOSE
        subplot(n_line,8,(nl_-1)*8+7);
        hold on        
        for next_= 1:n_parplot
            n_curve= (nl_-1)*n_parplot + next_;
            if n_curve> n_ctxt; break;end            
            plot(pred(n_curve,end)','linewidth',2,'color',cmp(mod(n_curve-1,n_parplot)+1,:),'Marker','+'); 
            M_= max(M_,max(pred(n_curve,end)));            
            assert(all(ishandle(h)));            
            set(gca,'xtick',[]);
            set(gca,'xticklabel',[]);            
        end        
    end
    for nl_=1:n_line    
        subplot(n_line,8,(nl_-1)*8+7);
        set(gca,'ylim',[0 M_]);
        order_m= 10^floor(1+log10(M_))/10;
        set(gca,'ytick',[0:order_m:M_]);
        set(gca,'yticklabel',num2str([0:order_m:M_]'));
    end
    n= newsDb(1);
    lgd_= n.id2name(pred(:,2));
    lgd_= cellfun(@(x) strrep(x,'_','-'),lgd_,'uni',0);
    lgd_= chartableau(pred(:,1),lgd_);
    lgd_ = cellstr(lgd_);  
    %zoom_max('xy',gcf);    
    subplot(n_line,4,1:3);
    if isempty(R.volume.destinationID); TD= 'all';else; TD= num2str(R.volume.destinationID);end
    model= cellfun(@(x) [num2str(x) ' '],vargs,'uni',0 );
    model=[model{:}];
    ttl={};
    ttl{1}= 'Contextual volume curves';
    ttl{2}= sprintf('Stock= <%s,%s>. Trading-dest= <%s>',num2str(R.volume.info.security_id),B.NAMEstock,TD);
    ttl{3}= sprintf('Bins frequency= <%s>. Dates= <%s>-<%s>',num2str(R.volume.frequency),datestr(R.volume.date(1)),datestr(R.volume.date(end)));
    ttl{4}= sprintf('Selected model: <%s>.',model);
    title(ttl);
    
    lgd_=cellstr(lgd_);
    legend(h,lgd_); %,'Location','EastOutside');
    
    subplot(n_line,8,8*(n_line-1)+7);
%     set(gca,'xtick',1);
%     set(gca,'ytick',[0 1]);
%     set(gca,'xticklabel','close');
    
    subplot(n_line,4,4*(n_line-1)+(1:3));
    set(gca,'xtick',dts_(1:4:end));
    set(gca,'xticklabel',cellfun(@(x) x(1:5) ,R.volume.colnames(1:4:end),'uni',0));
    
   %zoom_max('xy',gca);
   
     c=get(gcf,'children');
     l= arrayfun(@(x) strcmpi(get(x,'tag'),'legend'),c);
     l= c(find(l));

     pos_= [ 0.82    0.1100    0.1560    0.8150];
     set(l,'position',pos_);
     my_buffer(['pred' num2str(sec_id)],gcf);

function name_= plot_predictors_news(ip,data,inhib_type) %pred,B,R,pred_ni,info,inh,show_inhib,vargs,sec_id)    
    show_inhib=1;
    % filter 5 best
    ranks= data.ranks;
    ranks(data.inh==inhib_type)=100000;
    [~,i]= sort(ranks,'ascend');
    if length(i)>5;i=i(1:5);end    
    data.curves= data.curves(:,i);
    data.rcurves= data.rcurves(:,i);
    data.sec_id= data.sec_id(i);
    data.inh= data.inh(i);
    data.ranks= data.ranks(i);
    pred= data.curves';
    rpred= data.rcurves';
    
    n_stock= length(data.sec_id);
    sec_names= get_security_name(data.sec_id);
    if ~iscell(sec_names);sec_names= {sec_names};end
    
    n_parplot=5;    
    cmp= hsv(n_parplot);        
    dts_=cellfun(@(x) datenum(x),data.colnames(4:end),'uni',1);
    %dts_= datenum(data.colnames,'hh:mm:ss');   
    ns_figure;    
    %n_line= ceil(n_ctxt/n_parplot);
    h=[];    
    hh=[];
    M_= 0;
    subplot(2,4,0*4+(1:3));
    hold on;
    for next_= 1:length(data.sec_id)       
        ls= '-'; lw= 2; mark= '.';vs= 'on';
        if data.inh(next_);ls= '-.';lw=1;mark='None';vs='off';end
        if show_inhib==1;vs= 'on';end
        %h(end+1)= plot(dts_,pred(n_curve,4:end)','linewidth',2,'LineStyle',ls,'color',cmp(mod(n_curve-1,n_parplot)+1,:));
        h(end+1)= plot(dts_(1:end-1),pred(next_,4:end-1)','linewidth',lw,'LineStyle',ls,'color',cmp(next_,:),'Marker',mark,'visible',vs);
        assert(all(ishandle(h)));            
    end            
    set(gca,'xtick',dts_(1:4:end));
    set(gca,'xticklabel',[]);
    zoom_max('xy','handle',gca);
    % CLOSE
    subplot(2,8,(1-1)*8+7);
    hold on        
    for next_= 1:length(data.sec_id)           
        plot(pred(next_,end)','linewidth',2,'color',cmp(next_,:),'Marker','+'); 
        M_= max(M_,max(pred(next_,end)));            
        assert(all(ishandle(h)));            
        set(gca,'xtick',[]);
        set(gca,'xticklabel',[]);            
    end
    set(gca,'ytick',[0 1]);
    % courbes relatives ? Usual days
    subplot(2,4,1*4+(1:3));
    hold on;
    for next_= 1:length(data.sec_id)       
        ls= '-'; lw= 2; mark= '.';vs= 'on';
        if data.inh(next_);ls= '-.';lw=1;mark='None';vs='off';end
        if show_inhib==1;vs= 'on';end
        %h(end+1)= plot(dts_,pred(n_curve,4:end)','linewidth',2,'LineStyle',ls,'color',cmp(mod(n_curve-1,n_parplot)+1,:));
        plot(dts_(1:end-1),rpred(next_,4:end-1)','linewidth',lw,'LineStyle',ls,'color',cmp(next_,:),'Marker',mark,'visible',vs);
        assert(all(ishandle(h)));            
    end            
    
    set(gca,'xtick',dts_(1:4:end));
    set(gca,'xticklabel',[]);
    zoom_max('xy','handle',gca);
    line(get(gca,'xlim'),[1 1],'linestyle','--','color','k');
    % CLOSE
    subplot(2,8,(2-1)*8+7);
    hold on        
    for next_= 1:length(data.sec_id)           
        plot(rpred(next_,end)','linewidth',2,'color',cmp(next_,:),'Marker','+'); 
        M_= max(M_,max(rpred(next_,end)));            
        assert(all(ishandle(h)));            
        set(gca,'xtick',[]);
        set(gca,'xticklabel',[]);            
    end    
    
    n_line=2;
    for nl_=1:2    
        subplot(n_line,8,(nl_-1)*8+7);
        set(gca,'ylim',[0 M_]);
        order_m= 10^floor(1+log10(M_))/10;
        set(gca,'ytick',[0:order_m:M_]);
        set(gca,'yticklabel',num2str([0:order_m:M_]'));
    end
    line(get(gca,'xlim'),[1 1],'linestyle','--','color','k');
    
    if isfield(ip,'RPC')
        src= ip.RPC;
        id_news_= find([src.key0{:}]== data.news_id);
        assert(length(id_news_)==1);
        co_= [];
        for n_= 1:length(data.sec_id)
            id_stocks= find(src.sec_ids_0==data.sec_id(n_));
            assert(length(id_stocks)==1);
            co_(n_)= src.X0(id_stocks,id_news_);
        end
        lgd_= chartableau(data.sec_id','-',cellfun(@(x)x(1:min(10,length(x))),sec_names,'uni',0),'(', data.ranks' ,',', num2str(floor(100*co_'),'%d') ,')');
    else
        lgd_= chartableau(data.sec_id','-',sec_names,'(', data.ranks' ,')');
    end
    
%     n= newsDb(1);
%     lgd_= n.id2name(pred(:,2));
%     lgd_= cellfun(@(x) strrep(x,'_','-'),lgd_,'uni',0);
%     lgd_= chartableau(pred(:,1),lgd_);
     lgd_ = cellstr(lgd_);  
     zoom_max('xy',gcf);    
     subplot(2,4,1:3);
    if isempty(ip.TD); TD= 'all';else; TD= num2str(ip.TD);end
    %model= cellfun(@(x) [num2str(x) ' '],vargs,'uni',0 );
    %model=[model{:}];
    n= newsDb(1); news_name= n.id2name(data.news_id);
    ttl={};
    ttl{1}= 'Contextual volume curves (top 5 rankings, non inhibited)';
    ttl{2}= sprintf('News= <%s,%s>. Trading-dest= <%s>',num2str(data.news_id),news_name{1},TD);
    %ttl{3}= sprintf('Bins frequency= <%s>. Dates= <%s>-<%s>',num2str(R.volume.frequency),datestr(R.volume.date(1)),datestr(R.volume.date(end)));
    %ttl{4}= sprintf('Selected model: <%s>.',model);
    title(ttl);
%     
    subplot(2,4,5:7);
    title('relative to Usual Day volume curve');
    
%     lgd_=cellstr(lgd_);
     legend(h,lgd_,'Location','EastOutside');
%     
     subplot(n_line,8,8*(n_line-1)+7);
     set(gca,'xtick',1);
     set(gca,'xticklabel','close');
     set(gca,'ytick',[0 1]);
%     
     subplot(n_line,4,4*(n_line-1)+(1:3));
     set(gca,'xtick',dts_(1:4:end));
     %set(gca,'xticklabel',cellfun(@(x) x(1:5) ,R.volume.colnames(1:4:end),'uni',0));
     set(gca,'xticklabel',cellfun(@(x) x(1:5) ,data.colnames(4:4:end),'uni',0));
%     
    zoom_max('xy',gca);
%    
      c=get(gcf,'children');
      l= arrayfun(@(x) strcmpi(get(x,'tag'),'legend'),c);
      l= c(find(l));
% 
      pos_= [ 0.82    0.1100    0.1560    0.8150];
      set(l,'position',pos_);
      name_= ['news' num2str(data.news_id)];
      if inhib_type==1
          name_=['I' name_];
      end
      my_buffer(name_,gcf);     
     

%% internals
function data= get_non_inhibited_model(model_options,sec_id)
    o=model_options.get();
    oo= options();
    oo.set(o{:});
    assert(str2num(oo.get('inhibition'))==1);
    if ~strcmpi(oo.get('noter'),'none')
        oo.set('inhibition',0);
    end
    oo= oo.get();
    tbx= newsProjectTbx();
    IO= fileToolbox(tbx.FILEconfig.folder(sec_id,'stock'));
    try;data= IO.loadSingle(oo{:});
    catch ME; % SOLUTION SALE il vaudrait mieux indexer inhib0 par learning_freq
       oo= options(oo) ;
       oo.remove('learning_frequency');
       oo= oo.get();
       data= IO.loadSingle(oo{:});
    end
    data= data{1}.savedVariable;

function info=info_rank_1stock(pred,B,R)    
    info.model= B.opt.get();
    
    info.data= {'security',[B.NAMEstock ' ' num2str(B.IDstock) ],'TD',R.volume.destinationID,...%R.IDdestination ,...
    'nb of observations',num2str(size(B.value,1)) ,'from',datestr(B.date(1)),'to',datestr(B.date(end))};
    
    info.model= {info.model{:}};

    %
            
    %[n_ctxt,n_dt]=size(pred);
    %n_ctxt= sum(pred(:,2)==0);
    n_ctxt= size(pred,1); %length(pred(:,2));
    stats={};
    nn= newsDb(1);
    c_names= nn.id2name(pred(:,2));
    for n=1:n_ctxt
        stats{n,1}= n;
        stats{n,2}= c_names{n};
        %stats{n,3}= pred(n,2);
        stats{n,3}= num2str(pred(n,2)); % no decimal ?
        idx= B.BN == pred(n,3);
        stats{n,4}= mean(B.value(idx,4));   
        if pred(n,3)==0
            stats{n,5}= nan;
        else
            idx2= B.news.value(:,pred(n,3));
            stats{n,5}= mean(B.value(idx2==1,4));
        end            
        if ismember(pred(n,3),B.inhibitedCtxt);
            stats{n,6}= 'YES';
        else;stats{n,6}= 'no';end
        if ~isfield(R,'note') 
            stats{n,7}= 'na';
        elseif length(R.note)< n
            assert(n-length(R.note)==1)
            stats{n,7}= 'na';
        else
            stats{n,7}= R.note(n);
        end
        stats{n,8}= num2str(mean(B.BN== pred(n,3)));
        if pred(n,3)==0
            stats{n,9}= 'na';
        else
            stats{n,9}= num2str(mean(B.news.value(:,pred(n,3))));
        end
    end    
    info.stats= stats;
    info.colnames= {'rank','Event_name','Event_id','O.C. when selected', 'O.C. when occurs',...
        'inhibited','learning note','nb. of selection','nb. of observation'};
    
    
    
    
%%
      
     
function TEST() 
    t=test_learning_output();        
    ip= indexProcess('CAC40');
    
    
    
    ip.comp=2;
    ip.TD=[];4;
    ip.dates= [733043,734319];
    t.make_report(ip);
    
    t.show_performance(); % clear persist variables
    
    my_buffer('clear');
    
    [failed,x_]= t.show_performance(ip,1,0); % OK       % 3 graphes 
    
    [D,b,base,failed]= contexte_learning(ip.comp(:));
    base.value= base.num;
    [R,N]= t.show_average_ranks(base,1) % 1 G
    
    Y= t.show_ranks(base,1) %1G               
    %res01= gatherResults(ip.comp(:)',ip.useInhibition,ip.final_);
    
    res01= gatherResults(ip.comp(:)',1,0);
    
    
    [X,keys,X0,key0]= t.show_co(res01.alldata,1)  ; % 2G
    
    [X,keys,X0,key0]= t.show_co(res01.alldata,1)  ;
    [X,keys,X0,key0]= t.show_co(res01.alldata,1,0,1)  ;
    
    [pred,B,R,info]= t.plot_predictors(2)
    
    
    b=my_buffer();
    b.get()
     
    ns_figure;
    
    [n_ctxt,n_time]= size(pred);n_time=n_time-3;    
    matrixplot(pred(:,4:end),30,'nobone');
    
    ax= get(gcf,'children');
    f= find(strcmpi(get(ax,'tag'),'Colorbar'));
    ax= ax(f);
    l= get(ax,'yticklabel');
    l= cellfun(@(x) num2str(exp(str2num(x)),2), cellstr(l),'uni',0)
    set(ax,'yticklabel',l);
    
    hold on
    M= exp(max(max((pred(1,4:end)))));
    for n_= 1:n_ctxt
        plot(0.5+(1:n_time),0.9+n_-1/M*exp(pred(n_,4:end)),'color','white','Marker','+','linewidth',2,'linestyle','--');        
    end
    
%%
p= t.atypie(ip)
pp= p(:,5:end);rank(pp)
[P,Q]= size(pp);
lbls={};
for p_=1:P
    lbls{p_}= [num2str(p(p_,3)) '.' num2str(p(p_,1))];
end

d=[];
for p_=1:P; for q_=1:P
        %d(p_,q_)=  sqrt(sum(abs(pp(p_,:)/max(pp(p_,:))-pp(q_,:)/max(pp(q_,:))).^2)) ;%mean(abs(pp(p_,:)-pp(q_,:))); %./pp(p_,:)); %max(abs(cumsum(pp(p_,:))-cumsum(pp(q_,:)))./cumsum(pp(p_,:)) ); %sum(abs(pp(p_,:)-pp(q_,:)));  %
        %d(p_,q_)=  sqrt(sum(abs(pp(p_,:)-pp(q_,:)).^2)) ;
        %d(p_,q_)=  mean(abs(pp(p_,:)-pp(q_,:))); %./pp(p_,:)); %max(abs(cumsum(pp(p_,:))-cumsum(pp(q_,:)))./cumsum(pp(p_,:)) ); %sum(abs(pp(p_,:)-pp(q_,:)));  %
        %d(p_,q_)= max(abs(cumsum(pp(p_,:))-cumsum(pp(q_,:))));
        [~,i1]= sort(pp(p_,:),'ascend');
        [~,i2]= sort(pp(q_,:),'ascend');
        idx_=[1:5,length(i1)-5+1:length(i1)];
        d(p_,q_)= sum(abs(i1(idx_)-i2(idx_)));
    end;end

% n_ppv= floor(0.2* P);
% aty= [];
% for p_=1:P;
%     v= sort(d(p_,:),'ascend');
%     %v= v(v>0);
%     v= mean(v(1:n_ppv));
%     %v= quantile(v,.25)
%     aty(p_)=v;
% end


n_ppv= floor(0.2* P);
aty= [];
for p_=1:P;
    idx_= p(:,3)== p(p_,3);
    v= sort(d(p_,idx_)/mean(d(p_,idx_)),'ascend');
    %v= v(v>0);
    %v= mean(v( 1:(floor(0.2 *sum(idx_)))  ));
    v= quantile(v,.1)
    aty(p_)=v;
end

[~,i]= sort(aty,'descend');
ppp= p(i(1:n_ppv),1:4)

i= find(sum(abs(bsxfun(@plus,p(:,1:4),-ppp(4,:))),2)==0)
plot(pp(i,:))

hist(ans(:,3),30)

[a,pc]= ACPrincipale(pp,'R');
%pc.plan(a,1,2,lbls)
pc.plot(a)
pc.planObs(a,1,2,lbls)

function latex_()
    fn ='report.html';
     latex('header', fn, 'title', 'contextual volume forecast : learning reporting', 'author', 'Nathana?l Mayo')

     
     latex('section', fn, 'Global performances')
     
     latex('graphic', fn, 'graph_handle', gcf, 'scalebox', ...         
        0.75,  'close', true, ...
         'fullscreen', false) 
      
      latex('end', fn)
     
      m=[1 2 3; 4 5 6];
      latex('array', fn, m, 'col_header', {'a','b','c'},  'linesHeader', {'1','2'})
      
      
      latex('html',fn);
     latex('compile', fn)
        
        
        
        
        