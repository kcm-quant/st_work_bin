%% Test des principales �tapes de ContextLearning

sec_id= 2;
%% 1/ Learning

n= newsDb();
%% 2/ Mise en base (v.pipo)
global base
[D,b,base,failed]= contexte_learning(2,1);

%% 3 / Update Predictors

args_={10735, {} , 'day', 1000, '01/01/2011',[]};
%[vol,news,R,B,pred,log_,pred_freq_l]pred
pred=se_run_context_volume(args_{:});

chartableau(pred.update_info.news_.colnames,sum(pred.update_info.news_.value))
pred.update_info.quality_update./pred.update_info.quality_ref
pred.update_info.log{:}
pred.update_info.R
%


lgd_= n.id2name(str2num(char(pred.colnames)));
i= ismember(lgd_,pred.update_info.R.news.colnames(pred.update_info.R.ranks));
i= find(i);
for k=(i(:)')
   lgd_{k}= ['----- ' lgd_{k}];
end
lgd_ = chartableau(pred.colnames',lgd_);

%select_bins= @(cname) strcmp(cname(1:5),'slice') | strcmp(cname(1:8),'auction_');
select_bins= @(cn) cellfun(@(x)...
    (length(x)>=5 &&strcmp(x(1:5),'slice')) ||(length(x)>=8 && strcmp(x(1:8),'auction_')) ,...
    cn) ;
select_bins(pred.rownames)
figure,
subplot(2,1,1);
plot(pred.value(select_bins(pred.rownames),:));
legend(lgd_);
subplot(2,1,2);
x_= pred.value; x_(isnan(x_))=0;
plot(cumsum(x_(select_bins(pred.rownames),:),1));

plot(pred.update_info.pred_freq_l);legend(lgd_);

plot(pred.update_info.vol_.value')



[pv,p_]= se_check_context_volume(pred, 'day', 500, '01/01/2010',[])

vol= pred.update_info.vol_;the_med= median(vol.value,1);
the_med= bsxfun(@times,the_med,1./sum(the_med))

figure, hold on
plot(pred.update_info.vol_.value(pred.update_info.news_.value(:,9)==1,:)')
plot(pred.update_info.pred_freq_l(:,1),'r-.')
plot(the_med,'k-.')

mean(sum(bsxfun(@plus,pred.update_info.vol_.value(pred.update_info.news_.value(:,9)==1,:)',-pred.update_info.pred_freq_l(:,1))))
mean(sum(bsxfun(@plus,pred.update_info.vol_.value(pred.update_info.news_.value(:,9)==1,:)',-the_med')))


% %% NN pred
% nn= model2str(base{1,4})
% tbx= prevNoNews();
% 
% f= nn.get('nnfh');
% f= func2str(f);
% eval(['g=' f])
% %g(1,2)
% 
% pred= @(x) g(x, num2str(nn.get('nnp1')))
% 
% pars= [(nn.get('nnp1')),(nn.get('nnp2')),(nn.get('nnp3'))]
% 
% pred(randn(1,1000))
% 
% p= @(data,bn) tbx.predict(data,bn,pred,pars(:));
% 
% p(randn(1000,10))

%% fill entire table
DB=contexte_volume_dbIO();
ip= indexProcess('CAC40');
for sec_id= (ip.comp(:)')
    [D,b,base,failed]= contexte_learning(sec_id);
    Z=[];
    Z.value=base.num;
    Z.colnames=  {'security_id'  'context'  'rankings'};
    Z.no_news_string= base.model{1}{2};
    Z.security_id= sec_id;
    Z.trading_destination_id= {};
    DB.set(Z)
end

% ip.comp = ip.comp(ip.comp < 41) % ce titre n'est pas associ� dans
[D,b,base,failed]= contexte_learning(ip.comp(:));
base.write()
Z=[];
Z.value=base.num;
Z.colnames=  {'security_id'  'context'  'rankings'};
Z.no_news_string= base.model{1}{2};
Z.security_id= sec_id;
Z.trading_destination_id= {};
DB.set(Z)
