function [dayExtrap,disp_from,suppr_idx]=treat_market_time(data0_conti,t0_all,t1,p0_conti,p1,  disp_from,vol,ta,pa,idx,p)
% si une phase continue a �t� dispatch en dehors de ses begin/end, on
% enleve les new-freq slices correspondants
% t1 contient les fins des slices, du continu seulement
%
% Cela revient � dispatcher 9:00/9:15 sur 13 bins lorsque le march� ouvre �
% 9:02. Du coup on ne va plus faire ca, on va extrapoler comme si le march�
% �tait ouvert, puis d�tecter les bins pr�vus inexistants sur le march� et
% les redispatcher !
%
%

%[idx,t1,p1,ta,pa]= create_subbin(old_freq/24/60,mod(datenum(vol.colnames),1),vol.phases,new_freq/24/60);    

[no_use,phase_id,no_use,from_phase_id_to_phase_name] = trading_time_interpret([],[]);
I_open= find(pa==phase_id.OPEN_FIXING);
if isempty(I_open)
    warning('pas d open auction !!!');
end
I_close= find(pa==phase_id.CLOSE_FIXING);
if isempty(I_close)
    warning('pas de close auction !!!');
end

t0= t0_all(idx);

beg_= [ta(I_open),t1];
end_= [t1,ta(I_close)];
beg_= beg_(1:end-1);
end_= end_(1:end-1);

epsi= 1/24/60/60*5;

N= length(end_);
assert(length(beg_)==N);
suppr_idx=[];
CONTI_beg= vol.begin_.value;
CONTI_beg= CONTI_beg(idx);
CONTI_end= vol.end_.value;
CONTI_end= CONTI_end(idx);
for n=1:length(t1)
    b= CONTI_beg(disp_from(n));
    e= CONTI_end(disp_from(n));    
    if end_(n) < b+epsi
        suppr_idx(end+1)= n;
     elseif beg_(n) > e-epsi
        suppr_idx(end+1)= n;
    end
end

% suppression des bin hors march� continu
t1(suppr_idx)= [];
p1(suppr_idx)= [];
[dayExtrap,disp_from]= dispatchSubbin_v2(data0_conti,t0,t1,p0_conti,p1);  

%[dayExtrap,disp_from]= dispatchSubbin_v2(p(idx),mod(datenum(vol.colnames(idx)),1),t1,vol.phases(idx),p1);    % dispatch phase continue     
    