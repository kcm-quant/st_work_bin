function [dayExtrap,volume_manquant]=treat_market_time_v2(prev,real, aligned_volume)
% treat_market_time_v2 - Aligne un pr?dicteur (prev) sur les donn?es r?alis?es (real)
%
% < Inputs >
%  * real := st_data (with phases field and some 'volume' colnames )
%  * prev := struct with fields ('date','value') : continuous phase times and data
%                               ('NC','NCp')     : non continuous data and phases
% - Both arguments are at the same frequency (See also dispatchSubbin_v2),
%   but have not necessarily same length
% - prev and real only contain one day
%
% < Outputs >
%  - dayExtrap is the predictor pred, aligned on real (sums to 1)
%  - volume_manquant is the missing volume that has been renormalized
%
% <Alignement>
% * Non continous phases are matched,
% * If b in setdiff(prev.bins, real.bins), data of prev in b is lost (cannot be aligned)
%           ==> Extrapolated data are renormalized
% * If b in setdiff(real.bins , prev.bins), b is left with nan -> throws error
%   But if the third argument <aligned_volume> is given (in learning only, not se_run estimation)
%   aligned_volume.changing_continuous_bins are allowed to be nan, in which
%   case :
%      - nan are replaced with 0 -> critere op. is impacted
%      - [CHOSEN] nan are replaced with real data -> critere op. is not impacted
%                 but changing_continuous_bins are simply and entirely lost
%                     
%       DONE: only non changing_continuous_bins should normalized, by the sum of NCCB

% * rajoute les bins non continus ? l'interpolation faite par dispatchSubbin_v2
% * si une phase continue a ?t? dispatch?e en dehors de ses begin/end, on
% enleve rer?partit le volume sur la journ?e
% * Il faut aussi traiter les auctions pr?vues inexistantes
% * identifier les bins diff?rents. Il peut manquer des bins dans real, mais
% pas le contraire

if nargin<3; aligned_volume= [];end

ini_somme= sum(prev.value);

idx= find(real.phases==-1);
if ~isempty(idx)
   warning('il reste des phase==-1 dans le buffer');
   real.phases(idx)= 0;
end

iCOL= strcmpi(real.colnames,'volume');
real.date= mod(real.date,1);
[no_use,phase_id,no_use,from_phase_id_to_phase_name] = trading_time_interpret([],[]);
%I_open= find(pa==phase_id.OPEN_FIXING);
%[dayExtrap,disp_from]= dispatchSubbin_v2(p(idx),mod(datenum(vol.colnames(idx)),1),t1,vol.phases(idx),p1);    % dispatch phase continue     
idx= real.phases== phase_id.CONTINUOUS;

% D?tecte les bins conserv?s
z= abs(bsxfun(@minus,real.date(idx),prev.date'));
[a,b]= min(z');
assert(min(diff(b))>=0.9999999);

% Remplit les bons bins continus. La somme ne fait plus 1 si on a perdu des bins
E= nan(size(real.value,1),1);
E(idx)= prev.value(b);

% Remplit les bins non continus, qu'ils existent ou pas aujourd'hui
up= setdiff(unique(real.phases),phase_id.CONTINUOUS);
for p=(up(:)')
   i= find(real.phases== p) ;
   if ~isempty(i)
        assert(length(i)<=1);
        j= find(prev.NCp == p);
        assert(length(j)==1);
        E(i) = prev.NC(j);        
   end        
end


% OV: assert(all(~isnan(E)));
if isempty(aligned_volume) | all(~isnan(E))
    assert(all(~isnan(E)),'Nan values in treat_market_time_v2 (didnt check changing_continuous_bin)');
    volume_manquant= (1-sum(E));% / ini_somme;
    dayExtrap= E/sum(E);%* ini_somme;
    % somme diff?rente de 1 dans le else !
    assert(abs(sum(dayExtrap)-1)< 0.000001);  
else
    %% patch <changing_continuous_bins>
    E0=E;
    % La somme fait 1 seulement sur les bins non nans. Le reste n'est la
    % que pour ne pas impacter le critOp
    
    volume_manquant= (1-nansum(E));
    dayExtrap= E/nansum(E);
    assert(abs(nansum(dayExtrap)-1)< 0.000001);
    
    inan= isnan(E);
    igt= bsxfun(@gt,mod(real.date(inan),1),aligned_volume.changing_continuous_bin(:,1)');
    ile= bsxfun(@le,mod(real.date(inan),1),aligned_volume.changing_continuous_bin(:,2)');
    iph= bsxfun(@eq,real.phases(inan),aligned_volume.changing_continuous_bin(:,3)');
    expected_nans= sum(igt&ile&iph,2); % si c'est >0, le bin fait partie des changing_continuous_bin
    assert( unique(expected_nans)==1 ); 
    % * Erreur si 2 changing_continuous_bins diff?rents sont match?s !
    % * Erreur si un nan n'est pas retrouv? dans les changing_continuous_bins
    ivolume= find(strcmpi('volume',real.colnames)); assert(length(ivolume)==1);
    dayExtrap(inan) = real.value(inan,ivolume) /sum(real.value(:,ivolume)); %relatif
    assert(all(~isnan(dayExtrap)),'Nan values in treat_market_time_v2, after checking changing_continuous_bin!');
    
end


%% Renormalisation
% -> on renormalise (plutot que rer?partir les volume en moins sur ts les bins)
%idx_rerepartir= 1:length(E)
% if 0 % real.is_auction_today
%     idx= find(real.phases== phase_id.ID_FIXING2) ; % IA !!!!
%     assert(length(idx)==1);
%     %volume_manquant= volume_manquant + E(idx);    
%     E(idx)= 0;
% end


assert(all(~isnan(dayExtrap)))
assert(all(length(dayExtrap)==size(real.value,1)));




