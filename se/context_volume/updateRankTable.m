function self= updateRankTable(stockID,modelID)
% met le mod�le en base
% arg= name, ou mod�le lui m�me ()
%
% modelID= {'par',val,...}



self= loadData(stockID,modelID{:});%'dim',36);


function main()
    a= {'Npoint',10,'sbreak',0,'noter','pv','dim',36,'seuil',0.9,'estimateur','mean'};
    urt= updateRankTable(2,a);



function self= loadData(stockID, varargin)       
    ep= eModelPool();
    ep= ep.inidata(ep,stockID);        
    self= ep.getAllErrors(ep,varargin{:});       
    assert(length(self)==1);
    assert(self.savedVariable.IDstock== stockID);

    tbx= newsProjectTbx();
    F= tbx.FILEconfig.folder(self.savedVariable.IDstock,'stock');  
    f= [F, self.savedVariable.filetrace.get('rankLoad')];
    u= load(f);
    self.savedVariable.ranks= u.savedVariable.ranks;
    

