function self= vwap_order_weights()
persistent vwap_order_weights_glob_data;
global st_version;
if isempty(vwap_order_weights_glob_data)
    disp('init');
    self= [];
    self.data= [];
    self.range=[];
    self.w=[];
    self.load= @load_;
    self.init= @init;
    self.this= @this;
    self.getW= @getW;
    self.clear= @clear_;
    
    self.load();
    self.init();
    vwap_order_weights_glob_data= self;
else
    self= vwap_order_weights_glob_data;
end


    function x= this();x= self;end
        
    function load_()
        MAIN_EXCHGID = {'SEAS', 'SEPA', 'SEFE', 'SETS'};
        %char(arrayfun(@get_td_name,main_markets,'uni',0))
        %pth= fullfile()
        pth= fullfile(st_version.my_env.st_repository, 'news_project', 'pp_vwap.mat' );
        x=load(pth); % FIXME Y:\\ 
        x= x.all_pp_vwap;
        self.data= filterstruct(x,...
            ismember(x.main_td,MAIN_EXCHGID),[],...
            {'all_sec_ids','all_td_info','idx2del'});        
    end

    function init()    
        % passer en relatif � la journ�e de trading [0,1]
        cth= self.data.continuous_th;
        d= self.data.begin_end(:,1);
        d= (d-cth(:,1)) ./ (cth(:,2)-cth(:,1) );
        self.range.b= [0.9*min(d),(1.1*max(d)-0.9*min(d))/10];
        Zb= bsxfun(@gt,d,linspace(0.9*min(d),1.1*max(d),10) );
        Zb= sum(Zb,2);
        e= self.data.begin_end(:,2);
        e= (e-cth(:,1)) ./ (cth(:,2)-cth(:,1) );
        self.range.e= [0.9*min(e),(1.1*max(e)-0.9*min(e))/10];
        Ze= bsxfun(@gt,e,linspace(0.9*min(e),1.1*max(e),10) );
        Ze= sum(Ze,2);
        l= e-d;
        Zl= bsxfun(@gt,l,linspace(0.9*min(l),1.1*max(l),10) );
        Zl= sum(Zl,2);
        self.range.l= [0.9*min(l),(1.1*max(l)-0.9*min(l))/10];
        
        for k=1:10
           self.w.b(k)= mean(Zb== k-1);
           self.w.e(k)= mean(Ze== k-1);
           self.w.l(k)= mean(Zl== k-1);           
        end
        
        for k=1:10
           self.w.b(k)= sum(self.data.euro_turnover(Zb== k-1));
           self.w.e(k)= sum(self.data.euro_turnover(Ze== k-1));
           self.w.l(k)= sum(self.data.euro_turnover(Zl== k-1));           
        end
        self.w.b=self.w.b/ sum(self.w.b);
        self.w.e=self.w.e/ sum(self.w.e);
        self.w.l=self.w.l/ sum(self.w.l);
        
        self.w.o= mean(self.data.use_fixing(:,1));
        self.w.o= [1-self.w.o,self.w.o];
        self.w.c= mean(self.data.use_fixing(:,2));   
        self.w.c= [1-self.w.c,self.w.c];
        assert(abs(sum(self.w.b)-1)<0.0001);
        assert(abs(sum(self.w.e)-1)<0.0001);
        assert(abs(sum(self.w.l)-1)<0.0001);
    end


%     function init()    
%         % passer en relatif � la journ�e de trading [0,1]
%         cth= self.data.continuous_th;
%         d= self.data.begin_end(:,1);
%         d= (d-cth(:,1)) ./ (cth(:,2)-cth(:,1) );
%         self.range.b= [0.9*min(d),(1.1*max(d)-0.9*min(d))/10];
%         Zb= bsxfun(@gt,d,linspace(0.9*min(d),1.1*max(d),10) );
%         Zb= sum(Zb,2);
%         e= self.data.begin_end(:,2);
%         e= (e-cth(:,1)) ./ (cth(:,2)-cth(:,1) );
%         self.range.e= [0.9*min(e),(1.1*max(e)-0.9*min(e))/10];
%         Ze= bsxfun(@gt,e,linspace(0.9*min(e),1.1*max(e),10) );
%         Ze= sum(Ze,2);
%         l= e-d;
%         Zl= bsxfun(@gt,l,linspace(0.9*min(l),1.1*max(l),10) );
%         Zl= sum(Zl,2);
%         self.range.l= [0.9*min(l),(max(l)-0.9*min(l))/10];
%         
%         for k=1:10
%            self.w.b(k)= mean(Zb== k);
%            self.w.e(k)= mean(Ze== k);
%            self.w.l(k)= mean(Zl== k);           
%         end
%         
%         self.w.o= mean(self.data.use_fixing(:,1));
%         self.w.o= [1-self.w.o,self.w.o];
%         self.w.c= mean(self.data.use_fixing(:,2));   
%         self.w.c= [1-self.w.c,self.w.c];
%         assert(abs(sum(self.w.b)-1)<0.0001);
%         assert(abs(sum(self.w.e)-1)<0.0001);
%         assert(abs(sum(self.w.l)-1)<0.0001);
%     end

    function [W,b,e,l,o,c]= getW(b,e,l,o,c)
        % vectoriser !!!
        kb= 1+floor( (b-self.range.b(:,1))/ self.range.b(:,2) );
        b= self.w.b(kb);
        ke= 1+floor( (e-self.range.e(:,1))/ self.range.e(:,2) );
        e= self.w.e(ke);
        kl= 1+floor( (l-self.range.l(:,1))/ self.range.l(:,2) );
        l= self.w.l(kl);
        c= self.w.c((c==1)+1);
        o= self.w.o((o==1)+1);  
        W= mean([b;e;l;c;o],1);
    end

    function clear_()       
        %global vwap_order_weights_glob_data;
        %clear vwap_order_weights_glob_data;
        vwap_order_weights_glob_data=[];
    end

end


%%

function test2()

V= vwap_order_weights();
V= vwap_order_weights();
[W,b,e,l,c,o]= V.getW(10/24,16/24,6/24,1,0);

[W,b,e,l,c,o]= V.getW(0/24,1/24,1/24,0,1);

V.clear();
end

function test()
vwap_order_weights_data= vwap_order_weights('init');
vwap_order_weights_data.load();
vwap_order_weights_data.init();
%V=V.this()
[W,b,e,l,c,o]= V.getW(10/24,16/24,6/24,1,0);

[W,b,e,l,c,o]= vwap_order_weights_data.getW(10/24,16/24,6/24,1,0);
end