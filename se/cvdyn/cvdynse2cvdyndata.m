function cvdyndata = cvdynse2cvdyndata(cvdynse)
% cvdynse2cvdyndata - passage d'une sortie du SE cvdyn (ou d'une sortie de st_vc) � un st data dat� utilis� pour le calcul du volume temps r�el
%
% EXAMPLE 1: utiliser � partir d'un se_run
% sec_id=get_sec_id_from_ric('CSGN.VX');
% cvdynse=se_run_cvdyn(sec_id,[],'day',180,'20/01/2010');
% cvdyndata = cvdynse2cvdyndata(cvdynse)
% dynamic_vc= cvdyndata;
% list_msafp = {
%     {'std_ne', {'std'}},...
%     {'gaussian curve 2',{'std',2,'quant0',1}}, ...
%     {'gaussian curve 2',{'composite 1',2,0.1,0.2,'curve_ref',30,'emaLength',4,'quant0',1,'meanNb05m',10,'pctMedian05m',0}},...
%     };
% out=compute_volume_proportion_forecast('security_id',sec_id,'trading_destination_id',[],'date','20/01/2010','forecast_params_list',list_msafp,'cvdyn_volume_curve',dynamic_vc);
%
% EXAMPLE 2: utiliser � partir d'un se_get_prama(run_id)
%  change_connections('production')
%  sec_id=556;
%  cvdynse=se_get_param(887014);
%   change_connections('dev')
% cvdyndata = cvdynse2cvdyndata(cvdynse);
% dynamic_vc= cvdyndata;
% list_msafp = {
%     {'std_ne', {'std'}},...
%     {'gaussian curve 2',{'std',2,'quant0',1}}, ...
%     {'gaussian curve 2',{'composite 1',2,0.1,0.2,'curve_ref',30,'emaLength',4,'quant0',1,'meanNb05m',10,'pctMedian05m',0}},...
%     };
% out=compute_volume_proportion_forecast('security_id',sec_id,'trading_destination_id',[],'date','20/01/2010','forecast_params_list',list_msafp,'cvdyn_volume_curve',dynamic_vc);
% REMARQUE : fonctionne uniquement sur un seul context (le 'Usual Day')
 
 
bin_duration_cvdyn_se=datenum(0,0,0,0,30,0);
bin_duration_ref=datenum(0,0,0,0,5,0);

param_name_from_se = cvdynse.rownames;
param_value_from_se = cvdynse.value;


nb_params = length(param_name_from_se);

%-----------------------------
% - on recupere le vecteur des slices en bin_duration_cvdyn_se 
slices_b = false(nb_params, 1);
for i = 1 : nb_params
    if length(param_name_from_se{i}) > 5 && strcmp(param_name_from_se{i}(1:5), 'slice')
        slices_b(i) = true;
    end
end
eofs = param_value_from_se(strmatch('end_of_first_slice', param_name_from_se), 1);
eols = param_value_from_se(strmatch('end_of_last_slice', param_name_from_se), 1);

slices_values = param_value_from_se(slices_b, :);
slices_date = NaN(length(param_value_from_se(slices_b, :)), 1);
j = 1;
for i = 1 : nb_params
    if slices_b(i)
        slices_date(j) = min(eols,eofs + (str2double(param_name_from_se{i}(7:end))-1)*bin_duration_cvdyn_se);
        j = j + 1;
    end
end
auc_param_name = {'auction_open', 'auction_mid', 'auction_close'};
auc_colnames = {'opening_auction', 'intraday_auction', 'closing_auction'};
for i = 1 : length(auc_param_name)
    idx = strmatch(auc_param_name{i}, param_name_from_se, 'exact');
    if ~isempty(idx)
        idx_h = strmatch([auc_param_name{i}, '_closing'], param_name_from_se, 'exact');
        if ~isempty(idx_h)
            slices_values(end+1, :) = [param_value_from_se(idx, :) false(1, size(slices_values, 2)-1)];
            slices_values(:, end+1) = [false(size(slices_values, 1)-1, 1);true];
            slices_date(end+1, :) = param_value_from_se(idx_h, 1);
        else
            error('se_check_volume_curve:check_params_consistency', 'Found one auction parameter without any stamp_time : <%s>', auc_param_name{idx});
        end
    else
        slices_values(:, end+1) = false(size(slices_values, 1), 1);
    end
end

[slices_date, idxes] = sort(slices_date);
slices_values = slices_values(idxes, :);

%-----------------------------
% - on recupere le vecteur des slices en bin_duration_ref
nb_bin_ref=round(bin_duration_cvdyn_se/bin_duration_ref);

bins_idx=NaN(size(slices_values,1),2);
idx_open=any(slices_values(:,end-2),2);
idx_close=any(slices_values(:,end),2);


% on scinde le cas du dernier slice, qui ne contient pas forc�ment
% nb_bin_ref slice de duree bin_duration_ref
% --- ancienne version sans les nb_bins_...
% idx_continuous=find(~any(slices_values(:,end-2:end),2));
% if any(idx_open)
%     bins_idx(1,:)=[1 1];
%     bins_cont=cat(2,2+6*[0;transpose(1:length(idx_continuous)-2)],2+nb_bin_ref-1+nb_bin_ref*[0;transpose(1:length(idx_continuous)-2)]);
%     bins_idx(2:idx_continuous(end-1),:)=bins_cont;
%     bins_idx(idx_continuous(end),1)=bins_cont(end,1)+nb_bin_ref;
% else
%     bins_cont=cat(2,1+6*[0;transpose(1:length(idx_continuous)-2)],1+nb_bin_ref-1+nb_bin_ref*[0;transpose(1:length(idx_continuous)-2)]);
%     bins_idx(idx_continuous(end-1),:)=bins_cont;   
%     bins_idx(idx_continuous(end),1)=bins_cont(end,1)+nb_bin_ref;
% end
% start_last_cont_slice=slices_date(idx_continuous(end-1));
% nb_bin_last_cont_slice=ceil((slices_date(idx_continuous(end))-datenum(0,0,0,0,0,10)-start_last_cont_slice)/bin_duration_ref);
% bins_idx(idx_continuous(end),2)=bins_idx(idx_continuous(end),1)+nb_bin_last_cont_slice-1;
% if any(idx_close)
%     bins_idx(end,:)=[1+bins_idx(end-1,2) 1+bins_idx(end-1,2)];
% end

% --- nouvelle version avec les nb_bins_...
nb_bins_first_slice=param_value_from_se(strmatch('nb_bins_first_slice', param_name_from_se));
nb_bins_last_slice=param_value_from_se(strmatch('nb_bins_last_slice', param_name_from_se));

if any(idx_open) && any(idx_close)
    nb_slice_cont=size(slices_values,1)-2;
    bins_idx(1,:)=[1 1];
    bins_idx(2,:)=[2 2+nb_bins_first_slice-1];
    
    bins_idx(3:end-1,1)=transpose((2+nb_bins_first_slice):nb_bin_ref:(2+nb_bins_first_slice+nb_bin_ref*(nb_slice_cont-2)));
    bins_idx(3:end-2,2)= bins_idx(3:end-2,1)+nb_bin_ref-1;
    bins_idx(end-1,2)=bins_idx(end-1,1)+nb_bins_last_slice-1;
    bins_idx(end,:)=[bins_idx(end-1,2)+1 bins_idx(end-1,2)+1];
elseif any(idx_open)
    nb_slice_cont=size(slices_values,1)-1;
    bins_idx(1,:)=[1 1];
    bins_idx(2,:)=[2 2+nb_bins_first_slice-1];
    
    bins_idx(3:end,1)=transpose((2+nb_bins_first_slice):nb_bin_ref:(2+nb_bins_first_slice+nb_bin_ref*(nb_slice_cont-2)));
    bins_idx(3:end-1,2)= bins_idx(3:end-1,1)+nb_bin_ref-1;
    bins_idx(end,2)=bins_idx(end,1)+nb_bins_last_slice-1;
elseif any(idx_close)
    nb_slice_cont=size(slices_values,1)-1;
    bins_idx(1,:)=[1 1+nb_bins_first_slice-1];
    bins_idx(2:end-1,1)=transpose((1+nb_bins_first_slice):nb_bin_ref:(1+nb_bins_first_slice+nb_bin_ref*(nb_slice_cont-2)));
    bins_idx(2:end-2,2)= bins_idx(2:end-2,1)+nb_bin_ref-1;
    bins_idx(end-1,2)=bins_idx(end-1,1)+nb_bins_last_slice-1;
    bins_idx(end,:)=[bins_idx(end-1,2)+1 bins_idx(end-1,2)+1];
else
    nb_slice_cont=size(slices_values,1);
    bins_idx(1,:)=[1 1+nb_bins_first_slice-1];
    bins_idx(2:end,1)=transpose((1+nb_bins_first_slice):nb_bin_ref:(1+nb_bins_first_slice+nb_bin_ref*(nb_slice_cont-2)));
    bins_idx(2:end-1,2)= bins_idx(2:end-1,1)+nb_bin_ref-1;
    bins_idx(end,2)=bins_idx(end,1)+nb_bins_last_slice-1;
end



nb_bins_vector=bins_idx(:, 2)-bins_idx(:, 1) +1;
slices_values_ref = [];
slices_date_ref = [];
for i = 1 : length(bins_idx)
    slices_values_ref = cat(1, slices_values_ref, repmat(slices_values(i,1)-log(nb_bins_vector(i)),nb_bins_vector(i),1));
    if nb_bins_vector(i)==1
    slices_date_ref = cat(1, slices_date_ref, ...
        slices_date(i));
    else
    slices_date_ref = cat(1, slices_date_ref, ...
        slices_date(i)-transpose((nb_bins_vector(i)-1:-1:0)*bin_duration_ref));
    end
end


slices_values_ref=cat(2,slices_values_ref,repmat(0,size(slices_values_ref,1),3));
if any(idx_open)
    slices_values_ref(1,end-2)=1;
end
if any(idx_close)
    slices_values_ref(end,end)=1;
end

%-----------------------------
% - on recupere la matrice de covariance
nb30MinPeriod=size(slices_values,1);
reconstructCovQuant0=NaN(nb30MinPeriod,nb30MinPeriod);

for i_row=1:nb30MinPeriod
    for j_row=i_row:nb30MinPeriod
        cov_name_tmp=sprintf('cov_%s_%s',idxToChar(i_row),idxToChar(j_row));
        idx_cov_name_tmp= strcmpi(param_name_from_se, cov_name_tmp);
        reconstructCovQuant0(i_row,j_row)=param_value_from_se(idx_cov_name_tmp, :);
        if i_row ~=j_row
            reconstructCovQuant0(j_row,i_row)=param_value_from_se(idx_cov_name_tmp, :);
        end
    end
end
    


%-----------------------------
% - on recupere forecast param
% - + on cr�e la structure critere (fake) pour el calculd e l'estimation
forecast_method=param_value_from_se(strmatch('forecast_method', param_name_from_se));
critere=[];
if forecast_method ==0
    critere.mean_nb_0=50;
    critere.nb_period_median0=1;
elseif  forecast_method ==1
    critere.mean_nb_0=0;
    critere.nb_period_median0=0;
end


%-----------------------------
% - on recupere le st_data de sortie






cvdyndata_se=st_data('init','title','',...
    'date',slices_date,...
    'value',slices_values,...
    'colnames',cat(2,'Usual day',auc_colnames));
cvdyndata_se.valueQuant0=slices_values; 
cvdyndata_se.model.critere=critere;
cvdyndata_se.model.covQuant0=reconstructCovQuant0; 
cvdyndata_se.model.cov=reconstructCovQuant0; 
cvdyndata_se.model.quant0.q=0.1;
cvdyndata_se.model.prop.med_prop=NaN;
cvdyndata_se.model.is_static=0;
cvdyndata_se.model.name='gc';       
cvdyndata_se.model.bins_idx=bins_idx;       
cvdyndata_se.model.forecast_method=forecast_method;   
cvdyndata_se.info.security_key='';



cvdyndata=st_data('init','title','',...
    'date',slices_date_ref,...
    'value',slices_values_ref,...
    'colnames',cat(2,'Usual day',auc_colnames));
cvdyndata.valueQuant0=slices_values_ref;  
cvdyndata.model.critere=critere;
cvdyndata.model.prop.med_prop=NaN;
cvdyndata.model.is_static=0;
cvdyndata.model.name='gc';  
cvdyndata.model.other_models=cvdyndata_se;
cvdyndata.model.other_models(end+1)=cvdyndata_se;




% cvdyndata = st_data('init', 'date', slices_date, 'value', slices_values, 'title', 'se volume curve', 'colnames', cat(2, cvdynse.colnames, auc_colnames));
% 
% if isfield( cvdynse, 'info')
%     cvdyndata.info = cvdynse.info;
% end
% cvdyndata.attach_format = 'HH:MM';
% 
% try
%     idx=strmatch('alpha', param_name_from_se, 'exact');
%     cvdyndata.info.ci.alpha = param_value_from_se(idx);
% catch
    
end

function char_idx=idxToChar(idx)

if idx<10
    char_idx=sprintf('00%d',idx);
elseif idx>=10 && idx<99
    char_idx=sprintf('0%d',idx);
else
    char_idx=sprintf('0d',idx);
end
end







