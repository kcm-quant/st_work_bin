function res=find_idx_slice(mode,varargin)
% FIND_IDX_SLICE : aims at finding the active slice to consider 
% More precisely, it is used in cvdyn codes (mode='cvdyn'), and in SIMAP codes (mode='sequence')
% %--- INPUT
% mode='cvdyn' : 'forecast params' + 'data_vc'
% mode='sequence' : 'fixing_booleans'+ 'slice_time' + 'start_time' + 'end_time' + 'is_auction'
% %--- OUPUT
% mode='cvdyn' : res= [index from start_slice, index from end_slice] 
% mode='sequence' res=cell array for each sequence with a vector of active indexes
%
% See also 
%
% author   : 'njoseph@cheuvreux.com'
% reviewer : ''
% version  : '...'
% date     : '21/10/2010'


opt  = options( {'forecast_params',{},...
    'data_vc',st_data('empty-init'),...
    'fixing_booleans',[],...
    'slice_time',[],...
    'start_time',[],...
    'end_time',[],...
    'is_auction',[],...
    }, varargin);


switch mode
    
    case 'cvdyn'
        
        forecast_params=opt.get('forecast_params');
        data_vc=opt.get('data_vc');
        % data_vc correspond au vc5m de vc_forecast
        start_slice=1;
        end_slice=length(data_vc.date);
        
        start_sliceIndex=find(strcmpi(forecast_params,'start_slice'));
        end_sliceIndex=find(strcmpi(forecast_params,'end_slice'));
        
        dates=data_vc.date;
        if any(dates>70000)
            dates=mod(dates,1);
        end
        
        % - attention dans vc_forecast (desfois), on a pas les donn�es d'auction !!!!
        idx_opening_auction=strcmp(data_vc.colnames,'opening_auction');
        if any(idx_opening_auction)
            opening_auction=data_vc.value(:,idx_opening_auction);
        else
            opening_auction=NaN(size(data_vc.value,1),1);
        end
        idx_closing_auction=strcmp(data_vc.colnames,'closing_auction');
        if any(idx_closing_auction)
            closing_auction=data_vc.value(:,idx_closing_auction);
        else
            closing_auction=NaN(size(data_vc.value,1),1);
        end

        
        if ~isempty(start_sliceIndex)
            start_slice_tmp=forecast_params{start_sliceIndex+1};
            
            %%%on a entr�e une date avec la partie du jour
            if start_slice_tmp>70000
                start_slice_tmp=mod(start_slice_tmp,1);
            end
            
            %%% case no_open
            if start_slice_tmp==0
                start_slice=2;
            elseif start_slice_tmp<1 %%% cas ou on donne l'horaire
                start_slice=find(dates>=start_slice_tmp,1,'first');
                if isempty(start_slice)
                    start_slice=1;
                elseif start_slice==1 && start_slice_tmp>dates(1)+datenum(0,0,0,0,0,5) && (isnan(opening_auction(1)) || opening_auction(1)==1) 
                    start_slice=2; %%% en effet, on demarre strictement apres l'auction d'ouverture
                end
            elseif start_slice_tmp>=1 %%% ca veu dire qu'on a direct entr�� le num�ro du slice dans forecast_params
                start_slice=start_slice_tmp;
            end
        end
        if ~isempty(end_sliceIndex)
            end_slice_tmp=forecast_params{end_sliceIndex+1};
            %%%on a entr�e une date avec la partie du jour
            if end_slice_tmp>70000
                end_slice_tmp=mod(end_slice_tmp,1);
            end
            
            %%% case no_close
            if end_slice_tmp<0
                end_slice=length(dates)+end_slice_tmp;
            elseif  end_slice_tmp==0
                end_slice=length(dates)-1;
            elseif end_slice_tmp<1 %%% cas ou on donne l'horaire
                end_slice=find(dates>=end_slice_tmp,1,'first');
                if isempty(end_slice)
                    end_slice=length(dates);
                elseif end_slice==length(dates) && end_slice_tmp<=dates(end)-datenum(0,0,0,0,0,1) && (isnan(closing_auction(end)) ||closing_auction(end)==1)
                    end_slice=length(dates)-1;
                end
            elseif start_slice_tmp>=1
                end_slice=end_slice_tmp;
            end
        end
        
        res=[start_slice end_slice];
        
        
        
        
        
    case 'sequence'
        
        
        %%% on gere ici si il y a plusieurs sequence !
        
        slice_time=opt.get('slice_time');
        start_time=opt.get('start_time');        
        end_time=opt.get('end_time');
        is_auction=opt.get('is_auction');
        fixing_booleans=opt.get('fixing_booleans');
        eps_dt=datenum(0,0,0,0,0,1);% on ajoute un temps de latence pour consid�rer un nouvel interval
        
        %%% test on stock slice
        if size(fixing_booleans,1)~=size(slice_time,1)
            error('find_idx_slice: not the same slice !!!');
        end
        if size(is_auction,1)~=size(start_time,1)
            error('find_idx_slice: not the same slice !!!');
        end
        
        %%% compute needed params
        start_time=start_time-floor(start_time);
        end_time=end_time-floor(end_time);
        
        %%% default_value
        res=cell(size(start_time,1),1);
        
        for i_seq=1:size(start_time,1)
            
            slice_sequence_tmp=repmat(nan,3,1);
            
            if any(fixing_booleans(:,1))
                if is_auction(i_seq,1)
                    slice_sequence_tmp(1)=find(fixing_booleans(:,1),1,'first');
                else
                    idx_first_tmp=find(slice_time>=start_time(i_seq),1,'first');
                    if ~isempty(idx_first_tmp)
                        slice_sequence_tmp(1)=max(2,idx_first_tmp);
                    end
                end
            else
                slice_sequence_tmp(1)=max(1,find(slice_time>=start_time(i_seq),1,'first'));
            end
            if any(fixing_booleans(:,3))
                if is_auction(i_seq,3)
                    slice_sequence_tmp(3)=find(fixing_booleans(:,3),1,'last');
                else
                    idx_end=find(slice_time>=end_time(i_seq),1,'first');

                    if isempty(idx_end) || idx_end==length(fixing_booleans(:,3))
                        slice_sequence_tmp(3)=length(fixing_booleans(:,3))-1;
                    else
                        slice_sequence_tmp(3)=idx_end;
                    end
                end
            else
                %idx_end=find(slice_time>end_time(i_seq)-eps_dt,1,'first');
                idx_end=find(slice_time>=end_time(i_seq),1,'first');
                if isempty(idx_end)
                    slice_sequence_tmp(3)=length(fixing_booleans(:,3));
                else
                    slice_sequence_tmp(3)=idx_end;
                end
            end
            
            if any(fixing_booleans(:,2)) && ~isnan(slice_sequence_tmp(1)) && ~isnan(slice_sequence_tmp(3))
                slice_mid=find(fixing_booleans(:,2));
                if slice_mid>slice_sequence_tmp(1) && slice_mid<slice_sequence_tmp(3)
                    if is_auction(i_seq,2)
                        slice_sequence=slice_sequence_tmp(1):slice_sequence_tmp(3);
                    else
                        slice_sequence=[slice_sequence_tmp(1):(slice_mid-1) (slice_mid+1):slice_sequence_tmp(3)];
                    end
                else
                    slice_sequence=slice_sequence_tmp(1):slice_sequence_tmp(3);
                end
            elseif ~isnan(slice_sequence_tmp(1)) && ~isnan(slice_sequence_tmp(3))
                slice_sequence=slice_sequence_tmp(1):slice_sequence_tmp(3);
            elseif ~isnan(slice_sequence_tmp(1)) 
                slice_sequence=slice_sequence_tmp(1);
            elseif ~isnan(slice_sequence_tmp(3)) 
                slice_sequence=slice_sequence_tmp(3);
            else
                slice_sequence=[];
            end
            
            
            res{i_seq}=slice_sequence;
        end
        
end