function rslt = se_check_cvdyn(security_id, trading_destination_id, window_type, window_width, as_of_day,vc)
%  se_check_cvdyn - Check de l'estimateur Volume curves dynamique
%
% example :
% vc=se_run_cvdyn(110,[],'day',180,'20/01/2010');
% res=se_check_cvdyn(110,[],'day',30,'20/01/2010',vc);


% author   : 'njoseph@cheuvreux.com'
% reviewer : ''
% version  : ''
% date     : ''


% methode d'estimation des param?tres par d?faut (mem que dans st_vc pour
% le se_run, la donn? de strat_slice et end_slice est calcul?e par la suite !!!)
default_forecast_params={'composite 1',2,0.1,0.2,'curve_ref',30,'emaLength',4,...
    'quant0',1,'meanNb05m',10,'pctMedian05m',0};

vc = cvdynse2cvdyndata(vc);


% R?cup?ration des donn?es hors ?chantillon
stropt = opt2str(options({'step:time', [char(165) 'datenum(0,0,0,0,5,0)'], ...
    'window:time', [char(165) 'datenum(0,0,0,0,5,0)'], ...
    'bins4stop:b', false}));

[from, to] = se_window_spec2from_to(window_type, window_width, as_of_day);


data = read_dataset(['gi:basic_indicator/' stropt],...
    'security_id', security_id,...
    'from', datestr(from, 'dd/mm/yyyy'),...
    'to', datestr(to, 'dd/mm/yyyy'), ...
    'trading-destinations', trading_destination_id,...
    'output-mode', 'day');


idx = find(~cellfun(@isempty,data));
idx = idx(~cellfun(@(x)isempty(x.date),data(idx)));
if isempty(idx)
    error('se_check_cvdyn:exec', 'NO_DATA_FILTER:no data');
else
    rslt=[];
    for iDay=1:length(idx)
        data_day=data{idx(iDay)}; 
        % methode d'estimation des param?tres par d?faut (mem que dans st_vc pour le st_vc !!!)
        default_forecast_params_for_check=cat(2,default_forecast_params,...
            {'start_slice',6,'end_slice',length(data_day.date)-6});
        forecast = vc_forecast(vc,data_day,default_forecast_params_for_check{:});
        check_values=vc_check('cvdyn_indicator', forecast,data_day);
        rslt=cat(1,rslt,check_values(1));
    end
    
end

rslt=nanmedian(rslt);


end

%%%%%% TRASH

% 
% %%% R?cup?ration des donn?es du SE
% meanValueOpen=find(cellfun(@(x)(strcmp(x,'auction_open')),vc.rownames)==1);
% meanValueClose=find(cellfun(@(x)(strcmp(x,'auction_close')),vc.rownames)==1);
% meanValueslice=find(cellfun(@(x)(strcmp(x(1:6),'slice_')),vc.rownames)==1);
% meanValue30m=vc.value(cat(1,meanValueOpen,meanValueslice,meanValueClose));
% vc.date=vc.date(cat(1,meanValueOpen,meanValueslice,meanValueClose));
% 
% covValueRow=cellfun(@(x)(strcmp(x(1:3),'cov')),vc.rownames);
% nb30MinPeriod=max(cellfun(@(x)(str2num(substr(x,4,3))),vc.rownames(covValueRow)));
% endIndex=min(find(covValueRow==1))-1+cat(2,0,cumsum(nb30MinPeriod:-1:1));
% reconstructCovQuant0=repmat(nan,nb30MinPeriod,nb30MinPeriod);
% for iConstruct=1:nb30MinPeriod
%     reconstructCovQuant0(iConstruct,iConstruct:nb30MinPeriod)=vc.value(endIndex(iConstruct)+1:endIndex(iConstruct+1))';
%     reconstructCovQuant0(iConstruct:nb30MinPeriod,iConstruct)=vc.value(endIndex(iConstruct)+1:endIndex(iConstruct+1));
% end
% 
% 
% %%% utilisation des donn?es du SE
% vc.model.other_models(2).valueQuant0(:,1)=meanValue30m;
% vc.model.other_models(2).model.covQuant0=reconstructCovQuant0;
% 
% 
% % - remarques
% % - les modifications de vc sont suffisantes car dans vc_forecast on
% % - n'utilise uniquement l'information de other_models(2) sur valueQuant0,
% % - sur covQuant0 et sur les valeurs des crit?res de s?lection
% % - (vc5m.model.critere)
% % - de plus, vu qu'on fournit bien le numero du slice, le passage dans
% % - idx_slice de vc_forecast va bien se d?rouler...
