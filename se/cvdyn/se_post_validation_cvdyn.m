function se_post_validation_cvdyn()

global st_version
quant = st_version.bases.quant;

% Estimator
estimator = se_db('get_estimator','estimator_name','cvdyn');
estimator_id = estimator.estimator_id;

% Les jobs
job = se_db('get_job','estimator_id',estimator_id);
window_width = max([job.run_window_width]);

% Les security_id
security = se_db('get_domain_security','domain_id',[job.domain_id]);
security_id = [security.security_id];

% corporate actions entre (today - window_width) et (today + 2)
corporate_action = get_repository('cac',security_id,datestr(today+2,'yyyy-mm-dd'));
corporate_action = st_data('from-idx',corporate_action,corporate_action.date>=today-window_width);

% Récupération dans QUANT..JOB des jobs associés au sec_id splités
sec_id_splitted = st_data('cols',corporate_action,'security_id');
domain_id_splitted = [security(ismember([security.security_id],sec_id_splitted)).domain_id];
job2invalidate = [job(ismember([job.domain_id],domain_id_splitted)).job_id];

if ~isempty(job2invalidate)
    st_log('run2invalidate_lst : %s \n', sprintf('%d,',job2invalidate));
    
    %récuperation du type "STAND_BY"
    validation_id = cell2mat(exec_sql('QUANT',['SELECT validation_type_id',...
        ' from ',quant,'..validation_type',...
        ' where validation_type_name = ''STAND_BY''']));
    
    %Dévalidation de ces job via la proc stockee 'upd_validation'
    for i = 1:length(job2invalidate)
        exec_sql('QUANT',[quant,'..upd_validation ',num2str(job2invalidate(i)),',0,NULL,',num2str(validation_id)]);
    end
end
st_log('se_post_validation_cvdyn: END OF DEVALIDATION of %d jobs\n',length(job2invalidate));
