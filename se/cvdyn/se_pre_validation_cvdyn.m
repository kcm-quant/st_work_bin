function se_pre_validation_cvdyn(varargin)
% SE_PRE_VALIDATION_CVDYN - Validation des courbes cvdyn
%
%
%
% Examples:
% se_pre_validation_cvdyn('session' , 786,'threshold',0.5)
%
%
%
% See also: se_post_validation_cvdyn
%
%
%   author   : 'dcroize@cheuvreux.com'
%   reviewer : 'mlasnier@cheuvreux.com'
%   version  : '2'
%   date     :  '31/03/2011'

global st_version
quant = st_version.bases.quant;

opt = options({'session', nan , ...
    'threshold', .50, ...
    'estimator_name','cvdyn',...
    'owner',{},...
    },varargin);
if isempty(opt.get('owner'))
    opt.set('owner',exec_sql('QUANT',['select owner from ',quant,'..estimator where estimator_name = ''cvdyn''']));
end

estimator_id = getfield(se_db('get_estimator' , 'estimator_name' , opt.get('estimator_name')), 'estimator_id' );

if isnan(opt.get('session'))
    opt.set('session',cell2mat(exec_sql('QUANT',['select max (session_num) from ',quant,'..session'])));
end

oldDataReturnFormat = setdbprefs('DataReturnFormat');
setdbprefs('DataReturnFormat','structure');
quals = exec_sql('QUANT',sprintf(['select er.job_id,er.run_id,er.run_quality,er.session_num as last_run_session_num,',...
    'lr.last_valid_run_id',...
    ' from ',quant,'..estimator_runs er,',quant,'..last_run lr,',quant,'..job_status js,',quant,'..estimator e',...
    ' where e.estimator_name = ''cvdyn''',...
    ' and e.estimator_id = er.estimator_id',...
    ' and js.session_num = %d',...
    ' and js.job_or_run_id = er.job_id',...
    ' and js.is_run =1',...
    ' and lr.job_id = er.job_id',...
    ' and lr.last_run_id = er.run_id'],opt.get('session')));
setdbprefs('DataReturnFormat',oldDataReturnFormat);

% Methodologie de validation :
% 1. On devalide tous les jobs de l'estimateur cvdyn qui ont tourn� pendant
% la session OPT.GET('SESSION')
% 2. On valide les run de la session OPT.GET('SESSION') dont la run quality > THRESHOLD
try
    if isempty(quals)
        st_log('<se_pre_validation_cvdyn> No job to validate on estimator <cvdyn> in environment <%s> for session %d\n ',...
            st_version.my_env.target_name,opt.get('session'));
    else
        % Etape 1 :
        if ~all(isnan(quals.last_valid_run_id))
            job_str = sprintf('%d,',quals.job_id(~isnan(quals.last_valid_run_id)));
            exec_sql('QUANT',...
                sprintf(['update ',quant,'..estimator_runs set is_valid = 0 where job_id in (%s) and is_valid = 1'],job_str(1:end-1)));
        end
        
        % Etape 2:
        run2val = quals.run_id(quals.last_run_session_num == opt.get('session')&quals.run_quality >= opt.get('threshold'));
        if ~isempty(run2val)
            run_str = sprintf('%d,',run2val);
            exec_sql('QUANT',...
                sprintf(['update ',quant,'..estimator_runs set is_valid = 1 where run_id in (%s)',...
                ' and is_valid = 0',...
                ' and estimator_id = %d'],...
                run_str(1:end-1),estimator_id));
        end
    end
catch e
    st_log(sprintf('%s\n', e.message));
    subject    = sprintf('Echec de la validation de l''estimateur <cvdyn> en environnement <%s>',st_version.my_env.target_name);
    message    = se_stack_error_message(e);
    recipients = unique([regexp(st_version.my_env.se_admin,',','split'),opt.get('owner')]);
    sendmail(recipients,subject,message);
end



% try
%     run_2v = [];
%     job_2v = [];
%
%     if (~isempty(quals))
%
%         st_log('se_validation_run: validation off for %d runs ... %s\n', size(quals,1) , estimator_name);
%
%         data_lr = st_data('from-cell', 'Les Last_runs', colnames , quals);
%         unique_job = st_data( 'cols', data_lr, 'job_id');
%
%
%         %***************************************************************
%         %FIRST validation : on valide les jobs dont le run_quality >0.5
%         %***************************************************************
%         if (opt.get('first-validation')==1)
%             job_2v = st_data('cols',st_data('where', data_lr, sprintf('{quality}>= %d' , threshold)),'job_id');
%
%             %**********************************************************
%             %UPDATE VALIDATION
%             %***********************************************************
%         else
%             job_2v = st_data('cols',st_data('where', data_lr, sprintf('{quality}>= %d' , threshold)),'job_id');
%         end
%
%         %Requ�te sur les jobs pour r�cuperer les differents runs/contextes d'un m�me job
%         if (~isempty(job_2v))
%             if isnan(opt.get('session'))
%                 f_run_2v = @(jb)cell2mat(exec_sql('QUANT', sprintf( [ ...
%                     'SELECT last_run_id ' ...
%                     'FROM %s..last_run ' ...
%                     'WHERE job_id in (%d) AND ' ...
%                     'last_run_date in (select max(last_run_date) from %s..last_run where job_id = %d)' ],q_base, jb,q_base , jb)));
%             else
%                 f_run_2v = @(jb)cell2mat(exec_sql('QUANT', sprintf( [ ...
%                     'SELECT run_id ' ...
%                     'FROM %s..estimator_runs ' ...
%                     'WHERE job_id in (%d) AND ' ...
%                     'session_num = %d '], q_base , jb , opt.get('session'))));
%             end
%
%
%             run_2v = cell2mat(arrayfun(@(j) f_run_2v (j) , job_2v , 'uni' , false));
%         end
%
%         info_validation = struct('better_lr', [],'worst_lr',[] ,'better_than_min_last_valid' ,[],'worst_than_min_last_valid' ,[]);
%
%     else
%
%         info_validation = struct('better_lr', [],'worst_lr',[] ,'better_than_min_last_valid' ,[],'worst_than_min_last_valid' ,[]);
%     end
%
%     varargout = {struct('estimator',estimator_name,'run_valid',run_2v,'job_id',job_2v,'domain_type',domain_type,'info',info_validation)};
%
% catch e
%
%
%     varargout = {struct('estimator',estimator_name,'run_valid',[],'job_id',[],'domain_type',domain_type ,'info',info_validation)};
%
% end