function data=se_run_cvdyn(security_id, trading_destination_id, window_type, window_width, as_of_date,dt_remove,varargin_)
%  se_run_cvdyn - Run de l'estimateur Volume curve dynamique
% exemple
% a=se_run_cvdyn(2,4,'day',180,'17/06/2009')
% a=se_run_cvdyn(get_sec_id_from_ric('CSGN.VX'),[],'day',180,'07/07/2010')
% a=se_run_cvdyn(get_sec_id_from_ric('LUX.MI'),[],'day',180,'23/01/2009')


% author   : 'njoseph@cheuvreux.com'
% reviewer : ''
% version  : ''
% date     : ''

default_opt = {'mode_season', 'gaussian curve 2', ...
    'window_spec:char', sprintf('%s|%d', window_type, window_width)};

if nargin>6 && ~isempty(varargin_)
    error('se_run_cvdyn:check_args', 'varargin_ is not handled yet');
else
    opt = options(default_opt);
    if nargin < 6
        dt_remove = [];
    end
end

vcoptstr = ['gi:vc/' opt2str(opt)];

%%% recup�ration des donn�es
data= read_dataset(vcoptstr, ...
    'security_id', security_id,...
    'from', as_of_date,...
    'to', as_of_date, ...
    'trading-destinations',trading_destination_id);

if isempty(data) || isempty(data.value)
    error('se_run_cvdyn:check_args', 'NODATA_FILTER: empty data from vc graph');
end


%%% Mise en forme des donn�es
data30m=data.model.other_models(2);
auc_colns = 'opening_auction;intraday_auction;closing_auction';
auc_param_name = {'auction_open','auction_close'};
auc_vals = logical(st_data('cols', data30m, auc_colns));
data = st_data('drop', data, auc_colns);
data.colnames=upper(data.colnames);

%%% Choix de la methode de forecast
if data.model.critere.nb_period_median0>0 || data.model.critere.nb_period_median0<=0 && data.model.critere.mean_nb_0>10
    forecast_method=0;
else
    forecast_method=1;
end


%%% r�cup�ration de la matrice de covariance
%%% ATTENTION : matrice unique suivant le contexte..si plusieurs contexte � traiter...
covarianceMatrix=data30m.model.covQuant0;
meanVector=data30m.valueQuant0(:,1);


%%% calcul du nombre de bins (5 minutes) par slice
%%% pour slice(1) et poutr slice(end)
nb_bins_vals=data30m.model.bins_idx(:,2)-data30m.model.bins_idx(:,1)+1;
nb_bins_first_slice=nb_bins_vals(1);
nb_bins_last_slice=nb_bins_vals(end);
if any(auc_vals(:,1))
    nb_bins_first_slice=nb_bins_vals(2);
end
if any(auc_vals(:,end))
    nb_bins_last_slice=nb_bins_vals(end-1);
end

%%% gestion des param�tres
nbParamMean=size(meanVector,1);
nbparamCov=size(covarianceMatrix,1)*(size(covarianceMatrix,1)+1)/2;

data.rownames = cell(nbParamMean+nbparamCov, 1);
data.value = repmat(nan,nbParamMean+nbparamCov, length(data.colnames));

slice_num=1;
for iMean = 1 : nbParamMean
    if any(auc_vals(iMean, [1 3]))
        data.rownames{iMean} = auc_param_name{auc_vals(iMean, [1 3])};
        data.value(iMean,:)=meanVector(iMean,:);
        data.rownames{end + 1} = [auc_param_name{auc_vals(iMean, [1 3])} '_closing'];
        data.value(end + 1, :) = mod((data30m.date(iMean)), 1);
    else
        if slice_num == 1
            data.rownames{end + 1} = 'end_of_first_slice';
            data.value(end + 1, :) = mod(data30m.date(iMean), 1);
        end
        data.rownames{iMean}=MAKE_PARAM_NAME(slice_num);
        data.value(iMean,:)=meanVector(iMean,:);
        slice_num=slice_num+1;
    end
end

if slice_num-1<10
    idx_last_slice=find(cellfun(@(x)(strcmpi(x,sprintf('slice_00%d',slice_num-1))),data.rownames)==1);
elseif slice_num-1<100
    idx_last_slice=find(cellfun(@(x)(strcmpi(x,sprintf('slice_0%d',slice_num-1))),data.rownames)==1);
else
    idx_last_slice=find(cellfun(@(x)(strcmpi(x,sprintf('slice_%d',slice_num-1))),data.rownames)==1);
end
data.rownames{end + 1} = 'end_of_last_slice';
data.value(end + 1, :) = mod(data30m.date(idx_last_slice), 1);

idx_rownames=nbParamMean+1;
for iCov = 1 : size(covarianceMatrix,1)
    for jCov = iCov:size(covarianceMatrix,1)
        data.rownames{idx_rownames}=MAKE_PARAM_NAME_COV(iCov,jCov);
        data.value(idx_rownames,:)=covarianceMatrix(iCov,jCov);
        idx_rownames=idx_rownames+1;
    end
end

data.value(end+1,:)=forecast_method;
data.rownames{end+1}='forecast_method';

data.value(end+1,:)=nb_bins_first_slice;
data.rownames{end+1}='nb_bins_first_slice';

data.value(end+1,:)=nb_bins_last_slice;
data.rownames{end+1}='nb_bins_last_slice';

data.date = [data30m.date; NaN(length(data.rownames)-length(data30m.date), 1)];


end


function str = MAKE_PARAM_NAME(i)
str = num2str(i);
str = ['slice_' repmat('0', 1, 3-length(str)) str];
end

function str = MAKE_PARAM_NAME_COV(i,j)
str1 = num2str(i);
str2 = num2str(j);
str = ['cov' '_' repmat('0', 1, 3-length(str1)) str1 '_' repmat('0', 1, 3-length(str2)) str2];
end

