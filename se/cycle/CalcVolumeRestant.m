function vr= CalcVolumeRestant(v,sbt,myvb)
vr = max(0,min(myvb,sbt-v));