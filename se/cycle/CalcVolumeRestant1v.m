function [vr pexec]= CalcVolumeRestant1v(ob,myp,myv)
ix=find(ob(:,2)>=myp,1,'last');
V=cumsum(ob(1:ix,1));
i=find(V>=myv,1,'first');
inonnul=find(ob(:,2)~=0);
ptemp=ob(:,2);
if ~isempty(inonnul)
    inul=find(ob(:,2)==0);
    if ~isempty(inul)
        ptemp(inul)=mean(ob(inonnul,2));
    end
else
    error=1;
end

if isempty(i)
    vr=myv-V(end);
    pexec=sum(ptemp(1:ix).*ob(1:ix,1));
else
    vr=0;
    if i==1
        pexec=ptemp(1)*myv;
    else
        pexec=sum(ptemp(1:i-1).*ob(1:i-1,1))+ptemp(i)*(myv-sum(ob(1:i-1,1)));
    end
end