Part=[5];
for uuu=[1];
    for cp=1:length(Part)
        a=-0.1;
        b=0.1;
        mu=1;
        part=Part(cp)*0.01;
        n_jour=20;% le nombre minimum pour verifier le crit�re d'�quilibre de volume
        seuilsv=0.07;
        sp=10;
        fi=dir('C:\dev\st_repository\read_dataset\orderbooks');
        nam=arrayfun(@(i)fi(i+2).name,(1:length(fi)-2)','uni',false);
        
        %nam ={'fte'};%'FTE','SOGN','SASY','TMS','VLOF','VLLP','ALSO','LAFP','STM' 'PEUP'};
        for kn=5:5%length(nam)
            % creer le repertoire
            dir_path  = fullfile('C:\dev\AutSept\strategyEC186N', nam{kn});
            file_path = fullfile(dir_path, 'st_table.mat');
            if ~exist(dir_path, 'dir')
                % si trouve pas --> cr�er le r�pertoire
                mkdir(dir_path);
            end
            if exist(file_path, 'file')
                st_hash_table = getfield(load(fullfile(dir_path,'st_table.mat')),'st_hash_table');
                first_time = false;
            else
                st_hash_table = struct('date', 0, 'cyjht', {{[0]}});
                first_time = true;
            end   
            
            % Para initial
            c=[1 2 4 6 8];
            cy=[5 10 20 40];
            la=[0 0.04  0.08];
            R=[];ParaT=[];Para=[];
            m=1;
            for k1=1:length(cy)
                for k2=1:length(c)
                    for k3=1:length(la)
                        cym(m)=cy(k1);
                        cm(m)=c(k2);
                        lam(m)=la(k3);
                        Para(m,:)=[cym(m) cm(m) lam(m)];
                        m=m+1;
                    end
                end
            end
            X=[];
            ly=length(la);lx=length(c);lai=[];ci=[];xi=[];yi=[];
            for i=ly:-1:1
                for j=1:lx
                    y=i;% coordonn�es (x, y) de la g�om�trie 
                    x=j;
                    X(ly+1-i,j)=y*(lx-x)+(ly-y)*x;% aire d'�limination potentielle
                    ci(i,j)=c(j);%c croissant en j
                    lai(ly+1-i,j)=la(i);%l d�croissant en i 
                    xi(i,j)=j;
                    yi(i,j)=i;
                end
            end
            % mapping 2 dim -> 1 dim
            ee=1:size(X,1)*size(X,2);
            rX=reshape(X,1,size(X,1)*size(X,2));
            rci=reshape(ci,1,size(ci,1)*size(ci,2));
            rlai=reshape(lai,1,size(lai,1)*size(lai,2));
            rxi=reshape(xi,1,size(xi,1)*size(xi,2));
            ryi=reshape(yi,1,size(yi,1)*size(yi,2));
            lpa=length(c)*length(cy)*length(la);

            n=strcat('C:\dev\st_repository\read_dataset\orderbooks\',nam{kn});%PRTP, VLOF, SOGN, ALSO, VLLP, LAFP, PEUP, BNPP, LAGA, TOTF, ATOS, MWDP, EXHO, SASY
            fichier =fuf( n, 'detail');
            ParaT=[];ParaResT=[];
            dn=[];
            j0=1;
            while str2num(fichier{j0}(46:49))==2008
                j0=j0+1;
            end
%                 if kn==1
%                     j0=49;
%                 else
%                     j0=45;
%                 end
            for j=j0:length(fichier)
                y=fichier{j};
                ss=strcat(y(end-13:end-12),'/',y(end-10:end-9),'/',y(end-7:end-4));
                dn(j-(j0-1))=datenum(ss,'dd/mm/yyyy');
            end
            [v ix0]=sort(dn);
            ix0=ix0+j0-1;
            e=1;  
            fee=0.0;
            bl0=ones(ly,lx);
            KT=[];
            KT=cell(size(Para,1),1);
            si=unidrnd(length(ix0),1,n_jour);
            % pour un cy donn�, pour une strat�gie  donn�e (c,l),
            % calculer KT sur plusieurs jours.
            for u1=1:length(cy)
                setnot0=1:ly*lx;
                xnul=logical(ones(1,ly*lx));%cy �tant fix�, une strat�gie <-> (l,c)
                while ~isempty(setnot0)
                    [vz  iz]=sort(rX(setnot0));%parmi les strat�gies non encore test�e, prendre celle ayant le rX le plus grand
                    is=setnot0(iz(end));% indice globale dans 1:ly*lx
                    bl=[];bl=bl0;
                    xit=rxi(is);% utilisation du mapping ds le sens 1D -> 2D
                    yit=ryi(is);
                    pa=[cy(u1) rci(is) rlai(is)];
                    nump=find(Para(:,1)==pa(1) & Para(:,2)==pa(2) & Para(:,3)==pa(3));
                    for i=si                                
                        y=fichier{ix0(i)};
                        z=[];
                        z=y(end-13:end-4);

                        % pour un jour z donn�, r�cup�rer les jeux de
                        % donn�es dans cy_hash_table
                        if first_time
                            index_j = 1;
                            st_hash_table.date(index_j) = datenum(z,'dd_mm_yyyy');
                            cyj = java.util.Properties;  
                            first_time = false;
                        else
                            index_j = find(st_hash_table.date == datenum(z,'dd_mm_yyyy'));
                            if isempty(index_j)                            
                                % je concat�ne � la fin de la liste des dates
                                index_j = length(st_hash_table.date)+1;
                                st_hash_table.date(index_j) = datenum(z,'dd_mm_yyyy');
                                cyj = java.util.Properties;
                            else
                                cyj = st_hash_table.cyjht{index_j};
                            end
                        end
                        
                        q=0;inv=0;
                        params = mat2str([0 0 uuu part a b pa]);
                        % cr�er la cl�
                        key = hash(params, 'MD5');
                        % chercher si d�j� dans le cyj
                        params = cyj.get(key);
                        
                        if ~isempty(params)    
                            PA = eval(params);
                            sl = PA(1);
                            sv = PA(2);
                        else
                            load(fullfile(n,sprintf('%s_%s',nam{kn},y(end-13:end))));
                            [sl sv]=ec186N(uuu,day,inv,q,part,fee,a,b,mu,e,pa');
                            PA = [sl sv]; 
                            cyj.put(key, mat2str(PA));
                        end
                        KT{nump}=[KT{nump}; PA];
                        % mise � jour st_hash_table avec la nouvelle cyj
                        st_hash_table.cyjht{index_j} = cyj;   
                    end
                    
                    
                    %mean(KT{nump}(:,2))
                    if mean(KT{nump}(:,2))>seuilsv % supprime les strat�gies en slippage volumique trop grand                        
                        bl(yit:end,xit:end)=0;xnult=[];
                        xnult=logical(reshape(bl,1,size(X,1)*size(X,2)));% 2D -> 1D
                        xnul=xnul & xnult;
                        KT{nump}=[];
                    elseif mean(KT{nump}(:,2))<-seuilsv % supprime les strat�gies en slippage volumique trop faible 
                        bl(1:yit,1:xit)=0;xnult=[];
                        xnult=logical(reshape(bl,1,size(X,1)*size(X,2)));
                        xnul=xnul & xnult;
                        KT{nump}=[];
                    else
                        xnul(is)=0; %on garde KT{nump} mais on met is � 0, puisque la strat�gie correspondante a �t� test�e.
                    end
                    setnot0=ee(xnul);
                end
            end
            
            
            %  Para possible
            inn=cellfun(@(x)~isempty(x),KT);
            ix=1:size(KT,1);
            ix=ix(inn);
            ktt1=arrayfun(@(k)mean(KT{ix(k)}(:,1)),1:length(ix));
            ktt2=arrayfun(@(k)mean(KT{ix(k)}(:,2)),1:length(ix));
            [v ix1]=sort(ktt1);
            iy=ix(ix1);
            ParaResT=[Para(iy,:) ktt1(ix1)' ktt2(ix1)'];
            Para=Para(iy,:);
            
            % KT : test sur ix0 des strat�gies Para
            m=1;
            KT=[];
            for i=1:length(ix0)                                                   
                y=fichier{ix0(i)};
                z=[];
                z=y(end-13:end-4);
                           
                index_j = find(st_hash_table.date == datenum(z,'dd_mm_yyyy'));
                if isempty(index_j)
                    % je concat�ne � la fin de la liste des dates
                    index_j = length(st_hash_table.date)+1;
                    st_hash_table.date(index_j) = datenum(z,'dd_mm_yyyy');
                    cyj = java.util.Properties;
                else
                    cyj = st_hash_table.cyjht{index_j};
                end
                for k=1:size(Para,1)
                    pa=Para(k,:);                                  
                    q=0;inv=0;
                    params = mat2str([0 0 uuu part a b pa]);
                                       
                    % cr�er la cl�
                    key = hash(params, 'MD5');
                    % chercher si d�j� dans le cyj
                    params = cyj.get(key);
                        
                    if ~isempty(params)    
                        PA = eval(params);
                        sl = PA(1);
                        sv = PA(2);
                    else                                    
                        load(fullfile(n,sprintf('%s_%s',nam{kn},y(end-13:end))));
                        [sl sv]=EC0(uuu,day,inv,q,part,fee,a,b,mu,e,pa');
                        PA = [sl sv]; 
                        cyj.put(key, mat2str(PA));
                    end                      
                    KT(i,k,:)=PA;% i : jour, k : strat�gie
                end
                % mise � jour st_hash_table avec la nouvelle cyj
                st_hash_table.cyjht{index_j} = cyj;   
            end            
            % puis sauvegarder � nouveau st_hash_table sur le disque
            save(fullfile(dir_path, 'st_table.mat'), 'st_hash_table');
            save( sprintf('%s.mat', strcat('C:\dev\AutSept\aut1\achat\',num2str(uuu),'\',num2str(Part(cp)),'%\',nam{kn},'1')),'ParaResT','Para','KT')
        end
    end
end
  