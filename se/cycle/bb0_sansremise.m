function [sl sv]=bb0_sansremise(day,theta)
zz=theta(3);
part=theta(4);
a=theta(5);
b=theta(6);
Pa=theta(7:9);

A=day.value;
da=day.date;
if length(da)>10

    tv=find(da(2:end)-da(1:end-1));

    p=A(:,1);v=A(:,2);side=A(:,3);
    SQT=[];
    vol=v;
    vemd=mean(v);
    ev=[];
    alpha=1;
    e=1;
    n1 = 2;
    n2 = 1;
    n3 = 2;
    pb=A(:,4);pa=A(:,5);sb=alpha*A(:,6);sa=alpha*A(:,7);

    lpa=length(Pa(1));
    cycle=Pa(1);
    c=Pa(2);
    ea=Pa(3);
    eb=ea;

    ddd=da(1):cycle*1.1574e-005:da(end);
    TV1=unique(arrayfun(@(i)find(da>ddd(i),1,'first'),1:length(ddd)));
    % TV1=cell2mat(TV1);
    % bustime
    % TV1=tv(1:cycle:end);

    TV=TV1(2:end);
    
    u1=0;
    uu=0;

    mid=(pa+pb)/2;
    %s=[mean(pa-pb) mean(mid-pb) mean(mid-pb2) mean(mid-pb3) mean(mid-pb4) mean(mid-pb5)];
    W=[];
    vw=[];
    Q=[];
    q=0;
    Q(1)=q;
    I=[];
    inv=0;
    I(1,:)=inv;
    dv=[];
    l=length(A(:,1));
    %ix=find(floor(Rn(:,1))==floor(da(1)),1,'first');
    %m=1;
    if zz==1
        tdebut=max(l-2500,1);
    elseif zz==2
        tdebut=max(l-2000,1);
    elseif zz==3
        tdebut=max(l-1500,1);
    elseif zz==4
        tdebut=max(l-1000,1);
    elseif zz==8
        tdebut=max(l-500,1);
    elseif zz==11
        tdebut=1;
        l=min(l,2500);
    end

    t1=1;
    t2=tv(min(1*cycle,length(tv)));
    vmarket=sum(v(t1:t2));

    V=part*vmarket;%/cycle;
    sbt=V+sb(tdebut+1);

    fa=b*ones(1,l);
    ta=floor(tdebut+0.9*(l-tdebut));
    tb=floor(tdebut+0.98*(l-tdebut));
    fa(ta:tb)=arrayfun(@(t)1-(t-tdebut)/(l-tdebut),ta:tb);
    fa(tb+1:end)=0.02;

    P(1)=p(tdebut);
    n=1;
    mypb=pb(tdebut+1)*ones(1,lpa);
    myvb=part*vmarket*ones(1,lpa);
    t2=1;
    PV=0;
    Vold=0;
    Ic=0;
    D=0;
    Vmarket=0;
    vw(1)=p(1);
    t=tdebut;
    [ob obold]=makeobafter2(A(t,:),alpha);
    ob=obold;

    for t=tdebut:l-1
        Wb=0;
        d=0;
        dum=0;
        execQty=0;

        ix1= p(t)<=mypb & myvb>0;
        if ix1
            myvb0=myvb;sbt0=sbt;
            if ob(1,3)>0
                ix2= mypb<ob(1,3);
                iz=ix1 & ix2;izc=ix1 & (1-ix2);
                if iz
                    myvb=CalcVolumeRestant(v(t),sbt,myvb);
                    sbt=max(0,sbt-v(t));
                    Wb=-(myvb0-myvb).*(mypb+(mypb==0)*p(t));
                end
                if izc
                    [myvb Wbt]=CalcVolumeRestant1(ob,mypb,myvb);
                    Wbt=-Wbt;
                    Wb=Wbt;
                end
            end

            I(n)=I(n)+(myvb0-myvb);
            Ic=Ic+(myvb0-myvb);
            execQty=myvb0-myvb;
        end

        [ob obold]=makeobafter2(A(t,:),alpha);
        obold(2:end,:)=[];
        ob(2:end,:)=[];

       it=find(TV==t);

        if ~isempty(it) || t==tdebut
            if t==tdebut
                t1=tdebut;
                t2=TV(find(TV>tdebut,1,'first'));
            end
            if ~isempty(it)
                if it<length(TV)
                    t1=TV(it);
                    t2=TV(it+1);
                else
                    t2=l-1;
                end
            end
            vmarket=sum(v(t1:t2));
            pvmarket=sum(p(t1:t2).*v(t1:t2));
        end

       if ~isempty(it) || t==tdebut
            d=zeros(1,lpa);        
            PV=PV+pvmarket;
            Vmarket=Vmarket+vmarket;
            vwapmarket=PV/Vmarket;

            if uu>0
                Vold=V;
            end
            uu=1;
            dv=[dv vemd-part*vmarket];
            V=0;
            V=floor(max(vemd,c*(part*vmarket)));%part*le volume ecoul� pdt le dernier cycle
            dum=1;
            myvb=V;%max(0,V+(Vold-Ic))*(t>1);
            if u1>0
                sqt=sqto;
            end
            u1=1;
            sqt=(Q(end)-q-part*Vmarket)/(part*Vmarket);
            SQT=[SQT ;sqt];
            sqto=sqt;
            ix1=sqt<-fa(t);
            ix2= myvb<ob(1,4);
            ix2c=myvb>=ob(1,4);

            if ix1 && ix2 && ob(1,3)>0
                mypb=ob(1,3);
                sbt=V;
                d=1;
            end
            if ix2c && ix1 && ob(1,3)>0
                mypb=ob(1,3)+0.01;
                sbt=V;
                d=1;
            end
            ix0=sqt >= -fa(t) & sqt < fa(t);
            if ix0 && ob(n2,2)>0
                mypb=ob(n2,2)-eb;
                iy=sum(mypb==ob(:,2));
                iy=ix0 & iy;
                if iy
                    fz=find(mypb==ob(:,2),1,'first');
                    fy=find(iy~=0);
                    sbt(fy)=V(fy)+ob(fz,1)';
                    d=2;
                else
                    sbt=V;
                    d=2;
                end
            end
            ix0 = sqt >= fa(t);
            if ix0
                mypb=0;
                sbt=1e5;
                d=4;
            end
            Ic=0;
        end
        ix=myvb==0 & dum==0;
%         if  0%ix;
%             d=0;
%             V=e*max(0,Vold-Ic);
%             myvb=V;%approx
%             ix1=sqt<a ;
%             ix2= myvb<ob(1,4);
%             ix2c=myvb>=ob(1,4);
%             if ix1 && ix2 && ob(1,3)>0
%                 mypb=ob(1,3);
%                 sbt=myvb;
%                 d=1;
%             end
%             if ix2c && ix1 && ob(1,3)>0
%                 mypb=ob(1,3)+0.01;
%                 sbt=myvb;
%                 d=1;
%             end
%              ix0=sqt >= a & sqt < b;
%             if ix0 && ob(n2,2)>0
%                 mypb=ob(n2,2)-eb;
%                 iy=sum(mypb==ob(:,2));
%                 iy=ix0 & iy;
%                 if iy
%                     fz=find(mypb==ob(:,2),1,'first');
%                     fy=find(iy~=0);
%                     sbt(fy)=V(fy)+ob(fz,1)';
%                     d=2;
%                 else
%                     sbt=V;
%                     d=2;
%                 end
%             end
%             ix0 = sqt >= fa(t);
%             if ix0 && ob(n2,2)>0
%                 mypb=0;
%                 sbt=1e5;
%                 d=4;
%             end
%          end
        dum=0;
        D(n,:)=d;
        if sum(execQty)>0
            W(n)=(Wb);
            Q(n)=Q(n)+execQty;
            P(n)=p(t);
        else
            W(n)=0;
        end

        I(n+1)=I(n);
        D(n+1)=D(n);
        Q(n+1)=Q(n);
        P(n+1)=P(n);
        vw(n+1)=sum(p(tdebut:t).*v(tdebut:t))/sum(v(tdebut:t));
        n=n+1;

    end

    SL=(-cumsum(W)./(Q(1:end-1)-q)-vw(1:end-1))./vw(1:end-1)*1e4;
    sv=(Q(end)-part*Vmarket)/(part*Vmarket);
    if length(SL)>2000
        sl=mean(SL(end-100:end-10));
    elseif length(SL)>1000
        sl=mean(SL(end-50:end-10));
    elseif length(SL)>20
        sl=mean(SL(end-20:end-5));
    else
        sl=mean(SL)
    end    
else
    sl=NaN;
    sv=NaN;
end
%plot(SQT(50:end))
uu=1;