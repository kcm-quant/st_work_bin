function data = get_PLcycle(mode, varargin)
% GET_PLCYCLE - interface pour les donn�es de type positionnement en cycle 
%
% internal uses:
% - get_PLcycle('read', 'security_id', 112, 'day', '01/04/2008', 'theta', [????])
% - get_PLcycle('read', 'security_id', 'FTE.PA', 'day', '01/04/2008', 'theta', [????])
% - get_PLcycle('save', 'security_id', 'FTE.PA')
% - get_PLcycle('clear', 'security_id', 'FTE.PA')
%
% See also get_tick from_buffer read_dataset
persistent st_hash
persistent first_time

%<* Read into the tickdb
opt = options({'security_id', 110, 'day', '01/04/2008',...
    'date_format:char', 'dd/mm/yyyy',...
    'theta', repmat(NaN, 1, 9),'trading-destinations',{ 'MAIN' }, ...
    }, varargin);
name_t = opt.get('security_id');
if ~ischar(name_t)
    name_t = get_repository( 'security-key', opt.get('security_id'));
end
hash_key = [ 'k_' strrep( name_t, '.', '_')];
data = [];

switch lower(mode)

    case 'read'
        % initialiser la variable st_table
        % lecture � partir de disque local
        if ~isempty( st_hash) && isfield( st_hash, hash_key)
            st_hash_table = st_hash.(hash_key);
            first_time = false;
        else
            % st_hash est vide --> premier appel � la fonction
            % besoin de rien faire
            % lecture � partir de disque local
            % ssi st_hash n'existe pas (premi�re fois)
            % ou on ne trouve pas le titre 
            dir_path  = fullfile(getenv('st_repository'), 'get_PLcyclev');
            if ~exist(dir_path, 'dir')
                mkdir(dir_path);
            end
        
            file_path = fullfile(dir_path, sprintf('%s.mat', name_t));
        
            if exist(file_path, 'file') && isfield( load(file_path), 'st_hash_table')
                 % lire st_hash_table ssi le fichier existe ET n'est pas vide
                st_hash_table = getfield(load(file_path), 'st_hash_table');
                first_time = false;
            else
                st_hash_table = struct('date', 0, 'cyjht', {{[0]}});
                first_time = true;
            end
        end
        %< Lecture des options
        format_date = opt.get('date_format:char');
        sec_id = opt.get('security_id');
        if ~isnumeric( sec_id) && numel(sec_id)==1
            error('get_tick:secid', 'I need an unique numerical security id');
        end
        day_ = opt.get('day');
        if ischar( day_)
            day_ = datenum( day_, format_date);
        end
        %>
        
        % pour un jour z donn�, r�cup�rer les jeux de
        % donn�es dans cy_hash_table
        % v�rifier si ce jour est d�j� existant
        if first_time
            index_j = 1;
            st_hash_table.date(index_j) = day_;
            cyj = java.util.Properties;
        else
            index_j = find(st_hash_table.date == day_);
            if isempty(index_j)
                % je concat�ne � la fin de la liste des dates
                index_j = length(st_hash_table.date)+1;
                st_hash_table.date(index_j) = day_;
                cyj = java.util.Properties;
            else
                cyj = st_hash_table.cyjht{index_j};
            end
        end
        
        % cr�er la cl�
        key = hash(mat2str(opt.get('theta')), 'MD5');
        % chercher si d�j� dans le cyj
        params = cyj.get(key);
        
        if ~isempty(params)
            PA = eval(params);
        else
            % si ce jeu de params n'existe pas
            % alors on calcule
            % charger les donn�es
            data_d = read_dataset('tickdb', 'security', name_t, 'from', opt.get('day'), 'to', opt.get('day'),'trading-destinations',opt.get('trading-destinations'));
            if ~isempty(data_d.value)
                [sl sv] = bb0v(data_d,opt.get('theta'));
                cyj.put(key, mat2str([sl sv]));
                PA=[sl sv];
            else
                PA=[];
            end
        end
        % mise � jour st_hash_table avec la nouvelle cyj
        if ~isempty(PA)
            st_hash_table.cyjht{index_j} = cyj;
            st_hash.(hash_key) = st_hash_table;
            data = st_data('init', 'title', 'Cycle parameters', 'colnames',{'sl' 'sv'}, 'value', PA, 'date', 1);
        else
            data = st_data( 'empty-init');
        end
    case 'save'
        % puis sauvegarder � nouveau st_hash_table sur le disque
        if isempty( st_hash)
            st_log('mysterious command... nothing done...\n');
            return
        end
        if ~isfield( st_hash, hash_key)
            st_log('no data for <%s>... nothing done...\n', hash_key);
            return
        end
        dir_path  = fullfile(getenv('st_repository'), 'get_PLcyclev');
        file_path = fullfile(dir_path, sprintf('%s.mat', name_t));
        st_hash_table = st_hash.(hash_key);
        if ~isempty(st_hash_table)
            % pour ne pas sauvegarder la struct sans field
            save(file_path, 'st_hash_table');
        end        
        
    case 'clear'
        if isempty( st_hash)
            st_log('mysterious command... nothing done...\n');
            return
        end
        if ~isfield( st_hash, hash_key)
            st_log('no data for <%s>... nothing done...\n', hash_key);
            return
        end
        st_hash = rmfield( st_hash, hash_key);
    otherwise
        error('get_PLcycle:mode', 'mode <%s> unknown', mode);
end
end