function [ob obold]=makeobafter2(A,alpha)
p=A(1);v=A(2);
pb=A(4);pa=A(5);sb=alpha*A(6);sa=alpha*A(7);
obold=makeob2(A,alpha);
ob=obold;
if p==pb
    if v<=sb
        ob(1,1)=obold(1,1)-v;
    else
        ob(1,1)=0;
    end

elseif p==pa
    if v<=sa
        ob(1,4)=obold(1,4)-v;
    else
        ob(1,4)=0;
    end
end
