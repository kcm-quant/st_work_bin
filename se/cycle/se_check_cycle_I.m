function res = se_check_cycle_I(security_id, trading_destination_id, window_type, window_width, as_of_day, data) 
% SE_CHECK_VOL2FPM - function that check the vol2fpm model
%
% use:
%  q = se_check_cycle(security_id, trading_destination_id, window_type, window_width, as_of_day, data) 
%  window_type, window_width are not used: always day/1
% example:
% security_id=17;%17;%ATOS%187michelin
% trading_destination_id=4;
% window_type=0;
% window_width=20;
% window_width2=5;
% as_of_day='03/03/2009';
% as_of_day2='10/11/2008';
% dt_remove=0;
% data=se_run_cycle_I(110, 4, [], 50, as_of_day, dt_remove,[])
% res = se_check_cycle_I(110, 4, [], 5, as_of_day, data) 

pl_old= st_data('cols', st_data('transpose', data), 'cycle;surmasse;distance;pl_med');

[qual data_new] = cycle_I(0,security_id, trading_destination_id, window_type, window_width, as_of_day, '', '');
pl_new= st_data('cols', st_data('transpose', data_new), 'cycle;surmasse;distance;pl_med');
res = zeros(1,10);
for j=1:10
    ix=[];
    %ix=find(all(repmat(pl_new(j,1:3),10,1)==pl_old(:,1:3),2));
    ix=find(all(repmat(pl_old(j,1:3),10,1)==pl_new(:,1:3),2));
    if ~isempty(ix)
        res(j) = (10-ix(1)+1)/10;
    else
        res(j) = 0;
    end
    if res(j)<0
        res(j)=0;
    end
end
end