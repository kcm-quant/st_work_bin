function [vr pexec]= CalcVolumeRestant1(ob,myp,myv)
for j=1:length(myp)
    ix=find(ob(:,3)<=myp(j),1,'last');
    V=cumsum(ob(1:ix,4));
    i=find(V>=myv(j),1,'first');
    inonnul=find(ob(:,3)~=0);
    ptemp=ob(:,3);
    if ~isempty(inonnul)
        inul=find(ob(:,3)==0);
        if ~isempty(inul)
            ptemp(inul)=mean(ob(inonnul,3));
        end
    else
        error=1;
    end

    if isempty(i)
        vr(j)=myv(j)-V(end);
        pexec(j)=sum(ptemp(1:ix).*ob(1:ix,4));
    else
        vr(j)=0;
        if i==1
            pexec(j)=ptemp(1)*myv(j);
        else
            pexec(j)=sum(ptemp(1:i-1).*ob(1:i-1,4))+ptemp(i)*(myv(j)-sum(ob(1:i-1,4)));
        end
    end
end