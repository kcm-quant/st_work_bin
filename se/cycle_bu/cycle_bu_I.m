function [quality res]=cycle_bu_I(mode, security_id, trading_destination_id, window_type, window_width, as_of_day, dt_remove, varargin_)
%[quality res]=cycle_I(1, 110, 4, [], 100, '01/02/2009')
uuu=1;
a=-0.08;
b=0.08;
mu=1;
part=5*0.01;
n_jour=5;% le nombre minimum pour verifier le crit�re d'�quilibre de volume
seuilsv=0.03;


% Para initial

c=[1 1.5 2 2.5];
cy=[2 4 8 15 20];
%cy=[5 10 20 40]; bustime
la=0;%[0 0.04  0.08];
R=[];ParaT=[];Para=[];
m=1;
for k1=1:length(cy)
    for k2=1:length(c)
        for k3=1:length(la)
            cym(m)=cy(k1);
            cm(m)=c(k2);
            lam(m)=la(k3);
            Para(m,:)=[cym(m) cm(m) lam(m)];
            m=m+1;
        end
    end
end
X=[];
ly=length(la);lx=length(c);lai=[];ci=[];xi=[];yi=[];
for i=ly:-1:1
    for j=1:lx
        y=i;% coordonn�es (x, y) de la g�om�trie 
        x=j;
        X(ly+1-i,j)=y*(lx-x)+(ly-y)*x;% aire d'�limination potentielle
        ci(i,j)=c(j);%c croissant en j
        lai(ly+1-i,j)=la(i);%l d�croissant en i 
        xi(i,j)=j;
        yi(i,j)=i;
    end
end
% mapping 2 dim -> 1 dim
ee=1:size(X,1)*size(X,2);
rX=reshape(X,1,size(X,1)*size(X,2));
rci=reshape(ci,1,size(ci,1)*size(ci,2));
rlai=reshape(lai,1,size(lai,1)*size(lai,2));
rxi=reshape(xi,1,size(xi,1)*size(xi,2));
ryi=reshape(yi,1,size(yi,1)*size(yi,2));
lpa=length(c)*length(cy)*length(la);


ParaT=[];ParaResT=[];
bl0=ones(ly,lx);
KT=[];
KT=cell(size(Para,1),1);
i_mis_day=[];i_mis_day_test=[];
window_width_test1=2*max(n_jour,window_width);%max(window_width,2*n_jour);
%window_width_test=2*n_jour;%max(window_width,2*n_jour);
%dateLearn=datenum(as_of_day, 'dd/mm/yyyy')-window_width:datenum(as_of_day, 'dd/mm/yyyy');
dateLearn_test1=datenum(as_of_day, 'dd/mm/yyyy')-window_width_test1:datenum(as_of_day, 'dd/mm/yyyy');

for i=1:length(dateLearn_test1)
    data_d = read_dataset('tickdb', 'security', security_id, 'from', dateLearn_test1(i),...
        'to', dateLearn_test1(i),'trading-destinations',trading_destination_id);
    if isempty(data_d.value)
        i_mis_day=[i_mis_day i];
        %mis_date_test=[mis_date_test data_d.date];
    end
end
%on prend d'abord la profondeur maximale
dateLearn_test=datenum(as_of_day, 'dd/mm/yyyy')-window_width_test1:datenum(as_of_day, 'dd/mm/yyyy');
%on enl�ve les dates manquantes
dateLearn_test(i_mis_day)=[];
%on garde les n_jour derniers jours
dateLearn_test = dateLearn_test(max(1,end-n_jour+1):end);
si=1:length(dateLearn_test);

dateLearn=datenum(as_of_day, 'dd/mm/yyyy')-window_width_test1:datenum(as_of_day, 'dd/mm/yyyy');
dateLearn(i_mis_day)=[];
dateLearn = dateLearn(max(1,end-window_width+1):end);
% pour un cy donn�, pour une strat�gie  donn�e (c,l),
% calculer KT sur plusieurs jours.

if length(dateLearn)>window_width/3
    for u1=1:length(cy)
        setnot0=1:ly*lx;
        xnul=logical(ones(1,ly*lx));%cy �tant fix�, une strat�gie <-> (l,c)
        while ~isempty(setnot0)
            [vz  iz]=sort(rX(setnot0));%parmi les strat�gies non encore test�e, prendre celle ayant le rX le plus grand
            is=setnot0(iz(end));% indice globale dans 1:ly*lx
            bl=[];bl=bl0;
            xit=rxi(is);% utilisation du mapping ds le sens 1D -> 2D
            yit=ryi(is);
            pa=[cy(u1) rci(is) rlai(is)];
            nump=find(Para(:,1)==pa(1) & Para(:,2)==pa(2) & Para(:,3)==pa(3));
            for i=si                                
                theta=[0 0 uuu part a b pa];
                dat=get_PLcycle_bu('read', 'security_id', security_id, 'day', dateLearn_test(i), 'theta', theta);
                if ~isempty(dat.value) 
                    KT{nump}=[KT{nump}; dat.value];% i : jour, k : strat�gie
                end
            end

            %mean(KT{nump}(:,2))
            if mean(KT{nump}(:,2))>seuilsv % supprime les strat�gies en slippage volumique trop grand                        
                bl(yit:end,xit:end)=0;xnult=[];
                xnult=logical(reshape(bl,1,size(X,1)*size(X,2)));% 2D -> 1D
                xnul=xnul & xnult;
                KT{nump}=[];
            elseif mean(KT{nump}(:,2))<-seuilsv % supprime les strat�gies en slippage volumique trop faible 
                bl(1:yit,1:xit)=0;xnult=[];
                xnult=logical(reshape(bl,1,size(X,1)*size(X,2)));
                xnul=xnul & xnult;
                KT{nump}=[];
            else
                xnul(is)=0; %on garde KT{nump} mais on met is � 0, puisque la strat�gie correspondante a �t� test�e.
            end
            setnot0=ee(xnul);
        end
    end


    %  Para possible
    inn=cellfun(@(x)~isempty(x),KT);
    ix=1:size(KT,1);
    ix=ix(inn);
    ktt1=arrayfun(@(k)mean(KT{ix(k)}(:,1)),1:length(ix));
    ktt2=arrayfun(@(k)mean(KT{ix(k)}(:,2)),1:length(ix));
    [v ix1]=sort(ktt1);
    iy=ix(ix1);
    ParaResT=[Para(iy,:) ktt1(ix1)' ktt2(ix1)'];
    Para=Para(iy,:);

    % KT : test sur ix0 des strat�gies Para
    KT=[];
    for i=1:length(dateLearn)                                                   
        for k=1:size(Para,1)
            pa=Para(k,:);       
            theta=[0 0 uuu part a b pa];
            dat=get_PLcycle_bu('read', 'security_id', security_id, 'day', dateLearn(i), 'theta', theta);
            if ~isempty(dat.value) 
                KT(i,k,:)=dat.value;% i : jour, k : strat�gie
            end
        end
    end            
    % puis sauvegarder � nouveau st_hash_table sur le disque
    if ~isempty(KT)
        get_PLcycle_bu('save', 'security_id', security_id)
        get_PLcycle_bu('clear', 'security_id', security_id);
        agreg = nanmean(KT(:,1,3));
        X1=arrayfun(@(i)nanmean(KT(:,i,1)),1:length(KT(1,:,1)))';
        X4=arrayfun(@(i)nanmean(KT(:,i,2)),1:length(KT(1,:,1)))';
        X2=arrayfun(@(i)quantile(KT(:,i,1),0.5),1:length(KT(1,:,1)))';
        X3=arrayfun(@(i)nanstd(KT(:,i,1)),1:length(KT(1,:,1)))';
        [v ix]=sort(X1);
        Xo=[Para(ix,:) X2(ix) X1(ix) X3(ix) X3(ix)/sqrt(size(KT,1)) X4(ix)];
        Xo(isnan(Xo(:,end)),:)=[];
        if isempty(Xo)
            error('NODATA_ROOT: KT empty, moins de 10 transactions par jour')
        end
        Y=[];
        if size(Xo,1)<10
            Y=zeros(10,size(Xo,2));
            Y(1:size(Xo,1),:)=Xo;
        else
            Y=Xo(1:10,:);
        end

        if mode==1
            quality=zeros(1,10);

            Z1=[];z1=1:5:size(KT,1);
            if z1(end)<size(KT,1)
                z1=[z1 size(KT,1)];
            end
            for j=1:size(KT,2)
                %Z1(j,:)=arrayfun(@(i)mean(KT(z1(i):z1(i+1)-1,j,1)),1:length(z1)-1);
                Z1(j,:)=arrayfun(@(i)nanmean(KT(z1(i):z1(i+1)-1,j,1)),1:length(z1)-1);
            end
            if ~isnan(sum(mean(Z1)))
                r=[];
                for j=1:size(Z1,2)
                   [v iy]=sort(Z1(:,j));
                   x=arrayfun(@(i)find(iy==i),1:length(iy));
                   r(j,:)=x;%classement de chaque indicateur ds l'ordre Para
                end
                mr=mean(r);
                [v classe]=sort(mr);

                for j=1:min(10,length(ix))
                    quality(j)=max(0,(10-find(classe==ix(j))+1)/10);
                end
            end


        elseif mode==0
            for j=1:10
                quality(j)=NaN;
            end
        end
    else
        error('NODATA_ROOT: KT empty, probably last <%d> days data empty', window_width)
        Y=zeros(10,8);
        Y(1,:)=[5 1 0 0 0 0 0 0];
        if mode==1
            for j=1:10
                quality(j)=0;
            end
        elseif mode==0
            for j=1:10
                quality(j)=NaN;
            end
        end
    end

else
    error('NODATA_ROOT: last <%d> days data 2/3 empty', window_width)
    Y=zeros(10,8);
    Y(1,:)=[5 1 0 0 0 0 0 0];
    if mode==1
        for j=1:10
            quality(j)=0;
        end
    elseif mode==0
        for j=1:10
            quality(j)=NaN;
        end
    end
end

%Y(:,1)=Y(:,1)*agreg);
Y(:,size(Y,2)+1)=agreg*ones(10,1);
    
res=st_data( 'init', 'title', 'cycleI_bu', 'value', Y', 'rownames', {'cycle','surmasse','distance','pl_med','pl_mean','pl_std','pl_std_norm','sv','agreg'}, ...
        'colnames', {'context1', 'context2', 'context3', 'context4', 'context5', 'context6', 'context7', ...
        'context8', 'context9', 'context10'}, 'date', (1:9)');
end
