function est_params=se_run_cycle_bu_I(security_id, trading_destination_id, window_type, window_width, as_of_day, dt_remove, varargin_)


[quality data] = cycle_bu_I(1,security_id, trading_destination_id, window_type, window_width, as_of_day, dt_remove, varargin_);

if isempty(data)
    error('No data for this job!')
end

val = st_data('cols', st_data('transpose', data), 'cycle;surmasse;distance;pl_med;agreg');
est_params = st_data( 'init', 'title', '', 'value', val', 'date', (1:5)', 'colnames', {'context1', ...
    'context2', 'context3', 'context4', 'context5', 'context6', 'context7', 'context8', 'context9', 'context10'});
est_params.rownames = {'cycle', 'surmasse', 'distance', 'pl_med', 'agreg'};
 
est_params.info.run_quality = quality;% st_data('col', data, '<<QUALITY_COL>>');