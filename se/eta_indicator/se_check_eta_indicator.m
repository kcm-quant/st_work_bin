function out = se_check_eta_indicator(security_id, trading_destination_id, window_type, window_width, as_of_day, dt_remove, varargin_)

% SE_CHECK_ETA_INDICATOR Check  de l'estimateur de viscosité
% se_check_eta_indicator(110, { 'MAIN' }, 'day', 14, '01/06/2008', [], [])

% author   : 'dcroize@cheuvreux.com'
% reviewer : 
% version  : '1'
% date     : '05/01/2009'
%
%
% Y EN AT IL VRAIMENT pour ce genre d'indicateur

out = 0;

end