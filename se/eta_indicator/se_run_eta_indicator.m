
function out = se_run_eta_indicator(security_id, trading_destination_id, window_type, window_width, as_of_date, ...
    dt_remove, varargin_)
% SE_RUN_ETA_INDICATOR Run de l'estimateur viscosité
% se_run_eta_indicator(110, { 'MAIN' }, 'day', 30, '07/06/2008', [], [])

% author   : 'dcroize@cheuvreux.com'
% reviewer : 
% version  : '1'
% date     : '05/01/2009'
%
% 

out = read_dataset(['gi:eta_indicator/' 'window:char' char(167) '"', sprintf('%s|%d', window_type, window_width)], ...
    'security_id', security_id, ...
    'trading-destinations', trading_destination_id, ...
    'from', as_of_date, ...
    'to', as_of_date);


if st_data('isempty-nl', out)
    error('No data for this job!')
end

out =  st_data('transpose', out);
out.colnames = {'context1'};
out.info.run_quality = 0.5;   % TODO REFLECHIR A UN QUALITY RUN! EN FAUT IL UN?

end

