function se_pre_validation_float_signal(varargin)
% SE_PRE_VALIDATION_FLOAT_SIGNAL - Validation des courbes cvdyn
%
%
%
% Examples:
% se_pre_validation_float_signal('session',786)
%
%
%
% See also: se_pre_validation_cvdyn, se_post_validation_cvdyn
%
%
%   author   : 'mlasnier@cheuvreux.com'
%   reviewer : ''
%   version  : '1'
%   date     : '09/07/2012'

global st_version
quant = st_version.bases.quant;

opt = options({'session', nan , ...
    'estimator_name','Float signal',...
    'owner',{},...
    },varargin);
if isempty(opt.get('owner'))
    opt.set('owner',exec_sql('QUANT',['select owner from ',quant,'..estimator where estimator_name = ''Float signal''']));
end

estimator_id = getfield(se_db('get_estimator' , 'estimator_name' , opt.get('estimator_name')), 'estimator_id' );

if isnan(opt.get('session'))
    opt.set('session',cell2mat(exec_sql('QUANT',['select max (session_num) from ',quant,'..session'])));
end

oldDataReturnFormat = setdbprefs('DataReturnFormat');
setdbprefs('DataReturnFormat','structure');
% R�cup�ration de tous les runs de la session opt.get('SESSION')
quals = exec_sql('QUANT',sprintf(['select js.job_or_run_id',...
    ' from ',quant,'..job_status js,',quant,'..estimator e,',quant,'..job j',...
    ' where e.estimator_name = ''Float signal''',...
    ' and e.estimator_id = j.estimator_id',...
    ' and js.session_num = %d',...
    ' and js.job_or_run_id = j.job_id',...
    ' and js.is_run =1'],opt.get('session')));
setdbprefs('DataReturnFormat',oldDataReturnFormat);

% Methodologie de validation :
% 1. On devalide tous les jobs de l'estimateur 'Float signal' qui ont
% tourn� pendans la session OPT.GET('SESSION')
% 2. On valide les runs qui ont tourn� durant la session OPT.GET('SESSION')
% -> Cela assure que les jobs qui ce sont plant�s pendant la sesssion
% courante ne sont pas valid�s.
try
    if isempty(quals)
        st_log(['<se_pre_validation_float_signal> No job to validate on estimator <Float signal>.',...
            ' Environment <%s>, session %d\n'],...
            st_version.my_env.target_name,opt.get('session'));
    else
        % Etape 1 :
        job_str = sprintf('%d,',quals.job_or_run_id);
        exec_sql('QUANT',['update ',quant,'..estimator_runs set is_valid = 0 from ',quant,'..estimator e',...
            ' where e.estimator_name = ''Float signal''',...
            ' and e.estimator_id = ',quant,'..estimator_runs.estimator_id',...
            ' and ',quant,'..estimator_runs.is_valid = 1',...
            ' and ',quant,'..estimator_runs.job_id in (',job_str(1:end-1),')']);
        
        run2val = cell2mat(exec_sql('QUANT',['select er.run_id from ',quant,'..estimator_runs er,',quant,'..estimator e',...
            ' where e.estimator_name = ''Float signal''',...
            ' and e.estimator_id = er.estimator_id',...
            ' and er.session_num = ',num2str(opt.get('session'))]));
        % Etape 2:
        if ~isempty(run2val)
            run_str = sprintf('%d,',run2val);
            exec_sql('QUANT',...
                sprintf(['update ',quant,'..estimator_runs set is_valid = 1 where run_id in (%s)',...
                ' and is_valid = 0',...
                ' and estimator_id = %d'],...
                run_str(1:end-1),estimator_id));
        end
    end
catch e
    st_log(sprintf('%s\n', e.message));
    subject    = sprintf('Echec de la validation de l''estimateur <Float signal> en environnement <%s>',st_version.my_env.target_name);
    message    = se_stack_error_message(e);
    recipients = unique([regexp(st_version.my_env.se_admin,',','split'),opt.get('owner')]);
    sendmail(recipients,subject,message);
end