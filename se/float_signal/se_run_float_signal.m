function data = se_run_float_signal(security_id, trading_destination_id, window_type, window_width, as_of_date, dt_remove, varargin_)
% SE_RUN_FLOAT_SIGNAL - calibrate the float signal
%
% Examples:
% data = se_run_float_signal(110, 4, 'day', 60, '25/04/2012', [], opt2str(options({'plot', 0})))
%
% See also:
%
%   author   : 'mdang@cheuvreux.com'
%   reviewer : ''
%   date     :  '05/07/2012'
%

if nargin==0
    security_id = 110;
    trading_destination_id = 4;
    window_width = 60;
    as_of_date = '25/04/2012';
    varargin_ = opt2str(options({'plot', 1}));
end
default_opt = {'v_quantile', 0.05:0.05:0.95,...
    'step_times_second', [60 120 300 600 900],...
    'offset_start_day', 1,...
    'offset_end_day', 0,...
    'plot', 0};

if nargin>6 && ~isempty(deblank(varargin_))
    opt = str2opt(varargin_);
    opt = options(default_opt, opt.get());
else
    opt = options(default_opt);
    if nargin < 6
        dt_remove = [];
    end
end

% get 4 indices
index_type = {'PLACE', 'MSCI_SECTOR', 'MSCI_ZONE', 'WORLD'};

index_id = NaN(1,4);
index_name = cell(1,4);
index_name(1) = get_repository('refindex','security_id', security_id, 'ref_type', 'place', 'output_mode','name');
index_id(1)   = get_repository('refindex','security_id',security_id,'ref_type','place');

index_name(2) = get_repository('refindex','security_id',security_id,'ref_type','msci_sector', 'output_mode','name');
index_id(2)   = get_repository('refindex','security_id',security_id,'ref_type','msci_sector');

index_name(3) = get_repository('refindex','security_id',security_id,'ref_type','msci_zone', 'output_mode','name');
index_id(3)   = get_repository('refindex','security_id',security_id,'ref_type','msci_zone');

index_name(4) = get_repository('refindex','security_id', security_id, 'ref_type','world', 'output_mode','name');
index_id(4)   = get_repository('refindex','security_id', security_id, 'ref_type','world');

index_name = index_name(~isnan(index_id));
index_id = index_id(~isnan(index_id));
if isempty(index_id)
    error('se_run_float_signal:get_data', 'NOINDEX_ID: No index id found for this security id');
end
nb_indexes = numel(index_id);

% compute daily beta
end_date = as_of_date;
begin_date = datestr(datewrkdy(datenum(end_date, 'dd/mm/yyyy'), -window_width), 'yyyymmdd');
end_date = datestr(datenum(end_date, 'dd/mm/yyyy'), 'yyyymmdd');

v_quantile = opt.get('v_quantile');
t_quantile = NaN(length(v_quantile), nb_indexes);
t_beta     = NaN(1, nb_indexes);
f_ret      = @(x)diff(x)./x(1:end-1);




% STEP 1: daily beta estimation
% get data
sec_data_is = exec_sql('BSIRIUS', sprintf('select date, close_prc from tick_db..trading_daily where security_id = %d and trading_destination_id = %d and date >= ''%s'' and date < ''%s''', ...
    security_id, trading_destination_id, begin_date, end_date));
if isempty(sec_data_is)
    error('se_run_float_signal:get_data', 'NO_DAILY_DATA_FOR_SEC_ID: No data in trading daily for the requested security, period and destination');
end
sec_data_date = datenum(sec_data_is(:,1), 'yyyy-mm-dd');
sec_data_price = cell2mat(sec_data_is(:,2));
for i = 1:nb_indexes
    index_data_is = exec_sql('BSIRIUS', sprintf('select date, close_prc from tick_db..indice_daily where indice_id = %d and date >= ''%s'' and date < ''%s''', ...
        index_id(i), begin_date, end_date));
    index_data_date = datenum(index_data_is(:,1), 'yyyy-mm-dd');
    index_data_price = cell2mat(index_data_is(:,2));
    % intersection
    [~, ia, ib] = intersect(sec_data_date, index_data_date);
    sec_price = sec_data_price(ia);
    index_price = index_data_price(ib);
    
    % linear regression
    y = f_ret(sec_price);
    x = f_ret(index_price);
    [beta, ~, r, ~, ~] = regress(y, x);
    t_quantile(:,i) = quantile(r, v_quantile);
    t_beta(i) = beta;    
end

% compute intraday residuals
OS_from = datestr(datewrkdy(datenum(end_date, 'yyyymmdd'), -12), 'dd/mm/yyyy');
OS_to = datestr(datenum(end_date, 'yyyymmdd'), 'dd/mm/yyyy');
all_price_intraday = read_dataset('tickdb', 'security', security_id,...
    'from', OS_from, 'to', OS_to, 'output-mode', 'day');
all_price_intraday = all_price_intraday(cellfun(@(c)~st_data('isempty-no_log',c), all_price_intraday));
if isempty(all_price_intraday)
    error('se_run_float_signal:get_data', 'NO_INTRADAY_DATA: No data in tickdb for the requested period and destination');
end
step_times_second  = opt.get('step_times_second');
step_times = datenum(0,0,0, 0,0,1)*step_times_second;

nb_step_times = numel(step_times_second);

trading_times = trading_time(get_repository('tdinfo', security_id), datenum(OS_from, 'dd/mm/yyyy'));
offset_start_day = opt.get('offset_start_day');
offset_end_day = opt.get('offset_end_day');
start_day = trading_times.opening + datenum(0,0,0,0, offset_start_day,0);
end_day = trading_times.closing_auction + datenum(0,0,0,0, offset_end_day,0);

tracking_errors = cell(nb_indexes, nb_step_times);
nb_vals_per_day = cell(nb_indexes, nb_step_times);
ref_price_per_day = cell(nb_indexes, nb_step_times);
sampled_price_per_day = cell(nb_indexes, nb_step_times);
lag_1_sampled_price_per_day = cell(nb_indexes, nb_step_times);

for d = 1:numel(all_price_intraday)
    % intraday price
    % convert to GMT
    price_intraday = timezone('back_to_gmt', 'data', all_price_intraday{d});
    delta_gmt = all_price_intraday{d}.date(1) - price_intraday.date(1);
    current_day = datestr(price_intraday.date(1), 'dd/mm/yyyy');
    price_date = rem(price_intraday.date,1);
    price_intraday = st_data('col', price_intraday, 'price');
    
    for i = 1:nb_indexes
        % index price
        price_intraday_index = get_price('index', 'security_id', index_id(i), 'from', current_day, 'to', current_day);
        index_date = rem(price_intraday_index.date,1);
        price_intraday_index = st_data('col', price_intraday_index, 'price');
        
        for s = 1:nb_step_times
            step_time = step_times(s);
            time_out  = (start_day:step_time:end_day) - delta_gmt;
            
            sub_price_intraday = price_intraday(price_date <= index_date(end) & price_date >= index_date(1));
            sub_price_date = price_date(price_date <= index_date(end) & price_date >= index_date(1));
            
            idx_time_price = arrayfun(@(t)sum(find(sub_price_date<=t, 1, 'last')), time_out);
            idx_time_price = idx_time_price(idx_time_price > 0);
            
            nb_vals_per_day{i,s} = vertcat(nb_vals_per_day{i,s}, length(idx_time_price));
            
            sub_price_intraday = sub_price_intraday(idx_time_price);
            sub_price_date = sub_price_date(idx_time_price);
            
            stock_ret = f_ret(sub_price_intraday);
            
            idx_time_index = arrayfun(@(t)sum(find(index_date<=t, 1, 'last')), sub_price_date);
            sub_index_intraday = price_intraday_index(idx_time_index(idx_time_index > 0));
            
            index_ret = f_ret(sub_index_intraday);
            tracking_error_day = stock_ret - t_beta(i)*index_ret;
            tracking_errors{i,s} = vertcat(tracking_errors{i,s}, tracking_error_day);
            
            med_eps = quantile(tracking_errors{i,s}, .5);
            ref_price = sub_price_intraday(1:end-1) .* (1 + t_beta(i)*index_ret + med_eps);
            ref_price_per_day{i,s} = vertcat(ref_price_per_day{i,s}, ref_price);
            
            sampled_price_per_day{i,s} = vertcat(sampled_price_per_day{i,s}, sub_price_intraday(2:end));
            lag_1_sampled_price_per_day{i,s} = vertcat(lag_1_sampled_price_per_day{i,s}, sub_price_intraday(1:end-1));
        end
    end
end

% intraday TE
quantile_intraday = cell(nb_indexes, 1);

for i = 1:nb_indexes
    quantile_intraday_tempo = NaN(numel(v_quantile), nb_step_times);
    for s = 1:nb_step_times
        quantile_intraday_tempo(:,s) = (quantile(tracking_errors{i,s}, v_quantile))';
    end
    quantile_intraday{i} = quantile_intraday_tempo;
end

% median of number of values per day
med_nb_vals_per_day = NaN(size(step_times));
for s = 1:nb_step_times
    med_nb_vals_per_day(s) = median(nb_vals_per_day{i,s});
end

% distance
d_infty = NaN(nb_indexes, nb_step_times);
d_1 = NaN(nb_indexes, nb_step_times);
for i = 1:nb_indexes
    for s = 1:nb_step_times
        d_1(i,s) = 1e4*sum(abs(t_quantile(:,i) - sqrt(med_nb_vals_per_day(s)) * quantile_intraday{i}(:,s)));
        d_infty(i,s) = 1e4*max(abs(t_quantile(:,i) - sqrt(med_nb_vals_per_day(s)) * quantile_intraday{i}(:,s)));
    end
end

% correlation with (instantaneous, next, final) spread
inst_corr = NaN(nb_indexes, nb_step_times);
next_corr = NaN(nb_indexes, nb_step_times);
last_corr = NaN(nb_indexes, nb_step_times);
for i = 1:nb_indexes
    for s = 1:nb_step_times
        ref_prices = ref_price_per_day{i,s};
        sampled_prices = sampled_price_per_day{i,s};
        nb_sampled_prices = numel(sampled_prices);
        lag_1_sampled_prices = lag_1_sampled_price_per_day{i,s};
        spread = ref_prices - sampled_prices;
        inst_corr(i,s) = corr(spread, lag_1_sampled_prices - sampled_prices );
        next_corr(i,s) = corr(spread(1:end-1), diff(sampled_prices));
        last_corr(i,s) = corr(spread, cumsum(sampled_prices)./(nb_sampled_prices:-1:1)' - sampled_prices);
    end
end

% ouput data
rownames = arrayfun(@(v)sprintf('QUANTILE_%02.0f', v), 1e2*v_quantile', 'uni', false);
rownames = vertcat(rownames, {'INDEX'; 'DURATION'; 'BETA'; 'MEDIAN_RESIDUALS';...
    'MEAN_RESIDUALS'; 'STD_RESIDUALS'; 'QQ_DISTANCE';...
    'INST_CORR'; 'NEXT_CORR'; 'LAST_CORR'});
colnames = {};
values = [];
run_quality = [];
for i = 1:nb_indexes
    for s = 1:nb_step_times
        colnames = horzcat(colnames, sprintf('INDEX_%s_DURATION_%2.0f', index_type{i}, step_times_second(s)));
        values   = horzcat(values, vertcat(quantile(tracking_errors{i,s}, v_quantile'),...
            [index_id(i); step_times_second(s); t_beta(i);...
            median(tracking_errors{i,s}); mean(tracking_errors{i,s}); std(tracking_errors{i,s}); ...
            d_1(i,s); inst_corr(i,s); next_corr(i,s); last_corr(i,s)]));
        run_quality = horzcat(run_quality, next_corr(i,s));
    end
end

% select the (best index, best duration)
used_distance = -next_corr;
[distance_best, best_duration] = min(used_distance, [], 2);
[~, best_index] = min(distance_best);
best_duration = best_duration(best_index);
colnames = horzcat(colnames, 'BEST_INDEX');
values   = horzcat(values, vertcat(quantile(tracking_errors{best_index,best_duration}, v_quantile'),...
    [index_id(best_index); step_times_second(best_duration); t_beta(best_index); ...
    median(tracking_errors{best_index,best_duration}); mean(tracking_errors{best_index,best_duration}); std(tracking_errors{best_index,best_duration}); ...
    abs(used_distance(best_index,best_duration)); ...
    inst_corr(best_index,best_duration); next_corr(best_index,best_duration); last_corr(best_index,best_duration)]));
run_quality = horzcat(run_quality, abs(used_distance(best_index,best_duration)));

data = st_data('init', 'title', 'Float Signal Calibration', ...
    'date', (1:size(rownames,1))', ...
    'value', values, ...
    'colnames', colnames,...
    'rownames', rownames);
data.info.run_quality = 1e-4*run_quality;

if opt.get('plot')
    for i=1:nb_indexes
        get_focus('QQ plot');
        clf
        hold on
        plot(t_quantile(:,i), sqrt(med_nb_vals_per_day(s))*quantile_intraday{i});
        plot(t_quantile(:,i), t_quantile(:,i), 'k--');
        hold off
        legend(arrayfun(@(s)sprintf('%d s', s), step_times_second, 'uni', false));
        title(sprintf('QQ-plot for index %d - %s', index_id(i), index_name{i}));
        xlabel('daily quantile');
        ylabel('normalized intraday quantile');
        pause
    end
    
    get_focus('Correlation plot');    
    clf
    linetypes = {'--', '-', '-.', ':'};
    colors = lines(nb_indexes);
    h1 = subplot(3,1,1);
    hold on
    for i=1:nb_indexes    
        plot(h1, step_times_second, inst_corr(i,:), linetypes{i}, 'color', colors(i,:), 'linewidth', 2);        
    end
    hold off
    legend(index_name);
    title(h1, 'Correlation-plot');
    xlabel(h1, 'duration (s)');
    ylabel(h1, 'inst. correlation');
    
    h2 = subplot(3,1,2);
    hold on
    for i=1:nb_indexes    
        plot(h2, step_times_second, next_corr(i,:), linetypes{i}, 'color', colors(i,:), 'linewidth', 2);        
    end
    hold off    
    xlabel(h2, 'duration (s)');
    ylabel(h2, 'next correlation');
    
    h3 = subplot(3,1,3);
    hold on
    for i=1:nb_indexes    
        plot(h3, step_times_second, last_corr(i,:), linetypes{i}, 'color', colors(i,:), 'linewidth', 2);        
    end
    hold off    
    xlabel(h3, 'duration (s)');
    ylabel(h3, 'last correlation');
end