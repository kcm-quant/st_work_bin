function res = se_check_heatmap (security_id, trading_destination_id, window_type, window_width, as_of_day,data_heatmap) 
% se_check_heatmap - 
%
% example
%  out = se_run_heatmap('FTE.PA',[], [], 30, '29/03/2009')
%  res  = se_check_heatmap( 110, {}, [], 5, '29/03/2009',out),
%  data)



% Resultats
mean_old= st_data('cols',st_data('transpose', data_heatmap),'qmax',1)/5;

var_liq = st_data('cols', st_data('transpose', data_heatmap),'var_liq_res',1);

data_new = se_run_heatmap (security_id, trading_destination_id, window_type, window_width, as_of_day, '', '');

[data b] = st_data('isempty', data_new);
if b
    error('No data for the Heatmap of security_id=%d, trading_destination_id=%d, on %s', ...
        security_id, trading_destination_id, as_of_day)
end 

mean_new= st_data('cols',st_data('transpose', data_new),'qmax',1)/5;
if isempty(mean_old)
    mean_old=mean_new;
end

res=(log(mean_new)-log(mean_old))/var_liq;
