function out = se_run_heatmap (security_id, trading_destination_id, window_type, window_width, as_of_day, dt_remove, varargin_)

% SE_RUN_HEATMAP - function that interface the heatmap
%
% example:
%  out = se_run_heatmap('FTE.PA',[], [], 30, '03/11/2008')
%

r=0.33;
as_of_day1=as_of_day;

    
data = read_dataset('btickdb', 'security_id',  security_id, 'from', datestr(datenum(as_of_day1, 'dd/mm/yyyy')-window_width,'dd/mm/yyyy') , 'to', as_of_day1  ...
           ,'trading-destinations', { 'main' } );
if 1%~isempty(data.value)
    u=tokenize(get_repository( 'security-key', security_id),'.');
    if length(u)==2
        u=u{2};
    elseif length(u)==1
        u=tokenize(get_repository( 'security-key', security_id),'_');
        if length(u)==1
            error('REPOSITORY: je ne reconnais pas le RIC de <%d>', security_id);
        else
            u=u{2};
        end
    end

    coeff=1;
    if strcmp(u,'L') || strcmp(u,'LN')
        coeff=0.01;
    end
    if isempty( data.value)
        error('se_run_heatmap:data', 'NODATA: no data for %d the %s', security_id, as_of_day1);
    end
    meanprice = coeff*nanmean(data.value(:,1));

    x1=data.value(:,1).*data.value(:,2);
    x1=log(x1(x1>0));

    ats = coeff*mean(data.value(:,1).*data.value(:,2));


    qmin = 2*ats;

    qmax = 5*ats;


     q=quantile(x1,0:0.05:1);

     q1 = q(2);
     m  = q(11);
     q2 = q(end-1);
     s  = std(x1);
     s0=1.65;
     e=max((m-q1)/s,(q2-m)/s )/s0 ;

     var_liq_res = s;

     out = st_data('init', 'title', 'heatmap', 'value', [r;qmin/meanprice; qmax/meanprice;var_liq_res], 'colnames', {'Euromillenium'}, ...
         'date', repmat(as_of_day,4,1), 'rownames', {'r';'qmin';'qmax';'var_liq_res'});

    out.info.run_quality = 1/e;
else
    error('NODATA_ROOT: pas de donn�e pour ce titre')
end

