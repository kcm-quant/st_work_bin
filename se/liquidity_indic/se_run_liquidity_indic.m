function out = se_run_liquidity_indic(security_id, trading_destination_id, window_type, window_width, as_of_date, dt_remove ,varargin_)
% SE_RUN_LIQUIDITY_INDIC Run de l'estimateur liquidity_indic
% se_run_liquidity_indic(110, {}, 'day', 14, '01/06/2008', [], [])

% author   : 'dcroize@cheuvreux.com'
% reviewer : 
% version  : '1'
% date     : '31/12/2008'
%

name = (fullfile(getenv('st_work'),'bin','graph_indicators','projects'));
cd(name);
load('liquidity_indicator.mat')
if isempty(gr)
     st_log('...se_runliquidity_indic - Please check that the garaph for liquidity_indicator exists in the SE, without it , it could not work ')
end
gr.init();
gr.set_param('st read dataset', 'RIC:char', security_id)
gr.set_param('st read dataset', 'trading-destinations:cell', trading_destination_id)
gr.set_param('st read dataset', 'from:dd/mm/yyyy',as_of_date )
gr.set_param('st read dataset', 'to:dd/mm/yyyy', as_of_date)
gr.set_param('st read dataset', 'source:char', 'daily');

gr.next();
out = gr.get_memory( 'liquidityIndic');

%detection des NANs s'il y en a et verifier pkoi?
out{2}.value(isnan(out{2}.value)) = -9999;
out =  st_data('transpose', out{2});

out.colnames = {'context1','context2','context3','context4','context5','context6'};
out.info.run_quality = repmat(0.5, 1, 6);

end
