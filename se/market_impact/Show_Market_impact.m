function Show_Market_impact(mode,varargin)
  
  switch lower(mode)
      case 'run'
                opt_run = options({'security_id',110,'trading_destination_id',4,'window_width',1,'window_type',1,'as_of_date','','varargin',''},varargin); 
          
                 security_id = opt_run.get('security_id');
                 trading_destination_id = opt_run.get('trading_destination_id');        
                 window_width = opt_run.get('window_width');
                 window_type = opt_run.get('window_type');
                 as_of_date   =  opt_run.get('as_of_date');           
                 endd   = datenum(as_of_date,'dd/mm/yyyy');
                 begind = endd - window_width;
                 from_date = datestr(begind,'dd/mm/yyyy');
                 to_date   = datestr(endd,'dd/mm/yyyy');        
                 varargin_ = opt_run.get('varargin');

                 
      case 'check'
                opt_check = options({'security_id',110,'trading-destinations',4,'window_width',1,'window_type',1,'as_od_date','','data',st_data( 'empty-init')},varargin);     
                                      
                security_id  = opt_check.get('security_id');
                trading_destinations = opt_check.get('trading-destinations');
                window_width = opt_check.get('window_width'); 
                window_type = opt_check.get('window_type'); 
                as_of_date =  opt_check.get('as_of_date');           
                data = opt_check.get('data');
                
                se_vcheck_mi(security_id, trading_destinations, window_type, window_width, as_of_date, data);
               
      otherwise
          error('Show_Market_impact:Show_Market_impact','mode <%s> unknown',mode);
                
  end
end