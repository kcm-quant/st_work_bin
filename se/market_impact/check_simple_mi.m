function q = check_simple_mi( params, security_id, trading_destinations, a_day, window_type, window_width)
% CHECK_SIMPLE_MI - check quality for the simple MI model
%
% use:
%   q = check_simple_mi( params, security_id, trading_destinations, a_day, 'day', 60)
% example:
%   params = struct('s0', 4.48, 'kappa', 2.82, 'gamma', 1.35)
%   q = check_simple_mi( params, 'FTE.PA', {}, '14/04/2008', 'day', 60)
%
% See also set_params_process4market_impact_01b
q = [];
data = read_dataset(['gi:preprocess4market_impact_02/step:time' char(167) ...
    '0.00034722' char(167) 'window:char' char(167) '"' window_type char(124) num2str(window_width) char(167) 'window:time' char(167) '0.010417'], ...
     'security_id', security_id, ...
     'from', a_day, 'to', a_day, 'trading-destinations', trading_destinations);
if isempty(data) || isempty(data.value)
    q = 0;
    return;
end
% correspond � la deuxi�me bo�te du graphe d'estimation: 
vals = st_data('col', data, 'Normalized volume;dS;vwas');
data.value = [exp(vals(:,1))-1,vals(:,2),vals(:,3)];
data.colnames = { 'rho', 'dS', 'spread'};
% correspond � la trois�me bo�te
data = st_data('where', data, '({rho}<=12)&({rho}>0)');
if isempty(data) || isempty(data.value)
    q = 0;
    return;
end
rho = st_data('col', data, 'rho');
dS  = st_data('col', data, 'dS');
d_dS = dS - params.s0 + params.kappa * rho.^params.gamma;
q    = 1/sqrt(mean((d_dS).^2));


