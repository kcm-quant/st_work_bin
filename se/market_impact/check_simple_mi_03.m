function q = check_simple_mi_03( params, security_id, trading_destinations, a_day, window_type, window_width)
% CHECK_SIMPLE_MI_03 - check quality for the simple MI model version 03
%
% use:
%   q = check_simple_mi_03( params, security_id, trading_destinations, a_day, 'day', 1)
% example:
%   params = struct('s0', 4.48, 'kappa', 2.82, 'gamma', 1.35)
%   q = check_simple_mi_03( params,2, {},'04/12/2009','day', 1)
%
% See also set_params_process4market_impact_03
q = [];

[from, to] = se_window_spec2from_to(window_type, window_width, a_day);

data = read_dataset(['gi:preprocess4market_impact_03/step:time' char(167) '0.00034722'  char(167)...
     'imbalance_add_threshold' char(167) '0.7' char(167) ...
     'window:time' char(167) '0.010417'], ...
     'security_id', security_id, ...
     'from', from, 'to', to, 'trading-destinations', trading_destinations);
 
if isempty(data) || isempty(data.value)
    q = 0;
    return;
end
% correspond � bo�te "usual" du graphe d'estimation "preprocess4market_impact_03": 
data = st_data('where', data, '({rho_v}<=3)&({rho_v}>0.2)');
if isempty(data) || isempty(data.value)
    q = 0;
    return;
end
rho = st_data('col', data, 'rho_v');
dS  = st_data('col', data, 'delta_dS_side');
d_dS = dS - params.s0 + params.kappa * rho.^params.gamma;
q    = 1/sqrt(mean((d_dS).^2));






