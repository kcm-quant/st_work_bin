function q = se_check_mi(security_id, trading_destination_id, window_type, window_width, as_of_day, data) 
% SE_CHECK_MI - function that check the simple market impact model
%
% use:
%  q = se_check_mi(security_id, trading_destination_id, window_type, window_width, as_of_day, data) 
%  window_type, window_width are not used: always day/1
% example:
%  params = st_data('init', 'colnames', {'s0', 'kappa', 'gamma'}, ...
%      'value', [ 4.48, 2.82, 1.35], 'date', 1, 'title', 'MI params')
%   q = se_check_mi( 'FTE.PA', {}, [],[], '14/04/2008', params)
% 
% See also check_simple_mi se_run_mi  set_params_process4market_impact_01b 

params = struct('s0', st_data('col', st_data('transpose', data), 's0',1), ...
    'kappa', st_data('col', st_data('transpose', data), 'kappa',1), ...
    'gamma', st_data('col', st_data('transpose', data), 'gamma',1));

q = tanh( 10*check_simple_mi( params, security_id, trading_destination_id, as_of_day, window_type, window_width));