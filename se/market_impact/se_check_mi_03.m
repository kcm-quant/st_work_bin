function q = se_check_mi_03(security_id, trading_destination_id, window_type, window_width, as_of_day, data) 
% SE_CHECK_MI_03 - function that check the simple market impact model v03

% use:
%  q = se_check_mi_03(security_id, trading_destination_id, window_type, window_width, as_of_day, data) 
% example:
%   params = se_run_mi_03('ACCP.PA',{}, 'day', 60, '04/10/2009', [])
%   q = se_check_mi_03('ACCP.PA', {}, 'day',1, '04/12/2009', params)
% 
% See also check_simple_mi_03 se_run_mi  set_params_process4market_impact_03 

params = struct('s0', st_data('col', st_data('transpose', data), 's0',1), ...
    'kappa', st_data('col', st_data('transpose', data), 'kappa',1), ...
    'gamma', st_data('col', st_data('transpose', data), 'gamma',1));

q = tanh( 10*check_simple_mi_03( params, security_id, trading_destination_id, as_of_day, window_type, window_width));