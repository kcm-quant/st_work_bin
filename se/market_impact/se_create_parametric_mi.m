function mi_data = se_create_parametric_mi(varargin)
% create_parametric_mi : create an st_data with the parametric mi value
% cac_list=get_repository('index-comp','CAC40');
% mi=create_parametric_mi('security_id',cac_list,'date','30/06/2010')


opt  = options( {'security_id',110,...
    'date','29/06/2010',...
    'PARAMS', [2.27 0.72 1.11 0.51 0.41 0.11 -0.03],...
    'NB_DAY_MIN',20,...
    'NB_DAY_HISTO',60}, varargin);

% fit sur le wiki
% [2.27 0.72 1.11 0.52 0.42 0.11 -0.03]


sec_id=opt.get('security_id');
date=opt.get('date');
NB_DAY_MIN=opt.get('NB_DAY_MIN');
NB_DAY_HISTO=opt.get('NB_DAY_HISTO');
PARAMS=opt.get('PARAMS');

params_mi=NaN(length(sec_id),5);

for i=1:length(sec_id)
    sec_id_tmp=sec_id(i);
    
    
    trdt_info=get_repository('tdinfo',sec_id_tmp);
    td_id_main=trdt_info(1).trading_destination_id;
    
    stats=compute_trading_daily_stats(sec_id_tmp,date,td_id_main,NB_DAY_HISTO,NB_DAY_MIN);
    
    if ~isempty(stats)
        phi_m=0.5*stats(1);
        volatility_m=stats(2);
        
        s0_hat=PARAMS(1)+PARAMS(2)*phi_m;
        kappa_hat=PARAMS(3)+PARAMS(4)*volatility_m;
        gamma_hat=exp(PARAMS(5)+PARAMS(6)*log(phi_m)+PARAMS(7)*log(volatility_m.^2));
        gamma_hat=max(1,min(gamma_hat,1.8));
        
        params_mi(i,:)=[phi_m volatility_m s0_hat kappa_hat gamma_hat];
        %     mi=[s0_hat kappa_hat gamma_hat];
        % else
        %     mi=[];
    end
      mi=cat(2,sec_id,params_mi);
    
end

mi_data = st_data( 'init', 'Mi', 'Mes Parametres', 'value', mi, ...
                   'date', (1:size(mi,1))', 'colnames', { 'security_id', 'phi_m' ,'volatility_m' ,'s0', 'kappa', 'gamma' });
end




function stats_out=compute_trading_daily_stats(sec_id,date,td_id_main,NB_DAY_HISTO,NB_DAY_MIN)

stats_out=[];
try
    func_vol_GK=@(x,o,h,l,c)(sqrt(((x(:,h)-x(:,l)).^2/2-(2*log(2)-1)*(x(:,c)-x(:,o)).^2 )./ ( (0.5*(x(:,o)+x(:,c))).^2 * 51 )  ) * 10000);
    [from,to] = se_window_spec2from_to('day',190,date);
    
    res_trading_daily=exec_sql('BSIRIUS',sprintf(['select '...
        'date,volume,turnover,nb_deal,open_prc,high_prc,low_prc,close_prc,average_spread_numer,average_spread_denom'...
        ' from tick_db..trading_daily '...
        ' where security_id = %d and date>=''%s'' and  date<=''%s'' and trading_destination_id=%d'],...
        sec_id,datestr(from,'yyyymmdd'),datestr(to,'yyyymmdd'),td_id_main));
    
    if size(res_trading_daily,1)>NB_DAY_HISTO
        res_trading_daily=res_trading_daily(end-NB_DAY_HISTO-1:end,:);
    end
    
    value_trading_daily=cell2mat(res_trading_daily(:,2:end));
    if size(value_trading_daily,1)<NB_DAY_MIN
        return
    end
    %%% compute vals
    vol_gk=func_vol_GK(value_trading_daily,4,5,6,7);
    spread_trading_daily=value_trading_daily(:,8)./value_trading_daily(:,9);
    
    stats_out=cat(2,nanmean(spread_trading_daily),sqrt(nanmean(vol_gk.^2)));
catch
end
end