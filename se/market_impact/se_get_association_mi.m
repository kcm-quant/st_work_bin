function out = se_get_association_mi (security_id,trading_destination_id,as_of_date)
% SE_GET_ASSOCIATION_MI

global st_version;
bq = st_version.bases.quant;

ESTIMATOR_NAME  ='Market Impact' ;

trdt_info=get_repository('tdinfo',security_id);
td_id_main=trdt_info(1).trading_destination_id;

run_ids_info_1=st_data('empty-init');


% Selection de l'association
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

assoc_1 = exec_sql('QUANT', sprintf( [...
    'SELECT job_id,varargin from %s..association ass, %s..estimator est '...
    'WHERE ass.security_id=%d AND '...
    'ass.trading_destination_id=%d AND ' ...
    'upper(est.estimator_name) =''%s'' AND ' ...
    'ass.estimator_id=est.estimator_id ' ],bq, bq ,security_id, td_id_main, upper(ESTIMATOR_NAME)));
if ~isempty(assoc_1)
    run_ids_info_1 = se_get_run_frm_job('run' , cell2mat(assoc_1(:, 1)), 'is_valid', 1);
end

if st_data('isempty-nl',run_ids_info_1)
    mi = se_create_parametric_mi('security_id',security_id,...
        'trading_destination_id',trading_destination_id,...
        'date',as_of_date);
    mi = st_data( 'keep-cols', mi, 's0;kappa;gamma');
    if isempty(sum(isnan(mi.value)))
        error('se_get_association_mi:no parametric mi');
    end
else
    %%%%%%%%%%% ATTENTION  %%%%%%%%%%%%%%%%%
    job_ids=assoc_1{:, 1};
    job_ids=unique(job_ids);
    %%%%%%%%%%%%  !!!!!!!!!!!!!!!!!!!  %%%%%%%%%%%%%%%%%
    run_ids_info4this_job = st_data('where', run_ids_info_1, sprintf('{job_id}==%d',job_ids));
    
    params = se_get_param(st_data('cols', run_ids_info4this_job, 'run_id'));
    if isempty(params)
        error('se_get_association_mi:no params');
    end
    
    mi = st_data('transpose', params);
    mi.colnames= params.rownames;
    if st_data('isempty-nl', mi)
        error('se_get_association_mi:no mi');
    end
    title = exec_sql('QUANT',sprintf( 'select domain_name from %s..domain where domain_id = %d', bq, unique(st_data('cols', run_ids_info4this_job, 'domain_id'))));
    mi.title=title{1};
    
end

out = st_data('init', ...
    'title', ESTIMATOR_NAME, ...
    'date', datenum(as_of_date, 'dd/mm/yyyy'), ...
    'value', [mi.value 0], ...
    'colnames', {mi.colnames{:}, 'default'});


end