function est_params = se_run_mi(security_id, trading_destination_id, window_type, window_width, as_of_day, dt_remove, varargin_)
% SE_RUN_MI - function that interface simple market impact model
%
% use:
%  est_params = se_run_mi(security_id, trading_destination_id, window_type,
%  window_width, as_of_day)
% example:
%  est_params = se_run_mi('FTE.PA',{}, 'day', 60, '07/04/2008', [])
%
% See also se_check_mi  set_params_process4market_impact_01b

data = read_dataset( sprintf('gi:process4market_impact_01b/window:char%s"%s|%d', char(167), window_type, window_width) , ...
    'security_id',security_id, 'from', as_of_day, 'to', as_of_day,  ...
    'remove',dt_remove, ...
    'trading-destinations', trading_destination_id);

if st_data('isempty-nl', data)
    error('se_run_mi:exec', 'NOT_ENOUGH_DATA: Empty set returned by read_dataset used with gi:process4market_impact_01b');
end

est_params = st_data('keep', data, 's_0;\kappa;\gamma');
est_params.value = est_params.value';
est_params.rownames = { 's0'; 'kappa'; 'gamma' };
est_params.colnames = { 'Overall' };
%est_params.info.run_quality = tanh( st_data('col', data, '\gamma_{conf}'));


 