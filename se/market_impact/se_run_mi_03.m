function est_params = se_run_mi_03(security_id, trading_destination_id, window_type, window_width, as_of_day, dt_remove, varargin_)
% se_run_mi_03 - function that interface simple market impact model
%
% use:
%  est_params = se_run_mi_03(security_id, trading_destination_id, window_type,
%  window_width, as_of_day)
% example:
%  est_params = se_run_mi_03('AKGRT.IS',{}, 'day', 60, '04/01/2010', [])
%  est_params = se_run_mi_03('ACCP.PA',{}, 'day', 60, '12/12/2009', [])
%  est_params = se_run_mi_03('BMWG.DE',{}, 'day', 60, '04/12/2008', [])
%
% See also se_check_mi_03  set_params_process4market_impact_03

tdinfo = get_repository('tdinfo', security_id);
switch lower(tdinfo(1).global_zone_name)
    case {'america','asia'}
data = read_dataset(sprintf('gi:process4market_impact_03/my-window:char%s"%s|%d%sstep:time%s%s'...
    ,char(167),window_type,window_width,char(167),char(167),num2str(datenum(0,0,0,0,1,0))), ...
    'security_id',security_id, 'from', as_of_day, 'to', as_of_day,  ...
    'remove',dt_remove, ...
    'trading-destinations', trading_destination_id);
case 'europe'
    data = read_dataset(sprintf('gi:process4market_impact_03/my-window:char%s"%s|%d',char(167),window_type,window_width) , ...
        'security_id',security_id, 'from', as_of_day, 'to', as_of_day,  ...
        'remove',dt_remove, ...
        'trading-destinations', trading_destination_id);
end
if st_data('isempty-nl', data)
    error('se_run_mi:exec', 'NOT_ENOUGH_DATA: Empty set returned by read_dataset used with gi:process4market_impact_03');
end

est_params = st_data('keep', data, 's_0;\kappa;\gamma');
est_params.value = est_params.value';
est_params.rownames = { 's0'; 'kappa'; 'gamma' };
est_params.colnames = { 'Overall' };
%est_params.info.run_quality = tanh( st_data('col', data, '\gamma_{conf}'));
