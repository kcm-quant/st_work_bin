function q = se_vcheck_mi(security_id, trading_destination_id, window_type, window_width, as_of_day, data) 
% SE_VCHECK_MI - function that visually check the simple market impact model
%
% use:
%  q = se_check_mi(security_id, trading_destination_id, window_type, window_width, as_of_day, data) 
%  window_type, window_width are not used: always day/1
% example:
%  params = st_data('init', 'colnames', {'s0', 'kappa', 'gamma'}, ...
%      'value', [ 4.48, 2.82, 1.35; 1, 2, 3], 'date', [1;2], 'title', 'MI params')
%   q = se_vcheck_mi( 'FTE.PA', {}, [],[], '14/04/2008', params)
% 
% See also check_simple_mi se_run_mi  set_params_process4market_impact_01b 

params = struct('s0', st_data('col', data, 's0',1), ...
    'kappa', st_data('col', data, 'kappa',1), ...
    'gamma', st_data('col', data, 'gamma',1));

% q = check_simple_mi( params, security_id, trading_destination_id, as_of_day);

q = [];
data = read_dataset('gi:preprocess4market_impact_02/step:time�0.00034722�window:char�"day|180�window:time�0.010417', ...
     'security_id', security_id, ...
     'from', as_of_day, 'to', as_of_day, 'trading-destinations', trading_destination_id);
if isempty(data) || isempty(data.value)
    return
end
% correspond � la deuxi�me bo�te du graphe d'estimation: 
vals = st_data('col', data, 'Normalized volume;dS;vwas');
data.value = [vals(:,1)-1,vals(:,2),vals(:,3)];
data.colnames = { 'rho', 'dS', 'spread'};
% correspond � la trois�me bo�te
data = st_data('where', data, '({rho}<=12)&({rho}>0)');
if isempty(data) || isempty(data.value)
    return
end
rho = st_data('col', data, 'rho');
dS  = st_data('col', data, 'dS');
d_dS = params.s0 + params.kappa * rho.^params.gamma;
q    = 1/std(dS-d_dS);
sec_id = security_id;
if isnumeric(sec_id)
    sec_id = get_repository( 'security-key', sec_id);
end
this_date = as_of_day;
if isnumeric( this_date)
    this_date = datestr( this_date, 'dd/mm/yyyy');
end
fname = [ sec_id ' - ' this_date ];
h = get_focus( fname);
clf

a1=subplot(3,3,1);
plot(dS, rho, '.', 'linewidth',2);
hold on
ax =axis;
plot([params.s0 params.s0], ax(3:4),'k');
hold off
text(params.s0,mean(ax(3:4)),sprintf(' s_0=%3.2f', params.s0));
axis(ax);
ylabel('\rho');xlabel('dS');

a2=subplot(3,3,4);
plot(dS, d_dS, '.', 'linewidth',2);
ax =axis;
hold on
plot([params.s0 params.s0], ax(3:4),'k');
hold off
ylabel('$\hat{dS}$', 'interpreter', 'latex');
axis([min(dS) max(dS) min(d_dS) max(d_dS)]);
linkaxes([a2,a1],'x');

a3=subplot(3,3,[2 3 5 6]);
mds = median( dS-d_dS);
[y,x] = ksdensity( dS-d_dS);
fill(x,y,'r','facecolor',[.67 .12 0],'edgecolor','none');
hold on
ax =axis;
h1=plot([params.gamma,params.gamma],ax(3:4),'k');
h2=plot([mds,mds],ax(3:4),':k','linewidth',2);
text(params.gamma, mean(ax(3:4)), sprintf(' \\gamma=%3.2f', params.gamma));
axis(ax);
hold off
title( fname);
legend([h1,h2],{'Historical median', 'Local median'});

a4=subplot(3,3,[8 9]);
r = linspace(0, 2, 200);
p = r./(1+r)*100;
dS_h = params.s0 + params.kappa * r.^params.gamma;
plot(p, dS_h, 'r', 'linewidth',2);
ax = axis;
axis([p(1) p(end) ax(3:4)]);
xlabel( sprintf('r; \\kappa=%3.2f', ...
    params.kappa));
ylabel('MI');