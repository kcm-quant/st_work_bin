function data = se_run_parametric_cv(security_id, trading_destination_id, window_type, window_width, as_of_date,dt_remove,varargin_)

% Détarmination de la destination primaire la plus courante.
prim_id = get_primary(reshape(security_id,[],1));
freq = count(prim_id);
[~,I] = sort(freq(:,2),'descend');
trading_destination_id = freq(I(1),1);


job_id = cell2mat(exec_sql('QUANT',['select top 1 job_id from QUANT..association where security_id is null',...
    ' and estimator_id = 2',...
    sprintf(' and trading_destination_id = %d and varargin is null',trading_destination_id),...
    ' order by rank']));
if isempty(job_id)
    error('se_run_parametric_cv:unable_to_find_job','No volume curve job associated to the provided list of stoks')
end
    
run_id = cell2mat(exec_sql('QUANT',[sprintf('select run_id from estimator_runs where job_id = %d',job_id),...
    ' and is_valid = 1 and context_id = 2']));
if isempty(run_id)
    error('se_run_parametric_cv:unable_to_find_run','No volume curve run associated to the provided list of stoks')
end

% Récupération de la partie continue de la courbe
cv = cell2mat(exec_sql('QUANT',['select pv.value',...
    ' from param_value pv, param_desc pd',...
    sprintf(' where pv.run_id = %d and pv.parameter_id = pd.parameter_id',run_id),...
    ' and pd.parameter_name like ''slice%''',...
    ' order by pd.parameter_name']));
curve = cv/sum(cv);

% Récupération des autres paramètres
parameter = exec_sql('QUANT',['select pd.parameter_name,pv.value',...
    ' from param_value pv, param_desc pd',...
    sprintf(' where pv.run_id = %d and pv.parameter_id = pd.parameter_id',run_id),...
    ' and pd.parameter_name not like ''slice%''',...
    ' order by pd.parameter_name']);
param_cv_parameter_list = exec_sql('QUANT','select parameter_name from QUANT..param_desc where estimator_id = 8');

N = length(curve);
fun = @(a,x)a(1)*x.^3+a(2)*x.^2+(1-a(1)-a(2))*x;
nonlincon = @(a)deal(3*a(1)^2+3*a(1)*a(2)+a(2)^2-3*a(1),[]);
[alpha,fval,exitflag] = fmincon(@(a)mean(abs(diff(fun(a,linspace(0,1,N+1)'))-curve)),...
    [1;-1],[-3,-1],0,[],[],[0;-inf],[inf;0],nonlincon);
value = joint([parameter(:,1);{'a';'b'}],[cell2mat(parameter(:,2));alpha],param_cv_parameter_list);

data = struct('rownames',{param_cv_parameter_list},'date',(1:length(param_cv_parameter_list))',...
    'value',{value},'colnames',{{'Usual day'}},'title','CV param');
idx_rm_param = isnan(data.value);
data.value(idx_rm_param) = [];
data.rownames(idx_rm_param) = [];
data.date(idx_rm_param) = [];

data.info.run_quality = 1;