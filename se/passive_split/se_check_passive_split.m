function q = se_check_passive_split(security_id, trading_destination_id, window_type, window_width, as_of_day, data) 
% SE_CHECK_PASSIVE_SPLIT - function that check passive split estimator
%
% use:
%  q = se_check_passive_split(security_id, trading_destination_id, window_type, window_width, as_of_day, data) 
%  window_type, window_width are not used: always day/1
% example:
%   est_params = se_run_passive_split('FTE.PA',{}, 'day', 30, '30/01/2009', [])
%   q = se_check_passive_split( 'FTE.PA', {}, [],[], '02/02/2009', est_params)
% 
% See also check_simple_mi se_run_mi  set_params_process4market_impact_01b 

rownames = data.colnames';
est_params = st_data('transpose', data);
est_params.rownames = rownames;
r     = st_data('col', est_params, '&r');
data  = read_dataset( 'tickdb', 'security', security_id, 'from', as_of_day, 'to',as_of_day, 'where_f-td', '~{auction}');
this_day = keep_intraday_volume( 'intra-day', data, 'out', 'cell') ;

L = length(this_day);
day_td_name = arrayfun(@(c)get_repository( 'td_id2td_name', this_day{c}.info.trading_destination_id), 1:L, 'uni', false);

N = length(est_params.rownames);
check_qual = repmat(0, 1, N);
for n=1:N
    tf = find(ismember( day_td_name, est_params.rownames{n}));
    if isempty( tf)
        check_qual(n) = 0;
    else
        rn = r(n,:)';
        check_qual(n) = 1-chi2cdf(  ...
            sum( (rn-st_data('col', this_day{tf}, 'Market share')).^2./(rn.*(1-rn))), ...
            length(rn));
    end
    
end
q = check_qual;
