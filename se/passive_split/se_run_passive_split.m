function est_params = se_run_passive_split(security_id, trading_destination_id, window_type, window_width, as_of_day, dt_remove, varargin_)
% SE_RUN_PASSIVE_SPLIT - function that interface passive split graph
%
% use:
%  est_params = se_run_passive_split(security_id, trading_destination_id, window_type,  window_width, as_of_day)
% example:
%  est_params = se_run_passive_split('FTE.PA',{}, 'day', 30, '30/01/2009', [])
%
% See also se_check_mi  set_params_explore_passive_heatmap
data = read_dataset( sprintf('gi:explore_passive_heatmap/window_inertia%s%d', char(167), window_width) , ...
    'security_id',security_id, 'from', as_of_day, 'to', as_of_day,  ...
    'remove',dt_remove, ...
    'trading-destinations', trading_destination_id);

if st_data('isempty-nl', data)
    error('se_run_mi:exec', 'Empty set returned by read_dataset used with gi:process4market_impact_01b');
end

data_se = keep_intraday_volume( 'format4se', data);
% prop
% ATS
% ATS quantile

fiab = 1 - data_se{3}.info.quality;

ind_names = {'r', 'Qmin', 'Qmax'};
first_one = 1;
est_params = data_se{first_one};
est_params.rownames = arrayfun(@(c)sprintf('%s - slice %d', ind_names{first_one}, c),1:size(data_se{first_one}.value,1),'uni',false)';
for ind=2:3
    est_params.value = cat(1, est_params.value, data_se{ind}.value);
    est_params.date  = cat(1, est_params.date , data_se{ind}.date );
    est_params.rownames = cat(1, est_params.rownames, ...
        arrayfun(@(c)sprintf('%s - slice %d', ind_names{ind}, c),1:size(data_se{ind}.value,1),'uni',false)' );
end
dts = est_params.date(1:3);
est_params.date  = cat(1, est_params.date, dts);
est_params.value = cat(1, est_params.value, repmat(dts, 1, size(est_params.value,2)));
est_params.rownames = cat(1, est_params.rownames, ...
    arrayfun(@(c)sprintf('begin_time - slice %d',c), 1:size(data_se{ind}.value,1),'uni',false)' );
est_params.info.run_quality = fiab ;

%ATTENTION rajout� temporairement: un contexte supplementaire "main_place"
%qui correspond � une copie des parametres estim�s pour la place principale 
est_params = st_data('add-col', est_params, est_params.value(:,1), 'USUAL');
est_params.info.quality = [est_params.info.quality est_params.info.quality(1)];
est_params.info.run_quality = [est_params.info.run_quality est_params.info.run_quality(1)];


 