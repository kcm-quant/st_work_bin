function rslt = se_check_pre_trade_indicator(security_id, trading_destination_id, window_type, window_width, as_of_day,pre_trade_ind)
% SE_CHECK_PRE_TRADE_INDICATOR - "Check" of the estimator Pre-Trade Indicators
%
% example :
% 
% pre_trade_ind = se_run_pre_trade_indicator(110, 4, [], 7, '25/01/2011');
% pre_trade_ind.info.run_quality
% rslt = se_check_pre_trade_indicator([], [], [], [], '25/01/2011', pre_trade_ind)
%
% 
%
% author   : 'mlasnier-ext@cheuvreux.com'
% reviewer : ''
% version  : '1'
% date     : '25/01/2011'

as_of_day_num = datenum(as_of_day,'dd/mm/yyyy');
rslt = min(exp(-(as_of_day_num-pre_trade_ind.info.as_of_date)/14),1);