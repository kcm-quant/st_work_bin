function data = se_run_pre_trade_indicator(security_id, trading_destination_id, window_type, window_width, as_of_date,dt_remove,varargin_)
% SE_RUN_PRE_TRADE_INDICATOR - "Run" of Pre-Trade Indicators
%
% se_run_pre_trade_indicator(110, 4, [], 7, '25/01/2011')
% 
% author   : 'mlasnier-ext@cheuvreux.com'
% reviewer : ''
% version  : '1'
% date     : '25/01/2011'


param_list = {'moving_average-5','moving_average-10','moving_average-20','moving_average-50','moving_average-100','moving_average-200','commodity_channel_index','bollinger_bands','MACD','mass_index','money_flow','osc_stochastic','RSI','sequential','trix'};
as_of_date_num = datenum(as_of_date,'dd/mm/yyyy');

% temp = regexp(as_of_date,'/','split');
% end_date = sprintf('%s/%s/%s',temp{3},temp{2},temp{1});
end_date = as_of_date;

daily_data = get_trading_daily('base','security_id',security_id,...
    'day_start',datestr(datenum(end_date,'dd/mm/yyyy')-286,'dd/mm/yyyy'),...
    'day',end_date,'trading_destination_id','main','td_include_all_destination',false);
daily_data = st_data('keep',daily_data,'open_prc;high_prc;low_prc;close_prc;volume');
daily_data.colnames = joint({'open_prc','high_prc','low_prc','close_prc','volume'},...
    {'open','high','low','close','volume'},...
    daily_data.colnames);

% fetch today's close
if as_of_date_num > max(daily_data.date)
    % get Primary Destination
    td_prim = cell2mat(exec_sql('BSIRIUS',sprintf(...
        'select trading_destination_id from repository..security_market where security_id = %d and ranking = 1',...
        security_id)));
    %get corresponding deal_day Table
    zone_suff = get_repository('tdid2global_zone_suffix',td_prim);
    deal_day_table = sprintf('deal_day%s',zone_suff);
    % get open
    
    open = exec_sql('BSIRIUS',sprintf(['select price from tick_db..%s',...
        ' where security_id = %d',...
        ' and trading_destination_id = %d',...
        ' and opening_auction = 1',...
        ' and date = ''%s'''],deal_day_table,security_id,td_prim,datestr(as_of_date_num,'yyyy-mm-dd')));
    
    % get close
    close = exec_sql('BSIRIUS',sprintf(['select price from tick_db..%s',...
        ' where security_id = %d',...
        ' and trading_destination_id = %d',...
        ' and closing_auction = 1',...
        ' and date = ''%s'''],deal_day_table,security_id,td_prim,datestr(as_of_date_num,'yyyy-mm-dd')));
    if ~isempty(close) && ~isempty(open)
        % get high,low,volume
        hlvol = exec_sql('BSIRIUS',sprintf(['select max(price),min(price),sum(size) from tick_db..%s',...
            ' where security_id = %d',...
            ' and trading_destination_id = %d',...
            ' and date = ''%s'''],deal_day_table,security_id,td_prim,datestr(as_of_date_num,'yyyy-mm-dd')));
        daily_data.value = [daily_data.value;[open{1},[hlvol{1:2}],close{1},hlvol{3}]];
        daily_data.date = [daily_data.date;as_of_date_num];
    end
end

% Calculation of the indicators
data = struct();
data.rownames = param_list;
data.value = nan(length(param_list),1);
data.colnames = {'Usual day'};
for i=1:length(param_list)
    
    ind_name = regexp(param_list{i},'-','split');
    if strcmp(ind_name{1},'moving_average')
        signal = moving_average(str2double(ind_name{2}),moving_average(str2double(ind_name{2}),daily_data),'signal');
    else
        fun = str2func(ind_name{1});
        signal = fun(fun(daily_data),'signal');
    end
    dates = ((as_of_date_num-window_width+1):as_of_date_num)';
    [~,a,b] = intersect(signal.date,dates);
    [~,~,position] = find(signal.value(a),1,'last');
    if isempty(position)
        position = 0;
    end
    data.value(i) = position;
end
data.date = (1:length(data.rownames))';
data.info.as_of_date = max(daily_data.date);
data.info.run_quality = min(exp(-(as_of_date_num-max(daily_data.date))/14),1);