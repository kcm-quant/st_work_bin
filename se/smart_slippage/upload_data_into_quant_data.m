

% quand ce sera bon, effacer, puis : thsyb043,9550

% conn_params = { 'thsyb092:5010', 'temp_works','batch_tick', 'batick'};
% c = database(conn_params{2:end}, ...
%     'com.sybase.jdbc3.jdbc.SybDriver', ...
%     ['jdbc:sybase:Tds:' conn_params{1} '/']);

% conn_params = { 'thsyb043:9550', 'temp_works','batch_tick', 'batick'};
% c = database(conn_params{2:end}, ...
%     'com.sybase.jdbc3.jdbc.SybDriver', ...
%     ['jdbc:sybase:Tds:' conn_params{1} '/']);

% curs = exec( c, 'select * from quant_data..quant_param where estimator_id = 13')
% d = fetch(curs)

global st_version
quant = st_version.bases.quant;

d = load('G:\DEVELOPMENT\PARIS\TEAM_QUANT_RESEARCH\team\wehua\Projets\SmartSlippage\Results.mat');
Result = d.Result;
nb_action = length(Result.date);
security_vec = st_data('col',Result,'security_id');
td_vec = st_data('col',Result,'td_id');
cont_id = cell2mat(exec_sql('QUANT',['select context_id from ',quant,'..context',...
    ' where estimator_id = 13']));
% para_vec = Result.colnames(3:end);
for i = 1:nb_action
    
    % domain_id
    security_id = security_vec(i);
    td_id = td_vec(i);
    exec_str = sprintf(['select domain_id from ',quant,'..association a,',quant,'..job j',...
        ' where a.estimator_id = 13 and a.security_id = %d',...
        ' and a.trading_destination_id = %d',...
        ' and a.job_id = j.job_id'], security_id, td_id);
    d = exec_sql('QUANT',exec_str);
    %     curs = exec(c,exec_str);
    %     d = fetch(curs);
    domain_id = d{1};
    
    res = exec_sql('QUANT',sprintf(['select * from quant_data..quant_reference ',...
        ' where estimator_id = 13',...
        ' and context_id =  1',...
        ' and domain_id = %d',...
        ' and rank = 1',...
        ' and security_id= %d',...
        ' and trading_destination_id = %d',...
        ' and varargin is null',...
        ' and default_context = 1',...
        ' and run_id = -1'],domain_id,security_id,td_id));
    if ~isempty(res)
        continue
    end
    
    % quant_data..quant_reference
    exec_str = sprintf(['INSERT INTO quant_data..quant_reference ',...
        ' (estimator_id, context_id, domain_id, rank, security_id, ',...
        ' trading_destination_id, varargin, default_context, run_id) ',...
        ' VALUES (13, %d, %d, 1, %d, %d, null, 1, -1)'],cont_id, domain_id,security_id,td_id);
    exec_sql('QUANT',exec_str);
    %     curs = exec(c,exec_str);
    %     d = fetch(curs);
    
    % quant_data..quant_param
    param_name = {'ltb_low','ltb_med','ltb_high','lto_low','lto_med','lto_high','ltb_4_low',...
        'ltb_4_med','ltb_4_high','lto_4_low','lto_4_med','lto_4_high','ltb_3_low',...
        'ltb_3_med','ltb_3_high','lto_3_low','lto_3_med','lto_3_high','ltb_2_low',...
        'ltb_2_med','ltb_2_high','lto_2_low','lto_2_med','lto_2_high'};
    for j = 1:length(param_name)
        param_value = st_data('cols',Result,param_name{j},i);
        if ~isempty(param_value)
            temp = exec_sql('QUANT',['select parameter_id,x_value from ',quant,'..param_desc',...
                ' where estimator_id = 13 and parameter_name = ''',param_name{j},'''']);
            param_id = temp{1};
            x_value = temp{2};
            exec_str = sprintf(['INSERT INTO quant_data..quant_param ',...
                '(estimator_id, context_id, domain_id, x_value, parameter_id, parameter_name, value)',...
                ' VALUES (13,%d,%d,%d,%d,''%s'',%f)'], cont_id,domain_id,x_value,param_id,param_name{j},param_value);
            exec_sql('QUANT',exec_str);
            %             curs = exec(c,exec_str);
            %             d = fetch(curs);
        end
    end
end



% -- verifier le contenu
% select * from quant_reference where estimator_id = 13
%
% -- tout effacer dans les references
% delete from quant_reference where estimator_id = 13

% -- verifier le contenu
% select * from quant_param where estimator_id = 13
%
% -- tout effacer param
% delete from quant_param where estimator_id = 13

