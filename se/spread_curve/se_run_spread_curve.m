function out = se_run_spread_curve(security_id,trading_destination_id,window_type_name,run_window_width,run_date,dt_remove,job_varargin)
% NEW_SPREAD_CURVE  - Spread curve computation for SE
% The function signature is conform to function call in function se_run:
% List of input arguments:
% * security_id: security_id of the stock considered. Could be a list of
% security_id. This feature is disable this function.
% * trading_destination_id: trading_destination_id. Empty
% trading_destination_id corresponds to the consolidated destination. This
% feature is disabled for this function
% * window_type_name: Unit of the run_window_width
% * run_window_width: Size of the period (unit = window_type_name) over
% which the data is collected
% * run_date: end date of the period. The expected date format is dd/mm/yyyy
% * dt_remove: dates to be removed
% * job_varargin: additional arguments if needed.
%
% Contextualisation feature:
% The contextualisation is done with helper function ny_context4spread_curve,
% which allows to split the data set into two parts:
% Usual days and New York shitf days.
% For further details, see the documentation of function ny_context4spread_curve

assert(isscalar(security_id),'se_run_spread_curve:wrong_security_id_format',...
    'security_id must be a scalar in se_run_spread_curve')
% assert(~isempty(trading_destination_id),'se_run_spread_curve:empty_td_id',...
%     'Multi-destination is not available in se_run_spread_curve') % Il a
%     fallu c�der... � cause des US !

%%% HARD CODED VARIABLES
USUAL_DAYS_CONTEXT_MIN_NB_DAYS = 100;
NY_CONTEXT_MIN_NB_DAYS = 50;
MAX_PROPORTION_EMPTY_BINS = 0.3;

% Dates treatment
[from_datenum, to_datenum] = se_window_spec2from_to(window_type_name, run_window_width, run_date);
date_ny = ny_context4spread_curve(security_id,from_datenum,to_datenum);
[usual_compo,ny_compo] = date_connexe_components(from_datenum,to_datenum,date_ny);


try
    % First try with 5-minute bins.
    [usual_day_curve,ny_shift_curve] = build_curve(security_id,trading_destination_id,usual_compo,ny_compo,5/60/24);
    slice_duration = 5/60/24;
catch er
    if strcmp(er.identifier,'computing_curve:too_much_empty_bins')
        % Not enough non-empty bins. Second try with 15 minutes
        [usual_day_curve,ny_shift_curve] = build_curve(security_id,trading_destination_id,usual_compo,ny_compo,15/60/24);
        slice_duration = 15/60/24;
    else
        rethrow(er)
    end
end

%%% Build of the standardised st_data for output
% The following fields are required:
% .info.run_quality, size 1 x N vector, where N = context number
% .rownames, parameter names
% .colnames, context names

nb_slices = length(usual_day_curve.date);
rownames = reshape(tokenize(sprintf('slice%03d;',1:nb_slices)),[],1);
out = st_data('init','colnames',{'Usual day'},'value',usual_day_curve.value,...
    'date',usual_day_curve.date);
out.info = struct('run_quality',usual_day_curve.info.quality);
out.rownames = rownames;

if ~st_data('isempty-no_log',ny_shift_curve)
    out.value(:,end+1) = ny_shift_curve.value;
    out.colnames{end+1} = 'New York shift';
    out.info.run_quality = [out.info.run_quality,ny_shift_curve.info.quality];
end

% Adding three rows begin_of_first_slice, end_of_first_slice and slice_duration
out.value(end+1,:)  = usual_day_curve.info.begin_of_first_slice;
out.rownames{end+1} = 'begin_of_first_slice';
out.date(end+1) = 1;
out.value(end+1,:) = usual_day_curve.info.end_of_first_slice;
out.rownames{end+1} = 'end_of_first_slice';
out.date(end+1) = 2;
out.value(end+1,:) = usual_day_curve.info.end_of_last_slice;
out.rownames{end+1} = 'end_of_last_slice';
out.date(end+1) = 3;
out.value(end+1,:) = slice_duration;
out.rownames{end+1} = 'slice_duration';
out.date(end+1) = 4;

    function [usual_curve,ny_curve] = build_curve(security_id,trading_destination_id,usual_dates,ny_dates,time_frame)
        % BUILD_CURVE - Helper function building the spread curves
        % * usual_dates and ny_dates are resp. usual days connexe components and ny
        % shift connexe components.
        % * time_frame gives the size of the grid for the spread curve computation
        % in Matlab date serial numbers
        
        % Building the option cellarray for get_basic_indicator_v2 called in get_data
        opts = {'source:char','tick4bi','window:time',time_frame,'step:time',time_frame,...
            'bins4stop:b', true,'context_selection:char','null','ind_set:char','std',...
            'grid_mode','time_and_phase','kingpin:time',0.5,'t_acc:time',11/60/24};
        
        % Grabing data for both contexts
        data_usual = get_data(security_id,trading_destination_id,usual_dates(:,1),usual_dates(:,2),...
            opts,USUAL_DAYS_CONTEXT_MIN_NB_DAYS);
        if isempty(ny_dates)
            data_ny = st_data('from-idx',data_usual,[]);
        else
            data_ny = get_data(security_id,trading_destination_id,ny_dates(:,1),ny_dates(:,2),...
                opts,NY_CONTEXT_MIN_NB_DAYS);
        end
        
        % Usual day curve
        assert(~st_data('isempty-no_log',data_usual),'build_curve:no_usual_day_data',...
            'The data set for context usual day is empty')
        nb_usual_days = length(unique(floor(data_usual.date)));
        assert(nb_usual_days>=USUAL_DAYS_CONTEXT_MIN_NB_DAYS,'build_curve:not_enough_usual_dates',...
            'Number of usual dates is lower than %d',USUAL_DAYS_CONTEXT_MIN_NB_DAYS)
        usual_curve = computing_curve(data_usual);
        
        % NY curve
        try
            assert(~st_data('isempty-no_log',data_ny),'build_curve:no_ny_day_data',...
                'The data set for context ny is empty')
            nb_usual_days = length(unique(floor(data_ny.date)));
            assert(nb_usual_days>=NY_CONTEXT_MIN_NB_DAYS,'build_curve:not_enough_ny_dates',...
                'Number of ny context dates is lower than %d',NY_CONTEXT_MIN_NB_DAYS)
            ny_curve = computing_curve(data_ny);
        catch e
            ny_curve = st_data('from-idx',usual_curve,[]);
        end
    end
    

    function curve = computing_curve(data)
        % COMPUTING_CURVE - Helper function computing curve from st_data DATA
        
        % Auctions are removed
        idx_auction = any(st_data('cols',data,'opening_auction;closing_auction;intraday_auction;stop_auction')==1,2);
        data = st_data('from-idx',data,~idx_auction);
        [binned_vwas,~,data,bin_vwas_has_changed] = safer_bin_separator(data,data,'vwas','colnames_format','HH:MM:SS');
        [binned_volume,~,data,bin_volume_has_changed] = safer_bin_separator(data,data,'volume');
        [binned_begin_slice,~,data,bin_vwas_has_changed] = safer_bin_separator(data,data,'begin_slice','colnames_format','HH:MM:SS');
        [binned_end_slice,~,data,bin_vwas_has_changed] = safer_bin_separator(data,data,'end_slice','colnames_format','HH:MM:SS');
        daily_vwas = nansum(binned_vwas.value.*binned_volume.value,2)./nansum(binned_volume.value,2);
        vwas_curve = bsxfun(@rdivide,binned_vwas.value,daily_vwas);
        if mean(isnan(vwas_curve(:))) > MAX_PROPORTION_EMPTY_BINS % An error is risen
            error('computing_curve:too_much_empty_bins',...
                'There are too much empty bins to compute a curve')
        end
        [median_curve,quality] = stats(vwas_curve);
        curve = st_data('init','colnames',{'Curve'},'value',median_curve',...
            'date',datenum(binned_vwas.colnames,'HH:MM:SS'),'title','spread curve');
        curve.info = struct('quality',quality,'begin_of_first_slice',binned_begin_slice.value(end,1),...
            'end_of_first_slice',binned_end_slice.value(end,1),'end_of_last_slice',binned_end_slice.value(end,end));
    end
end

function [remain,remove] = date_connexe_components(beg_date,end_date,rm_date)
% DATE_CONNEXE_COMPONENTS - Helper function to find connexe periods
% clean out dates from rm_date that are not within the period from beg_date
% to end_date
rm_date(rm_date<beg_date|end_date<rm_date) = [];
if isempty(rm_date)
    remain = [beg_date,end_date];
    remove = nan(0,2);
    return
end
remove = [reshape(rm_date(1:end-1),[],1),reshape(rm_date(2:end),[],1)];
remove = [[remove(1);remove(diff(rm_date)>1,2)],[remove(diff(rm_date)>1,1);remove(end)]];
remain = [[beg_date;remove(:,2)+1],[remove(:,1)-1;end_date]];
remain = remain(diff(remain,[],2)>-1,:);
end

function data = get_data(security_id,trading_destination_id,beg_date,end_date,opts,min_nb_day)
% GET_DATA - Helper function to grab data ensuring minimum of days
N = length(beg_date);
data = cell(N,1);
nb_days = zeros(N,1);
for i = N:-1:1
    beg_date_str = datestr(beg_date(i),'dd/mm/yyyy');
    end_date_str = datestr(end_date(i),'dd/mm/yyyy');
    data{i} = from_buffer('get_basic_indicator_v2','basic_indicator','security_id',security_id,...
        'trading-destinations',trading_destination_id,'from',beg_date_str,'to',end_date_str,opts{:},...
        'output-mode', 'all');
    nb_days(i) = length(unique(floor(data{i}.date)));
    if sum(nb_days) >= min_nb_day
        break
    end
end
data(cellfun('isempty',data)) = [];
data = st_data('stack',data{end:-1:1});
end

function [estimate,confidence] = stats(curves)
% STATS - Compute the stats for spread curve estimation
estimate = nanmedian(curves);
% Normalisation of the median curve
estimate = estimate/nanmean(estimate);
estimate(isnan(estimate)) = 0; % Replace NaNs with 0
residuals = bsxfun(@minus,curves,estimate);
confidence = max(0,1-nanvar(residuals(:))/nanvar(curves(:))); % R2
end