function upload_market_impact_data_into_quant_data(mode)
% UNTITLED - Short_one_line_description
%
%
%
% Examples:
% upload_market_impact_data_into_quant_data('all')
%
%
%
% See also:
%
%
%   author   : 'mlasnier-ext@cheuvreux.com'
%   reviewer : ''
%   date     :  '05/06/2012'
%
%   last_checkin_info : $Header: upload_market_impact_data_into_quant_data.m: Revision: 1: Author: malas: Date: 06/07/2012 11:38:08 AM$

global st_version
quant = st_version.bases.quant;

res = exec_sql('QUANT',['select a.estimator_id,a.context_id,j.domain_id',...
    ' from ',quant,'..estimator e,',quant,'..association a,',quant,'..job j',...
    ' where e.estimator_name = ''Universal Market Impact''',...
    ' and e.estimator_id = a.estimator_id',...
    ' and a.job_id = j.job_id']);
estimator_id = res{1};
cont_id = res{2};
domain_id = res{3};

switch mode
    case 'upload'
        param_name = {'alpha','gamma','kappa'};
        param_value = [0.42,0.88,1.17];
        
        exec_str = sprintf(['INSERT INTO quant_data..quant_reference ',...
            ' (estimator_id, context_id, domain_id, rank, security_id, ',...
            ' trading_destination_id, varargin, default_context, run_id) ',...
            ' VALUES (%d,%d,%d,1,null,1,null,1,-1)'],estimator_id,cont_id, domain_id);
        exec_sql('QUANT',exec_str);
        
        for i = 1:length(param_name)
            temp = exec_sql('QUANT',['select parameter_id,x_value from ',quant,'..param_desc',...
                ' where parameter_name = ''',param_name{i},'''']);
            param_id = temp{1};
            x_value = temp{2};
            exec_str = sprintf(['INSERT INTO quant_data..quant_param ',...
                '(estimator_id, context_id, domain_id, x_value, parameter_id, parameter_name, value)',...
                ' VALUES (%d,%d,%d,%d,%d,''%s'',%f)'], estimator_id,cont_id,domain_id,x_value,param_id,param_name{i},param_value(i));
            exec_sql('QUANT',exec_str);
        end
    case 'clear_all'
        exec_sql('QUANT',['delete from quant_data..quant_reference where estimator_id = ',num2str(estimator_id)]);
        exec_sql('QUANT',['delete from quant_data..quant_param where estimator_id = ',num2str(estimator_id)]);
    case 'all'
        upload_market_impact_data_into_quant_data('clear_all')
        upload_market_impact_data_into_quant_data('upload')
end