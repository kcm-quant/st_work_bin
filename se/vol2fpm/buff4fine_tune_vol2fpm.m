function buff4fine_tune_vol2fpm(security_id, trading_destination_id, from, to)
% buff4fine_tune_vol2fpm([], [], '18/11/2008', '12/12/2008')

if isempty(security_id)
    %TODO : requete plus generique, par exemple ne pas se baser sur
    %l'estimator_id...
    domains = get_domain4estimator('Placement en cycle');
    security_id = cell2mat(domains(:, 1));
    trading_destination_id = cell2mat([domains{:, 2}]);
    if length(security_id)~=length(trading_destination_id)
        error('fine_tune_vol2fpm:exec', 'this m-file was designed to work with 1 and only 1 trading_destination per security');
    end
end

num_from = datenum(from, 'dd/mm/yyyy');
end_learn = datestr(num_from-1, 'dd/mm/yyyy');
days4fine_tuning = datenum(to, 'dd/mm/yyyy')-num_from;

jm = findResource('scheduler','type','local');
job = createJob(jm);

for i = 1 : length(security_id)
    createTask(job,'unit_buff4fine_tune_vol2fpm',0,{security_id(i), trading_destination_id(i), days4fine_tuning, to, end_learn, i});
end

submit(job);
