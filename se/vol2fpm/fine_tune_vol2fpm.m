function fine_tune_vol2fpm(security_id, trading_destination_id, from, to)
% FINE_TUNE_VOL2FPM - fonction aidant � trouver un horizon temporel et une
% probabilit� d'�x�cution telles que les distances conseill�es entrent
% rarement en "conflit" avec le placement en cycle (c'est � dire que l'on doit aller se placer
% derriere le placement en cycle)
%
% ex : fine_tune_vol2fpm(26, 4, '18/11/2008', '12/12/2008')
% fine_tune_vol2fpm(352, 5, '18/11/2008', '12/12/2008')
%
% fine_tune_vol2fpm([352 26], [5 4], '18/11/2008', '12/12/2008')
%
% fine_tune_vol2fpm([], [], '18/11/2008', '12/12/2008')

fichier = 'report_fine_tune_vol2fpm';

latex('header', fichier, 'title', 'Rapport pour la calibration de volatility2fpm');

latex('text', fichier, sprintf(['Dans cette version, on consid�re que le placement en cycle va toujours se positionner au meilleur prix., "sans collision" est donc �quivalent � "strictement au dela du spread pour tout ce rapport"'...
    '\nCe rapport est cens� permettre la calibration de ' ...
    'la probabilit� d''�x�cution et de l''horizon temporel n�cessaires � l''utilisation ' ...
    'de l''estimateur volatility to FPM.\n Le but recherch� pour l''instant est de ' ...
    'ne pas empi�ter sur le terrain du placement en cycle. On s''int�ressera donc particuli�rement '...
    ' � ce qui sera d�nomm� "proportion sans collision", � savoir la proportion d''ordres' ...
    'tels que le placement en volatilit� est strictement plus loin que le placement en cycle.\n\n\n' ...
    'Remarques : \nDans les sous-sections d�nomm�es "Histogrammes du placement des ordres par rapport au spread", '...
    'le premier intervalle correspond � la proportion d''ordres march�s+� l''int�rieur du spread+� la meilleure limites. '...
    'les intervalles suivants sont derri�re la meilleur offre, en incr�mentant d''un centime � chaque fois.' ...
    '\nTests hors-�chantillon r�alis�s sur la p�riode du %s au %s'], from, to));

if isempty(security_id)
    %TODO : requete plus generique, par exemple ne pas se baser sur
%l'estimator_id...
    domains = get_domain4estimator('Placement en cycle');
    security_id = cell2mat(domains(:, 1));
    trading_destination_id = cell2mat([domains{:, 2}]);
    if length(security_id)~=length(trading_destination_id)
        error('fine_tune_vol2fpm:exec', 'this m-file was designed to work with 1 and only 1 trading_destination per security');
    end
end

num_from = datenum(from, 'dd/mm/yyyy');
end_learn = datestr(num_from-1, 'dd/mm/yyyy');
days4fine_tuning = datenum(to, 'dd/mm/yyyy')-num_from;
prop_sans_collision = [];
prop_au_dela_du_spread = [];
unused_securities = [];
ric = temporary_bufferize('get_ric_from_sec_id', {security_id});
names = temporary_bufferize('get_security_name', {security_id});
ts = [5 10 20 40];
q = [0.9 0.8 0.7 0.6 0.5];
col_header = cat(2, {''}, tokenize(sprintf('%g;', q)));
lines_header = tokenize(sprintf('%dmins;', ts));
latex('section', fichier, 'Rapport d�taill� titre par titre');
for i = 1 : length(security_id)
    try
        st_log('Security : <%s> \t <%d/%d>\n', ric{i}, i, length(security_id));
%         d_c = temporary_bufferize('se_run_cycle_I', {security_id(i), trading_destination_id(i), 'day', 100, end_learn, [], []}, 'buff_errors');
        d_v = se_run_vol2fpm(security_id(i), trading_destination_id(i), 'day', 30, end_learn, []);
        
%         if d_v.info.run_quality < 0.0000002
%             unused_securities(end+1) = i;
%             continue
%         end
        
        [chec_quality, info] = temporary_bufferize('se_check_vol2fpm', {security_id(i), trading_destination_id(i), 'day', days4fine_tuning, to, d_v}, 'buff_errors');
    catch
        unused_securities(end+1) = i;
        continue;
    end
%     idx_distance = strmatch('distance', d_c.rownames, 'exact');
%     distance = d_c.value(idx_distance, 1);
    distance = 0;
    idx_in_test = find(info.calibration.dist2test==distance);
    prop_sans_collision(:, :, end+1) = sum(info.calibration.spread_plus_dist(:, :, idx_in_test:end), 3);
    prop_au_dela_du_spread(:, :, end+1) = sum(info.calibration.spread_plus_dist(:, :, 2:end), 3);
    
    latex('subsection', fichier, [ric{i} ' = ' names{i}]);
    latex('text', fichier, sprintf('distance fournie par le placement en cycle : <%g>\n', distance));
    latex('subsubsection', fichier, 'Proportions sans collision');
    latex('array', fichier, prop_sans_collision(:, :, end), 'col_header', col_header, 'linesHeader', lines_header);
    latex('subsubsection', fichier, 'Histogrammes du placement des ordres par rapport au spread');
    for i_ts = 1 : length(ts)
        % %histogrammes superpos�s
%         latex('text', fichier, sprintf('Horizon : %d minutes', ts(i_ts)));
%         figure('visible', 'off');
%         plot([info.calibration.dist2test(2:end-1) NaN]', reshape(info.calibration.spread_plus_dist(i_ts, :, :), [size(info.calibration.spread_plus_dist, 2) size(info.calibration.spread_plus_dist, 3)]));
%         latex('graphic', fichier);
        for i_q = 1 : length(q)
            latex('text', fichier, sprintf('Horizon : %d minutes, et proba = %g', ts(i_ts), q(i_q)));
            figure('visible', 'off');
            bar([info.calibration.dist2test(2:end-1) NaN]', reshape(info.calibration.spread_plus_dist(i_ts, i_q, :), [size(info.calibration.spread_plus_dist, 3) 1]));
            latex('graphic', fichier, ric{i});
        end
    end
end
if length(security_id) - length(unused_securities) + 1 == size(prop_sans_collision, 3)
    prop_sans_collision(:, :, 1) = []; % effet de bord de la troisi�me dimension, une matrice de z�ros est rajout�e en 1er element
    prop_au_dela_du_spread(:, :, 1) = []; % effet de bord de la troisi�me dimension, une matrice de z�ros est rajout�e en 1er element
end
% latex('section', fichier, 'Histogrammes des proportions sans collision');
% latex('subsection', fichier, 'histogrammes pour chaque couple horizon*proba');
% for i_ts = 1 : length(ts)
%     for i_q = 1 : length(q)
%         latex('subsubsection', fichier, sprintf('Horizon : %d minutes, et proba = %g', ts(i_ts), q(i_q)));
%         figure('visible', 'off');
%         hist(reshape(prop_sans_collision(i_ts, i_q, :), [size(prop_sans_collision, 3) 1]));
%         latex('graphic', fichier, ric{i});
%     end
% end
% latex('subsection', fichier, 'histogrammes superpos�s pour chaque horizon temporels');
% bins = [0.1*(0:9), 1+eps(1)];
% for i_ts = 1 : length(ts)
%     latex('subsubsection', fichier, sprintf('Horizon : %d minutes', ts(i_ts)));
%     N = [];
%     for i_q = 1 : length(q)
%         N(:, i_q) = histc(reshape(prop_sans_collision(i_ts, i_q, :), [size(prop_sans_collision, 3) 1]), bins);
%     end
%     N(end,:) = [];
%     figure('visible', 'off');
%     plot((bins(1:end-1)+bins(2:end))/2, N);
%     latex('graphic', fichier);
% end
latex('section', fichier, 'Histogrammes des proportions strictement au dela du spread');
latex('subsection', fichier, 'histogrammes pour chaque couple horizon*proba');
for i_ts = 1 : length(ts)
    for i_q = 1 : length(q)
        latex('subsubsection', fichier, sprintf('Horizon : %d minutes, et proba = %g', ts(i_ts), q(i_q)));
        figure('visible', 'off');
        hist(reshape(prop_au_dela_du_spread(i_ts, i_q, :), [size(prop_au_dela_du_spread, 3) 1]));
        latex('graphic', fichier, ric{i});
    end
end
latex('subsection', fichier, 'histogrammes superpos�s pour chaque horizon temporels');
bins = [0.1*(0:9), 1+eps(1)];
for i_ts = 1 : length(ts)
    latex('subsubsection', fichier, sprintf('Horizon : %d minutes', ts(i_ts)));
    N = [];
    for i_q = 1 : length(q)
        N(:, i_q) = histc(reshape(prop_au_dela_du_spread(i_ts, i_q, :), [size(prop_au_dela_du_spread, 3) 1]), bins);
    end
    N(end,:) = [];
    figure('visible', 'off');
    plot((bins(1:end-1)+bins(2:end))/2, N./repmat(sum(N), size(N, 1), 1));
    latex('graphic', fichier);
end
latex('end', fichier);
latex('compile', fichier);
 