function [q, info] = se_check_vol2fpm(security_id, trading_destination_id, window_type, window_width, as_of_day, data)
% SE_CHECK_VOL2FPM - function that check the vol2fpm model
%
% d = se_run_vol2fpm(280, 4, 'day', 14, '11/11/2008', [])
% d.info.run_quality
% [check_quality, info]=se_check_vol2fpm(280, 4, 'day', 14, '11/11/2008', d)
% info.calibration.spread_plus_dist
% info.calibration.rel2spread
%

if length(security_id) ~= 1
    error('se_check_vol2fpm:check_args', 'Currently, only one security_id can be handled');
end
if length(trading_destination_id) ~= 1
    error('se_check_vol2fpm:check_args', 'Currently, only one trading_destination_id can be handled');
end

% I hope these parameters will be the same as the ones used for the run,
% otherwise....
default_opt = {'quant', [0.9; 0.8; 0.7; 0.6; 0.5], ...
    'time_scales', datenum(0,0,0,0,[5,10,20,40],0), ...
    'step:time', datenum(0,0,0,0,0,30), ...
    'vol_width:time', datenum(0,0,0,0,15,0), ...
    'q_step', 0.02, ...
    'smooth_volatility', 0, ...
    'window:char', sprintf('%s|%d', window_type, window_width),...
    'security_id', security_id, ...
    'from', as_of_day, ...
    'to', as_of_day, ...
    'trading_destination_id', trading_destination_id...
    };

% this loads gr the st_grapher object for volatility2fpm
load('volatility2fpm.mat');
gr.init();

% this sets the parameters defined above
set_params_volatility2fpm(gr, options(default_opt));

% sort the parameters by name, then give it to the node which knows what to
% do
[tmp, idx] = sort(data.rownames);
gr.set_param('st volatility2fpm', 'params2test', {data.value(idx)});

% calcul du check quality
gr.next();

% récupération
rslt = cell2mat(gr.get_target_outputs());

if isempty(rslt) || isempty(rslt.value)
    for i = 1 : length(rslt.info.data_log)
        target_log_mess = rslt.info.data_log(i).message;
        nb_days_removed = sscanf(target_log_mess, '<%d> days have been removed from this data has they had a stop auction');
        if isnumeric(nb_days_removed) && nb_days_removed > 0
            error('se_check_vol2fpm:check_emptiness', 'NO_DATA_FILTER:Too much stop_auctions');
        end
    end
    error('se_check_vol2fpm:check_quality', 'NO_DATA_FILTER:No data');
end
% mise en forme des ouputs
q = rslt.value;
info = rslt.info;