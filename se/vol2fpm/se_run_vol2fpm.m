function data = se_run_vol2fpm(security_id, trading_destination_id, window_type, window_width, as_of_date,dt_remove,varargin_)
% d = se_run_vol2fpm(110, 4, 'day', 14, '10/11/2008', [])
% d.info.run_quality
% author   : 'rburgot@cheuvreux.com'
% reviewer : ''
% version  : '1'
% date     : '21/08/2008'

% TODO ex-ante quality

if length(security_id) ~= 1
    error('se_run_vol2fpm:check_args', 'Currently, only one security_id can be handled');
end
if length(trading_destination_id) ~= 1
    error('se_run_vol2fpm:check_args', 'Currently, only one trading_destination_id can be handled');
end

default_opt = {'quant', [0.9; 0.8; 0.7; 0.6; 0.5], ...
    'time_scales', datenum(0,0,0,0,[5,10,20,40],0), ...
    'step:time', datenum(0,0,0,0,0,30), ...
    'vol_width:time', datenum(0,0,0,0,15,0), ...
    'q_step', 0.02, ...
    'smooth_volatility', 0, ...
    'window:char', sprintf('%s|%d', window_type, window_width),...
    };

if nargin>6 && ~isempty(varargin_)
    opt = str2opt(varargin_);
    opt = options(default_opt, opt.get());
else
    opt = options(default_opt);
    if nargin < 6
        dt_remove = [];
    end
end

data = read_dataset(['gi:volatility2fpm/' opt2str(opt)], ...
    'security_id', security_id,...
    'from', as_of_date,...
    'to', as_of_date, ...
    'remove',dt_remove,...
    'trading-destinations', trading_destination_id);%'from_buffer:cmd', 'show_mode'

if isempty(data) || isempty(data.value)
    for i = 1 : length(data.info.data_log)
        target_log_mess = data.info.data_log(i).message;
        nb_days_removed = sscanf(target_log_mess, '<%d> days have been removed from this data has they had a stop auction');
        if ~isempty(nb_days_removed) && isnumeric(nb_days_removed) && nb_days_removed > 0
            error('se_run_vol2fpm:check_emptiness', 'NODATA_FILTER:Too much stop_auctions');
        end
    end
    error('se_run_vol2fpm:check_emptiness', 'NODATA_FILTER:No data');
end

data.value = data.value';
data.date = NaN(length(data.value), 1);
data.rownames = data.colnames';
data.colnames = {'context1'};