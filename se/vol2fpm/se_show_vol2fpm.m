function se_show_vol2fpm(mode,varargin)

   
  opt = options({'mode','run'},{'mode',mode});
  
  switch lower(opt.get('mode'))
      case 'run'
        
        opt_run = options({'security_id',110,'trading-destinations',4,'window_width',1,'window_type',1,'as_of_date','','varargin',''},varargin);   
        security_id = opt.get('security_id');
        trading_destination_id = opt.get('trading-destinations');
        window_width = opt.get('window_width');
        window_type = opt.get('window_type');
        as_of_date = opt.get('as_of_date');
        varargin_ = opt.get('varargin');
        endd   = datenum(as_of_date,'dd/mm/yyyy');
        begind = endd - window_width;
        from_date = datestr(begind,'dd/mm/yyyy');
        to_date   = datestr(endd,'dd/mm/yyyy');
        
        opt4graph = options({'from','','to','','security_id','','trading-destinations','',},{'from',from_date,'to',to_date,'security_id',security_id,...
                            'trading-destinations',trading_destination_id});
                              
        gr = st_grapher('load', 'volatility2fpm.mat');
        gr.run_enrichment('set params', opt4graph); 
        win_graph('show',gr);
          
      case 'check'   
    
          opt_check = options({'security_id',110,'trading-destinations',4,'window_width',1,'window_type',1,'as_od_date','','data',st_data( 'empty-init')},varargin);     
                                      
          security_id  = opt_check.get('security_id');
          trading_destination_id = opt.get('trading-destinations');
          window_width = opt_check.get('window_width'); 
          window_type = opt_check.get('window_type'); 
          as_of_date =  opt_check.get('as_od_date');           
                
          gr = st_grapher('load','process4market_impact_01b');
          opt4gi = options({'security_id',110,'trading-destinations',4,'from','','to',''},...
                     {'security_id',security_id,'trading-destinations',trading_destination_id,'from',from_date,'to',to_date});
                 
          gr.run_enrichement('set params',opt4gi);
          win_graph('show',gr); 
        
        
      otherwise
          error('Show_Vol2fpm:Show_Vol2fpm','mode <%s> unknown',opt.get('mode'));
  end
end