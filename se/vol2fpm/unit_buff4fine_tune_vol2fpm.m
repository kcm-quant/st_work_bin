function unit_buff4fine_tune_vol2fpm(security_id, trading_destination_id, days4fine_tuning, to, end_learn, i)
% fid=fopen(['C:\st_repository\temp\' num2str(i) '_b.txt'], 'a+');
% fclose(fid);
d_c = temporary_bufferize('se_run_cycle_I', {security_id, trading_destination_id, 'day', 100, end_learn, [], []}, 'recompute_errors');
d_v = se_run_vol2fpm(security_id, trading_destination_id, 'day', 14, end_learn, []);

if d_v.info.run_quality < 0.02
    return;
end

[chec_quality, info] = temporary_bufferize('se_check_vol2fpm', {security_id, trading_destination_id, 'day', days4fine_tuning, to, d_v}, 'buff_errors');
% fid=fopen(['C:\st_repository\temp\' num2str(i) '_e.txt'], 'a+');
% fclose(fid);
