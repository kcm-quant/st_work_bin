function AllAssociations = get_association_volatility_curve(sec_id, trading_destination_id, varargin_association, varargin)
% SE_GET_ASSOCIATION_VOLATILITY_CURVE:  fonction qui renvoie l'ensemble des jobs
%associ�s pour le triplet (security_id x xtrading_destination_id x varargin) ,
%pour une plage temporelle si celle ci est pr�cis�e. 
%
%%exemples:
%get_association_volatility_curve(2, 4,'')
%get_association_volatility_curve(2, 4,'', 'as_of_date', '13/10/2010')
%
%
% use in get_se

% author:  'Dana Croiz�'
% version: '1.0'
% date:    '15/09/2010'


global st_version
sv = st_version.bases.quant;

opt = options({ 'as_of_date',...
    datestr(today,'dd/mm/yyyy'), ...
    'from_meta' , 1 }, varargin);

as_of_date = datestr(datenum(opt.get('as_of_date'),'dd/mm/yyyy'),'yyyymmdd');
td_is_null = isempty(trading_destination_id);
st_res1 = [];
st_res2 = [];

%from_meta_subquery
from_meta_subquery = sprintf(' (ass.from_meta = %d OR ass.from_meta = -1 )AND ' , opt.get('from_meta'));


% VOLATILITY CURVE is only estimated for stocks
if isempty(sec_id )
    if td_is_null
        st_log(' Please contact a SE admnistrator. Incorrect parameters (sec_id  and trading destination are NULL)\n');
        AllAssociations = { st_res1 , st_res2};
        return;
    else
        st_log('VOLATILITY CURVE is only estimated for stocks, please enter security_id. \n');
        AllAssociations = { st_res1 , st_res2};
        return;
    end
end


trdt_info=get_repository('tdinfo',sec_id);
td_id_main=trdt_info(1).trading_destination_id;

if td_is_null
    
    % FIRST level :We want All trading destinations for a specific sec_id
    SQL_query_First_Level_Association = sprintf([ ...
        'SELECT hist_ass.job_id, hist_ass.rank,  hist_ass.from_meta , hist_ass.varargin, hist_ass.context_id ' ...
        'FROM %s..association ass, %s..estimator est , %s.. histo_association hist_ass '...
        'WHERE ass.security_id=%d AND '...
        'ass.trading_destination_id= %d  AND ' ...
        'upper(est.estimator_name) =''VOLATILITY CURVE'' AND ' ...
        'ass.estimator_id=est.estimator_id AND '...
        'isnull(ass.security_id , -1) = isnull(hist_ass.security_id ,-1)  AND  ' ...
        'isnull(ass.trading_destination_id, -1) = isnull(hist_ass.trading_destination_id,-1)  AND  ' ...
        'isnull(ass.varargin,'''') = isnull(hist_ass.varargin,'''')  AND  ' ...
        '%s ' ...
        'ass.from_meta = hist_ass.from_meta  AND  ' ...
        'ass.context_id = hist_ass.context_id  AND  ' ...
        'hist_ass.beg_date <= ''%s'' AND ' ...
        '(hist_ass.end_date > ''%s'' OR hist_ass.end_date is null ) AND ' ...
        'hist_ass.rank = ass.rank' ],sv , sv, sv ,sec_id ,td_id_main , from_meta_subquery ,as_of_date, as_of_date );
    
    %SECOND level :
    SQL_query_Second_Level_Association = sprintf([ ...
        'SELECT hist_ass.job_id, hist_ass.rank,  hist_ass.from_meta , hist_ass.varargin, hist_ass.context_id '...
        'FROM %s..association ass, %s..estimator est,  %s.. histo_association hist_ass '...
        'WHERE ass.security_id=%d AND '...
        'ass.trading_destination_id=%d AND ' ...
        'upper(est.estimator_name) =''VOLATILITY CURVE'' AND ' ...
        'ass.estimator_id=est.estimator_id AND ' ...
        'isnull(ass.security_id , -1) = isnull(hist_ass.security_id ,-1)  AND  ' ...
        'isnull(ass.trading_destination_id, -1) = isnull(hist_ass.trading_destination_id,-1)  AND  ' ...
        'isnull(ass.varargin,'''') = isnull(hist_ass.varargin,'''')  AND  ' ...
        '%s ' ...
        'ass.from_meta = hist_ass.from_meta  AND  ' ...
        'ass.context_id = hist_ass.context_id  AND  ' ...
        'hist_ass.beg_date <= ''%s'' AND ' ...
        '(hist_ass.end_date > ''%s'' OR hist_ass.end_date is null ) AND ' ...
        'hist_ass.rank = ass.rank'],sv, sv , sv , sec_id, td_id_main, from_meta_subquery, as_of_date, as_of_date);
    
else  %if td isn't null
    
    % specifc trading destination
    SQL_query_First_Level_Association = sprintf([ ...
        'SELECT hist_ass.job_id, hist_ass.rank,  hist_ass.from_meta , hist_ass.varargin, hist_ass.context_id '...
        'FROM %s..association ass, %s..estimator est ,%s.. histo_association hist_ass '...
        'WHERE ass.security_id=%d AND '...
        'ass.trading_destination_id=%d AND ' ...
        'upper(est.estimator_name) =''VOLATILITY CURVE'' AND ' ...
        'ass.estimator_id=est.estimator_id AND ' ...
        'isnull(ass.security_id , -1) = isnull(hist_ass.security_id ,-1)  AND  ' ...
        'isnull(ass.trading_destination_id, -1) = isnull(hist_ass.trading_destination_id,-1)  AND  ' ...
        'isnull(ass.varargin,'''') = isnull(hist_ass.varargin,'''')  AND  ' ...
        '%s ' ...
        'ass.from_meta = hist_ass.from_meta  AND  ' ...
        'ass.context_id = hist_ass.context_id  AND  ' ...
        'hist_ass.beg_date <= ''%s'' AND ' ...
        '(hist_ass.end_date > ''%s'' OR hist_ass.end_date is null ) AND ' ...
        'hist_ass.rank = ass.rank'],sv, sv , sv , sec_id,trading_destination_id, from_meta_subquery, as_of_date, as_of_date);
    
    
    % Second level : not exist
    SQL_query_Second_Level_Association =  '';
end

%% run the queries

FirstAssociation=  exec_sql('QUANT',SQL_query_First_Level_Association);
if ~isempty(SQL_query_Second_Level_Association)
    SecondAssociation =  exec_sql('QUANT',SQL_query_Second_Level_Association);
else
    SecondAssociation = '';
end

if ~isempty(FirstAssociation)
    st_res1 = CreateOutputStruct(FirstAssociation,'first');
end

if ~isempty(SecondAssociation)
    st_res2 = CreateOutputStruct(SecondAssociation,'backup');
end

AllAssociations = { st_res1 , st_res2};

end


function st_res = CreateOutputStruct(Association,level_of_depth)
[vals, cdk] = codebook('build', {'first','backup','default' });
cdk.colname = 'depth_of_association' ;
index = find(strcmpi(level_of_depth, codebook('get', cdk, 'depth_of_association')));
colnames = {'job_id','rank',  'from_meta' ,'varargin_association' ,'context_id', 'depth_of_association'};

%Cas o� les requ�tes renvoie plus d'une association pour un seul type,
%alors on filtre par from_meta et rank.
st_res_filter = st_data('from-cell', 'All Associations', colnames(1:end-1), Association);
if size(st_res_filter.value ,1)>1
    min_from_meta  = min(st_data('cols', st_res_filter, 'from_meta'));
    idx_min_from_meta = find(st_data('cols' ,st_res_filter, 'from_meta') == min_from_meta);
    
    if length(idx_min_from_meta)>1  %dans le cas ou l'on a deux associations avec la mm valeur dans from_meta on filtre par le rank
        min_rank = min(st_data('cols', st_res_filter, 'rank'));
        idx_min_rank = find(st_data('cols' ,st_res_filter, 'rank') == min_rank);
        
        if length(idx_min_rank ==1)
            Association = Association(idx_min_rank, :);
        end
    else
       Association = Association(idx_min_from_meta, :);
    
    end
end

st_res = st_data('from-cell', sprintf('Association %d', level_of_depth), colnames, [Association, index]);
st_res.codebook(end+1) = cdk;

st_res.title = sprintf('%s association' , level_of_depth );

end







