function ny_date = ny_context4volatility_curve(security_id,beg_date,end_date)

assert(beg_date<=end_date,'ny_context4volatility_curve:inconsistent_period',...
    'beg_date should be before end_date')
assert(isscalar(security_id),'ny_context4volatility_curve:wrong_security_id_size',...
    'security_id must be a scalar')
td_info = get_repository('tdinfo',security_id);
assert(~isempty(td_info),'ny_context4volatility_curve:empty_referential',...
    'No referential data for this stock')

prim_id = td_info(1).trading_destination_id;

% Looking for context NY shift in the calendar
cal_date = exec_sql('KGR',sprintf(['select c.EVENTDATE from KGR..CALEVENT c,KGR..CALEVENTTYPE t',...
    ' where c.SCOPETYPE = 2 and c.SCOPEID = %d',...
    ' and t.EVENTTYPE = c.EVENTTYPE and t.SHORTNAME = ''new_york_shift''',...
    ' and c.EVENTDATE between ''%s'' and ''%s'''],...
    prim_id,datestr(beg_date,'yyyymmdd'),datestr(end_date,'yyyymmdd')));

if isempty(cal_date)
    ny_date = [];
else
    ny_date = datenum(cal_date,'yyyy-mm-dd');
end