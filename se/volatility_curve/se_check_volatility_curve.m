function out = se_check_volatility_curve(security_id, trading_destination_id, window_type, window_width, as_of_day, data_c, varargin_)

% SE_CHECK_VOLATILITE_CURVE Check  de la courbe de volatilité
% example
%  data = se_run_volatility_curve(110, { 'MAIN' }, 'day', 30,'01/04/2010', [], [])
%  res = se_check_volatility_curve(110, { 'MAIN' }, 'day', 30, datestr( today-14, 'dd/mm/yyyy'),data, [])

% author   : 'dcroize@cheuvreux.com'
% reviewer :
% version  : '1'
% date     : '29/01/2009'
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bin_duration = datenum(0,0,0,0,5,0); % constante universelle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
opt = options({'step:time', bin_duration, 'window:time', datenum(0,0,0,0,5,0)});

data = read_dataset(['gi:basic_indicator/' opt2str(opt)], ...
    'security_id', security_id,...
    'from', datestr(datenum(as_of_day,'dd/mm/yyyy')-window_width,'dd/mm/yyyy'),...%TODO : this must not be the right way to do it (cf. window_type)
    'to', as_of_day, ...
    'trading-destinations',trading_destination_id, ...
    'where_formula:char', '~{auction}');

if st_data('isempty-nl', data)
    error('NODATA_ROOT: No data for this (checked) day.')
end

data = st_data('where', data, '~{auction}');
data = st_data ( 'iday2array', data, 'vol_GK' );
data.value ( data.value <= 0 ) = NaN;
data_c = vcse2vcdata(data_c, bin_duration);

%Check
curve = st_data( 'cols', data_c, 'USUAL DAY');
curve ( curve <= 0 ) = NaN;
log_curve = log ( curve );
%alpha = (data_c.info.ci.alpha);
alphas = data_c.info.var_vec;

nb_j = size ( log_vol_day, 2 );
Qj = ( log ( data.value ) - repmat ( log_curve, 1, nb_j ) ) .^2 ./ repmat ( alphas, 1, nb_j );%TODO : make sure this is not a problem... if nb_slices for run is different, this will be a pb.

out = prod ( 1 - chi2cdf ( nansum ( Qj, 1 ), sum ( ~isnan ( Qj ), 1 ) ) ) ^ ( 1 / nb_j );



end