function vlt = se_create_parametric_volatility_curve(varargin)
% se_create_parametric_volume_curve
% example : output in local time
% vc_parametric=se_create_parametric_volatility_curve('security_id',110,'date',
% '10/04/2010');


opt  = options( {'security_id',110,...
    'date','01/04/2010',...
    'get_se_output', 0 ,...
    'BIN_DURATION_REF',datenum(0,0,0,0,5,0),...
    'FUNC_FIDESSA',@(x)(-5.40*x.^3+11.19*x.^2-6.77*x+1.87),...
    'NB_DAY_MIN',20,...
    'NB_DAY_HISTO',60, ...
    'is_gmt:bool',false}, varargin);

sec_id=opt.get('security_id');
date=opt.get('date');
BIN_DURATION_REF=opt.get('BIN_DURATION_REF');
FUNC_FIDESSA=opt.get('FUNC_FIDESSA');
NB_DAY_MIN=opt.get('NB_DAY_MIN');
NB_DAY_HISTO=opt.get('NB_DAY_HISTO');
is_gmt=opt.get('is_gmt:bool');
vlt = [];


trdt_info=get_repository('tdinfo',sec_id);
td_id_main=trdt_info(1).trading_destination_id;
stats=compute_trading_daily_stats(sec_id,date,td_id_main,NB_DAY_HISTO,NB_DAY_MIN);

if ~isempty(stats)
    volatility_m=stats(2);
    
    vlt=st_data('empty-init');
    
    % - on cherche la grille de temps qui correspondrait � une grille d'un basic
    % - indicator pour ce jour la
    try
        grid_time = get_bi_time_grid('security_id',sec_id,'date',date,'is_gmt:bool',is_gmt,...
            'step_time',BIN_DURATION_REF,'window_time',BIN_DURATION_REF);
        
        if ~isempty(grid_time)
            
            fixing_booleans=grid_time.value;
            slice_time=grid_time.date-floor(grid_time.date);
            slice_time_renorm=(slice_time-slice_time(1))./(slice_time(end)-slice_time(1));
            vlt_value_fidessa=repmat(0,size(slice_time_renorm,1),1);
            vlt_value_fidessa(find(fixing_booleans(:,1)))=nan;
            vlt_value_fidessa(find(fixing_booleans(:,3)))=nan;
            idx_na=~fixing_booleans(:,1) & ~fixing_booleans(:,3);
            fidessa_na= FUNC_FIDESSA(slice_time_renorm(idx_na));
            fidessa_na = fidessa_na.* volatility_m ;
            vlt_value_fidessa = fidessa_na;
            
            vlt.title = 'DEFAULT parameters';
            %             vlt.value=cat(2,vlt_value_fidessa, repmat(0,size(vlt_value_fidessa,1),3));
            
            vlt.value=vlt_value_fidessa;
            vlt.colnames={'Usual day'};
            vlt.date = slice_time(idx_na);
            
            %slice
            slice_num = 1;
            vlt.rownames = cell(length(vlt.date), 1);
            
            for i =1 : length(fidessa_na)
                vlt.rownames{i}  =  MAKE_PARAM_NAME(slice_num);
                slice_num = slice_num + 1;
            end
            
            %end_of_first_slice
            vlt.value =  [vlt.value ; vlt.date(2)];
            vlt.rownames(end+1) = {'end_of_first_slice'};
            
            vlt.date = (1:length(vlt.value))';
        end
        
    catch e
        vlt = [];
        st_log ('<se_create_parametric_volatility_curve> : parametric curve couldn''t be generated ')
        
    end
    
end
end

function stats_out=compute_trading_daily_stats(sec_id,date,td_id_main,NB_DAY_HISTO,NB_DAY_MIN)

stats_out=[];
try
    func_vol_GK=@(x,o,h,l,c)(sqrt(((x(:,h)-x(:,l)).^2/2-(2*log(2)-1)*(x(:,c)-x(:,o)).^2 )./ ( (0.5*(x(:,o)+x(:,c))).^2 * 51 )  ) * 10000);
    [from,to] = se_window_spec2from_to('day',190,date);
    
    res_trading_daily=exec_sql('BSIRIUS',sprintf(['(select '...
        'date,volume,turnover,nb_deal,open_prc,high_prc,low_prc,close_prc,average_spread_numer,average_spread_denom'...
        ' from tick_db..trading_daily'...
        ' where security_id = %d and date>=''%s'' and  date<=''%s'' and trading_destination_id=%d)',...
        ' UNION (select '...
        'date,volume,turnover,nb_deal,open_prc,high_prc,low_prc,close_prc,average_spread_numer,average_spread_denom'...
        ' from tick_db..trading_daily_ameri'...
        ' where security_id = %d and date>=''%s'' and  date<=''%s'' and trading_destination_id=%d)',...
        ' UNION (select '...
        'date,volume,turnover,nb_deal,open_prc,high_prc,low_prc,close_prc,average_spread_numer,average_spread_denom'...
        ' from tick_db..trading_daily_asia'...
        ' where security_id = %d and date>=''%s'' and  date<=''%s'' and trading_destination_id=%d)'],...
        sec_id,datestr(from,'yyyymmdd'),datestr(to,'yyyymmdd'),td_id_main,...
        sec_id,datestr(from,'yyyymmdd'),datestr(to,'yyyymmdd'),td_id_main,...
        sec_id,datestr(from,'yyyymmdd'),datestr(to,'yyyymmdd'),td_id_main));
    
    if size(res_trading_daily,1)>NB_DAY_HISTO
        res_trading_daily=res_trading_daily(end-NB_DAY_HISTO-1:end,:);
    end
    
    value_trading_daily=cell2mat(res_trading_daily(:,2:end));
    if size(value_trading_daily,1)<NB_DAY_MIN
        return
    end
    %%% compute vals
    vol_gk=func_vol_GK(value_trading_daily,4,5,6,7);
    spread_trading_daily=value_trading_daily(:,8)./value_trading_daily(:,9);
    
    stats_out=cat(2,nanmean(spread_trading_daily),sqrt(nanmean(vol_gk.^2)));
catch
end

end

function str = MAKE_PARAM_NAME(i)
str = num2str(i);
str = ['slice' repmat('0', 1, 3-length(str)) str];
end