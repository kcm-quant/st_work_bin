function out = se_get_association_volatility_curve (security_id,trading_destination_id,as_of_date,context)
% SE_GET_ASSOCIATION_VOLATILITY_CURVE

global st_version;
bq = st_version.bases.quant;

ESTIMATOR_NAME  ='Volatility curve' ;
BIN_DURATION_REF=datenum(0,0,0,0,5,0);
CURVE_NATURE_CELL=cat(1,arrayfun(@(a)(a),[ -1 0 1 2 3 4 5],'uni',false),...
    {'beug','algo_fidessa','algo_external_so_fidessa',...
    'algo_cb_valeur','algo_cb_indice','validated_fidessa','validated'})';

trdt_info=get_repository('tdinfo',security_id);
td_id_main=trdt_info(1).trading_destination_id;

run_ids_info_1=st_data('empty-init');

% Selection de l'association
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%

assoc_1 = exec_sql('QUANT', sprintf([
    'SELECT job_id,varargin from %s..association ass, %s..estimator est '...
    'WHERE ass.security_id=%d AND '...
    'ass.trading_destination_id=%d AND ' ...
    'upper(est.estimator_name) =''%s'' AND ' ...
    'ass.estimator_id=est.estimator_id ' ],bq, bq ,security_id, td_id_main, upper(ESTIMATOR_NAME)));

if ~isempty(assoc_1)
    run_ids_info_1 = se_get_run_frm_job('run' , cell2mat(assoc_1(:, 1)), 'is_valid', 1);
end

if st_data('isempty-nl',run_ids_info_1) 
    vlt = se_create_parametric_volatility_curve('security_id',security_id,...
        'date',as_of_date);
    vlt.info.curve_nature=4;
    
else
    %%%%%%%%%%% ATTENTION  %%%%%%%%%%%%%%%%%
    job_ids=assoc_1{:, 1};
    job_ids=unique(job_ids);
    %%%%%%%%%%%%  !!!!!!!!!!!!!!!!!!!  %%%%%%%%%%%%%%%%%
    run_ids_info4this_job = st_data('where', run_ids_info_1, sprintf('{job_id}==%d',job_ids));
       
    params = se_get_param(st_data('cols', run_ids_info4this_job, 'run_id'));
    if isempty(params)
        error('se_get_association_volatility_curve:no params');
    end
    vlt = vcse2vcdata(params,BIN_DURATION_REF);
    if st_data('isempty-nl', vlt)
        error('se_get_association_volatility_curve:no vc');
    end
    title = exec_sql('QUANT',sprintf( 'select domain_name from %s..domain where domain_id = %d', bq, unique(st_data('cols', run_ids_info4this_job, 'domain_id'))));
    vlt.title=title{1};
    vlt.info.curve_nature=5;
    
end

vlt =  st_data( 'keep-cols', vlt,'USUAL DAY;opening_auction;intraday_auction;closing_auction');
vlt.colnames{1}='VOL_GK';
vlt.info.CURVE_NATURE_CELL=CURVE_NATURE_CELL;

out = st_data('init', ...
    'title', ESTIMATOR_NAME, ...
    'date', vlt.date + datenum(as_of_date, 'dd/mm/yyyy'), ...
    'value', [vlt.value, zeros(length(vlt.value), 1)], ...
    'colnames', {vlt.colnames{:}, 'default'});


end