function out = se_run_volatility_curve(security_id, trading_destination_id, window_type_name, run_window_width, run_date, dt_remove, varargin_)
% SE_RUN_VOLATILITY_CURVE - Volatility curve estimation
% Quick example:
% se_run_volatility_curve(110, [], 'day', 1000, '23/10/2015', [], [])
%
% The function signature is conform to function call in function se_run:
% List of input arguments:
% * security_id: security_id of the stock considered. Could be a list of
% security_id. This feature is disable this function.
% * trading_destination_id: trading_destination_id. Empty
% trading_destination_id corresponds to the consolidated destination. This
% feature is disabled for this function
% * window_type_name: Unit of the run_window_width
% * run_window_width: Size of the period (unit = window_type_name) over
% which the data is collected
% * run_date: end date of the period. The expected date format is dd/mm/yyyy
% * dt_remove: dates to be removed
% * job_varargin: additional arguments if needed.
%
% Methodology:
% In the same way as for volume curves, volatility curves 
% give the volatility over intraday intervals as a proportion of of
% the daily volatility.
% the daily volatility centralised indicator is calculated as follows:
% ((sqrt( (H - L)^2/2 - (2*log(2)-1)*(C - O)^2) / ( (O + C)/2))/(sqrt(8.5*6)))*10000
% Remark: The normalisation factor 10000/sqrt(8.5*6) gives volatility in bps/10 min


assert(isscalar(security_id),'se_run_volatility_curve:wrong_security_id_format',...
    'security_id must be a scalar in se_run_volatility_curve')
assert(isempty(trading_destination_id),'se_run_volatility_curve:empty_td_id',...
    'se_run_volatility_curve runs on consolidated data only')

%%% HARD CODED VARIABLES
USUAL_DAYS_CONTEXT_MIN_NB_DAYS = 200;
NY_CONTEXT_MIN_NB_DAYS = 50;
MAX_PROPORTION_EMPTY_BINS = 0.3;

% Dates treatment
[from_datenum, to_datenum] = se_window_spec2from_to(window_type_name, run_window_width, run_date);
date_ny = ny_context4volatility_curve(security_id,from_datenum,to_datenum);
[usual_compo,ny_compo] = date_connexe_components(from_datenum,to_datenum,date_ny);


try
    % First try with 5-minute bins.
    [usual_day_curve,ny_shift_curve] = build_curve(security_id,trading_destination_id,usual_compo,ny_compo,5/60/24);
    slice_duration = 5/60/24;
catch er
    if strcmp(er.identifier,'computing_curve:too_much_empty_bins')
        % Not enough non-empty bins. Second try with 15 minutes
        [usual_day_curve,ny_shift_curve] = build_curve(security_id,trading_destination_id,usual_compo,ny_compo,15/60/24);
        slice_duration = 15/60/24;
    else
        rethrow(er)
    end
end

%%% Build of the standardised st_data for output
% The following fields are required:
% .info.run_quality, size 1 x N vector, where N = context number
% .rownames, parameter names
% .colnames, context names

nb_slices = length(usual_day_curve.date);
rownames = reshape(tokenize(sprintf('slice%03d;',1:nb_slices)),[],1);
out = st_data('init','colnames',{'Usual day'},'value',usual_day_curve.value,...
    'date',usual_day_curve.date);
out.info = struct('run_quality',usual_day_curve.info.quality);
out.rownames = rownames;

if ~st_data('isempty-no_log',ny_shift_curve)
    out.value(:,end+1) = ny_shift_curve.value;
    out.colnames{end+1} = 'New York shift';
    out.info.run_quality = [out.info.run_quality,ny_shift_curve.info.quality];
end

% Adding three rows begin_of_first_slice, end_of_first_slice and slice_duration
out.value(end+1,:)  = usual_day_curve.info.begin_of_first_slice;
out.rownames{end+1} = 'begin_of_first_slice';
out.date(end+1) = 1;
out.value(end+1,:) = usual_day_curve.info.end_of_first_slice;
out.rownames{end+1} = 'end_of_first_slice';
out.date(end+1) = 2;
out.value(end+1,:) = usual_day_curve.info.end_of_last_slice;
out.rownames{end+1} = 'end_of_last_slice';
out.date(end+1) = 3;
out.value(end+1,:) = slice_duration;
out.rownames{end+1} = 'slice_duration';
out.date(end+1) = 4;

    function [usual_curve,ny_curve] = build_curve(security_id,trading_destination_id,usual_dates,ny_dates,time_frame)
        % BUILD_CURVE - Helper function building the spread curves
        % * usual_dates and ny_dates are resp. usual days connexe components and ny
        % shift connexe components.
        % * time_frame gives the size of the grid for the spread curve computation
        % in Matlab date serial numbers
        
        % Building the option cellarray for get_basic_indicator_v2 called in get_data
        opts = {'source:char','tick4bi','window:time',time_frame,'step:time',time_frame,...
            'bins4stop:b', false,'context_selection:char','null','ind_set:char','std',...
            'grid_mode','time_and_phase','kingpin:time',0.5,'t_acc:time',11/60/24};
        
        % Grabing data for both contexts
        data_usual = get_data(security_id,trading_destination_id,usual_dates(:,1),usual_dates(:,2),...
            opts,USUAL_DAYS_CONTEXT_MIN_NB_DAYS);
        if isempty(ny_dates)
            data_ny = st_data('from-idx',data_usual,[]);
        else
            data_ny = get_data(security_id,trading_destination_id,ny_dates(:,1),ny_dates(:,2),...
                opts,NY_CONTEXT_MIN_NB_DAYS);
        end
        
        % Usual day curve
        assert(~st_data('isempty-no_log',data_usual),'build_curve:no_usual_day_data',...
            'The data set for context usual day is empty')
        nb_usual_days = length(unique(floor(data_usual.date)));
        assert(nb_usual_days>=USUAL_DAYS_CONTEXT_MIN_NB_DAYS,'build_curve:not_enough_usual_dates',...
            'Number of usual dates is lower than %d',USUAL_DAYS_CONTEXT_MIN_NB_DAYS)
        usual_curve = computing_curve(data_usual);
        
        % NY curve
        try
            assert(~st_data('isempty-no_log',data_ny),'build_curve:no_ny_day_data',...
                'The data set for context ny is empty')
            nb_usual_days = length(unique(floor(data_ny.date)));
            assert(nb_usual_days>=NY_CONTEXT_MIN_NB_DAYS,'build_curve:not_enough_ny_dates',...
                'Number of ny context dates is lower than %d',NY_CONTEXT_MIN_NB_DAYS)
            ny_curve = computing_curve(data_ny);
        catch e
            ny_curve = st_data('from-idx',usual_curve,[]);
        end
    end
    

    function curve = computing_curve(data)
        % COMPUTING_CURVE - Helper function computing curve from st_data DATA
        % Stop auctions are mixed in the data. Those events contribute to
        % the volatility. We remove intraday, opneing and closing auction
        % as they do not seem to impact the volatility.
        idx_cont = all(st_data('cols',data,'opening_auction;intraday_auction;closing_auction')==0,2);
        data = st_data('from-idx',data,idx_cont);
        gk = @(o,h,l,c)((h-l).^2/2 - (2*log(2)-1)*(c-o).^2)./((o+c).^2/4); % Squared GK volatility
        [o,~,data,~] = safer_bin_separator(data,data,'open','colnames_format','HH:MM:SS');
        h = safer_bin_separator(data,data,'high','colnames_format','HH:MM:SS');
        l = safer_bin_separator(data,data,'low','colnames_format','HH:MM:SS');
        c = safer_bin_separator(data,data,'close','colnames_format','HH:MM:SS');
        begin_slice = safer_bin_separator(data,data,'begin_slice','colnames_format','HH:MM:SS');
        end_slice = safer_bin_separator(data,data,'end_slice','colnames_format','HH:MM:SS');
        intraday_volatility = gk(o.value,h.value,l.value,c.value);
        idx_nan = isnan(intraday_volatility);
        if mean(idx_nan(:)) > MAX_PROPORTION_EMPTY_BINS % An error is risen
            error('computing_curve:too_much_empty_bins',...
                'There are too much empty bins to compute a curve')
        end
        [median_curve,quality] = stats(intraday_volatility);
        % Normalistion factor to get real bps/10 minutes after
        % multiplication by the centralised indicator of daily volatility.
        centralised_ind_norm = 8.5*6/(end_slice.value(1,end)-begin_slice.value(1,1))/24/6;
        curve = st_data('init','colnames',{'Curve'},'value',sqrt(median_curve')*centralised_ind_norm,...
            'date',datenum(o.colnames,'HH:MM:SS'),'title','Volatility curve');
        curve.info = struct('quality',quality,'begin_of_first_slice',begin_slice.value(1,1),...
            'end_of_first_slice',end_slice.value(1,1),'end_of_last_slice',end_slice.value(1,end));
    end
end

function [remain,remove] = date_connexe_components(beg_date,end_date,rm_date)
% DATE_CONNEXE_COMPONENTS - Helper function to find connexe periods
% clean out dates from rm_date that are not within the period from beg_date
% to end_date
rm_date(rm_date<beg_date|end_date<rm_date) = [];
if isempty(rm_date)
    remain = [beg_date,end_date];
    remove = nan(0,2);
    return
end
remove = [reshape(rm_date(1:end-1),[],1),reshape(rm_date(2:end),[],1)];
remove = [[remove(1);remove(diff(rm_date)>1,2)],[remove(diff(rm_date)>1,1);remove(end)]];
remain = [[beg_date;remove(:,2)+1],[remove(:,1)-1;end_date]];
remain = remain(diff(remain,[],2)>-1,:);
end

function data = get_data(security_id,trading_destination_id,beg_date,end_date,opts,min_nb_day)
% GET_DATA - Helper function to grab data ensuring minimum of days
N = length(beg_date);
data = cell(N,1);
nb_days = zeros(N,1);
for i = N:-1:1
    beg_date_str = datestr(beg_date(i),'dd/mm/yyyy');
    end_date_str = datestr(end_date(i),'dd/mm/yyyy');
    data{i} = from_buffer('get_basic_indicator_v2','basic_indicator','security_id',security_id,...
        'trading-destinations',trading_destination_id,'from',beg_date_str,'to',end_date_str,opts{:},...
        'output-mode', 'all');
    nb_days(i) = length(unique(floor(data{i}.date)));
    if sum(nb_days) >= min_nb_day
        break
    end
end
data(cellfun('isempty',data)) = [];
data = st_data('stack',data{end:-1:1});
end

function [estimate,confidence] = stats(curves)
% STATS - Compute the stats for spread curve estimation
norm_curve = bsxfun(@rdivide,curves,sum(curves,2));
estimate = mle_lognorm_max(norm_curve);
residuals = bsxfun(@minus,norm_curve,estimate);
confidence = max(0,1-nanvar(residuals(:))/nanvar(norm_curve(:))); % R2
end

function estimate = mle_lognorm_max(U)
U(U==0) = nan;
nb_slice = size(U,2);
% log_U = log(U);
% fun = @(mu_sigma)nansum(nanmean(...
%     bsxfun(@minus,log_U',mu_sigma(1:length(mu_sigma)/2)).^2,2)./mu_sigma(1+length(mu_sigma)/2:end).^2);
% nonlcon = @(mu_sigma)deal(-1,sum(exp(mu_sigma(1:length(mu_sigma)/2)+mu_sigma(1+length(mu_sigma)/2:end).^2/2))-1);
initial = -log(nb_slice);
mu_sigma_0 = [-12*ones(nb_slice,1);sqrt(2*(initial+12))*ones(nb_slice,1)];
% options = optimset('fmincon');
% options = optimset(options,'Algorithm','interior-point','MaxFunEvals',10000,...
%     'Display','off');
% [param,fun_val,exit_flag] = fmincon(fun,mu_sigma_0,[],[],...
%     [],[],[],[],nonlcon,options);
options = optimset('fsolve');
options = optimset(options,'Display','off');
fun_zeros = @(mu_sigma)mle_helper_fun(mu_sigma(1:length(mu_sigma)/2),mu_sigma(1+length(mu_sigma)/2:end),U);
[param,fun_zeros_val,exit_flag] = fsolve(fun_zeros,mu_sigma_0,options);
estimate = exp(param(1:nb_slice)+param(1+nb_slice:end).^2/2)';
assert(any(exit_flag==[1,0]),'mle_lognorm_max:fmincon_convergence_error',...
    'fmincon was unable to converge correctly')
% Constraint satisfaction check
assert(abs(sum(estimate)-1)<0.0001,'mle_lognorm_max:normaliseed_slice_sum',...
    'The estimated slices do not sum up to 1')

% % Case when exit_flag = 0 (i.e. fmincon hit max number of iterations)
% if exit_flag == 0
%     (exp(param(1+nb_slice:end).^2)-1).*exp(2*param(1:nb_slice)+param(1+nb_slice:end).^2)
%     fun(mu_sigma_0)
end

function Y = mle_helper_fun(mu,sigma,xi)
% mu et sigma : deux vecteurs de meme taille
% xi : matrice avec autant de lignes que d'observation
%      et autant de colonnes que d'elements dans mu et sigma
xi(xi==0) = nan;
ni = sum(~isnan(xi));
mu = reshape(mu,1,[]);
sigma = reshape(sigma,1,[]);
sigma_2 = sigma.^2;
Y1 = ni./sigma_2 + nansum(bsxfun(@rdivide,bsxfun(@minus,log(xi),mu),sigma_2).^2) - ...
    nansum(bsxfun(@rdivide,bsxfun(@minus,log(xi),mu),sigma_2));
Y2 = nansum(bsxfun(@rdivide,bsxfun(@minus,log(xi),mu),sigma_2)) - ...
    exp(mu+sigma_2/2).*nansum(nansum(bsxfun(@rdivide,bsxfun(@minus,log(xi),mu),sigma_2)));
Y = [reshape(Y1,[],1);reshape(Y2,[],1)];
end