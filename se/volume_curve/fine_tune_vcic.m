function rslt = fine_tune_vcic(security_id, trading_destination_id, window_spec, end_date, varargin)
% FINE_TUNE_VCIC - fonction d'aide � la calibration des niveaux de
% confiance � utiliser
%
% rslt = fine_tune_vcic({110 276}, {{4}, {4}}, 'day|180', '17/11/2008')
% rslt = fine_tune_vcic({[110 276]}, {{4}}, 'day|180', '17/11/2008')
% rslt = fine_tune_vcic([], [], 'day|180', '17/11/2008')


fichier = 'report_fine_tune_vcic';

latex('header', fichier, 'title', 'Rapport pour la calibration des niveaux de confiance des courbes de volume');

if isempty(security_id)
    %TODO : requete plus generique, par exemple ne pas se baser sur
    %l'estimator_id...
    domains = get_domain4estimator('Volume curve');
    security_id = domains(:, 1);
    trading_destination_id = domains(:, 2);
    names = domains(:, 3);
else
    %TODO
    names = repmat({'faux nom'}, length(security_id), 1);
end

opt = options({'step:time', datenum(0,0,0,0,5,0), 'window_spec:char', window_spec}, varargin);
rslt = [];
q = [1-(0.8:-0.05:0.6),0.6:0.05:0.8];%0.1*(1:9);

colnames_ = tokenize(sprintf('%g;', q));
latex('section', fichier, 'Rapport d�taill� courbe par courbe');
for i = 1 : length(security_id)
    st_log('Domain : <%s> \t <%d/%d>\n', names{i}, i, length(security_id));
    try
        tmp = temporary_bufferize('read_dataset',{['gi:seasonnality/' opt2str(opt)], ...
            'security_id', security_id{i}, 'trading-destinations', trading_destination_id{i}{1}, ...
            'from', end_date, 'to', end_date}, 'buff_errors');
        tmp.info.ci.qg_key = ['trading_destination_id=' num2str(tmp.info.td_info(1).trading_destination_id) '/quotation_group=' tmp.info.td_info(1).quotation_group];
        tmp.attach_format = 'HH:MM';
    catch ME
        st_log(ME.message);
        continue;
    end
    latex('subsection', fichier, names{i});
    latex('subsubsection', fichier, 'Courbe non cumul�e, avec fixings');
    latex('st_plot+graphic', fichier, st_data('keep', tmp, 'Usual day'));
    
    tmp = st_data('where', tmp, '~({opening_auction}|{intraday_auction}|{closing_auction})');
    latex('subsubsection', fichier, 'Courbe non cumul�e, sans fixings');
    latex('st_plot+graphic', fichier, st_data('keep', tmp, 'Usual day'));
    
    if numel(q)~=9 || ~all(q==0.1*(1:9))
        [trashes, tmp.info.ci.dist] = vc2cumci(tmp, q);
    end
    
    latex('subsubsection', fichier, 'Intervalles de confiance autour de la courbe cumul�e');
    data = st_data('init', 'value', repmat(trashes, 1, size(tmp.info.ci.dist, 2))+tmp.info.ci.dist, 'colnames', colnames_,...
        'date', tmp.date(2:end), 'attach_format', 'HH:MM', 'title', names{i});
    st_plot(data, 'visible', 'off');legend(data.colnames, 'location', 'northwest')
    latex('graphic', fichier, data);
    
    rslt = [rslt; tmp.info.ci];
end


latex('section', fichier, 'Distribution des intervalles de confiance');
[tmp, idx] = sort({rslt.qg_key});
rslt = rslt(idx);
qg = {rslt.qg_key};
u_qg = unique(qg);
q4ic = [0.05 0.25 0.5 0.75 0.95];
leg4q4ic = tokenize(sprintf('%g;', q4ic));
for k = 1 : length(u_qg)
    latex('subsection', fichier, u_qg{k});
    this_qg_rslt = rslt(strmatch(u_qg{k}, qg));
    for i = 1 : length(q)
        latex('subsubsection', fichier, sprintf('niveau de confiance : %g', q(i)));
        sample = [];
        for j = 1 : length(this_qg_rslt)
            sample = [sample, this_qg_rslt(j).dist(:, i)];
        end
        figure('visible', 'off');
        plot(quantile(sample, [0.05 0.25 0.5 0.75 0.95], 2));legend(leg4q4ic)
        latex('graphic', fichier);
    end
end

% latex('section', fichier, 'Boite � moustache du maximum de la valeur absolue de la distance entre la courbe de volume et les intervalles de confiance');
% for k = 1 : length(u_qg)
%     latex('subsection', fichier, u_qg{k});
%     this_qg_rslt = rslt(strmatch(u_qg{k}, qg));
%     for i = 1 : length(q)
%         latex('subsubsection', fichier, sprintf('niveau de confiance : %g', q(i)));
%         sample = [];
%         for j = 1 : length(this_qg_rslt)
%             sample = [sample, nanmax(abs(this_qg_rslt(j).dist(:, i)))];
%         end
%         if any(isfinite(sample))
%             figure('visible', 'off');
%             boxplot(sample);
%             latex('graphic', fichier);
%         end
%     end
% end

latex('section', fichier, 'Boite � moustache du maximum de la valeur absolue de la distance entre la courbe de volume et les intervalles de confiance');
quantile_labels = tokenize(sprintf('%g;', q));
for k = 1 : length(u_qg)
    latex('subsection', fichier, u_qg{k});
    this_qg_rslt = rslt(strmatch(u_qg{k}, qg));
    sample = [];
    group_num = [];
    for i = 1 : length(q)
        for j = 1 : length(this_qg_rslt)
            group_num(end+1) = i;
            sample = [sample, nanmax(abs(this_qg_rslt(j).dist(:, i)))];
        end
    end
    if any(isfinite(sample))
        figure('visible', 'off');
        boxplot(sample, group_num, 'labels', quantile_labels);
        latex('graphic', fichier);
    end
end

latex('end', fichier);
latex('compile', fichier);