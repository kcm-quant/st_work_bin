function [index_domains_ameri,is_successfully_update] = get_index_domains_ameri()
% GET_INDEX_DOMAINS_AMERI - creation domaines amérique du nord =\= Mexique.

%%%% Paremetres codes en dur.
VALID_EXCHGID = {'NYSE','NASD','TSE'};
AS_OF_DATE = today;
WIDTH_NB_DAY = 100; % Taille de la fenetre en nb de jours calendaires pour le calcul du nombre de transactions
OFFSET_NB_DAYS = 7; % Nb de jours de decalage par rap. a AS_OF_DATE pour recup. donnees de transaction
NB_DEAL_TRESH = 1.8e3; % pour sélectionner un titre comme représentant d'un groupe de quotation, on veut qu'il ait une certain nombre moyen de transactions par jour
NB_STOCK_TRESHOLD = 10; % pour garder un groupe de quotation on veut qu'il ait un certain nombre de représentants

global st_version;
quant = st_version.bases.quant;
repository = st_version.bases.repository;
market_data = st_version.bases.market_data;

is_successfully_update = 1;

from = datestr(AS_OF_DATE-OFFSET_NB_DAYS-WIDTH_NB_DAY, 'yyyy-mm-dd');
to = datestr(AS_OF_DATE-OFFSET_NB_DAYS, 'yyyy-mm-dd');

%mail en cas d'erreur
address = exec_sql('QUANT',['SELECT owner from ',quant,'..estimator GROUP BY owner']);

index_domains_ameri = {};

%fichier de log
dir_path = fullfile(getenv('st_repository'), 'log', 'perim_update');
log_path = fullfile(dir_path , sprintf('lmi_mtd_ameri_update.txt'));
st_log('$out+$', log_path);

exchg_str = sprintf('''%s'',',VALID_EXCHGID{:});

exch_mktgr_secid = exec_sql('KGR',['select distinct EXCHGID,MKTGROUP,convert(int,SYMBOL6)',...
    ' from ',repository,'..SECURITY',...
    ' where EXCHGID in (',exchg_str(1:end-1),')',...
    ' and SYMBOL6 is not null',...
    ' and MKTGROUP is not null',...
    ' and STATUS = ''A''']);
[mktgroup,~,idx_mktgroup] = unique(exch_mktgr_secid(:,2));

for i = 1:length(mktgroup)
    try
        sec_and_nb_trades = check_data();
        if isempty(sec_and_nb_trades)
            continue
        end
        [~,I] = sort(sec_and_nb_trades(:,2));
        sec_and_nb_trades = sec_and_nb_trades(I,:);
        
        %%% on veut qu'il y ait au moins un certain nombre de titres dans le groupe
        
        if ~isempty(sec_and_nb_trades) && size(sec_and_nb_trades, 1) >= NB_STOCK_TRESHOLD
            top_N = max(floor(sum(sec_and_nb_trades(:,2) > NB_DEAL_TRESH)*0.1),NB_STOCK_TRESHOLD);
            tmp_top = mktgroup(i);
            tmp_top{2} = sprintf('Top%d',top_N);
            tmp_top{3} = sec_and_nb_trades(end-top_N+1:end,1);
            index_domains_ameri = cat(1,index_domains_ameri,tmp_top);
        end
    catch e
        st_log(sprintf('ERROR : %s\n', e.message));
        error_message = se_stack_error_message(e);
        identification_err = sprintf(['ECHEC DANS LA MISE A JOUR DU GROUPE DE QUOTATION AMERICAIN :''',mktgroup{i},'''\n%s'],error_message);
        subject = sprintf('Erreur de mise a jour du perimetre : LMI MTD AMERI %s', hostname());
        sendmail(address,subject,identification_err)
        is_successfully_update=0;
    end
    
end

    function sec_and_nb_trades = check_data()
        sec_str = join(',',cell2mat(exch_mktgr_secid(idx_mktgroup == i,3)));
        sec_and_nb_trades = cell2mat(exec_sql('MARKET_DATA',sprintf(...
            [' select security_id,avg(nb_deal)               ',...
            ' from ',market_data,'..trading_daily_ameri      ',...
            ' where security_id in (%s)                      ',...
            ' and date between ''%s'' and ''%s''             ',...
            ' and trading_destination_id is null             ',...
            ' group by security_id                           '],...
            sec_str,from,to)));
        if isempty(sec_and_nb_trades)
            return
        end
        sec_str = join(',',sec_and_nb_trades(:,1));
        sec_id = cell2mat(exec_sql('MARKET_DATA',...
            ['select distinct security_id from ',market_data,'..trading_daily_ameri',...
            ' where security_id in (',sec_str,')',...
            ' and date between ''',datestr(datenum(from,'yyyy-mm-dd')-30,'yyyy-mm-dd'),''' and ''',from,'''']));
        sec_and_nb_trades = sec_and_nb_trades(ismember(sec_and_nb_trades(:,1),sec_id),:);
    end
end

function str = join(separator,list)
if isempty(list)
    str = '';
    return
end
if isnumeric(list)
    str = sprintf(['%d',separator],list);
elseif iscellstr(list)
    str = sprintf(['%s',separator],list{:});
else
    error('join:wronginputargs','Second argument should be either numeric or cellstr')
end
str(end-length(sprintf(separator))+1:end) = [];
end