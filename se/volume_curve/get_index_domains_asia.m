function [index_domains_asia is_successfully_update] = get_index_domains_asia()
% GET_index_DOMAINS_ASIA - cr�e les domaines pour les actions asiatiques


global st_version;
sv = st_version.bases.quant;
is_successfully_update =1;
%%%% parameters
TD_ID_VALID=[20 21 22 23 26 31 32 117 118 119 121 122];


as_of_date = now;
width_nb_day = 183; % la taille calendaire de la fan�tre sur laquelle on va regarder le nombre de transactions
offset_nb_days = 14; % pour les donn�es de transaction on prend un peu de retard, pour �tre sur qu'elles sont l�
%nb_deal_tresh = 200*5/7*width_nb_day; % pour s�lectionner un titre comme repr�sentant d'un groupe de quotation, on veut qu'il ait une certain nombre moyen de transactions par jour
nb_stock_treshold = 10; % pour garder un groupe de quotation on veut qu'il ait un certain nombre de repr�sentants

from = datestr(as_of_date-offset_nb_days-width_nb_day, 'yyyymmdd');
to = datestr(as_of_date-offset_nb_days, 'yyyymmdd');

%mail en cas d'erreur
address = exec_sql('QUANT', sprintf('SELECT owner from %s..estimator GROUP BY owner', sv));

index_domains_asia={};

%fichier de log
dir_path = fullfile(getenv('st_repository'), 'log', 'perim_update');
log_path = fullfile(dir_path , sprintf('lmi_mtd_asia_update.txt'));
st_log('$out+$', log_path);

if ispc()
    blank_char = sprintf('\n');
else
    blank_char = sprintf('\\n');
end


% < On s�lectionne certains groupes de cotation asiatique
try
    tmp_index_domains_asia = exec_sql('BSIRIUS', ['select qgth.trading_destination_id,'...
        '      qgth.quotation_group'...
        '  from'...
        '      repository..quotation_group_trading_hours qgth'...
        '  where '...
        '      qgth.opening is not null' ... % on d�sire qu'il y ait une phase continue
        '      and qgth.end_date is null'... % on d�sire que la groupe de quotation existe � l'heure actuelle
        '      and trading_destination_id in '...
        '     (select distinct trading_destination_id from '...
        '      tick_db..trading_daily_asia )' ...
        ]);
    %>
catch e
    st_log(sprintf('ERROR : %s\n', e.message));
    error_message = se_stack_error_message(e);
    if ~ispc()
        error_message = regexprep(error_message, '\n', '\r');
    end
    
    %envoie de mail
    identification_err = sprintf('%s%s%s', 'ECHEC DANS LA MISE A JOUR D''UN OU PLUSIEURS INDICES ASIATIQUES',blank_char,error_message);
    subject = sprintf('Erreur de mise a jour du perimetre:LMI MTD ASIA %s', hostname());
    
    for j=1:length(address)
        se_sendmail( address{j},subject,identification_err)
    end
    is_successfully_update=0;
    index_domains_asia={};
    return;
    
end

% Vu qu'il n'y a rien dans les tables actuellement, on conserve juste les trading destination qui nous int�resse, et on
% cr�e nous-m�me nos groupes de quotation !
% idx_ok_null=cellfun(@(c)(strcmpi(c,'null')),index_domains_asia(:,2));
% index_domains_asia=index_domains_asia(idx_ok_null,:);
%
tmp_index_domains_asia(:, end + 1) = cell(size(tmp_index_domains_asia, 1), 1);

for i = 1 : size(tmp_index_domains_asia, 1)
    try
        sec_and_nb_trades = cell2mat(exec_sql('BSIRIUS', sprintf([...
            ' select ' ...
            '    trd.security_id,' ...
            '    avg(trd.nb_deal) mean_nb_deal' ...
            ' from ' ...
            '    tick_db..trading_daily_asia trd' ...
            ' where ' ...
            '    trd.security_id in ( select distinct sm.security_id' ...
            '                            from ' ...
            '                                repository..security_market sm,' ...
            '                                tick_db..tickdb_feeder_security tfs' ... % c'es juste pour s'assurer qu'il est cr�dible d'avoir des donn�es de transaction sur ce titre dans tick_db
            '                            where ' ...
            '                                sm.trading_destination_id = %d' ...
            '                                and sm.security_id = tfs.security_id '...
            '                                and sm.quotation_group = %s)' ...
            '    and trd.trading_destination_id = %d' ...
            '    and trd.date between ''%s'' and ''%s''' ...
            ' group by trd.security_id' ...
            ' order by mean_nb_deal'], tmp_index_domains_asia{i, 1}, ...
            add_quote_if_not_null(tmp_index_domains_asia{i, 2}), ...
            tmp_index_domains_asia{i, 1}, ...
            from, to)));
        
        %%% on ne garde que les actions qui cotent encore !
        if ~isempty(sec_and_nb_trades)
            sec_ids_txt=sprintf('%d,', sec_and_nb_trades(:,1));
            sec_ids_txt(end)=[];
            sec_valid = cell2mat(exec_sql('BSIRIUS', sprintf(['select distinct security_id from repository..security_historic where end_date is null and security_id in (%s)'], sec_ids_txt)));
            sec_and_nb_trades=sec_and_nb_trades(ismember(sec_and_nb_trades(:,1),sec_valid),:);
        end
        
        %%% on veut qu'il y ait au moins un certain nombre de titres dans le groupe
        if ~isempty(sec_and_nb_trades) && size(sec_and_nb_trades, 1) >= nb_stock_treshold
            
            tmp_top10=tmp_index_domains_asia(i,:);
            tmp_top10{2}='Top10';
            tmp_top10{3}=sec_and_nb_trades(end-nb_stock_treshold+1:end,:);
            index_domains_asia=cat(1,index_domains_asia,tmp_top10);
            
        end
    catch e
        st_log(sprintf('ERROR : %s\n', e.message));
        
        error_message = se_stack_error_message(e);
        if ~ispc()
            error_message = regexprep(error_message, '\n', '\r');
        end
        %envoie de mail
        identification_err = sprintf('%s%s%s', 'ECHEC DANS LA MISE A JOUR DU GROUPE DE QUOTATION ASIATIQUE:',sprintf('''%d''%s', tmp_index_domains_asia{1},blank_char),error_message);
        subject = sprintf('Erreur de mise a jour du perimetre:LMI MTD ASIA %s', hostname());
        
        for j=1:length(address)
            se_sendmail( address{j},subject,identification_err)
        end
        is_successfully_update=0;
        
    end
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Limitation des p�rim�tres aux donn�es OK !!!!!!!!!!!!!!!!!!!
%%% ATTENTION A MODIFIER
if ~isempty(tmp_index_domains_asia)
    all_td_index_domains_asia=cat(1,index_domains_asia{:,1});
    index_domains_asia=index_domains_asia(ismember(all_td_index_domains_asia,TD_ID_VALID),:);
end




end

function str = add_quote_if_not_null(str)
if ~strcmp(str, 'null')
    str = sprintf('''%s''', str);
end
end

%%% test requete sql
% select distinct sm.security_id from repository..security_market sm, tick_db..tickdb_feeder_security tfs where sm.trading_destination_id = 20 and sm.security_id = tfs.security_id and sm.quotation_group is null











