function [quotation_group_domains, is_successfully_update ]= get_quotation_group_domains(exchdid)
% GET_QUOTATION_GROUP_DOMAINS - Cr�e un domaine pour chaque groupe de
% cotation, domaine qui contient les titres les plus liquides de ce groupe
% de cotation
%
% quotation_group_domains = get_quotation_group_domains()
% quotation_group_domains{i, 1} = trading_destination_id
% quotation_group_domains{i, 2} = quotation_group
% quotation_group_domains{i, 3} = les security_id des titres les plus
% liquides dans ce groupe de cotation
%

global st_version
quant = st_version.bases.quant;
repository = st_version.bases.repository;
market_data = st_version.bases.market_data;
decay = st_version.my_env.decay;

is_successfully_update =1;

w = weekday(today - decay);
as_of_date = today - (w==1)*2-(w==7);

width_nb_day = 100; % la taille calendaire de la fen�tre sur laquelle on va regarder le nombre de transactions
nb_deal_tresh = 200*5/7; % pour s�lectionner un titre comme repr�sentant
% d'un groupe de quotation, on veut qu'il ait une certain nombre moyen de transactions par jour
nb_stock_treshold = 10; % Nombre minimum de valeurs requis au sein d'un groupe de cotation

from = datestr(as_of_date-width_nb_day+1, 'yyyymmdd');
to = datestr(as_of_date, 'yyyymmdd');

%mail en cas d'erreur
address = exec_sql('QUANT',['SELECT owner from ',quant,'..estimator GROUP BY owner']);

%fichier de log
dir_path = fullfile(getenv('st_repository'), 'log', 'perim_update');
log_path = fullfile(dir_path , sprintf('quotation_group_update.txt'));
st_log('$out+$', log_path);

% Selection des groupes de cot. qui satisfont le nombre min. de valeurs
exchdid_str = sprintf('''%s'',',exchdid{:});
exchange_mktgroup = exec_sql('KGR',['select ec.EXCHGID,q.MKTGROUP',...
    ' from ',repository,'..QUOTATIONGROUP q,',repository,'..EXCHANGEREFCOMPL ec,',...
    repository,'..SECURITY s',...
    ' where ec.EXCHGID in (',exchdid_str(1:end-1),')',...
    ' and ec.EXCHANGE = q.EXCHANGE and s.MKTGROUP = q.MKTGROUP',...
    ' and s.EXCHGID = ec.EXCHGID',...
    ' and q.MKTGROUP is not null',...
    ' group by ec.EXCHGID,q.MKTGROUP',...
    ' having count(distinct s.SYMBOL6) > ',num2str(nb_stock_treshold)]);

% Au sein de chaque groupe cotation on conserve les valeurs qui :
% 1. ont un nombre de deals / jour moyen sup�rieur � "nb_deal_tresh" sur
%    la p�riode de "from" � "to"
% 2. Ont des donn�es pour des dates ant�rieures � "from"
quotation_group_domains = cell(size(exchange_mktgroup,1),3);
idx2delete = false(size(quotation_group_domains,1),1);
for i  = 1:size(quotation_group_domains,1)
    try
       quotation_group_domains{i,3} = get_security_with_data();
%         quotation_group_domains{i,3} = cell2mat(exec_sql('KGR',['select td.security_id',...
%             ' from ',repository,'..SECURITY s,',market_data,'..trading_daily td,',repository,'..EXCHANGEMAPPING em',...
%             ' where s.MKTGROUP = ''',exchange_mktgroup{i,2},'''',...
%             ' and s.STATUS = ''A'' and s.SYMBOL6 is not null and s.EXCHGID = ''',exchange_mktgroup{i,1},'''',...
%             ' and em.EXCHGID = s.EXCHGID and em.EXCHANGE = td.trading_destination_id',...
%             ' and td.security_id = convert(int,s.SYMBOL6)',...
%             ' and td.date between ''',from,''' and ''',to,'''',...
%             ' group by td.security_id',...
%             ' having avg(td.nb_deal) > ',num2str(nb_deal_tresh)]));
%         if ~isempty(quotation_group_domains{i,3})
%             quotation_group_domains{i,3} = cell2mat(exec_sql('MARKET_DATA',['select security_id',...
%                 ' from MARKET_DATA..trading_daily where date <= ''',from,'''',...
%                 ' and security_id in (',join(',',quotation_group_domains{i,3}),')',...
%                 ' and trading_destination_id is null group by security_id']));
%         end
        if isempty(quotation_group_domains{i,3}) || length(quotation_group_domains{i,3}) < nb_stock_treshold
            idx2delete(i) = true;
        end
        quotation_group_domains{i,1} = exchange_mktgroup{i,1};
        quotation_group_domains{i,2} = exchange_mktgroup{i,2};
    catch e
        idx2delete(i) = true;
        st_log(sprintf('ERROR : %s\n', e.message));
        error_message = se_stack_error_message(e);
        %envoie de mail
        identification_err = sprintf('ECHEC DANS LA MISE A JOUR DU GROUPE DE QUOTATION <%s,%s> :\n%s',...
            exchange_mktgroup{i,1},exchange_mktgroup{i,2},error_message);
        subject = sprintf('Erreur de mise a jour du perimetre:QUOTATION_GROUP %s', hostname());
        sendmail(address,subject,identification_err)
        
        is_successfully_update=0;
    end
end
quotation_group_domains(idx2delete, :) = [];

    function sec_list = get_security_with_data()
        switch cell2mat(exec_sql('KGR',strcat('select GLOBALZONEID from KGR..EXCHANGEREFCOMPL where EXCHGID = ''',exchange_mktgroup{i,1},'''')))
            case 1
                table_suf = '';
            case 2
                table_suf = '_ameri';
        end
        sec_avg_ndeal = cell2mat(exec_sql('KGR',['select td.security_id,avg(td.nb_deal)',...
            ' from ',repository,'..SECURITY s,',market_data,'..trading_daily',table_suf,' td,',repository,'..EXCHANGEMAPPING em',...
            ' where s.MKTGROUP = ''',exchange_mktgroup{i,2},'''',...
            ' and s.STATUS = ''A'' and s.SYMBOL6 is not null and s.EXCHGID = ''',exchange_mktgroup{i,1},'''',...
            ' and em.EXCHGID = s.EXCHGID and em.EXCHANGE = td.trading_destination_id',...
            ' and td.security_id = convert(int,s.SYMBOL6)',...
            ' and td.date between ''',from,''' and ''',to,'''',...
            ' group by td.security_id']));
        if isempty(sec_avg_ndeal)
            sec_list = [];
            return
        end
        sec_min_date = exec_sql('MARKET_DATA',['select security_id,max(date)',...
            ' from MARKET_DATA..trading_daily',table_suf,...
            ' where security_id in (',join(',',sec_avg_ndeal(:,1)),')',...
            ' and date < ''',from,''' group by security_id']);
        if isempty(sec_min_date)
            sec_list = [];
            return
        end
        sec_list = intersect(sec_avg_ndeal(sec_avg_ndeal(:,2)>nb_deal_tresh,1),...
            cell2mat(sec_min_date(datenum(sec_min_date(:,2),'yyyy-mm-dd')>datenum(from,'yyyymmdd')-30,1)));
        
    end
end

function str = join(separator,list)
if isempty(list)
    str = '';
    return
end
if isnumeric(list)
    str = sprintf(['%d',separator],list);
elseif iscellstr(list)
    str = sprintf(['%s',separator],list{:});
else
    error('join:wronginputargs','Second argument should be either numeric or cellstr')
end
str(end-length(sprintf(separator))+1:end) = [];
end