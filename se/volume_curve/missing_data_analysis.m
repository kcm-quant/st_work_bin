change_connections('homolo');

%% Le 19 juin 2006 :


exec_sql('QUANT', [ ...
' select trading_destination_id, count(*) from quant_homolo..domain_security ' ...
' where domain_id in ( ' ...
' select er.domain_id from quant_homolo..error e, quant_homolo..estimator_runs er  ' ...
' where message like ''%NO_DATA_FILTER:no data %''  ' ...
' and e.stamp_date = ''20090703''  ' ...
' and e.is_run = 0  ' ...
' and er.run_id = e.job_or_run_id ' ...
' and er.estimator_id = 2 ' ...
' ) ' ...
' group by trading_destination_id ' ])

% => Il manque Helsinki, Stocholm et londres

% les march� nordiques semblent ferm�s (en tout cas la su�de est ferm�e,
% pour cause de 'Midsummer Eve'

% De plus, pas de basic indicator pour Londres car il s'agit d'un witching
% day, pendant lequel il y a donc une ench�re milieu de journ�e.

exec_sql('QUANT', [ ...
' select trading_destination_id, sum(volume)  ' ...
' from tick_db..trading_daily  ' ...
' where date = ''20090619''  ' ...
' group by trading_destination_id ' ...
' order by trading_destination_id '])

% => On voit que dans les bases il y des donn�es pour Londres, mais ni pour
% Helsinki ni Stockolm

witching_days('FCE', datenum(2009,6,19,0,0,0))

% => On vaoit que c'est un witching day, et m�me un triple