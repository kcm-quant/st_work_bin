function rslt = se_check_volume_curve(security_id, trading_destination_id, window_type, window_width, as_of_day,vc)
% se_check_volume_curve - Check de l'estimateur Volume curves
%
% example :
% 
% vc = se_run_volume_curve(110, 4, 'day', 180, '17/11/2008');
% vc.info.run_quality
% rslt = se_check_volume_curve(110, 4, 'day', 1, '17/11/2008', vc)
%
% vc = se_run_volume_curve(get_repository('index-comp', 'SBF120'), [], 'day', 180, '17/11/2008');
% vc.info.run_quality
% rslt = se_check_volume_curve(get_repository('index-comp', 'SBF120'), [], 'day', 1, '17/11/2008', vc)
% 
%
% author   : 'rburgot@cheuvreux.com'
% reviewer : ''
% version  : '1'
% date     : '24/11/2008'


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
bin_duration = datenum(0,0,0,0,5,0); % constante universelle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cette valeur est cens�e �tre v�rifi�e � la cr�ation, voir se_run_volume_curve, notmalement l y a un test sur la valeur de l'option 'step:time'
% Par ailleurs elle est r�utilis�e en bas de ce fichier

vc = vcse2vcdata(vc, bin_duration);

[from, to] = se_window_spec2from_to(window_type, window_width, as_of_day);

opt = options({'step:time', bin_duration, 'window:time', bin_duration, 'bins4stop:b', false});
rslt = [];
for  i = 1 : length(security_id)
    data = read_dataset(['gi:basic_indicator/' opt2str(opt)], ...
        'security_id', security_id(i),...
        'from', from,...
        'to', to, ...
        'trading-destinations',trading_destination_id);
    rslt(i, :) = vc_check('vwap_slippage', vc, data);
end
if length(security_id) > 1
    rslt = nanmean(rslt);
end
if all(~isfinite(rslt))
    error('se_check_volume_curve:exec', 'NO_DATA_FILTER:no data');
end