function vc = se_create_parametric_volume_curve(varargin)
% se_create_parametric_volume_curve
% example : output in local time
% vc_parametric=se_create_parametric_volume_curve('security_id',110,'date',
% '10/04/2010');


opt  = options( {'security_id',110,...
    'date','01/04/2010',...
    'get_se_output', 0 ,...
    'BIN_DURATION_REF',datenum(0,0,0,0,5,0),...
    'FUNC_FIDESSA',@(x)(11*x.^3-4*x.^2-4*x+3),...
    'is_gmt:bool',false}, varargin);

sec_id=opt.get('security_id');
date=opt.get('date');
BIN_DURATION_REF=opt.get('BIN_DURATION_REF');
FUNC_FIDESSA=opt.get('FUNC_FIDESSA');
is_gmt=opt.get('is_gmt:bool');

vc=st_data('empty-init');

% - on cherche la grille de temps qui correspondrait � une grille d'un basic
% - indicator pour ce jour la
try
    grid_time = get_bi_time_grid('security_id',sec_id,'date',date,'is_gmt:bool',is_gmt,...
        'step_time',BIN_DURATION_REF,'window_time',BIN_DURATION_REF);
    
    if ~isempty(grid_time)
        
        fixing_booleans=grid_time.value;
        slice_time=grid_time.date-floor(grid_time.date);
        slice_time_renorm=(slice_time-slice_time(1))./(slice_time(end)-slice_time(1));
        vc_value_fidessa=repmat(0,size(slice_time_renorm,1),1);
        vc_value_fidessa(find(fixing_booleans(:,1)))=0.01;
        vc_value_fidessa(find(fixing_booleans(:,2)))=0;
        vc_value_fidessa(find(fixing_booleans(:,3)))=0.04;
        idx_na=~fixing_booleans(:,1) & ~fixing_booleans(:,2) & ~fixing_booleans(:,3);
        fidessa_na=(1-sum(vc_value_fidessa))*FUNC_FIDESSA(slice_time_renorm(idx_na))/sum(FUNC_FIDESSA(slice_time_renorm(idx_na)));
        vc_value_fidessa(idx_na)=fidessa_na;
        vc_value_fidessa=vc_value_fidessa/sum(vc_value_fidessa);
        
        bool1=is_context_march_special_ny('security_id',sec_id,'date',date);
        if bool1
            context_name = 'March special ny';
        else
            context_name = 'Usual day';
        end
        
        vc.title = 'DEFAULT parameters';
        vc.value = cat(2,vc_value_fidessa);
        vc.colnames = {context_name} ;
        vc.date=slice_time;
        
        %slice
        slice_num = 1;
        vc.rownames = cell(length(vc.date), 1);
        idx = find(idx_na);
        for i =1 : length(idx)
            vc.rownames{idx(i)}  =  MAKE_PARAM_NAME(slice_num);
            slice_num = slice_num + 1;
        end
        
        %AUCTIONS
        auction_fields = {'auction_open','auction_mid','auction_close'};
        is_auction_open  = ~isempty(find(fixing_booleans(:,1)));
        is_auction_mid   = ~isempty(find(fixing_booleans(:,2)));
        is_auction_close = ~isempty(find(fixing_booleans(:,3)));
        idx = find(~idx_na);
        count = 1 ;
        for i =1 : 3 %length(idx)
            switch i
                case 1
                    if is_auction_open
                        vc.rownames{idx(count)}  =  'auction_open';
                        count= count+1;
                    end
                case 2
                    if is_auction_mid
                        vc.rownames{idx(count)}  =  'auction_mid';
                        count= count+1;
                    end
                case 3
                    if is_auction_close
                        vc.rownames{idx(count)}  =  'auction_close';
                    end
            end
        end
        
        %open/mid/close Auction_closing
        for i= 1 :length(auction_fields)
            vc.value =  [vc.value ;vc.date( strmatch(auction_fields{i}, vc.rownames, 'exact'))];
            if eval(sprintf('is_%s' , auction_fields{i}))
                vc.rownames(end + 1) = {[auction_fields{i} '_closing']};
            end
        end
        
        %end_of_first_slice
        vc.value =  [vc.value ;vc.date(2)];
        vc.rownames(end+1) = {'end_of_first_slice'};
        
        vc.date = (1:length(vc.value))';
    end
catch e
    vc = [];
    st_log ('<se_create_parametric_volume_curve> : parametric curve couldn''t be generated ')
    
end
end

function str = MAKE_PARAM_NAME(i)
str = num2str(i);
str = ['slice' repmat('0', 1, 3-length(str)) str];
end
