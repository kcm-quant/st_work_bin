function out = se_get_association_volume_curve (security_id,trading_destination_id,as_of_date,context)
% SE_GET_ASSOCIATION_VOLUME_CURVE

global st_version;
bq = st_version.bases.quant;

ESTIMATOR_NAME  ='volume curve' ;
BIN_DURATION_REF=datenum(0,0,0,0,5,0);
CURVE_NATURE_CELL=cat(1,arrayfun(@(a)(a),[ -1 0 1 2 3 4 5],'uni',false),...
    {'beug','algo_fidessa','algo_external_so_fidessa',...
    'algo_cb_valeur','algo_cb_indice','validated_fidessa','validated'})';

trdt_info=get_repository('tdinfo',security_id);
td_id_main=trdt_info(1).trading_destination_id;
quotation_grp=trdt_info(1).quotation_group;

run_ids_info_1=st_data('empty-init');
run_ids_info_2=st_data('empty-init');


if isempty(trading_destination_id)
    trading_destination_id_str =   sprintf('ass.trading_destination_id is null AND ');
else
    trading_destination_id_str =  sprintf('ass.trading_destination_id = %d AND ',trading_destination_id);
end

if isempty(context)
    context = exec_sql('QUANT', ...
        sprintf(['select ctx.context_name from %s..estimator est , %s..context ctx, %s..association ass '...
        'where  ' ...
        'upper(est.estimator_name) = ''%s''AND ' ....
        'ass.security_id = %d AND '...
        '%s ' ...
        'ass.estimator_id = est.estimator_id AND ' ...
        'ctx.context_id = ass.context_id '],bq,bq ,bq,upper(ESTIMATOR_NAME), security_id,trading_destination_id_str));
    context = context{:};
end


% Selection de l'association
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isempty(trading_destination_id)
    assoc_1 = exec_sql('QUANT', sprintf([ ...
        'SELECT job_id,varargin from %s..association ass, %s..estimator est '...
        'WHERE ass.security_id=%d AND '...
        'ass.trading_destination_id is null AND ' ...
        'upper(est.estimator_name) =''%s'' AND ' ...
        'ass.estimator_id=est.estimator_id ' ],bq, bq ,security_id, upper(ESTIMATOR_NAME)));
   
    if ~isempty(assoc_1)
        run_ids_info_1 = se_get_run_frm_job('run' , cell2mat(assoc_1(:, 1)), 'is_valid', 1);
    end
    assoc_2 = exec_sql('QUANT', sprintf([
        'SELECT job_id,varargin from %s..association ass, %s..estimator est '...
        'WHERE ass.security_id is null AND '...
        'ass.trading_destination_id=%d AND ' ...
        'ass.varargin is null AND ' ...
        'upper(est.estimator_name) =''%s'' AND ' ...
        'ass.estimator_id=est.estimator_id ' ],bq, bq, td_id_main, upper(ESTIMATOR_NAME)));
    
    if ~isempty(assoc_2)
        run_ids_info_2 = se_get_run_frm_job('run' , cell2mat(assoc_2(:, 1)), 'is_valid', 1);
    end
    
else
    if(trading_destination_id == td_id_main )
        assoc_1 = exec_sql('QUANT', sprintf([
            'SELECT job_id,varargin from %s..association ass, %s..estimator est '...
            'WHERE ass.security_id=%d AND '...
            'ass.trading_destination_id=%d AND ' ...
            'upper(est.estimator_name) =''%s'' AND ' ...
            'ass.estimator_id=est.estimator_id ' ],bq, bq ,security_id, td_id_main, upper(ESTIMATOR_NAME)));
        
        if ~isempty(assoc_1)
            run_ids_info_1 = se_get_run_frm_job('run' , cell2mat(assoc_1(:, 1)), 'is_valid', 1);
        end
        assoc_2 = exec_sql('QUANT', sprintf([
            'SELECT job_id,varargin from %s..association ass, %s..estimator est '...
            'WHERE ass.security_id is null AND '...
            'ass.trading_destination_id=%d AND ' ...
            'ass.varargin=''%s'' AND ' ...
            'upper(est.estimator_name) =''%s'' AND ' ...
            'ass.estimator_id=est.estimator_id ' ],bq, bq, td_id_main, quotation_grp ,upper(ESTIMATOR_NAME)));
        
        if ~isempty(assoc_2)
            run_ids_info_2 = se_get_run_frm_job('run' , cell2mat(assoc_2(:, 1)), 'is_valid', 1);
        end
    else
        error('se_get_association_volume_curve:trading_destination_id not equal to td_id_main');
    end
    
end

if st_data('isempty-nl',run_ids_info_1) && st_data('isempty-nl',run_ids_info_2)
    vc=se_create_parametric_volume_curve('security_id',security_id,...
        'date',as_of_date);
    vc.info.curve_nature=4;
    
else
    if ~st_data('isempty-nl',run_ids_info_1)
        %%%%%%%%%%% ATTENTION  %%%%%%%%%%%%%%%%%
        job_ids=assoc_1{:, 1};
        job_ids=unique(job_ids);
        %%%%%%%%%%%%  !!!!!!!!!!!!!!!!!!!  %%%%%%%%%%%%%%%%%
        run_ids_info4this_job = st_data('where', run_ids_info_1, sprintf('{job_id}==%d',job_ids));
    else
        %%%%%%%%%%% ATTENTION  %%%%%%%%%%%%%%%%%
        job_ids=assoc_2{:, 1};
        job_ids=unique(job_ids);
        %%%%%%%%%%%%  !!!!!!!!!!!!!!!!!!!  %%%%%%%%%%%%%%%%%
        run_ids_info4this_job = st_data('where', run_ids_info_2, sprintf('{job_id}==%d',job_ids));
    end
    
    params = se_get_param(st_data('cols', run_ids_info4this_job, 'run_id'));
    if isempty(params)
        error('se_get_association_volume_curve:no params');
    end
    vc = vcse2vcdata(params,BIN_DURATION_REF);
    if st_data('isempty-nl', vc)
        error('se_get_association_volume_curve:no vc');
    end
    title = exec_sql('QUANT',sprintf( 'select domain_name from %s..domain where domain_id = %d', bq, unique(st_data('cols', run_ids_info4this_job, 'domain_id'))));
    vc.title=title{1};
    vc.info.curve_nature=5;
    
end

vc =  st_data( 'keep-cols', vc, sprintf('%s;opening_auction;intraday_auction;closing_auction' , context));
vc.colnames{1}='volume';
vc.info.CURVE_NATURE_CELL=CURVE_NATURE_CELL;

out = st_data('init', ...
    'title', ESTIMATOR_NAME, ...
    'date', vc.date + datenum(as_of_date, 'dd/mm/yyyy'), ...
    'value', [vc.value, zeros(length(vc.value), 1)], ...
    'colnames', {vc.colnames{:}, 'default'});


end