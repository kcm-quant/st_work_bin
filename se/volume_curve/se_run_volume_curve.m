function data = se_run_volume_curve(security_id, trading_destination_id, window_type, window_width, as_of_date,dt_remove,varargin_)
% SE_RUN_VOLUME_CURVE - Run de l'estimateur Volume curves
%
% se_run_volume_curve(110, [], 'day', 180, '03/11/2014')
% se_run_volume_curve(get_repository('index-comp', 'SBF120'), [], 'day',180, '17/11/2008')
% 
%
% author   : 'rburgot@cheuvreux.com'
% reviewer : ''
% version  : '1'
% date     : '20/08/2008'

MIN_AV_NBDEAL = 1000;

global st_version
market_data = st_version.bases.market_data;
%< Checking that the average number of deal is greater than MIN_AV_NBDEAL
[from, to] = se_window_spec2from_to(window_type, window_width, as_of_date);
if length(security_id) == 1
    td = get_repository('trading-destination',security_id);
    suff_base = get_repository('tdid2global_zone_suffix',td(1));
    if isempty(trading_destination_id)
        tdstr = 'is null';
    else
        tdstr = sprintf('= %d', trading_destination_id);
    end
    mean_nb_deal = cell2mat(exec_sql('MARKET_DATA', sprintf(['select avg(nb_deal) from ',market_data,'..trading_daily%s ' ...
            ' where security_id = %d and trading_destination_id %s and date between ''%s'' and ''%s'' '], ...
            suff_base,security_id, tdstr, datestr(from, 'yyyymmdd'), datestr(to, 'yyyymmdd'))));
    if ~isfinite(mean_nb_deal)
        error('se_run_volume_curve:exec', 'NODATA_FILTER: No data in trading daily for the requested period and destination');
    elseif mean_nb_deal < MIN_AV_NBDEAL
        error('st_seasonnality:exec', 'INAPPROPRIATE_METHOD: Not enough deals per day to compute a volume curve');
    end
end
%>

default_opt = {'seas_func', @nanmedian, ...
    'step:time', datenum(0,0,0,0,5,0),...
    'window_spec:char', sprintf('%s|%d', window_type, window_width),...
    'mode_season', 'std',...
    'force_unique_intraday_auction:b',false,...
    'norm_fun',@(x,v)simplex_proj(x)};

if nargin>6 && ~isempty(varargin_)
    opt = str2opt(varargin_);
    opt = options(default_opt, opt.get());
else
    opt = options(default_opt);
    if nargin < 6
        dt_remove = [];
    end
end

if abs(opt.get('step:time')/datenum(0,0,0,0,5,0) - 1) > 0.01
    error('se_run_volume_curve:check_args', 'Volume curves must be computed on a 5 minutes grid');
end

data = read_dataset(['gi:seasonnality/' opt2str(opt)], ...
    'security_id', security_id,...
    'from', as_of_date,...
    'to', as_of_date, ...
    'remove',dt_remove,...
    'trading-destinations',trading_destination_id);

if st_data('isempty-nl', data)
    error('se_run_volume_curve:exec', 'NODATA_FILTER: Empty set returned by read_dataset used with gi:seasonnality');
end

auc_colns = 'opening_auction;intraday_auction;closing_auction';
auc_param_name = {'auction_open', 'auction_mid', 'auction_close'};
auc_vals = logical(st_data('cols', data, auc_colns));
data = st_data('drop', data, auc_colns);

slice_num = 1;
data.rownames = cell(length(data.date), 1);
for i = 1 : length(data.date)
    if any(auc_vals(i, :))
        data.rownames{i} = auc_param_name{auc_vals(i, :)};
        data.rownames{end + 1} = [auc_param_name{auc_vals(i, :)} '_closing'];
        data.value(end + 1, :) = mod(data.date(i), 1);
    else
        if slice_num == 1
            data.rownames{end + 1} = 'end_of_first_slice';
            data.value(end + 1, :) = mod(data.date(i), 1);
        end
        data.rownames{i} = MAKE_PARAM_NAME(slice_num);
        slice_num = slice_num + 1;
    end
end

data.rownames{end + 1} = 'alpha';
data.value(end + 1, :) = data.info.ci.alpha;
data.rownames{end + 1} = 'beta';
data.value(end + 1, :) = data.info.ci.beta;
data.date = [data.date; NaN(length(data.rownames)-length(data.date), 1)];

function str = MAKE_PARAM_NAME(i)
str = num2str(i);
str = ['slice' repmat('0', 1, 3-length(str)) str];