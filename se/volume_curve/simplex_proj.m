function p = simplex_proj(y)
% SIMPLEX_PROJ - implents the simplex projection.
% ... According to the Euclidian norm.
% 
% Input:
% * Y is the input one-col vector
%
% Ouput:
% * P is the output one-col vector, with
%   for all i, 0 <= p(i) <= 1 and sum(p) = 1
%
% Example:
% y = randn(100,1);
% p = simplex_proj(y);
% y = ones(100,1)/102;
% p = simplex_proj(y);

% The implemented methodology follows https://arxiv.org/pdf/1101.6081.pdf

[y,I] = sort(y);
t = cumsum(y);
t(2:end) = t(1:end-1);
t(1) = 0;
t = sum(y)-t;
t = (t-1)./(length(t):-1:1)';
i = find(y-t>0,1);
if isempty(i)
    i = 1;
end
p = zeros(length(t),1);
p(I) = max(y-t(i),0);
