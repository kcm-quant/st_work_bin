function [cumvc, ci] = vc2cumci(vc, conf_levels)
% [cumvc, ci] = vc2cumci(data, 0.1*(1:9))
% plot(ci)
% 
% [cumvc, ci] = vc2cumci(data, [1-(0.8:-0.05:0.6),0.6:0.05:0.8])
%

% on retire les auctions
idx_continuous = ~any(st_data('cols', vc, {'opening_auction','intraday_auction','closing_auction'}), 2);

% ici la courbe de volume cumul�e moyen
cumvc = vc.value(idx_continuous);
cumvc = cumvc ./ sum(cumvc);
cumvc = cumsum(cumvc);
cumvc(end, :) = [];

% et enfin le param�tre de la loi beta
alpha = vc.info.ci.alpha;

% Petit calcul de qualit�. TODO mettre �a en m�moire pour permettre
% un plot
ci = NaN(length(cumvc), length(conf_levels));
for j = 1 : length(conf_levels)
    ci(:, j) = betainv(conf_levels(j), alpha.*cumvc, alpha.*(1-cumvc)) - cumvc;
end