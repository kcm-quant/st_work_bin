function Y = vc_agg(mode, X, varargin)
switch mode
    case 'agg_zeros'
        if ~isempty(varargin)
            target_zp = varargin{1}; 
        else
            target_zp = 0.05;% MAGIC NUMBER la proportion de z�ros dans les intervalles somm�s � laquelle on s'arr�te
        end
    case 'ranksum'
        if ~isempty(varargin)
            rs_treshold = varargin{1};
        else
            rs_treshold = 0.05;
        end
    case 'nbfixed'
        if ~isempty(varargin)
            nbbins = varargin{1};
        else
            nbbins = 2;
        end
        Y = X;
        for i = 1 : nbbins : (nbbins*floor(size(X, 2)/nbbins))
            Y(:, i:(i+nbbins-1)) = repmat(mean(X(:, i:(i+nbbins-1)), 2), 1, nbbins);
        end
        nbmissing = size(X, 2) - nbbins*floor(size(X, 2)/nbbins);
        if  nbmissing > 1
            Y(:, (end-nbmissing+1):end) = repmat(mean(X(:, (end-nbmissing+1):end), 2), 1, nbmissing);
        end
        return;
end
%

[N, K] = size(X);

init_grps;

while ~has_to_stop
    [i, j] = get_grps_with_min_dist;
    agg2grps(i, j);
end

Y = X;
for i = 1 : length(grps)
    Y(:, grps(i).members) = repmat(grps(i).grp_sample/length(grps(i).members), 1, length(grps(i).members));
end

    function init_grps
        switch mode
            case {'agg_zeros', 'ranksum'}
                % < initialisation des groupes
                grps = struct('members', num2cell(1:K), ... % les membres du groupe
                    ...
                    'grp_sample', [], ... % l'�chantillon que l'on obtiendra en sommant les �chantillons des membres du groupe
                    'zp_igs', 0, ... % le nombre de z�ros dans l'�chantilon du groupe
                    ...
                    'whole_sample', [], ... % l'�chantillon total
                    'zp_iws', 0 ... % la proportion de z�ros dans l'�chantillon total
                    , ...
                    'rspvalwprev', [] ...
                    );
                rs = strcmp(mode, 'ranksum');
                for i = 1 : K
                    grps(i).grp_sample   = X(:, i);
                    grps(i).whole_sample = X(:, i);
                    grps(i).zp_igs       = mean(~X(:, i));
                    grps(i).zp_iws       = mean(~X(:, i));
                    if rs && i > 1
                        grps(i).rspvalwprev = ranksum(X(:, i),X(:, i-1));
                    end
                end
                % >
            otherwise
                error();
        end
    end

    function b=has_to_stop
        switch mode
            case 'agg_zeros'
                b = ~any([grps.zp_igs] > target_zp & [grps.zp_igs] < 1-target_zp);
            case 'ranksum'
                 b = ~any([grps.rspvalwprev] >= rs_treshold);
            otherwise
                error();
        end
    end

    function [i, j] = get_grps_with_min_dist
        switch mode
            case 'agg_zeros'
                [tmp, idx_max_zeros] = max([grps.zp_igs] .* ([grps.zp_igs] < 1-target_zp));
                if idx_max_zeros == 1
                    i = 1;
                    j = 2;
                elseif idx_max_zeros == length(grps)
                    i = length(grps)-1;
                    j = length(grps);
                else
                    if abs(grps(idx_max_zeros).zp_iws-grps(idx_max_zeros-1).zp_iws) ...
                            > abs(grps(idx_max_zeros).zp_iws-grps(idx_max_zeros+1).zp_iws)
                        i = idx_max_zeros;
                        j = idx_max_zeros+1;
                    else
                        i = idx_max_zeros-1;
                        j = idx_max_zeros;
                    end
                end
            case 'ranksum'
                [tmp, idx_max] = max([grps.rspvalwprev]);
                i = idx_max;
                j = idx_max+1;
            otherwise
                error();
        end
    end

    function agg2grps(i, j)
        grps(i).members = cat(2, grps(i).members, grps(j).members);
        grps(i).grp_sample = grps(i).grp_sample + grps(j).grp_sample;
        grps(i).zp_igs = mean(~grps(i).grp_sample);
        grps(i).whole_sample = cat(1, grps(i).whole_sample, grps(j).whole_sample);
        grps(i).zp_iws = mean(~grps(i).whole_sample);
        grps(j) = [];
        if strcmp(mode, 'ranksum')
            if i < length(grps)
                grps(i+1).rspvalwprev = ranksum(grps(i).whole_sample, grps(i+1).whole_sample);
            end
            if i > 1
                grps(i).rspvalwprev = ranksum(grps(i).whole_sample, grps(i-1).whole_sample);
            end
        end
    end
end
