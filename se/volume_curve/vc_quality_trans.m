function out = vc_quality_trans(mode, q)
switch mode
    case 'trans'
        out = 2 ./ (1+exp(q/10));
    case 'inv-trans'
        out = 10 * log(2./q-1);
    otherwise
        error('vc_quality_trans:exec', 'unknown mode : <%s>', mode);
end