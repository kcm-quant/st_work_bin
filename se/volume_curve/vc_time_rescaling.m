function modified_curves = vc_time_rescaling(mode, data, varargin)
% sec_id = get_repository('index-comp', 'CAC40');
% sec_id = 110; %12058;
% d2=vcse2vcdata( se_run_volume_curve(sec_id, [], 'day', 180, '10/02/2008'), datenum(0,0,0,0,5,0));
% if length(sec_id)>1
%     d=se_run_volume_curve(sec_id, [], 'day', 21, '28/03/2008');
% else
%     data = read_dataset('gi:basic_indicator/window:time?"?datenum(0,0,0,0,5,0)?step:time?"?datenum(0,0,0,0,5,0)?bins4stop:b?false', ...
%         'security_id', sec_id, 'from', '09/03/2008', 'to', '28/03/2008', 'trading-destinations', {'MAIN'}, 'last_formula', '[{volume}]')
%     d = volume_to_proportion(data);
%     d = st_data('iday2array', d, 'volume');
%     %d = bin_separator( d  );
%     d.value = median( d.value,2);
%     d.colnames = { 'Volume'};
% end
%
% modified_curves = vc_time_rescaling('std', d2, 'plot', true)
% st_plot(modified_curves)

opt = options({ 'idx_NY_news', [], 'has_closing_auction', [], ...
    'dt_translate', 12, 'peak_duration', 12, ...
    'plot', false}, varargin);
idx_NY_news          = opt.get('idx_NY_news');
has_closing_auction  = opt.get('has_closing_auction');
dt                   = opt.get('dt_translate');
dur                  = opt.get('peak_duration');

if isempty(idx_NY_news)
    idx_NY_news = find(mod(data.date, 1) > mod(timezone('convert','initial_timezone','America/New_York','final_timezone', data.info.td_info(1).timezone,'dates', datenum(2009,3,5,8,31,0)), 1), 1, 'first');
    has_closing_auction = any(st_data('cols', data, 'closing_auction'));
end
last_continuous_idx = size(data.value, 1) - has_closing_auction;
% TODO
% en fait il y a bien de plus de cas de figure que celui l? o? l'on ne
% saitr pas quoi faire.
% Ici on s'est juste assur? que l'horaire des news macro-?conomique US
% se situe avant l'un des horiares 
if isempty(idx_NY_news) ...
        || ( idx_NY_news+dur > last_continuous_idx ) ...
        || ( idx_NY_news <= dt )
    modified_curves = st_data('log', data, 'warn', ...
        ['This method requires that the trading occur on this stock one hour ' ...
        'before the US macroeconomic news time, and one hour after']);
    return;
end
switch mode
    case 'std'
        peak = data.value(idx_NY_news:idx_NY_news+dur-1,1) - data.value(idx_NY_news-1,1);
        valP = data.value(1:last_continuous_idx,1);
        peak = ( (valP(idx_NY_news:idx_NY_news+dur-1,1) - peak) < 0) .* (valP(idx_NY_news:idx_NY_news+dur-1,1) - min( valP)/2 );
        valP(idx_NY_news:idx_NY_news+dur-1,1) = valP(idx_NY_news:idx_NY_news+dur-1,1) - peak;
        valP(idx_NY_news-dt:idx_NY_news-dt+dur-1,1) = valP(idx_NY_news-dt:idx_NY_news-dt+dur-1,1) + peak;
        
        dec0 = idx_NY_news+dur;
        val0 = valP(dec0:last_continuous_idx,1);
        dec2 = dec0-dt;
        val2 = interp1(linspace(dec2,last_continuous_idx,length(val0)), val0, dec2:last_continuous_idx)';
        val2 = val2/sum(val2)*sum(valP(dec2:end));

        if opt.get('plot')
            dts = data.date(1:last_continuous_idx);
            get_focus( [ mfilename ':' data.title]);
            stairs(dts, data.value(1:last_continuous_idx, 1), 'b-','linewidth',2);
            hold on
            stairs(dts, valP, 'r--','linewidth',2);
            stairs(dts, [valP(1:dec2-1, 1); val2] , 'k:','linewidth',2);
            hold off
            mima = [min(dts), max(dts)];
            axaxis( mima);
            attach_date('init', 'date', mima, 'format', 'HH:MM');
            legend({'Historique', 'Peak', 'Modifi?'})
        end
        modified_curves = rmfield( data, 'value');
        modified_curves.value = [valP(1:dec2-1, 1); val2];
        modified_curves.colnames = { 'March special ny'};
        if has_closing_auction
            modified_curves.value = [modified_curves.value;data.value(last_continuous_idx+1, 1)];
        end
    otherwise
        error('vc_time_rescaling:exec', 'unknown mode <%s>', mode);
end