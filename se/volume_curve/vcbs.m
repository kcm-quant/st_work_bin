function vcbs(index, mode)
% vcbs('CAC40')
%
% indexes = {'CAC40', 'SBF120 hors CAC',  'IBEX35', 'MIB30', 'SMI', 'AEX', 'FTSE 100', ...
%    'BEL20', 'OMX', 'FTSE / JSE', 'PSI 20', 'WIG20', 'KFX', 'HEX', 'OBX',
%    ...
%    'FTSE/ATHEX20'} % 'DAX',
%
% jm = findResource('scheduler','type','local');
% job = createJob(jm);
% for i = 1 : length(indexes)
%   createTask(job, 'vcbs', 0, indexes(i));
% end
% submit(job)

% from = '01/03/2010';
% to = '01/03/2011';
from = '05/09/2010';
to = '05/11/2010';



% ------------------------------------------ < r�cdup�ration des donn�es
if ~strcmp(index, 'SBF120 hors CAC')
    %     sec_ids = get_repository('index-comp', index);
    sec_ids = get_index_comp(index, 'recent_date_constraint', false);
else
    sec_ids = setdiff(get_repository('index-comp', 'SBF120', 'recent_date_constraint', false), ...
        get_repository('index-comp', 'CAC40', 'recent_date_constraint', false));
end
sec_ids = sec_ids(1:2);

% ------------------------------- % < Quelles donn�es : Primaire ou All ??
td_ids={};
%td_ids={'MAIN'};

% ----------------------------- %  < Quelles p�riode : num�ro de slice ou times
% slices=[];
slices=[ 0 0 ];
% slices=[20 80];


% ------------------------------------------- %  < Quelles strat�gie de forecast
% list_msafp = { ...
%     {'std', {'std'}}, ...
%     {'std', {'std'}, 'smallest_index_containing-excluding_smaller'}, ...
%     {'std', {'std'}, 'main_place_index'}, ...
%     {'moyenne', {'std'}}, ...
%     {'moyenne', {'std'}, 'smallest_index_containing-excluding_smaller'}, ...
%     {'moyenne', {'std'}, 'main_place_index'}, ...
%     {'isoquantile', {'std'}}, ...
%     {'isoquantile', {'std'}, 'smallest_index_containing-excluding_smaller'}, ...
%     {'isoquantile', {'std'}, 'main_place_index'}, ...
%     {'std_nij', {'std'}}, ...
%     {'std_nij', {'std'}, 'smallest_index_containing-excluding_smaller'}, ...
%     {'std_nij', {'std'}, 'main_place_index'}, ...
%     };

%      {'static_agg', {'static_agg', 5,'start_slice',0,'end_slice',0}},...


% list_msafp = {
%     {'parametric', {'std','start_slice',0,'end_slice',0}},...
%    };

% list_msafp = {
%     {'parametric', {'std'}},...
%     {'std_ne', {'std'}},...
%     {'std_ne', {'std','seas_func', @nanmean,'start_slice',0,'end_slice',0}},...
%     {'gaussian curve 2',{'std', 2,'quant0',1}}, ...
%     {'gaussian curve 2',{'composite 1',2,0.1,0.2,'curve_ref',30,'emaLength',4,'quant0',1,'meanNb05m',10,'pctMedian05m',0}},...
%     {'gaussian curve 2',{'composite 1',2,100000000,1,'curve_ref',30,'emaLength',4,'quant0',1,'meanNb05m',10,'pctMedian05m',0}},...
%     ...
%     };

% list_msafp = {
%     {'std_ne', {'std'}},...
%     {'gaussian curve 2',{'std', 2,'quant0',1}}, ...
%     {'gaussian curve 2',{'std', 2,'quant0',1,'mean_scaling',60}}, ...
%     {'gaussian curve 2',{'std cap', 2,0.1,0.2,'quant0',1}}, ...
%     {'gaussian curve 2',{'std cap', 2,0.1,0.2,'quant0',1,'mean_scaling',60}}, ...
%     {'gaussian curve 2',{'composite 1',2,0,0,'curve_ref',30,'emaLength',4,'quant0',1,'meanNb05m',10,'pctMedian05m',0}},...
%     {'gaussian curve 2',{'composite 1',2,0.1,0.2,'curve_ref',30,'emaLength',4,'quant0',1,'meanNb05m',10,'pctMedian05m',0}},...
%     {'gaussian curve 2',{'composite 1',2,0.1,0.2,'curve_ref',30,'emaLength',4,'quant0',1,'meanNb05m',10,'pctMedian05m',0,'mean_scaling',60}},...
%     ...
%     };

% list_msafp = {
%     {'std_ne', {'std'}},...
%     {'parametric', {'std'}},...
%     {'gaussian curve 2',{'composite 1',2,0,0,'curve_ref',30,'emaLength',4,'quant0',1,'meanNb05m',10,'pctMedian05m',0}},...
%     {'gaussian curve 2',{'composite 1',2,0.1,0.2,'curve_ref',30,'emaLength',4,'quant0',1,'meanNb05m',10,'pctMedian05m',0}},...
%     {'gaussian curve 2',{'composite 1',2,0.1,0.2,'curve_ref',30,'emaLength',4,'quant0',1,'meanNb05m',10,'pctMedian05m',0,'mean_scaling',60}},...
%     ...
%     };

list_msafp = {
    {'std_ne', {'std'}},...
    {'gaussian curve 2',{'composite 1',2,0,0,'curve_ref',30,'emaLength',4,'quant0',1,'meanNb05m',10,'pctMedian05m',0}},...
    {'gaussian curve 2',{'composite 1',2,0.1,0.2,'curve_ref',30,'emaLength',4,'quant0',1,'meanNb05m',10,'pctMedian05m',0}},...
    {'gaussian curve 2',{'composite 1',2,0.1,0.2,'curve_ref',30,'emaLength',4,'quant0',1,'meanNb05m',10,'pctMedian05m',0,'mean_scaling',60}},...
    ...
    };


% ------------------------------------------- %  < On ajoute les slices de la p�riode ??
if ~isempty(slices)
    start_s=slices(1);
    end_s=slices(2);
    slices_list={'start_slice',start_s,'end_slice',end_s};
    list_msafp_tmp=cellfun(@(c)(cat(2,c{2},slices_list)),list_msafp,'uni',false);
    list_msafp=cellfun(@(c1,c2)({c1{1},c2}),list_msafp,list_msafp_tmp,'uni',false);
end





% ------------------------------------------- %  < Quelles stats selectionner ?
if nargin > 1
    add_params = {};
else
    add_params = {'last_formula', {['[{slippage},{prop_dist},{rel_vol_dist},{real_eod_error},{abs_real_eod_error},' ...
        '{turnover},{turnover_ch_buy},{turnover_ch_sell},{vwap},{date4vc},{next_volume_error},{cfv_error}]']}};
end
% 'next_volume_error', 'cfv_error'






% ------------------------------------------- %  < calculs !!!!!!!
perf = backtest( 'debug', 'backtest_vc', 'compute', ...  'test'
    'domain', struct('security_id', num2cell(sec_ids), 'trading_destination_id',{td_ids}), ... % pour mono destination, mettre : 'MAIN' {repmat({'MAIN'}, length(sec_ids))}
    'from', from, ...
    'to', to, ...
    'backtest_vc_frequency', {30}, ...
    'mode_season and forecast_params',  list_msafp, ...
    add_params{:});

idx = strmatch('mode_season and forecast_params', {perf.codebook.colname});
oldcode_book = perf.codebook(idx);
old_book = oldcode_book.book;
perf.codebook(idx).book = tokenize(sprintf('%d;', 1:length(old_book)))';


if nargin > 1
    stropt = opt2str(options({'step:time', [char(165) 'datenum(0,0,0,0,5,0)'], ...
        'window:time', [char(165) 'datenum(0,0,0,0,5,0)'], ...
        'bins4stop:b', false}));
    data_aux = backtest( 'debug', 'get_gi', ['basic_indicator/' stropt], ...
        'domain', struct('security_id', num2cell(sec_ids), 'trading_destination_id',{{}}), ...
        'from', from, ...
        'to', to, ...
        'last_formula', {'[{volume},{vwap},{nb_trades},{vwas},{vol_GK},{mean_price}]'});
end
%>


% aux_data =

% ------ < cr�tion du .tex
if isempty(td_ids)
    if ~isempty(slices)
        filen = sprintf('vcbs_all_%d_%d_%s', start_s,end_s,index);
    else
        filen = sprintf('vcbs_all_daily_%s', index);
    end
else
    if ~isempty(slices)
        filen = sprintf('vcbs_main_%d_%d_%s', start_s,end_s,index);
    else
        filen = sprintf('vcbs_main_daily_%s', index);
    end
    
end

latex('header', filen, 'title', 'Backtest des courbes de volume');

latex('array', filen,cat(2, regexprep(old_book, '~|"|{||}', ''), perf.codebook(idx).book));

% >
%
% < Recensement des donn�es pr�sentes
latex('part', filen, 'Donn�es pr�sentes');
latex('section', filen, 'Par security_id et m�thodes');
non_missing_data = st_data('accumarray', perf, 'security_id','mode_season and forecast_params', 'sum(isfinite({slippage}))');
max_nb_data = max(non_missing_data.value, [], 2);
bold_mat = ~bsxfun(@eq, non_missing_data.value, max_nb_data);
%

latex('array', filen, non_missing_data.value, ...
    'linesHeader', cellfun(@(d)get_ric_from_sec_id(str2double(d)), non_missing_data.rownames), ...
    'bold_matrix', bold_mat, ...
    'col_header', regexprep(regexprep(non_missing_data.colnames, '~|"|{||}', ''), '_|-', ' '), 'maxcols', 8);
latex('text', filen, 'Maintenant nous allons faire l''intersection des dates, afin que les statistiques pr�sent�es plus loin comprenne toujours les m�mes jours pour chacune des m�thodes �valu�es');
% >

% < Intersection des dates pour mettre toutes les m�thodes sur un pied
% d'�galit�. Il vaudrait mieux que tout le monde ai bien tout ces jours
% car sinon cette intersection peut introduire un biais

perf = st_data('add-col', perf, perf.date, 'date');
tmp = st_data('group-by', perf, '{security_id},{date}', 'apply-formula', 'sum(isfinite({slippage}))');
idx2del = tmp.value(:, 3) ~= length(list_msafp);
idx2del = find(idx2del);
for i = idx2del'
    perf = st_data('where', perf, sprintf('~({security_id}==%d & {date}==%d)', tmp.value(i, 1), tmp.value(i, 2)));
end

% for i = 1 : length(sec_ids)
%     sec
%     [my_perf_by_sec, idx] = st_data('where', perf, sprintf('{security_id}==%d', sec_ids(i)));
%     my_perf_by_sec = st_data('add-col', my_perf_by_sec, my_perf_by_sec.date, 'date');
%     tmp = st_data('group-by', perf, 'data.date,{mode_season and forecast_params}', 'apply-formula', 'sum(isfinite({slippage}))');
%     dates2remove = tmp.date(tmp.value(:, 3) ~= length(list_msafp));
%     idx2remove = idx & ismember(perf.date, dates2remove);
%     perf = st_data('from-idx', perf, ~idx2remove);
% end
% >


if ~isempty(perf.date)
    
    
    % < Recensement des donn�es pr�sentes
    latex('part', filen, 'Donn�es pr�sentes apr�s intersection');
    latex('section', filen, 'Par security_id et m�thodes');
    non_missing_data = st_data('accumarray', perf, 'security_id','mode_season and forecast_params', 'sum(isfinite({slippage}))');
    max_nb_data = max(non_missing_data.value, [], 2);
    bold_mat = ~bsxfun(@eq, non_missing_data.value, max_nb_data);
    latex('array', filen, non_missing_data.value, ...
        'linesHeader', cellfun(@(d)get_ric_from_sec_id(str2double(d)), non_missing_data.rownames), ...
        'bold_matrix', bold_mat, ...
        'col_header', regexprep(regexprep(non_missing_data.colnames, '~|"|{||}', ''), '_|-', ' '), 'maxcols', 8);
    % >
    
    % < * R�sum� des performances des m�thodes
    latex('formula', filen,'\newpage')
    latex('part', filen, 'R�sum� pour le VWAP');
    latex('section', filen, 'Titre par titre');
    % < Crit�re op�rationnel
    latex('subsection', filen, 'Hypothetical (abs) slippage (considered participation constant) ');
    my_weighted_mean(filen, perf, 'security_id','mode_season and forecast_params', 'abs({slippage})', '{turnover}')
    % >
    latex('subsection', filen, 'Hypothetical (abs) slippage (considered volume constant) ');
    my_weighted_volume_constant(filen,perf,'abs({slippage})');
    % >
    latex('subsection', filen, 'Hypothetical slippage (buy order only)');
    latex('text', filen, 'Remarque : les slippage negatif = on perd de largent')
    my_weighted_mean(filen, perf, 'security_id','mode_season and forecast_params', '-1*{slippage}', '{turnover}')
    % >
    latex('subsection', filen, 'Hypothetical (abs) slippage (cheuvreux algo VWAP turnover)');
    my_weighted_mean(filen, perf, 'security_id','mode_season and forecast_params', 'abs({slippage})', '{turnover_ch_buy}+{turnover_ch_sell}')
    % >
    latex('subsection', filen, 'Hypothetical slippage (cheuvreux algo VWAP turnover) ');
    latex('text', filen, 'Remarque : les slippage negatif = on perd de largent')
    my_weighted_mean_v2(filen, perf, 'security_id','mode_season and forecast_params', '{slippage}.*({turnover_ch_sell}-{turnover_ch_buy})./({turnover_ch_buy}+{turnover_ch_sell})', '({turnover_ch_buy}+{turnover_ch_sell})')
    
    
    latex('subsection', filen, 'Max Hypothetical (abs) slippage');
    my_func(filen, perf, 'security_id','mode_season and forecast_params', 'abs({slippage})', 'max')
    
    latex('subsection', filen, 'Std Hypothetical (abs) slippage');
    my_func(filen, perf, 'security_id','mode_season and forecast_params', 'abs({slippage})', 'std')
    
    latex('subsection', filen, 'Q80 Hypothetical (abs) slippage');
    my_quantile(filen, perf, 'security_id','mode_season and forecast_params', 'abs({slippage})',0.8)
    
    % < Distance entre les proportions pr�conis�es et les proportions march�
    latex('subsection', filen, 'Proportion distance (L2 criterion on proportion * 100)');
    my_mean(filen, perf, 'security_id','mode_season and forecast_params', '100*{prop_dist}')
    % >
    
    % < Distance entre les proportions pr�conis�es et les proportions march�
    latex('subsection', filen, 'Relative cumcurve Proportion distance (max*100)');
    my_mean(filen, perf, 'security_id','mode_season and forecast_params', '100*{rel_vol_dist}')
    % >
    % < Distance entre les proportions pr�conis�es et les proportions march�
    latex('subsection', filen, 'Relative cumcurve Proportion distance WEIGHTED turnover (max*100)');
    my_weighted_mean(filen, perf, 'security_id','mode_season and forecast_params', '100*{rel_vol_dist}', '{turnover}')
    % >

    % < Distance entre le volume fin de p�riode march� et estim�
    latex('subsection', filen, 'Mean Bias on eod forecast');
    my_mean(filen, perf, 'security_id','mode_season and forecast_params', '100*{real_eod_error}');
    latex('subsection', filen, ' Mean Abs value on eod forecast');
    my_mean(filen, perf, 'security_id','mode_season and forecast_params', '100*{abs_real_eod_error}');
    latex('subsection', filen, ' Q80 Abs value on eod forecast');
    my_quantile(filen, perf, 'security_id','mode_season and forecast_params', '100*{abs_real_eod_error}',0.8)
    % >
    
    latex('formula', filen,'\newpage')
    latex('section', filen, 'P�riode par p�riode');
    % < Crit�re op�rationnel
    latex('subsection', filen, 'Hypothetical (abs) slippage');
    my_weighted_mean(filen, perf, 'date4vc','mode_season and forecast_params', 'abs({slippage})', '{turnover}', @(d)datestr(d, 'dd/mm/yyyy'))
    % >
    latex('subsection', filen, 'Hypothetical slippage (buy order only)');
    my_weighted_mean(filen, perf, 'date4vc','mode_season and forecast_params', '{slippage}', '{turnover}', @(d)datestr(d, 'dd/mm/yyyy'))
    % >
    % < Distance entre les proportions pr�conis�es et les proportions march�
    latex('subsection', filen, 'Proportion distance (L2 criterion on proportion * 100)');
    my_mean(filen, perf, 'date4vc','mode_season and forecast_params', '100*{prop_dist}', @(d)datestr(d, 'dd/mm/yyyy'))
    % >
    % < Distance entre le volume relatif pr�conis� et le volume relatif march�
    latex('subsection', filen, 'Relative volume distance');
    my_mean(filen, perf, 'date4vc','mode_season and forecast_params', '100*{rel_vol_dist}',@(d)datestr(d, 'dd/mm/yyyy'))
    % >
     % < Distance entre le volume fin de p�riode march� et estim�
    latex('subsection', filen, 'Mean Biais value on eod forecast');
    my_mean(filen, perf, 'date4vc','mode_season and forecast_params', '100*{real_eod_error}', @(d)datestr(d, 'dd/mm/yyyy'))
    % >   
      % < Distance entre le volume fin de p�riode march� et estim�
    latex('subsection', filen, 'Mean Abs value on eod forecast');
    my_mean(filen, perf, 'date4vc','mode_season and forecast_params', '100*{abs_real_eod_error}', @(d)datestr(d, 'dd/mm/yyyy'))
    % >      
    
    
    latex('formula', filen,'\newpage')
    latex('part', filen, 'R�sum� pour le Closing fixing');
    latex('section', filen, 'Titre par titre');
    latex('subsection', filen, 'Biais');
    my_mean(filen, perf, 'security_id','mode_season and forecast_params', '{cfv_error}');
    latex('subsection', filen, 'MAPE');
    my_mean(filen, perf, 'security_id','mode_season and forecast_params', 'abs({cfv_error})');
    latex('section', filen, 'P�riode par p�riode');
    latex('subsection', filen, 'Biais');
    my_mean(filen, perf, 'date4vc', 'mode_season and forecast_params', '{cfv_error}',  @(d)datestr(d, 'dd/mm/yyyy'));
    latex('subsection', filen, 'MAPE');
    my_mean(filen, perf, 'date4vc', 'mode_season and forecast_params', 'abs({cfv_error})',  @(d)datestr(d, 'dd/mm/yyyy'));
    
    latex('part', filen, 'R�sum� pour le Pourcentage Volume');
    latex('section', filen, 'Titre par titre');
    my_mean(filen, perf, 'security_id','mode_season and forecast_params', 'abs({next_volume_error})');
    latex('section', filen, 'P�riode par p�riode');
    my_mean(filen, perf, 'date4vc', 'mode_season and forecast_params', 'abs({next_volume_error})',  @(d)datestr(d, 'dd/mm/yyyy'));
    % > *
    
    
    %< * Perfomrance sp�cifiquement pour el vwap sur certain jours
    latex('part', filen, 'Specific wvap performance');
    all_security_id=unique(perf.value(:,strcmp(perf.colnames,'security_id')));
    all_dates=unique(perf.date);
    march_special_ny_days=is_context_march_special_ny('security_id',all_security_id(1),'date',all_dates);
    wd=witching_days('FCE',all_dates);
    wd_3=wd==3;
    wd_2=wd==2;
    wd_1=wd==1;
    latex('section', filen, 'Hypothetical (abs) slippage (considered volume constant) ');
    % < Crit�re op�rationnel
    latex('subsection', filen, 'Witching days 3');
    if any(wd_3)
        perf_wd3= st_data('from-idx', perf, ismember(perf.date,all_dates(wd_3)));
        latex('text', filen, sprintf('Nb days : %d',length(find(wd_3))));
        my_weighted_volume_constant(filen,perf_wd3,'abs({slippage})');
    end
    % >
    latex('subsection', filen, 'Witching days 2');
    if any(wd_2)
        perf_wd2= st_data('from-idx', perf, ismember(perf.date,all_dates(wd_2)));
        latex('text', filen, sprintf('Nb days : %d',length(find(wd_2))));
        my_weighted_volume_constant(filen,perf_wd2,'abs({slippage})');
    end

    latex('subsection', filen, 'Witching days 1');
    if any(wd_1)
        perf_wd1= st_data('from-idx', perf, ismember(perf.date,all_dates(wd_1)));
        latex('text', filen, sprintf('Nb days : %d',length(find(wd_1))));
        my_weighted_volume_constant(filen,perf_wd1,'abs({slippage})');
    end
    
    latex('subsection', filen, 'March special NY');
    if any(march_special_ny_days)
        perf_msny= st_data('from-idx', perf, ismember(perf.date,all_dates(march_special_ny_days)));
        latex('text', filen, sprintf('Nb days : %d',length(find(march_special_ny_days))));
        my_weighted_volume_constant(filen,perf_msny,'abs({slippage})');
    end
    
    
    
    if nargin > 1
        latex('part', filen, 'Profil moyen d''�x�cution');
        list_msafp = unique(st_data('cols', perf, 'mode_season and forecast_params'));
        fixed_nb_bins = 150;
        names_msafp = cellfun(@(c)strtrim(regexprep(regexprep(convs('safe_str', c), '~|"|{||}', ''), '_|-', ' ')), codebook( 'get', perf.codebook, 'mode_season and forecast_params'), 'uni', false);
        for i = 1 : length(sec_ids)
            latex('section', filen, cell2mat(get_ric_from_sec_id(sec_ids(i))));
            this_secrealized = bin_separator(volume_to_proportion(st_data('keep', st_data('where', data_aux, sprintf('{security_id}==%d', sec_ids(i))), 'volume')));
            these_vwaps = bin_separator(st_data('keep', st_data('where', data_aux, sprintf('{security_id}==%d', sec_ids(i))), 'vwap'));
            these_vwaps = these_vwaps.value;
            for j = 1 : size(these_vwaps, 1)
                these_vwaps(j, :) = fill_nan_v(these_vwaps(j, :));
            end
            daily_vwaps = sum(this_secrealized.value .* these_vwaps, 2);
            turnover = daily_vwaps .* sum(this_secrealized.value, 2);
            p = 1e4*sum(abs(1-sum(bsxfun(@times, median(this_secrealized.value)/sum(median(this_secrealized.value)), these_vwaps), 2)./daily_vwaps).*turnover) / sum(turnover);
            latex('text', filen, sprintf('Performance en utilisant le profile moyen r�alis� : <%2.2f>', p));
            for j = 1 : length(list_msafp)
                latex('subsection', filen, names_msafp{j});
                this_msafp = st_data('where', perf, sprintf('({mode_season and forecast_params}==%d)&{security_id}==%d', list_msafp(j), sec_ids(i)));
                this_msafp = st_data('cols', this_msafp, tokenize(sprintf('exec_%d;', 1:fixed_nb_bins)));
                this_msafp(:, any(~isfinite(this_msafp), 1)) = [];
                plot(1:size(this_msafp, 2)-1, mean(this_msafp(:,1:end-1)), 'r',1:size(this_msafp, 2)-1, mean(this_secrealized.value(:,1:end-1)), 'b', 'linewidth', 2);
                p = 1e4*sum(abs(1-sum(bsxfun(@times, mean(this_msafp), these_vwaps), 2)./daily_vwaps).*turnover) / sum(turnover);
                title(sprintf('Fixing de cloture, moyenne r�alis�e : <%2.2f>, moyenne du march� : <%2.2f>, (performance avec ce profil moyen : <%2.2f>)\nFixing de cloture, m�diane r�alis�e : <%2.2f>, m�diane du march� : <%2.2f>',...
                    mean(this_msafp(:,end)), mean(this_secrealized.value(:,end)), p, median(this_msafp(:,end)), median(this_secrealized.value(:,end))));
                latex('graphic', filen);
            end
        end
    end
    % < Compilation du fichier
    
else
    
    latex('text',filen,'no perf');
    
end




latex('end', filen);
latex('compile', filen);
% >


end

function my_mean(filen, data, crit1, crit2, val_n, h_fct_row)
if nargin < 6
    h_fct_row = @get_ric_from_sec_id;
end
prop_dist = st_data('accumarray', data, crit1, crit2, sprintf('mean(%s)', val_n));
vals = [prop_dist.value;mean(prop_dist.value)];
[tmp, idx] = sort(vals(end, :), 2);
vals = vals(:, idx);
prop_dist.colnames = prop_dist.colnames(idx);
latex('array', filen, vals, ...
    'linesHeader', cat(1, transpose_ifnot_col(cellflat(cellfun(@(d)h_fct_row(str2double(d)), prop_dist.rownames, 'uni', false))), 'over all'), ...
    'bold_matrix', 'min', ...
    'col_header', regexprep(regexprep(prop_dist.colnames, '~|"|{||}', ''), '_|-', ' '), 'maxcols', 8);

end

function my_func(filen, data, crit1, crit2, val_n, func2apply, h_fct_row)
if nargin < 7
    h_fct_row = @get_ric_from_sec_id;
end
prop_dist = st_data('accumarray', data, crit1, crit2, sprintf('%s(%s)', func2apply, val_n));
vals = [prop_dist.value;mean(prop_dist.value)];
[tmp, idx] = sort(vals(end, :), 2);
vals = vals(:, idx);
prop_dist.colnames = prop_dist.colnames(idx);
latex('array', filen, vals, ...
    'linesHeader', cat(1, transpose_ifnot_col(cellflat(cellfun(@(d)h_fct_row(str2double(d)), prop_dist.rownames, 'uni', false))), 'over all'), ...
    'bold_matrix', 'min', ...
    'col_header', regexprep(regexprep(prop_dist.colnames, '~|"|{||}', ''), '_|-', ' '), 'maxcols', 8);
end

function my_quantile(filen, data, crit1, crit2, val_n,quant, h_fct_row)
if nargin < 7
    h_fct_row = @get_ric_from_sec_id;
end
prop_dist = st_data('accumarray', data, crit1, crit2, sprintf('quantile(%s,%s)',val_n,quant));
vals = [prop_dist.value;mean(prop_dist.value)];
[tmp, idx] = sort(vals(end, :), 2);
vals = vals(:, idx);
prop_dist.colnames = prop_dist.colnames(idx);
latex('array', filen, vals, ...
    'linesHeader', cat(1, transpose_ifnot_col(cellflat(cellfun(@(d)h_fct_row(str2double(d)), prop_dist.rownames, 'uni', false))), 'over all'), ...
    'bold_matrix', 'min', ...
    'col_header', regexprep(regexprep(prop_dist.colnames, '~|"|{||}', ''), '_|-', ' '), 'maxcols', 8);

end



function my_weighted_mean(filen, data, crit1, crit2, val_n, weigth_n, h_fct_row)
if nargin < 7
    h_fct_row = @get_ric_from_sec_id;
end
prop_dist = st_data('accumarray', data, crit1, crit2, sprintf('sum(%s.*%s)', val_n, weigth_n));
tmp = st_data('accumarray', data, crit1, crit2, sprintf('sum(%s)', weigth_n));
over_all = sum(prop_dist.value) ./ sum(tmp.value);
[over_all, idx] = sort(over_all);
prop_dist.value = prop_dist.value./ tmp.value;
prop_dist.value = prop_dist.value(:, idx);
prop_dist.colnames = prop_dist.colnames(idx);

latex('array', filen, [prop_dist.value;over_all], ...
    'linesHeader', cat(1, transpose_ifnot_col(cellflat(cellfun(@(d)h_fct_row(str2double(d)), prop_dist.rownames, 'uni', false))), 'over all weighted'), ...
    'bold_matrix', 'min', ...
    'col_header', regexprep(regexprep(prop_dist.colnames, '~|"|{||}', ''), '_|-', ' '), 'maxcols', 8);


end

function my_weighted_mean_v2(filen, data, crit1, crit2, val_n, weigth_n, h_fct_row)
if nargin < 7
    h_fct_row = @get_ric_from_sec_id;
end
prop_dist = st_data('accumarray', data, crit1, crit2, sprintf('nansum(%s.*%s)', val_n, weigth_n));
tmp = st_data('accumarray', data, crit1, crit2, sprintf('sum(%s)', weigth_n));
over_all = sum(prop_dist.value) ./ sum(tmp.value);
[over_all, idx] = sort(over_all);
prop_dist.value = prop_dist.value./ tmp.value;
prop_dist.value = prop_dist.value(:, idx);
prop_dist.colnames = prop_dist.colnames(idx);

latex('array', filen, [prop_dist.value;over_all], ...
    'linesHeader', cat(1, transpose_ifnot_col(cellflat(cellfun(@(d)h_fct_row(str2double(d)), prop_dist.rownames, 'uni', false))), 'over all weigted'), ...
    'bold_matrix', 'min', ...
    'col_header', regexprep(regexprep(prop_dist.colnames, '~|"|{||}', ''), '_|-', ' '), 'maxcols', 8);


end

% -----my_weighted_volume_constant
% we consider that each day of the backtesting period, we executed the same volume on each stock
% -> for one stock : daily slippage is then weigthed by the vwap daily price
% in order to aggregate at index level, we give the aggregation weigthed by
% the turnover of each stock
function my_weighted_volume_constant(filen,data,val_n)

h_fct_row = @get_ric_from_sec_id;
prop_dist = st_data('accumarray', data, 'security_id','mode_season and forecast_params',...
    sprintf('nansum(%s.*%s)', val_n, '{vwap}'));
tmp_vwap = st_data('accumarray', data,'security_id','mode_season and forecast_params', sprintf('sum(%s)','{vwap}'));
tmp_turnover = st_data('accumarray', data,'security_id','mode_season and forecast_params', sprintf('sum(%s)','{turnover}'));
prop_dist_all=prop_dist.value./ tmp_vwap.value;
over_all = (tmp_turnover.value./repmat(sum(tmp_turnover.value),size(tmp_turnover.value,1),1)).*(prop_dist_all)  ;
[over_all, idx] = sort(sum(over_all));
prop_dist_all=prop_dist_all(:, idx);
prop_dist.colnames = prop_dist.colnames(idx);

latex('array', filen, [prop_dist_all;over_all], ...
    'linesHeader', cat(1, transpose_ifnot_col(cellflat(cellfun(@(d)h_fct_row(str2double(d)), prop_dist.rownames, 'uni', false))), 'over all weigted'), ...
    'bold_matrix', 'min', ...
    'col_header', regexprep(regexprep(prop_dist.colnames, '~|"|{||}', ''), '_|-', ' '), 'maxcols', 8);

end





function v=transpose_ifnot_col(v)
k = size(v, 2);
if k ~= 1
    v = v';
end
end