function data = vcdata2diff_ic(vcdata, cl)
% sec_id = 110;
%
% d = se_run_volume_curve(sec_id, [], 'day', 180, '01/07/2008')
% d2 = vcse2vcdata(d, datenum(0,0,0,0,5,0))
%
% data = vcdata2diff_ic(d2)
%
%

% �a marche mal.... normal c'est une correction en dur...
%
% opt = options({'step:time', [char(165) 'datenum(0,0,0,0,5,0)'], ...
%    'window:time', [char(165) 'datenum(0,0,0,0,5,0)'], ...
%    'bins4stop:b', false});
%
% volume = read_dataset(['gi:basic_indicator/' opt2str(opt)],...
%        'security_id', sec_id, 'from', '02/01/2008', 'to', '01/07/2008', 'trading-destinations', [], ...
%        'last_formula', '{volume}', 'where_formula', '~{auction}');
%
% prop = volume_to_proportion(volume)
% prop = bin_separator(prop);
% prop = prop.value';
%
% failure_prop = [];
% for i = 1 : size(data.value, 2)
%   failure_prop(:, i) = mean(bsxfun(@gt, prop, data.value(:, i)), 2);
% end
%
% plot(failure_prop)
if nargin < 2
    cl = 0.25;
end

vcdata = st_data('from-idx', vcdata, ~(vcdata.value(:, 2)|vcdata.value(:, 3)|vcdata.value(:, 4)));

m = vcdata.value(:, 1);

alpha = vcdata.info.ci.alpha;
alpha = alpha./(m.*(1-m)*5).^0.5;% j'ai plaid� pour faire mieux que cela, et j'ai mieux � proposer,
% mais il parait que pour l'instant on aime bien les corrections �crites en dur
% pour le faire, il faudrat rajouter un param�tre aux courbe de volume

vals = NaN(length(m), 2*length(cl)+1);
vals(:, length(cl)+1) = betainv(0.5, m.*alpha, (1-m).*alpha);
for i = 1 : length(cl)
    vals(:, i) = betainv(cl, m.*alpha, (1-m).*alpha);
    vals(:, length(cl) + 1 + i) = betainv(1-cl, m.*alpha, (1-m).*alpha);
end
colnames = tokenize(sprintf('q_%g;', 100*cl));
colnames = cat(2, colnames, 'center', tokenize(sprintf('q_%g;', 100*(1-cl))));


data = struct('value', vals, 'date', vcdata.date, ...
    'title', 'Courbe de volume diff�renci�e et ses intervalles de confiance', ...
    'colnames', {colnames}, 'attach_format', 'HH:MM');