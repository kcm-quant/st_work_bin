function vcdata = vcse2vcdata(vcse, bin_duration)
% vcse2vcdata - passage d'une sortie du SE � un st data dat�
% 
% Attention l'horaire du dernier slice du continu n'est pas forc�lent
% correct, il faudrait rajouter un param�tre end_of_last_slice pour qu'il
% le soit.
% 
% ex : de ce probl�me 
%   a=se_run_volume_curve(16832, [], 'day', 180, '30/06/2010')
%   b=vcse2vcdata(a,datenum(0,0,0,0,5,0))
%   datestr(b.date, 'HH:MM:SS.FFF') % => le dernier interval du continu devrait �tre 17h28 et pas 17h30

if length(vcse.colnames)>1
    vcdata = [];
    for c=1:length(vcse.colnames)
        tmp_data = vcse2vcdata(st_data('keep', vcse, vcse.colnames{c}), bin_duration);
        if isempty( vcdata)
            vcdata = tmp_data;
        else
            vcdata = st_data('add-col', vcdata, tmp_data.value(:,1), vcse.colnames{c});
        end
    end
    return
end

param_name_from_se = vcse.rownames;
param_value_from_se = vcse.value;

nb_params = length(param_name_from_se);

slices_b = false(nb_params, 1);
for i = 1 : nb_params
    if length(param_name_from_se{i}) > 5 && strcmp(param_name_from_se{i}(1:5), 'slice')
        slices_b(i) = true;
    end
end

eofs = param_value_from_se(strmatch('end_of_first_slice', param_name_from_se, 'exact'), 1);

slices_values = param_value_from_se(slices_b, :);
slices_date = NaN(length(param_value_from_se(slices_b, :)), 1);
j = 1;
for i = 1 : nb_params
    if slices_b(i)
        slices_date(j) = eofs + (str2double(param_name_from_se{i}(6:end))-1)*bin_duration;
        j = j + 1;
    end
end

last_continuous_idx = j-1;

auc_param_name = {'auction_open', 'auction_mid', 'auction_close'};
auc_colnames = {'opening_auction', 'intraday_auction', 'closing_auction'};
for i = 1 : length(auc_param_name)
    idx = strmatch(auc_param_name{i}, param_name_from_se, 'exact');
    if i == 2 && isempty(idx)
        idx = strmatch('auction_mid2', param_name_from_se, 'exact');
        auc_param_name{i} = 'auction_mid2';
    end
    if ~isempty(idx)
        idx_h = strmatch([auc_param_name{i}, '_closing'], param_name_from_se, 'exact');
        if ~isempty(idx_h)
            slices_values(end+1, :) = [param_value_from_se(idx, :) false(1, size(slices_values, 2)-1)];
            slices_values(:, end+1) = [false(size(slices_values, 1)-1, 1);true];
            slices_date(end+1, :) = param_value_from_se(idx_h, 1);
            % PATCH d�gueu pour avoir le dernier horaire du continu qui
            % sera inf�rieur � l'horaire du fixing de fermeture
            if slices_date(end) <= slices_date(last_continuous_idx);
                slices_date(last_continuous_idx) = slices_date(last_continuous_idx)-0.5/24/3600;
            end
        else
            error('se_check_volume_curve:check_params_consistency', 'Found one auction parameter without any stamp_time : <%s>', auc_param_name{idx});
        end
    else
        slices_values(:, end+1) = false(size(slices_values, 1), 1);
    end
end

[slices_date, idxes] = sort(slices_date);
slices_values = slices_values(idxes, :);

vcdata = st_data('init', 'date', slices_date, 'value', slices_values, 'title', 'se volume curve', 'colnames', cat(2, vcse.colnames, auc_colnames));

if isfield( vcse, 'info')
    vcdata.info = vcse.info;
end
vcdata.attach_format = 'HH:MM';

try
    idx=strmatch('alpha', param_name_from_se, 'exact');
    vcdata.info.ci.alpha = param_value_from_se(idx);
catch
    
end