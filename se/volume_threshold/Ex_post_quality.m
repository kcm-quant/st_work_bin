function res = Ex_post_quality (security_id, trading_destination_id, window_type, window_width, as_of_day,data_thres) 
% Ex_post_quality - 

% res = Ex_post_quality (get_index('secid', 'FTE.PA'), [],[], 20, datestr( today-8, 'dd/mm/yyyy'),0)

% author   : 'mimel@cheuvreux.com'
% reviewer : ''
% version  : '1.0'
% date     : ''

% See also enr_pics_volumes_test,Volumes_max4SE

% TODO utiliser trading_destination_id, window_type, window_width

sec = [];
sec.security_id = security_id;
sec.trading_destination_id = trading_destination_id;

date   =  as_of_day;           % TODO voir si date doit entrer en argument

% Resultats
Thres = st_data('cols',data_thres,'Threshold',1);

 res    = enr_pics_volumes_test( [], sec,as_of_day,Thres);

