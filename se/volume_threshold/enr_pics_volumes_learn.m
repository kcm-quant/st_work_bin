function out = enr_pics_volumes_learn( gr, sec_id, trading_dest, dt_from, dt_to, dt_remove,varargin_)
% ENRICH_SEUILS_MAX_LEARN - 
%
% out = enr_pics_volumes_learn( gr, ric, dt_from, dt_to)
%
% exemple:
% out = enr_pics_volumes_learn( [], 'FTE.PA', '01/02/2008', '01/04/2008')
%
% designed to work with SE-seuils-volumes.mat
%
% See also enr_pics_volumes_test

if isempty( gr)
    gr = st_grapher('load', 'volume_threshold.mat');
end

gr.set_param( 'tickdb', 'mode:char', 'all');
gr.set_param( 'learn max', 'learn-mode', 'exec');
gr.set_param( 'learn max', 'plot:b', 0);

% set the varargin.
if nargin>5
  if ~isempty(varargin_)
    succ = set_gr_options(gr,varargin_);
  end
end

if nargin > 3
       gr.set_param('tickdb','RIC:char',sec_id);      
       gr.set_param('tickdb','trading-destinations', trading_dest);
end
if nargin > 1
    gr.set_param('tickdb','from:date',dt_from);
end
if nargin > 1
    gr.set_param('tickdb','to:date',dt_to);
end

gr.set_param('volume 2 capital','nonans:b',0);
gr.set_param('tickdb','plot:b',0);
        
gr.set_param('learn max','plot:b',0);

gr.init();
gr.next();

out = gr.get_outputs( 'learn max', 1);
% 'Threshold', 'Threshold opt 1', 'Threshold opt 2', 'ATS', 'Turnover'


% transpose the st_data such as 
% the colnames are the contexts and the rownames are the params.
run_quality = st_data( 'cols',out, 'run_quality');
out = st_data('remove', out, 'run_quality');
out = st_data('transpose', out);
out.colnames = {'context1'};
out.info.run_quality = run_quality;

gr.set_param( 'tickdb', 'mode:char', 'day');
gr.set_param( 'learn max', 'method', 'learn');
gr.set_param( 'learn max', 'plot:b', 1);

