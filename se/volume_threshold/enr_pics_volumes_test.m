function out = enr_pics_volumes_test( gr, sec_id, trading_dest, window_type, window_width, date_test, thres, plot_b)

% ENRICH_SEUILS_MAX_TEST - 
%
% out = enr_pics_volumes_test( [], security_id,
% trading_destination_id,window_type, window_width, as_of_day,thres) / 100)
%
% exemple:
% out = enr_pics_volumes_test( [], 26,4, 1, 1, '06/05/2008', 12000, 1 )
%
% designed to work with SE-seuils-volumes.mat
%
% See also enr_pics_volumes_learn

if isempty( gr)
    gr = st_grapher('load', 'volume_threshold.mat');
end

if nargin > 1
    gr.set_param('tickdb','RIC:char',sec_id);      
    gr.set_param('tickdb','trading-destinations', trading_dest);
    
end
if nargin>4 % value
    gr.set_param( 'tickdb', 'window:char',  sprintf('day|%d',window_width));
end
if nargin>5 % date
    gr.set_param('tickdb','from:dd/mm/yyyy',date_test)
    gr.set_param('tickdb','to:dd/mm/yyyy'  ,date_test)
end
if nargin>6 % value
    gr.set_param( 'learn max', 'test-threshold', thres);
end
if nargin>7 % plot
    gr.set_param( 'learn max', 'plot:b', plot_b);
else
    gr.set_param( 'learn max', 'plot:b', false);
end
gr.set_param('volume 2 capital','nonans:b',0);
gr.set_param( 'tickdb', 'mode:char', 'day');
gr.set_param( 'learn max', 'learn-mode', 'learn');

gr.init();
gr.next();

out = gr.get_outputs( 'learn max', 3);
out = out.info.learnmax.nb_out ;
