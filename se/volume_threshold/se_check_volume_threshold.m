function res = se_check_volume_threshold (security_id, trading_destination_id, window_type, window_width, as_of_day,data_thres) 
% se_check_volume_threshold - 
%
% example
%  data = se_run_volume_threshold(110, { 'MAIN' }, 'day', 14, '01/06/2008', [], [])
%  res  = se_check_volume_threshold( 110, {}, [], [], datestr( today-8, 'dd/mm/yyyy'), data)

% author   : edarchimbaud@cheuvreux.com
% reviewer : ''
% version  : '1.0'
% date     : ''

% See also enr_pics_volumes_test,Volumes_max4SE


% Resultats
thres = st_data('cols', st_data('transpose', data_thres),'Threshold',1);

% tanh to normalize the check value
 % BE CAREFUL: a greater value of run_quality cannot be interpreted as a
% better performance
res    = tanh(enr_pics_volumes_test( [], security_id, trading_destination_id, window_type, window_width, as_of_day,thres) / 100);

