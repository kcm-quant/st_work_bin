function out = se_run_volume_threshold(security_id, trading_destination_id, window_type, window_width, as_of_date, ...
    dt_remove, varargin_)
% SE_RUN_VOLUME_THRESHOLD - Run de l'estimateur Volume curves
%
% se_run_volume_threshold(110, { 'MAIN' }, 'day', 14, '01/06/2008', [], [])
%

% author   : 'clehalle@cheuvreux.com'
% reviewer : 'edarchibaud@cheuvreux.com'
% version  : '1'
% date     : '23/10/2008'
%
% TODO ex_ante quality

% pb de remove!
% idem varargin_




out = read_dataset(['gi:volume_threshold/' 'window:char' char(167) '"', sprintf('%s|%d', window_type, window_width)], ...
    'security_id', security_id, ...
    'trading-destinations', trading_destination_id, ...
    'from', as_of_date, ...
    'to', as_of_date);

if st_data('isempty-nl', out)
    error('NODATA_FILTER: No data for this job.')
end

run_quality = st_data( 'cols',out, 'run_quality');
out = st_data('remove', out, 'run_quality');
out = st_data('transpose', out);
out.colnames = {'context1'};
% tanh is user to normalize run_quality value
% BE CAREFUL: a greater value of run_quality cannot be interpreted as a
% better performance
out.info.run_quality = tanh(run_quality / 100);