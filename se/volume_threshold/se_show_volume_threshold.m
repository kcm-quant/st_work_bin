function se_show_volume_threshold(mode,varargin)
% SE_SHOW_VOLUME_THRESHOLD - fonction aasoci�e � volume_threshold pour
%    l'introspection par le SE
%
% example:
%   show_volume_threshold( 'run', 'security_id', 110, 'trading-destinations',4, ...
%               'window_width',16,'window_type',1,'as_of_date','03/10/2008' );
%   show_volume_threshold( 'check', 'security_id', 110, 'trading-destinations',4, ...
%               'window_width',1,'window_type',1,'as_of_date','03/10/2008' , 'data', thres_data);
%
% todo:
%   �a vaudrait le coup de voir comment utiliser les set_params_* pour les
%   show...
%
% See also set_params_volume_threshold

switch lower(mode)
    case 'run'
        %<* Mode run
        opt_run = options({'security_id',110,'trading-destinations',4,'window_width',1,'window_type',1,'as_of_date','','varargin',''},varargin);
        
        security_id = opt_run.get('security_id');
        trading_destination_id = opt_run.get('trading-destinations');
        window_width = opt_run.get('window_width');
        window_type = opt_run.get('window_type');
        
        as_of_date   =  opt_run.get('as_of_date');
        if ischar( as_of_date)
            endd = datenum(as_of_date,'dd/mm/yyyy');
        else
            endd = as_of_date;
        end
        begind = endd - window_width;
        BeginD = datestr(begind,'dd/mm/yyyy');
        EndD   = datestr(endd,'dd/mm/yyyy');
        varargin_ = opt_run.get('varargin');

        gr = st_grapher('load', 'volume_threshold.mat');

        gr.set_param('tickdb','RIC:char',security_id);
        gr.set_param('tickdb','trading-destinations', trading_destination_id);
        gr.set_param('volume 2 capital','nonans:b',0);
        gr.set_param('tickdb', 'from:dd/mm/yyyy',BeginD);
        gr.set_param('tickdb', 'to:dd/mm/yyyy',EndD);
        gr.set_node_param(  'tickdb', 'source:char', 'btickdb'); % attention je n'ai pas les remove date
        gr.set_param('learn max', 'test-threshold',[]);
        gr.set_param('learn max', 'plot:b',1);
        if ~isempty( varargin_)
            set_gr_options(gr,varargin_);
        end
        win_graph('show',gr);
        %>*
    case 'check'
        %<* Mode check
        opt_check = options({'security_id',110,'trading-destinations',4,'window_width',1,'window_type',1,'as_of_date','','data',st_data( 'empty-init')},varargin);

        security_id  = opt_check.get('security_id');
        trading_destination_id = opt_check.get('trading-destinations');
        window_width = opt_check.get('window_width');
        window_type = opt_check.get('window_type');
        as_of_date =  opt_check.get('as_of_date');
        data_thres = opt_check.get('data');

        gr = st_grapher('load', 'volume_threshold.mat');

        gr.set_param('tickdb','RIC:char',security_id);
        gr.set_param('tickdb','trading-destinations', trading_destination_id);
        gr.set_param('volume 2 capital','nonans:b',0);
        gr.set_param('tickdb', 'from:dd/mm/yyyy', as_of_date);
        gr.set_param('tickdb', 'to:dd/mm/yyyy', as_of_date);
        Thres = st_data('cols',data_thres,'Threshold',1);
        gr.set_param('learn max', 'test-threshold', Thres);
        gr.set_param('learn max', 'plot:b', 1);
        win_graph('show',gr);
        %>*
    otherwise
        error('Show_Volume_threshold:Show_Volume_threshold','mode <%s> unknown',mode);
end

end