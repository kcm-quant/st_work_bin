function res = volumes_max4SE(security_id, trading_destination_id, window_type, window_width,as_of_day,dt_remove,varargin_)
% volumes_max4SE - 
 
% res = volumes_max4SE( 110, [], [], 20, datestr( today-1, 'dd/mm/yyyy'))

% author   : 'mimel@cheuvreux.com'
% reviewer : ''
% version  : '1.0'
% date     : '' 
 
% See also enr_pics_volumes_learn, Ex_post_quality

% TODO utiliser trading_destination_id, window_type, window_width

sec = [];
sec.security_id = security_id;
sec.trading_destination_id = trading_destination_id;
date   =  as_of_day;           % TODO voir si date doit entrer en argument
endd   = datenum(date,'dd/mm/yyyy');
begind = endd - window_width;
BeginD = datestr(begind,'dd/mm/yyyy');
EndD   = datestr(endd,'dd/mm/yyyy');

res    = enr_pics_volumes_learn([], sec, BeginD, EndD,dt_remove,varargin_);
